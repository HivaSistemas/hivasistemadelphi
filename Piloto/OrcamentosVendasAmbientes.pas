unit OrcamentosVendasAmbientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.ExtCtrls,
  _HerancaBarra, _Biblioteca, Vcl.StdCtrls, EditLuka, Vcl.Buttons, FrameVendedores, _FrameHerancaPrincipal, _Sessao, System.Math, _FrameHenrancaPesquisas, FrameCondicoesPagamento,
  FrameClientes, Vcl.Grids, PesquisaProdutosVenda, GridLuka, _Produtos, _RecordsCadastros, _RecordsEspeciais, _Imagens, _RecordsOrcamentosVendas, _ProdutosVenda, ComboBoxLuka,
  Vcl.ComCtrls, _Orcamentos, _OrcamentosItens, _OrcamentosPagamentos, FrameFechamento, _OrcamentosPagamentosCheques, _ExibirMensagemMemo, System.StrUtils, _OrcamentosBloqueios,
  Buscar.ProdutosRelacionados, Impressao.OrcamentoGraficoAmbiente, _ContasReceber, Pesquisa.Orcamentos, Vcl.Mask, EditTelefoneLuka, EditLukaData, EditHoras, Buscar.ProdutosRetirarAto,
  DateUtils, MixVendas, _HerancaCadastro, Vcl.Menus, Legendas, Informacoes.ProdutosComposicaoKit, BuscaDados, FrameEndereco, SelecionarVariosOpcoes, CheckBoxLuka, _ContasPagar,
  _CondicoesPagamento, BuscarPrecoManual, FrameIndicesDescontosVenda, FrameProfissionais, FrameArquivos, _ClientesCondicPagtoRestrit, _CodigoBarras, _Clientes,
  EditCpfCnpjLuka, ImpressaoComprovanteEntregaGrafico, _RelacaoEntregasPendentes,
  Data.DB, Datasnap.DBClient, frxClass, frxDBSet;

type
  TFormOrcamentosVendasAmbientes = class(TFormHerancaCadastroCodigo)
    FrCondicoesPagamento: TFrCondicoesPagamento;
    FrVendedor: TFrVendedores;
    FrCliente: TFrClientes;
    lb8: TLabel;
    eOverPrice: TEditLuka;
    pcDados: TPageControl;
    tsProdutos: TTabSheet;
    tsFechamento: TTabSheet;
    lb1: TLabel;
    lb2: TLabel;
    lb4: TLabel;
    lb6: TLabel;
    sgProdutos: TGridLuka;
    stQuantidadeItens: TStaticText;
    stQuantidadeTotalItens: TStaticText;
    stMultiploVenda: TStaticText;
    stValorTotalProdutos: TStaticText;
    eNomeConsumidorFinal: TEditLuka;
    lb7: TLabel;
    sbGravarOrcamento: TSpeedButton;
    lb11: TLabel;
    eTelefoneConsumidorFinal: TEditTelefoneLuka;
    lb10: TLabel;
    cbTipoEntrega: TComboBoxLuka;
    eDataEntrega: TEditLukaData;
    lb12: TLabel;
    eObservacoesCaixa: TMemo;
    lb5: TLabel;
    lb13: TLabel;
    eObservacoesExpedicao: TMemo;
    lb14: TLabel;
    eObservacoesNotaFiscalEletronica: TMemo;
    eHoraEntrega: TEditHoras;
    eValorFrete: TEditLuka;
    sbMixVenda: TSpeedButton;
    pmOpcoesGrid: TPopupMenu;
    miLegendas: TMenuItem;
    miProdutosKit: TMenuItem;
    sbCopiarOrcamento: TSpeedButton;
    FrEnderecoEntrega: TFrEndereco;
    st3: TStaticText;
    sbCarregarEnderecoPrincipalEntrega: TSpeedButton;
    sbCarregarEnderecos: TSpeedButton;
    Label2: TLabel;
    StaticText1: TStaticText;
    sbCadastrarNovoEndereco: TSpeedButton;
    miSeparadorAlteracaoPreco: TMenuItem;
    miDeletarProduto: TMenuItem;
    lb3: TLabel;
    stValorTotalPromocao: TStaticText;
    FrFechamento: TFrFechamento;
    miN1: TMenuItem;
    FrIndiceDescontoVenda: TFrIndicesDescontosVenda;
    FrProfissional: TFrProfissionais;
    FrArquivos: TFrArquivos;
    ckReceberNaEntrega: TCheckBoxLuka;
    StaticText2: TStaticText;
    lblcpfconsumidorfinal: TLabel;
    edtCPFConsumidorFinal: TEditCPF_CNPJ_Luka;
    stTotalFreteProdutos: TStaticText;
    lb15: TLabel;
    Label3: TLabel;
    eObservacoesTitulo: TMemo;
    Label4: TLabel;
    eAmbiente: TEditLuka;
    eCodigo: TEditLuka;
    Label5: TLabel;
    lbNomeProduto: TLabel;
    eNomeProduto: TEditLuka;
    Label6: TLabel;
    eMarca: TEditLuka;
    Label7: TLabel;
    eQuantidade: TEditLuka;
    Label8: TLabel;
    eUnidade: TEditLuka;
    Label9: TLabel;
    ePrecoUnitario: TEditLuka;
    Label10: TLabel;
    eTotalProduto: TEditLuka;
    Label11: TLabel;
    eFrete: TEditLuka;
    Label12: TLabel;
    eEstoque: TEditLuka;
    frxReport: TfrxReport;
    dstReceber: TfrxDBDataset;
    cdsReceber: TClientDataSet;
    cdsReceberCodigo: TStringField;
    cdsNomeProduto: TStringField;
    cdsNomeMarca: TStringField;
    cdsUnidade: TStringField;
    cdsQuantidade: TFloatField;
    cdsPrecoUnitario: TFloatField;
    cdsTotalProduto: TFloatField;
    cdsReceberAdiantamento: TFloatField;
    cdsReceberValorLiquido: TFloatField;
    cdsReceberDtVencto: TDateField;
    cdsReceberDtCadastro: TDateField;
    cdsPrecoUnitarioString: TStringField;
    cdsReceberTipoCobranca: TStringField;
    cdsReceberDiasAtraso: TIntegerField;
    cdsReceberParcela: TStringField;
    SpeedButton1: TSpeedButton;
    cdsReceberEmpresa: TStringField;
    Alterarpreomanua1: TMenuItem;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgProdutosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure eOverPriceExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cbTipoEntregaChange(Sender: TObject);
    procedure sbMixVendaClick(Sender: TObject);
    procedure miLegendasClick(Sender: TObject);
    procedure miProdutosKitClick(Sender: TObject);
    procedure sgProdutosArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sbCopiarOrcamentoClick(Sender: TObject);
    procedure sbCarregarEnderecosClick(Sender: TObject);
    procedure sbCarregarEnderecoPrincipalEntregaClick(Sender: TObject);
    procedure miAlterarPrecoClick(Sender: TObject);
    procedure sgProdutosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure eOverPriceChange(Sender: TObject);
    procedure eValorFreteChange(Sender: TObject);
    procedure miDeletarProdutoClick(Sender: TObject);
    procedure sbCadastrarNovoEnderecoClick(Sender: TObject);
    procedure miAlterarPrecoManualClick(Sender: TObject);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure ckReceberNaEntregaClick(Sender: TObject);
    procedure FrFechamentosbBuscarDadosCartoesDebitoClick(Sender: TObject);
    procedure FrArquivossbArquivosClick(Sender: TObject);
    procedure eCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure eQuantidadeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sgProdutosDblClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Alterarpreomanua1Click(Sender: TObject);
  private
    FBloqueios: TArray<string>;
    FLegendaGrid: TImage;
    FDataMinimaEntrega: TDate;
    FIndiceCondicaoPagamentoPrecoManual: Currency;
    FProdutoAtual: RecProdutosVendas;
    FProdutosAmbiente: TArray<RecAmbientes>;

    procedure PreencherRegistro(pOrcamento: RecOrcamentos);
    procedure ImprimirOrcamento();

    function FindProdutoExisteAmbiente(produtoId: Integer; posicaoAmbiente: Integer): Boolean;
    function FindPosicaoAmbiente(nomeAmbiente: string): Integer;
    function AdicionarProdutos(): Boolean;
    function AdicionarProdutosAmbienteGrid(): Boolean;

    function AdicionarProdutosGrid(
      pProdutoId: Integer;
      pNomeProduto: string;
      pUnidadeVenda: string;
      pQuantidade: Currency;
      pMultiploVenda: Currency;
      pEstoque: Currency;
      pMarca: string;
      pTipoControleEstoque: string;
      pPrecoVarejo: Currency;
      pPrecoPromocional: Currency;
      pPrecoAtacado1: Currency;
      pQtdeMinAtacado1: Double;
      pPrecoAtacado2: Currency;
      pQtdeMinAtacado2: Double;
      pPrecoAtacado3: Currency;
      pQtdeMinAtacado3: Double;
      pFreteProduto: Double;
      pLinha: Integer;
      pPrecoPontaEstoque: Currency;
      pAmbiente: string
    ): Boolean;

    procedure AtualizarDadosProdutosGrid(pUtilizarPrecoVarejoAtual: Boolean);

    procedure AddBloqueio(pMensagem: string);
    procedure RatearValoresItens(pGravando: Boolean);

    procedure CalcularFrete;
    procedure CalcularValoresItens;

    procedure AplicarIndiceAcrescimoCondicao;

    (* Procedimentos dos frames *)
    procedure FrClienteOnAposPesquisar(Sender: TObject);
    procedure FrClienteOnAposDeletar(Sender: TObject);

    procedure FrCondicoesPagamentoOnAposPesquisar(Sender: TObject);
    procedure FrCondicoesPagamentoOnAposDeletar(Sender: TObject);
    (*********************************************)

    procedure AlterarQuantidadeProduto(produtoId: Integer; quantidade: Double; ambiente: string; row: Integer);
    procedure VerificarPrecoUtilizar(pVerificarQuantidade: Boolean);
    function LengendaGrid(
      pSemEstoque: Boolean;
      pVariosPrecos: Boolean;
      pProdutoKit: Boolean;
      pEmPromocao: Boolean
    ): TPicture;

    function getItens: TArray<RecOrcamentoItens>;
    function getIndiceCondicaoPagamento: Double;

    procedure VerificarCondicaoRestrita;
    function getTotalFreteItens: Currency;

    procedure VerificarHabilitarReceberNaEntrega;
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses _Cidades, Opcoes.OrcamentoGrafico, ValidarUsuarioAutorizado, _Funcionarios;

{ TFormOrcamentosVendas }

const
  coLegenda        = 0;
  coProdutoId      = 1;
  coNomeProduto    = 2;
  coMarca          = 3;
  coQuantidade     = 4;
  coUndVenda       = 5;
  coPrecoUnitario  = 6;
  coTotalProduto   = 7;
  coFreteCobrar    = 8;
  coEstoque        = 9;

  (* Colunas ocultas *)
  coMultiploVenda         = 10;
  coValorTotalOutDespProp = 11;
  coValorTotalDescProp    = 12;
  coTipoControleEstoq     = 13;
  coPrecoVarejo           = 14;
  coPrecoPromocional      = 15;
  coPrecoAtacado1         = 16;
  coQtdeMinAtacado1       = 17;
  coPrecoAtacado2         = 18;
  coQtdeMinAtacado2       = 19;
  coPrecoAtacado3         = 20;
  coQtdeMinAtacado3       = 21;
  coTipoPrecoUtilizar     = 22;
  coPrecoBaseUtilizado    = 23;
  coExisteVariosPrecos    = 24;
  coFreteProduto          = 25;
  coValorTotalFreteProp   = 26;
  coPrecoPontaEst         = 27;

  // Colunas para validar o pre�o manual
  coPrecoManual           = 28;
  coPercDescPrecoManual   = 29;
  coValorDescontoPrecoMan = 30;

  coColunaMaxima          = 31;
  coAmbiente              = 32;

  coTipoLinha             = 33;

  // Cores das legendas
  coCorSemEstoque   = clRed;
  coCorKit          = $000096DB;
  coCorVariosPrecos = clBlue;
  coCorPromocao     = clGreen;

procedure TFormOrcamentosVendasAmbientes.AddBloqueio(pMensagem: string);
begin
  if FrCliente.getCliente.IgnorarBloqueiosVenda = 'S' then
    Exit;

  SetLength(FBloqueios, Length(FBloqueios) + 1);
  FBloqueios[High(FBloqueios)] := pMensagem;
end;

function TFormOrcamentosVendasAmbientes.AdicionarProdutos: Boolean;
var
  posAmbiente: Integer;
var
  vLinha: Integer;
  vPosAmbiente: Integer;
  vProdutoExisteAmbiente: Boolean;
  vPosProduto: Integer;
begin
  Result := False;

  vPosAmbiente := FindPosicaoAmbiente(eAmbiente.Text);
  if vPosAmbiente > -1 then begin
    vProdutoExisteAmbiente := FindProdutoExisteAmbiente(FProdutoAtual.produto_id, vPosAmbiente);
    if vProdutoExisteAmbiente then begin
      _Biblioteca.Exclamar('O produto ' + NFormat(FProdutoAtual.produto_id) + ' - ' + FProdutoAtual.nome + ' j� foi adicionado ao ambiente ""' + eAmbiente.Text + '"" !');
      SetarFoco(eCodigo);
      Exit;
    end;    
  end
  else begin
    SetLength(FProdutosAmbiente, Length(FProdutosAmbiente) + 1);
    vPosAmbiente := Length(FProdutosAmbiente) - 1;
    FProdutosAmbiente[vPosAmbiente].NomeAmbiente := eAmbiente.Text;
  end;

  SetLength(FProdutosAmbiente[vPosAmbiente].Produtos, Length(FProdutosAmbiente[vPosAmbiente].Produtos) + 1);
  vPosProduto := Length(FProdutosAmbiente[vPosAmbiente].Produtos) - 1;

  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].ProdutoId := FProdutoAtual.produto_id;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].NomeProduto := FProdutoAtual.nome;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].UndVenda := FProdutoAtual.unidade_venda;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].Quantidade := eQuantidade.AsDouble;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].MultiploVenda := FProdutoAtual.multiplo_venda;   
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].TipoControleEstoq := FProdutoAtual.TipoControleEstoque;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].Estoque := FProdutoAtual.disponivel;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].Marca := FProdutoAtual.nome_marca;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoVarejo := FProdutoAtual.preco_varejo;   
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoPromocional := FProdutoAtual.PrecoPromocional;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoPontaEst := FProdutoAtual.PrecoPontaEstoque;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoAtacado1 := FProdutoAtual.PrecoAtacado1;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].QtdeMinAtacado1 := FProdutoAtual.QuantidadeMinPrecoAtac1;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoAtacado2 := FProdutoAtual.PrecoAtacado2;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].QtdeMinAtacado2 := FProdutoAtual.QuantidadeMinPrecoAtac2;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoAtacado3 := FProdutoAtual.PrecoAtacado3;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].QtdeMinAtacado3 := FProdutoAtual.QuantidadeMinPrecoAtac3;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].FreteProduto := FProdutoAtual.ValorAdicionalFrete;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoBaseUtilizado := FProdutoAtual.PrecoBase;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].TipoPrecoUtilizar := FProdutoAtual.TipoPreco;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoUnitari := FProdutoAtual.PrecoUnitario;

  Result := True;
end;

function TFormOrcamentosVendasAmbientes.AdicionarProdutosGrid(
  pProdutoId: Integer;
  pNomeProduto: string;
  pUnidadeVenda: string;
  pQuantidade: Currency;
  pMultiploVenda: Currency;
  pEstoque: Currency;
  pMarca: string;
  pTipoControleEstoque: string;
  pPrecoVarejo: Currency;
  pPrecoPromocional: Currency;
  pPrecoAtacado1: Currency;
  pQtdeMinAtacado1: Double;
  pPrecoAtacado2: Currency;
  pQtdeMinAtacado2: Double;
  pPrecoAtacado3: Currency;
  pQtdeMinAtacado3: Double;
  pFreteProduto: Double;
  pLinha: Integer;
  pPrecoPontaEstoque: Currency;
  pAmbiente: string
): Boolean;
var
  vLinha: Integer;
  vPosAmbiente: Integer;
  vProdutoExisteAmbiente: Boolean;
  vPosProduto: Integer;
begin
  Result := False;

  //validar se o produto j� foi adicionado no AMBIENTE

  vPosAmbiente := FindPosicaoAmbiente(eAmbiente.Text);
  if vPosAmbiente > -1 then begin
    vProdutoExisteAmbiente := FindProdutoExisteAmbiente(FProdutoAtual.produto_id, vPosAmbiente);
    if vProdutoExisteAmbiente then begin
      _Biblioteca.Exclamar('O produto ' + NFormat(pProdutoId) + ' - ' + pNomeProduto + ' j� foi adicionado ao ambiente ""' + eAmbiente.Text + '"" !');
      SetarFoco(eCodigo);
      Exit;
    end;    
  end
  else begin
    SetLength(FProdutosAmbiente, Length(FProdutosAmbiente) + 1);
    vPosAmbiente := Length(FProdutosAmbiente) - 1;
    FProdutosAmbiente[vPosAmbiente].NomeAmbiente := eAmbiente.Text;
  end;

  SetLength(FProdutosAmbiente[vPosAmbiente].Produtos, Length(FProdutosAmbiente[vPosAmbiente].Produtos) + 1);
  vPosProduto := Length(FProdutosAmbiente[vPosAmbiente].Produtos) - 1;

  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].ProdutoId := FProdutoAtual.produto_id;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].NomeProduto := FProdutoAtual.nome;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].UndVenda := FProdutoAtual.unidade_venda;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].Quantidade := eQuantidade.AsDouble;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].MultiploVenda := FProdutoAtual.multiplo_venda;   
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].TipoControleEstoq := FProdutoAtual.TipoControleEstoque;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].Estoque := FProdutoAtual.estoque;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].Marca := FProdutoAtual.nome_marca;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoVarejo := FProdutoAtual.preco_varejo;   
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoPromocional := FProdutoAtual.PrecoPromocional;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoPontaEst := FProdutoAtual.PrecoPontaEstoque;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoAtacado1 := FProdutoAtual.PrecoAtacado1;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].QtdeMinAtacado1 := FProdutoAtual.QuantidadeMinPrecoAtac1;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoAtacado2 := FProdutoAtual.PrecoAtacado2;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].QtdeMinAtacado2 := FProdutoAtual.QuantidadeMinPrecoAtac2;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].PrecoAtacado3 := FProdutoAtual.PrecoAtacado3;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].QtdeMinAtacado3 := FProdutoAtual.QuantidadeMinPrecoAtac3;
  FProdutosAmbiente[vPosAmbiente].Produtos[vPosProduto].FreteProduto := FProdutoAtual.ValorAdicionalFrete;

  //Corrigir a adi��o dos itens no AMBIENTE
  vLinha := sgProdutos.RowCount - 1;

  sgProdutos.Cells[coProdutoId, vLinha]         := NFormat(pProdutoId);
  sgProdutos.Cells[coNomeProduto, vLinha]       := pNomeProduto;
  sgProdutos.Cells[coUndVenda, vLinha]          := pUnidadeVenda;
  sgProdutos.Cells[coQuantidade, vLinha]        := NFormatNEstoque(pQuantidade);
  sgProdutos.Cells[coMultiploVenda, vLinha]     := NFormatNEstoque(pMultiploVenda);
  sgProdutos.Cells[coTipoControleEstoq, vLinha] := pTipoControleEstoque;
  sgProdutos.Cells[coEstoque, vLinha]           := NFormatNEstoque(pEstoque);
  sgProdutos.Cells[coMarca, vLinha]             := pMarca;
  sgProdutos.Cells[coPrecoVarejo, vLinha]       := NFormat(pPrecoVarejo);
  sgProdutos.Cells[coPrecoPromocional, vLinha]  := NFormatN(pPrecoPromocional);
  sgProdutos.Cells[coPrecoPontaEst, vLinha]     := NFormatN(pPrecoPontaEstoque);
  sgProdutos.Cells[coPrecoAtacado1, vLinha]     := NFormatN(pPrecoAtacado1);
  sgProdutos.Cells[coQtdeMinAtacado1, vLinha]   := NFormatN(pQtdeMinAtacado1, 3);
  sgProdutos.Cells[coPrecoAtacado2, vLinha]     := NFormatN(pPrecoAtacado2);
  sgProdutos.Cells[coQtdeMinAtacado2, vLinha]   := NFormatN(pQtdeMinAtacado2, 3);
  sgProdutos.Cells[coPrecoAtacado3, vLinha]     := NFormatN(pPrecoAtacado3);
  sgProdutos.Cells[coQtdeMinAtacado3, vLinha]   := NFormatN(pQtdeMinAtacado3, 3);
  sgProdutos.Cells[coFreteProduto, vLinha]      := NFormatN(pFreteProduto);
  sgProdutos.Cells[coAmbiente, vLinha]          := pAmbiente;

  Result := True;
end;

procedure TFormOrcamentosVendasAmbientes.Alterarpreomanua1Click(
  Sender: TObject);
var
  i: Integer;
  j: Integer;
  l: Integer;
  vLinha: Integer;
  vIndice: Double;
  vItens: TArray<RecOrcamentoItens>;
  vRetTela: TRetornoTelaFinalizar< TArray<RecOrcamentoItens> >;
begin
  inherited;

  if not Sessao.AutorizadoRotina('alterar_preco_manualmente') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  vItens := getItens;
  if vItens = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    SetarFoco(sgProdutos);
    Exit;
  end;

  vRetTela := BuscarPrecoManual.Buscar( vItens );
  if vRetTela.BuscaCancelada then
    Exit;

  FIndiceCondicaoPagamentoPrecoManual := getIndiceCondicaoPagamento;
  vIndice := getIndiceCondicaoPagamento;

  // Aqui fazer as altera��es de pre�os
  for i := Low(vRetTela.Dados) to High(vRetTela.Dados) do begin
    for j := Low(FProdutosAmbiente) to High(FProdutosAmbiente) do begin
      for l := Low(FProdutosAmbiente[j].Produtos) to High(FProdutosAmbiente[j].Produtos) do begin
        if FProdutosAmbiente[j].Produtos[l].ProdutoId <> vRetTela.Dados[i].produto_id then
          Continue;

        // Pode ser que o pre�o tenha sido limpado, se for voltando ao pre�o normal de varejo.
        if NFormatN( vRetTela.Dados[i].PercDescontoPrecoManual ) = '' then begin
          if FProdutosAmbiente[j].Produtos[l].TipoPrecoUtilizar = 'MAN' then begin
            FProdutosAmbiente[j].Produtos[l].PrecoManual           := 0.00;
            FProdutosAmbiente[j].Produtos[l].TipoPrecoUtilizar     := 'VAR';
            FProdutosAmbiente[j].Produtos[l].PrecoUnitari          := FProdutosAmbiente[j].Produtos[l].PrecoVarejo;
            FProdutosAmbiente[j].Produtos[l].PercDescPrecoManual   := 0.00;
            FProdutosAmbiente[j].Produtos[l].ValorDescontoPrecoMan := 0.00;
          end;
        end
        else begin
          FProdutosAmbiente[j].Produtos[l].PrecoManual           := vRetTela.Dados[i].PrecoManual;
          FProdutosAmbiente[j].Produtos[l].PrecoUnitari          := vRetTela.Dados[i].PrecoManual;
          FProdutosAmbiente[j].Produtos[l].PercDescPrecoManual   := vRetTela.Dados[i].PercDescontoPrecoManual;
          FProdutosAmbiente[j].Produtos[l].TipoPrecoUtilizar     := 'MAN';
          FProdutosAmbiente[j].Produtos[l].ValorDescontoPrecoMan := vRetTela.Dados[i].ValorDescUnitPrecoManual;
        end;

      end;
    end;
  end;

  CalcularValoresItens;
  AdicionarProdutosAmbienteGrid();
  FrFechamento.LimparDadosTiposCobranca(False);
end;

procedure TFormOrcamentosVendasAmbientes.AlterarQuantidadeProduto(
  produtoId: Integer; 
  quantidade: Double; 
  ambiente: string;
  row: Integer
);
var
  idx: Integer;  
  idxProduto: Integer;
  i: Integer;
  vTotalProdutos: Double;
  vTotalFrete: Double;
  vTotalPromocao: Double;
  idxTotal: Integer;
begin
  idx := FindPosicaoAmbiente(ambiente);
  if idx = -1 then
    Exit; 

  vTotalProdutos := 0.00;
  vTotalFrete := 0.00;
  vTotalPromocao := 0.0;
  for i := Low(FProdutosAmbiente[idx].Produtos) to High(FProdutosAmbiente[idx].Produtos) do begin
    if FProdutosAmbiente[idx].Produtos[i].ProdutoId <> produtoId then begin
      vTotalProdutos := vTotalProdutos + FProdutosAmbiente[idx].Produtos[i].ValorTotalProduto;
      vTotalFrete := vTotalFrete + FProdutosAmbiente[idx].Produtos[i].FreteCobrar;

      if FProdutosAmbiente[idx].Produtos[i].TipoPrecoUtilizar = 'PRO' then
        vTotalPromocao := vTotalPromocao + FProdutosAmbiente[idx].Produtos[i].ValorTotalProduto;
              
      Continue;
    end;
    
    //
    FProdutosAmbiente[idx].Produtos[i].Quantidade := quantidade;
    FProdutosAmbiente[idx].Produtos[i].ValorTotalProduto := Arredondar(quantidade * FProdutosAmbiente[idx].Produtos[i].PrecoUnitari, 2);
    sgProdutos.Cells[coTotalProduto, row] := NFormatN(FProdutosAmbiente[idx].Produtos[i].ValorTotalProduto);

    vTotalProdutos := vTotalProdutos + FProdutosAmbiente[idx].Produtos[i].ValorTotalProduto;

    if FProdutosAmbiente[idx].Produtos[i].TipoPrecoUtilizar = 'PRO' then
      vTotalPromocao := vTotalPromocao + FProdutosAmbiente[idx].Produtos[i].ValorTotalProduto;    

    if Em(cbTipoEntrega.GetValor, ['EN', 'SE']) then begin
      FProdutosAmbiente[idx].Produtos[i].FreteCobrar := Arredondar(FProdutosAmbiente[idx].Produtos[i].FreteProduto * quantidade, 2);
      sgProdutos.Cells[coFreteCobrar, row] := NFormatN(FProdutosAmbiente[idx].Produtos[i].FreteCobrar);
      vTotalFrete := vTotalFrete + FProdutosAmbiente[idx].Produtos[i].FreteCobrar;
    end
    else begin
      FProdutosAmbiente[idx].Produtos[i].FreteCobrar := 0;
      sgProdutos.Cells[coFreteCobrar, row] := '';
    end;
  end;

  if Em(cbTipoEntrega.GetValor, ['EN', 'SE']) then
    vTotalFrete := 0.0;  

  for i := sgProdutos.FixedRows to sgProdutos.RowCount - 1 do begin
    if sgProdutos.Cells[coAmbiente, i] <> ambiente then
      Continue;  

    if sgProdutos.Cells[coTipoLinha, i] <> 'TOT' then
      Continue;        

    sgProdutos.Cells[coTotalProduto, i] := NFormatN(vTotalProdutos);
    Exit;
  end;
end;

function TFormOrcamentosVendasAmbientes.AdicionarProdutosAmbienteGrid(): Boolean;
var
  i: Integer;
  j: Integer;
  vLinha: Integer;
  vPosAmbiente: Integer;
  vProdutoExisteAmbiente: Boolean;
  vPosProduto: Integer;
  vTotalAmbiente: Double;
begin
  Result := False;

  sgProdutos.ClearGrid();

  vLinha := 0;
  for j := Low(FProdutosAmbiente) to High(FProdutosAmbiente) do begin
    vLinha := vLinha + 1;
    sgProdutos.Cells[coNomeProduto, vLinha]       := FProdutosAmbiente[j].NomeAmbiente;
    sgProdutos.Cells[coTipoLinha, vLinha]         := 'AMB';
    vLinha := vLinha + 1;
    
    vTotalAmbiente := 0;
    for i := Low(FProdutosAmbiente[j].Produtos) to High(FProdutosAmbiente[j].Produtos) do begin
      sgProdutos.Cells[coProdutoId, vLinha]         := NFormat(FProdutosAmbiente[j].Produtos[i].ProdutoId);
      sgProdutos.Cells[coNomeProduto, vLinha]       := FProdutosAmbiente[j].Produtos[i].NomeProduto;
      sgProdutos.Cells[coUndVenda, vLinha]          := FProdutosAmbiente[j].Produtos[i].UndVenda;
      sgProdutos.Cells[coQuantidade, vLinha]        := NFormatNEstoque(FProdutosAmbiente[j].Produtos[i].Quantidade);
      sgProdutos.Cells[coMultiploVenda, vLinha]     := NFormatNEstoque(FProdutosAmbiente[j].Produtos[i].MultiploVenda);
      sgProdutos.Cells[coTipoControleEstoq, vLinha] := FProdutosAmbiente[j].Produtos[i].TipoControleEstoq;
      sgProdutos.Cells[coEstoque, vLinha]           := NFormatNEstoque(FProdutosAmbiente[j].Produtos[i].Estoque);
      sgProdutos.Cells[coMarca, vLinha]             := FProdutosAmbiente[j].Produtos[i].Marca;
      sgProdutos.Cells[coPrecoVarejo, vLinha]       := NFormat(FProdutosAmbiente[j].Produtos[i].PrecoVarejo);
      sgProdutos.Cells[coPrecoPromocional, vLinha]  := NFormatN(FProdutosAmbiente[j].Produtos[i].PrecoPromocional);
      sgProdutos.Cells[coPrecoPontaEst, vLinha]     := NFormatN(FProdutosAmbiente[j].Produtos[i].PrecoPontaEst);
      sgProdutos.Cells[coPrecoAtacado1, vLinha]     := NFormatN(FProdutosAmbiente[j].Produtos[i].PrecoAtacado1);
      sgProdutos.Cells[coQtdeMinAtacado1, vLinha]   := NFormatN(FProdutosAmbiente[j].Produtos[i].QtdeMinAtacado1, 3);
      sgProdutos.Cells[coPrecoAtacado2, vLinha]     := NFormatN(FProdutosAmbiente[j].Produtos[i].PrecoAtacado2);
      sgProdutos.Cells[coQtdeMinAtacado2, vLinha]   := NFormatN(FProdutosAmbiente[j].Produtos[i].QtdeMinAtacado2, 3);
      sgProdutos.Cells[coPrecoAtacado3, vLinha]     := NFormatN(FProdutosAmbiente[j].Produtos[i].PrecoAtacado3);
      sgProdutos.Cells[coQtdeMinAtacado3, vLinha]   := NFormatN(FProdutosAmbiente[j].Produtos[i].QtdeMinAtacado3, 3);
      sgProdutos.Cells[coFreteProduto, vLinha]      := NFormatN(FProdutosAmbiente[j].Produtos[i].FreteProduto);
      sgProdutos.Cells[coPrecoUnitario, vLinha]     := NFormatN(FProdutosAmbiente[j].Produtos[i].PrecoUnitari);
      sgProdutos.Cells[coTotalProduto, vLinha]      := NFormatN(FProdutosAmbiente[j].Produtos[i].ValorTotalProduto);
      sgProdutos.Cells[coTipoPrecoUtilizar, vLinha] := FProdutosAmbiente[j].Produtos[i].TipoPrecoUtilizar; 
      sgProdutos.Cells[coPrecoBaseUtilizado, vLinha] := NFormatN(FProdutosAmbiente[j].Produtos[i].PrecoBaseUtilizado);

      sgProdutos.Cells[coPrecoManual, vLinha]           := NFormat(FProdutosAmbiente[j].Produtos[i].PrecoManual);
//      sgProdutos.Cells[coPrecoUnitario, vLinha]         := NFormat(FProdutosAmbiente[j].Produtos[i].PrecoManual);
      sgProdutos.Cells[coPercDescPrecoManual, vLinha]   := NFormatN(FProdutosAmbiente[j].Produtos[i].PercDescPrecoManual, 5);
      sgProdutos.Cells[coValorDescontoPrecoMan, vLinha] := NFormat(FProdutosAmbiente[j].Produtos[i].ValorDescontoPrecoMan);

//      if FProdutosAmbiente[j].Produtos[i].PercDescPrecoManual = 0.00 then begin
//        if sgProdutos.Cells[coTipoPrecoUtilizar, vLinha] = 'MAN' then begin
//          sgProdutos.Cells[coPrecoManual, vLinha]           := '';
//          sgProdutos.Cells[coPercDescPrecoManual, vLinha]   := '';
//          sgProdutos.Cells[coValorDescontoPrecoMan, vLinha] := '';
//        end;
//      end
//      else begin
//        sgProdutos.Cells[coPrecoManual, vLinha]           := NFormat(vRetTela.Dados[i].PrecoManual);
//        sgProdutos.Cells[coPrecoUnitario, vLinha]         := NFormat(vRetTela.Dados[i].PrecoManual);
//        sgProdutos.Cells[coPercDescPrecoManual, vLinha]   := NFormatN(vRetTela.Dados[i].PercDescontoPrecoManual, 5);
//        sgProdutos.Cells[coTipoPrecoUtilizar, vLinha]     := 'MAN';
//        sgProdutos.Cells[coValorDescontoPrecoMan, vLinha] := NFormat(vRetTela.Dados[i].ValorDescUnitPrecoManual);
//      end;

      sgProdutos.Cells[coAmbiente, vLinha]          := FProdutosAmbiente[j].NomeAmbiente;    
      sgProdutos.Cells[coTipoLinha, vLinha]         := 'DET';

      vTotalAmbiente := vTotalAmbiente + FProdutosAmbiente[j].Produtos[i].ValorTotalProduto;
      vLinha := vLinha + 1;
    end;

    sgProdutos.Cells[coAmbiente, vLinha]            := FProdutosAmbiente[j].NomeAmbiente;     
    sgProdutos.Cells[coMarca, vLinha]               := 'TOTAL: ';   
    sgProdutos.Cells[coTipoLinha, vLinha]           := 'TOT'; 
    sgProdutos.Cells[coTotalProduto, vLinha]        := NFormatN(vTotalAmbiente);
  end;
  
  if vLinha = 0 then
    sgProdutos.RowCount := 2
  else
    sgProdutos.RowCount := vLinha + 1;
  Result := True;
end;

procedure TFormOrcamentosVendasAmbientes.AplicarIndiceAcrescimoCondicao;
var
  i: Integer;
  j, k: Integer;
  vIndiceCondicao: Currency;
  vIndicePromocional: Currency;
  vAplicarIndicePromocao: Boolean;
begin
  vIndiceCondicao := 1;
  vIndicePromocional := 1;
  vAplicarIndicePromocao := False;

  if not FrCondicoesPagamento.EstaVazio then begin
    vIndiceCondicao        := FrCondicoesPagamento.getDados().indice_acrescimo;
    vIndicePromocional     := FrCondicoesPagamento.getDados().getIndiceAcrescimoPrecoPromocional;
    vAplicarIndicePromocao := FrCondicoesPagamento.getDados().AplicarIndPrecoPromocional = 'S';
  end;

  vIndiceCondicao := vIndiceCondicao + eOverPrice.AsDouble - 1;
  for i := Low(FProdutosAmbiente) to High(FProdutosAmbiente) do begin
    for j := Low(FProdutosAmbiente[i].Produtos) to High(FProdutosAmbiente[i].Produtos) do begin
      if FProdutosAmbiente[i].Produtos[j].TipoPrecoUtilizar = 'PRO' then begin
        if vAplicarIndicePromocao then
          FProdutosAmbiente[i].Produtos[j].PrecoUnitari := Arredondar(FProdutosAmbiente[i].Produtos[j].PrecoBaseUtilizado * vIndicePromocional, 2)
        else
          FProdutosAmbiente[i].Produtos[j].PrecoUnitari := FProdutosAmbiente[i].Produtos[j].PrecoBaseUtilizado;
      end
      else if FProdutosAmbiente[i].Produtos[j].TipoPrecoUtilizar = 'MAN' then begin
        if (not FrCondicoesPagamento.EstaVazio) and (FIndiceCondicaoPagamentoPrecoManual <> vIndiceCondicao) then
          FProdutosAmbiente[i].Produtos[j].PrecoUnitari :=  Arredondar(FProdutosAmbiente[i].Produtos[j].PrecoBaseUtilizado * vIndiceCondicao, 2);
      end
      else
        FProdutosAmbiente[i].Produtos[j].PrecoUnitari := Arredondar(FProdutosAmbiente[i].Produtos[j].PrecoBaseUtilizado * vIndiceCondicao, 2)
    end;
  end;

//  for i := sgProdutos.FixedRows to sgProdutos.RowCount - 1 do begin
//    // Se for o pre�o da promo��o aplicando a regra especifica
//    if sgProdutos.Cells[coTipoPrecoUtilizar, i] = 'PRO' then begin
//      if vAplicarIndicePromocao then
//        sgProdutos.Cells[coPrecoUnitario, i] := NFormatN( Arredondar(SFormatDouble(sgProdutos.Cells[coPrecoBaseUtilizado, i]) * vIndicePromocional, 2) )
//      else
//        sgProdutos.Cells[coPrecoUnitario, i] := sgProdutos.Cells[coPrecoBaseUtilizado, i];
//    end
//    else if sgProdutos.Cells[coTipoPrecoUtilizar, i] = 'MAN' then begin
//      if (not FrCondicoesPagamento.EstaVazio) and (FIndiceCondicaoPagamentoPrecoManual <> vIndiceCondicao) then
//        sgProdutos.Cells[coPrecoUnitario, i] := NFormatN( Arredondar(SFormatDouble(sgProdutos.Cells[coPrecoBaseUtilizado, i]) * vIndiceCondicao, 2) );
//    end
//    else
//      sgProdutos.Cells[coPrecoUnitario, i] := NFormatN( Arredondar(SFormatDouble(sgProdutos.Cells[coPrecoBaseUtilizado, i]) * vIndiceCondicao, 2) );
//  end;

  CalcularValoresItens;
//  sgProdutos.Repaint;
end;

procedure TFormOrcamentosVendasAmbientes.AtualizarDadosProdutosGrid(pUtilizarPrecoVarejoAtual: Boolean);
var
  i: Integer;
  j: Integer;
  k: Integer;
  vLinha: Integer;
  vProdutosIds: TArray<Integer>;
  vProdutos: TArray<RecProdutosVendas>;
begin
  vProdutosIds := nil;
  for i := 1 to sgProdutos.RowCount -1 do begin
    if sgProdutos.Cells[coProdutoId, i] = '' then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgProdutos.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then
    Exit;

  vProdutos :=
    _ProdutosVenda.BuscarProdutosVendasComando(
      Sessao.getConexaoBanco,
      'and EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId) + ' ' +
      'and ' + FiltroInInt('PRODUTO_ID', vProdutosIds)
    );

  for i := Low(vProdutos) to High(vProdutos) do begin
    for j := Low(FProdutosAmbiente) to High(FProdutosAmbiente) do begin
      for k := Low(FProdutosAmbiente[j].Produtos) to High(FProdutosAmbiente[j].Produtos) do begin
        if FProdutosAmbiente[j].Produtos[k].ProdutoId <> vProdutos[i].produto_id then
          Continue;

        if pUtilizarPrecoVarejoAtual then begin
          FProdutosAmbiente[j].Produtos[k].PrecoVarejo := vProdutos[i].preco_varejo;
          FProdutosAmbiente[j].Produtos[k].PrecoBaseUtilizado := vProdutos[i].preco_varejo;
          FProdutosAmbiente[j].Produtos[k].TipoPrecoUtilizar := 'VAR';
        end;

        FProdutosAmbiente[j].Produtos[k].Estoque := vProdutos[i].disponivel;
        FProdutosAmbiente[j].Produtos[k].PrecoVarejo := vProdutos[i].preco_varejo;
        FProdutosAmbiente[j].Produtos[k].PrecoPromocional := vProdutos[i].PrecoPromocional;
        FProdutosAmbiente[j].Produtos[k].PrecoAtacado1 := vProdutos[i].PrecoAtacado1;
        FProdutosAmbiente[j].Produtos[k].QtdeMinAtacado1 := vProdutos[i].QuantidadeMinPrecoAtac1;
        FProdutosAmbiente[j].Produtos[k].PrecoAtacado2 := vProdutos[i].PrecoAtacado2;
        FProdutosAmbiente[j].Produtos[k].QtdeMinAtacado2 := vProdutos[i].QuantidadeMinPrecoAtac2;
        FProdutosAmbiente[j].Produtos[k].PrecoAtacado3 := vProdutos[i].PrecoAtacado3;
        FProdutosAmbiente[j].Produtos[k].QtdeMinAtacado3 := vProdutos[i].QuantidadeMinPrecoAtac3;
        FProdutosAmbiente[j].Produtos[k].FreteProduto := vProdutos[i].ValorAdicionalFrete;
      end;
    end;
  end;

  if pUtilizarPrecoVarejoAtual then
    AplicarIndiceAcrescimoCondicao;
end;

procedure TFormOrcamentosVendasAmbientes.BuscarRegistro;
var
  vOrcamento: TArray<RecOrcamentos>;
begin
  vOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 9, [eID.AsInt]);

  if vOrcamento = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  if vOrcamento[0].status = co_orcamento_cancelado then begin
    if not _Biblioteca.Perguntar('Este or�amento est� cancelado, deseja realmente utiliz�-lo?') then
      Exit;
  end
  else if vOrcamento[0].status = co_orcamento_bloqueado then begin
    _Biblioteca.Exclamar('Este or�amento est� bloqueado!');
    Exit;
  end
  else if vOrcamento[0].status <> co_orcamento_em_aberto then begin
    _Biblioteca.Exclamar('Este or�amento n�o est� em aberto, por favor, verifique!');
    SetarFoco(eID);
    eID.Clear;
    Exit;
  end;

  inherited;
  PreencherRegistro(vOrcamento[0]);
end;

procedure TFormOrcamentosVendasAmbientes.CalcularFrete;
var
  i: Integer;
  j: Integer;
  vFreteCapa: Double;
  vFreteProdutos: Double;

  vQuantidade: Double;
  vPrecoProduto: Double;
begin
//  RA
//  RE
//  EN
//  SP
//  SE

  vFreteCapa := eValorFrete.AsDouble;

  // Frete por produto
  vFreteProdutos := 0;
  for j := Low(FProdutosAmbiente) to High(FProdutosAmbiente) do begin
    for i := Low(FProdutosAmbiente[j].Produtos) to High(FProdutosAmbiente[j].Produtos) do begin  
      if Em(cbTipoEntrega.GetValor, ['EN', 'SE']) then begin
        vQuantidade := FProdutosAmbiente[j].Produtos[i].Quantidade;
        FProdutosAmbiente[j].Produtos[i].FreteCobrar := Arredondar(FProdutosAmbiente[j].Produtos[i].FreteProduto * vQuantidade, 2);

        vFreteProdutos := vFreteProdutos + FProdutosAmbiente[j].Produtos[i].FreteCobrar; 
      end
      else begin
        FProdutosAmbiente[j].Produtos[i].FreteCobrar := 0;
      end;
    end;
  end;

  stTotalFreteProdutos.Caption := NFormatN(vFreteProdutos);
  FrFechamento.ValorFrete := vFreteCapa + vFreteProdutos;
end;

procedure TFormOrcamentosVendasAmbientes.CalcularValoresItens;
var
  i: Integer;
  j: Integer;
  vQtdeItens: Integer;

  vValorTotalProdutos: Double;
  vValorTotalPromocao: Double;
  vTotalQuantidades: Double;
  vQuantidade: Double;
  vPrecoUnitario: Double;
begin
  vQtdeItens := 0;
  stValorTotalProdutos.Caption := '';
  stQuantidadeTotalItens.Caption := '';

  vValorTotalProdutos := 0;
  vValorTotalPromocao := 0;
  vTotalQuantidades := 0;

  for j := Low(FProdutosAmbiente) to High(FProdutosAmbiente) do begin
    for i := Low(FProdutosAmbiente[j].Produtos) to High(FProdutosAmbiente[j].Produtos) do begin
      vQuantidade := FProdutosAmbiente[j].Produtos[i].Quantidade;
      vPrecoUnitario := FProdutosAmbiente[j].Produtos[i].PrecoUnitari; 
      FProdutosAmbiente[j].Produtos[i].ValorTotalProduto := Arredondar(vQuantidade * vPrecoUnitario, 2);
      vValorTotalProdutos := vValorTotalProdutos + FProdutosAmbiente[j].Produtos[i].ValorTotalProduto; 

      if FProdutosAmbiente[j].Produtos[i].TipoPrecoUtilizar = 'PRO' then
        vValorTotalPromocao := vValorTotalPromocao + FProdutosAmbiente[j].Produtos[i].ValorTotalProduto;

      vTotalQuantidades := vTotalQuantidades + vQuantidade;
      Inc(vQtdeItens);
    end;
  end;

  CalcularFrete;

  stQuantidadeItens.Caption := NFormat(vQtdeItens);
  stValorTotalProdutos.Caption   := NFormatN( vValorTotalProdutos );
  stValorTotalPromocao.Caption   := NFormatN( vValorTotalPromocao );
  stQuantidadeTotalItens.Caption := NFormatNEstoque( vTotalQuantidades );

  FrFechamento.setTotalProdutos( vValorTotalProdutos );
  FrFechamento.setTotalPromocional( vValorTotalPromocao );
end;

procedure TFormOrcamentosVendasAmbientes.cbTipoEntregaChange(Sender: TObject);
var
  vCidade: TArray<RecCidades>;
begin
  inherited;
  _Biblioteca.Habilitar([
    eDataEntrega,
    eHoraEntrega,
    eValorFrete,
    sbCarregarEnderecos,
    sbCarregarEnderecoPrincipalEntrega,
    sbCadastrarNovoEndereco],
    (Em(cbTipoEntrega.GetValor, ['EN', 'SP', 'SE'])) or (not FrCliente.EstaVazio and (FrCliente.getCliente(0).tipo_cliente = 'PR'))
  );

  if Em(cbTipoEntrega.GetValor, ['EN', 'SE']) then begin
    if not FrEnderecoEntrega.FrBairro.EstaVazio then begin
      if (Sessao.getParametrosEmpresa.CalcularFreteVendaPorKM = 'S') and (FrEnderecoEntrega.FrBairro.GetBairro.cidade_id > 0) then begin
        vCidade := _Cidades.BuscarCidades(Sessao.getConexaoBanco, 0, [FrEnderecoEntrega.FrBairro.GetBairro.cidade_id]);

        if vCidade = nil then
          Exit;

        if vCidade[0].valor_calculo_frete_km > 0 then
          eValorFrete.AsDouble := vCidade[0].valor_calculo_frete_km * Sessao.getParametrosEmpresa.ValorFretePadraoVenda;
      end;
    end
    else
      eValorFrete.AsDouble := Sessao.getParametrosEmpresa.ValorFretePadraoVenda;

    VerificarHabilitarReceberNaEntrega;
  end;

  CalcularFrete;
end;

procedure TFormOrcamentosVendasAmbientes.ckReceberNaEntregaClick(Sender: TObject);
begin
  inherited;
  if ckReceberNaEntrega.Checked then
    FrFechamento.setRecebimentoNaEntrega
  else
    FrFechamento.setCondicaoPagamento(FrCondicoesPagamento.getDados().condicao_id, FrCondicoesPagamento.getDados().nome, FrCondicoesPagamento.getDados().PrazoMedio);
end;

procedure TFormOrcamentosVendasAmbientes.eOverPriceChange(Sender: TObject);
begin
  inherited;
  AplicarIndiceAcrescimoCondicao;
end;

procedure TFormOrcamentosVendasAmbientes.eOverPriceExit(Sender: TObject);
begin
  inherited;
  if eOverPrice.AsCurr < 1 then begin
    _Biblioteca.Exclamar('O indice de over price n�o pode ser menor que 1!');
    SetarFoco(eOverPrice);
    Abort;
  end;
end;

procedure TFormOrcamentosVendasAmbientes.eQuantidadeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);

  procedure AddProdutoRelacionado(const pProdutoPrincipalId: Integer);
  var
    i: Integer;
    vProdutosRelacionados: TArray<RecProdutosRelacionados>;
    vProdutos: TArray<RecProdutosVendas>;
    vLinha: Integer;
  begin
    vProdutosRelacionados := Buscar.ProdutosRelacionados.Buscar(pProdutoPrincipalId);
    for i := Low(vProdutosRelacionados) to High(vProdutosRelacionados) do begin
      vLinha := sgProdutos.RowCount;
      vProdutos :=
        _ProdutosVenda.BuscarProdutosVendas(
          Sessao.getConexaoBanco,
          0,
          [Sessao.getEmpresaLogada.EmpresaId, vProdutosRelacionados[i].ProdutoRelacionadoId],
          False
        );

      if
        AdicionarProdutosGrid(
          vProdutos[0].produto_id,
          vProdutos[0].nome,
          vProdutos[0].unidade_venda,
          vProdutosRelacionados[i].QuantidadeAdicionar,
          vProdutos[0].multiplo_venda,
          vProdutos[0].disponivel,
          vProdutos[0].nome_marca,
          vProdutos[0].TipoControleEstoque,
          vProdutos[0].preco_varejo,
          vProdutos[0].PrecoPromocional,
          vProdutos[0].PrecoAtacado1,
          vProdutos[0].QuantidadeMinPrecoAtac1,
          vProdutos[0].PrecoAtacado2,
          vProdutos[0].QuantidadeMinPrecoAtac2,
          vProdutos[0].PrecoAtacado3,
          vProdutos[0].QuantidadeMinPrecoAtac3,
          vProdutos[0].ValorAdicionalFrete,
          vLinha,
          vProdutos[0].PrecoPontaEstoque,
          eAmbiente.Text
        )
      then
        sgProdutos.RowCount := sgProdutos.RowCount + 1;
    end;
  end;
begin
  inherited;
  if Key = VK_RETURN then begin
    if eAmbiente.Text = '' then begin
      Exclamar('Para inserir um produto � necess�rio primeiro definir um Ambiente.');
      SetarFoco(eAmbiente);
      Exit;
    end;

    if eQuantidade.AsDouble = 0 then begin
      Exclamar('Para inserir um produto � necess�rio primeiro definir a quantidade.');
      SetarFoco(eQuantidade);
      Exit;
    end;

    if not ValidarMultiplo(eQuantidade.AsDouble, FProdutoAtual.multiplo_venda) then begin
      eQuantidade.AsDouble := 0.000;
      Exit;
    end
    else
      VerificarPrecoUtilizar(true);

    if AdicionarProdutos() then begin
      CalcularValoresItens;
    end;

    if AdicionarProdutosAmbienteGrid() then begin
      //sgProdutos.RowCount := sgProdutos.RowCount + 1;
      //sgProdutos.Col := coQuantidade;
      eCodigo.Clear;
      eNomeProduto.Clear;
      eMarca.Clear;
      eQuantidade.Clear;
      eUnidade.Clear;
      ePrecoUnitario.Clear;
      eTotalProduto.Clear;
      eFrete.Clear;
      eEstoque.Clear;
      
      SetarFoco(eCodigo);

      // Veriricando os pre�os
      //VerificarPrecoUtilizar(sgProdutos.RowCount, False, False);
      //CalcularValoresItens;

      // TEM QUE ARRUMAR PARA ADICIONAR PRODUTOS RELACIONADOS
      //AddProdutoRelacionado(FProdutoAtual.produto_id);
    end;
  end;
end;

procedure TFormOrcamentosVendasAmbientes.eCodigoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  vProdutos: TArray<RecProdutosVendas>;

  procedure AdicionarProdutos;
  var
    i: Integer;
    vLinha: Integer;
    vCodigoPesquisa: string;
    vProdutos: TArray<RecProdutosVendas>;

    procedure AddProdutoRelacionado(const pProdutoPrincipalId: Integer);
    var
      i: Integer;
      vProdutosRelacionados: TArray<RecProdutosRelacionados>;
    begin
      vProdutosRelacionados := Buscar.ProdutosRelacionados.Buscar(pProdutoPrincipalId);
      for i := Low(vProdutosRelacionados) to High(vProdutosRelacionados) do begin
        vLinha := sgProdutos.RowCount;
        vProdutos :=
          _ProdutosVenda.BuscarProdutosVendas(
            Sessao.getConexaoBanco,
            0,
            [Sessao.getEmpresaLogada.EmpresaId, vProdutosRelacionados[i].ProdutoRelacionadoId],
            False
          );

        if
          AdicionarProdutosGrid(
            vProdutos[0].produto_id,
            vProdutos[0].nome,
            vProdutos[0].unidade_venda,
            vProdutosRelacionados[i].QuantidadeAdicionar,
            vProdutos[0].multiplo_venda,
            vProdutos[0].disponivel,
            vProdutos[0].nome_marca,
            vProdutos[0].TipoControleEstoque,
            vProdutos[0].preco_varejo,
            vProdutos[0].PrecoPromocional,
            vProdutos[0].PrecoAtacado1,
            vProdutos[0].QuantidadeMinPrecoAtac1,
            vProdutos[0].PrecoAtacado2,
            vProdutos[0].QuantidadeMinPrecoAtac2,
            vProdutos[0].PrecoAtacado3,
            vProdutos[0].QuantidadeMinPrecoAtac3,
            vProdutos[0].ValorAdicionalFrete,
            vLinha,
            vProdutos[0].PrecoPontaEstoque,
            eAmbiente.Text
          )
        then
          sgProdutos.RowCount := sgProdutos.RowCount + 1;
      end;
    end;

  begin
    if FrCondicoesPagamento.EstaVazio then begin
      Exclamar('Para fazer a pesquisa de produtos, � necess�rio selecionar primeiramente a condi��o de pagamento!');
      SetarFoco(FrCondicoesPagamento);
      Abort;
    end;

    if eCodigo.AsInt = 0 then
      vProdutos := PesquisaProdutosVenda.PesquisarProdutosVendas(FrCondicoesPagamento.getDados.condicao_id, False, False, False)
    else begin
      vProdutos :=
        _ProdutosVenda.BuscarProdutosVendas(
          Sessao.getConexaoBanco,
          0,
          [Sessao.getEmpresaLogada.EmpresaId, eCodigo.AsInt],
          False
        );
    end;

    if vProdutos = nil then
      Exit;

    // Adicionando produto nos campos
    FProdutoAtual := vProdutos[0];
    VerificarPrecoUtilizar(false);

    eCodigo.AsInt := FProdutoAtual.produto_id; 
    eNomeProduto.Text := FProdutoAtual.nome; 
    eMarca.Text := FProdutoAtual.nome_marca; 
    eQuantidade.AsDouble := 0.0; 
    eUnidade.Text := FProdutoAtual.unidade_venda; 
    ePrecoUnitario.AsDouble := FProdutoAtual.PrecoUnitario; 
    eEstoque.AsDouble := FProdutoAtual.disponivel;
    SetarFoco(eQuantidade);
  end;

begin
  inherited;

  if Key = VK_RETURN then
    AdicionarProdutos
end;

procedure TFormOrcamentosVendasAmbientes.eValorFreteChange(Sender: TObject);
begin
  inherited;
  CalcularFrete;
end;

function TFormOrcamentosVendasAmbientes.FindProdutoExisteAmbiente(
  produtoId: Integer; posicaoAmbiente: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;

  if FProdutosAmbiente[posicaoAmbiente].Produtos = nil then
    Exit;

  for i := Low(FProdutosAmbiente[posicaoAmbiente].Produtos) to High(FProdutosAmbiente[posicaoAmbiente].Produtos) do begin
    if FProdutosAmbiente[posicaoAmbiente].Produtos[i].ProdutoId <> produtoId then
      Continue;
    
    Result := True;
    Exit;
  end;
end;

function TFormOrcamentosVendasAmbientes.FindPosicaoAmbiente(nomeAmbiente: string): Integer;
var
  i: Integer;
begin
  Result := -1;

  if FProdutosAmbiente = nil then
    Exit;

  for i := Low(FProdutosAmbiente) to High(FProdutosAmbiente) do begin
    if FProdutosAmbiente[i].NomeAmbiente <> nomeAmbiente then
      Continue;

    Result := i;
    Exit;
  end;
end;

procedure TFormOrcamentosVendasAmbientes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  FLegendaGrid.Free;
end;

procedure TFormOrcamentosVendasAmbientes.FormCreate(Sender: TObject);
begin
  inherited;

  if Sessao.getEmpresaLogada.TipoEmpresa = 'D' then begin
    _Biblioteca.RotinaNaoPermitidaDepositoFechado;
    Close;
  end;

  FBloqueios := nil;
  pcDados.ActivePage := tsProdutos;

  sgProdutos.Col := coProdutoId;

  FrCliente.OnAposDeletar   := FrClienteOnAposDeletar;
  FrCliente.OnAposPesquisar := FrClienteOnAposPesquisar;

  FrCondicoesPagamento.OnAposDeletar   := FrCondicoesPagamentoOnAposDeletar;
  FrCondicoesPagamento.OnAposPesquisar := FrCondicoesPagamentoOnAposPesquisar;

  FrFechamento.TelaChamada := ttOrcamentosVendas;

  miSeparadorAlteracaoPreco.Visible := Sessao.getParametrosEmpresa.TipoPrecoUtilizarVenda = 'DMA';
  FrIndiceDescontoVenda.Visible     := Sessao.AutorizadoRotina('definir_indice_desconto_venda');

  FLegendaGrid := TImage.Create(Self);
  FLegendaGrid.Height := 15;
  FrCondicoesPagamento.FSomenteEmpresaLogada := True;
end;

procedure TFormOrcamentosVendasAmbientes.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  case Key of
    VK_F6: if sbGravarOrcamento.Enabled then sbGravarOrcamento.Click;
    VK_F8: if sbMixVenda.Enabled then sbMixVenda.Click;
  end;
end;

procedure TFormOrcamentosVendasAmbientes.FrArquivossbArquivosClick(Sender: TObject);
begin
  inherited;
  FrArquivos.sbArquivosClick(Sender);

end;

procedure TFormOrcamentosVendasAmbientes.FrClienteOnAposDeletar(Sender: TObject);
var
  i: Integer;
begin
  _Biblioteca.Habilitar([eNomeConsumidorFinal, edtCPFConsumidorFinal, eTelefoneConsumidorFinal, FrIndiceDescontoVenda], False);
  FrEnderecoEntrega.Clear;

  FrFechamento.eValorCredito.ReadOnly := True;
  FrFechamento.CadastroId := 0;

  // Limpando os pre�os de atacado se neces�rio
  for i := 1 to sgProdutos.RowCount -1 do begin
    if not Em(sgProdutos.Cells[coTipoPrecoUtilizar, i], ['AT1', 'AT2', 'AT3']) then
      Continue;

    sgProdutos.Cells[coPrecoUnitario, i]      := sgProdutos.Cells[coPrecoVarejo, i];
    sgProdutos.Cells[coTipoPrecoUtilizar, i]  := 'VAR';
    sgProdutos.Cells[coPrecoBaseUtilizado, i] := sgProdutos.Cells[coPrecoVarejo, i];
    AplicarIndiceAcrescimoCondicao;
  end;

  cbTipoEntregaChange(nil);
end;

procedure TFormOrcamentosVendasAmbientes.FrClienteOnAposPesquisar(Sender: TObject);
var
  vEntregas: TArray<RecEntregasPendentes>;
  vMsgEntregas: string;
begin
  if FrCliente.getCliente.ListaNegraId > 0 then
    _Biblioteca.Informar('O cliente selecionado possui restri��es financeiros.');

  if Sessao.getParametrosEmpresa.AlertaEntregaPendente = 'S' then begin
    vEntregas := _RelacaoEntregasPendentes.BuscarEntregas(
      Sessao.getConexaoBanco,
      ' WHERE CLIENTE_ID = ' + IntToStr(FrCliente.getCliente.cadastro_id)
    );

    if Length(vEntregas) > 0 then begin
      if Length(vEntregas) = 1 then
        vMsgEntregas := IntToStr(Length(vEntregas)) + ' entrega pendente.'
      else
        vMsgEntregas := IntToStr(Length(vEntregas)) + ' entregas pendentes.';

      _Biblioteca.Informar('O cliente selecionado possui ' + vMsgEntregas);
    end;
  end;

  if
    _ContasReceber.BloquearVendaPorQtdeDiasTitulosVencidos(
      Sessao.getConexaoBanco,
      FrCliente.getCliente().cadastro_id,
      Sessao.getParametrosEmpresa.QtdeDiasBloqVendaTitVenc
    )
  then
    _Biblioteca.Informar('O cliente selecionado possui t�tulos financeiros vencidos.');

  if _Clientes.ValidadeLimiteCreditoVencida(Sessao.getConexaoBanco, FrCliente.getCliente().cadastro_id) then
    _Biblioteca.Informar('O cliente selecionado est� com a data de validade do limite de cr�dito vencida.');

    if FrCliente.getCliente.ObservacoesVenda <> '' then
    _ExibirMensagemMemo.Mensagem('Observa��es do cliente', FrCliente.getCliente.ObservacoesVenda);

  if FrCliente.getCliente().cadastro_id = Sessao.getParametros.cadastro_consumidor_final_id then begin
    _Biblioteca.Habilitar([eNomeConsumidorFinal, edtCPFConsumidorFinal, eTelefoneConsumidorFinal], True);
    eNomeConsumidorFinal.Text := 'CONSUMIDOR FINAL';

    _Biblioteca.Habilitar([FrIndiceDescontoVenda], False);
  end
  else
    _Biblioteca.Habilitar([cbTipoEntrega, FrIndiceDescontoVenda], True, False);

  if FrCondicoesPagamento.EstaVazio and (FrCliente.getCliente.condicao_id > 0) then
    FrCondicoesPagamento.InserirDadoPorChave(FrCliente.getCliente.condicao_id, False);

  //if FrVendedor.EstaVazio and (FrCliente.getCliente().vendedor_id > 0) then  antes era assim mudado por ezequiel a proxima linha
    if ((FrVendedor.EstaVazio) or (FrCliente.getCliente().vendedor_id <> FrVendedor.getVendedor().funcionario_id)) and (FrCliente.getCliente().vendedor_id > 0) then
    FrVendedor.InserirDadoPorChave(FrCliente.getCliente.vendedor_id, False);



  FrIndiceDescontoVenda.InserirDadoPorChave( FrCliente.getCliente.IndiceDescontoVendaId, False );
  FrFechamento.CadastroId := FrCliente.getCliente().cadastro_id;
  FrEnderecoEntrega.Clear;

  cbTipoEntregaChange(nil);
end;

procedure TFormOrcamentosVendasAmbientes.FrCondicoesPagamentoOnAposDeletar(Sender: TObject);
begin
  FrFechamento.LimparDadosTiposCobranca(True);
end;

procedure TFormOrcamentosVendasAmbientes.FrCondicoesPagamentoOnAposPesquisar(Sender: TObject);
var
  i, k: Integer;
  j: Integer;
begin
  VerificarHabilitarReceberNaEntrega;

  FrFechamento.setCondicaoPagamento(
    FrCondicoesPagamento.getDados.condicao_id,
    FrCondicoesPagamento.getDados.nome,
    FrCondicoesPagamento.getDados.PrazoMedio
  );

  for i := Low(FProdutosAmbiente) to High(FProdutosAmbiente) do begin
    for j := Low(FProdutosAmbiente[i].Produtos) to High(FProdutosAmbiente[i].Produtos)  do begin
      if
        ((FrCondicoesPagamento.getDados.PermitirPrecoPromocional = 'N') and (FProdutosAmbiente[i].Produtos[j].TipoPrecoUtilizar = 'PRO'))
        or
        ((not Em(FrCondicoesPagamento.getDados().TipoPreco, ['A', 'T'])) and (Em(FrCondicoesPagamento.getDados().TipoPreco, ['AT1', 'AT2', 'AT3'])))
      then begin
        FProdutosAmbiente[i].Produtos[j].PrecoUnitari := FProdutosAmbiente[i].Produtos[j].PrecoVarejo;
        FProdutosAmbiente[i].Produtos[j].PrecoBaseUtilizado := FProdutosAmbiente[i].Produtos[j].PrecoVarejo;
        FProdutosAmbiente[i].Produtos[j].TipoPrecoUtilizar := 'VAR';
      end;
    end;
  end;

  AplicarIndiceAcrescimoCondicao;
  AdicionarProdutosAmbienteGrid();
end;

procedure TFormOrcamentosVendasAmbientes.FrFechamentosbBuscarDadosCartoesDebitoClick(
  Sender: TObject);
begin
  inherited;
  FrFechamento.sbBuscarDadosCartoesDebitoClick(Sender);

end;

function TFormOrcamentosVendasAmbientes.getIndiceCondicaoPagamento: Double;
begin
  Result := 1;

  if not FrCondicoesPagamento.EstaVazio then
    Result := FrCondicoesPagamento.getDados.indice_acrescimo;

  Result := Result + eOverPrice.AsDouble - 1;
end;

function TFormOrcamentosVendasAmbientes.getItens: TArray<RecOrcamentoItens>;
var
  i: Integer;
  j: Integer;
  idxProd: Integer;
  vIndice: Double;
  itemId: Integer;
begin
  Result := nil;
  vIndice := getIndiceCondicaoPagamento;
  itemId := 0;

  for i := sgProdutos.FixedRows to sgProdutos.RowCount - 1 do begin
    if sgProdutos.Cells[coNomeProduto, i] = '' then
      Continue;

    if sgProdutos.Cells[coTipoLinha, i] <> 'DET' then
      Continue;

    idxProd := -1;
    for j := Low(Result) to High(Result) do begin
      if Result[j].produto_id <> SFormatInt(sgProdutos.Cells[coProdutoId, i]) then
        Continue;

      idxProd := j;
      Break;  
    end;

    if idxProd > -1 then begin
      Result[idxProd].quantidade                  := Result[idxProd].quantidade + _Biblioteca.SFormatDouble(sgProdutos.Cells[coQuantidade, i]);
      Result[idxProd].valor_total_outras_despesas := Result[idxProd].valor_total_outras_despesas + _Biblioteca.SFormatDouble(sgProdutos.Cells[coValorTotalOutDespProp, i]);
      Result[idxProd].valor_total_desconto        := Result[idxProd].valor_total_desconto + _Biblioteca.SFormatDouble(sgProdutos.Cells[coValorTotalDescProp, i]);      
      Result[idxProd].ValorTotalFrete             := Result[idxProd].ValorTotalFrete + _Biblioteca.SFormatDouble(sgProdutos.Cells[coValorTotalFreteProp, i]);      
      Result[idxProd].valor_total                 := Result[idxProd].valor_total + _Biblioteca.SFormatDouble(sgProdutos.Cells[coTotalProduto, i]);      
      Result[idxProd].QuantidadeRetirarAto        := Result[idxProd].QuantidadeRetirarAto + IIfDbl(cbTipoEntrega.GetValor = 'RA', Result[i - 1].quantidade);      
    end               
    else begin     
      SetLength(Result, Length(Result) + 1);
      itemId := itemId + 1;

      Result[itemId - 1].produto_id                  := SFormatInt(sgProdutos.Cells[coProdutoId, i]);
      Result[itemId - 1].nome                        := sgProdutos.Cells[coNomeProduto, i];
      Result[itemId - 1].nome_marca                  := sgProdutos.Cells[coMarca, i];
      Result[itemId - 1].item_id                     := itemId;
      Result[itemId - 1].quantidade                  := _Biblioteca.SFormatDouble(sgProdutos.Cells[coQuantidade, i]);
      Result[itemId - 1].unidade_venda               := sgProdutos.Cells[coUndVenda, i];
      Result[itemId - 1].PrecoManual                 := _Biblioteca.SFormatDouble(sgProdutos.Cells[coPrecoManual, i]);
      Result[itemId - 1].preco_unitario              := _Biblioteca.SFormatDouble(sgProdutos.Cells[coPrecoUnitario, i]);
      Result[itemId - 1].PrecoBase                   := _Biblioteca.SFormatDouble(sgProdutos.Cells[coPrecoBaseUtilizado, i]);

      Result[itemId - 1].valor_total_outras_despesas := _Biblioteca.SFormatDouble(sgProdutos.Cells[coValorTotalOutDespProp, i]);
      Result[itemId - 1].valor_total_desconto        := _Biblioteca.SFormatDouble(sgProdutos.Cells[coValorTotalDescProp, i]);
      Result[itemId - 1].ValorTotalFrete             := _Biblioteca.SFormatDouble(sgProdutos.Cells[coValorTotalFreteProp, i]);
      Result[itemId - 1].ValorDescUnitPrecoManual    := _Biblioteca.SFormatDouble(sgProdutos.Cells[coValorDescontoPrecoMan, i]);

      Result[itemId - 1].valor_total                 := _Biblioteca.SFormatDouble(sgProdutos.Cells[coTotalProduto, i]);
      Result[itemId - 1].Estoque                     := _Biblioteca.SFormatCurr(sgProdutos.Cells[coEstoque, i]);
      Result[itemId - 1].QuantidadeRetirarAto        := IIfDbl(cbTipoEntrega.GetValor = 'RA', Result[itemId - 1].quantidade);
      Result[itemId - 1].multiplo_venda              := SFormatDouble(sgProdutos.Cells[coMultiploVenda, i]);
      Result[itemId - 1].TipoPrecoUtilizado          := sgProdutos.Cells[coTipoPrecoUtilizar, i];
      Result[itemId - 1].TipoControleEstoque         := sgProdutos.Cells[coTipoControleEstoq, i];
      Result[itemId - 1].PercDescontoPrecoManual     := SFormatDouble(sgProdutos.Cells[coPercDescPrecoManual, i]);

      Result[itemId - 1].PrecoVarejoBasePrecoManual :=
        IIfDbl(
          Result[itemId - 1].TipoPrecoUtilizado = 'MAN',
          Arredondar(_Biblioteca.SFormatDouble(sgProdutos.Cells[coPrecoBaseUtilizado, i]) * vIndice, 2),
          Result[itemId - 1].preco_unitario
        );    
    end;
  end;
end;

function TFormOrcamentosVendasAmbientes.getTotalFreteItens: Currency;
var
  i: Integer;
begin
  Result := 0;
  for i := 1 to sgProdutos.RowCount -1 do begin
    if sgProdutos.Cells[coTipoLinha, i] <> 'DET' then
      Continue;
      
    Result := Result + SFormatCurr( sgProdutos.Cells[coFreteCobrar, i] );
  end;
end;

procedure TFormOrcamentosVendasAmbientes.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vItens: TArray<RecOrcamentoItens>;
  vBloqueios: TArray<RecOrcamentosBloqueios>;

  vBairroId: Integer;
  vOrcamentoId: Integer;
  vRetBanco: RecRetornoBD;
  vIndiceDescontoVendaId: Integer;
  vProfissionalId: Integer;

  vStatusOrcamento: string;
  vCreditosIds: TArray<Integer>;
  vRetTelaEntregasAto: TRetornoTelaFinalizar<RecProdutosRetirar>;

  vExisteItensAto: Boolean;
  vExisteItensRetirar: Boolean;
  vExisteItensEntregar: Boolean;
  vExisteItensSemPrevisao: Boolean;
  pExibirPrecoUnitarioProdutos: Boolean;
  pExibirPrecoTotalProdutos: Boolean;
  pExibirDescontoProdutos: Boolean;
begin
  vBloqueios := nil;
  vExisteItensEntregar := False;
  vExisteItensSemPrevisao := False;
  RatearValoresItens(True);

  vItens := getItens;

  MixVendas.Mix(
    FrCliente.getCliente().cadastro_id,
    FrCliente.getCliente().RecCadastro.nome_fantasia,
    FrCondicoesPagamento.getDados().condicao_id,
    FrCondicoesPagamento.getDados().nome,
    vItens,
    False
  );

  if Sender = sbGravar then begin // Se gravar como venda
    vStatusOrcamento := IIfStr(FBloqueios = nil, 'VR', 'VB');
    vCreditosIds := FrFechamento.Creditos;

    vRetTelaEntregasAto := Buscar.ProdutosRetirarAto.Buscar(vItens, eDataEntrega.AsData, eHoraEntrega.AsHora, IIfStr(cbTipoEntrega.GetValor = 'SE', 'SP', cbTipoEntrega.GetValor));
    if vRetTelaEntregasAto.RetTela <> trOk then
      Abort;

    vItens := vRetTelaEntregasAto.Dados.Itens;
    eDataEntrega.AsData := vRetTelaEntregasAto.Dados.DataEntrega;
    eHoraEntrega.AsHora := vRetTelaEntregasAto.Dados.HoraEntrega;

    vExisteItensAto := False;
    vExisteItensRetirar := False;
    vExisteItensEntregar := False;
    vExisteItensSemPrevisao := False;
    for i := Low(vItens) to High(vItens) do begin
      if not vExisteItensAto then
        vExisteItensAto := vItens[i].QuantidadeRetirarAto > 0;

      if not vExisteItensRetirar then
        vExisteItensRetirar := vItens[i].QuantidadeRetirar > 0;

      if not vExisteItensEntregar then
        vExisteItensEntregar := vItens[i].QuantidadeEntregar > 0;

      if not vExisteItensSemPrevisao then
        vExisteItensSemPrevisao := vItens[i].QuantidadeSemPrevisao > 0;
    end;

    if vExisteItensEntregar or vExisteItensSemPrevisao then
      FrEnderecoEntrega.VerificarDados;
  end
  else begin // se gravar como or�amento
    vStatusOrcamento := IIfStr(FBloqueios = nil, 'OE', 'OB');
    vCreditosIds := nil;
  end;

  if vExisteItensEntregar then begin
    if FrEnderecoEntrega.FrBairro.GetBairro.TipoBloqueioVendaEntregar = 'S' then
      AddBloqueio('A cidade definida para entrega n�o esta autorizada para vendas "A entregar". Cidade: ' + FrEnderecoEntrega.FrBairro.GetBairro.nome_cidade)
    else if FrEnderecoEntrega.FrBairro.GetBairro.TipoBloqueioVendaEntregar = 'V' then begin
      _Biblioteca.Exclamar('N�o � permitido entregas para a cidade ' + FrEnderecoEntrega.FrBairro.GetBairro.nome_cidade + '!');
      SetarFoco(FrEnderecoEntrega.FrBairro);
      Abort;
    end;
  end;

  vBairroId := 0;
  if vExisteItensEntregar or vExisteItensSemPrevisao or ((not FrCliente.EstaVazio) and (FrEnderecoEntrega.getBairroId <> 0)) then
    vBairroId := FrEnderecoEntrega.getBairroId;

  vIndiceDescontoVendaId := 0;
  if not FrIndiceDescontoVenda.EstaVazio then
    vIndiceDescontoVendaId := FrIndiceDescontoVenda.getDados().IndiceId;

  vProfissionalId := 0;
  if not FrProfissional.EstaVazio then
    vProfissionalId := FrProfissional.GetDado.CadastroId;

  Sessao.getConexaoBanco.IniciarTransacao;

  vRetBanco :=
    _Orcamentos.AtualizarOrcamento(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNomeConsumidorFinal.Text,
      eTelefoneConsumidorFinal.Text,
      Sessao.getEmpresaLogada.EmpresaId,
      FrCliente.getCliente.cadastro_id,
      FrCondicoesPagamento.getDados.condicao_id,
      FrVendedor.getVendedor.funcionario_id,
      FrCondicoesPagamento.getDados.indice_acrescimo,
      SFormatDouble(stValorTotalProdutos.Caption),
      SFormatDouble(stValorTotalPromocao.Caption),
      FrFechamento.eValorTotalASerPago.AsCurr,
      FrFechamento.eValorOutrasDespesas.AsCurr,
      FrFechamento.eValorDesconto.AsCurr,
      FrFechamento.eValorDinheiro.AsCurr,
      FrFechamento.eValorCheque.AsCurr,
      FrFechamento.eValorCartaoDebito.AsCurr,
      FrFechamento.eValorCartaoCredito.AsCurr,
      FrFechamento.eValorCobranca.AsCurr,
      FrFechamento.eValorAcumulativo.AsCurr,
      FrFechamento.eValorFinanceira.AsCurr,
      FrFechamento.eValorCredito.AsCurr,
      eValorFrete.AsDouble,
      SFormatDouble(stTotalFreteProdutos.Caption),
      0, // TURNO_ID
      vStatusOrcamento,
      eOverPrice.AsDouble,
      cbTipoEntrega.GetValor,
      eObservacoesCaixa.Lines.Text,
      eObservacoesTitulo.Lines.Text,
      eObservacoesExpedicao.Lines.Text,
      eObservacoesNotaFiscalEletronica.Lines.Text,
      eDataEntrega.AsData + eHoraEntrega.AsHora,
      eHoraEntrega.AsHora,
      'N',
      Self.tipoAnaliseCusto,
      FrEnderecoEntrega.getLogradouro,
      FrEnderecoEntrega.getComplemento,
      FrEnderecoEntrega.getNumero,
      FrEnderecoEntrega.getPontoReferencia,
      vBairroId,
      vIndiceDescontoVendaId,
      FrEnderecoEntrega.getCep,
      vItens,
      vCreditosIds,
      vProfissionalId,
      FrArquivos.Arquivos,
      FrEnderecoEntrega.getInscricaoEstadual,
      ckReceberNaEntrega.CheckedStr,
      Sessao.getParametros.DefinirLocalManual,
      edtCPFConsumidorFinal.Text,
      FrFechamento.eValorPix.AsCurr,
      Sessao.getUsuarioLogado.cadastro_id,
      FProdutosAmbiente,
      'N'
    );

  Sessao.AbortarSeHouveErro(vRetBanco);
  vOrcamentoId := vRetBanco.AsInt;

  SetLength(vBloqueios, Length(FBloqueios));
  for i := Low(FBloqueios) to High(FBloqueios) do begin
    vBloqueios[i].OrcamentoId := vOrcamentoId;
    vBloqueios[i].ItemId      := i;
    vBloqueios[i].Descricao   := FBloqueios[i];
  end;

  vRetBanco :=
    _Orcamentos.AtualizarOrcamentosPagamentos(
      Sessao.getConexaoBanco,
      vOrcamentoId,
      FrFechamento.Cheques,
      nil,
      FrFechamento.Cobrancas,
      nil,
      nil,
      True,
      nil
    );

//  vRetBanco :=
//    _ContasReceberBaixas.AtualizarContaReceberBaixa(
//      Sessao.getConexaoBanco,
//      0,
//      Sessao.getEmpresaLogada.EmpresaId,
//      0,
//      IIfInt((vTurnoId > 0) and ((FrPagamento.Dinheiro = nil) or (FrPagamento.eValorCheque = nil)) and (FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr = 0), vTurnoId),
//      eValorTitulos.AsCurr,
//      FrPagamento.eValorMulta.AsCurr,
//      FrPagamento.eValorJuros.AsCurr,
//      FrPagamento.eValorDesconto.AsCurr,
//      eValorRetencao.AsCurr,
//      eValorAdiantado.AsCurr,
//      FrPagamento.eValorDinheiro.AsCurr,
//      FrPagamento.eValorCheque.AsCurr,
//      FrPagamento.eValorCartaoDebito.AsCurr,
//      FrPagamento.eValorCartaoCredito.AsCurr,
//      FrPagamento.eValorCobranca.AsCurr,
//      FrPagamento.eValorCredito.AsCurr,
//      IIfData( FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr = 0, eDataPagamento.AsData),
//      IIfStr( FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr > 0, 'S', 'N' ),
//      eObservacoes.Text,
//      IIfStr( FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr = 0 , 'S', 'N' ),
//      'BCR',
//      0,
//      FrPagamento.Dinheiro,
//      FrPagamento.CartoesDebito,
//      FrPagamento.CartoesCredito,
//      FrPagamento.Cobrancas,
//      FrPagamento.Cheques,
//      FrPagamento.Creditos,
//      0,
//      vValorTroco,
//      True,
//      0,
//      nil
//    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  vRetBanco := _OrcamentosBloqueios.AtualizarOrcamentosBloqueios(Sessao.getConexaoBanco, vOrcamentoId, vBloqueios);
  Sessao.AbortarSeHouveErro(vRetBanco);

  Sessao.getConexaoBanco.FinalizarTransacao;

  if eID.AsInt = 0 then
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + NFormat(vOrcamentoId));

  if vStatusOrcamento = 'OE' then begin
    pExibirPrecoUnitarioProdutos := True;
    pExibirPrecoTotalProdutos := True;
    pExibirDescontoProdutos := True;

    if Perguntar('Deseja imprimir o or�amento agora?') then begin
      Impressao.OrcamentoGraficoAmbiente.Imprimir(
        vOrcamentoId,
        FProdutosAmbiente,
        False,
        False
      );
    end;

    if Perguntar('Deseja enviar or�amento por email?','Envio de E-Mail') then begin
      Impressao.OrcamentoGraficoAmbiente.EnviarPorEmail(
        vOrcamentoId,
        FProdutosAmbiente
      );
    end;

  end
  else if FBloqueios <> nil then begin
    if Sender = sbGravarOrcamento then
      _Biblioteca.Informar('O or�amento est� bloqueado.')
    else
      _Biblioteca.Informar('A venda est� bloqueada.')
  end;

  if (Sessao.getParametros.ImprimirListaSeparacaoVenda = 'S') and (cbTipoEntrega.GetValor = 'RA') then
    ImpressaoComprovanteEntregaGrafico.Imprimir( vOrcamentoId, tpRetirada, True );

  inherited;
end;

procedure TFormOrcamentosVendasAmbientes.ImprimirOrcamento;
var
  vCnt: Integer;
  totalReceber: Double;
begin
  cdsReceber.Close;
  cdsReceber.CreateDataSet;
  cdsReceber.Open;
  inherited;
  totalReceber := 0.0;
  for vCnt := sgProdutos.RowCount - 1 downto 1 do
  begin

//  coLegenda        = 0;
//  coProdutoId      = 1;
//  coNomeProduto    = 2;
//  coMarca          = 3;
//  coQuantidade     = 4;
//  coUndVenda       = 5;
//  coPrecoUnitario  = 6;
//  coTotalProduto   = 7;
//  coFreteCobrar    = 8;
//  coEstoque        = 9;  
  
    cdsReceber.Insert;
    if sgProdutos.Cells[coTipoLinha, vCnt] = 'AMB'  then begin
      cdsNomeProduto.AsString         := 'AMBIENTE: ' + sgProdutos.Cells[coNomeProduto,vCnt];
    end
    else if sgProdutos.Cells[coTipoLinha, vCnt] = 'DET'  then begin
      cdsReceberCodigo.AsString       := sgProdutos.Cells[coProdutoId,vCnt];
      cdsNomeProduto.AsString         := sgProdutos.Cells[coNomeProduto,vCnt];
      cdsNomeMarca.AsString           := sgProdutos.Cells[coMarca,vCnt];
      cdsQuantidade.AsFloat           := SFormatDouble(sgProdutos.Cells[coQuantidade,vCnt]);
      cdsUnidade.AsString             := sgProdutos.Cells[coUndVenda,vCnt];
      cdsPrecoUnitario.AsFloat        := SFormatDouble(sgProdutos.Cells[coPrecoUnitario,vCnt]);
      cdsTotalProduto.AsFloat         := SFormatDouble(sgProdutos.Cells[coTotalProduto,vCnt]);      
    end
    else begin
      cdsNomeMarca.AsString           := 'TOTAL: ';
      cdsTotalProduto.AsFloat         := SFormatDouble(sgProdutos.Cells[coTotalProduto,vCnt]);
    end;

    cdsReceber.Post;
  end;

  TfrxMemoView(frxReport.FindComponent('mmValorProduto')).Text :=   'Valor Total Liquido.....: ' + NFormat(totalReceber);
  frxReport.ShowReport;
end;

function TFormOrcamentosVendasAmbientes.LengendaGrid(
  pSemEstoque,
  pVariosPrecos,
  pProdutoKit: Boolean;
  pEmPromocao: Boolean
): TPicture;
var
	p: Integer;
  l: Integer;
begin
 	l := 0;

  if pSemEstoque then
    Inc(l, 10);

  if pVariosPrecos then
    Inc(l, 10);

  if pProdutoKit then
    Inc(l, 10);

  if pEmPromocao then
    Inc(l, 10);

  p := 1;
  FLegendaGrid.Width := l + p;
  FLegendaGrid.Picture := nil;

  if pSemEstoque then begin
  	FLegendaGrid.Canvas.Pen.Color := coCorSemEstoque;
  	FLegendaGrid.Canvas.Brush.Color := coCorSemEstoque;
  	FLegendaGrid.Canvas.Rectangle(p, 0, p + 5, 15);
    Inc(p, 10);
  end;

  if pVariosPrecos then begin
  	FLegendaGrid.Canvas.Pen.Color := coCorVariosPrecos;
  	FLegendaGrid.Canvas.Brush.Color := coCorVariosPrecos;
  	FLegendaGrid.Canvas.Rectangle(p, 0, p + 5, 15);
    Inc(p, 10);
  end;

  if pProdutoKit then begin
  	FLegendaGrid.Canvas.Pen.Color := coCorKit;
  	FLegendaGrid.Canvas.Brush.Color := coCorKit;
  	FLegendaGrid.Canvas.Rectangle(p, 0, p + 5, 15);
    Inc(p, 10);
  end;

  if pEmPromocao then begin
  	FLegendaGrid.Canvas.Pen.Color := coCorPromocao;
  	FLegendaGrid.Canvas.Brush.Color := coCorPromocao;
  	FLegendaGrid.Canvas.Rectangle(p, 0, p + 5, 15);
//    Inc(p, 10);
  end;

  Result := FLegendaGrid.Picture;
end;

procedure TFormOrcamentosVendasAmbientes.miAlterarPrecoClick(Sender: TObject);
begin
  inherited;
  //VerificarPrecoUtilizar(sgProdutos.Row, True, True);
  //CalcularValoresItens;
end;

procedure TFormOrcamentosVendasAmbientes.miAlterarPrecoManualClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vIndice: Double;
  vItens: TArray<RecOrcamentoItens>;
  vRetTela: TRetornoTelaFinalizar< TArray<RecOrcamentoItens> >;
begin
  inherited;

  if not Sessao.AutorizadoRotina('alterar_preco_manualmente') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  vItens := getItens;
  if vItens = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    SetarFoco(sgProdutos);
    Exit;
  end;

  vRetTela := BuscarPrecoManual.Buscar( vItens );
  if vRetTela.BuscaCancelada then
    Exit;

  FIndiceCondicaoPagamentoPrecoManual := getIndiceCondicaoPagamento;
  vIndice := getIndiceCondicaoPagamento;

  for i := Low(vRetTela.Dados) to High(vRetTela.Dados) do begin
    vLinha := sgProdutos.Localizar([coProdutoId], [NFormat(vRetTela.Dados[i].produto_id)]);

    // Pode ser que o pre�o tenha sido limpado, se for voltando ao pre�o normal de varejo.
    if NFormatN( vRetTela.Dados[i].PercDescontoPrecoManual ) = '' then begin
      if sgProdutos.Cells[coTipoPrecoUtilizar, vLinha] = 'MAN' then begin
        sgProdutos.Cells[coPrecoManual, vLinha]           := '';
        sgProdutos.Cells[coTipoPrecoUtilizar, vLinha]     := 'VAR';
        sgProdutos.Cells[coPrecoUnitario, vLinha]         := NFormat(Arredondar(SFormatDouble(sgProdutos.Cells[coPrecoVarejo, vLinha]) * vIndice, 2));
        sgProdutos.Cells[coPercDescPrecoManual, vLinha]   := '';
        sgProdutos.Cells[coValorDescontoPrecoMan, vLinha] := '';
      end;
    end
    else begin
      sgProdutos.Cells[coPrecoManual, vLinha]           := NFormat(vRetTela.Dados[i].PrecoManual);
      sgProdutos.Cells[coPrecoUnitario, vLinha]         := NFormat(vRetTela.Dados[i].PrecoManual);
      sgProdutos.Cells[coPercDescPrecoManual, vLinha]   := NFormatN(vRetTela.Dados[i].PercDescontoPrecoManual, 5);
      sgProdutos.Cells[coTipoPrecoUtilizar, vLinha]     := 'MAN';
      sgProdutos.Cells[coValorDescontoPrecoMan, vLinha] := NFormat(vRetTela.Dados[i].ValorDescUnitPrecoManual);
    end;
  end;

  CalcularValoresItens;
  FrFechamento.LimparDadosTiposCobranca(False);
end;

procedure TFormOrcamentosVendasAmbientes.miDeletarProdutoClick(Sender: TObject);
var
  FProdutosRestantes: TArray<RecAmbientes>;
  i, j: Integer;
  ambiente: string;
  produtoId: Integer;
  restaProdutos: Boolean;
  idxAmbiente: Integer;
  vLinha: Integer;
begin
  inherited;
  if sgProdutos.Cells[coTipoLinha, sgProdutos.Row] <> 'DET' then begin
    _Biblioteca.Informar('S� � permitido deletar produtos.');
    Exit;
  end;

  produtoId := SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]);
  ambiente := sgProdutos.Cells[coAmbiente, sgProdutos.Row];

  idxAmbiente := FindPosicaoAmbiente(ambiente);
  restaProdutos := (Length(FProdutosAmbiente[idxAmbiente].Produtos) > 1);

  if not restaProdutos then begin
    SetLength(FProdutosRestantes, Length(FProdutosAmbiente) - 1);
    vLinha := 0;
    for i := Low(FProdutosAmbiente) to High(FProdutosAmbiente) do begin
      if idxAmbiente = i then
        Continue;

      FProdutosRestantes[vLinha].NomeAmbiente := FProdutosAmbiente[i].NomeAmbiente;
      FProdutosRestantes[vLinha].Produtos     := FProdutosAmbiente[i].Produtos;      
      vLinha := vLinha + 1;
    end;
  end
  else begin
    SetLength(FProdutosRestantes, Length(FProdutosAmbiente));

    for i := Low(FProdutosAmbiente) to High(FProdutosAmbiente) do begin
      FProdutosRestantes[i].NomeAmbiente := FProdutosAmbiente[i].NomeAmbiente;
      vLinha := 0;

      for j := Low(FProdutosAmbiente[i].Produtos) to High(FProdutosAmbiente[i].Produtos) do begin
        if (produtoId = FProdutosAmbiente[i].Produtos[j].ProdutoId) and (ambiente = FProdutosAmbiente[i].NomeAmbiente)  then
          Continue;

        SetLength(FProdutosRestantes[i].Produtos, Length(FProdutosRestantes[i].Produtos) + 1);
        FProdutosRestantes[i].Produtos[vLinha] := FProdutosAmbiente[i].Produtos[j];
        vLinha := vLinha + 1;
      end;
    end;
  end;

  FProdutosAmbiente := FProdutosRestantes;
  CalcularValoresItens;
  AdicionarProdutosAmbienteGrid();
end;

procedure TFormOrcamentosVendasAmbientes.miLegendasClick(Sender: TObject);
var
  vLegendas: TFormLegendas;
begin
  inherited;
  vLegendas := TFormLegendas.Create(Self);

  vLegendas.AddLegenda(coCorVariosPrecos, 'V�rios pre�os disp.', clBlack);
  vLegendas.AddLegenda(coCorSemEstoque, 'Sem estoque', clBlack);
  vLegendas.AddLegenda(coCorKit, 'Produto kit', clBlack);
  vLegendas.AddLegenda(coCorPromocao, 'Utilizando pre�o promocional', clBlack);

  vLegendas.Show;
end;

procedure TFormOrcamentosVendasAmbientes.miProdutosKitClick(Sender: TObject);
begin
  inherited;
  if not Em(sgProdutos.Cells[coTipoControleEstoq, sgProdutos.Row], ['K', 'A']) then
    Exit;

  Informacoes.ProdutosComposicaoKit.Informar(SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]), sgProdutos.Cells[coNomeProduto, sgProdutos.Row]);
end;

procedure TFormOrcamentosVendasAmbientes.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    ckReceberNaEntrega,
    FrCliente,
    FrVendedor,
    FrCondicoesPagamento,
    sgProdutos,
    eOverPrice,
    FrFechamento,
    cbTipoEntrega,
    eDataEntrega,
    eHoraEntrega,
    eObservacoesCaixa,
    eObservacoesTitulo,
    eObservacoesExpedicao,
    eObservacoesNotaFiscalEletronica,
    eValorFrete,
    pcDados,
    sbMixVenda,
    FrEnderecoEntrega,
    FrIndiceDescontoVenda,
    FrProfissional,
    eAmbiente,
    FrArquivos],
    pEditando
  );

  _Biblioteca.Habilitar([sbGravarOrcamento], pEditando, False);
  _Biblioteca.Habilitar([eNomeConsumidorFinal, edtCPFConsumidorFinal, eTelefoneConsumidorFinal], False);
  FBloqueios := nil;
  cbTipoEntregaChange(nil);
  _Biblioteca.Habilitar([sbCopiarOrcamento], not pEditando);

  _Biblioteca.LimparCampos([
    stTotalFreteProdutos,
    stMultiploVenda,
    stQuantidadeItens,
    stQuantidadeTotalItens,
    stValorTotalProdutos,
    FrEnderecoEntrega
  ]);

  FrEnderecoEntrega.SomenteLeitura(True);
  FIndiceCondicaoPagamentoPrecoManual := -1;

  if pEditando then begin
    eOverPrice.AsDouble := 1;
    Sessao.SetComboBoxTipoEntregas(cbTipoEntrega);
    cbTipoEntrega.SetIndicePorValor('RA');

    if Sessao.getUsuarioLogado.Vendedor = 'S' then
      FrVendedor.InserirDadoPorChave(Sessao.getUsuarioLogado.funcionario_id, False);

    FrCondicoesPagamento.InserirDadoPorChave(Sessao.getParametrosEmpresa.CondicaoPagtoPadVendaAssis, False);

    if Sessao.getParametros.iniciar_venda_consumidor_final = 'S' then
      FrCliente.InserirDadoPorChave(Sessao.getParametros.cadastro_consumidor_final_id, False);

    VerificarHabilitarReceberNaEntrega;

    SetarFoco(FrCliente);
    FrFechamento.PermitirEditarTotalPagar( True );
    FDataMinimaEntrega := Sessao.getData + Sessao.getParametrosEmpresa.QuantidadeMinDiasEntrega;
  end;

  if not pEditando then begin
    FProdutosAmbiente := nil;
  end;
end;

procedure TFormOrcamentosVendasAmbientes.PesquisarRegistro;
var
  vRetTela: TRetornoTelaFinalizar<RecOrcamentos>;
begin
  vRetTela := Pesquisa.Orcamentos.Pesquisar(tpOrcamentos);

  if vRetTela.BuscaCancelada then
    Exit;

  inherited;
  if vRetTela.Dados.status = co_orcamento_cancelado then begin
    if not _Biblioteca.Perguntar('Este or�amento est� cancelado, deseja realmente utiliz�-lo?') then
      Exit;
  end;

  if vRetTela.Dados.OrcPorAmbiente = 'N' then begin
    Exclamar('S� � permitido carregar or�amentos por ambiente!');
    Exit;
  end;

  PreencherRegistro(vRetTela.Dados);
end;

procedure TFormOrcamentosVendasAmbientes.PreencherRegistro(pOrcamento: RecOrcamentos);
var
  i: Integer;
  j: Integer;
  k: Integer;
  idxProd: Integer;
  vItens: TArray<RecOrcamentoItens>;
  FProdutosAmbientePreencher: TArray<RecAmbientes>;
begin
  eID.AsInt := pOrcamento.orcamento_id;

  vItens := _OrcamentosItens.BuscarOrcamentosItens(Sessao.getConexaoBanco, 0, [eID.AsInt]);
  if vItens = nil then
    _Biblioteca.Exclamar('Erro ao buscar itens do or�amento');

  FProdutosAmbientePreencher := _OrcamentosItens.BuscarOrcamentosItensAmbiente(
    Sessao.getConexaoBanco,
    eID.AsInt
  );
  if FProdutosAmbientePreencher = nil then
    _Biblioteca.Exclamar('Erro ao buscar itens do or�amento por ambiente');

  FrCliente.InserirDadoPorChave(pOrcamento.cliente_id, False);
  eNomeConsumidorFinal.Text := pOrcamento.nome_consumidor_final;
  edtCPFConsumidorFinal.Text := pOrcamento.cpf_consumidor_final;
  eTelefoneConsumidorFinal.Text := pOrcamento.TelefoneConsumidorFinal;
  cbTipoEntrega.SetIndicePorValor(pOrcamento.TipoEntrega);
  cbTipoEntregaChange(nil);

  eDataEntrega.AsData := pOrcamento.DataEntrega;
  eHoraEntrega.AsHora := pOrcamento.HoraEntrega;
  eValorFrete.AsDouble := pOrcamento.ValorFrete;

  eObservacoesCaixa.Lines.Text                := pOrcamento.ObservacoesCaixa;
  eObservacoesTitulo.Lines.Text               := pOrcamento.ObservacoesTitulo;
  eObservacoesExpedicao.Lines.Text            := pOrcamento.ObservacoesExpedicao;
  eObservacoesNotaFiscalEletronica.Lines.Text := pOrcamento.ObservacoesNFe;

  FrVendedor.InserirDadoPorChave(pOrcamento.vendedor_id, False);
  FrCondicoesPagamento.InserirDadoPorChave(pOrcamento.condicao_id, False);
  FrProfissional.InserirDadoPorChave(pOrcamento.ProfissionalId, False);
  eOverPrice.AsDouble := pOrcamento.indice_over_price;

  for i := Low(FProdutosAmbientePreencher) to High(FProdutosAmbientePreencher) do begin
    for j := Low(FProdutosAmbientePreencher[i].Produtos) to High(FProdutosAmbientePreencher[i].Produtos) do begin
      idxProd := -1;
      for k := Low(vItens) to High(vItens) do begin
        if vItens[k].produto_id = FProdutosAmbientePreencher[i].Produtos[j].ProdutoId then begin
          idxProd := k;
          Break;
        end;
      end;

      FProdutosAmbientePreencher[i].Produtos[j].NomeProduto := vItens[idxProd].nome;
      FProdutosAmbientePreencher[i].Produtos[j].Marca := vItens[idxProd].nome_marca;
      FProdutosAmbientePreencher[i].Produtos[j].UndVenda := vItens[idxProd].unidade_venda;
      FProdutosAmbientePreencher[i].Produtos[j].PrecoUnitari := vItens[idxProd].preco_unitario;
      FProdutosAmbientePreencher[i].Produtos[j].PrecoBaseUtilizado := vItens[idxProd].PrecoBase;
      FProdutosAmbientePreencher[i].Produtos[j].ValorTotalProduto := Arredondar(FProdutosAmbientePreencher[i].Produtos[j].Quantidade * vItens[idxProd].preco_unitario, 2);
      FProdutosAmbientePreencher[i].Produtos[j].TipoPrecoUtilizar := vItens[idxProd].TipoPrecoUtilizado;
      FProdutosAmbientePreencher[i].Produtos[j].FreteCobrar := vItens[idxProd].ValorTotalFrete;
      FProdutosAmbientePreencher[i].Produtos[j].MultiploVenda := vItens[idxProd].multiplo_venda;
      FProdutosAmbientePreencher[i].Produtos[j].TipoControleEstoq := vItens[idxProd].TipoControleEstoque;

      FProdutosAmbientePreencher[i].Produtos[j].PrecoManual := vItens[idxProd].PrecoManual;
      FProdutosAmbientePreencher[i].Produtos[j].PercDescPrecoManual := vItens[idxProd].PercDescontoPrecoManual;
      FProdutosAmbientePreencher[i].Produtos[j].ValorDescontoPrecoMan := vItens[idxProd].ValorDescUnitPrecoManual;
    end;
  end;

  FProdutosAmbiente := FProdutosAmbientePreencher;

  (* Se o prazo do or�amento estiver vencido *)
  if Sessao.getData > pOrcamento.data_cadastro + Sessao.getParametrosEmpresa.QtdeDiasValidadeOrcamento then begin
    _Biblioteca.Informar('Este or�amento est� com o prazo de validade excedido, os pre�os ser�o recalculados.');
    AtualizarDadosProdutosGrid(True);
  end
  else
    AtualizarDadosProdutosGrid(False);

  CalcularValoresItens;
  AdicionarProdutosAmbienteGrid();

  FrFechamento.eValorOutrasDespesas.AsDouble := pOrcamento.valor_outras_despesas;
  FrFechamento.eValorOutrasDespesasChange(nil);

  FrFechamento.eValorDesconto.AsDouble := pOrcamento.valor_desconto;
  FrFechamento.eValorDescontoChange(nil);

  FrFechamento.TotalizarValorASerPago;

  if not FrCondicoesPagamento.EstaVazio then
    FIndiceCondicaoPagamentoPrecoManual := FrCondicoesPagamento.getDados().indice_acrescimo;
end;

(* Procedimento criado para ratear os valores *)
(* de outras despesas, desconto e frete *)
procedure TFormOrcamentosVendasAmbientes.RatearValoresItens(pGravando: Boolean);
var
  i: Integer;
  vIndiceRateio: Double;
  vIndiceRateioDesc: Double;

  vFreteItens: Currency;
  vValorFaltaRatearFrete: Currency;
  vValorFaltaRatearDesconto: Currency;
  vValorFaltaRatearOutDespesas: Currency;

  vLinhaItemMaisSignificativo: Integer;
  vValorItemMaisSignificativo: Currency;
begin
  vLinhaItemMaisSignificativo := -1;
  vValorItemMaisSignificativo := 0;

  vFreteItens                  := getTotalFreteItens;
  vValorFaltaRatearFrete       := eValorFrete.AsDouble;
  vValorFaltaRatearDesconto    := FrFechamento.eValorDesconto.AsDouble;
  vValorFaltaRatearOutDespesas := FrFechamento.eValorOutrasDespesas.AsDouble;

  for i := sgProdutos.FixedRows to sgProdutos.RowCount - 1 do begin
    if sgProdutos.Cells[coNomeProduto, i] = '' then
     Continue;

    sgProdutos.Cells[coValorTotalDescProp, i]    := '';
    sgProdutos.Cells[coValorTotalOutDespProp, i] := '';
    sgProdutos.Cells[coValorTotalFreteProp, i]   := '';

    // Rateando outras despesas dado na capa para os vItens para registro no ECF - Luka 13/10/2013 - 14:21
    vIndiceRateio := _Biblioteca.SFormatDouble(sgProdutos.Cells[coTotalProduto, i]) / SFormatDouble(stValorTotalProdutos.Caption);

    // Se o produto n�o estiver em promo��o guardando ele para jogar os valores de desconto e outras despesas caso d� diferen�a.
    if sgProdutos.Cells[coTipoPrecoUtilizar, i] <> 'PRO' then begin
      if _Biblioteca.SFormatCurr(sgProdutos.Cells[coTotalProduto, i]) > vValorItemMaisSignificativo then begin
        vLinhaItemMaisSignificativo := i;
        vValorItemMaisSignificativo := _Biblioteca.SFormatCurr(sgProdutos.Cells[coTotalProduto, i]);
      end;
    end;

    if NFormatN(SFormatDouble(stValorTotalProdutos.Caption) - SFormatDouble(stValorTotalPromocao.Caption)) <> '' then begin
      vIndiceRateioDesc :=
        _Biblioteca.SFormatDouble(sgProdutos.Cells[coTotalProduto, i]) / ( SFormatDouble(stValorTotalProdutos.Caption) - SFormatDouble(stValorTotalPromocao.Caption) );
    end
    else
      vIndiceRateioDesc := vIndiceRateio;

    if vValorFaltaRatearDesconto > 0 then begin
      if sgProdutos.Cells[coTipoPrecoUtilizar, i] <> 'PRO' then begin
        sgProdutos.Cells[coValorTotalDescProp, i] := NFormat(_Biblioteca.Arredondar(FrFechamento.eValorDesconto.AsDouble * vIndiceRateioDesc, 2));
        vValorFaltaRatearDesconto := vValorFaltaRatearDesconto - SFormatDouble(sgProdutos.Cells[coValorTotalDescProp, i]);

        if SFormatCurr(sgProdutos.Cells[coValorTotalDescProp, i]) >= SFormatCurr(sgProdutos.Cells[coTotalProduto, i]) then begin
          _Biblioteca.Exclamar('O valor total do desconto n�o pode ser maior ou igual ao valor dos produtos sem promo��o!');
          Abort;
        end;
      end;
    end;

    if vValorFaltaRatearOutDespesas > 0 then begin
      sgProdutos.Cells[coValorTotalOutDespProp, i] := NFormat(_Biblioteca.Arredondar(FrFechamento.eValorOutrasDespesas.AsDouble * vIndiceRateio, 2));
      vValorFaltaRatearOutDespesas := vValorFaltaRatearOutDespesas - SFormatDouble(sgProdutos.Cells[coValorTotalOutDespProp, i]);
    end;

    if vValorFaltaRatearFrete > 0 then begin
      sgProdutos.Cells[coValorTotalFreteProp, i] := NFormat(_Biblioteca.Arredondar((FrFechamento.ValorFrete - vFreteItens) * vIndiceRateio, 2));
      vValorFaltaRatearFrete := vValorFaltaRatearFrete - SFormatDouble(sgProdutos.Cells[coValorTotalFreteProp, i]);
    end;
    // Somando o frete por produto
    sgProdutos.Cells[coValorTotalFreteProp, i] :=
      NFormatN(SFormatDouble(sgProdutos.Cells[coValorTotalFreteProp, i]) + SFormatDouble(sgProdutos.Cells[coFreteCobrar, i]));
  end;

  if NFormatN(vValorFaltaRatearDesconto) <> '' then begin
    if vLinhaItemMaisSignificativo > -1 then begin
      sgProdutos.Cells[coValorTotalDescProp, vLinhaItemMaisSignificativo] := NFormat(SFormatDouble(sgProdutos.Cells[coValorTotalDescProp, vLinhaItemMaisSignificativo]) + vValorFaltaRatearDesconto);
      vValorFaltaRatearDesconto := 0;
    end;
  end;

  if NFormatN(vValorFaltaRatearOutDespesas) <> '' then begin
    if vLinhaItemMaisSignificativo > -1 then
      sgProdutos.Cells[coValorTotalOutDespProp, vLinhaItemMaisSignificativo] := NFormat(SFormatDouble(sgProdutos.Cells[coValorTotalOutDespProp, vLinhaItemMaisSignificativo]) + vValorFaltaRatearOutDespesas);
  end;

  if NFormatN(vValorFaltaRatearFrete) <> '' then begin
    if vLinhaItemMaisSignificativo > -1 then
      sgProdutos.Cells[coValorTotalFreteProp, vLinhaItemMaisSignificativo] := NFormat(SFormatDouble(sgProdutos.Cells[coValorTotalFreteProp, vLinhaItemMaisSignificativo]) + vValorFaltaRatearFrete);
  end;

  if pGravando then begin
    if NFormatN(vValorFaltaRatearDesconto) <> '' then begin
      _Biblioteca.Exclamar('N�o foi poss�vel ratear o desconto em todos os produtos, por favor verifique!');
      Abort;
    end;
  end;
end;

procedure TFormOrcamentosVendasAmbientes.sbCadastrarNovoEnderecoClick(Sender: TObject);
begin
  inherited;
  if FrCliente.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio selecionar o cliente para definir o endere�o da entrega!');
    SetarFoco(FrCliente);
    Exit;
  end;

  FrEnderecoEntrega.CadastrarNovoEndereco(FrCliente.getCliente().cadastro_id);
end;

procedure TFormOrcamentosVendasAmbientes.sbCarregarEnderecoPrincipalEntregaClick(Sender: TObject);
begin
  inherited;
  if FrCliente.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio selecionar o cliente para definir o endere�o da entrega!');
    SetarFoco(FrCliente);
    Exit;
  end;

  FrEnderecoEntrega.setLogradouro(FrCliente.getCliente().RecCadastro.logradouro);
  FrEnderecoEntrega.setComplemento(FrCliente.getCliente().RecCadastro.complemento);
  FrEnderecoEntrega.setNumero(FrCliente.getCliente().RecCadastro.numero);
  FrEnderecoEntrega.setBairroId(FrCliente.getCliente().RecCadastro.bairro_id);
  FrEnderecoEntrega.setPontoReferencia(FrCliente.getCliente().RecCadastro.ponto_referencia);
  FrEnderecoEntrega.setCep(FrCliente.getCliente().RecCadastro.cep);
  FrEnderecoEntrega.setInscricaoEstadual(FrCliente.getCliente().RecCadastro.inscricao_estadual);

//  cbTipoEntregaChange(nil);
end;

procedure TFormOrcamentosVendasAmbientes.sbCarregarEnderecosClick(Sender: TObject);
begin
  inherited;
  if FrCliente.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio selecionar o cliente para definir o endere�o da entrega!');
    SetarFoco(FrCliente);
    Exit;
  end;

  FrEnderecoEntrega.CarregarEnderecoCliente(FrCliente.getCliente().cadastro_id, True);
  cbTipoEntregaChange(nil);
end;

procedure TFormOrcamentosVendasAmbientes.sbCopiarOrcamentoClick(Sender: TObject);
var
  vOrcamento: TArray<RecOrcamentos>;
  vOrcamentoId: Integer;
begin
  vOrcamentoId := StrToIntDef(BuscaDados.BuscarDados(tiNumerico, 'Busca digitada', ''), 0);
  if vOrcamentoId = 0 then
    Exit;

  vOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 0, [vOrcamentoId]);
  if vOrcamento = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  if vOrcamento[0].OrigemVenda = 'P' then begin
    _Biblioteca.Exclamar('Venda realizada no PDV n�o pode ser copiada!');
    SetarFoco(eID);
    Exit;
  end;

  Modo(True);
  PreencherRegistro(vOrcamento[0]);
  eID.Clear;
end;

procedure TFormOrcamentosVendasAmbientes.sbMixVendaClick(Sender: TObject);
var
  vItens: TArray<RecOrcamentoItens>;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio selecionar o cliente antes de continuar!');
    SetarFoco(FrCliente);
    Exit;
  end;

  if FrCondicoesPagamento.EstaVazio then begin
    Exclamar('� necess�rio selecionar a condi��o de pagamento antes de continuar!');
    SetarFoco(FrCondicoesPagamento);
    Exit;
  end;

  RatearValoresItens(False);
  vItens := getItens;

  if vItens = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi informado na venda!');
    SetarFoco(sgProdutos);
    Exit;
  end;

  MixVendas.Mix(
    FrCliente.getCliente().cadastro_id,
    FrCliente.getCliente().RecCadastro.nome_fantasia,
    FrCondicoesPagamento.getDados().condicao_id,
    FrCondicoesPagamento.getDados().nome,
    vItens,
    True
  );
end;

procedure TFormOrcamentosVendasAmbientes.sgProdutosArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
var
  i: Integer;
  vSetouPosicao: Boolean;
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coQuantidade then begin 
    if not ValidarMultiplo(SFormatDouble(TextCell), SFormatDouble(sgProdutos.Cells[coMultiploVenda, ARow])) then begin
      sgProdutos.Cells[coQuantidade, sgProdutos.row] := '';
      TextCell := '';
    end;

    // modificar quantidade do ambiente
    AlterarQuantidadeProduto(
      SFormatInt(sgProdutos.Cells[coProdutoId, ARow]),
      SFormatDouble(TextCell),
      sgProdutos.Cells[coAmbiente, ARow],
      ARow
    );

    CalcularValoresItens;
    CalcularFrete;
  
  // tirar
//    TextCell := NFormatNEstoque( SFormatDouble(TextCell) );
//    if TextCell <> '' then begin
//
//      if not ValidarMultiplo(SFormatDouble(TextCell), SFormatDouble(sgProdutos.Cells[coMultiploVenda, sgProdutos.Row])) then begin
//        TextCell := '';
//        sgProdutos.Cells[coQuantidade, sgProdutos.Row] := '';
//      end
//      else
//        //VerificarPrecoUtilizar(sgProdutos.Row, True, False);
//
//      // Se a quantidade n�o foi apagada indo para a pr�xima inser��o de produtos
////      if TextCell <> '' then begin
////        if
////          (sgProdutos.Row = sgProdutos.RowCount - 1) and
////          (sgProdutos.Cells[coNomeProduto, sgProdutos.Row] <> '') and
////          (sgProdutos.Cells[coQuantidade, sgProdutos.Row] <> '')
////        then begin
////          sgProdutos.RowCount := sgProdutos.RowCount + 1;
////          sgProdutos.Row := sgProdutos.RowCount - 1;
////          sgProdutos.Col := coProdutoId;
////        end
////        else if sgProdutos.Row <> sgProdutos.RowCount - 1 then begin
////          vSetouPosicao := False;
////          for i := sgProdutos.Row to sgProdutos.RowCount - 1 do begin
////            if sgProdutos.Cells[coProdutoId, i] = '' then begin
////              sgProdutos.Col := coProdutoId;
////              vSetouPosicao  := True;
////              sgProdutos.Row := i;
////              Break;
////            end
////            else if sgProdutos.Cells[coQuantidade, i] = '' then begin
////              sgProdutos.Col := coQuantidade;
////              vSetouPosicao  := True;
////              sgProdutos.Row := i;
////              Break;
////            end;
////          end;
////
////          if not vSetouPosicao then begin
////            sgProdutos.RowCount := sgProdutos.RowCount + 1;
////            sgProdutos.Row := sgProdutos.RowCount - 1;
////            sgProdutos.Col := coProdutoId;
////          end;
////        end;
////      end;
//    end;
  end;

  CalcularValoresItens();
end;

procedure TFormOrcamentosVendasAmbientes.sgProdutosDblClick(Sender: TObject);
begin
  inherited;
  //Verificar possibilidade de fazer uma edi��o de ambientes depois v2
  if sgProdutos.Cells[coTipoLinha, sgProdutos.Row] = 'AMB' then
    Exit;
  
  
end;

procedure TFormOrcamentosVendasAmbientes.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coProdutoId, coQuantidade, coPrecoUnitario, coTotalProduto, coEstoque, coFreteCobrar] then
    vAlinhamento := taRightJustify
  else if ACol = coLegenda then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormOrcamentosVendasAmbientes.sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgProdutos.Cells[coTipoPrecoUtilizar, ARow] = 'PRO' then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end;

  if SFormatCurr(sgProdutos.Cells[coEstoque, ARow]) < SFormatCurr(sgProdutos.Cells[coQuantidade, ARow]) then
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;

  if ACol = coEstoque then begin
    if SFormatCurr(sgProdutos.Cells[ACol, ARow]) < 0 then
      AFont.Color := clRed;
  end;

  if sgProdutos.Cells[coTipoLinha, ARow] = 'AMB' then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end;

  if sgProdutos.Cells[coTipoLinha, ARow] = 'TOT' then begin
    AFont.Style := [fsBold];
  end;  
end;

procedure TFormOrcamentosVendasAmbientes.sgProdutosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgProdutos.Cells[coProdutoId, ARow] = '' then
    Exit;

  if ACol = coLegenda then begin
    APicture :=
      LengendaGrid(
        (SFormatCurr(sgProdutos.Cells[coEstoque, ARow]) <= 0) or (SFormatCurr(sgProdutos.Cells[coEstoque, ARow]) < SFormatCurr(sgProdutos.Cells[coQuantidade, ARow])),
        sgProdutos.Cells[coExisteVariosPrecos, ARow] = 'S',
        Em(sgProdutos.Cells[coTipoControleEstoq, ARow], ['K', 'A']),
        sgProdutos.Cells[coTipoPrecoUtilizar, ARow] = 'PRO'
      );
  end
end;

procedure TFormOrcamentosVendasAmbientes.sgProdutosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

  procedure AdicionarProdutos;
  var
    i: Integer;
    vLinha: Integer;
    vCodigoPesquisa: string;
    vProdutos: TArray<RecProdutosVendas>;

    procedure AddProdutoRelacionado(const pProdutoPrincipalId: Integer);
    var
      i: Integer;
      vProdutosRelacionados: TArray<RecProdutosRelacionados>;
    begin
      vProdutosRelacionados := Buscar.ProdutosRelacionados.Buscar(pProdutoPrincipalId);
      for i := Low(vProdutosRelacionados) to High(vProdutosRelacionados) do begin
        vLinha := sgProdutos.RowCount;
        vProdutos :=
          _ProdutosVenda.BuscarProdutosVendas(
            Sessao.getConexaoBanco,
            0,
            [Sessao.getEmpresaLogada.EmpresaId, vProdutosRelacionados[i].ProdutoRelacionadoId],
            False
          );

        if
          AdicionarProdutosGrid(
            vProdutos[0].produto_id,
            vProdutos[0].nome,
            vProdutos[0].unidade_venda,
            vProdutosRelacionados[i].QuantidadeAdicionar,
            vProdutos[0].multiplo_venda,
            vProdutos[0].disponivel,
            vProdutos[0].nome_marca,
            vProdutos[0].TipoControleEstoque,
            vProdutos[0].preco_varejo,
            vProdutos[0].PrecoPromocional,
            vProdutos[0].PrecoAtacado1,
            vProdutos[0].QuantidadeMinPrecoAtac1,
            vProdutos[0].PrecoAtacado2,
            vProdutos[0].QuantidadeMinPrecoAtac2,
            vProdutos[0].PrecoAtacado3,
            vProdutos[0].QuantidadeMinPrecoAtac3,
            vProdutos[0].ValorAdicionalFrete,
            vLinha,
            vProdutos[0].PrecoPontaEstoque,
            eAmbiente.Text
          )
        then
          sgProdutos.RowCount := sgProdutos.RowCount + 1;
      end;
    end;

  begin
    if (sgProdutos.Col = coProdutoId) and (sgProdutos.Cells[coProdutoId, sgProdutos.Row] <> '') then begin
      if sgProdutos.Cells[coNomeProduto, sgProdutos.Row] <> '' then
        Exit
      else if sgProdutos.Cells[coNomeProduto, sgProdutos.RowCount - 1] = '' then begin
        sgProdutos.DeleteRow(sgProdutos.RowCount - 1);
        sgProdutos.Invalidate;
      end;

      vLinha := -1;
      if sgProdutos.Cells[coNomeProduto, sgProdutos.Row] <> '' then
        vLinha := sgProdutos.Row;

      if Length(sgProdutos.Cells[coProdutoId, sgProdutos.Row]) > 10 then
        vCodigoPesquisa := IntToStr(_CodigoBarras.getProdutoId(sgProdutos.Cells[coProdutoId, sgProdutos.Row]))
      else
        vCodigoPesquisa := IntToStr(SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]));

      vProdutos :=
        _ProdutosVenda.BuscarProdutosVendas(
          Sessao.getConexaoBanco,
          0,
          [Sessao.getEmpresaLogada.EmpresaId, vCodigoPesquisa],
          False
        );

      if vProdutos = nil then begin
        sgProdutos.ClearRow(sgProdutos.Row);
        Exit;
      end;

      if
        not AdicionarProdutosGrid(
          vProdutos[0].produto_id,
          vProdutos[0].nome,
          vProdutos[0].unidade_venda,
          0,
          vProdutos[0].multiplo_venda,
          vProdutos[0].disponivel,
          vProdutos[0].nome_marca,
          vProdutos[0].TipoControleEstoque,
          vProdutos[0].preco_varejo,
          vProdutos[0].PrecoPromocional,
          vProdutos[0].PrecoAtacado1,
          vProdutos[0].QuantidadeMinPrecoAtac1,
          vProdutos[0].PrecoAtacado2,
          vProdutos[0].QuantidadeMinPrecoAtac2,
          vProdutos[0].PrecoAtacado3,
          vProdutos[0].QuantidadeMinPrecoAtac3,
          vProdutos[0].ValorAdicionalFrete,
          vLinha,
          vProdutos[0].PrecoPontaEstoque,
          vProdutos[0].Ambiente
        )
      then begin
        sgProdutos.ClearRow(sgProdutos.Row, coColunaMaxima);
        SetarFoco(sgProdutos);
        Abort;
      end;

      // Veriricando os pre�os
      VerificarPrecoUtilizar(false);
      CalcularValoresItens;

      AddProdutoRelacionado(vProdutos[0].produto_id);
      sgProdutos.Col := coQuantidade;
      SetarFoco(sgProdutos);
    end;
  end;

  procedure IrParaColuna(pColunaAtual: Integer; Key: Word);
  begin
    if (pColunaAtual = coProdutoId) and (Key = VK_RIGHT) then
      sgProdutos.Col := coQuantidade
    else if (pColunaAtual = coQuantidade) and (Key = VK_LEFT) then
      sgProdutos.Col := coProdutoId;
  end;

begin
  inherited;
end;

procedure TFormOrcamentosVendasAmbientes.sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := (ACol in[coQuantidade]) and (sgProdutos.Cells[coTipoLinha, ARow] = 'DET');

  if not CanSelect then
    Exit;

  if ACol = coProdutoId then begin
    sgProdutos.OnKeyPress := Numeros;
    if sgProdutos.Cells[coProdutoId, ARow] <> '' then
      sgProdutos.Options := sgProdutos.Options - [goEditing]
    else
      sgProdutos.Options := sgProdutos.Options + [goEditing];
  end
  else if ACol = coQuantidade then begin
    sgProdutos.OnKeyPress := NumerosVirgula;
    sgProdutos.Options := sgProdutos.Options + [goEditing];
  end;

  stMultiploVenda.Caption := sgProdutos.Cells[coMultiploVenda, ARow];
  CalcularValoresItens;
end;

procedure TFormOrcamentosVendasAmbientes.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  ImprimirOrcamento;
end;

procedure TFormOrcamentosVendasAmbientes.VerificarCondicaoRestrita;
begin
  if FrCondicoesPagamento.EstaVazio then
    Exit;

  if FrCondicoesPagamento.getDados.Restrita = 'N' then
    Exit;

  if FrCliente.EstaVazio then begin
    _Biblioteca.Exclamar('N�o � permitido utilizar uma condi��o de pagamento restrita sem definir o cliente!');
    FrCondicoesPagamento.Clear;
    SetarFoco(FrCliente);
    Exit;
  end;

  if
     not _ClientesCondicPagtoRestrit.CondicaoRestritaLiberada(
      Sessao.getConexaoBanco,
      FrCondicoesPagamento.getDados().condicao_id,
      FrCliente.getCliente().cadastro_id
    )
  then begin
    _Biblioteca.Exclamar('N�o � permitido utilizar esta condi��o de pagamento restrita para este cliente!');
    FrCondicoesPagamento.Clear;
    SetarFoco(FrCondicoesPagamento);
    Abort;
  end;
end;

procedure TFormOrcamentosVendasAmbientes.VerificarHabilitarReceberNaEntrega;
begin
  ckReceberNaEntrega.Enabled := False;
  ckReceberNaEntrega.Checked := False;

  if FrCondicoesPagamento.EstaVazio then
    Exit;

  if FrCondicoesPagamento.getDados.RecebimentoNaEntrega = 'N' then
    Exit;

  if not Em(cbTipoEntrega.GetValor, ['EN', 'SE']) then
    Exit;

  ckReceberNaEntrega.Enabled := True;
end;

procedure TFormOrcamentosVendasAmbientes.VerificarPrecoUtilizar(pVerificarQuantidade: Boolean);
type
  RecPrecos = record
    TipoPreco: string;
    Preco: Double;
    PrecoBase: Double;
  end;
  
var
  i: Integer;
  vPrecos: TArray<RecPrecos>;
  vPrecosString: TArray<String>;
  vOpcaoSelecionada: Integer;
  vIndiceCondicao: Double;

  procedure addPreco(pDescricao: string; pTipoPreco: string; pPreco: string );
  begin
    if not FrCondicoesPagamento.EstaVazio then begin
      if pTipoPreco = 'PRO' then begin
        if FrCondicoesPagamento.getDados.PermitirPrecoPromocional = 'N' then
          Exit;

        vIndiceCondicao := FrCondicoesPagamento.getDados.getIndiceAcrescimoPrecoPromocional;
      end
      else if pTipoPreco = 'PES' then begin
        if FrCondicoesPagamento.getDados.PermitirUsoPontaEstoque = 'N' then
          Exit;
      end
      else if Em(pTipoPreco, ['AT1', 'AT2', 'AT3']) then begin
        if FrCliente.EstaVazio then
          Exit;

        if FrCondicoesPagamento.getDados.TipoPreco = 'V' then
          Exit;

        if (FrCliente.getCliente.TipoPreco = 'V') and (FrCondicoesPagamento.getDados.TipoPreco <> 'I') then
          Exit;
      end;
    end;

    SetLength(vPrecos, Length(vPrecos) + 1);
    SetLength(vPrecosString, Length(vPrecosString) + 1);

    vPrecos[High(vPrecos)].PrecoBase := SFormatDouble(pPreco);
    vPrecos[High(vPrecos)].Preco     := Arredondar(SFormatDouble(pPreco) * vIndiceCondicao, 2);
    vPrecos[High(vPrecos)].TipoPreco := pTipoPreco;

    vPrecosString[High(vPrecosString)] := pDescricao + NFormat( vPrecos[High(vPrecosString)].Preco );
  end;

  procedure setPreco(pPreco: Double; pTipoPreco: string; pPrecoBase: Double);
  begin
    FProdutoAtual.PrecoUnitario := pPreco;
    FProdutoAtual.TipoPreco := pTipoPreco;
    FProdutoAtual.PrecoBase := pPrecoBase;
  end;
begin
  // Verificando se a tela de pre�os j� esta aberta
  if (Sessao.getParametrosEmpresa.TipoPrecoUtilizarVenda <> 'APA') or (Sessao.getParametrosEmpresa.TipoPrecoUtilizarVenda = 'APA') then begin
    for i := 0 to Screen.FormCount - 1 do begin
      if Screen.Forms[i].ClassName = 'TFormSelecionarVariosOpcoes' then
        Exit;
    end;
  end;

  vIndiceCondicao := getIndiceCondicaoPagamento;
  addPreco('Varejo R$ ', 'VAR', NFormat(FProdutoAtual.preco_varejo));

  if FProdutoAtual.PrecoPromocional > 0 then
    addPreco('Promocional R$ ', 'PRO', NFormat(FProdutoAtual.PrecoPromocional)); 

  // Definindo j� como base o pre�o de varejo se n�o estiver sendo alterado manualmente
  setPreco( vPrecos[0].Preco, vPrecos[0].TipoPreco, vPrecos[0].PrecoBase );

  // Se n�o houver mais pre�os sai fora
  if Length(vPrecos) = 1 then
    Exit;    

  if (Sessao.getParametrosEmpresa.TipoPrecoUtilizarVenda <> 'APA') or ((Sessao.getParametrosEmpresa.TipoPrecoUtilizarVenda = 'APA') and (1 > 2) and pVerificarQuantidade) then begin
    vOpcaoSelecionada := SelecionarVariosOpcoes.Selecionar('Pre�os dispon�veis', vPrecosString, 0).Dados;
    if vOpcaoSelecionada = -1 then
      Exit;

    setPreco( vPrecos[vOpcaoSelecionada].Preco, vPrecos[vOpcaoSelecionada].TipoPreco, vPrecos[vOpcaoSelecionada].PrecoBase );
  end;    
end;

procedure TFormOrcamentosVendasAmbientes.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  vDataHora: TDateTime;
  vGravandoVenda: Boolean;
  vLimiteCredito: Currency;

  vTemPrecoManual: Boolean;
  vTotalDescontos: Double;
  vPercentualDescontoGeral: Double;
  vItens: TArray<RecOrcamentoItens>;
  vTotalProdutosOrigSemPromocao: Currency;
begin
  inherited;
  vGravandoVenda := (Sender = sbGravar);

  if stValorTotalProdutos.Caption = stValorTotalPromocao.Caption then begin
    if FrFechamento.eValorDesconto.AsCurr > 0 then begin
      _Biblioteca.Exclamar('N�o � permitido conceder descontos quando todos os produtos est�o utilizando pre�o promocional!');
      SetarFoco(FrCondicoesPagamento);
      Abort;
    end;
  end;

  CalcularValoresItens();

  if FrCliente.EstaVazio then begin
    _Biblioteca.Exclamar('O cliente n�o foi informado corretamente, verifique!');
    SetarFoco(FrCliente);
    Abort;
  end;

  if FrVendedor.EstaVazio then begin
    _Biblioteca.Exclamar('O vendedor n�o foi informado corretamente, verifique!');
    SetarFoco(FrVendedor);
    Abort;
  end;

  if FrCondicoesPagamento.EstaVazio then begin
    _Biblioteca.Exclamar('A condi��o de pagamento n�o foi informada corretamente, verifique!');
    SetarFoco(FrCondicoesPagamento);
    Abort;
  end;

  if eOverPrice.AsCurr <= 0 then begin
    _Biblioteca.Exclamar('O �ndice "Overprice" n�o foi informado corretamente, verifique!');
    SetarFoco(eOverPrice);
    Abort;
  end;

  if FProdutosAmbiente = nil then begin
    _Biblioteca.Exclamar('Nenhum produto adicionado, verifique!');
    SetarFoco(sgProdutos);
    Abort;  
  end;

  if FrCliente.getCliente.cadastro_id = Sessao.getParametros.cadastro_consumidor_final_id then begin
    eNomeConsumidorFinal.Text := Trim(eNomeConsumidorFinal.Text);
    if
      (Sessao.getParametrosEmpresa.ExigirNomeConsFinalVenda = 'S') and
      ((eNomeConsumidorFinal.Text = '') or (eNomeConsumidorFinal.Text = 'CONSUMIDOR FINAL'))
    then begin
      _Biblioteca.Exclamar('� necess�rio informar o nome do cliente!');
      SetarFoco(eNomeConsumidorFinal);
      Abort;
    end;

    if vGravandoVenda then begin
      if FrCondicoesPagamento.getDados.NaoPermitirConsFinal = 'S' then begin
        _Biblioteca.Exclamar('Esta condi��o de pagamento n�o aceita vendas para consumidor final!');
        SetarFoco(FrCliente);
        Abort;
      end;

      if not Em(cbTipoEntrega.GetValor, ['RA']) then begin
        _Biblioteca.Exclamar('N�o � permitido venda com entrega diferente de "Retirar ato" para consumidor final!');
        SetarFoco(FrCliente);
        Abort;
      end;
    end;
  end;

  if vGravandoVenda then begin
    VerificarCondicaoRestrita;

    if cbTipoEntrega.GetValor = 'EN' then begin
      vDataHora := Sessao.getDataHora;
      if eDataEntrega.AsData < DateOf(vDataHora) then begin
        _Biblioteca.Exclamar('A data de entrega dos produtos n�o pode ser menor que a data atual!');
        SetarFoco(eDataEntrega);
        Abort;
      end;
    end;
  end;

  for i := 1 to sgProdutos.RowCount - 1 do begin
    if (sgProdutos.Cells[coNomeProduto, i] = '') or (sgProdutos.Cells[coTipoLinha, i] = 'AMB') or (sgProdutos.Cells[coNomeProduto, i] = 'TOT') then
      Continue;

    if SFormatCurr(sgProdutos.Cells[coPrecoUnitario, i]) = 0 then begin
      _Biblioteca.Exclamar('O produto ' + sgProdutos.Cells[coNomeProduto, i] + ' est� com o pre�o unit�rio zerado, verifique!');
      SetarFoco(sgProdutos);
      sgProdutos.Row := i;
      Abort;
    end;

    if SFormatDouble(sgProdutos.Cells[coQuantidade, i]) = 0 then begin
      _Biblioteca.Exclamar('O produto ' + sgProdutos.Cells[coNomeProduto, i] + ' est� com a quantidade zerada, verifique!');
      SetarFoco(sgProdutos);
      sgProdutos.Col := coQuantidade;
      sgProdutos.Row := i;
      Abort;
    end;
  end;

  if vGravandoVenda then begin
    if FrFechamento.TemValoresComprometemCreditoCliente then begin
      if (FrCliente.getCliente.restricao_spc = 'S') then begin
        _Biblioteca.Exclamar('A venda para este cliente n�o � permitida por quest�es financeiras, procure o departamento financeiro! Mot: 002');
        SetarFoco(FrCliente);
        Abort;
      end;

      if (FrCliente.getCliente.restricao_serasa = 'S') then begin
        _Biblioteca.Exclamar('A venda para este cliente n�o � permitida por quest�es financeiras, procure o departamento financeiro! Mot: 003');
        SetarFoco(FrCliente);
        Abort;
      end;
    end;

    if FrFechamento.eValorTroco.AsCurr < 0 then begin
      _Biblioteca.Exclamar('Existe diferen�a no valor a ser pago e as formas de pagamentos definidas!');
      SetarFoco(FrFechamento.eValorDinheiro);
      Abort;
    end;

    if FrCliente.getCliente.cadastro_id = Sessao.getParametros.cadastro_consumidor_final_id then begin
      if FrFechamento.eValorCheque.AsCurr > 0 then begin
        _Biblioteca.Exclamar('N�o s�o permitidas vendas no cheque para consumidor final, por favor cadastre o cliente!');
        SetarFoco(FrFechamento.eValorCheque);
        Abort;
      end;

      if FrFechamento.eValorCredito.AsCurr > 0 then begin
        _Biblioteca.Exclamar('N�o � permitido utilizar cr�ditos para consumidor final, por favor cadastre o cliente!');
        SetarFoco(FrFechamento.eValorCredito);
        Abort;
      end;
    end;

    if FrFechamento.getFormasPagamentoMaiorTotalPagarEUtilizandoCredito then begin
      _Biblioteca.Exclamar('As formas de pagamento j� definidas � maior ou igual ao valor a ser pago pelo pedido, n�o ser� poss�vel utilizar cr�ditos!');
      SetarFoco(FrFechamento.eValorCredito);
      Abort;
    end;

    if FrFechamento.getCreditoMaiorTotalPagarETemFormasPagamento then begin
      _Biblioteca.Exclamar('Os cr�ditos que est�o sendo utilizados j� cobrem o valor do pedido, n�o ser� poss�vel utilizar outras formas de pagamento!');
      SetarFoco(FrFechamento.eValorDinheiro);
      Abort;
    end;

    if Em(cbTipoEntrega.GetValor, ['EN', 'SE']) then begin
      if FrEnderecoEntrega.FrBairro.EstaVazio then begin
        _Biblioteca.Exclamar('Quando o tipo de entrega for "Entregar" ou "Sem previs�o - Entregar" o endere�o de entrega deve ser informado!');
        SetarFoco(FrEnderecoEntrega);
        Abort;
      end;

      if FrEnderecoEntrega.FrBairro.GetBairro.bairro_id = 1 then begin
        _Biblioteca.Exclamar('O bairro informado para entrega � o bairro "1 - Importa��o", o uso deste bairro n�o e permitido na venda, fa�a o ajuste no endere�o do cliente!');
        SetarFoco(FrEnderecoEntrega);
        Abort;
      end;

      if eDataEntrega.AsData < FDataMinimaEntrega then begin
        _Biblioteca.Exclamar('A data defininida de entrega � menor que a m�nima de entrega! Data m�nima: ' + sLineBreak + DFormat(FDataMinimaEntrega));
        SetarFoco(eDataEntrega);
        Abort;
      end;
    end
    else if FrCliente.getCliente.RecCadastro.bairro_id = 1 then begin
      _Biblioteca.Exclamar('O bairro do cadastro do cliente � o bairro "1 - Importa��o", o uso deste bairro n�o e permitido na venda, fa�a o ajuste no endere�o do cliente!');
      SetarFoco(FrCliente);
      Abort;
    end;
  end;

  if FrFechamento.eValorCredito.AsCurr > 0 then begin
    if _ContasPagar.getTemCreditoIndiceMaiorCondicao( Sessao.getConexaoBanco, FrCondicoesPagamento.getDados.indice_acrescimo, FrFechamento.Creditos ) then begin
      _Biblioteca.Exclamar('O �ndice do cr�dito utilizado e maior que o �ndice da condi��o de pagamento atual! utilize uma condi��o de pagamento que tenha um �ndice igual ou maior que o �ndice do cr�dito!');
      SetarFoco(FrFechamento.eValorCredito);
      Abort;
    end;
  end;

  if (FrFechamento.eValorDesconto.AsCurr > 0) and (FrFechamento.eValorDesconto.AsCurr >= SFormatCurr(stValorTotalProdutos.Caption) - SFormatCurr(stValorTotalPromocao.Caption)) then begin
    _Biblioteca.Exclamar(
      'O valor total do desconto n�o pode ser maior ou igual ao valor dos produtos sem promo��o!' + chr(13) +
      'Valor produtos sem promo��o: ' + NFormat(SFormatCurr(stValorTotalProdutos.Caption) - SFormatCurr(stValorTotalPromocao.Caption)) + chr(13) +
      'Valor do desconto..........: ' + NFormat(FrFechamento.eValorDesconto.AsCurr)
    );
    SetarFoco(FrFechamento.eValorDesconto);
    Abort;
  end;

  if vGravandoVenda then begin
    if Sessao.getParametrosEmpresa.ObrigarVendedorSelTipoCobr = 'S' then begin
      if (FrFechamento.eValorCheque.AsCurr > 0) and (FrFechamento.Cheques = nil) then begin
        _Biblioteca.Exclamar('Os cheques n�o foram definidos corretamente, verifique!');
        SetarFoco(FrFechamento.eValorCheque);
        Abort;
      end;

      if (FrFechamento.eValorCobranca.AsCurr > 0) and (FrFechamento.Cobrancas = nil) then begin
        _Biblioteca.Exclamar('As cobran�as n�o foram definidas corretamente, verifique!');
        SetarFoco(FrFechamento.eValorCobranca);
        Abort;
      end;
    end;

    if ckReceberNaEntrega.Checked then begin
      if FrFechamento.eValorCobranca.AsCurr + FrFechamento.eValorAcumulativo.AsCurr > 0 then begin
        _Biblioteca.Exclamar('Quando a venda for para "Recebimento na entrega" os campos "Cobran�a" ou "Acumulado" n�o podem ser preenchidos!');
        SetarFoco(FrFechamento.eValorDinheiro);
        Abort;
      end;

      if FrFechamento.eValorCredito.AsCurr >= FrFechamento.eValorTotalASerPago.AsCurr then begin
        _Biblioteca.Exclamar('Quando a venda for para "Recebimento na entrega" o cr�dito n�o pode ser maior que o total da venda!');
        SetarFoco(FrFechamento.eValorDinheiro);
        Abort;
      end;
    end;

    if (eValorFrete.AsCurr > 0) and (not Em(cbTipoEntrega.GetValor, ['SE', 'EN']) ) then begin
      _Biblioteca.Exclamar('Quando h� frete a ser cobrado o tipo de entrega deve ser "A Entregar" ou "Sem previs�o - Entregar", verifique!');
      SetarFoco(cbTipoEntrega);
      Abort;
    end;
  end;

  if (FrCondicoesPagamento.getDados.Acumulativo = 'S') and (frFechamento.eValorAcumulativo.AsCurr = 0) then begin
    _Biblioteca.Exclamar('A condi��o de pagamento selecionada � "Acumluativo" e o valor na forma de pagamento "Acumulado" n�o foi informado!');
    SetarFoco(frFechamento.eValorAcumulativo);
    Abort;
  end;

  // Validando se a soma dos descontos dados ( Manual + Geral ) � maior que o permitido
  // Nos descontos s�o desconsiderdos os produtos em promo��o
  vItens := getItens;
  vTotalDescontos := 0;
  vTemPrecoManual := False;
  vTotalProdutosOrigSemPromocao := 0;
  for i := Low(vItens) to High(vItens) do begin
    if not vTemPrecoManual then
      vTemPrecoManual := vItens[i].TipoPrecoUtilizado = 'MAN';

    if vItens[i].TipoPrecoUtilizado = 'PRO' then
      Continue;

    vTotalDescontos := vTotalDescontos + (vItens[i].ValorDescUnitPrecoManual * vItens[i].quantidade);

    if vItens[i].TipoPrecoUtilizado <> 'MAN' then
      vTotalProdutosOrigSemPromocao := vTotalProdutosOrigSemPromocao + vItens[i].valor_total
    else
      vTotalProdutosOrigSemPromocao := vTotalProdutosOrigSemPromocao + (vItens[i].PrecoVarejoBasePrecoManual * vItens[i].quantidade);
  end;

  vTotalProdutosOrigSemPromocao := vTotalProdutosOrigSemPromocao + FrFechamento.eValorOutrasDespesas.AsCurr;
  vTotalDescontos   := vTotalDescontos + FrFechamento.eValorDesconto.AsCurr;

  if vTotalProdutosOrigSemPromocao > 0 then
    vPercentualDescontoGeral := Arredondar(vTotalDescontos / vTotalProdutosOrigSemPromocao * 100, 5)
  else
    vPercentualDescontoGeral := 0;

//  if vTemPrecoManual and (FIndiceCondicaoPagamentoPrecoManual <> FrCondicoesPagamento.getDados().indice_acrescimo) then begin
//    _Biblioteca.Exclamar(
//      'Existem produtos que est�o utilizando pre�o manual e n�o foram reprocessados ap�s a troca de condi��o de pagamento, fa�a o reprocessamento!'
//    );
//    Abort;
//  end;

  (******************* BLOQUEIOS ****************************)
  FBloqueios := nil;

  if
    (FrCondicoesPagamento.getDados.valor_minimo_venda > 0) and
    (FrFechamento.eValorTotalASerPago.AsCurr < FrCondicoesPagamento.getDados.valor_minimo_venda)
  then begin
    AddBloqueio(
      'A condi��o de pagamento exige um valor m�nimo de R$ ' + NFormat(FrCondicoesPagamento.getDados.valor_minimo_venda) + '.' + Chr(13) + Chr(10) +
      'O valor da venda � de R$ ' + NFormat(FrFechamento.eValorTotalASerPago.AsCurr) + '.'
    );
  end;

  if
//    (vPercentualDescontoGeral > (FrCondicoesPagamento.getDados.PercentualDescontoComercial + Sessao.getUsuarioLogado.PercentualDescontoAdicVenda))
    (vPercentualDescontoGeral > (Sessao.getUsuarioLogado.PercentualDescontoAdicVenda))
    or
    ((FrCondicoesPagamento.getDados.PercentualDescontoPermVenda > 0) and (FrFechamento.ePercentualDesconto.AsCurr > FrCondicoesPagamento.getDados.PercentualDescontoPermVenda))
  then begin
    AddBloqueio(
      'O percentual de desconto dado no or�amento � maior que o permitido!' + sLineBreak +
      'Percentual desconto dado.....: ' + NFormatN(vPercentualDescontoGeral) + sLineBreak +
      'Percentual desconto permitido: ' +
        NFormat(getMenorValor([
          //FrCondicoesPagamento.getDados.PercentualDescontoComercial + Sessao.getUsuarioLogado.PercentualDescontoAdicVenda,
          Sessao.getUsuarioLogado.PercentualDescontoAdicVenda,
          FrCondicoesPagamento.getDados.PercentualDescontoPermVenda],
          True
        ))
    );
  end;

  if vGravandoVenda then begin
    if FrFechamento.TemValoresComprometemCreditoCliente then begin
      if FrCliente.getCliente.LimiteCredito > 0 then begin
        vLimiteCredito := _ContasReceber.BuscarLimiteCreditoUtilizadoAtualmente(Sessao.getConexaoBanco, FrCliente.getCliente().cadastro_id, False);
        if (FrCliente.getCliente.LimiteCredito - (FrFechamento.getValoresComprometemCreditoCliente + vLimiteCredito) < 0) then begin
          AddBloqueio(
            'O limite de cr�dito a ser utilizado � maior que o dispon�vel atualmente!' + Chr(13) + Chr(10) +
            'Limite dispon�vel.....: R$ ' + NFormat(FrCliente.getCliente().LimiteCredito) + Chr(13) + Chr(10) +
            'Limite a ser utilizado: R$ ' + NFormat(FrFechamento.getValoresComprometemCreditoCliente + vLimiteCredito)
          );
        end;
      end;

      if FrCliente.getCliente.LimiteCreditoMensal > 0 then begin
        vLimiteCredito := _ContasReceber.BuscarLimiteCreditoUtilizadoAtualmente(Sessao.getConexaoBanco, FrCliente.getCliente().cadastro_id, True);
        if (FrCliente.getCliente.LimiteCreditoMensal - (FrFechamento.getValoresComprometemCreditoCliente + vLimiteCredito) < 0) then begin
          AddBloqueio(
            'O limite de cr�dito a ser utilizado � maior que o dispon�vel mensalmente!' + Chr(13) + Chr(10) +
            'Limite dispon�vel mensal.: R$ ' + NFormat(FrCliente.getCliente().LimiteCreditoMensal) + Chr(13) + Chr(10) +
            'Limite a ser utilizado...: R$ ' + NFormat(FrFechamento.getValoresComprometemCreditoCliente + vLimiteCredito)
          );
        end;
      end;

      if _ContasReceber.BloquearVendaPorQtdeDiasTitulosVencidos(Sessao.getConexaoBanco, FrCliente.getCliente().cadastro_id, Sessao.getParametrosEmpresa.QtdeDiasBloqVendaTitVenc) then begin
        AddBloqueio(
          'O cliente possui t�tulos a receber vencidos a mais de ' + IntToStr(Sessao.getParametrosEmpresa.QtdeDiasBloqVendaTitVenc) + ' dias!'
        );
      end;

      if _Clientes.ValidadeLimiteCreditoVencida(Sessao.getConexaoBanco, FrCliente.getCliente().cadastro_id) then
        AddBloqueio('O cliente selecionado est� com a data de validade do limite de cr�dito vencida.');

      if FrCliente.getCliente.ListaNegraId > 0 then
        AddBloqueio('O cliente est� inclu�do em uma lista financeira.');
    end;

    if FrCondicoesPagamento.getDados.BloquearVendaSempre = 'S' then
      AddBloqueio('Venda bloqueada por causa da condi��o de pagamento.');
  end;

  if Em(cbTipoEntrega.GetValor, ['EN', 'SE']) then begin
    if (Sessao.getParametrosEmpresa.CalcularFreteVendaPorKM = 'N') and (Sessao.getParametrosEmpresa.ValorFretePadraoVenda > 0) and (FrFechamento.eValorFrete.AsCurr < Sessao.getParametrosEmpresa.ValorFretePadraoVenda) then begin
      AddBloqueio(
        'O frete definido na venda � menor que o frete m�nimo padr�o!' + sLineBreak +
        'Frete cobranco na venda.: R$ ' + NFormat(FrFechamento.eValorFrete.AsDouble) + sLineBreak +
        'Frete padr�o na venda...: R$ ' + NFormat(Sessao.getParametrosEmpresa.ValorFretePadraoVenda)
      );
    end;
  end;

  if FBloqueios <> nil then begin
    if not _ExibirMensagemMemo.Mensagem('Bloqueio de or�amento', 'Caso continue o or�amento ser� bloqueado pelos seguintes motivos: ', FBloqueios, True) then
      Abort;
//    Exclamar('Voc� n�o est� autorizado a conceder este percentual de desconto!');
//
//    vRetTelaEntregasAto := ValidarUsuarioAutorizado.validaUsuarioAutorizado;
//    if vRetTelaEntregasAto.RetTela <> trOk then begin
//      if not _ExibirMensagemMemo.Mensagem('Bloqueio de or�amento', 'Caso continue o or�amento ser� bloqueado pelos seguintes motivos: ', FBloqueios, True) then
//        Abort;
//    end;
//
//    funcionarioId := vRetTelaEntregasAto.RetInt;
//    funcionarios := _Funcionarios.BuscarFuncionarios(Sessao.getConexaoBanco, 0, [funcionarioId], False, False, False, False, False);
//
//    if funcionarios[0].GerenteSistema = False then begin
//      if funcionarios[0].PercentualDescontoAdicVenda < FrFechamento.ePercentualDesconto.AsCurr then begin
//        Exclamar('Funcion�rio n�o est� autorizado a conceder este percentual de desconto!');
//        if not _ExibirMensagemMemo.Mensagem('Bloqueio de or�amento', 'Caso continue o or�amento ser� bloqueado pelos seguintes motivos: ', FBloqueios, True) then
//          Abort;
//      end;
//    end
//    else
//      FBloqueios := nil;
  end;
end;

end.
