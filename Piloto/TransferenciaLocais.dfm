inherited FormTransferenciaLocais: TFormTransferenciaLocais
  Caption = 'Transfer'#234'ncia de locais'
  ClientHeight = 405
  ClientWidth = 742
  ExplicitWidth = 748
  ExplicitHeight = 434
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [1]
    Left = 510
    Top = 5
    Width = 63
    Height = 14
    Caption = 'Observa'#231#227'o'
  end
  object lb7: TLabel [2]
    Left = 419
    Top = 93
    Width = 64
    Height = 14
    Caption = 'Quantidade'
  end
  inherited pnOpcoes: TPanel
    Height = 405
    ExplicitHeight = 405
    inherited sbExcluir: TSpeedButton
      Visible = False
    end
  end
  inherited eID: TEditLuka
    TabOrder = 7
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 743
    TabOrder = 8
    Visible = False
    ExplicitLeft = 743
  end
  inline FrLocalOrigem: TFrLocais
    Left = 210
    Top = 2
    Width = 290
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 210
    ExplicitTop = 2
    ExplicitWidth = 290
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 265
      Height = 23
      ExplicitWidth = 265
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 290
      ExplicitWidth = 290
      inherited lbNomePesquisa: TLabel
        Width = 86
        Caption = 'Local de origem'
        ExplicitWidth = 86
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 185
        ExplicitLeft = 185
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 265
      Height = 24
      ExplicitLeft = 265
      ExplicitHeight = 24
    end
  end
  inline FrLocalDestino: TFrLocais
    Left = 427
    Top = 45
    Width = 310
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 427
    ExplicitTop = 45
    ExplicitWidth = 310
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 285
      Height = 23
      ExplicitWidth = 285
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 310
      ExplicitWidth = 310
      inherited lbNomePesquisa: TLabel
        Width = 89
        Caption = 'Local de destino'
        ExplicitWidth = 89
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 205
        ExplicitLeft = 205
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 285
      Height = 24
      ExplicitLeft = 285
      ExplicitHeight = 24
    end
  end
  object eObservacao: TEditLuka
    Left = 510
    Top = 19
    Width = 227
    Height = 22
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 200
    ParentFont = False
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrProduto: TFrProdutos
    Left = 126
    Top = 46
    Width = 297
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 46
    ExplicitWidth = 297
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 272
      Height = 23
      TabOrder = 1
      ExplicitWidth = 272
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 3
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 5
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 7
    end
    inherited PnTitulos: TPanel
      Width = 297
      TabOrder = 0
      ExplicitWidth = 297
      inherited lbNomePesquisa: TLabel
        Width = 42
        Caption = 'Produto'
        ExplicitWidth = 42
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 192
        ExplicitLeft = 192
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 272
      Height = 24
      TabOrder = 2
      ExplicitLeft = 272
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      TabOrder = 6
    end
  end
  object eQuantidade: TEditLuka
    Left = 419
    Top = 107
    Width = 81
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Text = '0,0000'
    OnKeyDown = eQuantidadeKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    PadraoEstoque = True
    NaoAceitarEspaco = False
  end
  inline FrLote: TFrameLotes
    Left = 125
    Top = 93
    Width = 287
    Height = 41
    Enabled = False
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 93
    inherited lbLote: TLabel
      Width = 23
      Height = 14
      ExplicitWidth = 23
      ExplicitHeight = 14
    end
    inherited lbDataFabricacao: TLabel
      Width = 75
      Height = 14
      ExplicitWidth = 75
      ExplicitHeight = 14
    end
    inherited lbDataVencimento: TLabel
      Width = 56
      Height = 14
      ExplicitWidth = 56
      ExplicitHeight = 14
    end
    inherited lbBitola: TLabel
      Width = 33
      Height = 14
      ExplicitWidth = 33
      ExplicitHeight = 14
    end
    inherited lbTonalidade: TLabel
      Width = 62
      Height = 14
      ExplicitWidth = 62
      ExplicitHeight = 14
    end
    inherited eLote: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited eDataFabricacao: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
    inherited eDataVencimento: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
    inherited eBitola: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited eTonalidade: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
  end
  object sgItens: TGridLuka
    Left = 125
    Top = 135
    Width = 613
    Height = 270
    Align = alCustom
    ColCount = 7
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 9
    OnDblClick = sgItensDblClick
    OnDrawCell = sgItensDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Und.'
      'Lote'
      'Quantidade'
      'Local destino')
    Grid3D = False
    RealColCount = 10
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      49
      209
      95
      32
      73
      78
      115)
  end
  object StaticTextLuka4: TStaticTextLuka
    Left = 625
    Top = 96
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Est. fisico'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stEstoqueFisico: TStaticText
    Left = 625
    Top = 112
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '5,0000'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 11
    Transparent = False
  end
  object stEstoqueDisponivel: TStaticText
    Left = 510
    Top = 112
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '10,0000'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    Transparent = False
  end
  object StaticTextLuka2: TStaticTextLuka
    Left = 510
    Top = 96
    Width = 105
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Est. dispon'#237'vel'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 13
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object frxReport: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 44043.704685613400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 360
    Top = 184
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstTrans
        DataSetName = 'frxdstTrans'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 150.000000000000000000
      PaperSize = 256
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 147.401670000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Top = 124.724490000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000001000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559059999999999000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000010000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133889999999990000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 196.535560000000000000
          Top = 49.133889999999990000
          Width = 30.236240000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000001000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000010000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133889999999990000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000001000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000001000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000010000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000010000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133889999999990000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 225.771800000000000000
          Top = 49.133889999999990000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_FAX"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133889999999990000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000001000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 68.811070000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo18: TfrxMemoView
          Left = 3.779530000000000000
          Top = 124.724490000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 64.252010000000000000
          Top = 124.724490000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 495.118430000000000000
          Top = 124.724490000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 627.401980000000000000
          Top = 124.724490000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Top = 71.811070000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            'Local Origem: [frxdstTrans."LocalOrigem"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 366.614410000000000000
          Top = 71.811070000000000000
          Width = 351.496290000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            'Local Destino: [frxdstTrans."LocalDestino"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 3.779530000000000000
          Top = 94.488250000000000000
          Width = 714.331170000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            'Observa'#231#227'o: [frxdstTrans."Observacao"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 226.771800000000000000
        Width = 718.110700000000000000
        DataSet = dstTrans
        DataSetName = 'frxdstTrans'
        RowCount = 0
        object frxdstTransNomeProduto: TfrxMemoView
          Left = 64.252010000000000000
          Top = 1.000000000000000000
          Width = 427.086890000000000000
          Height = 18.897650000000000000
          DataField = 'NomeProduto'
          DataSet = dstTrans
          DataSetName = 'frxdstTrans'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstTrans."NomeProduto"]')
          ParentFont = False
        end
        object frxdstTranscodigoProd: TfrxMemoView
          Left = 3.779530000000000000
          Top = 1.000000000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DataField = 'codigoProd'
          DataSet = dstTrans
          DataSetName = 'frxdstTrans'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstTrans."codigoProd"]')
          ParentFont = False
        end
        object frxdstTransLote: TfrxMemoView
          Left = 495.118430000000000000
          Top = 1.000000000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          DataField = 'Lote'
          DataSet = dstTrans
          DataSetName = 'frxdstTrans'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstTrans."Lote"]')
          ParentFont = False
        end
        object frxdstTransQuantidade: TfrxMemoView
          Left = 589.606680000000000000
          Top = 1.000000000000000000
          Width = 90.708720000000000000
          Height = 18.897650000000000000
          DataField = 'Quantidade'
          DataSet = dstTrans
          DataSetName = 'frxdstTrans'
          DisplayFormat.FormatStr = '000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTrans."Quantidade"]')
          ParentFont = False
        end
      end
    end
  end
  object dstTrans: TfrxDBDataset
    UserName = 'frxdstTrans'
    CloseDataSource = False
    FieldAliases.Strings = (
      'codigoProd=codigoProd'
      'NomeProduto=NomeProduto'
      'Marca=Marca'
      'Unidade=Unidade'
      'Quantidade=Quantidade'
      'LocalDestino=LocalDestino'
      'LocalOrigem=LocalOrigem'
      'Observacao=Observacao'
      'Lote=Lote')
    DataSet = cdsProduto
    BCDToCurrency = False
    Left = 360
    Top = 248
  end
  object OraSession1: TOraSession
    Options.Direct = True
    Username = 'ENCASA'
    Server = 'LOCALHOST:1521/HIVA'
    LoginPrompt = False
    HomeName = 'hiva'
    Left = 248
    Top = 216
    EncryptedPassword = 'BBFFBEFFBBFFB0FFACFF'
  end
  object cdsProduto: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'codigoProd'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'NomeProduto'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Marca'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Unidade'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Quantidade'
        DataType = ftFloat
      end
      item
        Name = 'LocalDestino'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'LocalOrigem'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'Observacao'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Lote'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 360
    Top = 312
    object cdsProdutocodigoProd: TStringField
      FieldName = 'codigoProd'
    end
    object cdsProdutoNomeProduto: TStringField
      FieldName = 'NomeProduto'
      Size = 60
    end
    object cdsProdutoMarca: TStringField
      FieldName = 'Marca'
      Size = 60
    end
    object cdsProdutoUnidade: TStringField
      FieldName = 'Unidade'
    end
    object cdsProdutoQuantidade: TFloatField
      FieldName = 'Quantidade'
    end
    object cdsProdutoLocalDestino: TStringField
      FieldName = 'LocalDestino'
      Size = 40
    end
    object cdsProdutoLocalOrigem: TStringField
      FieldName = 'LocalOrigem'
      Size = 40
    end
    object cdsProdutoObservacao: TStringField
      FieldName = 'Observacao'
      Size = 200
    end
    object cdsProdutoLote: TStringField
      FieldName = 'Lote'
    end
  end
end
