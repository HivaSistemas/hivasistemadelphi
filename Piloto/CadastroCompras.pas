unit CadastroCompras;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Mask, EditLukaData, FrameEmpresas, _Compras, _ComprasItens,
  FrameFuncionarios, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, _RecordsEspeciais,
  FrameFornecedores, FrameProdutos, StaticTextLuka, Vcl.Grids, GridLuka, PesquisaCompras,
  FrameMultiplosCompra, Vcl.ComCtrls, FramePlanosFinanceiros, FrameTiposCobranca,
  MemoAltis, _ComprasPrevisoes, FrameDadosCobranca, CheckBoxLuka, ImpressaoPedidoCompraGrafico;

type
  TFormCadastroCompras = class(TFormHerancaCadastroCodigo)
    FrFornecedor: TFrFornecedores;
    FrComprador: TFrFuncionarios;
    FrEmpresa: TFrEmpresas;
    pcDados: TPageControl;
    tsPrincipais: TTabSheet;
    tsProdutos: TTabSheet;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    FrProduto: TFrProdutos;
    eValorDesconto: TEditLuka;
    eValorOutrasDespesas: TEditLuka;
    eValorLiquido: TEditLuka;
    sgItens: TGridLuka;
    FrMultiplosCompra: TFrMultiplosCompra;
    eValorTotal: TEditLuka;
    lbl4: TLabel;
    ePrecoUnitario: TEditLuka;
    lbl3: TLabel;
    eQuantidade: TEditLuka;
    lbllb7: TLabel;
    StaticTextLuka1: TStaticTextLuka;
    stQuantidadeItensGrid: TStaticText;
    stValorTotal: TStaticText;
    StaticTextLuka2: TStaticTextLuka;
    lbl1: TLabel;
    lbl2: TLabel;
    eDataFaturamento: TEditLukaData;
    ePrevisaoEntrega: TEditLukaData;
    FrPlanoFinanceiro: TFrPlanosFinanceiros;
    meObservacoes: TMemoAltis;
    lbl8: TLabel;
    FrDadosCobranca: TFrDadosCobranca;
    StaticTextLuka3: TStaticTextLuka;
    procedure eValorOutrasDespesasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure sgItensDblClick(Sender: TObject);
    procedure sbDesfazerClick(Sender: TObject);
    procedure eValorOutrasDespesasChange(Sender: TObject);
  private
    FCadastrandoCompraViaSugestaoCompras: Boolean;

    procedure PreencherRegistro(pCompra: RecCompras; pItens: TArray<RecComprasItens>);

    procedure InserirProdutoNoGrid(
      pProdutoId: Integer;
      pNome: string;
      pMarca: string;
      pUnidade: string;
      pMultiploCompra: Double;
      pQuantidade: Double;
      pQuantidadeEmbalagem: Double;
      pPrecoUnitario: Double;
      pValorTotal: Double;
      pValorDesconto: Double;
      pValorOutDespesas: Double;
      pValorLiquido: Double
    );

    procedure CalcularTotalLiquido;

    procedure FrProdutoOnAposPesquisar(Sender: TObject);
    procedure FrProdutoOnAposDeletar(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

function GerarCompraViaSugestaoCompras(pItens: TArray<RecComprasItens>): TRetornoTelaFinalizar;

implementation

uses
  _Produtos;

{$R *.dfm}

{ TFormCadastroCompras }

const
  coProdutoId        = 0;
  coNome             = 1;
  coMarca            = 2;
  coUnidade          = 3;
  coQuantidade       = 4;
  coPrecoUnitario    = 5;
  coValorTotal       = 6;
  coValorDesconto    = 7;
  covalorOutDespesas = 8;
  coValorLiquido     = 9;
  coMultiploCompra   = 10;
  coQuantidadeEmbalagem = 11;


function GerarCompraViaSugestaoCompras(pItens: TArray<RecComprasItens>): TRetornoTelaFinalizar;
var
  i: Integer;
  vForm: TFormCadastroCompras;
begin
  vForm := TFormCadastroCompras.Create(nil);

  vForm.FormStyle := fsNormal;
  vForm.Visible := False;

  vForm.sbExcluir.Visible := False;
  vForm.sbPesquisar.Visible := False;
  vForm.Modo(True);
  vForm.FCadastrandoCompraViaSugestaoCompras := True;

  vForm.FrEmpresa.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
  vForm.FrComprador.InserirDadoPorChave(Sessao.getUsuarioLogado.funcionario_id, False);
  vForm.eDataFaturamento.AsData := Sessao.getData;

  for i := Low(pItens) to High(pItens) do begin
    vForm.InserirProdutoNoGrid(
      pItens[i].ProdutoId,
      pItens[i].NomeProduto,
      pItens[i].NomeMarca,
      pItens[i].UnidadeCompraId,
      pItens[i].MultiploCompra,
      pItens[i].Quantidade,
      pItens[i].QuantidadeEmbalagem,
      pItens[i].PrecoUnitario,
      pItens[i].ValorTotal,
      pItens[i].ValorDesconto,
      pItens[i].ValorOutrasDespesas,
      pItens[i].ValorLiquido
    );
  end;

  SetarFoco(vForm.FrFornecedor);

  vForm.CalcularTotalLiquido;

  Result.Ok(vForm.ShowModal, True);
end;

procedure TFormCadastroCompras.BuscarRegistro;
var
  vDados: TArray<RecCompras>;
begin
  vDados := _Compras.BuscarCompras(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vDados = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  if vDados[0].Status = 'C' then begin
    _Biblioteca.Exclamar('Um pedido de compra cancelado n�o pode ser editado!');
    SetarFoco(eId);
    Abort;
  end
  else if vDados[0].Status = 'B' then begin
    _Biblioteca.Exclamar('Um pedido de compra baixado n�o pode ser editado!');
    SetarFoco(eId);
    Abort;
  end;

  if _Compras.ExistemProdutosEntreguesBaixadosCancelados(Sessao.getConexaoBanco, eId.AsInt) then begin
    _Biblioteca.Exclamar('Um pedido de compra com produtos entregues, baixados ou cancelados n�o pode ser editado!');
    Abort;
  end;

  inherited;

  PreencherRegistro(vDados[0], _ComprasItens.BuscarComprasItens(Sessao.getConexaoBanco, 0, [eId.AsInt]));
end;

procedure TFormCadastroCompras.eValorOutrasDespesasChange(Sender: TObject);
begin
  inherited;
  eValorTotal.AsDouble := ePrecoUnitario.AsDouble * eQuantidade.AsDouble;
  eValorLiquido.AsDouble := eValorTotal.AsDouble - eValorDesconto.AsDouble + eValorOutrasDespesas.AsDouble;
end;

procedure TFormCadastroCompras.eValorOutrasDespesasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if FrProduto.EstaVazio then begin
    _Biblioteca.Exclamar('O produto n�o foi informado corretamente, verifique!');
    SetarFoco(FrProduto);
    Exit;
  end;

  if eQuantidade.AsCurr = 0 then begin
    _Biblioteca.Exclamar('A quantidade do produto n�o foi informado corretamente, verifique!');
    SetarFoco(eQuantidade);
    Exit;
  end;

  if ePrecoUnitario.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O pre�o unit�rio do produto n�o foi informado corretamente, verifique!');
    SetarFoco(ePrecoUnitario);
    Exit;
  end;

  if not _Biblioteca.ValidarMultiplo(eQuantidade.AsDouble, FrMultiplosCompra.Multiplo) then begin
    SetarFoco(eQuantidade);
    Exit;
  end;

  InserirProdutoNoGrid(
    FrProduto.getProduto().produto_id,
    FrProduto.getProduto().nome_compra,
    FrProduto.getProduto().nome_marca,
    FrMultiplosCompra.UnidadeId,
    FrMultiplosCompra.Multiplo,
    eQuantidade.AsDouble,
    FrMultiplosCompra.QuantidadeEmbalagem,
    ePrecoUnitario.AsDouble,
    eValorTotal.AsDouble,
    eValorDesconto.AsDouble,
    eValorOutrasDespesas.AsDouble,
    eValorLiquido.AsDouble
  );

  _Biblioteca.LimparCampos([FrProduto, eQuantidade, ePrecoUnitario, eValorTotal, eValorDesconto, eValorOutrasDespesas, eValorOutrasDespesas]);
  SetarFoco(FrProduto);
  CalcularTotalLiquido;
end;

procedure TFormCadastroCompras.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormCadastroCompras.FormCreate(Sender: TObject);
begin
  inherited;
  FrProduto.OnAposPesquisar := FrProdutoOnAposPesquisar;
  FrProduto.OnAposDeletar := FrProdutoOnAposDeletar;
  FrDadosCobranca.setEditDataBase(eDataFaturamento);
end;

procedure TFormCadastroCompras.FrProdutoOnAposDeletar(Sender: TObject);
begin
  _Biblioteca.LimparCampos([FrMultiplosCompra]);
end;

procedure TFormCadastroCompras.FrProdutoOnAposPesquisar(Sender: TObject);
var
  vCustoProduto: Double;
  vPrecoVenda: Double;
begin
  FrMultiplosCompra.SetProdutoId( FrProduto.getProduto.produto_id );

  if (not FrEmpresa.EstaVazio) and (not FrProduto.EstaVazio) then begin
    _Produtos.BuscarPrecoVendaCustoCompra(
      Sessao.getConexaoBanco,
      FrEmpresa.getEmpresa().EmpresaId,
      FrProduto.getProduto.produto_id,
      vPrecoVenda,
      vCustoProduto
    );

    ePrecoUnitario.AsDouble := vCustoProduto;
  end;
end;

procedure TFormCadastroCompras.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vItens: TArray<RecComprasItens>;

  vPlanoFinanceiroId: string;
begin
  vItens := nil;
  SetLength(vItens, sgItens.RowCount -1);
  for i := 1 to sgItens.RowCount -1 do begin
    vItens[i - 1].ProdutoId := SFormatInt(sgItens.Cells[coProdutoId, i]);

    vItens[i - 1].Quantidade          := SFormatDouble(sgItens.Cells[coQuantidade, i]);
    vItens[i - 1].PrecoUnitario       := SFormatDouble(sgItens.Cells[coPrecoUnitario, i]);
    vItens[i - 1].ValorTotal          := SFormatDouble(sgItens.Cells[coValorTotal, i]);
    vItens[i - 1].MultiploCompra      := SFormatDouble(sgItens.Cells[coMultiploCompra, i]);
    vItens[i - 1].QuantidadeEmbalagem := SFormatDouble(sgItens.Cells[coQuantidadeEmbalagem, i]);
    vItens[i - 1].UnidadeCompraId     := sgItens.Cells[coUnidade, i];
    vItens[i - 1].ValorDesconto       := SFormatDouble(sgItens.Cells[coValorDesconto, i]);
    vItens[i - 1].ValorOutrasDespesas := SFormatDouble(sgItens.Cells[covalorOutDespesas, i]);
    vItens[i - 1].ValorLiquido        := SFormatDouble(sgItens.Cells[coValorLiquido, i]);
  end;

  vPlanoFinanceiroId := '';
  if not FrPlanoFinanceiro.EstaVazio then
    vPlanoFinanceiroId := FrPlanoFinanceiro.getDados().PlanoFinanceiroId;

  vRetBanco :=
    _Compras.AtualizarCompras(
      Sessao.getConexaoBanco,
      eID.AsInt,
      FrFornecedor.GetFornecedor().cadastro_id,
      FrEmpresa.getEmpresa().EmpresaId,
      FrComprador.GetFuncionario().funcionario_id,
      eDataFaturamento.AsData,
      ePrevisaoEntrega.AsData,
      meObservacoes.Text,
      vPlanoFinanceiroId,
      SFormatCurr(stValorTotal.Caption),
      vItens,
      FrDadosCobranca.Titulos
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);

  if _Biblioteca.Perguntar('Deseja imprimir o pedido de compra agora?') then
    ImpressaoPedidoCompraGrafico.Imprimir(IIfInt(eID.AsInt > 0, eID.AsInt, vRetBanco.AsInt));

  inherited;
end;

procedure TFormCadastroCompras.InserirProdutoNoGrid(
  pProdutoId: Integer;
  pNome,
  pMarca,
  pUnidade: string;
  pMultiploCompra: Double;
  pQuantidade: Double;
  pQuantidadeEmbalagem: Double;
  pPrecoUnitario: Double;
  pValorTotal: Double;
  pValorDesconto: Double;
  pValorOutDespesas: Double;
  pValorLiquido: Double
);
var
  vLinha: Integer;
begin
  vLinha := sgItens.Localizar([coProdutoId], [NFormat(pProdutoId)]);
  if vLinha = -1 then begin

    if sgItens.Cells[coProdutoId, 1] = '' then
      vLinha := 1
    else begin
      vLinha := sgItens.RowCount;
      sgItens.RowCount := sgItens.RowCount + 1;
    end;
  end;

  sgItens.Cells[coProdutoId, vLinha]        := NFormat(pProdutoId);
  sgItens.Cells[coNome, vLinha]             := pNome;
  sgItens.Cells[coMarca, vLinha]            := pMarca;
  sgItens.Cells[coUnidade, vLinha]          := pUnidade;
  sgItens.Cells[coQuantidade, vLinha]       := NFormatEstoque(pQuantidade);
  sgItens.Cells[coPrecoUnitario, vLinha]    := NFormat(pPrecoUnitario);
  sgItens.Cells[coValorTotal, vLinha]       := NFormat(pValorTotal);
  sgItens.Cells[coValorDesconto, vLinha]    := NFormatN(pValorDesconto);
  sgItens.Cells[coValorOutDespesas, vLinha] := NFormatN(pValorOutDespesas);
  sgItens.Cells[coValorLiquido, vLinha]     := NFormat(pValorLiquido);
  sgItens.Cells[coMultiploCompra, vLinha]   := NFormat(pMultiploCompra, 4);
  sgItens.Cells[coQuantidadeEmbalagem, vLinha] := NFormat(pQuantidadeEmbalagem, 4);

  stQuantidadeItensGrid.Caption := NFormat(vLinha);
end;

procedure TFormCadastroCompras.Modo(pEditando: Boolean);
begin
  inherited;
  FCadastrandoCompraViaSugestaoCompras := False;

   _Biblioteca.Habilitar([
    pcDados,
    FrFornecedor,
    FrEmpresa,
    FrComprador,
    eDataFaturamento,
    ePrevisaoEntrega,
    FrPlanoFinanceiro,
    FrDadosCobranca,
    meObservacoes,
    FrProduto,
    ePrecoUnitario,
    eQuantidade,
    eValorTotal,
    eValorDesconto,
    eValorOutrasDespesas,
    eValorLiquido,
    sgItens,
    stQuantidadeItensGrid,
    stValorTotal,
    FrMultiplosCompra],
    pEditando
  );

  if pEditando then
    SetarFoco(FrFornecedor);
end;

procedure TFormCadastroCompras.PesquisarRegistro;
var
  vRetTela: TRetornoTelaFinalizar<RecDadosCompras>;
begin
  vRetTela := PesquisaCompras.Pesquisar;
  if vRetTela.BuscaCancelada then
    Exit;

  if vRetTela.Dados.Compra.Status = 'C' then begin
    _Biblioteca.Exclamar('Um pedido de compra cancelado n�o pode ser editado!');
    SetarFoco(eId);
    Abort;
  end
  else if vRetTela.Dados.Compra.Status = 'B' then begin
    _Biblioteca.Exclamar('Um pedido de compra baixado n�o pode ser editado!');
    SetarFoco(eId);
    Abort;
  end;

  if _Compras.ExistemProdutosEntreguesBaixadosCancelados(Sessao.getConexaoBanco, vRetTela.Dados.Compra.CompraId) then begin
    _Biblioteca.Exclamar('Um pedido de compra com produtos entregues, baixados ou cancelados n�o pode ser editado!');
    Abort;
  end;

  inherited;
  PreencherRegistro(vRetTela.Dados.Compra, vRetTela.Dados.Itens);
end;

procedure TFormCadastroCompras.PreencherRegistro(pCompra: RecCompras; pItens: TArray<RecComprasItens>);
var
  i: Integer;
begin
  eID.AsInt := pCompra.CompraId;
  FrFornecedor.InserirDadoPorChave( pCompra.FornecedorId );
  FrEmpresa.InserirDadoPorChave( pCompra.EmpresaId );
  FrComprador.InserirDadoPorChave( pCompra.CompradorId );
  eDataFaturamento.AsData := pCompra.DataFaturamento;
  ePrevisaoEntrega.AsData := pCompra.PrevisaoEntrega;
  FrPlanoFinanceiro.InserirDadoPorChave( pCompra.PlanoFinanceiroId );

  for i := Low(pItens) to High(pItens) do begin
    InserirProdutoNoGrid(
      pItens[i].ProdutoId,
      pItens[i].NomeProduto,
      pItens[i].NomeMarca,
      pItens[i].UnidadeCompraId,
      pItens[i].MultiploCompra,
      pItens[i].Quantidade,
      pItens[i].QuantidadeEmbalagem,
      pItens[i].PrecoUnitario,
      pItens[i].ValorTotal,
      pItens[i].ValorDesconto,
      pItens[i].ValorOutrasDespesas,
      pItens[i].ValorLiquido
    );
  end;

  FrDadosCobranca.Titulos := _ComprasPrevisoes.BuscarComprasPrevisoes(Sessao.getConexaoBanco, 0, [eId.AsInt]);

  CalcularTotalLiquido;
end;

procedure TFormCadastroCompras.CalcularTotalLiquido;
var
  i: Integer;
  vTotal: Double;
begin
  vTotal := 0;

  for i := 1 to sgItens.RowCount -1 do
    vTotal := vTotal + SFormatDouble(sgItens.Cells[coValorLiquido, i]);

  stValorTotal.Caption := NFormatN(vTotal);
  FrDadosCobranca.ValorPagar := vTotal;
end;

procedure TFormCadastroCompras.sbDesfazerClick(Sender: TObject);
begin
  inherited;
  if FCadastrandoCompraViaSugestaoCompras then
    ModalResult := mrCancel;
end;

procedure TFormCadastroCompras.sgItensDblClick(Sender: TObject);
begin
  inherited;
  if sgItens.Cells[coProdutoId, sgItens.Row] = '' then
    Exit;

  FrProduto.InserirDadoPorChave( SFormatInt(sgItens.Cells[coProdutoId, sgItens.Row]) );
  eQuantidade.AsDouble := SFormatDouble( sgItens.Cells[coQuantidade, sgItens.Row] );
  ePrecoUnitario.AsDouble := SFormatDouble( sgItens.Cells[coPrecoUnitario, sgItens.Row] );
  eValorTotal.AsDouble := SFormatDouble( sgItens.Cells[coValorTotal, sgItens.Row] );
  eValorDesconto.AsDouble := SFormatDouble( sgItens.Cells[coValorDesconto, sgItens.Row] );
  eValorOutrasDespesas.AsDouble := SFormatDouble( sgItens.Cells[covalorOutDespesas, sgItens.Row] );
  eValorLiquido.AsDouble := SFormatDouble( sgItens.Cells[coValorLiquido, sgItens.Row] );

  FrMultiplosCompra.setUnidadeDefinida(
    sgItens.Cells[coUnidade, sgItens.Row],
    SFormatDouble(sgItens.Cells[coMultiploCompra, sgItens.Row]),
    SFormatDouble(sgItens.Cells[coQuantidadeEmbalagem, sgItens.Row])
  );

  SetarFoco( FrProduto );
end;

procedure TFormCadastroCompras.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coProdutoId,
    coQuantidade,
    coPrecoUnitario,
    coValorTotal,
    coValorDesconto,
    covalorOutDespesas,
    coValorLiquido]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCadastroCompras.sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
    sgItens.DeleteRow(sgItens.Row);
end;

procedure TFormCadastroCompras.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrFornecedor.EstaVazio then begin
    _Biblioteca.Exclamar('O fornecedor da compra n�o foi informado corretamente, verifique!');
    SetarFoco(FrFornecedor);
    Abort;
  end;

  if FrEmpresa.EstaVazio then begin
    _Biblioteca.Exclamar('A empresa da compra n�o foi informada corretamente, verifique!');
    SetarFoco(FrEmpresa);
    Abort;
  end;

  if FrComprador.EstaVazio then begin
    _Biblioteca.Exclamar('O comprador n�o foi informado corretamente, verifique!');
    SetarFoco(FrComprador);
    Abort;
  end;

  if not eDataFaturamento.DataOk then begin
    _Biblioteca.Exclamar('A data de faturamento da compra n�o foi informada corretamente, verifique!');
    SetarFoco(eDataFaturamento);
    Abort;
  end;

  if not ePrevisaoEntrega.DataOk then begin
    _Biblioteca.Exclamar('A previs�o de entrega da compra n�o foi informada corretamente, verifique!');
    SetarFoco(ePrevisaoEntrega);
    Abort;
  end;

  if sgItens.Cells[coProdutoId, 1] = '' then begin
    _Biblioteca.Exclamar('Nenhum produto foi informado na compra, verifique!');
    SetarFoco(FrProduto);
    Abort;
  end;

  if FrDadosCobranca.ValorDefinido > 0 then begin
    if FrDadosCobranca.ExisteDiferenca then begin
      _Biblioteca.Exclamar('O valor total dos t�tulos informados n�o � o mesmo valor dos produtos informados, verifique!');
      SetarFoco(FrDadosCobranca);
      Abort;
    end;

    if FrPlanoFinanceiro.EstaVazio then begin
      _Biblioteca.Exclamar('Ao informar previs�o financeira o plano financeiro deve ser informado!');
      SetarFoco(FrPlanoFinanceiro);
      Abort;
    end;
  end;
end;

end.
