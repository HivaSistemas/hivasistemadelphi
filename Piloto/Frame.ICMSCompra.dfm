inherited FrICMSCompra: TFrICMSCompra
  Width = 768
  Height = 391
  ExplicitWidth = 768
  ExplicitHeight = 391
  object pnBotoes: TPanel
    Left = 678
    Top = 0
    Width = 90
    Height = 391
    Align = alRight
    Anchors = []
    BevelOuter = bvNone
    TabOrder = 0
    object lb1: TLabel
      Left = 5
      Top = 4
      Width = 66
      Height = 14
      Caption = 'Estado sel.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbEstadoSelecionado: TLabel
      Left = 5
      Top = 19
      Width = 26
      Height = 13
      Caption = 'Goi'#225's'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 38619
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object sbPreencher: TSpeedButton
      Left = 8
      Top = 78
      Width = 75
      Height = 26
      Caption = 'Pre. todos'
      Flat = True
      OnClick = sbPreencherClick
    end
    object eValor: TEditLuka
      Left = 3
      Top = 55
      Width = 85
      Height = 21
      Hint = 'Valor a ser preenchido em todos os estados'
      Alignment = taRightJustify
      CharCase = ecUpperCase
      TabOrder = 0
      OnKeyDown = eValorKeyDown
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
  end
  object pnGrid: TPanel
    Left = 0
    Top = 0
    Width = 678
    Height = 391
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object sgICMS: TGridLuka
      Left = 0
      Top = 0
      Width = 678
      Height = 391
      Align = alClient
      ColCount = 8
      DefaultRowHeight = 19
      DrawingStyle = gdsGradient
      FixedColor = 15395562
      FixedCols = 0
      RowCount = 29
      FixedRows = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
      TabOrder = 0
      OnDrawCell = sgICMSDrawCell
      OnSelectCell = sgICMSSelectCell
      IncrementExpandCol = 0
      IncrementExpandRow = 0
      CorLinhaFoco = 38619
      CorFundoFoco = 16774625
      CorLinhaDesfoque = 14869218
      CorFundoDesfoque = 16382457
      CorSuperiorCabecalho = clWhite
      CorInferiorCabecalho = 13553358
      CorSeparadorLinhas = 12040119
      CorColunaFoco = clActiveCaption
      HCol.Strings = (
        'Estado'
        'Revenda'
        'Distribuidor'
        'Industria '
        'Aliq. ICMS  '
        'IVA'
        'Ind. reducao'
        'Pre'#231'o pauta')
      HRow.Strings = (
        'Estado'
        'AC'
        'AL'
        'AP'
        'AM'
        'BA'
        'CE'
        'DF'
        'ES'
        'GO'
        'MA'
        'MT'
        'MS'
        'MG'
        'PA'
        'PB'
        'PR'
        'PE'
        'PI'
        'RJ'
        'RN'
        'RS'
        'RO'
        'RR'
        'SC'
        'SP'
        'SE'
        'TO')
      Grid3D = False
      RealColCount = 12
      Indicador = True
      AtivarPopUpSelecao = False
      ColWidths = (
        41
        59
        77
        55
        63
        53
        72
        81)
    end
  end
end
