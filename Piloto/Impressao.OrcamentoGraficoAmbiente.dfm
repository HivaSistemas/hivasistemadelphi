inherited FormImpressaoOrcamentoGraficoAmbiente: TFormImpressaoOrcamentoGraficoAmbiente
  Caption = 'FormImpressaoOrcamentoGraficoAmbiente'
  ClientHeight = 749
  ClientWidth = 1229
  OnCreate = FormCreate
  ExplicitLeft = 0
  ExplicitTop = -71
  ExplicitWidth = 1245
  ExplicitHeight = 788
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioNF: TQuickRep
    Left = 0
    Top = -64
    Height = 527
    BeforePrint = qrRelatorioNFBeforePrint
    Font.Height = -7
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrRelatorioNFNeedData
    Page.Values = (
      0.000000000000000000
      871.471354166666800000
      10.000000000000000000
      750.755208333333200000
      10.000000000000000000
      0.000000000000000000
      0.000000000000000000)
    ExplicitLeft = 0
    ExplicitTop = -64
    ExplicitHeight = 527
    inherited qrBandTitulo: TQRBand
      Height = 236
      Size.Values = (
        390.260416666666700000
        740.833333333333300000)
      ExplicitHeight = 236
      inherited qrlNFNomeEmpresa: TQRLabel
        Left = 1
        Top = 3
        Width = 439
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          4.960937500000000000
          725.950520833333200000)
        FontSize = 6
        ExplicitLeft = 1
        ExplicitTop = 3
        ExplicitWidth = 439
        ExplicitHeight = 17
      end
      inherited qrNFCnpj: TQRLabel
        Left = 1
        Top = 16
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          26.458333333333330000
          641.614583333333200000)
        Font.Height = -8
        FontSize = 6
        ExplicitLeft = 1
        ExplicitTop = 16
        ExplicitHeight = 17
      end
      inherited qrNFEndereco: TQRLabel
        Left = 1
        Top = 32
        Height = 19
        Size.Values = (
          31.419270833333330000
          1.653645833333333000
          52.916666666666670000
          639.960937500000000000)
        Font.Height = -8
        FontSize = 6
        ExplicitLeft = 1
        ExplicitTop = 32
        ExplicitHeight = 19
      end
      inherited qrNFCidadeUf: TQRLabel
        Left = 1
        Top = 46
        Height = 19
        Size.Values = (
          31.419270833333330000
          1.653645833333333000
          76.067708333333320000
          277.812500000000000000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -8
        FontSize = 6
        ExplicitLeft = 1
        ExplicitTop = 46
        ExplicitHeight = 19
      end
      inherited qrNFTipoDocumento: TQRLabel
        Left = 130
        Top = 96
        Width = 213
        Size.Values = (
          33.072916666666670000
          214.973958333333300000
          158.750000000000000000
          352.226562500000000000)
        Caption = 'OR'#199'AMENTO DE VENDAS'
        FontSize = 7
        ExplicitLeft = 130
        ExplicitTop = 96
        ExplicitWidth = 213
      end
      inherited qrSeparador2: TQRShape
        Left = -8
        Top = 122
        Width = 462
        Height = 1
        Size.Values = (
          1.653645833333333000
          -13.229166666666670000
          201.744791666666700000
          763.984375000000000000)
        ExplicitLeft = -8
        ExplicitTop = 122
        ExplicitWidth = 462
        ExplicitHeight = 1
      end
      inherited qrNFTitulo: TQRLabel
        Left = 432
        Width = 4
        Size.Values = (
          33.072916666666670000
          714.375000000000000000
          122.369791666666700000
          6.614583333333332000)
        Font.Height = -7
        FontSize = 5
        ExplicitLeft = 432
        ExplicitWidth = 4
      end
      inherited qrlNFEmitidoEm: TQRLabel
        Left = 1
        Top = 74
        Width = 202
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          122.369791666666700000
          334.036458333333300000)
        Alignment = taLeftJustify
        Font.Height = -8
        Font.Style = []
        FontSize = 6
        ExplicitLeft = 1
        ExplicitTop = 74
        ExplicitWidth = 202
        ExplicitHeight = 17
      end
      inherited qrlNFTelefoneEmpresa: TQRLabel
        Left = 1
        Top = 60
        Width = 157
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          99.218750000000000000
          259.622395833333300000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -8
        FontSize = 6
        ExplicitLeft = 1
        ExplicitTop = 60
        ExplicitWidth = 157
        ExplicitHeight = 17
      end
      object QRLabel4: TQRLabel
        Left = 1
        Top = 126
        Width = 94
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          208.359375000000000000
          155.442708333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'N'#186' Or'#231'amento:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel6: TQRLabel
        Left = 241
        Top = 126
        Width = 39
        Height = 17
        Size.Values = (
          28.111979166666670000
          398.528645833333300000
          208.359375000000000000
          64.492187500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vend.:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrOrcamentoIdTM: TQRLabel
        Left = 96
        Top = 126
        Width = 41
        Height = 17
        Size.Values = (
          28.111979166666670000
          158.750000000000000000
          208.359375000000000000
          67.799479166666680000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1615'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrVendedorTM: TQRLabel
        Left = 280
        Top = 126
        Width = 166
        Height = 17
        Size.Values = (
          28.111979166666670000
          463.020833333333300000
          208.359375000000000000
          274.505208333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ricardo Sampaio'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrBairroClienteTM: TQRLabel
        Left = 285
        Top = 174
        Width = 162
        Height = 17
        Size.Values = (
          28.111979166666670000
          471.289062500000000000
          287.734375000000000000
          267.890625000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Jardim Rosa do Sul'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrCelularClienteTM: TQRLabel
        Left = 292
        Top = 141
        Width = 153
        Height = 17
        Size.Values = (
          28.111979166666670000
          482.864583333333300000
          233.164062500000000000
          253.007812500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(62) 98631-2848'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrCepClienteTM: TQRLabel
        Left = 32
        Top = 190
        Width = 66
        Height = 17
        Size.Values = (
          28.111979166666670000
          52.916666666666670000
          314.192708333333300000
          109.140625000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '74980-970'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrCidadeClienteTM: TQRLabel
        Left = 51
        Top = 174
        Width = 192
        Height = 17
        Size.Values = (
          28.111979166666670000
          84.335937500000000000
          287.734375000000000000
          317.500000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Aparecida de Goiania'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrClienteTM: TQRLabel
        Left = 54
        Top = 141
        Width = 186
        Height = 17
        Size.Values = (
          28.111979166666670000
          89.296875000000000000
          233.164062500000000000
          307.578125000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ezequiel Eugenio do Prado'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrEnderecoClienteTM: TQRLabel
        Left = 31
        Top = 157
        Width = 208
        Height = 17
        Size.Values = (
          28.111979166666670000
          51.263020833333330000
          259.622395833333300000
          343.958333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Av. Brasil qd 27 lt 30 N'#186' 606'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrEstadoClienteTM: TQRLabel
        Left = 264
        Top = 157
        Width = 182
        Height = 17
        Size.Values = (
          28.111979166666670000
          436.562500000000000000
          259.622395833333300000
          300.963541666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Mato Grosso do Sul'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel11: TQRLabel
        Left = 1
        Top = 190
        Width = 33
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          314.192708333333300000
          54.570312500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CEP:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel12: TQRLabel
        Left = 241
        Top = 141
        Width = 51
        Height = 17
        Size.Values = (
          28.111979166666670000
          398.528645833333300000
          233.164062500000000000
          84.335937500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Celular:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel13: TQRLabel
        Left = 241
        Top = 157
        Width = 24
        Height = 17
        Size.Values = (
          28.111979166666670000
          398.528645833333300000
          259.622395833333300000
          39.687500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'UF:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel5: TQRLabel
        Left = 1
        Top = 157
        Width = 30
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          259.622395833333300000
          49.609375000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'End.:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel7: TQRLabel
        Left = 241
        Top = 174
        Width = 44
        Height = 17
        Size.Values = (
          28.111979166666670000
          398.528645833333300000
          287.734375000000000000
          72.760416666666680000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Bairro:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel8: TQRLabel
        Left = 1
        Top = 141
        Width = 50
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          233.164062500000000000
          82.682291666666680000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cliente:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel9: TQRLabel
        Left = 1
        Top = 174
        Width = 50
        Height = 17
        Size.Values = (
          28.111979166666670000
          1.653645833333333000
          287.734375000000000000
          82.682291666666680000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cidade:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRShape4: TQRShape
        Left = -8
        Top = 213
        Width = 462
        Height = 1
        Size.Values = (
          1.653645833333333000
          -13.229166666666670000
          352.226562500000000000
          763.984375000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Brush.Color = 15395562
        Shape = qrsRectangle
        VertAdjust = 0
      end
      object QRLabel1: TQRLabel
        Left = 4
        Top = 217
        Width = 30
        Height = 18
        Size.Values = (
          29.765625000000000000
          6.614583333333332000
          358.841145833333300000
          49.609375000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cod.:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel2: TQRLabel
        Left = 34
        Top = 217
        Width = 244
        Height = 18
        Size.Values = (
          29.765625000000000000
          56.223958333333330000
          358.841145833333300000
          403.489583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel3: TQRLabel
        Left = 280
        Top = 217
        Width = 42
        Height = 18
        Size.Values = (
          29.765625000000000000
          463.020833333333300000
          358.841145833333300000
          69.453125000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vl unt'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel14: TQRLabel
        Left = 324
        Top = 217
        Width = 32
        Height = 18
        Size.Values = (
          29.765625000000000000
          535.781250000000000000
          358.841145833333300000
          52.916666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtd'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel15: TQRLabel
        Left = 358
        Top = 217
        Width = 27
        Height = 18
        Size.Values = (
          29.765625000000000000
          592.005208333333200000
          358.841145833333300000
          44.648437500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Und'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel16: TQRLabel
        Left = 390
        Top = 217
        Width = 52
        Height = 18
        Size.Values = (
          29.765625000000000000
          644.921875000000000000
          358.841145833333300000
          85.989583333333320000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
    end
    object QRBand1: TQRBand [1]
      Left = 6
      Top = 242
      Width = 448
      Height = 22
      AlignToBottom = False
      BeforePrint = QRBand1BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        36.380208333333330000
        740.833333333333300000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrProdutoIdTM: TQRLabel
        Left = 1
        Top = 1
        Width = 30
        Height = 20
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          1.653645833333333000
          49.609375000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cod.:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrNomeProdutoTM: TQRLabel
        Left = 31
        Top = 1
        Width = 244
        Height = 20
        Size.Values = (
          33.072916666666670000
          51.263020833333330000
          1.653645833333333000
          403.489583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrValorUnitarioTM: TQRLabel
        Left = 277
        Top = 1
        Width = 42
        Height = 20
        Size.Values = (
          33.072916666666670000
          458.059895833333300000
          1.653645833333333000
          69.453125000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vl unt'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrQuantidadeTM: TQRLabel
        Left = 321
        Top = 1
        Width = 32
        Height = 20
        Size.Values = (
          33.072916666666670000
          530.820312500000000000
          1.653645833333333000
          52.916666666666670000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtd'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrUnidadeTM: TQRLabel
        Left = 355
        Top = 1
        Width = 27
        Height = 20
        Size.Values = (
          33.072916666666670000
          587.044270833333200000
          1.653645833333333000
          44.648437500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Und'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrValorTotalTM: TQRLabel
        Left = 387
        Top = 1
        Width = 58
        Height = 20
        Size.Values = (
          33.072916666666670000
          639.960937500000000000
          1.653645833333333000
          95.911458333333320000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
    end
    inherited PageFooterBand1: TQRBand
      Top = 264
      Height = 81
      Size.Values = (
        133.945312500000000000
        740.833333333333300000)
      ExplicitTop = 264
      ExplicitHeight = 81
      inherited qrlNFSistema: TQRLabel
        Left = 328
        Top = 293
        Width = 126
        Size.Values = (
          23.151041666666670000
          542.395833333333400000
          484.518229166666800000
          208.359375000000000000)
        FontSize = -6
        ExplicitLeft = 328
        ExplicitTop = 293
        ExplicitWidth = 126
      end
      object QRLabel10: TQRLabel
        Left = 243
        Top = 4
        Width = 118
        Height = 17
        Size.Values = (
          28.111979166666670000
          401.835937500000000000
          6.614583333333332000
          195.130208333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel17: TQRLabel
        Left = 245
        Top = 18
        Width = 110
        Height = 17
        Size.Values = (
          28.111979166666670000
          405.143229166666700000
          29.765625000000000000
          181.901041666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Outras despesas:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel18: TQRLabel
        Left = 245
        Top = 33
        Width = 118
        Height = 17
        Size.Values = (
          28.111979166666670000
          405.143229166666700000
          54.570312500000000000
          195.130208333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Frete:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel19: TQRLabel
        Left = 245
        Top = 46
        Width = 118
        Height = 17
        Size.Values = (
          28.111979166666670000
          405.143229166666700000
          76.067708333333320000
          195.130208333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Desconto:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object QRLabel20: TQRLabel
        Left = 243
        Top = 62
        Width = 128
        Height = 17
        Size.Values = (
          28.111979166666670000
          401.835937500000000000
          102.526041666666700000
          211.666666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total geral:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrTotalProdutosTM: TQRLabel
        Left = 351
        Top = 3
        Width = 87
        Height = 17
        Size.Values = (
          28.111979166666670000
          580.429687500000000000
          4.960937500000000000
          143.867187500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrValorOutrasDespesastm: TQRLabel
        Left = 352
        Top = 18
        Width = 86
        Height = 18
        Size.Values = (
          29.765625000000000000
          582.083333333333200000
          29.765625000000000000
          142.213541666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Outras despesas'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrlGrValorFreteTM: TQRLabel
        Left = 351
        Top = 34
        Width = 87
        Height = 17
        Size.Values = (
          28.111979166666670000
          580.429687500000000000
          56.223958333333330000
          143.867187500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Frete'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrValorDescontoTM: TQRLabel
        Left = 351
        Top = 48
        Width = 87
        Height = 17
        Size.Values = (
          28.111979166666670000
          580.429687500000000000
          79.375000000000000000
          143.867187500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Desconto'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
      object qrTotalSerPagoTM: TQRLabel
        Left = 351
        Top = 62
        Width = 87
        Height = 17
        Size.Values = (
          28.111979166666670000
          580.429687500000000000
          102.526041666666700000
          143.867187500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total geral'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 6
      end
    end
    object qrMensagensFinaisTM: TQRMemo
      Left = 6
      Top = 450
      Width = 239
      Height = 75
      Size.Values = (
        124.023437500000000000
        9.921875000000000000
        744.140625000000000000
        395.221354166666700000)
      XLColumn = 0
      XLNumFormat = nfGeneral
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -8
      Font.Name = 'Arial'
      Font.Style = []
      Lines.Strings = (
        'Or'#231'amento v'#225'lido pelo prazo de 15 dias'
        '')
      ParentFont = False
      Transparent = False
      FullJustify = False
      MaxBreakChars = 0
      FontSize = 6
    end
  end
  inherited qrRelatorioA4: TQuickRep
    Left = -13
    Top = 1
    Width = 952
    Height = 1347
    BeforePrint = qrRelatorioBeforePrint
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrRelatorioNeedData
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    Zoom = 120
    ExplicitLeft = -13
    ExplicitTop = 1
    ExplicitWidth = 952
    ExplicitHeight = 1347
    inherited qrTitulo: TQRBand [0]
      Left = 45
      Top = 251
      Width = 861
      Height = 94
      Size.Values = (
        207.256944444444400000
        1898.385416666667000000)
      ExplicitLeft = 45
      ExplicitTop = 251
      ExplicitWidth = 861
      ExplicitHeight = 94
      inherited QRShape2: TQRShape
        Left = -1
        Top = 77
        Width = 862
        Height = 13
        Size.Values = (
          28.663194444444440000
          -2.204861111111111000
          169.774305555555600000
          1900.590277777778000000)
        ExplicitLeft = -1
        ExplicitTop = 77
        ExplicitWidth = 862
        ExplicitHeight = 13
      end
    end
    inherited qrCabecalho: TQRBand [1]
      Left = 45
      Top = 45
      Width = 861
      Height = 206
      Size.Values = (
        454.201388888888900000
        1898.385416666667000000)
      ExplicitLeft = 45
      ExplicitTop = 45
      ExplicitWidth = 861
      ExplicitHeight = 206
      inherited qrCaptionEndereco: TQRLabel
        Left = 103
        Top = 27
        Width = 64
        Height = 17
        Size.Values = (
          37.482638888888890000
          227.100694444444400000
          59.531250000000000000
          141.111111111111100000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 103
        ExplicitTop = 27
        ExplicitWidth = 64
        ExplicitHeight = 17
      end
      inherited qrEmpresa: TQRLabel
        Left = 171
        Top = 10
        Width = 333
        Height = 17
        Size.Values = (
          37.482638888888890000
          377.031250000000000000
          22.048611111111110000
          734.218750000000000000)
        Caption = 'Hiva Solucoes Ltda'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        FontSize = 9
        ExplicitLeft = 171
        ExplicitTop = 10
        ExplicitWidth = 333
        ExplicitHeight = 17
      end
      inherited qrEndereco: TQRLabel
        Left = 171
        Top = 27
        Width = 333
        Height = 17
        Size.Values = (
          37.482638888888890000
          377.031250000000000000
          59.531250000000000000
          734.218750000000000000)
        Caption = 'Av Independencia qd 20 lote 30 Nr 100'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 171
        ExplicitTop = 27
        ExplicitWidth = 333
        ExplicitHeight = 17
      end
      inherited qrCaptionEmpresa: TQRLabel
        Left = 103
        Top = 10
        Width = 64
        Height = 17
        Size.Values = (
          37.482638888888890000
          227.100694444444400000
          22.048611111111110000
          141.111111111111100000)
        Alignment = taLeftJustify
        Caption = 'Empresa:'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 103
        ExplicitTop = 10
        ExplicitWidth = 64
        ExplicitHeight = 17
      end
      inherited qrEmitidoEm: TQRLabel
        Left = 744
        Top = 3
        Width = 112
        Height = 13
        Size.Values = (
          28.663194444444440000
          1640.416666666667000000
          6.614583333333332000
          246.944444444444400000)
        Caption = 'Data 11/04/2014 '#225's 13:57:55'
        Font.Name = 'Cambria'
        FontSize = 5
        ExplicitLeft = 744
        ExplicitTop = 3
        ExplicitWidth = 112
        ExplicitHeight = 13
      end
      inherited qr1: TQRLabel
        Left = 510
        Top = 27
        Width = 65
        Height = 17
        Size.Values = (
          37.482638888888890000
          1124.479166666667000000
          59.531250000000000000
          143.315972222222200000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 510
        ExplicitTop = 27
        ExplicitWidth = 65
        ExplicitHeight = 17
      end
      inherited qrBairro: TQRLabel
        Left = 584
        Top = 27
        Width = 273
        Height = 17
        Size.Values = (
          37.482638888888890000
          1287.638888888889000000
          59.531250000000000000
          601.927083333333200000)
        Caption = 'Setor Serra Dourada III'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 584
        ExplicitTop = 27
        ExplicitWidth = 273
        ExplicitHeight = 17
      end
      inherited qr3: TQRLabel
        Left = 510
        Top = 46
        Width = 72
        Height = 17
        Size.Values = (
          37.482638888888890000
          1124.479166666667000000
          101.423611111111100000
          158.750000000000000000)
        Alignment = taLeftJustify
        Caption = 'Cid./UF:'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 510
        ExplicitTop = 46
        ExplicitWidth = 72
        ExplicitHeight = 17
      end
      inherited qrCidadeUf: TQRLabel
        Left = 584
        Top = 46
        Width = 273
        Height = 17
        Size.Values = (
          37.482638888888890000
          1287.638888888889000000
          101.423611111111100000
          601.927083333333200000)
        Caption = 'Aparecida de Goi'#226'nia / GO'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 584
        ExplicitTop = 46
        ExplicitWidth = 273
        ExplicitHeight = 17
      end
      inherited qr5: TQRLabel
        Left = 510
        Top = 10
        Width = 72
        Height = 17
        Size.Values = (
          37.482638888888890000
          1124.479166666667000000
          22.048611111111110000
          158.750000000000000000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 510
        ExplicitTop = 10
        ExplicitWidth = 72
        ExplicitHeight = 17
      end
      inherited qrCNPJ: TQRLabel
        Left = 584
        Top = 10
        Width = 152
        Height = 17
        Size.Values = (
          37.482638888888890000
          1287.638888888889000000
          22.048611111111110000
          335.138888888888900000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 584
        ExplicitTop = 10
        ExplicitWidth = 152
        ExplicitHeight = 17
      end
      inherited qr7: TQRLabel
        Left = 103
        Top = 46
        Width = 64
        Height = 17
        Size.Values = (
          37.482638888888890000
          227.100694444444400000
          101.423611111111100000
          141.111111111111100000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 103
        ExplicitTop = 46
        ExplicitWidth = 64
        ExplicitHeight = 17
      end
      inherited qrTelefone: TQRLabel
        Left = 171
        Top = 46
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          377.031250000000000000
          101.423611111111100000
          244.739583333333300000)
        Caption = '(62) 9999-9999'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 171
        ExplicitTop = 46
        ExplicitWidth = 111
        ExplicitHeight = 17
      end
      inherited qrFax: TQRLabel
        Left = 171
        Top = 64
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          377.031250000000000000
          141.111111111111100000
          244.739583333333300000)
        Caption = '(62) 9999-9999'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 171
        ExplicitTop = 64
        ExplicitWidth = 111
        ExplicitHeight = 17
      end
      inherited qr10: TQRLabel
        Left = 103
        Top = 64
        Width = 64
        Height = 17
        Size.Values = (
          37.482638888888890000
          227.100694444444400000
          141.111111111111100000
          141.111111111111100000)
        Alignment = taLeftJustify
        Caption = 'Whatsapp: '
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 103
        ExplicitTop = 64
        ExplicitWidth = 64
        ExplicitHeight = 17
      end
      inherited qr11: TQRLabel
        Left = 510
        Top = 64
        Width = 72
        Height = 17
        Size.Values = (
          37.482638888888890000
          1124.479166666667000000
          141.111111111111100000
          158.750000000000000000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 510
        ExplicitTop = 64
        ExplicitWidth = 72
        ExplicitHeight = 17
      end
      inherited qrEmail: TQRLabel
        Left = 584
        Top = 64
        Width = 273
        Height = 17
        Size.Values = (
          37.482638888888890000
          1287.638888888889000000
          141.111111111111100000
          601.927083333333200000)
        Caption = 'hivasolucoes@gmail.com'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 584
        ExplicitTop = 64
        ExplicitWidth = 273
        ExplicitHeight = 17
      end
      inherited qr13: TQRLabel
        Left = 316
        Top = 90
        Width = 214
        Height = 28
        Size.Values = (
          61.736111111111110000
          696.736111111111100000
          198.437500000000000000
          471.840277777777800000)
        Caption = 'Or'#231'amento'
        Font.Charset = ANSI_CHARSET
        Font.Height = -20
        Font.Name = 'Cambria'
        FontSize = 15
        ExplicitLeft = 316
        ExplicitTop = 90
        ExplicitWidth = 214
        ExplicitHeight = 28
      end
      inherited QRShape1: TQRShape
        Top = 120
        Width = 862
        Height = 2
        Enabled = True
        Size.Values = (
          4.409722222222222000
          0.000000000000000000
          264.583333333333300000
          1900.590277777778000000)
        ExplicitTop = 120
        ExplicitWidth = 862
        ExplicitHeight = 2
      end
      inherited qrLogoEmpresa: TQRImage
        Left = 6
        Width = 96
        Height = 96
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
        ExplicitLeft = 6
        ExplicitWidth = 96
        ExplicitHeight = 96
      end
      inherited qrLabelCep: TQRLabel
        Left = 751
        Top = 37
        Width = 34
        Height = 18
        Size.Values = (
          39.687500000000000000
          1656.291666666667000000
          82.020833333333320000
          74.083333333333320000)
        FontSize = 9
        ExplicitLeft = 751
        ExplicitTop = 37
        ExplicitWidth = 34
        ExplicitHeight = 18
      end
      inherited qrCEP: TQRLabel
        Left = 787
        Top = 37
        Width = 78
        Height = 18
        Size.Values = (
          39.687500000000000000
          1735.666666666667000000
          82.020833333333320000
          171.979166666666700000)
        FontSize = 9
        ExplicitLeft = 787
        ExplicitTop = 37
        ExplicitWidth = 78
        ExplicitHeight = 18
      end
    end
    inherited qrBandaCabecalhoColunas: TQRBand
      Left = 45
      Top = 345
      Width = 861
      Height = 17
      Size.Values = (
        37.482638888888890000
        1898.385416666667000000)
      ExplicitLeft = 45
      ExplicitTop = 345
      ExplicitWidth = 861
      ExplicitHeight = 17
    end
    inherited qrBandaDetalhes: TQRBand
      Left = 45
      Top = 410
      Width = 861
      Height = 38
      BeforePrint = qrBandaDetalhesBeforePrint
      Size.Values = (
        83.784722222222220000
        1898.385416666667000000)
      ExplicitLeft = 45
      ExplicitTop = 410
      ExplicitWidth = 861
      ExplicitHeight = 38
      object QRShape3: TQRShape
        Left = -1
        Top = 58
        Width = 862
        Height = 2
        Size.Values = (
          4.409722222222222000
          -2.204861111111111000
          127.881944444444500000
          1900.590277777778000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qrProdutoId: TQRLabel
        Left = 2
        Top = 0
        Width = 51
        Height = 16
        Size.Values = (
          35.277777777777780000
          4.409722222222222000
          0.000000000000000000
          112.447916666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Produto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrNomeProduto: TQRLabel
        Left = 54
        Top = 0
        Width = 296
        Height = 16
        Size.Values = (
          35.277777777777780000
          119.062500000000000000
          0.000000000000000000
          652.638888888889000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrMarca: TQRLabel
        Left = 350
        Top = 0
        Width = 77
        Height = 16
        Size.Values = (
          35.277777777777780000
          771.701388888889000000
          0.000000000000000000
          169.774305555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Votorantim'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrQuantidade: TQRLabel
        Left = 428
        Top = 0
        Width = 53
        Height = 16
        Size.Values = (
          35.277777777777780000
          943.680555555555700000
          0.000000000000000000
          116.857638888888900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '2350,000'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrUnidade: TQRLabel
        Left = 483
        Top = 0
        Width = 45
        Height = 16
        Size.Values = (
          35.277777777777780000
          1064.947916666667000000
          0.000000000000000000
          99.218750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Und.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorUnitario: TQRLabel
        Left = 529
        Top = 0
        Width = 69
        Height = 16
        Size.Values = (
          35.277777777777780000
          1166.371527777778000000
          0.000000000000000000
          152.135416666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pre'#231'o unit.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrDescontoUnitario: TQRLabel
        Left = 600
        Top = 0
        Width = 65
        Height = 16
        Size.Values = (
          35.277777777777780000
          1322.916666666667000000
          0.000000000000000000
          143.315972222222200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '150,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrPrcoUnitLiq: TQRLabel
        Left = 671
        Top = 0
        Width = 84
        Height = 16
        Size.Values = (
          35.277777777777780000
          1479.461805555556000000
          0.000000000000000000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '150,00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrValorTotal: TQRLabel
        Left = 760
        Top = 0
        Width = 100
        Height = 16
        Size.Values = (
          35.277777777777780000
          1675.694444444444000000
          0.000000000000000000
          220.486111111111200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total do produto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    inherited qrbndRodape: TQRBand
      Left = 45
      Top = 496
      Width = 861
      Height = 186
      Size.Values = (
        410.104166666666700000
        1898.385416666667000000)
      ExplicitLeft = 45
      ExplicitTop = 496
      ExplicitWidth = 861
      ExplicitHeight = 186
      inherited qrUsuarioImpressao: TQRLabel
        Left = 4
        Top = 169
        Width = 161
        Height = 14
        Size.Values = (
          30.868055555555560000
          8.819444444444444000
          372.621527777777800000
          354.982638888888900000)
        Caption = 'EZEQUIEL EUGENIO DO PRADO'
        Font.Height = -7
        Font.Name = 'Cambria'
        FontSize = 5
        ExplicitLeft = 4
        ExplicitTop = 169
        ExplicitWidth = 161
        ExplicitHeight = 14
      end
      inherited qrSistema: TQRLabel
        Left = 4
        Top = 74
        Width = 168
        Height = 14
        Size.Values = (
          30.868055555555560000
          8.819444444444444000
          163.159722222222200000
          370.416666666666700000)
        Alignment = taLeftJustify
        Caption = 'Hiva 3.0'
        Font.Height = -7
        Font.Name = 'Cambria'
        FontSize = 5
        ExplicitLeft = 4
        ExplicitTop = 74
        ExplicitWidth = 168
        ExplicitHeight = 14
      end
      object qrMensagensFinais: TQRMemo
        Left = 4
        Top = 94
        Width = 853
        Height = 56
        Size.Values = (
          123.472222222222200000
          8.819444444444444000
          207.256944444444400000
          1880.746527777778000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        Lines.Strings = (
          'Or'#231'amento v'#225'lido pelo prazo de 15 dias'
          '')
        ParentFont = False
        Transparent = False
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 9
      end
      object qr20: TQRLabel
        Left = 601
        Top = 22
        Width = 122
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          48.506944444444440000
          268.993055555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Outras despesas:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr22: TQRLabel
        Left = 601
        Top = 54
        Width = 122
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          119.062500000000000000
          268.993055555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desconto: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr29: TQRLabel
        Left = 601
        Top = 71
        Width = 122
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          156.545138888888900000
          268.993055555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total geral:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrTotalSerPago: TQRLabel
        Left = 725
        Top = 71
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          156.545138888888900000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total ser pago: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrValorDesconto: TQRLabel
        Left = 725
        Top = 54
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          119.062500000000000000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desconto: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrValorOutrasDespesas: TQRLabel
        Left = 725
        Top = 22
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          48.506944444444440000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor outras desp: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrl1: TQRLabel
        Left = 601
        Top = 37
        Width = 119
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          81.579861111111100000
          262.378472222222200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Frete:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrlGrValorFrete: TQRLabel
        Left = 725
        Top = 37
        Width = 108
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          81.579861111111100000
          238.125000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor frete'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrTotalProdutos: TQRLabel
        Left = 725
        Top = 6
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          13.229166666666670000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr16: TQRLabel
        Left = 601
        Top = 6
        Width = 122
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          13.229166666666670000
          268.993055555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
    end
    object QRGroup2: TQRGroup
      Left = 45
      Top = 362
      Width = 861
      Height = 48
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        105.833333333333300000
        1898.385416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      Master = qrRelatorioA4
      ReprintOnNewPage = False
      object QRLabel21: TQRLabel
        Left = 3
        Top = 0
        Width = 69
        Height = 15
        Size.Values = (
          33.072916666666670000
          6.614583333333332000
          0.000000000000000000
          152.135416666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ambiente: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qrAmbiente: TQRLabel
        Left = 75
        Top = -1
        Width = 296
        Height = 16
        Size.Values = (
          35.277777777777780000
          165.364583333333300000
          -2.204861111111111000
          652.638888888889000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Ambiente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel22: TQRLabel
        Left = 3
        Top = 18
        Width = 52
        Height = 15
        Size.Values = (
          33.072916666666670000
          6.614583333333332000
          39.687500000000000000
          114.652777777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Produto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel23: TQRLabel
        Left = 55
        Top = 18
        Width = 296
        Height = 15
        Size.Values = (
          33.072916666666670000
          121.267361111111100000
          39.687500000000000000
          652.638888888889000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel24: TQRLabel
        Left = 350
        Top = 18
        Width = 77
        Height = 15
        Size.Values = (
          33.072916666666670000
          771.701388888889000000
          39.687500000000000000
          169.774305555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Marca'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel25: TQRLabel
        Left = 428
        Top = 18
        Width = 55
        Height = 15
        Size.Values = (
          33.072916666666670000
          943.680555555555700000
          39.687500000000000000
          121.267361111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel26: TQRLabel
        Left = 485
        Top = 18
        Width = 45
        Height = 15
        Size.Values = (
          33.072916666666670000
          1069.357638888889000000
          39.687500000000000000
          99.218750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Und.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel27: TQRLabel
        Left = 530
        Top = 18
        Width = 70
        Height = 15
        Size.Values = (
          33.072916666666670000
          1168.576388888889000000
          39.687500000000000000
          154.340277777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pre'#231'o unit.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qlDescontoUnitario: TQRLabel
        Left = 602
        Top = 18
        Width = 65
        Height = 15
        Size.Values = (
          33.072916666666670000
          1327.326388888889000000
          39.687500000000000000
          143.315972222222200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Desconto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object qlPrcoUnitLiq: TQRLabel
        Left = 669
        Top = 18
        Width = 88
        Height = 15
        Size.Values = (
          33.072916666666670000
          1475.052083333333000000
          39.687500000000000000
          194.027777777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Prco unit liq'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel30: TQRLabel
        Left = 761
        Top = 18
        Width = 100
        Height = 15
        Size.Values = (
          33.072916666666670000
          1677.899305555556000000
          39.687500000000000000
          220.486111111111200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produto'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    object QRSubDetail1: TQRSubDetail
      Left = 45
      Top = 448
      Width = 861
      Height = 48
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        105.833333333333300000
        1898.385416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      Master = qrRelatorioA4
      PrintBefore = False
      PrintIfEmpty = True
    end
  end
  object qrOrcamentoAmbiente: TQuickRep [2]
    Left = 251
    Top = 29
    Width = 952
    Height = 1347
    DataSet = qMestre
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.MemoryLimit = 1000000
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 120
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PrevInitialZoom = qrZoomToWidth
    PreviewDefaultSaveType = stPDF
    PreviewLeft = 0
    PreviewTop = 0
    object QRBand2: TQRBand
      Left = 45
      Top = 45
      Width = 861
      Height = 208
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        458.611111111111100000
        1898.385416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageHeader
      object QRLabel28: TQRLabel
        Left = 103
        Top = 26
        Width = 64
        Height = 18
        Size.Values = (
          39.687500000000000000
          227.541666666666700000
          58.208333333333340000
          140.229166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Endere'#231'o: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrEmpresaAmbiente: TQRLabel
        Left = 170
        Top = 10
        Width = 334
        Height = 18
        Size.Values = (
          39.687500000000000000
          375.708333333333400000
          21.166666666666670000
          735.541666666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Hiva Solucoes Ltda'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrEnderecoEmpresaAmbiente: TQRLabel
        Left = 170
        Top = 26
        Width = 334
        Height = 18
        Size.Values = (
          39.687500000000000000
          375.708333333333400000
          58.208333333333340000
          735.541666666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Av Independencia qd 20 lote 30 Nr 100'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel32: TQRLabel
        Left = 103
        Top = 10
        Width = 64
        Height = 18
        Size.Values = (
          39.687500000000000000
          227.541666666666700000
          21.166666666666670000
          140.229166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Empresa:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrEmitidoEmAmbiente: TQRLabel
        Left = 736
        Top = 3
        Width = 123
        Height = 13
        Size.Values = (
          28.663194444444450000
          1622.777777777778000000
          6.614583333333332000
          271.197916666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Data 11/04/2014 '#225's 13:57:55'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel34: TQRLabel
        Left = 510
        Top = 26
        Width = 65
        Height = 18
        Size.Values = (
          39.687500000000000000
          1124.479166666667000000
          58.208333333333340000
          142.875000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Bairro: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrBairroEmpresaAmbiente: TQRLabel
        Left = 584
        Top = 26
        Width = 272
        Height = 18
        Size.Values = (
          39.687500000000000000
          1288.520833333333000000
          58.208333333333340000
          600.604166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Setor Serra Dourada III'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel36: TQRLabel
        Left = 510
        Top = 46
        Width = 72
        Height = 18
        Size.Values = (
          39.687500000000000000
          1124.479166666667000000
          100.541666666666700000
          158.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cid./UF:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCidadeEmpresaAmbiente: TQRLabel
        Left = 584
        Top = 46
        Width = 272
        Height = 18
        Size.Values = (
          39.687500000000000000
          1288.520833333333000000
          100.541666666666700000
          600.604166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Aparecida de Goi'#226'nia / GO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel38: TQRLabel
        Left = 510
        Top = 10
        Width = 72
        Height = 18
        Size.Values = (
          39.687500000000000000
          1124.479166666667000000
          21.166666666666670000
          158.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CNPJ: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCNPJAmbiente: TQRLabel
        Left = 584
        Top = 10
        Width = 152
        Height = 18
        Size.Values = (
          39.687500000000000000
          1288.520833333333000000
          21.166666666666670000
          336.020833333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '00.000.000/0001-00'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel40: TQRLabel
        Left = 103
        Top = 46
        Width = 64
        Height = 18
        Size.Values = (
          39.687500000000000000
          227.541666666666700000
          100.541666666666700000
          140.229166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Telefone: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrTelefoneEmpresaAmbiente: TQRLabel
        Left = 170
        Top = 46
        Width = 110
        Height = 18
        Size.Values = (
          39.687500000000000000
          375.708333333333400000
          100.541666666666700000
          243.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(62) 9999-9999'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCelularEmpresaAmbiente: TQRLabel
        Left = 170
        Top = 64
        Width = 110
        Height = 18
        Size.Values = (
          39.687500000000000000
          375.708333333333400000
          140.229166666666700000
          243.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(62) 9999-9999'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel43: TQRLabel
        Left = 103
        Top = 64
        Width = 64
        Height = 18
        Size.Values = (
          39.687500000000000000
          227.541666666666700000
          140.229166666666700000
          140.229166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Whatsapp: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel44: TQRLabel
        Left = 510
        Top = 64
        Width = 72
        Height = 18
        Size.Values = (
          39.687500000000000000
          1124.479166666667000000
          140.229166666666700000
          158.750000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'E-mail: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrEmailEmpresaAmbiente: TQRLabel
        Left = 584
        Top = 64
        Width = 272
        Height = 18
        Size.Values = (
          39.687500000000000000
          1288.520833333333000000
          140.229166666666700000
          600.604166666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'hivasolucoes@gmail.com'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel46: TQRLabel
        Left = 246
        Top = 90
        Width = 359
        Height = 29
        Size.Values = (
          63.940972222222230000
          542.395833333333400000
          198.437500000000000000
          791.545138888889000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Or'#231'amento por ambiente'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -20
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 15
      end
      object QRShape5: TQRShape
        Left = 0
        Top = 120
        Width = 862
        Height = 2
        Size.Values = (
          4.409722222222222000
          0.000000000000000000
          264.583333333333300000
          1900.590277777778000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qrLogoEmpresaAmbiente: TQRImage
        Left = 6
        Top = 1
        Width = 96
        Height = 96
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Center = True
        Stretch = True
      end
      object QRLabel47: TQRLabel
        Left = 751
        Top = 37
        Width = 34
        Height = 18
        Size.Values = (
          39.687500000000000000
          1656.291666666667000000
          82.020833333333320000
          74.083333333333320000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CEP:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCEPAmbiente: TQRLabel
        Left = 787
        Top = 37
        Width = 78
        Height = 18
        Size.Values = (
          39.687500000000000000
          1735.666666666667000000
          82.020833333333320000
          171.979166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '74916-200'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr2: TQRLabel
        Left = 6
        Top = 126
        Width = 75
        Height = 17
        Size.Values = (
          37.482638888888890000
          13.229166666666670000
          277.812500000000000000
          165.364583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Orcamento:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr8: TQRLabel
        Left = 6
        Top = 142
        Width = 66
        Height = 17
        Size.Values = (
          37.482638888888890000
          13.229166666666670000
          313.090277777777800000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cliente: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr12: TQRLabel
        Left = 6
        Top = 158
        Width = 66
        Height = 17
        Size.Values = (
          37.482638888888890000
          13.229166666666670000
          348.368055555555600000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Endereco: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr18: TQRLabel
        Left = 6
        Top = 175
        Width = 66
        Height = 17
        Size.Values = (
          37.482638888888890000
          13.229166666666670000
          385.850694444444400000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cid./UF:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCidadeCliente: TQRLabel
        Left = 73
        Top = 175
        Width = 388
        Height = 17
        Size.Values = (
          37.482638888888890000
          160.954861111111100000
          385.850694444444400000
          855.486111111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'GOI'#194'NIA'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrEnderecoCliente: TQRLabel
        Left = 73
        Top = 158
        Width = 393
        Height = 17
        Size.Values = (
          37.482638888888890000
          160.954861111111100000
          348.368055555555600000
          866.510416666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'RUA 1020 QD36B LT 15 N'#186' 503'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCliente: TQRLabel
        Left = 73
        Top = 142
        Width = 388
        Height = 17
        Size.Values = (
          37.482638888888890000
          160.954861111111100000
          313.090277777777800000
          855.486111111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '8 - TALITA C'#194'NDIA LIMA SILVA'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrOrcamentoId: TQRLabel
        Left = 83
        Top = 126
        Width = 114
        Height = 17
        Size.Values = (
          37.482638888888890000
          183.003472222222200000
          277.812500000000000000
          251.354166666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '146'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr9: TQRLabel
        Left = 195
        Top = 126
        Width = 66
        Height = 17
        Size.Values = (
          37.482638888888890000
          429.947916666666700000
          277.812500000000000000
          145.520833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vendedor:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrVendedor: TQRLabel
        Left = 262
        Top = 126
        Width = 199
        Height = 17
        Size.Values = (
          37.482638888888890000
          577.673611111111100000
          277.812500000000000000
          438.767361111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'JOSE DA SILVA'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr17: TQRLabel
        Left = 467
        Top = 126
        Width = 84
        Height = 17
        Size.Values = (
          37.482638888888890000
          1029.670138888889000000
          277.812500000000000000
          185.208333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cond. pagto.: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCondicaoPagamento: TQRLabel
        Left = 552
        Top = 126
        Width = 314
        Height = 17
        Size.Values = (
          37.482638888888890000
          1217.083333333333000000
          277.812500000000000000
          692.326388888888900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1 - Avista'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr4: TQRLabel
        Left = 467
        Top = 142
        Width = 32
        Height = 17
        Size.Values = (
          37.482638888888890000
          1029.670138888889000000
          313.090277777777800000
          70.555555555555560000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Tel.: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrTelefoneCliente: TQRLabel
        Left = 500
        Top = 142
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1102.430555555556000000
          313.090277777777800000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(62) 3288-1655'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr14: TQRLabel
        Left = 636
        Top = 142
        Width = 29
        Height = 17
        Size.Values = (
          37.482638888888890000
          1402.291666666667000000
          313.090277777777800000
          63.940972222222220000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cel.: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCelularCliente: TQRLabel
        Left = 667
        Top = 142
        Width = 117
        Height = 17
        Size.Values = (
          37.482638888888890000
          1470.642361111111000000
          313.090277777777800000
          257.968750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '(62) 9876-5432'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr15: TQRLabel
        Left = 467
        Top = 158
        Width = 43
        Height = 17
        Size.Values = (
          37.482638888888890000
          1029.670138888889000000
          348.368055555555600000
          94.809027777777780000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Bairro: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrBairroCliente: TQRLabel
        Left = 511
        Top = 158
        Width = 346
        Height = 17
        Size.Values = (
          37.482638888888890000
          1126.684027777778000000
          348.368055555555600000
          762.881944444444400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'SETOR PEDRO LUDOVICO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr21: TQRLabel
        Left = 467
        Top = 175
        Width = 43
        Height = 17
        Size.Values = (
          37.482638888888890000
          1029.670138888889000000
          385.850694444444400000
          94.809027777777780000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CEP: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrCepCliente: TQRLabel
        Left = 511
        Top = 175
        Width = 79
        Height = 17
        Size.Values = (
          37.482638888888890000
          1126.684027777778000000
          385.850694444444400000
          174.184027777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '74800-008'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrIndice: TQRLabel
        Left = 703
        Top = 174
        Width = 79
        Height = 17
        Enabled = False
        Size.Values = (
          37.482638888888890000
          1550.017361111111000000
          383.645833333333300000
          174.184027777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1,25'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRShape6: TQRShape
        Left = 0
        Top = 197
        Width = 862
        Height = 2
        Size.Values = (
          4.409722222222222000
          0.000000000000000000
          434.357638888888900000
          1900.590277777778000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
    end
    object qrGroup: TQRGroup
      Left = 45
      Top = 253
      Width = 861
      Height = 27
      AlignToBottom = False
      BeforePrint = qrGroupBeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        59.531250000000000000
        1898.385416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      Expression = 'qMestre.AMBIENTE'
      Master = qrOrcamentoAmbiente
      ReprintOnNewPage = False
      object QRDBText1: TQRDBText
        Left = 85
        Top = 3
        Width = 336
        Height = 20
        Size.Values = (
          44.097222222222230000
          187.413194444444400000
          6.614583333333332000
          740.833333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qMestre
        DataField = 'AMBIENTE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
      object QRLabel49: TQRLabel
        Left = 2
        Top = 1
        Width = 77
        Height = 20
        Size.Values = (
          44.097222222222220000
          4.409722222222222000
          2.204861111111111000
          169.774305555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Ambiente:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
    end
    object qrGroupHeader: TQRBand
      Left = 45
      Top = 280
      Width = 861
      Height = 22
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        48.506944444444440000
        1898.385416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbGroupHeader
      object QRLabel50: TQRLabel
        Left = 2
        Top = 1
        Width = 52
        Height = 17
        Size.Values = (
          37.482638888888900000
          4.409722222222222000
          2.204861111111111000
          114.652777777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Produto'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel53: TQRLabel
        Left = 54
        Top = 1
        Width = 296
        Height = 17
        Size.Values = (
          37.482638888888900000
          119.062500000000000000
          2.204861111111111000
          652.638888888889000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel54: TQRLabel
        Left = 350
        Top = 1
        Width = 77
        Height = 17
        Size.Values = (
          37.482638888888900000
          771.701388888889000000
          2.204861111111111000
          169.774305555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Marca'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel55: TQRLabel
        Left = 427
        Top = 1
        Width = 55
        Height = 17
        Size.Values = (
          37.482638888888900000
          941.475694444444400000
          2.204861111111111000
          121.267361111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel56: TQRLabel
        Left = 482
        Top = 1
        Width = 45
        Height = 17
        Size.Values = (
          37.482638888888900000
          1062.743055555556000000
          2.204861111111111000
          99.218750000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Und.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel51: TQRLabel
        Left = 527
        Top = 1
        Width = 70
        Height = 17
        Size.Values = (
          37.482638888888900000
          1161.961805555556000000
          2.204861111111111000
          154.340277777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pre'#231'o unit.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel58: TQRLabel
        Left = 597
        Top = 1
        Width = 65
        Height = 17
        Size.Values = (
          37.482638888888900000
          1316.302083333333000000
          2.204861111111111000
          143.315972222222200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Desconto'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel59: TQRLabel
        Left = 662
        Top = 1
        Width = 88
        Height = 17
        Size.Values = (
          37.482638888888900000
          1459.618055555556000000
          2.204861111111111000
          194.027777777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pre'#231'o unit. liq.'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
      object QRLabel60: TQRLabel
        Left = 750
        Top = 1
        Width = 88
        Height = 17
        Size.Values = (
          37.482638888888900000
          1653.645833333333000000
          2.204861111111111000
          194.027777777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produto'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    object qrSubdetail: TQRSubDetail
      Left = 45
      Top = 302
      Width = 861
      Height = 23
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        50.711805555555560000
        1898.385416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      Master = qrOrcamentoAmbiente
      DataSet = qDetalhe
      FooterBand = qrGroupFooter
      HeaderBand = qrGroupHeader
      PrintBefore = False
      PrintIfEmpty = True
      object QRDBText2: TQRDBText
        Left = 2
        Top = 0
        Width = 52
        Height = 17
        Size.Values = (
          37.482638888888900000
          4.409722222222222000
          0.000000000000000000
          114.652777777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qDetalhe
        DataField = 'PRODUTO_ID'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
      object QRDBText3: TQRDBText
        Left = 427
        Top = 0
        Width = 55
        Height = 17
        Size.Values = (
          37.482638888888900000
          941.475694444444400000
          0.000000000000000000
          121.267361111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qDetalhe
        DataField = 'QUANTIDADE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
      object QRDBText5: TQRDBText
        Left = 54
        Top = -1
        Width = 296
        Height = 18
        Size.Values = (
          39.687500000000000000
          119.062500000000000000
          -2.204861111111111000
          652.638888888889000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qDetalhe
        DataField = 'NOME_PRODUTO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
      object QRDBText6: TQRDBText
        Left = 350
        Top = 0
        Width = 77
        Height = 17
        Size.Values = (
          37.482638888888900000
          771.701388888889000000
          0.000000000000000000
          169.774305555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qDetalhe
        DataField = 'NOME_MARCA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
      object QRDBText7: TQRDBText
        Left = 482
        Top = 0
        Width = 46
        Height = 17
        Size.Values = (
          37.482638888888900000
          1062.743055555556000000
          0.000000000000000000
          101.423611111111100000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qDetalhe
        DataField = 'UNIDADE_VENDA'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
      object QRDBText8: TQRDBText
        Left = 528
        Top = 0
        Width = 69
        Height = 17
        Size.Values = (
          37.482638888888900000
          1164.166666666667000000
          0.000000000000000000
          152.135416666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qDetalhe
        DataField = 'PRECO_UNITARIO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
      object QRDBText9: TQRDBText
        Left = 597
        Top = 0
        Width = 65
        Height = 17
        Size.Values = (
          37.482638888888900000
          1316.302083333333000000
          0.000000000000000000
          143.315972222222200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qDetalhe
        DataField = 'VALOR_TOTAL_DESCONTO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
      object QRDBText10: TQRDBText
        Left = 662
        Top = 0
        Width = 88
        Height = 17
        Size.Values = (
          37.482638888888900000
          1459.618055555556000000
          0.000000000000000000
          194.027777777777800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qDetalhe
        DataField = 'PRECO_LIQUIDO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
      object QRDBText11: TQRDBText
        Left = 751
        Top = 0
        Width = 87
        Height = 17
        Size.Values = (
          37.482638888888900000
          1655.850694444444000000
          0.000000000000000000
          191.822916666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qDetalhe
        DataField = 'TOTAL_PRODUTO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
    end
    object qrGroupFooter: TQRBand
      Left = 45
      Top = 325
      Width = 861
      Height = 25
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        55.121527777777780000
        1898.385416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbGroupFooter
      object QRLabel52: TQRLabel
        Left = 608
        Top = -2
        Width = 113
        Height = 20
        Size.Values = (
          44.097222222222220000
          1340.555555555556000000
          -4.409722222222222000
          249.149305555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Total produtos:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 10
      end
      object QRDBText4: TQRDBText
        Left = 725
        Top = -2
        Width = 113
        Height = 20
        Size.Values = (
          44.097222222222230000
          1598.524305555556000000
          -4.409722222222222000
          249.149305555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qDetalheTotal
        DataField = 'TOTAL_PRODUTO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
    end
    object QRBand3: TQRBand
      Left = 45
      Top = 350
      Width = 861
      Height = 186
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        410.104166666666700000
        1898.385416666667000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageFooter
      object qrUsuarioImpressaoAmbiente: TQRLabel
        Left = 4
        Top = 169
        Width = 161
        Height = 14
        Size.Values = (
          30.868055555555560000
          8.819444444444444000
          372.621527777777800000
          354.982638888888900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'EZEQUIEL EUGENIO DO PRADO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRLabel31: TQRLabel
        Left = 4
        Top = 74
        Width = 168
        Height = 14
        Size.Values = (
          30.868055555555560000
          8.819444444444444000
          163.159722222222200000
          370.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Hiva 3.0'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -7
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 5
      end
      object QRMemo1: TQRMemo
        Left = 4
        Top = 94
        Width = 853
        Height = 56
        Size.Values = (
          123.472222222222200000
          8.819444444444444000
          207.256944444444400000
          1880.746527777778000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        Lines.Strings = (
          'Or'#231'amento v'#225'lido pelo prazo de 15 dias'
          '')
        ParentFont = False
        Transparent = False
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 9
      end
      object QRLabel33: TQRLabel
        Left = 601
        Top = 22
        Width = 122
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          48.506944444444440000
          268.993055555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Outras despesas:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel35: TQRLabel
        Left = 601
        Top = 54
        Width = 122
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          119.062500000000000000
          268.993055555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desconto: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel37: TQRLabel
        Left = 601
        Top = 71
        Width = 122
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          156.545138888888900000
          268.993055555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total geral:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrTotalGeralAmbiente: TQRLabel
        Left = 725
        Top = 71
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          156.545138888888900000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total ser pago: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrValorDescontoAmbiente: TQRLabel
        Left = 725
        Top = 54
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          119.062500000000000000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desconto: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrOutrasDespesasAmbiente: TQRLabel
        Left = 725
        Top = 22
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          48.506944444444440000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor outras desp: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel45: TQRLabel
        Left = 601
        Top = 37
        Width = 119
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          81.579861111111100000
          262.378472222222200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Frete:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrValorFreteAmbiente: TQRLabel
        Left = 725
        Top = 37
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888900000
          1598.524305555556000000
          81.579861111111120000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor frete'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrTotalProdutosAmbiente: TQRLabel
        Left = 725
        Top = 6
        Width = 111
        Height = 17
        Size.Values = (
          37.482638888888890000
          1598.524305555556000000
          13.229166666666670000
          244.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel61: TQRLabel
        Left = 601
        Top = 6
        Width = 122
        Height = 17
        Size.Values = (
          37.482638888888890000
          1325.121527777778000000
          13.229166666666670000
          268.993055555555600000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos: '
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
    end
  end
  object QRPDFFilter1: TQRPDFFilter
    CompressionOn = False
    TextEncoding = AnsiEncoding
    Codepage = '1252'
    Left = 24
    Top = 248
  end
  object OraSession1: TOraSession
    Options.Direct = True
    Username = 'DESENVOLVIMENTO'
    Server = 'localhost:1521:ORCL'
    Left = 535
    Top = 530
    EncryptedPassword = 'BBFFBEFFBBFFB0FFACFF'
  end
  object qMestre: TOraQuery
    Session = OraSession1
    SQL.Strings = (
      'SELECT DISTINCT'
      '  AMBIENTE'
      'FROM ORCAMENTOS_ITENS_AMBIENTE'
      ''
      'WHERE ORCAMENTO_ID = :ORCAMENTO')
    AfterScroll = qMestreAfterScroll
    Left = 429
    Top = 506
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ORCAMENTO'
        Value = nil
      end>
    object qMestreAMBIENTE: TStringField
      FieldName = 'AMBIENTE'
      Required = True
      Size = 200
    end
  end
  object qDetalhe: TOraQuery
    Session = OraSession1
    SQL.Strings = (
      'SELECT'
      '  OIA.PRODUTO_ID,'
      '  PRO.NOME AS NOME_PRODUTO,'
      '  OIA.QUANTIDADE,'
      '  ITE.PRECO_UNITARIO,'
      '  MAR.NOME AS NOME_MARCA,'
      '  OIA.QUANTIDADE * ITE.PRECO_UNITARIO AS TOTAL_PRODUTO,'
      '  PRO.UNIDADE_VENDA,'
      
        '  (ITE.VALOR_TOTAL_DESCONTO / ITE.QUANTIDADE) * OIA.QUANTIDADE A' +
        'S VALOR_TOTAL_DESCONTO,'
      '  ITE.PRECO_LIQUIDO'
      'FROM ORCAMENTOS_ITENS_AMBIENTE OIA'
      ''
      'INNER JOIN ORCAMENTOS_ITENS ITE'
      'ON OIA.PRODUTO_ID = ITE.PRODUTO_ID'
      'AND OIA.ORCAMENTO_ID = ITE.ORCAMENTO_ID'
      ''
      'INNER JOIN PRODUTOS PRO'
      'ON OIA.PRODUTO_ID = PRO.PRODUTO_iD'
      'AND ITE.PRODUTO_ID = PRO.PRODUTO_iD'
      ''
      'INNER JOIN MARCAS MAR'
      'ON PRO.MARCA_ID = MAR.MARCA_ID'
      ''
      'WHERE OIA.ORCAMENTO_ID = :ORCAMENTO'
      'AND OIA.AMBIENTE = :AMBIENTE')
    Left = 368
    Top = 505
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ORCAMENTO'
        Value = nil
      end
      item
        DataType = ftUnknown
        Name = 'AMBIENTE'
        Value = nil
      end>
    object qDetalhePRODUTO_ID: TFloatField
      FieldName = 'PRODUTO_ID'
      Required = True
    end
    object qDetalheNOME_PRODUTO: TStringField
      FieldName = 'NOME_PRODUTO'
      Size = 60
    end
    object qDetalheQUANTIDADE: TFloatField
      FieldName = 'QUANTIDADE'
      Required = True
      DisplayFormat = '0.000'
    end
    object qDetalhePRECO_UNITARIO: TFloatField
      FieldName = 'PRECO_UNITARIO'
      currency = True
    end
    object qDetalheNOME_MARCA: TStringField
      FieldName = 'NOME_MARCA'
      Size = 40
    end
    object qDetalheTOTAL_PRODUTO: TFloatField
      FieldName = 'TOTAL_PRODUTO'
      currency = True
    end
    object qDetalheUNIDADE_VENDA: TStringField
      FieldName = 'UNIDADE_VENDA'
      Size = 5
    end
    object qDetalheVALOR_TOTAL_DESCONTO: TFloatField
      FieldName = 'VALOR_TOTAL_DESCONTO'
      currency = True
    end
    object qDetalhePRECO_LIQUIDO: TFloatField
      FieldName = 'PRECO_LIQUIDO'
      currency = True
    end
  end
  object qDetalheTotal: TOraQuery
    Session = OraSession1
    SQL.Strings = (
      'SELECT'
      '  SUM(OIA.QUANTIDADE * ITE.PRECO_UNITARIO) AS TOTAL_PRODUTO'
      'FROM ORCAMENTOS_ITENS_AMBIENTE OIA'
      ''
      'INNER JOIN ORCAMENTOS_ITENS ITE'
      'ON OIA.PRODUTO_ID = ITE.PRODUTO_ID'
      'AND OIA.ORCAMENTO_ID = ITE.ORCAMENTO_ID'
      ''
      'WHERE OIA.AMBIENTE = :AMBIENTE'
      'AND OIA.ORCAMENTO_ID = :ORCAMENTO')
    Left = 299
    Top = 505
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'AMBIENTE'
        Value = nil
      end
      item
        DataType = ftUnknown
        Name = 'ORCAMENTO'
        Value = nil
      end>
    object qDetalheTotalTOTAL_PRODUTO: TFloatField
      FieldName = 'TOTAL_PRODUTO'
      currency = True
    end
  end
end
