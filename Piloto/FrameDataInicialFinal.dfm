inherited FrDataInicialFinal: TFrDataInicialFinal
  Width = 194
  Height = 41
  ExplicitWidth = 194
  ExplicitHeight = 41
  object Label1: TLabel
    Left = 2
    Top = 1
    Width = 83
    Height = 13
    Caption = 'Data de cadastro'
  end
  object lb1: TLabel
    Left = 89
    Top = 20
    Width = 18
    Height = 13
    Caption = 'at'#233
  end
  object eDataFinal: TEditLukaData
    Left = 108
    Top = 15
    Width = 86
    Height = 21
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
  end
  object eDataInicial: TEditLukaData
    Left = 1
    Top = 15
    Width = 86
    Height = 21
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 0
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
end
