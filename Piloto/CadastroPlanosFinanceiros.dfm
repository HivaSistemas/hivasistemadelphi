inherited FormCadastroPlanosFinanceiros: TFormCadastroPlanosFinanceiros
  Caption = 'Cadastro de planos financeiros'
  ClientHeight = 271
  ClientWidth = 544
  ExplicitWidth = 550
  ExplicitHeight = 300
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 125
    Top = 88
    ExplicitLeft = 125
    ExplicitTop = 88
  end
  object lbDepartamento: TLabel [1]
    Left = 124
    Top = 130
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Height = 271
    TabOrder = 4
    ExplicitHeight = 271
    inherited sbGravar: TSpeedButton
      Left = 1
      ExplicitLeft = 1
    end
    inherited sbDesfazer: TSpeedButton
      Left = 1
      Top = 97
      ExplicitLeft = 1
      ExplicitTop = 97
    end
    inherited sbExcluir: TSpeedButton
      Left = 1
      Top = 156
      ExplicitLeft = 1
      ExplicitTop = 156
    end
    inherited sbPesquisar: TSpeedButton
      Left = 1
      Top = 47
      ExplicitLeft = 1
      ExplicitTop = 47
    end
    inherited sbLogs: TSpeedButton
      Left = 1
      Top = 218
      ExplicitLeft = 1
      ExplicitTop = 218
    end
  end
  inherited eID: TEditLuka
    Left = 125
    Top = 102
    Alignment = taLeftJustify
    TabOrder = 0
    OnKeyPress = NumerosPonto
    TipoCampo = tcTexto
    ExplicitLeft = 125
    ExplicitTop = 102
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 1000
    Top = 8
    Enabled = False
    TabOrder = 5
    Visible = False
    ExplicitLeft = 1000
    ExplicitTop = 8
  end
  object rgTipo: TRadioGroupLuka
    Left = 125
    Top = 8
    Width = 147
    Height = 36
    BiDiMode = bdLeftToRight
    Caption = 'Tipo'
    Columns = 2
    Enabled = False
    ItemIndex = 0
    Items.Strings = (
      'Despesa'
      'Receita')
    ParentBiDiMode = False
    TabOrder = 2
    Valores.Strings = (
      'D'
      'R')
  end
  object ckExibirDRE: TCheckBoxLuka
    Left = 125
    Top = 64
    Width = 89
    Height = 17
    Caption = 'Exibir no DRE'
    Enabled = False
    TabOrder = 3
    CheckedStr = 'N'
  end
  object eDescricao: TEditLuka
    Left = 125
    Top = 144
    Width = 400
    Height = 22
    CharCase = ecUpperCase
    Enabled = False
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
