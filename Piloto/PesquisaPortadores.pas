unit PesquisaPortadores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Biblioteca, _Portadores,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Sessao, Vcl.ExtCtrls;

type
  TFormPesquisaPortadores = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarPortador: TObject;

implementation

{$R *.dfm}

const
  coDescricao = 2;
  coAtivo     = 3;

{ TFormPesquisaPortadores }

function PesquisarPortador: TObject;
var
  obj: TObject;
begin
  obj := _HerancaPesquisas.Pesquisar(TFormPesquisaPortadores, _Portadores.GetFiltros);
  if obj = nil then
    Result := nil
  else
    Result := RecPortadores(obj);
end;

procedure TFormPesquisaPortadores.BuscarRegistros;
var
  i: Integer;
  vPortadores: TArray<RecPortadores>;
begin
  inherited;

  vPortadores :=
    _Portadores.BuscarPortadores(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if vPortadores = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vPortadores);

  for i := Low(vPortadores) to High(vPortadores) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]      := vPortadores[i].PortadorId;
    sgPesquisa.Cells[coDescricao, i + 1]    := vPortadores[i].Descricao;
    sgPesquisa.Cells[coAtivo, i + 1]        := vPortadores[i].ativo;
  end;
  sgPesquisa.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vPortadores) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaPortadores.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, coAtivo]) then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
