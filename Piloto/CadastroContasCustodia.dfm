inherited FormCadastroContasCustodia: TFormCadastroContasCustodia
  Caption = 'Cadastro de contas custodia'
  ClientHeight = 206
  ClientWidth = 451
  ExplicitWidth = 457
  ExplicitHeight = 235
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Top = 61
    ExplicitTop = 61
  end
  object lb1: TLabel [1]
    Left = 126
    Top = 109
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  inherited pnOpcoes: TPanel
    Height = 206
    ExplicitHeight = 171
  end
  inherited eID: TEditLuka
    Top = 75
    ExplicitTop = 75
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 126
    Top = 14
    ExplicitLeft = 126
    ExplicitTop = 14
  end
  object eNome: TEditLuka
    Left = 126
    Top = 123
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
