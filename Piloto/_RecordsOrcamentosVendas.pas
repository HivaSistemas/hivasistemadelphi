unit _RecordsOrcamentosVendas;

interface

uses
  System.Classes;

type
  RecRetornoRecebimento = record
    EmitirDocumentoFiscal: Boolean;
    NotasFiscaisIds: TArray<Integer>;
    TipoNota: string;
  end;

  RecProdutosVendas = record
    empresa_id: Integer;
    fornecedor_id: Integer;
    produto_id: Integer;
    peso: Double;
    multiplo_venda: Double;
    multiplo_venda_2: Double;
    multiplo_venda_3: Double;
    preco_varejo: Currency;
    QuantidadeMinPrecoVarejo: Double;
    PrecoPromocional: Currency;
    QuantidadeMinPrecoPromocional: Double;
    PrecoPontaEstoque: Currency;
    PrecoAtacado1: Currency;
    QuantidadeMinPrecoAtac1: Double;
    PrecoAtacado2: Currency;
    QuantidadeMinPrecoAtac2: Double;
    PrecoAtacado3: Currency;
    QuantidadeMinPrecoAtac3: Double;
    ValorAdicionalFrete: Double;
    preco_pdv: Currency;
    QuantidadeMinimaPDV: Double;
    estoque: Double;
    disponivel: Double;
    nome_marca: string;
    caracteristicas: string;
    unidade_venda: string;
    nome: string;
    TipoControleEstoque: string;
    CodigoBarras: string;
    CodigoOriginalFabricante: string;
    Fisico: Double;
    produtoDiversosPDV: string;
    Ambiente: string;

    Foto1: TMemoryStream;
    Foto2: TMemoryStream;
    Foto3: TMemoryStream;
    ControlaPeso: string;

    TipoPreco: string;
    PrecoBase: Double;
    PrecoUnitario: Double;
  end;

  RecOrcamentos = record
    orcamento_id: Integer;
    nome_consumidor_final: string;
    cpf_consumidor_final: string;
    TelefoneConsumidorFinal: string;
    empresa_id: Integer;
    cliente_id: Integer;
    condicao_id: Integer;
    vendedor_id: Integer;
    tipo_acompanhamento_id: Integer;
    IndiceCondicaoPagamento: Double;
    valor_total_produtos: Double;
    ValorTotalProdutosPromocao: Double;
    valor_cobranca: Double;
    valor_pix: Double;
    valor_credito: Double;
    valor_total: Double;
    valor_outras_despesas: Double;
    valor_desconto: Double;
    valor_dinheiro: Double;
    valor_cheque: Double;
    ValorCartaoDebito: Double;
    ValorCartaoCredito: Double;
    valorTroco: Double;
    ValorDevAcumuladoAberto: Double;
    AguardandoCliente: string;
    ValorAcumulativo: Double;
    ValorFinanceira: Double;
    status: string;
    status_analitico: string;
    turno_id: Integer;
    AcumuladoId: Integer;
    IndiceDescontoVendaId: Integer;
    NomeIndDescontoVenda: string;
    data_cadastro: TDateTime;
    indice_over_price: Double;
    nome_vendedor: string;
    nome_cliente: string;
    nome_condicao_pagto: string;
    nome_empresa: string;
    usuario_cadastro_id: Integer;
    nome_usuario_cadastro: string;
    data_hora_recebimento: TDateTime;
    usuario_recebimento_id: Integer;
    nome_usuario_recebimento: string;
    telefone_principal: string;
    telefone_celular: string;
    TipoEntrega: string;
    TipoEntregaAnalitico: string;
    OrigemVendaAnalitico: string;
    DataEntrega: TDateTime;
    HoraEntrega: TDateTime;
    ValorFrete: Double;
    ValorFreteItens: Double;
    OrigemVenda: string;
    InfComplCompPagamentos: string;
    ObservacoesCaixa: string;
    ObservacoesTitulo: string;
    ObservacoesExpedicao: string;
    ObservacoesNFe: string;
    tipoAnaliseCusto: string;
    ReceberNaEntrega: string;
    Logradouro: string;
    Complemento: string;
    Numero: string;
    PontoReferencia: string;
    BairroId: Integer;
    NomeBairroCliente: string;
    NomeCidadeCliente: string;
    UfCliente: string;
    LogradouroCliente: string;
    ComplementoCliente: string;
    NumeroCliente: string;
    CepCliente: string;
    CpfCnpjCliente: string;
    InscricaoEstadualCliente: string;
    Cep: string;
    InscricaoEstadual: string;
    QtdeDiasVencimentoAcumulado: Integer;
    IndicePedidos: Double;
    ValorAdiantadoAcumulatoAber: Double;
    PrazoMedioCondicaoPagto: Double;
    Acumulado: string;
    ProfissionalId: Integer;
    SaldoDevedorAcumulado: Currency;
    OrcPorAmbiente: string;
    PedidoLiberado: string;
    PercentualLiberado: Double;
  end;

  RecDefinicoesLoteVenda = record
    EmpresaId: Integer;
    Lote: string;
    LocalId: Integer;
    LocalNome: string;
    QuantidadeRetirarAto: Double;
    QuantidadeRetirar: Double;
    QuantidadeEntregar: Double;
  end;

  RecDefinicoesLocaisVenda = record
    LocalId: Integer;
    EmpresaId: Integer;
    NomeEmpresa: string;
    Nome: string;
    Disponivel: Double;
    QuantidadeAto: Double;
    QuantidadeRetirar: Double;
    QuantidadeEntregar: Double;
    ProdutoId: Integer;
    AceitaEstoqueNegativo: string;
  end;

  RecOrcamentoItens = record
    orcamento_id: Integer;
    produto_id: Integer;
    item_id: Integer;
    valor_total: Double;
    quantidade: Double;

    QuantidadeRetirarAto: Double;
    QuantidadeRetirar: Double;
    QuantidadeEntregar: Double;

    QuantidadeDevolvEntregues: Double;
    QuantidadeDevolvPendentes: Double;
    QuantidadeDevolvSemPrevisao: Double;
    QuantidadeSemPrevisao: Double;

    QuantidadeRetirados: Double;
    QuantidadeEntregues: Double;

    SaldoRestanteAcumulado: Double;

    preco_unitario: Double;
    PrecoBase: Double;
    preco_varejo: Double;
    PrecoPromocional: Double;
    nome: string;
    multiplo_venda: Double;
    multiplo_venda_2: Double;
    multiplo_venda_3: Double;
    nome_marca: string;
    unidade_venda: string;
    UnidadeEntregaId: string;
    CodigoOriginalFabricante: string;
    CodigoBarras: string;

    valor_total_desconto: Currency;
    valor_total_outras_despesas: Currency;
    ValorTotalFrete: Currency;

    Estoque: Double;
    TipoControleEstoque: string;
    TipoPrecoUtilizado: string;
    TipoPrecoUtilizadoAnalitico: string;

    CustoEntrada: Double;
    IndiceCondicaoPagamento: Double;
    PesoTotal: Double;
    ValorImpostos: Double;
    ValorEncargos: Double;
    ValorCustoVenda: Double;
    ValorCustoFinal: Double;
    ValorLiquido: Double;

    PrecoManual: Double;
    PercDescontoPrecoManual: Double;
    ValorDescUnitPrecoManual: Double;

    PrecoVarejoBasePrecoManual: Double;

    Lotes: TArray<RecDefinicoesLoteVenda>;
    Locais: TArray<RecDefinicoesLocaisVenda>;
  end;

  RecDevolucoes = record
    devolucao_id: Integer;
    EmpresaId: Integer;
    NomeEmpresa: string;
    orcamento_id: Integer;
    ClienteCreditoId: Integer;
    usuario_cadastro_id: Integer;
    NomeUsuarioDevolucao: string;
    valor_bruto: Double;
    valor_liquido: Double;
    valor_desconto: Double;
    ValorOutrasDespesas: Double;
    ValorFrete: Double;
    data_hora_devolucao: TDateTime;
    observacoes: string;
    ClienteId: Integer;
    NomeCliente: string;
    NomeClienteCredito: string;
    Status: string;
    UsuarioConfirmacaoId: Integer;
    NomeUsuarioConfirmacao: string;
    DataHoraConfirmacao: TDateTime;
    DevolverFrete: string;
    DevolverOutrasDespesas: string;
    MotivoDevolucao: string;
    CreditoPagarId: Integer;
    TemProdutosEntregues: string;
    SomenteFiscal: string;

    Apelido: string;
    Logradouro: string;
    Complemento: string;
    Numero: string;
    BairroId: Integer;
    NomeBairro: string;
    Cep: string;
    CidadeId: Integer;
    NomeCidade: string;
    EstadoId: string;
    NomeEstado: string;
    TelefonePrincipal: string;
    TelefoneCelular: string;
    CondicaoId: Integer;
    NomeCondicaoPagamento: string;
    QtdeDiasDevolucao: Integer;
  end;

  RecDevolucoesItens = record
    DevolucaoId: Integer;
    ItemId: Integer;
    ProdutoId: Integer;
    NomeProduto: string;
    MarcaId: Integer;
    NomeMarca: string;
    QuantidadeDevolvidosEntregues: Double;
    QuantidadeDevolvidosPendencias: Double;
    QuantidadeDevolvSemPrevisao: Double;
    ValorLiquido: Double;
    ValorBruto: Double;
    ValorDesconto: Double;
    ValorOutrasDespesas: Double;

    ValorFrete: Double;
    ValorST: Double;
    ValorIPI: Double;

    PrecoUnitario: Double;
    UnidadeVenda: string;
    CodigoBarras: string;
    CodigoOriginalFabricante: string;
    UnidadeEntregaId: string;
    MultiploVenda: Double;
    CustoEntrada: Double;
    ValorCustoFinal: Double;
    SomenteFiscal: string;
  end;

  RecOrcamentosBloqueios = record
    OrcamentoId: Integer;
    ItemId: Integer;
    Descricao: string;
    DataHoraLiberacao: TDateTime;
    UsuarioLiberacaoId: Integer;
    NomeUsuarioLiberacao: string;
  end;

  RecFotosProdutosVendas = record
    ProdutoId: Integer;
    Foto1: TMemoryStream;
    Foto2: TMemoryStream;
    Foto3: TMemoryStream;
  end;

  RecComissoesPorFuncionarios = record
    Id: Integer;
    Tipo: string;
    FuncionarioId: Integer;
    NomeFuncionario: string;
    ParceiroId: Integer;
    NomeParceiro: string;
    Data: TDateTime;
    CadastroId: Integer;
    NomeCliente: string;
    ValorVenda: Double;
    BaseComissao: Double;
    TipoComissao: string;
    PercentualComissao: Double;
    ValorComissao: Double;
    ValorCustoFinal: Double;
  end;

  RecComissoesPorProdutos  = record
    Id: Integer;
    Tipo: string;
    Data: TDateTime;
    VendedorId: Integer;
    NomeFuncionario: string;
    CadastroId: Integer;
    NomeCliente: string;
    ProdutoId: Integer;
    NomeProduto: string;
    MarcaId: Integer;
    NomeMarca: string;
    ValorVenda: Double;
    Marca: string;
    BaseComissaoProduto: Double;
    TipoComissao: string;
    PercentualComissaoProduto: Double;
    ValorComissaoProduto: Double;
  end;

  RecOrcamentosImpressao = record
    OrcamentoId: Integer;
    EmpresaId: Integer;
    ClienteId: Integer;
    NomeCliente: string;
    Apelido: string;
    Logradouro: string;
    Complemento: string;
    Numero: string;
    BairroId: Integer;
    NomeBairro: string;
    Cep: string;
    CidadeId: Integer;
    NomeCidade: string;
    EstadoId: string;
    NomeEstado: string;
    TelefonePrincipal: string;
    TelefoneCelular: string;
    CondicaoId: Integer;
    NomeCondicaoPagamento: string;
    VendedorId: Integer;
    NomeVendedor: string;
    ValorTotal: Double;
    ValorTotalProdutos: Double;
    ValorTotalProdutosPromocao: Double;
    ValorOutrasDespesas: Double;
    ValorFrete: Double;
    ValorFreteItens: Double;
    ValorDesconto: Double;
    ValorDinheiro: Double;
    ValorAdiantamento: Double;
    ValorCheque: Double;
    ValorCartao: Double;
    ValorCobranca: Double;
    ValorCredito: Double;
    Status: string;
    ObservacoesCaixa: string;
    EmailCliente: string;
    CpfCnpj: string;
    Indice: Double;
  end;

  RecOrcamentosItensImpressao = record
    ProdutoId: Integer;
    ItemId: Integer;
    NomeProduto: string;
    MarcaId: Integer;
    NomeMarca: string;
    PrecoUnitario: Double;
    Quantidade: Double;
    UnidadeVenda: string;
    ValorTotal: Double;
    TipoPrecoUtilitario: string;
    ValorTotalDesconto: Double;
    ValorDescUnitPrecoManual: Double;
    ValorOriginal: Double;
    UnidadeEntrega: string;
    MultiploVenda: Double;
  end;


implementation

end.
