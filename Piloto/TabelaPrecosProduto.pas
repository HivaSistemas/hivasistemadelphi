unit TabelaPrecosProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, _Sessao, _Biblioteca, _PrecosProdutos,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameCondicoesPagamento, _ProdutosVenda,
  StaticTextLuka, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, _RecordsOrcamentosVendas;

type
  TFormTabelaPrecosProduto = class(TFormHerancaFinalizar)
    st5: TStaticText;
    st1: TStaticText;
    st4: TStaticText;
    st6: TStaticText;
    stPrecoAtacado2: TStaticTextLuka;
    stPrecoVarejo: TStaticTextLuka;
    stPrecoPromocional: TStaticTextLuka;
    stPrecoPontaEstoque: TStaticTextLuka;
    st3: TStaticText;
    stPrecoPDV: TStaticTextLuka;
    st7: TStaticText;
    stPrecoAtacado1: TStaticTextLuka;
    st2: TStaticText;
    stPrecoAtacado3: TStaticTextLuka;
    FrCondicaoPagamento: TFrCondicoesPagamento;
    st9: TStaticText;
    stQtdeMinVarejo: TStaticTextLuka;
    stQtdeMinPDV: TStaticTextLuka;
    stQtdeMinPromocional: TStaticTextLuka;
    stQtdeMinPontaEstoque: TStaticTextLuka;
    stQtdeMinAtacado1: TStaticTextLuka;
    stQtdeMinAtacado2: TStaticTextLuka;
    stQtdeMinAtacado3: TStaticTextLuka;
    procedure FormCreate(Sender: TObject);
  private
    FPrecos: TArray<RecProdutosVendas>;

    procedure montarPrecos;
    procedure FrCondicaoPagtoOnAposPesquisar(Sender: TObject);
  public
    { Public declarations }
  end;

procedure Informar(pProdutoId: Integer; pEmpresaId: Integer; pCondicaoPagamentoId: Integer);

implementation

{$R *.dfm}

procedure Informar(pProdutoId: Integer; pEmpresaId: Integer; pCondicaoPagamentoId: Integer);
var
  vForm: TFormTabelaPrecosProduto;
begin
  if pProdutoId = 0 then
    Exit;

  vForm := TFormTabelaPrecosProduto.Create(nil);

  vForm.FPrecos := _ProdutosVenda.BuscarProdutosVendas(Sessao.getConexaoBanco, 5, [pEmpresaId, pProdutoId], False);
  vForm.FrCondicaoPagamento.InserirDadoPorChave( pCondicaoPagamentoId, False );
  SetarFoco(vForm.FrCondicaoPagamento);

  vForm.montarPrecos;

  vForm.ShowModal;

  vForm.Free;
end;

{ TFormTabelaPrecosProduto }

procedure TFormTabelaPrecosProduto.FormCreate(Sender: TObject);
begin
  inherited;
  FrCondicaoPagamento.OnAposPesquisar := FrCondicaoPagtoOnAposPesquisar;
end;

procedure TFormTabelaPrecosProduto.FrCondicaoPagtoOnAposPesquisar(Sender: TObject);
begin
  montarPrecos;
end;

procedure TFormTabelaPrecosProduto.montarPrecos;
begin
  stPrecoVarejo.Caption        := NFormatN(FPrecos[0].preco_varejo * FrCondicaoPagamento.getDados.indice_acrescimo);
  stPrecoPDV.Caption           := NFormatN(FPrecos[0].preco_pdv * FrCondicaoPagamento.getDados.indice_acrescimo);

  stPrecoPromocional.Caption :=
    NFormatN(
      FPrecos[0].PrecoPromocional * IIfDbl(
        FrCondicaoPagamento.getDados.AplicarIndPrecoPromocional = 'S',
        FrCondicaoPagamento.getDados.getIndiceAcrescimoPrecoPromocional,
        1
      )
    );

  stPrecoPontaEstoque.Caption  := NFormatN(FPrecos[0].PrecoPontaEstoque * FrCondicaoPagamento.getDados.indice_acrescimo);
  stPrecoAtacado1.Caption      := NFormatN(FPrecos[0].PrecoAtacado1 * FrCondicaoPagamento.getDados.indice_acrescimo);
  stPrecoAtacado2.Caption      := NFormatN(FPrecos[0].PrecoAtacado2 * FrCondicaoPagamento.getDados.indice_acrescimo);
  stPrecoAtacado3.Caption      := NFormatN(FPrecos[0].PrecoAtacado3 * FrCondicaoPagamento.getDados.indice_acrescimo);

  stQtdeMinVarejo.Caption := NFormatNEstoque(FPrecos[0].QuantidadeMinPrecoVarejo);
  stQtdeMinPDV.Caption    := NFormatNEstoque(FPrecos[0].QuantidadeMinimaPDV);
  stQtdeMinPromocional.Caption := NFormatNEstoque(FPrecos[0].QuantidadeMinPrecoPromocional);
//  stQtdeMinPontaEstoque.AsDouble := FPrecos[0].
  stQtdeMinAtacado1.Caption := NFormatNEstoque(FPrecos[0].QuantidadeMinPrecoAtac1);
  stQtdeMinAtacado2.Caption := NFormatNEstoque(FPrecos[0].QuantidadeMinPrecoAtac2);
  stQtdeMinAtacado3.Caption := NFormatNEstoque(FPrecos[0].QuantidadeMinPrecoAtac3);
end;

end.
