inherited FormPesquisaCargosProfissionais: TFormPesquisaCargosProfissionais
  Caption = 'Pesquisa de cargos de profissionais'
  ClientHeight = 212
  ClientWidth = 441
  ExplicitWidth = 447
  ExplicitHeight = 241
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 441
    Height = 166
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Ativo')
    RealColCount = 4
    ExplicitLeft = 0
    ExplicitTop = 46
    ExplicitWidth = 441
    ExplicitHeight = 166
    ColWidths = (
      28
      55
      199
      57)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 441
    ExplicitWidth = 441
    inherited eValorPesquisa: TEditLuka
      Width = 235
      ExplicitWidth = 235
    end
  end
end
