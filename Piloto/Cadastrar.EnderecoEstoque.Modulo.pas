unit Cadastrar.EnderecoEstoque.Modulo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca, _RecordsEspeciais, _RecordsCadastros,
  Pesquisa.MotivosAjusteEstoque, _HerancaCadastro, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FramePlanosFinanceiros, CheckBoxLuka, _EnderecoEstoqueRua;

type
  TFormEnderecoEstoqueModulo = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
  private
    procedure PreencherRegistro(pRuaId: RecEnderecoEstoqueModulo);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses Pesquisa.EnderecoEstoqueModulo, _EnderecoEstoqueModulo;

{ TFormEnderecoEstoqueModulo }

procedure TFormEnderecoEstoqueModulo.BuscarRegistro;
var
  vEnderecoEstoqueModulo: TArray<RecEnderecoEstoqueModulo>;
begin
  vEnderecoEstoqueModulo := _EnderecoEstoqueModulo.BuscarEnderecoEstoqueModulo(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vEnderecoEstoqueModulo = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(vEnderecoEstoqueModulo[0]);
end;

procedure TFormEnderecoEstoqueModulo.ExcluirRegistro;
var
  vRetorno: RecRetornoBD;
begin
  vRetorno := _EnderecoEstoqueModulo.ExcluirEnderecoEstoqueModulo(Sessao.getConexaoBanco, eId.AsInt);
  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormEnderecoEstoqueModulo.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;
  vRetorno :=
    _EnderecoEstoqueModulo.AtualizarEnderecoEstoqueModulo(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text
    );

  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetorno.AsInt);
end;

procedure TFormEnderecoEstoqueModulo.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eDescricao, ckAtivo], pEditando);

  if pEditando then begin
    SetarFoco(eDescricao);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormEnderecoEstoqueModulo.PesquisarRegistro;
var
  vEnderecoEstoqueModulo: RecEnderecoEstoqueModulo;
begin
  vEnderecoEstoqueModulo := RecEnderecoEstoqueModulo(Pesquisa.EnderecoEstoqueModulo.Pesquisar());  //AquiS
  if vEnderecoEstoqueModulo = nil then
    Exit;

  inherited;
  PreencherRegistro(vEnderecoEstoqueModulo);
end;

procedure TFormEnderecoEstoqueModulo.PreencherRegistro(pRuaId: RecEnderecoEstoqueModulo);
begin
  eID.AsInt                     := pRuaId.modulo_id;
  eDescricao.Text               := pRuaId.descricao;
  pRuaId.Free;
end;

procedure TFormEnderecoEstoqueModulo.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o n�o foi informado corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;
end;

end.
