inherited FormBuscarDadosDinheiro: TFormBuscarDadosDinheiro
  Caption = 'Buscar dados de dinheiro'
  ClientHeight = 263
  ClientWidth = 389
  OnShow = FormShow
  ExplicitWidth = 395
  ExplicitHeight = 292
  PixelsPerInch = 96
  TextHeight = 14
  object lbllb11: TLabel [0]
    Left = 283
    Top = 3
    Width = 44
    Height = 14
    AutoSize = False
    Caption = 'Valor'
  end
  inherited pnOpcoes: TPanel
    Top = 226
    Width = 389
    TabOrder = 3
    ExplicitTop = 226
    ExplicitWidth = 389
  end
  object eValorDinheiro: TEditLuka
    Left = 283
    Top = 17
    Width = 102
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    Text = '0,00'
    OnKeyDown = eValorDinheiroKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  inline FrConta: TFrContas
    Left = 3
    Top = 0
    Width = 273
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 0
    TabStop = True
    ExplicitLeft = 3
    ExplicitWidth = 273
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 248
      Height = 24
      ExplicitWidth = 248
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 273
      ExplicitWidth = 273
      inherited lbNomePesquisa: TLabel
        Width = 31
        Caption = 'Conta'
        ExplicitWidth = 31
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 168
        ExplicitLeft = 168
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 248
      Height = 25
      ExplicitLeft = 248
      ExplicitHeight = 25
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited rgTipo: TRadioGroupLuka
      Left = 111
      Top = 16
      ExplicitLeft = 111
      ExplicitTop = 16
    end
    inherited ckAutorizados: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object sgContas: TGridLuka
    Left = 3
    Top = 41
    Width = 382
    Height = 152
    ColCount = 3
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goRowSelect]
    TabOrder = 2
    OnDblClick = sgContasDblClick
    OnDrawCell = sgContasDrawCell
    OnKeyDown = sgContasKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Conta'
      'Descri'#231#227'o'
      'Valor')
    Grid3D = False
    RealColCount = 4
    AtivarPopUpSelecao = False
    ColWidths = (
      60
      211
      92)
  end
  object stValorTotal: TStaticText
    Left = 8
    Top = 194
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Total a ser pago'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
  end
  object stValorTotalASerPago: TStaticText
    Left = 8
    Top = 209
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    Transparent = False
  end
  object stValorTotalDefinido: TStaticText
    Left = 132
    Top = 209
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    Transparent = False
  end
  object stTotalDefinido: TStaticText
    Left = 132
    Top = 194
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Total definido'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
  end
  object stDiferenca: TStaticText
    Left = 256
    Top = 209
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 4868863
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    Transparent = False
  end
  object st2: TStaticText
    Left = 256
    Top = 194
    Width = 125
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Diferen'#231'a'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 9
    Transparent = False
  end
end
