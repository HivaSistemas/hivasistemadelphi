unit BuscarDadosEnderecoEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  Frame.EnderecoEstoque, _RecordsCadastros, _Biblioteca;

type
  TFormBuscarDadosEnderecoEstoque = class(TFormHerancaFinalizar)
    frEnderecoEstoque: TFrEnderecoEstoque;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosEnderecoEstoqueProdutos: TRetornoTelaFinalizar<TArray<Integer>>;

implementation

{$R *.dfm}

function BuscarDadosEnderecoEstoqueProdutos: TRetornoTelaFinalizar<TArray<Integer>>;
var
  vForm: TFormBuscarDadosEnderecoEstoque;
begin
  vForm := TFormBuscarDadosEnderecoEstoque.Create(Application);

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.frEnderecoEstoque.GetArrayOfInteger;

//  FreeAndNil(vForm);
end;

end.
