inherited FormListaContagem: TFormListaContagem
  Caption = 'Lista de contagem'
  ClientHeight = 604
  ClientWidth = 934
  ExplicitWidth = 940
  ExplicitHeight = 633
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 604
    ExplicitHeight = 604
  end
  inherited pcDados: TPageControl
    Width = 812
    Height = 604
    ActivePage = tsResultado
    ExplicitWidth = 812
    ExplicitHeight = 604
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 804
      ExplicitHeight = 575
      object lbTipoControleEstoque: TLabel
        Left = 381
        Top = 121
        Width = 154
        Height = 14
        Caption = 'Tipo de controle de estoque'
      end
      object lbAtivo: TLabel
        Left = 620
        Top = 85
        Width = 26
        Height = 14
        Caption = 'Ativo'
      end
      object lb3: TLabel
        Left = 381
        Top = 82
        Width = 112
        Height = 14
        Caption = 'Situa'#231#227'o do estoque'
      end
      object Label1: TLabel
        Left = 381
        Top = 163
        Width = 129
        Height = 14
        Caption = 'Quantidade de estoque'
      end
      object Label2: TLabel
        Left = 620
        Top = 5
        Width = 78
        Height = 14
        Caption = 'Agrupar locais'
      end
      object Label3: TLabel
        Left = 620
        Top = 46
        Width = 73
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Agrupamento'
        Visible = False
      end
      inline FrProdutos: TFrProdutos
        Left = 2
        Top = 266
        Width = 323
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 266
        ExplicitWidth = 323
        inherited sgPesquisa: TGridLuka
          Width = 298
          ExplicitWidth = 298
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 323
          ExplicitWidth = 323
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 218
            ExplicitLeft = 218
          end
        end
        inherited pnPesquisa: TPanel
          Left = 298
          ExplicitLeft = 298
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas
        Left = 2
        Top = 467
        Width = 319
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 467
        ExplicitWidth = 319
        ExplicitHeight = 81
        inherited sgPesquisa: TGridLuka
          Width = 294
          Height = 64
          ExplicitWidth = 294
          ExplicitHeight = 64
        end
        inherited PnTitulos: TPanel
          Width = 319
          ExplicitWidth = 319
          inherited lbNomePesquisa: TLabel
            Width = 98
            Caption = 'Grupo de estoque'
            ExplicitWidth = 98
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 214
            ExplicitLeft = 214
          end
        end
        inherited pnPesquisa: TPanel
          Left = 294
          Height = 65
          ExplicitLeft = 294
        end
      end
      inline FrMarcas: TFrMarcas
        Left = 2
        Top = 367
        Width = 321
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 367
        ExplicitWidth = 321
        inherited sgPesquisa: TGridLuka
          Width = 296
          ExplicitWidth = 296
        end
        inherited PnTitulos: TPanel
          Width = 321
          ExplicitWidth = 321
          inherited lbNomePesquisa: TLabel
            Width = 39
            ExplicitWidth = 39
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 216
            ExplicitLeft = 216
          end
        end
        inherited pnPesquisa: TPanel
          Left = 296
          ExplicitLeft = 296
        end
      end
      inline FrDataCadastroProduto: TFrDataInicialFinal
        Left = 379
        Top = 3
        Width = 196
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 379
        ExplicitTop = 3
        ExplicitWidth = 196
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 156
          Height = 14
          Caption = 'Data de cadastro do produto'
          ExplicitWidth = 156
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object cbTipoControleEstoque: TComboBoxLuka
        Left = 381
        Top = 135
        Width = 157
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 5
        TabOrder = 8
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'Normal'
          'Lote'
          'Piso'
          'Grade'
          'Kit'
          'N'#227'o filtrar')
        Valores.Strings = (
          'N'
          'L'
          'P'
          'G'
          'K'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbAtivo: TComboBoxLuka
        Left = 620
        Top = 99
        Width = 105
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 2
        TabOrder = 6
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'Sim'
          'N'#227'o'
          'N'#227'o filtrar')
        Valores.Strings = (
          'S'
          'N'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      inline FrLocais: TFrLocais
        Left = 2
        Top = 169
        Width = 319
        Height = 79
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 169
        ExplicitWidth = 319
        ExplicitHeight = 79
        inherited sgPesquisa: TGridLuka
          Width = 294
          Height = 62
          ExplicitWidth = 294
          ExplicitHeight = 62
        end
        inherited CkAspas: TCheckBox
          Top = 60
          ExplicitTop = 60
        end
        inherited CkFiltroDuplo: TCheckBox
          Top = 23
          ExplicitTop = 23
        end
        inherited CkPesquisaNumerica: TCheckBox
          Top = 40
          ExplicitTop = 40
        end
        inherited PnTitulos: TPanel
          Width = 319
          ExplicitWidth = 319
          inherited lbNomePesquisa: TLabel
            Width = 94
            Caption = 'Local do produto:'
            ExplicitWidth = 94
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 214
            ExplicitLeft = 214
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 294
          Height = 63
          ExplicitLeft = 294
          ExplicitHeight = 63
        end
      end
      inline FrEmpresas: TFrEmpresas
        Left = 2
        Top = 7
        Width = 319
        Height = 132
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 7
        ExplicitWidth = 319
        ExplicitHeight = 132
        inherited sgPesquisa: TGridLuka
          Width = 294
          Height = 115
          ExplicitWidth = 294
          ExplicitHeight = 115
        end
        inherited PnTitulos: TPanel
          Width = 319
          ExplicitWidth = 319
          inherited lbNomePesquisa: TLabel
            Width = 50
            Caption = 'Empresa:'
            ExplicitWidth = 50
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 214
            ExplicitLeft = 214
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 294
          Height = 116
          ExplicitLeft = 294
          ExplicitHeight = 116
        end
      end
      object cbSituacaoDoEstoque: TComboBoxLuka
        Left = 381
        Top = 95
        Width = 156
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 3
        TabOrder = 9
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'Positivo'
          'Negativo'
          'Zerado'
          'N'#227'o filtrar')
        Valores.Strings = (
          'POS'
          'NEG'
          'ZERO'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object pnEnderecos: TPanel
        Left = 372
        Top = 220
        Width = 341
        Height = 332
        BevelOuter = bvNone
        TabOrder = 12
        inline FrEnderecoEstoque: TFrEnderecoEstoque
          Left = 8
          Top = 7
          Width = 320
          Height = 59
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          TabStop = True
          ExplicitLeft = 8
          ExplicitTop = 7
          ExplicitWidth = 320
          ExplicitHeight = 59
          inherited sgPesquisa: TGridLuka
            Width = 295
            Height = 42
            ExplicitWidth = 295
            ExplicitHeight = 42
          end
          inherited PnTitulos: TPanel
            Width = 320
            ExplicitWidth = 320
            inherited lbNomePesquisa: TLabel
              Width = 121
              ExplicitWidth = 121
              ExplicitHeight = 14
            end
            inherited CkChaveUnica: TCheckBox
              Checked = False
              State = cbUnchecked
            end
            inherited pnSuprimir: TPanel
              Left = 215
              ExplicitLeft = 215
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
          inherited pnPesquisa: TPanel
            Left = 295
            Height = 43
            ExplicitLeft = 295
            ExplicitHeight = 43
          end
        end
        inline FrEnderecoEstoqueRua: TFrEnderecoEstoqueRua
          Left = 8
          Top = 65
          Width = 320
          Height = 59
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          ParentBackground = False
          ParentColor = False
          TabOrder = 1
          TabStop = True
          ExplicitLeft = 8
          ExplicitTop = 65
          ExplicitWidth = 320
          ExplicitHeight = 59
          inherited sgPesquisa: TGridLuka
            Width = 295
            Height = 42
            ExplicitWidth = 295
            ExplicitHeight = 42
          end
          inherited PnTitulos: TPanel
            Width = 320
            ExplicitWidth = 320
            inherited lbNomePesquisa: TLabel
              Width = 21
              ExplicitWidth = 21
              ExplicitHeight = 14
            end
            inherited CkChaveUnica: TCheckBox
              Checked = False
              State = cbUnchecked
            end
            inherited pnSuprimir: TPanel
              Left = 215
              ExplicitLeft = 215
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
          inherited pnPesquisa: TPanel
            Left = 295
            Height = 43
            ExplicitLeft = 295
            ExplicitHeight = 43
          end
        end
        inline FrEnderecoEstoqueModulo: TFrEnderecoEstoqueModulo
          Left = 8
          Top = 130
          Width = 320
          Height = 59
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          ParentBackground = False
          ParentColor = False
          TabOrder = 2
          TabStop = True
          ExplicitLeft = 8
          ExplicitTop = 130
          ExplicitWidth = 320
          ExplicitHeight = 59
          inherited sgPesquisa: TGridLuka
            Width = 295
            Height = 42
            ExplicitWidth = 295
            ExplicitHeight = 42
          end
          inherited CkFiltroDuplo: TCheckBox
            Left = 3
            ExplicitLeft = 3
          end
          inherited PnTitulos: TPanel
            Width = 320
            ExplicitWidth = 320
            inherited lbNomePesquisa: TLabel
              Width = 42
              ExplicitWidth = 42
              ExplicitHeight = 14
            end
            inherited CkChaveUnica: TCheckBox
              Checked = False
              State = cbUnchecked
            end
            inherited pnSuprimir: TPanel
              Left = 215
              ExplicitLeft = 215
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
          inherited pnPesquisa: TPanel
            Left = 295
            Height = 43
            ExplicitLeft = 295
            ExplicitHeight = 43
          end
        end
        inline FrEnderecoEstoqueNivel: TFrEnderecoEstoqueNivel
          Left = 8
          Top = 190
          Width = 320
          Height = 79
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          ParentBackground = False
          ParentColor = False
          TabOrder = 3
          TabStop = True
          ExplicitLeft = 8
          ExplicitTop = 190
          ExplicitWidth = 320
          ExplicitHeight = 79
          inherited sgPesquisa: TGridLuka
            Width = 295
            Height = 62
            ExplicitWidth = 295
            ExplicitHeight = 62
          end
          inherited PnTitulos: TPanel
            Width = 320
            ExplicitWidth = 320
            inherited lbNomePesquisa: TLabel
              Width = 27
              ExplicitWidth = 27
              ExplicitHeight = 14
            end
            inherited CkChaveUnica: TCheckBox
              Checked = False
              State = cbUnchecked
            end
            inherited pnSuprimir: TPanel
              Left = 215
              ExplicitLeft = 215
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
          inherited pnPesquisa: TPanel
            Left = 295
            Height = 63
            ExplicitLeft = 295
            ExplicitHeight = 63
          end
        end
        inline FrEnderecoEstoqueVao: TFrEnderecoEstoqueVao
          Left = 8
          Top = 269
          Width = 320
          Height = 59
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Color = clWhite
          ParentBackground = False
          ParentColor = False
          TabOrder = 4
          TabStop = True
          ExplicitLeft = 8
          ExplicitTop = 269
          ExplicitWidth = 320
          ExplicitHeight = 59
          inherited sgPesquisa: TGridLuka
            Width = 295
            Height = 42
            ExplicitWidth = 295
            ExplicitHeight = 42
          end
          inherited PnTitulos: TPanel
            Width = 320
            ExplicitWidth = 320
            inherited lbNomePesquisa: TLabel
              Width = 20
              ExplicitWidth = 20
              ExplicitHeight = 14
            end
            inherited CkChaveUnica: TCheckBox
              Checked = False
              State = cbUnchecked
            end
            inherited pnSuprimir: TPanel
              Left = 215
              ExplicitLeft = 215
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
          inherited pnPesquisa: TPanel
            Left = 295
            Height = 43
            ExplicitLeft = 295
            ExplicitHeight = 43
          end
        end
      end
      object cbQuantidadeEstoque: TComboBoxLuka
        Left = 381
        Top = 177
        Width = 140
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 3
        TabOrder = 11
        Text = 'N'#227'o filtrar'
        OnChange = cbQuantidadeEstoqueChange
        Items.Strings = (
          'Entre'
          'Maior que'
          'Menor que'
          'N'#227'o filtrar')
        Valores.Strings = (
          'EN'
          'MA'
          'ME'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object pnQuantidadeEstoque: TPanel
        Left = 529
        Top = 160
        Width = 185
        Height = 41
        BevelOuter = bvNone
        TabOrder = 13
        object lb1: TLabel
          Left = 0
          Top = 3
          Width = 60
          Height = 14
          Caption = 'Valor entre'
        end
        object lb2: TLabel
          Left = 79
          Top = 21
          Width = 7
          Height = 14
          Caption = 'e'
        end
        object eValor1: TEdit
          Left = 0
          Top = 17
          Width = 76
          Height = 22
          TabOrder = 0
          Text = '0'
          OnKeyPress = eValor1KeyPress
        end
        object eValor2: TEdit
          Left = 92
          Top = 17
          Width = 76
          Height = 22
          TabOrder = 1
          Text = '0'
          OnKeyPress = eValor2KeyPress
        end
      end
      object cbAgruparLocais: TComboBoxLuka
        Left = 620
        Top = 19
        Width = 105
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 7
        Text = 'N'#227'o'
        Items.Strings = (
          'N'#227'o'
          'Sim')
        Valores.Strings = (
          'N'
          'S')
        AsInt = 0
        AsString = 'N'
      end
      inline FrDataAjuste: TFrDataInicialFinal
        Left = 380
        Top = 43
        Width = 197
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 10
        TabStop = True
        ExplicitLeft = 380
        ExplicitTop = 43
        ExplicitWidth = 197
        inherited Label1: TLabel
          Left = 1
          Width = 200
          Height = 14
          Caption = 'Produtos que n'#227'o tiveram corre'#231#245'es::'
          ExplicitLeft = 1
          ExplicitWidth = 200
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Left = 88
          Width = 18
          Height = 14
          ExplicitLeft = 88
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Left = 107
          Height = 22
          ExplicitLeft = 107
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Width = 85
          Height = 22
          ExplicitWidth = 85
          ExplicitHeight = 22
        end
      end
      object cbAgrupamento: TComboBoxLuka
        Left = 620
        Top = 60
        Width = 105
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 14
        Text = 'Nenhum'
        Items.Strings = (
          'Nenhum'
          'Marca')
        Valores.Strings = (
          'NEN'
          'MAR')
        AsInt = 0
        AsString = 'NEN'
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 804
      ExplicitHeight = 575
      object sgItens: TGridLuka
        Left = 0
        Top = 0
        Width = 804
        Height = 575
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 6
        Ctl3D = True
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        GradientEndColor = 15395562
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goThumbTracking]
        ParentCtl3D = False
        TabOrder = 0
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Und.'
          'Marca'
          'Lote'
          'Local')
        OnGetCellColor = sgItensGetCellColor
        Grid3D = False
        RealColCount = 6
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          52
          320
          38
          127
          64
          64)
      end
    end
  end
  object frxReport: TfrxReport
    Version = '5.2.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 44362.890600868100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 360
    Top = 184
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstPagar
        DataSetName = 'frxdstPagar'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 90.708720000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Top = 70.811070000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133890000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000000000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000000000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133890000000000000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133890000000000000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 1.779530000000000000
          Top = 72.811070000000000000
          Width = 30.236240000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Prod.')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 31.795300000000000000
          Top = 72.811070000000000000
          Width = 336.378170000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 651.961040000000000000
          Top = 72.811070000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Qtde. contada')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 602.827150000000000000
          Top = 72.811070000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde f'#237'sico')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo32: TfrxMemoView
          Left = 513.016080000000000000
          Top = 72.811070000000000000
          Width = 26.456710000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Und.')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 368.393940000000000000
          Top = 72.811070000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'C'#243'd. barras')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 436.425480000000000000
          Top = 72.811070000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 542.354670000000000000
          Top = 72.811070000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 16.832405710000000000
        Top = 170.078850000000000000
        Width = 718.110700000000000000
        DataSet = dstPagar
        DataSetName = 'frxdstPagar'
        RowCount = 0
        object frxdstTranscodigoProd: TfrxMemoView
          Left = 1.779530000000000000
          Top = 1.000000000000000000
          Width = 30.236213150000000000
          Height = 15.118120000000000000
          DataField = 'Produto'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Produto"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo25: TfrxMemoView
          Left = 31.795300000000000000
          Top = 1.000000000000000000
          Width = 336.378045510000000000
          Height = 15.118120000000000000
          DataField = 'Nome'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Nome"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 368.393940000000000000
          Top = 1.000000000000000000
          Width = 64.251961180000000000
          Height = 15.118120000000000000
          DataField = 'CodigoBarras'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."CodigoBarras"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 543.693260000000000000
          Top = 1.000000000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataField = 'Lote'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Lote"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 436.425480000000000000
          Top = 1.000000000000000000
          Width = 71.811023620000000000
          Height = 15.118120000000000000
          DataField = 'Marca'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Marca"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 514.016080000000000000
          Top = 1.000000000000000000
          Width = 26.456692910000000000
          Height = 15.118120000000000000
          DataField = 'Unidade'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Unidade"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 603.504369060000000000
          Top = 1.000000000000000000
          Width = 52.913380940000000000
          Height = 15.118120000000000000
          DataField = 'QuantidadeFisico'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstPagar."QuantidadeFisico"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 658.961040000000000000
          Top = 2.220470000000000000
          Width = 56.692898740000000000
          Height = 13.228348900000000000
          DataField = 'QuantidadeContada'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."QuantidadeContada"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 249.448980000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
    end
  end
  object dstPagar: TfrxDBDataset
    UserName = 'frxdstPagar'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Produto=Produto'
      'Nome=Nome'
      'CodigoBarras=CodigoBarras'
      'Marca=Marca'
      'Unidade=Unidade'
      'CodigoOriginal=CodigoOriginal'
      'Lote=Lote'
      'QuantidadeFisico=QuantidadeFisico'
      'Agrupador=Agrupador'
      'QuantidadeContada=QuantidadeContada')
    DataSet = cdsPagar
    BCDToCurrency = False
    Left = 360
    Top = 248
  end
  object cdsPagar: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Produto'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Nome'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'CodigoBarras'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Marca'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Unidade'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CodigoOriginal'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Lote'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'QuantidadeFisico'
        DataType = ftFloat
      end
      item
        Name = 'QuantidadeContada'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Agrupador'
        DataType = ftString
        Size = 200
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 360
    Top = 312
    object cdsProduto: TStringField
      FieldName = 'Produto'
    end
    object cdsNome: TStringField
      FieldName = 'Nome'
      Size = 200
    end
    object cdsCodigoBarras: TStringField
      FieldName = 'CodigoBarras'
      Size = 200
    end
    object cdsMarca: TStringField
      FieldName = 'Marca'
      Size = 200
    end
    object cdsUnidade: TStringField
      FieldName = 'Unidade'
    end
    object cdsCodigoOriginal: TStringField
      FieldName = 'CodigoOriginal'
      Size = 200
    end
    object cdsLote: TStringField
      FieldName = 'Lote'
      Size = 200
    end
    object cdsQuantidadeFisico: TFloatField
      FieldName = 'QuantidadeFisico'
    end
    object cdsAgrupador: TStringField
      FieldName = 'Agrupador'
      Size = 200
    end
    object cdsQuantidadeContada: TStringField
      FieldName = 'QuantidadeContada'
    end
  end
end
