unit _RecordsExpedicao;

interface

type
  RecRetirada = record
    RetiradaId: Integer;
    LocalId: Integer;
    NomeLocal: string;
    OrcamentoId: Integer;
    EmpresaId: Integer;
    ClienteId: Integer;
    NomeCliente: string;
    Apelido: string;
    VendedorId: Integer;
    NomeVendedor: string;
    DataHoraCadastro: TDateTime;
    TipoMovimento: string;
    TipoMovimentoAnalitico: string;
    UsuarioCadastroId: Integer;
    NomeUsuarioCadastro: string;
    Confirmada: string;
    DataHoraRetirada: TDateTime;
    NomePessoaRetirada: string;
    CpfPessoaRetirada: string;
    Observacoes: string;
    UsuarioConfirmacaoId: Integer;
    NomeUsuarioConfirmacao: string;
    NomeEmpresa: string;
    TelefonePrincipal: string;
    TelefoneCelular: string;
    NotaFiscalId: Integer;
    CondicaoId: Integer;
    NomeCondicaoPagamento: string;
  end;

  RecRetiradaItem = record
    RetiradaId: Integer;
    ProdutoId: Integer;
    ItemId: Integer;
    Quantidade: Double;
    NomeProduto: string;
    NomeMarca: string;
    Unidade: string;
    UnidadeEntregaId: string;
    Lote: string;
    PesoTotal: Double;
    ConfSomenteCodigoBarras: string;
    CodigoOriginalFabricante: string;
    CodigoBarras: string;
    TipoControleEstoque: string;
    MultiploVenda: Double;
    Devolvidos: Double;
    DataHoraCadastro: TDateTime;

    (* Utilizada na confirmação de devolução *)
    QuantidadeDevolver: Double;
  end;

  RecEntrega = record
    EntregaId: Integer;
    OrcamentoId: Integer;
    UsuarioCadastroId: Integer;
    NomeUsuarioCadastro: string;
    DataHoraCadastro: TDateTime;
    PrevisaoEntrega: TDateTime;
    Status: string;
    StatusAnalitico: string;
    UsuarioConfirmacaoId: Integer;
    DataHoraRealizouEntrega: TDateTime;
    NomePessoaRecebeuEntrega: string;
    CpfPessoaRecebeuEntrega: string;
    Observacoes: string;
    ClienteId: Integer;
    NomeCliente: string;
    Apelido: string;
    VendedorId: Integer;
    NomeVendedor: string;
    TelefonePrincipal: string;
    TelefoneCelular: string;
    NomeUsuarioConfirmacao: string;
    EmpresaId: Integer;
    NomeEmpresa: string;
    EmpresaGeracaoId: Integer;
    NomeEmpresaGeracao: string;

    Logradouro: string;
    Complemento: string;
    Numero: string;
    PontoReferencia: string;
    BairroId: Integer;
    NomeBairro: string;
    Cidade: string;
    UF: string;
    LocalId: Integer;
    NomeLocal: string;
    RotaId: Integer;
    NomeRota: string;
    ValorEntrega: Double;
    QtdeProdutos: Integer;
    PesoTotal: Double;
    ValorReceberEntrega: Double;
    CondicaoId: Integer;
    ControleEntregaId: Integer;
    UsuarioInicioSeparaoId: Integer;
    NomeUsuarioInicioSep: string;
    DataHoraInicioSeparacao: TDateTime;
    UsuarioFinalizouSepId: Integer;
    NomeUsuarioFinSep: string;
    DataHoraFinalizouSep: TDateTime;
    NomeCondicaoPagamento: string;
    ReceberNaEntrega: string;
    ObservacoesExpedicao: string;
  end;

  RecEntregaItem = record
    EntregaId: Integer;
    ProdutoId: Integer;
    ItemId: Integer;
    Quantidade: Double;
    QtdeEntregar: Double;
    QtdeRetornar: Double;
    NomeProduto: string;
    NomeMarca: string;
    Unidade: string;
    UnidadeEntregaId: string;
    Lote: string;
    PesoTotal: Double;
    CodigoOriginalFabricante: string;
    ConfSomenteCodigoBarras: string;
    CodigoBarras: string;
    TipoControleEstoque: string;
    MultiploVenda: Double;
    Devolvidos: Double;
    Retornados: Double;
    NaoSeparados: Double;
    DataHoraCadastro: TDateTime;

    (* Utilizada na confirmação de devolução *)
    QuantidadeDevolver: Double;
  end;

  RecItensPendentesEntrega = record
    OrcamentoId: Integer;
    EmpresaId: Integer;
    NomeEmpresa: string;
    CadastroId: Integer;
    NomeCliente: string;
    NomeProduto: string;
    Saldo: Double;
    AguardarContatoCliente: string;
    PrevisaoEntrega: TDateTime;
    TipoMovimento: string;
  end;

  RecItemConferencia = record
    ProdutoId: Integer;
    ItemId: Integer;
    NomeProduto: string;
    Marca: string;
    Quantidade: Double;
    Unidade: string;
    Lote: string;
    CodigoBarras: string;
    TipoControleEstoque: string;
    MultiploVenda: Double;
    ConferirSomenteCodigoBarras: string;
    NaoSeparados: Double;
  end;


implementation

end.
