inherited FormEntradaNotasFiscaisServicos: TFormEntradaNotasFiscaisServicos
  Caption = 'Entrada de notas fiscais de servi'#231'os'
  ClientHeight = 344
  ClientWidth = 950
  ExplicitWidth = 956
  ExplicitHeight = 373
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [1]
    Left = 762
    Top = 5
    Width = 47
    Height = 14
    Caption = 'N'#186' da NF'
  end
  object lb2: TLabel [2]
    Left = 893
    Top = 5
    Width = 28
    Height = 14
    Caption = 'S'#233'rie'
  end
  object lb3: TLabel [3]
    Left = 828
    Top = 5
    Width = 42
    Height = 14
    Caption = 'Modelo'
  end
  object lb6: TLabel [4]
    Left = 126
    Top = 44
    Width = 76
    Height = 14
    Caption = 'Data emiss'#227'o'
  end
  object lb17: TLabel [5]
    Left = 591
    Top = 45
    Width = 102
    Height = 14
    Caption = 'Valor total da nota'
  end
  object lb4: TLabel [6]
    Left = 213
    Top = 44
    Width = 69
    Height = 14
    Caption = 'Data cont'#225'b.'
  end
  inherited pnOpcoes: TPanel
    Height = 344
    TabOrder = 13
    ExplicitHeight = 344
  end
  inherited eID: TEditLuka
    Width = 64
    TabOrder = 0
    ExplicitWidth = 64
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 883
    Top = -24
    TabOrder = 14
    Visible = False
    ExplicitLeft = 883
    ExplicitTop = -24
  end
  inline FrFornecedor: TFrFornecedores
    Left = 458
    Top = 3
    Width = 300
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 458
    ExplicitTop = 3
    ExplicitWidth = 300
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 275
      Height = 23
      ExplicitWidth = 275
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 300
      ExplicitWidth = 300
      inherited lbNomePesquisa: TLabel
        Width = 61
        Caption = 'Fornecedor'
        ExplicitWidth = 61
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 230
        ExplicitLeft = 230
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 275
      Height = 24
      ExplicitLeft = 275
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited ckRevenda: TCheckBox
      Checked = False
      State = cbUnchecked
    end
    inherited ckUsoConsumo: TCheckBox
      Checked = False
      State = cbUnchecked
    end
  end
  object eNumeroNotaFiscal: TEditLuka
    Left = 762
    Top = 19
    Width = 62
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    NumbersOnly = True
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object cbModeloNota: TComboBoxLuka
    Left = 828
    Top = 19
    Width = 61
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    ParentShowHint = False
    ShowHint = False
    TabOrder = 4
    OnKeyDown = ProximoCampo
    Items.Strings = (
      '6'
      '21'
      '99')
    Valores.Strings = (
      '6'
      '21'
      '99')
    AsInt = 0
  end
  object cbSerieNota: TComboBoxLuka
    Left = 893
    Top = 19
    Width = 53
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    ItemIndex = 0
    TabOrder = 5
    Text = '0'
    OnKeyDown = cbSerieNotaKeyDown
    Items.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9')
    Valores.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9')
    AsInt = 0
    AsString = '0'
  end
  inline FrEmpresa: TFrEmpresas
    Left = 194
    Top = 3
    Width = 260
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 194
    ExplicitTop = 3
    ExplicitWidth = 260
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 235
      Height = 23
      ExplicitWidth = 235
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 260
      ExplicitWidth = 260
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 190
        ExplicitLeft = 190
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 235
      Height = 24
      ExplicitLeft = 235
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eDataEmissaoNota: TEditLukaData
    Left = 126
    Top = 59
    Width = 83
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  inline FrCFOPCapa: TFrCFOPs
    Left = 300
    Top = 42
    Width = 289
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 8
    TabStop = True
    ExplicitLeft = 300
    ExplicitTop = 42
    ExplicitWidth = 289
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 264
      Height = 24
      ExplicitWidth = 264
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 289
      ExplicitWidth = 289
      inherited lbNomePesquisa: TLabel
        Width = 26
        Caption = 'CFOP'
        ExplicitWidth = 26
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 219
        ExplicitLeft = 219
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited cbTipoCFOPs: TComboBoxLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited pnPesquisa: TPanel
      Left = 264
      Height = 25
      ExplicitLeft = 264
      ExplicitHeight = 25
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited ckEntradaServicos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eValorTotalNota: TEditLuka
    Left = 593
    Top = 59
    Width = 112
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    Text = '0,00'
    OnChange = eValorTotalNotaChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  inline FrDadosCobranca: TFrDadosCobranca
    Left = 125
    Top = 87
    Width = 578
    Height = 257
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 87
    ExplicitHeight = 257
    inherited pnInformacoesPrincipais: TPanel
      inherited lbllb10: TLabel
        Width = 39
        Height = 14
        ExplicitWidth = 39
        ExplicitHeight = 14
      end
      inherited lbllb9: TLabel
        Width = 29
        Height = 14
        ExplicitWidth = 29
        ExplicitHeight = 14
      end
      inherited lbllb6: TLabel
        Width = 28
        Height = 14
        ExplicitWidth = 28
        ExplicitHeight = 14
      end
      inherited lbllb2: TLabel
        Width = 64
        Height = 14
        ExplicitWidth = 64
        ExplicitHeight = 14
      end
      inherited lbl2: TLabel
        Width = 62
        Height = 14
        ExplicitWidth = 62
        ExplicitHeight = 14
      end
      inherited eRepetir: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited ePrazo: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eDataVencimento: TEditLukaData
        Height = 22
        ExplicitHeight = 22
      end
      inherited FrTiposCobranca: TFrTiposCobranca
        inherited PnTitulos: TPanel
          inherited lbNomePesquisa: TLabel
            Height = 15
          end
        end
      end
      inherited eDocumento: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
    inherited pnInformacoesBoleto: TPanel
      inherited lbllb31: TLabel
        Width = 79
        Height = 14
        ExplicitWidth = 79
        ExplicitHeight = 14
      end
      inherited lbl1: TLabel
        Width = 92
        Height = 14
        ExplicitWidth = 92
        ExplicitHeight = 14
      end
      inherited eNossoNumero: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eCodigoBarras: TMaskEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
    inherited pn1: TPanel
      Height = 144
      ExplicitHeight = 144
      inherited sgCobrancas: TGridLuka
        Top = 2
        Height = 139
        ExplicitTop = 2
        ExplicitHeight = 139
      end
      inherited stDiferenca: TStaticText
        Font.Color = clRed
      end
      inherited StaticTextLuka4: TStaticTextLuka
        Color = 38619
        Font.Color = clBlack
      end
    end
    inherited pnInformacoesCheque: TPanel
      inherited lbllb1: TLabel
        Width = 33
        Height = 14
        ExplicitWidth = 33
        ExplicitHeight = 14
      end
      inherited lbllb3: TLabel
        Width = 43
        Height = 14
        ExplicitWidth = 43
        ExplicitHeight = 14
      end
      inherited lbllb4: TLabel
        Width = 79
        Height = 14
        ExplicitWidth = 79
        ExplicitHeight = 14
      end
      inherited lbllb5: TLabel
        Width = 71
        Height = 14
        ExplicitWidth = 71
        ExplicitHeight = 14
      end
      inherited lbl3: TLabel
        Width = 85
        Height = 14
        ExplicitWidth = 85
        ExplicitHeight = 14
      end
      inherited lbl4: TLabel
        Width = 48
        Height = 14
        ExplicitWidth = 48
        ExplicitHeight = 14
      end
      inherited lbCPF_CNPJ: TLabel
        Width = 52
        Height = 14
        ExplicitWidth = 52
        ExplicitHeight = 14
      end
      inherited eBanco: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eAgencia: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eContaCorrente: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eNumeroCheque: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eNomeEmitente: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eTelefoneEmitente: TEditTelefoneLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eCPF_CNPJ: TEditCPF_CNPJ_Luka
        Height = 22
        ExplicitHeight = 22
      end
    end
  end
  inline FrPlanoFinanceiro: TFrPlanosFinanceiros
    Left = 709
    Top = 42
    Width = 239
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 10
    TabStop = True
    ExplicitLeft = 709
    ExplicitTop = 42
    ExplicitWidth = 239
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 214
      Height = 23
      ExplicitWidth = 214
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 239
      ExplicitWidth = 239
      inherited lbNomePesquisa: TLabel
        Width = 88
        Caption = 'Plano financeiro'
        ExplicitWidth = 88
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 169
        ExplicitLeft = 169
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 214
      Height = 24
      ExplicitLeft = 214
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eDataContabil: TEditLukaData
    Left = 213
    Top = 59
    Width = 83
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  inline FrCentroCusto: TFrCentroCustos
    Left = 709
    Top = 86
    Width = 241
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 11
    TabStop = True
    ExplicitLeft = 709
    ExplicitTop = 86
    ExplicitWidth = 241
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 216
      Height = 24
      ExplicitWidth = 216
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 241
      ExplicitWidth = 241
      inherited lbNomePesquisa: TLabel
        Width = 90
        ExplicitWidth = 90
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 171
        ExplicitLeft = 171
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 216
      Height = 25
      ExplicitLeft = 216
      ExplicitHeight = 25
    end
  end
end
