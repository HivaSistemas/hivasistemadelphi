inherited FormRecebimentoPedidos: TFormRecebimentoPedidos
  Caption = 'Recebimento de caixa'
  ClientHeight = 566
  ClientWidth = 994
  ExplicitTop = -46
  ExplicitWidth = 1000
  ExplicitHeight = 595
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 566
    ExplicitHeight = 566
    inherited sbImprimir: TSpeedButton
      Top = 352
      Height = 44
      Hint = 'Emitir documentos fiscais'
      ExplicitTop = 352
      ExplicitHeight = 44
    end
    object miCancelarFechamentoPedido: TSpeedButton [2]
      Left = 0
      Top = 160
      Width = 117
      Height = 35
      Caption = 'Voltar p/ or'#231'.'
      Flat = True
      OnClick = miCancelarFechamentoPedidoClick
    end
    object miLegendasPedidos: TSpeedButton [3]
      Left = 0
      Top = 522
      Width = 117
      Height = 35
      Caption = 'Outras informa'#231#245'es'
      Flat = True
      OnClick = miLegendasPedidosClick
    end
    object miGerarDocumentoRecebimentoNaEntrega: TSpeedButton [4]
      Left = 0
      Top = 208
      Width = 117
      Height = 35
      Caption = 'Liberar rec. entrega'
      Flat = True
      OnClick = miGerarDocumentoRecebimentoNaEntregaClick
    end
    object sbCpfNota: TSpeedButton [5]
      Left = 1
      Top = 256
      Width = 117
      Height = 35
      Caption = 'CPF na nota'
      Flat = True
      OnClick = sbCpfNotaClick
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Left = 1
      Top = 400
      ExplicitLeft = 1
      ExplicitTop = 400
    end
  end
  object pcDados: TPageControl
    Left = 122
    Top = 0
    Width = 872
    Height = 566
    ActivePage = tsPedidos
    Align = alClient
    TabOrder = 1
    object tsPedidos: TTabSheet
      Caption = 'Recebimento de vendas'
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 864
        Height = 537
        ActivePage = tsAguardandoRecebimento
        Align = alClient
        TabOrder = 0
        object tsAguardandoRecebimento: TTabSheet
          Caption = 'Aguardando recebimento'
          object sgOrcamentos: TGridLuka
            Left = 0
            Top = 0
            Width = 856
            Height = 508
            Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
            Align = alClient
            ColCount = 8
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
            PopupMenu = pmOpcoesPedidos
            TabOrder = 0
            OnDblClick = sgOrcamentosDblClick
            OnDrawCell = sgOrcamentosDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'Leg.'
              'Pedido'
              'Cliente'
              'Vendedor'
              'Data ped.'
              'Tipo entrega'
              'Valor total'
              'Condi'#231#227'o de pagamento')
            OnGetCellColor = sgOrcamentosGetCellColor
            OnGetCellPicture = sgOrcamentosGetCellPicture
            Grid3D = False
            RealColCount = 12
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              30
              64
              176
              145
              62
              141
              82
              184)
          end
        end
        object tsAguardandoRecebimentoEntrega: TTabSheet
          Caption = 'Aguar. recebimento na entrega'
          ImageIndex = 1
          object sgPedidosReceberEntrega: TGridLuka
            Left = 0
            Top = 0
            Width = 856
            Height = 508
            Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
            Align = alClient
            ColCount = 6
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
            PopupMenu = pmOpcoesReceberNaEntrega
            TabOrder = 0
            OnDblClick = sgPedidosReceberEntregaDblClick
            OnDrawCell = sgPedidosReceberEntregaDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'Pedido'
              'Cliente'
              'Vendedor'
              'Data ped.'
              'Valor total'
              'Condi'#231#227'o de pagamento')
            Grid3D = False
            RealColCount = 12
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              44
              218
              176
              63
              104
              196)
          end
        end
      end
    end
    object tsAcumulados: TTabSheet
      Caption = 'Acerto de acumulados'
      ImageIndex = 3
      object sgAcumulados: TGridLuka
        Left = 0
        Top = 0
        Width = 864
        Height = 537
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 7
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        PopupMenu = pmOpcoesAcumulados
        TabOrder = 0
        OnDblClick = sgAcumuladosDblClick
        OnDrawCell = sgAcumuladosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Leg.'
          'Acumulado'
          'Cliente'
          'Data fechamento'
          'Usu'#225'rio fechamento'
          'Valor total'
          'Condi'#231#227'o de pagamento')
        Grid3D = False
        RealColCount = 12
        AtivarPopUpSelecao = False
        ColWidths = (
          30
          68
          195
          106
          171
          85
          180)
      end
    end
    object tsTitulosFinanceiro: TTabSheet
      Caption = 'T'#237'tulos do financeiro'
      ImageIndex = 1
      object sgBaixas: TGridLuka
        Left = 0
        Top = 0
        Width = 864
        Height = 537
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 6
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        GradientEndColor = 16767449
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        PopupMenu = pmOpcoesTitulos
        TabOrder = 0
        OnDblClick = sgBaixasDblClick
        OnDrawCell = sgBaixasDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Baixa'
          'Tipo'
          'Cliente/Fornecedor'
          'Valor'
          'Data envio'
          'Usu'#225'rio envio')
        Grid3D = False
        RealColCount = 12
        AtivarPopUpSelecao = False
        ColWidths = (
          50
          89
          249
          91
          76
          230)
      end
    end
    object tsNotasAguardandoEmissao: TTabSheet
      Caption = 'Notas aguardando emiss'#227'o'
      ImageIndex = 2
      object sgNotasAguardandoEmissao: TGridLuka
        Left = 0
        Top = 0
        Width = 864
        Height = 537
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 7
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        PopupMenu = pmOpcoesNotas
        TabOrder = 0
        OnDblClick = sgNotasAguardandoEmissaoDblClick
        OnDrawCell = sgNotasAguardandoEmissaoDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Or'#231'amento'
          'Cliente'
          'Valor total'
          'Data pedido'
          'Tipo nota'
          'Vendedor'
          'Condic'#227'o pagto')
        OnGetCellColor = sgNotasAguardandoEmissaoGetCellColor
        Grid3D = False
        RealColCount = 12
        AtivarPopUpSelecao = False
        ColWidths = (
          67
          173
          81
          77
          84
          111
          135)
      end
    end
  end
  object pmOpcoesPedidos: TPopupMenu
    Left = 184
    Top = 240
    object miReceberPedidoSelecionado: TMenuItem
      Caption = 'Receber pedido selecionado'
      OnClick = miReceberPedidoSelecionadoClick
    end
    object miN2: TMenuItem
      Caption = '-'
    end
    object miConsultarStatusServicoNFe: TMenuItem
      Caption = 'Consultar Status servi'#231'o NFe'
      Visible = False
      OnClick = miConsultarStatusServicoNFeClick
    end
  end
  object pmOpcoesTitulos: TPopupMenu
    Left = 184
    Top = 392
    object miReceberBaixaSelecionada: TMenuItem
      Caption = 'Receber baixa selecionada'
      OnClick = miReceberBaixaSelecionadaClick
    end
    object miN3: TMenuItem
      Caption = '-'
    end
    object miVoltarFinanceiro: TMenuItem
      Caption = 'Voltar para o financeiro'
      OnClick = miVoltarFinanceiroClick
    end
  end
  object pmOpcoesAcumulados: TPopupMenu
    Left = 184
    Top = 336
    object miReceberAcumuladoSelecionado: TMenuItem
      Caption = 'Receber acumulado selecionado'
      OnClick = miReceberAcumuladoSelecionadoClick
    end
    object miN5: TMenuItem
      Caption = '-'
    end
    object miCancelarFechamentoAcumulado: TMenuItem
      Caption = 'Cancelar fechamento do acumulado selecionado'
      OnClick = miCancelarFechamentoAcumuladoClick
    end
  end
  object pmOpcoesNotas: TPopupMenu
    Left = 184
    Top = 448
    object miEmitirDocumentoFiscalPendente: TMenuItem
      Caption = 'Emitir documento fiscal pendente'
      OnClick = miEmitirDocumentoFiscalPendenteClick
    end
  end
  object pmOpcoesReceberNaEntrega: TPopupMenu
    Left = 186
    Top = 288
    object miReceberEntregaSelecionada: TMenuItem
      Caption = 'Receber pedido selecionado'
      OnClick = miReceberEntregaSelecionadaClick
    end
    object miN6: TMenuItem
      Caption = '-'
    end
    object miReimprimirdocrecebimentonaentrega1: TMenuItem
      Caption = 'Reimprimir doc. recebimento na entrega'
      OnClick = miReimprimirdocrecebimentonaentrega1Click
    end
    object miN7: TMenuItem
      Caption = '-'
    end
    object miCancelarfechamentodepedido1: TMenuItem
      Caption = 'Cancelar fechamento de pedido'
      OnClick = miCancelarfechamentodepedido1Click
    end
  end
end
