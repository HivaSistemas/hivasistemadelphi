inherited FormCadastroIndicesDescontosVenda: TFormCadastroIndicesDescontosVenda
  Caption = 'Indice de dedu'#231#227'o de venda'
  ClientHeight = 223
  ClientWidth = 334
  ExplicitWidth = 340
  ExplicitHeight = 252
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Top = 3
    ExplicitTop = 3
  end
  object lb1: TLabel [1]
    Left = 125
    Top = 39
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  object lb2: TLabel [2]
    Left = 126
    Top = 98
    Width = 62
    Height = 14
    Caption = '% desconto'
  end
  object lb3: TLabel [3]
    Left = 125
    Top = 180
    Width = 120
    Height = 14
    Caption = 'Tipo desc. pre'#231'o custo'
  end
  object lb4: TLabel [4]
    Left = 125
    Top = 140
    Width = 73
    Height = 14
    Caption = 'Tipo de custo'
  end
  inherited pnOpcoes: TPanel
    Height = 223
    ExplicitHeight = 216
  end
  inherited eID: TEditLuka
    Top = 16
    ExplicitTop = 16
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 278
    Top = 4
    OnKeyDown = ProximoCampo
    ExplicitLeft = 278
    ExplicitTop = 4
  end
  object eDescricao: TEditLuka
    Left = 125
    Top = 53
    Width = 199
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 3
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ePercentualDesconto: TEditLuka
    Left = 126
    Top = 112
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 8
    TabOrder = 4
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    ValorMaximo = 98.000000000000000000
    NaoAceitarEspaco = True
  end
  object ckPrecoCusto: TCheckBoxLuka
    Left = 126
    Top = 78
    Width = 116
    Height = 17
    Caption = '1 - Pre'#231'o de custo'
    TabOrder = 5
    OnKeyDown = ProximoCampo
    CheckedStr = 'N'
  end
  object cbTipoDescontoPrecoCusto: TComboBoxLuka
    Left = 125
    Top = 194
    Width = 140
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 6
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Nenhum'
      'Diminuir'
      'Aumentar')
    Valores.Strings = (
      'N'
      'D'
      'A')
    AsInt = 0
  end
  object cbTipoCusto: TComboBoxLuka
    Left = 125
    Top = 154
    Width = 140
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 7
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Nenhum'
      'Pre'#231'o de compra'
      'Pre'#231'o final'
      'CMV')
    Valores.Strings = (
      'N'
      'C'
      'F'
      'M')
    AsInt = 0
  end
end
