unit Relacao.ProdutosSemVender;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls,
  Vcl.Grids, GridLuka, GroupBoxLuka, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FrameEmpresas, EditLuka, _Produtos, _Sessao,
  RadioGroupLuka, Data.DB, Datasnap.DBClient, frxClass, frxDBSet,
  FrameGruposTributacoesEstadualVenda, FrameMarcas,
  Frame.DepartamentosSecoesLinhas;

type
  TFormRelacaoProdutosSemVender = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    gbStatus: TGroupBoxLuka;
    Label1: TLabel;
    sgProdutos: TGridLuka;
    Panel1: TPanel;
    eQtdeDias: TEditLuka;
    ckProdutosNuncaVenderam: TCheckBoxLuka;
    rgSituacao: TRadioGroupLuka;
    st2: TStaticText;
    st1: TStaticText;
    frxReport: TfrxReport;
    dstProdutos: TfrxDBDataset;
    cdsProdutos: TClientDataSet;
    cdsProdutosProduto: TStringField;
    cdsProdutosNome: TStringField;
    cdsProdutosUnidade: TStringField;
    cdsProdutosMarca: TStringField;
    cdsProdutosAtivo: TStringField;
    cdsProdutosQtdeEstoque: TFloatField;
    cdsProdutosCustoUnitario: TFloatField;
    cdsProdutosCustoEstoqueTotal: TFloatField;
    cdsProdutosQtdeDiasSemVender: TIntegerField;
    SpeedButton2: TSpeedButton;
    FrGrupoTributacoesEstadualVenda: TFrGruposTributacoesEstadualVenda;
    FrMarca: TFrMarcas;
    FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas;
    ckProdutosEstoqueZerado: TCheckBoxLuka;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgProdutosDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure ckProdutosNuncaVenderamClick(Sender: TObject);
    procedure sbImprimirClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure Carregar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

const
  coProdutoId         = 0;
  coNome              = 1;
  coUnidade           = 2;
  coMarca             = 3;
  coAtivo             = 4;
  coQtdeDiasSemVender = 5;
  coQtdeEstoque       = 6;
  coCustoUnitario     = 7;
  coCustoEstoqueTotal = 8;
  coTipoLinha         = 9;

  coLinhaDetalhe      = 'D';
  coLinhaTotalizador  = 'TV';

implementation

{$R *.dfm}

uses InformacoesProduto, _Biblioteca;

procedure TFormRelacaoProdutosSemVender.Carregar(Sender: TObject);
var
  empresaId: Integer;
  qtdDias: Integer;
  produtos: TArray<RecProdutosSemVender>;
  i: Integer;
  vLinha: Integer;
  vCustoTotal: Double;
  qtdeProdutos: Integer;
  vSqlTributacao: string;
  vSql: string;
begin
  inherited;

  sgProdutos.ClearGrid();
  empresaId := FrEmpresas.getEmpresa.EmpresaId;
  qtdDias := eQtdeDias.AsInt;

  if not FrMarca.EstaVazio then
    vSql := ' and ' + FrMarca.getSqlFiltros('PRO.MARCA_ID');

  if not FrDepartamentosSecoesLinhas.EstaVazio then
    vSql := ' and ' +  FrDepartamentosSecoesLinhas.getSqlFiltros('PRO.LINHA_PRODUTO_ID');

  if not ckProdutosEstoqueZerado.Checked then
    vSql := ' and EST.DISPONIVEL > 0 ';

  if not FrGrupoTributacoesEstadualVenda.EstaVazio then begin
    vSqlTributacao :=
      '  inner join VW_GRUPOS_TRIBUTACOES_ESTADUAL GTE ' +
      '  on PRO.PRODUTO_ID = GTE.PRODUTO_ID ' +
      '  and GTE.EMPRESA_ID = ''' + IntToStr(empresaId) + ''' ' +
      '  and GTE.ESTADO_ID_VENDA = ''' + FrEmpresas.getEmpresa().EstadoId + ''' ' +
      '  and GTE.ESTADO_ID_COMPRA = ''' + FrEmpresas.getEmpresa().EstadoId + ''' ' +
      '  and GTE.GRUPO_TRIB_ESTADUAL_VENDA_ID = ''' + IntToStr(FrGrupoTributacoesEstadualVenda.getGrupo.GrupoTribEstadualVendaId) + ''' ' +

      '  inner join GRUPOS_TRIB_ESTADUAL_VENDA TRE ' +
      '  on GTE.GRUPO_TRIB_ESTADUAL_VENDA_ID = TRE.GRUPO_TRIB_ESTADUAL_VENDA_ID ';
  end;

  produtos := _Produtos.BuscarProdutosSemVender(
    Sessao.getConexaoBanco,
    qtdDias,
    empresaId,
    ckProdutosNuncaVenderam.Checked,
    rgSituacao.GetValor,
    vSqlTributacao,
    vSql
  );

  if produtos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vCustoTotal := 0;
  vLinha := 0;
  for i := Low(produtos) to High(produtos) do begin
    Inc(vLinha);

    sgProdutos.Cells[coProdutoId, vLinha]          := _Biblioteca.NFormat(produtos[i].produtoId);
    sgProdutos.Cells[coNome, vLinha]               := produtos[i].nomeProduto;
    sgProdutos.Cells[coUnidade, vLinha]            := produtos[i].unidade;
    sgProdutos.Cells[coMarca, vLinha]              := IntToStr(produtos[i].marcaId) + ' - ' + produtos[i].nomeMarca;
    sgProdutos.Cells[coAtivo, vLinha]              := produtos[i].ativo;
    sgProdutos.Cells[coQtdeDiasSemVender, vLinha]  := _Biblioteca.NFormat(produtos[i].qtdeDiasSemVender);
    sgProdutos.Cells[coQtdeEstoque, vLinha]        := _Biblioteca.NFormat(produtos[i].qtdeEstoque);
    sgProdutos.Cells[coCustoUnitario, vLinha]      := _Biblioteca.NFormat(produtos[i].custoUnitario);
    sgProdutos.Cells[coCustoEstoqueTotal, vLinha]  := _Biblioteca.NFormat(produtos[i].custoTotal);
    sgProdutos.Cells[coTipoLinha, vLinha]          := coLinhaDetalhe;
    vCustoTotal := vCustoTotal + produtos[i].custoTotal;
  end;

  st2.Caption := 'Quantidade de produtos: ' + _Biblioteca.NFormat(Length(produtos) + 1);
  st1.Caption := 'Custo total: R$ ' + _Biblioteca.NFormat(vCustoTotal);

  sgProdutos.SetLinhasGridPorTamanhoVetor(vLinha);
  SetarFoco(sgProdutos);
end;

procedure TFormRelacaoProdutosSemVender.ckProdutosNuncaVenderamClick(
  Sender: TObject);
begin
  inherited;
  eQtdeDias.Enabled := not ckProdutosNuncaVenderam.Checked;
end;

procedure TFormRelacaoProdutosSemVender.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId);
end;

procedure TFormRelacaoProdutosSemVender.sbImprimirClick(Sender: TObject);
var
  vCnt: Integer;
begin
  inherited;
  cdsProdutos.Close;
  cdsProdutos.CreateDataSet;
  cdsProdutos.Open;
  for vCnt := sgProdutos.RowCount - 1 downto 1 do begin
    cdsProdutos.Insert;

    cdsProdutosProduto.AsString            := sgProdutos.Cells[coProdutoId, vCnt];
    cdsProdutosNome.AsString               := sgProdutos.Cells[coNome, vCnt];
    cdsProdutosUnidade.AsString            := sgProdutos.Cells[coUnidade, vCnt];
    cdsProdutosMarca.AsString              := sgProdutos.Cells[coMarca, vCnt];
    cdsProdutosAtivo.AsString              := sgProdutos.Cells[coAtivo, vCnt];
    cdsProdutosQtdeDiasSemVender.AsInteger := StrToIntDef(sgProdutos.Cells[coQtdeDiasSemVender, vCnt], 0);
    cdsProdutosQtdeEstoque.AsFloat         := SFormatDouble(sgProdutos.Cells[coQtdeEstoque, vCnt]);
    cdsProdutosCustoUnitario.AsFloat       := SFormatDouble(sgProdutos.Cells[coCustoUnitario, vCnt]);
    cdsProdutosCustoEstoqueTotal.AsFloat   := SFormatDouble(sgProdutos.Cells[coCustoEstoqueTotal, vCnt]);

    cdsProdutos.Post;
  end;

  TfrxMemoView(frxReport.FindComponent('mmQuantidadeProdutos')).Text :=   st2.Caption;
  TfrxMemoView(frxReport.FindComponent('mmCustoTotal')).Text         :=   st1.Caption;
  frxReport.ShowReport;
end;

procedure TFormRelacaoProdutosSemVender.sgProdutosDblClick(
  Sender: TObject);
begin
  inherited;
  InformacoesProduto.Informar(SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]));
end;

procedure TFormRelacaoProdutosSemVender.sgProdutosDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coNome, coUnidade, coMarca] then
    vAlinhamento := taLeftJustify
  else if ACol in[coAtivo] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taRightJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoProdutosSemVender.sgProdutosGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgProdutos.Cells[coTipoLinha, ARow] = coLinhaTotalizador then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
end;

procedure TFormRelacaoProdutosSemVender.SpeedButton2Click(Sender: TObject);
begin
  inherited;
  GridToExcel(sgProdutos);
end;

procedure TFormRelacaoProdutosSemVender.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrEmpresas.EstaVazio then begin
    _Biblioteca.Exclamar('� neces�rio informar uma empresa!');
    SetarFoco(FrEmpresas);
    Abort;
  end;

  if eQtdeDias.AsInt = 0 then begin
    _Biblioteca.Exclamar('� neces�rio informar a quantidade de dias!');
    SetarFoco(eQtdeDias);
    Abort;
  end;
end;

end.
