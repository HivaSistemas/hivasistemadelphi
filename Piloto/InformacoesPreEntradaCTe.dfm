inherited FormInformacoesPreEntradaCTe: TFormInformacoesPreEntradaCTe
  Caption = 'Informa'#231#245'es da pr'#233'-entrada de CTe'
  ClientHeight = 311
  ClientWidth = 662
  OnShow = FormShow
  ExplicitWidth = 668
  ExplicitHeight = 340
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 2
    Top = 0
    Width = 64
    Height = 14
    Caption = 'Pr'#233'-entrada'
  end
  object lb2: TLabel [1]
    Left = 82
    Top = 0
    Width = 75
    Height = 14
    Caption = 'CNPJ Emitente'
  end
  object lb13: TLabel [2]
    Left = 197
    Top = 0
    Width = 122
    Height = 14
    Caption = 'Raz'#227'o social emitente'
  end
  object lb11: TLabel [3]
    Left = 375
    Top = 0
    Width = 35
    Height = 14
    Caption = 'Nr. CTe'
  end
  object lb12: TLabel [4]
    Left = 441
    Top = 0
    Width = 28
    Height = 14
    Caption = 'S'#233'rie'
  end
  inherited pnOpcoes: TPanel
    Top = 274
    Width = 662
    ExplicitTop = 274
    ExplicitWidth = 662
  end
  object ePreEntradaId: TEditLuka
    Left = 2
    Top = 14
    Width = 75
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCNPJEmitente: TEditLuka
    Left = 81
    Top = 14
    Width = 112
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    Text = '02.250.123/0001-40'
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eRazaoSocialEmitente: TEditLuka
    Left = 197
    Top = 14
    Width = 172
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    Text = 'CIPLAN CIMENTOS S/A'
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object stStatusAltis: TStaticText
    Left = 496
    Top = 19
    Width = 165
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Ag.manifesta'#231#227'o'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
  end
  object st1: TStaticTextLuka
    Left = 496
    Top = 2
    Width = 165
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Status'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object eNumeroNota: TEditLuka
    Left = 375
    Top = 14
    Width = 60
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 6
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eSerieNota: TEditLuka
    Left = 441
    Top = 14
    Width = 50
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 7
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object pcDados: TPageControl
    Left = 0
    Top = 36
    Width = 661
    Height = 237
    ActivePage = tsPrincipais
    TabOrder = 8
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      object lb14: TLabel
        Left = 172
        Top = -1
        Width = 58
        Height = 14
        Caption = 'Nr. conhec.'
      end
      object sbInformaocoesEntrada: TSpeedButtonLuka
        Left = 234
        Top = 17
        Width = 18
        Height = 18
        Hint = 'Abrir informa'#231#245'es do turno'
        Flat = True
        NumGlyphs = 2
        TagImagem = 13
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object lb15: TLabel
        Left = 258
        Top = -1
        Width = 47
        Height = 14
        Caption = 'Empresa'
      end
      object lb16: TLabel
        Left = 436
        Top = -1
        Width = 106
        Height = 14
        Caption = 'Data/hora emiss'#227'o'
      end
      object lb17: TLabel
        Left = 557
        Top = -1
        Width = 78
        Height = 14
        Caption = 'Valor total CTe'
      end
      object lb18: TLabel
        Left = 294
        Top = 38
        Width = 80
        Height = 14
        Caption = 'Base c'#225'lc.ICMS'
      end
      object lb19: TLabel
        Left = 0
        Top = 80
        Width = 94
        Height = 14
        Caption = 'Chave acesso CTe'
      end
      object lb5: TLabel
        Left = 1
        Top = 38
        Width = 86
        Height = 14
        Caption = 'CNPJ Remetente'
      end
      object lb6: TLabel
        Left = 116
        Top = 38
        Width = 129
        Height = 14
        Caption = 'Raz'#227'o social remetente'
      end
      object lb7: TLabel
        Left = 387
        Top = 38
        Width = 54
        Height = 14
        Caption = 'Perc. ICMS'
      end
      object lb8: TLabel
        Left = 466
        Top = 38
        Width = 57
        Height = 14
        Caption = 'Valor ICMS'
      end
      object st2: TStaticTextLuka
        Left = 1
        Top = 1
        Width = 165
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Status SEFAZ'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object stStatusSefaz: TStaticText
        Left = 1
        Top = 18
        Width = 165
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Autorizada'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        Transparent = False
      end
      object eConhecimentoId: TEditLuka
        Left = 172
        Top = 13
        Width = 60
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 2
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eEmpresa: TEditLuka
        Left = 258
        Top = 13
        Width = 172
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 3
        Text = 'CIPLAN CIMENTOS S/A'
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataHoraEmissao: TEditLukaData
        Left = 436
        Top = 13
        Width = 115
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        ReadOnly = True
        TabOrder = 4
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object eValorTotalNota: TEditLuka
        Left = 557
        Top = 13
        Width = 91
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 5
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eBaseCalculoICMS: TEditLuka
        Left = 294
        Top = 52
        Width = 87
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 6
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eChaveAcessoNFe: TEditLuka
        Left = 0
        Top = 94
        Width = 321
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 7
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eCnpjRemetente: TEditLuka
        Left = 0
        Top = 52
        Width = 112
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 8
        Text = '02.250.123/0001-40'
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eRazaoSocialRemetente: TEditLuka
        Left = 116
        Top = 52
        Width = 172
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 9
        Text = 'CIPLAN CIMENTOS S/A'
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ePercICMS: TEditLuka
        Left = 387
        Top = 52
        Width = 73
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 10
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorICMS: TEditLuka
        Left = 466
        Top = 52
        Width = 87
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 11
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
    object tsChavesNFe: TTabSheet
      Caption = 'Chaves NFes referenciadas'
      ImageIndex = 2
      object sgReferencias: TGridLuka
        Left = 0
        Top = 0
        Width = 653
        Height = 208
        Align = alClient
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 0
        OnDrawCell = sgReferenciasDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Pre-entrada'
          'Nr. nota'
          'S'#233'rie'
          'CNPJ emitente'
          'Raz'#227'o social emitente'
          'Data\hora emiss'#227'o'
          'Valor total'
          'Chave NFe')
        Grid3D = False
        RealColCount = 20
        OrdenarOnClick = True
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          71
          64
          36
          100
          168
          115
          75
          257)
      end
    end
    object tsXML: TTabSheet
      Caption = 'XML'
      ImageIndex = 1
      object wbXML: TWebBrowser
        Left = 0
        Top = 0
        Width = 653
        Height = 208
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 300
        ExplicitHeight = 150
        ControlData = {
          4C0000007D4300007F1500000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
  end
end
