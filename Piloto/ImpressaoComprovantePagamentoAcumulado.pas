unit ImpressaoComprovantePagamentoAcumulado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos, QRExport, _Acumulados, _Sessao,
  QRPDFFilt, QRWebFilt, QRCtrls, QuickRpt, Vcl.ExtCtrls, _Biblioteca, _AcumuladosItens, _RecordsFinanceiros;

type
  TFormImpressaoComprovantePagamentoAcumuladoGrafico = class(TFormHerancaRelatoriosGraficos)
    qrl1: TQRLabel;
    qrlClienteNF: TQRLabel;
    qrl4: TQRLabel;
    qrlCondicaoPagamentoNF: TQRLabel;
    qrl5: TQRLabel;
    qrl6: TQRLabel;
    qrl7: TQRLabel;
    qrl9: TQRLabel;
    qrlTotalSerPagoNF: TQRLabel;
    qrlTotalDescontoNF: TQRLabel;
    qrlTotalOutrasDespesasNF: TQRLabel;
    qrlTotalProdutosNF: TQRLabel;
    qrl8: TQRLabel;
    qrlNFValorFrete: TQRLabel;
    qrl10: TQRLabel;
    qrlNFValorJuros: TQRLabel;
    qrl11: TQRLabel;
    qrbndNFItens: TQRBand;
    qrlNFNomeProduto: TQRLabel;
    qrlNFQuantidade: TQRLabel;
    qrlCabProdutoId: TQRLabel;
    qrl12: TQRLabel;
    qrlNFMarca: TQRLabel;
    qrl15: TQRLabel;
    qrlNFUnidade: TQRLabel;
    qrl16: TQRLabel;
    qrlNFPrecoUnitario: TQRLabel;
    qrl17: TQRLabel;
    qrlNFValorTotal: TQRLabel;
    qrl18: TQRLabel;
    qrNFmeFormasPagto: TQRMemo;
    QRBand17: TQRBand;
    QRShape7: TQRShape;
    QRLabel28: TQRLabel;
    qrNomeProdutoA4: TQRLabel;
    QRLabel33: TQRLabel;
    qrMarcaA4: TQRLabel;
    QRLabel36: TQRLabel;
    qrQuantidadeA4: TQRLabel;
    qrUnidadeA4: TQRLabel;
    QRLabel42: TQRLabel;
    qrlNFPrecoUnitarioA4: TQRLabel;
    QRLabel44: TQRLabel;
    qrlNFValorTotalA4: TQRLabel;
    qrluniA4: TQRLabel;
    qrTitulo: TQRBand;
    QRShape2: TQRShape;
    qr2: TQRLabel;
    qrOrcamentoId: TQRLabel;
    qr8: TQRLabel;
    qrCliente: TQRLabel;
    qr4: TQRLabel;
    qrTelefoneCliente: TQRLabel;
    qr14: TQRLabel;
    qrCelularCliente: TQRLabel;
    qr17: TQRLabel;
    qrCondicaoPagamento: TQRLabel;
    qr28: TQRLabel;
    qr30: TQRLabel;
    qr31: TQRLabel;
    qr29: TQRLabel;
    qrTotalSerPago: TQRLabel;
    qrValorDesconto: TQRLabel;
    qrValorOutrasDespesas: TQRLabel;
    qrTotalProdutos: TQRLabel;
    qrReportPix: TQuickRep;
    QRBand19: TQRBand;
    QRLabel7: TQRLabel;
    qrValorPix: TQRLabel;
    qrReportDinheiro: TQuickRep;
    ColumnHeaderBand1: TQRBand;
    qr16: TQRLabel;
    qrValorDinheiro: TQRLabel;
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    qrReportCredito: TQuickRep;
    QRBand6: TQRBand;
    qr27: TQRLabel;
    qrValorCredito: TQRLabel;
    qrReportCobranca: TQuickRep;
    QRBand4: TQRBand;
    qr25: TQRLabel;
    qrValorCob: TQRLabel;
    QRBand5: TQRBand;
    qrTipoCobranca: TQRLabel;
    qrValorTipoCobranca: TQRLabel;
    qr35: TQRLabel;
    qr34: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    qrVencimentoCobranca: TQRLabel;
    qrValorCobranca: TQRLabel;
    qrReportCartao: TQuickRep;
    QRBand2: TQRBand;
    qr24: TQRLabel;
    qrValorCrt: TQRLabel;
    qr26: TQRLabel;
    qr33: TQRLabel;
    QRBand3: TQRBand;
    qrTipoCobrancaCartao: TQRLabel;
    qrValorCartao: TQRLabel;
    qrJuntar: TQRCompositeReport;
    qrReportCheque: TQuickRep;
    QRBand9: TQRBand;
    qr12: TQRLabel;
    qr15: TQRLabel;
    qr18: TQRLabel;
    qr19: TQRLabel;
    qr20: TQRLabel;
    qr21: TQRLabel;
    qr22: TQRLabel;
    qr23: TQRLabel;
    DetailBand1: TQRBand;
    qrVencimentoCheque: TQRLabel;
    qrBancoCheque: TQRLabel;
    qrAgenciaCheque: TQRLabel;
    qrContaCheque: TQRLabel;
    qrNumeroCheque: TQRLabel;
    qrValorChq: TQRLabel;
    qrNomeDonoCheque: TQRLabel;
    qrTelefoneDonoCheque: TQRLabel;
    TitleBand1: TQRBand;
    qr6: TQRLabel;
    qrValorCheque: TQRLabel;
    qrReportFinal: TQuickRep;
    qrRodape: TQRBand;
    qrVersaoSistema: TQRLabel;
    procedure qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrbndNFItensBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRBand17BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qrJuntarAddReports(Sender: TObject);
    procedure qrReportCartao2BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrReportCartao2NeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand7BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrReportCartaoBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrReportCartaoNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    FPosicItens: Integer;
    FItens: TArray<RecAcumuladosItens>;
    FAcumuladoId: Integer;

    FPosicCheque: Integer;
    FCheques: TArray<RecTitulosFinanceiros>;

    FPosicCartao: Integer;
    FCartoes: TArray<RecTitulosFinanceiros>;

    FPosicCobrancas: Integer;
    FPosicItensCobr: Integer;
  public
    { Public declarations }
  end;

procedure Imprimir(const pAcumuladoId: Integer);

implementation

{$R *.dfm}

uses _AcumuladosCreditos, _AcumuladosOrcamentos, _AcumuladosPagamentos,
  _AcumuladosPagamentosCheques;

procedure Imprimir(const pAcumuladoId: Integer);
var
  vAcumulado: TArray<RecAcumulados>;
  vForm: TFormImpressaoComprovantePagamentoAcumuladoGrafico;

  vImpressora: RecImpressora;

  procedure AddFormaPagamento(pForma: string; pValor: Double);
  begin
    if pValor > 0 then
      vForm.qrNFmeFormasPagto.Lines.Add( _Biblioteca.RPad(pForma, 17, '.') + '.: R$ ' + NFormat(pValor) );
  end;

begin
  if pAcumuladoId = 0 then
    Exit;

  vAcumulado := _Acumulados.BuscarAcumulados(Sessao.getConexaoBanco, 2, [pAcumuladoId]);
  if vAcumulado = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vImpressora := Sessao.getImpressora( toComprovantePagamento );
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;

  vForm := TFormImpressaoComprovantePagamentoAcumuladoGrafico.Create(Application, vImpressora);

  VForm.FAcumuladoId := pAcumuladoId;
  vForm.FItens := _AcumuladosItens.BuscarAcumuladosItens(Sessao.getConexaoBanco, 0, [pAcumuladoId]);
  vForm.PreencherCabecalho( vAcumulado[0].EmpresaId );

  if vForm.ImpressaoGrafica then begin
    vForm.qrOrcamentoId.Caption       := NFormat(vAcumulado[0].AcumuladoId);
    vForm.qrCondicaoPagamento.Caption := NFormat(vAcumulado[0].condicaoId) + ' - ' + vAcumulado[0].NomeCondicaoPagamento;
    vForm.qrCliente.Caption           := NFormat(vAcumulado[0].clienteId) + ' - ' + vAcumulado[0].nomeCliente;
    vForm.qrTelefoneCliente.Caption   := vAcumulado[0].TelefonePrincipal;
    vForm.qrCelularCliente.Caption    := vAcumulado[0].TelefoneCelular;
    vForm.qrTotalProdutos.Caption     := NFormat(vAcumulado[0].ValorTotalProdutos);
    vForm.qrValorOutrasDespesas.Caption := NFormat(vAcumulado[0].ValorOutrasDespesas);
    vForm.qrValorDesconto.Caption     := NFormat(vAcumulado[0].ValorDesconto);
    vForm.qrTotalSerPago.Caption      := NFormat(vAcumulado[0].ValorTotal);
    vForm.qrValorDinheiro.Caption     := NFormatN(vAcumulado[0].ValorDinheiro);
    vForm.qrValorCheque.Caption       := NFormatN(vAcumulado[0].ValorCheque);
    vForm.qrValorCrt.Caption          := NFormatN(vAcumulado[0].ValorCartaoDebito);
    vForm.qrValorCob.Caption          := NFormatN(vAcumulado[0].ValorCobranca);
    vForm.qrValorCredito.Caption      := NFormatN(vAcumulado[0].ValorCredito);
    vForm.qrValorPix.Caption          := NFormatN(vAcumulado[0].ValorPix);

    vForm.qrJuntar.PrinterSettings.PrinterIndex := vImpressora.getIndex;
    if vImpressora.AbrirPreview then
      vForm.qrJuntar.Preview
    else
      vForm.qrJuntar.Print;
  end
  else begin
    vForm.qrlCondicaoPagamentoNF.Caption   := NFormat(vAcumulado[0].CondicaoId) + ' - ' + vAcumulado[0].NomeCondicaoPagamento;
    vForm.qrlClienteNF.Caption             := NFormat(vAcumulado[0].ClienteId) + ' - ' + vAcumulado[0].NomeCliente;
    vForm.qrlTotalProdutosNF.Caption       := NFormat(vAcumulado[0].ValorTotalProdutos);
    vForm.qrlTotalOutrasDespesasNF.Caption := NFormat(vAcumulado[0].ValorOutrasDespesas);
    vForm.qrlNFValorFrete.Caption          := NFormat(vAcumulado[0].ValorFrete);
    vForm.qrlTotalDescontoNF.Caption       := NFormat(vAcumulado[0].ValorDesconto);
    vForm.qrlNFValorJuros.Caption          := NFormat(vAcumulado[0].ValorJuros);
    vForm.qrlTotalSerPagoNF.Caption        := NFormat(vAcumulado[0].ValorTotal);

    vForm.qrNFmeFormasPagto.Lines.Clear;
    AddFormaPagamento('Dinheiro', vAcumulado[0].ValorDinheiro);
    AddFormaPagamento('Cart�o cr�dito', vAcumulado[0].ValorCartaoCredito);
    AddFormaPagamento('Cart�o d�bito', vAcumulado[0].ValorCartaoDebito);
    AddFormaPagamento('Cobran�a', vAcumulado[0].ValorCobranca);
    AddFormaPagamento('Cr�dito', vAcumulado[0].ValorCredito);
    AddFormaPagamento('Pix', vAcumulado[0].ValorPix);

    vForm.qrRelatorioNF.Height := vForm.qrBandTitulo.Height + (vForm.qrbndNFItens.Height * Length(vForm.FItens)) + vForm.PageFooterBand1.Height + 10;

    vForm.Imprimir;
  end;

  vForm.Free;
end;

procedure TFormImpressaoComprovantePagamentoAcumuladoGrafico.QRBand17BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;

  qrNomeProdutoA4.Caption    := _Biblioteca.NFormat(FItens[FPosicItens].ProdutoId) + ' - ' + FItens[FPosicItens].NomeProduto;
  if Length(qrNomeProdutoA4.Caption) > 38 then
    qrNomeProdutoA4.Font.Size := 8
  else
    qrNomeProdutoA4.Font.Size := 8;

  qrMarcaA4.Caption          := FItens[FPosicItens].NomeMarca;
  qrQuantidadeA4.Caption     := _Biblioteca.NFormatEstoque(FItens[FPosicItens].Quantidade);
  qrUnidadeA4.Caption        := FItens[FPosicItens].UnidadeVenda;
  qrlNFPrecoUnitarioA4.Caption := NFormat(FItens[FPosicItens].PrecoUnitario);
  qrlNFValorTotalA4.Caption    := NFormat(FItens[FPosicItens].ValorTotal);
end;

procedure TFormImpressaoComprovantePagamentoAcumuladoGrafico.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrTipoCobrancaCartao.Caption := NFormat(FCartoes[FPosicCartao].CobrancaId) + ' - ' + FCartoes[FPosicCartao].NomeCobranca;
  qrValorCartao.Caption        := NFormat(FCartoes[FPosicCartao].valor);
end;

procedure TFormImpressaoComprovantePagamentoAcumuladoGrafico.QRBand7BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrTipoCobrancaCartao.Caption := NFormat(FCartoes[FPosicCartao].CobrancaId) + ' - ' + FCartoes[FPosicCartao].NomeCobranca;
  qrValorCartao.Caption        := NFormat(FCartoes[FPosicCartao].valor);
end;

procedure TFormImpressaoComprovantePagamentoAcumuladoGrafico.qrbndNFItensBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrlNFNomeProduto.Caption    := _Biblioteca.NFormat(FItens[FPosicItens].ProdutoId) + ' - ' + FItens[FPosicItens].NomeProduto;
  if Length(qrlNFNomeProduto.Caption) > 38 then
    qrlNFNomeProduto.Font.Size := 6
  else
    qrlNFNomeProduto.Font.Size := 7;

  qrlNFMarca.Caption          := FItens[FPosicItens].NomeMarca;

  qrlNFQuantidade.Caption     := _Biblioteca.NFormatEstoque(FItens[FPosicItens].Quantidade);
  qrlNFUnidade.Caption        := FItens[FPosicItens].UnidadeVenda;

  qrlNFPrecoUnitario.Caption := NFormat(FItens[FPosicItens].PrecoUnitario);
  qrlNFValorTotal.Caption    := NFormat(FItens[FPosicItens].ValorTotal);
end;

procedure TFormImpressaoComprovantePagamentoAcumuladoGrafico.qrJuntarAddReports(
  Sender: TObject);
var
  i: Integer;
  vCobrancaId: Integer;
  vCobrancas: TArray<RecTitulosFinanceiros>;
begin
  inherited;
  if ImpressaoGrafica then begin
    qrJuntar.Reports.Add(qrRelatorioA4);

    if qrValorDinheiro.Caption <> '' then
      qrJuntar.Reports.Add(qrReportDinheiro);

    if qrValorCheque.Caption <> '' then begin
      qrJuntar.Reports.Add(qrReportCheque);
      if FCheques = nil then
        FCheques := _AcumuladosPagamentosCheques.BuscarAcumuladosPagamentosCheques(Sessao.getConexaoBanco, 0, [FAcumuladoId]);
    end;

    if qrValorCrt.Caption <> '' then begin
      qrJuntar.Reports.Add(qrReportCartao);
      if FCartoes = nil then
        FCartoes := _AcumuladosPagamentos.BuscarAcumuladosPagamentos(Sessao.getConexaoBanco, 2, [FAcumuladoId]);
    end;

    if qrValorPix.Caption <> '' then
      qrJuntar.Reports.Add( qrReportPix );

    if qrValorCredito.Caption <> '' then
      qrJuntar.Reports.Add(qrReportCredito);

    qrJuntar.Reports.Add(qrReportFinal);
  end
  else begin

  end;
end;

procedure TFormImpressaoComprovantePagamentoAcumuladoGrafico.qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicItens := -1;
end;

procedure TFormImpressaoComprovantePagamentoAcumuladoGrafico.qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicItens);
  MoreData := FPosicItens < Length(FItens);
end;

procedure TFormImpressaoComprovantePagamentoAcumuladoGrafico.qrReportCartao2BeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicCartao := -1;
end;

procedure TFormImpressaoComprovantePagamentoAcumuladoGrafico.qrReportCartao2NeedData(
  Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicCartao);
  MoreData := FPosicCartao < Length(FCartoes);
end;

procedure TFormImpressaoComprovantePagamentoAcumuladoGrafico.qrReportCartaoBeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicCartao := -1;
end;

procedure TFormImpressaoComprovantePagamentoAcumuladoGrafico.qrReportCartaoNeedData(
  Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicCartao);
  MoreData := FPosicCartao < Length(FCartoes);
end;

end.
