unit RelacaoComissoesPorTipoCobranca;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameDataInicialFinal,
  FrameFuncionarios, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameEmpresas, Vcl.Grids, GridLuka, Vcl.StdCtrls, FrameTiposCobranca,
  CheckBoxLuka;

type
  TFormRelacaoComissoesPorTipoCobranca = class(TFormHerancaRelatoriosPageControl)
    FrFuncionarios: TFrFuncionarios;
    FrDataCadastro: TFrDataInicialFinal;
    st2: TStaticText;
    sgPedidos: TGridLuka;
    sp1: TSplitter;
    st1: TStaticText;
    sgProdutos: TGridLuka;
    ger: TFrEmpresas;
    FrTiposCobranca: TFrTiposCobranca;
  private
    { Private declarations }
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormRelacaoComissoesPorTipoCobranca }

procedure TFormRelacaoComissoesPorTipoCobranca.Carregar(Sender: TObject);
begin
  inherited;

end;

procedure TFormRelacaoComissoesPorTipoCobranca.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormRelacaoComissoesPorTipoCobranca.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
