unit _EnderecoEstoqueRua;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _Sessao, _RecordsCadastros;

{$M+}
type
  TEnderecoEstoqueRua = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEnderecoEstoqueRua: RecEnderecoEstoqueRua;
  end;

function AtualizarEnderecoEstoqueRua(
  pConexao: TConexao;
  pRuaId: Integer;
  pdescricao: string
): RecRetornoBD;

function BuscarEnderecoEstoqueRua(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEnderecoEstoqueRua>;

function ExcluirEnderecoEstoqueRua(
  pConexao: TConexao;
  pRuaId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEnderecoEstoqueRua }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where RUA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o',
      True,
      0,
      'where DESCRICAO like :P1 || ''%'' ' +
      'order by ' +
      '  DESCRICAO '
    );
end;

constructor TEnderecoEstoqueRua.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENDERECO_EST_RUA');

  FSql :=
    'select ' +
    '  RUA_ID, ' +
    '  DESCRICAO ' +
    'from ' +
    '  ENDERECO_EST_RUA ';

  setFiltros(getFiltros);

  AddColuna('RUA_ID', True);
  AddColuna('DESCRICAO');
end;

function TEnderecoEstoqueRua.getRecordEnderecoEstoqueRua: RecEnderecoEstoqueRua;
begin
  Result := RecEnderecoEstoqueRua.Create;
  Result.rua_id     := getInt('RUA_ID', True);
  Result.descricao  := getString('DESCRICAO');
end;

function AtualizarEnderecoEstoqueRua(
  pConexao: TConexao;
  pRuaId: Integer;
  pDescricao: string
): RecRetornoBD;
var
  t: TEnderecoEstoqueRua;
  vNovo: Boolean;
  seq: TSequencia;
begin
  Result.Iniciar;
  t := TEnderecoEstoqueRua.Create(pConexao);

  vNovo := pRuaId = 0;
  if vNovo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_END_EST_RUA_ID');
    pRuaId := seq.getProximaSequencia;
    result.AsInt := pRuaId;
    seq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    t.setInt('RUA_ID', pRuaId, True);
    t.setString('DESCRICAO', pDescricao);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarEnderecoEstoqueRua(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEnderecoEstoqueRua>;
var
  i: Integer;
  t: TEnderecoEstoqueRua;
begin
  Result := nil;
  t := TEnderecoEstoqueRua.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEnderecoEstoqueRua;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirEnderecoEstoqueRua(
  pConexao: TConexao;
  pRuaId: Integer
): RecRetornoBD;
var
  t: TEnderecoEstoqueRua;
begin
  Result.TeveErro := False;
  t := TEnderecoEstoqueRua.Create(pConexao);

  try
    t.setInt('RUA_ID', pRuaId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
