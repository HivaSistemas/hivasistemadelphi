unit Relacao.Orcamentos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _RecordsRelatorios,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.StdCtrls, GroupBoxLuka, FrameDataInicialFinal, FrameEmpresas, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes, Vcl.Grids,
  GridLuka, FrameCondicoesPagamento, _RelacaoOrcamentosVendas, _Biblioteca, _Sessao, System.Math, System.StrUtils, Informacoes.Orcamento, Frame.TipoAnaliseCusto, CheckBoxLuka, Frame.Inteiros,
  _Orcamentos, StaticTextLuka, Vcl.Menus, Impressao.ComprovantePagamentoGrafico, _RecordsEspeciais, FrameVendedores, FrameProdutos, _RecordsOrcamentosVendas, _OrcamentosItens,
  ImpressaoConfissaoDivida, BuscarIndiceDescontoVenda, Impressao.OrcamentoGrafico, ImpressaoMeiaPaginaDuplicataMercantil,
  FrameFaixaNumeros, ComboBoxLuka, FrameGruposClientes, Data.DB,
  Datasnap.DBClient, frxClass, frxDBSet, frxExportPDF,
  Frame.TipoAcompanhamentoOrcamento, _RecordsCadastros, BuscarTipoAcompanhamentoOrcamento;

type
  TFormRelacaoOrcamentos = class(TFormHerancaRelatoriosPageControl)
    FrClientes: TFrClientes;
    FrEmpresas: TFrEmpresas;
    FrDataCadastro: TFrDataInicialFinal;
    spSeparador: TSplitter;
    sgOrcamentos: TGridLuka;
    sgProdutos: TGridLuka;
    FrCodigoOrcamento: TFrameInteiros;
    FrCodigoTurno: TFrameInteiros;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    pmOpcoesOrcamento: TPopupMenu;
    FrVendedores: TFrVendedores;
    FrProdutos: TFrProdutos;
    miN1: TMenuItem;
    miN2: TMenuItem;
    FrValorTotal: TFrFaixaNumeros;
    lb1: TLabel;
    cbAgrupamento: TComboBoxLuka;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    miReimprimirOrcamento: TSpeedButton;
    miGerarPDFdooramentovendaFocada: TSpeedButton;
    Panel1: TPanel;
    frxReport: TfrxReport;
    dstVendas: TfrxDBDataset;
    cdsVendas: TClientDataSet;
    cdsVendaspedido: TStringField;
    cdsVendasdata_recebimento: TDateField;
    cdsVendascliente: TStringField;
    cdsVendasvendedor: TStringField;
    cdsVendascond_pgto: TStringField;
    cdsVendasvalor_produto: TFloatField;
    cdsVendasvalor_total: TFloatField;
    cdsVendasoutras_despesas: TFloatField;
    cdsVendasdesconto: TFloatField;
    cdsVendasvalor_bruto: TFloatField;
    cdsVendasvalor_liquido: TFloatField;
    cdsVendasperc_lucro_bruto: TFloatField;
    cdsVendasperc_lucro_liquido: TFloatField;
    cdsVendasempresa: TStringField;
    sbAlterarTipoAcompanhamento: TSpeedButton;
    FrTipoAcompanhamento: TFrTipoAcompanhamentoOrcamento;
    sbExcluir: TSpeedButton;
    ckbPedidosCancelados: TCheckBox;
    procedure sgOrcamentosClick(Sender: TObject);
    procedure sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgOrcamentosDblClick(Sender: TObject);
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure sgOrcamentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure miReimprimirOrcamentoClick(Sender: TObject);
    procedure miGerarPDFdooramentovendaFocadaClick(Sender: TObject);
    procedure miReimprimirDuplicataMercantilClick(Sender: TObject);
    procedure sbAlterarTipoAcompanhamentoClick(Sender: TObject);
    procedure sbExcluirClick(Sender: TObject);
  private
    FProdutosOrcamentos: TArray<RecOrcamentoItens>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

uses
  ImpressaoRelacaoVendasProdutosGrafico, SelecionarVariosOpcoes, untDmRelatorio,
  Opcoes.OrcamentoGrafico;

{$R *.dfm}

{ TFormRelacaoOrcamentosVendas }

const
  coOrcamentoId      = 0;
  coCliente          = 1;
  coDataCadastro     = 2;
  coVendedor         = 3;
  coTipoAcompanhamento = 4;
  coValorProdutos    = 5;
  coValorTotal       = 6;
  coValorOutDesp     = 7;
  coValorFrete       = 8;
  coValorDesconto    = 9;
  coValorLucroBruto  = 10;
  coPercLucroBruto   = 11;
  coValorLucroLiq    = 12;
  coPercLucroLiquido = 13;
  coEmpresa          = 14;
  coTipoAcompId      = 15;
  coIndiceDescVenda  = 16;
  coTipoLinha        = 17;

  (* Grid de produto *)
  cpProdutoId        = 0;
  cpNome             = 1;
  cpMarca            = 2;
  cpPrecoUnitario    = 3;
  cpQuantidade       = 4;
  cpUnidade          = 5;
  cpValorTotal       = 6;
  cpValorOutDesp     = 7;
  cpValorDesconto    = 8;
  cpValorLucroBruto  = 9;
  cpPercLucroBruto   = 10;
  cpValorLucroLiquido = 11;
  cpPercLucroLiquido  = 12;

  coLinhaDetalhe   = 'D';
  coLinhaCabAgrup  = 'CAGRU';
  coLinhaTotVenda  = 'TV';
  coTotalAgrupador = 'TAGRU';

procedure TFormRelacaoOrcamentos.Carregar(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vAgrupadorId: Integer;

  vComando: string;
  vOrcamentosIds: TArray<Integer>;
  vOrcamentosVendas: TArray<RecOrcamentosVendas>;

  vSqlFormasPagto: string;
  vSqlStatus: string;

  vTotais: record
    totalProdutos: Double;
    totalOutrasDespesas: Double;
    totalValorFrete: Double;
    totalDesconto: Double;
    valorTotal: Double;
    ValorLucroBruto: Double;
    PercLucroBruto: Double;
    ValorLucroLiquido: Double;
    PercLucroLiquido: Double;
  end;

  vTotaisAgrupador: record
    totalProdutos: Double;
    totalOutrasDespesas: Double;
    totalValorFrete: Double;
    totalDesconto: Double;
    valorTotal: Double;
    ValorLucroBruto: Double;
    PercLucroBruto: Double;
    ValorLucroLiquido: Double;
    PercLucroLiquido: Double;
  end;

  procedure TotalizarGrupo;
  var
    vAgrupador: string;
  begin
    Inc(vLinha);

    vAgrupador := _Biblioteca.Decode(cbAgrupamento.GetValor, ['VEN', 'VENDEDOR', 'CLI', 'CLIENTE', 'GRUPO DE CLIENTE']);

    sgOrcamentos.Cells[coTipoAcompanhamento, vLinha]    := 'TOTAL DO ' + vAgrupador + ': ';
    sgOrcamentos.Cells[coValorProdutos, vLinha]    := NFormatN(vTotaisAgrupador.totalProdutos);
    sgOrcamentos.Cells[coValorOutDesp, vLinha]     := NFormatN(vTotaisAgrupador.totalOutrasDespesas);
    sgOrcamentos.Cells[coValorFrete, vLinha]       := NFormatN(vTotaisAgrupador.totalValorFrete);
    sgOrcamentos.Cells[coValorDesconto, vLinha]    := NFormatN(vTotaisAgrupador.totalDesconto);
    sgOrcamentos.Cells[coValorTotal, vLinha]       := NFormatN(vTotaisAgrupador.valorTotal);
    sgOrcamentos.Cells[coValorLucroBruto, vLinha]  := NFormatN(vTotaisAgrupador.ValorLucroBruto);
    sgOrcamentos.Cells[coValorLucroLiq, vLinha]    := NFormatN(vTotaisAgrupador.ValorLucroLiquido);
    sgOrcamentos.Cells[coPercLucroBruto, vLinha]   := NFormatN( Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(vTotaisAgrupador.valorTotal, vTotaisAgrupador.ValorLucroBruto, 4), 4 );
    sgOrcamentos.Cells[coPercLucroLiquido, vLinha] := NFormatN( Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vTotaisAgrupador.valorTotal, vTotaisAgrupador.ValorLucroLiquido,4 ), 4);
    sgOrcamentos.Cells[coTipoLinha, vLinha]        := coTotalAgrupador;

    vTotaisAgrupador.totalProdutos       := 0;
    vTotaisAgrupador.totalOutrasDespesas := 0;
    vTotaisAgrupador.totalValorFrete     := 0;
    vTotaisAgrupador.totalDesconto       := 0;
    vTotaisAgrupador.valorTotal          := 0;
    vTotaisAgrupador.ValorLucroBruto     := 0;
    vTotaisAgrupador.PercLucroBruto      := 0;
    vTotaisAgrupador.ValorLucroLiquido   := 0;
    vTotaisAgrupador.PercLucroLiquido    := 0;
  end;

begin
  inherited;
  sgOrcamentos.ClearGrid();
  sgProdutos.ClearGrid();
  FProdutosOrcamentos := nil;

  if not FrCodigoOrcamento.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrCodigoOrcamento.getSqlFiltros('ORCAMENTO_ID'));

  if not FrCodigoTurno.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrCodigoTurno.getSqlFiltros('TURNO_ID'));

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrEmpresas.getSqlFiltros('EMPRESA_ID'));

  if not FrClientes.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrClientes.getSqlFiltros('CLIENTE_ID'));

  if not FrDataCadastro.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vComando, FrDataCadastro.getSqlFiltros('DATA_CADASTRO'));

  if not FrVendedores.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrVendedores.getSqlFiltros('VENDEDOR_ID'));

  if not FrTipoAcompanhamento.EstaVazio then
    _Biblioteca.WhereOuAnd(vComando, FrTipoAcompanhamento.getSqlFiltros('TIPO_ACOMPANHAMENTO_ID'));

  if ckbPedidosCancelados.Checked then
  begin
    _Biblioteca.WhereOuAnd(vComando,
                      'ORCAMENTO_ID in (select distinct ORCAMENTO_ID '
                                      +' from ORCAMENTOS_TRAMITES '
                                      +' where ROTINA_ALTERACAO = ''CANCELAR_RECEBIMENTO_PEDIDO'' '
                                      +'    AND STATUS = ''OE''  )'
                                      +'    AND STATUS <> ''RE'' ');
  end else
    _Biblioteca.WhereOuAnd(vComando, ' STATUS = ''OE'' ' );

  _Biblioteca.WhereOuAnd(vComando, FrValorTotal.getSQL('VALOR_TOTAL'));

  if not FrProdutos.EstaVazio then begin
    _Biblioteca.WhereOuAnd(
      vComando,
      'ORCAMENTO_ID in(select distinct ORCAMENTO_ID from ORCAMENTOS_ITENS where ' + FiltroInInt('PRODUTO_ID', FrProdutos.GetArrayOfInteger) + ') '
    );
  end;


  _Biblioteca.WhereOuAnd(vComando, 'TIPO_ANALISE_CUSTO = ''C'' ');

  if cbAgrupamento.GetValor = 'VEN' then
    vComando := vComando + ' order by NOME_VENDEDOR '
  else if cbAgrupamento.GetValor = 'CLI' then
    vComando := vComando + ' order by NOME_CLIENTE '
  else if cbAgrupamento.GetValor = 'GCL' then
    vComando := vComando + ' order by NOME_GRUPO_CLIENTE '
  else
    vComando := vComando + ' order by ORCAMENTO_ID ';

  vOrcamentosVendas := _RelacaoOrcamentosVendas.BuscarOrcamentos(Sessao.getConexaoBanco, vComando);
  if vOrcamentosVendas = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vAgrupadorId := -1;

  vTotais.totalProdutos       := 0;
  vTotais.totalOutrasDespesas := 0;
  vTotais.totalValorFrete     := 0;
  vTotais.totalDesconto       := 0;
  vTotais.valorTotal          := 0;
  vTotais.ValorLucroBruto     := 0;
  vTotais.PercLucroBruto      := 0;
  vTotais.ValorLucroLiquido   := 0;
  vTotais.PercLucroLiquido    := 0;

  vTotaisAgrupador.totalProdutos       := 0;
  vTotaisAgrupador.totalOutrasDespesas := 0;
  vTotaisAgrupador.totalValorFrete     := 0;
  vTotaisAgrupador.totalDesconto       := 0;
  vTotaisAgrupador.valorTotal          := 0;
  vTotaisAgrupador.ValorLucroBruto     := 0;
  vTotaisAgrupador.PercLucroBruto      := 0;
  vTotaisAgrupador.ValorLucroLiquido   := 0;
  vTotaisAgrupador.PercLucroLiquido    := 0;

  vLinha := 0;
  for i := Low(vOrcamentosVendas) to High(vOrcamentosVendas) do begin
    if cbAgrupamento.GetValor <> 'NEN' then begin
      if cbAgrupamento.GetValor = 'VEN' then begin
        if vAgrupadorId <> vOrcamentosVendas[i].VendedorId then begin
          if i > 0 then
            TotalizarGrupo;

          Inc(vLinha);

          sgOrcamentos.Cells[coCliente, vLinha]   := getInformacao(vOrcamentosVendas[i].VendedorId, vOrcamentosVendas[i].NomeVendedor);
          sgOrcamentos.Cells[coTipoLinha, vLinha] := coLinhaCabAgrup;

          vAgrupadorId := vOrcamentosVendas[i].VendedorId;
        end;
      end
      else if cbAgrupamento.GetValor = 'CLI' then begin
        if vAgrupadorId <> vOrcamentosVendas[i].ClienteId then begin
          if i > 0 then
            TotalizarGrupo;

          Inc(vLinha);

          sgOrcamentos.Cells[coCliente, vLinha]   := getInformacao(vOrcamentosVendas[i].ClienteId, vOrcamentosVendas[i].NomeCliente);
          sgOrcamentos.Cells[coTipoLinha, vLinha] := coLinhaCabAgrup;

          vAgrupadorId := vOrcamentosVendas[i].ClienteId;
        end;
      end
      else if cbAgrupamento.GetValor = 'GCL' then begin
        if vAgrupadorId <> vOrcamentosVendas[i].ClienteGrupoId then begin
          if i > 0 then
            TotalizarGrupo;

          Inc(vLinha);

          sgOrcamentos.Cells[coCliente, vLinha]   := getInformacao(vOrcamentosVendas[i].ClienteGrupoId, vOrcamentosVendas[i].NomeGrupoCliente);
          sgOrcamentos.Cells[coTipoLinha, vLinha] := coLinhaCabAgrup;

          vAgrupadorId := vOrcamentosVendas[i].ClienteGrupoId;
        end;
      end;
    end;

    Inc(vLinha);

    sgOrcamentos.Cells[coOrcamentoId, vLinha]      := NFormat(vOrcamentosVendas[i].OrcamentoId);
    sgOrcamentos.Cells[coCliente, vLinha]          := NFormat(vOrcamentosVendas[i].ClienteId) + ' - ' + vOrcamentosVendas[i].NomeCliente;
    sgOrcamentos.Cells[coDataCadastro, vLinha]     := DFormat(vOrcamentosVendas[i].DataCadastro);
    sgOrcamentos.Cells[coVendedor, vLinha]         := getInformacao(vOrcamentosVendas[i].VendedorId, vOrcamentosVendas[i].NomeVendedor);
    sgOrcamentos.Cells[coTipoAcompanhamento, vLinha] := NFormat(vOrcamentosVendas[i].TipoAcompanhamentoId) + ' - ' + vOrcamentosVendas[i].TipoAcompanhamentoDesc;
    sgOrcamentos.Cells[coTipoAcompId, vLinha]      := NFormat(vOrcamentosVendas[i].TipoAcompanhamentoId);
    sgOrcamentos.Cells[coValorProdutos, vLinha]    := NFormatN(vOrcamentosVendas[i].ValorTotalProdutos);
    sgOrcamentos.Cells[coValorOutDesp, vLinha]     := NFormatN(vOrcamentosVendas[i].ValorOutrasDespesas);
    sgOrcamentos.Cells[coValorFrete, vLinha]       := NFormatN(vOrcamentosVendas[i].ValorFrete);
    sgOrcamentos.Cells[coValorDesconto, vLinha]    := NFormatN(vOrcamentosVendas[i].ValorDesconto);
    sgOrcamentos.Cells[coValorTotal, vLinha]       := NFormatN(vOrcamentosVendas[i].ValorTotal);
    sgOrcamentos.Cells[coValorLucroBruto, vLinha]  := NFormatN(vOrcamentosVendas[i].ValorTotal - vOrcamentosVendas[i].ValorCustoEntrada);
    sgOrcamentos.Cells[coValorLucroLiq, vLinha]    := NFormatN(vOrcamentosVendas[i].ValorTotal - vOrcamentosVendas[i].ValorCustoFinal);
    sgOrcamentos.Cells[coPercLucroBruto, vLinha]   := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(vOrcamentosVendas[i].ValorTotal, vOrcamentosVendas[i].ValorTotal - vOrcamentosVendas[i].ValorCustoEntrada, 4), 4);
    sgOrcamentos.Cells[coPercLucroLiquido, vLinha] := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vOrcamentosVendas[i].ValorTotal, vOrcamentosVendas[i].ValorTotal - vOrcamentosVendas[i].ValorCustoFinal, 4), 4);
    sgOrcamentos.Cells[coEmpresa, vLinha]          := NFormat(vOrcamentosVendas[i].EmpresaId) + ' - ' + vOrcamentosVendas[i].NomeEmpresa;
    sgOrcamentos.Cells[coIndiceDescVenda, vLinha]  := NFormat(vOrcamentosVendas[i].IndiceDescontoVendaId);
    sgOrcamentos.Cells[coTipoLinha, vLinha]        := coLinhaDetalhe;

    vTotais.totalProdutos       := vTotais.totalProdutos + vOrcamentosVendas[i].ValorTotalProdutos;
    vTotais.totalOutrasDespesas := vTotais.totalOutrasDespesas + vOrcamentosVendas[i].ValorOutrasDespesas;
    vTotais.totalValorFrete     := vTotais.totalValorFrete + vOrcamentosVendas[i].ValorFrete;
    vTotais.totalDesconto       := vTotais.totalDesconto + vOrcamentosVendas[i].ValorDesconto;
    vTotais.valorTotal          := vTotais.valorTotal + vOrcamentosVendas[i].ValorTotal;
    vTotais.ValorLucroBruto     := vTotais.ValorLucroBruto + SFormatDouble( sgOrcamentos.Cells[coValorLucroBruto, vLinha] );
    vTotais.ValorLucroLiquido   := vTotais.ValorLucroLiquido + SFormatDouble( sgOrcamentos.Cells[coValorLucroLiq, vLinha] );
    vTotais.PercLucroBruto      := vTotais.PercLucroBruto + SFormatDouble( sgOrcamentos.Cells[coPercLucroBruto, vLinha] );
    vTotais.PercLucroLiquido    := vTotais.PercLucroLiquido + SFormatDouble( sgOrcamentos.Cells[coPercLucroLiquido, vLinha] );

    vTotaisAgrupador.totalProdutos       := vTotaisAgrupador.totalProdutos + vOrcamentosVendas[i].ValorTotalProdutos;
    vTotaisAgrupador.totalOutrasDespesas := vTotaisAgrupador.totalOutrasDespesas + vOrcamentosVendas[i].ValorOutrasDespesas;
    vTotaisAgrupador.totalValorFrete     := vTotaisAgrupador.totalValorFrete + vOrcamentosVendas[i].ValorFrete;
    vTotaisAgrupador.totalDesconto       := vTotaisAgrupador.totalDesconto + vOrcamentosVendas[i].ValorDesconto;
    vTotaisAgrupador.valorTotal          := vTotaisAgrupador.valorTotal + vOrcamentosVendas[i].ValorTotal;
    vTotaisAgrupador.ValorLucroBruto     := vTotaisAgrupador.ValorLucroBruto + SFormatDouble( sgOrcamentos.Cells[coValorLucroBruto, vLinha] );
    vTotaisAgrupador.ValorLucroLiquido   := vTotaisAgrupador.ValorLucroLiquido + SFormatDouble( sgOrcamentos.Cells[coValorLucroLiq, vLinha] );
    vTotaisAgrupador.PercLucroBruto      := vTotaisAgrupador.PercLucroBruto + SFormatDouble( sgOrcamentos.Cells[coPercLucroBruto, vLinha] );
    vTotaisAgrupador.PercLucroLiquido    := vTotaisAgrupador.PercLucroLiquido + SFormatDouble( sgOrcamentos.Cells[coPercLucroLiquido, vLinha] );

    _Biblioteca.AddNoVetorSemRepetir(vOrcamentosIds, vOrcamentosVendas[i].OrcamentoId);
  end;

  if cbAgrupamento.GetValor <> 'NEN' then begin
    TotalizarGrupo;
    Inc(vLinha);
  end;

  Inc(vLinha);

  sgOrcamentos.Cells[coTipoAcompanhamento, vLinha]    := 'TOTAIS: ';
  sgOrcamentos.Cells[coValorProdutos, vLinha]    := NFormatN(vTotais.totalProdutos);
  sgOrcamentos.Cells[coValorOutDesp, vLinha]     := NFormatN(vTotais.totalOutrasDespesas);
  sgOrcamentos.Cells[coValorFrete, vLinha]       := NFormatN(vTotais.totalValorFrete);
  sgOrcamentos.Cells[coValorDesconto, vLinha]    := NFormatN(vTotais.totalDesconto);
  sgOrcamentos.Cells[coValorTotal, vLinha]       := NFormatN(vTotais.valorTotal);
  sgOrcamentos.Cells[coValorLucroBruto, vLinha]  := NFormatN(vTotais.ValorLucroBruto);
  sgOrcamentos.Cells[coValorLucroLiq, vLinha]    := NFormatN(vTotais.ValorLucroLiquido);
  sgOrcamentos.Cells[coPercLucroBruto, vLinha]   := NFormatN( Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(vTotais.valorTotal, vTotais.ValorLucroBruto, 4), 4 );
  sgOrcamentos.Cells[coPercLucroLiquido, vLinha] := NFormatN( Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vTotais.valorTotal, vTotais.ValorLucroLiquido,4 ), 4);
  sgOrcamentos.Cells[coTipoLinha, vLinha]        := coLinhaTotVenda;

  Inc(vLinha);
  sgOrcamentos.RowCount := vLinha;

  FProdutosOrcamentos := _OrcamentosItens.BuscarOrcamentosItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.ORCAMENTO_ID', vOrcamentosIds) + ' and ITE.ITEM_KIT_ID is null order by PRO.NOME ');
  sgOrcamentosClick(sgOrcamentos);
  SetarFoco(sgOrcamentos);
end;

procedure TFormRelacaoOrcamentos.FormCreate(Sender: TObject);
var
  vDataAtual: TDateTime;
begin
  inherited;
  AtivarAnaliseCusto(Sessao.getParametros.UtilizarAnaliseCusto);

  ActiveControl := FrEmpresas.sgPesquisa;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);

  if Sessao.getUsuarioLogado.Vendedor = 'S' then begin
    vDataAtual := Sessao.getData;

    if not Sessao.AutorizadoRotina('visualizar_vendas_outras_vendedores') then begin
      FrVendedores.InserirDadoPorChave(Sessao.getUsuarioLogado.funcionario_id, False);
      FrVendedores.Modo(False, False);
    end;

    if not Sessao.AutorizadoRotina('visualizar_vendas_fora_dia_atual') then begin
      FrDataCadastro.eDataInicial.AsData := vDataAtual;
      FrDataCadastro.eDataFinal.AsData := vDataAtual;
      FrDataCadastro.Modo(False, False);
    end;

    if not Sessao.AutorizadoRotina('visualizar_lucro_venda') then begin
     sgOrcamentos.OcultarColunas([
       coValorLucroBruto,
       coValorLucroLiq,
       coPercLucroBruto,
       coPercLucroLiquido
     ]);

     sgProdutos.OcultarColunas([
       cpValorLucroBruto,
       cpValorLucroLiquido,
       cpPercLucroBruto,
       cpPercLucroLiquido
     ]);
    end;

    if not Sessao.AutorizadoRotina('visualizar_lucro_venda') then
     sgOrcamentos.OcultarColunas([coValorLucroBruto, coValorLucroLiq, coPercLucroBruto, coPercLucroLiquido, cpValorLucroBruto, cpValorLucroLiquido, cpPercLucroBruto, cpPercLucroLiquido]);
  end;
end;

procedure TFormRelacaoOrcamentos.Imprimir(Sender: TObject);
var
  i: Integer;
  vUltimoVendedorId: Integer;
  vDados: TArray<RecDadosImpressao>;
  vOpcao: TRetornoTelaFinalizar<Integer>;
begin
  inherited;

//  if cbAgrupamento.GetValor <> caAgrupadoVendedor then begin
//    _Biblioteca.ImpressaoNaoDisponivel;
//    Exit;
//  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Tipo relat�rio', ['Gr�fico'], 0);
  if vOpcao.BuscaCancelada then
    Exit;
  if vOpcao.Dados = 0 then
  begin
    cdsVendas.Close;
    cdsVendas.CreateDataSet;
    cdsVendas.Open;
    for i := sgOrcamentos.RowCount - 2 downto 1 do
    begin
      cdsVendas.Insert;
      cdsVendaspedido.AsString              := sgOrcamentos.Cells[coOrcamentoId,i];
      cdsVendascliente.AsString             := sgOrcamentos.Cells[coCliente,i];
      cdsVendasvendedor.AsString            := sgOrcamentos.Cells[coVendedor,i];

      cdsVendasvalor_produto.AsFloat        := SFormatDouble(sgOrcamentos.Cells[coValorProdutos,i]);
      cdsVendasvalor_total.AsFloat          := SFormatDouble(sgOrcamentos.Cells[coValorTotal,i]);
      cdsVendasoutras_despesas.AsFloat      := SFormatDouble(sgOrcamentos.Cells[coValorOutDesp,i]);
      cdsVendasdesconto.AsFloat             := SFormatDouble(sgOrcamentos.Cells[coValorDesconto,i]);

      if Sessao.AutorizadoRotina('visualizar_lucro_venda') then begin
        cdsVendasvalor_bruto.AsFloat          := SFormatDouble(sgOrcamentos.Cells[coValorLucroBruto,i]);
        cdsVendasvalor_liquido.AsFloat        := SFormatDouble(sgOrcamentos.Cells[coValorLucroLiq,i]);
        cdsVendasperc_lucro_bruto.AsFloat     := SFormatDouble(sgOrcamentos.Cells[coPercLucroBruto,i]);
        cdsVendasperc_lucro_liquido.AsFloat   := SFormatDouble(sgOrcamentos.Cells[coPercLucroLiquido,i]);
      end;

      cdsVendasempresa.AsString             := sgOrcamentos.Cells[coEmpresa,i];
      cdsVendas.Post;
    end;

    if Sessao.AutorizadoRotina('visualizar_lucro_venda') then begin
      TfrxMemoView(frxReport.FindComponent('mmPercLucroBruto')).Text := '% Lucro Bruto.....: ' + sgOrcamentos.Cells[coPercLucroBruto,sgOrcamentos.RowCount - 1];
      TfrxMemoView(frxReport.FindComponent('mmPercLucroLiq')).Text   := '% Lucro Liquido...: ' + sgOrcamentos.Cells[coPercLucroLiquido,sgOrcamentos.RowCount - 1];
    end;

    frxReport.ShowReport;
  end else
  begin
    vDados := nil;
    vUltimoVendedorId := -1;
    for i := 1 to sgProdutos.RowCount -1 do begin
  //    if sgProdutos.Cells[coTipoLinha, i] = clLinhaAgrupamento then begin
  //      if vUltimoVendedorId <> SFormatInt(sgProdutos.Cells[coVendedorId, i]) then begin
  //        SetLength(vDados, Length(vDados) + 1);
  //        vDados[High(vDados)].Vendedor := sgProdutos.Cells[coProdutoid, i];
  //
  //        vUltimoVendedorId := SFormatInt(sgProdutos.Cells[coVendedorId, i]);
  //
  //        Continue;
  //      end;
  //    end;

      // Se for a linha de totalizacao
  //    if sgProdutos.Cells[coTipoLinha, i] = clLinhaTotVendedor then begin
  //      vDados[High(vDados)].QtdeTotal    := SFormatDouble(sgProdutos.Cells[coQtdVendida, i]) - SFormatDouble(sgProdutos.Cells[coQtdDevolvida, i]);
  //      vDados[High(vDados)].TotalLiquido := SFormatDouble(sgProdutos.Cells[coValorVenda, i]) - SFormatDouble(sgProdutos.Cells[coValorDevolucoes, i]);
  //
  //      Continue;
  //    end;

  //    if sgProdutos.Cells[coTipoLinha, i] <> clLinhaDetalhe then
  //      Continue;

      SetLength(vDados[High(vDados)].Produtos, Length(vDados[High(vDados)].Produtos) + 1);
      vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].ProdutoId    := SFormatInt(sgProdutos.Cells[cpProdutoId, i]);
      vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].NomeProduto  := sgProdutos.Cells[cpNome, i];
      vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].NomeMarca    := sgProdutos.Cells[cpMarca, i];
      vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].Quantidade   := SFormatDouble(sgProdutos.Cells[cpQuantidade, i]) {- SFormatDouble(sgProdutos.Cells[coQtdDevolvida, i])};
      vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].Unidade      := sgProdutos.Cells[cpUnidade, i];
      vDados[High(vDados)].Produtos[High(vDados[High(vDados)].Produtos)].ValorLiquido := SFormatDouble(sgProdutos.Cells[cpValorTotal, i]) {- SFormatDouble(sgProdutos.Cells[coValorDevolucoes, i])};
    end;


    ImpressaoRelacaoVendasProdutosGrafico.Imprimir(Self, vOpcao.Dados, vDados, FFiltrosUtilizados)

  end;


end;

procedure TFormRelacaoOrcamentos.miGerarPDFdooramentovendaFocadaClick(Sender: TObject);
begin
  inherited;
  Impressao.OrcamentoGrafico.Imprimir( SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]), True );
end;

procedure TFormRelacaoOrcamentos.miReimprimirDuplicataMercantilClick(Sender: TObject);
begin
  inherited;
  ImpressaoMeiaPaginaDuplicataMercantil.Emitir( SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]), tpOrcamento );
end;

procedure TFormRelacaoOrcamentos.miReimprimirOrcamentoClick(Sender: TObject);
var
  pExibirPrecoUnitarioProdutos: Boolean;
  pExibirPrecoTotalProdutos: Boolean;
  pExibirDescontoProdutos: Boolean;
begin
  inherited;
  pExibirPrecoUnitarioProdutos := True;
  pExibirPrecoTotalProdutos := True;
  pExibirDescontoProdutos   := True;
  Opc��esImpressaoOrcamentoGrafico(pExibirPrecoUnitarioProdutos, pExibirPrecoTotalProdutos, pExibirDescontoProdutos);
  Impressao.OrcamentoGrafico.Imprimir(
    SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]),
    False,
    False,
    pExibirPrecoUnitarioProdutos,
    pExibirPrecoTotalProdutos,
    pExibirDescontoProdutos
  );
end;

procedure TFormRelacaoOrcamentos.sbAlterarTipoAcompanhamentoClick(
  Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecTipoAcompanhamentoOrcamento>;
  vRetBanco: RecRetornoBD;
  vTipoAcompanhamentoId: Integer;
  vOrcamentoId: Integer;
begin
  inherited;

  if sgOrcamentos.Cells[coOrcamentoId, 1] = '' then
    Exit;

  vTipoAcompanhamentoId := StrToInt(sgOrcamentos.Cells[coTipoAcompId, sgOrcamentos.Row]);
  vOrcamentoId := SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]);

  vRetorno := BuscarTipoAcompanhamentoOrcamento.BuscarDadosCliente(vTipoAcompanhamentoId);
  if vRetorno.BuscaCancelada then
    Exit;

  vRetBanco := _RelacaoOrcamentosVendas.AtualizarTipoAcompanhamentoOrcamento(
    Sessao.getConexaoBanco,
    vRetorno.Dados.tipo_acompanhamento_id,
    vOrcamentoId
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Erro ao atualizar o tipo de acompanhamento do or�amento.');
    Exit;
  end;

  _Biblioteca.Informar('Tipo de acompanhamento de or�amento atualizado!');
  Carregar(nil);
end;

procedure TFormRelacaoOrcamentos.sbExcluirClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if sgOrcamentos.IsRowEmpty(2) then
  begin
    _Biblioteca.Exclamar('Nenhum or�amento selecionado!');
    exit;
  end;


  if not _Biblioteca.Perguntar('Aten��o, deseja realmente excluir o or�amento selecionado? Este processo � irrevers�vel.') then
    Exit;


  Sessao.getConexaoBanco.IniciarTransacao;

  vRetBanco :=
    _Orcamentos.ExcluirOrcamento(
      Sessao.getConexaoBanco,
      SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]));

  Sessao.AbortarSeHouveErro(vRetBanco);

  Sessao.getConexaoBanco.FinalizarTransacao;

  sgOrcamentos.DeleteRow(sgOrcamentos.Row);
  sgOrcamentosClick(nil);

end;

procedure TFormRelacaoOrcamentos.sgOrcamentosClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
begin
  inherited;
  sgProdutos.ClearGrid;
  if sgOrcamentos.Cells[cpNome, 1] = '' then
    Exit;

  vLinha := 0;
  for i := Low(FProdutosOrcamentos) to High(FProdutosOrcamentos) do begin
    if SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]) <> FProdutosOrcamentos[i].orcamento_id then
      Continue;

    Inc(vLinha);

    sgProdutos.Cells[cpProdutoId, vLinha]         := NFormat(FProdutosOrcamentos[i].produto_id);
    sgProdutos.Cells[cpNome, vLinha]              := FProdutosOrcamentos[i].nome;
    sgProdutos.Cells[cpMarca, vLinha]             := FProdutosOrcamentos[i].nome_marca;
    sgProdutos.Cells[cpPrecoUnitario, vLinha]     := NFormat(FProdutosOrcamentos[i].preco_unitario);
    sgProdutos.Cells[cpQuantidade, vLinha]        := NFormatEstoque(FProdutosOrcamentos[i].Quantidade);
    sgProdutos.Cells[cpUnidade, vLinha]           := FProdutosOrcamentos[i].unidade_venda;
    sgProdutos.Cells[cpValorTotal, vLinha]        := NFormat(FProdutosOrcamentos[i].valor_total);
    sgProdutos.Cells[cpValorLucroBruto, vLinha]   := NFormat(FProdutosOrcamentos[i].ValorLiquido - FProdutosOrcamentos[i].CustoEntrada);
    sgProdutos.Cells[cpValorLucroLiquido, vLinha] := NFormat(FProdutosOrcamentos[i].ValorLiquido - FProdutosOrcamentos[i].ValorCustoFinal);
    sgProdutos.Cells[cpPercLucroBruto, vLinha]    := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(FProdutosOrcamentos[i].ValorLiquido, FProdutosOrcamentos[i].ValorLiquido - FProdutosOrcamentos[i].CustoEntrada, 4), 4);
    sgProdutos.Cells[cpPercLucroLiquido, vLinha]  := NFormatN(Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(FProdutosOrcamentos[i].ValorLiquido, FProdutosOrcamentos[i].ValorLiquido - FProdutosOrcamentos[i].ValorCustoFinal, 4), 4);
  end;
  sgProdutos.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormRelacaoOrcamentos.sgOrcamentosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]));
end;

procedure TFormRelacaoOrcamentos.sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coOrcamentoId, coValorProdutos, coValorOutDesp, coValorFrete, coValorDesconto, coValorTotal, coValorLucroBruto, coValorLucroLiq, coPercLucroBruto, coPercLucroLiquido] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgOrcamentos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoOrcamentos.sgOrcamentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coPercLucroBruto then
    AFont.Color := _Sessao.Sessao.getCorLucro( SFormatDouble( sgOrcamentos.Cells[coPercLucroBruto, ARow] ) )
  else if ACol = coPercLucroLiquido then
    AFont.Color := _Sessao.Sessao.getCorLucro( SFormatDouble( sgOrcamentos.Cells[coPercLucroLiquido, ARow] ) );

  if sgOrcamentos.Cells[coTipoLinha, ARow] = coLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if sgOrcamentos.Cells[coTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgOrcamentos.Cells[coTipoLinha, ARow] = coTotalAgrupador then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
  end
end;

procedure TFormRelacaoOrcamentos.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    cpProdutoId,
    cpPrecoUnitario,
    cpQuantidade,
    cpValorTotal,
    cpValorLucroBruto,
    cpValorLucroLiquido,
    cpPercLucroBruto,
    cpPercLucroLiquido]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoOrcamentos.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if not FrCodigoOrcamento.EstaVazio then
    Exit;

end;

end.
