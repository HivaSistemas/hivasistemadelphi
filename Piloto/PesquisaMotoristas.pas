unit PesquisaMotoristas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Sessao, _Motoristas,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Biblioteca, Vcl.ExtCtrls;

type
  TFormPesquisaMotoristas = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
function PesquisarVarios(pSomenteAtivos: Boolean): TArray<TObject>;

implementation

{$R *.dfm}

const
  coRazaoSocial  = 2;
  coNomeFantasia = 3;
  coCpfCNPJ      = 4;
  coCNH          = 5;
  coDataValCNH   = 6;

var
  FSomenteAtivos: Boolean;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
var
  vObj: TObject;
begin
  FSomenteAtivos := pSomenteAtivos;
  vObj := _HerancaPesquisas.Pesquisar(TFormPesquisaMotoristas, _Motoristas.GetFiltros);
  if vObj = nil then
    Result := nil
  else
    Result := RecMotoristas(vObj);
end;

function PesquisarVarios(pSomenteAtivos: Boolean): TArray<TObject>;
var
  vObj: TArray<TObject>;
begin
  FSomenteAtivos := pSomenteAtivos;
  vObj := _HerancaPesquisas.PesquisarVarios(TFormPesquisaMotoristas, _Motoristas.GetFiltros);
  if vObj = nil then
    Result := nil
  else
    Result := TArray<TObject>(vObj);
end;

procedure TFormPesquisaMotoristas.BuscarRegistros;
var
  i: Integer;
  vDados: TArray<RecMotoristas>;
begin
  inherited;

  vDados :=
    _Motoristas.BuscarMotoristas(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FSomenteAtivos
    );

  if vDados = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vDados);

  for i := Low(vDados) to High(vDados) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := NFormat(vDados[i].CadastroId);
    sgPesquisa.Cells[coNomeFantasia, i + 1] := vDados[i].Cadastro.nome_fantasia;
    sgPesquisa.Cells[coRazaoSocial, i + 1]  := vDados[i].Cadastro.razao_social;
    sgPesquisa.Cells[coCpfCNPJ, i + 1]      := vDados[i].Cadastro.cpf_cnpj;
    sgPesquisa.Cells[coCNH, i + 1]          := vDados[i].NumeroCNH;
    sgPesquisa.Cells[coDataValCNH, i + 1]   := DFormat(vDados[i].DataValidadeCNH);
  end;
  sgPesquisa.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vDados) );
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaMotoristas.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado]) then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
