inherited FormSangriaCaixa: TFormSangriaCaixa
  Caption = 'Retirada de caixa'
  ClientHeight = 266
  ClientWidth = 541
  ExplicitWidth = 547
  ExplicitHeight = 295
  PixelsPerInch = 96
  TextHeight = 14
  object lb11: TLabel [1]
    Left = 131
    Top = 175
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Dinheiro'
  end
  object lb12: TLabel [2]
    Left = 131
    Top = 198
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Cheque'
  end
  object lb13: TLabel [3]
    Left = 131
    Top = 221
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Cart'#227'o'
  end
  object lb14: TLabel [4]
    Left = 131
    Top = 244
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Cobran'#231'a'
  end
  object sbBuscarDadosCheques: TSpeedButton [5]
    Left = 292
    Top = 194
    Width = 18
    Height = 18
    Caption = '...'
    OnClick = sbBuscarDadosChequesClick
  end
  object sbBuscarDadosCartoes: TSpeedButton [6]
    Left = 292
    Top = 217
    Width = 18
    Height = 18
    Caption = '...'
    OnClick = sbBuscarDadosCartoesClick
  end
  object sbBuscarDadosCobranca: TSpeedButton [7]
    Left = 292
    Top = 240
    Width = 18
    Height = 18
    Caption = '...'
    OnClick = sbBuscarDadosCobrancaClick
  end
  object lb1: TLabel [8]
    Left = 319
    Top = 150
    Width = 69
    Height = 14
    Caption = 'Observa'#231#245'es'
  end
  object lb2: TLabel [9]
    Left = 453
    Top = 55
    Width = 30
    Height = 14
    Caption = 'Turno'
  end
  inherited pnOpcoes: TPanel
    Height = 266
    inherited sbDesfazer: TSpeedButton
      Top = 46
      ExplicitTop = 46
    end
    inherited sbExcluir: TSpeedButton
      Top = 89
      ExplicitTop = 89
    end
    inherited sbPesquisar: TSpeedButton
      Top = 166
      Visible = False
      ExplicitTop = 166
    end
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 459
    TabOrder = 9
    Visible = False
    ExplicitLeft = 459
  end
  inline FrCaixaAberto: TFrFuncionarios
    Left = 128
    Top = 54
    Width = 318
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 128
    ExplicitTop = 54
    ExplicitWidth = 318
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 293
      Height = 23
      TabOrder = 1
      ExplicitWidth = 293
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 2
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 4
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 5
    end
    inherited CkMultiSelecao: TCheckBox
      TabOrder = 3
    end
    inherited PnTitulos: TPanel
      Width = 318
      TabOrder = 0
      ExplicitWidth = 318
      inherited lbNomePesquisa: TLabel
        Width = 29
        Caption = 'Caixa'
        ExplicitWidth = 29
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 213
        ExplicitLeft = 213
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 293
      Height = 24
      ExplicitLeft = 293
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited ckCaixa: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eValorDinheiro: TEditLuka
    Left = 193
    Top = 168
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 5
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorCheque: TEditLuka
    Left = 193
    Top = 191
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 6
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorCartao: TEditLuka
    Left = 193
    Top = 214
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 7
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorCobranca: TEditLuka
    Left = 193
    Top = 237
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 8
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object stValoresSangria: TStaticText
    Left = 128
    Top = 146
    Width = 182
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = 'Valores de sangria'
    Color = 15790320
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Transparent = False
  end
  object eObservacoes: TMemo
    Left = 319
    Top = 165
    Width = 214
    Height = 93
    TabOrder = 11
  end
  object eTurnoId: TEditLuka
    Left = 453
    Top = 70
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 4
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrContaDestino: TFrContas
    Left = 126
    Top = 99
    Width = 320
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 99
    ExplicitWidth = 320
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 295
      Height = 23
      ExplicitWidth = 295
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 320
      ExplicitWidth = 320
      inherited lbNomePesquisa: TLabel
        Width = 75
        Caption = 'Conta destino'
        ExplicitWidth = 75
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 215
        ExplicitLeft = 215
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 295
      Height = 24
      ExplicitLeft = 295
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited rgTipo: TRadioGroupLuka
      ItemIndex = 0
    end
  end
end
