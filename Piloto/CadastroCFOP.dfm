inherited FormCadastroCFOP: TFormCadastroCFOP
  Caption = 'Cadastro de CFOP'
  ClientHeight = 295
  ClientWidth = 666
  ExplicitWidth = 672
  ExplicitHeight = 324
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 226
    Top = 3
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  inherited Label1: TLabel
    Top = 3
    Width = 66
    Caption = 'C'#243'digo CFOP'
    ExplicitTop = 3
    ExplicitWidth = 66
  end
  inherited pnOpcoes: TPanel
    Height = 295
    ExplicitHeight = 295
  end
  inherited eID: TEditLuka
    Top = 17
    Width = 96
    Alignment = taLeftJustify
    MaxLength = 7
    TabOrder = 2
    TipoCampo = tcTexto
    ExplicitTop = 17
    ExplicitWidth = 96
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 623
    Top = 0
    Width = 45
    TabOrder = 3
    ExplicitLeft = 623
    ExplicitTop = 0
    ExplicitWidth = 45
  end
  object eNome: TEditLuka
    Left = 226
    Top = 17
    Width = 437
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 60
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object pcParametros: TPageControl
    Left = 124
    Top = 41
    Width = 541
    Height = 253
    ActivePage = tsPrincipais
    TabOrder = 4
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      object lb2: TLabel
        Left = 3
        Top = -2
        Width = 53
        Height = 14
        Caption = 'Descri'#231#227'o'
      end
      object eDescricaoCFOP: TMemo
        Left = 0
        Top = 11
        Width = 533
        Height = 213
        Align = alBottom
        MaxLength = 400
        TabOrder = 0
        OnKeyDown = ProximoCampo
      end
    end
    object tsParametrosCompra: TTabSheet
      Caption = 'Par'#226'metros de compra'
      ImageIndex = 1
      object GroupBoxLuka1: TGroupBoxLuka
        Left = 2
        Top = 5
        Width = 203
        Height = 85
        TabOrder = 0
        OpcaoMarcarDesmarcar = False
        object ckEntradaMercBonificacao: TCheckBoxLuka
          Left = 25
          Top = 46
          Width = 171
          Height = 17
          Caption = 'Bonifica'#231#227'o'
          TabOrder = 0
          CheckedStr = 'N'
        end
        object ckEntradaMercUsoConsumo: TCheckBoxLuka
          Left = 25
          Top = 29
          Width = 171
          Height = 17
          Caption = 'Uso e consumo'
          TabOrder = 1
          CheckedStr = 'N'
        end
        object ckEntradaMercRevenda: TCheckBoxLuka
          Left = 25
          Top = 12
          Width = 171
          Height = 17
          Caption = 'Revenda'
          TabOrder = 2
          CheckedStr = 'N'
        end
        object ckNaoExigirItensEntrada: TCheckBoxLuka
          Left = 25
          Top = 63
          Width = 175
          Height = 17
          Caption = 'N'#227'o exigir itens na entrada'
          TabOrder = 3
          CheckedStr = 'N'
        end
      end
      object ckEntradaConhecimentoFrete: TCheckBoxLuka
        Left = 211
        Top = 17
        Width = 163
        Height = 17
        Caption = '3 - Conhecimento de frete'
        TabOrder = 1
        OnClick = ckEntradaConhecimentoFreteClick
        CheckedStr = 'N'
      end
      inline FrCFOPEntrada: TFrCFOPs
        Left = 2
        Top = 97
        Width = 266
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 97
        ExplicitWidth = 266
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 241
          Height = 23
          ExplicitWidth = 241
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 266
          ExplicitWidth = 266
          inherited lbNomePesquisa: TLabel
            Width = 100
            Caption = 'CFOP para entrada'
            ExplicitWidth = 100
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 161
            ExplicitLeft = 161
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited cbTipoCFOPs: TComboBoxLuka
          Height = 22
          ItemIndex = 0
          Text = 'Entrada'
          ExplicitHeight = 22
        end
        inherited pnPesquisa: TPanel
          Left = 241
          Height = 24
          ExplicitLeft = 241
          ExplicitHeight = 24
        end
      end
      inline FrPlanoFinanceiroEntrada: TFrPlanosFinanceiros
        Left = 269
        Top = 97
        Width = 262
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 269
        ExplicitTop = 97
        ExplicitWidth = 262
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 237
          Height = 23
          ExplicitWidth = 237
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 262
          ExplicitWidth = 262
          inherited lbNomePesquisa: TLabel
            Width = 162
            Caption = 'Plano financeiro para entrada'
            ExplicitWidth = 162
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 157
            ExplicitLeft = 157
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 237
          Height = 24
          ExplicitLeft = 237
          ExplicitHeight = 24
        end
      end
      object ckEntradaServicos: TCheckBoxLuka
        Left = 211
        Top = 2
        Width = 154
        Height = 17
        Caption = '2 - Entrada de servi'#231'os'
        TabOrder = 4
        CheckedStr = 'N'
      end
      object ckEntradaMercadorias: TCheckBoxLuka
        Left = 14
        Top = -2
        Width = 171
        Height = 17
        Caption = ' 1 - Entrada de mercadorias '
        TabOrder = 5
        OnClick = ckEntradaMercadoriasClick
        CheckedStr = 'N'
      end
    end
    object tsFiscal: TTabSheet
      Caption = 'Fiscal'
      ImageIndex = 2
      inline FrParametrosPorEmpresa: TFrCfopParametrosFiscaisEmpresas
        Left = 0
        Top = 0
        Width = 533
        Height = 224
        Align = alClient
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 533
        ExplicitHeight = 224
        inherited st1: TStaticTextLuka
          Width = 533
          ExplicitWidth = 533
        end
        inherited sgParametros: TGridLuka
          Width = 533
          Height = 208
          ExplicitWidth = 533
          ExplicitHeight = 208
          ColWidths = (
            51
            219
            122)
        end
      end
    end
  end
end
