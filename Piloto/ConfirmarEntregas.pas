unit ConfirmarEntregas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, _Biblioteca, _RecordsExpedicao, System.Math,
  FrameDataInicialFinal, Vcl.Menus, Vcl.Grids, GridLuka, Vcl.StdCtrls, EditLuka, _Sessao, _Entregas,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes, Vcl.Buttons, _EntregasItens,
  Vcl.ExtCtrls, Informacoes.Orcamento, Buscar.PessoaRecebimentoMercadorias, _RecordsEspeciais,
  StaticTextLuka;

type
  TFormConfirmarEntregas = class(TFormHerancaRelatorios)
    lb1: TLabel;
    FrCliente: TFrClientes;
    eOrcamentoId: TEditLuka;
    sgEntregas: TGridLuka;
    sgItens: TGridLuka;
    pmEntregas: TPopupMenu;
    miConfirmarRetirada: TMenuItem;
    FrDataInicialFinal: TFrDataInicialFinal;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    miCancelarEntrega: TMenuItem;
    miReemitirComprovanteEntrega: TMenuItem;
    procedure sgEntregasClick(Sender: TObject);
    procedure sgEntregasDblClick(Sender: TObject);
    procedure sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure miConfirmarRetiradaClick(Sender: TObject);
    procedure miCancelarEntregaClick(Sender: TObject);
  private
    FItens: TArray<RecEntregaItem>;

    procedure FrClienteAposPesquisar(Sender: TObject);
    procedure FrClienteAposDeletar(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  coEntregaId            = 0;
  coOrcamentoId          = 1;
  coCliente              = 2;
  coDataCadastro         = 3;
  coVendedor             = 4;
  coUsuarioGerouEntrega  = 5;

  ciProdutoId  = 0;
  ciNome       = 1;
  ciMarca      = 2;
  ciQuantidade = 3;
  ciUnidade    = 4;

{ TFormConfirmarEntregas }

procedure TFormConfirmarEntregas.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vEntregasIds: TArray<Integer>;
  vEntregas: TArray<RecEntrega>;
begin
  inherited;
  sgEntregas.ClearGrid;
  vEntregasIds := nil;
  sgItens.ClearGrid;

  vSql := ' where ENT.STATUS = ''AGC'' ';

  if not FrCliente.EstaVazio then
    vSql := vSql + ' and ' + FrCliente.TrazerFiltros('ORC.CLIENTE_ID')
  else if eOrcamentoId.AsInt > 0 then
    vSql := vSql + ' and ORC.ORCAMENTO_ID = ' + eOrcamentoId.Text
  else
    vSql := vSql + ' and ' + FrDataInicialFinal.getSQLData('trunc(ENT.DATA_HORA_CADASTRO)');

  vEntregas := _Entregas.BuscarEntregasComando(Sessao.getConexaoBanco, vSql);
  if vEntregas = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vEntregas) to High(vEntregas) do begin
    sgEntregas.Cells[coOrcamentoId, i + 1]         := NFormat(vEntregas[i].OrcamentoId);
    sgEntregas.Cells[coCliente, i + 1]             := NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;
    sgEntregas.Cells[coDataCadastro, i + 1]        := DFormat(vEntregas[i].DataHoraCadastro);
    sgEntregas.Cells[coVendedor, i + 1]            := NFormat(vEntregas[i].VendedorId) + ' - ' + vEntregas[i].NomeVendedor;
    sgEntregas.Cells[coUsuarioGerouEntrega, i + 1] := NFormatN(vEntregas[i].UsuarioCadastroId) + ' - ' + vEntregas[i].NomeUsuarioCadastro;
    sgEntregas.Cells[coEntregaId, i + 1]           := NFormat(vEntregas[i].EntregaId);

    _Biblioteca.AddNoVetorSemRepetir(vEntregasIds, vEntregas[i].EntregaId);
  end;
  sgEntregas.RowCount := IfThen(Length(vEntregasIds) > 1, High(vEntregasIds) + 2, 2);

  FItens := _EntregasItens.BuscarEntregaItensComando(Sessao.getConexaoBanco, 'where ' + _Biblioteca.FiltroInInt('ITE.ENTREGA_ID', vEntregasIds) + ' order by PRO.NOME ');
  sgEntregasClick(Sender);
end;

procedure TFormConfirmarEntregas.FormCreate(Sender: TObject);
begin
  inherited;
  FrCliente.OnAposPesquisar := FrClienteAposPesquisar;
  FrCliente.OnAposDeletar := FrClienteAposDeletar;
end;

procedure TFormConfirmarEntregas.FrClienteAposDeletar(Sender: TObject);
begin
  _Biblioteca.Habilitar([eOrcamentoId], True);
end;

procedure TFormConfirmarEntregas.FrClienteAposPesquisar(Sender: TObject);
begin
  _Biblioteca.Habilitar([eOrcamentoId], False);
end;

procedure TFormConfirmarEntregas.miCancelarEntregaClick(Sender: TObject);
var
  vRetBanco: RecRespostaBanco;
begin
  inherited;
  if not Perguntar('Deseja realmente cancelar a entrega selecionada?') then
    Exit;

  vRetBanco := _Entregas.CancelarEntrega(Sessao.getConexaoBanco, SFormatInt(sgEntregas.Cells[coEntregaId, sgEntregas.Row]));
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  _Biblioteca.Informar('Entrega cancelada com sucesso!');
  Carregar(Sender);
end;

procedure TFormConfirmarEntregas.miConfirmarRetiradaClick(Sender: TObject);
var
  vPessoaRetirada: TRetornoTelaFinalizar<TInfoEntrega>;
  vRetBanco: RecRespostaBanco;
  vEntregaId: Integer;
begin
  inherited;
  vEntregaId := SFormatInt(sgEntregas.Cells[coEntregaId, sgEntregas.Row]);
  if vEntregaId = 0 then begin
    Exclamar('Nenhuma entrega selecionada!');
    Exit;
  end;

  vPessoaRetirada := Buscar.PessoaRecebimentoMercadorias.Buscar;
  if vPessoaRetirada.RetTela = trCancelado then
    Exit;

  vRetBanco :=
    _Entregas.ConfirmarEntrega(
      Sessao.getConexaoBanco,
      vEntregaId,
      vPessoaRetirada.Dados.NomePessoa,
      vPessoaRetirada.Dados.CPFPessoa,
      vPessoaRetirada.Dados.DataHoraEntrega,
      vPessoaRetirada.Dados.Observacoes
    );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  Carregar(Sender);
end;

procedure TFormConfirmarEntregas.sgEntregasClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vEntregaId: Integer;
begin
  inherited;
  vLinha := 0;
  sgItens.ClearGrid;
  vEntregaId := SFormatInt(sgEntregas.Cells[coEntregaId, sgEntregas.Row]);
  for i := Low(FItens) to High(FItens) do begin
    if vEntregaId <> FItens[i].EntregaId then
      Continue;

    Inc(vLinha);

    sgItens.Cells[ciProdutoId, vLinha]  := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]       := FItens[i].NomeProduto;
    sgItens.Cells[ciMarca, vLinha]      := FItens[i].NomeMarca;
    sgItens.Cells[ciQuantidade, vLinha] := NFormatEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciUnidade, vLinha]    := FItens[i].Unidade;
  end;
  sgItens.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);
end;

procedure TFormConfirmarEntregas.sgEntregasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(SFormatInt(sgEntregas.Cells[coOrcamentoId, sgEntregas.Row]));
end;

procedure TFormConfirmarEntregas.sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [coOrcamentoId, coEntregaId] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgEntregas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormConfirmarEntregas.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ciProdutoId, ciQuantidade] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormConfirmarEntregas.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if
    FrCliente.EstaVazio and
    (eOrcamentoId.AsInt = 0) and
    FrDataInicialFinal.NenhumaDataValida
  then begin
    Exclamar('Nenhum filtro foi definido!');
    Abort;
  end;
end;

end.
