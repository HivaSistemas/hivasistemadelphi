unit PercentuaisCustosEmpresa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, Vcl.Buttons, _Biblioteca, _Sessao,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas,
  Vcl.StdCtrls, EditLuka, _PercentuaisCustosEmpresa, _RecordsEspeciais;

type
  TFormPercentuaisCustoEmpresa = class(TFormHerancaCadastro)
    FrEmpresa: TFrEmpresas;
    lb5: TLabel;
    ePercentualPis: TEditLuka;
    ePercentualCOFINS: TEditLuka;
    Label1: TLabel;
    Label2: TLabel;
    ePercentualIRPJ: TEditLuka;
    Label3: TLabel;
    ePercentualCSLL: TEditLuka;
    Label4: TLabel;
    ePercentualFreteVenda: TEditLuka;
    Label5: TLabel;
    ePercentualCustoFixo: TEditLuka;
    Label6: TLabel;
    ePercentualComissao: TEditLuka;
    procedure FormCreate(Sender: TObject);
  private
    procedure FrEmpresaOnAposPesquisar(Sender: TObject);
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormPercentuaisCustoEmpresa }

procedure TFormPercentuaisCustoEmpresa.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresa.OnAposPesquisar := FrEmpresaOnAposPesquisar;
end;

procedure TFormPercentuaisCustoEmpresa.FrEmpresaOnAposPesquisar(Sender: TObject);
var
  vPercentuais: TArray<RecPercentuaisCustosEmpresa>;
begin
  vPercentuais := _PercentuaisCustosEmpresa.BuscarPercentuaisCustos(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId]);

  Modo(True);
  FrEmpresa.Modo(False, False);

  ePercentualPis.AsDouble        := vPercentuais[0].PercentualPis;
  ePercentualCOFINS.AsDouble     := vPercentuais[0].PercentualCofins;
  ePercentualCSLL.AsDouble       := vPercentuais[0].PercentualCSLL;
  ePercentualIRPJ.AsDouble       := vPercentuais[0].PercentualIRPJ;
  ePercentualComissao.AsDouble   := vPercentuais[0].PercentualComissao;
  ePercentualFreteVenda.AsDouble := vPercentuais[0].PercentualFreteVenda;
  ePercentualCustoFixo.AsDouble  := vPercentuais[0].PercentualCustoFixo;
end;

procedure TFormPercentuaisCustoEmpresa.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco :=
    _PercentuaisCustosEmpresa.AtualizarPercentuaisCustos(
      Sessao.getConexaoBanco,
      FrEmpresa.getEmpresa().EmpresaId,
      ePercentualPis.AsDouble,
      ePercentualCOFINS.AsDouble,
      ePercentualCSLL.AsDouble,
      ePercentualIRPJ.AsDouble,
      ePercentualComissao.AsDouble,
      ePercentualFreteVenda.AsDouble,
      ePercentualCustoFixo.AsDouble
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  RotinaSucesso;

  inherited;
end;

procedure TFormPercentuaisCustoEmpresa.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    ePercentualPis,
    ePercentualCOFINS,
    ePercentualIRPJ,
    ePercentualCSLL,
    ePercentualFreteVenda,
    ePercentualComissao,
    ePercentualCustoFixo],
    pEditando
  );

  sbGravar.Enabled := pEditando;
  sbDesfazer.Enabled := pEditando;
  if not pEditando then begin
    FrEmpresa.Modo(True);
    _Biblioteca.SetarFoco(FrEmpresa);
  end
  else
    _Biblioteca.SetarFoco(ePercentualPis);
end;

procedure TFormPercentuaisCustoEmpresa.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
