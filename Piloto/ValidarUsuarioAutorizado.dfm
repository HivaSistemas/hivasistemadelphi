inherited FormValidarUsuarioAutorizado: TFormValidarUsuarioAutorizado
  Caption = 'Validar usu'#225'rio autorizado'
  ClientHeight = 147
  ClientWidth = 297
  ExplicitWidth = 303
  ExplicitHeight = 176
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel [0]
    Left = 8
    Top = 8
    Width = 43
    Height = 14
    Caption = 'Usu'#225'rio'
  end
  object Label2: TLabel [1]
    Left = 118
    Top = 233
    Width = 34
    Height = 14
    Caption = 'Senha'
  end
  object Label3: TLabel [2]
    Left = 8
    Top = 58
    Width = 34
    Height = 14
    Caption = 'Senha'
  end
  inherited pnOpcoes: TPanel
    Top = 110
    Width = 297
    inherited sbFinalizar: TSpeedButton
      Left = 0
      Width = 293
      Height = 33
      Align = alClient
      Caption = '&Confirmar'
      ExplicitLeft = 57
    end
  end
  object eUsuario: TEditLuka
    Left = 8
    Top = 23
    Width = 281
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    OnExit = eUsuarioExit
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eSenha: TEditLuka
    Left = 8
    Top = 72
    Width = 281
    Height = 22
    CharCase = ecUpperCase
    PasswordChar = '*'
    TabOrder = 2
    OnKeyDown = eSenhaKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
