unit Relacao.MovimentacoesBancosCaixas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, Vcl.ComCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Biblioteca, _Sessao, FrameDataInicialFinal, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, Informacoes.Orcamento, _RecordsCaixa, Informacoes.OperacoesCaixa, Vcl.StdCtrls,
  EditLuka, Frame.Inteiros, FrameContas, _MovimentosContas, CheckBoxLuka, _Imagens, LancamentoManualCaixaBanco,
  ComboBoxLuka, Vcl.Menus, _RecordsEspeciais, Informacoes.ContasReceberBaixa, InformacoesContasPagarBaixa,
  StaticTextLuka, ofxreader, FrameDiretorioArquivo, Data.DB, Datasnap.DBClient,
  frxClass, frxDBSet;

type
  TFormRelacaoMovimentacoesBancosCaixas = class(TFormHerancaRelatoriosPageControl)
    FrDataInicialFinal: TFrDataInicialFinal;
    sgMovimentacoes: TGridLuka;
    FrConta: TFrContas;
    lb1: TLabel;
    cbMovimentos: TComboBoxLuka;
    pmOpcoes: TPopupMenu;
    st3: TStaticText;
    st8: TStaticText;
    sgArquivoOFX: TGridLuka;
    st1: TStaticTextLuka;
    splArquivoOFX: TSplitter;
    sbArquivoOFX: TSpeedButton;
    pnArquivoOFX: TPanel;
    FrCaminhoArquivoOFX: TFrCaminhoArquivo;
    stSaldoAtual: TStaticTextLuka;
    stSaldoAtualConciliado: TStaticTextLuka;
    pmArquivoOFX: TPopupMenu;
    miLancarTransferencia: TMenuItem;
    miN3: TMenuItem;
    miLancarTarifa: TMenuItem;
    pnDiferencas: TPanel;
    st5: TStaticText;
    st6: TStaticText;
    stQtdeMovAltis: TStaticTextLuka;
    stQtdeMovArquivo: TStaticTextLuka;
    st11: TStaticText;
    stQtdeMovConcAltis: TStaticTextLuka;
    stQtdeMovConcArquivo: TStaticTextLuka;
    st15: TStaticText;
    pnl1: TPanel;
    StaticText1: TStaticText;
    stTotalDebitos: TStaticTextLuka;
    StaticText2: TStaticText;
    stTotalCreditos: TStaticTextLuka;
    pnl2: TPanel;
    miConciliarMovimentosSelecionados: TSpeedButton;
    miDesconciliarMovimentosSelecionados: TSpeedButton;
    frxReport: TfrxReport;
    dstMovimentos: TfrxDBDataset;
    cdsMovimentos: TClientDataSet;
    cdsMovimentosMovimento: TStringField;
    cdsMovimentosOperacao: TStringField;
    cdsMovimentosConciliado: TStringField;
    cdsMovimentosDataMovimento: TDateField;
    cdsMovimentosFuncionario: TStringField;
    cdsMovimentosValorMovimento: TFloatField;
    cdsMovimentosAcumulado: TFloatField;
    procedure sgMovimentacoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgMovimentacoesDblClick(Sender: TObject);
    procedure sgMovimentacoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sgMovimentacoesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgMovimentacoesGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure miConciliarMovimentosSelecionadosClick(Sender: TObject);
    procedure miDesconciliarMovimentosSelecionadosClick(Sender: TObject);
    procedure miMarcarDesmarcarTItuloFocadoClick(Sender: TObject);
    procedure miMarcarDesmarcarTodosTitulosClick(Sender: TObject);
    procedure sgArquivoOFXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgArquivoOFXGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgMovimentacoesMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure sgArquivoOFXGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgArquivoOFXDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure sgArquivoOFXDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure sbArquivoOFXClick(Sender: TObject);
    procedure miLancarTransferenciaClick(Sender: TObject);
    procedure miLancarTarifaClick(Sender: TObject);
    procedure sbCarregarClick(Sender: TObject);
  private
    procedure conciliarDesconciliar(pConciliar: Boolean);

    procedure CarregarViaArquivo;

    procedure FrContaOnAposPesquisar(Sender: TObject);
    procedure FrContaOnAposDeletar(Sender: TObject);
  protected
    procedure Carregar(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TRelacaoMovimentacoesBancosCaixas }

const
  coSelecionado         = 0;
  coId                  = 1;
  coTipoMovimentoAna    = 2;  // Anal�tico
  coValor               = 3;
  coValorAcumulado      = 4;
  coDataHoraMovimento   = 5;
  coFuncionario         = 6;
  coConciliado          = 7;
  coValorAcumuladoConc  = 8;
  coDataHoraMovAcumConc = 9;
  coUsuarioConciliacao  = 10;

  coTipoMovimentoSint  = 11;
  coNatureza           = 13;
  coMovimentoId        = 14;
  coMovimentoIdBanco   = 15;

  (* Grid do arquivo de concilia��o *)
  cofxIdentificado     = 0;
  cofxMovimentoAltisId = 1;
  cofxDataMovimento    = 2;
  cofxValorMovimento   = 3;
  cofxOperacao         = 4;
  cofxNatureza         = 5;
  cofxConciliado       = 6;
  cofxValorAcumulado   = 7;
  cofxMovimentoIdBanco = 8;

procedure TFormRelacaoMovimentacoesBancosCaixas.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vMovimentos: TArray<RecMovimentosContas>;

  vLinha: Integer;
  vValorTotal: Currency;
  vValorTotalConc: Currency;

  vQtdeConciliados: Integer;
begin
  vSql := '';
  vLinha := 1;
  vQtdeConciliados := 0;

  _Biblioteca.LimparCampos([sgMovimentacoes, stTotalDebitos, stTotalCreditos]);

  _Biblioteca.WhereOuAnd(vSql, FrConta.getSqlFiltros('MOV.CONTA_ID'));
  _Biblioteca.WhereOuAnd(vSql, FrDataInicialFinal.getSqlFiltros('trunc(MOV.DATA_HORA_MOVIMENTO)'));

  vSql := vSql + ' order by MOV.DATA_HORA_MOVIMENTO, MOV.MOVIMENTO_ID ';
  vMovimentos := _MovimentosContas.BuscarMovimentosContas(Sessao.getConexaoBanco, vSql);

  if vMovimentos = nil then begin
    NenhumRegistro;
    Abort;
  end;

  vValorTotal     := _MovimentosContas.BuscarSaldoInicialConta(Sessao.getConexaoBanco, FrConta.GetConta().Conta, FrDataInicialFinal.eDataInicial.AsData, False);
  vValorTotalConc := _MovimentosContas.BuscarSaldoInicialConta(Sessao.getConexaoBanco, FrConta.GetConta().Conta, FrDataInicialFinal.eDataInicial.AsData, True);

  sgMovimentacoes.Cells[coId, vLinha]                 := 'Inicial';
  sgMovimentacoes.Cells[coValorAcumulado, vLinha]     := NFormat(vValorTotal);
  sgMovimentacoes.Cells[coValorAcumuladoConc, vLinha] := NFormat(vValorTotalConc);

  for i := Low(vMovimentos) to High(vMovimentos) do begin
    Inc(vLinha);

    sgMovimentacoes.Cells[coSelecionado, vLinha]       := _Biblioteca.charNaoSelecionado;

    sgMovimentacoes.Cells[coId, vLinha] :=
      NFormat(
        Integer(_Biblioteca.Decode(
          vMovimentos[i].TipoMovimento, [
            'BXR',
            vMovimentos[i].BaixaReceberId,
             'PIX',
            vMovimentos[i].BaixaReceberId,
            'BXP',
            vMovimentos[i].BaixaPagarId,
            vMovimentos[i].MovimentoId
          ]
        ))
      );

    sgMovimentacoes.Cells[coTipoMovimentoAna, vLinha]  := vMovimentos[i].TipoMovimentoAnalitico;
    sgMovimentacoes.Cells[coTipoMovimentoSint, vLinha] := vMovimentos[i].TipoMovimento;
    sgMovimentacoes.Cells[coValor, vLinha]             := NFormatN(vMovimentos[i].ValorMovimento);
    sgMovimentacoes.Cells[coDataHoraMovimento, vLinha] := DHFormat(vMovimentos[i].DataHoraMovimento);
    sgMovimentacoes.Cells[coFuncionario, vLinha]       := NFormat(vMovimentos[i].UsuarioMovimentoId) + ' - ' + vMovimentos[i].NomeUsuario;

    if vMovimentos[i].Natureza = 'E' then begin
      vValorTotal     := vValorTotal + vMovimentos[i].ValorMovimento;
      vValorTotalConc := vValorTotalConc + IIfDbl(vMovimentos[i].Conciliado = 'S', vMovimentos[i].ValorMovimento);
      stTotalCreditos.Somar(vMovimentos[i].ValorMovimento);
    end
    else begin
      vValorTotal := vValorTotal - vMovimentos[i].ValorMovimento;
      vValorTotalConc := vValorTotalConc - IIfDbl(vMovimentos[i].Conciliado = 'S', vMovimentos[i].ValorMovimento);
      stTotalDebitos.Somar(vMovimentos[i].ValorMovimento);
    end;

    if vMovimentos[i].Conciliado = 'S' then
      Inc(vQtdeConciliados);

    sgMovimentacoes.Cells[coValorAcumulado, vLinha]     := NFormat(vValorTotal);
    sgMovimentacoes.Cells[coValorAcumuladoConc, vLinha] := NFormat(vValorTotalConc);

    sgMovimentacoes.Cells[coConciliado, vLinha]          := _Biblioteca.SimNao(vMovimentos[i].Conciliado);
    sgMovimentacoes.Cells[coDataHoraMovAcumConc, vLinha] := _Biblioteca.DHFormatN(vMovimentos[i].DataHoraConciliacao);
    sgMovimentacoes.Cells[coUsuarioConciliacao, vLinha]  := getInformacao(vMovimentos[i].UsuarioConciliacaoId, vMovimentos[i].NomeUsuarioConciliacao);

    sgMovimentacoes.Cells[coNatureza, vLinha]          := vMovimentos[i].Natureza;
    sgMovimentacoes.Cells[coMovimentoId, vLinha]       := NFormatN(vMovimentos[i].MovimentoId);
    sgMovimentacoes.Cells[coMovimentoIdBanco, vLinha]  := vMovimentos[i].MovimentoIdBanco;
  end;
  sgMovimentacoes.RowCount := vLinha + 1;

  stQtdeMovAltis.AsInt     := Length(vMovimentos);
  stQtdeMovConcAltis.AsInt := vQtdeConciliados;

  SetarFoco(sgMovimentacoes);
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.conciliarDesconciliar(pConciliar: Boolean);
var
  i: Integer;

  vConta: string;
  vRetBanco: RecRetornoBD;
  vMovIdsPagar: TArray<Integer>;
  vMovIdsReceber: TArray<Integer>;
begin
  vMovIdsReceber := nil;
  vMovIdsPagar   := nil;

  if pConciliar and (not Sessao.AutorizadoRotina('conciliar_movimento_conta')) then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end
  else if (not pConciliar) and (not Sessao.AutorizadoRotina('desconciliar_movimento_conta')) then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  for i := 2 to sgMovimentacoes.RowCount -1 do begin
    if sgMovimentacoes.Cells[coSelecionado, i] <> _Biblioteca.charSelecionado then
      Continue;

    if sgMovimentacoes.Cells[coNatureza, i] = 'E' then
      _Biblioteca.AddNoVetorSemRepetir(vMovIdsReceber, SFormatInt( sgMovimentacoes.Cells[coMovimentoId, i]))
    else
      _Biblioteca.AddNoVetorSemRepetir(vMovIdsPagar, SFormatInt( sgMovimentacoes.Cells[coMovimentoId, i]));
  end;

  if vMovIdsReceber + vMovIdsPagar = nil then begin
    _Biblioteca.NenhumRegistroSelecionado;
    Exit;
  end;

  vRetBanco :=
    _MovimentosContas.AtualizarMovimentosConciliacao(
      Sessao.getConexaoBanco,
      pConciliar,
      vMovIdsReceber,
      vMovIdsPagar
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RotinaSucesso;
  Carregar(nil);

  vConta := FrConta.GetConta().Conta;
  FrConta.Clear;
  FrConta.InserirDadoPorChave(vConta, True);

  SetarFoco(sgMovimentacoes);
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.FormCreate(Sender: TObject);
begin
  inherited;
  FrDataInicialFinal.eDataInicial.AsData := Now;
  FrDataInicialFinal.eDataFinal.AsData := Now;

  FrConta.OnAposPesquisar := FrContaOnAposPesquisar;
  FrConta.OnAposDeletar   := FrContaOnAposDeletar;
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrConta);
  sbArquivoOFXClick(Sender);
//  FrCaminhoArquivoOFX.ExtensaoArquivo:
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.CarregarViaArquivo;
var
  i: Integer;
  j: Integer;
  vOFX: TOFXReader;
  vCaminhoArquivo: string;

  vIdsBanco: TArray<string>;
  vSaldoAcumulado: Currency;
begin
  inherited;
  sgArquivoOFX.ClearGrid;

  vCaminhoArquivo := FrCaminhoArquivoOFX.getCaminhoArquivo;
  if vCaminhoArquivo = '' then
    Exit;

  vIdsBanco := nil;
  vOFX := TOFXReader.Create(Self);

  try
    vOFX.OFXFile := vCaminhoArquivo;

    if not vOFX.Import then
      Exit;

    FrConta.InserirDadoPorChave( vOFX.AccountID, True );
    if FrConta.EstaVazio then begin
      _Biblioteca.Exclamar('A conta do arquivo selecionado n�o foi encontrada no Hiva, por favor verifique!' + sLineBreak + 'Conta arq: ' + vOFX.AccountID);
      FrCaminhoArquivoOFX.Clear;
      Exit;
    end;

    FrContaOnAposPesquisar(nil);
    FrDataInicialFinal.eDataInicial.AsData := vOFX.DataInicial;
    FrDataInicialFinal.eDataFinal.AsData   := vOFX.DataFinal;

    Carregar(nil);
    _Biblioteca.Visibilidade([pnArquivoOFX, splArquivoOFX], True);

    vSaldoAcumulado := vOFX.SaldoInicial;

    for i := 0 to vOFX.Count -1 do begin
      vSaldoAcumulado := vSaldoAcumulado + vOFX.Get(i).Value * IIfDbl(vOFX.Get(i).MovType = 'C', 1, -1);

      sgArquivoOFX.Cells[cofxIdentificado, i + 1]     := _Biblioteca.charNaoSelecionado;
      sgArquivoOFX.Cells[cofxDataMovimento, i + 1]    := DFormat(vOFX.Get(i).MovDate);
      sgArquivoOFX.Cells[cofxValorMovimento, i + 1]   := NFormatN(vOFX.Get(i).Value);
      sgArquivoOFX.Cells[cofxOperacao, i + 1]         := vOFX.Get(i).Description;
      sgArquivoOFX.Cells[cofxConciliado, i + 1]       := 'N�o';
      sgArquivoOFX.Cells[cofxValorAcumulado, i + 1]   := NFormat(vSaldoAcumulado);
      sgArquivoOFX.Cells[cofxNatureza, i + 1]         := IIfStr(vOFX.Get(i).MovType = 'C', 'Entrada', 'Sa�da');
      sgArquivoOFX.Cells[cofxMovimentoIdBanco, i + 1] := vOFX.Get(i).ID;

      _Biblioteca.AddNoVetorSemRepetir(vIdsBanco, vOFX.Get(i).ID);

      for j := 2 to sgMovimentacoes.RowCount -1 do begin
        if
          (vOFX.Get(i).MovDate <> ToData(sgMovimentacoes.Cells[coDataHoraMovimento, j])) or
          (vOFX.Get(i).Value <> SFormatCurr(sgMovimentacoes.Cells[coValor, j])) or
          (IIfStr(vOFX.Get(i).MovType = 'C', 'E', 'S') <> sgMovimentacoes.Cells[coNatureza, j]) or
          (sgMovimentacoes.Cells[coSelecionado, j] = _Biblioteca.charSelecionado)
        then
          Continue;

        sgArquivoOFX.Cells[cofxIdentificado, i + 1]     := _Biblioteca.charSelecionado;
        sgArquivoOFX.Cells[cofxMovimentoAltisId, i + 1] := sgMovimentacoes.Cells[coId, j];

        sgMovimentacoes.Cells[coSelecionado, j] := _Biblioteca.charSelecionado;
        Break;
      end;
    end;
    sgArquivoOFX.SetLinhasGridPorTamanhoVetor( vOFX.Count );

    stQtdeMovArquivo.AsInt := vOFX.Count;
  finally
    vOfx.Free;
  end;
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.FrContaOnAposDeletar(Sender: TObject);
begin
  stSaldoAtual.AsCurr           := 0;
  stSaldoAtualConciliado.AsCurr := 0;
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.FrContaOnAposPesquisar(Sender: TObject);
begin
  stSaldoAtual.AsCurr           := FrConta.GetConta.SaldoAtual;
  stSaldoAtualConciliado.AsCurr := FrConta.GetConta.SaldoConciliadoAtual;
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.Imprimir(Sender: TObject);
var
  vCnt: Integer;
  //totalPagar: Double;
begin
  cdsMovimentos.Close;
  cdsMovimentos.CreateDataSet;
  cdsMovimentos.Open;
  inherited;

  for vCnt := sgMovimentacoes.RowCount - 1 downto 1 do
  begin
    cdsMovimentos.Insert;
    cdsMovimentosMovimento.AsString       := sgMovimentacoes.Cells[coId,vCnt];
    cdsMovimentosOperacao.AsString        := sgMovimentacoes.Cells[coTipoMovimentoAna,vCnt];
    cdsMovimentosValorMovimento.AsFloat   := SFormatDouble(sgMovimentacoes.Cells[coValor,vCnt]);
    cdsMovimentosAcumulado.AsFloat        := SFormatDouble(sgMovimentacoes.Cells[coValorAcumulado,vCnt]);
    cdsMovimentosConciliado.AsString      := sgMovimentacoes.Cells[coConciliado,vCnt];
    cdsMovimentosFuncionario.AsString     := sgMovimentacoes.Cells[coFuncionario,vCnt];
    cdsMovimentosDataMovimento.AsDateTime := ToData(sgMovimentacoes.Cells[coDataHoraMovimento,vCnt]);
    cdsMovimentos.Post;
  end;

  //TfrxMemoView(frxReport.FindComponent('mmValorProduto')).Text :=   'Valor Total.....: ' + NFormat(vTotalEntradas);
  frxReport.ShowReport;
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.miConciliarMovimentosSelecionadosClick(Sender: TObject);
begin
  inherited;
  conciliarDesconciliar(True);
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.miDesconciliarMovimentosSelecionadosClick(Sender: TObject);
begin
  inherited;
  conciliarDesconciliar(False);
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.miLancarTarifaClick(Sender: TObject);
begin
  inherited;
  LancamentoManualCaixaBanco.LancarTarifaTransferencia(
    SFormatCurr(sgArquivoOFX.Cells[cofxValorMovimento, sgArquivoOFX.Row]),
    ToData(sgArquivoOFX.Cells[cofxDataMovimento, sgArquivoOFX.Row]),
    FrConta.GetConta().Conta,
    False,
    True
  );
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.miLancarTransferenciaClick(Sender: TObject);
begin
  inherited;
  LancamentoManualCaixaBanco.LancarTarifaTransferencia(
    SFormatCurr(sgArquivoOFX.Cells[cofxValorMovimento, sgArquivoOFX.Row]),
    ToData(sgArquivoOFX.Cells[cofxDataMovimento, sgArquivoOFX.Row]),
    FrConta.GetConta().Conta,
    sgArquivoOFX.Cells[cofxNatureza, sgArquivoOFX.Row] = 'Entrada',
    False
  );
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.miMarcarDesmarcarTItuloFocadoClick(Sender: TObject);
begin
  inherited;
  MarcarDesmarcarTodos(sgMovimentacoes, coSelecionado, VK_SPACE, []);
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.miMarcarDesmarcarTodosTitulosClick(Sender: TObject);
begin
  inherited;
  MarcarDesmarcarTodos(sgMovimentacoes, coSelecionado, VK_SPACE, [ssCtrl]);
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sbArquivoOFXClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Visibilidade([pnArquivoOFX, splArquivoOFX], not pnArquivoOFX.Visible);
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sbCarregarClick(Sender: TObject);
begin
//  inherited; Peiii na heran�a
  if FrCaminhoArquivoOFX.EstaVazio then begin
    VerificarRegistro(Sender);
    Carregar(Sender);
  end
  else
    CarregarViaArquivo
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sgArquivoOFXDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  vLinhaAltis: Integer;
  vLinhaArquivo: Integer;

  vCoordGrid: TGridCoord;
begin
  inherited;

  if Source <> sgMovimentacoes then
    Exit;

  vCoordGrid := sgArquivoOFX.MouseCoord(X, Y);

  vLinhaAltis := sgMovimentacoes.Row;
  vLinhaArquivo := vCoordGrid.Y;

  if sgArquivoOFX.Cells[cofxIdentificado, vLinhaArquivo] = _Biblioteca.charSelecionado then begin
    _Biblioteca.Exclamar('O registro de destino j� foi identificado, a altera��o n�o ser� permitida, por favor verifique!');
    Exit;
  end;

  if sgMovimentacoes.Cells[coNatureza, vLinhaAltis] <> Copy(sgArquivoOFX.Cells[cofxNatureza, vLinhaArquivo], 1, 1) then begin
    _Biblioteca.Exclamar('A natureza do registro selecionado no Hiva � diferente da natureza do registro destino, por favor verifique!');
    Exit;
  end;

  if ToData(sgMovimentacoes.Cells[coDatahoraMovimento, vLinhaAltis]) <> ToData(sgArquivoOFX.Cells[cofxDataMovimento, vLinhaArquivo]) then begin
    _Biblioteca.Exclamar('A data de movimento do registro selecionado no Hiva � diferente da data de movimento do registro destino, por favor verifique!');
    Exit;
  end;

  if SFormatCurr(sgMovimentacoes.Cells[coValor, vLinhaAltis]) <> SFormatCurr(sgArquivoOFX.Cells[cofxValorMovimento, vLinhaArquivo]) then begin
    _Biblioteca.Exclamar('O valor de movimento do registro selecionado no Hiva � diferente do valor de movimento do registro destino, por favor verifique!');
    Exit;
  end;

  sgArquivoOFX.Cells[cofxIdentificado, vLinhaArquivo]      := _Biblioteca.charSelecionado;
  sgArquivoOFX.Cells[cofxMovimentoAltisId, vLinhaArquivo]  := sgMovimentacoes.Cells[coId, vLinhaAltis];
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sgArquivoOFXDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
  vCordenadas: TGridCoord;
begin
  inherited;

  vCordenadas := (Sender as TGridLuka).MouseCoord(X, Y);

  Accept :=
    (Source is TGridLuka) and
    (vCordenadas.X > (Sender as TGridLuka).FixedCols - 1) and
    (vCordenadas.Y > (Sender as TGridLuka).FixedRows - 1) and
    ((TGridLuka(Sender).Cells[cofxIdentificado, vCordenadas.Y] <> ''));
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sgArquivoOFXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[cofxMovimentoAltisId, cofxValorMovimento, cofxValorAcumulado] then
    vAlinhamento := taRightJustify
  else if ACol in[cofxIdentificado, cofxConciliado, cofxNatureza] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgArquivoOFX.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sgArquivoOFXGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = cofxValorMovimento then
    AFont.Color := IIfInt(sgArquivoOFX.Cells[cofxNatureza, ARow] = 'Entrada', clGreen, clRed)
  else if ACol = cofxNatureza then begin
    AFont.Style := [fsBold];
    AFont.Color := IIfInt(sgArquivoOFX.Cells[cofxNatureza, ARow] = 'Entrada', clGreen, clRed);
  end
  else if ACol = cofxConciliado then begin
    AFont.Style := [fsBold];
    AFont.Color := IIfInt(sgArquivoOFX.Cells[cofxConciliado, ARow] = 'Sim', clGreen, clRed);
  end
  else if ACol = cofxValorAcumulado then
    AFont.Color := IIfInt( SFormatCurr(sgMovimentacoes.Cells[ACol, ARow]) >= 0, clGreen, clRed );
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sgArquivoOFXGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol <> cofxIdentificado) or (sgArquivoOFX.Cells[ACol, ARow] = '') then
   Exit;

  if sgArquivoOFX.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sgMovimentacoesDblClick(Sender: TObject);
begin
  inherited;
  if sgMovimentacoes.Cells[coTipoMovimentoSint, sgMovimentacoes.Row] = 'REV' then
    Informacoes.Orcamento.Informar(SFormatInt(sgMovimentacoes.Cells[coId, sgMovimentacoes.Row]))
  else if  sgMovimentacoes.Cells[coTipoMovimentoSint, sgMovimentacoes.Row] = 'BXR' then
    Informacoes.ContasReceberBaixa.Informar(SFormatInt(sgMovimentacoes.Cells[coId, sgMovimentacoes.Row]))
  else if  sgMovimentacoes.Cells[coTipoMovimentoSint, sgMovimentacoes.Row] = 'BXP' then
    InformacoesContasPagarBaixa.Informar(SFormatInt(sgMovimentacoes.Cells[coId, sgMovimentacoes.Row]))
  else
    Informacoes.OperacoesCaixa.Informar(SFormatInt(sgMovimentacoes.Cells[coId, sgMovimentacoes.Row]));
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sgMovimentacoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if (ACol = coId) and (ARow = 1) then
    vAlinhamento := taLeftJustify
  else if ACol in [
    coId,
    coValor,
    coValorAcumulado,
    coValorAcumuladoConc]
  then
    vAlinhamento := taRightJustify
  else if ACol in[coTipoMovimentoAna, coConciliado, coSelecionado] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgMovimentacoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sgMovimentacoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coTipoMovimentoAna then begin
    AFont.Style := [fsBold];
    AFont.Color := IIfInt(sgMovimentacoes.Cells[coNatureza, ARow] = 'E', clGreen, clRed);
  end
  else if ACol in[coValorAcumulado, coValorAcumuladoConc] then begin
    AFont.Style := [fsBold];
    AFont.Color := IIfInt(SFormatCurr(sgMovimentacoes.Cells[ACol, ARow]) >= 0, clBlue, clRed);
  end
  else if ACol = coConciliado then begin
    AFont.Style := [fsBold];
    AFont.Color := IIfInt(sgMovimentacoes.Cells[coConciliado, ARow] = 'Sim', clGreen, clRed);
  end;
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sgMovimentacoesGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow < 2 then
    Exit;

  if (ACol <> coSelecionado) or (sgMovimentacoes.Cells[ACol, ARow] = '') then
   Exit;

  if sgMovimentacoes.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sgMovimentacoesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  _Biblioteca.MarcarDesmarcarTodos(sgMovimentacoes, coSelecionado, Key, Shift);
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.sgMovimentacoesMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  vLinha: Integer;
  vColuna: Integer;
begin
  inherited;
  if Button = mbRight then
    Exit;

  sgMovimentacoes.MouseToCell(X, Y, vColuna, vLinha);

  if (vColuna > 0) and (vLinha > 0) and (sgMovimentacoes.Cells[coId, vLinha] <> '') then
    sgMovimentacoes.BeginDrag(False, 4);
end;

procedure TFormRelacaoMovimentacoesBancosCaixas.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrConta.EstaVazio then begin
    Exclamar('A conta n�o foi informada corretamente, verifique!');
    SetarFoco(FrConta);
    Abort;
  end;

  if not FrDataInicialFinal.DatasValidas then begin
    Exclamar('A data inicial e final n�o foram bem definidas, verifique!');
    SetarFoco(FrDataInicialFinal);
    Abort;
  end;
end;

end.
