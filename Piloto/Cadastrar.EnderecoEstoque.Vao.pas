unit Cadastrar.EnderecoEstoque.Vao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca, _RecordsEspeciais, _RecordsCadastros,
  Pesquisa.MotivosAjusteEstoque, _HerancaCadastro, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FramePlanosFinanceiros, CheckBoxLuka, _EnderecoEstoqueVao;

type
  TFormEnderecoEstoqueVao = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
  private
    procedure PreencherRegistro(pModuloId: RecEnderecoEstoqueVao);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses Pesquisa.EnderecoEstoqueVao;

{ TFormEnderecoEstoqueVao }

procedure TFormEnderecoEstoqueVao.BuscarRegistro;
var
  vEnderecoEstoqueVao: TArray<RecEnderecoEstoqueVao>;
begin
  vEnderecoEstoqueVao := _EnderecoEstoqueVao.BuscarEnderecoEstoqueVao(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vEnderecoEstoqueVao = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(vEnderecoEstoqueVao[0]);
end;

procedure TFormEnderecoEstoqueVao.ExcluirRegistro;
var
  vRetorno: RecRetornoBD;
begin
  vRetorno := _EnderecoEstoqueVao.ExcluirEnderecoEstoqueVao(Sessao.getConexaoBanco, eId.AsInt);
  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormEnderecoEstoqueVao.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;
  vRetorno :=
    _EnderecoEstoqueVao.AtualizarEnderecoEstoqueVao(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text
    );

  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetorno.AsInt);
end;

procedure TFormEnderecoEstoqueVao.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eDescricao, ckAtivo], pEditando);

  if pEditando then begin
    SetarFoco(eDescricao);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormEnderecoEstoqueVao.PesquisarRegistro;
var
  vEnderecoEstoqueVao: RecEnderecoEstoqueVao;
begin
  vEnderecoEstoqueVao := RecEnderecoEstoqueVao(Pesquisa.EnderecoEstoqueVao.Pesquisar());  //AquiS
  if vEnderecoEstoqueVao = nil then
    Exit;

  inherited;
  PreencherRegistro(vEnderecoEstoqueVao);
end;

procedure TFormEnderecoEstoqueVao.PreencherRegistro(pModuloId: RecEnderecoEstoqueVao);
begin
  eID.AsInt                     := pModuloId.Vao_id;
  eDescricao.Text               := pModuloId.descricao;
  pModuloId.Free;
end;

procedure TFormEnderecoEstoqueVao.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o n�o foi informado corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;
end;

end.
