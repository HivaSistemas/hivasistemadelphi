inherited FormInformacoesConhecimentoFrete: TFormInformacoesConhecimentoFrete
  Caption = 'Informa'#231#245'es do conhecimento de frete'
  ClientHeight = 353
  ClientWidth = 665
  OnShow = FormShow
  ExplicitWidth = 671
  ExplicitHeight = 382
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 4
    Top = 1
    Width = 37
    Height = 14
    Caption = 'C'#243'digo'
  end
  object lbl1: TLabel [1]
    Left = 88
    Top = 1
    Width = 76
    Height = 14
    Caption = 'Trasportadora'
  end
  object lbl2: TLabel [2]
    Left = 474
    Top = 1
    Width = 43
    Height = 14
    Caption = 'N'#250'mero'
  end
  object lbl3: TLabel [3]
    Left = 595
    Top = 1
    Width = 28
    Height = 14
    Caption = 'S'#233'rie'
  end
  object lbl4: TLabel [4]
    Left = 542
    Top = 1
    Width = 42
    Height = 14
    Caption = 'Modelo'
  end
  object lbl27: TLabel [5]
    Left = 303
    Top = 1
    Width = 47
    Height = 14
    Caption = 'Empresa'
  end
  inherited pnOpcoes: TPanel
    Top = 316
    Width = 665
    TabOrder = 6
    ExplicitTop = 316
    ExplicitWidth = 665
  end
  object eConhecimentoId: TEditLuka
    Left = 4
    Top = 15
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 0
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eTransportadora: TEditLuka
    Left = 88
    Top = 15
    Width = 211
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNumero: TEditLuka
    Left = 474
    Top = 15
    Width = 62
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 3
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eSerie: TEditLuka
    Left = 595
    Top = 15
    Width = 66
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 5
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eModelo: TEditLuka
    Left = 542
    Top = 15
    Width = 49
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 4
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmpresaNota: TEditLuka
    Left = 303
    Top = 15
    Width = 167
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object pcDados: TPageControl
    Left = 4
    Top = 37
    Width = 659
    Height = 278
    ActivePage = tsPrincipais
    TabOrder = 7
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      object lbl14: TLabel
        Left = 1
        Top = 0
        Width = 26
        Height = 14
        Caption = 'CFOP'
      end
      object lbllb3: TLabel
        Left = 300
        Top = 0
        Width = 76
        Height = 14
        Caption = 'Data cadastro'
      end
      object lbl28: TLabel
        Left = 1
        Top = 41
        Width = 56
        Height = 14
        Caption = 'Chave NFe'
      end
      object lbl5: TLabel
        Left = 389
        Top = 0
        Width = 76
        Height = 14
        Caption = 'Data emiss'#227'o'
      end
      object lbl15: TLabel
        Left = 468
        Top = 0
        Width = 56
        Height = 14
        Caption = 'Base ICMS'
      end
      object lbl16: TLabel
        Left = 588
        Top = 0
        Width = 57
        Height = 14
        Caption = 'Valor ICMS'
      end
      object lbl17: TLabel
        Left = 534
        Top = 0
        Width = 38
        Height = 14
        Caption = '% ICMS'
      end
      object lb2: TLabel
        Left = 314
        Top = 41
        Width = 67
        Height = 14
        Caption = 'Valor do frete'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb3: TLabel
        Left = 459
        Top = 41
        Width = 93
        Height = 14
        Caption = 'Usu'#225'rio cadastro'
      end
      object eCFOPNota: TEditLuka
        Left = 1
        Top = 14
        Width = 292
        Height = 22
        CharCase = ecUpperCase
        TabOrder = 0
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataCadastro: TEditLukaData
        Left = 300
        Top = 14
        Width = 83
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 1
        Text = '  /  /    '
      end
      object eChaveAcessoNFe: TMaskEdit
        Left = 1
        Top = 55
        Width = 308
        Height = 22
        EditMask = '9999.9999.9999.9999.9999.9999.9999.9999.9999.9999.9999;1;_'
        MaxLength = 54
        TabOrder = 2
        Text = '    .    .    .    .    .    .    .    .    .    .    '
      end
      object eDataEmissao: TEditLukaData
        Left = 389
        Top = 14
        Width = 75
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 3
        Text = '  /  /    '
      end
      object eValorBaseCalculoICMS: TEditLuka
        Left = 468
        Top = 14
        Width = 62
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 4
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorICMS: TEditLuka
        Left = 588
        Top = 14
        Width = 58
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 5
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object ePercICMS: TEditLuka
        Left = 534
        Top = 14
        Width = 49
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 6
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorTotal: TEditLuka
        Left = 315
        Top = 53
        Width = 141
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object sgNotasConhecimento: TGridLuka
        Left = 0
        Top = 98
        Width = 646
        Height = 151
        Align = alCustom
        ColCount = 8
        Ctl3D = True
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect]
        ParentCtl3D = False
        TabOrder = 8
        OnDrawCell = sgNotasConhecimentoDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Fornecedor'
          'N'#186' da NF'
          'Modelo'
          'S'#233'rie'
          'Data emiss'#227'o'
          'Peso'
          'Qtde.'
          'Valor nota')
        Grid3D = False
        RealColCount = 9
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          219
          53
          63
          47
          83
          53
          52
          61)
      end
      object st1: TStaticTextLuka
        Left = -11
        Top = 81
        Width = 657
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Notas fiscais referenciadas pelo conhecimento de frete'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 9
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object eUsuarioCadastro: TEditLuka
        Left = 459
        Top = 55
        Width = 187
        Height = 22
        CharCase = ecUpperCase
        TabOrder = 10
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
    object tsFinanceiro: TTabSheet
      Caption = 'Financeiro'
      ImageIndex = 1
      OnShow = tsFinanceiroShow
      object sgFinanceiros: TGridLuka
        Left = 0
        Top = 0
        Width = 651
        Height = 249
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        OnDblClick = sgFinanceirosDblClick
        OnDrawCell = sgFinanceirosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'C'#243'digo'
          'Tipo de cobran'#231'a'
          'Valor'
          'Status'
          'Data vencimento')
        Grid3D = False
        RealColCount = 5
        AtivarPopUpSelecao = False
        ColWidths = (
          57
          257
          99
          97
          103)
      end
    end
  end
end
