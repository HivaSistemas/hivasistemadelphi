inherited FormPesquisaPlanosFinanceiros: TFormPesquisaPlanosFinanceiros
  Caption = 'Pesquisa de planos financeiros'
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    ColCount = 6
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o'
      'Tipo'
      'Exibir DRE'
      'Ativo')
    RealColCount = 6
    ExplicitLeft = 0
    ExplicitTop = 46
    ExplicitWidth = 648
    ExplicitHeight = 248
    ColWidths = (
      28
      103
      269
      81
      64
      33)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    inherited eValorPesquisa: TEditLuka
      Width = 439
      ExplicitWidth = 439
    end
  end
end
