inherited FormPesquisaTransportadoras: TFormPesquisaTransportadoras
  Caption = 'Pesquisa de transportadoras'
  ClientHeight = 328
  ClientWidth = 622
  ExplicitWidth = 628
  ExplicitHeight = 357
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 622
    Height = 282
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Raz'#227'o Social'
      'Nome Fanstasia'
      '')
    RealColCount = 4
    ExplicitLeft = 0
    ExplicitTop = 46
    ExplicitWidth = 622
    ExplicitHeight = 282
    ColWidths = (
      28
      55
      268
      251)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 622
    ExplicitWidth = 622
    inherited eValorPesquisa: TEditLuka
      Width = 414
      ExplicitWidth = 414
    end
  end
end
