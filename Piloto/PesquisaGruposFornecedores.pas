unit PesquisaGruposFornecedores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Sessao, _GruposFornecedores,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Biblioteca, Vcl.ExtCtrls;

type
  TFormPesquisaGruposFornecedores = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pSomenteAtivos: Boolean): TObject;

implementation

{$R *.dfm}

const
  coNome   = 2;
  coAtivo  = 3;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
var
  obj: TObject;
begin
  obj := _HerancaPesquisas.Pesquisar(TFormPesquisaGruposFornecedores, _GruposFornecedores.GetFiltros, [pSomenteAtivos]);
  if obj = nil then
    Result := nil
  else
    Result := RecGruposFornecedores(obj);
end;

{ TFormPesquisaGruposFornecedores }

procedure TFormPesquisaGruposFornecedores.BuscarRegistros;
var
  i: Integer;
  vRotas: TArray<RecGruposFornecedores>;
begin
  inherited;

  vRotas :=
    _GruposFornecedores.BuscarGrupos(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FAuxiliares[0]
    );

  if vRotas = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vRotas);

  for i := Low(vRotas) to High(vRotas) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := NFormat(vRotas[i].GrupoId);
    sgPesquisa.Cells[coNome, i + 1]         := vRotas[i].Descricao;
    sgPesquisa.Cells[coAtivo, i + 1]        := vRotas[i].Ativo;
  end;
  sgPesquisa.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vRotas) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaGruposFornecedores.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coSelecionado, coAtivo] then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
