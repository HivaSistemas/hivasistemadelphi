unit ControleEntregas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, _Biblioteca,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.StdCtrls, ComboBoxLuka, _Entregas, _EntregasItens,
  Vcl.Grids, GridLuka, FrameEmpresas, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes,
  FrameDataInicialFinal, _RecordsExpedicao, _RecordsEspeciais, _Imagens, DefinirTransporte, BuscaDados,
  Vcl.Menus, _ControlesEntregas, BaixarControleEntrega, Informacoes.Entrega, InformacoesControleEntrega,
  StaticTextLuka, ImpressaoListaSeparacaoGrafico, ConferirSaidaProdutos, ImpressaoComprovanteEntregaGrafico,
  CheckBoxLuka, _RecordsNotasFiscais, _NotasFiscais, _ComunicacaoNFE, Data.DB,
  MemDS, DBAccess, Ora, frxClass, frxDBSet, OraCall;

type
  TFormControleEntregas = class(TFormHerancaRelatoriosPageControl)
    pcControle: TPageControl;
    tsAguardandoTransporte: TTabSheet;
    tsEmTransporte: TTabSheet;
    Label7: TLabel;
    cbAgrupamento: TComboBoxLuka;
    sgEntregas: TGridLuka;
    sgControles: TGridLuka;
    FrClientes: TFrClientes;
    FrEmpresas: TFrEmpresas;
    FrDataGeracaoEntrega: TFrDataInicialFinal;
    FrDataPrevisaoEntrega: TFrDataInicialFinal;
    pmOpcoesEntregas: TPopupMenu;
    pmOpcoesControle: TPopupMenu;
    miN1: TMenuItem;
    tsAguardandoSeparacao: TTabSheet;
    tsEmSeparacao: TTabSheet;
    st1: TStaticTextLuka;
    sgAguardandoSep: TGridLuka;
    splSeparador: TSplitter;
    sgItensAguardandoSep: TGridLuka;
    st2: TStaticTextLuka;
    pmAguardandoSeparacao: TPopupMenu;
    miGerarControleSeparacoesEntregasSelecionadas: TMenuItem;
    pmOpcoesEmSeparacao: TPopupMenu;
    miFinalizarSeparacao: TMenuItem;
    miN2: TMenuItem;
    miReimprimirListaSeparacao: TMenuItem;
    st3: TStaticTextLuka;
    sgEmSeparacao: TGridLuka;
    spl1: TSplitter;
    sgItensEmSeparacao: TGridLuka;
    st4: TStaticTextLuka;
    miCancelarSeparacao: TMenuItem;
    miN3: TMenuItem;
    miImprimirReimprimirComprovanteEntrega: TMenuItem;
    miGerarTransporte: TSpeedButton;
    miCancelarEntregas: TSpeedButton;
    miReimprimirComprovanteEntrega: TSpeedButton;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    miBaixarTransporte: TSpeedButton;
    miCancelarControleTransporte: TSpeedButton;
    miReimprimirComprovanteManifesto: TSpeedButton;
    frxReport: TfrxReport;
    frxProdutos: TfrxDBDataset;
    qryProdutos: TOraQuery;
    qryProdutosPRODUTO_ID: TFloatField;
    qryProdutosNOME_PRODUTO: TStringField;
    qryProdutosUNIDADE_VENDA: TStringField;
    qryProdutosNOME_MARCA: TStringField;
    qryProdutosQUENTIDADE: TFloatField;
    qryProdutosNOME_VEICULO: TStringField;
    qryProdutosNOME_MOTORISTA: TStringField;
    qryProdutosNOME_AJUDANTE: TStringField;
    qryProdutosVALOR_ENTREGAS: TFloatField;
    qryProdutosQTDE_ENTREGAS: TFloatField;
    qryProdutosQTDE_PRODUTOS: TFloatField;
    qryProdutosPESO_TOTAL: TFloatField;
    qryProdutosCONTROLE_ENTREGA_ID: TFloatField;
    procedure sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEntregasGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgEntregasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure miGerarTransporteClick(Sender: TObject);
    procedure sgControlesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miCancelarControleTransporteClick(Sender: TObject);
    procedure miBaixarTransporteClick(Sender: TObject);
    procedure sgEntregasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure miCancelarEntregasClick(Sender: TObject);
    procedure sgEntregasDblClick(Sender: TObject);
    procedure sgControlesDblClick(Sender: TObject);
    procedure miReimprimirComprovanteEntregaClick(Sender: TObject);
    procedure sgAguardandoSepDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEmSeparacaoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgAguardandoSepDblClick(Sender: TObject);
    procedure sgEmSeparacaoDblClick(Sender: TObject);
    procedure miGerarControleSeparacoesEntregasSelecionadasClick(Sender: TObject);
    procedure miReimprimirListaSeparacaoClick(Sender: TObject);
    procedure miFinalizarSeparacaoClick(Sender: TObject);
    procedure sgEmSeparacaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure miCancelarSeparacaoClick(Sender: TObject);
    procedure sgAguardandoSepClick(Sender: TObject);
    procedure sgItensAguardandoSepDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensEmSeparacaoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEmSeparacaoClick(Sender: TObject);
    procedure sgControlesKeyPress(Sender: TObject; var Key: Char);
    procedure sgEntregasKeyPress(Sender: TObject; var Key: Char);
    procedure sgEmSeparacaoKeyPress(Sender: TObject; var Key: Char);
    procedure miImprimirReimprimirComprovanteEntregaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure miReimprimirComprovanteManifestoClick(Sender: TObject);
    procedure pcControleChange(Sender: TObject);
  private
    FItens: TArray<RecEntregaItem>;
    FEntregasControlesIds: TArray<RecControleEntregasIds>;
    procedure ImprimirManifesto(pEntregasId : string);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormControleEntregas }

const
  (* Grid de aguardando separa��es *)
  caEntrega      = 0;
  caDataGeracao  = 1;
  caOrcamento    = 2;
  caCliente      = 3;
  caPrevEntrega  = 4;
  caEmpresa      = 5;
  caLocal        = 6;
  caBairro       = 7;
  caRota         = 8;
  caPesoTotal    = 9;
  caQtdeProdutos = 10;
  caTipoLinha    = 11;
  caMaximo       = 12;

  (* Grid de itens aguardando separa��es *)
  ciProdutoId   = 0;
  ciNomeProduto = 1;
  ciMarca       = 2;
  ciQtde        = 3;
  ciUnidade     = 4;
  ciLote        = 5;

  (* Grid de Em separa��es *)
  csEntrega      = 0;
  csDataGeracao  = 1;
  csOrcamento    = 2;
  csCliente      = 3;
  csPrevEntrega  = 4;
  csEmpresa      = 5;
  csLocal        = 6;
  csBairro       = 7;
  csRota         = 8;
  csPesoTotal    = 9;
  csQtdeProdutos = 10;
  csTipoLinha    = 11;
  csMaximo       = 12;

  (* Grid de itens aguardando separa��es *)
  cisProdutoId   = 0;
  cisNomeProduto = 1;
  cisMarca       = 2;
  cisQtde        = 3;
  cisUnidade     = 4;
  cisLote        = 5;

  (* Grid de entregas *)
  ceSelecionado  = 0;
  ceEntrega      = 1;
  ceDataGeracao  = 2;
  ceOrcamento    = 3;
  ceCliente      = 4;
  cePrevEntrega  = 5;
  ceEmpresa      = 6;
  ceLocal        = 7;
  ceBairro       = 8;
  ceRota         = 9;
  cePesoTotal    = 10;
  ceQtdeProdutos = 11;
  ceTipoLinha    = 12;
  ceMaximo       = 13;

  (* Grid de controles *)
  coControleId      = 0;
  coDataHoraGeracao = 1;
  coVeiculo         = 2;
  coMotorista       = 3;
  coEmpresa         = 4;
  coQtdeEntregas    = 5;
  coQtdeProdutos    = 6;
  coPesoTotal       = 7;

  clCabecalho = 'CAB';
  clNormal    = 'NOR';

procedure TFormControleEntregas.Carregar(Sender: TObject);
var
  i: Integer;

  vLinhaEnt: Integer;
  vLinhaAgSep: Integer;
  vLinhaEmSep: Integer;

  vAgrupEntregas: Integer;
  vAgrupAguardSep: Integer;
  vAgrupEmSeparacao: Integer;

  vSql: string;
  vEntregas: TArray<RecEntrega>;
  vControles: TArray<RecControlesEntregas>;

  vEntregasIds: TArray<Integer>;
  vControleEntregasIds: TArray<Integer>;
begin
  inherited;

  FItens := nil;
  vEntregasIds := nil;
  vControleEntregasIds := nil;

  vLinhaEnt := 0;
  vLinhaAgSep := 0;
  vLinhaEmSep := 0;

  vAgrupEntregas := -1;
  vAgrupAguardSep := -1;
  vAgrupEmSeparacao := -1;

  _Biblioteca.LimparCampos([
    sgAguardandoSep,
    sgItensAguardandoSep,
    sgEmSeparacao,
    sgItensEmSeparacao,
    sgEntregas,
    sgControles
  ]);

  vSql := 'where ENT.STATUS in(''AGS'', ''ESE'', ''AGC'') ';

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrEmpresas.getSqlFiltros('ENT.EMPRESA_ENTREGA_ID'));

  if not FrClientes.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrClientes.getSqlFiltros('ORC.CLIENTE_ID'));

  if not FrDataGeracaoEntrega.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vSql, FrDataGeracaoEntrega.getSqlFiltros('trunc(DATA_HORA_CADASTRO)'));

  if not FrDataPrevisaoEntrega.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vSql, FrDataPrevisaoEntrega.getSqlFiltros('ENT.PREVISAO_ENTREGA'));

  if cbAgrupamento.GetValor = 'ROT' then
    vSql := vSql + ' order by BAI.ROTA_ID '
  else if cbAgrupamento.GetValor = 'CLI' then
    vSql := vSql + ' order by ORC.CLIENTE_ID '
  else
    vSql := vSql + ' order by ENT.ENTREGA_ID ';

  vEntregas := _Entregas.BuscarEntregasComando(Sessao.getConexaoBanco, vSql);
  for i := Low(vEntregas) to High(vEntregas) do begin
    _Biblioteca.AddNoVetorSemRepetir(vEntregasIds, vEntregas[i].EntregaId);

    if vEntregas[i].Status = 'AGS' then begin
      Inc(vLinhaAgSep);

      if cbAgrupamento.GetValor = 'ROT' then begin
        if vAgrupAguardSep <> vEntregas[i].RotaId then begin
          if vEntregas[i].RotaId = 0 then
            sgAguardandoSep.Cells[caEntrega, vLinhaAgSep] := 'ROTA: SEM ROTA DEFINIDA'
          else
            sgAguardandoSep.Cells[caEntrega, vLinhaAgSep] := 'ROTA: ' + NFormat(vEntregas[i].RotaId) + ' - ' + vEntregas[i].NomeRota;

          sgAguardandoSep.Cells[caTipoLinha, vLinhaAgSep] := clCabecalho;

          Inc(vLinhaAgSep);
          vAgrupAguardSep := vEntregas[i].RotaId;
        end;
      end
      else if cbAgrupamento.GetValor = 'CLI' then begin
        if vAgrupAguardSep <> vEntregas[i].ClienteId then begin
          sgAguardandoSep.Cells[caEntrega, vLinhaAgSep] := 'CLIENTE: ' + NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;

          sgAguardandoSep.Cells[caTipoLinha, vLinhaAgSep] := clCabecalho;

          Inc(vLinhaAgSep);
          vAgrupAguardSep := vEntregas[i].ClienteId;
        end;
      end;

      sgAguardandoSep.Cells[caEntrega, vLinhaAgSep]      := NFormat(vEntregas[i].EntregaId);
      sgAguardandoSep.Cells[caDataGeracao, vLinhaAgSep]  := DFormat(vEntregas[i].DataHoraCadastro);
      sgAguardandoSep.Cells[caOrcamento, vLinhaAgSep]    := NFormat(vEntregas[i].OrcamentoId);
      sgAguardandoSep.Cells[caCliente, vLinhaAgSep]      := NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;
      sgAguardandoSep.Cells[caPrevEntrega, vLinhaAgSep]  := DHFormat(vEntregas[i].PrevisaoEntrega);
      sgAguardandoSep.Cells[caEmpresa, vLinhaAgSep]      := NFormat(vEntregas[i].EmpresaId) + ' - ' + vEntregas[i].NomeEmpresa;
      sgAguardandoSep.Cells[caLocal, vLinhaAgSep]        := NFormat(vEntregas[i].LocalId) + ' - ' + vEntregas[i].NomeLocal;
      sgAguardandoSep.Cells[caBairro, vLinhaAgSep]       := NFormat(vEntregas[i].BairroId) + ' - ' + vEntregas[i].NomeBairro;
      sgAguardandoSep.Cells[caRota, vLinhaAgSep]         := getInformacao(vEntregas[i].RotaId, vEntregas[i].NomeRota);
      sgAguardandoSep.Cells[caPesoTotal, vLinhaAgSep]    := NFormat(vEntregas[i].PesoTotal, 3);
      sgAguardandoSep.Cells[caQtdeProdutos, vLinhaAgSep] := NFormat(vEntregas[i].QtdeProdutos);
      sgAguardandoSep.Cells[caTipoLinha, vLinhaAgSep]    := clNormal;
    end
    else if vEntregas[i].Status = 'ESE' then begin
      Inc(vLinhaEmSep);

      if cbAgrupamento.GetValor = 'ROT' then begin
        if vAgrupEmSeparacao <> vEntregas[i].RotaId then begin
          if vEntregas[i].RotaId = 0 then
            sgEmSeparacao.Cells[csEntrega, vLinhaEmSep] := 'ROTA: SEM ROTA DEFINIDA'
          else
            sgEmSeparacao.Cells[csEntrega, vLinhaEmSep] := 'ROTA: ' + NFormat(vEntregas[i].RotaId) + ' - ' + vEntregas[i].NomeRota;

          sgEmSeparacao.Cells[cSTipoLinha, vLinhaEmSep] := clCabecalho;

          Inc(vLinhaEmSep);
          vAgrupEmSeparacao := vEntregas[i].RotaId;
        end;
      end
      else if cbAgrupamento.GetValor = 'CLI' then begin
        if vAgrupEmSeparacao <> vEntregas[i].ClienteId then begin
          sgEmSeparacao.Cells[csEntrega, vLinhaEmSep] := 'CLIENTE: ' + NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;

          sgEmSeparacao.Cells[cSTipoLinha, vLinhaEmSep] := clCabecalho;

          Inc(vLinhaEmSep);
          vAgrupEmSeparacao := vEntregas[i].ClienteId;
        end;
      end;

      sgEmSeparacao.Cells[csEntrega, vLinhaEmSep]      := NFormat(vEntregas[i].EntregaId);
      sgEmSeparacao.Cells[csDataGeracao, vLinhaEmSep]  := DFormat(vEntregas[i].DataHoraCadastro);
      sgEmSeparacao.Cells[csOrcamento, vLinhaEmSep]    := NFormat(vEntregas[i].OrcamentoId);
      sgEmSeparacao.Cells[csCliente, vLinhaEmSep]      := NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;
      sgEmSeparacao.Cells[csPrevEntrega, vLinhaEmSep]  := DHFormat(vEntregas[i].PrevisaoEntrega);
      sgEmSeparacao.Cells[csEmpresa, vLinhaEmSep]      := NFormat(vEntregas[i].EmpresaId) + ' - ' + vEntregas[i].NomeEmpresa;
      sgEmSeparacao.Cells[csLocal, vLinhaEmSep]        := NFormat(vEntregas[i].LocalId) + ' - ' + vEntregas[i].NomeLocal;
      sgEmSeparacao.Cells[csBairro, vLinhaEmSep]       := NFormat(vEntregas[i].BairroId) + ' - ' + vEntregas[i].NomeBairro;
      sgEmSeparacao.Cells[csRota, vLinhaEmSep]         := getInformacao(vEntregas[i].RotaId, vEntregas[i].NomeRota);
      sgEmSeparacao.Cells[csPesoTotal, vLinhaEmSep]    := NFormat(vEntregas[i].PesoTotal, 3);
      sgEmSeparacao.Cells[csQtdeProdutos, vLinhaEmSep] := NFormat(vEntregas[i].QtdeProdutos);
      sgEmSeparacao.Cells[cSTipoLinha, vLinhaEmSep]    := clNormal;
    end
    else begin
      Inc(vLinhaEnt);

      if cbAgrupamento.GetValor = 'ROT' then begin
        if vAgrupEntregas <> vEntregas[i].RotaId then begin
          if vEntregas[i].RotaId = 0 then
            sgEntregas.Cells[ceEntrega, vLinhaEnt] := 'ROTA: SEM ROTA DEFINIDA'
          else
            sgEntregas.Cells[ceEntrega, vLinhaEnt] := 'ROTA: ' + NFormat(vEntregas[i].RotaId) + ' - ' + vEntregas[i].NomeRota;

          sgEntregas.Cells[ceTipoLinha, vLinhaEnt] := clCabecalho;

          Inc(vLinhaEnt);
          vAgrupEntregas := vEntregas[i].RotaId;
        end;
      end
      else if cbAgrupamento.GetValor = 'CLI' then begin
        if vAgrupEntregas <> vEntregas[i].ClienteId then begin
          sgEntregas.Cells[ceEntrega, vLinhaEnt] := 'CLIENTE: ' + NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;

          sgEntregas.Cells[ceTipoLinha, vLinhaEnt] := clCabecalho;

          Inc(vLinhaEnt);
          vAgrupEntregas := vEntregas[i].ClienteId;
        end;
      end;

      sgEntregas.Cells[ceSelecionado, vLinhaEnt]  := charNaoSelecionado;
      sgEntregas.Cells[ceEntrega, vLinhaEnt]      := NFormat(vEntregas[i].EntregaId);
      sgEntregas.Cells[ceDataGeracao, vLinhaEnt]  := DFormat(vEntregas[i].DataHoraCadastro);
      sgEntregas.Cells[ceOrcamento, vLinhaEnt]    := NFormat(vEntregas[i].OrcamentoId);
      sgEntregas.Cells[ceCliente, vLinhaEnt]      := NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;
      sgEntregas.Cells[cePrevEntrega, vLinhaEnt]  := DHFormat(vEntregas[i].PrevisaoEntrega);
      sgEntregas.Cells[ceEmpresa, vLinhaEnt]      := NFormat(vEntregas[i].EmpresaId) + ' - ' + vEntregas[i].NomeEmpresa;
      sgEntregas.Cells[ceLocal, vLinhaEnt]        := NFormat(vEntregas[i].LocalId) + ' - ' + vEntregas[i].NomeLocal;
      sgEntregas.Cells[ceBairro, vLinhaEnt]       := NFormat(vEntregas[i].BairroId) + ' - ' + vEntregas[i].NomeBairro;
      sgEntregas.Cells[ceRota, vLinhaEnt]         := getInformacao(vEntregas[i].RotaId, vEntregas[i].NomeRota);
      sgEntregas.Cells[cePesoTotal, vLinhaEnt]    := NFormat(vEntregas[i].PesoTotal, 3);
      sgEntregas.Cells[ceQtdeProdutos, vLinhaEnt] := NFormat(vEntregas[i].QtdeProdutos);
      sgEntregas.Cells[ceTipoLinha, vLinhaEnt]    := clNormal;
    end;
  end;
  sgAguardandoSep.SetLinhasGridPorTamanhoVetor( vLinhaAgSep );
  sgEmSeparacao.SetLinhasGridPorTamanhoVetor( vLinhaEmSep );
  sgEntregas.SetLinhasGridPorTamanhoVetor( vLinhaEnt );

  vSql := 'where CON.STATUS = ''T'' order by CON.DATA_HORA_CADASTRO asc ';
  vControles := _ControlesEntregas.BuscarControlesComando(Sessao.getConexaoBanco, vSql);

  if (vEntregas = nil) and (vControles = nil) then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  for i := Low(vControles) to High(vControles) do begin
    sgControles.Cells[coControleId, i + 1]      := NFormat(vControles[i].ControleEntregaId);
    sgControles.Cells[coDataHoraGeracao, i + 1] := DHFormat(vControles[i].DataHoraCadastro);
    sgControles.Cells[coVeiculo, i + 1]         := NFormat(vControles[i].VeiculoId) + ' - ' + vControles[i].NomeVeiculo;
    sgControles.Cells[coMotorista, i + 1]       := NFormat(vControles[i].MotoristaId) + ' - ' + vControles[i].NomeMotorista;
    sgControles.Cells[coEmpresa, i + 1]         := NFormat(vControles[i].EmpresaId) + ' - ' + vControles[i].NomeEmpresa;
    sgControles.Cells[coQtdeEntregas, i + 1]    := NFormat(vControles[i].QtdeEntregas);
    sgControles.Cells[coQtdeProdutos, i + 1]    := NFormat(vControles[i].QtdeProdutos);
    sgControles.Cells[coPesoTotal, i + 1]       := NFormatNEstoque(vControles[i].PesoTotal);

    _Biblioteca.AddNoVetorSemRepetir(vControleEntregasIds, vControles[i].ControleEntregaId);
  end;
  sgControles.SetLinhasGridPorTamanhoVetor( Length(vControles) );

  if vEntregasIds <> nil then
    FItens := _EntregasItens.BuscarEntregaItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.ENTREGA_ID', vEntregasIds));

  if vControleEntregasIds <> nil then
    FEntregasControlesIds := _Entregas.BuscarEntregasIdsPorControle(Sessao.getConexaoBanco, vControleEntregasIds);

  sgAguardandoSepClick(Sender);
  sgEmSeparacaoClick(Sender);

  if (vEntregas = nil) and (vControles = nil) then
    NenhumRegistro
  else if sgAguardandoSep.Cells[caEntrega, 1] <> '' then
    SetarFoco(sgAguardandoSep)
  else if sgEmSeparacao.Cells[csEntrega, 1] <> '' then
    SetarFoco(sgEmSeparacao)
  else if sgEntregas.Cells[ceEntrega, 1] <> '' then
    SetarFoco(sgEntregas)        
  else
    SetarFoco(sgControles);
end;

procedure TFormControleEntregas.FormShow(Sender: TObject);
begin
  inherited;
  tsAguardandoSeparacao.TabVisible := Sessao.getParametrosEmpresa.TrabalharControleSeparacao = 'S';
  tsEmSeparacao.TabVisible         := Sessao.getParametrosEmpresa.TrabalharControleSeparacao = 'S';
end;

procedure TFormControleEntregas.Imprimir(Sender: TObject);
begin
  inherited;
end;

procedure TFormControleEntregas.ImprimirManifesto(pEntregasId: string);
var
  vSql: string;
  vFiltro : string;
begin
   if (pcDados.ActivePage = tsResultado) and (pcControle.ActivePage = tsEmTransporte) then
     vFiltro :=  ' WHERE  ITE.CONTROLE_ENTREGA_ID = '+ pEntregasId
   else
    vFiltro :=  ' WHERE '+ pEntregasId;
  vSql :=
          'SELECT ITE.CONTROLE_ENTREGA_ID, '
          +'       EIT.PRODUTO_ID, '
          +'       PRO.NOME AS NOME_PRODUTO, '
          +'       PRO.UNIDADE_VENDA, '
          +'       MAR.NOME AS NOME_MARCA, '
          +'       VEI.NOME AS NOME_VEICULO, '
          +'       CAD.NOME_FANTASIA AS NOME_MOTORISTA, '
          +'       FUNC.NOME AS NOME_AJUDANTE, '
          +'       ITE.VALOR_ENTREGAS, '
          +'       ENT.QTDE_ENTREGAS, '
          +'       ITE.QTDE_PRODUTOS, '
          +'       ITE.PESO_TOTAL, '
          +'       SUM(EIT.QUANTIDADE) AS QUENTIDADE '
          +'  FROM CONTROLES_ENTREGAS CON '
          +' INNER JOIN CONTROLES_ENTREGAS_ITENS ITE '
          +'    ON CON.CONTROLE_ENTREGA_ID = ITE.CONTROLE_ENTREGA_ID '
          +' INNER JOIN ENTREGAS_ITENS EIT '
          +'    ON ITE.ENTREGA_ID = EIT.ENTREGA_ID '
          +' INNER JOIN PRODUTOS PRO '
          +'    ON EIT.PRODUTO_ID = PRO.PRODUTO_ID '
          +' INNER JOIN MARCAS MAR '
          +'    ON PRO.MARCA_ID = MAR.MARCA_ID '
          +' INNER JOIN VEICULOS VEI '
          +'    ON CON.VEICULO_ID = VEI.VEICULO_ID '
          +' INNER JOIN CADASTROS CAD '
          +'    ON CON.MOTORISTA_ID = CAD.CADASTRO_ID '
          +'  LEFT JOIN CONTROLES_ENTREGAS_AJUDANTES CEA '
          +'    ON ITE.CONTROLE_ENTREGA_ID = CEA.CONTROLE_ENTREGA_ID '
          +'  LEFT JOIN FUNCIONARIOS FUNC '
          +'    ON CEA.AJUDANTE_ID = FUNC.FUNCIONARIO_ID '

          +' inner join (select CONTROLE_ENTREGA_ID, count(*) as QTDE_ENTREGAS '
          +'               from CONTROLES_ENTREGAS_ITENS '
          +'              group by CONTROLE_ENTREGA_ID) ENT '
          +'    on CON.CONTROLE_ENTREGA_ID = ENT.CONTROLE_ENTREGA_ID '

          +' inner join (select ENT.CONTROLE_ENTREGA_ID, '
          +'                    sum(round((ORC.VALOR_TOTAL - ORC.VALOR_FRETE) * '
          +'                              OIT.VALOR_TOTAL / OIT.QUANTIDADE *   '
          +'                              ITE.QUANTIDADE / ORC.VALOR_TOTAL_PRODUTOS, '
          +'                              2)) as VALOR_ENTREGAS, '
          +'                    count(ITE.PRODUTO_ID) as QTDE_PRODUTOS, '
          +'                    sum(ITE.QUANTIDADE * PRO.PESO) as PESO_TOTAL '
          +'               from ENTREGAS ENT '

          +'              inner join ENTREGAS_ITENS ITE '
          +'                 on ENT.ENTREGA_ID = ITE.ENTREGA_ID '

          +'              inner join ORCAMENTOS ORC '
          +'                 on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID '

          +'              inner join ORCAMENTOS_ITENS OIT '
          +'                 on ORC.ORCAMENTO_ID = OIT.ORCAMENTO_ID '
          +'                and ITE.ITEM_ID = OIT.ITEM_ID '

          +'              inner join PRODUTOS PRO '
          +'                 on ITE.PRODUTO_ID = PRO.PRODUTO_ID '

          +'              where ENT.CONTROLE_ENTREGA_ID is not null '

          +'              group by ENT.CONTROLE_ENTREGA_ID) ITE '
          +'    on CON.CONTROLE_ENTREGA_ID = ITE.CONTROLE_ENTREGA_ID '


          + vFiltro
          +' GROUP BY ITE.CONTROLE_ENTREGA_ID, '
          +'          EIT.PRODUTO_ID, '
          +'          PRO.NOME, '
          +'          PRO.UNIDADE_VENDA, '
          +'          MAR.NOME, '
          +'          VEI.NOME, '
          +'          CAD.NOME_FANTASIA, '
          +'          FUNC.NOME, '
          +'          ITE.VALOR_ENTREGAS, '
          +'          ENT.QTDE_ENTREGAS, '
          +'          ITE.QTDE_PRODUTOS, '
          +'          ITE.PESO_TOTAL '

          +' ORDER BY PRO.NOME ';


  qryProdutos.Session := Sessao.getConexaoBanco;
  qryProdutos.Close;
  qryProdutos.SQL.Clear;
  qryProdutos.SQL.Add(vSql);
  qryProdutos.Open;
  if qryProdutos.IsEmpty then
  begin
    NenhumRegistro;
    exit;
  end;
  frxReport.ShowReport;
end;

procedure TFormControleEntregas.miReimprimirListaSeparacaoClick(Sender: TObject);
begin
  inherited;
  ImpressaoListaSeparacaoGrafico.Imprimir(
    SFormatInt(sgEmSeparacao.Cells[csEntrega, sgEmSeparacao.Row]),
    ImpressaoListaSeparacaoGrafico.tpEntrega
  );
end;

procedure TFormControleEntregas.pcControleChange(Sender: TObject);
begin
  inherited;
  miReimprimirComprovanteManifesto.Visible := (pcDados.ActivePage = tsResultado) and (pcControle.ActivePage = tsEmTransporte) ;
end;

procedure TFormControleEntregas.miBaixarTransporteClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;

  vRetTela := BaixarControleEntrega.Baixar( SFormatInt(sgControles.Cells[coControleId, sgControles.Row]) );
  if vRetTela.BuscaCancelada then
    Exit;

  Carregar(nil);
end;

procedure TFormControleEntregas.miCancelarControleTransporteClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  if sgControles.Cells[coControleId, sgControles.Row] = '' then
    Exit;

  vRetBanco := _ControlesEntregas.ExcluirControle(Sessao.getConexaoBanco, SFormatInt(sgControles.Cells[coControleId, sgControles.Row]) );
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormControleEntregas.miCancelarEntregasClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  if not _Biblioteca.Perguntar('Deseja realmente cancelar esta entrega?') then
    Abort;

  vRetBanco := _Entregas.CancelarEntrega( Sessao.getConexaoBanco, SFormatInt( sgEntregas.Cells[ceEntrega, sgEntregas.Row] ) );

  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.Informar('Cancelamento de entrega realizado com sucesso.');
  Carregar( Sender );
end;

procedure TFormControleEntregas.miCancelarSeparacaoClick(Sender: TObject);
var
  vEntregaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vEntregaId := SFormatInt(sgEmSeparacao.Cells[csEntrega, sgEmSeparacao.Row]);
  if vEntregaId = 0 then
    Exit;

  if not _Biblioteca.Perguntar('Deseja realmente cancelar a separa��o desta entrega?') then
    Exit;

  vRetBanco := _Entregas.AlterarStatusSeparacao( Sessao.getConexaoBanco, vEntregaId, 'AGS' );
  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);

end;

procedure TFormControleEntregas.miFinalizarSeparacaoClick(Sender: TObject);
var
  vEntregaId: Integer;
  vQtdeViasJaImpressasCompEntrega: Integer;
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;

  vEntregaId := SFormatInt(sgEmSeparacao.Cells[csEntrega, sgEmSeparacao.Row]);
  if vEntregaId = 0 then
    Exit;

  vRetTela := ConferirSaidaProdutos.Conferir( ConferirSaidaProdutos.tpEntrega, vEntregaId );
  if vRetTela.BuscaCancelada then
    Exit;

  // Se n�o imprimiu o comprovante ainda ent�o realizando a impress�o
  vQtdeViasJaImpressasCompEntrega := _Entregas.getQtdeViasImpressasComprovanteEntrega(Sessao.getConexaoBanco, vEntregaId);
  if vQtdeViasJaImpressasCompEntrega = 0 then  
    ImpressaoComprovanteEntregaGrafico.Imprimir(vEntregaId, tpEntrega);

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormControleEntregas.miGerarControleSeparacoesEntregasSelecionadasClick(Sender: TObject);
var
  vEntregaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vEntregaId := SFormatInt(sgAguardandoSep.Cells[caEntrega, sgAguardandoSep.Row]);
  if vEntregaId = 0 then
    Exit;

  vRetBanco := _Entregas.AlterarStatusSeparacao( Sessao.getConexaoBanco, vEntregaId, 'ESE' );
  Sessao.AbortarSeHouveErro( vRetBanco );

  ImpressaoListaSeparacaoGrafico.Imprimir( vEntregaId, ImpressaoListaSeparacaoGrafico.tpEntrega );

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormControleEntregas.miGerarTransporteClick(Sender: TObject);
var
  i: Integer;
  vEntregasIds: TArray<Integer>;
  vEntregas: TArray<RecEntrega>;
  vRetTela: TRetornoTelaFinalizar;

  vNotas: TArray<RecNotaFiscal>;
begin
  inherited;

  vEntregas := nil;
  vEntregasIds := nil;

  for i := 1 to sgEntregas.RowCount -1 do begin
    if sgEntregas.Cells[ceSelecionado, i] <> charSelecionado then
      Continue;

    SetLength(vEntregas, Length(vEntregas) + 1);

    vEntregas[High(vEntregas)].EntregaId    := SFormatInt(sgEntregas.Cells[ceEntrega, i]);
    vEntregas[High(vEntregas)].PesoTotal    := SFormatDouble(sgEntregas.Cells[cePesoTotal, i]);
    vEntregas[High(vEntregas)].QtdeProdutos := SFormatInt(sgEntregas.Cells[ceQtdeProdutos, i]);

    _Biblioteca.AddNoVetorSemRepetir(vEntregasIds, vEntregas[High(vEntregas)].EntregaId);
  end;
    
  if vEntregas = nil then begin
    _Biblioteca.NenhumRegistroSelecionado;
    SetarFoco(sgEntregas);
    Exit;  
  end;
  
  vRetTela := DefinirTransporte.Definir( vEntregas );
  if vRetTela.BuscaCancelada then
    Exit;
  ImprimirManifesto( _Biblioteca.FiltroInInt('ITE.ENTREGA_ID', vEntregasIds) );

  vNotas :=
    _NotasFiscais.BuscarNotasFiscaisComando(
      Sessao.getConexaoBanco,
      'where ' + _Biblioteca.FiltroInInt('NFI.ENTREGA_ID', vEntregasIds) +
      ' and NFI.TIPO_MOVIMENTO = ''VEN'' ' +
      ' and NFI.STATUS = ''N'' ' +
      ' and NFI.ENTREGA_ID in(select NFI2.ENTREGA_ID from ENTREGAS NFI2 where NFI2.TIPO_NOTA_GERAR <> ''NI'') '
    );

  for i := Low(vNotas) to High(vNotas) do begin
    if not _ComunicacaoNFE.EnviarNFe(vNotas[i].nota_fiscal_id) then begin
      _Biblioteca.Exclamar(
        'Ocorreu um problema ao tentar emitir a nota fiscal eletr�nica automaticamente!' + Chr(13) + Chr(10) +
        'Fa�a a emiss�o pela pela de "Rela��o de notas fiscais"'
      );
    end;
  end;

  Carregar(nil);
end;

procedure TFormControleEntregas.miImprimirReimprimirComprovanteEntregaClick(Sender: TObject);
begin
  inherited;
  ImpressaoComprovanteEntregaGrafico.Imprimir( SFormatInt(sgEmSeparacao.Cells[csEntrega, sgEmSeparacao.Row]), tpEntrega );
end;

procedure TFormControleEntregas.miReimprimirComprovanteEntregaClick(Sender: TObject);
begin
  inherited;
 ImpressaoComprovanteEntregaGrafico.Imprimir( SFormatInt(sgEntregas.Cells[ceEntrega, sgEntregas.Row]) , tpEntrega );
end;

procedure TFormControleEntregas.miReimprimirComprovanteManifestoClick(
  Sender: TObject);
begin
  ImprimirManifesto( IntToStr(SFormatInt(sgControles.Cells[coControleId, sgControles.Row])));
end;

procedure TFormControleEntregas.sgEmSeparacaoClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vEntregaId: Integer;
begin
  inherited;
  vLinha := 0;
  sgItensEmSeparacao.ClearGrid;
  vEntregaId := SFormatInt(sgEmSeparacao.Cells[csEntrega, sgEmSeparacao.Row]);
  for i := Low(FItens) to High(FItens) do begin
    if vEntregaId <> FItens[i].EntregaId then
      Continue;

    Inc(vLinha);

    sgItensEmSeparacao.Cells[cisProdutoId, vLinha]   := NFormat(FItens[i].ProdutoId);
    sgItensEmSeparacao.Cells[cisNomeProduto, vLinha] := FItens[i].NomeProduto;
    sgItensEmSeparacao.Cells[cisMarca, vLinha]       := FItens[i].NomeMarca;
    sgItensEmSeparacao.Cells[cisQtde, vLinha]        := NFormatNEstoque(FItens[i].Quantidade);
    sgItensEmSeparacao.Cells[cisUnidade, vLinha]     := FItens[i].Unidade;
    sgItensEmSeparacao.Cells[cisLote, vLinha]        := FItens[i].Lote;
  end;
  sgItensEmSeparacao.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormControleEntregas.sgEmSeparacaoDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Entrega.Informar( SFormatInt(sgEmSeparacao.Cells[csEntrega, sgEmSeparacao.Row]) );
end;

procedure TFormControleEntregas.sgEmSeparacaoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[csEntrega, csOrcamento, csPesoTotal, csQtdeProdutos] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgEmSeparacao.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormControleEntregas.sgEmSeparacaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = Ord('F') then
    miFinalizarSeparacaoClick(Sender);
end;

procedure TFormControleEntregas.sgEmSeparacaoKeyPress(Sender: TObject; var Key: Char);
var
  vLinhaEntrega: Integer;
  vValorDigitado: string;
begin
  inherited;
  if CharInSet(Key, ['f', 'F', 'c', 'C']) then
    Exit;

  if CharInSet(Key, ['0'..'9', 'A'..'Z', 'a'..'z' , '.', '-' , '?']) then begin
    vValorDigitado := BuscaDados.BuscarDados(tiTexto, 'Busca digitada', Key);
    if vValorDigitado <> '' then begin
      vLinhaEntrega := sgEmSeparacao.Localizar([csEntrega], [ NFormat(SFormatInt(vValorDigitado)) ]);
      if vLinhaEntrega > -1 then begin
        sgEmSeparacao.Row := vLinhaEntrega;
        miFinalizarSeparacaoClick(nil);
      end;
    end
    else
      SetFocus;
  end;
end;

procedure TFormControleEntregas.sgEntregasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Entrega.Informar( SFormatInt(sgEntregas.Cells[ceEntrega, sgEntregas.Row]) );
end;

procedure TFormControleEntregas.sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ceSelecionado] then
    vAlinhamento := taCenter
  else if (ACol in[ceOrcamento, ceEntrega, cePesoTotal, ceQtdeProdutos]) and (sgEntregas.Cells[ceTipoLinha, ARow] <> clCabecalho) then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  if sgEntregas.Cells[ceTipoLinha, ARow] = clCabecalho then
    sgEntregas.MergeCells([ARow, ACol], [ARow, ceEntrega], [ARow, ceQtdeProdutos], vAlinhamento, Rect)
  else
    sgEntregas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormControleEntregas.sgEntregasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgEntregas.Cells[ceTipoLinha, ARow] = clCabecalho then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end;
end;

procedure TFormControleEntregas.sgEntregasGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgEntregas.Cells[ceEntrega, ARow] = '' then
    Exit;

  if ACol = ceSelecionado then begin
    if sgEntregas.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgEntregas.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormControleEntregas.sgEntregasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if sgEntregas.Cells[ceTipoLinha, sgEntregas.Row] = clCabecalho then
    Exit;

  if sgEntregas.Cells[ceEntrega, sgEntregas.Row] = '' then
    Exit;

  if Key = VK_SPACE then
    sgEntregas.Cells[ceSelecionado, sgEntregas.Row] := IIfStr(sgEntregas.Cells[ceSelecionado, sgEntregas.Row] = charNaoSelecionado, charSelecionado, charNaoSelecionado);
end;

procedure TFormControleEntregas.sgEntregasKeyPress(Sender: TObject; var Key: Char);
var
  vLinhaEntrega: Integer;
  vValorDigitado: string;
begin
  inherited;
  if CharInSet(Key, ['g', 'G']) then
    Exit;

  if CharInSet(Key, ['0'..'9', 'A'..'Z', 'a'..'z' , '.', '-' , '?']) then begin
    vValorDigitado := BuscaDados.BuscarDados(tiTexto, 'Busca digitada', Key);
    if vValorDigitado <> '' then begin
      vLinhaEntrega := sgEntregas.Localizar([ceEntrega], [ NFormat(SFormatInt(vValorDigitado)) ]);
      if vLinhaEntrega > -1 then begin
        sgEntregas.Row := vLinhaEntrega;
        keybd_event(VK_SPACE, 0, 0, 0)
      end;
    end
    else
      SetFocus;
  end;
end;

procedure TFormControleEntregas.sgItensAguardandoSepDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[ciProdutoId, ciQtde] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItensAguardandoSep.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormControleEntregas.sgItensEmSeparacaoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[cisProdutoId, cisQtde] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItensEmSeparacao.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormControleEntregas.sgAguardandoSepClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vEntregaId: Integer;
begin
  inherited;
  vLinha := 0;
  sgItensAguardandoSep.ClearGrid;
  vEntregaId := SFormatInt(sgAguardandoSep.Cells[caEntrega, sgAguardandoSep.Row]);
  for i := Low(FItens) to High(FItens) do begin
    if vEntregaId <> FItens[i].EntregaId then
      Continue;

    Inc(vLinha);

    sgItensAguardandoSep.Cells[ciProdutoId, vLinha]   := NFormat(FItens[i].ProdutoId);
    sgItensAguardandoSep.Cells[ciNomeProduto, vLinha] := FItens[i].NomeProduto;
    sgItensAguardandoSep.Cells[ciMarca, vLinha]       := FItens[i].NomeMarca;
    sgItensAguardandoSep.Cells[ciQtde, vLinha]        := NFormatNEstoque(FItens[i].Quantidade);
    sgItensAguardandoSep.Cells[ciUnidade, vLinha]     := FItens[i].Unidade;
    sgItensAguardandoSep.Cells[ciLote, vLinha]        := FItens[i].Lote;
  end;
  sgItensAguardandoSep.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormControleEntregas.sgAguardandoSepDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Entrega.Informar( SFormatInt(sgAguardandoSep.Cells[caEntrega, sgAguardandoSep.Row]) );
end;

procedure TFormControleEntregas.sgAguardandoSepDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[caEntrega, caOrcamento, caPesoTotal, caQtdeProdutos] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgAguardandoSep.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormControleEntregas.sgControlesDblClick(Sender: TObject);
begin
  inherited;
  InformacoesControleEntrega.Informar( SFormatInt( sgControles.Cells[coControleId, sgControles.Row] ) );
end;

procedure TFormControleEntregas.sgControlesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coControleId,
    coQtdeEntregas,
    coQtdeProdutos,
    coPesoTotal]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgControles.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormControleEntregas.sgControlesKeyPress(Sender: TObject; var Key: Char);
var
  i: Integer;
  vEntregaId: Integer;
  vValorDigitado: string;
  vLinhaControleId: Integer;
begin
  inherited;
  if CharInSet(Key, ['0'..'9', 'A'..'Z', 'a'..'z' , '.', '-' , '?']) then begin
    vValorDigitado := BuscaDados.BuscarDados(tiTexto, 'Busca digitada', Key);
    if vValorDigitado <> '' then begin
      vEntregaId := SFormatInt(vValorDigitado);
      if vEntregaId = 0 then
        Exit;

      for i := Low(FEntregasControlesIds) to High(FEntregasControlesIds) do begin
        if vEntregaId <> FEntregasControlesIds[i].EntregaId then
          Continue;

        vLinhaControleId := sgControles.Localizar([coControleId], [ NFormat(FEntregasControlesIds[i].ControleEntregaId) ]);
        if vLinhaControleId > -1 then begin
          sgControles.Row := vLinhaControleId;
          miBaixarTransporteClick(Sender);
        end;
      end;
    end
    else
      SetFocus;
  end;
end;

procedure TFormControleEntregas.VerificarRegistro(Sender: TObject);
begin
  inherited;
  FEntregasControlesIds := nil;
end;

end.
