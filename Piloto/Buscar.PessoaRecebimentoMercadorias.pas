unit Buscar.PessoaRecebimentoMercadorias;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca, _Sessao,
  EditHoras, EditLukaData, Vcl.Mask, EditCpfCnpjLuka, EditLuka, Vcl.Buttons,
  Vcl.ExtCtrls;

type
  TInfoEntrega = record
    NomePessoa: string;
    CPFPessoa: string;
    DataHoraEntrega: TDateTime;
    Observacoes: string;
  end;

  TFormBuscarPessoaRecebimentoMercadorias = class(TFormHerancaFinalizar)
    lblCPF_CNPJ: TLabel;
    lbl1: TLabel;
    lbllb12: TLabel;
    lbllb13: TLabel;
    eNome: TEditLuka;
    eCPF: TEditCPF_CNPJ_Luka;
    eDataRetirada: TEditLukaData;
    eHoraRetirada: TEditHoras;
    eObservacoes: TMemo;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(): TRetornoTelaFinalizar<TInfoEntrega>;

implementation

{$R *.dfm}

function Buscar(): TRetornoTelaFinalizar<TInfoEntrega>;
var
  vForm: TFormBuscarPessoaRecebimentoMercadorias;
begin
  vForm := TFormBuscarPessoaRecebimentoMercadorias.Create(Application);

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.NomePessoa      := vForm.eNome.Text;
    Result.Dados.CPFPessoa       := vForm.eCPF.Text;
    Result.Dados.DataHoraEntrega := ToDataHora(vForm.eDataRetirada.AsData, vForm.eHoraRetirada.AsHora);
    Result.Dados.Observacoes     := vForm.eObservacoes.Lines.Text;
  end;
end;

{ TFormBuscarPessoaRecebimentoEntrega }

procedure TFormBuscarPessoaRecebimentoMercadorias.VerificarRegistro(Sender: TObject);
begin
  if eNome.Trim = '' then begin
    Exclamar('� necess�rio informar o nome da pessoa que retirou as mercadorias!');
    SetarFoco(eNome);
    Abort;
  end;

  if not eCPF.getCPF_CNPJOk then begin
    Exclamar('� necess�rio informar o cpf da pessoa que retirou as mercadorias!');
    SetarFoco(eCPF);
    Abort;
  end;

  inherited;
end;


end.
