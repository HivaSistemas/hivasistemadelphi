unit _EntradaNotasFiscaisCalculos;

interface

uses
  _ParametrosEmpresa, _RecordsCadastros, _Sessao, _Biblioteca;

{$M+}

type
  TCalculoImpostos = class
  type
    RecImpostosCalculos = record
      BaseCalculoIcms: Double;
      ValorIcms: Double;

      BaseCalculoIcmsSt: Double;
      ValorIcmsSt: Double;

      BaseCalculoIpi: Double;
      ValorIpi: Double;

      BaseCalculoPis: Double;
      PercentualPis: Double;
      ValorPis: Double;

      BaseCalculoCofins: Double;
      PercentualCofins: Double;
      ValorCofins: Double;

      ValorDifal: Double;
    end;
  private
    FEstadoIdEmpresa: string;
    FEstadoIdFornecedor: string;
    FValorUnitario: Double;
    FQuantidade: Double;
    FQtdeEntradaAltis: Double;
    FValorDesconto: Double;
    FValorOutrasDespesas: Double;
    FValorFrete: Double;
    FCST: string;
    FIndiceReducaoBaseCalculoICMS: Double;
    FPercentualIcmsFornecedor: Double;
    FPercentualIcmsInterno: Double;
    FPercentualIVA: Double;
    FPercentualIPI: Double;
    FPrecoPauta: Double;

    FEmpresaId: Integer;
    FParametrosEmpresa: RecParametrosEmpresa;

    FImpostosCalculados: RecImpostosCalculos;

    procedure CalcularICMS;
    procedure CalcularPIS;
    procedure CalcularCOFINS;
    procedure CalcularIPI;
    procedure CalcularDifal;
  public
    procedure setDados(
      pEstadoIdEmpresa: string;
      pEstadoIdFornecedor: string;
      pValorUnitario: Double;
      pQuantidade: Double;
      pValorDesconto: Double;
      pValorOutrasDespesas: Double;
      pValorFrete: Double;
      pCST: string;
      pIndiceReducaoBaseCalculoICMS: Double;
      pPercentualIcmsFornecedor: Double;
      PPercentualIcmsInterno: Double;
      pPercentualIVA: Double;
      pPercentualIPI: Double;
      pPrecoPauta: Double;
      pEmpresaId: Integer
    ); overload;

    procedure setDados(
      pEstadoIdEmpresa: string;
      pEstadoIdFornecedor: string;
      pValorUnitario: Double;
      pQuantidade: Double;
      pQtdeEntradaAltis: Double;
      pValorDesconto: Double;
      pValorOutrasDespesas: Double;
      pValorICMS: Double;
      pValorICMSST: Double;
      pValorFrete: Double;
      pPercentualIPI: Double;
      pEmpresaId: Integer
    ); overload;

  published
    property BaseCalculoIcms: Double   read FImpostosCalculados.BaseCalculoIcms;
    property ValorIcms: Double         read FImpostosCalculados.ValorIcms;
    property BaseCalculoIcmsSt: Double read FImpostosCalculados.BaseCalculoIcmsSt;
    property ValorIcmsSt: Double       read FImpostosCalculados.ValorIcmsSt;
    property BaseCalculoIpi: Double    read FImpostosCalculados.BaseCalculoIpi;
    property ValorIpi: Double          read FImpostosCalculados.ValorIpi;

    property BaseCalculoPis: Double    read FImpostosCalculados.BaseCalculoPis;
    property PercentualPis: Double     read FImpostosCalculados.PercentualPis;
    property ValorPis: Double          read FImpostosCalculados.ValorPis;

    property BaseCalculoCofins: Double read FImpostosCalculados.BaseCalculoCofins;
    property PercentualCofins: Double  read FImpostosCalculados.PercentualCofins;
    property ValorCofins: Double       read FImpostosCalculados.ValorCofins;
    property ValorDifal: Double        read FImpostosCalculados.ValorDifal;
  end;

implementation

{ TCalculoNotasFiscais }

procedure TCalculoImpostos.setDados(
  pEstadoIdEmpresa,
  pEstadoIdFornecedor: string;
  pValorUnitario,
  pQuantidade,
  pValorDesconto,
  pValorOutrasDespesas,
  pValorFrete: Double;
  pCST: string;
  pIndiceReducaoBaseCalculoICMS,
  pPercentualIcmsFornecedor,
  PPercentualIcmsInterno,
  pPercentualIVA,
  pPercentualIPI,
  pPrecoPauta: Double;
  pEmpresaId: Integer
);
begin
  FEstadoIdEmpresa              := pEstadoIdEmpresa;
  FEstadoIdFornecedor           := pEstadoIdFornecedor;
  FValorUnitario                := pValorUnitario;
  FQuantidade                   := pQuantidade;
  FValorDesconto                := pValorDesconto;
  FValorOutrasDespesas          := pValorOutrasDespesas;
  FValorFrete                   := pValorFrete;
  FCST                          := pCST;
  FIndiceReducaoBaseCalculoICMS := pIndiceReducaoBaseCalculoICMS;
  FPercentualIcmsFornecedor     := pPercentualIcmsFornecedor;
  FPercentualIcmsInterno        := PPercentualIcmsInterno;
  FPercentualIVA                := pPercentualIVA;
  FPercentualIPI                := pPercentualIPI;
  FPrecoPauta                   := pPrecoPauta;

  FEmpresaId := pEmpresaId;
  FParametrosEmpresa := _ParametrosEmpresa.BuscarParametrosEmpresas(Sessao.getConexaoBanco, 0, [pEmpresaId])[0];

  FImpostosCalculados.BaseCalculoIcms   := 0;
  FImpostosCalculados.ValorIcms         := 0;

  FImpostosCalculados.BaseCalculoIcmsSt := 0;
  FImpostosCalculados.ValorIcmsSt       := 0;

  FImpostosCalculados.BaseCalculoIpi    := 0;
  FImpostosCalculados.ValorIpi          := 0;

  FImpostosCalculados.BaseCalculoPis    := 0;
  FImpostosCalculados.PercentualPis     := FParametrosEmpresa.AliquotaPIS;
  FImpostosCalculados.ValorPis          := 0;

  FImpostosCalculados.BaseCalculoCofins := 0;
  FImpostosCalculados.PercentualCofins  := FParametrosEmpresa.AliquotaCOFINS;
  FImpostosCalculados.ValorCofins       := 0;

  CalcularIPI;
  CalcularICMS;
  CalcularPIS;
  CalcularCOFINS;
  CalcularDifal;
end;

procedure TCalculoImpostos.setDados(
  pEstadoIdEmpresa: string;
  pEstadoIdFornecedor: string;
  pValorUnitario: Double;
  pQuantidade: Double;
  pQtdeEntradaAltis: Double;
  pValorDesconto: Double;
  pValorOutrasDespesas: Double;
  pValorICMS: Double;
  pValorICMSST: Double;
  pValorFrete: Double;
  pPercentualIPI: Double;
  pEmpresaId: Integer
);
begin
  FEstadoIdEmpresa              := pEstadoIdEmpresa;
  FEstadoIdFornecedor           := pEstadoIdFornecedor;
  FValorUnitario                := pValorUnitario;
  FQuantidade                   := pQuantidade;
  FQtdeEntradaAltis             := pQtdeEntradaAltis;
  FValorDesconto                := pValorDesconto;
  FValorOutrasDespesas          := pValorOutrasDespesas;
  FValorFrete                   := pValorFrete;
  FPercentualIPI                := pPercentualIPI;

  FEmpresaId := pEmpresaId;
  FParametrosEmpresa := _ParametrosEmpresa.BuscarParametrosEmpresas(Sessao.getConexaoBanco, 0, [pEmpresaId])[0];

  FImpostosCalculados.BaseCalculoIcms   := 0;
  FImpostosCalculados.ValorIcms         := 0;

  FImpostosCalculados.BaseCalculoIcmsSt := 0;
  FImpostosCalculados.ValorIcmsSt       := 0;

  FImpostosCalculados.BaseCalculoIpi    := 0;
  FImpostosCalculados.ValorIpi          := 0;

  FImpostosCalculados.BaseCalculoPis    := 0;
  FImpostosCalculados.PercentualPis     := FParametrosEmpresa.AliquotaPIS;
  FImpostosCalculados.ValorPis          := 0;

  FImpostosCalculados.BaseCalculoCofins := 0;
  FImpostosCalculados.PercentualCofins  := FParametrosEmpresa.AliquotaCOFINS;
  FImpostosCalculados.ValorCofins       := 0;

  CalcularIPI;

  if (FParametrosEmpresa.RegimeTributario = 'SN') or (pValorICMSST > 0) then
    FImpostosCalculados.ValorIcms := 0
  else
    FImpostosCalculados.ValorIcms   := pValorICMS;

  FImpostosCalculados.ValorIcmsSt := pValorICMSST;

  CalcularPIS;
  CalcularCOFINS;
  CalcularDifal;

  // Somente aqui dividimos pela quantidade de entada pra chegar no unit�rio, acima n�o para n�o
  // pois � necess�rios os valores cheios para os calculos
  FImpostosCalculados.ValorIcms   := _Biblioteca.Arredondar( FImpostosCalculados.ValorIcms / pQtdeEntradaAltis, 6 );
  FImpostosCalculados.ValorIcmsSt := _Biblioteca.Arredondar( FImpostosCalculados.ValorIcmsSt / pQtdeEntradaAltis, 6 );
end;

procedure TCalculoImpostos.CalcularIPI;
begin
  FImpostosCalculados.BaseCalculoIpi := FValorUnitario * FQuantidade + FValorOutrasDespesas + FValorFrete;
  FImpostosCalculados.ValorIpi       := FImpostosCalculados.BaseCalculoIpi * FPercentualIPI * 0.01;
end;

procedure TCalculoImpostos.CalcularICMS;
begin
  if Em(FCST, ['00', '000']) then begin// Tributada Integralmente
    FImpostosCalculados.BaseCalculoIcms := (FValorUnitario * FQuantidade) + FImpostosCalculados.ValorIpi;
    FImpostosCalculados.ValorIcms := (FImpostosCalculados.BaseCalculoIcms) * (FPercentualIcmsFornecedor * 0.01);
  end
  else if Em(FCST, ['10', '010']) then begin // Tributada e com cobran�a do ICMS por substitui��o tribut�ria
    if FPrecoPauta = 0 then begin // Se n�o for para utilizar pre�o de pauta
      // ICMS Pr�prio ou cr�dito de ICMS
      FImpostosCalculados.BaseCalculoIcms := FValorUnitario * FQuantidade;
      FImpostosCalculados.ValorIcms := FImpostosCalculados.BaseCalculoIcms * (FPercentualIcmsFornecedor * 0.01);

      // ST
      FImpostosCalculados.BaseCalculoIcmsSt := (FImpostosCalculados.BaseCalculoIcms + FImpostosCalculados.ValorIpi) * (1 + FPercentualIVA * 0.01);
      FImpostosCalculados.ValorIcmsSt := (FImpostosCalculados.BaseCalculoIcmsSt * FPercentualIcmsFornecedor * 0.01) - FImpostosCalculados.ValorIcms;
    end
    else begin
      // ICMS Pr�prio ou cr�dito de ICMS
      FImpostosCalculados.BaseCalculoIcms := FValorUnitario * FQuantidade;
      FImpostosCalculados.ValorIcms := FImpostosCalculados.BaseCalculoIcms * (FPercentualIcmsFornecedor * 0.01);

      // ST
      FImpostosCalculados.BaseCalculoIcmsSt := FPrecoPauta;
      FImpostosCalculados.ValorIcmsSt := (FImpostosCalculados.BaseCalculoIcmsSt * FPercentualIcmsFornecedor) - FImpostosCalculados.ValorIcms;
    end;
  end
  else if Em(FCST, ['20', '020']) then begin// Com redu��o da base de c�lculo
    FImpostosCalculados.BaseCalculoIcms := FValorUnitario * FQuantidade * FIndiceReducaoBaseCalculoICMS;
    FImpostosCalculados.ValorIcms := FImpostosCalculados.BaseCalculoIcms * (FPercentualIcmsFornecedor * 0.01);
  end
  else if Em(FCST, ['30', '030']) then begin // Isenta ou n�o-tributada e com cobran�a do ICMS por substitui��o tribut�ria

  end
  else if Em(FCST, ['40', '040']) then begin // Isenta

  end
  else if Em(FCST, ['41', '041']) then begin // N�o-tributada

  end
  else if Em(FCST, ['50', '050']) then begin // Suspens�o

  end
  else if Em(FCST, ['51', '051']) then begin // Diferimento

  end
  else if Em(FCST, ['60', '060']) then begin // ICMS cobrado anteriormente por substitui��o tribut�ria

  end
  else if Em(FCST, ['70', '070']) then begin // Com redu��o da base de c�lculo e com cobran�a de ICMS por substitui��o tribut�ria
    FImpostosCalculados.BaseCalculoIcms := FValorUnitario * FQuantidade * FIndiceReducaoBaseCalculoICMS;
    FImpostosCalculados.ValorIcms := FImpostosCalculados.BaseCalculoIcms * (FPercentualIcmsFornecedor * 0.01);

    FImpostosCalculados.BaseCalculoIcmsSt := (FValorUnitario * FQuantidade + FImpostosCalculados.ValorIpi) * (1 + FPercentualIVA * 0.01);
    FImpostosCalculados.BaseCalculoIcmsSt := FImpostosCalculados.BaseCalculoIcmsSt * FIndiceReducaoBaseCalculoICMS;

    FImpostosCalculados.ValorIcmsSt := (FImpostosCalculados.BaseCalculoIcmsSt * FPercentualIcmsFornecedor) - FImpostosCalculados.ValorIcms;
  end
  else if Em(FCST, ['90', '090']) then begin // Outras

  end;
end;

procedure TCalculoImpostos.CalcularPIS;
begin
  FImpostosCalculados.BaseCalculoPis := (FValorUnitario * FQuantidade) - FValorDesconto + FValorOutrasDespesas + FImpostosCalculados.ValorIpi + FImpostosCalculados.ValorIcmsSt;
  FImpostosCalculados.ValorPis := Arredondar(FImpostosCalculados.BaseCalculoPis * FImpostosCalculados.PercentualPIS * 0.01 / FQtdeEntradaAltis, 6);
end;

procedure TCalculoImpostos.CalcularCOFINS;
begin
  FImpostosCalculados.BaseCalculoCofins := (FValorUnitario * FQuantidade) - FValorDesconto + FValorOutrasDespesas + FImpostosCalculados.ValorIpi + FImpostosCalculados.ValorIcmsSt;
  FImpostosCalculados.ValorCofins := Arredondar(FImpostosCalculados.BaseCalculoCofins * FImpostosCalculados.PercentualCofins * 0.01 / FQtdeEntradaAltis, 6);
end;

procedure TCalculoImpostos.CalcularDifal;
begin
  FImpostosCalculados.ValorDifal := 0;

  if FImpostosCalculados.ValorIcmsSt > 0 then
    Exit;

  if FEstadoIdEmpresa = FEstadoIdFornecedor then
    Exit;

  // Se n�o for empresas do super simples saindo fora
  if FParametrosEmpresa.RegimeTributario <> 'SN' then
    Exit;

  FImpostosCalculados.ValorDifal := (FValorUnitario * FQuantidade) * IIfDbl( FPercentualIcmsFornecedor = 4, 0.0787, 0.0449 ) / FQtdeEntradaAltis;
end;

end.
