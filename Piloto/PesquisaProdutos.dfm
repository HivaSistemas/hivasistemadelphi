inherited FormPesquisaProdutos: TFormPesquisaProdutos
  Caption = 'Pesquisa de  produtos'
  ClientHeight = 423
  ClientWidth = 759
  ExplicitWidth = 767
  ExplicitHeight = 454
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Top = 106
    Width = 759
    Height = 317
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    ColCount = 9
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goThumbTracking]
    PopupMenu = pmOpcoesGrid
    ScrollBars = ssBoth
    TabOrder = 2
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Marca'
      'Und'
      'Ativo'
      'C'#243'digo Original'
      'Nome de compra'
      'Fabricante'
      ''
      '')
    OnGetCellColor = sgPesquisaGetCellColor
    RealColCount = 9
    Indicador = True
    ExplicitTop = 106
    ExplicitWidth = 759
    ExplicitHeight = 317
    ColWidths = (
      28
      55
      272
      129
      48
      54
      131
      231
      223)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Left = 11
    Top = 331
    TabOrder = 3
    Text = '1'
    ExplicitLeft = 11
    ExplicitTop = 331
  end
  inherited pnFiltro1: TPanel
    Top = 60
    Width = 759
    TabOrder = 1
    ExplicitTop = 60
    ExplicitWidth = 759
    object lblMarca: TLabel [2]
      Left = 545
      Top = 4
      Width = 33
      Height = 14
      Caption = 'Marca'
    end
    inherited eValorPesquisa: TEditLuka
      Width = 338
      ExplicitWidth = 338
    end
    inherited cbOpcoesPesquisa: TComboBoxLuka
      Left = -1
      OnChange = cbOpcoesPesquisaChange
      ExplicitLeft = -1
    end
    object eMarca: TEditLuka
      Tag = 999
      Left = 545
      Top = 18
      Width = 208
      Height = 22
      Anchors = [akLeft, akTop, akRight]
      CharCase = ecUpperCase
      Ctl3D = True
      ParentCtl3D = False
      TabOrder = 2
      OnKeyDown = eMarcaKeyDown
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
  end
  object pnFiltroExtra: TPanel
    Left = 0
    Top = 0
    Width = 759
    Height = 60
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    object ckAtivo: TCheckBox
      Left = 3
      Top = 20
      Width = 70
      Height = 17
      Caption = 'Ativos'
      TabOrder = 1
    end
    object ckInativos: TCheckBox
      Left = 3
      Top = 38
      Width = 70
      Height = 17
      Caption = 'Inativos'
      TabOrder = 7
    end
    object ckAtivoImobilizado: TCheckBox
      Left = 182
      Top = 20
      Width = 115
      Height = 17
      Caption = 'Ativo Imobilizado'
      TabOrder = 3
    end
    object ckBloquearParaCompras: TCheckBox
      Left = 593
      Top = 20
      Width = 170
      Height = 17
      Caption = 'Bloqueado para compras'
      TabOrder = 6
      OnKeyDown = ProximoCampo
    end
    object ckBloquearParaVendas: TCheckBox
      Left = 593
      Top = 38
      Width = 160
      Height = 17
      Caption = 'Bloqueado para vendas'
      TabOrder = 12
      OnKeyDown = ProximoCampo
    end
    object ckPermitirDevolucao: TCheckBox
      Left = 300
      Top = 38
      Width = 125
      Height = 17
      Caption = 'Permite Devolu'#231#227'o'
      TabOrder = 10
    end
    object ckRevenda: TCheckBox
      Left = 73
      Top = 20
      Width = 105
      Height = 17
      Caption = 'Revenda'
      TabOrder = 2
    end
    object ckServico: TCheckBox
      Left = 182
      Top = 38
      Width = 115
      Height = 17
      Caption = 'Servico'
      TabOrder = 9
    end
    object ckSobEncomenda: TCheckBox
      Left = 300
      Top = 20
      Width = 125
      Height = 17
      Caption = 'Sob Encomenda'
      TabOrder = 4
    end
    object ckUsoConsumo: TCheckBox
      Left = 73
      Top = 38
      Width = 105
      Height = 17
      Caption = 'Uso e Consumo'
      TabOrder = 8
    end
    object stctxtlk3: TStaticTextLuka
      Left = 1
      Top = 1
      Width = 757
      Height = 15
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      Caption = 'Filtros Adicionais'
      Color = 38619
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 0
      Transparent = False
      AsInt = 0
      TipoCampo = tcTexto
      CasasDecimais = 0
    end
    object ckInativarZeraEstoque: TCheckBox
      Left = 431
      Top = 20
      Width = 160
      Height = 17
      Caption = 'Inativar ao Zerar Estoque'
      TabOrder = 5
      OnKeyDown = ProximoCampo
    end
    object ckAceitaEstoqueNegativo: TCheckBox
      Left = 431
      Top = 38
      Width = 160
      Height = 17
      Caption = 'Aceita Estoque Negativo'
      TabOrder = 11
      OnKeyDown = ProximoCampo
    end
    object ckOmitir: TCheckBox
      Left = 593
      Top = 2
      Width = 170
      Height = 15
      Caption = 'Omitir Filtros Selecionados'
      Color = 10066176
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 13
    end
  end
  object pmOpcoesGrid: TPopupMenu
    Left = 568
    Top = 296
    object FiltrosExtra1: TMenuItem
      Caption = 'Filtros Extra'
      Enabled = False
      Visible = False
      OnClick = FiltrosExtra1Click
    end
    object MarcaDemarcaTodosCtrlespao1: TMenuItem
      Caption = 'Marca/Demarca Todos (Ctrl + espa'#231'o)'
      Enabled = False
      OnClick = MarcaDemarcaTodosCtrlespao1Click
    end
  end
end
