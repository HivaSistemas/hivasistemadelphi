inherited FormBuscarReferenciaNFe: TFormBuscarReferenciaNFe
  Caption = 'Busca de refer'#202'ncia NFe'
  ClientHeight = 85
  ClientWidth = 554
  OnShow = FormShow
  ExplicitWidth = 560
  ExplicitHeight = 114
  PixelsPerInch = 96
  TextHeight = 14
  object lbl28: TLabel [0]
    Left = 194
    Top = 3
    Width = 56
    Height = 14
    Caption = 'Chave NFe'
  end
  inherited pnOpcoes: TPanel
    Top = 48
    Width = 554
    ExplicitTop = 48
    ExplicitWidth = 554
  end
  object rgTipoReferencia: TRadioGroupLuka
    Left = 3
    Top = 3
    Width = 185
    Height = 38
    Caption = ' Tipo de refer'#234'ncia '
    Columns = 2
    Items.Strings = (
      'NFe'
      'NFCe')
    TabOrder = 1
    Valores.Strings = (
      'N'
      'C')
  end
  object eChaveAcessoNFe: TMaskEdit
    Left = 194
    Top = 17
    Width = 355
    Height = 22
    EditMask = '9999.9999.9999.9999.9999.9999.9999.9999.9999.9999.9999;1;_'
    MaxLength = 54
    TabOrder = 2
    Text = '    .    .    .    .    .    .    .    .    .    .    '
  end
end
