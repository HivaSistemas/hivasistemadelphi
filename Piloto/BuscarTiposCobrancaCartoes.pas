unit BuscarTiposCobrancaCartoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _Biblioteca,
  Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _TiposCobranca, _RecordsCadastros, BuscarDadosTransacaoCartao;

type
  RecDadosCartao = record
    CobrancaId: Integer;
    CodigoAutorizacao: string;
    NsuTef: string;
    NumeroCartao: string;
  end;

  TFormBuscarTiposCobrancaCartoes = class(TFormHerancaFinalizar)
    sgCobrancas: TGridLuka;
    procedure sgCobrancasDblClick(Sender: TObject);
    procedure sgCobrancasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgCobrancasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    FNumeroCartao: string;
    FNSU: string;
    FCodigoAutorizacao: string;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(
  pQtdeParcelas: Integer;
  pTipoCartao: string;
  pNumeroCartao: string;
  pNSU: string;
  pCodigoAutorizacao: string
): TRetornoTelaFinalizar<RecDadosCartao>;

implementation

{$R *.dfm}

const
  coCobrancaId = 0;
  coNome       = 1;

  (* Ocultas *)
  coCodigoAutorizacao = 2;
  coNsuTef            = 3;
  coNumeroCartao      = 4;

function Buscar(
  pQtdeParcelas: Integer;
  pTipoCartao: string;
  pNumeroCartao: string;
  pNSU: string;
  pCodigoAutorizacao: string
): TRetornoTelaFinalizar<RecDadosCartao>;
var
  i: Integer;

  vCobrancas: TArray<RecTiposCobranca>;
  vForm: TFormBuscarTiposCobrancaCartoes;
begin
  Result.RetTela := trCancelado;

  vCobrancas := _TiposCobranca.BuscarTiposCobrancas(Sessao.getConexaoBanco, 3, [IIfInt(pTipoCartao = 'D', 1, pQtdeParcelas), pTipoCartao], 'R');
  if vCobrancas = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vForm := TFormBuscarTiposCobrancaCartoes.Create(nil);

  vForm.FNumeroCartao := pNumeroCartao;
  vForm.FNSU := pNSU;
  vForm.FCodigoAutorizacao := pCodigoAutorizacao;

  for i := Low(vCobrancas) to High(vCobrancas) do begin
    vForm.sgCobrancas.Cells[coCobrancaId, i + 1] := NFormat(vCobrancas[i].CobrancaId);
    vForm.sgCobrancas.Cells[coNome, i + 1]       := vCobrancas[i].Nome;
  end;
  vForm.sgCobrancas.SetLinhasGridPorTamanhoVetor( Length(vCobrancas) );

  if Result.Ok(vForm.ShowModal) then begin
    Result.Dados.CobrancaId        := SFormatInt( vForm.sgCobrancas.Cells[coCobrancaId, vForm.sgCobrancas.Row] );
    Result.Dados.CodigoAutorizacao := vForm.sgCobrancas.Cells[coCodigoAutorizacao, vForm.sgCobrancas.Row];
    Result.Dados.NsuTef            := vForm.sgCobrancas.Cells[coNsuTef, vForm.sgCobrancas.Row];
    Result.Dados.NumeroCartao      := vForm.sgCobrancas.Cells[coNumeroCartao, vForm.sgCobrancas.Row];
  end;
end;

procedure TFormBuscarTiposCobrancaCartoes.sgCobrancasDblClick(Sender: TObject);
var
  vDadosTransacao: TRetornoTelaFinalizar<RecDadosTransacao>;
begin
  inherited;

  vDadosTransacao := BuscarDadosTransacaoCartao.Buscar(FNumeroCartao, FNSU, FCodigoAutorizacao);
  if vDadosTransacao.BuscaCancelada then
    Abort;

  sgCobrancas.Cells[coCodigoAutorizacao, sgCobrancas.Row] := vDadosTransacao.Dados.CodigoAutorizacao;
  sgCobrancas.Cells[coNsuTef, sgCobrancas.Row]            := vDadosTransacao.Dados.Nsu;
  sgCobrancas.Cells[coNumeroCartao, sgCobrancas.Row]      := vDadosTransacao.Dados.NumeroCartao;

  ModalResult := mrOk;
end;

procedure TFormBuscarTiposCobrancaCartoes.sgCobrancasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coCobrancaId then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgCobrancas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscarTiposCobrancaCartoes.sgCobrancasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sgCobrancasDblClick(Sender);
end;

procedure TFormBuscarTiposCobrancaCartoes.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if
    (sgCobrancas.Cells[coCodigoAutorizacao, sgCobrancas.Row] = '') or
    (sgCobrancas.Cells[coNsuTef, sgCobrancas.Row] = '') or
    (sgCobrancas.Cells[coNumeroCartao, sgCobrancas.Row] = '')
  then
    sgCobrancasDblClick(Sender);
end;

end.
