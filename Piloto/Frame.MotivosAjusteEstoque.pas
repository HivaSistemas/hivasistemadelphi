unit Frame.MotivosAjusteEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _Sessao, _Biblioteca,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _MotivosAjusteEstoque, Pesquisa.MotivosAjusteEstoque,
  _RecordsCadastros, System.Math, Vcl.Buttons, Vcl.Menus;

type
  TFrMotivosAjusteEstoque = class(TFrameHenrancaPesquisas)
  public
    function GetMotivoAjusteEstoque(pLinha: Integer = -1): RecMotivoAjusteEstoque;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrameMotivosAjusteEstoque }

function TFrMotivosAjusteEstoque.AdicionarDireto: TObject;
var
  vMotivos: TArray<RecMotivoAjusteEstoque>;
begin
  vMotivos := _MotivosAjusteEstoque.BuscarMotivoAjusteEstoques(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vMotivos = nil then
    Result := nil
  else
    Result := vMotivos[0];
end;

function TFrMotivosAjusteEstoque.AdicionarPesquisando: TObject;
begin
  Result := Pesquisa.MotivosAjusteEstoque.Pesquisar();
end;

function TFrMotivosAjusteEstoque.GetMotivoAjusteEstoque(pLinha: Integer): RecMotivoAjusteEstoque;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecMotivoAjusteEstoque(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrMotivosAjusteEstoque.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecMotivoAjusteEstoque(FDados[i]).motivo_ajuste_id = RecMotivoAjusteEstoque(pSender).motivo_ajuste_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrMotivosAjusteEstoque.MontarGrid;
var
  i: Integer;
  vSender: RecMotivoAjusteEstoque;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecMotivoAjusteEstoque(FDados[i]);
      AAdd([IntToStr(vSender.motivo_ajuste_id), vSender.descricao]);
    end;
  end;
end;

end.
