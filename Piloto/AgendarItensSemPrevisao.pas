unit AgendarItensSemPrevisao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.Grids, GridLuka, _RecordsOrcamentosVendas,
  Vcl.Mask, EditLukaData, Vcl.StdCtrls, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Pesquisa.Orcamentos,
  _Biblioteca, _Sessao, _EntregasItensSemPrevisao, _Orcamentos, _HerancaCadastro, _RecordsEspeciais,
  EditHoras, Informacoes.Orcamento, CheckBoxLuka, Vcl.Menus, _Imagens, BuscarDefinicaoLotesVenda, _EstoquesDivisao;

type
  RecLotes = record
    ItemId: Integer;
    Lotes: TArray<RecDefinicoesLoteVenda>;
  end;

  TFormAgendarItensSemPrevisao = class(TFormHerancaCadastroCodigo)
    lb2: TLabel;
    lb12: TLabel;
    lb3: TLabel;
    sbInfoPedido: TSpeedButton;
    sbInfoCliente: TSpeedButton;
    eCliente: TEditLuka;
    eVendedor: TEditLuka;
    eDataCadastro: TEditLukaData;
    lb4: TLabel;
    eDataRecebimento: TEditLukaData;
    eObservacoes: TEditLuka;
    lb1: TLabel;
    sgProdutos: TGridLuka;
    eDataEntrega: TEditLukaData;
    eHoraEntrega: TEditHoras;
    Label2: TLabel;
    pmItens: TPopupMenu;
    miDefinirQuantidadeTodosItens: TMenuItem;
    miRemoverQuantidadeTodosItensF8: TMenuItem;
    miN1: TMenuItem;
    sgLocais: TGridLuka;
    miDefinirLotes: TPanel;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgProdutosArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); override;
    procedure sbInfoPedidoClick(Sender: TObject);
    procedure miDefinirQuantidadeTodosItensClick(Sender: TObject);
    procedure miRemoverQuantidadeTodosItensF8Click(Sender: TObject);
    procedure miDefinirLotesClick(Sender: TObject);
    procedure sgProdutosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure FormCreate(Sender: TObject);
    procedure sgProdutosClick(Sender: TObject);
    procedure sgLocaisDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgLocaisArrumarGrid(Sender: TObject; ARow, ACol: Integer;
      var TextCell: string);
    procedure sgProdutosEnter(Sender: TObject);
  private
    FLotes: TArray<RecLotes>;
    FLocais: TArray<RecDefinicoesLocaisVenda>;

    procedure PreencherRegistro(pOrcamento: RecOrcamentos);
  protected
    procedure BuscarRegistro; override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure GravarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TTFormAgendarItensSemPrevisao }

const
  coProdutoId    = 0;
  coNome         = 1;
  coMarca        = 2;
  coUnidade      = 3;
  coSaldo        = 4;
  coQtdeAgendar  = 5;
  coDefiniuLotes = 6;
  (* Ocultas *)
  coItemId              = 7;
  coTipoControleEstoque = 8;
  coMultiploVenda       = 9;

  //sgLocais
  loEmpresaId          = 0;
  loNomeEmpresa        = 1;
  loLocalId            = 2;
  loNomeLocal          = 3;
  loDisponivel         = 4;
  loQuantidadeEntregar = 5;

  //Colunas ocultas
  loPermiteNegativo    = 6;
  loPosicaoArray       = 7;

procedure TFormAgendarItensSemPrevisao.BuscarRegistro;
var
  vOrcamento: TArray<RecOrcamentos>;

  procedure Erro(pTexto: string);
  begin
    _Biblioteca.Exclamar(pTexto);
    SetarFoco(eID);
    eID.Clear;
    Abort;
  end;

begin
  vOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 4, [eID.AsInt]);
  if vOrcamento = nil then
    Erro(coNenhumRegistroEncontrado)
  else if vOrcamento[0].status = co_orcamento_cancelado then
    Erro('Este or�amento est� cancelado!')
  else if vOrcamento[0].status = co_orcamento_bloqueado then
    Erro('Este or�amento est� bloquedo, por favor, verifique!')
  else if not Em(vOrcamento[0].status, [coOrcamentoRecebido, coPedidoReceberEntrega]) then
    Erro('Este or�amento n�o est� recebido, por favor, verifique!');

  inherited;
  PreencherRegistro(vOrcamento[0]);
end;

procedure TFormAgendarItensSemPrevisao.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key <> VK_RETURN then
    Exit;

  if eID.Text = '' then begin
    _Biblioteca.Exclamar('C�digo do pedido n�o informado!');
    SetarFoco(eID);
    Abort;
  end;

  inherited;
end;

procedure TFormAgendarItensSemPrevisao.FormCreate(Sender: TObject);
begin
  inherited;
  sgLocais.Visible := Sessao.getParametros.DefinirLocalManual = 'S';
  if not sgLocais.Visible then
    sgProdutos.Height := 305;

end;

procedure TFormAgendarItensSemPrevisao.GravarRegistro(Sender: TObject);
var
  i: Integer;
  j: Integer;
  indice: Integer;
  vRetBanco: RecRetornoBD;
  vDataHoraEntrega: TDateTime;
  vItens: TArray<RecEntregasItensSemPrevisao>;

  function getLotes( pItemId: Integer ): TArray<RecDefinicoesLoteVenda>;
  var
    i: Integer;
  begin
    Result := nil;
    for i := Low(FLotes) to High(FLotes) do begin
      if FLotes[i].ItemId <> pItemId then
        Continue;

      Result := FLotes[i].Lotes;
      Break;
    end;
  end;

begin
  inherited;
  vItens := nil;
  for i := 1 to sgProdutos.RowCount - 1 do begin
    if SFormatCurr(sgProdutos.Cells[coQtdeAgendar, i]) = 0 then
      Continue;

    SetLength(vItens, Length(vItens) + 1);
    vItens[High(vItens)].ProdutoId  := SFormatInt(sgProdutos.Cells[coProdutoId, i]);
    vItens[High(vItens)].ItemId     := SFormatInt(sgProdutos.Cells[coItemId, i]);
    vItens[High(vItens)].Quantidade := SFormatDouble(sgProdutos.Cells[coQtdeAgendar, i]);
    vItens[High(vItens)].Lotes      := getLotes( vItens[High(vItens)].ItemId );

    if sgLocais.Visible then begin

      for j := Low(FLocais) to High(FLocais) do begin
        if vItens[High(vItens)].ProdutoId <> FLocais[j].ProdutoId then
          Continue;

        if FLocais[j].QuantidadeEntregar = 0 then
          Continue;

        indice := Length(vItens[High(vItens)].Locais);
        SetLength(vItens[High(vItens)].Locais, indice + 1);
        vItens[High(vItens)].Locais[indice] := FLocais[j];
      end;
    end;
  end;

  if vItens = nil then begin
    _Biblioteca.Exclamar('Nenhuma quantidade dos itens foi definida para agendamento!');
    SetarFoco(sgProdutos);
    Abort;
  end;

  vDataHoraEntrega := eDataEntrega.AsData + eHoraEntrega.AsHora;

  vRetBanco :=
    _EntregasItensSemPrevisao.AgendarItens(
      Sessao.getConexaoBanco,
      Sessao.getEmpresaLogada.EmpresaId,
      eID.AsInt,
      vDataHoraEntrega,
      vItens,
      Sessao.getParametros.DefinirLocalManual
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  FLocais := nil;
  _Biblioteca.Informar('Agendamento de entrega realizado com sucesso!');
end;

procedure TFormAgendarItensSemPrevisao.miDefinirLotesClick(Sender: TObject);
var
  vLotes: TRetornoTelaFinalizar< TArray<RecDefinicoesLoteVenda> >;

  function ExisteProdutoVetor(pItemId: Integer): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i := Low(FLotes) to High(FLotes) do begin
      Result := FLotes[i].ItemId = pItemId;
      if Result then
        Exit;
    end;
  end;

begin
  inherited;
  if not Em(sgProdutos.Cells[coTipoControleEstoque, sgProdutos.Row], ['P', 'G', 'L']) then begin
    _Biblioteca.Exclamar('O produto selecionado n�o possui controle do tipo "Piso", "Grade" ou "Lote"!');
    Exit;
  end;

  if SFormatCurr(sgProdutos.Cells[coQtdeAgendar, sgProdutos.Row]) = 0 then begin
    _Biblioteca.Exclamar('Nenhuma quantidade foi definida para agendamento!');
    Exit;
  end;

  vLotes :=
    BuscarDefinicaoLotesVenda.Buscar(
      Sessao.getEmpresaLogada.EmpresaId,
      SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]),
      sgProdutos.Cells[coNome, sgProdutos.Row],
      0,
      0,
      SFormatDouble(sgProdutos.Cells[coQtdeAgendar, sgProdutos.Row]),
      SFormatDouble(sgProdutos.Cells[coMultiploVenda, sgProdutos.Row]),
      ttAgendarItensSemPrevisao
    );

  if vLotes.BuscaCancelada then
    Exit;

  if not ExisteProdutoVetor(SFormatInt(sgProdutos.Cells[coItemId, sgProdutos.Row])) then begin
    SetLength(FLotes, Length(FLotes) + 1);

    FLotes[High(FLotes)].ItemId := SFormatInt(sgProdutos.Cells[coItemId, sgProdutos.Row]);
    FLotes[High(FLotes)].Lotes  := vLotes.Dados;
  end;

  sgProdutos.Cells[coDefiniuLotes, sgProdutos.Row] := charSelecionado;
  sgProdutos.Repaint;
end;

procedure TFormAgendarItensSemPrevisao.miDefinirQuantidadeTodosItensClick(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  for i := 1 to sgProdutos.RowCount -1 do
    sgProdutos.Cells[coQtdeAgendar, i] := sgProdutos.Cells[coSaldo, i];
end;

procedure TFormAgendarItensSemPrevisao.miRemoverQuantidadeTodosItensF8Click(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  for i := 1 to sgProdutos.RowCount -1 do
    sgProdutos.Cells[coQtdeAgendar, i] := '';
end;

procedure TFormAgendarItensSemPrevisao.Modo(pEditando: Boolean);
begin
  inherited;
  FLotes := nil;

  _Biblioteca.Habilitar([
    eCliente,
    eVendedor,
    eDataCadastro,
    eDataRecebimento,
    eObservacoes,
    sgProdutos,
    sbInfoPedido,
    sbInfoCliente,
    eDataEntrega,
    eHoraEntrega,
    sgLocais],
    pEditando
  );

  _Biblioteca.Habilitar([eId], not pEditando);

  if pEditando then begin
    _Biblioteca.SomenteLeitura([eObservacoes], True);
    sgProdutos.Col := coQtdeAgendar;
    SetarFoco(eDataEntrega);
  end
  else begin
    FLocais := nil;
  end;
end;

procedure TFormAgendarItensSemPrevisao.PesquisarRegistro;
var
  vRetTela: TRetornoTelaFinalizar<RecOrcamentos>;
begin
  vRetTela := Pesquisa.Orcamentos.Pesquisar(tpAgendarItensSemPrevisao);
  if vRetTela.BuscaCancelada then
    Exit;

  Modo(True);
  PreencherRegistro(vRetTela.Dados);
end;

procedure TFormAgendarItensSemPrevisao.PreencherRegistro(pOrcamento: RecOrcamentos);
var
  i: Integer;
  j: Integer;
  indice: Integer;
  vItens: TArray<RecEntregasItensSemPrevisao>;
  locais: TArray<RecDefinicoesLocaisVenda>;
begin
  vItens := _EntregasItensSemPrevisao.BuscarEntregasItensSemPrevisao(Sessao.getConexaoBanco, 1, [pOrcamento.orcamento_id]);
  if vItens = nil then begin
    Exclamar('N�o foi encontrado nenhum produto para devolu��o no pedido digitado!');
    Modo(False);
    Exit;
  end;

  for i := Low(vItens) to High(vItens) do begin
    locais := nil;

    locais := _EstoquesDivisao.BuscarEstoquesLocaisDisponiveis(
      Sessao.getConexaoBanco,
      Sessao.getEmpresaLogada.EmpresaId,
      vItens[i].ProdutoId
    );

    if locais = nil then
      Continue;

    for j := Low(locais) to High(locais) do begin
      indice := Length(FLocais);
      SetLength(FLocais, Length(FLocais) + 1);

      FLocais[indice].ProdutoId := locais[j].ProdutoId;
      FLocais[indice].LocalId := locais[j].LocalId;
      FLocais[indice].Nome := locais[j].Nome;
      FLocais[indice].Disponivel := locais[j].Disponivel;
      FLocais[indice].EmpresaId := Locais[j].EmpresaId;
      FLocais[indice].NomeEmpresa := locais[j].NomeEmpresa;
    end;
  end;

  eID.SetInformacao(pOrcamento.orcamento_id);
  eCliente.SetInformacao(pOrcamento.cliente_id, pOrcamento.nome_cliente);
  eVendedor.SetInformacao(pOrcamento.vendedor_id, pOrcamento.nome_vendedor);
  eDataCadastro.AsData := pOrcamento.data_cadastro;
  eDataRecebimento.AsData := pOrcamento.data_hora_recebimento;

  for i := Low(vItens) to High(vItens) do begin
    sgProdutos.Cells[coProdutoId, i + 1]    := _Biblioteca.NFormat(vItens[i].ProdutoId);
    sgProdutos.Cells[coNome, i + 1]         := vItens[i].Nome;
    sgProdutos.Cells[coMarca, i + 1]        := vItens[i].Marca;
    sgProdutos.Cells[coUnidade, i + 1]      := vItens[i].Unidade;
    sgProdutos.Cells[coSaldo, i + 1]        := _Biblioteca.NFormatEstoque(vItens[i].Saldo);
    sgProdutos.Cells[coQtdeAgendar, i + 1]  := '';
    sgProdutos.Cells[coItemId, i + 1]       := _Biblioteca.NFormat(vItens[i].ItemId);

    if Em(vItens[i].TipoControleEstoque, ['P', 'L', 'G']) then
      sgProdutos.Cells[coDefiniuLotes, i + 1] := charNaoSelecionado
    else
      sgProdutos.Cells[coDefiniuLotes, i + 1] := charSelecionado;

    sgProdutos.Cells[coTipoControleEstoque, i + 1] := vItens[i].TipoControleEstoque;
    sgProdutos.Cells[coMultiploVenda, i + 1]       := NFormatNEstoque(vItens[i].MultiploVenda);
  end;

  sgProdutos.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor(Length(vItens));
end;

procedure TFormAgendarItensSemPrevisao.sbInfoPedidoClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(eId.AsInt);
end;

procedure TFormAgendarItensSemPrevisao.sgLocaisArrumarGrid(Sender: TObject;
  ARow, ACol: Integer; var TextCell: string);
var
  i: Integer;
  posicao: Integer;
  quantidadeInformada: Double;
  quantidadePermitida: Double;
  quantidadeInformadaLocal: Double;
  aceitaNegativo: Boolean;
begin
  inherited;
  quantidadePermitida := SFormatCurr(sgProdutos.Cells[coSaldo, sgProdutos.Row]);

  quantidadeInformada := SFormatDouble(TextCell);

  if not ValidarMultiplo(quantidadeInformada, SFormatDouble(sgProdutos.Cells[coMultiploVenda, sgProdutos.Row])) then begin
    TextCell := NFormatEstoque(0);
    Exit;
  end;

  aceitaNegativo := sgLocais.Cells[loPermiteNegativo, ARow] = 'S';

  for i := 1 to sgLocais.RowCount - 1 do begin

    quantidadeInformadaLocal := SFormatDouble(sgLocais.Cells[loQuantidadeEntregar, i]);

    if (ACol = loQuantidadeEntregar) and (i <> ARow) then begin
      quantidadeInformada := quantidadeInformada + SFormatDouble(sgLocais.Cells[loQuantidadeEntregar, i]);
    end;

    if (not aceitaNegativo) and (quantidadeInformadaLocal > 0) then begin
      if (SFormatDouble(sgLocais.Cells[loDisponivel, i]) - quantidadeInformadaLocal) < 0 then begin
        Exclamar('O produto ' + sgProdutos.Cells[coProdutoId, sgProdutos.Row] + ' - ' + sgProdutos.Cells[coNome, sgProdutos.Row] + ' n�o permite estoque negativo!');
        TextCell := NFormatEstoque(0);
        Exit;
      end;
    end;
  end;

  if quantidadePermitida < quantidadeInformada then begin
    Exclamar('A quantidade definida n�o pode ser maior que a quantidade da venda!');
    TextCell := NFormatEstoque(0);
    Exit;
  end;

  posicao := SFormatInt(sgLocais.Cells[loPosicaoArray, ARow]);

  FLocais[posicao].QuantidadeEntregar := SFormatDouble(sgLocais.Cells[loQuantidadeEntregar, ARow]);
  sgLocais.Cells[ACol, ARow]   := NFormatEstoque(FLocais[posicao].QuantidadeEntregar);
end;

procedure TFormAgendarItensSemPrevisao.sgLocaisDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[loLocalId, loDisponivel, loQuantidadeEntregar] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgLocais.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormAgendarItensSemPrevisao.sgProdutosArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  TextCell := NFormatNEstoque(SFormatDouble(TextCell));

  if not ValidarMultiplo(SFormatDouble(sgProdutos.Cells[ACol, sgProdutos.Row]), SFormatDouble(sgProdutos.Cells[coMultiploVenda, sgProdutos.Row])) then begin
    SetarFoco(sgProdutos);
    TextCell := '';
    Exit;
  end;

  sgProdutos.Cells[coDefiniuLotes, ARow] := charSelecionado;
  if Em(sgProdutos.Cells[coTipoControleEstoque, ARow], ['P', 'L', 'G']) and (SFormatCurr(sgProdutos.Cells[coQtdeAgendar, ARow]) > 0) then
    sgProdutos.Cells[coDefiniuLotes, ARow] := charNaoSelecionado;
end;

procedure TFormAgendarItensSemPrevisao.sgProdutosClick(Sender: TObject);
var
  i: Integer;
  indice: Integer;
begin
  inherited;
  sgLocais.ClearGrid;

  if Em(sgProdutos.Cells[coTipoControleEstoque, sgProdutos.Row], ['P', 'G', 'L']) then
    Exit;

  indice := 0;

  for i := Low(FLocais) to High(FLocais) do begin
    if Flocais[i].ProdutoId <> SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]) then
      Continue;

    sgLocais.Cells[loEmpresaId, indice + 1]      := NFormat(FLocais[i].EmpresaId);
    sgLocais.Cells[loNomeEmpresa, indice + 1]    := FLocais[i].NomeEmpresa;
    sgLocais.Cells[loLocalId, indice + 1]      := NFormat(FLocais[i].LocalId);
    sgLocais.Cells[loNomeLocal, indice + 1]    := FLocais[i].nome;
    sgLocais.Cells[loDisponivel, indice + 1]   := NFormatEstoque(FLocais[i].Disponivel);
    sgLocais.Cells[loQuantidadeEntregar, indice + 1]   := NFormatEstoque(FLocais[i].QuantidadeEntregar);
    sgLocais.Cells[loPermiteNegativo, indice + 1] := FLocais[i].AceitaEstoqueNegativo;
    sgLocais.Cells[loPosicaoArray, indice + 1] := NFormat(i);

    Inc(indice);
  end;

  sgLocais.RowCount := IIfInt(indice + 1 > 2, indice + 1, 2);
end;

procedure TFormAgendarItensSemPrevisao.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coProdutoId, coSaldo, coQtdeAgendar] then
    vAlinhamento := taRightJustify
  else if ACol = coDefiniuLotes then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormAgendarItensSemPrevisao.sgProdutosEnter(Sender: TObject);
begin
  inherited;
  sgProdutosClick(nil);
end;

procedure TFormAgendarItensSemPrevisao.sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coQtdeAgendar then begin
    AFont.Color := coCorFonteEdicao1;
    ABrush.Color := coCorCelulaEdicao1;
  end;
end;

procedure TFormAgendarItensSemPrevisao.sgProdutosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coDefiniuLotes then begin
    if sgPRodutos.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgPRodutos.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormAgendarItensSemPrevisao.sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := ACol in[coQtdeAgendar];
end;

procedure TFormAgendarItensSemPrevisao.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  j: Integer;
  quantidadeEntregar: Double;
begin
  inherited;

  if not eDataEntrega.DataOk then begin
    _Biblioteca.Exclamar('A data da previs�o de entrega n�o foi informado corretamente, verifique!');
    SetarFoco(eDataEntrega);
    Abort;
  end;

  for i := 1 to sgProdutos.RowCount -1 do begin
    if (SFormatCurr(sgProdutos.Cells[coQtdeAgendar, i]) > 0) and (sgProdutos.Cells[coDefiniuLotes, i] = charNaoSelecionado) then begin
      Exclamar('Os lotes n�o foram definidos corretamente para o produto ' + sgProdutos.Cells[coNome, i] + '!');
      Abort;
    end;
  end;

  if sgLocais.Visible then begin
    for i := 1 to sgProdutos.RowCount - 1 do begin

      if Em(sgProdutos.Cells[coTipoControleEstoque, i], ['P', 'G', 'L']) then
        Continue;

      quantidadeEntregar := 0;
      for j := Low(FLocais) to High(FLocais) do begin

        if SFormatInt(sgProdutos.Cells[coProdutoId, i]) <> FLocais[j].ProdutoId then
          Continue;

        quantidadeEntregar := quantidadeEntregar + FLocais[j].QuantidadeEntregar;
      end;

      if quantidadeEntregar <> SFormatDouble(sgProdutos.Cells[coQtdeAgendar, i]) then begin
        Exclamar(
          'Os locais do produto ' + sgProdutos.Cells[coProdutoId, i] + ' - ' + sgProdutos.Cells[coNome, i] +
          ' n�o foram definidos corretamente para entregar!'
        );
        sgProdutos.SetFocus;
        sgProdutos.Row := i;
        Abort;
      end;
    end;
  end;
end;

end.
