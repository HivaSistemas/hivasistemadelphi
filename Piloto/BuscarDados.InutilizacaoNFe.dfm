inherited FormBuscarDadosInutilizacaoNFe: TFormBuscarDadosInutilizacaoNFe
  Caption = 'Buscar dados da inutiliza'#231#227'o de NFe'
  ClientHeight = 125
  ClientWidth = 489
  ExplicitWidth = 495
  ExplicitHeight = 154
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 100
    Top = 3
    Width = 103
    Height = 14
    Caption = 'Numero nota inicial'
  end
  object lb2: TLabel [1]
    Left = 209
    Top = 3
    Width = 97
    Height = 14
    Caption = 'Numero nota final'
  end
  object lb3: TLabel [2]
    Left = 318
    Top = 3
    Width = 27
    Height = 14
    Caption = 'S'#233'rie'
  end
  object lb4: TLabel [3]
    Left = 369
    Top = 3
    Width = 22
    Height = 14
    Caption = 'Ano'
  end
  object lb5: TLabel [4]
    Left = 6
    Top = 42
    Width = 61
    Height = 14
    Caption = 'Justificativa'
  end
  object Label1: TLabel [5]
    Left = 6
    Top = 3
    Width = 39
    Height = 14
    Caption = 'Modelo'
  end
  inherited pnOpcoes: TPanel
    Top = 88
    Width = 489
    TabOrder = 6
    ExplicitTop = 88
    ExplicitWidth = 489
  end
  object eNumeracaoInicial: TEditLuka
    Left = 100
    Top = 18
    Width = 105
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 9
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
  end
  object eNumeracaoFinal: TEditLuka
    Left = 209
    Top = 18
    Width = 105
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 9
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
  end
  object eJustificativa: TEditLuka
    Left = 6
    Top = 57
    Width = 477
    Height = 22
    Hint = 
      'Justificativa a ser informada para a SEFAZ de o porque da inutil' +
      'iza'#231#227'o da numera'#231#227'o.'
    CharCase = ecUpperCase
    MaxLength = 255
    TabOrder = 5
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
  end
  object cbAnoInutilizacao: TComboBoxLuka
    Left = 369
    Top = 18
    Width = 114
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnKeyDown = ProximoCampo
  end
  object eSerie: TEditLuka
    Left = 318
    Top = 18
    Width = 47
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 1
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
  end
  object cbModeloNota: TComboBoxLuka
    Left = 6
    Top = 18
    Width = 90
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'NFe'
      'NFCe')
    Valores.Strings = (
      '55'
      '65')
  end
end
