unit FrameLocais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _RecordsCadastros,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, _Biblioteca, PesquisaLocaisProdutos,
  _LocaisProdutos, Vcl.Menus;

type
  TFrLocais = class(TFrameHenrancaPesquisas)
  public
    function GetLocais(pLinha: Integer = -1): RecLocaisProdutos;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrLocais }

function TFrLocais.AdicionarDireto: TObject;
var
  vLocais: TArray<RecLocaisProdutos>;
begin
  vLocais := _LocaisProdutos.BuscarLocaisProdutos(Sessao.getConexaoBanco, 0, [FChaveDigitada] );
  if vLocais = nil then
    Result := nil
  else
    Result := vLocais[0];
end;

function TFrLocais.AdicionarPesquisando: TObject;
begin
  Result := PesquisaLocaisProdutos.PesquisarLocalProduto;
end;

function TFrLocais.GetLocais(pLinha: Integer): RecLocaisProdutos;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecLocaisProdutos(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrLocais.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecLocaisProdutos(FDados[i]).local_id = RecLocaisProdutos(pSender).local_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrLocais.MontarGrid;
var
  i: Integer;
  vSender: RecLocaisProdutos;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecLocaisProdutos(FDados[i]);
      AAdd([IntToStr(vSender.local_id), vSender.nome]);
    end;
  end;
end;

end.
