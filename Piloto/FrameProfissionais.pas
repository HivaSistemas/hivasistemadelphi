unit FrameProfissionais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Profissionais, Vcl.Menus;

type
  TFrProfissionais = class(TFrameHenrancaPesquisas)
  public
    function GetDado(pLinha: Integer = -1): RecProfissionais;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

uses PesquisaProfissionais, _Sessao, _Biblioteca;

{ TFrProfissionais }

function TFrProfissionais.AdicionarDireto: TObject;
var
  vDados: TArray<RecProfissionais>;
begin
  vDados := _Profissionais.BuscarProfissionais(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrProfissionais.AdicionarPesquisando: TObject;
begin
  Result := PesquisaProfissionais.Pesquisar(ckSomenteAtivos.Checked);
end;

function TFrProfissionais.AdicionarPesquisandoTodos: TArray<TObject>;
begin
  Result := PesquisaProfissionais.PesquisarVarios(ckSomenteAtivos.Checked);
end;

function TFrProfissionais.GetDado(pLinha: Integer): RecProfissionais;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecProfissionais(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrProfissionais.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecProfissionais(FDados[i]).CadastroId = RecProfissionais(pSender).CadastroId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrProfissionais.MontarGrid;
var
  i: Integer;
  pSender: RecProfissionais;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecProfissionais(FDados[i]);
      AAdd([IntToStr(pSender.CadastroId), pSender.cadastro.nome_fantasia]);
    end;
  end;
end;

end.
