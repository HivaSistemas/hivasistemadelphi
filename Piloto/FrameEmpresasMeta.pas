unit FrameEmpresasMeta;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, PesquisaEmpresas, _Empresas, System.Math, _Sessao,
  Vcl.Buttons, Vcl.Menus;

type
  TFrEmpresasMeta = class(TFrameHenrancaPesquisas)
  public
    function getEmpresa(pLinha: Integer = -1): RecEmpresas;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrEmpresas }

function TFrEmpresasMeta.AdicionarDireto: TObject;
var
  empresas: TArray<RecEmpresas>;
begin
  empresas := _Empresas.BuscarEmpresas(Sessao.getConexaoBanco, 0, [FChaveDigitada] );
  if empresas = nil then
    Result := nil
  else
    Result := empresas[0];
end;

function TFrEmpresasMeta.AdicionarPesquisando: TObject;
begin
  Result := PesquisaEmpresas.PesquisarEmpresa;
end;

function TFrEmpresasMeta.AdicionarPesquisandoTodos: TArray<TObject>;
begin
  Result := PesquisaEmpresas.PesquisarEmpresas;
end;

function TFrEmpresasMeta.getEmpresa(pLinha: Integer): RecEmpresas;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecEmpresas(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrEmpresasMeta.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecEmpresas(FDados[i]).EmpresaId = RecEmpresas(pSender).EmpresaId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrEmpresasMeta.MontarGrid;
var
  i: Integer;
  pSender: RecEmpresas;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecEmpresas(FDados[i]);
      AAdd([IntToStr(pSender.EmpresaId), pSender.NomeFantasia]);
    end;
  end;
end;

end.
