inherited FormBuscarFornecedorCodigoOriginal: TFormBuscarFornecedorCodigoOriginal
  Caption = 'FormBuscarFornecedorCodigoOriginal'
  ClientHeight = 88
  ClientWidth = 488
  OnShow = FormShow
  ExplicitWidth = 494
  ExplicitHeight = 117
  PixelsPerInch = 96
  TextHeight = 14
  object lbllb1: TLabel [0]
    Left = 325
    Top = 4
    Width = 83
    Height = 14
    Caption = 'C'#243'digo original'
  end
  inherited pnOpcoes: TPanel
    Top = 51
    Width = 488
    ExplicitTop = 51
    ExplicitWidth = 488
  end
  inline FrFornecedor: TFrFornecedores
    Left = 6
    Top = 3
    Width = 313
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 6
    ExplicitTop = 3
    ExplicitWidth = 313
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 288
      Height = 23
      ExplicitWidth = 288
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 313
      ExplicitWidth = 313
      inherited lbNomePesquisa: TLabel
        Width = 61
        Caption = 'Fornecedor'
        ExplicitWidth = 61
      end
    end
    inherited pnPesquisa: TPanel
      Left = 288
      Height = 24
      ExplicitLeft = 288
      ExplicitHeight = 24
    end
  end
  object eCodigoOriginal: TEditLuka
    Left = 325
    Top = 19
    Width = 156
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 20
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
  end
end
