unit ImpressaoComprovanteCartao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, QRCtrls, QuickRpt, _Sessao,
  Vcl.ExtCtrls;

type
  TFormImpressaoComprovanteCartao = class(TFormHerancaPrincipal)
    qrRelatorioNF: TQuickRep;
    qrbndTitulo: TQRBand;
    qrlNFTipoDocumento: TQRLabel;
    qrlNFEmitidoEm: TQRLabel;
    qrbndPageFooterBand1: TQRBand;
    qrlNFSistema: TQRLabel;
    qrRelatorioA4: TQuickRep;
    qrbndCabecalho: TQRBand;
    qrlCaptionEndereco: TQRLabel;
    qrlEmpresa: TQRLabel;
    qrlEndereco: TQRLabel;
    qrLogoEmpresa: TQRImage;
    qrlCaptionEmpresa: TQRLabel;
    qrlEmitidoEm: TQRLabel;
    qrlqr1: TQRLabel;
    qrlBairro: TQRLabel;
    qrlqr3: TQRLabel;
    qrlCidadeUf: TQRLabel;
    qrlqr5: TQRLabel;
    qrlCNPJ: TQRLabel;
    qrlqr7: TQRLabel;
    qrlTelefone: TQRLabel;
    qrlFax: TQRLabel;
    qrlqr10: TQRLabel;
    qrlqr11: TQRLabel;
    qrlEmail: TQRLabel;
    qrlqr13: TQRLabel;
    qrl1: TQRLabel;
    qrMemoNF1Via: TQRExprMemo;
  private
    FTexto: TArray<string>;
  public
    { Public declarations }
  end;

procedure Imprimir( pTexto1Via: TArray<string>; pTexto2Via: TArray<string> );

implementation

{$R *.dfm}

procedure Imprimir( pTexto1Via: TArray<string>; pTexto2Via: TArray<string> );
var
  i: Integer;
  vImpressora: RecImpressora;
  vForm: TFormImpressaoComprovanteCartao;
begin
  vImpressora := Sessao.getImpressora(toComprovantePagamento);
  if not vImpressora.validaImpresssora then
    Exit;

  vForm := TFormImpressaoComprovanteCartao.Create(nil);

  vForm.FTexto := pTexto1Via;
  vForm.qrMemoNF1Via.Lines.Clear;
  for i := Low(pTexto1Via) to High(pTexto1Via) do
    vForm.qrMemoNF1Via.Lines.Add(pTexto1Via[i]);

  vForm.qrMemoNF1Via.Lines.Add('');
  vForm.qrMemoNF1Via.Lines.Add('');

  for i := Low(pTexto2Via) to High(pTexto2Via) do
    vForm.qrMemoNF1Via.Lines.Add(pTexto2Via[i]);

  vForm.qrMemoNF1Via.Height := (Length(pTexto1Via) + Length(pTexto2Via) + 2) * 14;

  vForm.qrbndTitulo.Height := vForm.qrbndTitulo.Height + vForm.qrMemoNF1Via.Height;
  vForm.qrRelatorioNF.Height := vForm.qrRelatorioNF.Height + vForm.qrbndTitulo.Height + 10;

  vForm.qrRelatorioNF.PrinterSettings.PrinterIndex := vImpressora.getIndex;
  vForm.qrRelatorioNF.Print;

  vForm.Free;
end;

end.
