inherited FormRelacaoProdutosPaiFilho: TFormRelacaoProdutosPaiFilho
  Caption = 'Rela'#231#227'o de produtos pai/filho'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Visible = False
    end
  end
  inherited pcDados: TPageControl
    ActivePage = tsResultado
    inherited tsFiltros: TTabSheet
      inline FrProdutosPai: TFrProdutos
        Left = 0
        Top = -2
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitTop = -2
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 69
            Caption = 'Produtos pai'
            ExplicitWidth = 69
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas
        Left = 0
        Top = 168
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitTop = 168
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 128
            Caption = 'Departamentos/se'#231#245'es'
            ExplicitWidth = 128
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrDataCadastroProduto: TFrDataInicialFinal
        Left = 424
        Top = 0
        Width = 217
        Height = 68
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 424
        ExplicitWidth = 217
        ExplicitHeight = 68
        inherited Label1: TLabel
          Width = 156
          Height = 14
          Caption = 'Data de cadastro do produto'
          ExplicitWidth = 156
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrProdutosFilho: TFrProdutos
        Left = 0
        Top = 83
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitTop = 83
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 75
            Caption = 'Produtos filho'
            ExplicitWidth = 75
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object spSeparador: TSplitter
        Left = 0
        Top = 129
        Width = 884
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ResizeStyle = rsUpdate
        ExplicitLeft = -40
        ExplicitTop = 513
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 135
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Produtos filho'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
        ExplicitLeft = 2
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Produtos pai'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgProdutosPai: TGridLuka
        Left = 0
        Top = 17
        Width = 884
        Height = 112
        Align = alTop
        ColCount = 2
        Ctl3D = True
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        GradientEndColor = 15395562
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
        ParentCtl3D = False
        TabOrder = 2
        OnClick = sgProdutosPaiClick
        OnDrawCell = sgProdutosPaiDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Produto'
          'Nome')
        Grid3D = False
        RealColCount = 54
        AtivarPopUpSelecao = False
        ColWidths = (
          67
          355)
      end
      object sgProdutosFilho: TGridLuka
        Left = 0
        Top = 152
        Width = 884
        Height = 366
        Align = alClient
        ColCount = 3
        Ctl3D = True
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        GradientEndColor = 15395562
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
        ParentCtl3D = False
        TabOrder = 3
        OnDrawCell = sgProdutosFilhoDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Produto'
          'Nome'
          'Qtde. vezes pai')
        Grid3D = False
        RealColCount = 54
        AtivarPopUpSelecao = False
        ColWidths = (
          49
          367
          103)
      end
    end
  end
end
