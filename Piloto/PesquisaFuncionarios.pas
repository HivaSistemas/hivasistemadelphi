unit PesquisaFuncionarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Sessao, System.Math,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Funcionarios, _RecordsCadastros, _Biblioteca,
  Vcl.ExtCtrls;

type
  TFormPesquisaFuncionarios = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FSomenteVendedores: Boolean;
    FSomenteCaixas: Boolean;
    FSomenteCompradores: Boolean;
    FSomenteMotoristas: Boolean;
    FSomenteAjudantes: Boolean;

    FEmpresaId: Integer;
    FBuscarSomenteAtivos: Boolean;
    FBuscarTodasEmpresas: Boolean;
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarFuncionario(
  pSomenteVendedores: Boolean;
  pSomenteCaixas: Boolean;
  pSomenteCompradores: Boolean;
  pSomenteMotoristas: Boolean;
  pEmpresaId: Integer;
  pBuscarSomenteAtivos: Boolean;
  pBuscarTodasEmpresas: Boolean;
  pSomenteAjudantes: Boolean
): TObject;

implementation

{$R *.dfm}

const
  cp_nome    = 2;
  cp_apelido = 3;
  cpCargo    = 4;
  cp_ativo   = 5;

{ TFormPesquisaFuncionarios }

function PesquisarFuncionario(
  pSomenteVendedores: Boolean;
  pSomenteCaixas: Boolean;
  pSomenteCompradores: Boolean;
  pSomenteMotoristas: Boolean;
  pEmpresaId: Integer;
  pBuscarSomenteAtivos: Boolean;
  pBuscarTodasEmpresas: Boolean;
  pSomenteAjudantes: Boolean
): TObject;
var
  vForm: TFormPesquisaFuncionarios;
begin
  Result := nil;
  vForm := TFormPesquisaFuncionarios.Create(Application);

  vForm.FSomenteMotoristas   := pSomenteMotoristas;
  vForm.FSomenteCaixas       := pSomenteCaixas;
  vForm.FSomenteCompradores  := pSomenteCompradores;
  vForm.FSomenteVendedores   := pSomenteVendedores;
  vForm.FSomenteAjudantes    := pSomenteAjudantes;
  vForm.FEmpresaId           := pEmpresaId;
  vForm.FBuscarSomenteAtivos := pBuscarSomenteAtivos;
  vForm.FBuscarTodasEmpresas := pBuscarTodasEmpresas;
  vForm.sgPesquisa.OcultarColunas([coSelecionado]);

  if vForm.ShowModal = mrOk then
    Result := vForm.FDados[vForm.sgPesquisa.Row - 1];

  vForm.Free;
end;

procedure TFormPesquisaFuncionarios.BuscarRegistros;
var
  i: Integer;
  vComando: string;
  vFuncionarios: TArray<RecFuncionarios>;
begin
  inherited;
  vComando := 'where ';

  if cbOpcoesPesquisa.ItemIndex = 0 then
    vComando := vComando + 'FUN.NOME like ''' + eValorPesquisa.Text + '%'' '
  else if cbOpcoesPesquisa.ItemIndex = 1 then
    vComando := vComando + 'FFU.NOME like ''' + eValorPesquisa.Text + '%'' '
  else
    vComando := vComando + 'FUN.APELIDO = ''' + eValorPesquisa.Text + ''' ';

  if FBuscarSomenteAtivos then
    vComando := vComando + 'and FUN.ATIVO = ''S'' ';

//  if not FBuscarTodasEmpresas then IMPLEMENTAR

  if FSomenteCaixas then
    vComando := vComando + 'and FUN.CAIXA = ''S'' ';

  if FSomenteVendedores then
    vComando := vComando + 'and FUN.VENDEDOR = ''S'' ';

  if FSomenteCompradores then
    vComando := vComando + 'and FUN.COMPRADOR = ''S'' ';

  if FSomenteAjudantes then
    vComando := vComando + 'and FUN.AJUDANTE = ''S'' ';

  vFuncionarios :=
    _Funcionarios.BuscarFuncionariosComando(
      Sessao.getConexaoBanco,
      vComando
    );

  if vFuncionarios = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vFuncionarios);

  for i := Low(vFuncionarios) to High(vFuncionarios) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]      := NFormat(vFuncionarios[i].funcionario_id);
    sgPesquisa.Cells[cp_nome, i + 1]        := vFuncionarios[i].nome;
    sgPesquisa.Cells[cp_apelido, i + 1]     := vFuncionarios[i].apelido;
    sgPesquisa.Cells[cpCargo, i + 1]        := NFormat(vFuncionarios[i].CargoId) + ' - ' + vFuncionarios[i].nome_funcao;
    sgPesquisa.Cells[cp_ativo, i + 1]       := SimNao(vFuncionarios[i].ativo);
  end;

  sgPesquisa.RowCount := IfThen(Length(vFuncionarios) = 1, 2, High(vFuncionarios) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaFuncionarios.FormShow(Sender: TObject);
begin
  inherited;
  cbOpcoesPesquisa.Filtros := _Funcionarios.GetFiltros;
end;

procedure TFormPesquisaFuncionarios.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, cp_ativo]) then
    alinhamento := taCenter
  else if ACol = coCodigo then
    alinhamento := taRightJustify
  else
    alinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

procedure TFormPesquisaFuncionarios.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = cp_ativo then begin
    AFont.Color := AzulVermelho(sgPesquisa.Cells[ACol, ARow]);
    AFont.Style := [fsBold];
  end;
end;

end.
