inherited FormCancelarItensCompras: TFormCancelarItensCompras
  Caption = 'Cancelamento itens pedido de compras'
  ClientHeight = 371
  ClientWidth = 813
  ExplicitWidth = 819
  ExplicitHeight = 400
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 334
    Width = 813
    ExplicitTop = 334
    ExplicitWidth = 813
  end
  object sgItens: TGridLuka
    Left = 1
    Top = 2
    Width = 810
    Height = 330
    Align = alCustom
    ColCount = 9
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 2
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 1
    OnDrawCell = sgItensDrawCell
    OnSelectCell = sgItensSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Und.'
      'Quantidade'
      'Entregues'
      'Cancelados'
      'Saldo'
      'Qtde.cancelar')
    OnGetCellColor = sgItensGetCellColor
    OnArrumarGrid = sgItensArrumarGrid
    Grid3D = False
    RealColCount = 13
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      54
      214
      112
      34
      74
      63
      71
      74
      89)
  end
end
