unit Informacoes.OperacoesCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Sessao, _RecordsCaixa,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, Vcl.Mask, EditLukaData, Vcl.ComCtrls,
  Vcl.Grids, GridLuka, System.Math, _Biblioteca, _MovimentosTurnosItens, Informacoes.TituloReceber,
  Informacoes.ContasReceberBaixa, Informacoes.TurnoCaixa, Informacoes.Orcamento, _MovimentosTurnos;

type
  TFormInformacoesOperacoesCaixa = class(TFormHerancaFinalizar)
    lb1: TLabel;
    eMovimentoId: TEditLuka;
    st3: TStaticText;
    stTipoMovimento: TStaticText;
    lb19: TLabel;
    eUsuarioMovimento: TEditLuka;
    lb6: TLabel;
    eDataCadastro: TEditLukaData;
    pcDados: TPageControl;
    tsGerais: TTabSheet;
    tsItensOperacao: TTabSheet;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    lb14: TLabel;
    eValorDinheiro: TEditLuka;
    eValorCheque: TEditLuka;
    eValorCartao: TEditLuka;
    eValorCobranca: TEditLuka;
    stSPC: TStaticText;
    lb2: TLabel;
    eContaOrigemDestino: TEditLuka;
    eObservacoes: TMemo;
    lb4: TLabel;
    lb17: TLabel;
    eTurnoId: TEditLuka;
    sgTitulosMovimento: TGridLuka;
    lb5: TLabel;
    eValorCredito: TEditLuka;
    sbAbrirInformacoesTurno: TSpeedButton;
    Label1: TLabel;
    eMovimentoOrigemId: TEditLuka;
    sbInformacoesMovimentoOrigem: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure sgTitulosMovimentoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTitulosMovimentoDblClick(Sender: TObject);
    procedure sbAbrirInformacoesTurnoClick(Sender: TObject);
    procedure sbInformacoesMovimentoOrigemClick(Sender: TObject);
  end;

procedure Informar(pMovimentoId: Integer);

implementation

{$R *.dfm}

const
  coReceberId     = 0;
  coTipoCobranca  = 1;
  coDocumento     = 2;
  coOrcamentoId   = 3;
  coValor         = 4;

procedure Informar(pMovimentoId: Integer);
var
  i: Integer;
  vForm: TFormInformacoesOperacoesCaixa;
  vMovimento: TArray<RecMovimentosTurnos>;
  vMovimentoItens: TArray<RecMovimentoTurnoItens>;
begin
  if pMovimentoId = 0 then
    Exit;

  vMovimento := _MovimentosTurnos.BuscarMovimentosTurnos(Sessao.getConexaoBanco, 1, [pMovimentoId]);
  if vMovimento = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vMovimentoItens := _MovimentosTurnosItens.BuscarMovimentosTurnosItens(Sessao.getConexaoBanco, 0, [pMovimentoId]);
  vForm := TFormInformacoesOperacoesCaixa.Create(Application);

  vForm.eMovimentoId.AsInt         := pMovimentoId;
  vForm.eUsuarioMovimento.Text     := vMovimento[0].funcionario;
  vForm.eDataCadastro.AsData       := vMovimento[0].DataHoraMovimento;
  vForm.stTipoMovimento.Caption    := vMovimento[0].TipoMovimentoAnalitico;

  vForm.stTipoMovimento.Font.Color :=
    _Biblioteca.Decode(
      vMovimento[0].TipoMovimento,[
        'ABE', cor_abertura,
        'SUP', cor_suprimento,
        'SAN', cor_sangria,

        'SBC', cor_saida_abert,
        'ESC', cor_entrada_sang,
        'FEC', cor_fechamento,
        'REV', cor_recebimento,
        'SSC', cor_saida_supri,
        'CRV', cor_canc_venda,
        'EFC', cor_ent_fec_cai,
        'BTR', cor_saida_bx_tit,
        'EBR', cor_ent_bx_tit,
        clBlack
      ]
    );

  vForm.eContaOrigemDestino.SetInformacao(vMovimento[0].ContaId, vMovimento[0].NomeContaOrigem);
  vForm.eObservacoes.Text := vMovimento[0].observacao;
  vForm.eTurnoId.SetInformacao(vMovimento[0].TurnoId);
//  vForm.eMovimentoOrigemId.SetInformacao(vMovimento[0].movimentoOrigemId);

  vForm.eValorDinheiro.AsDouble := vMovimento[0].ValorDinheiro;
  vForm.eValorCartao.AsDouble   := vMovimento[0].ValorCartao;
  vForm.eValorCheque.AsDouble   := vMovimento[0].ValorCheque;
  vForm.eValorCobranca.AsDouble := vMovimento[0].ValorCobranca;
  vForm.eValorCredito.AsDouble  := vMovimento[0].ValorCredito;

  if vMovimentoItens <> nil then begin
    for i := Low(vMovimentoItens) to High(vMovimentoItens) do begin
      vForm.sgTitulosMovimento.Cells[coReceberId, i + 1]    := NFormat(vMovimentoItens[i].id);
      vForm.sgTitulosMovimento.Cells[coTipoCobranca, i + 1] := NFormat(vMovimentoItens[i].CobrancaId) + ' - ' + vMovimentoItens[i].NomeCobranca;
      vForm.sgTitulosMovimento.Cells[coDocumento, i + 1]    := vMovimentoItens[i].documento;
      vForm.sgTitulosMovimento.Cells[coOrcamentoId, i + 1]  := NFormatN(vMovimentoItens[i].orcamento_id);
      vForm.sgTitulosMovimento.Cells[coValor, i + 1]        := NFormat(vMovimentoItens[i].ValorDocumento);
    end;
    vForm.sgTitulosMovimento.RowCount := IfThen(Length(vMovimentoItens) > 1, High(vMovimentoItens) + 2, 2);
  end;
  vForm.tsItensOperacao.TabVisible := (Length(vMovimentoItens) > 0);
  vForm.ReadOnlyTodosObjetos(True);
  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesOperacoesCaixa.FormCreate(Sender: TObject);
begin
  inherited;
  pcDados.ActivePage := tsGerais;
end;

procedure TFormInformacoesOperacoesCaixa.sbAbrirInformacoesTurnoClick(Sender: TObject);
begin
  inherited;
  Informacoes.TurnoCaixa.Informar(eTurnoId.IdInformacao);
end;

procedure TFormInformacoesOperacoesCaixa.sbInformacoesMovimentoOrigemClick(Sender: TObject);
begin
  inherited;
  Informacoes.OperacoesCaixa.Informar(eMovimentoOrigemId.IdInformacao);
end;

procedure TFormInformacoesOperacoesCaixa.sgTitulosMovimentoDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloReceber.Informar(SFormatInt(sgTitulosMovimento.Cells[coReceberId, sgTitulosMovimento.Row]));
end;

procedure TFormInformacoesOperacoesCaixa.sgTitulosMovimentoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coReceberId, coOrcamentoId, coValor] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgTitulosMovimento.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
