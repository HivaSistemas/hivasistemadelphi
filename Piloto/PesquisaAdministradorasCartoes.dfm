inherited FormPesquisaAdministradorasCartoes: TFormPesquisaAdministradorasCartoes
  Caption = 'Pesquisa de administradoras de car'#245'es'
  ClientHeight = 236
  ClientWidth = 480
  ExplicitWidth = 486
  ExplicitHeight = 265
  PixelsPerInch = 96
  TextHeight = 14
  inherited eValorPesquisa: TEditLuka
    Width = 276
  end
  inherited sgPesquisa: TGridLuka
    Width = 476
    Height = 192
    ColCount = 5
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Descri'#231#227'o'
      'CNPJ'
      'Ativo?')
    OnGetCellColor = sgPesquisaGetCellColor
    RealColCount = 5
    ColWidths = (
      28
      47
      210
      106
      60)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Top = 168
    Text = '0'
    ExplicitTop = 168
  end
end
