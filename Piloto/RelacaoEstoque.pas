unit RelacaoEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  _HerancaRelatoriosPageControl, Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameProdutos, Vcl.StdCtrls, ComboBoxLuka, _FrameHenrancaPesquisas,
  Frame.DepartamentosSecoesLinhas, _FrameHerancaPrincipal, FrameDataInicialFinal, FrameEmpresas, Vcl.Grids, GridLuka, _RelacaoEstoque,
  _Biblioteca, _Sessao, RadioGroupLuka, FrameMarcas, StaticTextLuka, ComObj, Clipbrd,
  CheckBoxLuka, FileCtrl, EditLuka, _RecordsCadastros, _RecordsEspeciais,
  Data.DB, Datasnap.DBClient, frxClass, frxDBSet, FrameLocais;

type
  TFormRelacaoEstoque = class(TFormHerancaRelatoriosPageControl)
    lb1: TLabel;
    lb2: TLabel;
    FrDataCadastroProduto: TFrDataInicialFinal;
    FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas;
    cbTipoControleEstoque: TComboBoxLuka;
    cbAtivo: TComboBoxLuka;
    FrProdutos: TFrProdutos;
    sgItens: TGridLuka;
    FrEmpresa: TFrEmpresas;
    rgTipoCusto: TRadioGroupLuka;
    rgTipoEstoque: TRadioGroupLuka;
    FrMarcas: TFrMarcas;
    stQtdeTotalEstoque: TStaticTextLuka;
    st4: TStaticText;
    stValorTotalEstoque: TStaticTextLuka;
    st3: TStaticText;
    st1: TStaticTextLuka;
    st2: TStaticText;
    stQtdeProdutos: TStaticTextLuka;
    lb3: TLabel;
    cbQuantidadeEmEstoque: TComboBoxLuka;
    SpeedButton1: TSpeedButton;
    Panel1: TPanel;
    stTotaisFiscal: TStaticTextLuka;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    stValorTotalEstoqueFiscal: TStaticTextLuka;
    stQtdeTotalEstoqueFiscal: TStaticTextLuka;
    stQtdeProdutosFiscal: TStaticTextLuka;
    sbAtualizarCMV: TSpeedButton;
    sbAjustarVlrEstoque: TSpeedButton;
    eValorEstoque: TEditLuka;
    lbNovoValor: TLabel;
    Label2: TLabel;
    eAno: TEditLuka;
    Label1: TLabel;
    eMes: TEditLuka;
    StaticText4: TStaticText;
    frxReport: TfrxReport;
    dstReceber: TfrxDBDataset;
    cdsReceber: TClientDataSet;
    cdsReceberProdutoId: TStringField;
    cdsReceberProdutoNome: TStringField;
    cdsReceberUnidade: TStringField;
    cdsReceberEstoque: TFloatField;
    cdsReceberCustoUnitario: TFloatField;
    cdsReceberCustoTotal: TFloatField;
    Label3: TLabel;
    eLivro: TEditLuka;
    Label4: TLabel;
    cbPaiFilho: TComboBoxLuka;
    FrLocais: TFrLocais;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure FormCreate(Sender: TObject);
    procedure rgTipoCustoClick(Sender: TObject);
    procedure sbAtualizarCMVClick(Sender: TObject);
    procedure sbAjustarVlrEstoqueClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
  private
    valorEstoqueRetroativo: Boolean;
    periodoEstoqueRetroativo: string;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses _Estoques, _Empresas;

const
  coProdutoId       = 0;
  coNome            = 1;
  coUnidade         = 2;
  coMarca           = 3;
  coNCM             = 4;
  coEstoque         = 5;
  coEstoqueFiscal   = 6;
  coCustoUnitario   = 7;
  coValor           = 8;
  coValorFiscal     = 9;

{ TFormRelacaoEstoque }

procedure TFormRelacaoEstoque.FormCreate(Sender: TObject);
begin
  inherited;
  sgItens.OcultarColunas([coEstoqueFiscal, coValorFiscal]);
  Panel1.Visible := False;
end;

procedure TFormRelacaoEstoque.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  // CTRL + ALT + F1
  if (ssCtrl in Shift) and (ssAlt in Shift) and (key = 112) then begin
    if Sessao.getUsuarioLogado.funcionario_id <> 1 then
      Exit;

    lbNovoValor.Visible := true;
    sbAtualizarCMV.Visible := true;
    sbAjustarVlrEstoque.Visible := true;
    eValorEstoque.Visible := true;
  end;
end;

procedure TFormRelacaoEstoque.FormShow(Sender: TObject);
begin
  inherited;
  rgTipoCusto.SetIndicePorValor( Sessao.getParametrosEmpresa.TipoCustoVisLucroVenda );
  FrEmpresa.InserirDadoPorChave( Sessao.getEmpresaLogada.EmpresaId, False );
  SetarFoco(FrProdutos);
end;

procedure TFormRelacaoEstoque.rgTipoCustoClick(Sender: TObject);
begin
  inherited;
  sbAtualizarCMV.Visible := rgTipoCusto.GetValor = 'CMV';
  sbAjustarVlrEstoque.Visible := rgTipoCusto.GetValor = 'CMV';
  eValorEstoque.Visible := rgTipoCusto.GetValor = 'CMV';
end;

procedure TFormRelacaoEstoque.sbAtualizarCMVClick(Sender: TObject);
var
  i: Integer;
  produtoId: Integer;
  empresaId: Integer;
  empresas: TArray<RecEmpresas>;
  comando: string;
  retorno: RecRetornoBD;
begin
  inherited;
  empresaId := FrEmpresa.getEmpresa.EmpresaId;
  comando := 'where EMP.EMPRESA_ID not in (''' + IntToStr(empresaId) + ''') ';
  empresas := _Empresas.BuscarEmpresasComando(Sessao.getConexaoBanco, comando);

  for i := 1 to sgItens.RowCount - 1 do begin
    if SFormatDouble(sgItens.Cells[coCustoUnitario, i]) > 0 then
      Continue;

    produtoId := SFormatInt(sgItens.Cells[coProdutoId, i]);
    retorno := _Estoques.AtualizarCMVProdutos(Sessao.getConexaoBanco, produtoId, empresaId, empresas);
    if retorno.TeveErro then begin
      Exclamar('Erro ao atualizar o CMV do produto ' + sgItens.Cells[coProdutoId, i] + ' Erro: ' + retorno.MensagemErro);
      Exit;
    end;
  end;

  RotinaSucesso;
  Carregar(nil);
end;

procedure TFormRelacaoEstoque.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coNome, coUnidade, coMarca, coNCM] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoEstoque.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coValor then begin
    AFont.Style := [fsBold];

    if SFormatCurr(sgItens.Cells[coValor, ARow]) >= 0 then
      AFont.Color := clBlue
    else
      AFont.Color := clRed;
  end;

  if ACol = coValorFiscal then begin
    AFont.Style := [fsBold];

    if SFormatCurr(sgItens.Cells[coValorFiscal, ARow]) >= 0 then
      AFont.Color := clBlue
    else
      AFont.Color := clRed;
  end;
end;

procedure TFormRelacaoEstoque.SpeedButton1Click(Sender: TObject);
var
 coluna, linha: integer;
 excel: variant;
 valor: string;
 i: Integer;
 dataHoraString: string;
 dataAtual: TDateTime;
 horaAtual: TDateTime;
 caminhoArquivo: string;
begin
  try
    excel := CreateOleObject('Excel.Application');
    excel.Workbooks.add(1);
  except
    Application.MessageBox ('Vers�o do Ms-Excel'+
    'Incompat�vel','Erro',MB_OK+MB_ICONEXCLAMATION);
  end;

  try
    linha := 0;
    for i := 1 to sgItens.RowCount - 1 do begin

      excel.cells[linha + 2, 1] := SFormatInt(sgItens.Cells[coProdutoId, i]);
      excel.cells[linha + 2, 2] := sgItens.Cells[coNome, i];
      excel.cells[linha + 2, 3] := sgItens.Cells[coUnidade, i];
      excel.cells[linha + 2, 4] := sgItens.Cells[coMarca, i];
      excel.cells[linha + 2, 5] := sgItens.Cells[coNCM, i];

      if rgTipoEstoque.GetValor = 'FISXFIS' then begin
        excel.cells[linha + 2, 6] := SFormatDouble(sgItens.Cells[coEstoque, i]);
        excel.cells[linha + 2, 7] := SFormatDouble(sgItens.Cells[coEstoqueFiscal, i]);

        excel.cells[linha + 2, 8].NumberFormat := '#.##0,0000';
        excel.cells[linha + 2, 8] := SFormatDouble(sgItens.Cells[coCustoUnitario, i]);

        excel.cells[linha + 2, 9].NumberFormat := '#.##0,0000';
        excel.cells[linha + 2, 9] := SFormatDouble(sgItens.Cells[coValor, i]);
        excel.cells[linha + 2, 10].NumberFormat := '#.##0,0000';
        excel.cells[linha + 2, 10] := SFormatDouble(sgItens.Cells[coValorFiscal, i]);
      end
      else begin
        excel.cells[linha + 2, 6] := SFormatDouble(sgItens.Cells[coEstoque, i]);
        excel.cells[linha + 2, 7].NumberFormat := '#.##0,0000';
        excel.cells[linha + 2, 7] := SFormatDouble(sgItens.Cells[coCustoUnitario, i]);
        excel.cells[linha + 2, 8].NumberFormat := '#.##0,0000';
        excel.cells[linha + 2, 8] := SFormatDouble(sgItens.Cells[coValor, i]);
      end;

      linha := linha + 1;
    end;

    if rgTipoEstoque.GetValor = 'FISXFIS' then begin
      excel.cells[linha + 2, 5] := 'TOTAL: ';
      excel.cells[linha + 2, 6] := SFormatDouble(stQtdeTotalEstoque.Caption);
      excel.cells[linha + 2, 7] := SFormatDouble(stQtdeTotalEstoqueFiscal.Caption);
      excel.cells[linha + 2, 9].NumberFormat := '#.##0,0000';
      excel.cells[linha + 2, 9] := SFormatDouble(stValorTotalEstoque.Caption);
      excel.cells[linha + 2, 10].NumberFormat := '#.##0,0000';
      excel.cells[linha + 2, 10] := SFormatDouble(stValorTotalEstoqueFiscal.Caption);
    end
    else begin
      excel.cells[linha + 2, 5] := 'TOTAL: ';
      excel.cells[linha + 2, 6] := SFormatDouble(stQtdeTotalEstoque.Caption);
      excel.cells[linha + 2, 8].NumberFormat := '#.##0,0000';
      excel.cells[linha + 2, 8] := SFormatDouble(stValorTotalEstoque.Caption);
    end;

   //Cabe�alho
    if rgTipoEstoque.GetValor = 'FISXFIS' then begin
      sgItens.MostrarColunas([coEstoqueFiscal, coValorFiscal]);
      excel.cells[1, 1]  := 'PRODUTO';
      excel.cells[1, 2]  := 'NOME';
      excel.cells[1, 3]  := 'UNIDADE';
      excel.cells[1, 4]  := 'MARCA';
      excel.cells[1, 5]  := 'NCM';
      excel.cells[1, 6]  := 'ESTOQUE F�SICO';
      excel.cells[1, 7]  := 'ESTOQUE FISCAL';
      excel.cells[1, 8]  := 'CUSTO UNIT�RIO';
      excel.cells[1, 9]  := 'VALOR F�SICO';
      excel.cells[1, 10] := 'VALOR FISCAL';

      //Cabe�alho negrito
      for i := 1 to 10 do
       excel.cells[1, i].Font.Bold := True;

      //Linha abaixo do cabe�alho
      for i := 1 to 10 do
       excel.cells[1, i].Borders.LineStyle := 1;

      //Cor de preenchimento cabe�alho
      for i := 1 to 9 do
       excel.cells[1, i].Interior.Color := RGB(210, 220, 255);
    end
    else begin
      excel.cells[1, 1] := 'PRODUTO';
      excel.cells[1, 2] := 'NOME';
      excel.cells[1, 3] := 'UNIDADE';
      excel.cells[1, 4] := 'MARCA';
      excel.cells[1, 5] := 'NCM';
      excel.cells[1, 6] := 'ESTOQUE';
      excel.cells[1, 7] := 'CUSTO UNIT�RIO';
      excel.cells[1, 8] := 'VALOR';

      //Cabe�alho negrito
      for i := 1 to 8 do
       excel.cells[1, i].Font.Bold := True;

      //Linha abaixo do cabe�alho
      for i := 1 to 8 do
       excel.cells[1, i].Borders.LineStyle := 1;

      //Cor de preenchimento cabe�alho
      for i := 1 to 8 do
       excel.cells[1, i].Interior.Color := RGB(210, 220, 255);
    end;

    excel.columns.AutoFit; // esta linha � para fazer com que o Excel dimencione as c�lulas adequadamente.

    SelectDirectory('Selecione onde ser� gerado o arquivo', '', caminhoArquivo);

    if caminhoArquivo <> '' then begin
      dataAtual := Date;
      horaAtual := Time;

      dataHoraString :=
        StringReplace(DateToStr(dataAtual), '/', '', [rfReplaceAll]) +
        StringReplace(TimeToStr(horaAtual), ':', '', [rfReplaceAll]);

      caminhoArquivo := caminhoArquivo + '\RELACAO_VALOR_ESTOQUE_' + dataHoraString + '.xlsx';

      excel.Workbooks[1].SaveAs(caminhoArquivo);
      Exclamar('Planilha gerada com sucesso!');
    end;

    excel.visible:=true;
    excel := NULL;
  except
      Application.MessageBox ('Aconteceu um erro desconhecido durante a convers�o'+
      'da tabela para o Ms-Excel','Erro',MB_OK+MB_ICONEXCLAMATION);
  end;
end;

procedure TFormRelacaoEstoque.sbAjustarVlrEstoqueClick(Sender: TObject);
var
  i: Integer;
  empresaId: Integer;

  retorno: RecRetornoBD;
  indice: Double;
  vProdutos: TArray<RecRelacaoEstoque>;
begin
  inherited;

  if rgTipoCusto.GetValor <> 'CMV' then begin
    Exclamar('Para realizar o processo de ajustar o valor de estoque � necess�rio que a pesquisa seja feita selecionando "Tipo de Custo = CMV".');
    Abort;
  end;


  if eValorEstoque.AsDouble > 0 then begin
    indice := eValorEstoque.AsDouble / stValorTotalEstoque.AsDouble;
    empresaId := FrEmpresa.getEmpresa.EmpresaId;

    for i := 1 to sgItens.RowCount - 1 do begin
      if SFormatDouble(sgItens.Cells[coCustoUnitario, i]) = 0 then
        Continue;

      SetLength(vProdutos, Length(vProdutos) + 1);
      vProdutos[High(vProdutos)].ProdutoId := SFormatInt(sgItens.Cells[coProdutoId, i]);
      vProdutos[High(vProdutos)].CMV := SFormatDouble(sgItens.Cells[coCustoUnitario, i]);
    end;

    if valorEstoqueRetroativo then begin
      if _Biblioteca.Perguntar('Esse processo � irrevers�vel, tem certeza que deseja alterar o valor de estoque do m�s ' + IntToStr(eMes.AsInt) + ' do ano de ' + IntToStr(eAno.AsInt) + '?' ) then
        retorno := _Estoques.AtualizarCustoEstoqueRetroativo(Sessao.getConexaoBanco, vProdutos, indice, empresaId, periodoEstoqueRetroativo);
    end
    else begin
      if _Biblioteca.Perguntar('Esse processo � irrevers�vel, tem certeza que deseja alterar o valor de estoque atual?') then
        retorno := _Estoques.AtualizarCustoEstoque(Sessao.getConexaoBanco, vProdutos, indice, empresaId);

    end;
  end;

  RotinaSucesso;
  Carregar(nil);
end;

procedure TFormRelacaoEstoque.VerificarRegistro(Sender: TObject);
begin
  inherited;
  _Biblioteca.LimparCampos([stQtdeProdutos, stQtdeTotalEstoque, stValorTotalEstoque]);

  if FrEmpresa.EstaVazio then begin
    _Biblioteca.Exclamar('A empresa n�o foi informada corretamente, verifique!');
    SetarFoco(FrEmpresa);
    Abort;
  end;

  if (not FrLocais.EstaVazio) and (rgTipoEstoque.GetValor <> 'FIS') then begin
    _Biblioteca.Exclamar('Ao filtrar por local s� � permitido selecionar tipo de estoque fisico!');
    SetarFoco(rgTipoEstoque);
    Abort;
  end;

  if (not FrLocais.EstaVazio) and ((eMes.AsInt > 0) or (eAno.AsInt > 0)) then begin
    _Biblioteca.Exclamar('Ao filtrar por local n�o � permitido estoque retroativo!');
    SetarFoco(eMes);
    Abort;
  end;
end;

procedure TFormRelacaoEstoque.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vEstoques: TArray<RecRelacaoEstoque>;
  vCusto: Double;
  VEstoque: Double;
  vEstoqueFiscal: Double;
  vColunaEstoque: string;
begin
  inherited;
  periodoEstoqueRetroativo := '';
  valorEstoqueRetroativo := false;
  sgItens.ClearGrid;
  vEstoques := nil;
  stQtdeProdutos.AsDouble := 0;
  stQtdeTotalEstoque.AsDouble := 0;
  stValorTotalEstoque.AsDouble := 0;

  stQtdeProdutosFiscal.AsDouble := 0;
  stQtdeTotalEstoqueFiscal.AsDouble := 0;
  stValorTotalEstoqueFiscal.AsDouble := 0;

  if rgTipoEstoque.GetValor = 'FISXFIS' then begin
    sgItens.MostrarColunas([coEstoqueFiscal, coValorFiscal]);
    st1.Caption := 'Totais f�sico';
    Panel1.Visible := True;
  end
  else begin
    sgItens.OcultarColunas([coEstoqueFiscal, coValorFiscal]);
    st1.Caption := 'Totais';
    Panel1.Visible := False;
  end;

  vSql := ' where ' + FrEmpresa.getSqlFiltros('EST.EMPRESA_ID');

  if not FrProdutos.EstaVazio then
    vSql := vSql + ' and ' + FrProdutos.getSqlFiltros('PRO.PRODUTO_ID');

  if not FrDepartamentosSecoesLinhas.EstaVazio then
    vSql := vSql + ' and ' + FrDepartamentosSecoesLinhas.getSqlFiltros('PRO.LINHA_PRODUTO_ID');

  if not FrDepartamentosSecoesLinhas.EstaVazio then
    vSql := vSql + ' and ' + FrDepartamentosSecoesLinhas.getSqlFiltros('PRO.LINHA_PRODUTO_ID');

  if not FrDataCadastroProduto.NenhumaDataValida then
    vSql := vSql + ' and ' + FrDataCadastroProduto.getSqlFiltros('PRO.DATA_CADASTRO');

  if not FrMarcas.EstaVazio then
    vSql := vSql + ' and ' + FrMarcas.getSqlFiltros('PRO.MARCA_ID');

  if cbTipoControleEstoque.GetValor <> 'NaoFiltrar' then
    vSql := vSql + ' and PRO.TIPO_CONTROLE_ESTOQUE = ''' + cbTipoControleEstoque.GetValor + ''' ';

  if cbAtivo.GetValor <> 'NaoFiltrar' then
    vSql := vSql + ' and PRO.ATIVO = ''' + cbAtivo.GetValor + ''' ';

  vColunaEstoque :=
    _Biblioteca.Decode(
      rgTipoEstoque.GetValor, [
      'FIS', 'EST.FISICO',
      'DIS', 'EST.DISPONIVEL',
      'RES', 'EST.RESERVADO',
      'FISCAL', 'EST.ESTOQUE',
      'FISXFIS', 'EST.FISICO'
    ]);

  if cbQuantidadeEmEstoque.GetValor <> 'NaoFiltrar' then begin
    if cbQuantidadeEmEstoque.GetValor = 'POS' then
      vSql := vSql + ' and ' + vColunaEstoque + ' > 0 '
    else if cbQuantidadeEmEstoque.GetValor = 'NEG' then
      vSql := vSql + ' and ' + vColunaEstoque + ' < 0 '
    else
      vSql := vSql + ' and ' + vColunaEstoque + ' = 0 ';
  end;

  if cbPaiFilho.GetValor = 'F' then begin
    vSql := vSql + ' and PRO.PRODUTO_ID <> PRO.PRODUTO_PAI_ID ';
//    vSql := vSql + ' and PRO.PRODUTO_ID NOT IN (' +
//      '  SELECT ' +
//      '    PRODUTO_PAI_ID AS PRODUTO_ID ' +
//      '  FROM PRODUTOS ' +
//      '  WHERE PRODUTO_ID <> PRODUTO_PAI_ID ' +
//      '  GROUP BY PRODUTO_PAI_ID ' +
//      ') ';
  end;

  if cbPaiFilho.GetValor = 'P' then begin
    vSql := vSql + ' and PRO.PRODUTO_ID = PRO.PRODUTO_PAI_ID ';
//    vSql := vSql + ' and PRO.PRODUTO_ID IN (' +
//      '  SELECT ' +
//      '    PRODUTO_ID ' +
//      '  FROM PRODUTOS ' +
//      '  WHERE PRODUTO_ID = PRODUTO_PAI_ID ' +
//      ') ';
  end;


  if not FrLocais.EstaVazio then begin
    vSql := vSql + ' and ' + FrLocais.getSqlFiltros('EST.LOCAL_ID');
    vEstoques := _RelacaoEstoque.BuscarEstoqueLocais( Sessao.getConexaoBanco, vSql );
    valorEstoqueRetroativo := False;
  end
  else if (eMes.AsInt > 0) and (eAno.AsInt > 0) then begin
    if Length(IntToStr(eMes.AsInt)) = 2 then
      periodoEstoqueRetroativo := IntToStr(eAno.AsInt) + IntToStr(eMes.AsInt)
    else
      periodoEstoqueRetroativo := IntToStr(eAno.AsInt) + '0' + IntToStr(eMes.AsInt);

    vSql := vSql + ' and EST.ANO_MES = ' + periodoEstoqueRetroativo;
    vSql := vSql + ' and CUS.ANO_MES = ' + periodoEstoqueRetroativo;

    vSql := vSql + ' order by PRO.NOME ';

    vEstoques := _RelacaoEstoque.BuscarEstoqueRetroativo( Sessao.getConexaoBanco, vSql );
    valorEstoqueRetroativo := True;
  end
  else begin
    vSql := vSql + ' order by PRO.NOME ';

    vEstoques := _RelacaoEstoque.BuscarEstoque( Sessao.getConexaoBanco, vSql );
    valorEstoqueRetroativo := False;
  end;

  if vEstoques = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  for i := Low(vEstoques) to High(vEstoques) do begin
    if rgTipoCusto.GetValor = 'FIN' then
      vCusto := vEstoques[i].PrecoFinal
    else if rgTipoCusto.GetValor = 'COM' then
      vCusto := vEstoques[i].PrecoCompra
    else
      vCusto := vEstoques[i].CMV;

    if rgTipoEstoque.GetValor = 'FIS' then
      vEstoque := vEstoques[i].Fisico
    else if rgTipoEstoque.GetValor = 'DIS' then
      vEstoque := vEstoques[i].Disponivel
    else if rgTipoEstoque.GetValor = 'RES' then
      vEstoque := vEstoques[i].Reservado
    else if rgTipoEstoque.GetValor = 'FISXFIS' then begin
      vEstoque       := vEstoques[i].Fisico;
      vEstoqueFiscal := vEstoques[i].EstoqueFiscal;
    end
    else
      vEstoque := vEstoques[i].EstoqueFiscal;

    sgItens.Cells[coProdutoId, i + 1]       := NFormat(vEstoques[i].ProdutoId);
    sgItens.Cells[coNome, i + 1]            := vEstoques[i].NomeProduto;
    sgItens.Cells[coUnidade, i + 1]         := vEstoques[i].UnidadeVenda;
    sgItens.Cells[coMarca, i + 1]           := NFormatN(vEstoques[i].MarcaId) + ' - ' + vEstoques[i].NomeMarca;
    sgItens.Cells[coNCM, i + 1]             := vEstoques[i].NCM;

    if rgTipoEstoque.GetValor = 'FISXFIS' then begin
      sgItens.Cells[coEstoque, i + 1]         := NFormatNEstoque(vEstoque);
      sgItens.Cells[coValor, i + 1]           := NFormatN(vEstoque * vCusto, 4);
      sgItens.Cells[coEstoqueFiscal, i + 1]   := NFormatNEstoque(vEstoqueFiscal);
      sgItens.Cells[coValorFiscal, i + 1]     := NFormatN(vEstoqueFiscal * vCusto, 4);
    end
    else begin
      sgItens.Cells[coEstoque, i + 1]         := NFormatNEstoque(vEstoque);
      sgItens.Cells[coValor, i + 1]           := NFormatN(vEstoque * vCusto, 4);
    end;

    sgItens.Cells[coCustoUnitario, i + 1]   := NFormatN(vCusto, 4);

    stQtdeTotalEstoque.Somar( SFormatDouble(sgItens.Cells[coEstoque, i + 1]) );
    stValorTotalEstoque.Somar( SFormatDouble(sgItens.Cells[coValor, i + 1]) );
    stQtdeTotalEstoqueFiscal.Somar( SFormatDouble(sgItens.Cells[coEstoqueFiscal, i + 1]) );
    stValorTotalEstoqueFiscal.Somar( SFormatDouble(sgItens.Cells[coValorFiscal, i + 1]) );
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( Length(vEstoques) );

  stQtdeProdutos.Somar( Length(vEstoques) );
  stQtdeProdutosFiscal.Caption := stQtdeProdutos.Caption;

  SetarFoco(sgItens);
end;

procedure TFormRelacaoEstoque.Imprimir(Sender: TObject);
var
  vCnt: Integer;
  total: Double;
  totalUnitario: Double;
begin
  cdsReceber.Close;
  cdsReceber.CreateDataSet;
  cdsReceber.Open;
  inherited;
  total := 0.0;
  totalUnitario := 0.0;
  for vCnt := sgItens.RowCount - 1 downto 1 do
  begin
    cdsReceber.Insert;
    cdsReceberProdutoId.AsString       := sgItens.Cells[coProdutoId,vCnt];
    cdsReceberProdutoNome.AsString    := sgItens.Cells[coNome,vCnt];
    cdsReceberUnidade.AsString  := sgItens.Cells[coUnidade,vCnt];
    cdsReceberEstoque.AsFloat         := SFormatDouble(sgItens.Cells[coEstoque,vCnt]);
    cdsReceberCustoUnitario.AsFloat  := SFormatDouble(sgItens.Cells[coCustoUnitario,vCnt]);
    cdsReceberCustoTotal.AsFloat  := SFormatDouble(sgItens.Cells[coValor,vCnt]);

    totalUnitario := totalUnitario + SFormatDouble(sgItens.Cells[coCustoUnitario,vCnt]);
    total := total + SFormatDouble(sgItens.Cells[coValor,vCnt]);

    cdsReceber.Post;
  end;

  TfrxMemoView(frxReport.FindComponent('mmValorLivro')).Text := eLivro.Text;
  TfrxMemoView(frxReport.FindComponent('mmTotalUnitario')).Text := NFormat(totalUnitario);
  TfrxMemoView(frxReport.FindComponent('mmTotal')).Text := NFormat(total);

  if periodoEstoqueRetroativo <> '' then begin
    periodoEstoqueRetroativo := Copy(periodoEstoqueRetroativo, 5, 2) + '/' + Copy(periodoEstoqueRetroativo, 1, 4);

    if StrToInt(Copy(periodoEstoqueRetroativo, 0, 2)) in ([1, 3, 5, 7, 8, 10, 12]) then
      periodoEstoqueRetroativo := '31/' + periodoEstoqueRetroativo
    else if StrToInt(Copy(periodoEstoqueRetroativo, 0, 2)) in [4, 6, 9, 11] then
      periodoEstoqueRetroativo := '30/' + periodoEstoqueRetroativo
    else
      periodoEstoqueRetroativo := '28/' + periodoEstoqueRetroativo;

    TfrxMemoView(frxReport.FindComponent('mmDataEstoque')).Text := periodoEstoqueRetroativo;
  end
  else
    TfrxMemoView(frxReport.FindComponent('mmDataEstoque')).Text := getDataAtual;

  frxReport.ShowReport;
end;

end.

