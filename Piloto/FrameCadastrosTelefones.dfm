inherited FrCadastrosTelefones: TFrCadastrosTelefones
  Width = 587
  Height = 220
  ExplicitWidth = 587
  ExplicitHeight = 220
  object lb14: TLabel
    Left = 0
    Top = 0
    Width = 20
    Height = 13
    Caption = 'Tipo'
  end
  object lb11: TLabel
    Left = 173
    Top = 0
    Width = 42
    Height = 13
    Caption = 'Telefone'
  end
  object lb9: TLabel
    Left = 341
    Top = 0
    Width = 58
    Height = 13
    Caption = 'Observa'#231#227'o'
  end
  object lb3: TLabel
    Left = 296
    Top = 0
    Width = 29
    Height = 13
    Caption = 'Ramal'
  end
  object sgTelefones: TGridLuka
    Left = 0
    Top = 36
    Width = 587
    Height = 184
    TabStop = False
    Align = alBottom
    ColCount = 4
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 4
    OnDblClick = sgTelefonesDblClick
    OnDrawCell = sgTelefonesDrawCell
    OnKeyDown = sgTelefonesKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Tipo'
      'Telefone'
      'Ramal'
      'Observa'#231#227'o')
    Grid3D = False
    RealColCount = 6
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      90
      119
      55
      292)
  end
  object eTelefone: TEditTelefoneLuka
    Left = 173
    Top = 13
    Width = 119
    Height = 21
    EditMask = '(99) 9999-9999;1; '
    MaxLength = 14
    TabOrder = 1
    Text = '(  )     -    '
    OnKeyDown = ProximoCampo
  end
  object eRamal: TEditLuka
    Left = 296
    Top = 13
    Width = 41
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eObservacao: TMemo
    Left = 341
    Top = 13
    Width = 246
    Height = 21
    TabOrder = 3
    OnKeyDown = eObservacaoKeyDown
  end
  object cbTipoTelefone: TComboBoxLuka
    Left = 0
    Top = 13
    Width = 170
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 0
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Residencial'
      'Comercial'
      'Fax'
      'Celular'
      'Recado'
      'Pai/Mae'
      'Gratuito')
    Valores.Strings = (
      'R'
      'C'
      'F'
      'E'
      'A'
      'P'
      'G')
    AsInt = 0
  end
end
