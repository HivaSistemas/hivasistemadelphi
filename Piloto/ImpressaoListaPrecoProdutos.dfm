inherited FormImpressaoListaPrecoProdutos: TFormImpressaoListaPrecoProdutos
  Caption = 'FormImpressaoListaPrecoProdutos'
  ExplicitWidth = 1030
  ExplicitHeight = 754
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioNF: TQuickRep
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.Values = (
      0.000000000000000000
      509.300000000000000000
      0.000000000000000000
      750.100000000000000000
      0.000000000000000000
      0.000000000000000000
      0.000000000000000000)
    inherited qrBandTitulo: TQRBand
      Size.Values = (
        289.388020833333300000
        750.755208333333300000)
      inherited qrlNFNomeEmpresa: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          16.536458333333330000
          744.140625000000000000)
        FontSize = 6
      end
      inherited qrNFCnpj: TQRLabel
        Top = 31
        Size.Values = (
          33.072916666666670000
          6.614583333333332000
          51.263020833333340000
          744.140625000000000000)
        FontSize = 7
        ExplicitTop = 31
      end
      inherited qrNFEndereco: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          85.989583333333340000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFCidadeUf: TQRLabel
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          122.369791666666700000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFTipoDocumento: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          211.666666666666700000
          719.335937500000000000)
        FontSize = 7
      end
      inherited qrSeparador2: TQRShape
        Size.Values = (
          3.307291666666666000
          3.307291666666666000
          281.119791666666700000
          724.296875000000000000)
      end
      inherited qrNFTitulo: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          246.393229166666700000
          719.335937500000000000)
        FontSize = 7
      end
      inherited qrlNFEmitidoEm: TQRLabel
        Size.Values = (
          23.151041666666670000
          370.416666666666700000
          3.307291666666666000
          357.187500000000000000)
        FontSize = -6
      end
      inherited qrlNFTelefoneEmpresa: TQRLabel
        Top = 101
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          167.018229166666700000
          744.140625000000000000)
        FontSize = 7
        ExplicitTop = 101
      end
    end
    inherited PageFooterBand1: TQRBand
      Size.Values = (
        24.804687500000000000
        750.755208333333300000)
      inherited qrlNFSistema: TQRLabel
        Size.Values = (
          23.151041666666670000
          582.083333333333400000
          0.000000000000000000
          138.906250000000000000)
        FontSize = -6
      end
    end
  end
  inherited qrRelatorioA4: TQuickRep
    Left = 43
    Top = 0
    BeforePrint = qrRelatorioA4BeforePrint
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrRelatorioA4NeedData
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    ExplicitLeft = 43
    ExplicitTop = 0
    inherited qrCabecalho: TQRBand
      Height = 116
      Size.Values = (
        306.916666666666700000
        1899.708333333333000000)
      ExplicitHeight = 116
      inherited qrCaptionEndereco: TQRLabel
        Top = 18
        Width = 55
        Height = 15
        Size.Values = (
          39.687500000000000000
          222.250000000000000000
          47.625000000000000000
          145.520833333333300000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitTop = 18
        ExplicitWidth = 55
        ExplicitHeight = 15
      end
      inherited qrEmpresa: TQRLabel
        Left = 139
        Width = 248
        Height = 15
        Size.Values = (
          39.687500000000000000
          367.770833333333300000
          7.937500000000000000
          656.166666666666700000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        FontSize = 9
        ExplicitLeft = 139
        ExplicitWidth = 248
        ExplicitHeight = 15
      end
      inherited qrEndereco: TQRLabel
        Left = 139
        Top = 18
        Width = 248
        Height = 15
        Size.Values = (
          39.687500000000000000
          367.770833333333300000
          47.625000000000000000
          656.166666666666700000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 139
        ExplicitTop = 18
        ExplicitWidth = 248
        ExplicitHeight = 15
      end
      inherited qrLogoEmpresa: TQRImage
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
      end
      inherited qrCaptionEmpresa: TQRLabel
        Width = 55
        Height = 15
        Size.Values = (
          39.687500000000000000
          222.250000000000000000
          7.937500000000000000
          145.520833333333300000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitWidth = 55
        ExplicitHeight = 15
      end
      inherited qrEmitidoEm: TQRLabel
        Size.Values = (
          29.104166666666670000
          1529.291666666667000000
          0.000000000000000000
          370.416666666666700000)
        Font.Height = -8
        Font.Name = 'Cambria'
        FontSize = 6
      end
      inherited qr1: TQRLabel
        Left = 393
        Top = 18
        Width = 44
        Height = 15
        Size.Values = (
          39.687500000000000000
          1039.812500000000000000
          47.625000000000000000
          116.416666666666700000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 393
        ExplicitTop = 18
        ExplicitWidth = 44
        ExplicitHeight = 15
      end
      inherited qrBairro: TQRLabel
        Left = 437
        Top = 18
        Width = 275
        Height = 15
        Size.Values = (
          39.687500000000000000
          1156.229166666667000000
          47.625000000000000000
          727.604166666666700000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 437
        ExplicitTop = 18
        ExplicitWidth = 275
        ExplicitHeight = 15
      end
      inherited qr3: TQRLabel
        Left = 393
        Top = 33
        Width = 44
        Height = 15
        Size.Values = (
          39.687500000000000000
          1039.812500000000000000
          87.312500000000000000
          116.416666666666700000)
        Alignment = taLeftJustify
        Caption = 'Cid./UF: '
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 393
        ExplicitTop = 33
        ExplicitWidth = 44
        ExplicitHeight = 15
      end
      inherited qrCidadeUf: TQRLabel
        Left = 437
        Top = 33
        Width = 275
        Height = 15
        Size.Values = (
          39.687500000000000000
          1156.229166666667000000
          87.312500000000000000
          727.604166666666700000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 437
        ExplicitTop = 33
        ExplicitWidth = 275
        ExplicitHeight = 15
      end
      inherited qr5: TQRLabel
        Left = 393
        Width = 44
        Height = 15
        Size.Values = (
          39.687500000000000000
          1039.812500000000000000
          7.937500000000000000
          116.416666666666700000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 393
        ExplicitWidth = 44
        ExplicitHeight = 15
      end
      inherited qrCNPJ: TQRLabel
        Left = 437
        Width = 121
        Height = 15
        Size.Values = (
          39.687500000000000000
          1156.229166666667000000
          7.937500000000000000
          320.145833333333300000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 437
        ExplicitWidth = 121
        ExplicitHeight = 15
      end
      inherited qr7: TQRLabel
        Top = 33
        Width = 55
        Height = 15
        Size.Values = (
          39.687500000000000000
          222.250000000000000000
          87.312500000000000000
          145.520833333333300000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitTop = 33
        ExplicitWidth = 55
        ExplicitHeight = 15
      end
      inherited qrTelefone: TQRLabel
        Left = 139
        Top = 33
        Width = 94
        Height = 15
        Size.Values = (
          39.687500000000000000
          367.770833333333300000
          87.312500000000000000
          248.708333333333300000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 139
        ExplicitTop = 33
        ExplicitWidth = 94
        ExplicitHeight = 15
      end
      inherited qrFax: TQRLabel
        Left = 139
        Top = 48
        Width = 110
        Height = 15
        Size.Values = (
          39.687500000000000000
          367.770833333333300000
          127.000000000000000000
          291.041666666666700000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 139
        ExplicitTop = 48
        ExplicitWidth = 110
        ExplicitHeight = 15
      end
      inherited qr10: TQRLabel
        Left = 84
        Top = 48
        Width = 55
        Height = 15
        Size.Values = (
          39.687500000000000000
          222.250000000000000000
          127.000000000000000000
          145.520833333333300000)
        Alignment = taLeftJustify
        Caption = 'Celular:'
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 84
        ExplicitTop = 48
        ExplicitWidth = 55
        ExplicitHeight = 15
      end
      inherited qr11: TQRLabel
        Left = 393
        Top = 48
        Width = 44
        Height = 15
        Size.Values = (
          39.687500000000000000
          1039.812500000000000000
          127.000000000000000000
          116.416666666666700000)
        Alignment = taLeftJustify
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        FontSize = 9
        ExplicitLeft = 393
        ExplicitTop = 48
        ExplicitWidth = 44
        ExplicitHeight = 15
      end
      inherited qrEmail: TQRLabel
        Left = 437
        Top = 48
        Width = 275
        Height = 15
        Size.Values = (
          39.687500000000000000
          1156.229166666667000000
          127.000000000000000000
          727.604166666666700000)
        Font.Charset = ANSI_CHARSET
        Font.Height = -12
        Font.Name = 'Cambria'
        FontSize = 9
        ExplicitLeft = 437
        ExplicitTop = 48
        ExplicitWidth = 275
        ExplicitHeight = 15
      end
      inherited qr13: TQRLabel
        Left = 243
        Top = 83
        Width = 210
        Height = 25
        Size.Values = (
          66.145833333333330000
          642.937500000000000000
          219.604166666666700000
          555.625000000000000000)
        Caption = 'LISTA DE PRE'#199'OS'
        Font.Charset = ANSI_CHARSET
        Font.Height = -20
        Font.Name = 'Cambria'
        FontSize = 15
        ExplicitLeft = 243
        ExplicitTop = 83
        ExplicitWidth = 210
        ExplicitHeight = 25
      end
      object QRShape1: TQRShape
        Left = -1
        Top = 109
        Width = 718
        Height = 2
        Size.Values = (
          5.291666666666667000
          -2.645833333333333000
          288.395833333333300000
          1899.708333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object QRShape2: TQRShape
        Left = -1
        Top = 108
        Width = 718
        Height = 2
        Size.Values = (
          5.291666666666667000
          -2.645833333333333000
          285.750000000000000000
          1899.708333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
    end
    inherited qrbndRodape: TQRBand
      Top = 194
      Height = 46
      Size.Values = (
        121.708333333333300000
        1899.708333333333000000)
      ExplicitTop = 194
      ExplicitHeight = 46
      inherited qrUsuarioImpressao: TQRLabel
        Top = 29
        Height = 13
        Size.Values = (
          34.395833333333330000
          7.937500000000000000
          76.729166666666670000
          354.541666666666700000)
        Caption = '1 - HIVA SISTEMAS'
        Font.Height = -11
        Font.Name = 'Cambria'
        FontSize = 8
        ExplicitTop = 29
        ExplicitHeight = 13
      end
      inherited qrSistema: TQRLabel
        Top = 27
        Height = 13
        Size.Values = (
          34.395833333333330000
          1529.291666666667000000
          71.437500000000000000
          370.416666666666700000)
        Caption = 'Hiva 3.0'
        Font.Height = -11
        Font.Name = 'Cambria'
        FontSize = 8
        ExplicitTop = 27
        ExplicitHeight = 13
      end
      object qrCondicaoPagamento: TQRLabel
        Left = 3
        Top = 12
        Width = 279
        Height = 13
        Size.Values = (
          34.395833333333330000
          7.937500000000000000
          31.750000000000000000
          738.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Condi'#231#227'o de Pagamento: Dinheiro/Cartao'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 8
      end
    end
    object qrbndColumnHeaderBand1: TQRBand
      Left = 38
      Top = 154
      Width = 718
      Height = 16
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        42.333333333333330000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbColumnHeader
      object qr23: TQRLabel
        Left = 3
        Top = 2
        Width = 40
        Height = 15
        Size.Values = (
          39.687500000000000000
          7.937500000000000000
          5.291666666666667000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Prod.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qr24: TQRLabel
        Left = 43
        Top = 2
        Width = 471
        Height = 15
        Size.Values = (
          39.687500000000000000
          113.770833333333300000
          5.291666666666667000
          1246.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel6: TQRLabel
        Left = 623
        Top = 2
        Width = 85
        Height = 15
        Size.Values = (
          39.687500000000000000
          1648.354166666667000000
          5.291666666666667000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pre'#231'o'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRLabel1: TQRLabel
        Left = 518
        Top = 2
        Width = 102
        Height = 15
        Size.Values = (
          39.687500000000000000
          1370.541666666667000000
          5.291666666666667000
          269.875000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cod. Fabricante'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
    end
    object qrbndDetailBand1: TQRBand
      Left = 38
      Top = 170
      Width = 718
      Height = 24
      AlignToBottom = False
      BeforePrint = qrbndDetailBand1BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        63.500000000000000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrProdutoId: TQRLabel
        Left = 3
        Top = 2
        Width = 40
        Height = 15
        Size.Values = (
          39.687500000000000000
          7.937500000000000000
          5.291666666666667000
          105.833333333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Prod.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrNomeProduto: TQRLabel
        Left = 43
        Top = 2
        Width = 471
        Height = 15
        Size.Values = (
          39.687500000000000000
          113.770833333333300000
          5.291666666666667000
          1246.187500000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Nome'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object qrPrecoProduto: TQRLabel
        Left = 623
        Top = 2
        Width = 85
        Height = 15
        Size.Values = (
          39.687500000000000000
          1648.354166666667000000
          5.291666666666667000
          224.895833333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '999,99'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
      object QRShape7: TQRShape
        Left = -1
        Top = 17
        Width = 718
        Height = 2
        Size.Values = (
          5.291666666666667000
          -2.645833333333333000
          44.979166666666670000
          1899.708333333333000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Shape = qrsHorLine
        VertAdjust = 0
      end
      object qrCodigoFabricante: TQRLabel
        Left = 518
        Top = 2
        Width = 102
        Height = 15
        Size.Values = (
          39.687500000000000000
          1370.541666666667000000
          5.291666666666667000
          269.875000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cod. Fabricante'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Cambria'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 9
      end
    end
  end
end
