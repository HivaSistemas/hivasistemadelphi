unit BloquearDesbloquearTitulos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls,
  MemoAltis, Vcl.ExtCtrls, RadioGroupLuka, Vcl.Buttons, _Biblioteca, _Sessao, _ContasPagar,
  _RecordsEspeciais;

type
  TFormBloquearDesbloquearTitulos = class(TFormHerancaFinalizar)
    rgBloquearDesbloquear: TRadioGroupLuka;
    meMotivoBloqueio: TMemoAltis;
  private
    FContasPagarIds: TArray<Integer>;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Bloquear(pContasPagarIds: TArray<Integer>): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

function Bloquear(pContasPagarIds: TArray<Integer>): TRetornoTelaFinalizar;
var
  vForm: TFormBloquearDesbloquearTitulos;
begin
  vForm := TFormBloquearDesbloquearTitulos.Create(nil);

  vForm.FContasPagarIds := pContasPagarIds;
  Result.Ok(vForm.ShowModal, True);
end;

{ TFormBloquearDesbloquearTitulos }

procedure TFormBloquearDesbloquearTitulos.Finalizar(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco :=
    _ContasPagar.AtualizarBloquearDesbloquear(
      Sessao.getConexaoBanco,
      FContasPagarIds,
      rgBloquearDesbloquear.GetValor,
      meMotivoBloqueio.Lines.Text
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  inherited;
end;

procedure TFormBloquearDesbloquearTitulos.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if rgBloquearDesbloquear.GetValor = '' then begin
    _Biblioteca.Exclamar('� necess�rio selecionar uma op��o de bloqueio!');
    SetarFoco(rgBloquearDesbloquear);
    Abort;
  end;

  meMotivoBloqueio.Lines.Text := Trim(meMotivoBloqueio.Lines.Text);

  if (rgBloquearDesbloquear.GetValor = 'S') and (meMotivoBloqueio.Lines.Text = '') then begin
    _Biblioteca.Exclamar('� necess�rio informar o motivo de bloqueio!');
    SetarFoco(meMotivoBloqueio);
    Abort;
  end;

end;

end.
