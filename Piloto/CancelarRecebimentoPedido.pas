unit CancelarRecebimentoPedido;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.Mask, _RecordsOrcamentosVendas,
  EditLukaData, Vcl.StdCtrls, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca,
  _Orcamentos, _RecordsEspeciais, _NotasFiscais, _RecordsNotasFiscais, System.DateUtils,
  Informacoes.Orcamento, Pesquisa.Orcamentos, _HerancaCadastro, _ComunicacaoNFE,
  CheckBoxLuka, SpeedButtonLuka;

type
  TFormCancelarRecebimentoPedido = class(TFormHerancaCadastroCodigo)
    lb2: TLabel;
    lb12: TLabel;
    eCliente: TEditLuka;
    eVendedor: TEditLuka;
    lb18: TLabel;
    eCondicaoPagamento: TEditLuka;
    lb3: TLabel;
    eDataCadastro: TEditLukaData;
    lb5: TLabel;
    eTotalOrcamento: TEditLuka;
    sbInformacoesOrcamento: TSpeedButtonLuka;
    procedure sbInformacoesOrcamentoClick(Sender: TObject);
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); reintroduce;
    procedure FormCreate(Sender: TObject);
  private
    procedure PreencherRegistro(pOrcamento: RecOrcamentos);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses BuscaDados;

{ TFormCancelarRecebimentoPedido }

procedure TFormCancelarRecebimentoPedido.BuscarRegistro;
var
  pOrcamento: TArray<RecOrcamentos>;
begin
  pOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if pOrcamento = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end
  else if Em(pOrcamento[0].status, ['OE','VB','VR','CA']) then begin
    _Biblioteca.Exclamar('Somente or�amentos j� recebidos podem ser cancelados, verifique!');
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(pOrcamento[0]);
end;

procedure TFormCancelarRecebimentoPedido.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key <> VK_RETURN then
    Exit;

  if eID.AsInt = 0 then begin
    Exclamar('� necess�rio informar o n�mero do pedido!');
    SetarFoco(eID);
    Abort;
  end;

  BuscarRegistro;
end;

procedure TFormCancelarRecebimentoPedido.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormCancelarRecebimentoPedido.FormCreate(Sender: TObject);
begin
//  inherited;  N�o chamar a herana�a neste tela para n�o bagun�ar o funcionamento
  Modo(False);
end;

procedure TFormCancelarRecebimentoPedido.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;
  vRetorno := _Orcamentos.CancelarRecebimentoPedido(Sessao.getConexaoBanco, eId.AsInt);
  Sessao.AbortarSeHouveErro(vRetorno);

  RotinaSucesso;
  SetarFoco(eId);
end;

procedure TFormCancelarRecebimentoPedido.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eTotalOrcamento, sbInformacoesOrcamento, eCliente, eVendedor, eCondicaoPagamento], pEditando);
  //_Biblioteca.LimparCampos([eCliente, eVendedor, eDataCadastro, eCondicaoPagamento, eTotalOrcamento, sbInformacoesOrcamento]);

  if pEditando then
    eTotalOrcamento.SetFocus;
end;

procedure TFormCancelarRecebimentoPedido.PesquisarRegistro;
var
  vRetTela: TRetornoTelaFinalizar<RecOrcamentos>;
begin
  vRetTela := Pesquisa.Orcamentos.Pesquisar(tpVendasDevolucao);
  if vRetTela.BuscaCancelada then
    Exit;

  inherited;
  PreencherRegistro(vRetTela.Dados);
end;

procedure TFormCancelarRecebimentoPedido.PreencherRegistro(pOrcamento: RecOrcamentos);
begin
  eID.SetInformacao(pOrcamento.orcamento_id);
  eCliente.Text            := pOrcamento.nome_cliente;
  eVendedor.Text           := pOrcamento.nome_vendedor;
  eCondicaoPagamento.Text  := pOrcamento.nome_condicao_pagto;
  eDataCadastro.AsData     := pOrcamento.data_cadastro;
  eTotalOrcamento.AsDouble := pOrcamento.valor_total;
end;

procedure TFormCancelarRecebimentoPedido.sbInformacoesOrcamentoClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(eID.IdInformacao);
end;

procedure TFormCancelarRecebimentoPedido.VerificarRegistro(Sender: TObject);
var
  vNota: TArray<RecNotaFiscal>;
  vRetBanco: RecRetornoBD;

  oNFe: TComunicacaoNFe;
  vJustificativa: string;
begin
  inherited;

  vRetBanco := _Orcamentos.PedidoPodeSerCancelado(Sessao.getConexaoBanco, eId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  if not Perguntar('Esta rotina � irrevers�vel. Deseja realmente cancelar este pedido?') then
    Abort;

  vNota := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 1, [eID.AsInt]);
  if vNota <> nil  then begin
    if (Em(vNota[0].tipo_nota, ['C', 'N'])) and (vNota[0].status = 'E') then begin
      if not _Biblioteca.Perguntar('Existe uma ' + vNota[0].TipoNotaAnalitico + ' emitida para esta venda, a mesma ser� cancelada se poss�vel, deseja realmente continuar?' ) then
        Abort;

      vRetBanco := _NotasFiscais.PodeCancelarNFe(Sessao.getConexaoBanco, vNota[0].nota_fiscal_id);
      if vRetBanco.TeveErro then begin
        _Biblioteca.Exclamar(vRetBanco.MensagemErro);
        Abort;
      end;

      vJustificativa := BuscaDados.BuscarDados('Justificativa', '');
      if vJustificativa = '' then begin
        Exclamar('� necess�rio informar a justificativa para cancelamento da NF-e!');
        Abort;
      end;

      if Length(Trim(vJustificativa)) < 15 then begin
        Exclamar('A justificativa para cancelamento da NF-e deve ter no m�nimo 15 caracteres!');
        Abort;
      end;

      oNFe := TComunicacaoNFe.Create(Self);
      if not oNFe.CancelarNFe(vNota[0].nota_fiscal_id, vJustificativa) then begin
        oNFe.Free;
        Abort;
      end;

      oNFe.Free;
    end;
  end;
end;

end.
