unit RelacaoComissoesPorProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, System.Math,
  FrameProdutos, FrameDataInicialFinal, FrameFuncionarios, _Sessao, _Biblioteca,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, Vcl.ComCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.StdCtrls, _RecordsOrcamentosVendas,
  _RelacaoComissoesPorProduto, Informacoes.Orcamento, Informacoes.Devolucao,
  CheckBoxLuka;

type
  TFormRelacaoComissoesPorProduto = class(TFormHerancaRelatoriosPageControl)
    FrFuncionarios: TFrFuncionarios;
    FrDataCadastro: TFrDataInicialFinal;
    FrProdutos: TFrProdutos;
    sgProdutos: TGridLuka;
    sp1: TSplitter;
    st1: TStaticText;
    sgPedidos: TGridLuka;
    st2: TStaticText;
    FrEmpresas: TFrEmpresas;
    procedure FormCreate(Sender: TObject);
    procedure sgPedidosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgPedidosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgPedidosDblClick(Sender: TObject);
    procedure sgPedidosClick(Sender: TObject);
  private
    FComissoes: TArray<RecComissoesPorProdutos>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormRelacaoComissoesPorProduto }

const
  coTipo          = 0;
  coId            = 1;
  coData          = 2;
  coCliente       = 3;
  coValorVenda    = 4;
  coBaseComissao  = 5;
  coTipoComissao  = 6;
  coPercComissao  = 7;
  coValorComissao = 8;
  (* Colunas ocultas *)
  coTipoLinha     = 9;

  (* Grid de produtos *)
  cpProdutoId     = 0;
  cpNome          = 1;
  cpMarca         = 2;
  cpBaseComissao  = 3;
  cpPercComissao  = 4;
  cpValorComissao = 5;

  ctCabecalho   = 'CAB';
  ctLinhaNorm   = 'LIN';
  ctSubTotal    = 'SUB';
  ctTotalizador = 'TOT';

procedure TFormRelacaoComissoesPorProduto.Carregar(Sender: TObject);
var
  i: Integer;
  vComando: string;

  vId: Integer;
  vLinha: Integer;
  vVendedorId: Integer;

  vBaseComissaoVenda: Double;
  vValorComissaoVenda: Double;

  vTotalValorVenda: Double;
  vTotalBaseComissaoVenda: Double;
  vTotalValorComissaoVenda: Double;

  vTotalValorVendaGeral: Double;
  vTotalBaseComissaoVendaGeral: Double;
  vTotalValorComissaoVendaGeral: Double;

  procedure TotalizarVendedor;
  begin
    sgPedidos.Cells[coCliente, vLinha]       := 'Total: ';
    sgPedidos.Cells[coValorVenda, vLinha]    := NFormatN(vTotalValorVenda);
    sgPedidos.Cells[coBaseComissao, vLinha]  := NFormatN(vTotalBaseComissaoVenda);

    if vTotalBaseComissaoVenda > 0 then
      sgPedidos.Cells[coPercComissao, vLinha]  := NFormatN(vTotalValorComissaoVenda / vTotalBaseComissaoVenda * 100);

    sgPedidos.Cells[coValorComissao, vLinha] := NFormatN(vTotalValorComissaoVenda);
    sgPedidos.Cells[coTipoLinha, vLinha]     := ctSubTotal;

    vTotalValorVendaGeral := vTotalValorVendaGeral + vTotalValorVenda;
    vTotalBaseComissaoVendaGeral := vTotalBaseComissaoVendaGeral + vTotalBaseComissaoVenda;
    vTotalValorComissaoVendaGeral := vTotalValorComissaoVendaGeral + vTotalValorComissaoVenda;

    vTotalValorVenda := 0;
    vTotalBaseComissaoVenda := 0;
    vTotalValorComissaoVenda := 0;

    Inc(vLinha, 2);
  end;

  procedure TotalizarVenda;
  begin
    sgPedidos.Cells[coTipo, vLinha]          := FComissoes[i - 1].Tipo;
    sgPedidos.Cells[coId, vLinha]            := NFormat(FComissoes[i - 1].Id);
    sgPedidos.Cells[coData, vLinha]          := DFormat(FComissoes[i - 1].Data);
    sgPedidos.Cells[coCliente, vLinha]       := NFormat(FComissoes[i - 1].CadastroId) + ' - ' + FComissoes[i - 1].NomeCliente;

    if FComissoes[i - 1].Tipo = 'ORC' then
      sgPedidos.Cells[coValorVenda, vLinha]    := NFormatN(FComissoes[i - 1].ValorVenda);

    sgPedidos.Cells[coBaseComissao, vLinha]  := NFormatN(vBaseComissaoVenda);
    sgPedidos.Cells[coTipoComissao, vLinha]  := FComissoes[i - 1].TipoComissao;

    if vBaseComissaoVenda > 0 then
      sgPedidos.Cells[coPercComissao, vLinha]  := NFormatN(vValorComissaoVenda / vBaseComissaoVenda * 100);

    sgPedidos.Cells[coValorComissao, vLinha] := NFormatN(vValorComissaoVenda);
    sgPedidos.Cells[coTipoLinha, vLinha]     := ctLinhaNorm;

    if FComissoes[i - 1].Tipo = 'ORC' then begin
      vTotalValorVenda := vTotalValorVenda + FComissoes[i - 1].ValorVenda;
      vTotalBaseComissaoVenda := vTotalBaseComissaoVenda + vBaseComissaoVenda;
      vTotalValorComissaoVenda := vTotalValorComissaoVenda + vValorComissaoVenda;
    end
    else begin
      vTotalBaseComissaoVenda := vTotalBaseComissaoVenda - vBaseComissaoVenda;
      vTotalValorComissaoVenda := vTotalValorComissaoVenda - vValorComissaoVenda;
    end;

    vBaseComissaoVenda := 0;
    vValorComissaoVenda := 0;

    Inc(vLinha);
  end;

begin
  inherited;

  FComissoes := nil;
  sgPedidos.ClearGrid;
  sgProdutos.ClearGrid;

  vComando := ' where COM.EMPRESA_ID ' + FrEmpresas.getSqlFiltros + ' ';

  if not FrFuncionarios.EstaVazio then
    vComando := vComando + ' and COM.VENDEDOR_ID ' + FrFuncionarios.getSqlFiltros + ' ';

  if not FrProdutos.EstaVazio then
    vComando := vComando + ' and COM.PRODUTO_ID ' + FrProdutos.getSqlFiltros + ' ';

  if not FrDataCadastro.NenhumaDataValida then
    vComando := vComando + ' and ' + FrDataCadastro.getSqlFiltros('COM.DATA') + ' ';

  vComando :=
    vComando +
      'order by ' +
      '  VENDEDOR_ID, ' +
      '  DATA, ' +
      '  ID ';

  FComissoes := _RelacaoComissoesPorProduto.BuscarComissoes(Sessao.getConexaoBanco, vComando);
  if FComissoes = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vId := -1;
  vLinha := 1;
  vVendedorId := -1;

  vBaseComissaoVenda := 0;
  vValorComissaoVenda := 0;

  vTotalValorVenda := 0;
  vTotalBaseComissaoVenda := 0;
  vTotalValorComissaoVenda := 0;

  vTotalValorVendaGeral := 0;
  vTotalBaseComissaoVendaGeral := 0;
  vTotalValorComissaoVendaGeral := 0;

  for i := Low(FComissoes) to High(FComissoes) do begin
    if i = 0 then
      vId := FComissoes[i].Id
    else if vId <> FComissoes[i].Id then begin
      TotalizarVenda;
      vId := FComissoes[i].Id;
    end;

    vBaseComissaoVenda := vBaseComissaoVenda + FComissoes[i].BaseComissaoProduto;
    vValorComissaoVenda := vValorComissaoVenda + FComissoes[i].ValorComissaoProduto;

    if vVendedorId <> FComissoes[i].VendedorId then begin
      if i > 0 then
        TotalizarVendedor;

      vVendedorId := FComissoes[i].VendedorId;

      sgPedidos.Cells[coId, vLinha]        := '  ' + NFormat(FComissoes[i].VendedorId) + ' - ' + FComissoes[i].NomeFuncionario;
      sgPedidos.Cells[coTipoLinha, vLinha] := ctCabecalho;
      Inc(vLinha);
    end;
  end;
  TotalizarVenda;
  TotalizarVendedor;

  sgPedidos.Cells[coCliente, vLinha]       := 'Total Geral: ';
  sgPedidos.Cells[coValorVenda, vLinha]    := NFormatN(vTotalValorVendaGeral);
  sgPedidos.Cells[coBaseComissao, vLinha]  := NFormatN(vTotalBaseComissaoVendaGeral);

  if vTotalBaseComissaoVendaGeral > 0 then
    sgPedidos.Cells[coPercComissao, vLinha]  := NFormatN(vTotalValorComissaoVendaGeral / vTotalBaseComissaoVendaGeral * 100);

  sgPedidos.Cells[coValorComissao, vLinha] := NFormatN(vTotalValorComissaoVendaGeral);
  sgPedidos.Cells[coTipoLinha, vLinha]     := ctTotalizador;
  Inc(vLinha);

  sgPedidos.RowCount := vLinha;
  pcDados.ActivePage := tsResultado;
end;

procedure TFormRelacaoComissoesPorProduto.FormCreate(Sender: TObject);
begin
  inherited;
  ActiveControl := FrEmpresas.sgPesquisa;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
end;

procedure TFormRelacaoComissoesPorProduto.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormRelacaoComissoesPorProduto.sgPedidosClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
begin
  inherited;
  vLinha := 0;
  sgProdutos.ClearGrid;
  for i := Low(FComissoes) to High(FComissoes) do begin
    if
      (sgPedidos.Cells[coTipo, sgPedidos.Row] <> FComissoes[i].Tipo) or
      (SFormatInt(sgPedidos.Cells[coId, sgPedidos.Row]) <> FComissoes[i].Id)
    then
      Continue;

    Inc(vLinha);

    sgProdutos.Cells[cpProdutoId, vLinha]     := NFormat(FComissoes[i].ProdutoId);
    sgProdutos.Cells[cpNome, vLinha]          := FComissoes[i].NomeProduto;
    sgProdutos.Cells[cpMarca, vLinha]         := NFormat(FComissoes[i].MarcaId) + ' - ' + FComissoes[i].NomeMarca;
    sgProdutos.Cells[cpBaseComissao, vLinha]  := NFormatN(FComissoes[i].BaseComissaoProduto);
    sgProdutos.Cells[cpPercComissao, vLinha]  := NFormatN(FComissoes[i].PercentualComissaoProduto);
    sgProdutos.Cells[cpValorComissao, vLinha] := NFormatN(FComissoes[i].ValorComissaoProduto);
  end;
  sgProdutos.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);
end;

procedure TFormRelacaoComissoesPorProduto.sgPedidosDblClick(Sender: TObject);
begin
  inherited;
  if sgPedidos.Cells[coTipo, sgPedidos.Row] = 'ORC' then
    Informacoes.Orcamento.Informar(SFormatInt(sgPedidos.Cells[coId, sgPedidos.Row]))
  else if sgPedidos.Cells[coTipo, sgPedidos.Row] = 'DEV' then
    Informacoes.Devolucao.Informar(SFormatInt(sgPedidos.Cells[coId, sgPedidos.Row]));
end;

procedure TFormRelacaoComissoesPorProduto.sgPedidosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coId, coValorVenda, coBaseComissao, coPercComissao, coValorComissao] then
    vAlinhamento := taRightJustify
  else if ACol in[coTipo, coTipoComissao] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  if (ARow > 0) and (sgPedidos.Cells[coTipoLinha, ARow] = ctCabecalho) then
    sgPedidos.MergeCells([ARow, ACol], [ARow, coId], [ARow, coCliente], vAlinhamento, Rect)
  else
    sgPedidos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoComissoesPorProduto.sgPedidosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgPedidos.Cells[coTipoLinha, ARow] = ctCabecalho then begin
    AFont.Style  := [fsBold];
    AFont.Color  := clWhite;
    ABrush.Color := $000096DB;
  end
  else if sgPedidos.Cells[coTipoLinha, ARow] = ctSubTotal then begin
    AFont.Style  := [fsBold];
    AFont.Color  := clWhite;
    ABrush.Color := $000096DB;
  end
  else if sgPedidos.Cells[coTipoLinha, ARow] = ctTotalizador then begin
    AFont.Style  := [fsBold];
    AFont.Color  := clWhite;
    ABrush.Color := $006C6C6C;
  end
  else if ACol = coTipo then begin
    AFont.Style := [fsBold];
    if sgPedidos.Cells[coTipo, ARow] = 'DEV' then
      AFont.Color := clRed
    else if sgPedidos.Cells[coTipo, ARow] = 'ORC' then
      AFont.Color := clBlue;
  end
  else if ACol = coTipoComissao then begin
    AFont.Style := [fsBold];
    if sgPedidos.Cells[coTipoComissao, ARow] = 'A vista' then
      AFont.Color := clBlue
    else if sgPedidos.Cells[coTipoComissao, ARow] = 'A prazo' then
      AFont.Color := $000096DB;
  end;
end;

procedure TFormRelacaoComissoesPorProduto.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[cpProdutoId, cpBaseComissao, cpPercComissao, cpValorComissao] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoComissoesPorProduto.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrEmpresas.EstaVazio then begin
    Exclamar('Ao menos uma empresa deve ser informada!');
    SetarFoco(FrEmpresas);
    Abort;
  end;
end;

end.
