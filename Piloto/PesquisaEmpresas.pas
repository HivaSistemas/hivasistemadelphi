unit PesquisaEmpresas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Empresas, _Sessao,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _RecordsCadastros, System.Math, _Biblioteca,
  Vcl.ExtCtrls;

type
  TFormPesquisaEmpresas = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarEmpresa: TObject;
function PesquisarEmpresas: TArray<TObject>;

implementation

uses
  _Funcionarios;

{$R *.dfm}

{ TFormPesquisaEmpresas }

const
  cp_razao_social  = 2;
  cp_nome_fantasia = 3;
  cp_cnpj          = 4;
  cp_ativo         = 5;

function PesquisarEmpresa: TObject;
begin
  Result := _HerancaPesquisas.Pesquisar(TFormPesquisaEmpresas, _Empresas.getFiltros);
end;

function PesquisarEmpresas: TArray<TObject>;
begin
  Result := _HerancaPesquisas.PesquisarVarios(TFormPesquisaEmpresas, _Empresas.getFiltros);
end;

procedure TFormPesquisaEmpresas.BuscarRegistros;
var
  i: Integer;
  vEmpresas: TArray<RecEmpresas>;

  empresasFiltradas: TArray<RecEmpresas>;
  empresasAutorizadas: TArray<Integer>;
  j: Integer;
begin
  inherited;

  vEmpresas :=
    _Empresas.BuscarEmpresas(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if Sessao.getUsuarioLogado.GerenteSistema then
    empresasFiltradas := vEmpresas
  else begin
    empresasAutorizadas := _Funcionarios.BuscarEmpresasFuncionario(
      Sessao.getConexaoBanco,
      Sessao.getUsuarioLogado.funcionario_id
    );

    for i := Low(vEmpresas) to High(vEmpresas) do begin
      for j := Low(empresasAutorizadas) to High(empresasAutorizadas) do begin
        if empresasAutorizadas[j] = vEmpresas[i].EmpresaId then begin
          SetLength(empresasFiltradas, Length(empresasFiltradas) + 1);
          empresasFiltradas[High(empresasFiltradas)] := vEmpresas[i];
          Continue;
        end;
      end;
    end;
  end;

  if empresasFiltradas = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(eValorPesquisa);
    Exit;
  end;

  FDados := TArray<TObject>(empresasFiltradas);

  for i := Low(empresasFiltradas) to High(empresasFiltradas) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]   := charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]        := NFormat(empresasFiltradas[i].EmpresaId);
    sgPesquisa.Cells[cp_razao_social, i + 1]  := empresasFiltradas[i].RazaoSocial;
    sgPesquisa.Cells[cp_nome_fantasia, i + 1] := empresasFiltradas[i].NomeFantasia;
    sgPesquisa.Cells[cp_cnpj, i + 1]          := empresasFiltradas[i].Cnpj;
    sgPesquisa.Cells[cp_ativo, i + 1]         := empresasFiltradas[i].Ativo;
  end;

  sgPesquisa.RowCount := IfThen(Length(empresasFiltradas) = 1, 2, High(empresasFiltradas) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaEmpresas.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, cp_ativo]) then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
