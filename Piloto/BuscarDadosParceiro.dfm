inherited FormBuscarDadosParceiros: TFormBuscarDadosParceiros
  Caption = 'Buscar dados do parceiro'
  ClientHeight = 113
  ClientWidth = 406
  ExplicitWidth = 412
  ExplicitHeight = 142
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 76
    Width = 406
  end
  inline FrProfissional: TFrProfissionais
    Left = 1
    Top = 2
    Width = 403
    Height = 63
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 1
    ExplicitTop = 2
    ExplicitWidth = 403
    ExplicitHeight = 63
    inherited sgPesquisa: TGridLuka
      Width = 378
      Height = 46
      Align = alNone
      ExplicitWidth = 378
      ExplicitHeight = 46
    end
    inherited CkAspas: TCheckBox
      Top = 20
      ExplicitTop = 20
    end
    inherited PnTitulos: TPanel
      Width = 403
      ExplicitWidth = 403
      inherited lbNomePesquisa: TLabel
        Width = 97
        Caption = 'Parceiro da venda'
        ExplicitWidth = 97
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 298
        ExplicitLeft = 298
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 378
      Height = 47
      ExplicitLeft = 378
    end
    inherited poOpcoes: TPopupMenu
      Left = 120
      Top = 16
    end
  end
end
