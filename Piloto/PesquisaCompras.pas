unit PesquisaCompras;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisasAvancadas, Vcl.StdCtrls, _Compras,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _HerancaPesquisas, _Biblioteca, _Sessao,
  FrameDataInicialFinal, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, _ComprasItens,
  FrameFornecedores, StaticTextLuka, Vcl.Buttons, Vcl.ExtCtrls;

type
  RecDadosCompras = record
    Compra: RecCompras;
    Itens: TArray<RecComprasItens>;
  end;

  TFormPesquisaCompras = class(TFormHerancaPesquisasAvancadas)
    FrFornecedor: TFrFornecedores;
    FrDatasCadastro: TFrDataInicialFinal;
    FrDatasFaturamento: TFrDataInicialFinal;
    StaticTextLuka1: TStaticTextLuka;
    sgItens: TGridLuka;
    pnOpcoes: TPanel;
    sbFinalizar: TSpeedButton;
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaClick(Sender: TObject);
    procedure sgPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbFinalizarClick(Sender: TObject);
    procedure sgPesquisaGetCellPicture(Sender: TObject; ARow, ACol: Integer;
      var APicture: TPicture);
  private
    FDados: TArray<RecCompras>;
    FItens: TArray<RecComprasItens>;

    function getItens(pCompraId: Integer): TArray<RecComprasItens>;
  protected
    procedure BuscarRegistros; override;
    procedure VerificarRegistro; override;
  end;

function Pesquisar: TRetornoTelaFinalizar<RecDadosCompras>; overload;
function Pesquisar( pEmpresaId: Integer; pProdutoId: Integer; pFornecedorId: Integer ): TRetornoTelaFinalizar<RecDadosCompras>; overload;

implementation

uses
  _Imagens;

{$R *.dfm}

{ TFormPesquisaCompras }

const
  coSelecionado     = 0;
  coCompraId        = 1;
  coFornecedor      = 2;
  coDataCadastro    = 3;
  coPrevisaoEntrega = 4;
  coComprador       = 5;
  coEmpresa         = 6;

  (* Grid de itens *)
  ciProdutoId  = 0;
  ciNome       = 1;
  ciMarca      = 2;
  ciQuantidade = 3;
  ciUnidade    = 4;
  ciEntregues  = 5;
  ciBaixados   = 6;
  ciCancelados = 7;
  ciSaldo      = 8;

function Pesquisar: TRetornoTelaFinalizar<RecDadosCompras>;
begin
  Result := Pesquisar(-1, -1, -1);
end;

function Pesquisar( pEmpresaId: Integer; pProdutoId: Integer; pFornecedorId: Integer ): TRetornoTelaFinalizar<RecDadosCompras>; overload;
var
  vForm: TFormPesquisaCompras;
begin
  vForm := TFormPesquisaCompras.Create(nil);

  if pFornecedorId > 0 then begin
    vForm.FrFornecedor.InserirDadoPorChave(pFornecedorId);
    _Biblioteca.Habilitar([vForm.FrFornecedor], False, False);
  end;

  vForm.addFiltro('COM.EMPRESA_ID = ', pEmpresaId);
  vForm.addFiltro('COM.COMPRA_ID in(select distinct COMPRA_ID from COMPRAS_ITENS where SALDO > 0 and PRODUTO_ID = ', pProdutoId, True);

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.Compra := vForm.FDados[vForm.sgPesquisa.Row - 1];
    Result.Dados.Itens  := vForm.getItens(Result.Dados.Compra.CompraId);
  end;
end;

procedure TFormPesquisaCompras.BuscarRegistros;
var
  i: Integer;
  vSql: string;
  vComprasIds: TArray<Integer>;
begin
  inherited;

  FItens := nil;
  vComprasIds := nil;
  sgItens.ClearGrid();
  sgPesquisa.ClearGrid();

  vSql := '';
  if not FrFornecedor.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrFornecedor.getSqlFiltros('COM.FORNECEDOR_ID'));

  if not FrDatasCadastro.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vSql, FrDatasCadastro.getSqlFiltros('trunc(COM.DATA_HORA_COMPRA)'));

  if not FrDatasFaturamento.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vSql, FrDatasCadastro.getSqlFiltros('COM.DATA_FATURAMENTO'));

  _Biblioteca.WhereOuAnd(vSql, getSqlAvancado);

  vSql := vSql + ' order by COM.COMPRA_ID';
  FDados := _Compras.BuscarComprasComando(Sessao.getConexaoBanco, vSql);

  if FDados = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(FDados) to High(FDados) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]     := charNaoSelecionado;
    sgPesquisa.Cells[coCompraId, i + 1]        := NFormat(FDados[i].CompraId);
    sgPesquisa.Cells[coFornecedor, i + 1]      := NFormat(FDados[i].FornecedorId) + ' - ' + FDados[i].NomeFornecedor;
    sgPesquisa.Cells[coDataCadastro, i + 1]    := DFormat(FDados[i].DataHoraCompra);
    sgPesquisa.Cells[coPrevisaoEntrega, i + 1] := DFormat(FDados[i].PrevisaoEntrega);
    sgPesquisa.Cells[coComprador, i + 1]       := NFormat(FDados[i].CompraId) + ' - ' + FDados[i].NomeComprador;
    sgPesquisa.Cells[coEmpresa, i + 1]         := NFormat(FDados[i].EmpresaId) + ' - ' + FDados[i].NomeEmpresa;

    _Biblioteca.AddNoVetorSemRepetir(vComprasIds, FDados[i].CompraId);
  end;
  sgPesquisa.SetLinhasGridPorTamanhoVetor( Length(FDados) );

  FItens := _ComprasItens.BuscarComprasItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.COMPRA_ID', vComprasIds));
  sgPesquisaClick(nil);
end;

procedure TFormPesquisaCompras.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrFornecedor);
end;

function TFormPesquisaCompras.getItens(pCompraId: Integer): TArray<RecComprasItens>;
var
  i: Integer;
  vCompraId: Integer;
begin
  Result := nil;
  vCompraId := SFormatInt( sgPesquisa.Cells[coCompraId, sgPesquisa.Row] );
  for i := Low(FItens) to High(FItens) do begin
    if vCompraId <> FItens[i].CompraId then
      Continue;

    SetLength(Result, Length(Result) + 1);
    Result[High(Result)] := FItens[i];
  end;
end;

procedure TFormPesquisaCompras.sbFinalizarClick(Sender: TObject);
begin
  VerificarRegistro;
  if fsModal in FormState then
    ModalResult := mrOk
  else
    Self.Close;
end;

procedure TFormPesquisaCompras.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[ciProdutoId, ciQuantidade, ciEntregues, ciBaixados, ciCancelados, ciSaldo ] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaCompras.sgPesquisaClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vCompraId: Integer;
begin
  inherited;
  vLinha := 0;
  sgItens.ClearGrid();
  vCompraId := SFormatInt( sgPesquisa.Cells[coCompraId, sgPesquisa.Row] );
  for i := Low(FItens) to High(FItens) do begin
    if vCompraId <> FItens[i].CompraId then
      Continue;

    Inc(vLinha);

    sgItens.Cells[ciProdutoId, vLinha]  := NFormat( FItens[i].ProdutoId );
    sgItens.Cells[ciNome, vLinha]       := FItens[i].NomeProduto;
    sgItens.Cells[ciMarca, vLinha]      := NFormat( FItens[i].MarcaId ) + ' - ' + FItens[i].NomeMarca;
    sgItens.Cells[ciQuantidade, vLinha] := NFormat( FItens[i].Quantidade, 3 );
    sgItens.Cells[ciUnidade, vLinha]    := FItens[i].UnidadeCompraId;
    sgItens.Cells[ciEntregues, vLinha]  := NFormatN( FItens[i].Entregues, 3 );
    sgItens.Cells[ciBaixados, vLinha]   := NFormatN( FItens[i].Baixados, 3 );
    sgItens.Cells[ciCancelados, vLinha] := NFormatN( FItens[i].Cancelados, 3 );
    sgItens.Cells[ciSaldo, vLinha]      := NFormatN( FItens[i].Saldo, 3 );
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormPesquisaCompras.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol = coCompraId then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaCompras.sgPesquisaGetCellPicture(Sender: TObject; ARow,
  ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol <> coSelecionado) or (sgPesquisa.Cells[ACol, ARow] = '') then
   Exit;

  if sgPesquisa.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else if sgPesquisa.Cells[ACol, ARow] = charSelecionado then
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFormPesquisaCompras.sgPesquisaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  MarcarDesmarcarTodos(sgPesquisa, coSelecionado, Key ,Shift);
end;

procedure TFormPesquisaCompras.VerificarRegistro;
var
  i: Integer;
  vTemSelecionado: Boolean;
begin
  inherited;
  vTemSelecionado := false;
  for i := sgPesquisa.FixedRows to sgPesquisa.RowCount - 1 do begin
    if sgPesquisa.Cells[coSelecionado, i] = charSelecionado then begin
      vTemSelecionado := true;
      Exit;
    end;
  end;

  if not vTemSelecionado then begin
    Informar('� necess�rio selecionar pelo menos um pedido.');
    Abort;
  end;
end;

end.
