unit FrameGruposTributacoesFederalVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Grids, GridLuka, _Sessao, _Biblioteca, _GruposTribFederalVenda, PesquisaGruposTributacoesFederalVenda,
  Vcl.Menus;

type
  TFrGruposTributacoesFederalVenda = class(TFrameHenrancaPesquisas)
  public
    function getGrupo(pLinha: Integer = -1): RecGruposTribFederalVenda;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrGruposTributacoesFederalVenda }

function TFrGruposTributacoesFederalVenda.AdicionarDireto: TObject;
var
  vDados: TArray<RecGruposTribFederalVenda>;
begin
  vDados := _GruposTribFederalVenda.BuscarGruposTribFederalVenda(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrGruposTributacoesFederalVenda.AdicionarPesquisando: TObject;
begin
  Result := PesquisaGruposTributacoesFederalVenda.Pesquisar;
end;

function TFrGruposTributacoesFederalVenda.AdicionarPesquisandoTodos: TArray<TObject>;
begin

end;

function TFrGruposTributacoesFederalVenda.getGrupo(pLinha: Integer): RecGruposTribFederalVenda;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecGruposTribFederalVenda(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrGruposTributacoesFederalVenda.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecGruposTribFederalVenda(FDados[i]).GrupoTribFederalVendaId = RecGruposTribFederalVenda(pSender).GrupoTribFederalVendaId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrGruposTributacoesFederalVenda.MontarGrid;
var
  i: Integer;
  vSender: RecGruposTribFederalVenda;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecGruposTribFederalVenda(FDados[i]);
      AAdd([IntToStr(vSender.GrupoTribFederalVendaId), vSender.Descricao]);
    end;
  end;
end;

end.
