unit FrameCFOPs;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, _Cfop, PesquisaCFOP, System.Math, _Biblioteca,
  ComboBoxLuka, Vcl.Buttons, Vcl.Menus;

type
  TFrCFOPs = class(TFrameHenrancaPesquisas)
    cbTipoCFOPs: TComboBoxLuka;
    ckEntradaMercadorias: TCheckBox;
    ckEntradaConhecimentoFrete: TCheckBox;
    ckEntradaServicos: TCheckBox;
  private
    FInicioCFOP: string;
  public
    constructor Create(AOwner: TComponent); override;
    function GetCFOP(pLinha: Integer = -1): RecCFOPs;
    procedure SetInicioCFOPPesquisa(pInicio: string);
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrCFOPs }

function TFrCFOPs.AdicionarDireto: TObject;
var
  vCfopPesquisaId: string;
  vCFOPs: TArray<RecCFOPs>;
begin
  vCfopPesquisaId := FChaveDigitada;
  if Length(_Biblioteca.RetornaNumeros(FChaveDigitada)) = 4 then
    vCfopPesquisaId := _Biblioteca.RetornaNumeros(FChaveDigitada) + '-0';

  vCFOPs :=
    _Cfop.BuscarCFOPs(
      Sessao.getConexaoBanco,
      0,
      [vCfopPesquisaId],
      TTipoCFOP(cbTipoCFOPs.ItemIndex),
      ckSomenteAtivos.Checked,
      FInicioCFOP,
      ckEntradaMercadorias.Checked,
      ckEntradaServicos.Checked,
      ckEntradaConhecimentoFrete.Checked
    );

  if vCFOPs = nil then
    Result := nil
  else
    Result := vCFOPs[0];
end;

function TFrCFOPs.AdicionarPesquisando: TObject;
begin
  Result :=
    PesquisaCFOP.PesquisarCFOP(
      TTipoCFOP(cbTipoCFOPs.ItemIndex),
      ckSomenteAtivos.Checked,
      FInicioCFOP,
      ckEntradaMercadorias.Checked,
      ckEntradaServicos.Checked,
      ckEntradaConhecimentoFrete.Checked
    );
end;

constructor TFrCFOPs.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FInicioCFOP := '';
end;

function TFrCFOPs.GetCFOP(pLinha: Integer): RecCFOPs;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecCFOPs(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrCFOPs.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecCFOPs(FDados[i]).CfopId = RecCFOPs(pSender).CfopId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrCFOPs.MontarGrid;
var
  i: Integer;
  pSender: RecCFOPs;
begin
  inherited;
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecCFOPs(FDados[i]);
      AAdd([pSender.CfopPesquisaId, pSender.nome]);
    end;
  end;
end;

procedure TFrCFOPs.SetInicioCFOPPesquisa(pInicio: string);
begin
  FInicioCFOP := pInicio;
end;

end.
