inherited FormCadastroTransportadoras: TFormCadastroTransportadoras
  Caption = 'Cadastro de transportadoras'
  ClientHeight = 360
  ExplicitHeight = 389
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label2: TLabel
    Left = 637
    Top = 5
    ExplicitLeft = 637
    ExplicitTop = 5
  end
  inherited pnOpcoes: TPanel
    Height = 360
  end
  inherited eRazaoSocial: TEditLuka
    Width = 306
    ExplicitWidth = 306
  end
  inherited rgTipoPessoa: TRadioGroupLuka
    Left = 677
    Top = 47
    ExplicitLeft = 677
    ExplicitTop = 47
  end
  inherited pcDados: TPageControl
    Left = 125
    Top = 86
    Height = 274
    ExplicitLeft = 125
    ExplicitTop = 86
    ExplicitHeight = 274
    inherited tsPrincipais: TTabSheet
      ExplicitHeight = 283
      inherited lb9: TLabel
        Left = 372
        Top = 45
        ExplicitLeft = 372
        ExplicitTop = 45
      end
      inherited lb11: TLabel
        Left = 3
        Width = 100
        Caption = 'Telefone principal'
        ExplicitLeft = 3
        ExplicitWidth = 100
      end
      inherited lb12: TLabel
        Left = 254
        ExplicitLeft = 254
      end
      inherited lb13: TLabel
        Left = 347
        Top = 173
        Visible = False
        ExplicitLeft = 347
        ExplicitTop = 173
      end
      object lbl1: TLabel [8]
        Left = 129
        Top = 45
        Width = 112
        Height = 14
        Caption = 'Telefone secund'#225'rio'
      end
      inherited eEmail: TEditLuka
        Left = 372
        Top = 60
        ExplicitLeft = 372
        ExplicitTop = 60
      end
      inherited eTelefone: TEditTelefoneLuka
        Left = 3
        Top = 60
        ExplicitLeft = 3
        ExplicitTop = 60
      end
      inherited eCelular: TEditTelefoneLuka
        Left = 254
        Top = 60
        Width = 112
        ExplicitLeft = 254
        ExplicitTop = 60
        ExplicitWidth = 112
      end
      inherited eFax: TEditTelefoneLuka
        Left = 347
        Top = 187
        Visible = False
        ExplicitLeft = 347
        ExplicitTop = 187
      end
      object edt1: TEditTelefoneLuka
        Left = 129
        Top = 60
        Width = 119
        Height = 22
        EditMask = '(99) 9999-9999;1; '
        MaxLength = 14
        TabOrder = 10
        Text = '(  )     -    '
        OnKeyDown = ProximoCampo
      end
    end
    inherited tsContatos: TTabSheet
      TabVisible = False
      inherited FrTelefones: TFrCadastrosTelefones
        Height = 245
        inherited sgTelefones: TGridLuka
          Top = -9
          ExplicitLeft = -4
          ExplicitTop = 40
        end
      end
    end
    inherited tsEnderecos: TTabSheet
      TabVisible = False
      inherited FrDiversosEnderecos: TFrDiversosEnderecos
        Height = 245
        inherited sgOutrosEnderecos: TGridLuka
          Top = 38
          ExplicitLeft = -4
          ExplicitTop = 85
        end
        inherited FrBairro: TFrBairros
          inherited PnTitulos: TPanel
            inherited pnSuprimir: TPanel
              inherited ckSuprimir: TCheckBox
                Visible = False
              end
            end
          end
        end
      end
    end
    inherited tsObservacoes: TTabSheet
      inherited eObservacoes: TMemo
        Height = 245
      end
    end
  end
  inherited eDataCadastro: TEditLukaData
    Left = 637
    Top = 19
    ExplicitLeft = 637
    ExplicitTop = 19
  end
end
