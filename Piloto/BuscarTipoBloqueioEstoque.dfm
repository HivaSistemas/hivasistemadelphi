inherited FormBuscarTipoBloqueioEstoque: TFormBuscarTipoBloqueioEstoque
  Caption = 'Buscar tipos de bloqueios de estoque'
  ClientHeight = 133
  ClientWidth = 357
  ExplicitWidth = 363
  ExplicitHeight = 162
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 96
    Width = 357
    inherited sbFinalizar: TSpeedButton
      Left = 121
      ExplicitLeft = 121
    end
  end
  inline FrTipoBloqueioEstoque: TFrTipoBloqueioEstoque
    Left = 7
    Top = 5
    Width = 345
    Height = 81
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 7
    ExplicitTop = 5
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 94
        ExplicitWidth = 94
        ExplicitHeight = 14
      end
    end
  end
end
