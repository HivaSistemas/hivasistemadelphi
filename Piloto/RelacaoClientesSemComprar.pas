unit RelacaoClientesSemComprar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls, _Sessao,
  RadioGroupLuka, EditLuka, GroupBoxLuka, _FrameHerancaPrincipal, _Clientes,
  _FrameHenrancaPesquisas, FrameEmpresas, Vcl.Grids, GridLuka, _Biblioteca,
  FrameVendedores, ComboBoxLuka;

type
  TFormRelacaoClienteSemComprar = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    gbStatus: TGroupBoxLuka;
    Label1: TLabel;
    eQtdeDias: TEditLuka;
    ckClientesNuncaCompraram: TCheckBoxLuka;
    rgSituacao: TRadioGroupLuka;
    sgClientes: TGridLuka;
    FrVendedores: TFrVendedores;
    Label2: TLabel;
    cbAgrupamento: TComboBoxLuka;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    procedure sgClientesDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure rgSituacaoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sbImprimirClick(Sender: TObject);
    procedure sgClientesGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure Carregar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

const
  coCliente        = 0;
  coNomeFantasia   = 1;
  coDataUltCompra  = 2;
  coQtdDias        = 3;
  coValorUltCompra = 4;
  coVendedorPadrao = 5;
  coTelefonePrincipal = 6;
  coTelefoneCelular = 7;
  coEmail          = 8;
  coAtivo          = 9;

  //OCULTAS
  tiTipoLinha       = 10;

  coLinhaDetalhe   = 'D';
  coLinhaCabAgrup  = 'CAGRU';

implementation

{$R *.dfm}

{ TFormHerancaRelatoriosPageControl1 }

procedure TFormRelacaoClienteSemComprar.Carregar(Sender: TObject);
var
  empresaId: Integer;
  qtdDias: Integer;
  clientes: TArray<RecClientesSemComprar>;
  i: Integer;
  vLinha: Integer;
  vFiltroVendedores: string;
  vOrderBySQL: string;
  vAgrupadorId: Integer;
begin
  inherited;
  sgClientes.ClearGrid();

  vFiltroVendedores := '';
  if not FrVendedores.EstaVazio then
    vFiltroVendedores := ' and ' + FrVendedores.getSqlFiltros('CLI.VENDEDOR_ID');

  if cbAgrupamento.GetValor = 'VEN' then
    vOrderBySQL := ' order by fun.nome ASC, orc.data_cadastro DESC';

  if ckClientesNuncaCompraram.Checked then begin
    clientes := BuscarClientesNuncaCompraram(
      Sessao.getConexaoBanco,
      FrEmpresas.getEmpresa().EmpresaId,
      rgSituacao.GetValor,
      vFiltroVendedores,
      vOrderBySQL
    );
  end
  else begin
    clientes := _Clientes.BuscarClientesSemComprar(
      Sessao.getConexaoBanco,
      eQtdeDias.AsInt,
      FrEmpresas.getEmpresa().EmpresaId,
      rgSituacao.GetValor,
      vFiltroVendedores,
      vOrderBySQL
    );
  end;

  if clientes = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vLinha := 0;
  vAgrupadorId := -1;
  for i := Low(clientes) to High(clientes) do begin
    if cbAgrupamento.GetValor = 'VEN' then begin
      if vAgrupadorId <> clientes[i].vendedor_id then begin
        Inc(vLinha);

        sgClientes.Cells[tiTipoLinha, vLinha] := coLinhaCabAgrup;
        sgClientes.Cells[coNomeFantasia, vLinha] := 'VENDEDOR: ' + getInformacao(
          clientes[i].vendedor_id, IIFstr(clientes[i].vendedor_id > 0, clientes[i].vendedor_nome, 'SEM VENDEDOR')
        );

        vAgrupadorId := clientes[i].vendedor_id;
      end;
    end;

    Inc(vLinha);

    sgClientes.Cells[tiTipoLinha, vLinha]         := coLinhaDetalhe;
    sgClientes.Cells[coCliente, vLinha]           := _Biblioteca.NFormat(clientes[i].cadastro_id);
    sgClientes.Cells[coNomeFantasia, vLinha]      := clientes[i].nome_fantasia;
    sgClientes.Cells[coEmail, vLinha]             := clientes[i].email;

    if clientes[i].telefone_principal <> '(  )     -    ' then
      sgClientes.Cells[coTelefonePrincipal, vLinha] := clientes[i].telefone_principal;

    if clientes[i].telefone_celular <> '(  )     -    ' then
      sgClientes.Cells[coTelefoneCelular, vLinha]   := clientes[i].telefone_celular;

    sgClientes.Cells[coAtivo, vLinha]             := IIfStr(clientes[i].ativo = 'S', 'SIM', 'N�O');
    if clientes[i].vendedor_id > 0 then
      sgClientes.Cells[coVendedorPadrao, vLinha] := _Biblioteca.NFormat(clientes[i].vendedor_id) + ' - ' + clientes[i].vendedor_nome;

    if not ckClientesNuncaCompraram.Checked then begin
      sgClientes.Cells[coDataUltCompra, vLinha]  := _Biblioteca.DFormat(clientes[i].data_ultima_compra);
      sgClientes.Cells[coQtdDias, vLinha]        := _Biblioteca.NFormat(clientes[i].qtd_dias);
      sgClientes.Cells[coValorUltCompra, vLinha] := _Biblioteca.NFormat(clientes[i].valor_ultima_compra);
    end;
  end;

  sgClientes.SetLinhasGridPorTamanhoVetor(vLinha);
  SetarFoco(sgClientes);
end;

procedure TFormRelacaoClienteSemComprar.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId);
end;

procedure TFormRelacaoClienteSemComprar.rgSituacaoClick(Sender: TObject);
begin
  inherited;
  eQtdeDias.Enabled := not ckClientesNuncaCompraram.Checked;
end;

procedure TFormRelacaoClienteSemComprar.sbImprimirClick(Sender: TObject);
begin
  inherited;
  GridToExcel(sgClientes);
end;

procedure TFormRelacaoClienteSemComprar.sgClientesDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coNomeFantasia, coVendedorPadrao, coEmail, coTelefonePrincipal, coTelefoneCelular] then
    vAlinhamento := taLeftJustify
  else if ACol in[coAtivo, coDataUltCompra] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taRightJustify;

  sgClientes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoClienteSemComprar.sgClientesGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;

  if ARow = 0 then
    Exit;

  if sgClientes.Cells[tiTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
end;

procedure TFormRelacaoClienteSemComprar.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrEmpresas.EstaVazio then begin
    _Biblioteca.Exclamar('� neces�rio informar uma empresa!');
    SetarFoco(FrEmpresas);
    Abort;
  end;

  if eQtdeDias.AsInt = 0 then begin
    _Biblioteca.Exclamar('� neces�rio informar a quantidade de dias!');
    SetarFoco(eQtdeDias);
    Abort;
  end;
end;

end.
