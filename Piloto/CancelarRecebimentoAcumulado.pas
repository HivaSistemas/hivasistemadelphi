unit CancelarRecebimentoAcumulado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Acumulados, _Sessao,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Mask, EditLukaData, _Biblioteca, InformacoesAcumulado,
  _RecordsEspeciais, PesquisaAcumulados, SpeedButtonLuka;

type
  TFormCancelarRecebimentoAcumulado = class(TFormHerancaCadastroCodigo)
    lb12: TLabel;
    lb3: TLabel;
    lb5: TLabel;
    eUsuarioRecebimento: TEditLuka;
    eDataHoraRecebimento: TEditLukaData;
    eTotalAcumulado: TEditLuka;
    eCondicaoPagamento: TEditLuka;
    lb18: TLabel;
    eCliente: TEditLuka;
    lb2: TLabel;
    sbInformacoesAcumulado: TSpeedButtonLuka;
    procedure sbInformacoesAcumuladoClick(Sender: TObject);
  private
    procedure PreencherRegistro(pAcumulado: RecAcumulados);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCancelarRecebimentoAcumulado }

procedure TFormCancelarRecebimentoAcumulado.BuscarRegistro;
var
  pAcumulado: TArray<RecAcumulados>;
begin
  pAcumulado := _Acumulados.BuscarAcumulados(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if pAcumulado = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end
  else if pAcumulado[0].status <> 'RE' then begin
    _Biblioteca.Exclamar('Somente acumulados j� recebidos podem ser cancelados, verifique!');
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(pAcumulado[0]);
end;

procedure TFormCancelarRecebimentoAcumulado.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormCancelarRecebimentoAcumulado.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;
  vRetorno := _Acumulados.CancelarRecebimentoAcumulado(Sessao.getConexaoBanco, eId.AsInt);
  Sessao.AbortarSeHouveErro(vRetorno);

  RotinaSucesso;
  SetarFoco(eId);
end;

procedure TFormCancelarRecebimentoAcumulado.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eTotalAcumulado, sbInformacoesAcumulado, eCliente, eUsuarioRecebimento, eCondicaoPagamento, eDataHoraRecebimento], pEditando);

  if pEditando then
    SetarFoco(eTotalAcumulado);
end;

procedure TFormCancelarRecebimentoAcumulado.PesquisarRegistro;
var
  vRetTela: TRetornoTelaFinalizar<RecAcumulados>;
begin
  vRetTela := PesquisaAcumulados.Pesquisar;
  if vRetTela.BuscaCancelada then
    Exit;

  inherited;
  PreencherRegistro(vRetTela.Dados);
end;

procedure TFormCancelarRecebimentoAcumulado.PreencherRegistro(pAcumulado: RecAcumulados);
begin
  eID.SetInformacao(pAcumulado.AcumuladoId);
  eCliente.SetInformacao(pAcumulado.ClienteId, pAcumulado.NomeCliente);
  eUsuarioRecebimento.SetInformacao(pAcumulado.UsuarioRecebimentoId, pAcumulado.NomeUsuarioRecebimento);
  eCondicaoPagamento.SetInformacao(pAcumulado.CondicaoId, pAcumulado.NomeCondicaoPagamento);

  eDataHoraRecebimento.AsDataHora := pAcumulado.DataHoraRecebimento;
  eTotalAcumulado.AsDouble        := pAcumulado.ValorTotal;
end;

procedure TFormCancelarRecebimentoAcumulado.sbInformacoesAcumuladoClick(Sender: TObject);
begin
  inherited;
  InformacoesAcumulado.Informar(eID.IdInformacao);
end;

procedure TFormCancelarRecebimentoAcumulado.VerificarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco := _Acumulados.PodeCancelarRecebimentoAcumulado( Sessao.getConexaoBanco, eID.AsInt );
  Sessao.AbortarSeHouveErro(vRetBanco);
end;

end.
