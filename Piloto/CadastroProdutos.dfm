inherited FormCadastroProdutos: TFormCadastroProdutos
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Cadastro de produtos'
  ClientHeight = 500
  ClientWidth = 1015
  OnShow = FormShow
  ExplicitWidth = 1021
  ExplicitHeight = 529
  PixelsPerInch = 96
  TextHeight = 14
  object lbl1: TLabel [0]
    Left = 223
    Top = 1
    Width = 85
    Height = 14
    Caption = 'Nome de venda'
  end
  inherited Label1: TLabel
    Left = 125
    Top = 1
    ExplicitLeft = 125
    ExplicitTop = 1
  end
  inherited pnOpcoes: TPanel
    Height = 500
    ExplicitHeight = 500
    inherited sbGravar: TSpeedButton
      Top = 3
      ExplicitTop = 3
    end
    inherited sbDesfazer: TSpeedButton
      Top = 100
      ExplicitTop = 100
    end
    inherited sbExcluir: TSpeedButton
      Top = 168
      ExplicitTop = 168
    end
    inherited sbPesquisar: TSpeedButton
      Top = 51
      ExplicitTop = 51
    end
    inherited sbLogs: TSpeedButton
      Top = 448
      Width = 117
      Visible = True
      ExplicitTop = 448
      ExplicitWidth = 117
    end
    object sbCopiarProduto: TSpeedButton
      Left = 0
      Top = 261
      Width = 120
      Height = 48
      Hint = 'Copiar produto'
      Caption = 'Copiar prod.'
      Flat = True
      Glyph.Data = {
        36180000424D3618000000000000360000002800000040000000200000000100
        18000000000000180000600F0000600F00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC9C9C972
        7272767676D1D1D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEE0E0E0C1C1C1CD
        CDCDF5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4D4D46C6C6C60
        6060606060747474FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4E4E4BDBDBDBEBEBEBC
        BCBCCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E2E274747460606060
        6060606060606060FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAEAEABFBFBFBEBEBEC0C0C0BF
        BFBFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E980808060606060606060
        6060606060838383FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEFC3C3C3BCBCBCC0C0C0BFBFBFBC
        BCBCD7D7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFEFEFEFEFEFEFFFFFFFFFFFFFFFFFFF4F4F491919160606060606060606060
        6060838383E5E5E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFE
        FEFFFFFFFFFFFFFFFFFFFFFFFFF4F4F4C9C9C9BCBCBCBFBFBFBEBEBEBCBCBCD6
        D6D6FBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9D6D6D6B2B2
        B2989898969696ADADADD3D3D3F1F1F1A4A4A46060606060606060606060608C
        8C8CEAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFBFBECECECDBDBDBD3D3
        D3D6D6D6E0E0E0F2F2F2F7F7F7CFCFCFBBBBBBBFBFBFBDBDBDBCBCBCDADADAFC
        FCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E79999996060606060
        606060606060606060606060607F7F7F646464606060606060606060979797F1
        F1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEECECECEBBBBBBB7B7B7B8B8
        B8B8B8B8B7B7B7C1C1C1C9C9C9BEBEBEBFBFBFBDBDBDBEBEBEDFDFDFFEFEFEFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5E5E57B7B7B6060606565659393
        93B0B0B0B2B2B29999996B6B6B606060606060606060606060A1A1A1F7F7F7FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFECECECC2C2C2B6B6B6C4C4C4D8D8D8E1E1
        E1E0E0E0D3D3D3BDBDBDB9B9B9C0C0C0BEBEBEBFBFBFE4E4E4FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3F3F38D8D8D6060607B7B7BD1D1D1F9F9
        F9FFFFFFFFFFFFFEFEFEDDDDDD898989606060606060A5A5A5FBFBFBFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFF4F4F4C6C6C6B7B7B7D1D1D1F3F3F3FFFFFFFFFF
        FFFFFFFFFCFCFCEBEBEBC9C9C9BCBCBCC1C1C1E5E5E5FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC5C5C5606060717171DADADAFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFEBEBEB7D7D7D606060B5B5B5FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFDBDBDBB7B7B7CCCCCCF9F9F9FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFEEEEEEC1C1C1BDBDBDEEEEEEFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF919191606060ABABABFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC4C4C4606060818181F0F0F0FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFC8C8C8BABABAECECECFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFDBDBDBB8B8B8D6D6D6FEFEFEFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C6C6C686868D3D3D3FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E7707070646464DFDFDFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFBBBBBBC8C8C8F8F8F8FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFEAEAEABEBEBEC8C8C8FBFBFBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF606060747474E2E2E2FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF838383606060D7D7D7FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFB6B6B6D0D0D0FBFBFBFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFF1F1F1C3C3C3C3C3C3FAFAFAFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6666666D6D6DD9D9D9FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAEAEA777777606060DBDBDBFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFB9B9B9CBCBCBF9F9F9FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFEDEDEDC1C1C1C6C6C6FBFBFBFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF848484616161BABABAFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD3D3D3616161757575E9E9E9FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFC4C4C4BEBEBEF1F1F1FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFE0E0E0B9B9B9D0D0D0FEFEFEFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB4B4B46060607F7F7FEAEAEAFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFF7F7F79090906060609D9D9DFAFAFAFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFD4D4D4B6B6B6D6D6D6FDFDFDFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFF5F5F5C8C8C8B7B7B7E3E3E3FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E9E9747474606060969696EAEAEAFFFF
        FFFFFFFFFFFFFFFFFFFFF1F1F1A6A6A6606060676767D7D7D7FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFEEEEEEBFBFBFBABABADEDEDEFBFBFBFFFFFFFFFF
        FFFFFFFFFFFFFFF6F6F6D2D2D2B6B6B6C9C9C9F8F8F8FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC6868686060607D7D7DB4B4
        B4D0D0D0D3D3D3BBBBBB878787606060616161B7B7B7FEFEFEFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFEFEFEE0E0E0BABABABABABAD0D0D0E5E5E5EEEE
        EEECECECE0E0E0C8C8C8B7B7B7C1C1C1EDEDEDFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCACACA7777776060606060
        606767676969696060606060606E6E6EB9B9B9FCFCFCFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDE0E0E0C1C1C1B7B7B7BBBBBBC1C1
        C1C0C0C0BABABAB7B7B7C8C8C8ECECECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7E7E7B2B2B28181
        816767676666667B7B7BAAAAAADFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F1F1DADADAC9C9C9C1C1
        C1C2C2C2CDCDCDE0E0E0F7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      OnClick = sbCopiarProdutoClick
    end
  end
  inherited eID: TEditLuka
    Top = 15
    Width = 93
    TabOrder = 3
    ExplicitTop = 15
    ExplicitWidth = 93
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 836
    Top = 18
    Width = 94
    Alignment = taLeftJustify
    Caption = 'Produto ativo?'
    TabOrder = 4
    ExplicitLeft = 836
    ExplicitTop = 18
    ExplicitWidth = 94
  end
  object eNomeProduto: TEditLuka
    Left = 223
    Top = 15
    Width = 431
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 60
    TabOrder = 2
    OnKeyDown = eNomeProdutoKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object pcDados: TPageControl
    Left = 128
    Top = 43
    Width = 888
    Height = 531
    ActivePage = TabSheet1
    TabOrder = 1
    object tsPrincipal: TTabSheet
      Caption = 'Principais'
      object lb1: TLabel
        Left = 722
        Top = 60
        Width = 64
        Height = 14
        Caption = 'Mult. venda'
      end
      object lb5: TLabel
        Left = 2
        Top = 115
        Width = 131
        Height = 14
        Caption = 'C'#243'digo de barra princial'
      end
      object Label3: TLabel
        Left = 542
        Top = 60
        Width = 145
        Height = 14
        Caption = 'C'#243'd. balan'#231'a venda r'#225'pida'
      end
      object Label2: TLabel
        Left = 722
        Top = 108
        Width = 73
        Height = 14
        Caption = 'Mult. venda 2'
        Visible = False
      end
      object Label8: TLabel
        Left = 722
        Top = 156
        Width = 73
        Height = 14
        Caption = 'Mult. venda 3'
        Visible = False
      end
      object lb17: TLabel
        Left = 694
        Top = 1
        Width = 76
        Height = 14
        Caption = 'Data cadastro'
      end
      object lbl2: TLabel
        Left = 215
        Top = 116
        Width = 113
        Height = 14
        Caption = 'C'#243'd. de ref. ind'#250'stria'
      end
      object Label7: TLabel
        Left = 345
        Top = 115
        Width = 57
        Height = 14
        Caption = 'Frete adic.'
      end
      object lb2: TLabel
        Left = 449
        Top = 115
        Width = 26
        Height = 14
        Caption = 'Peso'
      end
      object lb3: TLabel
        Left = 549
        Top = 115
        Width = 41
        Height = 14
        Caption = 'Volume'
      end
      inline FrFabricante: TFrFornecedores
        Left = 2
        Top = 0
        Width = 313
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitWidth = 313
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 288
          Height = 23
          TabOrder = 1
          ExplicitWidth = 288
          ExplicitHeight = 23
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 313
          TabOrder = 0
          ExplicitWidth = 313
          inherited lbNomePesquisa: TLabel
            Width = 124
            Caption = 'Fabricante/Fornecedor'
            ExplicitWidth = 124
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 208
            ExplicitLeft = 208
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 288
          Height = 24
          ExplicitLeft = 288
          ExplicitHeight = 24
        end
      end
      inline FrUnidadeVenda: TFrUnidades
        Left = 2
        Top = 60
        Width = 186
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 60
        ExplicitWidth = 186
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 161
          Height = 23
          TabOrder = 1
          ExplicitWidth = 161
          ExplicitHeight = 23
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          Left = 106
          TabOrder = 3
          ExplicitLeft = 106
        end
        inherited PnTitulos: TPanel
          Width = 186
          TabOrder = 0
          ExplicitWidth = 186
          inherited lbNomePesquisa: TLabel
            Width = 100
            Caption = 'Unidade de venda'
            ExplicitWidth = 100
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 81
            ExplicitLeft = 81
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 161
          Height = 24
          ExplicitLeft = 161
          ExplicitHeight = 24
        end
      end
      inline FrMarca: TFrMarcas
        Left = 320
        Top = 0
        Width = 313
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 320
        ExplicitWidth = 313
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 288
          Height = 23
          TabOrder = 1
          ExplicitWidth = 288
          ExplicitHeight = 23
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 313
          TabOrder = 0
          ExplicitWidth = 313
          inherited lbNomePesquisa: TLabel
            Width = 33
            Caption = 'Marca'
            ExplicitWidth = 33
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 208
            ExplicitLeft = 208
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 288
          Height = 24
          ExplicitLeft = 288
          ExplicitHeight = 24
        end
      end
      object eMultiploVenda: TEditLuka
        Left = 722
        Top = 76
        Width = 84
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        TabOrder = 6
        Text = '0,000'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 3
        NaoAceitarEspaco = False
      end
      object eCodigoBarras: TEditLuka
        Left = 2
        Top = 131
        Width = 187
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 14
        TabOrder = 3
        OnKeyDown = ProximoCampo
        OnKeyPress = Numeros
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      inline FrLinhaProduto: TFrameDepartamentosSecoesLinhas
        Left = 205
        Top = 60
        Width = 323
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 205
        ExplicitTop = 60
        ExplicitWidth = 323
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 298
          Height = 23
          ExplicitWidth = 298
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 323
          ExplicitWidth = 323
          inherited lbNomePesquisa: TLabel
            Width = 102
            Caption = 'Grupo de produtos'
            ExplicitWidth = 102
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 218
            ExplicitLeft = 218
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 298
          Height = 24
          ExplicitLeft = 298
          ExplicitHeight = 24
        end
        inherited rgNivel: TRadioGroup
          ItemIndex = 3
        end
      end
      object eCodigoBalanca: TEditLuka
        Left = 542
        Top = 76
        Width = 155
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 5
        TabOrder = 4
        OnKeyDown = ProximoCampo
        OnKeyPress = Numeros
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eMultiploVenda2: TEditLuka
        Left = 722
        Top = 124
        Width = 84
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        TabOrder = 7
        Text = '0,000'
        Visible = False
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 3
        NaoAceitarEspaco = False
      end
      object eMultiploVenda3: TEditLuka
        Left = 722
        Top = 172
        Width = 84
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        TabOrder = 8
        Text = '0,000'
        Visible = False
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 3
        NaoAceitarEspaco = False
      end
      object eDataCadastro: TEditLukaData
        Left = 694
        Top = 16
        Width = 98
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        TabStop = False
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 9
        Text = '  /  /    '
      end
      inline FrCodigoBarrasAuxiliar: TFrCodigoBarrasAuxiliar
        Left = 2
        Top = 155
        Width = 187
        Height = 150
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 10
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 155
        ExplicitWidth = 187
        ExplicitHeight = 150
        inherited sgValores: TGridLuka
          Width = 187
          Height = 128
          Align = alNone
          CorLinhaFoco = 38619
          CorColunaFoco = clActiveCaption
          ExplicitWidth = 187
          ExplicitHeight = 128
        end
        inherited StaticTextLuka1: TStaticTextLuka
          Width = 187
          ExplicitWidth = 187
        end
        inherited pmOpcoes: TPopupMenu
          Left = 216
          Top = 208
        end
      end
      object eCodigoOriginalFabricante: TEditLuka
        Left = 215
        Top = 131
        Width = 113
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 20
        TabOrder = 11
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ePeso: TEditLuka
        Left = 449
        Top = 131
        Width = 64
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        TabOrder = 12
        Text = '0,000'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 3
        NaoAceitarEspaco = False
      end
      object eValorAdicionalFrete: TEditLuka
        Left = 345
        Top = 131
        Width = 64
        Height = 22
        Hint = 'Valor adicional frete'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        TabOrder = 13
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eVolume: TEditLuka
        Left = 549
        Top = 131
        Width = 66
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        TabOrder = 14
        Text = '0,000'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 3
        NaoAceitarEspaco = False
      end
    end
    object tsComplementos: TTabSheet
      Caption = 'Complementos'
      ImageIndex = 1
      object stCaracteristicas: TStaticText
        Left = 2
        Top = 146
        Width = 819
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Refer'#234'ncias do produto'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
      end
      object eCaracteristicas: TMemo
        Left = 2
        Top = 159
        Width = 819
        Height = 202
        Enabled = False
        MaxLength = 4000
        ScrollBars = ssVertical
        TabOrder = 1
      end
      inline FrFoto1: TFrImagem
        Left = 2
        Top = 14
        Width = 128
        Height = 126
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 14
        ExplicitHeight = 126
        inherited pnImagem: TPanel
          Height = 126
          ExplicitHeight = 126
          inherited imFoto: TImage
            Height = 122
            ExplicitLeft = 2
            ExplicitTop = 3
            ExplicitWidth = 124
            ExplicitHeight = 124
          end
        end
      end
      object stctxtlk3: TStaticTextLuka
        Left = 2
        Top = -1
        Width = 126
        Height = 15
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Imagen 1'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      inline FrFoto2: TFrImagem
        Left = 198
        Top = 14
        Width = 128
        Height = 126
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 198
        ExplicitTop = 14
        ExplicitHeight = 126
        inherited pnImagem: TPanel
          Height = 126
          ExplicitHeight = 126
          inherited imFoto: TImage
            Height = 122
            ExplicitLeft = -4
            ExplicitTop = 3
            ExplicitWidth = 124
            ExplicitHeight = 124
          end
        end
      end
      object stctxtlk1: TStaticTextLuka
        Left = 198
        Top = -1
        Width = 126
        Height = 15
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Imagen 2'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      inline FrFoto3: TFrImagem
        Left = 396
        Top = 14
        Width = 128
        Height = 126
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 6
        TabStop = True
        ExplicitLeft = 396
        ExplicitTop = 14
        ExplicitHeight = 126
        inherited pnImagem: TPanel
          Height = 126
          ExplicitHeight = 126
          inherited imFoto: TImage
            Height = 122
            ExplicitLeft = 2
            ExplicitTop = 3
            ExplicitWidth = 124
            ExplicitHeight = 120
          end
        end
      end
      object stctxtlk2: TStaticTextLuka
        Left = 396
        Top = -1
        Width = 126
        Height = 15
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Imagen 3'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 7
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
    end
    object tsParametrosCompra: TTabSheet
      Caption = 'Par'#226'metros de compra'
      ImageIndex = 5
      object lb6: TLabel
        Left = 2
        Top = 1
        Width = 92
        Height = 14
        Caption = 'Nome de compra'
      end
      object eNomeCompra: TEditLuka
        Left = 2
        Top = 15
        Width = 431
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 60
        TabOrder = 0
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      inline FrMultiplosCompra: TFrameConversaoUnidadesCompra
        Left = 2
        Top = 41
        Width = 234
        Height = 258
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 41
        ExplicitHeight = 258
        inherited sgValores: TGridLuka
          Height = 241
          CorLinhaFoco = 38619
          CorColunaFoco = clActiveCaption
          ExplicitHeight = 241
        end
        inherited StaticTextLuka1: TStaticTextLuka
          Caption = 'M'#250'ltiplos de compra'
        end
        inherited pmOpcoes: TPopupMenu
          Left = 16
          Top = 208
        end
      end
      inline FrCodigoOriginalFornecedores: TFrCodigoOriginalFornecedores
        Left = 241
        Top = 41
        Width = 480
        Height = 258
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 241
        ExplicitTop = 41
        ExplicitWidth = 480
        inherited sgValores: TGridLuka
          Width = 480
          CorLinhaFoco = 38619
          CorColunaFoco = clActiveCaption
          ExplicitWidth = 480
          ColWidths = (
            314
            160)
        end
        inherited StaticTextLuka1: TStaticTextLuka
          Width = 480
          ExplicitWidth = 480
        end
        inherited pmOpcoes: TPopupMenu
          Top = 208
          inherited miInserir: TMenuItem
            Visible = False
          end
        end
      end
    end
    object tsControleEstoque: TTabSheet
      Caption = 'Controle do estoque'
      ImageIndex = 6
      object Label4: TLabel
        Left = 328
        Top = 174
        Width = 64
        Height = 14
        Caption = 'Quantidade'
      end
      object lbNomePesquisa: TLabel
        Left = 3
        Top = 1
        Width = 24
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Tipo'
      end
      object Label5: TLabel
        Left = 3
        Top = 79
        Width = 100
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Defini'#231#227'o aut. lote'
      end
      object Label6: TLabel
        Left = 3
        Top = 120
        Width = 98
        Height = 14
        Caption = 'Dias aviso vencto.'
      end
      inline FrProdutoKit: TFrProdutos
        Left = 0
        Top = 173
        Width = 320
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitTop = 173
        ExplicitWidth = 320
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 23
          ExplicitWidth = 295
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 42
            Caption = 'Produto'
            ExplicitWidth = 42
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 24
          ExplicitLeft = 295
          ExplicitHeight = 24
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      object eQuantidadeKit: TEditLuka
        Left = 329
        Top = 190
        Width = 82
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 1
        Text = '0,0000'
        OnKeyDown = eQuantidadeKitKeyDown
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 4
        NaoAceitarEspaco = False
      end
      object sgProdutosKit: TGridLuka
        Left = 0
        Top = 214
        Width = 490
        Height = 185
        ColCount = 4
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
        TabOrder = 2
        OnDrawCell = sgProdutosKitDrawCell
        OnKeyDown = sgProdutosKitKeyDown
        OnSelectCell = sgProdutosKitSelectCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Quantidade'
          '% Participa'#231#227'o')
        OnGetCellColor = sgProdutosKitGetCellColor
        OnArrumarGrid = sgProdutosKitArrumarGrid
        Grid3D = False
        RealColCount = 5
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          64
          244
          84
          86)
      end
      object ckExigirDataFabricacaoLote: TCheckBoxLuka
        Left = 3
        Top = 43
        Width = 211
        Height = 17
        Caption = 'Exigir data de fabrica'#231#227'o do lote'
        Checked = True
        State = cbChecked
        TabOrder = 3
        CheckedStr = 'S'
        Valor = 'N'
      end
      object ckExigirDataVencimentoLote: TCheckBoxLuka
        Left = 3
        Top = 60
        Width = 211
        Height = 17
        Caption = 'Exigir data de vencimento do lote'
        Checked = True
        State = cbChecked
        TabOrder = 4
        CheckedStr = 'S'
        Valor = 'C'
      end
      object cbTipoControleEstoque: TComboBoxLuka
        Left = 3
        Top = 16
        Width = 211
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 5
        OnChange = cbTipoControleEstoqueChange
        Items.Strings = (
          'Normal'
          'Lote'
          'Piso'
          'Kit entr. desmembrada'
          'Kit entr. agrupada'
          'Produ'#231#227'o')
        Valores.Strings = (
          'N'
          'L'
          'P'
          'K'
          'A'
          'I')
        AsInt = 0
      end
      inline FrUnidadeEntrega: TFrUnidades
        Left = 220
        Top = -1
        Width = 160
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 6
        TabStop = True
        ExplicitLeft = 220
        ExplicitTop = -1
        ExplicitWidth = 160
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 135
          Height = 23
          TabOrder = 1
          ExplicitWidth = 135
          ExplicitHeight = 23
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 160
          TabOrder = 0
          ExplicitWidth = 160
          inherited lbNomePesquisa: TLabel
            Width = 109
            Caption = 'Unidade de entrega'
            ExplicitWidth = 109
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 55
            ExplicitLeft = 55
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 135
          Height = 24
          ExplicitLeft = 135
          ExplicitHeight = 24
        end
      end
      object cbDefinicaoAutomaticaLote: TComboBoxLuka
        Left = 3
        Top = 93
        Width = 377
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 7
        OnChange = cbTipoControleEstoqueChange
        Items.Strings = (
          'Produtos do mesmo lote que atendam a quantidade'
          'Misturar lotes dando prioridade ao vencimento')
        Valores.Strings = (
          'Q'
          'V')
        AsInt = 0
      end
      object eDiasAvisoVencimento: TEditLuka
        Left = 3
        Top = 134
        Width = 98
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        TabOrder = 8
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      inline frEnderecoEstoque: TFrEnderecoEstoque
        Left = 391
        Top = 8
        Width = 489
        Height = 148
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 9
        TabStop = True
        ExplicitLeft = 391
        ExplicitTop = 8
        ExplicitWidth = 489
        ExplicitHeight = 148
        inherited sgPesquisa: TGridLuka
          Width = 464
          Height = 131
          ExplicitWidth = 464
          ExplicitHeight = 131
        end
        inherited PnTitulos: TPanel
          Width = 489
          ExplicitWidth = 489
          inherited lbNomePesquisa: TLabel
            Width = 121
            ExplicitWidth = 121
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Left = 136
            Checked = False
            State = cbUnchecked
            ExplicitLeft = 136
          end
          inherited pnSuprimir: TPanel
            Left = 384
            ExplicitLeft = 384
          end
        end
        inherited pnPesquisa: TPanel
          Left = 464
          Height = 132
          ExplicitLeft = 464
          ExplicitHeight = 132
        end
      end
    end
    object tsFiscal: TTabSheet
      Caption = 'Fiscal'
      ImageIndex = 5
      object lb4: TLabel
        Left = 0
        Top = 0
        Width = 64
        Height = 14
        Caption = 'C'#243'digo NCM'
      end
      object lb7: TLabel
        Left = 155
        Top = 0
        Width = 24
        Height = 14
        Caption = 'CEST'
      end
      object eNCM: TEditLuka
        Left = 0
        Top = 14
        Width = 151
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 8
        NumbersOnly = True
        TabOrder = 0
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eCEST: TEditLuka
        Left = 155
        Top = 14
        Width = 151
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 7
        NumbersOnly = True
        TabOrder = 1
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      inline FrProdutosGruposTributacoes: TFrProdutosGruposTributacoesEmpresas
        Left = 0
        Top = 39
        Width = 880
        Height = 463
        Align = alBottom
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitTop = 39
        ExplicitWidth = 880
        ExplicitHeight = 463
        inherited sgGrupos: TGridLuka
          Width = 817
          Height = 447
          Align = alNone
          HCol.Strings = (
            'Sel.?'
            'Empresa'
            'Nome'
            'CST de compra'
            'CST de venda'
            'Pis/Cofins de compra'
            'Pis/Cofins de venda')
          ExplicitWidth = 817
          ExplicitHeight = 447
          ColWidths = (
            33
            54
            185
            136
            129
            132
            128)
        end
        inherited st1: TStaticTextLuka
          Width = 880
          Caption = 'Parametros de tributa'#231#245'es de mercadorias'
          ExplicitWidth = 880
        end
        inherited pmOpcoes: TPopupMenu
          Left = 836
          Top = 392
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Parametros'
      ImageIndex = 5
      object lbl3: TLabel
        Left = 3
        Top = 135
        Width = 112
        Height = 14
        Caption = 'Motivo de inativa'#231#227'o'
      end
      object Label11: TLabel
        Left = 3
        Top = 180
        Width = 131
        Height = 14
        Caption = 'C'#243'digo sistema anterior'
      end
      object ckAceitarEstoqueNegativo: TCheckBox
        Left = 2
        Top = 294
        Width = 247
        Height = 17
        Caption = '1 - Aceitar estoque negativo'
        TabOrder = 0
        OnKeyDown = ProximoCampo
      end
      object ckBloquearParaCompras: TCheckBox
        Left = 2
        Top = 311
        Width = 247
        Height = 17
        Caption = '2 - Bloqueado para compras'
        TabOrder = 1
        OnKeyDown = ProximoCampo
      end
      object ckBloquearParaVendas: TCheckBox
        Left = 2
        Top = 328
        Width = 247
        Height = 17
        Caption = '3 - Bloqueado para vendas'
        TabOrder = 2
        OnKeyDown = ProximoCampo
      end
      object ckProdutoDiversoPDV: TCheckBox
        Left = 2
        Top = 361
        Width = 258
        Height = 17
        Caption = '5 - Produto diversos (venda r'#225'pida)'
        TabOrder = 3
        OnKeyDown = ckPermitirDevolucaoKeyDown
      end
      object ckPermitirDevolucao: TCheckBox
        Left = 2
        Top = 345
        Width = 247
        Height = 17
        Caption = '4 - Permitir devolu'#231#227'o'
        TabOrder = 4
        OnKeyDown = ckPermitirDevolucaoKeyDown
      end
      object ckSobEncomenda: TCheckBox
        Left = 2
        Top = 377
        Width = 258
        Height = 17
        Caption = '6 - Sob encomenda'
        TabOrder = 5
        OnKeyDown = ProximoCampo
      end
      object ckBloquearEntradaSemPedCompra: TCheckBox
        Left = 293
        Top = 294
        Width = 258
        Height = 17
        Caption = '7 - Bloquear entrada sem pedido de compra'
        TabOrder = 6
        OnKeyDown = ProximoCampo
      end
      object ckExigirModeloNota: TCheckBox
        Left = 293
        Top = 328
        Width = 258
        Height = 17
        Caption = '9 - Exigir modelo nota fiscal'
        TabOrder = 7
        OnKeyDown = ProximoCampo
      end
      object ckSepararSomenteCodigoBarras: TCheckBoxLuka
        Left = 293
        Top = 345
        Width = 258
        Height = 17
        Caption = '10 - Separar somente com c'#243'digo de barras'
        TabOrder = 8
        CheckedStr = 'N'
      end
      object ckControlaPeso: TCheckBoxLuka
        Left = 293
        Top = 362
        Width = 169
        Height = 17
        Caption = '11 - Produto controla peso'
        TabOrder = 9
        CheckedStr = 'N'
      end
      object ckRevenda: TCheckBox
        Left = 293
        Top = 377
        Width = 90
        Height = 17
        Caption = '12 - Revenda'
        Checked = True
        State = cbChecked
        TabOrder = 10
        OnKeyDown = ProximoCampo
      end
      object ckUsoConsumo: TCheckBox
        Left = 640
        Top = 294
        Width = 133
        Height = 17
        Caption = '13 - Uso e consumo'
        TabOrder = 11
        OnKeyDown = ProximoCampo
      end
      object ckAtivoImobilizado: TCheckBox
        Left = 640
        Top = 311
        Width = 150
        Height = 17
        Caption = '14 - Ativo Imobilizado'
        TabOrder = 12
        OnKeyDown = ProximoCampo
      end
      object ckServico: TCheckBox
        Left = 640
        Top = 328
        Width = 150
        Height = 17
        Caption = '15 - Servi'#231'o'
        TabOrder = 13
        OnKeyDown = ProximoCampo
      end
      object ckExigirSeparacao: TCheckBox
        Left = 293
        Top = 311
        Width = 258
        Height = 17
        Caption = '8 - Exigir separa'#231#227'o'
        TabOrder = 16
        OnKeyDown = ProximoCampo
      end
      object ckSujeitoComissao: TCheckBox
        Left = 3
        Top = 18
        Width = 91
        Height = 17
        Caption = 'Comissionar'
        TabOrder = 17
        OnKeyDown = ProximoCampo
      end
      object GroupBoxLuka1: TGroupBoxLuka
        Left = 3
        Top = 34
        Width = 206
        Height = 46
        TabOrder = 18
        OpcaoMarcarDesmarcar = False
        object lb14: TLabel
          Left = 104
          Top = 4
          Width = 81
          Height = 14
          Caption = '% Com. a prazo'
        end
        object lb13: TLabel
          Left = 9
          Top = 4
          Width = 77
          Height = 14
          Caption = '% Com. a vista'
        end
        object ePercentualComissaoVista: TEditLuka
          Left = 9
          Top = 18
          Width = 90
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          MaxLength = 5
          TabOrder = 0
          Text = '0,00'
          OnKeyDown = ProximoCampo
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          NaoAceitarEspaco = False
        end
        object ePercentualComissaoPrazo: TEditLuka
          Left = 107
          Top = 18
          Width = 90
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          MaxLength = 5
          TabOrder = 1
          Text = '0,00'
          OnKeyDown = ProximoCampo
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          NaoAceitarEspaco = False
        end
      end
      object ckInativarProdutoZerarEstoque: TCheckBox
        Left = 3
        Top = 112
        Width = 247
        Height = 17
        Caption = 'Inativar ao zerar o estoque'
        TabOrder = 19
        OnKeyDown = ProximoCampo
      end
      object eMotivoInatividade: TEditLuka
        Left = 3
        Top = 150
        Width = 374
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 60
        TabOrder = 20
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ckEmitirSomenteNFE: TCheckBox
        Left = 640
        Top = 345
        Width = 159
        Height = 17
        Caption = '16 - Emitir somente NF-e'
        Enabled = False
        TabOrder = 14
        OnKeyDown = ProximoCampo
      end
      object eCodigoSistemaAnterior: TEditLuka
        Left = 3
        Top = 195
        Width = 156
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 60
        ReadOnly = True
        TabOrder = 21
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ckExportarWeb: TCheckBox
        Left = 640
        Top = 361
        Width = 159
        Height = 17
        Caption = '17 - Exportar para web'
        Enabled = False
        TabOrder = 15
        OnKeyDown = ProximoCampo
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Inf. Pre'#231'os/Custos'
      ImageIndex = 6
      object Label9: TLabel
        Left = 0
        Top = 9
        Width = 83
        Height = 14
        Caption = 'Pre'#231'o de Venda'
      end
      object Label10: TLabel
        Left = 98
        Top = 9
        Width = 91
        Height = 14
        Caption = 'Custo de Compra'
      end
      object ePrecoVenda: TEditLuka
        Left = 0
        Top = 25
        Width = 92
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        ReadOnly = True
        TabOrder = 0
        Text = '0,000'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 3
        NaoAceitarEspaco = False
      end
      object eCustoCompra: TEditLuka
        Left = 98
        Top = 25
        Width = 101
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        ReadOnly = True
        TabOrder = 1
        Text = '0,000'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 3
        NaoAceitarEspaco = False
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 64
        Width = 473
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Custo fixo de venda por produto'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgCustoCompraFixo: TGridLuka
        Left = 0
        Top = 77
        Width = 473
        Height = 156
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        ColCount = 4
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goEditing]
        TabOrder = 3
        OnDrawCell = sgCustoCompraFixoDrawCell
        OnKeyDown = sgCustoCompraFixoKeyDown
        OnKeyPress = NumerosVirgula
        OnSelectCell = sgCustoCompraFixoSelectCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Empresa'
          'Nome da empresa'
          'Custo %'
          'Zerar custo total')
        OnGetCellColor = sgCustoCompraFixoGetCellColor
        Grid3D = False
        RealColCount = 4
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          55
          201
          106
          97)
      end
    end
  end
end
