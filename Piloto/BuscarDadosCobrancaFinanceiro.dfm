inherited FormBuscarDadosCobrancaFinanceiro: TFormBuscarDadosCobrancaFinanceiro
  Caption = 'Buscar dados de cobran'#231'a - Financeiro'
  ClientHeight = 318
  ClientWidth = 581
  OnShow = FormShow
  ExplicitWidth = 587
  ExplicitHeight = 347
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 281
    Width = 581
    ExplicitTop = 281
    ExplicitWidth = 581
  end
  inline FrDadosCobranca: TFrDadosCobranca
    Left = 1
    Top = 3
    Width = 578
    Height = 274
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 1
    ExplicitTop = 3
    ExplicitHeight = 274
    inherited pnInformacoesPrincipais: TPanel
      inherited lbllb10: TLabel
        Width = 39
        Height = 14
        ExplicitWidth = 39
        ExplicitHeight = 14
      end
      inherited lbllb9: TLabel
        Width = 29
        Height = 14
        ExplicitWidth = 29
        ExplicitHeight = 14
      end
      inherited lbllb6: TLabel
        Width = 28
        Height = 14
        ExplicitWidth = 28
        ExplicitHeight = 14
      end
      inherited lbllb2: TLabel
        Width = 64
        Height = 14
        ExplicitWidth = 64
        ExplicitHeight = 14
      end
      inherited lbl2: TLabel
        Width = 62
        Height = 14
        ExplicitWidth = 62
        ExplicitHeight = 14
      end
      inherited eRepetir: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited ePrazo: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eDataVencimento: TEditLukaData
        Height = 22
        ExplicitHeight = 22
      end
      inherited FrTiposCobranca: TFrTiposCobranca
        inherited PnTitulos: TPanel
          inherited lbNomePesquisa: TLabel
            Height = 15
          end
        end
      end
      inherited eDocumento: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
    inherited pnInformacoesBoleto: TPanel
      inherited lbllb31: TLabel
        Width = 79
        Height = 14
        ExplicitWidth = 79
        ExplicitHeight = 14
      end
      inherited lbl1: TLabel
        Width = 92
        Height = 14
        ExplicitWidth = 92
        ExplicitHeight = 14
      end
      inherited eNossoNumero: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eCodigoBarras: TMaskEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
    inherited pn1: TPanel
      Height = 161
      ExplicitHeight = 161
      inherited sgCobrancas: TGridLuka
        Top = 2
        Height = 159
        ExplicitTop = 2
        ExplicitHeight = 159
      end
      inherited StaticTextLuka4: TStaticTextLuka
        Color = 38619
        Font.Color = clBlack
      end
    end
    inherited pnInformacoesCheque: TPanel
      inherited lbllb1: TLabel
        Width = 33
        Height = 14
        ExplicitWidth = 33
        ExplicitHeight = 14
      end
      inherited lbllb3: TLabel
        Left = 53
        Width = 43
        Height = 14
        ExplicitLeft = 53
        ExplicitWidth = 43
        ExplicitHeight = 14
      end
      inherited lbllb4: TLabel
        Left = 106
        Width = 79
        Height = 14
        ExplicitLeft = 106
        ExplicitWidth = 79
        ExplicitHeight = 14
      end
      inherited lbllb5: TLabel
        Left = 189
        Width = 71
        Height = 14
        ExplicitLeft = 189
        ExplicitWidth = 71
        ExplicitHeight = 14
      end
      inherited lbl3: TLabel
        Left = 267
        Width = 85
        Height = 14
        ExplicitLeft = 267
        ExplicitWidth = 85
        ExplicitHeight = 14
      end
      inherited lbl4: TLabel
        Left = 493
        Width = 48
        Height = 14
        ExplicitLeft = 493
        ExplicitWidth = 48
        ExplicitHeight = 14
      end
      inherited lbCPF_CNPJ: TLabel
        Width = 52
        Height = 14
        ExplicitWidth = 52
        ExplicitHeight = 14
      end
      inherited eBanco: TEditLuka
        Width = 48
        Height = 22
        ExplicitWidth = 48
        ExplicitHeight = 22
      end
      inherited eAgencia: TEditLuka
        Left = 53
        Width = 50
        Height = 22
        ExplicitLeft = 53
        ExplicitWidth = 50
        ExplicitHeight = 22
      end
      inherited eContaCorrente: TEditLuka
        Left = 106
        Height = 22
        ExplicitLeft = 106
        ExplicitHeight = 22
      end
      inherited eNumeroCheque: TEditLuka
        Left = 189
        Width = 74
        Height = 22
        ExplicitLeft = 189
        ExplicitWidth = 74
        ExplicitHeight = 22
      end
      inherited eNomeEmitente: TEditLuka
        Left = 267
        Width = 116
        Height = 22
        ExplicitLeft = 267
        ExplicitWidth = 116
        ExplicitHeight = 22
      end
      inherited eTelefoneEmitente: TEditTelefoneLuka
        Left = 493
        Width = 85
        Height = 22
        ExplicitLeft = 493
        ExplicitWidth = 85
        ExplicitHeight = 22
      end
      inherited eCPF_CNPJ: TEditCPF_CNPJ_Luka
        Width = 100
        Height = 22
        OnKeyDown = ProximoCampo
        ExplicitWidth = 100
        ExplicitHeight = 22
      end
    end
  end
end
