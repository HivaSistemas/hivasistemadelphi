inherited FormBuscarDadosCompras: TFormBuscarDadosCompras
  Caption = 'Buscar dados de compras'
  ClientHeight = 417
  ClientWidth = 683
  ExplicitWidth = 689
  ExplicitHeight = 446
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 380
    Width = 683
    ExplicitTop = 380
    ExplicitWidth = 683
    inherited sbFinalizar: TSpeedButton
      Left = 225
      ExplicitLeft = 225
    end
  end
  inline FrFornecedor: TFrFornecedores
    Left = 1
    Top = 0
    Width = 288
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 1
    ExplicitWidth = 288
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 263
      Height = 24
      ExplicitWidth = 263
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 288
      ExplicitWidth = 288
      inherited lbNomePesquisa: TLabel
        Width = 61
        Caption = 'Fornecedor'
        ExplicitWidth = 61
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 183
        ExplicitLeft = 183
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 263
      Height = 25
      ExplicitLeft = 263
      ExplicitHeight = 25
      inherited sbPesquisa: TSpeedButton
        Height = 24
        ExplicitLeft = 1
        ExplicitHeight = 24
      end
    end
    inherited ckUsoConsumo: TCheckBox
      Left = 110
      Top = 20
      ExplicitLeft = 110
      ExplicitTop = 20
    end
    inherited poOpcoes: TPopupMenu
      Left = 160
      Top = 16
    end
  end
  inline FrDatasCadastro: TFrDataInicialFinal
    Left = 296
    Top = 2
    Width = 201
    Height = 41
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 296
    ExplicitTop = 2
    ExplicitWidth = 201
    inherited Label1: TLabel
      Width = 93
      Height = 14
      ExplicitWidth = 93
      ExplicitHeight = 14
    end
    inherited lb1: TLabel
      Width = 18
      Height = 14
      ExplicitWidth = 18
      ExplicitHeight = 14
    end
    inherited eDataFinal: TEditLukaData
      Height = 22
      OnKeyDown = FrDatasCadastroeDataFinalKeyDown
      ExplicitHeight = 22
    end
    inherited eDataInicial: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
  end
  object sgPesquisa: TGridLuka
    Left = 1
    Top = 43
    Width = 680
    Height = 160
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 7
    Ctl3D = True
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
    ParentCtl3D = False
    TabOrder = 3
    OnClick = sgPesquisaClick
    OnDrawCell = sgPesquisaDrawCell
    OnKeyDown = sgPesquisaKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Sel.?'
      'Compra'
      'Fornecedor'
      'Data cadastro'
      'Previs'#227'o de entrega'
      'Comprador'
      'Empresa')
    OnGetCellPicture = sgPesquisaGetCellPicture
    Grid3D = False
    RealColCount = 7
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      34
      52
      192
      90
      113
      124
      126)
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 1
    Top = 203
    Width = 680
    Height = 15
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Itens da compra selecionada'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object sgItens: TGridLuka
    Left = 1
    Top = 218
    Width = 680
    Height = 162
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 9
    Ctl3D = True
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRowSelect, goThumbTracking]
    ParentCtl3D = False
    TabOrder = 5
    OnDrawCell = sgItensDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Qtde.'
      'Und.'
      'Entr.'
      'Baix.'
      'Canc.'
      'Saldo')
    Grid3D = False
    RealColCount = 9
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      52
      217
      143
      48
      31
      39
      35
      38
      49)
  end
end
