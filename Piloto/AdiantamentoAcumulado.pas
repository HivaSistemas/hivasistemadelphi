unit AdiantamentoAcumulado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca, _Sessao,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, SpeedButtonLuka, Vcl.Mask, _RecordsEspeciais,
  EditLukaData, _FrameHerancaPrincipal, FrameFechamento, _RecordsOrcamentosVendas, Pesquisa.Orcamentos,
  _Orcamentos, _TurnosCaixas, FramePagamentoFinanceiro, _ContasReceberBaixas, _ContasPagar, _ContasReceberBaixasItens,
  _ContasReceber, Informacoes.Orcamento;

type
  TFormAdiantamentoAcumulado = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eCliente: TEditLuka;
    lb3: TLabel;
    eDataRecebimento: TEditLukaData;
    sbInformacoesPedido: TSpeedButtonLuka;
    FrPagamentoFinanceiro: TFrPagamentoFinanceiro;
    lbl1: TLabel;
    eValorAdiantamento: TEditLuka;
    lb2: TLabel;
    eVendedor: TEditLuka;
    eSaldo: TEditLuka;
    lb4: TLabel;
    procedure eValorAdiantamentoChange(Sender: TObject);
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); reintroduce;
    procedure sbInformacoesPedidoClick(Sender: TObject);
  private
    FPedido: RecOrcamentos;

    procedure PreencherRegistro(pOrcamento: RecOrcamentos);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure Modo(pEditando: Boolean); override;
    procedure PesquisarRegistro; override;
  end;

implementation

{$R *.dfm}

{ TFormAdiantamentoAcumulado }

procedure TFormAdiantamentoAcumulado.BuscarRegistro;
var
  pOrcamento: TArray<RecOrcamentos>;
begin
  pOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 7, [eId.AsInt]);
  if pOrcamento = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(pOrcamento[0]);
end;

procedure TFormAdiantamentoAcumulado.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key <> VK_RETURN then
    Exit;

  if eID.AsInt = 0 then begin
    Exclamar('� necess�rio informar o n�mero do pedido!');
    SetarFoco(eID);
    Abort;
  end;

  BuscarRegistro;
end;

procedure TFormAdiantamentoAcumulado.eValorAdiantamentoChange(Sender: TObject);
begin
  inherited;
  FrPagamentoFinanceiro.ValorPagar := eValorAdiantamento.AsDouble;
end;

procedure TFormAdiantamentoAcumulado.GravarRegistro(Sender: TObject);
var
  vTurnoId: Integer;
  vTemCartao: Boolean;
  vRetBanco: RecRetornoBD;
  vBaixaReceberId: Integer;

  vReceberId: Integer;
  vDataAtual: TDate;
begin
  inherited;

  vTurnoId := _TurnosCaixas.getTurnoId( Sessao.getConexaoBanco, Sessao.getUsuarioLogado.funcionario_id );
  if (vTurnoId = 0) and (FrPagamentoFinanceiro.eValorDinheiro.AsCurr > 0) then begin
    _Biblioteca.Exclamar('Para pagamento em dinheiro � necess�rio um turno aberto!');
    SetarFoco(FrPagamentoFinanceiro.eValorDinheiro);
    Abort;
  end;

  vTemCartao := FrPagamentoFinanceiro.ValorCartaoDebito + FrPagamentoFinanceiro.ValorCartaoCredito > 0;
  vDataAtual := Sessao.getData;

  Sessao.getConexaoBanco.IniciarTransacao;

  // Lan�ando o t�tulo adiantamento no contas a receber para ser baixado posteriormente
  vRetBanco :=
    _ContasReceber.AtualizarContaReceber(
      Sessao.getConexaoBanco,
      0,
      FPedido.empresa_id,
      Sessao.getParametros.TipoCobAdiantamentoAcuId,
      FPedido.cliente_id,
      1,
      1,
      '9999',
      '1.002.001',
      '',
      '',
      0,
      eValorAdiantamento.AsCurr,
      '',
      'ADA' + NFormat(FPedido.orcamento_id),
      'A',
      vDataAtual,
      vDataAtual,
      vDataAtual,
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      1,
      'ADA',
      0,
      FPedido.orcamento_id,
      vDataAtual,
      0,
      True
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  vReceberId := vRetBanco.AsInt;

  // Gerando a baixa
  vRetBanco :=
    _ContasReceberBaixas.AtualizarContaReceberBaixa(
      Sessao.getConexaoBanco,
      0,
      FPedido.empresa_id,
      FPedido.cliente_id,
      IIfInt((vTurnoId > 0) and (FrPagamentoFinanceiro.Dinheiro = nil) and (not vTemCartao), vTurnoId),
      eValorAdiantamento.AsCurr,
      0,
      0,
      0,
      0,
      0,
      FrPagamentoFinanceiro.ValorDinheiro,
      FrPagamentoFinanceiro.ValorCheque,
      FrPagamentoFinanceiro.ValorCartaoDebito,
      FrPagamentoFinanceiro.ValorCartaoCredito,
      FrPagamentoFinanceiro.ValorCobranca,
      0,
      IIfDbl(vTemCartao, 0, Sessao.getData),
      IIfStr(vTemCartao, 'S', 'N'),
      '',
      IIfStr(vTemCartao, 'N', 'S'),
      'CRE',
      0,
      FrPagamentoFinanceiro.Dinheiro,
      FrPagamentoFinanceiro.CartoesDebito,
      FrPagamentoFinanceiro.CartoesCredito,
      FrPagamentoFinanceiro.Cobrancas,
      FrPagamentoFinanceiro.Cheques,
      0,
      0,
      0,
      True,
      FrPagamentoFinanceiro.eValorPix.AsCurr,
      FrPagamentoFinanceiro.Pix,
      'N'
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  vBaixaReceberId := vRetBanco.AsInt;

  // Gravando o t�tulo de adiantamento dentro da baixa
  vRetBanco :=
    _ContasReceberBaixasItens.AtualizarContasReceberBaixasItens(
      Sessao.getConexaoBanco,
      vBaixaReceberId,
      vReceberId,
      FrPagamentoFinanceiro.eValorDinheiro.AsCurr,
      FrPagamentoFinanceiro.eValorCartaoDebito.AsCurr,
      FrPagamentoFinanceiro.eValorCartaoCredito.AsCurr,
      FrPagamentoFinanceiro.eValorCheque.AsCurr,
      FrPagamentoFinanceiro.eValorCobranca.AsCurr,
      FrPagamentoFinanceiro.eValorCredito.AsCurr,
      0,
      0,
      0,
      FrPagamentoFinanceiro.eValorPix.AsCurr,
      True
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  // Lan�ando o cr�dito no contas a pagar
  vRetBanco :=
    _ContasPagar.AtualizarContaPagar(
      Sessao.getConexaoBanco,
      0,
      FPedido.empresa_id,
      FPedido.cliente_id,
      Sessao.getParametros.TipoCobrancaGeracaoCredId,
      '9999',
      '1.002.001',
      'CRE' + NFormat(FPedido.cliente_id) + DFormat(FPedido.data_hora_recebimento + FPedido.QtdeDiasVencimentoAcumulado),
      eValorAdiantamento.AsCurr,
      0,
      'ADA',
      vBaixaReceberId,
      1,
      1,
      0,
      'A',
      FPedido.data_hora_recebimento + FPedido.QtdeDiasVencimentoAcumulado,
      FPedido.data_hora_recebimento + FPedido.QtdeDiasVencimentoAcumulado,
      '',
      '',
      'N',
      '',
      FPedido.orcamento_id,
      vDataAtual,
      vDataAtual,
      '',
      1,
      True
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  // Ajustando o valor do adiantamento no pedido
  vRetBanco :=
    _Orcamentos.AtualizarValorAdiantado(
      Sessao.getConexaoBanco,
      FPedido.orcamento_id,
      eValorAdiantamento.AsCurr,
      True
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar('Adiantamento realizado com sucesso.');
end;

procedure TFormAdiantamentoAcumulado.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    sbInformacoesPedido,
    eCliente,
    eDataRecebimento,
    FrPagamentoFinanceiro,
    eVendedor,
    eSaldo,
    eValorAdiantamento],
    pEditando
  );

  if pEditando then
    SetarFoco(eValorAdiantamento);
end;

procedure TFormAdiantamentoAcumulado.PesquisarRegistro;
var
  vRetTela: TRetornoTelaFinalizar<RecOrcamentos>;
begin
  vRetTela := Pesquisa.Orcamentos.Pesquisar(tpAcumuladosAberto);
  if vRetTela.BuscaCancelada then
    Exit;

  inherited;
  PreencherRegistro(vRetTela.Dados);
end;

procedure TFormAdiantamentoAcumulado.PreencherRegistro(pOrcamento: RecOrcamentos);
begin
  eID.SetInformacao(pOrcamento.orcamento_id);
  eCliente.SetInformacao(pOrcamento.cliente_id, pOrcamento.nome_cliente);
  eVendedor.SetInformacao(pOrcamento.vendedor_id, pOrcamento.nome_vendedor);
  eSaldo.AsCurr                    := pOrcamento.SaldoDevedorAcumulado;
  eDataRecebimento.AsData          := pOrcamento.data_hora_recebimento;
  FrPagamentoFinanceiro.ValorPagar := 0;

  FPedido := pOrcamento;
end;

procedure TFormAdiantamentoAcumulado.sbInformacoesPedidoClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(eID.AsInt);
end;

procedure TFormAdiantamentoAcumulado.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if Sessao.getParametros.TipoCobAdiantamentoAcuId = 0 then begin
    _Biblioteca.Exclamar('O tipo de cobran�a para adiantamento do acumulado n�o foi parametrizado, entre em contato com a Hiva!');
    SetarFoco(eValorAdiantamento);
    Abort;
  end;

  if eValorAdiantamento.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O valor a ser adiantado n�o foi informado corretamente!');
    SetarFoco(eValorAdiantamento);
    Abort;
  end;

  if eValorAdiantamento.AsCurr >= eSaldo.AsCurr then begin
    _Biblioteca.Exclamar('O valor do adiantamento n�o pode ser igual ou maior que o saldo restante do acumulado!');
    SetarFoco(eValorAdiantamento);
    Abort;
  end;

  FrPagamentoFinanceiro.VerificarRegistro;

  if FrPagamentoFinanceiro.ExisteDiferenca then begin
    _Biblioteca.Exclamar('Existem diferen�as entre o valor a pagar e as formas de pagamento definidas!');
    SetarFoco(FrPagamentoFinanceiro.eValorDinheiro);
    Abort;
  end;

  if FrPagamentoFinanceiro.eValorCredito.AsCurr > eSaldo.AsCurr then begin
    _Biblioteca.Exclamar('O valor dos cr�ditos n�o pode ser maior que o valor do adiantamento!');
    SetarFoco(FrPagamentoFinanceiro.eValorCredito);
    Abort;
  end;
end;

end.
