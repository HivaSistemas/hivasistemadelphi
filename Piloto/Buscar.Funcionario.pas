unit Buscar.Funcionario;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, _Biblioteca,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameVendedores, Vcl.Buttons,
  Vcl.ExtCtrls, FrameFuncionarios;

type
  TFormBuscarFuncionario = class(TFormHerancaFinalizar)
    FrFuncionario: TFrFuncionarios;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(pFuncionarioId: Integer; pFuncao: Integer): TRetornoTelaFinalizar<Integer>;

implementation

{$R *.dfm}

function Buscar(pFuncionarioId: Integer; pFuncao: Integer): TRetornoTelaFinalizar<Integer>;
var
  vDescricao: string;
  vForm: TFormBuscarFuncionario;
begin
  vForm := TFormBuscarFuncionario.Create(Application);

  if pFuncao = 4 then
    vDescricao := 'Motorista'
  else
    vDescricao := 'Funcion�rio';

  if pFuncionarioId > 0 then
    vForm.FrFuncionario.InserirDadoPorChave(pFuncionarioId, False);

  vForm.FrFuncionario.lbNomePesquisa.Caption := vDescricao;

  if Result.Ok(vForm.ShowModal, True) then
    Result.Dados := vForm.FrFuncionario.GetFuncionario.funcionario_id;
end;

procedure TFormBuscarFuncionario.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrFuncionario.EstaVazio then begin
    Exclamar('O funcion�rio n�o foi definido!');
    SetarFoco(FrFuncionario);
    Abort;
  end;
end;


end.
