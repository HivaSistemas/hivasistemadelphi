inherited FormPesquisaContasCustodia: TFormPesquisaContasCustodia
  Caption = 'Pesquisa de contas cust'#243'dia'
  ClientHeight = 233
  ClientWidth = 396
  ExplicitWidth = 404
  ExplicitHeight = 264
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 396
    Height = 187
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Ativo?')
    RealColCount = 4
    ExplicitWidth = 396
    ExplicitHeight = 187
    ColWidths = (
      28
      55
      199
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    Width = 396
    ExplicitWidth = 396
    inherited eValorPesquisa: TEditLuka
      Width = 192
      ExplicitWidth = 192
    end
  end
end
