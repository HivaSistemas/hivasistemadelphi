unit FrameEdicaoTexto;

interface

uses
  FrameTextoFormatado, _Biblioteca, System.SysUtils, Vcl.Buttons, Vcl.StdCtrls, Vcl.Controls, Vcl.ExtCtrls, Vcl.ComCtrls, System.Classes, Vcl.Forms, Vcl.Graphics,
  Winapi.Windows;

type
  TFrEdicaoTexto = class(TFrTextoFormatado)
    pnFerramentas: TPanel;
    sbSublinhado: TSpeedButton;
    sbItalico: TSpeedButton;
    sbNegrito: TSpeedButton;
    Bevel2: TBevel;
    sbDireito: TSpeedButton;
    sbCentro: TSpeedButton;
    sbEsquerdo: TSpeedButton;
    cbCorFonte: TColorBox;
    cbFonte: TComboBox;
    sbRiscado: TSpeedButton;
    cbTamanho: TComboBox;

    procedure sbNegritoClick(Sender: TObject);
    procedure sbItalicoClick(Sender: TObject);
    procedure sbSublinhadoClick(Sender: TObject);
    procedure sbRiscadoClick(Sender: TObject);
    procedure rtTextoAuxSelectionChange(Sender: TObject);
    procedure sbEsquerdoClick(Sender: TObject);
    procedure sbCentroClick(Sender: TObject);
    procedure sbDireitoClick(Sender: TObject);
    procedure cbFonteChange(Sender: TObject);
    procedure cbTamanhoChange(Sender: TObject);
    procedure cbFonteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cbTamanhoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cbCorFonteChange(Sender: TObject);
    procedure cbCorFonteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure rtTextoAuxChange(Sender: TObject);
    procedure sbInserirMarcadorClick(Sender: TObject);
  private
    { Private declarations }

    procedure AjustarNegrito;
    procedure AjustarItalico;
    procedure AjustarSublinhado;
    procedure AjustarRiscado;
    procedure AjustarNomeFonte;
    procedure AjustarTamanhoFonte;
    procedure AjustarCorFonte;

    procedure AjustarAlinhamentoEsquerdo;
    procedure AjustarAlinhamentoCentro;
    procedure AjustarAlinhamentoDireito;

    procedure AjustarMarcador;

    procedure AjustarAtributosBarraFerramentas;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure SomenteLeitura(pValue: Boolean); override;
    function  EmEdicao: Boolean;
  protected
    procedure Atalhos(pSender: TObject; var pKey: Word; pShift: TShiftState); override;
  end;

implementation                                               

{$R *.dfm}

procedure TFrEdicaoTexto.AjustarAlinhamentoCentro;
begin
  if sbCentro.Down then
    rtTexto.Paragraph.Alignment := taCenter;
end;

procedure TFrEdicaoTexto.AjustarAlinhamentoDireito;
begin
  if sbDireito.Down then
    rtTexto.Paragraph.Alignment := taRightJustify;
end;

procedure TFrEdicaoTexto.AjustarAlinhamentoEsquerdo;
begin
  if sbEsquerdo.Down then
    rtTexto.Paragraph.Alignment := taLeftJustify;
end;

procedure TFrEdicaoTexto.AjustarAtributosBarraFerramentas;
begin
  with rtTexto.SelAttributes do begin
    sbNegrito.Down := fsBold in Style;
    sbItalico.Down := fsItalic in Style;
    sbSublinhado.Down := fsUnderline in Style;
    sbRiscado.Down := fsStrikeOut in Style;
    cbFonte.ItemIndex := cbFonte.Items.IndexOf(Name);
    cbTamanho.ItemIndex := cbTamanho.Items.IndexOf(IntToStr(Size));
    cbCorFonte.Selected := Color;
  end;

  with rtTexto.Paragraph do begin
    sbEsquerdo.Down := Alignment = taLeftJustify;
    sbCentro.Down := Alignment = taCenter;
    sbDireito.Down := Alignment = taRightJustify;
  end;
end;

procedure TFrEdicaoTexto.AjustarCorFonte;
begin
  rtTexto.SelAttributes.Color := cbCorFonte.Selected;
end;

procedure TFrEdicaoTexto.AjustarItalico;
begin
  if sbItalico.Down then
    rtTexto.SelAttributes.Style := rtTexto.SelAttributes.Style + [fsItalic]
  else
    rtTexto.SelAttributes.Style := rtTexto.SelAttributes.Style - [fsItalic];
end;

procedure TFrEdicaoTexto.AjustarMarcador;
begin
  rtTexto.Paragraph.Numbering := nsNone;
end;

procedure TFrEdicaoTexto.AjustarNegrito;
begin
  if sbNegrito.Down then
    rtTexto.SelAttributes.Style := rtTexto.SelAttributes.Style + [fsBold]
  else
    rtTexto.SelAttributes.Style := rtTexto.SelAttributes.Style - [fsBold];
end;

procedure TFrEdicaoTexto.AjustarNomeFonte;
begin
  rtTexto.SelAttributes.Name := cbFonte.Text;
end;

procedure TFrEdicaoTexto.AjustarSublinhado;
begin
  if sbSublinhado.Down then
    rtTexto.SelAttributes.Style := rtTexto.SelAttributes.Style + [fsUnderline]
  else
    rtTexto.SelAttributes.Style := rtTexto.SelAttributes.Style - [fsUnderline];
end;

procedure TFrEdicaoTexto.AjustarRiscado;
begin
  if sbRiscado.Down then
    rtTexto.SelAttributes.Style := rtTexto.SelAttributes.Style + [fsStrikeOut]
  else
    rtTexto.SelAttributes.Style := rtTexto.SelAttributes.Style - [fsStrikeOut];
end;

procedure TFrEdicaoTexto.AjustarTamanhoFonte;
begin
  rtTexto.SelAttributes.Size := StrToInt(cbTamanho.Text);
end;

procedure TFrEdicaoTexto.cbCorFonteChange(Sender: TObject);
begin
  AjustarCorFonte;
end;

procedure TFrEdicaoTexto.cbCorFonteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    SetarFoco(rtTexto);
end;

procedure TFrEdicaoTexto.cbFonteChange(Sender: TObject);
begin
  AjustarNomeFonte;
end;

procedure TFrEdicaoTexto.cbFonteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    SetarFoco(rtTexto);
end;

procedure TFrEdicaoTexto.cbTamanhoChange(Sender: TObject);
begin
  AjustarTamanhoFonte;
end;

procedure TFrEdicaoTexto.cbTamanhoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
    SetarFoco(rtTexto);
end;

constructor TFrEdicaoTexto.Create(AOwner: TComponent);
var
  i: Integer;
begin
  inherited;

  cbFonte.Items := Screen.Fonts;
  cbFonte.ItemIndex := cbFonte.Items.IndexOf(rtTexto.Font.Name);

  for i := 7 to 100 do
    cbTamanho.Items.Add(IntToStr(i));
  cbTamanho.ItemIndex := cbTamanho.Items.IndexOf(IntToStr(rtTexto.Font.Size));

end;

function TFrEdicaoTexto.EmEdicao: Boolean;
begin
  Result := not rtTexto.ReadOnly;
end;

procedure TFrEdicaoTexto.Modo(pEditando: Boolean; pLimpar: Boolean = True);
begin
  sbNegrito.Enabled := pEditando;
  sbItalico.Enabled := pEditando;
  sbSublinhado.Enabled := pEditando;
  sbRiscado.Enabled := pEditando;
  cbFonte.Enabled := pEditando;
  cbTamanho.Enabled := pEditando;
  cbCorFonte.Enabled := pEditando;

  sbEsquerdo.Enabled := pEditando;
  sbCentro.Enabled := pEditando;
  sbDireito.Enabled := pEditando;

	inherited;
end;

procedure TFrEdicaoTexto.rtTextoAuxChange(Sender: TObject);
begin
  stbStatus.Panels[1].Text := 'Caracteres: ' + FormatFloat('#,0', rtTexto.GetTextLen);
end;

procedure TFrEdicaoTexto.rtTextoAuxSelectionChange(Sender: TObject);
begin
  AjustarAtributosBarraFerramentas;
end;

procedure TFrEdicaoTexto.sbRiscadoClick(Sender: TObject);
begin
  AjustarRiscado;
end;

procedure TFrEdicaoTexto.sbCentroClick(Sender: TObject);
begin
  AjustarAlinhamentoCentro;
end;

procedure TFrEdicaoTexto.sbDireitoClick(Sender: TObject);
begin
  AjustarAlinhamentoDireito;
end;

procedure TFrEdicaoTexto.sbEsquerdoClick(Sender: TObject);
begin
  AjustarAlinhamentoEsquerdo;
end;

procedure TFrEdicaoTexto.sbInserirMarcadorClick(Sender: TObject);
begin
	AjustarMarcador;
end;

procedure TFrEdicaoTexto.sbItalicoClick(Sender: TObject);
begin
  AjustarItalico;
end;

procedure TFrEdicaoTexto.sbNegritoClick(Sender: TObject);
begin
  AjustarNegrito;
end;

procedure TFrEdicaoTexto.sbSublinhadoClick(Sender: TObject);
begin
  AjustarSublinhado;
end;

procedure TFrEdicaoTexto.SomenteLeitura(pValue: Boolean);
begin
	rtTexto.ReadOnly := pValue;
  pnFerramentas.Visible := not pValue;

  if not pnFerramentas.Visible then
  	pnFerramentas.Top := -100;
end;

procedure TFrEdicaoTexto.Atalhos(pSender: TObject; var pKey: Word; pShift: TShiftState);
begin
  inherited;

  if rtTexto.ReadOnly then
    Exit;

  if pShift = [ssCtrl] then begin
    if pKey = Ord('N') then begin
      sbNegrito.Down := not sbNegrito.Down;
      AjustarNegrito;
    end
    else if pKey = Ord('L') then begin
      sbItalico.Down := not sbItalico.Down;
      AjustarItalico;
    end
    else if pKey = Ord('B') then begin
      sbSublinhado.Down := not sbSublinhado.Down;
      AjustarSublinhado;
    end
    else if pKey = Ord('R') then begin
      sbRiscado.Down := not sbRiscado.Down;
      AjustarRiscado;
    end
    else if pKey = VK_ADD then begin
      if cbTamanho.ItemIndex < cbTamanho.Items.Count - 1 then begin
        cbTamanho.ItemIndex := cbTamanho.ItemIndex + 1;
        AjustarTamanhoFonte;
      end;
    end
    else if pKey = VK_SUBTRACT then begin
      if cbTamanho.ItemIndex > 0 then begin
        cbTamanho.ItemIndex := cbTamanho.ItemIndex - 1;
        AjustarTamanhoFonte;
      end;
    end;
  end
  else if pShift = [ssCtrl, ssShift] then begin
    if pKey = Ord('E') then begin
      sbEsquerdo.Down := True;
      AjustarAlinhamentoEsquerdo;
    end
    else if pKey = Ord('C') then begin
      sbCentro.Down := True;
      AjustarAlinhamentoCentro;
    end
    else if pKey = Ord('D') then begin
      sbDireito.Down := True;
      AjustarAlinhamentoDireito;
    end;
  end;
end;

end.
