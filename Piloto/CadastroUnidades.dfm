inherited FormCadastroUnidades: TFormCadastroUnidades
  Caption = 'Cadastro de unidades'
  ClientHeight = 206
  ClientWidth = 451
  ExplicitWidth = 457
  ExplicitHeight = 235
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 126
    Top = 58
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Height = 206
    TabOrder = 1
    ExplicitHeight = 191
  end
  inherited eID: TEditLuka
    Alignment = taLeftJustify
    TabOrder = 0
    TipoCampo = tcTexto
  end
  inherited ckAtivo: TCheckBoxLuka
    TabOrder = 3
  end
  object eDescricao: TEditLuka
    Left = 126
    Top = 73
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
