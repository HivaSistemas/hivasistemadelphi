inherited FormReceberBaixaContasReceber: TFormReceberBaixaContasReceber
  Caption = 'Rec. de baixas do contas a receber'
  ClientHeight = 226
  ClientWidth = 305
  OnShow = FormShow
  ExplicitWidth = 311
  ExplicitHeight = 255
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 189
    Width = 305
    ExplicitTop = 189
    ExplicitWidth = 332
  end
  inline FrPagamentoFinanceiro: TFrPagamentoFinanceiro
    Left = 1
    Top = 1
    Width = 300
    Height = 181
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 1
    ExplicitTop = 1
    ExplicitWidth = 300
    ExplicitHeight = 181
    inherited grpFechamento: TGroupBox
      Width = 300
      Height = 181
      ExplicitLeft = -8
      ExplicitWidth = 300
      ExplicitHeight = 181
      inherited lbllb11: TLabel
        Left = 306
        Visible = False
        ExplicitLeft = 306
      end
      inherited lbllb12: TLabel
        Left = 306
        Visible = False
        ExplicitLeft = 306
      end
      inherited lbllb13: TLabel
        Left = 32
        Top = 117
        Width = 78
        ExplicitLeft = 32
        ExplicitTop = 117
        ExplicitWidth = 78
      end
      inherited lbllb14: TLabel
        Left = 306
        Visible = False
        ExplicitLeft = 306
      end
      inherited lbllb15: TLabel
        Left = 306
        Visible = False
        ExplicitLeft = 306
      end
      inherited lbllb10: TLabel
        Top = 161
        ExplicitTop = 161
      end
      inherited lbllb7: TLabel
        Width = 9
        Height = 14
        ExplicitWidth = 9
        ExplicitHeight = 14
      end
      inherited lb4: TLabel
        Top = 140
        ExplicitTop = 140
      end
      inherited sbBuscarDadosDinheiro: TImage
        Left = 465
        Visible = False
        ExplicitLeft = 465
      end
      inherited sbBuscarDadosCheques: TImage
        Left = 465
        Visible = False
        ExplicitLeft = 465
      end
      inherited sbBuscarDadosCartoesDebito: TImage
        Top = 116
        ExplicitTop = 116
      end
      inherited sbBuscarDadosCartoesCredito: TImage
        Top = 137
        ExplicitTop = 137
      end
      inherited sbBuscarDadosCobranca: TImage
        Left = 465
        Visible = False
        ExplicitLeft = 465
      end
      inherited sbBuscarDadosCreditos: TImage
        Left = 465
        Visible = False
        ExplicitLeft = 465
      end
      inherited eValorDinheiro: TEditLuka
        Left = 368
        Height = 22
        Visible = False
        ExplicitLeft = 368
        ExplicitHeight = 22
      end
      inherited eValorCheque: TEditLuka
        Left = 368
        Height = 22
        Visible = False
        ExplicitLeft = 368
        ExplicitHeight = 22
      end
      inherited eValorCartaoDebito: TEditLuka
        Top = 112
        Height = 22
        ExplicitTop = 112
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Left = 368
        Height = 22
        Visible = False
        ExplicitLeft = 368
        ExplicitHeight = 22
      end
      inherited eValorCredito: TEditLuka
        Left = 368
        Height = 22
        Visible = False
        ExplicitLeft = 368
        ExplicitHeight = 22
      end
      inherited stPagamento: TStaticText
        Top = 95
        ExplicitTop = 95
      end
      inherited eValorDiferencaPagamentos: TEditLuka
        Top = 154
        Height = 22
        ExplicitTop = 154
        ExplicitHeight = 22
      end
      inherited eValorDesconto: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited ePercentualDesconto: TEditLuka
        Left = 193
        Top = 51
        Height = 22
        ExplicitLeft = 193
        ExplicitTop = 51
        ExplicitHeight = 22
      end
      inherited eValorJuros: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCartaoCredito: TEditLuka
        Top = 133
        Height = 22
        OnChange = nil
        ExplicitTop = 133
        ExplicitHeight = 22
      end
      inherited stValorTotal: TStaticText
        Top = 95
        ExplicitTop = 95
      end
      inherited stDinheiroDefinido: TStaticText
        Left = 484
        Visible = False
        ExplicitLeft = 484
      end
      inherited stChequeDefinido: TStaticText
        Left = 484
        Visible = False
        ExplicitLeft = 484
      end
      inherited stCartaoDebitoDefinido: TStaticText
        Top = 117
        ExplicitTop = 117
      end
      inherited stCobrancaDefinido: TStaticText
        Left = 484
        Visible = False
        ExplicitLeft = 484
      end
      inherited stCreditoDefinido: TStaticText
        Left = 484
        Visible = False
        ExplicitLeft = 484
      end
      inherited stCartaoCreditoDefinido: TStaticText
        Top = 138
        ExplicitTop = 138
      end
      inherited eValorMulta: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
  end
end
