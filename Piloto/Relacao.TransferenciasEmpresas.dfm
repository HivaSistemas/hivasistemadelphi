inherited FormRelacaoTransferenciasEmpresas: TFormRelacaoTransferenciasEmpresas
  Caption = 'Relat'#243'rio de transfer'#234'ncias de empresas'
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Visible = False
    end
  end
  inherited pcDados: TPageControl
    ActivePage = tsResultado
    inherited tsFiltros: TTabSheet
      inline FrEmpresasOrigem: TFrEmpresas
        Left = 1
        Top = 0
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 111
            Caption = 'Empresas de origem'
            ExplicitWidth = 111
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrDataCadastro: TFrDataInicialFinal
        Left = 416
        Top = 0
        Width = 217
        Height = 37
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 416
        ExplicitWidth = 217
        ExplicitHeight = 37
        inherited Label1: TLabel
          Width = 93
          Height = 14
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrCodigoTransferenciaEmpresas: TFrameInteiros
        Left = 416
        Top = 238
        Width = 187
        Height = 68
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 238
        ExplicitWidth = 187
        ExplicitHeight = 68
        inherited sgInteiros: TGridLuka
          Top = 19
          Width = 187
          Height = 49
          ExplicitTop = 19
          ExplicitWidth = 187
          ExplicitHeight = 49
          ColWidths = (
            64)
        end
        inherited Panel1: TPanel
          Width = 187
          Height = 19
          Caption = 'C'#243'digo da transfer'#234'ncia'
          ExplicitWidth = 187
          ExplicitHeight = 19
        end
      end
      inline FrUsuarioInsercao: TFrFuncionarios
        Left = 0
        Top = 262
        Width = 404
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitTop = 262
        ExplicitWidth = 404
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 381
          Height = 23
          ExplicitWidth = 381
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 404
          ExplicitWidth = 404
          inherited lbNomePesquisa: TLabel
            Width = 110
            Caption = 'Usu'#225'rio de inser'#231#227'o'
            ExplicitWidth = 110
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 299
            ExplicitLeft = 299
          end
        end
        inherited pnPesquisa: TPanel
          Left = 381
          Width = 23
          Height = 24
          ExplicitLeft = 381
          ExplicitWidth = 23
          ExplicitHeight = 24
          inherited sbPesquisa: TSpeedButton
            Left = -1
            ExplicitLeft = -1
          end
        end
      end
      inline FrEmpresasDestino: TFrEmpresas
        Left = 1
        Top = 88
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 88
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 114
            Caption = 'Empresas de destino'
            ExplicitWidth = 114
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 0
        Top = 176
        Width = 404
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitTop = 176
        ExplicitWidth = 404
        inherited sgPesquisa: TGridLuka
          Width = 379
          ExplicitWidth = 379
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 404
          ExplicitWidth = 404
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 299
            ExplicitLeft = 299
          end
        end
        inherited pnPesquisa: TPanel
          Left = 379
          ExplicitLeft = 379
        end
        inherited ckFiltroExtra: TCheckBox
          Left = 145
          Top = 61
          Checked = True
          State = cbChecked
          ExplicitLeft = 145
          ExplicitTop = 61
        end
      end
      inline FrDataBaixa: TFrDataInicialFinal
        Left = 416
        Top = 40
        Width = 217
        Height = 37
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 6
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 40
        ExplicitWidth = 217
        ExplicitHeight = 37
        inherited Label1: TLabel
          Width = 76
          Height = 14
          Caption = 'Data de baixa'
          ExplicitWidth = 76
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrUsuarioBaixa: TFrFuncionarios
        Left = 0
        Top = 310
        Width = 404
        Height = 39
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 7
        TabStop = True
        ExplicitTop = 310
        ExplicitWidth = 404
        ExplicitHeight = 39
        inherited sgPesquisa: TGridLuka
          Width = 381
          Height = 22
          ExplicitWidth = 381
          ExplicitHeight = 22
        end
        inherited PnTitulos: TPanel
          Width = 404
          ExplicitWidth = 404
          inherited lbNomePesquisa: TLabel
            Width = 93
            Caption = 'Usu'#225'rio de baixa'
            ExplicitWidth = 93
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 299
            ExplicitLeft = 299
          end
        end
        inherited pnPesquisa: TPanel
          Left = 381
          Width = 23
          Height = 23
          ExplicitLeft = 381
          ExplicitWidth = 23
          ExplicitHeight = 23
          inherited sbPesquisa: TSpeedButton
            Left = -1
            ExplicitLeft = -1
          end
        end
      end
      inline FrDataCancelamento: TFrDataInicialFinal
        Left = 416
        Top = 80
        Width = 217
        Height = 37
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 8
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 80
        ExplicitWidth = 217
        ExplicitHeight = 37
        inherited Label1: TLabel
          Width = 123
          Height = 14
          Caption = 'Data de cancelamento'
          ExplicitWidth = 123
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrUsuarioCancelamento: TFrFuncionarios
        Left = 0
        Top = 358
        Width = 404
        Height = 39
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 9
        TabStop = True
        ExplicitTop = 358
        ExplicitWidth = 404
        ExplicitHeight = 39
        inherited sgPesquisa: TGridLuka
          Width = 381
          Height = 22
          ExplicitWidth = 381
          ExplicitHeight = 22
        end
        inherited PnTitulos: TPanel
          Width = 404
          ExplicitWidth = 404
          inherited lbNomePesquisa: TLabel
            Width = 140
            Caption = 'Usu'#225'rio de cancelamento'
            ExplicitWidth = 140
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 299
            ExplicitLeft = 299
          end
        end
        inherited pnPesquisa: TPanel
          Left = 381
          Width = 23
          Height = 23
          ExplicitLeft = 381
          ExplicitWidth = 23
          ExplicitHeight = 23
          inherited sbPesquisa: TSpeedButton
            Left = -1
            ExplicitLeft = -1
          end
        end
      end
      object gbStatus: TGroupBoxLuka
        Left = 416
        Top = 124
        Width = 187
        Height = 109
        Caption = ' Status  '
        TabOrder = 10
        OpcaoMarcarDesmarcar = False
        object ckAbertos: TCheckBoxLuka
          Left = 10
          Top = 16
          Width = 111
          Height = 17
          Caption = 'Ag. carregamento'
          Checked = True
          State = cbChecked
          TabOrder = 0
          CheckedStr = 'S'
          Valor = 'A'
        end
        object ckBaixados: TCheckBoxLuka
          Left = 10
          Top = 39
          Width = 84
          Height = 17
          Caption = 'Baixados'
          Checked = True
          State = cbChecked
          TabOrder = 1
          CheckedStr = 'S'
          Valor = 'B'
        end
        object ckCancelados: TCheckBoxLuka
          Left = 10
          Top = 62
          Width = 84
          Height = 17
          Caption = 'Cancelada'
          Checked = True
          State = cbChecked
          TabOrder = 2
          CheckedStr = 'S'
          Valor = 'C'
        end
        object CheckBoxLuka1: TCheckBoxLuka
          Left = 10
          Top = 85
          Width = 84
          Height = 17
          Caption = 'Em tr'#226'nsito'
          Checked = True
          State = cbChecked
          TabOrder = 3
          CheckedStr = 'S'
          Valor = 'T'
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object spSeparador: TSplitter
        Left = 0
        Top = 212
        Width = 884
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ResizeStyle = rsUpdate
      end
      object sgTransferencias: TGridLuka
        Left = 0
        Top = 17
        Width = 884
        Height = 195
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 12
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 0
        OnClick = sgTransferenciasClick
        OnDblClick = sgTransferenciasDblClick
        OnDrawCell = sgTransferenciasDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Tranfer'#234'ncia'
          'Status'
          'Empresa de origem'
          'Empresa de destino'
          'Motorista'
          'Valor de transfer'#234'ncia'
          'Usu'#225'rio de inser'#231#227'o'
          'Data/ hora cadastro'
          'Usu'#225'rio de baixa'
          'Data/ hora baixa'
          'Usu'#225'rio de cancelamento'
          'Data/ hora cancelamento')
        OnGetCellColor = sgTransferenciasGetCellColor
        Grid3D = False
        RealColCount = 12
        OrdenarOnClick = True
        Indicador = True
        AtivarPopUpSelecao = False
        ExplicitLeft = 2
        ColWidths = (
          75
          119
          129
          126
          135
          131
          135
          121
          115
          112
          147
          150)
      end
      object lbTransferencia: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Transfer'#234'ncias'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 0
        Top = 218
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Itens da transfer'#234'ncia'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 235
        Width = 884
        Height = 283
        Align = alClient
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 3
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Local de origem'
          'Lote'
          'Preco unit'#225'rio'
          'Quantidade'
          'Valor total')
        Grid3D = False
        RealColCount = 10
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          178
          142
          125
          64
          95
          89
          107)
      end
    end
  end
end
