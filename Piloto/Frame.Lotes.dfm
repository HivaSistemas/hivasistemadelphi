inherited FrameLotes: TFrameLotes
  Width = 287
  Height = 41
  ExplicitWidth = 287
  ExplicitHeight = 41
  object lbLote: TLabel
    Left = 0
    Top = 0
    Width = 21
    Height = 13
    Caption = 'Lote'
  end
  object lbDataFabricacao: TLabel
    Left = 133
    Top = 0
    Width = 68
    Height = 13
    Caption = 'Dt. fabrica'#231#227'o'
  end
  object lbDataVencimento: TLabel
    Left = 212
    Top = 0
    Width = 55
    Height = 13
    Caption = 'Dt. vencto.'
  end
  object lbBitola: TLabel
    Left = 0
    Top = 0
    Width = 26
    Height = 13
    Caption = 'Bitola'
    Visible = False
  end
  object lbTonalidade: TLabel
    Left = 133
    Top = 0
    Width = 52
    Height = 13
    Caption = 'Tonalidade'
    Visible = False
  end
  object eLote: TEditLuka
    Left = 0
    Top = 15
    Width = 128
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 80
    TabOrder = 0
    OnKeyDown = ProximoCampo
    OnKeyPress = LimparEspaco
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataFabricacao: TEditLukaData
    Left = 133
    Top = 15
    Width = 75
    Height = 21
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object eDataVencimento: TEditLukaData
    Left = 212
    Top = 15
    Width = 75
    Height = 21
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object eBitola: TEditLuka
    Left = 0
    Top = 15
    Width = 128
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 20
    TabOrder = 3
    Visible = False
    OnExit = eBitolaExit
    OnKeyDown = ProximoCampo
    OnKeyPress = LimparEspaco
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eTonalidade: TEditLuka
    Left = 133
    Top = 15
    Width = 154
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 80
    TabOrder = 4
    Visible = False
    OnExit = eTonalidadeExit
    OnKeyDown = ProximoCampo
    OnKeyPress = LimparEspaco
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
