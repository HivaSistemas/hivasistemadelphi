inherited FormBaixarTitulosPagar: TFormBaixarTitulosPagar
  Caption = 'Baixa de t'#237'tulos a pagar'
  ClientHeight = 427
  ClientWidth = 806
  OnShow = FormShow
  ExplicitWidth = 812
  ExplicitHeight = 456
  PixelsPerInch = 96
  TextHeight = 14
  object lb8: TLabel [0]
    Left = 5
    Top = 139
    Width = 80
    Height = 14
    Caption = 'Data de pagto.'
  end
  object lb22: TLabel [1]
    Left = 95
    Top = 139
    Width = 89
    Height = 14
    Caption = 'Valor dos t'#237'tulos'
  end
  object lb23: TLabel [2]
    Left = 200
    Top = 139
    Width = 87
    Height = 14
    Caption = 'Valor descontos'
  end
  object lb1: TLabel [3]
    Left = 306
    Top = 139
    Width = 97
    Height = 14
    Caption = 'Valor total dos tit.'
  end
  object lb2: TLabel [4]
    Left = 410
    Top = 139
    Width = 69
    Height = 14
    Caption = 'Observa'#231#245'es'
  end
  inherited pnOpcoes: TPanel
    Top = 390
    Width = 806
    TabOrder = 7
    ExplicitTop = 390
    ExplicitWidth = 806
  end
  object eDataPagamento: TEditLukaData
    Left = 5
    Top = 153
    Width = 85
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object eValorTitulos: TEditLuka
    Left = 95
    Top = 153
    Width = 100
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorDescontos: TEditLuka
    Left = 200
    Top = 153
    Width = 100
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorTotal: TEditLuka
    Left = 305
    Top = 153
    Width = 100
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 4
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eObservacoes: TEditLuka
    Left = 410
    Top = 153
    Width = 393
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 5
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object sgTitulos: TGridLuka
    Left = -3
    Top = -2
    Width = 801
    Height = 135
    Align = alCustom
    ColCount = 9
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 0
    OnDblClick = sgTitulosDblClick
    OnDrawCell = sgTitulosDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Documento'
      'Cliente'
      'Parcela'
      'Valor'
      'Desconto'
      'Valor total'
      'Data Vencto.'
      'Dias atraso'
      'Tipo de cobran'#231'a')
    OnGetCellColor = sgTitulosGetCellColor
    Grid3D = False
    RealColCount = 15
    OrdenarOnClick = True
    AtivarPopUpSelecao = False
    ColWidths = (
      89
      153
      48
      62
      61
      79
      84
      68
      134)
  end
  inline FrPagamento: TFrPagamentoFinanceiro
    Left = 4
    Top = 178
    Width = 303
    Height = 207
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 4
    ExplicitTop = 178
    ExplicitHeight = 207
    inherited grpFechamento: TGroupBox
      Height = 207
      ExplicitHeight = 207
      inherited lbllb11: TLabel
        Top = 102
        ExplicitTop = 102
      end
      inherited lbllb12: TLabel
        Left = 46
        Top = 124
        ExplicitLeft = 46
        ExplicitTop = 124
      end
      inherited lbllb13: TLabel
        Left = 312
        Visible = False
        ExplicitLeft = 312
      end
      inherited lbllb14: TLabel
        Top = 144
        ExplicitTop = 144
      end
      inherited lbllb15: TLabel
        Top = 164
        ExplicitTop = 164
      end
      inherited lbllb10: TLabel
        Top = 184
        ExplicitTop = 184
      end
      inherited lbllb21: TLabel
        Top = 40
        ExplicitTop = 40
      end
      inherited lbllb7: TLabel
        Left = 236
        Top = 40
        Width = 9
        Height = 14
        Anchors = [akLeft, akTop, akBottom]
        ExplicitLeft = 236
        ExplicitTop = 40
        ExplicitWidth = 9
        ExplicitHeight = 14
      end
      inherited lbllb19: TLabel
        Top = 61
        ExplicitTop = 61
      end
      inherited lbllb1: TLabel
        Top = 20
        ExplicitTop = 20
      end
      inherited lb4: TLabel
        Left = 304
        Visible = False
        ExplicitLeft = 304
      end
      inherited lb1: TLabel
        Left = 307
        Visible = False
        ExplicitLeft = 307
      end
      inherited sbBuscarDadosDinheiro: TImage
        Top = 98
        Visible = True
        ExplicitTop = 98
      end
      inherited sbBuscarDadosCheques: TImage
        Top = 119
        ExplicitTop = 119
      end
      inherited sbBuscarDadosCartoesDebito: TImage
        Left = 481
        Visible = False
        ExplicitLeft = 481
      end
      inherited sbBuscarDadosCartoesCredito: TImage
        Left = 481
        Visible = False
        OnClick = nil
        ExplicitLeft = 481
      end
      inherited sbBuscarDadosCobranca: TImage
        Top = 141
        ExplicitTop = 141
      end
      inherited sbBuscarDadosCreditos: TImage
        Top = 161
        ExplicitTop = 161
      end
      inherited eValorDinheiro: TEditLuka
        Top = 95
        Height = 22
        ExplicitTop = 95
        ExplicitHeight = 22
      end
      inherited eValorCheque: TEditLuka
        Top = 116
        Height = 22
        ExplicitTop = 116
        ExplicitHeight = 22
      end
      inherited eValorCartaoDebito: TEditLuka
        Left = 384
        Height = 22
        Visible = False
        ExplicitLeft = 384
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Top = 137
        Height = 22
        ExplicitTop = 137
        ExplicitHeight = 22
      end
      inherited eValorCredito: TEditLuka
        Top = 157
        Height = 22
        ExplicitTop = 157
        ExplicitHeight = 22
      end
      inherited stPagamento: TStaticText
        Top = 78
        ExplicitTop = 78
      end
      inherited eValorDiferencaPagamentos: TEditLuka
        Top = 177
        Height = 22
        ExplicitTop = 177
        ExplicitHeight = 22
      end
      inherited eValorDesconto: TEditLuka
        Top = 33
        Height = 22
        ExplicitTop = 33
        ExplicitHeight = 22
      end
      inherited ePercentualDesconto: TEditLuka
        Left = 194
        Top = 33
        Height = 22
        ExplicitLeft = 194
        ExplicitTop = 33
        ExplicitHeight = 22
      end
      inherited eValorTotalASerPago: TEditLuka
        Top = 53
        ExplicitTop = 53
      end
      inherited eValorJuros: TEditLuka
        Top = 13
        Height = 22
        ExplicitTop = 13
        ExplicitHeight = 22
      end
      inherited eValorCartaoCredito: TEditLuka
        Left = 384
        Height = 22
        Visible = False
        OnChange = nil
        OnKeyDown = nil
        ExplicitLeft = 384
        ExplicitHeight = 22
      end
      inherited stValorTotal: TStaticText
        Top = 78
        ExplicitTop = 78
      end
      inherited stDinheiroDefinido: TStaticText
        Top = 99
        Visible = True
        ExplicitTop = 99
      end
      inherited stChequeDefinido: TStaticText
        Top = 120
        ExplicitTop = 120
      end
      inherited stCartaoDebitoDefinido: TStaticText
        Left = 500
        Visible = False
        ExplicitLeft = 500
      end
      inherited stCobrancaDefinido: TStaticText
        Top = 141
        ExplicitTop = 141
      end
      inherited stCreditoDefinido: TStaticText
        Top = 161
        ExplicitTop = 161
      end
      inherited stCartaoCreditoDefinido: TStaticText
        Left = 500
        Visible = False
        ExplicitLeft = 500
      end
      inherited eValorMulta: TEditLuka
        Left = 408
        Height = 22
        Visible = False
        ExplicitLeft = 408
        ExplicitHeight = 22
      end
      inherited eValorPix: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
  end
end
