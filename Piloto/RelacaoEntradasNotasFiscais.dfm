inherited FormRelacaoEntradaNotasFiscais: TFormRelacaoEntradaNotasFiscais
  Caption = 'Rela'#231#227'o de entradas de notas fiscais'
  ClientHeight = 621
  OnShow = FormShow
  ExplicitHeight = 650
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 621
    ExplicitHeight = 621
    inherited sbImprimir: TSpeedButton
      Top = 57
      ExplicitTop = 57
    end
    object miDesconsolidarEstoque: TSpeedButton [2]
      Left = 0
      Top = 123
      Width = 117
      Height = 35
      Caption = 'Voltar entrada'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      NumGlyphs = 2
      ParentFont = False
      OnClick = miDesconsolidarEstoqueClick
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Top = 256
      ExplicitTop = 256
    end
  end
  inherited pcDados: TPageControl
    Height = 621
    ActivePage = tsResultado
    ExplicitHeight = 621
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 592
      object lb1: TLabel
        Left = 348
        Top = 302
        Width = 73
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Agrupamento'
      end
      inline FrEmpresas: TFrEmpresas
        Left = 3
        Top = 2
        Width = 302
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 2
        ExplicitWidth = 302
        inherited sgPesquisa: TGridLuka
          Width = 299
          ExplicitWidth = 299
        end
        inherited PnTitulos: TPanel
          Width = 302
          ExplicitWidth = 302
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 197
            ExplicitLeft = 197
            inherited ckSuprimir: TCheckBox
              Left = 44
              Width = 58
              ExplicitLeft = 44
              ExplicitWidth = 58
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 299
          Width = 3
          Visible = False
          ExplicitLeft = 299
          ExplicitWidth = 3
          inherited sbPesquisa: TSpeedButton
            Left = -21
            ExplicitLeft = -21
          end
        end
      end
      inline FrFornecedores: TFrFornecedores
        Left = 3
        Top = 87
        Width = 306
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 87
        ExplicitWidth = 306
        inherited sgPesquisa: TGridLuka
          Width = 303
          ExplicitWidth = 303
        end
        inherited PnTitulos: TPanel
          Width = 306
          ExplicitWidth = 306
          inherited lbNomePesquisa: TLabel
            Width = 74
            ExplicitWidth = 74
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 201
            ExplicitLeft = 201
            inherited ckSuprimir: TCheckBox
              Left = 40
              Width = 61
              ExplicitLeft = 40
              ExplicitWidth = 61
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 303
          Width = 3
          Visible = False
          ExplicitLeft = 303
          ExplicitWidth = 3
          inherited sbPesquisa: TSpeedButton
            Left = -21
            ExplicitLeft = -21
          end
        end
      end
      inline FrDataContabil: TFrDataInicialFinal
        Left = 658
        Top = 376
        Width = 190
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        Visible = False
        ExplicitLeft = 658
        ExplicitTop = 376
        ExplicitWidth = 190
        inherited Label1: TLabel
          Width = 74
          Height = 14
          Caption = 'Data cont'#225'bil'
          ExplicitWidth = 74
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDataCadastro: TFrDataInicialFinal
        Left = 348
        Top = 2
        Width = 207
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 348
        ExplicitTop = 2
        ExplicitWidth = 207
        inherited Label1: TLabel
          Width = 111
          Height = 14
          Caption = 'Data de lan'#231'amento'
          ExplicitWidth = 111
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Left = 116
          Height = 22
          ExplicitLeft = 116
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Left = 0
          Height = 22
          ExplicitLeft = 0
          ExplicitHeight = 22
        end
      end
      inline FrNumeroNota: TFrNumeros
        Left = 348
        Top = 120
        Width = 109
        Height = 72
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 348
        ExplicitTop = 120
        ExplicitWidth = 109
        inherited sgNumeros: TGridLuka
          Width = 109
          ExplicitWidth = 109
        end
        inherited pnDescricao: TPanel
          Width = 109
          Caption = 'N'#250'mero da nota'
          ExplicitWidth = 109
        end
      end
      inline FrCodigoEntrada: TFrNumeros
        Left = 348
        Top = 216
        Width = 109
        Height = 72
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 6
        TabStop = True
        ExplicitLeft = 348
        ExplicitTop = 216
        ExplicitWidth = 109
        inherited sgNumeros: TGridLuka
          Width = 109
          ExplicitWidth = 109
        end
        inherited pnDescricao: TPanel
          Width = 109
          Caption = 'C'#243'digo da entrada'
          ExplicitWidth = 109
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 3
        Top = 172
        Width = 305
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 172
        ExplicitWidth = 305
        inherited sgPesquisa: TGridLuka
          Width = 302
          ExplicitWidth = 302
        end
        inherited PnTitulos: TPanel
          Width = 305
          ExplicitWidth = 305
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 200
            ExplicitLeft = 200
            inherited ckSuprimir: TCheckBox
              Left = 41
              Top = 1
              Width = 59
              ExplicitLeft = 41
              ExplicitTop = 1
              ExplicitWidth = 59
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 302
          Width = 3
          Visible = False
          ExplicitLeft = 302
          ExplicitWidth = 3
          inherited sbPesquisa: TSpeedButton
            Left = -21
            ExplicitLeft = -21
          end
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      object cbAgrupamento: TComboBoxLuka
        Left = 348
        Top = 316
        Width = 141
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 7
        Text = 'Nenhum'
        Items.Strings = (
          'Nenhum'
          'Fornecedor'
          'Grupo de fornecedor')
        Valores.Strings = (
          'NEN'
          'FOR'
          'GRU')
        AsInt = 0
        AsString = 'NEN'
      end
      inline FrPlanosFinanceiros: TFrPlanosFinanceiros
        Left = 3
        Top = 257
        Width = 308
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 8
        TabStop = True
        ExplicitLeft = 3
        ExplicitTop = 257
        ExplicitWidth = 308
        inherited sgPesquisa: TGridLuka
          Width = 305
          ExplicitWidth = 305
        end
        inherited PnTitulos: TPanel
          Width = 308
          ExplicitWidth = 308
          inherited lbNomePesquisa: TLabel
            Width = 100
            ExplicitWidth = 100
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 203
            ExplicitLeft = 203
            inherited ckSuprimir: TCheckBox
              Left = 38
              Top = -1
              Width = 51
              ExplicitLeft = 38
              ExplicitTop = -1
              ExplicitWidth = 51
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 305
          Width = 3
          Visible = False
          ExplicitLeft = 305
          ExplicitWidth = 3
          inherited sbPesquisa: TSpeedButton
            Left = -21
            ExplicitLeft = -21
          end
        end
        inherited ckSomenteNivel3: TCheckBox
          Width = 129
          ExplicitWidth = 129
        end
        inherited poOpcoes: TPopupMenu
          Left = 304
        end
      end
      inline FrDataEmissao: TFrDataInicialFinal
        Left = 348
        Top = 58
        Width = 207
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 9
        TabStop = True
        ExplicitLeft = 348
        ExplicitTop = 58
        ExplicitWidth = 207
        inherited Label1: TLabel
          Width = 92
          Height = 14
          Caption = 'Data de Emiss'#227'o'
          ExplicitWidth = 92
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Left = 116
          Height = 22
          ExplicitLeft = 116
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Left = 0
          Height = 22
          ExplicitLeft = 0
          ExplicitHeight = 22
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 592
      object splSeparador: TSplitter
        Left = 0
        Top = 279
        Width = 884
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ResizeStyle = rsUpdate
      end
      object sgEntradas: TGridLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 279
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 17
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        PopupMenu = pmOpcoes
        TabOrder = 0
        OnClick = sgEntradasClick
        OnDblClick = sgEntradasDblClick
        OnDrawCell = sgEntradasDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Entrada'
          'Fornecedor'
          'Status'
          'Numero nota'
          'Valor total nota'
          'Data lan'#231'amento'
          'Data cont'#225'bil'
          'CFOP'
          'Base ICMS'
          'Valor ICMS'
          'Base ICMS ST'
          'Valor ICMS ST'
          'Valor IPI'
          'Valor PIS'
          'Valor COFINS'
          'Empresa'
          'Usu'#225'rio cadastro')
        OnGetCellColor = sgEntradasGetCellColor
        Grid3D = False
        RealColCount = 17
        OrdenarOnClick = True
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          61
          167
          187
          80
          92
          100
          79
          43
          73
          64
          77
          78
          55
          56
          76
          164
          167)
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 285
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Itens da entrada selecinonada'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 302
        Width = 884
        Height = 290
        Align = alClient
        ColCount = 27
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 2
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'CST'
          'Pre'#231'o unit.'
          'Quantidade'
          'Qtde.emba.'
          'Und.'
          'Valor desc.'
          'Outras desp.'
          'Valor total'
          'Base c'#225'lc.ICMS'
          #205'nd.red.ICMS'
          '% ICMS'
          'Valor ICMS'
          'Base c'#225'lc.ICMS ST'
          #205'nd.red.ICMS ST'
          'Valor ICMS ST'
          'CST PIS'
          '% PIS'
          'Valor PIS'
          'CST COFINS'
          '% COFINS'
          'Valor COFINS'
          '% IVA'
          'Pre'#231'o pauta'
          '% IPI'
          'Valor IPI')
        Grid3D = False
        RealColCount = 27
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          190
          40
          66
          72
          68
          31
          66
          78
          72
          87
          76
          43
          67
          100
          91
          80
          43
          35
          60
          65
          56
          74
          36
          69
          39
          64)
      end
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 952
    Top = 192
  end
  object frxSintetico: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 45079.909893865700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 232
    Top = 168
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstPagar
        DataSetName = 'frxdstPagar'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Top = 70.811070000000000000
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133890000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000000000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000000000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133890000000000000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133890000000000000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 2.779530000000000000
          Top = 70.811070000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Entrada')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 49.133890000000000000
          Top = 70.811070000000000000
          Width = 355.275820000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Fornecedor')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 656.976810000000000000
          Top = 70.811070000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo5: TfrxMemoView
          Left = 408.968770000000000000
          Top = 70.811070000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'N'#250'mero da nota')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 495.677490000000000000
          Top = 70.811070000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Total')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 567.488560000000000000
          Top = 70.811070000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Data lan'#231'amento')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 22.677165350000000000
        Top = 166.299320000000000000
        Width = 718.110700000000000000
        DataSet = dstPagar
        DataSetName = 'frxdstPagar'
        RowCount = 0
        object frxdstTranscodigoProd: TfrxMemoView
          Left = 2.779530000000000000
          Top = 1.000000000000000000
          Width = 45.354330710000000000
          Height = 18.897637795275600000
          DataField = 'Entrada'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Entrada"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo15: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 49.133858270000000000
          Top = 1.000000000000000000
          Width = 359.055320710000000000
          Height = 18.897637800000000000
          StretchMode = smMaxHeight
          DataField = 'Fornecedor'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Fornecedor"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo22: TfrxMemoView
          Left = 412.724516850000000000
          Top = 1.000000000000000000
          Width = 83.149606300000000000
          Height = 18.897637800000000000
          DataField = 'NumeroNota'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstPagar."NumeroNota"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo23: TfrxMemoView
          Left = 499.275697950000000000
          Top = 1.000000000000000000
          Width = 68.031496060000000000
          Height = 18.897637800000000000
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstPagar."ValorTotalNota"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 571.086721570000000000
          Top = 1.000000000000000000
          Width = 86.929133860000000000
          Height = 18.897637795275600000
          DataField = 'DataLancamento'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstPagar."DataLancamento"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo26: TfrxMemoView
          Left = 660.661524720000000000
          Top = 1.000000000000000000
          Width = 37.795275590551200000
          Height = 18.897637795275600000
          DataField = 'CFOP'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."CFOP"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 41.952782760000000000
        Top = 249.448980000000000000
        Width = 718.110700000000000000
        object Line1: TfrxLineView
          ShiftMode = smWhenOverlapped
          Top = 3.779530000000000000
          Width = 718.110700000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object mmValorProduto: TfrxMemoView
          Left = 3.779530000000000000
          Top = 11.338590000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Valor total.....: ')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 313.700990000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
    end
  end
  object dstPagar: TfrxDBDataset
    UserName = 'frxdstPagar'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Entrada=Entrada'
      'Fornecedor=Fornecedor'
      'NumeroNota=NumeroNota'
      'CFOP=CFOP'
      'DataLancamento=DataLancamento'
      'Empresa=Empresa'
      'Usuario=Usuario'
      'ValorTotalNota=ValorTotalNota')
    DataSet = cdsPagar
    BCDToCurrency = False
    Left = 229
    Top = 220
  end
  object cdsPagar: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Entrada'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Fornecedor'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'NumeroNota'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ValorTotalNota'
        Attributes = [faUnNamed]
        DataType = ftFloat
      end
      item
        Name = 'DataLancamento'
        Attributes = [faUnNamed]
        DataType = ftDate
      end
      item
        Name = 'CFOP'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Empresa'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Usuario'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 60
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 228
    Top = 267
    object cdsPagarEntrada: TStringField
      FieldName = 'Entrada'
    end
    object cdsPagarFornecedor: TStringField
      FieldName = 'Fornecedor'
      Size = 60
    end
    object cdsPagarNumeroNota: TStringField
      FieldName = 'NumeroNota'
    end
    object cdsPagarCFOP: TStringField
      FieldName = 'CFOP'
    end
    object cdsPagarDataLancamento: TDateField
      FieldName = 'DataLancamento'
    end
    object cdsPagarEmpresa: TStringField
      FieldName = 'Empresa'
      Size = 60
    end
    object cdsPagarUsuario: TStringField
      FieldName = 'Usuario'
      Size = 60
    end
    object cdsPagarValorTotal: TFloatField
      FieldName = 'ValorTotalNota'
    end
  end
  object frxAnalitico: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 45079.909893865700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 552
    Top = 104
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dsItensEntrada
        DataSetName = 'frxdstItensProduto'
      end
      item
        DataSet = dstPagar
        DataSetName = 'frxdstPagar'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 71.811070000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133890000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000000000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000000000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133890000000000000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133890000000000000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Line2: TfrxLineView
          Top = 68.031540000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 44.133890000000000000
        Top = 196.535560000000000000
        Width = 718.110700000000000000
        DataSet = dstPagar
        DataSetName = 'frxdstPagar'
        RowCount = 0
        object frxdstTranscodigoProd: TfrxMemoView
          Left = 2.779530000000000000
          Top = 1.000000000000000000
          Width = 45.354330710000000000
          Height = 18.897637800000000000
          DataField = 'Entrada'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Entrada"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo15: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 51.133858270000000000
          Top = 1.000000000000000000
          Width = 370.393910710000000000
          Height = 18.897637800000000000
          StretchMode = smMaxHeight
          DataField = 'Fornecedor'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."Fornecedor"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo22: TfrxMemoView
          Left = 428.645950000000000000
          Top = 1.000000000000000000
          Width = 83.149606300000000000
          Height = 18.897637800000000000
          DataField = 'NumeroNota'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."NumeroNota"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo23: TfrxMemoView
          Left = 515.354713940000000000
          Top = 1.000000000000000000
          Width = 68.031496060000000000
          Height = 18.897637800000000000
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstPagar."ValorTotalNota"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 587.165740000000000000
          Top = 1.000000000000000000
          Width = 86.929133860000000000
          Height = 18.897637800000000000
          DataField = 'DataLancamento'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."DataLancamento"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo26: TfrxMemoView
          Left = 676.653990000000000000
          Top = 1.000000000000000000
          Width = 37.795275590000000000
          Height = 18.897637800000000000
          DataField = 'CFOP'
          DataSet = dstPagar
          DataSetName = 'frxdstPagar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstPagar."CFOP"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Subreport1: TfrxSubreport
          Top = 21.456710000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Page = frxAnalitico.Page2
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 41.952782760000000000
        Top = 302.362400000000000000
        Width = 718.110700000000000000
        object Line1: TfrxLineView
          ShiftMode = smWhenOverlapped
          Top = 3.779530000000000000
          Width = 718.110700000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object mmValorProduto: TfrxMemoView
          Left = 3.779530000000000000
          Top = 11.338590000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Valor total.....: ')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 366.614410000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 151.181200000000000000
        Width = 718.110700000000000000
        Condition = 'frxdstPagar."Entrada"'
        object Shape1: TfrxShapeView
          Left = 0.779530000000000000
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo18: TfrxMemoView
          Left = 4.559060000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Entrada')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 50.913420000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Fornecedor')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 676.653990000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'CFOP')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 428.645950000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'N'#250'mero da nota')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 515.354670000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Total')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 587.165740000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Data lan'#231'amento')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 60.472480000000000000
        Width = 718.110700000000000000
        DataSet = dsItensEntrada
        DataSetName = 'frxdstItensProduto'
        Filter = 
          'StrToInt(<frxdstPagar."Entrada">) = StrToInt(<frxdstItensProduto' +
          '."entradaId">)'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo41: TfrxMemoView
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DataSetName = 'dsItensEntrada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstItensProduto."produto"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 48.133890000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataSetName = 'dsItensEntrada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstItensProduto."nome"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 351.937230000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          DataSetName = 'dsItensEntrada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstItensProduto."CST"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 382.732530000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSetName = 'dsItensEntrada'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstItensProduto."prc_unitario"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 435.645950000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataSetName = 'dsItensEntrada'
          DisplayFormat.FormatStr = '000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstItensProduto."qtde"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 476.220780000000000000
          Width = 22.677180000000000000
          Height = 18.897650000000000000
          DataSetName = 'dsItensEntrada'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstItensProduto."unidade"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 505.677490000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DataSetName = 'dsItensEntrada'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstItensProduto."vlr_desc"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 576.488560000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DataSetName = 'dsItensEntrada'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstItensProduto."outras_despesas"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 636.961040000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataSetName = 'dsItensEntrada'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstItensProduto."vlr_total"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape3: TfrxShapeView
          ShiftMode = smDontShift
          Width = 718.110700000000000000
          Height = 15.118110240000000000
          Fill.BackColor = cl3DLight
        end
        object Memo29: TfrxMemoView
          ShiftMode = smDontShift
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          ShiftMode = smDontShift
          Left = 48.133890000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          ShiftMode = smDontShift
          Left = 351.937230000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'CST')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 435.645950000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          ShiftMode = smDontShift
          Left = 382.732530000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pr'#231'o Unit')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          ShiftMode = smWhenOverlapped
          Left = 476.220780000000000000
          Width = 26.456710000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Unid')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          ShiftMode = smDontShift
          Left = 505.677490000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr Desconto')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          ShiftMode = smDontShift
          Left = 576.488560000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Outras Desp.')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          ShiftMode = smDontShift
          Left = 648.299630000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr Total')
          ParentFont = False
        end
      end
    end
  end
  object cdsItensProduto: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'produto'
        Attributes = [faUnNamed]
        DataType = ftInteger
      end
      item
        Name = 'nome'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 60
      end
      item
        Name = 'CST'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 5
      end
      item
        Name = 'prc_unitario'
        Attributes = [faUnNamed]
        DataType = ftFloat
      end
      item
        Name = 'qtde'
        Attributes = [faUnNamed]
        DataType = ftFloat
      end
      item
        Name = 'unidade'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'vlr_desc'
        Attributes = [faUnNamed]
        DataType = ftFloat
      end
      item
        Name = 'outras_despesas'
        Attributes = [faUnNamed]
        DataType = ftFloat
      end
      item
        Name = 'vlr_total'
        Attributes = [faUnNamed]
        DataType = ftFloat
      end
      item
        Name = 'entradaId'
        Attributes = [faUnNamed]
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 552
    Top = 168
    object cdsItensProdutoproduto: TIntegerField
      FieldName = 'produto'
    end
    object cdsItensProdutonome: TStringField
      FieldName = 'nome'
      Size = 60
    end
    object cdsItensProdutoCST: TStringField
      FieldName = 'CST'
      Size = 5
    end
    object cdsItensProdutoprc_unitario: TFloatField
      FieldName = 'prc_unitario'
    end
    object cdsItensProdutounidade: TStringField
      FieldName = 'unidade'
    end
    object cdsItensProdutovlr_desc: TFloatField
      FieldName = 'vlr_desc'
    end
    object cdsItensProdutooutras_despesas: TFloatField
      FieldName = 'outras_despesas'
    end
    object cdsItensProdutovlr_total: TFloatField
      FieldName = 'vlr_total'
    end
    object cdsItensProdutoqtde: TFloatField
      FieldName = 'qtde'
    end
    object cdsItensProdutoentradaId: TStringField
      FieldName = 'entradaId'
    end
  end
  object dsItensEntrada: TfrxDBDataset
    UserName = 'frxdstItensProduto'
    CloseDataSource = False
    FieldAliases.Strings = (
      'produto=produto'
      'nome=nome'
      'CST=CST'
      'prc_unitario=prc_unitario'
      'unidade=unidade'
      'vlr_desc=vlr_desc'
      'outras_despesas=outras_despesas'
      'vlr_total=vlr_total'
      'qtde=qtde'
      'entradaId=entradaId')
    DataSet = cdsItensProduto
    BCDToCurrency = False
    Left = 557
    Top = 220
  end
end
