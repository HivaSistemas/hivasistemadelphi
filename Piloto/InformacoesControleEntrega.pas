unit InformacoesControleEntrega;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar,
  Vcl.Buttons, _Sessao, _Biblioteca, Vcl.ExtCtrls, _ControlesEntregas, Vcl.Mask, EditLukaData, Vcl.StdCtrls, EditLuka, Vcl.ComCtrls, Vcl.Grids, GridLuka,
  _Entregas, _RecordsExpedicao, _EntregasItens, Informacoes.Entrega, _ControlesEntregasAjudantes,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameFuncionarios;

type
  TFormInformacoesControleEntrega = class(TFormHerancaFinalizar)
    lb1: TLabel;
    lb2: TLabel;
    lb12: TLabel;
    eControle: TEditLuka;
    eVeiculo: TEditLuka;
    eMotorista: TEditLuka;
    eDataHoraGeracao: TEditLukaData;
    lb3: TLabel;
    pcDados: TPageControl;
    tsEntregas: TTabSheet;
    tsItens: TTabSheet;
    sgEntregas: TGridLuka;
    sgItens: TGridLuka;
    tsPrincipais: TTabSheet;
    eEmpresa: TEditLuka;
    lb6: TLabel;
    ePesoTotal: TEditLuka;
    lb5: TLabel;
    eQuantidadeProdutos: TEditLuka;
    eQuantidadeEntregas: TEditLuka;
    lb4: TLabel;
    lb13: TLabel;
    st3: TStaticText;
    stStatus: TStaticText;
    Label1: TLabel;
    eUsuarioCadastro: TEditLuka;
    Label2: TLabel;
    Label3: TLabel;
    eUsuarioBaixa: TEditLuka;
    Label4: TLabel;
    eDataHoraCadastro: TEditLukaData;
    eDataHoraBaixa: TEditLukaData;
    FrAjudantes: TFrFuncionarios;
    procedure FormCreate(Sender: TObject);
    procedure sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEntregasDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Informar(pControleId: Integer);

implementation

{$R *.dfm}

const
  (*  Grid de entregas *)
  ceEntregaId    = 0;
  ceDataGeracao  = 1;
  cePedidoId     = 2;
  ceCliente      = 3;
  cePrevEntrega  = 4;
  ceEmpresa      = 5;
  ceLocal        = 6;
  ceBairro       = 7;
  ceRota         = 8;
  cePesoTotal    = 9;
  ceQtdeProdutos = 10;

  (* Grid de itens *)
  coProdutoId  = 0;
  coNome       = 1;
  coMarca      = 2;
  coLote       = 3;
  coQuantidade = 4;
  coUnidade    = 5;
  coDevolvidos = 6;
  coRetornados = 7;

procedure Informar(pControleId: Integer);
var
  i: Integer;
  vControle: TArray<RecControlesEntregas>;
  vForm: TFormInformacoesControleEntrega;

  vEntregasIds: TArray<Integer>;

  vEntregas: TArray<RecEntrega>;
  vItens: TArray<RecEntregaItem>;
  vAjudantesIds: TArray<Integer>;
  vAjudantes: TArray<RecControlesEntregasAjudantes>;
begin
  if pControleId = 0 then
    Exit;

  vControle := _ControlesEntregas.BuscarControles(Sessao.getConexaoBanco, 0, [pControleId]);
  if vControle = nil then begin
    _Biblioteca.Exclamar('Controle de entrega n�o encontrado! Controle: ' + NFormat(pControleId, 0));
    Exit;
  end;

  vEntregas := _Entregas.BuscarEntregas( Sessao.getConexaoBanco, 2, [vControle[0].ControleEntregaId] );

  vForm := TFormInformacoesControleEntrega.Create(nil);

  vForm.eControle.AsInt             := vControle[0].ControleEntregaId;
  vForm.eDataHoraGeracao.AsDataHora := vControle[0].DataHoraCadastro;
  vForm.eVeiculo.SetInformacao( vControle[0].ControleEntregaId, vControle[0].NomeVeiculo );
  vForm.eMotorista.SetInformacao( vControle[0].MotoristaId, vControle[0].NomeMotorista );
  vForm.stStatus.Caption := vControle[0].StatusAnalitico;
  vForm.eEmpresa.SetInformacao( vControle[0].EmpresaId, vControle[0].NomeEmpresa );
  vForm.eQuantidadeEntregas.AsInt := vControle[0].QtdeEntregas;
  vForm.eQuantidadeProdutos.AsInt := vControle[0].QtdeProdutos;
  vForm.ePesoTotal.AsDouble := vControle[0].PesoTotal;
  vForm.eUsuarioCadastro.SetInformacao(vControle[0].UsuarioCadastroId, vControle[0].NomeUsuarioCadastro);
  vForm.eDataHoraCadastro.AsDataHora := vControle[0].DataHoraCadastro;
  vForm.eUsuarioBaixa.SetInformacao(vControle[0].UsuarioBaixaId, vControle[0].NomeUsuarioBaixa);
  vForm.eDataHoraBaixa.AsDataHora := vControle[0].DataHoraBaixa;

  vAjudantes := _ControlesEntregasAjudantes.BuscarControlesEntregasAjudantes(Sessao.getConexaoBanco, 0, [pControleId]);
  for i := Low(vAjudantes) to High(vAjudantes) do
    _Biblioteca.AddNoVetorSemRepetir(vAjudantesIds, vAjudantes[i].AjudanteId);

  vForm.FrAjudantes.InserirDadoPorChaveTodos(vAjudantesIds, False);

  vEntregasIds := nil;
  for i := Low(vEntregas) to High(vEntregas) do begin
    vForm.sgEntregas.Cells[ceEntregaId, i + 1]    := NFormat(vEntregas[i].EntregaId);
    vForm.sgEntregas.Cells[ceDataGeracao, i + 1]  := DFormat(vEntregas[i].DataHoraCadastro);
    vForm.sgEntregas.Cells[cePedidoId, i + 1]     := NFormat(vEntregas[i].OrcamentoId);
    vForm.sgEntregas.Cells[ceCliente, i + 1]      := NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;
    vForm.sgEntregas.Cells[cePrevEntrega, i + 1]  := DFormat(vEntregas[i].PrevisaoEntrega);
    vForm.sgEntregas.Cells[ceEmpresa, i + 1]      := NFormat(vEntregas[i].EmpresaId) + ' - ' + vEntregas[i].NomeEmpresa;
    vForm.sgEntregas.Cells[ceLocal, i + 1]        := NFormat(vEntregas[i].LocalId) + ' - ' + vEntregas[i].NomeLocal;
    vForm.sgEntregas.Cells[ceBairro, i + 1]       := NFormat(vEntregas[i].BairroId) + ' - ' + vEntregas[i].NomeBairro;
    vForm.sgEntregas.Cells[ceRota, i + 1]         := getInformacao(vEntregas[i].RotaId, vEntregas[i].NomeRota);
    vForm.sgEntregas.Cells[cePesoTotal, i + 1]    := NFormatNEstoque(vEntregas[i].PesoTotal);
    vForm.sgEntregas.Cells[ceQtdeProdutos, i + 1] := NFormat(vEntregas[i].QtdeProdutos);

    _Biblioteca.AddNoVetorSemRepetir( vEntregasIds, vEntregas[i].EntregaId );
  end;
  vForm.sgEntregas.SetLinhasGridPorTamanhoVetor( Length(vEntregas) );

  vItens := _EntregasItens.BuscarEntregaItensComando( Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.ENTREGA_ID', vEntregasIds) );
  for i := Low(vItens) to High(vItens) do begin
    vForm.sgItens.Cells[coProdutoId, i + 1]  := NFormat(vItens[i].ProdutoId);
    vForm.sgItens.Cells[coNome, i + 1]       := vItens[i].NomeProduto;
    vForm.sgItens.Cells[coMarca, i + 1]      := vItens[i].NomeMarca;
    vForm.sgItens.Cells[coLote, i + 1]       := vItens[i].Lote;
    vForm.sgItens.Cells[coQuantidade, i + 1] := NFormatEstoque(vItens[i].Quantidade);
    vForm.sgItens.Cells[coUnidade, i + 1]    := IIfStr(vItens[i].UnidadeEntregaId <> '', vItens[i].UnidadeEntregaId, vItens[i].Unidade);
    vForm.sgItens.Cells[coDevolvidos, i + 1] := NFormatNEstoque(vItens[i].Devolvidos);
    vForm.sgItens.Cells[coRetornados, i + 1] := NFormatNEstoque(vItens[i].Retornados);
  end;
  vForm.sgItens.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesControleEntrega.FormCreate(Sender: TObject);
begin
  inherited;
  pcDados.ActivePageIndex := 0;
  ReadOnlyTodosObjetos(True);
end;

procedure TFormInformacoesControleEntrega.sgEntregasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Entrega.Informar( SFormatInt(sgEntregas.Cells[ceEntregaId, sgEntregas.Row]) );
end;

procedure TFormInformacoesControleEntrega.sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    ceEntregaId,
    cePedidoId,
    cePesoTotal,
    ceQtdeProdutos
  ] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgEntregas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesControleEntrega.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coNome, coMarca, coLote, coUnidade] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
