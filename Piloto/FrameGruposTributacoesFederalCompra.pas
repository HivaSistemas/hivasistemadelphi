unit FrameGruposTributacoesFederalCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _GruposTribFederalCompra,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, PesquisaGruposTributacoesFederalCompra,
  _Biblioteca, Vcl.Menus;

type
  TFrGruposTributacoesFederalCompra = class(TFrameHenrancaPesquisas)
  public
    function getGrupo(pLinha: Integer = -1): RecGruposTribFederalCompra;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrGruposTributacoesFederalCompra }

function TFrGruposTributacoesFederalCompra.AdicionarDireto: TObject;
var
  vDados: TArray<RecGruposTribFederalCompra>;
begin
  vDados := _GruposTribFederalCompra.BuscarGruposTribFederalCompra(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrGruposTributacoesFederalCompra.AdicionarPesquisando: TObject;
begin
  Result := PesquisaGruposTributacoesFederalCompra.Pesquisar;
end;

function TFrGruposTributacoesFederalCompra.AdicionarPesquisandoTodos: TArray<TObject>;
begin

end;

function TFrGruposTributacoesFederalCompra.getGrupo(pLinha: Integer): RecGruposTribFederalCompra;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecGruposTribFederalCompra(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrGruposTributacoesFederalCompra.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecGruposTribFederalCompra(FDados[i]).GrupoTribFederalCompraId = RecGruposTribFederalCompra(pSender).GrupoTribFederalCompraId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrGruposTributacoesFederalCompra.MontarGrid;
var
  i: Integer;
  vSender: RecGruposTribFederalCompra;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecGruposTribFederalCompra(FDados[i]);
      AAdd([IntToStr(vSender.GrupoTribFederalCompraId), vSender.Descricao]);
    end;
  end;
end;

end.
