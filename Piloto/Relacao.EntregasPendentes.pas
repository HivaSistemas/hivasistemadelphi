unit Relacao.EntregasPendentes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, _Orcamentos,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.Buttons, _Biblioteca,
  StaticTextLuka, FrameDataInicialFinal, FrameNumeros, FrameProdutos, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FrameClientes, _RelacaoEntregasPendentes, FrameEmpresas, Informacoes.Orcamento,
  ComboBoxLuka, CheckBoxLuka, frxClass, FrameLocais, FrameVendedores;

type
  TFormRelacaoEntregasPendentes = class(TFormHerancaRelatoriosPageControl)
    sgEntregas: TGridLuka;
    spSeparador: TSplitter;
    sgItens: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    FrClientes: TFrClientes;
    FrProdutos: TFrProdutos;
    FrOrcamentos: TFrNumeros;
    FrPrevisaoEntrega: TFrDataInicialFinal;
    FrDataRecebimento: TFrDataInicialFinal;
    FrEmpresas: TFrEmpresas;
    lb1: TLabel;
    cbTipoEntrega: TComboBoxLuka;
    dsEntrega: TfrxUserDataSet;
    dsItens: TfrxUserDataSet;
    frxReport: TfrxReport;
    FrVendedores: TFrVendedores;
    FrLocalProduto: TFrLocais;
    st4: TStaticText;
    stTotalRetirar: TStaticTextLuka;
    StaticText1: TStaticText;
    stTotalEntregar: TStaticTextLuka;
    StaticText2: TStaticText;
    stTotalSemPrevisao: TStaticTextLuka;
    ckSomenteRecebimentoNaEntrega: TCheckBoxLuka;
    procedure FormShow(Sender: TObject);
    procedure sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEntregasDblClick(Sender: TObject);
    procedure sgEntregasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgEntregasClick(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure dsEntregaGetValue(const VarName: string; var Value: Variant);
    procedure dsItensGetValue(const VarName: string; var Value: Variant);
    procedure dsEntregaFirst(Sender: TObject);
    procedure dsEntregaNext(Sender: TObject);
    procedure dsEntregaPrior(Sender: TObject);
    procedure dsItensFirst(Sender: TObject);
    procedure dsItensCheckEOF(Sender: TObject; var Eof: Boolean);
    procedure dsItensNext(Sender: TObject);
    procedure dsItensPrior(Sender: TObject);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FItens: TArray<RecEntregasItensPendentes>;
    MasterNo: Integer;
    DetailNo: Integer;
  protected
    procedure Carregar(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  (* Grid Orcamentos *)
  coOrcamentoId     = 0;
  coCliente         = 1;
  coTipoMovimento   = 2;
  coDataCadastro    = 3;
  coVendedor        = 4;
  coPrevisaoEntrega = 5;
  coLocal           = 6;
  coLocalId         = 7;
  coEmpresaId       = 8;

  (* Grid de Itens *)
  ciProdutoId     = 0;
  ciNome          = 1;
  ciMarca         = 2;
  ciQuantidade    = 3;
  ciEntregues     = 4;
  ciDevolvidos    = 5;
  ciSaldo         = 6;
  ciValorTotal    = 7;
  ciUnidade       = 8;
  ciLote          = 9;
  ciTipoLinha     = 10;

{ TFormRelacaoEntregasPendentes }

procedure TFormRelacaoEntregasPendentes.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vEntregas: TArray<RecEntregasPendentes>;

  vPedidosIds: TArray<Integer>;
  vTotalRetirar: Integer;
  vTotalEntregar: Integer;
  vTotalSemPrevisao: Integer;
begin
  sgEntregas.ClearGrid();
  sgItens.ClearGrid();
  FItens := nil;
  vPedidosIds := nil;
  vTotalRetirar := 0;
  vTotalEntregar := 0;
  vTotalSemPrevisao := 0;

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, '( ' + FrEmpresas.getSqlFiltros('EMPRESA_ID') + ' or EMPRESA_ID is null) ' );

  if not FrClientes.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, FrClientes.getSqlFiltros('CLIENTE_ID') );

  if not FrPrevisaoEntrega.NenhumaDataValida then
    _Biblioteca.WhereOuAnd( vSql, FrPrevisaoEntrega.getSqlFiltros('trunc(PREVISAO_ENTREGA)') );

  if not FrDataRecebimento.NenhumaDataValida then
    _Biblioteca.WhereOuAnd( vSql, FrDataRecebimento.getSqlFiltros('trunc(DATA_HORA_RECEBIMENTO)') );

  if not FrOrcamentos.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, FrOrcamentos.TrazerFiltros('ORCAMENTO_ID') );

  if not FrProdutos.EstaVazio then begin
    _Biblioteca.WhereOuAnd(
      vSql,
      '(ORCAMENTO_ID, trunc(nvl(PREVISAO_ENTREGA, sysdate))) in(select distinct ORCAMENTO_ID, trunc(nvl(PREVISAO_ENTREGA, sysdate)) from VW_ITENS_ENTREGAS_PENDENTES where ' + FrProdutos.getSqlFiltros('PRODUTO_ID') + ') '
    );
  end;

  if not FrVendedores.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, FrVendedores.getSqlFiltros('VENDEDOR_ID') );

  if not FrLocalProduto.EstaVazio then
    _Biblioteca.WhereOuAnd( vSql, FrLocalProduto.getSqlFiltros('LOCAL_ID') );

  if cbTipoEntrega.GetValor <> 'NaoFiltrar' then
    _Biblioteca.WhereOuAnd( vSql, 'TIPO_MOVIMENTO = ''' + cbTipoEntrega.GetValor + ''' ' );

  if ckSomenteRecebimentoNaEntrega.Checked then
    _Biblioteca.WhereOuAnd(vSql, ' RECEBER_NA_ENTREGA = ''S'' ');

  vSql := vSql + ' order by ORCAMENTO_ID ';

  vEntregas := _RelacaoEntregasPendentes.BuscarEntregas(Sessao.getConexaoBanco, vSql);
  if vEntregas = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  for i := Low(vEntregas) to High(vEntregas) do begin
    sgEntregas.Cells[coOrcamentoId, i + 1]     := _Biblioteca.NFormat(vEntregas[i].OrcamentoId);
    sgEntregas.Cells[coCliente, i + 1]         := _Biblioteca.NFormat(vEntregas[i].ClienteId) + ' - ' + vEntregas[i].NomeCliente;
    sgEntregas.Cells[coTipoMovimento, i + 1]   := vEntregas[i].TipoMovimentoAnalitico;
    sgEntregas.Cells[coDataCadastro, i + 1]    := _Biblioteca.DHFormatN(vEntregas[i].DataHoraCadastro);
    sgEntregas.Cells[coVendedor, i + 1]        := _Biblioteca.NFormat(vEntregas[i].VendedorId) + ' - ' + vEntregas[i].NomeVendedor;
    sgEntregas.Cells[coPrevisaoEntrega, i + 1] := _Biblioteca.DHFormatN(vEntregas[i].PrevisaoEntrega);

    if vEntregas[i].LocalId > 0 then
      sgEntregas.Cells[coLocal, i + 1]           := _Biblioteca.NFormat(vEntregas[i].LocalId) + ' - ' + vEntregas[i].NomeLocal;

    sgEntregas.Cells[coLocalId, i + 1]         := _Biblioteca.NFormatN(vEntregas[i].LocalId);
    sgEntregas.Cells[coEmpresaId, i + 1]       := _Biblioteca.NFormatN(vEntregas[i].EmpresaId);

    _Biblioteca.AddNoVetorSemRepetir( vPedidosIds, vEntregas[i].OrcamentoId);

    if vEntregas[i].TipoMovimento = 'E' then
      vTotalEntregar := vTotalEntregar + 1
    else if vEntregas[i].TipoMovimento = 'R' then
      vTotalRetirar := vTotalRetirar + 1
    else
      vTotalSemPrevisao := vTotalSemPrevisao + 1;
  end;

  stTotalEntregar.AsInt := vTotalEntregar;
  stTotalRetirar.AsInt := vTotalRetirar;
  stTotalSemPrevisao.AsInt := vTotalSemPrevisao;

  sgEntregas.SetLinhasGridPorTamanhoVetor( Length(vEntregas) );

  FItens := _RelacaoEntregasPendentes.BuscarItensEntregas( Sessao.getConexaoBanco, 'where ' + FiltroInInt('ORCAMENTO_ID', vPedidosIds) );

  dsEntrega.RangeEnd := reCount;
  dsEntrega.RangeEndCount := i;

  dsItens.RangeEnd := reCount;
  dsItens.RangeEndCount := High(FItens) + 1;

  sgEntregasClick(Sender);
  _Biblioteca.SetarFoco(sgEntregas);
end;

procedure TFormRelacaoEntregasPendentes.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave( Sessao.getParametrosEmpresa.EmpresaId, False );
  _Biblioteca.SetarFoco(FrEmpresas);
end;

procedure TFormRelacaoEntregasPendentes.dsEntregaFirst(Sender: TObject);
begin
  inherited;
  MasterNo := 1;
end;

procedure TFormRelacaoEntregasPendentes.dsEntregaGetValue(
  const VarName: string; var Value: Variant);
begin
  inherited;
  if VarName = 'pedido' then
    Value := SFormatInt(sgEntregas.Cells[coOrcamentoId ,MasterNo])
  else
    Value := sgEntregas.Cells[dsEntrega.Fields.IndexOf(VarName) ,MasterNo];
end;

procedure TFormRelacaoEntregasPendentes.dsEntregaNext(Sender: TObject);
begin
  inherited;
  Inc(MasterNo);
end;

procedure TFormRelacaoEntregasPendentes.dsEntregaPrior(Sender: TObject);
begin
  inherited;
  Dec(MasterNo);
end;

procedure TFormRelacaoEntregasPendentes.dsItensCheckEOF(Sender: TObject;
  var Eof: Boolean);
var
  vTotalItens : integer;
begin
  vTotalItens := High(FItens);
  if not Eof then
    Eof := DetailNo > vTotalItens;
end;

procedure TFormRelacaoEntregasPendentes.dsItensFirst(Sender: TObject);
begin
  inherited;
  DetailNo := 0;
  while (not dsItens.Eof) and (FItens[DetailNo].OrcamentoId <> SFormatInt(sgEntregas.Cells[coOrcamentoId ,MasterNo])) do
    Inc(DetailNo);
end;

procedure TFormRelacaoEntregasPendentes.dsItensGetValue(const VarName: string;
  var Value: Variant);
begin
  inherited;
  if VarName = 'pedido' then
    Value := FItens[DetailNo].OrcamentoId;
  if VarName = 'produto' then
    Value := FItens[DetailNo].ProdutoId;
  if VarName = 'nome' then
    Value := FItens[DetailNo].Nome;
  if VarName = 'marca' then
    Value := FItens[DetailNo].NomeMarca;
  if VarName = 'qtde' then
    Value := FItens[DetailNo].Quantidade;
  if VarName = 'entr' then
    Value := FItens[DetailNo].Entregues;
  if VarName = 'devolu' then
    Value := FItens[DetailNo].Devolvidos;
  if VarName = 'saldo' then
    Value := FItens[DetailNo].Saldo;
  if VarName = 'unid' then
    Value := FItens[DetailNo].UnidadeVenda;
  if VarName = 'lote' then
    Value := FItens[DetailNo].Lote;
end;

procedure TFormRelacaoEntregasPendentes.dsItensNext(Sender: TObject);
begin
  inherited;
  Inc(DetailNo);
  while (not dsItens.Eof) and (FItens[DetailNo].OrcamentoId <> SFormatInt(sgEntregas.Cells[coOrcamentoId ,MasterNo])) do
    Inc(DetailNo);
end;

procedure TFormRelacaoEntregasPendentes.dsItensPrior(Sender: TObject);
begin
  inherited;
  Dec(DetailNo);
  while (not dsItens.Eof) and (FItens[DetailNo].OrcamentoId <> SFormatInt(sgEntregas.Cells[coOrcamentoId ,MasterNo])) do
    Dec(DetailNo);
end;

procedure TFormRelacaoEntregasPendentes.Imprimir(Sender: TObject);
begin
  inherited;
  frxReport.ShowReport();
end;

procedure TFormRelacaoEntregasPendentes.sgEntregasClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;

  vPrevisaoEntrega: TDateTime;
  vEmpresaId: Integer;
  vPedidoId: Integer;
  vLocalId: Integer;
  vValorTotal: Double;
begin
  inherited;

  vLinha := 0;
  vValorTotal := 0;
  sgItens.ClearGrid();

  vPrevisaoEntrega := ToData( sgEntregas.Cells[coPrevisaoEntrega, sgEntregas.Row] );
  vEmpresaId := SFormatInt( sgEntregas.Cells[coEmpresaId, sgEntregas.Row] );
  vPedidoId := SFormatInt( sgEntregas.Cells[coOrcamentoId, sgEntregas.Row] );
  vLocalId := SFormatInt( sgEntregas.Cells[coLocalId, sgEntregas.Row] );

  for i := Low(FItens) to High(FItens) do begin
    if
      (vEmpresaId <> FItens[i].EmpresaId) or
      (vPedidoId <> FItens[i].OrcamentoId)
//      (vLocalId <> FItens[i].LocalId) or
//      (vPrevisaoEntrega <> FItens[i].PrevisaoEntrega)
    then
      Continue;

    Inc(vLinha);

    sgItens.Cells[ciProdutoId, vLinha]     := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]          := FItens[i].Nome;
    sgItens.Cells[ciMarca, vLinha]         := NFormat(FItens[i].MarcaId) + ' - ' + FItens[i].NomeMarca;
    sgItens.Cells[ciQuantidade, vLinha]    := NFormatNEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciEntregues, vLinha]     := NFormatNEstoque(FItens[i].Entregues);
    sgItens.Cells[ciDevolvidos, vLinha]    := NFormatNEstoque(FItens[i].Devolvidos);
    sgItens.Cells[ciSaldo, vLinha]         := NFormatNEstoque(FItens[i].Saldo);
    sgItens.Cells[ciValorTotal, vLinha]    := NFormat(FItens[i].Saldo * FItens[i].PrecoUnitario);
    sgItens.Cells[ciUnidade, vLinha]       := FItens[i].UnidadeVenda;
    sgItens.Cells[ciLote, vLinha]          := FItens[i].Lote;
    sgItens.Cells[ciTipoLinha, vLinha]     := 'DET';

    vValorTotal := vValorTotal + FItens[i].Saldo * FItens[i].PrecoUnitario;
  end;

  if vValorTotal > 0 then begin
    Inc(vLinha);
    sgItens.Cells[ciMarca, vLinha]      := 'VALOR TOTAL:';
    sgItens.Cells[ciValorTotal, vLinha] := NFormat(vValorTotal);
    sgItens.Cells[ciTipoLinha, vLinha]  := 'TOT';
  end;

  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormRelacaoEntregasPendentes.sgEntregasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar( SFormatInt(sgEntregas.Cells[coOrcamentoId, sgEntregas.Row]) );
end;

procedure TFormRelacaoEntregasPendentes.sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coOrcamentoId] then
    vAlinhamento := taRightJustify
  else if ACol = coTipoMovimento then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgEntregas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoEntregasPendentes.sgEntregasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coTipoMovimento then begin
    AFont.Style := [fsBold];
    if sgEntregas.Cells[ACol, ARow] = 'Retirar' then
      AFont.Color := clBlue
    else if sgEntregas.Cells[ACol, ARow] = 'Entregar' then
      AFont.Color := $000096DB
    else
      AFont.Color := clGreen;
  end;
end;

procedure TFormRelacaoEntregasPendentes.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    ciProdutoId,
    ciQuantidade,
    ciEntregues,
    ciDevolvidos,
    ciSaldo,
    ciValorTotal]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoEntregasPendentes.sgItensGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgItens.Cells[ciTipoLinha, ARow] = 'TOT' then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
  end;
end;

end.
