unit DefinirEntregasRetiradasDevolucao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, _NotasFiscais,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca, _Sessao, _ComunicacaoNFe,
  StaticTextLuka, Vcl.Grids, GridLuka, Vcl.Buttons, Vcl.ExtCtrls, _Devolucoes, _DevolucoesItens, ImpressaoComprovanteDevolucaoGrafico,
  _RecordsOrcamentosVendas, _RecordsEspeciais, Vcl.Menus, _RetiradasItens, _EntregasItens, _RecordsExpedicao,
  _DevolucoesItensEntregas, EditLuka, _FrameHerancaPrincipal, _DevolucoesItensRetiradas, _FrameHenrancaPesquisas, FrameLocais;

type
  TFormDefinirEntregasRetiradasEntregasDevolucao = class(TFormHerancaFinalizar)
    sgEntregas: TGridLuka;
    sgItens: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    lb1: TLabel;
    eDevolucaoId: TEditLuka;
    FrLocalDevolucao: TFrLocais;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensClick(Sender: TObject);
    procedure sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEntregasSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgEntregasEnter(Sender: TObject);
    procedure sgEntregasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgEntregasArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
  private
    FRetiradas: TArray<RecRetiradaItem>;
    FEntregas: TArray<RecEntregaItem>;

    procedure AjustarQuantidadeDefinida;
    procedure SaltarItemDevolucao( pTipo: string; pId: Integer; pItemId: Integer; pQuantidade: Double );
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Definir(pDevolucaoId: Integer; pOrcamentoId: Integer): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

const
  (* Grid dos itens da devolu��o *)
  coProdutoId  = 0;
  coNome       = 1;
  coMarca      = 2;
  coUnidade    = 3;
  coQuantidade = 4;
  coQtdeDefinida = 5;
  coItemId       = 6;

  (* Grid das entregas a serem selecionadas *)
  ceCodigo            = 0;
  ceTipoEntregaAnalit = 1;
  ceDataHoraEntRetira = 2;
  ceLote              = 3;
  ceQtdeDisponivel    = 4;
  ceQtdeUtilizar      = 5;
  ceTipoEntrega       = 6;
  ceItemId            = 7;
  ceMultiploVenda     = 8;

function Definir(pDevolucaoId: Integer; pOrcamentoId: Integer): TRetornoTelaFinalizar;
var
  i: Integer;
  vSql: string;
  vForm: TFormDefinirEntregasRetiradasEntregasDevolucao;
  vItens: TArray<RecDevolucoesItens>;
  vProdutosIds: TArray<Integer>;

  vNotasIds: TArray<Integer>;
begin
  vItens := _DevolucoesItens.BuscarDevolucoesItens(Sessao.getConexaoBanco, 1, [pDevolucaoId]);
  if vItens = nil then begin
    _Biblioteca.Exclamar('Nenhum produto encontrado para confirma��o!');
    Exit;
  end;

  vProdutosIds := nil;
  vForm := TFormDefinirEntregasRetiradasEntregasDevolucao.Create(nil);
  vForm.eDevolucaoId.AsInt := pDevolucaoId;

  for i := Low(vItens) to High(vItens) do begin
    vForm.sgItens.Cells[coProdutoId, i + 1]  := _Biblioteca.NFormat(vItens[i].ProdutoId);
    vForm.sgItens.Cells[coNome, i + 1]       := vItens[i].NomeProduto;
    vForm.sgItens.Cells[coMarca, i + 1]      := vItens[i].NomeMarca;
    vForm.sgItens.Cells[coQuantidade, i + 1] := _Biblioteca.NFormatEstoque(vItens[i].QuantidadeDevolvidosEntregues);
    vForm.sgItens.Cells[coUnidade, i + 1]    := vItens[i].UnidadeVenda;
    vForm.sgItens.Cells[coItemId, i + 1]     := NFormat(vItens[i].ItemId);

    _Biblioteca.AddNoVetorSemRepetir(vProdutosIds, vItens[i].ProdutoId);
  end;
  vForm.sgItens.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  vSql := ' and RET.ORCAMENTO_ID = ' + IntToStr(pOrcamentoId);
  vSql := vSql + ' and ITE.QUANTIDADE - ITE.DEVOLVIDOS > 0 ';
  vSql := vSql + ' and ' + FiltroInInt('ITE.PRODUTO_ID', vProdutosIds);
  vForm.FRetiradas := _RetiradasItens.BuscarRetiradaItensComando(Sessao.getConexaoBanco, vSql);

  vSql := ' where ENT.ORCAMENTO_ID = ' + IntToStr(pOrcamentoId);
  vSql := vSql + ' and ITE.QUANTIDADE - ITE.DEVOLVIDOS - ITE.RETORNADOS > 0 ';
  vSql := vSql + ' and ' + FiltroInInt('ITE.PRODUTO_ID', vProdutosIds);
  vForm.FEntregas := _EntregasItens.BuscarEntregaItensComando(Sessao.getConexaoBanco, vSql);

  vForm.sgItensClick(nil);
  SetarFoco(vForm.FrLocalDevolucao);
  
  if Result.Ok(vForm.ShowModal, True) then begin
    vNotasIds := _NotasFiscais.BuscarIdsNotasDevolucoes(Sessao.getConexaoBanco, pDevolucaoId);
    for i := Low(vNotasIds) to High(vNotasIds) do begin
      if not _ComunicacaoNFe.EnviarNFe(vNotasIds[i]) then begin
        _Biblioteca.Exclamar('N�o foi poss�vel enviar todas as notas de devolu��o, fa�a o envio na tela de "Rela��o de notas fiscais"!');
        Break;
      end;
    end;
  end;
end;

procedure TFormDefinirEntregasRetiradasEntregasDevolucao.sgEntregasArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if SFormatCurr(TextCell) > SFormatCurr(sgEntregas.Cells[ceQtdeDisponivel, ARow]) then begin
    Exclamar('A quantidade a ser devolvida n�o pode ser maior que a quantidade dispon�vel!');
    SetarFoco(sgEntregas);


    sgEntregas.Cells[ceQtdeUtilizar, ARow] := '';
    TextCell := '';

    AjustarQuantidadeDefinida();
    Exit;
  end;

  TextCell := NFormatNEstoque(SFormatDouble(TextCell));
  if TextCell = '' then begin
    sgEntregas.Cells[ceQtdeUtilizar, ARow] := '';
    AjustarQuantidadeDefinida;
    Exit;
  end;

  if not ValidarMultiplo(SFormatDouble(sgEntregas.Cells[ACol, sgEntregas.Row]), SFormatDouble(sgEntregas.Cells[ceMultiploVenda, sgEntregas.Row])) then begin
    SetarFoco(sgEntregas);

    TextCell := '';
    sgEntregas.Cells[ceQtdeUtilizar, ARow] := '';

    AjustarQuantidadeDefinida();
    Exit;
  end;

  SaltarItemDevolucao(
    sgEntregas.Cells[ceTipoEntrega, ARow],
    SFormatInt(sgEntregas.Cells[ceCodigo, ARow]),
    SFormatInt(sgEntregas.Cells[ceItemId, ARow]),
    SFormatDouble(sgEntregas.Cells[ACol, sgEntregas.Row])
  );

  AjustarQuantidadeDefinida;
end;

procedure TFormDefinirEntregasRetiradasEntregasDevolucao.sgEntregasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  
  if ACol in[
    ceCodigo,
    ceQtdeDisponivel,
    ceQtdeUtilizar] 
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgEntregas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormDefinirEntregasRetiradasEntregasDevolucao.sgEntregasEnter(Sender: TObject);
begin
  inherited;
  sgEntregas.Col := ceQtdeUtilizar;
end;

procedure TFormDefinirEntregasRetiradasEntregasDevolucao.sgEntregasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = ceQtdeUtilizar then begin
    AFont.Color := _Biblioteca.coCorFonteEdicao2;  
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;    
  end;
end;

procedure TFormDefinirEntregasRetiradasEntregasDevolucao.sgEntregasSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := (ACol = ceQtdeUtilizar);
end;

procedure TFormDefinirEntregasRetiradasEntregasDevolucao.sgItensClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vItemId: Integer;
begin
  inherited;
  vLinha := 0;
  sgEntregas.ClearGrid();
  vItemId := SFormatInt(sgItens.Cells[coItemId, sgItens.Row]);

  for i := Low(FRetiradas) to High(FRetiradas) do begin
    if vItemId <> FRetiradas[i].ItemId then
      Continue;

    Inc(vLinha);

    sgEntregas.Cells[ceCodigo, vLinha]            := NFormat(FRetiradas[i].RetiradaId);
    sgEntregas.Cells[ceTipoEntregaAnalit, vLinha] := 'Retirada';
    sgEntregas.Cells[ceDataHoraEntRetira, vLinha] := DHFormat(FRetiradas[i].DataHoraCadastro);
    sgEntregas.Cells[ceLote, vLinha]              := FRetiradas[i].Lote;
    sgEntregas.Cells[ceQtdeDisponivel, vLinha]    := NFormatEstoque(FRetiradas[i].Quantidade - FRetiradas[i].Devolvidos);
    sgEntregas.Cells[ceQtdeUtilizar, vLinha]      := '';
    sgEntregas.Cells[ceTipoEntrega, vLinha]       := 'R';
    sgEntregas.Cells[ceItemId, vLinha]            := NFormat(FRetiradas[i].ItemId);
    sgEntregas.Cells[ceMultiploVenda, vLinha]     := NFormatEstoque(FRetiradas[i].MultiploVenda);
  end;

  for i := Low(FEntregas) to High(FEntregas) do begin
    if vItemId <> FEntregas[i].ItemId then
      Continue;

    Inc(vLinha);

    sgEntregas.Cells[ceCodigo, vLinha]            := NFormat(FEntregas[i].EntregaId);
    sgEntregas.Cells[ceTipoEntregaAnalit, vLinha] := 'Entrega';
    sgEntregas.Cells[ceDataHoraEntRetira, vLinha] := DHFormat(FEntregas[i].DataHoraCadastro);
    sgEntregas.Cells[ceLote, vLinha]              := FEntregas[i].Lote;
    sgEntregas.Cells[ceQtdeDisponivel, vLinha]    := NFormatEstoque(FEntregas[i].Quantidade - FEntregas[i].Devolvidos);
    sgEntregas.Cells[ceQtdeUtilizar, vLinha]      := '';
    sgEntregas.Cells[ceTipoEntrega, vLinha]       := 'E';
    sgEntregas.Cells[ceItemId, vLinha]            := NFormat(FEntregas[i].ItemId);
    sgEntregas.Cells[ceMultiploVenda, vLinha]     := NFormatEstoque(FEntregas[i].MultiploVenda);        
  end;
  sgEntregas.RowCount := IIfInt(vLinha > 1, vLinha + 1, 2);
end;

procedure TFormDefinirEntregasRetiradasEntregasDevolucao.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coProdutoId, coQuantidade, coQtdeDefinida] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormDefinirEntregasRetiradasEntregasDevolucao.AjustarQuantidadeDefinida;
var
  i: Integer;
  vQtde: Double;
begin
  vQtde := 0;
  for i := 1 to sgEntregas.RowCount -1 do
    vQtde := vQtde + SFormatDouble(sgEntregas.Cells[ceQtdeUtilizar, i]);

  sgItens.Cells[coQtdeDefinida, sgItens.Row] := NFormatNEstoque(vQtde);
end;

procedure TFormDefinirEntregasRetiradasEntregasDevolucao.VerificarRegistro(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  if FrLocalDevolucao.EstaVazio then begin
    _Biblioteca.Exclamar('O local de devolu��o das mercadorias n�o foi informado, verifique!');
    SetarFoco(FrLocalDevolucao);
    Abort;
  end;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coQuantidade, i] <> sgItens.Cells[coQtdeDefinida, i] then begin
      _Biblioteca.Exclamar('Existe diferen�a entre a quantidade a se devolver para o produto ' + sgItens.Cells[coNome, i] + ' com a quantidade definida, verifique!');
      sgItens.Row := i;
      Abort;
    end;
  end;
end;

procedure TFormDefinirEntregasRetiradasEntregasDevolucao.Finalizar(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vItens: TArray<RecDevolucoesItensRetiradas>;
  vItensEnt: TArray<RecDevolucoesItensEntregas>;
begin
  vItens := nil;
  vItensEnt := nil;

  for i := Low(FRetiradas) to High(FRetiradas) do begin
    if NFormatN(FRetiradas[i].QuantidadeDevolver) = '' then
      Continue;

    SetLength(vItens, Length(vItens) + 1);

    vItens[High(vItens)].DevolucaoId   := eDevolucaoId.AsInt;
    vItens[High(vItens)].ItemId        := FRetiradas[i].ItemId;
    vItens[High(vItens)].Lote          := FRetiradas[i].Lote;
    vItens[High(vItens)].RetiradaId    := FRetiradas[i].RetiradaId;
    vItens[High(vItens)].Quantidade    := FRetiradas[i].QuantidadeDevolver;
  end;

  for i := Low(FEntregas) to High(FEntregas) do begin
    if NFormatN(FEntregas[i].QuantidadeDevolver) = '' then
      Continue;

    SetLength(vItensEnt, Length(vItensEnt) + 1);

    vItensEnt[High(vItensEnt)].DevolucaoId   := eDevolucaoId.AsInt;
    vItensEnt[High(vItensEnt)].ItemId        := FEntregas[i].ItemId;
    vItensEnt[High(vItensEnt)].Lote          := FEntregas[i].Lote;
    vItensEnt[High(vItensEnt)].EntregaId     := FEntregas[i].EntregaId;
    vItensEnt[High(vItensEnt)].Quantidade    := FEntregas[i].QuantidadeDevolver;
  end;

  Sessao.getConexaoBanco.IniciarTransacao;

  vRetBanco :=
    _DevolucoesItensRetiradas.AtualizarDevolucoesItensRetiradas(
      Sessao.getConexaoBanco,
      eDevolucaoId.AsInt,
      FrLocalDevolucao.GetLocais().local_id,
      vItens,
      True
    );
  Sessao.AbortarSeHouveErro( vRetBanco );

  vRetBanco :=
    _DevolucoesItensEntregas.AtualizarDevolucoesItensEntregas(
      Sessao.getConexaoBanco,
      eDevolucaoId.AsInt,
      FrLocalDevolucao.GetLocais().local_id,
      vItensEnt,
      True
    );
  Sessao.AbortarSeHouveErro( vRetBanco );

  vRetBanco := _Devolucoes.ConsolidarDevolucao( Sessao.getConexaoBanco, eDevolucaoId.AsInt, True );
  Sessao.AbortarSeHouveErro( vRetBanco );

  Sessao.getConexaoBanco.FinalizarTransacao;

  ImpressaoComprovanteDevolucaoGrafico.Imprimir(eDevolucaoId.AsInt);

  inherited;
end;

procedure TFormDefinirEntregasRetiradasEntregasDevolucao.SaltarItemDevolucao(pTipo: string; pId, pItemId: Integer; pQuantidade: Double);
var
  i: Integer;
begin
  if pTipo = 'R' then begin
    for i := Low(FRetiradas) to High(FRetiradas) do begin
      if (pId <> FRetiradas[i].RetiradaId) or (pItemId <> FRetiradas[i].ItemId) then
        Continue;

      FRetiradas[i].QuantidadeDevolver := pQuantidade;
      Break;
    end;
  end
  else begin
    for i := Low(FEntregas) to High(FEntregas) do begin
      if (pId <> FEntregas[i].EntregaId) or (pItemId <> FEntregas[i].ItemId) then
        Continue;

      FEntregas[i].QuantidadeDevolver := pQuantidade;
      Break;      
    end;
  end;
end;

end.
