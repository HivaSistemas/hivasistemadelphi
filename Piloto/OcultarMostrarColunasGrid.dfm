inherited FormOcultarMostrarColunasGrid: TFormOcultarMostrarColunasGrid
  Hint = 'Click com bot'#227'o direito para Marcar/Desmarcar todos.'
  Caption = ''
  ClientHeight = 375
  ClientWidth = 600
  PopupMenu = pmOpcoes
  ExplicitWidth = 606
  ExplicitHeight = 404
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 338
    Width = 600
    ExplicitTop = 338
    ExplicitWidth = 600
    inherited sbFinalizar: TSpeedButton
      Left = 0
      Height = 33
      ParentBiDiMode = False
      ExplicitLeft = -2
      ExplicitHeight = 33
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 556
    Top = 291
    object miMarcaTodos: TMenuItem
      Caption = 'Marcar todos'
      OnClick = miMarcarTodosClick
    end
    object miDesmarcarTodos: TMenuItem
      Caption = 'Desmarcar todos'
      OnClick = miDesmarcarTodosClick
    end
  end
end
