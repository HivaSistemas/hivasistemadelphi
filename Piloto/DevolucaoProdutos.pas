unit DevolucaoProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.Mask, EditLukaData, Informacoes.Orcamento,
  _Orcamentos, _OrcamentosItens, _RecordsOrcamentosVendas, System.Math, _Devolucoes, _RecordsEspeciais,
  Pesquisa.Orcamentos, _ComunicacaoNFE, _ImpressaoDANFE, MenuPrincipal, ImpressaoComprovanteDevolucaoGrafico,
  StaticTextLuka, CheckBoxLuka, PesquisaClientes, _RecordsCadastros;

type
  TFormDevolucaoProdutos = class(TFormHerancaCadastroCodigo)
    lb2: TLabel;
    eCliente: TEditLuka;
    lb12: TLabel;
    eVendedor: TEditLuka;
    lb3: TLabel;
    eDataCadastro: TEditLukaData;
    lb4: TLabel;
    eDataRecebimento: TEditLukaData;
    sgProdutos: TGridLuka;
    stTotalDevolucao: TStaticText;
    sbInfoPedido: TSpeedButton;
    sbDevolucaoTotal: TSpeedButton;
    sbInfoVendedor: TSpeedButton;
    stTotalDesconto: TStaticText;
    stValorProdutos: TStaticText;
    stOutrasDespesas: TStaticText;
    lb1: TLabel;
    eObservacoes: TEditLuka;
    stValorFrete: TStaticText;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    StaticTextLuka3: TStaticTextLuka;
    StaticTextLuka4: TStaticTextLuka;
    StaticTextLuka5: TStaticTextLuka;
    stTotalSubstitucaoTributaria: TStaticText;
    StaticTextLuka6: TStaticTextLuka;
    stTotalIPI: TStaticText;
    StaticTextLuka7: TStaticTextLuka;
    ckDevolverFrete: TCheckBoxLuka;
    ckDevolverOutrasDespesas: TCheckBoxLuka;
    lb5: TLabel;
    eMotivoDevolucao: TEditLuka;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sbInfoPedidoClick(Sender: TObject);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgProdutosArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sbDevolucaoTotalClick(Sender: TObject);
    procedure ckDevolverOutrasDespesasClick(Sender: TObject);
    procedure ckDevolverFreteClick(Sender: TObject);
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); override;
    procedure sbGravarClick(Sender: TObject);
  private
    procedure PreencherRegistro(pOrcamento: RecOrcamentos);
    procedure CalcularDevolucao;
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure BuscarRegistro; override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
  end;

implementation

{$R *.dfm}

{ TFormDevolucaoProdutos }

const
  coProdutoId               = 0;
  coNome                    = 1;
  coMarca                   = 2;
  coUnidade                 = 3;
  coQuantidadeVendida       = 4;

  coQtdeEntregues           = 5;
  coQtdeDevolverEntregues   = 6;

  coQtdePendencias          = 7;
  coQtdeDevolverPendencias  = 8;

  coQtdeSemPrevisao         = 9;
  coQtdeDevolverSemPrevisao = 10;

  (* Colunas ocultas *)
  coCompletamenteDevolvido  = 11;
  coValorUnitario           = 12;
  coValorDescontoUnitario   = 13;
  coValorOutrasDespUnitario = 14;
  coValorFreteUnitario      = 15;
  coValorDevolucao          = 16;
  coValorDescontoDevolucao  = 17;
  coValorOutrasDespDevoluc  = 18;
  coValorFreteDevolucao     = 19;
  coItemId                  = 20;
  coMultiploVenda           = 21;

procedure TFormDevolucaoProdutos.BuscarRegistro;
var
  vOrcamento: TArray<RecOrcamentos>;
begin
  vOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 5, [eID.AsInt]);
  if vOrcamento = nil then begin
    _Biblioteca.NenhumRegistro;
    Abort;
  end;

  inherited;
  PreencherRegistro(vOrcamento[0]);
end;

procedure TFormDevolucaoProdutos.CalcularDevolucao;
var
  i: Integer;
  vQtdeDevolucao: Double;
  vValorDevolucao: Double;
  vValorOutrasDespesas: Double;
  vValorDescontoDevolucao: Double;
  vValorFreteDevolucao: Double;
begin
  vValorDevolucao         := 0;
  vValorOutrasDespesas    := 0;
  vValorDescontoDevolucao := 0;
  vValorFreteDevolucao    := 0;
  for i := sgProdutos.FixedRows to sgProdutos.RowCount -1 do begin
    if sgProdutos.Cells[coCompletamenteDevolvido, 1] = 'S' then
      Continue;

    vQtdeDevolucao :=
      SFormatDouble(sgProdutos.Cells[coQtdeDevolverEntregues, i]) +
      SFormatDouble(sgProdutos.Cells[coQtdeDevolverPendencias, i]) +
      SFormatDouble(sgProdutos.Cells[coQtdeDevolverSemPrevisao, i]);

    sgProdutos.Cells[coValorDevolucao, i]         := NFormat( vQtdeDevolucao * SFormatDouble(sgProdutos.Cells[coValorUnitario, i]));
    sgProdutos.Cells[coValorDescontoDevolucao, i] := NFormat( vQtdeDevolucao * SFormatDouble(sgProdutos.Cells[coValorDescontoUnitario, i]));
    sgProdutos.Cells[coValorOutrasDespDevoluc, i] := NFormat( IIfDbl(ckDevolverOutrasDespesas.Checked, vQtdeDevolucao * SFormatDouble(sgProdutos.Cells[coValorOutrasDespUnitario, i])) );
    sgProdutos.Cells[coValorFreteDevolucao, i]    := NFormat( IIfDbl(ckDevolverFrete.Checked, vQtdeDevolucao * SFormatDouble(sgProdutos.Cells[coValorFreteUnitario, i])) );

    vValorDevolucao         := vValorDevolucao + SFormatDouble(sgProdutos.Cells[coValorDevolucao, i]);
    vValorOutrasDespesas    := vValorOutrasDespesas + SFormatDouble(sgProdutos.Cells[coValorOutrasDespDevoluc, i]);
    vValorDescontoDevolucao := vValorDescontoDevolucao + SFormatDouble(sgProdutos.Cells[coValorDescontoDevolucao, i]);
    vValorFreteDevolucao    := vValorFreteDevolucao + SFormatDouble(sgProdutos.Cells[coValorFreteDevolucao, i]);
  end;
  stValorProdutos.Caption  := NFormat(vValorDevolucao);
  stValorFrete.Caption     := NFormat(vValorFreteDevolucao);
  stOutrasDespesas.Caption := NFormat(vValorOutrasDespesas);
  stTotalDesconto.Caption  := NFormat(vValorDescontoDevolucao);
  stTotalDevolucao.Caption := NFormat(vValorDevolucao + vValorOutrasDespesas - vValorDescontoDevolucao + vValorFreteDevolucao);
end;

procedure TFormDevolucaoProdutos.ckDevolverFreteClick(Sender: TObject);
begin
  inherited;
  CalcularDevolucao;
end;

procedure TFormDevolucaoProdutos.ckDevolverOutrasDespesasClick(Sender: TObject);
begin
  inherited;
  CalcularDevolucao;
end;

procedure TFormDevolucaoProdutos.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key <> VK_RETURN then
    Exit;

  if eID.AsInt = 0 then begin
    _Biblioteca.Exclamar('Pedido n�o informado corretamente!');
    SetarFoco(eID);
    Abort;
  end;

  inherited;
end;

procedure TFormDevolucaoProdutos.ExcluirRegistro;
begin

end;

procedure TFormDevolucaoProdutos.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vClienteId: Integer;
  vRetBanco: RecRetornoBD;

  vItens: TArray<RecDevolucoesItens>;
  vItensEntregasIds: TArray<Integer>;

  vProdutosIdsJaEntregues: TArray<Integer>;
  vCliente: RecClientes;
  vMaiorDataEntrega: TDate;
  vQtdeDiasDevolucao: Integer;
begin
  inherited;
  vItens := nil;
  vItensEntregasIds := nil;
  vProdutosIdsJaEntregues := nil;
  for i := 1 to sgProdutos.RowCount -1 do begin
    if
      (sgProdutos.Cells[coQtdeDevolverEntregues, i] = '') and
      (sgProdutos.Cells[coQtdeDevolverPendencias, i] = '') and
      (sgProdutos.Cells[coQtdeDevolverSemPrevisao, i] = '')
    then
      Continue;

    SetLength(vItens, Length(vItens) + 1);
    vItens[High(vItens)].DevolucaoId                    := 0;

    vItens[High(vItens)].ItemId                         := SFormatInt(sgProdutos.Cells[coItemId, i]);
    vItens[High(vItens)].ProdutoId                      := SFormatInt(sgProdutos.Cells[coProdutoId, i]);

    vItens[High(vItens)].QuantidadeDevolvidosEntregues  := SFormatCurr(sgProdutos.Cells[coQtdeDevolverEntregues, i]);
    vItens[High(vItens)].QuantidadeDevolvidosPendencias := SFormatDouble(sgProdutos.Cells[coQtdeDevolverPendencias, i]);
    vItens[High(vItens)].QuantidadeDevolvSemPrevisao    := SFormatDouble(sgProdutos.Cells[coQtdeDevolverSemPrevisao, i]);

    vItens[High(vItens)].ValorBruto                     := SFormatDouble(sgProdutos.Cells[coValorDevolucao, i]);
    vItens[High(vItens)].ValorDesconto                  := SFormatDouble(sgProdutos.Cells[coValorDescontoDevolucao, i]);
    vItens[High(vItens)].ValorOutrasDespesas            := SFormatDouble(sgProdutos.Cells[coValorOutrasDespDevoluc, i]);
    vItens[High(vItens)].PrecoUnitario                  := SFormatDouble(sgProdutos.Cells[coValorUnitario, i]);
    vItens[High(vItens)].ValorFrete                     := SFormatDouble(sgProdutos.Cells[coValorFreteDevolucao, i]);

    if vItens[High(vItens)].QuantidadeDevolvidosEntregues > 0 then
      _Biblioteca.AddNoVetorSemRepetir(vProdutosIdsJaEntregues, vItens[High(vItens)].ProdutoId);

    vItens[High(vItens)].ValorLiquido :=
      vItens[High(vItens)].ValorBruto -
      vItens[High(vItens)].ValorDesconto +
      vItens[High(vItens)].ValorOutrasDespesas +
//      vItens[High(vItens)].ValorST +
//      vItens[High(vItens)].ValorIPI +
      vItens[High(vItens)].ValorFrete;


    if vItens[High(vItens)].QuantidadeDevolvidosEntregues > 0 then begin
      SetLength(vItensEntregasIds, Length(vItensEntregasIds) + 1);
      vItensEntregasIds[High(vItensEntregasIds)] := vItens[High(vItens)].ProdutoId;
    end;
  end;

  if vProdutosIdsJaEntregues <> nil then begin
    if _Orcamentos.ExistemPendenciasConfirmacaoEntrega( Sessao.getConexaoBanco, eID.AsInt, vProdutosIdsJaEntregues ) then begin
      _Biblioteca.Exclamar('Existem produtos que ainda n�o foram confirmadas as suas sa�das, verifique antes de continuar!');
      Abort;
    end;

    if not _Orcamentos.ExistemEntregasRealizadas( Sessao.getConexaoBanco, eID.AsInt, vProdutosIdsJaEntregues ) then begin
      _Biblioteca.Exclamar(
        'N�o existem retiradas/entregas realizadas para um ou mais produtos selecionados para devolu��o, verifique se houve o cancelamento da retirada/entrega!'
      );
      Abort;
    end;
  end
  else begin
    vRetBanco := _Devolucoes.PodeGerarDevolucao( Sessao.getConexaoBanco, eID.AsInt );
    if vRetBanco.TeveErro then begin
      _Biblioteca.Exclamar( vRetBanco.MensagemErro );
      Abort;
    end;
  end;

  vClienteId := 0;
  if eCliente.IdInformacao = Sessao.getParametros.cadastro_consumidor_final_id then begin
    _Biblioteca.Informar('A venda foi realizada para um consumidor final, ser� necess�rio escolher um cadastro de cliente que n�o seja consumidor final.');
    vCliente := RecClientes(PesquisaClientes.Pesquisar(True));
    if vCliente = nil then begin
      _Biblioteca.NenhumRegistro;
      Abort;
    end;

    if vCliente.cadastro_id = Sessao.getParametros.cadastro_consumidor_final_id then begin
      _Biblioteca.Exclamar('N�o � permitido utilizar o cadastro do consumidor final!');
      Abort;
    end;

    vClienteId := vCliente.cadastro_id;
  end;

  if vItensEntregasIds <> nil then begin
    vMaiorDataEntrega := _Devolucoes.MaiorDataEntrega(Sessao.getConexaoBanco, eID.AsInt, vItensEntregasIds);
    vQtdeDiasDevolucao := Trunc(Sessao.getData - vMaiorDataEntrega);
    if (Sessao.getData - vMaiorDataEntrega) > Sessao.getParametros.QtdeMaximaDiasDevolucao  then begin
      if Sessao.getParametrosEmpresa.PermitirDevolucaoAposPrazo = 'N' then begin
        _Biblioteca.Exclamar('N�o � permitido fazer devolu��o com quantidade de dias maior que o permitido!');
        Abort;
      end;
    end;
  end;

  vRetBanco :=
    _Devolucoes.AtualizarDevolucoes(
      Sessao.getConexaoBanco,
      0,
      Sessao.getEmpresaLogada.EmpresaId,
      eID.AsInt,
      vClienteId,
      SFormatDouble(stValorProdutos.Caption),
      SFormatDouble(stTotalDevolucao.Caption),
      SFormatDouble(stTotalDesconto.Caption),
      SFormatDouble(stOutrasDespesas.Caption),
      SFormatDouble(stValorFrete.Caption),
      eObservacoes.Text,
      ckDevolverFrete.CheckedStr,
      ckDevolverOutrasDespesas.CheckedStr,
      eMotivoDevolucao.Text,
      vProdutosIdsJaEntregues <> nil,
      Sessao.getUsuarioLogado.DevolucaoSomenteFiscal = 'S',
      vItens,
      vQtdeDiasDevolucao
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  if vProdutosIdsJaEntregues = nil then
    _Biblioteca.Informar('Devolu��o efetuada com sucesso. C�digo: ' + NFormat(vRetBanco.AsInt) + '.')
  else begin
    _Biblioteca.Informar('Devolu��o efetuada com sucesso. C�digo: ' + NFormat(vRetBanco.AsInt) + '.' + #13 + 'Fa�a a confirma��o na tela de "Confirma��o de devolu��es".');
  end;

  if (vProdutosIdsJaEntregues = nil) or (Sessao.getParametrosEmpresa.PermitirDevolucaoAposPrazo = 'N') then
    ImpressaoComprovanteDevolucaoGrafico.Imprimir(vRetBanco.AsInt);
end;

procedure TFormDevolucaoProdutos.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eCliente,
    eVendedor,
    eDataCadastro,
    eDataRecebimento,
    eObservacoes,
    eMotivoDevolucao,
    ckDevolverFrete,
    ckDevolverOutrasDespesas,
    sgProdutos,
    sbInfoPedido,
    //sbInfoCliente,
    sbInfoVendedor,
    stValorProdutos,
    stValorFrete,
    stOutrasDespesas,
    sbDevolucaoTotal,
    stTotalDesconto,
    stTotalSubstitucaoTributaria,
    stTotalIPI,
    stTotalDevolucao],
    pEditando
  );

  if pEditando then begin
    sgProdutos.Col := coQuantidadeVendida;

    ckDevolverOutrasDespesas.Checked := True;
    ckDevolverFrete.Checked          := True;

    SetarFoco(sgProdutos);
  end;
end;

procedure TFormDevolucaoProdutos.PesquisarRegistro;
var
  vRetTela: TRetornoTelaFinalizar<RecOrcamentos>;
begin
  vRetTela := Pesquisa.Orcamentos.Pesquisar(tpVendasDevolucao);
  if vRetTela.BuscaCancelada then
    Exit;

  inherited;
  PreencherRegistro(vRetTela.Dados);
end;

procedure TFormDevolucaoProdutos.PreencherRegistro(pOrcamento: RecOrcamentos);
var
  i: Integer;
  vItens: TArray<RecOrcamentoItens>;
begin
  vItens := _OrcamentosItens.BuscarOrcamentosItens(Sessao.getConexaoBanco, 1, [pOrcamento.orcamento_id]);
  if vItens = nil then begin
    Exclamar('N�o foi encontrado nenhum produto para devolu��o no pedido digitado!');
    Modo(False);
    Exit;
  end;

  if _Devolucoes.ExistemDevolucoesPendentes(Sessao.getConexaoBanco, pOrcamento.orcamento_id) then begin
    _Biblioteca.Exclamar('N�o � permitida esta opera��o enquanto houver pend�ncias de devolu��es para o pedido ' + NFormat(pOrcamento.orcamento_id) + '!');
    Modo(False);
    Abort;
  end;

  eID.SetInformacao(pOrcamento.orcamento_id);
  eCliente.SetInformacao(pOrcamento.cliente_id, pOrcamento.nome_cliente);
  eVendedor.SetInformacao(pOrcamento.vendedor_id, pOrcamento.nome_vendedor);
  eDataCadastro.AsData := pOrcamento.data_cadastro;
  eDataRecebimento.AsData := pOrcamento.data_hora_recebimento;

  for i := Low(vItens) to High(vItens) do begin
    sgProdutos.Cells[coProdutoId, i + 1]               := NFormat(vItens[i].produto_id);
    sgProdutos.Cells[coNome, i + 1]                    := vItens[i].nome;
    sgProdutos.Cells[coMarca, i + 1]                   := vItens[i].nome_marca;
    sgProdutos.Cells[coUnidade, i + 1]                 := vItens[i].unidade_venda;
    sgProdutos.Cells[coQuantidadeVendida, i + 1]       := NFormat(vItens[i].quantidade);

    sgProdutos.Cells[coQtdeEntregues, i + 1]           := NFormatNEstoque(vItens[i].QuantidadeRetirados + vItens[i].QuantidadeEntregues);
    sgProdutos.Cells[coQtdeDevolverEntregues, i + 1]   := '';

    sgProdutos.Cells[coQtdePendencias, i + 1]          := NFormatNEstoque(vItens[i].QuantidadeRetirar + vItens[i].QuantidadeEntregar);
    sgProdutos.Cells[coQtdeDevolverPendencias, i + 1]  := '';

    sgProdutos.Cells[coQtdeSemPrevisao, i + 1]         := NFormatNEstoque(vItens[i].QuantidadeSemPrevisao);
    sgProdutos.Cells[coQtdeDevolverSemPrevisao, i + 1] := '';

    sgProdutos.Cells[coCompletamenteDevolvido, i + 1]  := ToChar(NFormat(vItens[i].quantidade) = NFormat(vItens[i].QuantidadeDevolvEntregues));
    sgProdutos.Cells[coValorUnitario, i + 1]           := NFormat(vItens[i].preco_unitario * pOrcamento.IndicePedidos);

    sgProdutos.Cells[coValorDescontoUnitario, i + 1]   := NFormat(vItens[i].valor_total_desconto / vItens[i].quantidade * pOrcamento.IndicePedidos, 7);
    sgProdutos.Cells[coValorOutrasDespUnitario, i + 1] := NFormat(vItens[i].valor_total_outras_despesas / vItens[i].quantidade * pOrcamento.IndicePedidos, 7);
    sgProdutos.Cells[coValorFreteUnitario, i + 1]      := NFormat(vItens[i].ValorTotalFrete / vItens[i].quantidade * pOrcamento.IndicePedidos, 7);

    sgProdutos.Cells[coItemId, i + 1]                  := NFormat(vItens[i].item_id);
    sgProdutos.Cells[coMultiploVenda, i + 1]           := NFormatNEstoque(vItens[i].multiplo_venda);
  end;
  sgProdutos.SetLinhasGridPorTamanhoVetor( Length(vItens) );
  sgProdutos.Options := sgProdutos.Options + [goEditing];

  for i := sgProdutos.FixedRows to sgProdutos.RowCount -1 do begin
    if sgProdutos.Cells[coCompletamenteDevolvido, i] = 'N' then begin
      sgProdutos.Row := 1;
      Break;
    end;
  end;
end;

procedure TFormDevolucaoProdutos.sbDevolucaoTotalClick(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  for i := sgProdutos.FixedRows to sgProdutos.RowCount -1 do begin
    if sgProdutos.Cells[coCompletamenteDevolvido, i] = 'S' then
      Continue;

//    sgProdutos.Cells[coQuantidadeDevolver, i] := NFormat(SFormatDouble(sgProdutos.Cells[coQuantidadeVendida, i]) - SFormatDouble(sgProdutos.Cells[coCompletamenteDevolvido, i]));
  end;
  CalcularDevolucao;
end;

procedure TFormDevolucaoProdutos.sbGravarClick(Sender: TObject);
begin
  CalcularDevolucao;
  inherited;
end;

procedure TFormDevolucaoProdutos.sbInfoPedidoClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(eID.AsInt);
end;

procedure TFormDevolucaoProdutos.sgProdutosArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if not ACol in[
    coQtdeDevolverEntregues,
    coQtdeDevolverPendencias,
    coQtdeDevolverSemPrevisao]
  then
    Exit;

  if SFormatCurr(TextCell) > SFormatCurr(sgProdutos.Cells[ACol - 1, ARow]) then begin
    Exclamar('A quantidade a ser devolvida n�o pode ser maior que o saldo restante!');
    sgProdutos.Cells[ACol, ARow] := '';
    SetarFoco(sgProdutos);
    TextCell := '';
    Exit;
  end;

  TextCell := NFormatNEstoque(SFormatDouble(TextCell));
  if TextCell = '' then begin
    CalcularDevolucao;
    Exit;
  end;

  if not ValidarMultiplo(SFormatDouble(sgProdutos.Cells[ACol, sgProdutos.Row]), SFormatDouble(sgProdutos.Cells[coMultiploVenda, sgProdutos.Row])) then begin
    SetarFoco(sgProdutos);
    TextCell := '';
    Exit;
  end;

  CalcularDevolucao;
end;

procedure TFormDevolucaoProdutos.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coProdutoId,
    coQuantidadeVendida,
    coQtdeEntregues,
    coQtdeDevolverEntregues,
    coQtdePendencias,
    coQtdeDevolverPendencias,
    coQtdeSemPrevisao,
    coQtdeDevolverSemPrevisao]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormDevolucaoProdutos.sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in[
    coQtdeEntregues,
    coQtdeDevolverEntregues]
  then begin
    AFont.Color := _Biblioteca.coCorFonteEdicao1;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao1;
  end
  else if ACol in[
    coQtdePendencias,
    coQtdeDevolverPendencias]
  then begin
    AFont.Color := _Biblioteca.coCorFonteEdicao2;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
  end
  else if ACol in[
    coQtdeSemPrevisao,
    coQtdeDevolverSemPrevisao]
  then begin
    AFont.Color := _Biblioteca.coCorFonteEdicao3;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
  end;
       
end;

procedure TFormDevolucaoProdutos.sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgProdutos.Cells[coCompletamenteDevolvido, ARow] <> 'N' then
    sgProdutos.Options := sgProdutos.Options - [goEditing]
  else if ACol in[
    coQtdeDevolverEntregues,
    coQtdeDevolverPendencias,
    coQtdeDevolverSemPrevisao]
  then
    sgProdutos.Options := sgProdutos.Options + [goEditing]
  else
    sgProdutos.Options := sgProdutos.Options - [goEditing];
end;

procedure TFormDevolucaoProdutos.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  vNenhumDevolver: Boolean;
begin
  inherited;
  vNenhumDevolver := True;
  for i := sgProdutos.FixedRows to sgProdutos.RowCount -1 do begin
    if
      (sgProdutos.Cells[coQtdeDevolverEntregues, i] = '') and
      (sgProdutos.Cells[coQtdeDevolverPendencias, i] = '') and
      (sgProdutos.Cells[coQtdeDevolverSemPrevisao, i] = '')
    then
      Continue;

    vNenhumDevolver := False;
    Break;
  end;

  if vNenhumDevolver then begin
    Exclamar('N�o foi selecionado nenhum produto para devolu��o, verifique!');
    SetarFoco(sgProdutos);
    Abort;
  end;

  if _Orcamentos.PedidoFazParteAcumuladoPendente( Sessao.getConexaoBanco, eID.AsInt ) then begin
    Exclamar('Este pedido faz parte de um acumulado ainda pendente de recebimento, por favor fa�a o recebimento antes de continuar!');
    SetarFoco(sgProdutos);
    Abort;
  end;
end;

end.
