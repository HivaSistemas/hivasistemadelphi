unit CadastroCentroCusto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _CentroCustos, _RecordsEspeciais, PesquisaCentroCustos,
  CheckBoxLuka;

type
  TFormCadastroCentroCustos = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
  private
    procedure PreencherRegistro(pCentro: RecCentrosCustos);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroCentroCustos }

procedure TFormCadastroCentroCustos.BuscarRegistro;
var
  vDados: TArray<RecCentrosCustos>;
begin
  vDados := _CentroCustos.BuscarCentroCustos(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vDados = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vDados[0]);
end;

procedure TFormCadastroCentroCustos.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _CentroCustos.ExcluirCentro(Sessao.getConexaoBanco, eId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroCentroCustos.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _CentroCustos.AtualizarCentroCusto(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      _Biblioteca.ToChar(ckAtivo)
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroCentroCustos.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eNome,
    ckAtivo],
    pEditando
  );

  if pEditando then
    SetarFoco(eNome);
end;

procedure TFormCadastroCentroCustos.PesquisarRegistro;
var
  vDados: RecCentrosCustos;
begin
  vDados := RecCentrosCustos(PesquisaCentroCustos.PesquisarCentro);
  if vDados = nil then
    Exit;

  inherited;
  PreencherRegistro(vDados);
end;

procedure TFormCadastroCentroCustos.PreencherRegistro(pCentro: RecCentrosCustos);
begin
  eID.AsInt       := pCentro.CentroCustoId;
  eNome.Text      := pCentro.nome;
  ckAtivo.Checked := (pCentro.ativo = 'S');

  pCentro.Free;
end;

procedure TFormCadastroCentroCustos.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('O nome da rota n�o foi informada corretamente, verifique!');
    SetarFoco(eNome);
    Abort;
  end;
end;

end.
