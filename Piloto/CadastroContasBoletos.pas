unit CadastroContasBoletos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsCadastros, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, _RecordsEspeciais, PesquisaContasBoletos, _ContasBoletos,
  RadioGroupLuka, ComboBoxLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameEmpresas, CheckBoxLuka, FrameFuncionarios, _ContasFuncAutorizados,
  Vcl.ExtDlgs, FrameImagem,  ACBrBoleto, ACBrBoletoConversao, ACBrBoletoFCFR,
  SpeedButtonLuka;

type
  TFrmCadastroContasBoletos = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    edtDescricao: TEditLuka;
    lb2: TLabel;
    edtNumeroAgencia: TEditLuka;
    lb3: TLabel;
    edtDigitoConta: TEditLuka;
    lb5: TLabel;
    lb8: TLabel;
    edtNossoNumeroInicial: TEditLuka;
    lb9: TLabel;
    edtProximoNossoNumero: TEditLuka;
    lb10: TLabel;
    edtNossoNumeroFinal: TEditLuka;
    Label3: TLabel;
    edtDigitoAgencia: TEditLuka;
    lb4: TLabel;
    edtCarteira: TEditLuka;
    lb11: TLabel;
    edtNumeroRemessaBoleto: TEditLuka;
    edtNumeroBanco: TEditLuka;
    Label2: TLabel;
    edtNomeBanco: TEditLuka;
    Label4: TLabel;
    edtNumeroConta: TEditLuka;
    Label5: TLabel;
    edtCedente: TEditLuka;
    Label6: TLabel;
    edtDiasProtesto: TEditLuka;
    Label7: TLabel;
    edtDiasBaixaDevolucao: TEditLuka;
    Label8: TLabel;
    edtDIasLimitePgto: TEditLuka;
    Label9: TLabel;
    edtCodigoTransmissao: TEditLuka;
    Label10: TLabel;
    edtConvenio: TEditLuka;
    Label11: TLabel;
    edtDVAgenciaConta: TEditLuka;
    Label12: TLabel;
    edtModalidade: TEditLuka;
    Label13: TLabel;
    edtOperacao: TEditLuka;
    Label14: TLabel;
    edtInstrucao1: TEditLuka;
    Label15: TLabel;
    edtInstrucao2: TEditLuka;
    Label16: TLabel;
    edtInstrucao3: TEditLuka;
    Label17: TLabel;
    edtEspecieMoeda: TEditLuka;
    cbxTipoCarteira: TComboBoxLuka;
    Label18: TLabel;
    cbxCaracteristicaTitulo: TComboBoxLuka;
    Label19: TLabel;
    cbxRespEmissao: TComboBoxLuka;
    Label20: TLabel;
    cbxLayout: TComboBoxLuka;
    Label21: TLabel;
    cbxModeloImpressao: TComboBoxLuka;
    Label22: TLabel;
    cbxEspecie: TComboBoxLuka;
    Label23: TLabel;
    ckAceite: TCheckBoxLuka;
    lblLocalPgto: TLabel;
    edtLocalPgto: TEditLuka;
    Label24: TLabel;
    edtNomeArquivoRem: TEditLuka;
    cbxExtArquivoRemessa: TComboBoxLuka;
    Label25: TLabel;
    Label26: TLabel;
    edtVersaoLayout: TEditLuka;
    Label27: TLabel;
    edtVersaoLote: TEditLuka;
    mmTextoLivre: TMemo;
    Label28: TLabel;
    FrLogo: TFrImagem;
    sbVisualizar: TSpeedButton;
    mmLayoutImpressao: TMemo;
    Label29: TLabel;
    edtDiasMultaJuros: TEditLuka;
    Label30: TLabel;
    cbxTipoNegativacao: TComboBoxLuka;
    procedure FormCreate(Sender: TObject);
    procedure sbVisualizarClick(Sender: TObject);
    procedure edtNumeroBancoExit(Sender: TObject);
  private
    ACBrBoletoComp: TACBrBoleto;
    ACBrBoletoFR: TACBrBoletoFCFR;
    procedure CarregarBoleto;
    procedure PreencherRegistro(pContaBoletos: RecContasBoletos);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroBancosCaixas }

procedure TFrmCadastroContasBoletos.BuscarRegistro;
var
  vContasBoletos: TArray<RecContasBoletos>;
begin
  vContasBoletos := _ContasBoletos.BuscarContasBoletos(Sessao.getConexaoBanco, 0, [eId.Text]);

  inherited;
  if vContasBoletos <> nil then
    PreencherRegistro(vContasBoletos[0]);
end;

procedure TFrmCadastroContasBoletos.CarregarBoleto;
var
  ACBrTitulo : TACBrTitulo;
  vDiretorioSistema: string;
  vEmpresas: TArray<RecEmpresas>;
begin
  vEmpresas := Sessao.getEmpresas;
  ACBrBoletoComp.Banco.TipoCobranca := ACBrBoletoComp.GetTipoCobranca(edtNumeroBanco.AsInt);

  if Trim(edtVersaoLayout.Text) <> '' then
  begin
    ACBrBoletoComp.Banco.LayoutVersaoArquivo := edtVersaoLayout.AsInt;
  end;

  if Trim(edtVersaoLote.Text) <> '' then
  begin
    ACBrBoletoComp.Banco.LayoutVersaoLote    := edtVersaoLote.AsInt;
  end;

  ACBrBoletoComp.Cedente.Agencia                       := edtNumeroAgencia.Text;
  ACBrBoletoComp.Cedente.AgenciaDigito                 := edtDigitoAgencia.Text;
  ACBrBoletoComp.Cedente.Conta                         := edtNumeroConta.Text;
  ACBrBoletoComp.Cedente.ContaDigito                   := edtDigitoConta.Text;
  ACBrBoletoComp.Cedente.CodigoCedente                 := edtCedente.Text;
  ACBrBoletoComp.Cedente.CodigoTransmissao             := edtCodigoTransmissao.Text;
  ACBrBoletoComp.Cedente.Convenio                      := edtConvenio.Text;
  ACBrBoletoComp.Cedente.DigitoVerificadorAgenciaConta := edtDVAgenciaConta.Text;
  ACBrBoletoComp.Cedente.Modalidade                    := edtModalidade.Text;
  ACBrBoletoComp.Cedente.Operacao                      := edtOperacao.Text;
  ACBrBoletoComp.LayoutRemessa                         := Iif(Trim(cbxLayout.Text).ToUpper = 'CNAB240', c240, c400);


  if not Trim(edtLocalPgto.Text).IsEmpty then
  begin
    ACBrBoletoComp.Banco.LocalPagamento := edtLocalPgto.Text;
  end;

  if (cbxTipoCarteira.GetValor = 'Simples') then
  begin
    ACBrBoletoComp.Cedente.TipoCarteira := tctSimples;
  end
  else if (cbxTipoCarteira.GetValor = 'Registrada') then
  begin
    ACBrBoletoComp.Cedente.TipoCarteira := tctRegistrada;
  end
  else if (cbxTipoCarteira.GetValor = 'Eletronica') then
  begin
    ACBrBoletoComp.Cedente.TipoCarteira := tctEletronica;
  end;

  if (cbxCaracteristicaTitulo.GetValor = 'Simples') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcSimples;
  end
  else if (cbxCaracteristicaTitulo.GetValor = 'Vinculada') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcVinculada;
  end
  else if (cbxCaracteristicaTitulo.GetValor = 'Caucionada') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcCaucionada;
  end
  else if (cbxCaracteristicaTitulo.GetValor = 'Descontada') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcDescontada;
  end
  else if (cbxCaracteristicaTitulo.GetValor = 'Vendor') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcVendor;
  end
  else if (cbxCaracteristicaTitulo.GetValor = 'Direta') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcDireta;
  end
  else if (cbxCaracteristicaTitulo.GetValor = 'Simples (R�pida com Registro)') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcSimplesRapComReg;
  end
  else if (cbxCaracteristicaTitulo.GetValor = 'Caucionada (R�pida com Registro)') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcCaucionadaRapComReg;
  end
  else if (cbxCaracteristicaTitulo.GetValor = 'Direta Especial') then
  begin
    ACBrBoletoComp.Cedente.CaracTitulo := tcDiretaEspecial;
  end;

  if (cbxRespEmissao.GetValor = 'Cliente') then
  begin
    ACBrBoletoComp.Cedente.ResponEmissao := tbCliEmite;
  end
  else if (cbxRespEmissao.GetValor = 'Banco') then
  begin
    ACBrBoletoComp.Cedente.ResponEmissao := tbBancoEmite;
  end
  else if (cbxRespEmissao.GetValor = 'Banco reemite') then
  begin
    ACBrBoletoComp.Cedente.ResponEmissao := tbBancoReemite;
  end
  else if (cbxRespEmissao.GetValor = 'Banco n�o reemite') then
  begin
    ACBrBoletoComp.Cedente.ResponEmissao := tbBancoNaoReemite;
  end
  else if (cbxRespEmissao.GetValor = 'Banco pr� emite') then
  begin
    ACBrBoletoComp.Cedente.ResponEmissao := tbBancoPreEmite;
  end;

  ACBrBoletoComp.Cedente.CNPJCPF         := RetornaNumeros(vEmpresas[0].Cnpj);
  ACBrBoletoComp.Cedente.Nome            := vEmpresas[0].RazaoSocial;
  ACBrBoletoComp.Cedente.FantasiaCedente := vEmpresas[0].NomeFantasia;
  ACBrBoletoComp.Cedente.Logradouro      := vEmpresas[0].Logradouro;
  ACBrBoletoComp.Cedente.Complemento     := vEmpresas[0].Complemento;
  ACBrBoletoComp.Cedente.Bairro          := vEmpresas[0].NomeBairro;
  ACBrBoletoComp.Cedente.CEP             := RetornaNumeros(vEmpresas[0].CEP);
  ACBrBoletoComp.Cedente.Cidade          := vEmpresas[0].NomeCidade;
  ACBrBoletoComp.Cedente.Telefone        := RetornaNumeros(vEmpresas[0].TelefonePrincipal);
  ACBrBoletoComp.Cedente.UF              := vEmpresas[0].EstadoId;

  ACBrBoletoComp.ACBrBoletoFC := ACBrBoletoFR;

  vDiretorioSistema := Sessao.getCaminhoPastaSistema;
  vDiretorioSistema := IncludeTrailingPathDelimiter(vDiretorioSistema);
  ForceDirectories(vDiretorioSistema + 'Boletos');
  mmLayoutImpressao.Lines.SaveToFile(vDiretorioSistema + 'Boletos\Boleto.fr3',TEncoding.UTF8);
  ForceDirectories(vDiretorioSistema + 'Boletos\Imagens');
  FrLogo.imFoto.Picture.SaveToFile(vDiretorioSistema +'Boletos\Imagens\' +  edtNumeroBanco.AsInt.ToString.PadLeft(3,'0') + '.bmp');

  ACBrBoletoFR.FastReportFile := vDiretorioSistema + 'Boletos\Boleto.fr3';
  ACBrBoletoFR.DirLogo := vDiretorioSistema + 'Boletos\Imagens';

  ACBrBoletoFR.CalcularNomeArquivoPDFIndividual := False;

  if (cbxModeloImpressao.GetValor = 'Padr�o') then
  begin
    ACBrBoletoFR.LayOut := lPadrao;
  end
  else if (cbxModeloImpressao.GetValor = 'Carn�') then
  begin
    ACBrBoletoFR.LayOut := lCarne;
  end
  else if (cbxModeloImpressao.GetValor = 'Fatura') then
  begin
    ACBrBoletoFR.LayOut := lFatura;
  end
  else if (cbxModeloImpressao.GetValor = 'Padr�o entrega') then
  begin
    ACBrBoletoFR.LayOut := lPadraoEntrega;
  end
  else if (cbxModeloImpressao.GetValor = 'Recibo topo') then
  begin
    ACBrBoletoFR.LayOut := lReciboTopo;
  end
  else if (cbxModeloImpressao.GetValor = 'Padr�o entrega 2') then
  begin
    ACBrBoletoFR.LayOut := lPadraoEntrega2;
  end
  else if (cbxModeloImpressao.GetValor = 'Fatura detalhe') then
  begin
    ACBrBoletoFR.LayOut := lFaturaDetal;
  end
  else if (cbxModeloImpressao.GetValor = 'T�rmica 80mm') then
  begin
    ACBrBoletoFR.LayOut := lTermica80mm;
  end;
  ACBrTitulo     := ACBrBoletoComp.CriarTituloNaLista;
  with ACBrTitulo do
  begin
    LocalPagamento    := edtLocalPgto.Text;
    Vencimento        := now;
    DataDocumento     := Vencimento - 3;
    NumeroDocumento   := '9999991';
    EspecieDoc        := cbxESPECIE.GetValor;
    Aceite            := Iif(ckAceite.Checked, atSim, atNao);
    DataProcessamento := Now;
    Carteira          := edtCarteira.Text;
    NossoNumero       := '9';
    ValorDocumento    := 1.23;
    SeuNumero         := '6464625252452';
    Sacado.NomeSacado := 'NOME DO SACADO';
    Sacado.CNPJCPF    := '00000000000';
    Sacado.Logradouro := 'ENDERE�O DO SACADO';
    Sacado.Numero     := '100';
    Sacado.Bairro     := 'BAIRRO DO SACADO';
    Sacado.Cidade     := 'CIDADE DO SACADO';
    Sacado.UF         := 'GO';
    Sacado.CEP        := '74000000';
    DiasDeProtesto    := edtDiasProtesto.AsInt;
    DataNegativacao   := now + 1;
    DataProtesto      := now + 1;
    ValorAbatimento   := 0.23;
    ValorDesconto     := 0.01;
    ValorDesconto2    := 0.02;
    ValorMoraJuros    := 0.03;
    PercentualMulta   := 2;
    DataLimitePagto   := now + 1;

    Mensagem.Add(mmTextoLivre.Lines.Text);

    Instrucao1        := edtInstrucao1.Text;
    Instrucao2        := edtInstrucao2.Text;
    Instrucao3        := edtInstrucao3.Text;

    Detalhamento.Add('Mensagem do recibo 1');
    Detalhamento.Add('Mensagem do recibo 2');
    Detalhamento.Add('Mensagem do recibo 3');
    Detalhamento.Add('Mensagem do recibo 4');
    Detalhamento.Add('Mensagem do recibo 5');

    if cbxTipoNegativacao.GetValor = 'Nenhum' then
      CodigoNegativacao := cnNenhum
    else if cbxTipoNegativacao.GetValor = 'Dias Corrido' then
      CodigoNegativacao := cnProtestarCorrido
    else if cbxTipoNegativacao.GetValor = 'Dias Uteis' then
      CodigoNegativacao := cnProtestarUteis
    else if cbxTipoNegativacao.GetValor = 'N�o Protestar' then
      CodigoNegativacao := cnNaoProtestar
    else if cbxTipoNegativacao.GetValor = 'Negativar' then
      CodigoNegativacao := cnNegativar
    else if cbxTipoNegativacao.GetValor = 'N�o Negativar' then
      CodigoNegativacao := cnNaoNegativar;

  end;
end;

procedure TFrmCadastroContasBoletos.edtNumeroBancoExit(Sender: TObject);
var
  vCodBanco: Integer;
begin
  if TryStrToInt(edtNumeroBanco.Text, vCodBanco) then
  begin
    edtNomeBanco.Text := RetornaNomeBanco(vCodBanco);
    if Trim(edtNomeBanco.Text).IsEmpty then
    begin
      Exclamar('Banco inv�lido!');
      edtNumeroBanco.Clear;
      edtNumeroBanco.SetFocus;
      Exit;
    end;
  end;
end;

procedure TFrmCadastroContasBoletos.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _ContasBoletos.ExcluirContasBoletos(Sessao.getConexaoBanco, StrToInt(eId.Text));
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFrmCadastroContasBoletos.FormCreate(Sender: TObject);
begin
  inherited;
  OnKeyDown := ProximoCampo;
  FrLogo.DefaultExt := '*.jpg';
  FrLogo.Filter := 'Bitmaps (*.bmp)|*.bmp';
  mmLayoutImpressao.Lines.Text :=  RetirarAcentos(mmLayoutImpressao.Lines.Text);
end;

procedure TFrmCadastroContasBoletos.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vStream : TMemoryStream;
begin
  inherited;
  vStream := TMemoryStream.Create;
  mmLayoutImpressao.Lines.SaveToStream(vStream,TEncoding.UTF8);
  vRetBanco :=
    _ContasBoletos.AtualizarContasBoletos(
      Sessao.getConexaoBanco,
      eId.AsInt,
      _Biblioteca.ToChar(ckAceite),
      cbxCaracteristicaTitulo.Text,
      edtCarteira.Text,
      edtConvenio.Text,
      edtDescricao.Text,
      edtDiasBaixaDevolucao.AsInt,
      edtDIasLimitePgto.AsInt,
      edtDiasProtesto.AsInt,
      edtDiasMultaJuros.AsInt,
      edtDigitoAgencia.Text,
      cbxEspecie.Text,
      edtEspecieMoeda.Text,
      cbxExtArquivoRemessa.Text,
      edtNossoNumeroFinal.AsInt,
      edtNossoNumeroInicial.AsInt,
      edtInstrucao1.Text,
      edtInstrucao2.Text,
      edtInstrucao3.Text,
      cbxLayout.Text,
      edtLocalPgto.Text,
      edtModalidade.Text,
      cbxModeloImpressao.Text,
      edtNomeArquivoRem.Text,
      edtNumeroAgencia.AsInt,
      edtNumeroBanco.Text,
      edtNumeroConta.AsInt,
      edtOperacao.Text,
      edtProximoNossoNumero.AsInt,
      cbxRespEmissao.Text,
      edtNumeroRemessaBoleto.AsInt,
      Trim(mmTextoLivre.Text),
      cbxTipoCarteira.Text,
      edtVersaoLayout.Text,
      edtVersaoLote.Text,
      edtCodigoTransmissao.Text,
      edtDigitoConta.Text,
      edtCedente.Text,
      edtDVAgenciaConta.Text,
      FrLogo.getImagem,
      vStream,
      cbxTipoNegativacao.Text
    );

  vStream.Free;

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFrmCadastroContasBoletos.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    edtDescricao,
    ckAceite,
    cbxCaracteristicaTitulo,
    edtCarteira,
    edtConvenio,
    edtDescricao,
    edtDiasBaixaDevolucao,
    edtDIasLimitePgto,
    edtDiasProtesto,
    edtDiasMultaJuros,
    edtDigitoAgencia,
    cbxEspecie,
    edtEspecieMoeda,
    cbxExtArquivoRemessa,
    edtNossoNumeroFinal,
    edtNossoNumeroInicial,
    edtInstrucao1,
    edtInstrucao2,
    edtInstrucao3,
    cbxLayout,
    edtLocalPgto,
    edtModalidade,
    cbxModeloImpressao,
    edtNomeArquivoRem,
    edtNumeroAgencia,
    edtNumeroBanco,
    edtNumeroConta,
    edtOperacao,
    edtProximoNossoNumero,
    cbxRespEmissao,
    edtNumeroRemessaBoleto,
    mmTextoLivre,
    cbxTipoCarteira,
    edtVersaoLayout,
    edtVersaoLote,
    edtCodigoTransmissao,
    edtDigitoConta,
    edtCedente,
    edtDVAgenciaConta,
    FrLogo,
    sbVisualizar,
    edtNomeBanco,
    cbxTipoNegativacao],
    pEditando
  );


  if pEditando then begin
    SetarFoco(edtDescricao);
    ckAtivo.Checked := True;
  end;
end;

procedure TFrmCadastroContasBoletos.PesquisarRegistro;
var
  vBancoBoletos: RecContasBoletos;
begin
  vBancoBoletos := RecContasBoletos(PesquisaContasBoletos.PesquisarContasBoletos('', False));
  if vBancoBoletos = nil then
    Exit;

  inherited;
  PreencherRegistro(vBancoBoletos);
end;

procedure TFrmCadastroContasBoletos.PreencherRegistro(pContaBoletos: RecContasBoletos);
var
  i: Integer;
begin
  eID.AsInt                      := pContaBoletos.ContaboletoId;
  edtDescricao.Text              := pContaBoletos.Descricao;
  ckAceite.Checked               := (pContaBoletos.Aceite = 'S');
  cbxCaracteristicaTitulo.SetIndicePorValor(pContaBoletos.Caracteristica);
  edtCarteira.Text               := pContaBoletos.Carteira;
	edtConvenio.Text               := pContaBoletos.Convenio;
	edtDiasBaixaDevolucao.AsInt    := pContaBoletos.DiasBaixaDevolucao;
	edtDIasLimitePgto.AsInt        := pContaBoletos.DiasLimitePagamento;
	edtDiasProtesto.AsInt          := pContaBoletos.DiasProtesto;
  edtDiasMultaJuros.AsInt        := pContaBoletos.DiasMultaJuros;
	edtDigitoAgencia.Text          := pContaBoletos.DigitoAgencia;
	cbxEspecie.SetIndicePorValor(pContaBoletos.Especie);
	edtEspecieMoeda.Text           := pContaBoletos.EspecieMoeda;
	cbxExtArquivoRemessa.SetIndicePorValor(pContaBoletos.ExtArquivoRemessa);
	edtNossoNumeroFinal.AsInt      := pContaBoletos.FimNossoNumero;
	edtNossoNumeroInicial.AsInt    := pContaBoletos.InicioNossoNumero;
	edtInstrucao1.Text             := pContaBoletos.Instrucao1;
  edtInstrucao2.Text             := pContaBoletos.Instrucao2;
  edtInstrucao3.Text             := pContaBoletos.Instrucao3;
	cbxLayout.SetIndicePorValor(pContaBoletos.Layout);
	edtLocalPgto.Text              := pContaBoletos.LocalPagamento;
	edtModalidade.Text             := pContaBoletos.Modalidade;
	cbxModeloImpressao.SetIndicePorValor(pContaBoletos.ModeloImpressao);
	edtNomeArquivoRem.Text         := pContaBoletos.NomeArquivoRemessa;
	edtNumeroAgencia.AsInt         := pContaBoletos.NumeroAgencia;
	edtNumeroBanco.Text            := pContaBoletos.NumeroBanco;
	edtNumeroConta.AsInt           := pContaBoletos.NumeroConta;
	edtOperacao.Text               := pContaBoletos.Operacao;
	edtProximoNossoNumero.AsInt    := pContaBoletos.ProximoNossoNumero;
	cbxRespEmissao.SetIndicePorValor(pContaBoletos.RespEmissao);
	edtNumeroRemessaBoleto.AsInt   := pContaBoletos.SequencialRemessa;
	mmTextoLivre.Lines.Add(pContaBoletos.TextoLivre);
	cbxTipoCarteira.SetIndicePorValor(pContaBoletos.TipoCarteira);
	edtVersaoLayout.Text           := pContaBoletos.VersaoLayout;
	edtVersaoLote.Text             := pContaBoletos.VersaoLote;
	edtCodigoTransmissao.Text      := pContaBoletos.Transmissao;
	edtDigitoConta.Text            := pContaBoletos.DigitoConta;
	edtCedente.Text                := pContaBoletos.Cedente;
	edtDVAgenciaConta.Text         := pContaBoletos.DvAgenciaConta;
  cbxTipoNegativacao.SetIndicePorValor(pContaBoletos.TipoNegativacao);
  FrLogo.setFoto(pContaBoletos.Logo);
  edtNumeroBancoExit(edtNumeroBanco);
end;

procedure TFrmCadastroContasBoletos.sbVisualizarClick(Sender: TObject);
begin
  inherited;
  ACBrBoletoComp := TACBrBoleto.Create(nil);
  ACBrBoletoFR   := TACBrBoletoFCFR.Create(ACBrBoletoComp);
  try
    CarregarBoleto;
    ACBrBoletoFR.PreparedReport;
    ACBrBoletoFR.Imprimir;
  finally
    FreeAndNil(ACBrBoletoFR);
    FreeAndNil(ACBrBoletoComp);
  end;
end;

procedure TFrmCadastroContasBoletos.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if edtDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('Descri��o inv�lida!');
    SetarFoco(edtDescricao);
    Abort;
  end;

  if edtNumeroBanco.AsInt = 0 then
  begin
    Exclamar('� necess�rio informar um banco v�lido!');
    SetarFoco(edtNumeroBanco);
    Abort;
  end;

  if FrLogo.EstaVazio then begin
    Exclamar('A imagem deve ser informada!');
    SetarFoco(FrLogo);
    Abort;
  end;

  if edtNumeroAgencia.Trim = '' then
  begin
    Exclamar('� necess�rio informar o n�mero da ag�ncia!');
    SetarFoco(edtNumeroAgencia);
    Abort;
  end;

//  if edtDigitoAgencia.Trim = '' then
//  begin
//    Exclamar('� necess�rio informar o d�gito da ag�ncia!');
//    SetarFoco(edtDigitoAgencia);
//    Abort;
//  end;

  if edtNumeroConta.Trim = '' then
  begin
    Exclamar('� necess�rio informar o n�mero da conta!');
    SetarFoco(edtNumeroConta);
    Abort;
  end;

  if edtDigitoConta.Trim = '' then
  begin
    Exclamar('� necess�rio informar o d�gito da conta!');
    SetarFoco(edtDigitoConta);
    Abort;
  end;

  if edtNossoNumeroInicial.Trim = '' then
  begin
    Exclamar('� necess�rio informar o nosso n�mero inicial!');
    SetarFoco(edtNossoNumeroInicial);
    Abort;
  end;

  if edtNossoNumeroFinal.Trim = '' then
  begin
    Exclamar('� necess�rio informar o nosso n�mero final!');
    SetarFoco(edtNossoNumeroFinal);
    Abort;
  end;

  if edtProximoNossoNumero.Trim = '' then
  begin
    Exclamar('� necess�rio informar o pr�ximo nosso n�mero!');
    SetarFoco(edtProximoNossoNumero);
    Abort;
  end;

  if edtNumeroRemessaBoleto.AsInt <= 0 then
  begin
    Exclamar('� necess�rio informar o sequencial da remessa!');
    SetarFoco(edtNumeroRemessaBoleto);
    Abort;
  end;

  if FrLogo.imFoto.Picture.Graphic = nil  then
  begin
    Exclamar('� necess�rio informar a logo do banco!');
    SetarFoco(FrLogo);
    Abort;
  end;

end;

end.
