unit _CodigoBarras;

interface

uses
  _Biblioteca, _ProdutosVenda, _Sessao, _RecordsOrcamentosVendas, _Produtos, _ProdutosCodigosBarrasAux, _RecordsCadastros;

function getProdutoId(pCodigoBarras: string): Integer;

function TratarCodigoBarras(pCodigoBarras: string): TArray<RecProdutosVendas>;

implementation

function getProdutoId(pCodigoBarras: string): Integer;
var
  vProduto: TArray<RecProdutos>;
  vProdutoAux: TArray<RecProdutosCodigosBarrasAux>;
begin
  Result := -1;

  // Se for menor que 11 � um c�digo de produto normal
  if Length(pCodigoBarras) < 11 then begin
    Result := SFormatInt(pCodigoBarras);
    Exit;    
  end;

  // Tentando encontrar pelo c�digo de barras normal
  vProduto := _Produtos.BuscarProdutos(Sessao.getConexaoBanco, 3, [pCodigoBarras], True, '');
  if vProduto <> nil then begin
    Result := vProduto[0].produto_id;
    Exit;
  end;

  // Tentando encontrar pelo c�digo de barras auxiliar
  vProdutoAux := _ProdutosCodigosBarrasAux.BuscarProdutosCodigosBarrasAux(Sessao.getConexaoBanco, 1, [pCodigoBarras]);
  if vProdutoAux <> nil then begin
    Result := vProdutoAux[0].ProdutoId;
    Exit;
  end;  
end;

function TratarCodigoBarras(pCodigoBarras: string): TArray<RecProdutosVendas>;
var
//  vCodigo: string;
  vProdutoId: Integer;
//  vPreco: Double;
begin
  Result := nil;

  // C�digo de barras normal
  if (Length(pCodigoBarras) = 13) then
    vProdutoId := _ProdutosVenda.getProdutoIdViaCodigo(Sessao.getConexaoBanco, pCodigoBarras, 'C')
  else
    vProdutoId := SFormatInt(pCodigoBarras);

  if vProdutoId = -1 then
    Exit;

  Result := _ProdutosVenda.BuscarProdutosVendas(Sessao.getConexaoBanco, 0, [Sessao.getEmpresaLogada.EmpresaId, vProdutoId], True);
  if Result = nil then
    Exit;

//  Result[0].Quantidade := 0;
//
//  if (Length(pCodigoBarras) = 13) and (Copy(pCodigoBarras, 1, 1) <> '2') then
//    Exit;
//
//  if Result[0].PrecoPDV = 0 then
//    Exit;
//
//  vPreco := SFormatDouble(Copy(pCodigoBarras, 6, 5) + ',' + Copy(pCodigoBarras, 11, 2));
//  Result[0].Quantidade := _Biblioteca.Arredondar( vPreco / Result[0].PrecoPDV, 3 );
end;

end.
