inherited FormPesquisaFuncionarios: TFormPesquisaFuncionarios
  Caption = 'Pesquisa de usu'#225'rios'
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    ColCount = 6
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Apelido'
      'Cargo'
      'Ativo')
    OnGetCellColor = sgPesquisaGetCellColor
    RealColCount = 6
    ColWidths = (
      28
      47
      190
      147
      142
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Top = 264
    Text = '0'
    ExplicitTop = 264
  end
  inherited pnFiltro1: TPanel
    inherited lblPesquisa: TLabel
      Width = 43
      Caption = 'Usu'#225'rio'
      ExplicitWidth = 43
    end
    inherited eValorPesquisa: TEditLuka
      Width = 439
      ExplicitWidth = 439
    end
  end
end
