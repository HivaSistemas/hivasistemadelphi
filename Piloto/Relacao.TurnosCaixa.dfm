inherited FormRelacaoTurnosCaixa: TFormRelacaoTurnosCaixa
  Caption = 'Movimenta'#231#245'es de caixa'
  ClientHeight = 566
  ClientWidth = 1015
  WindowState = wsMaximized
  OnShow = FormShow
  ExplicitWidth = 1021
  ExplicitHeight = 595
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 566
    ExplicitHeight = 638
  end
  inherited pcDados: TPageControl
    Width = 893
    Height = 566
    ActivePage = tsResultado
    ExplicitWidth = 900
    ExplicitHeight = 638
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 892
      ExplicitHeight = 609
      inline FrEmpresas: TFrEmpresas
        Left = 1
        Top = 0
        Width = 336
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitWidth = 336
        inherited sgPesquisa: TGridLuka
          Width = 333
          ExplicitWidth = 333
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 336
          ExplicitWidth = 336
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 231
            ExplicitLeft = 231
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 333
          Width = 3
          Visible = False
          ExplicitLeft = 333
          ExplicitWidth = 3
          inherited sbPesquisa: TSpeedButton
            Left = -21
            ExplicitLeft = -21
          end
        end
      end
      inline FrDataAbertura: TFrDataInicialFinal
        Left = 377
        Top = 1
        Width = 217
        Height = 40
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 377
        ExplicitTop = 1
        ExplicitWidth = 217
        ExplicitHeight = 40
        inherited Label1: TLabel
          Width = 93
          Height = 14
          Caption = 'Data de abertura'
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          Caption = #224'te'
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Left = 111
          Height = 22
          ExplicitLeft = 111
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDataFechamento: TFrDataInicialFinal
        Left = 376
        Top = 43
        Width = 217
        Height = 39
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 376
        ExplicitTop = 43
        ExplicitWidth = 217
        ExplicitHeight = 39
        inherited Label1: TLabel
          Left = 3
          Width = 111
          Height = 14
          Caption = 'Data de fechamento'
          ExplicitLeft = 3
          ExplicitWidth = 111
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          Caption = #224'te'
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Left = 112
          Height = 22
          ExplicitLeft = 112
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Left = 3
          Height = 22
          ExplicitLeft = 3
          ExplicitHeight = 22
        end
      end
      inline FrCodigoTurno: TFrameInteiros
        Left = 379
        Top = 185
        Width = 113
        Height = 96
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 379
        ExplicitTop = 185
        ExplicitWidth = 113
        ExplicitHeight = 96
        inherited sgInteiros: TGridLuka
          Width = 113
          Height = 78
          ExplicitWidth = 113
          ExplicitHeight = 78
        end
        inherited Panel1: TPanel
          Width = 113
          Caption = 'C'#243'digo do turno'
          ExplicitWidth = 113
        end
      end
      inline FrFuncionarios: TFrFuncionarios
        Left = 0
        Top = 88
        Width = 336
        Height = 97
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitTop = 88
        ExplicitWidth = 336
        inherited sgPesquisa: TGridLuka
          Width = 333
          ExplicitWidth = 333
        end
        inherited PnTitulos: TPanel
          Width = 336
          ExplicitWidth = 336
          inherited lbNomePesquisa: TLabel
            Width = 71
            ExplicitWidth = 71
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 231
            ExplicitLeft = 231
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 333
          Width = 3
          Visible = False
          ExplicitLeft = 333
          ExplicitWidth = 3
          inherited sbPesquisa: TSpeedButton
            Left = -21
            ExplicitLeft = -21
          end
        end
        inherited ckBuscarSomenteAtivos: TCheckBox
          Checked = False
          State = cbUnchecked
        end
        inherited ckBuscarTodasEmpresas: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      object gbStatus: TGroupBoxLuka
        Left = 379
        Top = 92
        Width = 113
        Height = 83
        Caption = '  Status    '
        TabOrder = 5
        OpcaoMarcarDesmarcar = True
        object ckAbertos: TCheckBoxLuka
          Left = 17
          Top = 21
          Width = 69
          Height = 17
          Caption = 'Abertos'
          Checked = True
          State = cbChecked
          TabOrder = 0
          CheckedStr = 'S'
          Valor = 'A'
        end
        object ckBaixados: TCheckBoxLuka
          Left = 17
          Top = 49
          Width = 85
          Height = 17
          Caption = 'Fechados'
          Checked = True
          State = cbChecked
          TabOrder = 1
          CheckedStr = 'S'
          Valor = 'B'
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 892
      ExplicitHeight = 609
      object sgTurnos: TGridLuka
        Left = 0
        Top = 0
        Width = 885
        Height = 412
        Align = alClient
        ColCount = 9
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        ParentFont = False
        TabOrder = 0
        OnClick = sgTurnosClick
        OnDblClick = sgTurnosDblClick
        OnDrawCell = sgTurnosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Turno'
          'Caixa'
          'Status'
          'Conta orig.din.'
          'Data/hora abertura'
          'Usu'#225'rio abertura'
          'Data/hora fechamento'
          'Usu'#225'rio fechamento'
          'Empresa')
        OnGetCellColor = sgTurnosGetCellColor
        Grid3D = False
        RealColCount = 15
        Indicador = True
        AtivarPopUpSelecao = False
        ExplicitWidth = 892
        ExplicitHeight = 484
        ColWidths = (
          57
          162
          131
          139
          123
          189
          152
          179
          179)
      end
      object Panel1: TPanel
        Left = 0
        Top = 412
        Width = 885
        Height = 125
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitTop = 484
        ExplicitWidth = 892
        object st3: TStaticText
          Left = 150
          Top = 2
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor dinheiro '
          Color = 38619
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Transparent = False
        end
        object stValorMovDinheiro: TStaticText
          Left = 150
          Top = 32
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          Transparent = False
        end
        object st1: TStaticText
          Left = 237
          Top = 2
          Width = 88
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor cheque '
          Color = 38619
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          Transparent = False
        end
        object stValorMovCheque: TStaticText
          Left = 237
          Top = 32
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 3
          Transparent = False
        end
        object stValorMovCartao: TStaticText
          Left = 324
          Top = 32
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 4
          Transparent = False
        end
        object st5: TStaticText
          Left = 324
          Top = 2
          Width = 88
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor cart'#227'o '
          Color = 38619
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 5
          Transparent = False
        end
        object stValorMovCobranca: TStaticText
          Left = 411
          Top = 32
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 6
          Transparent = False
        end
        object st7: TStaticText
          Left = 411
          Top = 2
          Width = 88
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor cobran'#231'a '
          Color = 38619
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 7
          Transparent = False
        end
        object stValorMovCredito: TStaticText
          Left = 587
          Top = 32
          Width = 89
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 8
          Transparent = False
        end
        object st9: TStaticText
          Left = 587
          Top = 2
          Width = 88
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor cr'#233'dito '
          Color = 38619
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 9
          Transparent = False
        end
        object st10: TStaticText
          Left = 2
          Top = 17
          Width = 149
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Vl. Abertura'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 10
          Transparent = False
        end
        object st11: TStaticText
          Left = 2
          Top = 92
          Width = 149
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Vl. Fechamento '
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 11
          Transparent = False
        end
        object st12: TStaticText
          Left = 2
          Top = 107
          Width = 149
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Diferen'#231'a '
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 12
          Transparent = False
        end
        object stValorInfDinheiro: TStaticText
          Left = 150
          Top = 92
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 13
          Transparent = False
        end
        object stValorInfCheque: TStaticText
          Left = 237
          Top = 92
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 14
          Transparent = False
        end
        object stValorInfCartao: TStaticText
          Left = 324
          Top = 92
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 15
          Transparent = False
        end
        object stValorInfCobranca: TStaticText
          Left = 411
          Top = 92
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 16
          Transparent = False
        end
        object stValorDiferencaDinheiro: TStaticText
          Left = 150
          Top = 107
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 17
          Transparent = False
        end
        object stValorDiferencaCheque: TStaticText
          Left = 237
          Top = 107
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 18
          Transparent = False
        end
        object stValorDiferencaCartao: TStaticText
          Left = 324
          Top = 107
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 19
          Transparent = False
        end
        object stValorDiferencaCobranca: TStaticText
          Left = 411
          Top = 107
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 20
          Transparent = False
        end
        object stValorAbertura: TStaticText
          Left = 150
          Top = 17
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 21
          Transparent = False
        end
        object st13: TStaticText
          Left = 2
          Top = 32
          Width = 149
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Recebimentos '
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 22
          Transparent = False
        end
        object st17: TStaticText
          Left = 2
          Top = 62
          Width = 149
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Suplementos'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 23
          Transparent = False
        end
        object stValorSuprimentos: TStaticText
          Left = 150
          Top = 62
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 24
          Transparent = False
        end
        object st18: TStaticText
          Left = 2
          Top = 77
          Width = 149
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Retiradas'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 25
          Transparent = False
        end
        object stValorSangriasDinheiro: TStaticText
          Left = 150
          Top = 77
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 26
          Transparent = False
        end
        object stValorSangriasCheque: TStaticText
          Left = 237
          Top = 77
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 27
          Transparent = False
        end
        object stValorSangriasCartao: TStaticText
          Left = 324
          Top = 77
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 28
          Transparent = False
        end
        object stValorSangriasCobranca: TStaticText
          Left = 411
          Top = 77
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 29
          Transparent = False
        end
        object stTotalMovimentos: TStaticText
          Left = 795
          Top = 32
          Width = 92
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 30
          Transparent = False
        end
        object st19: TStaticText
          Left = 794
          Top = 2
          Width = 93
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Total'
          Color = 38619
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 31
          Transparent = False
        end
        object stTotalAbertura: TStaticText
          Left = 795
          Top = 17
          Width = 92
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 32
          Transparent = False
        end
        object stTotalSuprimentos: TStaticText
          Left = 795
          Top = 62
          Width = 92
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 33
          Transparent = False
        end
        object stTotalSangrias: TStaticText
          Left = 795
          Top = 77
          Width = 92
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 34
          Transparent = False
        end
        object stTotalInformadoFechamento: TStaticText
          Left = 795
          Top = 92
          Width = 92
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 35
          Transparent = False
        end
        object stValorDiferencaCredito: TStaticText
          Left = 587
          Top = 107
          Width = 89
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 36
          Transparent = False
        end
        object st20: TStaticText
          Left = 587
          Top = 77
          Width = 89
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 37
          Transparent = False
        end
        object st21: TStaticText
          Left = 324
          Top = 17
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 38
          Transparent = False
        end
        object st22: TStaticText
          Left = 498
          Top = 17
          Width = 90
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 39
          Transparent = False
        end
        object st23: TStaticText
          Left = 324
          Top = 62
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 40
          Transparent = False
        end
        object st24: TStaticText
          Left = 587
          Top = 62
          Width = 89
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 41
          Transparent = False
        end
        object st2: TStaticText
          Left = 2
          Top = 47
          Width = 149
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Pagamentos '
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 42
          Transparent = False
        end
        object stValorPagamentosDinheiro: TStaticText
          Left = 150
          Top = 47
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 43
          Transparent = False
        end
        object stValorPagamentosCheques: TStaticText
          Left = 237
          Top = 47
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 44
          Transparent = False
        end
        object ststValorPagamentosCobrancas: TStaticText
          Left = 411
          Top = 47
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 45
          Transparent = False
        end
        object stTotalPagamentos: TStaticText
          Left = 795
          Top = 47
          Width = 92
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 46
          Transparent = False
        end
        object stValorPagamentosCreditos: TStaticText
          Left = 587
          Top = 47
          Width = 89
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 47
          Transparent = False
        end
        object stValorMovAcumulado: TStaticText
          Left = 498
          Top = 32
          Width = 90
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 48
          Transparent = False
        end
        object st6: TStaticText
          Left = 498
          Top = 2
          Width = 90
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor acumula.'
          Color = 38619
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 49
          Transparent = False
        end
        object stValorInfAcumulado: TStaticText
          Left = 498
          Top = 92
          Width = 90
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 50
          Transparent = False
        end
        object stValorDiferencaAcumulado: TStaticText
          Left = 498
          Top = 107
          Width = 90
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 51
          Transparent = False
        end
        object st14: TStaticText
          Left = 498
          Top = 77
          Width = 90
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 52
          Transparent = False
        end
        object st15: TStaticText
          Left = 498
          Top = 47
          Width = 90
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 53
          Transparent = False
        end
        object st4: TStaticText
          Left = 411
          Top = 17
          Width = 88
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 54
          Transparent = False
        end
        object st16: TStaticText
          Left = 498
          Top = 62
          Width = 90
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 55
          Transparent = False
        end
        object stValorInfCredito: TStaticText
          Left = 587
          Top = 92
          Width = 89
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 56
          Transparent = False
        end
        object stTotalDiferencaFechamento: TStaticText
          Left = 795
          Top = 107
          Width = 92
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 57
          Transparent = False
        end
        object StaticText1: TStaticText
          Left = 674
          Top = 2
          Width = 99
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Pix/Transfer'#234'ncia'
          Color = 38619
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Calibri'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 58
          Transparent = False
        end
        object stValorMovPix: TStaticText
          Left = 675
          Top = 32
          Width = 98
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,00'
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 59
          Transparent = False
        end
        object stValorInfPix: TStaticText
          Left = 675
          Top = 92
          Width = 98
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 60
          Transparent = False
        end
        object stValorDiferencaPix: TStaticText
          Left = 675
          Top = 107
          Width = 98
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 61
          Transparent = False
        end
        object StaticText5: TStaticText
          Left = 675
          Top = 77
          Width = 98
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 62
          Transparent = False
        end
        object stValorPagamentosPix: TStaticText
          Left = 675
          Top = 47
          Width = 98
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 63
          Transparent = False
        end
        object StaticText3: TStaticText
          Left = 675
          Top = 62
          Width = 98
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 64
          Transparent = False
        end
      end
    end
  end
  object frxTurnos: TfrxDBDataset
    UserName = 'frxdstTurnos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TURNO_ID=TURNO_ID'
      'EMPRESA_ID=EMPRESA_ID'
      'STATUS=STATUS'
      'NOME_EMPRESA=NOME_EMPRESA'
      'FUNCIONARIO_ID=FUNCIONARIO_ID'
      'NOME_CAIXA=NOME_CAIXA'
      'VALOR_INICIAL=VALOR_INICIAL'
      'DATA_HORA_ABERTURA=DATA_HORA_ABERTURA'
      'USUARIO_ABERTURA_ID=USUARIO_ABERTURA_ID'
      'NOME_USUARIO_ABERTURA=NOME_USUARIO_ABERTURA'
      'DATA_HORA_FECHAMENTO=DATA_HORA_FECHAMENTO'
      'USUARIO_FECHAMENTO_ID=USUARIO_FECHAMENTO_ID'
      'NOME_USUARIO_FECHAMENTO=NOME_USUARIO_FECHAMENTO'
      'CONTA_ORIGEM_DIN_INI_ID=CONTA_ORIGEM_DIN_INI_ID'
      'NOME_CONTA_ORIGEM=NOME_CONTA_ORIGEM'
      'VALOR_DINHEIRO=VALOR_DINHEIRO'
      'VALOR_PIX=VALOR_PIX'
      'VALOR_CARTAO=VALOR_CARTAO'
      'VALOR_CHEQUE=VALOR_CHEQUE'
      'VALOR_COBRANCA=VALOR_COBRANCA'
      'VALOR_ACUMULADO=VALOR_ACUMULADO'
      'VALOR_CREDITO=VALOR_CREDITO'
      'VALOR_PAGTOS_DINHEIRO=VALOR_PAGTOS_DINHEIRO'
      'VALOR_PAGTOS_CREDITO=VALOR_PAGTOS_CREDITO'
      'VALOR_SANGRIA_DINHEIRO=VALOR_SANGRIA_DINHEIRO'
      'VALOR_SANGRIA_CARTAO=VALOR_SANGRIA_CARTAO'
      'VALOR_SANGRIA_CHEQUE=VALOR_SANGRIA_CHEQUE'
      'VALOR_SANGRIA_COBRANCA=VALOR_SANGRIA_COBRANCA'
      'VALOR_SUPRIMENTO=VALOR_SUPRIMENTO'
      'VALOR_DINHEIRO_INF_FECHAMENTO=VALOR_DINHEIRO_INF_FECHAMENTO'
      'VALOR_CHEQUE_INF_FECHAMENTO=VALOR_CHEQUE_INF_FECHAMENTO'
      'VALOR_CARTAO_INF_FECHAMENTO=VALOR_CARTAO_INF_FECHAMENTO'
      'VALOR_COBRANCA_INF_FECHAMENTO=VALOR_COBRANCA_INF_FECHAMENTO')
    DataSet = qTurnos
    BCDToCurrency = False
    Left = 134
    Top = 297
  end
  object qTurnos: TOraQuery
    SQL.Strings = (
      'select '
      '  TUR.TURNO_ID, '
      '  TUR.EMPRESA_ID, '
      
        '  case when TUR.STATUS = '#39'A'#39' then '#39'Aberto'#39' else '#39'Baixado'#39' end as' +
        ' STATUS, '
      '  EMP.NOME_FANTASIA as NOME_EMPRESA, '
      '  TUR.FUNCIONARIO_ID, '
      '  FUN.NOME as NOME_CAIXA, '
      '  TUR.VALOR_INICIAL, '
      '  TUR.DATA_HORA_ABERTURA, '
      '  TUR.USUARIO_ABERTURA_ID, '
      '  USA.NOME as NOME_USUARIO_ABERTURA, '
      '  TUR.DATA_HORA_FECHAMENTO, '
      '  TUR.USUARIO_FECHAMENTO_ID, '
      '  USF.NOME as NOME_USUARIO_FECHAMENTO, '
      '  TUR.CONTA_ORIGEM_DIN_INI_ID, '
      '  CON.NOME as NOME_CONTA_ORIGEM, '
      ''
      '  MOV.VALOR_DINHEIRO, '
      '  MOV.VALOR_PIX, '
      '  MOV.VALOR_CARTAO, '
      '  MOV.VALOR_CHEQUE, '
      '  MOV.VALOR_COBRANCA, '
      '  MOV.VALOR_ACUMULADO, '
      '  MOV.VALOR_CREDITO, '
      ''
      '  MOV.VALOR_PAGTOS_DINHEIRO, '
      '  MOV.VALOR_PAGTOS_CREDITO, '
      ''
      '  MOV.VALOR_SANGRIA_DINHEIRO, '
      '  MOV.VALOR_SANGRIA_CARTAO, '
      '  MOV.VALOR_SANGRIA_CHEQUE, '
      '  MOV.VALOR_SANGRIA_COBRANCA, '
      ''
      '  MOV.VALOR_SUPRIMENTO, '
      '  TUR.VALOR_DINHEIRO_INF_FECHAMENTO, '
      '  TUR.VALOR_CHEQUE_INF_FECHAMENTO, '
      '  TUR.VALOR_CARTAO_INF_FECHAMENTO, '
      '  TUR.VALOR_COBRANCA_INF_FECHAMENTO '
      'from '
      '  TURNOS_CAIXA TUR '
      ''
      'inner join FUNCIONARIOS FUN '
      'on TUR.FUNCIONARIO_ID = FUN.FUNCIONARIO_ID '
      ''
      'inner join FUNCIONARIOS USA '
      'on TUR.USUARIO_ABERTURA_ID = USA.FUNCIONARIO_ID '
      ''
      'left join FUNCIONARIOS USF '
      'on TUR.USUARIO_FECHAMENTO_ID = USF.FUNCIONARIO_ID '
      ''
      'inner join EMPRESAS EMP '
      'on TUR.EMPRESA_ID = EMP.EMPRESA_ID '
      ''
      'left join CONTAS CON '
      'on TUR.CONTA_ORIGEM_DIN_INI_ID = CON.CONTA '
      ''
      'left join( '
      '  select '
      '    TURNO_ID, '
      
        '    sum(case when TIPO_MOVIMENTO in('#39'REV'#39', '#39'BXR'#39', '#39'ACU'#39') then VA' +
        'LOR_DINHEIRO else 0 end) as VALOR_DINHEIRO, '
      
        '    sum(case when TIPO_MOVIMENTO in('#39'REV'#39') then VALOR_PIX else 0' +
        ' end) as VALOR_PIX, '
      
        '    sum(case when TIPO_MOVIMENTO in('#39'REV'#39', '#39'BXR'#39', '#39'ACU'#39') then VA' +
        'LOR_CARTAO else 0 end) as VALOR_CARTAO, '
      
        '    sum(case when TIPO_MOVIMENTO in('#39'REV'#39', '#39'BXR'#39', '#39'ACU'#39') then VA' +
        'LOR_CHEQUE else 0 end) as VALOR_CHEQUE, '
      
        '    sum(case when TIPO_MOVIMENTO in('#39'REV'#39', '#39'BXR'#39', '#39'ACU'#39') then VA' +
        'LOR_COBRANCA else 0 end) as VALOR_COBRANCA, '
      
        '    sum(case when TIPO_MOVIMENTO in('#39'REV'#39') then VALOR_ACUMULADO ' +
        'else 0 end) as VALOR_ACUMULADO, '
      
        '    sum(case when TIPO_MOVIMENTO in('#39'REV'#39', '#39'BXR'#39', '#39'ACU'#39') then VA' +
        'LOR_CREDITO else 0 end) as VALOR_CREDITO, '
      ''
      
        '    sum(case when TIPO_MOVIMENTO in('#39'BXP'#39') then VALOR_DINHEIRO e' +
        'lse 0 end) as VALOR_PAGTOS_DINHEIRO, '
      
        '    sum(case when TIPO_MOVIMENTO in('#39'BXP'#39') then VALOR_CREDITO el' +
        'se 0 end) as VALOR_PAGTOS_CREDITO, '
      ''
      
        '    sum(case when TIPO_MOVIMENTO = '#39'SUP'#39' then VALOR_DINHEIRO els' +
        'e 0 end) as VALOR_SUPRIMENTO, '
      
        '    sum(case when TIPO_MOVIMENTO = '#39'SAN'#39' then VALOR_DINHEIRO els' +
        'e 0 end) as VALOR_SANGRIA_DINHEIRO, '
      
        '    sum(case when TIPO_MOVIMENTO = '#39'SAN'#39' then VALOR_CARTAO else ' +
        '0 end) as VALOR_SANGRIA_CARTAO, '
      
        '    sum(case when TIPO_MOVIMENTO = '#39'SAN'#39' then VALOR_CHEQUE else ' +
        '0 end) as VALOR_SANGRIA_CHEQUE, '
      
        '    sum(case when TIPO_MOVIMENTO = '#39'SAN'#39' then VALOR_COBRANCA els' +
        'e 0 end) as VALOR_SANGRIA_COBRANCA '
      ''
      '  from '
      '    VW_MOVIMENTOS_TURNOS '
      '  group by '
      '    TURNO_ID '
      ') MOV '
      'on TUR.TURNO_ID = MOV.TURNO_ID ')
    Left = 136
    Top = 344
    object qTurnosTURNO_ID: TFloatField
      FieldName = 'TURNO_ID'
      Required = True
    end
    object qTurnosEMPRESA_ID: TIntegerField
      FieldName = 'EMPRESA_ID'
      Required = True
    end
    object qTurnosSTATUS: TStringField
      FieldName = 'STATUS'
      FixedChar = True
      Size = 7
    end
    object qTurnosNOME_EMPRESA: TStringField
      FieldName = 'NOME_EMPRESA'
      Size = 60
    end
    object qTurnosFUNCIONARIO_ID: TIntegerField
      FieldName = 'FUNCIONARIO_ID'
      Required = True
    end
    object qTurnosNOME_CAIXA: TStringField
      FieldName = 'NOME_CAIXA'
      Size = 60
    end
    object qTurnosVALOR_INICIAL: TFloatField
      FieldName = 'VALOR_INICIAL'
      Required = True
    end
    object qTurnosDATA_HORA_ABERTURA: TDateTimeField
      FieldName = 'DATA_HORA_ABERTURA'
      Required = True
    end
    object qTurnosUSUARIO_ABERTURA_ID: TIntegerField
      FieldName = 'USUARIO_ABERTURA_ID'
      Required = True
    end
    object qTurnosNOME_USUARIO_ABERTURA: TStringField
      FieldName = 'NOME_USUARIO_ABERTURA'
      Size = 60
    end
    object qTurnosDATA_HORA_FECHAMENTO: TDateTimeField
      FieldName = 'DATA_HORA_FECHAMENTO'
    end
    object qTurnosUSUARIO_FECHAMENTO_ID: TIntegerField
      FieldName = 'USUARIO_FECHAMENTO_ID'
    end
    object qTurnosNOME_USUARIO_FECHAMENTO: TStringField
      FieldName = 'NOME_USUARIO_FECHAMENTO'
      Size = 60
    end
    object qTurnosCONTA_ORIGEM_DIN_INI_ID: TStringField
      FieldName = 'CONTA_ORIGEM_DIN_INI_ID'
      Size = 8
    end
    object qTurnosNOME_CONTA_ORIGEM: TStringField
      FieldName = 'NOME_CONTA_ORIGEM'
      Size = 60
    end
    object qTurnosVALOR_DINHEIRO: TFloatField
      FieldName = 'VALOR_DINHEIRO'
    end
    object qTurnosVALOR_PIX: TFloatField
      FieldName = 'VALOR_PIX'
    end
    object qTurnosVALOR_CARTAO: TFloatField
      FieldName = 'VALOR_CARTAO'
    end
    object qTurnosVALOR_CHEQUE: TFloatField
      FieldName = 'VALOR_CHEQUE'
    end
    object qTurnosVALOR_COBRANCA: TFloatField
      FieldName = 'VALOR_COBRANCA'
    end
    object qTurnosVALOR_ACUMULADO: TFloatField
      FieldName = 'VALOR_ACUMULADO'
    end
    object qTurnosVALOR_CREDITO: TFloatField
      FieldName = 'VALOR_CREDITO'
    end
    object qTurnosVALOR_PAGTOS_DINHEIRO: TFloatField
      FieldName = 'VALOR_PAGTOS_DINHEIRO'
    end
    object qTurnosVALOR_PAGTOS_CREDITO: TFloatField
      FieldName = 'VALOR_PAGTOS_CREDITO'
    end
    object qTurnosVALOR_SANGRIA_DINHEIRO: TFloatField
      FieldName = 'VALOR_SANGRIA_DINHEIRO'
    end
    object qTurnosVALOR_SANGRIA_CARTAO: TFloatField
      FieldName = 'VALOR_SANGRIA_CARTAO'
    end
    object qTurnosVALOR_SANGRIA_CHEQUE: TFloatField
      FieldName = 'VALOR_SANGRIA_CHEQUE'
    end
    object qTurnosVALOR_SANGRIA_COBRANCA: TFloatField
      FieldName = 'VALOR_SANGRIA_COBRANCA'
    end
    object qTurnosVALOR_SUPRIMENTO: TFloatField
      FieldName = 'VALOR_SUPRIMENTO'
    end
    object qTurnosVALOR_DINHEIRO_INF_FECHAMENTO: TFloatField
      FieldName = 'VALOR_DINHEIRO_INF_FECHAMENTO'
    end
    object qTurnosVALOR_CHEQUE_INF_FECHAMENTO: TFloatField
      FieldName = 'VALOR_CHEQUE_INF_FECHAMENTO'
    end
    object qTurnosVALOR_CARTAO_INF_FECHAMENTO: TFloatField
      FieldName = 'VALOR_CARTAO_INF_FECHAMENTO'
    end
    object qTurnosVALOR_COBRANCA_INF_FECHAMENTO: TFloatField
      FieldName = 'VALOR_COBRANCA_INF_FECHAMENTO'
    end
  end
  object dsTurnos: TDataSource
    DataSet = qTurnos
    Left = 134
    Top = 401
  end
  object qMovimentosTurnos: TOraQuery
    SQL.Strings = (
      'select'
      '  MOV.MOVIMENTO_TURNO_ID, '
      '  MOV.TURNO_ID, '
      '  MOV.ID, '
      '  MOV.CONTA_ID, '
      '  CON.NOME as NOME_CONTA_ORIGEM, '
      '  MOV.USUARIO_MOVIMENTO_ID, '
      
        '  case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU'#39', '#39 +
        'BXR'#39') then MOV.VALOR_DINHEIRO else MOV.VALOR_DINHEIRO * -1 END A' +
        'S VALOR_DINHEIRO,'
      
        '  case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU'#39', '#39 +
        'BXR'#39') then MOV.VALOR_PIX else MOV.VALOR_PIX * -1 END AS VALOR_PI' +
        'X,'
      
        '  case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU'#39', '#39 +
        'BXR'#39') then MOV.VALOR_CHEQUE else MOV.VALOR_CHEQUE * -1 END AS VA' +
        'LOR_CHEQUE,'
      
        '  case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU'#39', '#39 +
        'BXR'#39') then MOV.VALOR_CREDITO else MOV.VALOR_CREDITO * -1 END AS ' +
        'VALOR_CREDITO,'
      
        '  case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU'#39', '#39 +
        'BXR'#39') then MOV.VALOR_CARTAO else MOV.VALOR_CARTAO * -1 END AS VA' +
        'LOR_CARTAO,'
      
        '  case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU'#39', '#39 +
        'BXR'#39') then MOV.VALOR_COBRANCA else MOV.VALOR_COBRANCA * -1 END A' +
        'S VALOR_COBRANCA,'
      
        '  case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU'#39', '#39 +
        'BXR'#39') then MOV.VALOR_ACUMULADO else MOV.VALOR_ACUMULADO * -1 END' +
        ' AS VALOR_ACUMULADO,'
      '  MOV.DATA_HORA_MOVIMENTO,'
      '  case MOV.TIPO_MOVIMENTO'
      '    when '#39'ABE'#39' then '#39'Abertura'#39
      '    when '#39'SUP'#39' then '#39'Suprimento'#39
      '    when '#39'SAN'#39' then '#39'Sangria'#39
      '    when '#39'FEC'#39' then '#39'Fechamento'#39
      '    when '#39'REV'#39' then '#39'Recebimento de venda'#39
      '    when '#39'ACU'#39' then '#39'Recebimento de acumulados'#39
      '    when '#39'BXR'#39' then '#39'Baixa contas receber'#39
      '    when '#39'BXP'#39' then '#39'Baixa contas pagar'#39
      '  end as TIPO_MOVIMENTO,'
      '  nvl(FUN.APELIDO, FUN.NOME) as FUNCIONARIO, '
      '  MOV.OBSERVACAO '
      'from '
      '  VW_MOVIMENTOS_TURNOS MOV '
      ''
      'left join CONTAS CON '
      'on MOV.CONTA_ID = CON.CONTA '
      ''
      'inner join FUNCIONARIOS FUN '
      'on MOV.USUARIO_MOVIMENTO_ID = FUN.FUNCIONARIO_ID '
      '')
    MasterSource = dsTurnos
    MasterFields = 'TURNO_ID'
    DetailFields = 'TURNO_ID'
    Left = 210
    Top = 345
    ParamData = <
      item
        DataType = ftFloat
        Name = 'TURNO_ID'
        ParamType = ptInput
        Value = 67.000000000000000000
      end>
    object qMovimentosTurnosMOVIMENTO_TURNO_ID: TFloatField
      FieldName = 'MOVIMENTO_TURNO_ID'
    end
    object qMovimentosTurnosTURNO_ID: TFloatField
      FieldName = 'TURNO_ID'
    end
    object qMovimentosTurnosID: TFloatField
      FieldName = 'ID'
    end
    object qMovimentosTurnosCONTA_ID: TStringField
      FieldName = 'CONTA_ID'
    end
    object qMovimentosTurnosNOME_CONTA_ORIGEM: TStringField
      FieldName = 'NOME_CONTA_ORIGEM'
      Size = 60
    end
    object qMovimentosTurnosUSUARIO_MOVIMENTO_ID: TIntegerField
      FieldName = 'USUARIO_MOVIMENTO_ID'
    end
    object qMovimentosTurnosVALOR_DINHEIRO: TFloatField
      FieldName = 'VALOR_DINHEIRO'
    end
    object qMovimentosTurnosVALOR_PIX: TFloatField
      FieldName = 'VALOR_PIX'
    end
    object qMovimentosTurnosVALOR_CHEQUE: TFloatField
      FieldName = 'VALOR_CHEQUE'
    end
    object qMovimentosTurnosVALOR_CREDITO: TFloatField
      FieldName = 'VALOR_CREDITO'
    end
    object qMovimentosTurnosVALOR_CARTAO: TFloatField
      FieldName = 'VALOR_CARTAO'
    end
    object qMovimentosTurnosVALOR_COBRANCA: TFloatField
      FieldName = 'VALOR_COBRANCA'
    end
    object qMovimentosTurnosVALOR_ACUMULADO: TFloatField
      FieldName = 'VALOR_ACUMULADO'
    end
    object qMovimentosTurnosDATA_HORA_MOVIMENTO: TDateTimeField
      FieldName = 'DATA_HORA_MOVIMENTO'
    end
    object qMovimentosTurnosTIPO_MOVIMENTO: TStringField
      FieldName = 'TIPO_MOVIMENTO'
      Size = 25
    end
    object qMovimentosTurnosFUNCIONARIO: TStringField
      FieldName = 'FUNCIONARIO'
      Size = 60
    end
    object qMovimentosTurnosOBSERVACAO: TStringField
      FieldName = 'OBSERVACAO'
      Size = 200
    end
  end
  object dstMovimentos: TfrxDBDataset
    UserName = 'frxdstMovimentosTurnos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'MOVIMENTO_TURNO_ID=MOVIMENTO_TURNO_ID'
      'TURNO_ID=TURNO_ID'
      'ID=ID'
      'CONTA_ID=CONTA_ID'
      'NOME_CONTA_ORIGEM=NOME_CONTA_ORIGEM'
      'USUARIO_MOVIMENTO_ID=USUARIO_MOVIMENTO_ID'
      'VALOR_DINHEIRO=VALOR_DINHEIRO'
      'VALOR_PIX=VALOR_PIX'
      'VALOR_CHEQUE=VALOR_CHEQUE'
      'VALOR_CREDITO=VALOR_CREDITO'
      'VALOR_CARTAO=VALOR_CARTAO'
      'VALOR_COBRANCA=VALOR_COBRANCA'
      'VALOR_ACUMULADO=VALOR_ACUMULADO'
      'DATA_HORA_MOVIMENTO=DATA_HORA_MOVIMENTO'
      'TIPO_MOVIMENTO=TIPO_MOVIMENTO'
      'FUNCIONARIO=FUNCIONARIO'
      'OBSERVACAO=OBSERVACAO')
    DataSet = qMovimentosTurnos
    BCDToCurrency = False
    Left = 210
    Top = 298
  end
  object frxAnalitico: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 44529.883268425900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 360
    Top = 184
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstMovimentos
        DataSetName = 'frxdstMovimentosTurnos'
      end
      item
        DataSet = dstTotais
        DataSetName = 'frxdstTotais'
      end
      item
        DataSet = frxTurnos
        DataSetName = 'frxdstTurnos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133890000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000000000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000000000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133890000000000000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133890000000000000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 166.299320000000000000
        Width = 718.110700000000000000
        DataSet = frxTurnos
        DataSetName = 'frxdstTurnos'
        RowCount = 0
        object frxdstTranscodigoProd: TfrxMemoView
          Left = 2.779530000000000000
          Top = 19.897650000000000000
          Width = 37.795300000000000000
          Height = 11.338590000000000000
          DataField = 'TURNO_ID'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxdstTurnos."TURNO_ID"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 44.354360000000000000
          Top = 19.897650000000000000
          Width = 245.669450000000000000
          Height = 11.338590000000000000
          DataField = 'NOME_CAIXA'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxdstTurnos."NOME_CAIXA"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 507.590551180000000000
          Top = 19.897650000000000000
          Width = 136.063080000000000000
          Height = 11.338590000000000000
          DataField = 'DATA_HORA_FECHAMENTO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxdstTurnos."DATA_HORA_FECHAMENTO"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 375.173470000000000000
          Top = 19.897650000000000000
          Width = 124.724490000000000000
          Height = 11.338590000000000000
          DataField = 'DATA_HORA_ABERTURA'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxdstTurnos."DATA_HORA_ABERTURA"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 297.448818900000000000
          Top = 19.897650000000000000
          Width = 68.031496060000000000
          Height = 11.338582680000000000
          DataField = 'STATUS'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstTurnos."STATUS"]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo18: TfrxMemoView
          Left = 3.779530000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Turno')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 45.354360000000000000
          Width = 245.669450000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Caixa')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 508.457020000000000000
          Width = 136.063080000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Data/hora fechamento')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 298.582870000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Status')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 376.173470000000000000
          Width = 124.724490000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Data/hora abertura')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 41.952782760000000000
        Top = 438.425480000000000000
        Width = 718.110700000000000000
        object Line1: TfrxLineView
          Top = 3.779530000000000000
          Width = 718.110700000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 502.677490000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 272.126160000000000000
        Width = 718.110700000000000000
        DataSet = dstMovimentos
        DataSetName = 'frxdstMovimentosTurnos'
        KeepChild = True
        RowCount = 0
        object frxdstMovimentosTurnosVALOR_PIX: TfrxMemoView
          Left = 3.779530000000000000
          Width = 185.196970000000000000
          Height = 18.897650000000000000
          DataField = 'TIPO_MOVIMENTO'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentosTurnos'
          Memo.UTF8W = (
            '[frxdstMovimentosTurnos."TIPO_MOVIMENTO"]')
        end
        object frxdstMovimentosTurnosVALOR_CARTAO: TfrxMemoView
          Left = 192.756030000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_DINHEIRO'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentosTurnos'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstMovimentosTurnos."VALOR_DINHEIRO"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 257.008040000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_CHEQUE'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentosTurnos'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstMovimentosTurnos."VALOR_CHEQUE"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 321.260050000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_CARTAO'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentosTurnos'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstMovimentosTurnos."VALOR_CARTAO"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 385.512060000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_COBRANCA'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentosTurnos'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstMovimentosTurnos."VALOR_COBRANCA"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 449.764070000000000000
          Width = 64.251968500000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_ACUMULADO'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentosTurnos'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstMovimentosTurnos."VALOR_ACUMULADO"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 517.795610000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_CREDITO'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentosTurnos'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstMovimentosTurnos."VALOR_CREDITO"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 582.827150000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_PIX'
          DataSet = dstMovimentos
          DataSetName = 'frxdstMovimentosTurnos'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstMovimentosTurnos."VALOR_PIX"]')
          ParentFont = False
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 226.771800000000000000
        Width = 718.110700000000000000
        KeepChild = True
        object Shape4: TfrxShapeView
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Fill.BackColor = clMenu
        end
        object Memo5: TfrxMemoView
          Left = 3.779530000000000000
          Top = 2.000000000000000000
          Width = 185.196970000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Opera'#231#227'o')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 192.756030000000000000
          Top = 2.000000000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Dinheiro')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 257.008040000000000000
          Top = 2.000000000000000000
          Width = 60.472440940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cheque')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 449.764070000000000000
          Top = 2.000000000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Acumulado')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 321.260050000000000000
          Top = 2.000000000000000000
          Width = 60.472440940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cart'#227'o')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 385.512060000000000000
          Top = 2.000000000000000000
          Width = 60.472440940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cobran'#231'a')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 517.795610000000000000
          Top = 2.000000000000000000
          Width = 60.472440940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 582.827150000000000000
          Top = 1.779530000000000000
          Width = 60.472440940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Pix')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 64.252010000000000000
        Top = 313.700990000000000000
        Width = 718.110700000000000000
        object Memo40: TfrxMemoView
          Left = 120.944960000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Total:')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 192.755905510000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTotais."VALOR_DINHEIRO"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 257.007874020000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTotais."VALOR_CHEQUE"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 321.260050000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTotais."VALOR_CARTAO"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 385.512060000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTotais."VALOR_COBRANCA"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 449.764070000000000000
          Width = 64.251968500000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTotais."VALOR_ACUMULADO"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 517.795610000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTotais."VALOR_CREDITO"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 582.803149610000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTotais."VALOR_PIX"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 646.299630000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
        end
      end
    end
  end
  object dstTotais: TfrxDBDataset
    UserName = 'frxdstTotais'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TURNO_ID=TURNO_ID'
      'VALOR_DINHEIRO=VALOR_DINHEIRO'
      'VALOR_PIX=VALOR_PIX'
      'VALOR_CHEQUE=VALOR_CHEQUE'
      'VALOR_CREDITO=VALOR_CREDITO'
      'VALOR_CARTAO=VALOR_CARTAO'
      'VALOR_COBRANCA=VALOR_COBRANCA'
      'VALOR_ACUMULADO=VALOR_ACUMULADO')
    DataSet = qTotais
    BCDToCurrency = False
    Left = 290
    Top = 298
  end
  object qTotais: TOraQuery
    SQL.Strings = (
      'select'
      '  MOV.TURNO_ID,'
      
        '  sum(case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU' +
        #39', '#39'BXR'#39') then MOV.VALOR_DINHEIRO else MOV.VALOR_DINHEIRO * -1 e' +
        'nd) * -1 AS VALOR_DINHEIRO,'
      
        '  sum(case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU' +
        #39', '#39'BXR'#39') then MOV.VALOR_PIX else MOV.VALOR_PIX * -1 end) * -1 A' +
        'S VALOR_PIX,'
      
        '  sum(case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU' +
        #39', '#39'BXR'#39') then MOV.VALOR_CHEQUE else MOV.VALOR_CHEQUE * -1 end) ' +
        '* -1 AS VALOR_CHEQUE,'
      
        '  sum(case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU' +
        #39', '#39'BXR'#39') then MOV.VALOR_CREDITO else MOV.VALOR_CREDITO * -1 end' +
        ') * -1 AS VALOR_CREDITO,'
      
        '  sum(case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU' +
        #39', '#39'BXR'#39') then MOV.VALOR_CARTAO else MOV.VALOR_CARTAO * -1 end) ' +
        '* -1 AS VALOR_CARTAO,'
      
        '  sum(case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU' +
        #39', '#39'BXR'#39') then MOV.VALOR_COBRANCA else MOV.VALOR_COBRANCA * -1 e' +
        'nd) * -1 AS VALOR_COBRANCA,'
      
        '  sum(case when MOV.TIPO_MOVIMENTO in ('#39'ABE'#39', '#39'SUP'#39', '#39'REV'#39', '#39'ACU' +
        #39', '#39'BXR'#39') then MOV.VALOR_ACUMULADO else MOV.VALOR_ACUMULADO * -1' +
        ' end) * -1 AS VALOR_ACUMULADO'
      'from'
      '  VW_MOVIMENTOS_TURNOS MOV '
      ''
      'left join CONTAS CON '
      'on MOV.CONTA_ID = CON.CONTA '
      ''
      'inner join FUNCIONARIOS FUN '
      'on MOV.USUARIO_MOVIMENTO_ID = FUN.FUNCIONARIO_ID'
      ''
      'group by'
      '  TURNO_ID')
    MasterSource = dsTotais
    MasterFields = 'TURNO_ID'
    DetailFields = 'TURNO_ID'
    Left = 290
    Top = 345
    ParamData = <
      item
        DataType = ftFloat
        Name = 'TURNO_ID'
        ParamType = ptInput
        Value = 67.000000000000000000
      end>
    object qTotaisTURNO_ID: TFloatField
      FieldName = 'TURNO_ID'
    end
    object qTotaisVALOR_DINHEIRO: TFloatField
      FieldName = 'VALOR_DINHEIRO'
    end
    object qTotaisVALOR_PIX: TFloatField
      FieldName = 'VALOR_PIX'
    end
    object qTotaisVALOR_CHEQUE: TFloatField
      FieldName = 'VALOR_CHEQUE'
    end
    object qTotaisVALOR_CREDITO: TFloatField
      FieldName = 'VALOR_CREDITO'
    end
    object qTotaisVALOR_CARTAO: TFloatField
      FieldName = 'VALOR_CARTAO'
    end
    object qTotaisVALOR_COBRANCA: TFloatField
      FieldName = 'VALOR_COBRANCA'
    end
    object qTotaisVALOR_ACUMULADO: TFloatField
      FieldName = 'VALOR_ACUMULADO'
    end
  end
  object dsTotais: TDataSource
    DataSet = qMovimentosTurnos
    Left = 206
    Top = 401
  end
  object frxSintetico: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 44529.883268425900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 448
    Top = 184
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstMovimentos
        DataSetName = 'frxdstMovimentosTurnos'
      end
      item
        DataSet = dstTotais
        DataSetName = 'frxdstTotais'
      end
      item
        DataSet = frxTurnos
        DataSetName = 'frxdstTurnos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 147.401670000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133890000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000000000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000000000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133890000000000000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133890000000000000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object frxdstTranscodigoProd: TfrxMemoView
          Left = 2.779530000000000000
          Top = 106.826840000000000000
          Width = 37.795300000000000000
          Height = 11.338590000000000000
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxdstMovimentosTurnos."TURNO_ID"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 44.354360000000000000
          Top = 106.826840000000000000
          Width = 245.669450000000000000
          Height = 18.897650000000000000
          DataField = 'NOME_CAIXA'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxdstTurnos."NOME_CAIXA"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 507.590551180000000000
          Top = 106.826840000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DataField = 'DATA_HORA_FECHAMENTO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxdstTurnos."DATA_HORA_FECHAMENTO"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 375.173470000000000000
          Top = 106.826840000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DataField = 'DATA_HORA_ABERTURA'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxdstTurnos."DATA_HORA_ABERTURA"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 297.448818900000000000
          Top = 106.826840000000000000
          Width = 68.031496060000000000
          Height = 18.897642680000000000
          DataField = 'STATUS'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstTurnos."STATUS"]')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Top = 86.929190000000000000
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo18: TfrxMemoView
          Left = 3.779530000000000000
          Top = 86.929190000000000000
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Turno')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 45.354360000000000000
          Top = 86.929190000000000000
          Width = 245.669450000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Caixa')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 508.457020000000000000
          Top = 86.929190000000000000
          Width = 136.063080000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Data/hora fechamento')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 298.582870000000000000
          Top = 86.929190000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Status')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 376.173470000000000000
          Top = 86.929190000000000000
          Width = 124.724490000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Data/hora abertura')
          ParentFont = False
        end
        object Shape4: TfrxShapeView
          Top = 129.063080000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Fill.BackColor = clMenu
        end
        object Memo5: TfrxMemoView
          Left = 3.779530000000000000
          Top = 131.063080000000000000
          Width = 162.519790000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Opera'#231#227'o')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 170.858380000000000000
          Top = 131.063080000000000000
          Width = 83.149660000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Dinheiro')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 265.346630000000000000
          Top = 131.063080000000000000
          Width = 60.472440940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cheque')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 492.118430000000000000
          Top = 131.063080000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Acumulado')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 329.598640000000000000
          Top = 131.063080000000000000
          Width = 64.251970940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cart'#227'o')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 394.291590000000000000
          Top = 131.063080000000000000
          Width = 71.811030940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cobran'#231'a')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 586.606680000000000000
          Top = 131.063080000000000000
          Width = 49.133850940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 642.299630000000000000
          Top = 130.842610000000000000
          Width = 56.692910940000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Pix')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 41.952782760000000000
        Top = 445.984540000000000000
        Width = 718.110700000000000000
        object Line1: TfrxLineView
          Top = 3.779530000000000000
          Width = 718.110700000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 510.236550000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 158.740260000000000000
        Top = 226.771800000000000000
        Width = 718.110700000000000000
        DataSet = frxTurnos
        DataSetName = 'frxdstTurnos'
        RowCount = 0
        object Memo22: TfrxMemoView
          Top = 1.000000000000000000
          Width = 166.299320000000000000
          Height = 18.897650000000000000
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Vl. Abertura')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Top = 22.677180000000000000
          Width = 166.299320000000000000
          Height = 18.897637800000000000
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Recebimentos')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Top = 41.574830000000000000
          Width = 166.299320000000000000
          Height = 18.897650000000000000
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Pagamentos')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Top = 64.252010000000000000
          Width = 166.299320000000000000
          Height = 18.897650000000000000
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Suplementos')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Top = 86.929190000000000000
          Width = 166.299320000000000000
          Height = 18.897650000000000000
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Retiradas')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Top = 106.826840000000000000
          Width = 166.299320000000000000
          Height = 18.897650000000000000
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Vl. Fechamento')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Top = 129.504020000000000000
          Width = 166.299320000000000000
          Height = 18.897650000000000000
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Diferen'#231'a')
          ParentFont = False
        end
        object frxdstTurnosVALOR_INICIAL: TfrxMemoView
          Left = 170.858380000000000000
          Top = 1.000000000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_INICIAL"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_DINHEIRO: TfrxMemoView
          Left = 170.858380000000000000
          Top = 22.677180000000000000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_DINHEIRO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_DINHEIRO"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_CHEQUE: TfrxMemoView
          Left = 265.346590940000000000
          Top = 22.677180000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_CHEQUE'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_CHEQUE"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_CARTAO: TfrxMemoView
          Left = 325.819070940000000000
          Top = 22.677180000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_CARTAO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_CARTAO"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_COBRANCA: TfrxMemoView
          Left = 390.512020940000000000
          Top = 22.677180000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_COBRANCA'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_COBRANCA"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_ACUMULADO: TfrxMemoView
          Left = 495.897960000000000000
          Top = 22.677180000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_ACUMULADO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_ACUMULADO"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_CREDITO: TfrxMemoView
          Left = 575.268050940000000000
          Top = 22.677180000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_CREDITO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_CREDITO"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_PIX: TfrxMemoView
          Left = 644.079120940000000000
          Top = 22.677180000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_PIX'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_PIX"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_PAGTOS_DINHEIRO: TfrxMemoView
          Left = 174.637910000000000000
          Top = 41.574830000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_PAGTOS_DINHEIRO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_PAGTOS_DINHEIRO"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_PAGTOS_CREDITO: TfrxMemoView
          Left = 575.268050940000000000
          Top = 41.574830000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_PAGTOS_CREDITO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_PAGTOS_CREDITO"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_SUPRIMENTO: TfrxMemoView
          Left = 174.637910000000000000
          Top = 64.252010000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_SUPRIMENTO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_SUPRIMENTO"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_SANGRIA_DINHEIRO: TfrxMemoView
          Left = 174.637910000000000000
          Top = 86.929190000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_SANGRIA_DINHEIRO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_SANGRIA_DINHEIRO"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_SANGRIA_CHEQUE: TfrxMemoView
          Left = 246.448940940000000000
          Top = 86.929190000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_SANGRIA_CHEQUE'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_SANGRIA_CHEQUE"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_SANGRIA_CARTAO: TfrxMemoView
          Left = 306.921420940000000000
          Top = 86.929190000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_SANGRIA_CARTAO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_SANGRIA_CARTAO"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_SANGRIA_COBRANCA: TfrxMemoView
          Left = 371.614370940000000000
          Top = 86.929190000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_SANGRIA_COBRANCA'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_SANGRIA_COBRANCA"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_DINHEIRO_INF_FECHAMENTO: TfrxMemoView
          Left = 174.637910000000000000
          Top = 106.826840000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_DINHEIRO_INF_FECHAMENTO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_DINHEIRO_INF_FECHAMENTO"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_CHEQUE_INF_FECHAMENTO: TfrxMemoView
          Left = 246.448940940000000000
          Top = 106.826840000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_CHEQUE_INF_FECHAMENTO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_CHEQUE_INF_FECHAMENTO"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_CARTAO_INF_FECHAMENTO: TfrxMemoView
          Left = 306.921420940000000000
          Top = 106.826840000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_CARTAO_INF_FECHAMENTO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_CARTAO_INF_FECHAMENTO"]')
          ParentFont = False
        end
        object frxdstTurnosVALOR_COBRANCA_INF_FECHAMENTO: TfrxMemoView
          Left = 371.614370940000000000
          Top = 106.826840000000000000
          Width = 105.826840000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_COBRANCA_INF_FECHAMENTO'
          DataSet = frxTurnos
          DataSetName = 'frxdstTurnos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTurnos."VALOR_COBRANCA_INF_FECHAMENTO"]')
          ParentFont = False
        end
        object frxdstTotaisVALOR_DINHEIRO: TfrxMemoView
          Left = 174.637910000000000000
          Top = 129.504020000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_DINHEIRO'
          DataSet = dstTotais
          DataSetName = 'frxdstTotais'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstTotais."VALOR_DINHEIRO"]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          Top = 127.504020000000000000
          Width = 718.110236220472400000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line3: TfrxLineView
          Top = 105.826840000000000000
          Width = 718.110236220472400000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line4: TfrxLineView
          Top = 85.149660000000000000
          Width = 718.110236220472400000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Top = 62.472480000000000000
          Width = 718.110236220472400000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line6: TfrxLineView
          Top = 41.574830000000000000
          Width = 718.110236220472400000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line7: TfrxLineView
          Top = 21.677180000000000000
          Width = 718.110236220472400000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
    end
  end
end
