unit CadastroFornecedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, EditLuka, Vcl.Buttons,
  Vcl.ExtCtrls, FrameVendedores, _FrameHenrancaPesquisas, FrameCondicoesPagamento, FrameDiversosContatos,
  FrameDiversosEnderecos, EditTelefoneLuka, _FrameHerancaPrincipal, FrameEndereco, EditLukaData,
  Vcl.ComCtrls, ComboBoxLuka, RadioGroupLuka, Vcl.Mask, EditCpfCnpjLuka, _RecordsCadastros, _RecordsEspeciais,
  System.Math, System.StrUtils, _Sessao, _Biblioteca, _DiversosContatos, _DiversosEnderecos, _Fornecedores,
  PesquisaFornecedores, PesquisaClientes, Cadastros, FramePlanosFinanceiros,
  CheckBoxLuka, Vcl.Menus, FrameGruposFornecedores, FrameCadastrosTelefones;

type
  TFormCadastroFornecedores = class(TFormCadastros)
    ckSuperSimples: TCheckBox;
    tsCompras: TTabSheet;
    rgTipoFornecedor: TRadioGroupLuka;
    FrPlanoFinanceiroRevenda: TFrPlanosFinanceiros;
    FrPlanoFinanceiroUsoConsumo: TFrPlanosFinanceiros;
    ckFornecedorRevenda: TCheckBoxLuka;
    ckFornecedorUsoConsumo: TCheckBoxLuka;
    rgTipoRateioFrete: TRadioGroupLuka;
    FrGrupoFornecedor: TFrGruposFornecedores;
    ckServico: TCheckBoxLuka;
    FrPlanoFinanceiroServico: TFrPlanosFinanceiros;
    procedure eEmailKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbPesquisarCadastrosClick(Sender: TObject);
    procedure rgTipoPessoaClick(Sender: TObject);
    procedure ckFornecedorRevendaClick(Sender: TObject);
    procedure ckFornecedorUsoConsumoClick(Sender: TObject);
    procedure ckServicoClick(Sender: TObject);
  private
    FCadastroIdEntradaNota: Integer;

    procedure PreencherRegistro(pFornecedor: RecFornecedores);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Modo(pEditando: Boolean); override;
  end;

function CadastrarFornecedorEntradaNota(
  pCNPJ: string;
  pRazaoSocial: string;
  pNomeFantasia: string;
  pInscricaoEstadual: string;
  pBairro: string;
  pCep: string;
  pCodigoIBGeMunicipio: string;
  pEstado: string;
  pLogradouro: string;
  pComplemento: string;
  pCidade: string;
  pCodigoIBGECidade: string;
  pTelefone: string;
  pSuperSimples: Boolean
): TRetornoTelaFinalizar<Integer>;

implementation

{$R *.dfm}

{ TFormCadastroFornecedores }

function CadastrarFornecedorEntradaNota(
  pCNPJ: string;
  pRazaoSocial: string;
  pNomeFantasia: string;
  pInscricaoEstadual: string;
  pBairro: string;
  pCep: string;
  pCodigoIBGeMunicipio: string;
  pEstado: string;
  pLogradouro: string;
  pComplemento: string;
  pCidade: string;
  pCodigoIBGECidade: string;
  pTelefone: string;
  pSuperSimples: Boolean
): TRetornoTelaFinalizar<Integer>;
var
  vForm: TFormCadastroFornecedores;
begin
  vForm := TFormCadastroFornecedores.Create(nil);
  vForm.FormStyle := fsNormal;
  vForm.Visible := False;
  vForm.Modo(True);

  vForm.ckAtivo.Checked := True;
  vForm.rgTipoPessoa.SetIndicePorValor('J');
  vForm.eCPF_CNPJ.Text          := pCNPJ;
  vForm.eRazaoSocial.Text       := pRazaoSocial;
  vForm.eNomeFantasia.Text      := pNomeFantasia;
  vForm.eInscricaoEstadual.Text := pInscricaoEstadual;
  vForm.FrEndereco.setCep(pCep);
  vForm.ckSuperSimples.Checked := pSuperSimples;
  vForm.FrEndereco.setLogradouro(pLogradouro);
  vForm.FrEndereco.setComplemento(pComplemento);
  vForm.FrEndereco.BuscarBairro(pCodigoIBGeMunicipio, pBairro);
//  vForm.eTelefone.Text := pTelefone;

  if Result.Ok(vForm.ShowModal, True) then
    Result.Dados := vForm.FCadastroIdEntradaNota;
end;

procedure TFormCadastroFornecedores.BuscarRegistro;
var
  vDados: TArray<RecFornecedores>;
begin
  inherited;

  vDados := _Fornecedores.BuscarFornecedores(Sessao.getConexaoBanco, 0, [eID.AsInt], False, False, False);
  if vDados <> nil then
    PreencherRegistro(vDados[0]);
end;

procedure TFormCadastroFornecedores.ckFornecedorRevendaClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([FrPlanoFinanceiroRevenda], ckFornecedorRevenda.Checked);
end;

procedure TFormCadastroFornecedores.ckFornecedorUsoConsumoClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([FrPlanoFinanceiroUsoConsumo], ckFornecedorUsoConsumo.Checked);
end;

procedure TFormCadastroFornecedores.ckServicoClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([FrPlanoFinanceiroServico], ckServico.Checked);
end;

procedure TFormCadastroFornecedores.eEmailKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  SetarFoco(FrDiversosEnderecos, Key);
end;

procedure TFormCadastroFornecedores.ExcluirRegistro;
var
  retorno: RecRetornoBD;
begin
  retorno := _Fornecedores.ExcluirFornecedor(Sessao.getConexaoBanco, eId.AsInt);

  if retorno.TeveErro then begin
    _Biblioteca.Exclamar(retorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroFornecedores.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;

  vPlanoFinanceiroRevenda: string;
  vPlanoFinanceiroUsoConsumo: string;
  vPlanoFinanceiroServico: string;

  vGrupoFornecedorId: Integer;
begin
  inherited;

  vPlanoFinanceiroRevenda := '';
  if not FrPlanoFinanceiroRevenda.EstaVazio then
    vPlanoFinanceiroRevenda := FrPlanoFinanceiroRevenda.getDados().PlanoFinanceiroId;

  vPlanoFinanceiroUsoConsumo := '';
  if not FrPlanoFinanceiroUsoConsumo.EstaVazio then
    vPlanoFinanceiroUsoConsumo := FrPlanoFinanceiroUsoConsumo.getDados().PlanoFinanceiroId;

  vPlanoFinanceiroServico := '';
  if not FrPlanoFinanceiroServico.EstaVazio then
    vPlanoFinanceiroServico := FrPlanoFinanceiroServico.getDados().PlanoFinanceiroId;

  vGrupoFornecedorId := 0;
  if not FrGrupoFornecedor.EstaVazio then
    vGrupoFornecedorId := FrGrupoFornecedor.GetGrupo().GrupoId;

  vRetBanco :=
    _Fornecedores.AtualizarFornecedor(
      Sessao.getConexaoBanco,
      FPesquisouCadastro,
      eID.AsInt,
      FCadastro,
      FTelefones,
      FEnderecos,
      ToChar(ckSuperSimples),
      eObservacoes.Text,
      rgTipoFornecedor.GetValor,
      ToChar(ckAtivo),
      ckFornecedorRevenda.CheckedStr,
      ckFornecedorUsoConsumo.CheckedStr,
      ckServico.CheckedStr,
      vPlanoFinanceiroRevenda,
      vPlanoFinanceiroUsoConsumo,
      vPlanoFinanceiroServico,
      rgTipoRateioFrete.GetValor,
      vGrupoFornecedorId
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  if vRetBanco.AsInt > 0 then begin
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + IntToStr(vRetBanco.AsInt));
    FCadastroIdEntradaNota := vRetBanco.AsInt;
  end
  else
    _Biblioteca.Informar(coAlteracaoRegistroSucesso);
end;

procedure TFormCadastroFornecedores.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    rgTipoFornecedor,
    ckSuperSimples,
    ckFornecedorRevenda,
    ckFornecedorUsoConsumo,
    FrPlanoFinanceiroRevenda,
    FrPlanoFinanceiroUsoConsumo,
    rgTipoRateioFrete,
    FrGrupoFornecedor,
    ckServico,
    FrPlanoFinanceiroServico],
    pEditando
  );

  if pEditando then begin
    ckFornecedorRevendaClick(nil);
    ckFornecedorUsoConsumoClick(nil);
    ckServicoClick(nil);
  end;
end;

procedure TFormCadastroFornecedores.sbPesquisarCadastrosClick(Sender: TObject);
begin
  inherited;
  BuscarRegistro;
end;

procedure TFormCadastroFornecedores.PreencherRegistro(pFornecedor: RecFornecedores);
begin
  ckSuperSimples.Checked := (pFornecedor.super_simples = 'S');
  eObservacoes.Lines.Text := pFornecedor.observacoes;
  ckAtivo.Checked := (pFornecedor.ativo = 'S');

  ckFornecedorRevenda.CheckedStr    := pFornecedor.Revenda;
  ckFornecedorUsoConsumo.CheckedStr := pFornecedor.UsoConsumo;
  ckServico.CheckedStr              := pFornecedor.Servico;

  ckFornecedorRevendaClick(nil);
  ckFornecedorUsoConsumoClick(nil);
  ckServicoClick(nil);

  rgTipoPessoaClick(nil);
  rgTipoFornecedor.SetIndicePorValor(pFornecedor.tipo_fornecedor);

  FrPlanoFinanceiroRevenda.InserirDadoPorChave(pFornecedor.PlanoFinanceiroRevenda, False);
  FrPlanoFinanceiroUsoConsumo.InserirDadoPorChave(pFornecedor.PlanoFinanceiroUsoConsumo, False);
  FrPlanoFinanceiroServico.InserirDadoPorChave(pFornecedor.PlanoFinanceiroServico, False);

  rgTipoRateioFrete.SetIndicePorValor( pFornecedor.TipoRateioFrete );
  FrGrupoFornecedor.InserirDadoPorChave( pFornecedor.GrupoFornecedorId, False );

  pFornecedor.Free;
end;

procedure TFormCadastroFornecedores.rgTipoPessoaClick(Sender: TObject);
begin
  inherited;
//  _Biblioteca.Habilitar([ckSuperSimples, rgTipoRateioFrete, rgTipoFornecedor], rgTipoPessoa.GetValor = 'J');
  if not rgTipoRateioFrete.Enabled then
    rgTipoRateioFrete.SetIndicePorValor('M');

  if not rgTipoFornecedor.Enabled then
    rgTipoFornecedor.SetIndicePorValor('O');
end;

procedure TFormCadastroFornecedores.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if rgTipoFornecedor.ItemIndex < 0  then begin
    _Biblioteca.Exclamar('O tipo de fornecedor n�o foi informado corretamente, verifique!');
    SetarFoco(rgTipoFornecedor);
    Abort;
  end;

  if (rgTipoFornecedor.GetValor = 'O') and (ckFornecedorRevenda.Checked) then begin
    _Biblioteca.Exclamar('O tipo de fornecedor n�o pode ser para revenda quando o tipo do mesmo for "Outro", verifique!');
    SetarFoco(ckFornecedorRevenda);
    Abort;
  end;

  if rgTipoRateioFrete.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('O tipo de rateio do frete n�o foi informado corretamente, verifique!');
    SetarFoco(rgTipoRateioFrete);
    Abort;
  end;
end;

end.
