unit InclusaoTitulosReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca, System.StrUtils,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Mask, System.Math, _ContasReceber,
  EditLukaData, FrameEmpresas, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, _Sessao,
  FrameClientes, EditTelefoneLuka, FrameTiposCobranca, _RecordsFinanceiros, _RecordsEspeciais,
  FrameCentroCustos, FrameDadosCobranca, FramePlanosFinanceiros, CheckBoxLuka,
  MemoAltis, _BibliotecaGenerica;

type
  TFormInclusaoTitulosReceber = class(TFormHerancaCadastroCodigo)
    FrCliente: TFrClientes;
    FrEmpresa: TFrEmpresas;
    lb1: TLabel;
    eValorDocumento: TEditLuka;
    FrCentroCustos: TFrCentroCustos;
    FrDadosCobranca: TFrDadosCobranca;
    FrPlanoFinanceiro: TFrPlanosFinanceiros;
    lbllb12: TLabel;
    lbl2: TLabel;
    lb2: TLabel;
    eDataEmissao: TEditLukaData;
    meObservacoes: TMemoAltis;
    eDataContabil: TEditLukaData;
    Label2: TLabel;
    eValorRetencao: TEditLuka;
    Label3: TLabel;
    ePercentualRetencao: TEditLuka;
    procedure eValorDocumentoChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure eValorRetencaoChange(Sender: TObject);
    procedure ePercentualRetencaoChange(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormInclusaoTitulosReceber }

procedure TFormInclusaoTitulosReceber.BuscarRegistro;
begin
  inherited;

end;

procedure TFormInclusaoTitulosReceber.ePercentualRetencaoChange(
  Sender: TObject);
begin
  inherited;
  if ePercentualRetencao.Focused and (eValorDocumento.AsDouble > 0) then begin
    eValorRetencao.AsDouble := _BibliotecaGenerica.ZeroSeNegativo(eValorDocumento.AsDouble * ePercentualRetencao.AsDouble * 0.01);
  end;

  FrDadosCobranca.FDadosRetencao := eValorRetencao.AsDouble;
end;

procedure TFormInclusaoTitulosReceber.eValorDocumentoChange(Sender: TObject);
begin
  inherited;
  FrDadosCobranca.ValorPagar := eValorDocumento.AsDouble;
end;

procedure TFormInclusaoTitulosReceber.eValorRetencaoChange(Sender: TObject);
begin
  inherited;
  if (eValorRetencao.Focused or (Sender = nil)) and(eValorDocumento.AsDouble > 0) then begin
    ePercentualRetencao.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercDesconto(eValorDocumento.AsCurr, 0, eValorRetencao.AsDouble);
  end;

  FrDadosCobranca.FDadosRetencao := eValorRetencao.AsDouble;
end;

procedure TFormInclusaoTitulosReceber.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormInclusaoTitulosReceber.FormCreate(Sender: TObject);
begin
  inherited;
  FrDadosCobranca.Natureza := 'R';
end;

procedure TFormInclusaoTitulosReceber.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vData: TDateTime;
  vRetBanco: RecRetornoBD;
  vTitulos: TArray<RecTitulosFinanceiros>;
begin
  inherited;

  vData := Sessao.getData;

  vTitulos := FrDadosCobranca.Titulos;
  Sessao.getConexaoBanco.IniciarTransacao;

  for i := Low(vTitulos) to High(vTitulos) do begin
    vRetBanco :=
      _ContasReceber.AtualizarContaReceber(
        Sessao.getConexaoBanco,
        eID.AsInt,
        FrEmpresa.getEmpresa().EmpresaId,
        vTitulos[i].CobrancaId,
        FrCliente.getCliente.cadastro_id,
        vTitulos[i].Parcela,
        vTitulos[i].NumeroParcelas,
        vTitulos[i].PortadorId,
        FrPlanoFinanceiro.getDados().PlanoFinanceiroId,
        vTitulos[i].Agencia,
        vTitulos[i].Banco,
        vTitulos[i].NumeroCheque,
        vTitulos[i].Valor,
        vTitulos[i].NossoNumero,
        vTitulos[i].Documento,
        'A',
        vTitulos[i].DataVencimento,
        vTitulos[i].DataVencimento,
        vData,
        vTitulos[i].TelefoneEmitente,
        vTitulos[i].NomeEmitente,
        vTitulos[i].CpfCnpjEmitente,
        '',
        vTitulos[i].ContaCorrente,
        vTitulos[i].CodigoBarras,
        meObservacoes.Lines.Text,
        FrCentroCustos.GetCentro().CentroCustoId,
        'MAN',
        0,
        0,
        eDataContabil.AsData,
        vTitulos[i].ValorRetencao,
        True
      );

    Sessao.AbortarSeHouveErro( vRetBanco );
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar('Novo contas a receber gerado com sucesso. C�digo: ' + _Biblioteca.NFormat(vRetBanco.AsInt) );
end;

procedure TFormInclusaoTitulosReceber.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    FrCliente,
    FrEmpresa,
    eValorDocumento,
    eValorRetencao,
    ePercentualRetencao,
    meObservacoes,
    eDataEmissao,
    eDataContabil,
    FrDadosCobranca,
    FrPlanoFinanceiro,
    FrCentroCustos],
    pEditando
  );

  if pEditando then
    _Biblioteca.SetarFoco(FrCliente);
end;

procedure TFormInclusaoTitulosReceber.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormInclusaoTitulosReceber.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('O cliente n�o foi informado corretamente, verifique!');
    _Biblioteca.SetarFoco(FrCliente);
    Abort;
  end;

  if FrEmpresa.EstaVazio then begin
    Exclamar('A empresa n�o foi informada corretamente, verifique!');
    _Biblioteca.SetarFoco(FrEmpresa);
    Abort;
  end;

  if FrPlanoFinanceiro.EstaVazio then begin
    Exclamar('O plano financeiro n�o foi informado corretamente, verifique!');
    _Biblioteca.SetarFoco(FrPlanoFinanceiro);
    Abort;
  end;

  if FrCentroCustos.EstaVazio then begin
    Exclamar('O centro de custo n�o foi informado corretamente, verifique!');
    _Biblioteca.SetarFoco(FrCentroCustos);
    Abort;
  end;

  if eValorDocumento.AsCurr = 0 then begin
    Exclamar('O valor do t�tulo tem que ser maior que 0, verifique!');
    _Biblioteca.SetarFoco(eValorDocumento);
    Abort;
  end;

  if FrDadosCobranca.ExisteDiferenca then begin
    Exclamar('Existe diferen�a nos t�tulos lan�ados, verifique!');
    _Biblioteca.SetarFoco(FrDadosCobranca);
    Abort;
  end;
end;

end.
