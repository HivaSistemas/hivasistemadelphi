unit GerarRetiradas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, _Biblioteca, _RecordsExpedicao,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _RecordsOrcamentosVendas,
  _FrameHenrancaPesquisas, FrameClientes, FrameProdutos, Vcl.StdCtrls, _RetiradasPendentes, _RetiradasItensPendentes,
  FrameNumeros, Vcl.Grids, GridLuka, Vcl.Menus, System.Math, _RecordsEspeciais, _Orcamentos,
  _Retiradas, _ComunicacaoNFE, Informacoes.Orcamento, FrameDataInicialFinal, Frame.DocumentoFiscalEmitir,
  Buscar.DocumentoFiscalEmitir, StaticTextLuka, ImpressaoComprovanteEntregaGrafico, _Devolucoes, FrameLocais,
  FrameVendedores, CheckBoxLuka;

type
  TFormGerarRetiradas = class(TFormHerancaRelatoriosPageControl)
    FrClientes: TFrClientes;
    FrProdutos: TFrProdutos;
    FrOrcamentos: TFrNumeros;
    sgOrcamentos: TGridLuka;
    spSeparador: TSplitter;
    sgItens: TGridLuka;
    pmPendencias: TPopupMenu;
    FrDataRecebimento: TFrDataInicialFinal;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    FrLocaisPendencia: TFrLocais;
    pmItens: TPopupMenu;
    FrVendedores: TFrVendedores;
    miGerarRetiradaProdutosSelecionados: TSpeedButton;
    miGerarPendenciaEntregaProdutosSelecionados: TSpeedButton;
    miDefinirQuantidadeTodosItens: TSpeedButton;
    miRemoverQuantidadeTodosItensF8: TSpeedButton;
    procedure sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sgOrcamentosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure miGerarRetiradaProdutosSelecionadosClick(Sender: TObject);
    procedure sgOrcamentosDblClick(Sender: TObject);
    procedure miGerarPendenciaEntregaProdutosSelecionadosClick(Sender: TObject);
    procedure miDefinirQuantidadeTodosItensClick(Sender: TObject);
    procedure miRemoverQuantidadeTodosItensF8Click(Sender: TObject);
    procedure sgItensKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    FItens: TArray<RecRetiradasItensPendentes>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  (* Grid Orcamentos *)
  coEmpresa      = 0;
  coLocal        = 1;
  coOrcamentoId  = 2;
  coCliente      = 3;
  coDataCadastro = 4;
  coVendedor     = 5;

  (* Ocultas *)
  coLocalId      = 6;

  (* Grid de Itens *)
  ciProdutoId   = 0;
  ciNome        = 1;
  ciMarca       = 2;
  ciQuantidade  = 3;
  ciUnidade     = 4;
  ciLote        = 5;
  ciQtdeRetirar = 6;
  ciItemId      = 7;
  ciLocalId     = 8;
  ciMultiploVenda = 9;

procedure TFormGerarRetiradas.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = ciQtdeRetirar then begin
    AFont.Color := coCorFonteEdicao1;
    ABrush.Color := coCorCelulaEdicao1;
  end;
end;

procedure TFormGerarRetiradas.sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_F8 then
    miRemoverQuantidadeTodosItensF8Click(Sender)
  else if Key = VK_F6 then
    miDefinirQuantidadeTodosItensClick(Sender);
end;

procedure TFormGerarRetiradas.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := ACol in[ciQtdeRetirar];
end;

procedure TFormGerarRetiradas.sgOrcamentosClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vLocalId: Integer;
  vOrcamentoId: Integer;
begin
  inherited;
  vLinha := 0;
  sgItens.ClearGrid;
  vOrcamentoId := SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]);
  vLocalId := SFormatInt(sgOrcamentos.Cells[coLocalId, sgOrcamentos.Row]);
  for i := Low(FItens) to High(FItens) do begin
    if
      (vOrcamentoId <> FItens[i].OrcamentoId) or
      (vLocalId <> FItens[i].LocalId)
    then
      Continue;

    Inc(vLinha);

    sgItens.Cells[ciProdutoId, vLinha]   := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]        := FItens[i].Nome;
    sgItens.Cells[ciMarca, vLinha]       := FItens[i].Nome;
    sgItens.Cells[ciQuantidade, vLinha]  := NFormatEstoque(FItens[i].Saldo);
    sgItens.Cells[ciUnidade, vLinha]     := FItens[i].Unidade;
    sgItens.Cells[ciLote, vLinha]        := FItens[i].Lote;
    sgItens.Cells[ciQtdeRetirar, vLinha] := '';
    sgItens.Cells[ciItemId, vLinha]      := NFormat(FItens[i].ItemId);
    sgItens.Cells[ciLocalId, vLinha]     := NFormat(FItens[i].LocalId);
    sgItens.Cells[ciMultiploVenda, vLinha] := NFormat(FItens[i].MultiploVenda);
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormGerarRetiradas.sgOrcamentosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]));
end;

procedure TFormGerarRetiradas.sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coOrcamentoId then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgOrcamentos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormGerarRetiradas.Carregar(Sender: TObject);
var
  i: Integer;
  vComando: string;
  vOrcamentoIds: TArray<Integer>;
  vRetiradasPendentes: TArray<RecRetiradasPendentes>;
begin
  inherited;
  sgItens.ClearGrid;
  sgOrcamentos.ClearGrid;

  FItens := nil;
  vOrcamentoIds := nil;

  vComando := ' and RET.EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId) + ' ';
  vComando := vComando + ' and (RET.EMPRESA_ID, RET.LOCAL_ID, RET.ORCAMENTO_ID) in(select EMPRESA_ID, LOCAL_ID, ORCAMENTO_ID from RETIRADAS_ITENS_PENDENTES where SALDO > 0) ';

  if not FrClientes.EstaVazio then
    vComando := vComando + ' and ' + FrClientes.getSqlFiltros('ORC.CLIENTE_ID');

  if not FrProdutos.EstaVazio then
    vComando := vComando + ' and ORC.ORCAMENTO_ID in(select distinct ORCAMENTO_ID from ORCAMENTOS_ITENS where ' + FrProdutos.getSqlFiltros('PRODUTO_ID') + ') ';

  if not FrOrcamentos.EstaVazio then
    vComando := vComando + ' and ' + FrOrcamentos.TrazerFiltros('ORC.ORCAMENTO_ID');

  if not FrVendedores.EstaVazio then
    vComando := vComando + ' and ' + FrVendedores.getSqlFiltros('ORC.VENDEDOR_ID');

  if not FrDataRecebimento.NenhumaDataValida then
    vComando := vComando + ' and ' + FrDataRecebimento.getSqlFiltros('trunc(ORC.DATA_HORA_RECEBIMENTO)');

  if not FrLocaisPendencia.EstaVazio then
    vComando := vComando + ' and ' + FrLocaisPendencia.getSqlFiltros('RET.LOCAL_ID');

  vComando := vComando + ' order by RET.ORCAMENTO_ID ';

  vRetiradasPendentes := _RetiradasPendentes.BuscarRetiradasComando(Sessao.getConexaoBanco, vComando);
  if vRetiradasPendentes = nil then begin
    NenhumRegistro;
    Exit;
  end;

  SetLength(vOrcamentoIds, Length(vRetiradasPendentes));
  for i := Low(vRetiradasPendentes) to High(vRetiradasPendentes) do begin
    sgOrcamentos.Cells[coEmpresa, i + 1]      := NFormat(vRetiradasPendentes[i].EmpresaId) + ' - ' + vRetiradasPendentes[i].NomeEmpresa;
    sgOrcamentos.Cells[coLocal, i + 1]        := NFormat(vRetiradasPendentes[i].LocalId) + ' - ' + vRetiradasPendentes[i].NomeLocal;
    sgOrcamentos.Cells[coOrcamentoId, i + 1]  := NFormat(vRetiradasPendentes[i].OrcamentoId);
    sgOrcamentos.Cells[coCliente, i + 1]      := NFormat(vRetiradasPendentes[i].ClienteId) + ' - ' + vRetiradasPendentes[i].NomeCliente;
    sgOrcamentos.Cells[coDataCadastro, i + 1] := DFormat(vRetiradasPendentes[i].DataCadastro);
    sgOrcamentos.Cells[coVendedor, i + 1]     := NFormat(vRetiradasPendentes[i].VendedorId) + ' - ' + vRetiradasPendentes[i].NomeVendedor;
    sgOrcamentos.Cells[coLocalId, i + 1]      := NFormat(vRetiradasPendentes[i].LocalId);

    vOrcamentoIds[i] := vRetiradasPendentes[i].OrcamentoId;
  end;
  sgOrcamentos.RowCount := IfThen(Length(vRetiradasPendentes) > 1, High(vRetiradasPendentes) + 2, 2);

  FItens := _RetiradasItensPendentes.BuscarRetiradasItensPendentesComando(Sessao.getConexaoBanco, ' and ' + FiltroInInt('EIP.ORCAMENTO_ID', vOrcamentoIds) + ' and EIP.SALDO > 0 ');
  sgOrcamentosClick(Sender);

  pcDados.ActivePage := tsResultado;
  sgOrcamentos.SetFocus;
end;

procedure TFormGerarRetiradas.FormShow(Sender: TObject);
begin
  inherited;
  sgItens.Col := ciQtdeRetirar;
end;

procedure TFormGerarRetiradas.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormGerarRetiradas.miDefinirQuantidadeTodosItensClick(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  for i := 1 to sgItens.RowCount -1 do
    sgItens.Cells[ciQtdeRetirar, i] := sgItens.Cells[ciQuantidade, i];
end;

procedure TFormGerarRetiradas.miGerarPendenciaEntregaProdutosSelecionadosClick(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vItens: TArray<RecRetiradasItensPendentes>;
begin
  inherited;

  vItens := nil;
  for i := 1 to sgItens.RowCount -1 do begin
    if SFormatCurr(sgItens.Cells[ciQtdeRetirar, i]) = 0 then
      Continue;

    SetLength(vItens, Length(vItens) + 1);

    vItens[High(vItens)].ProdutoId  := SFormatInt(sgItens.Cells[ciProdutoId, i]);
    vItens[High(vItens)].ItemId     := SFormatInt(sgItens.Cells[ciItemId, i]);
    vItens[High(vItens)].LocalId    := SFormatInt(sgItens.Cells[ciLocalId, i]);
    vItens[High(vItens)].Quantidade := SFormatCurr(sgItens.Cells[ciQtdeRetirar, i]);
    vItens[High(vItens)].Lote       := sgItens.Cells[ciLote, i];
  end;

  if vItens = nil then begin
    Exclamar('N�o foi definido nenhum produto para gera��o de retirada!');
    Exit;
  end;

  if _Devolucoes.ExistemDevolucoesPendentes(Sessao.getConexaoBanco, SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row])) then begin
    Exclamar('N�o � permitida esta opera��o enquanto houver pend�ncias de devolu��es para o pedido ' + sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row] + '!');
    Abort;
  end;

  Sessao.getConexaoBanco.IniciarTransacao;

  vRetBanco :=
    _RetiradasPendentes.GerarEntregaPendenteRetira(
      Sessao.getConexaoBanco,
      SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]),
      Sessao.getEmpresaLogada.EmpresaId,
      SFormatInt(sgOrcamentos.Cells[coLocalId, sgOrcamentos.Row]),
      vItens,
      Sessao.getParametros.DefinirLocalManual
    );

  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar('Entrega pendente gerada com sucesso.');
  Carregar(Sender);
end;

procedure TFormGerarRetiradas.miGerarRetiradaProdutosSelecionadosClick(Sender: TObject);
var
  i: Integer;
  vRetiradaId: Integer;
  vNotaFiscalId: Integer;
  vRetBanco: RecRetornoBD;
  vItens: TArray<RecRetiradasItensPendentes>;
  vDocumentoEmitir: TRetornoTelaFinalizar<RecDocumentoFiscalEmitir>;
begin
  inherited;

  vItens := nil;
  for i := 1 to sgItens.RowCount -1 do begin
    if SFormatCurr(sgItens.Cells[ciQtdeRetirar, i]) = 0 then
      Continue;

    SetLength(vItens, Length(vItens) + 1);

    vItens[High(vItens)].ProdutoId  := SFormatInt(sgItens.Cells[ciProdutoId, i]);
    vItens[High(vItens)].ItemId     := SFormatInt(sgItens.Cells[ciItemId, i]);
    vItens[High(vItens)].LocalId    := SFormatInt(sgItens.Cells[ciLocalId, i]);
    vItens[High(vItens)].Quantidade := SFormatCurr(sgItens.Cells[ciQtdeRetirar, i]);
    vItens[High(vItens)].Lote       := sgItens.Cells[ciLote, i];
  end;

  if vItens = nil then begin
    Exclamar('N�o foi definido nenhum produto para gera��o de retirada!');
    Exit;
  end;

  if _Devolucoes.ExistemDevolucoesPendentes(Sessao.getConexaoBanco, SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row])) then begin
    Exclamar('N�o � permitida esta opera��o enquanto houver pend�ncias de devolu��es para o pedido ' + sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row] + '!');
    Abort;
  end;

  vDocumentoEmitir := Buscar.DocumentoFiscalEmitir.Buscar( False );
  if vDocumentoEmitir.BuscaCancelada then
    Exit;

  Sessao.getConexaoBanco.IniciarTransacao;

  vRetBanco :=
    _RetiradasPendentes.GerarRetiradaRetiraPendente(
      Sessao.getConexaoBanco,
      SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]),
      Sessao.getEmpresaLogada.EmpresaId,
      SFormatInt(sgOrcamentos.Cells[coLocalId, sgOrcamentos.Row]),
      vDocumentoEmitir.Dados.TipoNotaGerar,
      vItens
    );

  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  vRetiradaId := vRetBanco.AsInt;

  vRetBanco :=
    _Orcamentos.GerarNotaRetiraAto(
      Sessao.getConexaoBanco,
      [vRetiradaId],
      False
    );

  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  vNotaFiscalId := vRetBanco.AsInt;

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar('Retirada gerada com sucesso.');

  // Se n�o foi selecionado nada para ser emitido
  if vDocumentoEmitir.Dados.NenhumDocumentoFiscalEmitir then
    MensagemPadraoNaoEmissaoNFe
  else begin
    if not _ComunicacaoNFE.EnviarNFe(vNotaFiscalId) then
      MensagemPadraoNaoEmissaoNFe;
  end;

  if vDocumentoEmitir.Dados.EmitirRecibo then
    ImpressaoComprovanteEntregaGrafico.Imprimir( vRetiradaId, tpRetirada );

  Carregar(Sender);
end;

procedure TFormGerarRetiradas.miRemoverQuantidadeTodosItensF8Click(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  for i := 1 to sgItens.RowCount -1 do
    sgItens.Cells[ciQtdeRetirar, i] := '';
end;

procedure TFormGerarRetiradas.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

procedure TFormGerarRetiradas.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ACol <> ciQtdeRetirar then
    Exit;

  TextCell := NFormatNEstoque(SFormatDouble(TextCell));
  if SFormatCurr(TextCell) > SFormatCurr(sgItens.Cells[ciQuantidade, ARow]) then begin
    _Biblioteca.Exclamar('A quantidade a retirar n�o pode ser maior que a quantidade pendente!');
    TextCell := NFormatNEstoque(SFormatCurr(sgItens.Cells[ciQuantidade, ARow]));
    sgItens.SetFocus;
    Exit;
  end;

  if not ValidarMultiplo(SFormatDouble(TextCell), SFormatDouble(sgItens.Cells[ciMultiploVenda, ARow])) then begin
    TextCell := '';
    Exit;
  end;
end;

procedure TFormGerarRetiradas.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ciProdutoId, ciQuantidade, ciQtdeRetirar] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
