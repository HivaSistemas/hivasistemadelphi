unit FrameAdministradorasCartoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _AdministradorasCartoes,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, _Biblioteca, PesquisaAdministradorasCartoes,
  Vcl.Menus;

type
  TFrAdministradorasCartoes = class(TFrameHenrancaPesquisas)
  public
    function getDados(pLinha: Integer = -1): RecAdministradorasCartoes;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrAdministradorasCartoes }

function TFrAdministradorasCartoes.AdicionarDireto: TObject;
var
  vDados: TArray<RecAdministradorasCartoes>;
begin
  vDados := _AdministradorasCartoes.BuscarAdministradorasCartoes(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrAdministradorasCartoes.AdicionarPesquisando: TObject;
begin
  Result := PesquisaAdministradorasCartoes.Pesquisar(ckSomenteAtivos.Checked);
end;

function TFrAdministradorasCartoes.getDados(pLinha: Integer): RecAdministradorasCartoes;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecAdministradorasCartoes(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrAdministradorasCartoes.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecAdministradorasCartoes(FDados[i]).AdministradoraId = RecAdministradorasCartoes(pSender).AdministradoraId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrAdministradorasCartoes.MontarGrid;
var
  i: Integer;
  vSender: RecAdministradorasCartoes;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecAdministradorasCartoes(FDados[i]);
      AAdd([IntToStr(vSender.AdministradoraId), vSender.Descricao]);
    end;
  end;
end;

end.
