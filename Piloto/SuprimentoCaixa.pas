unit SuprimentoCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _MovimentosTurnos,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _RecordsCaixa, _RecordsEspeciais,
  _FrameHenrancaPesquisas, FrameFuncionarios, FrameContas, Vcl.Mask, EditLukaData,
  _Biblioteca, _Sessao, _Funcionarios, _TurnosCaixas, _HerancaCadastro;

type
  TFormSuprimentoCaixa = class(TFormHerancaCadastro)
    FrCaixaAberto: TFrFuncionarios;
    eValorDinheiro: TEditLuka;
    lb11: TLabel;
    FrContaOrigem: TFrContas;
    lb1: TLabel;
    eObservacoes: TMemo;
    procedure FormCreate(Sender: TObject);
  private
    FTurnoId: Integer;
    procedure FrCaixaOnAposPesquisar(Sender: TObject);
  protected
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormSuprimentoCaixa }

procedure TFormSuprimentoCaixa.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormSuprimentoCaixa.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;

  vRetorno :=
    _MovimentosTurnos.AtualizarMovimentosTurno(
      Sessao.getConexaoBanco,
      0,
      FTurnoId,
      FrContaOrigem.GetConta().Conta,
      eValorDinheiro.AsCurr,
      0,
      0,
      0,
      0,
      'SUP',
      eObservacoes.Text
    );

  if vRetorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  if vRetorno.AsInt > 0 then
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + IntToStr(vRetorno.AsInt));
end;

procedure TFormSuprimentoCaixa.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([FrContaOrigem, eValorDinheiro], pEditando);

  if pEditando then
    SetarFoco(FrContaOrigem)
  else begin
    FrCaixaAberto.Modo(True);
    SetarFoco(FrCaixaAberto);
  end;
end;

procedure TFormSuprimentoCaixa.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormSuprimentoCaixa.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrCaixaAberto.EstaVazio then begin
    Exclamar('N�o foi informado o caixa para ser realizada a opera��o, verifique!');
    FrCaixaAberto.SetFocus;
    Abort;
  end;

  if FrContaOrigem.EstaVazio then begin
    Exclamar('N�o foi inforamado o caixa/banco da origem do dinheiro, verifique!');
    FrContaOrigem.SetFocus;
    Abort;
  end;
end;

procedure TFormSuprimentoCaixa.FormCreate(Sender: TObject);
begin
  inherited;
  FrCaixaAberto.OnAposPesquisar := FrCaixaOnAposPesquisar;
  FrCaixaAberto.Modo(True);
end;

procedure TFormSuprimentoCaixa.FrCaixaOnAposPesquisar(Sender: TObject);
var
  vTurnos: TArray<RecTurnosCaixas>;
begin
  vTurnos := _TurnosCaixas.BuscarTurnosCaixas( Sessao.getConexaoBanco, 1, [FrCaixaAberto.GetFuncionario.funcionario_id] );

  if vTurnos = nil then begin
    Exclamar('N�o foi encontrado turno em aberto para o funcion�rio escolhido, verifique!');
    SetarFoco(FrCaixaAberto);
    Exit;
  end;

  FTurnoId := vTurnos[0].TurnoId;
  Destruir(TArray<TObject>(vTurnos));

  Modo(True);
  FrCaixaAberto.Modo(False, False);
end;

end.
