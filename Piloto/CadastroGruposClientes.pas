unit CadastroGruposClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Sessao, _ClientesGrupos,
  CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, _RecordsEspeciais, PesquisaClientesGrupos;

type
  TFormCadastroGruposClientes = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
  private
    procedure PreencherRegistro(pDados: RecClientesGrupos);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroGruposClientes }

procedure TFormCadastroGruposClientes.BuscarRegistro;
var
  vDados: TArray<RecClientesGrupos>;
begin
  vDados := _ClientesGrupos.BuscarClientesGrupos(Sessao.getConexaoBanco, 0, [eId.AsInt], False);
  if vDados = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vDados[0]);
end;

procedure TFormCadastroGruposClientes.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _ClientesGrupos.ExcluirClientesGrupos(Sessao.getConexaoBanco, eId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroGruposClientes.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _ClientesGrupos.AtualizarClientesGrupos(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      _Biblioteca.ToChar(ckAtivo)
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroGruposClientes.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eNome,
    ckAtivo],
    pEditando
  );

  if pEditando then
    SetarFoco(eNome);
end;

procedure TFormCadastroGruposClientes.PesquisarRegistro;
var
  vDado: RecClientesGrupos;
begin
  vDado := RecClientesGrupos(PesquisaClientesGrupos.Pesquisar(False));
  if vDado = nil then
    Exit;

  inherited;
  PreencherRegistro(vDado);
end;

procedure TFormCadastroGruposClientes.PreencherRegistro(pDados: RecClientesGrupos);
begin
  eID.AsInt          := pDados.ClienteGrupoId;
  eNome.Text         := pDados.Nome;
  ckAtivo.CheckedStr := pDados.Ativo;

  pDados.Free;
end;

procedure TFormCadastroGruposClientes.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('O nome grupo de cliente n�o foi informado corretamente, verifique!');
    SetarFoco(eNome);
    Abort;
  end;
end;

end.
