unit CadastroContas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsCadastros, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, _RecordsEspeciais, PesquisaBancosCaixas, _Contas,
  RadioGroupLuka, ComboBoxLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameEmpresas, CheckBoxLuka, FrameFuncionarios, _ContasFuncAutorizados;

type
  TFormCadastroContas = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
    lb2: TLabel;
    eAgencia: TEditLuka;
    lb3: TLabel;
    eDigitoConta: TEditLuka;
    ckAceitaSaldoNegativo: TCheckBox;
    eSaldoAtual: TEditLuka;
    lb6: TLabel;
    lb7: TLabel;
    eUltimoChequeEmitido: TEditLuka;
    ckEmiteCheque: TCheckBox;
    rgTipo: TRadioGroupLuka;
    cbBanco: TComboBoxLuka;
    lb5: TLabel;
    ckEmiteBoletoBancario: TCheckBox;
    lb8: TLabel;
    eNossoNumeroInicial: TEditLuka;
    lb9: TLabel;
    eProximoNossoNumero: TEditLuka;
    lb10: TLabel;
    eNossoNumeroFinal: TEditLuka;
    FrEmpresa: TFrEmpresas;
    Label3: TLabel;
    eDigitoAgencia: TEditLuka;
    lb4: TLabel;
    eCarteira: TEditLuka;
    lb11: TLabel;
    eNumeroRemessaBoleto: TEditLuka;
    lb12: TLabel;
    eCodigoEmpresaCobranca: TEditLuka;
    FrFuncionariosAutorizados: TFrFuncionarios;
    procedure rgTipoClick(Sender: TObject);
    procedure ckEmiteChequeClick(Sender: TObject);
    procedure ckEmiteBoletoBancarioClick(Sender: TObject);
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); reintroduce;
  private
    procedure PreencherRegistro(pConta: RecContas);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroBancosCaixas }

procedure TFormCadastroContas.BuscarRegistro;
var
  vBancosCaixas: TArray<RecContas>;
begin
  vBancosCaixas := _Contas.BuscarContas(Sessao.getConexaoBanco, 0, [eId.Text], '', False, 0);

  inherited;
  if vBancosCaixas <> nil then
    PreencherRegistro(vBancosCaixas[0]);
end;

procedure TFormCadastroContas.ckEmiteBoletoBancarioClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([eNossoNumeroInicial, eNossoNumeroFinal, eProximoNossoNumero], ckEmiteBoletoBancario.Checked);
end;

procedure TFormCadastroContas.ckEmiteChequeClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([eUltimoChequeEmitido], ckEmiteCheque.Checked);
end;

procedure TFormCadastroContas.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
//  inherited;
  if Key <> VK_RETURN then
   Exit;

  if Trim(eID.Text) = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar a conta!');
    Abort;
  end;

  BuscarRegistro;
end;

procedure TFormCadastroContas.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Contas.ExcluirContas(Sessao.getConexaoBanco, eId.Text);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroContas.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _Contas.AtualizarConta(
      Sessao.getConexaoBanco,
      eID.Text,
      eDigitoConta.Text,
      eAgencia.Text,
      eDigitoAgencia.Text,
      eNome.Text,
      FrEmpresa.getEmpresa().EmpresaId,
      rgTipo.ItemIndex,
      cbBanco.GetValor,
      _Biblioteca.ToChar(ckAceitaSaldoNegativo),
      _Biblioteca.ToChar(ckEmiteCheque),
      eUltimoChequeEmitido.AsInt,
      ToChar(ckEmiteBoletoBancario),
      eNossoNumeroInicial.AsInt,
      eNossoNumeroFinal.AsInt,
      eProximoNossoNumero.AsInt,
      eCarteira.Text,
      eNumeroRemessaBoleto.AsInt,
      eCodigoEmpresaCobranca.Text,
      _Biblioteca.ToChar(ckAtivo),
      FrFuncionariosAutorizados.GetArrayOfInteger
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroContas.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eNome,
    FrEmpresa,
    rgTipo,
    eDigitoConta,
    cbBanco,
    eAgencia,
    eDigitoAgencia,
    ckAceitaSaldoNegativo,
    eSaldoAtual,
    eUltimoChequeEmitido,
    ckEmiteBoletoBancario,
    eNossoNumeroInicial,
    eNossoNumeroFinal,
    eProximoNossoNumero,
    eCarteira,
    eNumeroRemessaBoleto,
    eCodigoEmpresaCobranca,
    ckAtivo,
    FrFuncionariosAutorizados],
    pEditando
  );

  rgTipoClick(nil);

  if pEditando then begin
    SetarFoco(eNome);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormCadastroContas.PesquisarRegistro;
var
  vBanco: RecContas;
begin
  vBanco := RecContas(PesquisaBancosCaixas.PesquisarBancoCaixa('', False, 0));
  if vBanco = nil then
    Exit;

  inherited;
  PreencherRegistro(vBanco);
end;

procedure TFormCadastroContas.PreencherRegistro(pConta: RecContas);
var
  i: Integer;
  vFuncionarios: TArray<Integer>;
  vAutorizados: TArray<RecContasFuncAutorizados>;
begin
  eID.Text                      := pConta.Conta;

  eNome.Text                    := pConta.Nome;
  FrEmpresa.InserirDadoPorChave(pConta.EmpresaId, False);
  rgTipo.ItemIndex              := pConta.Tipo;
  rgTipoClick(nil);

  eDigitoConta.Text             := pConta.DigitoConta;
  eAgencia.Text                 := pConta.Agencia;
  eDigitoAgencia.Text           := pConta.DigitoAgencia;

  ckAtivo.Checked               := (pConta.Ativo = 'S');
  ckAceitaSaldoNegativo.Checked := (pConta.AceitaSaldoNegativo = 'S');
  eSaldoAtual.AsDouble          := pConta.SaldoAtual;

  ckEmiteCheque.Checked      := (pConta.EmiteCheque = 'S');
  ckEmiteChequeClick(nil);
  eUltimoChequeEmitido.AsInt := pConta.NumeroUltimoChequeEmitido;

  cbBanco.SetIndicePorValor(pConta.CodigoBanco);
  ckEmiteBoletoBancario.Checked := (pConta.EmiteBoleto = 'S');
  ckEmiteBoletoBancarioClick(nil);
  eNossoNumeroInicial.AsInt  := pConta.NossoNumeroInicial;
  eNossoNumeroFinal.AsInt    := pConta.NossoNumeroFinal;
  eProximoNossoNumero.AsInt  := pConta.ProximoNossoNumero;
  eCarteira.Text             := pConta.Carteira;
  eCodigoEmpresaCobranca.Text := pConta.CodigoEmpresaCobranca;
  eNumeroRemessaBoleto.AsInt := pConta.NumeroRemessaBoleto;

  if eSaldoAtual.AsCurr < 0 then
    eSaldoAtual.Font.Color := clRed
  else
    eSaldoAtual.Font.Color := $000096DB;

  vAutorizados := _ContasFuncAutorizados.BuscarContasFuncAutorizados(Sessao.getConexaoBanco, 0, [pConta.Conta]);

  for i := Low(vAutorizados) to High(vAutorizados) do
    _Biblioteca.AddNoVetorSemRepetir(vFuncionarios, vAutorizados[i].FuncionarioId);

  FrFuncionariosAutorizados.InserirDadoPorChaveTodos(vFuncionarios, False);
end;

procedure TFormCadastroContas.rgTipoClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([
    eDigitoConta,
    eAgencia,
    eDigitoAgencia,
    eCodigoEmpresaCobranca,
    eCarteira,
    cbBanco,
    ckEmiteCheque,
    ckEmiteBoletoBancario],
    rgTipo.ItemIndex = 1
  );

  ckEmiteChequeClick(nil);
  ckEmiteBoletoBancarioClick(nil);
end;

procedure TFormCadastroContas.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o nome do banco!');
    SetarFoco(eNome);
    Abort;
  end;

  if FrEmpresa.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar a empresa a que pertence este banco/caixa!');
    SetarFoco(FrEmpresa);
    Abort;
  end;

  if rgTipo.ItemIndex = 1 then begin
    if cbBanco.GetValor = '' then begin
      _Biblioteca.Exclamar('� necess�rio informar o banco!');
      SetarFoco(cbBanco);
      Abort;
    end;
  end;
end;

end.
