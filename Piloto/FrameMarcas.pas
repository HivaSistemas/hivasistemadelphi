unit FrameMarcas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, _Marcas, PesquisaMarcas, System.Math,
  Vcl.Buttons, Vcl.Menus;

type
  TFrMarcas = class(TFrameHenrancaPesquisas)
  public
    function GetMarca(pLinha: Integer = -1): RecMarcas;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrMarcas }

function TFrMarcas.AdicionarDireto: TObject;
var
  marcas: TArray<RecMarcas>;
begin
  marcas := _Marcas.BuscarMarcas(Sessao.getConexaoBanco, 0, [FChaveDigitada] );
  if marcas = nil then
    Result := nil
  else
    Result := marcas[0];
end;

function TFrMarcas.AdicionarPesquisando: TObject;
begin
  Result := PesquisaMarcas.PesquisarMarca;
end;

function TFrMarcas.GetMarca(pLinha: Integer): RecMarcas;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecMarcas(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrMarcas.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecMarcas(FDados[i]).marca_id = RecMarcas(pSender).marca_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrMarcas.MontarGrid;
var
  i: Integer;
  pSender: RecMarcas;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecMarcas(FDados[i]);
      AAdd([IntToStr(pSender.marca_id), pSender.nome]);
    end;
  end;
end;

end.
