unit CentralRelacionamentoCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo,
  EditTelefoneLuka, Vcl.Mask, EditCpfCnpjLuka, Vcl.ComCtrls,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, Frame.Cadastros,
  Vcl.StdCtrls, CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids,
  GridLuka, FrameClientes, FrameTextoFormatado, FrameEdicaoTexto, ComboBoxLuka,
  FrameDataInicialFinal, FrameArquivos, Vcl.Menus;

type
  TFormCentralRelacionamentoCliente = class(TFormHerancaCadastroCodigo)
    pcDados: TPageControl;
    tsGeral: TTabSheet;
    tsOcorrencias: TTabSheet;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    lbCPF_CNPJ: TLabel;
    eCPF_CNPJ: TEditCPF_CNPJ_Luka;
    lbRazaoSocialNome: TLabel;
    eRazaoSocial: TEditLuka;
    lbNomeFantasiaApelido: TLabel;
    eNomeFantasia: TEditLuka;
    lb11: TLabel;
    eTelefone: TEditTelefoneLuka;
    lb12: TLabel;
    eCelular: TEditTelefoneLuka;
    lb9: TLabel;
    eEmail: TEditLuka;
    eBancoUsuario: TEditLuka;
    Label2: TLabel;
    Label3: TLabel;
    eBancoSenha: TEditLuka;
    Label4: TLabel;
    eBancoIpServidor: TEditLuka;
    sgBancoDados: TGridLuka;
    Label5: TLabel;
    eBancoPorta: TEditLuka;
    eBancoServico: TEditLuka;
    Label6: TLabel;
    eRemotoIP: TEditLuka;
    Label7: TLabel;
    Label8: TLabel;
    eRemotoSenha: TEditLuka;
    Label9: TLabel;
    eRemotoObservacoes: TEditLuka;
    sgAcessoRemoto: TGridLuka;
    Label10: TLabel;
    mmObservacoesCliente: TMemo;
    FrCliente: TFrClientes;
    sgOcorrencia: TGridLuka;
    pgDetalhesOcorrencia: TPageControl;
    tsDescricaoOcorrencia: TTabSheet;
    frDescricaoOcorrencia: TFrEdicaoTexto;
    cbStatusOcorrencia: TComboBoxLuka;
    Label11: TLabel;
    frDataCadastro: TFrDataInicialFinal;
    FrDataConclusao: TFrDataInicialFinal;
    sbFiltrarOcorrencia: TSpeedButton;
    sbGravarOcorrencia: TSpeedButton;
    sbNovaOcorrencia: TSpeedButton;
    sbCancelarOcorrencia: TSpeedButton;
    tsSolucaoOcorrencia: TTabSheet;
    frSolucaoOcorrencia: TFrEdicaoTexto;
    FrArquivos: TFrArquivos;
    TabSheet3: TTabSheet;
    Label12: TLabel;
    eRemotoUsuario: TEditLuka;
    pmOpcoesOcorrencias: TPopupMenu;
    miReceberPedidoSelecionado: TMenuItem;
    miN2: TMenuItem;
    miConsultarStatusServicoNFe: TMenuItem;
    Concluda1: TMenuItem;
    Programao1: TMenuItem;
    Emteste1: TMenuItem;
    Recusada1: TMenuItem;
    Reabrir1: TMenuItem;
    Mdia1: TMenuItem;
    Mdia2: TMenuItem;
    Alta1: TMenuItem;
    Urgente1: TMenuItem;
    N1: TMenuItem;
    Vincularsolicitao1: TMenuItem;
    Label13: TLabel;
    cbPrioridadeOcorrencia: TComboBoxLuka;
    procedure FormCreate(Sender: TObject);
    procedure eRemotoObservacoesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sgAcessoRemotoDblClick(Sender: TObject);
    procedure sgAcessoRemotoDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure eBancoServicoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sgBancoDadosDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgBancoDadosDblClick(Sender: TObject);
    procedure sbNovaOcorrenciaClick(Sender: TObject);
    procedure sbFiltrarOcorrenciaClick(Sender: TObject);
    procedure sgOcorrenciaClick(Sender: TObject);
    procedure sbGravarOcorrenciaClick(Sender: TObject);
    procedure sbCancelarOcorrenciaClick(Sender: TObject);
    procedure sbAnexosOcorrenciaClick(Sender: TObject);
    procedure sgOcorrenciaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgOcorrenciaGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure SpeedButton1Click(Sender: TObject);
    procedure sbAlterarStatusOcorrenciaClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FrArquivossbArquivosClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Concluda1Click(Sender: TObject);
    procedure Programao1Click(Sender: TObject);
    procedure Emteste1Click(Sender: TObject);
    procedure Recusada1Click(Sender: TObject);
    procedure Reabrir1Click(Sender: TObject);
    procedure Mdia1Click(Sender: TObject);
    procedure Mdia2Click(Sender: TObject);
    procedure Alta1Click(Sender: TObject);
    procedure Urgente1Click(Sender: TObject);
    procedure sgAcessoRemotoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sgBancoDadosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    crmId: Integer;
    FAcessoRemotoLinha: Integer;
    FBancoDadosLinha: Integer;
    FNovaOcorrencia: Boolean;
    procedure FrClienteOnAposPesquisar(Sender: TObject);
    procedure FrClienteOnAposDeletar(Sender: TObject);
    procedure AddAcessoRemotoGrid(
      pIP: string;
      pSenha: string;
      pObservacao: string;
      pUsuario: string
    );
    procedure AddBancoDadosGrid(
      pUsuario: string;
      pSenha: string;
      pServidor: string;
      pPorta: string;
      pServico: string
    );

    procedure AddOcorrenciaGrid(
      pCodigo: Integer;
      pDataCadastro: TDateTime;
      pDataConclusao: TDateTime;
      pStatusAnalitico: string;
      pStatus: string;
      pSolicitacao: Integer;
      pPrioridadeAnalitico: string;
      pPrioridade: string
    );

    procedure CarregarOcorrencias;
  public
    procedure Modo(pEditando: Boolean); override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
  end;

var
  FormCentralRelacionamentoCliente: TFormCentralRelacionamentoCliente;

implementation

{$R *.dfm}

uses _RecordsCadastros, _Clientes, _Sessao, _CRM, _RecordsEspeciais,
  _Biblioteca, _CRMAcessoRemoto, _CRMBancoDados, _CRMOcorrencias, _Arquivos;

const
  //sgAcessoRemoto
  coAcessoRemotoIp         = 0;
  coAcessoRemotoSenha      = 1;
  coAcessoRemotoUsuario    = 2;
  coAcessoRemotoObservacao = 3;

  //sgBancoDados
  coBancoDadosUsuario  = 0;
  coBancoDadosSenha    = 1;
  coBancoDadosServidor = 2;
  coBancoDadosPorta    = 3;
  coBancoDadosServico  = 4;

  //sgOcorrencias
  coOcorrenciasCodigo          = 0;
  coOcorrenciasDataCadastro    = 1;
  coOcorrenciasDataConclusao   = 2;
  coOcorrenciasStatusAnalitico = 3;
  coOcorrenciasPrioridadeAnalitico = 4;
  coOcorrenciasSolicitacao     = 5;
  coOcorrenciasAnexo           = 6;
  coOcorrenciasStatus          = 7;
  coOcorrenciasPrioridade      = 8;

{ TFormCentralRelacionamentoCliente }

procedure TFormCentralRelacionamentoCliente.eBancoServicoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eBancoUsuario.Text = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o "Usu�rio" do banco de dados!');
    SetarFoco(eBancoUsuario);
    Exit;
  end;

  if eBancoSenha.Text = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar a "Senha" do banco de dados!');
    SetarFoco(eBancoSenha);
    Exit;
  end;

  if eBancoIpServidor.Text = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o "IP Servidor" do banco de dados!');
    SetarFoco(eBancoIpServidor);
    Exit;
  end;

  if eBancoPorta.Text = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar a "Porta" do banco de dados!');
    SetarFoco(eBancoPorta);
    Exit;
  end;

  if eBancoServico.Text = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o "Servi�o" do banco de dados!');
    SetarFoco(eBancoServico);
    Exit;
  end;

  AddBancoDadosGrid(
    eBancoUsuario.Text,
    eBancoSenha.Text,
    eBancoIpServidor.Text,
    eBancoPorta.Text,
    eBancoServico.Text
  );

  eBancoUsuario.Text := '';
  eBancoSenha.Text := '';
  eBancoIpServidor.Text := '';
  eBancoPorta.Text := '';
  eBancoServico.Text := '';
  eBancoUsuario.SetFocus;
end;

procedure TFormCentralRelacionamentoCliente.Emteste1Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar o status da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'TE'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.AddBancoDadosGrid(
  pUsuario: string;
  pSenha: string;
  pServidor: string;
  pPorta: string;
  pServico: string
);
var
  vLinha: Integer;
begin
  if FBancoDadosLinha = 0 then begin

    if sgBancoDados.Cells[coBancoDadosUsuario, 1] = '' then
      FBancoDadosLinha := 1
    else begin
      FBancoDadosLinha := sgBancoDados.RowCount;
      sgBancoDados.RowCount := sgBancoDados.RowCount + 1;
    end;
  end;

  sgBancoDados.Cells[coBancoDadosUsuario, FBancoDadosLinha]  := pUsuario;
  sgBancoDados.Cells[coBancoDadosSenha, FBancoDadosLinha]    := pSenha;
  sgBancoDados.Cells[coBancoDadosServidor, FBancoDadosLinha] := pServidor;
  sgBancoDados.Cells[coBancoDadosPorta, FBancoDadosLinha]    := pPorta;
  sgBancoDados.Cells[coBancoDadosServico, FBancoDadosLinha]  := pServico;

  sgBancoDados.Row := FBancoDadosLinha;
  FBancoDadosLinha := 0;
end;

procedure TFormCentralRelacionamentoCliente.eRemotoObservacoesKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eRemotoIP.Text = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o "IP" do acesso remoto!');
    SetarFoco(eRemotoIP);
    Exit;
  end;

  if eRemotoSenha.Text = '' then begin
    Exclamar('� necess�rio informar a "Senha" do acesso remoto!');
    SetarFoco(eRemotoSenha);
    Exit;
  end;

  AddAcessoRemotoGrid(
    eRemotoIP.Text,
    eRemotoSenha.Text,
    eRemotoObservacoes.Text,
    eRemotoUsuario.Text
  );

  eRemotoIP.Text := '';
  eRemotoSenha.Text := '';
  eRemotoObservacoes.Text := '';
  eRemotoUsuario.Text := '';
  eRemotoIp.SetFocus;
end;

procedure TFormCentralRelacionamentoCliente.AddAcessoRemotoGrid(
  pIP: string;
  pSenha: string;
  pObservacao: string;
  pUsuario: string
);
var
  vLinha: Integer;
begin
  if FAcessoRemotoLinha = 0 then begin

    if sgAcessoRemoto.Cells[coAcessoRemotoIP, 1] = '' then
      FAcessoRemotoLinha := 1
    else begin
      FAcessoRemotoLinha := sgAcessoRemoto.RowCount;
      sgAcessoRemoto.RowCount := sgAcessoRemoto.RowCount + 1;
    end;
  end;

  sgAcessoRemoto.Cells[coAcessoRemotoIP, FAcessoRemotoLinha]         := pIP;
  sgAcessoRemoto.Cells[coAcessoRemotoSenha, FAcessoRemotoLinha]      := pSenha;
  sgAcessoRemoto.Cells[coAcessoRemotoObservacao, FAcessoRemotoLinha] := pObservacao;
  sgAcessoRemoto.Cells[coAcessoRemotoUsuario, FAcessoRemotoLinha]    := pUsuario;

  sgAcessoRemoto.Row := FAcessoRemotoLinha;
  FAcessoRemotoLinha := 0;
end;

procedure TFormCentralRelacionamentoCliente.ExcluirRegistro;
begin
  if crmId <> 0 then begin
    _CRMAcessoRemoto.ExcluirCRMAcessoRemoto(Sessao.getConexaoBanco, crmId);
    _CRMBancoDados.ExcluirCRMBancoDados(Sessao.getConexaoBanco, crmId);
    _CRM.ExcluirCRM(Sessao.getConexaoBanco, crmId, FrCliente.getCliente(0).cadastro_id);
  end;
  inherited;
end;

procedure TFormCentralRelacionamentoCliente.FormCreate(Sender: TObject);
begin
  inherited;
  FAcessoRemotoLinha := 0;
  FBancoDadosLinha := 0;
  FrCliente.OnAposDeletar   := FrClienteOnAposDeletar;
  FrCliente.OnAposPesquisar := FrClienteOnAposPesquisar;
end;

procedure TFormCentralRelacionamentoCliente.FormShow(Sender: TObject);
begin
  inherited;
  eRemotoIP.CharCase          := ecNormal;
  eRemotoSenha.CharCase       := ecNormal;
  eRemotoObservacoes.CharCase := ecNormal;
  eBancoUsuario.CharCase      := ecNormal;
  eBancoSenha.CharCase        := ecNormal;
  eBancoIpServidor.CharCase   := ecNormal;
  eBancoPorta.CharCase        := ecNormal;
  eBancoServico.CharCase      := ecNormal;
  eRemotoUsuario.CharCase     := ecNormal;
end;

procedure TFormCentralRelacionamentoCliente.FrArquivossbArquivosClick(
  Sender: TObject);
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para anexar documentos da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  if sgOcorrencia.Cells[coOcorrenciasCodigo, 1] = '' then
    Exit;

  FrArquivos.Origem := 'CRM';
  FrArquivos.Id     := StrToInt(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row]);
  FrArquivos.buscarArquivos;
  FrArquivos.sbArquivosClick(Sender);
  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.FrClienteOnAposDeletar(
  Sender: TObject);
begin
  sgOcorrencia.ClearGrid();
  frDescricaoOcorrencia.Clear;
  frSolucaoOcorrencia.Clear;
  sgAcessoRemoto.ClearGrid();
  sgBancoDados.ClearGrid();
  pcDados.ActivePage := tsGeral;
  Modo(False);
end;

procedure TFormCentralRelacionamentoCliente.FrClienteOnAposPesquisar(
  Sender: TObject);
var
  i: Integer;
  clientes: TArray<RecClientes>;
  crm: TArray<RecCRM>;
  acessosRemotos: TArray<RecCRMAcessoRemoto>;
  bancoDados: TArray<RecCRMBancoDados>;
begin
  Modo(True);
  clientes := _Clientes.BuscarClientes(Sessao.getConexaoBanco, 0, [FrCliente.getCliente(0).cadastro_id], False);
  eCPF_CNPJ.Text     := clientes[0].RecCadastro.cpf_cnpj;
  eRazaoSocial.Text  := clientes[0].RecCadastro.razao_social;
  eNomeFantasia.Text := clientes[0].RecCadastro.nome_fantasia;
  eTelefone.Text     := clientes[0].RecCadastro.TelefonePrincipal;
  eCelular.Text      := clientes[0].RecCadastro.TelefoneCelular;
  eEmail.Text        := clientes[0].RecCadastro.e_mail;

  crm := _CRM.BuscarCRM(Sessao.getConexaoBanco, 1, [FrCliente.getCliente(0).cadastro_id]);
  crmId := 0;
  if crm <> nil then begin
    crmId := crm[0].central_id;
    mmObservacoesCliente.Text := crm[0].observacao;
  end;

  if crmId > 0 then begin
    acessosRemotos := _CRMAcessoRemoto.BuscarCRMAcessoRemoto(Sessao.getConexaoBanco, 0, [crmId]);
    for i := 0 to Length(acessosRemotos) - 1 do
      AddAcessoRemotoGrid(acessosRemotos[i].ip, acessosRemotos[i].senha, acessosRemotos[i].observacao, acessosRemotos[i].usuario);

    bancoDados := _CRMBancoDados.BuscarCRMBancoDados(Sessao.getConexaoBanco, 0, [crmId]);
    for i := 0 to Length(bancoDados) - 1 do
      AddBancoDadosGrid(bancoDados[i].usuario, bancoDados[i].senha, bancoDados[i].ip_servidor, bancoDados[i].porta, bancoDados[i].servico);
  end;

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.AddOcorrenciaGrid(
  pCodigo: Integer;
  pDataCadastro: TDateTime;
  pDataConclusao: TDateTime;
  pStatusAnalitico: string;
  pStatus: string;
  pSolicitacao: Integer;
  pPrioridadeAnalitico: string;
  pPrioridade: string
);
var
  vLinha: Integer;
  Arquivos: TArray<RecArquivos>;
begin
  if sgOcorrencia.Cells[coOcorrenciasCodigo, 1] = '' then
    vLinha := 1
  else begin
    vLinha := sgOcorrencia.RowCount;
    sgOcorrencia.RowCount := sgOcorrencia.RowCount + 1;
  end;

  sgOcorrencia.Cells[coOcorrenciasCodigo, vLinha]          := NFormat(pCodigo);
  sgOcorrencia.Cells[coOcorrenciasDataCadastro, vLinha]    := DFormat(pDataCadastro);
  sgOcorrencia.Cells[coOcorrenciasDataConclusao, vLinha]   := DFormatN(pDataConclusao);
  sgOcorrencia.Cells[coOcorrenciasSolicitacao, vLinha]     := NFormatN(pSolicitacao);
  sgOcorrencia.Cells[coOcorrenciasStatusAnalitico, vLinha] := pStatusAnalitico;
  sgOcorrencia.Cells[coOcorrenciasStatus, vLinha]          := pStatus;

  sgOcorrencia.Cells[coOcorrenciasPrioridadeAnalitico, vLinha] := pPrioridadeAnalitico;
  sgOcorrencia.Cells[coOcorrenciasPrioridade, vLinha]          := pPrioridade;


  Arquivos := _Arquivos.BuscarArquivos(Sessao.getConexaoBanco, 2, [pCodigo]);
  if Arquivos <> nil then
    sgOcorrencia.Cells[coOcorrenciasAnexo, vLinha]           := 'Sim'
  else
    sgOcorrencia.Cells[coOcorrenciasAnexo, vLinha]           := 'N�o';

end;

procedure TFormCentralRelacionamentoCliente.Alta1Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar a prioridade da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarCRMPrioridades(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'AL'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.CarregarOcorrencias;
var
  i: Integer;
  ocorrencias: TArray<RecCRMOcorrencias>;
begin
  sgOcorrencia.ClearGrid();
  ocorrencias := _CRMOcorrencias.BuscarCRMOcorrencias(sessao.getConexaoBanco, 1, [FrCliente.getCliente(0).cadastro_id]);
  if ocorrencias <> nil then begin
    for i := 0 to Length(ocorrencias) - 1 do
      AddOcorrenciaGrid(
        ocorrencias[i].ocorrencia_id,
        ocorrencias[i].dataCadastro,
        ocorrencias[i].dataConclusao,
        ocorrencias[i].status_analitico,
        ocorrencias[i].status,
        ocorrencias[i].solicitacao,
        ocorrencias[i].prioridade_analitico,
        ocorrencias[i].prioridade
      );

    sgOcorrencia.Row := 1;
    sgOcorrenciaClick(nil);
  end;
end;

procedure TFormCentralRelacionamentoCliente.Concluda1Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar o status da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'CO'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  acessosRemoto: TArray<RecCRMAcessoRemoto>;
  bancoDados: Tarray<RecCRMBancoDados>;
begin
  inherited;
  for i := 1 to sgAcessoRemoto.RowCount -1 do begin
    SetLength(acessosRemoto, Length(acessosRemoto) + 1);

    acessosRemoto[High(acessosRemoto)].item_id    := i;
    acessosRemoto[High(acessosRemoto)].ip         := sgAcessoRemoto.Cells[coAcessoRemotoIP, i];
    acessosRemoto[High(acessosRemoto)].senha      := sgAcessoRemoto.Cells[coAcessoRemotoSenha, i];
    acessosRemoto[High(acessosRemoto)].observacao := sgAcessoRemoto.Cells[coAcessoRemotoObservacao, i];
    acessosRemoto[High(acessosRemoto)].usuario    := sgAcessoRemoto.Cells[coAcessoRemotoUsuario, i];
  end;

  for i := 1 to sgBancoDados.RowCount -1 do begin
    SetLength(bancoDados, Length(bancoDados) + 1);

    bancoDados[High(bancoDados)].item_id     := i;
    bancoDados[High(bancoDados)].ip_servidor := sgBancoDados.Cells[coBancoDadosServidor, i];
    bancoDados[High(bancoDados)].senha       := sgBancoDados.Cells[coBancoDadosSenha, i];
    bancoDados[High(bancoDados)].usuario     := sgBancoDados.Cells[coBancoDadosUsuario, i];
    bancoDados[High(bancoDados)].porta       := sgBancoDados.Cells[coBancoDadosPorta, i];
    bancoDados[High(bancoDados)].servico     := sgBancoDados.Cells[coBancoDadosServico, i];
  end;

  vRetBanco := _CRM.AtualizarCRM(
    Sessao.getConexaoBanco,
    crmId,
    FrCliente.getCliente(0).cadastro_id,
    mmObservacoesCliente.Text,
    acessosRemoto,
    bancoDados
  );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  if crmId = 0 then
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + NFormat(vRetBanco.AsInt));

  Modo(false);
end;

procedure TFormCentralRelacionamentoCliente.Mdia1Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar a prioridade da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarCRMPrioridades(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'BA'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.Mdia2Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar a prioridade da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarCRMPrioridades(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'ME'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.Modo(pEditando: Boolean);
begin
  FrCliente.Enabled := not pEditando;
  sbGravar.Enabled := pEditando;
  sbPesquisar.Enabled := not pEditando;
  sbDesfazer.Enabled := pEditando;
  sbExcluir.Enabled := pEditando;

  _Biblioteca.Habilitar([
    eCPF_CNPJ,
    eRazaoSocial,
    eNomeFantasia,
    eTelefone,
    eCelular,
    eEmail,
    mmObservacoesCliente,
    eRemotoIP,
    eRemotoSenha,
    eRemotoUsuario,
    eRemotoObservacoes,
    sgAcessoRemoto,
    eBancoUsuario,
    eBancoSenha,
    eBancoIpServidor,
    eBancoPorta,
    eBancoServico,
    sgBancoDados],
    pEditando
  );

  if not pEditando then begin
    Self.ActiveControl := FrCliente;
    FrCliente.Clear;
    FrCliente.SetFocus;
    crmId := 0;
  end;
end;

procedure TFormCentralRelacionamentoCliente.Programao1Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar o status da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'PR'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.Reabrir1Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar o status da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'AB'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.Recusada1Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar o status da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'RE'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.sgAcessoRemotoDblClick(
  Sender: TObject);
begin
  inherited;
  if sgAcessoRemoto.Cells[coAcessoRemotoIP, sgAcessoRemoto.Row] = '' then
    Exit;

  FAcessoRemotoLinha := sgAcessoRemoto.Row;
  eRemotoIP.Text          := sgAcessoRemoto.Cells[coAcessoRemotoIP, sgAcessoRemoto.Row];
  eRemotoSenha.Text       := sgAcessoRemoto.Cells[coAcessoRemotoSenha, sgAcessoRemoto.Row];
  eRemotoObservacoes.Text := sgAcessoRemoto.Cells[coAcessoRemotoObservacao, sgAcessoRemoto.Row];
  eRemotoUsuario.Text     := sgAcessoRemoto.Cells[coAcessoRemotoUsuario, sgAcessoRemoto.Row];
end;

procedure TFormCentralRelacionamentoCliente.sgAcessoRemotoDrawCell(
  Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  vAlinhamento := taLeftJustify;
  sgAcessoRemoto.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCentralRelacionamentoCliente.sgAcessoRemotoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_DELETE) then
    sgAcessoRemoto.DeleteRow(sgAcessoRemoto.Row);
end;

procedure TFormCentralRelacionamentoCliente.sgBancoDadosDblClick(
  Sender: TObject);
begin
  inherited;
  if sgBancoDados.Cells[coBancoDadosUsuario, sgBancoDados.Row] = '' then
    Exit;

  FBancoDadosLinha      := sgBancoDados.Row;
  eBancoUsuario.Text    := sgBancoDados.Cells[coBancoDadosUsuario, sgBancoDados.Row];
  eBancoSenha.Text      := sgBancoDados.Cells[coBancoDadosSenha, sgBancoDados.Row];
  eBancoIpServidor.Text := sgBancoDados.Cells[coBancoDadosServidor, sgBancoDados.Row];
  eBancoPorta.Text      := sgBancoDados.Cells[coBancoDadosPorta, sgBancoDados.Row];
  eBancoServico.Text    := sgBancoDados.Cells[coBancoDadosServico, sgBancoDados.Row];
end;

procedure TFormCentralRelacionamentoCliente.sgBancoDadosDrawCell(
  Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  vAlinhamento := taLeftJustify;
  sgBancoDados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCentralRelacionamentoCliente.sgBancoDadosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_DELETE) then
    sgBancoDados.DeleteRow(sgBancoDados.Row);
end;

procedure TFormCentralRelacionamentoCliente.sgOcorrenciaClick(Sender: TObject);
var
  ocorrencias: TArray<RecCRMOcorrencias>;
  ocorrenciaId: Integer;
begin
  inherited;
  if sgOcorrencia.Cells[coOcorrenciasCodigo, 1] = '' then
    Exit;

  FNovaOcorrencia := False;
  frDescricaoOcorrencia.Clear;
  frSolucaoOcorrencia.Clear;

  ocorrenciaId := StrToInt(RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row]));
  ocorrencias := _CRMOcorrencias.BuscarCRMOcorrencias(Sessao.getConexaoBanco, 0, [ocorrenciaId]);
  frDescricaoOcorrencia.SetTexto(ocorrencias[0].descricao);
  frSolucaoOcorrencia.SetTexto(ocorrencias[0].solucao);
end;

procedure TFormCentralRelacionamentoCliente.sgOcorrenciaDrawCell(
  Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coOcorrenciasCodigo, coOcorrenciasSolicitacao] then
    vAlinhamento := taRightJustify
  else if ACol in[coOcorrenciasStatusAnalitico, coOcorrenciasPrioridadeAnalitico] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgOcorrencia.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCentralRelacionamentoCliente.sgOcorrenciaGetCellColor(
  Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush;
  AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coOcorrenciasStatusAnalitico then begin
    if sgOcorrencia.Cells[coOcorrenciasStatus, ARow] = 'AB' then begin
      AFont.Color := clBlue;
      AFont.Style  := [fsBold];
    end
    else if sgOcorrencia.Cells[coOcorrenciasStatus, ARow] = 'CO' then begin
      AFont.Color := clGreen;
      AFont.Style  := [fsBold];
    end
    else if sgOcorrencia.Cells[coOcorrenciasStatus, ARow] = 'PR' then begin
      AFont.Color := clMaroon;
      AFont.Style  := [fsBold];
    end
    else if sgOcorrencia.Cells[coOcorrenciasStatus, ARow] = 'TE' then begin
      AFont.Color := clOlive;
      AFont.Style  := [fsBold];
    end
    else begin
      AFont.Color := clRed;
      AFont.Style  := [fsBold];
    end;
  end
  else if ACol = coOcorrenciasPrioridadeAnalitico then begin
    if sgOcorrencia.Cells[coOcorrenciasPrioridade, ARow] = 'BA' then begin
      AFont.Color := clBlue;
      AFont.Style  := [fsBold];
    end
    else if sgOcorrencia.Cells[coOcorrenciasPrioridade, ARow] = 'ME' then begin
      AFont.Color := clOlive;
      AFont.Style  := [fsBold];
    end
    else if sgOcorrencia.Cells[coOcorrenciasPrioridade, ARow] = 'AL' then begin
      AFont.Color := clGreen;
      AFont.Style  := [fsBold];
    end
    else if sgOcorrencia.Cells[coOcorrenciasPrioridade, ARow] = 'UR' then begin
      AFont.Color := clMaroon;
      AFont.Style  := [fsBold];
    end;
  end
  else if ACol = coOcorrenciasAnexo then begin
    if sgOcorrencia.Cells[coOcorrenciasAnexo, ARow] = 'Sim' then begin
      AFont.Color := clGreen;
      AFont.Style  := [fsBold];
    end
    else begin
      AFont.Color := clRed;
      AFont.Style  := [fsBold];
    end;
  end;
end;

procedure TFormCentralRelacionamentoCliente.SpeedButton1Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar o status da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'CO'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.SpeedButton2Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar o status da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'AB'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.SpeedButton3Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar o status da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'PR'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.SpeedButton5Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar o status da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'TE'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.Urgente1Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar a prioridade da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarCRMPrioridades(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'UR'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.sbAlterarStatusOcorrenciaClick(
  Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para alterar o status da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  vRetBanco := _CRMOcorrencias.AtualizarStatusCRMOcorrencias(
    Sessao.getConexaoBanco,
    StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row])),
    'RE'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao atualizar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    Abort;
  end
  else
    Informar('Ocorr�ncia atualizada!');

  CarregarOcorrencias;
end;

procedure TFormCentralRelacionamentoCliente.sbAnexosOcorrenciaClick(
  Sender: TObject);
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then begin
    Exclamar('Para anexar documentos da ocorr�ncia � necess�rio grava-l� primeiro');
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;
end;

procedure TFormCentralRelacionamentoCliente.sbCancelarOcorrenciaClick(
  Sender: TObject);
begin
  inherited;
  if FNovaOcorrencia and (not _Biblioteca.Perguntar('A nova ocorr�ncia ainda n�o foi gravada, deseja descartar?') ) then begin
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  FNovaOcorrencia := False;
  sgOcorrenciaClick(nil);
end;

procedure TFormCentralRelacionamentoCliente.sbFiltrarOcorrenciaClick(
  Sender: TObject);
var
  vComando: string;
  ocorrencias: TArray<RecCRMOcorrencias>;
  i: Integer;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia and (not _Biblioteca.Perguntar('A nova ocorr�ncia ainda n�o foi gravada, deseja descartar?') ) then begin
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  FNovaOcorrencia := False;
  frDescricaoOcorrencia.Clear;
  frSolucaoOcorrencia.Clear;

  sgOcorrencia.ClearGrid();

  if frDataCadastro.DatasValidas then
    _Biblioteca.WhereOuAnd(vComando, frDataCadastro.getSqlFiltros('DATA_CADASTRO'));

  if FrDataConclusao.DatasValidas then
    _Biblioteca.WhereOuAnd(vComando, FrDataConclusao.getSqlFiltros('DATA_CONCLUSAO'));

  if cbStatusOcorrencia.GetValor <> '' then
    _Biblioteca.WhereOuAnd(vComando, 'STATUS = ''' + cbStatusOcorrencia.GetValor + '''');

  if cbPrioridadeOcorrencia.GetValor <> '' then
    _Biblioteca.WhereOuAnd(vComando, 'PRIORIDADE = ''' + cbPrioridadeOcorrencia.GetValor + '''');

  _Biblioteca.WhereOuAnd(vComando, 'CADASTRO_ID = ''' + IntToStr(FrCliente.getCliente(0).cadastro_id) + '''');

  ocorrencias := _CRMOcorrencias.BuscarCRMOcorrenciasComando(sessao.getConexaoBanco, vComando);
  if ocorrencias <> nil then begin
    for i := 0 to Length(ocorrencias) - 1 do
      AddOcorrenciaGrid(
        ocorrencias[i].ocorrencia_id,
        ocorrencias[i].dataCadastro,
        ocorrencias[i].dataConclusao,
        ocorrencias[i].status_analitico,
        ocorrencias[i].status,
        ocorrencias[i].solicitacao,
        ocorrencias[i].prioridade_analitico,
        ocorrencias[i].prioridade
      );

    sgOcorrencia.Row := 1;
    sgOcorrenciaClick(nil);
  end
  else
   _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
end;

procedure TFormCentralRelacionamentoCliente.sbGravarOcorrenciaClick(
  Sender: TObject);
var
  ocorrenciaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if frDescricaoOcorrencia.EstaVazio then begin
    Exclamar('Para gravar uma ocorr�ncia � necess�rio descrever o problema no campo "Descri��o"');
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia then
    ocorrenciaId := 0
  else begin
    if sgOcorrencia.Cells[coOcorrenciasCodigo, 1] = '' then
      Exit;

    ocorrenciaId := StrToInt(_Biblioteca.RetornaNumeros(sgOcorrencia.Cells[coOcorrenciasCodigo, sgOcorrencia.Row]));
  end;

  vRetBanco := _CRMOcorrencias.AtualizarCRMOcorrencias(
    Sessao.getConexaoBanco,
    ocorrenciaId,
    FrCliente.getCliente(0).cadastro_id,
    frDescricaoOcorrencia.GetTexto,
    frSolucaoOcorrencia.GetTexto,
    'BA'
  );

  if vRetBanco.TeveErro then begin
    Exclamar('Ocorreu um erro ao gravar a ocorr�ncia. Mensagem do erro: ' + vRetBanco.MensagemErro);
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Abort;
  end;

  if ocorrenciaId = 0 then
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + NFormat(vRetBanco.AsInt));

  if FNovaOcorrencia then
    CarregarOcorrencias;

  FNovaOcorrencia := False;
end;

procedure TFormCentralRelacionamentoCliente.sbNovaOcorrenciaClick(Sender: TObject);
begin
  inherited;
  if FrCliente.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente!');
    FrCliente.SetFocus;
    Exit;
  end;

  if FNovaOcorrencia and (not _Biblioteca.Perguntar('A nova ocorr�ncia ainda n�o foi gravada, deseja descartar?') ) then begin
    pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
    frDescricaoOcorrencia.SetFocus;
    Exit;
  end;

  FNovaOcorrencia := True;
  pgDetalhesOcorrencia.ActivePage := tsDescricaoOcorrencia;
  frDescricaoOcorrencia.Clear;
  frSolucaoOcorrencia.Clear;
  frDescricaoOcorrencia.SetFocus;
end;

procedure TFormCentralRelacionamentoCliente.VerificarRegistro(Sender: TObject);
begin
  //
end;

end.
