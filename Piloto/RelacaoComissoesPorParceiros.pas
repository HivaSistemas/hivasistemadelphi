unit RelacaoComissoesPorParceiros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _Sessao, _RecordsOrcamentosVendas,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameFuncionarios, _RelacaoComissoesPorFuncionarios,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, Vcl.Grids, Informacoes.Devolucao,
  GridLuka, FrameDataInicialFinal, _Biblioteca, System.Math,
  Vcl.StdCtrls, CheckBoxLuka, InformacoesAcumulado, Informacoes.TituloReceber,
  frxClass, frxDBSet, Data.DB, Datasnap.DBClient,SelecionarVariosOpcoes,
  FrameProfissionais;

type
  TFormRelacaoComissoesPorParceiros = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    sgComissoes: TGridLuka;
    FrDataRecebimento: TFrDataInicialFinal;
    pcTipoVisao: TPageControl;
    tsAnalitico: TTabSheet;
    tsSintetico: TTabSheet;
    sgComissoesSintetico: TGridLuka;
    FrProfissional: TFrProfissionais;
    frxReportAnalitico: TfrxReport;
    frxReportSintetico: TfrxReport;
    dstComissaoSintetico: TfrxDBDataset;
    dstComissao: TfrxDBDataset;
    cdsComissao: TClientDataSet;
    cdsComissaoIdVendedor: TIntegerField;
    cdsComissaoNomeVendedor: TStringField;
    cdsComissaoTipoComissao: TStringField;
    cdsComissaoOrc_Dev: TStringField;
    cdsComissaoDataRecDev: TDateField;
    cdsComissaoCliente: TStringField;
    cdsComissaoBaseComissao: TFloatField;
    cdsComissaoPerComissao: TFloatField;
    cdsComissaoValorComissao: TFloatField;
    cdsComissaoSintetico: TClientDataSet;
    cdsComissaoSinteticoNomeVendedor: TStringField;
    cdsComissaoSinteticoBaseComissao: TFloatField;
    cdsComissaoSinteticoPerComissao: TFloatField;
    cdsComissaoSinteticoValorComissao: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure sgComissoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgComissoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgComissoesDblClick(Sender: TObject);
    procedure sgComissoesSinteticoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgComissoesSinteticoGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

uses
  Informacoes.Orcamento;

{$R *.dfm}

const
  coTipo          = 0;
  coId            = 1;
  coData          = 2;
  coCliente       = 3;
  coBaseComissao  = 4;
  coTipoComissao  = 5;
  coPercComissao  = 6;
  coValorComissao = 7;

  (* Colunas ocultas *)
  coTipoLinha     = 8;
  coIdVendedor    = 9;
  coNomeVendedor  = 10;

  //Grid Comissoes Sintetico
  csParceiro          = 0;
  csTotalVenda        = 1;
  csTotalBaseComissao = 2;
  csPercComissao      = 3;
  csValorComissao     = 4;

  (* Colunas ocultas *)
  csTipoLinha         = 5;

  ctCabecalho   = 'CAB';
  ctLinhaNorm   = 'LIN';
  ctSubTotal    = 'SUB';
  ctTotalizador = 'TOT';

procedure TFormRelacaoComissoesPorParceiros.Carregar(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vComando: string;
  vParceiroId: Integer;
  vNomeParceiro: string;
  vLinhaSintetico: Integer;
  vComissoes: TArray<RecComissoesPorFuncionarios>;

  vComissao: Double;
  vValorVenda: Double;
  vBaseComissao: Double;

  vTotalComissao: Double;
  vTotalValorVenda: Double;
  vTotalBaseComissao: Double;

  procedure InserirLinhaSintetico;
  begin
    sgComissoesSintetico.Cells[csParceiro, vLinhaSintetico]          := NFormat(vParceiroId) + ' - ' + vNomeParceiro;
    sgComissoesSintetico.Cells[csTotalVenda, vLinhaSintetico]        := NFormatN(vValorVenda);
    sgComissoesSintetico.Cells[csTotalBaseComissao, vLinhaSintetico] := NFormatN(vBaseComissao);

    if NFormatN(vBaseComissao) <> '' then
      sgComissoesSintetico.Cells[csPercComissao, vLinhaSintetico]    := NFormatN(vComissao / vBaseComissao * 100);

    sgComissoesSintetico.Cells[csValorComissao, vLinhaSintetico]     := NFormatN(vComissao);
    sgComissoesSintetico.Cells[csTipoLinha, vLinhaSintetico]         := ctLinhaNorm;

    Inc(vLinhaSintetico);
  end;

  procedure TotalizarFuncionario;
  begin
    sgComissoes.Cells[coCliente, vLinha]       := 'Total: ';
    sgComissoes.Cells[coBaseComissao, vLinha]  := NFormatN(vBaseComissao);

    if NFormatN(vBaseComissao) <> '' then
      sgComissoes.Cells[coPercComissao, vLinha] := NFormatN(vComissao / vBaseComissao * 100);

    sgComissoes.Cells[coValorComissao, vLinha] := NFormatN(vComissao);
    sgComissoes.Cells[coTipoLinha, vLinha]     := ctSubTotal;

    vTotalComissao          := vTotalComissao + vComissao;
    vTotalValorVenda        := vTotalValorVenda + vValorVenda;
    vTotalBaseComissao      := vTotalBaseComissao + vBaseComissao;
    InserirLinhaSintetico;

    vComissao := 0;
    vValorVenda := 0;
    vBaseComissao := 0;

    Inc(vLinha, 2);
  end;

begin
  inherited;
  sgComissoes.ClearGrid;
  sgComissoesSintetico.ClearGrid;

  vComando := ' where COM.EMPRESA_ID ' + FrEmpresas.getSqlFiltros + ' ';

//  //Se for vendedor s� vai ver a comiss�o dele
//  if Sessao.getUsuarioLogado.nome_funcao = 'VENDEDOR' then
//    vComando := vComando + ' and COM.FUNCIONARIO_ID = ' + IntToStr(Sessao.getUsuarioLogado.funcionario_id) + ' '
//  else if not FrProfissional.EstaVazio then
//    vComando := vComando + ' and COM.FUNCIONARIO_ID ' + FrProfissional.getSqlFiltros + ' ';

  if not FrProfissional.EstaVazio then
    vComando := vComando + ' and COM.PARCEIRO_ID ' + FrProfissional.getSqlFiltros + ' ';

  if not FrDataRecebimento.NenhumaDataValida then
    vComando := vComando + ' and ' + FrDataRecebimento.getSqlFiltros('COM.DATA') + ' ';

  vComando :=
    vComando +
      'order by ' +
      '  PARCEIRO_ID, ' +
      '  DATA, ' +
      '  ID ';

  vComissoes := _RelacaoComissoesPorFuncionarios.BuscarComissoesParceiros(Sessao.getConexaoBanco, vComando, Sessao.getParametros.CalcularComissaoAcumuladoRecFin);
  if vComissoes = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vLinha := 1;
  vLinhaSintetico := 1;

  vParceiroId := -1;
  vNomeParceiro := '';

  vComissao := 0;
  vValorVenda := 0;
  vBaseComissao := 0;

  vTotalComissao := 0;
  vTotalValorVenda := 0;
  vTotalBaseComissao := 0;

  for i := Low(vComissoes) to High(vComissoes) do begin
    if vParceiroId <> vComissoes[i].ParceiroId then begin
      if i > 0 then
        TotalizarFuncionario;

      vParceiroId   := vComissoes[i].ParceiroId;
      vNomeParceiro := vComissoes[i].NomeParceiro;

      sgComissoes.Cells[coId, vLinha]        := '  ' + NFormat(vParceiroId) + ' - ' + vNomeParceiro;
      sgComissoes.Cells[coTipoLinha, vLinha] := ctCabecalho;
      Inc(vLinha);
    end;


    sgComissoes.Cells[coTipo, vLinha]          := vComissoes[i].Tipo;
    sgComissoes.Cells[coId, vLinha]            := NFormat(vComissoes[i].Id);
    sgComissoes.Cells[coData, vLinha]          := DFormat(vComissoes[i].Data);
    sgComissoes.Cells[coCliente, vLinha]       := NFormat(vComissoes[i].CadastroId) + ' - ' + vComissoes[i].NomeCliente;
    sgComissoes.Cells[coBaseComissao, vLinha]  := NFormatN(vComissoes[i].BaseComissao);
    sgComissoes.Cells[coTipoComissao, vLinha]  := vComissoes[i].TipoComissao;
    sgComissoes.Cells[coPercComissao, vLinha]  := NFormatN(vComissoes[i].PercentualComissao);
    sgComissoes.Cells[coValorComissao, vLinha] := NFormatN(vComissoes[i].ValorComissao);
    sgComissoes.Cells[coTipoLinha, vLinha]     := ctLinhaNorm;
    sgComissoes.Cells[coIdVendedor, vLinha]    := IntToStr(vParceiroId);
    sgComissoes.Cells[coNomeVendedor, vLinha]  := vNomeParceiro;

    if Em(vComissoes[i].Tipo, ['ORC', 'ACU', 'FIN']) then begin
      vComissao          := vComissao + vComissoes[i].ValorComissao;
      vValorVenda        := vValorVenda + vComissoes[i].ValorVenda;
      vBaseComissao      := vBaseComissao + vComissoes[i].BaseComissao;
    end
    else begin
      vComissao          := vComissao - vComissoes[i].ValorComissao;
      vBaseComissao      := vBaseComissao - vComissoes[i].BaseComissao;
    end;

    Inc(vLinha);
  end;
  TotalizarFuncionario;

  sgComissoes.Cells[coCliente, vLinha]       := 'Total Geral: ';
  sgComissoes.Cells[coBaseComissao, vLinha]  := NFormatN(vTotalBaseComissao);
  sgComissoes.Cells[coPercComissao, vLinha]  := NFormatN(vTotalComissao / vTotalBaseComissao * 100);
  sgComissoes.Cells[coValorComissao, vLinha] := NFormatN(vTotalComissao);
  sgComissoes.Cells[coTipoLinha, vLinha]     := ctTotalizador;
  Inc(vLinha);

  sgComissoesSintetico.Cells[csParceiro, vLinhaSintetico]          := 'Total Geral: ';
  sgComissoesSintetico.Cells[csTotalVenda, vLinhaSintetico]        := NFormatN(vTotalValorVenda);
  sgComissoesSintetico.Cells[csTotalBaseComissao, vLinhaSintetico] := NFormatN(vTotalBaseComissao);
  sgComissoesSintetico.Cells[csPercComissao, vLinhaSintetico]      := NFormatN(vTotalComissao / vTotalBaseComissao * 100);
  sgComissoesSintetico.Cells[csValorComissao, vLinhaSintetico]     := NFormatN(vTotalComissao);
  sgComissoesSintetico.Cells[csTipoLinha, vLinhaSintetico]         := ctTotalizador;
  Inc(vLinhaSintetico);

  sgComissoes.RowCount := vLinha;
  sgComissoesSintetico.RowCount := vLinhaSintetico;
  pcDados.ActivePage := tsResultado;
end;

procedure TFormRelacaoComissoesPorParceiros.FormCreate(Sender: TObject);
begin
  inherited;
  ActiveControl          := FrEmpresas.sgPesquisa;
  pcTipoVisao.ActivePage := tsAnalitico;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
end;

procedure TFormRelacaoComissoesPorParceiros.Imprimir(Sender: TObject);
var
  i: Integer;
  vOpcao: TRetornoTelaFinalizar<Integer>;
begin
  inherited;
  vOpcao := SelecionarVariosOpcoes.Selecionar('Tipo relat�rio', ['Anal�tico','Sint�tico'], 0);
  if vOpcao.Dados = 0 then
  begin
    cdsComissao.Close;
    cdsComissao.CreateDataSet;
    cdsComissao.Open;
    for i := sgComissoes.RowCount - 2 downto 1 do
    begin
      if sgComissoes.Cells[coIdVendedor, i] = '' then
        continue;
      cdsComissao.Insert;
      cdsComissaoIdVendedor.AsString    := sgComissoes.Cells[coIdVendedor, i];
      cdsComissaoNomeVendedor.AsString  := sgComissoes.Cells[coNomeVendedor, i];
      cdsComissaoTipoComissao.AsString  := sgComissoes.Cells[coTipo, i];
      cdsComissaoOrc_Dev.AsString       := sgComissoes.Cells[coId, i];
      cdsComissaoDataRecDev.AsString    := sgComissoes.Cells[coData, i];
      cdsComissaoCliente.AsString       := sgComissoes.Cells[coCliente, i];
      cdsComissaoBaseComissao.AsFloat   := SFormatDouble(sgComissoes.Cells[coBaseComissao, i]) * Iif(Em(sgComissoes.Cells[coTipo, i], ['ORC', 'ACU', 'FIN']),1,-1);
      cdsComissaoPerComissao.AsFloat    := SFormatDouble(sgComissoes.Cells[coPercComissao, i]) * Iif(Em(sgComissoes.Cells[coTipo, i], ['ORC', 'ACU', 'FIN']),1,-1);
      cdsComissaoValorComissao.AsFloat  := SFormatDouble(sgComissoes.Cells[coValorComissao, i])* Iif(Em(sgComissoes.Cells[coTipo, i], ['ORC', 'ACU', 'FIN']),1,-1);
      cdsComissao.Post;
    end;


    frxReportAnalitico.ShowReport
  end else
  begin
    cdsComissaoSintetico.Close;
    cdsComissaoSintetico.CreateDataSet;
    cdsComissaoSintetico.Open;
    for i := sgComissoesSintetico.RowCount - 2 downto 1 do
    begin
      cdsComissaoSintetico.Insert;
      cdsComissaoSinteticoNomeVendedor.AsString  := sgComissoesSintetico.Cells[csParceiro,i];
      cdsComissaoSinteticoBaseComissao.AsFloat   := SFormatDouble(sgComissoesSintetico.Cells[csTotalBaseComissao,i]);
      cdsComissaoSinteticoPerComissao.AsFloat    := SFormatDouble(sgComissoesSintetico.Cells[csPercComissao,i]);
      cdsComissaoSinteticoValorComissao.AsFloat  := SFormatDouble(sgComissoesSintetico.Cells[csValorComissao,i]);
      cdsComissaoSintetico.Post;
    end;
    frxReportSintetico.ShowReport;
  end;
end;

procedure TFormRelacaoComissoesPorParceiros.sgComissoesDblClick(Sender: TObject);
begin
  inherited;
  if sgComissoes.Cells[coTipo, sgComissoes.Row] = 'ORC' then
    Informacoes.Orcamento.Informar(SFormatInt(sgComissoes.Cells[coId, sgComissoes.Row]))
  else if sgComissoes.Cells[coTipo, sgComissoes.Row] = 'DEV' then
    Informacoes.Devolucao.Informar(SFormatInt(sgComissoes.Cells[coId, sgComissoes.Row]))
  else if sgComissoes.Cells[coTipo, sgComissoes.Row] = 'ACU' then
    InformacoesAcumulado.Informar(SFormatInt(sgComissoes.Cells[coId, sgComissoes.Row]))
  else if sgComissoes.Cells[coTipo, sgComissoes.Row] = 'FIN' then
    Informacoes.TituloReceber.Informar(SFormatInt(sgComissoes.Cells[coId, sgComissoes.Row]));
end;

procedure TFormRelacaoComissoesPorParceiros.sgComissoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coId, coBaseComissao, coPercComissao, coValorComissao] then
    vAlinhamento := taRightJustify
  else if ACol in[coTipo, coTipoComissao] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  if (ARow > 0) and (sgComissoes.Cells[coTipoLinha, ARow] = ctCabecalho) then
    sgComissoes.MergeCells([ARow, ACol], [ARow, coId], [ARow, coCliente], vAlinhamento, Rect)
  else
    sgComissoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoComissoesPorParceiros.sgComissoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgComissoes.Cells[coTipoLinha, ARow] = ctCabecalho then begin
    AFont.Style  := [fsBold];
    AFont.Color  := clWhite;
    ABrush.Color := $000096DB;
  end
  else if sgComissoes.Cells[coTipoLinha, ARow] = ctSubTotal then begin
    AFont.Style  := [fsBold];
    AFont.Color  := clBlack;
    ABrush.Color := clGreen;
  end
  else if sgComissoes.Cells[coTipoLinha, ARow] = ctTotalizador then begin
    AFont.Style  := [fsBold];
    AFont.Color  := clWhite;
    ABrush.Color := $006C6C6C;
  end
  else if ACol = coTipo then begin
    AFont.Style := [fsBold];
    if sgComissoes.Cells[coTipo, ARow] = 'DEV' then
      AFont.Color := clRed
    else if sgComissoes.Cells[coTipo, ARow] = 'ORC' then
      AFont.Color := clBlue
    else if sgComissoes.Cells[coTipo, ARow] = 'ACU' then
      AFont.Color := $000096DB
    else if sgComissoes.Cells[coTipo, ARow] = 'FIN' then
      AFont.Color := $006C6C6C;
  end
  else if ACol = coTipoComissao then begin
    AFont.Style := [fsBold];
    if sgComissoes.Cells[coTipoComissao, ARow] = 'A vista' then
      AFont.Color := clBlue
    else if sgComissoes.Cells[coTipoComissao, ARow] = 'A prazo' then
      AFont.Color := $000096DB;
  end;
end;

procedure TFormRelacaoComissoesPorParceiros.sgComissoesSinteticoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [csTotalVenda, csTotalBaseComissao, csPercComissao, csValorComissao] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgComissoesSintetico.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoComissoesPorParceiros.sgComissoesSinteticoGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgComissoesSintetico.Cells[csTipoLinha, ARow] = ctTotalizador then begin
    AFont.Style  := [fsBold];
    AFont.Color  := clWhite;
    ABrush.Color := $000096DB;
  end
  else if ACol = csTotalVenda then
    AFont.Color := clBlue
  else if ACol = csTotalBaseComissao then
    AFont.Color := $000096DB
  else if ACol in [csPercComissao, csValorComissao] then
    AFont.Color := coCorFonteEdicao3;
end;

procedure TFormRelacaoComissoesPorParceiros.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrEmpresas.EstaVazio then begin
    Exclamar('Ao menos uma empresa deve ser informada!');
    SetarFoco(FrEmpresas);
    Abort;
  end;
end;

end.

