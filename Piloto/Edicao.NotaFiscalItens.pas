unit Edicao.NotaFiscalItens;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, _Biblioteca,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _RecordsNotasFiscais,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, Vcl.Grids, GridLuka, _Sessao, System.Math, System.StrUtils,
  _NotasFiscaisItens, _RecordsEspeciais;

type
  TFormEdicaoNotaFiscalItens = class(TFormHerancaFinalizar)
    sgProdutos: TGridLuka;
    pn1: TPanel;
    stTit_Total_Notas: TStaticText;
    st2: TStaticText;
    st6: TStaticText;
    st7: TStaticText;
    st8: TStaticText;
    st9: TStaticText;
    st11: TStaticText;
    st12: TStaticText;
    stTotalBaseCalculoICMS: TStaticText;
    stTotalValorICMS: TStaticText;
    stTotalValorIPI: TStaticText;
    stTotalBaseCalculoICMSST: TStaticText;
    stTotalValorICMSST: TStaticText;
    stTotalOutrasDespesas: TStaticText;
    stTotalDesconto: TStaticText;
    stTotalProdutos: TStaticText;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgProdutosArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
  private
    FNotaFiscalId: Integer;
    FItens: TArray<RecNotaFiscalItem>;

    procedure AtualizarTotais;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Editar(const pNotaFiscalId: Integer): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

const
  coProdutoId         = 0;
  coNome              = 1;
  coPrecoUnitario     = 2;
  coQuantidade        = 3;
  coUnidade           = 4;
  coValorTotal        = 5;
  coNCM               = 6;
  coCEST              = 7;
  coValorDesconto     = 8;
  coValorOutDespesas  = 9;
  coBaseCalculoICMS   = 10;
  coPercentualICMS    = 11;
  coValorICMS         = 12;
  coBaseCalculoICMSST = 13;
  coPercentualICMSST  = 14;
  coValorICMSST       = 15;
  coCSTPIS            = 16;
  coBaseCalculoPIS    = 17;
  coPercentualPIS     = 18;
  coValorPIS          = 19;
  coCSTCOFINS         = 20;
  coBaseCalculoCOFINS = 21;
  coPercentualCOFINS  = 22;
  coValorCOFINS       = 23;
  coValorIPI          = 24;
  coPercentualIPI     = 25;
  coCodigoBarras      = 26;
  coCFOP              = 27;

function Editar(const pNotaFiscalId: Integer): TRetornoTelaFinalizar;
var
  i: Integer;
  vItens: TArray<RecNotaFiscalItem>;
  vForm: TFormEdicaoNotaFiscalItens;
begin
  if pNotaFiscalId = 0 then
    Exit;

  vItens := _NotasFiscaisItens.BuscarNotasFiscaisItens(Sessao.getConexaoBanco, 0, [pNotaFiscalId]);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vForm := TFormEdicaoNotaFiscalItens.Create(Application);
  vForm.FNotaFiscalId := pNotaFiscalId;
  vForm.FItens := vItens;

  for i := Low(vItens) to High(vItens) do begin
    vForm.sgProdutos.Cells[coProdutoId, i + 1]         := NFormat(vItens[i].produto_id);
    vForm.sgProdutos.Cells[coNome, i + 1]              := vItens[i].nome_produto;
    vForm.sgProdutos.Cells[coPrecoUnitario, i + 1]     := NFormat(vItens[i].preco_unitario);
    vForm.sgProdutos.Cells[coQuantidade, i + 1]        := NFormatEstoque(vItens[i].quantidade);
    vForm.sgProdutos.Cells[coUnidade, i + 1]           := vItens[i].unidade;
    vForm.sgProdutos.Cells[coValorTotal, i + 1]        := NFormat(vItens[i].valor_total);
    vForm.sgProdutos.Cells[coNCM, i + 1]               := vItens[i].codigo_ncm;
    vForm.sgProdutos.Cells[coCEST, i + 1]              := vItens[i].cest;
    vForm.sgProdutos.Cells[coValorDesconto, i + 1]     := NFormatN(vItens[i].valor_total_desconto);
    vForm.sgProdutos.Cells[coValorOutDespesas, i + 1]  := NFormatN(vItens[i].valor_total_outras_despesas);
    vForm.sgProdutos.Cells[coBaseCalculoICMS, i + 1]   := NFormatN(vItens[i].base_calculo_icms);
    vForm.sgProdutos.Cells[coPercentualICMS, i + 1]    := NFormatN(vItens[i].percentual_icms);
    vForm.sgProdutos.Cells[coValorICMS, i + 1]         := NFormatN(vItens[i].valor_icms);
    vForm.sgProdutos.Cells[coBaseCalculoICMSST, i + 1] := NFormatN(vItens[i].base_calculo_icms_st);
    vForm.sgProdutos.Cells[coPercentualICMSST, i + 1]  := NFormatN(vItens[i].percentual_icms_st);
    vForm.sgProdutos.Cells[coValorICMSST, i + 1]       := NFormatN(vItens[i].valor_icms_st);
    vForm.sgProdutos.Cells[coCSTPIS, i + 1]            := vItens[i].cst_pis;
    vForm.sgProdutos.Cells[coBaseCalculoPIS, i + 1]    := NFormatN(vItens[i].base_calculo_pis);
    vForm.sgProdutos.Cells[coPercentualPIS, i + 1]     := NFormatN(vItens[i].percentual_pis);
    vForm.sgProdutos.Cells[coValorPIS, i + 1]          := NFormatN(vItens[i].valor_pis);
    vForm.sgProdutos.Cells[coCSTCOFINS, i + 1]         := vItens[i].cst_cofins;
    vForm.sgProdutos.Cells[coBaseCalculoCOFINS, i + 1] := NFormatN(vItens[i].base_calculo_cofins);
    vForm.sgProdutos.Cells[coPercentualCOFINS, i + 1]  := NFormatN(vItens[i].percentual_cofins);
    vForm.sgProdutos.Cells[coValorCOFINS, i + 1]       := NFormatN(vItens[i].valor_cofins);
    vForm.sgProdutos.Cells[coValorIPI, i + 1]          := NFormatN(vItens[i].valor_ipi);
    vForm.sgProdutos.Cells[coPercentualIPI, i + 1]     := NFormatN(vItens[i].percentual_ipi);
    vForm.sgProdutos.Cells[coCodigoBarras, i + 1]      := vItens[i].codigo_barras;
    vForm.sgProdutos.Cells[coCFOP, i + 1]              := vItens[i].CfopId;
  end;
  vForm.sgProdutos.RowCount := IfThen(Length(vItens) > 1, High(vItens) + 2, 2);
  vForm.AtualizarTotais;

  Result.Ok(vForm.ShowModal);

  vForm.Free;
end;

procedure TFormEdicaoNotaFiscalItens.AtualizarTotais;
var
  i: Integer;
  vTotalBaseICMS: Currency;
  vTotalICMS: Currency;
  vTotalBaseICMSST: Currency;
  vTotalICMSST: Currency;
  vTotalOutrasDesp: Currency;
  vTotalDesconto: Currency;
  vTotalIPI: Currency;
  vTotalProdutos: Currency;
begin
  vTotalBaseICMS   := 0;
  vTotalICMS       := 0;
  vTotalBaseICMSST := 0;
  vTotalICMSST     := 0;
  vTotalOutrasDesp := 0;
  vTotalDesconto   := 0;
  vTotalIPI        := 0;
  vTotalProdutos   := 0;

  for i := 1 to sgProdutos.RowCount - 1 do begin
    vTotalBaseICMS   := vTotalBaseICMS + SFormatDouble(sgProdutos.Cells[coBaseCalculoICMS, i]);
    vTotalICMS       := vTotalICMS + SFormatDouble(sgProdutos.Cells[coValorICMS, i]);
    vTotalBaseICMSST := vTotalBaseICMSST + SFormatDouble(sgProdutos.Cells[coBaseCalculoICMSST, i]);
    vTotalICMSST     := vTotalICMSST + SFormatDouble(sgProdutos.Cells[coValorICMSST, i]);
    vTotalOutrasDesp := vTotalOutrasDesp + SFormatDouble(sgProdutos.Cells[coValorOutDespesas, i]);
    vTotalDesconto   := vTotalDesconto + SFormatDouble(sgProdutos.Cells[coValorDesconto, i]);
    vTotalIPI        := vTotalIPI + SFormatDouble(sgProdutos.Cells[coValorIPI, i]);
    vTotalProdutos   := vTotalProdutos + SFormatDouble(sgProdutos.Cells[coValorTotal, i]);
  end;

  stTotalBaseCalculoICMS.Caption   := NFormatN(vTotalBaseICMS) + ' ';
  stTotalValorICMS.Caption         := NFormatN(vTotalICMS) + ' ';
  stTotalBaseCalculoICMSST.Caption := NFormatN(vTotalBaseICMSST) + ' ';
  stTotalValorICMSST.Caption       := NFormatN(vTotalICMSST) + ' ';
  stTotalOutrasDespesas.Caption    := NFormatN(vTotalOutrasDesp) + ' ';
  stTotalDesconto.Caption          := NFormatN(vTotalDesconto) + ' ';
  stTotalValorIPI.Caption          := NFormatN(vTotalIPI) + ' ';
  stTotalProdutos.Caption          := NFormatN(vTotalProdutos) + ' ';
end;

procedure TFormEdicaoNotaFiscalItens.Finalizar(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
begin
  for i := 1 to sgProdutos.RowCount -1 do begin
    FItens[i - 1].preco_unitario              := SFormatDouble(sgProdutos.Cells[coPrecoUnitario, i]);
    FItens[i - 1].quantidade                  := SFormatDouble(sgProdutos.Cells[coQuantidade, i]);
    FItens[i - 1].valor_total                 := SFormatDouble(sgProdutos.Cells[coValorTotal, i]);
    FItens[i - 1].codigo_ncm                  := sgProdutos.Cells[coNCM, i];
    FItens[i - 1].cest                        := sgProdutos.Cells[coCEST, i];
    FItens[i - 1].valor_total_desconto        := SFormatDouble(sgProdutos.Cells[coValorDesconto, i]);
    FItens[i - 1].valor_total_outras_despesas := SFormatDouble(sgProdutos.Cells[coValorOutDespesas, i]);
    FItens[i - 1].base_calculo_icms           := SFormatDouble(sgProdutos.Cells[coBaseCalculoICMS, i]);
    FItens[i - 1].percentual_icms             := SFormatDouble(sgProdutos.Cells[coPercentualICMS, i]);
    FItens[i - 1].valor_icms                  := SFormatDouble(sgProdutos.Cells[coValorICMS, i]);
    FItens[i - 1].base_calculo_icms_st        := SFormatDouble(sgProdutos.Cells[coBaseCalculoICMSST, i]);
    FItens[i - 1].percentual_icms_st          := SFormatDouble(sgProdutos.Cells[coPercentualICMSST, i]);
    FItens[i - 1].valor_icms_st               := SFormatDouble(sgProdutos.Cells[coValorICMSST, i]);
    FItens[i - 1].cst_pis                     := sgProdutos.Cells[coCSTPIS, i];
    FItens[i - 1].base_calculo_pis            := SFormatDouble(sgProdutos.Cells[coBaseCalculoPIS, i]);
    FItens[i - 1].percentual_pis              := SFormatDouble(sgProdutos.Cells[coPercentualPIS, i]);
    FItens[i - 1].valor_pis                   := SFormatDouble(sgProdutos.Cells[coValorPIS, i]);
    FItens[i - 1].cst_cofins                  := sgProdutos.Cells[coCSTCOFINS, i];
    FItens[i - 1].base_calculo_cofins         := SFormatDouble(sgProdutos.Cells[coBaseCalculoCOFINS, i]);
    FItens[i - 1].percentual_cofins           := SFormatDouble(sgProdutos.Cells[coPercentualCOFINS, i]);
    FItens[i - 1].valor_cofins                := SFormatDouble(sgProdutos.Cells[coValorCOFINS, i]);
    FItens[i - 1].valor_ipi                   := SFormatDouble(sgProdutos.Cells[coValorIPI, i]);
    FItens[i - 1].percentual_ipi              := SFormatDouble(sgProdutos.Cells[coPercentualIPI, i]);
    FItens[i - 1].codigo_barras               := sgProdutos.Cells[coCodigoBarras, i];
    FItens[i - 1].CfopId                      := sgProdutos.Cells[coCFOP, i];
  end;


  vRetBanco := _NotasFiscaisItens.AtualizarItensEdicaoNota(Sessao.getConexaoBanco, FNotaFiscalId, FItens);
  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormEdicaoNotaFiscalItens.sgProdutosArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ACol in[
    coPrecoUnitario,
    coValorTotal,
    coValorDesconto,
    coValorOutDespesas,
    coBaseCalculoICMS,
    coPercentualICMS,
    coValorICMS,
    coBaseCalculoICMSST,
    coPercentualICMSST,
    coValorICMSST,
    coBaseCalculoPIS,
    coPercentualPIS,
    coValorPIS,
    coBaseCalculoCOFINS,
    coPercentualCOFINS,
    coValorCOFINS,
    coValorIPI,
    coPercentualIPI]
  then begin
    if SFormatCurr(sgProdutos.Cells[ACol, ARow]) < 0 then begin
      TextCell := '';
      sgProdutos.Cells[ACol, ARow] := '';
    end
    else begin
      TextCell := NFormatN(SFormatCurr(sgProdutos.Cells[ACol, ARow]));
      sgProdutos.Cells[ACol, ARow] := NFormatN(SFormatCurr(sgProdutos.Cells[ACol, ARow]));;
    end;
  end
  else if ACol = coQuantidade then begin
    if SFormatCurr(sgProdutos.Cells[ACol, ARow]) < 0 then begin
      TextCell := '';
      sgProdutos.Cells[ACol, ARow] := '';
    end
    else begin
      TextCell := NFormatNEstoque(SFormatCurr(sgProdutos.Cells[ACol, ARow]));
      sgProdutos.Cells[ACol, ARow] := NFormatNEstoque(SFormatCurr(sgProdutos.Cells[ACol, ARow]));;
    end;
  end;

  AtualizarTotais;
end;

procedure TFormEdicaoNotaFiscalItens.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coNome, coUnidade, coNCM, coCEST, coCodigoBarras, coCFOP] then
    vAlinhamento := taLeftJustify
  else if ACol in[ coCSTPIS, coCSTCOFINS] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taRightJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormEdicaoNotaFiscalItens.sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  sgProdutos.Options := sgProdutos.Options - [goEditing];
  if ACol in[
    coPrecoUnitario,
    coQuantidade,
    coValorTotal,
    coNCM,
    coCEST,
    coValorDesconto,
    coValorOutDespesas,
    coBaseCalculoICMS,
    coPercentualICMS,
    coValorICMS,
    coBaseCalculoICMSST,
    coPercentualICMSST,
    coValorICMSST,
    coCSTPIS,
    coBaseCalculoPIS,
    coPercentualPIS,
    coValorPIS,
    coCSTCOFINS,
    coBaseCalculoCOFINS,
    coPercentualCOFINS,
    coValorCOFINS,
    coValorIPI,
    coPercentualIPI,
    coCodigoBarras,
    coCFOP]
  then
    sgProdutos.Options := sgProdutos.Options + [goEditing];
end;

procedure TFormEdicaoNotaFiscalItens.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
