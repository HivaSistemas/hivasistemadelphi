inherited FormInformacoesCreditosUtilizados: TFormInformacoesCreditosUtilizados
  Caption = 'Informa'#231#245'es de cr'#233'ditos utilizados'
  ClientHeight = 256
  ClientWidth = 471
  ExplicitWidth = 477
  ExplicitHeight = 285
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 219
    Width = 471
    ExplicitTop = 219
    ExplicitWidth = 471
  end
  object sgCreditos: TGridLuka
    Left = 2
    Top = 3
    Width = 467
    Height = 214
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alCustom
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 1
    OnDblClick = sgCreditosDblClick
    OnDrawCell = sgCreditosDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'C'#243'digo'
      'Tipo'
      'Data cadastro'
      'Documento'
      'Valor')
    Grid3D = False
    RealColCount = 8
    AtivarPopUpSelecao = False
    ColWidths = (
      46
      79
      92
      84
      101)
  end
end
