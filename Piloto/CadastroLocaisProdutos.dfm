inherited FormCadastroLocaisProdutos: TFormCadastroLocaisProdutos
  Caption = 'Cadastro de locais de produtos'
  ClientHeight = 228
  ClientWidth = 486
  ExplicitWidth = 492
  ExplicitHeight = 257
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 126
    Top = 87
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  object lb2: TLabel [1]
    Left = 246
    Top = 36
    Width = 24
    Height = 14
    Caption = 'Tipo'
  end
  inherited Label1: TLabel
    Top = 37
    ExplicitTop = 37
  end
  inherited pnOpcoes: TPanel
    Height = 228
    ExplicitHeight = 206
  end
  inherited eID: TEditLuka
    Top = 51
    TabOrder = 4
    ExplicitTop = 51
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 126
    Top = 8
    TabOrder = 5
    ExplicitLeft = 126
    ExplicitTop = 8
  end
  object eNome: TEditLuka
    Left = 126
    Top = 101
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object cbTipoLocal: TComboBoxLuka
    Left = 246
    Top = 51
    Width = 104
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = 16645629
    TabOrder = 2
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Dep'#243'sito'
      'Loja'
      'Exposi'#231#227'o')
    Valores.Strings = (
      'D'
      'L'
      'E')
    AsInt = 0
  end
  object ckPermiteDevolucaoVenda: TCheckBox
    Left = 126
    Top = 202
    Width = 183
    Height = 17
    Caption = 'Permite devolu'#231#227'o de vendas'
    TabOrder = 3
    OnKeyDown = ProximoCampo
  end
  inline FrEmpresaDirecionarNota: TFrEmpresas
    Left = 126
    Top = 138
    Width = 236
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 138
    ExplicitWidth = 236
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 211
      Height = 23
      ExplicitWidth = 211
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 236
      ExplicitWidth = 236
      inherited lbNomePesquisa: TLabel
        Width = 162
        Caption = 'Empresa para direcionar nota'
        ExplicitWidth = 162
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 131
        ExplicitLeft = 131
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 211
      Height = 24
      ExplicitLeft = 211
      ExplicitHeight = 24
      inherited sbPesquisa: TSpeedButton
        Visible = False
      end
    end
  end
end
