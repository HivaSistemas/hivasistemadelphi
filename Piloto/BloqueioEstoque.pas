unit BloqueioEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, FrameEmpresas, _Biblioteca, _Sessao,
  Vcl.StdCtrls, StaticTextLuka, Vcl.Grids, GridLuka, Frame.Lotes, FrameProdutos, _BloqueiosEstoques,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameLocais, EditLuka, _EstoquesDivisao,
  Vcl.Buttons, Vcl.ExtCtrls, _RecordsEspeciais, _BloqueiosEstoquesItens,
  _HerancaCadastroCodigo, CheckBoxLuka, Frame.TipoBloqueioEstoque;

type
  TFormBloqueioEstoque = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    lb7: TLabel;
    FrLocal: TFrLocais;
    eObservacao: TEditLuka;
    FrProduto: TFrProdutos;
    eQuantidade: TEditLuka;
    FrLote: TFrameLotes;
    sgItens: TGridLuka;
    st1: TStaticTextLuka;
    stEstoqueFisico: TStaticText;
    stEstoqueDisponivel: TStaticText;
    st2: TStaticTextLuka;
    FrEmpresa: TFrEmpresas;
    FrTipoBloqueioEstoque: TFrTipoBloqueioEstoque;
    procedure FormCreate(Sender: TObject);
    procedure eQuantidadeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    procedure PreencherRegistro(pBloqueio: RecBloqueiosEstoques);

    procedure FrProdutoLocalOnAposPesquisar(Sender: TObject);
    procedure FrProdutoLocalOnAposDeletar(Sender: TObject);
    procedure FrEmpresaOnAposPesquisar(Sender: TObject);

    procedure AdicionarNoProdutoGrid(
      pProdutoId: Integer;
      pNomeProduto: string;
      pMarca: string;
      pUnidade: string;
      pLote: string;
      pQuantidade: Double;
      pLocalId: Integer;
      pNomeLocal: string;
      pDataFabricacao: TDate;
      pDataVencimento: TDate;
      pEstoqueFisico: Double;
      pEstoqueDisponivel: Double
    );
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormBloqueioEstoque }

const
  coProdutoId         = 0;
  coNomeProduto       = 1;
  coMarca             = 2;
  coUnidade           = 3;
  coLote              = 4;
  coQuantidade        = 5;
  coLocalId           = 6;
  coNomeLocal         = 7;
  (* Ocultas *)
  coDataFabricacao    = 8;
  coDataVencimento    = 9;
  coEstoqueFisico     = 10;
  coEstoqueDisponivel = 11;


procedure TFormBloqueioEstoque.AdicionarNoProdutoGrid(
  pProdutoId: Integer;
  pNomeProduto: string;
  pMarca: string;
  pUnidade: string;
  pLote: string;
  pQuantidade: Double;
  pLocalId: Integer;
  pNomeLocal: string;
  pDataFabricacao: TDate;
  pDataVencimento: TDate;
  pEstoqueFisico: Double;
  pEstoqueDisponivel: Double
);
var
  vLinha: Integer;
begin
  vLinha := sgItens.Localizar([coProdutoId, coLocalId, coLote], [NFormat(pProdutoId), NFormat(pLocalId), pLote]);
  if vLinha > -1 then begin
    Exclamar('Este produto j� foi adicionado no grid!');
    sgItens.Row := vLinha;
    sgItens.SetFocus;
    Abort;
  end;

  if sgItens.Cells[coProdutoId, 1] = '' then
    vLinha := 1
  else begin
    vLinha := sgItens.RowCount;
    sgItens.RowCount := sgItens.RowCount + 1;
  end;

  sgItens.Cells[coProdutoId, vLinha]         := NFormat(pProdutoId);
  sgItens.Cells[coNomeProduto, vLinha]       := pNomeProduto;
  sgItens.Cells[coMarca, vLinha]             := pMarca;
  sgItens.Cells[coUnidade, vLinha]           := pUnidade;
  sgItens.Cells[coLocalId, vLinha]           := NFormat(pLocalId);
  sgItens.Cells[coNomeLocal, vLinha]         := pNomeLocal;
  sgItens.Cells[coQuantidade, vLinha]        := NFormatNEstoque(pQuantidade);
  sgItens.Cells[coLote, vLinha]              := pLote;
  sgItens.Cells[coDataFabricacao, vLinha]    := DFormatN(pDataFabricacao);
  sgItens.Cells[coDataVencimento, vLinha]    := DFormatN(pDataVencimento);
  sgItens.Cells[coEstoqueFisico, vLinha]     := NFormatNEstoque(pEstoqueFisico);
  sgItens.Cells[coEstoqueDisponivel, vLinha] := NFormatNEstoque(pEstoqueDisponivel);

  sgItens.Row := vLinha;
end;

procedure TFormBloqueioEstoque.BuscarRegistro;
var
  vDados: TArray<RecBloqueiosEstoques>;
begin
  vDados := _BloqueiosEstoques.BuscarBloqueiosEstoques(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vDados = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(eID);
    Exit;
  end;

  if vDados[0].Status = 'B' then begin
    _Biblioteca.Exclamar('N�o � permitido editar um bloqueio de estoque j� baixado!');
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vDados[0]);
end;

procedure TFormBloqueioEstoque.eQuantidadeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if FrProduto.EstaVazio then begin
    Exclamar('O produto n�o foi informado, verifique!');
    FrProduto.SetFocus;
    Exit;
  end;

  if FrLocal.EstaVazio then begin
    Exclamar('O local de sa�da do produto n�o foi definido!');
    SetarFoco(FrLocal);
    Exit;
  end;

  if FrTipoBloqueioEstoque.EstaVazio then begin
    Exclamar('O tipo de bloqueio do estoque n�o foi definido!');
    SetarFoco(FrTipoBloqueioEstoque);
    Exit;
  end;

  if not _Biblioteca.ValidarMultiplo(eQuantidade.AsDouble, FrProduto.getProduto().multiplo_venda) then begin
    SetarFoco(eQuantidade);
    Exit;
  end;

  if eQuantidade.AsCurr = 0 then begin
    Exclamar('A quantidade n�o foi inforamada corretamente, verifique!');
    SetarFoco(eQuantidade);
    Exit;
  end;

  if not FrLote.LoteValido then begin
    Exclamar('O lote informado n�o � v�lido, verifique!');
    SetarFoco(FrLote);
    Exit;
  end;

  if (FrProduto.getProduto.ExigirDataFabricacaoLote = 'S') or (FrProduto.getProduto.ExigirDataVencimentoLote = 'S') then begin


  end;

  AdicionarNoProdutoGrid(
    FrProduto.getProduto().produto_id,
    FrProduto.getProduto().nome,
    FrProduto.getProduto().nome_marca,
    FrProduto.getProduto().unidade_venda,
    FrLote.Lote,
    eQuantidade.AsDouble,
    FrLocal.GetLocais().local_id,
    FrLocal.GetLocais().nome,
    FrLote.DataFabricacao,
    FrLote.DataVencimento,
    0, // Estoque atual
    0 // Estoque disponivel
  );

  _Biblioteca.LimparCampos([FrProduto, FrLocal, eQuantidade]);
  SetarFoco(FrProduto);
end;

procedure TFormBloqueioEstoque.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _BloqueiosEstoques.ExcluirBloqueiosEstoques(Sessao.getConexaoBanco, eID.AsInt);
  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RegistroExcluidoSucesso;
  inherited;
end;

procedure TFormBloqueioEstoque.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresa.OnAposPesquisar := FrEmpresaOnAposPesquisar;
  FrProduto.OnAposPesquisar := FrProdutoLocalOnAposPesquisar;
  FrProduto.OnAposDeletar   := FrProdutoLocalOnAposDeletar;

  FrLocal.OnAposPesquisar   := FrProdutoLocalOnAposPesquisar;
  FrLocal.OnAposDeletar     := FrProdutoLocalOnAposDeletar;
end;

procedure TFormBloqueioEstoque.FrEmpresaOnAposPesquisar(Sender: TObject);
begin
  _Biblioteca.Habilitar([FrEmpresa], False, False);
  SetarFoco(eObservacao);
end;

procedure TFormBloqueioEstoque.FrProdutoLocalOnAposDeletar(Sender: TObject);
begin
  FrLote.Clear;
end;

procedure TFormBloqueioEstoque.FrProdutoLocalOnAposPesquisar(Sender: TObject);
var
  vEstoques: TArray<RecEstoquesDivisao>;
begin
  if FrProduto.EstaVazio then
    Exit;

  if FrProduto.Focused then begin
    if Em(FrProduto.getProduto.TipoControleEstoque, ['K', 'A']) then begin
      _Biblioteca.Informar('Produtos que s�o cotrolados como "Kit" n�o pode ter seu estoque bloqueado, bloqueie os produtos que comp�e o kit.');
      SetarFoco(FrProduto);
      FrProduto.Clear;
      Exit;
    end;
  end;

  if FrLocal.EstaVazio then
    Exit;

  vEstoques := _EstoquesDivisao.BuscarEstoque(Sessao.getConexaoBanco, 1, [FrEmpresa.getEmpresa.EmpresaId, FrLocal.GetLocais.local_id, FrProduto.getProduto.produto_id]);
  if vEstoques = nil then begin
    _Biblioteca.Exclamar('N�o foi encontrado estoque para o produto selecionado no local ' + FrLocal.GetLocais().nome + '!');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  if _Biblioteca.Arredondar(vEstoques[0].Fisico, _Biblioteca.getCasasDecimaisEstoque) <= 0 then begin
    _Biblioteca.Exclamar('N�o foi encontrado nenhuma quantidade de estoque f�sico para o produto selecionado no local ' + FrLocal.GetLocais().nome + '!');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  stEstoqueFisico.Caption := NFormatEstoque(vEstoques[0].Fisico);
  stEstoqueDisponivel.Caption := NFormatEstoque(vEstoques[0].Disponivel);

  if not FrProduto.EstaVazio then
    FrLote.SetTipoControleEstoque(FrProduto.getProduto.TipoControleEstoque, FrProduto.getProduto.ExigirDataFabricacaoLote, FrProduto.getProduto.ExigirDataVencimentoLote);
end;

procedure TFormBloqueioEstoque.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetornoBanco: RecRetornoBD;
  vItens: TArray<RecBloqueiosEstoquesItens>;
begin
  inherited;

  SetLength(vItens, sgItens.RowCount -1);
  for i := 1 to sgItens.RowCount -1 do begin
    vItens[i - 1].BloqueioId           := 0;
    vItens[i - 1].LocalId              := SFormatInt(sgItens.Cells[coLocalId, i]);
    vItens[i - 1].ItemId               := i;
    vItens[i - 1].ProdutoId            := SFormatInt(sgItens.Cells[coProdutoId, i]);
    vItens[i - 1].Quantidade           := SFormatDouble(sgItens.Cells[coQuantidade, i]);
    vItens[i - 1].Lote                 := sgItens.Cells[coLote, i];
  end;

  vRetornoBanco :=
    _BloqueiosEstoques.AtualizarBloqueiosEstoques(
      Sessao.getConexaoBanco,
      eID.AsInt,
      Sessao.getEmpresaLogada.EmpresaId,
      'A',
      eObservacao.Text,
      FrTipoBloqueioEstoque.GetTipoBloqueioEstoque().tipo_bloqueio_id,
      vItens
    );

  Sessao.AbortarSeHouveErro( vRetornoBanco );

  if vRetornoBanco.AsInt > 0 then
    _Biblioteca.Informar('Novo bloqueio de estoque efetuado com sucesso, c�digo: ' + NFormat(vRetornoBanco.AsInt));
end;

procedure TFormBloqueioEstoque.Modo(pEditando: Boolean);
begin
  inherited;
   _Biblioteca.Habilitar([
    FrEmpresa,
    FrTipoBloqueioEstoque,
    FrLocal,
    eObservacao,
    FrProduto,
    eQuantidade,
    sgItens,
    FrLote,
    stEstoqueFisico,
    stEstoqueDisponivel],
    pEditando
  );

  if pEditando then
    SetarFoco(FrEmpresa);
end;

procedure TFormBloqueioEstoque.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormBloqueioEstoque.PreencherRegistro(pBloqueio: RecBloqueiosEstoques);
var
  i: Integer;
  vItens: TArray<RecBloqueiosEstoquesItens>;
begin
  FrEmpresa.InserirDadoPorChave(pBloqueio.EmpresaId, False);
  FrTipoBloqueioEstoque.InserirDadoPorChave(pBloqueio.TipoBloqueioId, False);
  eObservacao.Text := pBloqueio.Observacoes;

  vItens := _BloqueiosEstoquesItens.BuscarBloqueiosEstoquesItens(Sessao.getConexaoBanco, 0, [pBloqueio.BloqueioId]);
  for i := Low(vItens) to High(vItens) do begin
    AdicionarNoProdutoGrid(
      vItens[i].ProdutoId,
      vItens[i].NomeProduto,
      vItens[i].NomeMarca,
      vItens[i].UnidadeVenda,
      vItens[i].Lote,
      vItens[i].Quantidade,
      vItens[i].LocalId,
      vItens[i].NomeLocal,
      0,
      0,
      0,
      0
    );
  end;
end;

procedure TFormBloqueioEstoque.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coProdutoId, coQuantidade, coLocalId] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBloqueioEstoque.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if sgItens.Cells[coProdutoId, 1] = '' then begin
    _Biblioteca.Exclamar('Nenhum produto foi informado!');
    SetarFoco(FrProduto);
    Abort;
  end;
end;

end.
