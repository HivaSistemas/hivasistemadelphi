unit PesquisaBairros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Bairros, System.StrUtils, System.Math, _Sessao,
  _Biblioteca, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameBairros,
  FrameCidades, Vcl.ExtCtrls;

type
  TFormPesquisaBairros = class(TFormHerancaPesquisas)
    FrCidade: TFrCidades;
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarBairro(pPreencherCidade:Boolean): TObject;

implementation

{$R *.dfm}

const
  cp_nome    = 2;
  cp_cidade  = 3;
  coEstadoId = 4;
  coRota     = 5;
  cp_ativo   = 6;

var
  FPreencherCidade: Boolean;

function PesquisarBairro(pPreencherCidade:Boolean): TObject;
begin
  FPreencherCidade := pPreencherCidade;
  Result := _HerancaPesquisas.Pesquisar(TFormPesquisaBairros, _Bairros.GetFiltros);
end;

procedure TFormPesquisaBairros.BuscarRegistros;
var
  i: Integer;
  vBairros: TArray<RecBairros>;
begin
  inherited;

  if FrCidade.EstaVazio then begin
    vBairros :=
      _Bairros.BuscarBairros(
        Sessao.getConexaoBanco,
        cbOpcoesPesquisa.GetIndice,
        [eValorPesquisa.Text],
        0
      );
  end
  else begin
    vBairros :=
      _Bairros.BuscarBairros(
        Sessao.getConexaoBanco,
        cbOpcoesPesquisa.GetIndice,
        [eValorPesquisa.Text],
        FrCidade.GetCidade.cidade_id
      );
  end;

  if vBairros = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vBairros);
  for i := Low(vBairros) to High(vBairros) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]      := NFormat(vBairros[i].bairro_id);
    sgPesquisa.Cells[cp_nome, i + 1]        := vBairros[i].nome;
    sgPesquisa.Cells[cp_cidade, i + 1]      := IntToStr(vBairros[i].cidade_id) + ' - ' + vBairros[i].nome_cidade;
    sgPesquisa.Cells[coEstadoId, i + 1]     := vBairros[i].EstadoId;
    if vBairros[i].nome_rota <> '' then
      sgPesquisa.Cells[coRota, i + 1]       := IntToStr(vBairros[i].rota_id) + ' - ' + vBairros[i].nome_rota
    else
      sgPesquisa.Cells[coRota, i + 1]       := '';
    sgPesquisa.Cells[cp_ativo, i + 1]       := vBairros[i].ativo;
  end;
  sgPesquisa.SetLinhasGridPorTamanhoVetor( Length(vBairros) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaBairros.FormCreate(Sender: TObject);
begin
  inherited;
  FrCidade.ckPreencherEstado.Checked :=  FPreencherCidade;
  if FPreencherCidade then
    FrCidade.InserirDadoPorChave( Sessao.getEmpresaLogada.CidadeId, False );
end;

procedure TFormPesquisaBairros.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, cp_ativo]) then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
