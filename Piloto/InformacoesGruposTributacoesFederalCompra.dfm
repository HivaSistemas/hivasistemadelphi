inherited FormInformacoesGruposTributacoesFederalCompra: TFormInformacoesGruposTributacoesFederalCompra
  Caption = 'Informa'#231#245'es do grupo de tribu'#231#245'es federal de compra'
  ClientHeight = 213
  ClientWidth = 510
  ExplicitWidth = 516
  ExplicitHeight = 242
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel [0]
    Left = 8
    Top = 8
    Width = 37
    Height = 14
    Caption = 'C'#243'digo'
  end
  object lb25: TLabel [1]
    Left = 8
    Top = 50
    Width = 102
    Height = 14
    Caption = 'Origem do produto'
  end
  object lb3: TLabel [2]
    Left = 8
    Top = 92
    Width = 83
    Height = 14
    Caption = 'CST entrada PIS'
  end
  object lb5: TLabel [3]
    Left = 8
    Top = 134
    Width = 105
    Height = 14
    Caption = 'CST entrada COFINS'
  end
  object lb1: TLabel [4]
    Left = 94
    Top = 8
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Top = 176
    Width = 510
    TabOrder = 6
  end
  object eID: TEditLuka
    Left = 8
    Top = 22
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 0
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ckAtivo: TCheckBoxLuka
    Left = 456
    Top = 5
    Width = 46
    Height = 17
    Caption = 'Ativo'
    Enabled = False
    TabOrder = 2
    CheckedStr = 'N'
  end
  object cbOrigemProduto: TComboBoxLuka
    Left = 8
    Top = 64
    Width = 494
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    Enabled = False
    ItemIndex = 0
    TabOrder = 3
    Text = '0 - Nacional'
    OnKeyDown = ProximoCampo
    Items.Strings = (
      '0 - Nacional'
      '1 - Entrangeira(Imp. Direta)'
      '2 - Estrangeira(Adiq. merc. interno)'
      '3 - Nacional(Com cont. imp. superior 40%)'
      '4 - Nacional(Produ'#231#227'o feita em conf. decreto 288/67)'
      '5 - Nacional(Com cont. imp. inferior 40%)'
      '6 - Estrangeira(Imp. direta, sem similiar merc. interno)'
      '7 - Estrangeira(Adiq. merc. interno, sem similiar merc. interno)')
    Valores.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7')
    AsInt = 0
    AsString = '0'
  end
  object cbCstEntradaPIS: TComboBoxLuka
    Left = 8
    Top = 106
    Width = 494
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    Enabled = False
    TabOrder = 4
    AsInt = 0
  end
  object cbCstEntradaCOFINS: TComboBoxLuka
    Left = 8
    Top = 148
    Width = 494
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    Enabled = False
    TabOrder = 5
    AsInt = 0
  end
  object eDescricao: TEditLuka
    Left = 94
    Top = 22
    Width = 356
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
