inherited FormPesquisaEnderecoEstoque: TFormPesquisaEnderecoEstoque
  Caption = 'Pesquisa Endere'#231'o de Estoque'
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    ColCount = 6
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Rua'
      'M'#243'dulo'
      'N'#237'vel'
      'V'#227'o')
    OnGetCellColor = sgPesquisaGetCellColor
    RealColCount = 6
    ColWidths = (
      28
      55
      139
      125
      129
      138)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
end
