inherited FrBuscarPedidosCompras: TFrBuscarPedidosCompras
  Width = 519
  Height = 140
  ExplicitWidth = 519
  ExplicitHeight = 140
  inherited sgValores: TGridLuka
    Width = 236
    Height = 120
    Align = alNone
    ColCount = 3
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    PopupMenu = nil
    OnDrawCell = sgValoresDrawCell
    OnSelectCell = sgValoresSelectCell
    HCol.Strings = (
      'Compra'
      'Saldo'
      'Qtde.utilizar')
    OnGetCellColor = sgValoresGetCellColor
    OnArrumarGrid = sgValoresArrumarGrid
    RealColCount = 3
    ExplicitWidth = 236
    ExplicitHeight = 120
    ColWidths = (
      57
      78
      69)
  end
  inherited StaticTextLuka1: TStaticTextLuka
    Width = 236
    Align = alNone
    Caption = 'Pedidos de compras'
    ExplicitWidth = 236
  end
  object StaticTextLuka2: TStaticTextLuka [2]
    Left = 242
    Top = 0
    Width = 268
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Pedidos de compras'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object sgPedidos: TGridLuka [3]
    Left = 242
    Top = 17
    Width = 268
    Height = 120
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    ColCount = 3
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    PopupMenu = pmOpcoes
    TabOrder = 3
    OnDrawCell = sgPedidosDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Pedido'
      'Data'
      'Valor')
    OnGetCellColor = sgValoresGetCellColor
    Grid3D = False
    RealColCount = 3
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      65
      79
      69)
  end
  inherited pmOpcoes: TPopupMenu
    Left = 96
    inherited miN1: TMenuItem
      Visible = False
    end
    inherited miSubir: TMenuItem
      Visible = False
    end
    inherited miDescer: TMenuItem
      Visible = False
    end
  end
end
