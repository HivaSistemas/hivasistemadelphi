unit ApuracaoICMS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, _Biblioteca, _Sessao, _RecordsEspeciais,
  FrameDataInicialFinal, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, _ApuracoesImpostos,
  FrameEmpresas, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls, Informacoes.NotaFiscal,
  Vcl.Grids, GridLuka;

type
  TFormApuracaoICMS = class(TFormHerancaRelatorios)
    FrEmpresas: TFrEmpresas;
    FrDataContabil: TFrDataInicialFinal;
    sgApuracao: TGridLuka;
    procedure FormCreate(Sender: TObject);
    procedure sgApuracaoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgApuracaoGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgApuracaoDblClick(Sender: TObject);
  private
    FLinhaSaidas: Integer;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormApuracaoICMS }

const
  coMovimentoId     = 0;
  coTipoMovimento   = 1;
  coDataContabil    = 2;
  coCfopId          = 3;
  coValorContabil   = 4;
  coBaseCalculoIcms = 5;
  coValorIcms       = 6;
  coIsentasNaoTrib  = 7;
  coOutras          = 8;

  (* Constantes com as linhas *)
  clEntradas = 1;

procedure TFormApuracaoICMS.Carregar(Sender: TObject);
var
  vSql: string;

  i: Integer;
  vLinha: Integer;
  vEntradas: TArray<RecApuracoesImpostos>;
  vSaidas: TArray<RecApuracoesImpostos>;
begin
  inherited;
  vLinha := 1;
  sgApuracao.ClearGrid;

  FrEmpresas.getSqlFiltros('EMPRESA_ID', vSql, True);
  FrDataContabil.getSqlFiltros('DATA_CONTABIL', vSql, True);

  vEntradas :=
    _ApuracoesImpostos.BuscarApuracoesImpostosComando(
      Sessao.getConexaoBanco,
      vSql + ' and NATUREZA = ''E'' ' +
      'order by ' +
      '  DATA_CONTABIL, ' +
      '  MOVIMENTO_ID '
    );

  vSaidas :=
    _ApuracoesImpostos.BuscarApuracoesImpostosComando(
      Sessao.getConexaoBanco,
      vSql + ' and NATUREZA = ''S'' ' +
      'order by ' +
      '  DATA_CONTABIL, ' +
      '  MOVIMENTO_ID '
    );

  if vEntradas + vSaidas = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  sgApuracao.Cells[coCfopId, vLinha] := 'Entradas';
  for i := Low(vEntradas) to High(vEntradas) do begin
    Inc(vLinha);

    sgApuracao.Cells[coMovimentoId, vLinha]     := NFormatN(vEntradas[i].MovimentoId);
    sgApuracao.Cells[coTipoMovimento, vLinha]   := vEntradas[i].TipoMovimento;
    sgApuracao.Cells[coDataContabil, vLinha]    := DFormat(vEntradas[i].DataContabil);
    sgApuracao.Cells[coCfopId, vLinha]          := vEntradas[i].CfopId;
    sgApuracao.Cells[coValorContabil, vLinha]   := NFormatN(vEntradas[i].ValorContabil);
    sgApuracao.Cells[coBaseCalculoIcms, vLinha] := NFormatN(vEntradas[i].BaseCalculoIcms);
    sgApuracao.Cells[coValorIcms, vLinha]       := NFormatN(vEntradas[i].ValorIcms);
//    sgApuracao.Cells[coIsentasNaoTrib, vLinha]  := NFormatN(vEntradas[i].);
//    sgApuracao.Cells[coOutras, vLinha]          := NFormatN(vEntradas[i].ValorContabil);
  end;

  Inc(vLinha, 2);

  FLinhaSaidas := vLinha;
  sgApuracao.Cells[coCfopId, vLinha] := 'Sa�das';
  for i := Low(vSaidas) to High(vSaidas) do begin
    Inc(vLinha);

    sgApuracao.Cells[coMovimentoId, vLinha]     := NFormatN(vSaidas[i].MovimentoId);
    sgApuracao.Cells[coTipoMovimento, vLinha]   := vSaidas[i].TipoMovimento;
    sgApuracao.Cells[coDataContabil, vLinha]    := DFormat(vSaidas[i].DataContabil);
    sgApuracao.Cells[coCfopId, vLinha]          := vSaidas[i].CfopId;
    sgApuracao.Cells[coValorContabil, vLinha]   := NFormatN(vSaidas[i].ValorContabil);
    sgApuracao.Cells[coBaseCalculoIcms, vLinha] := NFormatN(vSaidas[i].BaseCalculoIcms);
    sgApuracao.Cells[coValorIcms, vLinha]       := NFormatN(vSaidas[i].ValorIcms);
//    sgApuracao.Cells[coIsentasNaoTrib, vLinha]  := NFormatN(vSaidas[i].);
//    sgApuracao.Cells[coOutras, vLinha]          := NFormatN(vSaidas[i].ValorContabil);
  end;


  sgApuracao.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormApuracaoICMS.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId);
end;

procedure TFormApuracaoICMS.sgApuracaoDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.NotaFiscal.Informar( SFormatInt(sgApuracao.Cells[coMovimentoId, sgApuracao.Row]) );
end;

procedure TFormApuracaoICMS.sgApuracaoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coMovimentoId,
    coValorContabil,
    coBaseCalculoIcms,
    coValorIcms,
    coIsentasNaoTrib,
    coOutras]
  then
    vAlinhamento := taRightJustify
  else if ACol in[coTipoMovimento, coCfopId] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgApuracao.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormApuracaoICMS.sgApuracaoGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ARow = clEntradas then begin
    AFont.Style := [fsBold];
    AFont.Color := clBlue;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao1
  end
  else if ARow = FLinhaSaidas then begin
    AFont.Style := [fsBold];
    AFont.Color := clRed;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
  end
  else if ACol = coTipoMovimento then begin
    AFont.Style := [fsBold];
    AFont.Color := IIfInt(sgApuracao.Cells[ACol, ARow] = 'NFI', clBlue, $000096DB);
  end;
end;

procedure TFormApuracaoICMS.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrEmpresas.EstaVazio then begin
    _Biblioteca.Exclamar('Nenhuma empresa foi selecionada, selecione ao menos 1 antes de continuar!');
    SetarFoco(FrEmpresas);
    Abort;
  end;

  if not FrDataContabil.DatasValidas then begin
    _Biblioteca.Exclamar('Nenhuma data cont�bil foi selecionada, selecione o per�odo antes de continuar!');
    SetarFoco(FrDataContabil);
    Abort;
  end;
end;

end.
