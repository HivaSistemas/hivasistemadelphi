inherited FormSugestaoCompras: TFormSugestaoCompras
  Caption = 'Sugest'#227'o de compras'
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Top = 456
      Visible = False
      ExplicitTop = 456
    end
    object miRealizarPedidoCompra: TSpeedButton [2]
      Left = 1
      Top = 128
      Width = 117
      Height = 35
      Caption = 'Gerar pedido'
      Flat = True
      OnClick = miRealizarPedidoCompraClick
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Top = 488
      ExplicitTop = 488
    end
  end
  inherited pcDados: TPageControl
    ActivePage = tsResultado
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      inline FrFornecedores: TFrFornecedores
        Left = 467
        Top = 2
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 467
        ExplicitTop = 2
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 74
            Height = 14
            ExplicitWidth = 74
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrMarcas: TFrMarcas
        Left = 467
        Top = 87
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 467
        ExplicitTop = 87
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 39
            Height = 14
            ExplicitWidth = 39
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 2
        Top = 87
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 87
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 48
            Height = 14
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrEmpresas: TFrEmpresas
        Left = 2
        Top = 2
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 2
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 53
            Height = 14
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      object ckTrazerApenasItensSugestao: TCheckBoxLuka
        Left = 3
        Top = 419
        Width = 319
        Height = 17
        Anchors = [akLeft, akBottom]
        Caption = 'Somente itens com sugest'#227'o'
        Checked = True
        State = cbChecked
        TabOrder = 5
        CheckedStr = 'S'
      end
      object ckTrazerSugestaoBaseadaMovimentacoesFisicas: TCheckBoxLuka
        Left = 3
        Top = 442
        Width = 319
        Height = 17
        Anchors = [akLeft, akBottom]
        Caption = 'Sugest'#227'o com base no estoque f'#237'sico'
        TabOrder = 6
        CheckedStr = 'N'
      end
      object GroupBoxLuka1: TGroupBoxLuka
        Left = 2
        Top = 183
        Width = 241
        Height = 62
        TabOrder = 4
        OpcaoMarcarDesmarcar = False
        object Label1: TLabel
          Left = 24
          Top = 15
          Width = 91
          Height = 14
          Caption = 'Dias suprimento'
        end
        object Label2: TLabel
          Left = 123
          Top = 15
          Width = 78
          Height = 14
          Caption = '% crescimento'
        end
        object eDiasSuprimento: TEditLuka
          Left = 24
          Top = 29
          Width = 91
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          TabOrder = 0
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 0
          ValorMaximo = 365.000000000000000000
          NaoAceitarEspaco = False
        end
        object ePercentualCrescimento: TEditLuka
          Left = 123
          Top = 29
          Width = 91
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          TabOrder = 1
          Text = '0,00'
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          ValorMaximo = 500.000000000000000000
          NaoAceitarEspaco = False
        end
      end
      object gbMeses: TGroupBoxLuka
        Left = 0
        Top = 245
        Width = 243
        Height = 153
        Caption = '  Meses para sugest'#227'o'
        TabOrder = 7
        OpcaoMarcarDesmarcar = True
        object ckMesAtual: TCheckBoxLuka
          Left = 14
          Top = 15
          Width = 59
          Height = 17
          Caption = 'Atual'
          TabOrder = 0
          CheckedStr = 'N'
        end
        object ckMes1: TCheckBoxLuka
          Left = 14
          Top = 39
          Width = 59
          Height = 17
          Caption = '02/2018'
          TabOrder = 1
          CheckedStr = 'N'
        end
        object ckMes2: TCheckBoxLuka
          Left = 14
          Top = 63
          Width = 59
          Height = 17
          Caption = '01/2018'
          TabOrder = 2
          CheckedStr = 'N'
        end
        object ckMes3: TCheckBoxLuka
          Left = 14
          Top = 87
          Width = 59
          Height = 17
          Caption = '12/2017'
          TabOrder = 3
          CheckedStr = 'N'
        end
        object ckMes4: TCheckBoxLuka
          Left = 14
          Top = 110
          Width = 59
          Height = 17
          Caption = '11/2017'
          TabOrder = 4
          CheckedStr = 'N'
        end
        object ckMes5: TCheckBoxLuka
          Left = 14
          Top = 132
          Width = 59
          Height = 17
          Caption = '10/2017'
          TabOrder = 5
          CheckedStr = 'N'
        end
        object ckMes6: TCheckBoxLuka
          Left = 136
          Top = 15
          Width = 59
          Height = 17
          Caption = '09/2017'
          TabOrder = 6
          CheckedStr = 'N'
        end
        object ckMes7: TCheckBoxLuka
          Left = 136
          Top = 39
          Width = 59
          Height = 17
          Caption = '08/2017'
          TabOrder = 7
          CheckedStr = 'N'
        end
        object ckMes8: TCheckBoxLuka
          Left = 136
          Top = 63
          Width = 59
          Height = 17
          Caption = '07/2017'
          TabOrder = 8
          CheckedStr = 'N'
        end
        object ckMes9: TCheckBoxLuka
          Left = 136
          Top = 87
          Width = 59
          Height = 17
          Caption = '06/2017'
          TabOrder = 9
          CheckedStr = 'N'
        end
        object ckMes10: TCheckBoxLuka
          Left = 136
          Top = 110
          Width = 59
          Height = 17
          Caption = '05/2017'
          TabOrder = 10
          CheckedStr = 'N'
        end
        object ckMes11: TCheckBoxLuka
          Left = 136
          Top = 132
          Width = 59
          Height = 17
          Caption = '04/2017'
          TabOrder = 11
          CheckedStr = 'N'
        end
      end
      inline FrLocais: TFrLocais
        Left = 467
        Top = 184
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 8
        TabStop = True
        ExplicitLeft = 467
        ExplicitTop = 184
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 293
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 318
          inherited lbNomePesquisa: TLabel
            Width = 103
            Height = 14
            Caption = 'Locais de produtos'
            ExplicitWidth = 103
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 213
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 293
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
    end
    inherited tsResultado: TTabSheet
      object sgItens: TGridLuka
        Left = 0
        Top = 18
        Width = 884
        Height = 382
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 17
        Ctl3D = True
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 3
        RowCount = 2
        GradientEndColor = 15395562
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goThumbTracking]
        ParentCtl3D = False
        PopupMenu = pmOpcoes
        TabOrder = 0
        OnClick = sgItensClick
        OnDrawCell = sgItensDrawCell
        OnKeyPress = NumerosVirgula
        OnSelectCell = sgItensSelectCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Leg.'
          'Produto'
          'Nome'
          'M'#233'd.di'#225'ria'
          'Prev.ven.mens.'
          'Estoque'
          'Dias est.'
          'Pend'#234'ncias'
          'Sug.compra'
          'Und.ven.'
          'Pre'#231'o tabela'
          'Qtde.comprar'
          'Und.com.'
          'Pr.compra'
          'Vlr.tot.comprar'
          'Dt.ult.entrada'
          'Forn.ult.entr.')
        OnGetCellColor = sgItensGetCellColor
        OnArrumarGrid = sgItensArrumarGrid
        Grid3D = False
        RealColCount = 30
        OrdenarOnClick = True
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          26
          55
          170
          69
          86
          58
          58
          71
          71
          55
          75
          83
          59
          88
          91
          94
          242)
      end
      object pcInformacoes: TPageControl
        Left = 0
        Top = 400
        Width = 884
        Height = 118
        ActivePage = tsInformacoesProduto
        Align = alBottom
        TabOrder = 1
        object tsInformacoesProduto: TTabSheet
          Caption = 'Informa'#231#245'es do produto'
          object st4: TStaticText
            Left = 0
            Top = 0
            Width = 105
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Nome de compra '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 0
            Transparent = False
          end
          object stNomeCompra: TStaticText
            Left = 104
            Top = 0
            Width = 297
            Height = 16
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 1
            Transparent = False
          end
          object st1: TStaticText
            Left = 0
            Top = 43
            Width = 105
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'C'#243'digo de barras '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 2
            Transparent = False
          end
          object stCodigoBarras: TStaticText
            Left = 104
            Top = 43
            Width = 297
            Height = 16
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 3
            Transparent = False
          end
          object st2: TStaticText
            Left = 0
            Top = 58
            Width = 105
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'C'#243'digo original '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 4
            Transparent = False
          end
          object stCodigoOriginal: TStaticText
            Left = 104
            Top = 58
            Width = 297
            Height = 16
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 5
            Transparent = False
          end
          object st5: TStaticText
            Left = 0
            Top = 73
            Width = 105
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Unidade venda '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 6
            Transparent = False
          end
          object stUnidadeVenda: TStaticText
            Left = 104
            Top = 73
            Width = 89
            Height = 16
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 7
            Transparent = False
          end
          object stMultiploVenda: TStaticText
            Left = 296
            Top = 73
            Width = 105
            Height = 16
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 8
            Transparent = False
          end
          object st6: TStaticText
            Left = 192
            Top = 73
            Width = 105
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'M'#250'ltiplo venda '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 9
            Transparent = False
          end
          object sgMultiplosCompra: TGridLuka
            Left = 405
            Top = 0
            Width = 205
            Height = 89
            Hint = 
              'Clique duas vezes sobre a unidade de compra para converter a qua' +
              'ntidade sugerida para esta unidade'
            Align = alCustom
            ColCount = 3
            Ctl3D = True
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            GradientEndColor = 15395562
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
            ParentCtl3D = False
            TabOrder = 10
            OnDblClick = sgMultiplosCompraDblClick
            OnDrawCell = sgMultiplosCompraDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'Und.com.'
              'Mult.com.'
              'Qtde.emb.')
            Grid3D = False
            RealColCount = 20
            AtivarPopUpSelecao = False
            ColWidths = (
              58
              63
              62)
          end
          object st7: TStaticText
            Left = 0
            Top = 15
            Width = 105
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Fabricante '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 11
            Transparent = False
          end
          object stFabricante: TStaticText
            Left = 104
            Top = 15
            Width = 297
            Height = 16
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 12
            Transparent = False
          end
          object st10: TStaticText
            Left = 0
            Top = 28
            Width = 105
            Height = 16
            Alignment = taRightJustify
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Marca '
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 13
            Transparent = False
          end
          object stMarca: TStaticText
            Left = 104
            Top = 28
            Width = 297
            Height = 16
            Margins.Left = 0
            Margins.Top = 0
            Margins.Right = 0
            Margins.Bottom = 0
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Color = clWhite
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            TabOrder = 14
            Transparent = False
          end
        end
        object tsMovimentacoes: TTabSheet
          Caption = 'Movimenta'#231#245'es mensais'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object sgMovimentacoesMensais: TGridLuka
            Left = 0
            Top = 0
            Width = 876
            Height = 89
            Align = alClient
            ColCount = 14
            Ctl3D = True
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 4
            GradientEndColor = 15395562
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goThumbTracking]
            ParentCtl3D = False
            TabOrder = 0
            OnDrawCell = sgMovimentacoesMensaisDrawCell
            OnSelectCell = sgItensSelectCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'M'#234's/Ano'
              '08/18'
              '07/18'
              '06/18'
              '05/18'
              '04/18'
              '03/18'
              '02/18'
              '01/18'
              '12/17'
              '11/17'
              '10/17'
              '09/17'
              '08/17')
            HRow.Strings = (
              'Qtde.vendidos'
              'Qtde.devolvidos'
              'Qtde.compras')
            Grid3D = False
            RealColCount = 20
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              93
              62
              60
              53
              58
              57
              59
              57
              64
              64
              57
              55
              55
              64)
          end
        end
        object TabSheet1: TTabSheet
          Caption = 'Sugest'#227'o por locais'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object sgSugestaoLocais: TGridLuka
            Left = 0
            Top = 0
            Width = 876
            Height = 89
            Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
            Align = alClient
            ColCount = 4
            Ctl3D = True
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            GradientEndColor = 15395562
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goThumbTracking]
            ParentCtl3D = False
            PopupMenu = pmOpcoes
            TabOrder = 0
            OnDrawCell = sgSugestaoLocaisDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clActiveCaption
            HCol.Strings = (
              'Local'
              'Estoque'
              'Vendas do per'#237'odo'
              'Sugest'#227'o')
            Grid3D = False
            RealColCount = 30
            OrdenarOnClick = True
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              254
              122
              124
              100)
          end
        end
      end
      object pn1: TPanel
        Left = 0
        Top = 0
        Width = 884
        Height = 18
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object st3: TStaticText
          Left = -1
          Top = 0
          Width = 217
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Dias '#250'teis per'#237'odo selecionado '
          Color = 38619
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Transparent = False
        end
        object stDiasUteisPeridoSelecionado: TStaticText
          Left = 215
          Top = 0
          Width = 57
          Height = 16
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          Transparent = False
        end
        object st8: TStaticText
          Left = 352
          Top = 0
          Width = 207
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Dias '#250'teis para calculo suprimento '
          Color = 38619
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          Transparent = False
        end
        object stDiasUteisCalculoSuprimento: TStaticText
          Left = 558
          Top = 0
          Width = 57
          Height = 16
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 3
          Transparent = False
        end
      end
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 432
    Top = 232
  end
end
