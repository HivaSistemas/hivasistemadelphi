unit PesquisaBancosCaixas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _RecordsCadastros, _Sessao,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Contas, _Biblioteca, System.Math, System.StrUtils,
  Vcl.ExtCtrls;

type
  TFormPesquisaBancosCaixas = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarBancoCaixa(pTipo: string; pSomenteAtivos: Boolean; pFuncionarioID: Integer): TObject;

implementation

{$R *.dfm}

const
  cp_nome    = 2;
  cp_tipo    = 3;
  cp_agencia = 4;
  cp_conta   = 5;
  cp_ativo   = 6;

var
  FTipo: string;
  FSomenteAtivos: Boolean;
  FfuncionarioAutorizadoID: Integer;

function PesquisarBancoCaixa(pTipo: string; pSomenteAtivos: Boolean; pFuncionarioID: Integer): TObject;
var
  r: TObject;
begin
  FTipo := pTipo;
  FSomenteAtivos := pSomenteAtivos;
  FfuncionarioAutorizadoID := pFuncionarioID;
  r := _HerancaPesquisas.Pesquisar(TFormPesquisaBancosCaixas, _Contas.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := RecContas(r);
end;

procedure TFormPesquisaBancosCaixas.BuscarRegistros;
var
  i: Integer;
  vContas: TArray<RecContas>;
begin
  inherited;

  vContas :=
    _Contas.BuscarContas(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FTipo,
      FSomenteAtivos,
      FfuncionarioAutorizadoID
    );

  if vContas = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vContas);

  for i := Low(vContas) to High(vContas) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1] := vContas[i].Conta;
    sgPesquisa.Cells[cp_nome, i + 1] := vContas[i].Nome;
    sgPesquisa.Cells[cp_tipo, i + 1] := IntToStr(vContas[i].Tipo) + ' - ' + IfThen(vContas[i].Tipo = 0, 'Caixa', 'Banco');
    sgPesquisa.Cells[cp_agencia, i + 1] := IfThen(vContas[i].Agencia <> '', vContas[i].Agencia);
    sgPesquisa.Cells[cp_conta, i + 1] := IfThen(vContas[i].Conta <> '', vContas[i].Conta) + Ifthen(vContas[i].DigitoConta <> '', ' - ' + vContas[i].DigitoConta);
    sgPesquisa.Cells[cp_ativo, i + 1] := SimNao(vContas[i].Ativo);
  end;

  sgPesquisa.RowCount := IfThen(Length(vContas) = 1, 2, High(vContas) + 2);
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaBancosCaixas.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, cp_ativo]) then
    alinhamento := taCenter
  else if ACol = coCodigo then
    alinhamento := taRightJustify
  else
    alinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

procedure TFormPesquisaBancosCaixas.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = cp_ativo then begin
    AFont.Color := AzulVermelho(sgPesquisa.Cells[ACol, ARow]);
    AFont.Style := [fsBold];
  end;
end;

end.
