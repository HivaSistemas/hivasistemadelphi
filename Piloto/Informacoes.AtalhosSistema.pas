unit Informacoes.AtalhosSistema;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka;

type
  TFormAtalhoSistema = class(TFormHerancaFinalizar)
    sgAtalhos: TGridLuka;
    procedure FormShow(Sender: TObject);
    procedure sgAtalhosDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormAtalhoSistema: TFormAtalhoSistema;

implementation

uses _AtalhosSistema, _RecordsCadastros, _Sessao;

const
  coAtalho     = 0;
  coDescricao  = 1;

{$R *.dfm}

procedure TFormAtalhoSistema.FormShow(Sender: TObject);
var
  atalhos: TArray<RecAtalhosSistema>;
  i: Integer;
begin
  inherited;
  atalhos := _AtalhosSistema.BuscarAtalhoSistema(Sessao.getConexaoBanco, 1, ['']);

  for I := 0 to Length(atalhos) - 1 do begin
    sgAtalhos.Cells[coAtalho, i + 1]    := atalhos[i].atalho;
    sgAtalhos.Cells[coDescricao, i + 1] := atalhos[i].descricao;
  end;
  sgAtalhos.SetLinhasGridPorTamanhoVetor( Length(atalhos) );
end;

procedure TFormAtalhoSistema.sgAtalhosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  vAlinhamento := taLeftJustify;

  sgAtalhos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
