unit DefinirContaCustodiaContasReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca, _Sessao,
  EditLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, _RecordsEspeciais,
  FrameContasCustodia, Vcl.Buttons, Vcl.ExtCtrls, _ContasReceber, _RecordsFinanceiros;

type
  TFormDefinirContaCustodiaContasReceber = class(TFormHerancaFinalizar)
    FrContaCustodia: TFrContasCustodia;
    eObservacoes: TEditLuka;
    lb1: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    FReceberIds: TArray<Integer>;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Definir(pReceberIds: TArray<Integer>): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

function Definir(pReceberIds: TArray<Integer>): TRetornoTelaFinalizar;
var
  vContasReceber: TArray<RecContasReceber>;
  vForm: TFormDefinirContaCustodiaContasReceber;
begin
  Result.Iniciar;

  if pReceberIds = nil then
    Exit;

  vContasReceber := _ContasReceber.BuscarContasReceberComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('COR.RECEBER_ID', pReceberIds));
  if vContasReceber = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vForm := TFormDefinirContaCustodiaContasReceber.Create(nil);

  vForm.FReceberIds := pReceberIds;
//  vForm.FrContaCustodia.InserirDadoPorChave( vForm.FContasReceber.ContaCustodiaId, False );
//  vForm.eObservacoes.Text := vForm.FContasReceber.ObservacoesContaCustodia;

  Result.Ok(vForm.ShowModal, True);
end;

procedure TFormDefinirContaCustodiaContasReceber.Finalizar(Sender: TObject);
var
  vContaCustodiaId: Integer;

  vRetBanco: RecRetornoBD;
begin
  inherited;

  vContaCustodiaId := 0;
  if not FrContaCustodia.EstaVazio then
    vContaCustodiaId := FrContaCustodia.getDados().ContaCustodiaId;

  vRetBanco :=
    _ContasReceber.AtualizarContaCustodia(
      Sessao.getConexaoBanco,
      FReceberIds,
      vContaCustodiaId,
      eObservacoes.Text
    );
end;

procedure TFormDefinirContaCustodiaContasReceber.FormCreate(Sender: TObject);
begin
  inherited;
  SetarFoco(FrContaCustodia);
end;

procedure TFormDefinirContaCustodiaContasReceber.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrContaCustodia.EstaVazio and (eObservacoes.Trim <> '') then begin
    _Biblioteca.Exclamar('N�o � permitido informar observa��es quando n�o h� conta definida, verifique!');
    SetarFoco(FrContaCustodia);
    Abort;
  end;

  if (not FrContaCustodia.EstaVazio) and (eObservacoes.Trim = '') then begin
    _Biblioteca.Exclamar('� necess�rio informar as observa��es quando h� conta cust�dia definida, verifique!');
    SetarFoco(eObservacoes);
    Abort;
  end;
end;

end.
