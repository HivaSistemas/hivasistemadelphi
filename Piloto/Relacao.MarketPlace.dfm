inherited FormRelacaoMarketPlace: TFormRelacaoMarketPlace
  Caption = 'Rela'#231#227'o de Market Place'
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Left = -3
      Top = 103
      Visible = False
      ExplicitLeft = -3
      ExplicitTop = 103
    end
  end
  inherited pcDados: TPageControl
    ActivePage = tsResultado
    inherited tsFiltros: TTabSheet
      inline FrEmpresas: TFrEmpresas
        Left = 1
        Top = 8
        Width = 312
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 8
        ExplicitWidth = 312
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 287
          Height = 24
          ExplicitWidth = 287
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          Width = 312
          ExplicitWidth = 312
          inherited lbNomePesquisa: TLabel
            Width = 173
            Caption = 'Empresa de origem do estoque:'
            ExplicitWidth = 173
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Left = 145
            ExplicitLeft = 145
          end
          inherited pnSuprimir: TPanel
            Left = 207
            ExplicitLeft = 207
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 287
          Height = 25
          ExplicitLeft = 287
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 1
        Top = 173
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 173
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Top = 23
          Checked = True
          State = cbChecked
          ExplicitTop = 23
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrCodigoOrcamento: TFrameInteiros
        Left = 451
        Top = 68
        Width = 118
        Height = 84
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 451
        ExplicitTop = 68
        ExplicitWidth = 118
        inherited sgInteiros: TGridLuka
          Width = 118
          ExplicitWidth = 118
        end
        inherited Panel1: TPanel
          Width = 118
          Caption = 'C'#243'digo do pedido'
          ExplicitWidth = 118
        end
      end
      inline FrLocais: TFrLocais
        Left = 1
        Top = 114
        Width = 311
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 114
        ExplicitWidth = 311
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 286
          Height = 23
          ExplicitWidth = 286
          ExplicitHeight = 23
        end
        inherited CkAspas: TCheckBox
          Top = 68
          ExplicitTop = 68
        end
        inherited PnTitulos: TPanel
          Width = 311
          ExplicitWidth = 311
          inherited lbNomePesquisa: TLabel
            Width = 150
            Caption = 'Local de sa'#237'da de produtos:'
            ExplicitWidth = 150
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Left = 160
            ExplicitLeft = 160
          end
          inherited pnSuprimir: TPanel
            Left = 206
            ExplicitLeft = 206
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 286
          Height = 24
          ExplicitLeft = 286
          ExplicitHeight = 24
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrDataCadastro: TFrDataInicialFinal
        Left = 451
        Top = 9
        Width = 198
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 451
        ExplicitTop = 9
        ExplicitWidth = 198
        inherited Label1: TLabel
          Width = 151
          Height = 14
          Caption = 'Data de gera'#231#227'o da entrega'
          ExplicitWidth = 151
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrEmpresaFaturamento: TFrEmpresasFaturamento
        Left = 1
        Top = 58
        Width = 312
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 58
        ExplicitWidth = 312
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 287
          Height = 24
          ExplicitWidth = 287
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          Width = 312
          ExplicitWidth = 312
          inherited lbNomePesquisa: TLabel
            Width = 138
            Caption = 'Empresa de faturamento:'
            ExplicitWidth = 138
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Left = 145
            ExplicitLeft = 145
          end
          inherited pnSuprimir: TPanel
            Left = 207
            ExplicitLeft = 207
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 287
          Height = 25
          ExplicitLeft = 287
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object sgProdutos: TGridLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 483
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 0
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Pedido'
          'Produto'
          'Nome'
          'Data Cadastro'
          'Local'
          'Quantidade'
          'Valor Unit'#225'rio'
          'Valor Total')
        Grid3D = False
        RealColCount = 8
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          66
          57
          229
          83
          169
          78
          88
          88)
      end
      object stValorTotalEstoque: TStaticTextLuka
        Left = 736
        Top = 501
        Width = 147
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = '0,00'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        Transparent = False
        AsInt = 0
        TipoCampo = tcNumerico
        CasasDecimais = 2
      end
      object st3: TStaticText
        Left = 736
        Top = 486
        Width = 147
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Valor total geral'
        Color = 38619
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
      end
    end
  end
end
