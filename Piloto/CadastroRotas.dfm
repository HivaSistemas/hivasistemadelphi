inherited FormCadastroRotas: TFormCadastroRotas
  Caption = 'Cadastro de rotas'
  ClientHeight = 177
  ClientWidth = 495
  ExplicitWidth = 501
  ExplicitHeight = 206
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Top = 29
    ExplicitTop = 29
  end
  object lb1: TLabel [1]
    Left = 210
    Top = 29
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  inherited pnOpcoes: TPanel
    Height = 177
    ExplicitHeight = 177
  end
  inherited eID: TEditLuka
    Top = 43
    ExplicitTop = 43
  end
  object eNome: TEditLuka [4]
    Left = 210
    Top = 43
    Width = 255
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 450
    TabOrder = 3
    ExplicitLeft = 450
  end
end
