unit Login;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.Imaging.jpeg, Vcl.ExtCtrls, Vcl.StdCtrls,
  EditLuka, _Biblioteca, _RecordsCadastros, Vcl.Imaging.pngimage, Vcl.Buttons,
  ComboBoxLuka;

type
  TFormLogin = class(TFormHerancaPrincipal)
    imLogin: TImage;
    eNomeUsuario: TEditLuka;
    eSenha: TEditLuka;
    eCodigoUsuario: TEditLuka;
    sbOk: TSpeedButton;
    sbCancelar: TSpeedButton;
    imFuncionario: TImage;
    cbEmpresas: TComboBoxLuka;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eSenhaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eCodigoUsuarioExit(Sender: TObject);
    procedure sbCancelarClick(Sender: TObject);
    procedure sbOkClick(Sender: TObject);
    procedure eNomeUsuarioEnter(Sender: TObject);
    function BuscarEmpresaLogin(FuncionarioId: Integer): TArray<RecEmpresas>;
  private
    FUsuario: TArray<RecFuncionarios>;
    vEmpresas: TArray<RecEmpresas>;

    procedure BuscarFuncionario;
    procedure ValidarFuncionario;
  public
    { Public declarations }
  end;

function Logar(pFuncionarioId: Integer; var pEmpresaSelecionada: RecEmpresas): Integer;

implementation

uses
  _Sessao, _Funcionarios, _Empresas;

{$R *.dfm}

function Logar(pFuncionarioId: Integer; var pEmpresaSelecionada: RecEmpresas): Integer;
var
  vFormLogin: TFormLogin;
begin
  Result := 0;
  vFormLogin := TFormLogin.Create(Application);

  if pFuncionarioId > 0 then begin
    vFormLogin.eCodigoUsuario.AsInt := pFuncionarioId;
    vFormLogin.eCodigoUsuario.Enabled := False;
    vFormLogin.eCodigoUsuarioExit(nil);
  end;

  if vFormLogin.ShowModal = mrOk then begin
    pEmpresaSelecionada := vFormLogin.vEmpresas[vFormLogin.cbEmpresas.ItemIndex];
    Result := vFormLogin.FUsuario[0].funcionario_id;
    Destruir(TArray<TObject>(vFormLogin.Fusuario));
  end;

  vFormLogin.Free;
end;

procedure TFormLogin.BuscarFuncionario;
var
  i: Integer;
  vImagem: TJPEGImage;
begin
  _Biblioteca.LimparCampos([eNomeUsuario, eSenha, imFuncionario, cbEmpresas]);
  FUsuario := _Funcionarios.BuscarFuncionarios(Sessao.getConexaoBanco, 0, [eCodigoUsuario.AsInt], True, False, False, False, False);

  if FUsuario = nil then begin
    Exclamar('Usu�rio n�o encontrado');
    eCodigoUsuario.SetFocus;
    Exit;
  end;

  eNomeUsuario.Text := FUsuario[0].apelido;
  if FUsuario[0].foto <> nil then begin
    FUsuario[0].foto.Position := 0;
    vImagem := TJPEGImage.Create;
    vImagem.LoadFromStream(FUsuario[0].foto);
    imFuncionario.Picture.Graphic := vImagem;
  end;

  vEmpresas := BuscarEmpresaLogin(eCodigoUsuario.AsInt);
  if vEmpresas = nil then begin
    Exclamar('Nenhuma empresa autorizada para o funcion�rio!');
    eCodigoUsuario.SetFocus;
    Exit;
  end;

  for i := Low(vEmpresas) to High(vEmpresas) do begin
    cbEmpresas.Items.Add(IntToStr(vEmpresas[i].EmpresaId) + ' - ' + vEmpresas[i].NomeFantasia);
    cbEmpresas.Valores.Add(IntToStr(vEmpresas[i].EmpresaId));
  end;

  cbEmpresas.ItemIndex := 0;

  {$IFDEF DEBUG}
    if eCodigoUsuario.AsInt = 1 then
      eSenha.Text := '123456'
    else if  eCodigoUsuario.AsInt = 2 then
      eSenha.Text := 'dados1';
  {$ENDIF}
end;

function TFormLogin.BuscarEmpresaLogin(funcionarioId: Integer): TArray<RecEmpresas>;
var
  i: Integer;
  vComando: string;
  vEmpresasAutorizadas: TArray<Integer>;
begin
  Result := nil;

  vEmpresasAutorizadas := _Funcionarios.BuscarEmpresasFuncionario(Sessao.getConexaoBanco, funcionarioId);

  vComando := '';
  for i := Low(vEmpresasAutorizadas) to High(vEmpresasAutorizadas) do begin
    if vComando = '' then begin
      vComando := ' where EMP.EMPRESA_ID in(' + IntToStr(vEmpresasAutorizadas[i]);
      Continue;
    end;

    vComando := vComando + ' ,' + IntToStr(vEmpresasAutorizadas[i]);
  end;
  vComando := vComando + ') order by EMP.EMPRESA_ID asc';

  Result := _Empresas.BuscarEmpresasComando(Sessao.getConexaoBanco, vComando);
//  if vEmpresas = nil then
//    Exit;
//
//  if Length(vEmpresas) = 1 then begin
//    Result := vEmpresas[0];
//    Exit;
//  end;
//
//  t := TFormEmpresasLogin.Create(Application);
//  t.FDados := TList.Create;
//  for i := Low(vEmpresas) to High(vEmpresas) do begin
//    t.FDados.Add(vEmpresas[i]);
//    t.sgEmpresas.Cells[co_codigo, i + 1]        := NFormat(vEmpresas[i].EmpresaId);
//    t.sgEmpresas.Cells[co_nome_fantasia, i + 1] := vEmpresas[i].NomeFantasia;
//    t.sgEmpresas.Cells[co_razao_social, i + 1]  := vEmpresas[i].RazaoSocial;
//    t.sgEmpresas.Cells[co_cnpj, i + 1]          := vEmpresas[i].Cnpj;
//  end;
//  t.sgEmpresas.RowCount := IfThen(Length(vEmpresas) > 1, High(vEmpresas) + 2, 2);
//
//  if t.ShowModal = mrOk then
//    Result := RecEmpresas(t.FDados[t.sgEmpresas.Row - 1])
//  else
//    Halt;
//
//  for i := 0 to t.FDados.Count -1 do begin
//    if i = t.sgEmpresas.Row -1 then
//      Continue;
//
//    RecEmpresas(t.FDados[i]).Free;
//  end;
//
//  FreeAndNil(t.FDados);
//  t.Free;
end;

procedure TFormLogin.eCodigoUsuarioExit(Sender: TObject);
begin
  inherited;
  if eCodigoUsuario.AsInt = 0 then begin
    _Biblioteca.LimparCampos([eNomeUsuario, eSenha, imFuncionario]);
    Exit;
  end;

  BuscarFuncionario;
end;

procedure TFormLogin.eNomeUsuarioEnter(Sender: TObject);
begin
  inherited;
  SetarFoco(eCodigoUsuario);
end;

procedure TFormLogin.eSenhaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    ValidarFuncionario;
end;

procedure TFormLogin.FormCreate(Sender: TObject);
begin
  inherited;
  FUsuario := nil;
end;

procedure TFormLogin.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_ESCAPE then
    ModalResult := mrCancel;
end;

procedure TFormLogin.sbCancelarClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end;

procedure TFormLogin.sbOkClick(Sender: TObject);
begin
  inherited;
  ValidarFuncionario;
end;

procedure TFormLogin.ValidarFuncionario;
begin
  if FUsuario = nil then begin
    Exclamar('Usu�rio n�o encontrado');
    eCodigoUsuario.SetFocus;
    Exit;
  end;

  {$IFNDEF DEBUG}
    if Sessao.getCriptografia.Descriptografar(FUsuario[0].senha_sistema) <> eSenha.Text then begin
      Exclamar('A senha incorreta, verifique!');
      eSenha.SetFocus;
      Exit;
    end;
  {$ENDIF}

  ModalResult := mrOk;
end;

end.
