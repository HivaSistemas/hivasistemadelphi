unit CadastroDesenvolvimento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.Buttons, _Biblioteca,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, Vcl.ComCtrls, ComboBoxLuka, Data.DB,
  DBAccess, Ora, _CadastroDesenvolvimento, _Sessao, _RecordsEspeciais, _RecordsCadastros,
  Vcl.Mask, EditLukaData, _FrameHerancaPrincipal, FrameTextoFormatado,
  FrameEdicaoTexto, CheckBoxLuka, StaticTextLuka, _FrameHenrancaPesquisas,
  FrameClientes, FrameFuncionarios;

type
  TFormCadastroDesenvolvimento = class(TFormHerancaCadastroCodigo)
    Label3: TLabel;
    cbPrioridade: TComboBoxLuka;
    eDataCadastro: TEditLukaData;
    Label4: TLabel;
    lbl1: TLabel;
    cbTipoSolicitacao: TComboBoxLuka;
    lbl3: TLabel;
    stctxtlkcadastrado: TStaticTextLuka;
    lbl4: TLabel;
    stctxtlk2: TStaticTextLuka;
    lbl5: TLabel;
    stctxtlk3: TStaticTextLuka;
    lbl6: TLabel;
    stctxtlk4: TStaticTextLuka;
    frLeitura: TFrEdicaoTexto;
    FrEdicaoTexto: TFrEdicaoTexto;
    reAnalise: TRichEdit;
    reDesenvolvimento: TRichEdit;
    reTeste: TRichEdit;
    reDocumentacao: TRichEdit;
    reLiberacao: TRichEdit;
    FrClientes: TFrClientes;
    FrFuncionarios: TFrFuncionarios;
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroDesenvolvimento }

procedure TFormCadastroDesenvolvimento.BuscarRegistro;
var
  vDesenvolvimento: TArray<RecDesenvolvimento>;
begin
  vDesenvolvimento := _CadastroDesenvolvimento.BuscarDesenvolvimentos(Sessao.getConexaoBanco, 0, eId.AsInt);

  if vDesenvolvimento = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    Exit;
  end;

  inherited;
  cbPrioridade.SetIndicePorValor(vDesenvolvimento[0].prioridade);
  cbTipoSolicitacao.SetIndicePorValor(vDesenvolvimento[0].tipo_solicitacao);
  eID.AsInt            := vDesenvolvimento[0].desenvolvimento_id;
  eDataCadastro.AsData := vDesenvolvimento[0].data_cadastro;

  frLeitura.SetTexto(vDesenvolvimento[0].texto_historico);
  frLeitura.SomenteLeitura(True);

  SetTexto(reAnalise,vDesenvolvimento[0].texto_analise);
  SetTexto(reDesenvolvimento,vDesenvolvimento[0].texto_desenvolvimento);
  SetTexto(reTeste,vDesenvolvimento[0].texto_testes);
  SetTexto(reDocumentacao,vDesenvolvimento[0].texto_documentacao);
  SetTexto(reLiberacao,vDesenvolvimento[0].texto_liberacao);
  FrClientes.InserirDadoPorChave(vDesenvolvimento[0].clienteId);
  FrFuncionarios.InserirDadoPorChave(vDesenvolvimento[0].funcionarioId);

  if eID.Text <> '' then
  begin
    cbTipoSolicitacao.Enabled := False;
    FrEdicaoTexto.SomenteLeitura(vDesenvolvimento[0].status <> 'AA');
    SetarFoco(FrEdicaoTexto);
  end;

end;

procedure TFormCadastroDesenvolvimento.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _CadastroDesenvolvimento.ExcluirCadastroDesenvolvimento(Sessao.getConexaoBanco, eId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroDesenvolvimento.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  if not FrEdicaoTexto.Enabled then
  begin
    _Biblioteca.Exclamar('Solicita��o em andamento n�o pode ser editada nesta tela! Utilizar Acompanhamento de desenvolvimento.');
    Abort;
  end;

  FrEdicaoTexto.rtTexto.SelAttributes.style := [fsBold];
  FrEdicaoTexto.rtTexto.Lines.Insert(0,'Data: ' + DateToStr(Date));
  frLeitura.rtTexto.Lines.Insert(0,FrEdicaoTexto.rtTexto.Lines.Text);

  vRetBanco :=
    _CadastroDesenvolvimento.AtualizarCadastroDesenvolvimento(
      Sessao.getConexaoBanco,
      eID.AsInt,
      FrClientes.getCliente.cadastro_id,
      FrFuncionarios.GetFuncionario.funcionario_id,
      cbPrioridade.AsString,
      'EA',
      cbTipoSolicitacao.AsString,
      frLeitura.GetTexto,
      GetTexto(reAnalise),
      GetTexto(reDesenvolvimento),
      GetTexto(reTeste),
      GetTexto(reDocumentacao),
      GetTexto(reLiberacao)
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroDesenvolvimento.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    FrEdicaoTexto,
    FrClientes,
    FrFuncionarios,
    cbPrioridade,
    eDataCadastro,
    cbTipoSolicitacao,
    frLeitura,
    reAnalise,
    reDesenvolvimento,
    reTeste,
    reDocumentacao,
    reLiberacao],
    pEditando
  );

  _Biblioteca.Habilitar([eID], not pEditando);

  eDataCadastro.Enabled := False;
  
  frLeitura.SomenteLeitura(True);
  if pEditando then begin
    if eID.Text <> '' then
      cbTipoSolicitacao.Enabled := False
    else
      FrFuncionarios.InserirDadoPorChave(Sessao.getUsuarioLogado.funcionario_id);

    SetarFoco(cbTipoSolicitacao);
  end;
end;

procedure TFormCadastroDesenvolvimento.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormCadastroDesenvolvimento.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if cbPrioridade.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('� necess�rio informar a prioridade do desenvolvimento!');
    SetarFoco(cbPrioridade);
    Abort;
  end;

  if cbTipoSolicitacao.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('� necess�rio informar o tipo de solicitacao!');
    SetarFoco(cbTipoSolicitacao);
    Abort;
  end;

  if (Trim(FrEdicaoTexto.GetTexto) = '') and (eID.Text = '') then begin
    _Biblioteca.Exclamar('� necess�rio informar oque � para ser desenvolvido!');
    SetarFoco(FrEdicaoTexto);
    Abort;
  end;
end;

end.
