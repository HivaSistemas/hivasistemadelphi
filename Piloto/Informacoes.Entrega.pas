unit Informacoes.Entrega;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _Biblioteca,
  Vcl.Mask, EditLukaData, Vcl.StdCtrls, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Sessao,
  _Entregas, _EntregasItens, _RecordsExpedicao, SpeedButtonLuka;

type
  TFormInformacoesEntrega = class(TFormHerancaFinalizar)
    lb1: TLabel;
    lb2: TLabel;
    lb13: TLabel;
    lb17: TLabel;
    Label1: TLabel;
    lb4: TLabel;
    lb16: TLabel;
    eEntregaId: TEditLuka;
    eCliente: TEditLuka;
    eEmpresa: TEditLuka;
    stStatus: TStaticText;
    st3: TStaticText;
    eOrcamentoId: TEditLuka;
    eNotaFiscalId: TEditLuka;
    eDataHoraEntrega: TEditLukaData;
    eNomePessoaRetirou: TEditLuka;
    sgItens: TGridLuka;
    lb3: TLabel;
    eControleEntregaId: TEditLuka;
    sbInformacoesOrcamento: TSpeedButtonLuka;
    sbNotaFiscalId: TSpeedButtonLuka;
    sbInformacoesControle: TSpeedButtonLuka;
    lb5: TLabel;
    eUsuarioInicioSeparacao: TEditLuka;
    lb6: TLabel;
    eDataHoraInicioSeparacao: TEditLukaData;
    lb7: TLabel;
    eUsuarioFinalizouSeparacao: TEditLuka;
    lb8: TLabel;
    eDataHoraFinalizouSeparacao: TEditLukaData;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbNotaFiscalIdClick(Sender: TObject);
    procedure sbInformacoesControleClick(Sender: TObject);
    procedure sbInformacoesOrcamentoClick(Sender: TObject);
  end;

procedure Informar(pEntregaId: Integer);

implementation

{$R *.dfm}

uses
  Informacoes.Orcamento, InformacoesControleEntrega, Informacoes.NotaFiscal;

const
  coProdutoId  = 0;
  coNome       = 1;
  coMarca      = 2;
  coLote       = 3;
  coQuantidade = 4;
  coUnidade    = 5;
  coDevolvidos = 6;
  coRetornados = 7;
  coNaoSepar   = 8;

procedure Informar(pEntregaId: Integer);
var
  i: Integer;
  vForm: TFormInformacoesEntrega;
  vEntrega: TArray<RecEntrega>;
  vItens: TArray<RecEntregaItem>;
begin
  if pEntregaId = 0 then
    Exit;

  vEntrega := _Entregas.BuscarEntregas(Sessao.getConexaoBanco, 1, [pEntregaId]);
  if vEntrega = nil then begin
    _Biblioteca.Exclamar('Entrega n�o encontrada! ' + NFormat(pEntregaId));
    Exit;
  end;

  vItens := _EntregasItens.BuscarEntregaItens(Sessao.getConexaoBanco, 0, [pEntregaId]);

  vForm := TFormInformacoesEntrega.Create(nil);

  vForm.eEntregaId.SetInformacao(vEntrega[0].EntregaId);
  vForm.eCliente.SetInformacao(vEntrega[0].ClienteId, vEntrega[0].NomeCliente);
  vForm.eEmpresa.SetInformacao(vEntrega[0].EmpresaId, vEntrega[0].NomeEmpresa);
  vForm.eOrcamentoId.SetInformacao(vEntrega[0].OrcamentoId);
//  vForm.eNotaFiscalId.SetInformacao(vEntrega[0].NotaFiscalId);
  vForm.eControleEntregaId.SetInformacao(vEntrega[0].ControleEntregaId);
  vForm.stStatus.Caption := vEntrega[0].StatusAnalitico;
  vForm.eDataHoraEntrega.AsDataHora := vEntrega[0].DataHoraRealizouEntrega;
  vForm.eNomePessoaRetirou.Text := vEntrega[0].NomePessoaRecebeuEntrega;
  vForm.eUsuarioInicioSeparacao.SetInformacao(vEntrega[0].UsuarioInicioSeparaoId, vEntrega[0].NomeUsuarioInicioSep);
  vForm.eDataHoraInicioSeparacao.AsDataHora := vEntrega[0].DataHoraInicioSeparacao;
  vForm.eUsuarioFinalizouSeparacao.SetInformacao(vEntrega[0].UsuarioFinalizouSepId, vEntrega[0].NomeUsuarioFinSep);
  vForm.eDataHoraFinalizouSeparacao.AsDataHora := vEntrega[0].DataHoraFinalizouSep;

  for i := Low(vItens) to High(vItens) do begin
    vForm.sgItens.Cells[coProdutoId, i + 1]  := _Biblioteca.NFormat(vItens[i].ProdutoId);
    vForm.sgItens.Cells[coNome, i + 1]       := vItens[i].NomeProduto;
    vForm.sgItens.Cells[coMarca, i + 1]      := vItens[i].NomeMarca;
    vForm.sgItens.Cells[coLote, i + 1]       := vItens[i].Lote;
    vForm.sgItens.Cells[coQuantidade, i + 1] := _Biblioteca.NFormatEstoque(vItens[i].Quantidade);
    vForm.sgItens.Cells[coUnidade, i + 1]    := vItens[i].Unidade;
    vForm.sgItens.Cells[coDevolvidos, i + 1] := _Biblioteca.NFormatNEstoque(vItens[i].Devolvidos);
    vForm.sgItens.Cells[coRetornados, i + 1] := _Biblioteca.NFormatNEstoque(vItens[i].Retornados);
    vForm.sgItens.Cells[coNaoSepar, i + 1]   := _Biblioteca.NFormatNEstoque(vItens[i].NaoSeparados);
  end;
  vForm.sgItens.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  vForm.ShowModal;

  vForm.Free;
end;


procedure TFormInformacoesEntrega.sbInformacoesControleClick(Sender: TObject);
begin
  inherited;
  InformacoesControleEntrega.Informar( eControleEntregaId.IdInformacao );
end;

procedure TFormInformacoesEntrega.sbInformacoesOrcamentoClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar( eOrcamentoId.IdInformacao );
end;

procedure TFormInformacoesEntrega.sbNotaFiscalIdClick(Sender: TObject);
begin
  inherited;
  Informacoes.NotaFiscal.Informar( eNotaFiscalId.IdInformacao );
end;

procedure TFormInformacoesEntrega.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coProdutoId, coQuantidade, coDevolvidos, coRetornados, coNaoSepar] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
