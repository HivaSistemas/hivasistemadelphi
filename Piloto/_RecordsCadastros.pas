unit _RecordsCadastros;

interface

uses
  System.Classes;

type
  RecAtalhosSistema = record
    atalho_sistema_id: Integer;
    atalho: string;
    descricao: string;
  end;

  RecDesenvolvimento = record
    desenvolvimento_id: Integer;
    clienteId: Integer;
    nomeCliente: string;
    funcionarioId: Integer;
    nomeFuncionario: string;
    prioridade: string;
    data_cadastro: TDateTime;
    versao: string;
    status: string;
    tipo_solicitacao:string;
    texto_historico: string;
    texto_analise: string;
    texto_desenvolvimento: string;
    texto_testes: string;
    texto_documentacao: string;
    texto_liberacao: string;
  end;

  RecEstados = class
    estado_id: string;
    nome: string;
    codigo_ibge: Integer;
    PercIcmsInterno: Double;
    ativo: string;
  end;

  RecCidades = class
    cidade_id: Integer;
    nome: string;
    codigo_ibge: Integer;
    estado_id: string;
    TipoBloqueioVendaEntregar: string;
    ativo: string;
    valor_calculo_frete_km: Integer;
  end;

  RecBairros = class
    bairro_id: Integer;
    nome: string;
    cidade_id: Integer;
    rota_id: Integer;
    ativo: string;
    TipoBloqueioVendaEntregar: string;
    nome_cidade: string;
    EstadoId: string;
    nome_rota: string;
    CodigoIbgeCidade: Integer;
  end;

  RecCRM = class
    central_id: Integer;
    cadastro_id: Integer;
    observacao: string;
  end;

  RecCRMAcessoRemoto = record
    central_id: Integer;
    item_id: Integer;
    ip: string;
    senha: string;
    observacao: string;
    usuario: string;
  end;

  RecCRMBancoDados = record
    central_id: Integer;
    item_id: Integer;
    ip_servidor: string;
    senha: string;
    usuario: string;
    porta: string;
    servico: string;
  end;

  RecCRMOcorrencias = record
    ocorrencia_id: Integer;
    cadastro_id: Integer;
    dataCadastro: TDateTime;
    dataConclusao: TDateTime;
    descricao: string;
    solucao: string;
    status: string;
    status_analitico: string;
    prioridade: string;
    prioridade_analitico: string;
    solicitacao: Integer;
  end;

  RecContas = class
    Nome: string;
    EmpresaId: Integer;
    NomeEmpresa: string;
    Tipo: Byte;
    CodigoBanco: string;
    Agencia: string;
    DigitoAgencia: string;
    Conta: string;
    DigitoConta: string;
    Ativo: string;
    AceitaSaldoNegativo: string;
    SaldoAtual: Currency;
    SaldoConciliadoAtual: Currency;
    EmiteCheque: string;
    NumeroUltimoChequeEmitido: Integer;
    EmiteBoleto: string;
    NossoNumeroInicial: Integer;
    NossoNumeroFinal: Integer;
    ProximoNossoNumero: Integer;
    Carteira: string;
    NumeroRemessaBoleto: Integer;
    CodigoEmpresaCobranca: string;
  end;

  RecTiposCobranca = class
    CobrancaId: Integer;
    Nome: string;
    taxa_retencao_mes: Double;
    rede_cartao: string;
    tipo_cartao: string;
    forma_pagamento: string;
    TipoPagamentoComissao: string;
    PercentualComissao: Double;
    tipo_recebimento_cartao: string;
    UtilizarLimiteCredCliente: string;
    BoletoBancario: string;
    PermitirPrazoMedio: string;
    ativo: string;
    PortadorId: string;
    NomePortador: string;
    IncidenciaFinanceira: string;
    PermitirVinculoAutTEF: string;
    RedeRetornoTEF: string;
    BandeiraRetornoTEF: string;
    PrazoMedio: Double;
    AdministradoraCartaoId: Integer;
    BandeiraCartaoNFe: string;
    EmitirDuplicMercantilVenda: string;
  end;

  RecCondicoesPagamento = class
    condicao_id: Integer;
    nome: string;
    tipo: string;
    indice_acrescimo: Currency;
    valor_minimo_venda: Double;
    ValorMinimoParcela: Double;
    PercentualDescontoPermVenda: Double;
    ativo: string;
    PrazoMedio: Integer;
    PercentualJuros: Double;
    PercentualEncagos: Double;
    PercentualDescontoComercial: Double;
    PercentualCustoVenda: Double;
    NaoPermitirConsFinal: string;
    ExigirModeloNotaFiscal: string;
    PermitirPrecoPromocional: string;
    AplicarIndPrecoPromocional: string;
    PermitirUsoPontaEstoque: string;
    PermitirUsoFechamentoAcumu: string;
    TipoPreco: string;
    Restrita: string;
    RecebimentoNaEntrega: string;
    Acumulativo: string;
    ImprimirConfissaoDivida: string;
    ImprimirCompFaturamentoVenda: string;
    BloquearVendaSempre: string;
    ExportarWeb: string;
    function getIndiceAcrescimoPrecoPromocional: Double;
  end;

  RecTiposCobrancaCondicaoPagamentos = record
    condicao_id: Integer;
    cobranca_id: Integer
  end;

  RecCargosFuncionario = class
    CargoId: Integer;
    nome: string;
    ativo: string;
  end;

  RecFuncionarios = class(TComponent)
  public
    funcionario_id: Integer;
    nome: string;
    senha_sistema: string;
    apelido: string;
    data_cadastro: TDateTime;
    data_nascimento: TDateTime;
    CargoId: Integer;
    cpf: string;
    rg: string;
    orgao_expedidor_rg: string;
    data_admissao: TDateTime;
    logradouro: string;
    complemento: string;
    numero: string;
    bairro_id: Integer;
    cep: string;
    e_mail: string;
    servidor_smtp: string;
    usuario_e_mail: string;
    senha_e_mail: string;
    porta_e_mail: Integer;
    cadastro_id: Integer;
    foto: TMemoryStream;
    PercentualDescontoAdicVenda: Double;
    PercentualDescontoAdicFinan: Double;
    percentual_comissao_a_vista: Double;
    percentual_comissao_a_prazo: Double;
    PercDescItemPrecoManual: Double;
    salario: Double;
    ativo: string;
    Vendedor: string;
    Comprador: string;
    Motorista: string;
    Ajudante: string;
    NumeroCarteiraTrabalho: string;
    SerieCarteiraTrabalho: string;
    PisPasep: string;
    DataDesligamento: TDateTime;
    Caixa: string;
    DataUltimaSenha: TDateTime;
    nome_funcao: string;
    FinalizarSessao: string;
    DevolucaoSomenteFiscal: string;
    AcessaAltisW: string;
    SenhaAltisW: string;
    GerenteSistema: Boolean;
    AcessoHivaMobile: string;
  end;

  RecCFOPs = class
    CfopId: string;
    Flutuacao: Integer;
    CfopPesquisaId: string;
    CfopCorrespondenteId: string;
    nome: string;
    descricao: string;
    ativo: string;
    PlanoFinanceiroEntradaId: string;
    EntradaMercadorias: string;
    EntradaMercadoriasRevenda: string;
    EntradaMercUsoConsumo: string;
    EntradaMercadoriasBonific: string;
    EntradaServicos: string;
    EntradaConhecimentoFrete: string;
    NaoExigirItensEntrada: string;
  end;

  RecLocaisProdutos = class
    local_id: Integer;
    nome: string;
    tipo_local: string;
    tipo_local_analitico: string;
    permite_devolucao_venda: string;
    ativo: string;
    empresa_direcionar_nota: Integer;
  end;

  RecCadastros = class
    cadastro_id: Integer;
    nome_fantasia: string;
    razao_social: string;
    tipo_pessoa: string;
    tipo_cliente: string;
    cpf_cnpj: string;
    logradouro: string;
    complemento: string;
    numero: string;
    bairro_id: Integer;
    ponto_referencia: string;
    cep: string;
    TelefonePrincipal: string;
    TelefoneCelular: string;
    TelefoneFax: string;
    e_mail: string;
    sexo: string;
    estado_civil: string;
    data_nascimento: TDateTime;
    data_cadastro: TDateTime;
    inscricao_estadual: string;
    cnae: string;
    rg: string;
    orgao_expedidor_rg: string;
    e_cliente: string;
    e_fornecedor: string;
    EMotorista: string;
    ETransportadora: string;
    EProfissional: string;
    ativo: string;
    estado_id: string;
    NomeBairro: string;
    NomeCidade: string;
    Observacoes: string;
    CodigoIBGECidade: Integer;
  end;

  RecRelacaoCadastros = record
    CadastroId: Integer;
    NomeFantasia: string;
    RazaoSocial: string;
    TipoPessoa: string;
    TipoCliente: string;
    CpfCnpj: string;
    Logradouro: string;
    Complemento: string;
    Numero: string;
    BairroId: Integer;
    PontoReferencia: string;
    Cep: string;
    TelefonePrincipal: string;
    TelefoneCelular: string;
    TelefoneFax: string;
    EMail: string;
    Sexo: string;
    EstadoCivil: string;
    DataNascimento: TDateTime;
    DataCadastro: TDateTime;
    UsuarioSessaoId: Integer;
    NomeUsuario: string;
    InscricaoEstadual: string;
    Cnae: string;
    Rg: string;
    OrgaoExpedidorRg: string;
    ECliente: string;
    EFornecedor: string;
    EMotorista: string;
    ETransportadora: string;
    EProfissional: string;
    AtivoAnalitico: string;
    AtivoSintetico: string;
    EstadoId: string;
    NomeBairro: string;
    NomeCidade: string;
    Observacoes: string;
    CodigoIBGECidade: Integer;
  end;

  RecClientes = class
    cadastro_id: Integer;
    RecCadastro: RecCadastros;
    tipo_cliente: string;
    codigo_spc: string;
    data_pesquisa_spc: TDateTime;
    atendente_spc: string;
    restricao_spc: string;
    data_restricao_spc: TDateTime;
    local_spc: string;
    data_pesquisa_serasa: TDateTime;
    atendente_serasa: string;
    restricao_serasa: string;
    data_restricao_serasa: TDateTime;
    local_serasa: string;
    condicao_id: Integer;
    vendedor_id: Integer;
    emitir_somente_nfe: string;
    NaoGerarComissao: string;
    ExigirNotaFaturamento: string;
    BloquearVendas: string;
    ExigirModeloNotaFiscal: string;
    IgnorarRedirRegraNotaFisc: string;
    TipoPreco: string;
    DataValidadeLimiteCredito: TDateTime;
    QtdeDiasVencimentoAcumulado: Integer;
    IndiceDescontoVendaId: Integer;
    ListaNegraId: Integer;
    GrupoClienteId: Integer;
    ObservacoesListaNegra: string;
    ativo: string;
    IgnorarBloqueiosVenda: string;
    ObservacoesVenda: string;
    RegimeEmpresa: string;
    UsuarioBanco: string;
    SenhaBanco: string;
    IpServidor: string;
    ServicoOracle: string;
    QtdEstacoes: string;
    ResponsavelEmp: string;
    ValorAdesao: string;
    ValorMens: string;
    DataAdesao: TDateTime;
    DataVenc: String;
    Porta: String;
    LimiteCredito: Double;
    LimiteCreditoMensal: Double;
    DataAprovacaoLimiteCredito: TDateTime;
    UsuarioAprovLimiteCredId: Integer;
    CalcularIndiceDeducao: string;
    Validade: string;
    ExportarWeb: string;
  end;

  RecDiversosEnderecos = record
    cadastro_id: Integer;
    ordem: Integer;
    tipo_endereco: string;
    TipoEnderecoAnalitico: string;
    logradouro: string;
    complemento: string;
    numero: string;
    bairro_id: Integer;
    nome_bairro: string;
    nome_cidade: string;
    ponto_referencia: string;
    cep: string;
    InscricaoEstadual: string;
  end;

  RecDiversosContatos = record
    cadastro_id: Integer;
    ordem: Integer;
    descricao: string;
    telefone: string;
    ramal: Integer;
    celular: string;
    e_mail: string;
    observacao: string;
  end;

  RecFornecedores = class
    cadastro_id: Integer;
    cadastro: RecCadastros;
    super_simples: string;
    ativo: string;
    data_cadastro: TDateTime;
    observacoes: string;
    tipo_fornecedor: string;
    Revenda: string;
    UsoConsumo: string;
    AtivoImobilizado: string;
    Servico: string;
    PlanoFinanceiroRevenda: string;
    PlanoFinanceiroUsoConsumo: string;
    PlanoFinanceiroServico: string;
    TipoCobrancaEntradaNfe: string;
    TipoRateioFrete: string;
    GrupoFornecedorId: Integer;
  end;

  RecUnidades = class
    unidade_id: string;
    descricao: string;
    ativo: string;
  end;

  RecMarcas = class
    marca_id: Integer;
    nome: string;
    ativo: string;
  end;

  RecProdutos = class
    produto_id: Integer;
    data_cadastro: TDateTime;
    codigo_ncm: string;
    marca_id: Integer;
    fornecedor_id: Integer;
    multiplo_venda: Double;
    multiplo_venda_2: Double;
    multiplo_venda_3: Double;
    peso: Double;
    volume: Double;
    unidade_venda: string;
    bloqueado_compras: string;
    bloqueado_vendas: string;
    permitir_devolucao: string;
    caracteristicas: string;
    ativo: string;
    produtoDiversosPDV: string;
    nome_compra: string;
    cest: string;
    aceitar_estoque_negativo: string;
    LinhaProdutoId: string;
    codigo_barras: string;
    codigoBalanca: string;
    nome: string;
    foto_1: TMemoryStream;
    foto_2: TMemoryStream;
    foto_3: TMemoryStream;
    emitir_somente_nfe: string;
    exportar_web: string;

    inativar_zerar_estoque: string;
    nome_fabricante: string;
    nome_marca: string;

    ProdutoPaiId: Integer;
    QuantidadeVezesPai: Double;
    TipoControleEstoque: string;

    ExigirDataFabricacaoLote: string;
    ExigirDataVencimentoLote: string;

    SobEncomenda: string;
    Revenda: string;
    UsoConsumo: string;
    AtivoImobilizado: string;
    Servico: string;

    SujeitoComissao: string;
    BloquearEntradSemPedCompra: string;
    NaoExigirModeloNotaFiscal: string;
    ExigirSeparacao: string;
    DiasAvisoVencimento: Integer;
    MotivoInatividade: string;
    CodigoOriginalFabricante: string;
    TipoDefAutomaticaLote: string;
    UnidadeEntregaId: string;
    ConfSomenteCodigoBarras: string;
    ValorAdicionalFrete: Double;
    PercentualComissaoVista: Double;
    PercentualComissaoPrazo: Double;
    ControlaPeso: string;
    GrupoTribEstadualVendaId: Integer;
    GrupoTribEstadualVendaNome: string;
    LinhaProdutoNome: string;
    ChaveImportacao: string;

    function EProdutoFilho: Boolean;
  end;

  RecProdutosRelacionados = record
    ProdutoPrincipalId: Integer;
    ProdutoRelacionadoId: Integer;
    QuantidadeSugestao: Double;
    QuantidadeAdicionar: Double;
    NomeProdutoRelacionado: string;
    TipoControleEstoque: string;
  end;

  RecEmpresas = class(TComponent)
  public
    EmpresaId: Integer;
    CadastroId: Integer;
    BairroId: Integer;
    CidadeId: Integer;
    Logradouro: string;
    Complemento: string;
    Numero: string;
    PontoReferencia: string;
    Cep: string;
    TelefonePrincipal: string;
    TelefoneFax: string;
    EMail: string;
    DataFundacao: TDateTime;
    InscricaoEstadual: string;
    InscricaoMunicipal: string;
    Cnae: string;
    Cnpj: string;
    NomeFantasia: string;
    NomeResumidoImpressoesNF: string;
    Ativo: string;
    RazaoSocial: string;
    TipoEmpresa: string;
    EstadoId: string;
    NomeBairro: string;
    NomeCidade: string;
    CodigoIBGECidade: Integer;
    CodigoIBGEEstado: Integer;
    DataValidade: string;
    DataUltimaConsultaAPI: TDateTime;
  end;

  RecPrecosProdutos = class
    produto_id: Integer;
    empresa_id: Integer;
    perc_comissao_a_vista: Double;
    perc_comissao_a_prazo: Double;
    preco_atacado_1: Double;
    quantidade_min_preco_varejo: Double;
    quantidade_minima_pdv: Double;
    preco_pdv: Double;
    quantidade_min_preco_atac_3: Double;
    preco_varejo: Double;
    quantidade_min_preco_atac_1: Double;
    preco_atacado_2: Double;
    quantidade_min_preco_atac_2: Double;
    preco_atacado_3: Double;
    data_preco_atacado_3: TDateTime;
    data_preco_atacado_1: TDateTime;
    data_preco_atacado_2: TDateTime;
    data_preco_pdv: TDateTime;
    data_preco_varejo: TDateTime;
  end;

  RecTiposCobrancaLiberadosCondicao = record
    tipo_cobranca_id: Integer;
    nome_tipo_cobranca: string;
    TipoCartao: string;
    TipoCartaoAnalitico: string;
    rede_cartao: string;
    TipoRecebimentoCartao: string;
    UtilizarLimiteCredCliente: string;
  end;

  RecECF = class
    EcfId: Integer;
    ModeloEcf: Integer;
    empresa_id: Integer;
    nome_forma_pagamento_pos: string;
    nome_forma_pagamento_cartao: string;
    nome_forma_pagamento_cheque: string;
    nome_forma_pagamento_credito: string;
    nome_forma_pagamento_dinheiro: string;
    nome_forma_pagamento_cobranca: string;
    ativo: string;
    computador_ecf: string;
    nome_modelo_ecf: string;
    porta_serial: Integer;
    PercentualAliquota1: Double;
    PercentualAliquota2: Double;
    PercentualAliquota3: Double;
    PercentualAliquota4: Double;
    PercentualAliquota5: Double;
    PercentualAliquota6: Double;
    PercentualAliquota7: Double;
    PercentualAliquota8: Double;
    GerenciadorCartaoId: Integer;
    CodigoFabrica: string;
    NumeroEcf: Integer;
    IndiceRecebimentoNaoFiscal: Integer;
    DiretorioArquivosRequisicoes: string;
    DiretorioArquivosRespostas: string;
  end;

  RecTipoCobrancaDiasPrazo = record
    CobrancaId: Integer;
    Dias: Integer;
  end;

  RecParametrosEmpresa = class
    EmpresaId: Integer;
    UtilizaNfe: string;
    ValorMinDifTurnoContasRec: Double;
    ExigirNomeConsFinalVenda: string;
    PermiteAltFormaPagtoCaixa: string;
    PermAltFormaPagtoCxFinanc: string;
    NumeroUltNfeEmitida: Integer;
    CondicaoPagamentoPDV: Integer;
    SerieNFe: Integer;
    tipoAmbienteNFe: string;
    UltimoNsuConsultadoDistNFe: string;
    UltimoNsuConsultadoDistCTe: string;
    UtilizaNFCe: string;
    SerieNFCe: Integer;
    tipoAmbienteNFCe: string;
    infComplDocsEletronicos: string;
    NumeroUltNfceEmitida: Integer;
    CorLucratividade1: Integer;
    CorLucratividade2: Integer;
    CorLucratividade3: Integer;
    CorLucratividade4: Integer;
    RegimeTributario: string;
    AliquotaSimplesNacional: Double;
    AliquotaPIS: Double;
    AliquotaCOFINS: Double;
    AliquotaCSLL: Double;
    AliquotaIRPJ: Double;
    CondPagtoPadraoPesqVenda: Integer;

    TrabalhaRetirarAto: string;
    TrabalhaRetirar: string;
    TrabalhaEntregar: string;
    TrabalhaSemPrevisao: string;
    TrabalhaSemPrevisaoEntregar: string;
    ConfirmarSaidaProdutos: string;
    TrabalharControleSeparacao: string;
    TipoPrecoUtilizarVenda: string;
    CondicaoPagtoPadVendaAssis: Integer;
    CondicaoPagamentoConsProd2: Integer;
    NomeCondPagamentoCons2: string;
    IndiceCondPagtoCons2: Double;
    CondicaoPagamentoConsProd3: Integer;
    NomeCondPagamentoCons3: string;
    IndiceCondPagtoCons3: Double;
    CondicaoPagamentoConsProd4: Integer;
    NomeCondPagamentoCons4: string;
    IndiceCondPagtoCons4: Double;
    IndiceSabado: Double;
    IndiceDomingo: Double;
    ImprimirLoteCompEntrega: string;
    ImprimirCodOriginalCompEnt: string;
    ImprimirCodBarrasCompEnt: string;
    TipoCustoVisLucroVenda: string;
    QtdeDiasValidadeOrcamento: Integer;
    PercentualJurosMensal: Double;
    PercentualMulta: Double;
    TipoRedirecionamentoNota: string;
    ObrigarVendedorSelTipoCobr: string;
    EmpresaRedNotaRetirarAtoId: Integer;
    EmpresaRedNotaRetirarId: Integer;
    EmpresaRedNotaEntregarId: Integer;
    ValorFretePadraoVenda: Double;
    QuantidadeMinDiasEntrega: Integer;
    LogoEmpresa: TMemoryStream;
    TipoCustoAjusteEstoque: string;
    TrabalharConferenciaRetAto: string;
    TrabalharConferenciaRetirar: string;
    TrabalharConferenciaEntrega: string;
    PermBaixarEntRecPendente: string;
    NumeroEstabelecimentoCielo: string;
    ExirgirDadosCatao: string;
    GerarNfAgrupada: string;
    PermitirDevolucaoAposPrazo: string;
    QtdeDiasBloqVendaTitVenc: Integer;
    CalcularFreteVendaPorKM: string;
    HabilitarRecebimentoPix: string;
    BaixaContasReceberPixTurnoAberto: string;
    InfComplCompPagamentos: string;
    AlertaEntregaPendente: string;
    SerieCertificado: string;
    SenhaCertificado: string;
    ImpressoraNFe: string;
    Certificado: TMemoryStream;
    CscNFce: string;
    IdToken: string;
    ModeloImpressora: string;
  end;

  RecProdutoEntrada = class
    produto_id: Integer;
    nome: string;
    marca: string;
    metodo_encontrou: string;
  end;

  RecGerenciadorCartaoTEF = class
    gerenciador_cartao_id: Integer;
    ativo: string;
    diretorio_arquivos_respostas: string;
    nome: string;
    diretorio_arquivos_requisicoes: string;
    TextoProcurarBandeira1: string;
    PosicaoInicialProcBand1: Integer;
    QtdeCaracteresCopiarBand1: Integer;
    TextoProcurarBandeira2: string;
    PosicaoInicialProcBand2: Integer;
    QtdeCaracteresCopiarBand2: Integer;
    TextoProcurarRede1: string;
    PosicaoInicialProcRede1: Integer;
    QtdeCaracteresCopiarRede1: Integer;
    TextoProcurarRede2: string;
    PosicaoInicialProcRede2: Integer;
    QtdeCaracteresCopiarRede2: Integer;
    TextoProcurarNumeroCartao1: string;
    PosicIniProcNumeroCartao1: Integer;
    QtdeCaracCopiarNrCartao1: Integer;
    TextoProcurarNumeroCartao2: string;
    PosicIniProcNumeroCartao2: Integer;
    QtdeCaracCopiarNrCartao2: Integer;
    TextoProcurarNsu1: string;
    PosicaoIniProcurarNsu1: Integer;
    QtdeCaracCopiarNsu1: Integer;
    TextoProcurarNsu2: string;
    PosicaoIniProcurarNsu2: Integer;
    QtdeCaracCopiarNsu2: Integer;
    TextoProcurarCodAutoriz1: string;
    PosicaoIniProCodAutoriz1: Integer;
    QtdeCaracCopCodAutoriz1: Integer;
    TextoProcurarCodAutoriz2: string;
    PosicaoIniProCodAutoriz2: Integer;
    QtdeCaracCopCodAutoriz2: Integer;
  end;

  RecMotivoAjusteEstoque = class
    motivo_ajuste_id: Integer;
    descricao: string;
    PlanoFinanceiroId: string;
    ativo: string;
    IncideFinanceiro: string;
  end;

  RecEnderecosEstoque = class
    endereco_id: Integer;
    rua_id: Integer;
    nome_rua: string;
    modulo_id: Integer;
    nome_modulo: string;
    nivel_id: Integer;
    nome_nivel: string;
    vao_id: Integer;
    nome_vao: string;
  end;

  RecEnderecosEstoqueProduto = record
    posicao: Integer;
    produto_id: Integer;
    endereco_id: Integer;
  end;

  RecEnderecoEstoqueRua = class
    rua_id: Integer;
    descricao: string;
  end;

  RecEnderecoEstoqueModulo = class
    modulo_id: Integer;
    descricao: string;
  end;

  RecEnderecoEstoqueNivel = class
    nivel_id: Integer;
    descricao: string;
  end;

  RecEnderecoEstoqueVao = class
    vao_id: Integer;
    descricao: string;
  end;

  RecTipoAcompanhamentoOrcamento = class
    tipo_acompanhamento_id: Integer;
    descricao: string;
    ativo: string;
  end;

  RecTipoBloqueioEstoque = class
    tipo_bloqueio_id: Integer;
    descricao: string;
    ativo: string;
  end;

  RecModelosECF = record
    modelo_id: Integer;
    modelo_ecf: string;
  end;

  RecParametros = record
    cadastro_consumidor_final_id: Integer;
    iniciar_venda_consumidor_final: string;
    UtilizarAnaliseCusto: string;
    QtdeMensagensObrigarLeitura: Integer;
    QuantidadeDiasValidadeSenha: Integer;
    MotivoAjuEstBxCompraId: Integer;
    TipoCobrancaGeracaoCredId: Integer;
    TipoCobrancaGerCredImpId: Integer;
    TipoCobRecebEntregaId: Integer;
    TipoCobAcumulativoId: Integer;
    TipoCobAdiantamentoAcuId: Integer;
    TipoCobAdiantamentoFinId: Integer;
    TipoCobFechamentoTurnoId: Integer;
    TipoCobEntradaNfeXmlId: Integer;
    BloquearCreditoDevolucao: string;
    BloquearCreditoPagarManual: string;
    UtilModuloPreEntradas: string;
    QtdeSegTravarAltisOcioso: Integer;
    QtdeMaximaDiasDevolucao: Integer;
    DefinirLocalManual: string;
    PreencherLocaisAutomaticamente: string;
    SomenteLocaisEstoquePositivo: string;
    AplicarIndiceDeducao: string;
    EntradaFutura: string;
    UtilVendaRapida: string;
    UtilMarketPlace: string;
    EmitirNotaAcumuladoFechamentoPedido: string;
    UtilEnderecoEstoque: string;
    UtilControleEntrega: string;
    ImprimirListaSeparacaoVenda: string;
    UtilizaDirecionamentoPorLocais: string;
    QuantidadeViasReciboPagamento: Integer;
    UtilConfirmacaoTransferenciaLocais: string;
    AtualizarPrecoVendaEntradaNota: string;
    DeletarNFPendenteEmissao: string;
    SmtpServidor: string;
    SmtpUsuario: string;
    SmtpSenha: string;
    SmtpPorta: integer;
    SmtpAutenticacao: string;
    SmtpReqAutenticacao: string;
    AtualizarCustoProdutosAguardandoChegada: string;
    CalcularComissaoAcumuladoRecFin: string;
    CalcularComissaoAcumuladoRecFinData: TDateTime;
    HabilitarDescontoItemImpresaoOrcamento: string;
    AjusteEstoqueReal: string;
    SenhaPedidosBloqueados: string;
    CalcularComissaoAcumuladoRecPedido: string;
    CalcularAdicionalComissaoPorMetaFuncionario: string;
    UtilizaControleManifesto: string;
    OrcamentoAmbiente: string;
    TipoComprovante: string;
    UtilizaEmissaoBoleto: string;
    UtilizaDRE: string;
    UrlIntegracaoWeb: string;
    DataUltimaIntegracaoWeb: TdateTime;
    TokenIntegracaoWeb: string;
    IntervalorIntegracaoWeb: integer;
    PortaIntegracaoWeb: integer;

  end;

  RecCamposClientes = record
    id: Integer;
    descricao: string;
    tipo_cliente: string;
  end;

  RecCamposObrigatoriosClientes = record
    id: Integer;
    tipo_cliente: string;
    nome: string;
    descricao: string;
  end;

  RecAutorizacoes = record
    funcionario_id: Integer;
    tipo: string;
    id: string;
  end;

  RecClientesCondicPagtoRestrit = record
    ClienteId: Integer;
    CondicaoId: Integer;
  end;

  RecCST = record
    CST: string;
    Descricao: string;
  end;

  RecMensagem = record
    MensagemId: Integer;
    AgrupadorMensagemId: Integer;
    Assunto: string;
    Mensagem: string;
    UsuarioEnvioMensagemId: Integer;
    NomeUsuarioEnvio: string;
    UsuarioDestinoMensagemId: Integer;
    NomeUsuarioDestino: string;
    DataHoraEnvio: TDateTime;
    Respondida: string;
    PermitirResposta: string;
    GrupoMensagem: string;
    Lida: string;
  end;

  RecCustoProduto = record
    ProdutoId: Integer;
    EmpresaId: Integer;
    CustoCompraComercial: Double;
    PrecoFinal: Double;
    PrecoFinalMedio: Double;
    PrecoLiquido: Double;
    PrecoLiquidoMedio: Double;
    CustoAjusteEstoque: Double;
  end;

  RecFaixaSimplesNacional = record
    Faixa: Integer;
    Descricao: string;
    Percentual: Double;
  end;

  RecProdutoImpostosCalculados = record
    ProdutoId: Integer;
    ValorTotalProdutos: Currency;
    ValorOutrasDesp: Currency;
    ValorDesconto: Currency;

    ValorICMS: Currency;

    BaseCalculoPis: Currency;
    PercentualPIS: Currency;
    ValorPIS: Currency;

    BaseCalculoCofins: Currency;
    PercentualCofins: Currency;
    ValorCOFINS: Currency;

    ValorIPI: Currency;
  end;

  RecProdutoDeptoSecaoLinha = class
    DeptoSecaoLinhaId: string;
    DeptoSecaoLinhaPaiId: string;
    Nome: string;
    Ativo: string;

    EstruturaPai: string;
    Estrutura: string;

    DepartamentoId: string;
    NomeDepartamento: string;
    SecaoId: string;
    NomeSecao: string;
    LinhaId: string;
    NomeLinha: string;
  end;

  RecProdutoMultiploCompra = record
    ProdutoId: Integer;
    UnidadeId: string;
    Multiplo: Double;
    QuantidadeEmbalagem: Double;
    Ordem: Integer;
  end;

  RecCstPisCofins = record
    Cst: string;
    Descricao: string;
    CalcularBase: string;
    Tipo: string;
  end;

  RecTipoCobrancaBancoBoleto = Record
    CobrancaId: Integer;
    BancoId: Integer;
    NomeBanco: string;
    EmpresaId: Integer;
    NomeEmpresa: string;
    Conta: string;
    DigitoConta: string;
  end;

  RecLogsAlteracoes = record
    tipoAlteracaoLogId: Integer;
    campo: string;
    valorAnterior: string;
    novoValor: string;
    usuarioAlteracao: string;
    dataHoraAlteracao: TDateTime;
    rotinaAlteracao: string;
    EmpresaId: Integer;
    NomeEmpresa: string;
  end;

  RecProdutosAmbiente = record
    ProdutoId: Integer;
    NomeProduto: string;
    Marca: string;
    Quantidade: Double;
    UndVenda: string;
    PrecoUnitari: Double;
    PercDescPrecoManual: Double;
    ValorDescontoPrecoMan: Double;
    FreteCobrar: Double;
    Estoque: Double;

    (* Colunas ocultas *)
    MultiploVenda: Double;
    ValorTotalOutDespProp: Double;
    ValorTotalDescProp: Double;
    TipoControleEstoq: string;
    PrecoVarejo: Double;
    PrecoPromocional: Double;
    PrecoAtacado1: Double;
    QtdeMinAtacado1: Double;
    PrecoAtacado2: Double;
    QtdeMinAtacado2: Double;
    PrecoAtacado3: Double;
    QtdeMinAtacado3: Double;
    TipoPrecoUtilizar: string;
    PrecoBaseUtilizado: Double;
    ExisteVariosPrecos: string;
    FreteProduto: Double;
    ValorTotalFreteProp: Double;
    PrecoPontaEst: Double;
    ValorTotalProduto: Double;
    PrecoManual: Double;
  end;

  RecAmbientes = record
    NomeAmbiente: string;
    Produtos: TArray<RecProdutosAmbiente>;
  end;

  RecFaixaComissao = record
    ValorInicial: Double;
    ValorFinal: Double;
    Percentual: Double;
  end;

implementation

{ RecProdutos }

function RecProdutos.EProdutoFilho: Boolean;
begin
  Result := produto_id <> ProdutoPaiId;
end;

{ RecCondicoesPagamento }

function RecCondicoesPagamento.getIndiceAcrescimoPrecoPromocional: Double;
begin
  Result := 1;
  if AplicarIndPrecoPromocional = 'N' then
    Exit;

  Result := indice_acrescimo * (1 - PercentualDescontoComercial * 0.01);
end;

end.
