unit Buscar.ProdutosRelacionados;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _RecordsCadastros,
  Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca, _ProdutosRelacionados, System.Math;

type
  TFormBuscarProdutosRelacionados = class(TFormHerancaFinalizar)
    sgProdutosRelacionados: TGridLuka;
    procedure sgProdutosRelacionadosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosRelacionadosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgProdutosRelacionadosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sgProdutosRelacionadosArrumarGrid(Sender: TObject; ARow,
      ACol: Integer; var TextCell: string);
  end;

function Buscar(const pProdutoPrincipalId: Integer): TArray<RecProdutosRelacionados>;

implementation

{$R *.dfm}

const
  coProdutoId      = 0;
  coNome           = 1;
  coQuantidadeSug  = 2;
  coQuantidadeAdic = 3;

function Buscar(const pProdutoPrincipalId: Integer): TArray<RecProdutosRelacionados>;
var
  i: Integer;
  vForm: TFormBuscarProdutosRelacionados;
  vProdutosRelacionados: TArray<RecProdutosRelacionados>;
begin
  Result := nil;
  vProdutosRelacionados := _ProdutosRelacionados.BuscarProdutosRelacionados(Sessao.getConexaoBanco, 1, [pProdutoPrincipalId, Sessao.getParametrosEmpresa.EmpresaId]);
  if vProdutosRelacionados = nil then
    Exit;

  vForm := TFormBuscarProdutosRelacionados.Create(Application);

  for i := Low(vProdutosRelacionados) to High(vProdutosRelacionados) do begin
    vForm.sgProdutosRelacionados.Cells[coProdutoId, i + 1]      := NFormat(vProdutosRelacionados[i].ProdutoRelacionadoId);
    vForm.sgProdutosRelacionados.Cells[coNome, i + 1]           := vProdutosRelacionados[i].NomeProdutoRelacionado;
    vForm.sgProdutosRelacionados.Cells[coQuantidadeSug, i + 1]  := NFormatN(vProdutosRelacionados[i].QuantidadeSugestao, 4);
    vForm.sgProdutosRelacionados.Cells[coQuantidadeAdic, i + 1] := NFormatN(vProdutosRelacionados[i].QuantidadeSugestao, 4);
  end;
  vForm.sgProdutosRelacionados.Col := coQuantidadeAdic;
  vForm.sgProdutosRelacionados.RowCount := IfThen(Length(vProdutosRelacionados) > 1, High(vProdutosRelacionados) + 2, 2);

  if vForm.ShowModal = mrOk then begin
    for i := vForm.sgProdutosRelacionados.FixedRows to vForm.sgProdutosRelacionados.RowCount -1 do begin
      if SFormatCurr(vForm.sgProdutosRelacionados.Cells[coQuantidadeAdic, i]) = 0 then
        Continue;

      SetLength(Result, Length(Result) + 1);
      Result[High(Result)].ProdutoRelacionadoId := SFormatInt(vForm.sgProdutosRelacionados.Cells[coProdutoId, i]);
      Result[High(Result)].QuantidadeAdicionar  := SFormatDouble(vForm.sgProdutosRelacionados.Cells[coQuantidadeAdic, i]);
    end;
  end;

  vForm.Free;
end;

procedure TFormBuscarProdutosRelacionados.sgProdutosRelacionadosArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ACol = coQuantidadeAdic then
    TextCell := NFormatN( SFormatDouble(TextCell), 4 );
end;

procedure TFormBuscarProdutosRelacionados.sgProdutosRelacionadosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coProdutoId, coQuantidadeSug, coQuantidadeAdic] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutosRelacionados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscarProdutosRelacionados.sgProdutosRelacionadosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
    sgProdutosRelacionados.Cells[coQuantidadeAdic, sgProdutosRelacionados.Row] := '';
end;

procedure TFormBuscarProdutosRelacionados.sgProdutosRelacionadosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := ACol = coQuantidadeAdic;
end;

end.
