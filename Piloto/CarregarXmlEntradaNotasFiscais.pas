unit CarregarXmlEntradaNotasFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _RecordsEstoques,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, FrameDiretorioArquivo, _BibliotecaNFE, CadastroFornecedor,
  _FrameHenrancaPesquisas, FrameFornecedores, Vcl.StdCtrls, StaticTextLuka, _Sessao, _CFOP,
  Vcl.Grids, GridLuka, Vcl.Menus, _Biblioteca, _Imagens, _Cadastros, _RecordsCadastros,
  PesquisaProdutos, _ProdutosFornCodOriginais, SpeedButtonLuka, PesquisaCFOP, _RecordsEspeciais,
  _Empresas, _EntradasNotasFiscais, _ProdutosGruposTribEmpresas, _PreEntradasNotasFiscais,
  CheckBoxLuka, Vcl.ComCtrls, PageControlAltis, FrameDadosCobranca, _RecordsFinanceiros,
  _TiposCobranca ,Vcl.Mask, EditLuka, _Fornecedores;

type
  RecEntradaXML = record
    Capa: RecEntradaNotaFiscal;
    Faturas: TArray<RecFaturamentoNFe>;
    Itens: TArray<RecEntradaNotaFiscalItem>;
    Titulos: TArray<RecTitulosFinanceiros>;
  end;

  TFormCarregarXmlEntradaNotasFiscais = class(TFormHerancaFinalizar)
    FrCaminhoXML: TFrCaminhoArquivo;
    FrFornecedor: TFrFornecedores;
    pmOpcoes: TPopupMenu;
    miN1: TMenuItem;
    miMarcarTodos: TMenuItem;
    miDesmarcarTodos: TMenuItem;
    miAlterarCFOPEntrada: TMenuItem;
    miCadastrarProdutoSelecionado: TMenuItem;
    miMarcarSelecionado: TMenuItem;
    miDesmarcarSelecionado: TMenuItem;
    miN2: TMenuItem;
    ckNaoEscriturarItens: TCheckBoxLuka;
    pcDados: TPageControlAltis;
    tsDadosNota: TTabSheet;
    tsItensNota: TTabSheet;
    sgItens: TGridLuka;
    pnInformacoesItem: TPanel;
    pn1: TPanel;
    st5: TStaticTextLuka;
    st10: TStaticText;
    stNomeCompraXML: TStaticText;
    stCodigoNCMXml: TStaticText;
    st2: TStaticText;
    stCestXml: TStaticText;
    st3: TStaticText;
    stUnidadeXml: TStaticText;
    st4: TStaticText;
    st1: TStaticText;
    stFornecedorXML: TStaticText;
    stNomeFabricanteXML: TStaticText;
    st6: TStaticText;
    stCodigoOriginalXml: TStaticText;
    st8: TStaticText;
    stCodigoBarrasXml: TStaticText;
    st11: TStaticText;
    stCodigoOrigemXml: TStaticText;
    pn2: TPanel;
    st12: TStaticTextLuka;
    sbPesquisaProduto: TSpeedButtonLuka;
    stNomeCompraAltis: TStaticText;
    st15: TStaticText;
    st17: TStaticText;
    stCodigoNcmAltis: TStaticText;
    st7: TStaticText;
    st19: TStaticText;
    st9: TStaticText;
    st21: TStaticText;
    st23: TStaticText;
    st25: TStaticText;
    stCestAltis: TStaticText;
    stUnidadeAltis: TStaticText;
    stFabricanteAltis: TStaticText;
    stNomeFabricanteAltis: TStaticText;
    stCodigoOriginalAltis: TStaticText;
    stCodigoBarrasAltis: TStaticText;
    stCodigoOrigemAltis: TStaticText;
    st13: TStaticTextLuka;
    st14: TStaticTextLuka;
    lbl2: TLabel;
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    st16: TStaticTextLuka;
    lb5: TLabel;
    lbMod: TLabel;
    lb7: TLabel;
    lb6: TLabel;
    lb8: TLabel;
    lb9: TLabel;
    lb10: TLabel;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    lb15: TLabel;
    lb14: TLabel;
    lb16: TLabel;
    lb17: TLabel;
    lb18: TLabel;
    lb19: TLabel;
    lb20: TLabel;
    lb21: TLabel;
    lb22: TLabel;
    lb23: TLabel;
    stInformacoesComplementares: TStaticTextLuka;
    lb24: TLabel;
    sgCobrancas: TGridLuka;
    eRazaoSocial: TEditLuka;
    mskChaveAcessoNFe: TMaskEdit;
    eCNPJ: TEditLuka;
    eInscricaoEstadual: TEditLuka;
    eTelefone: TEditLuka;
    eEndereco: TEditLuka;
    eComplemento: TEditLuka;
    eNumNota: TEditLuka;
    eSerieNF: TEditLuka;
    eModeloNF: TEditLuka;
    eDataEmissao: TEditLuka;
    eDataSaida: TEditLuka;
    eNatuezaOperacao: TEditLuka;
    eBaseCalcIcms: TEditLuka;
    eValorIcms: TEditLuka;
    eBaseCalcST: TEditLuka;
    eValorST: TEditLuka;
    eValorIPI: TEditLuka;
    eValorPis: TEditLuka;
    eValorCofins: TEditLuka;
    eValorProdutos: TEditLuka;
    eValorFrete: TEditLuka;
    eValorOutras: TEditLuka;
    eValorDesconto: TEditLuka;
    eValorTotal: TEditLuka;
    ePesoBruto: TEditLuka;
    ePesoLiquido: TEditLuka;
    st18: TStaticTextLuka;
    lb25: TLabel;
    lb26: TLabel;
    eRazaoSocialDestinatario: TEditLuka;
    eCnpjDestinatario: TEditLuka;
    eInscEstadualDestinatario: TEditLuka;
    lb27: TLabel;
    lb28: TLabel;
    eTipoFrete: TEditLuka;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure miMarcarTodosClick(Sender: TObject);
    procedure miDesmarcarTodosClick(Sender: TObject);
    procedure miMarcarSelecionadoClick(Sender: TObject);
    procedure miDesmarcarSelecionadoClick(Sender: TObject);
    procedure sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgItensClick(Sender: TObject);
    procedure sbPesquisaProdutoClick(Sender: TObject);
    procedure miCadastrarProdutoSelecionadoClick(Sender: TObject);
    procedure miAlterarCFOPEntradaClick(Sender: TObject);
    procedure stNomeCompraAltisDblClick(Sender: TObject);
    procedure stCodigoNcmAltisDblClick(Sender: TObject);
    procedure stCestAltisDblClick(Sender: TObject);
    procedure stUnidadeAltisDblClick(Sender: TObject);
    procedure stCodigoOriginalAltisDblClick(Sender: TObject);
    procedure stFabricanteAltisDblClick(Sender: TObject);
    procedure stCodigoBarrasAltisDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FrCaminhoXMLsbPesquisarArquivoClick(Sender: TObject);
    procedure sgCobrancasDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  private
    FPreEntradaId: Integer;
    FDadosNota: RecCapaNFe;
    FEmpresaEntrada: RecEmpresas;
    FFaturas: TArray<RecFaturamentoNFe>;

    procedure FrCaminhoXMLOnAposPesquisarXML(Sender: TObject);
    procedure MarcarDesmarcarTodos(pMarcar: Boolean);
    procedure VincularProdutosAltis;
    procedure VerificarDiferencaXmlAltis;

    procedure AlterarDadosProdutoAltis(
      pStaticTextXml: TStaticText;
      pStaticTextAltis: TStaticText;
      pDescricao: string
    );

    procedure AddProdutoGrid(
      pCodigoOriginal: string;
      pCodigoBarras: string;
      pNome: string;
      pCST: string;
      pPrecoUnitario: Double;
      pQuantidade: Double;
      pUnidade: string;
      pValorDesconto: Double;
      pOutrasDespesas: Double;
      pValorTotal: Double;
      pCFOP: string;
      pNCM: string;
      pBaseCalculoICMS: Double;
      pIndiceRedBaseCalcICMS: Double;
      pPercICMS: Double;
      pValorICMS: Double;
      pBaseCalcICMSST: Double;
      pIndiceRedBaseCalcICMSST: Double;
      pPercICMSST: Double;
      pValorICMSST: Double;
      pBaseCalculoPIS: Double;
      pPercPIS: Double;
      pValorPIS: Double;
      pBaseCalcCOFINS: Double;
      pPercCOFINS: Double;
      pValorCOFINS: Double;
      pPercIVA: Double;
      pPrecoPauta: Double;
      pPercIPI: Double;
      pValorIPI: Double;
      pCest: string;
      pCodigoOrigem: Integer;
      pCstPis: string;
      pCstCofins: string
    );

    procedure AtualizarInformacoesProdutoAltis(
      pNomeAltis: string;
      pNCMAltis: string;
      pCest: string;
      pUndAltis: string;
      pCodOriginalAltis: string;
      pCodBarrasAltis: string;
      pCodOrigemAltis: Integer;
      pProdutoAltisId: Integer;
      pMarcaAltisId: Integer;
      pNomeMarcaAltisId: string;
      pFabricanteId: Integer;
      pNomeFabricante: string;
      pPeso: Double;
      pTipoControleEstoque: string;
      pExigirDataFabricacaoLote: string;
      pExigirDataVencimentoLote: string;
      pLinha: Integer;
      pVinculandoAut:Boolean
    );

    function getCapa: RecEntradaNotaFiscal;
    function getItensGrid: TArray<RecEntradaNotaFiscalItem>;
    function getFaturas: TArray<RecTitulosFinanceiros>;

  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function CarregarXML(pPreEntradaId: Integer): TRetornoTelaFinalizar<RecEntradaXML>;


implementation

{$R *.dfm}

uses
  _Produtos, CadastroProdutos;

const
  coLegenda                 = 0;
  coSelecionado             = 1;
  coNome                    = 2;
  coProdutoId               = 3;
  coNomeAltis               = 4;
  coCST                     = 5;
  coPrecoUnitario           = 6;
  coQuantidade              = 7;
  coUnidade                 = 8;
  coUnidadeAltis            = 9;
  coValorDesconto           = 10;
  coOutrasDespesas          = 11;
  coValorTotal              = 12;
  coCFOP                    = 13;
  coCFOPEntrada             = 14;
  coBaseCalculoICMS         = 15;
  coIndiceRedBaseCalcICMS   = 16;
  coPercICMS                = 17;
  coValorICMS               = 18;
  coBaseCalcICMSST          = 19;
  coIndiceRedBaseCalcICMSST = 20;
  coPercICMSST              = 21;
  coValorICMSST             = 22;
  coBaseCalculoPIS          = 23;
  coPercPIS                 = 24;
  coValorPIS                = 25;
  coBaseCalcCOFINS          = 26;
  coPercCOFINS              = 27;
  coValorCOFINS             = 28;
  coPercIVA                 = 29;
  coPrecoPauta              = 30;
  coPercIPI                 = 31;
  coValorIPI                = 32;
  coPercFrete               = 33;
  coValorFrete              = 34;

  (* Ocultas *)
  coNCM                     = 35;
  coCest                    = 36;
  coCodigoBarras            = 37;
  coCodigoOriginal          = 38;
  coCodigoOrigem            = 39;
  coCstPis                  = 40;
  coCstCofins               = 41;
  coNCMAltis                = 42;
  coCodOriginalAltis        = 43;
  coCodBarrasAltis          = 44;
  coCodOrigemAltis          = 45;
  coCestAltis               = 46;
  coMarcaAltisId            = 47;
  coNomeMarcaAltisId        = 48;
  coFabricanteId            = 49;
  coNomeFabricante          = 50;
  coPeso                    = 51;
  coTipoControleEstoque     = 52;
  coExigirDataFabricacaoLote = 53;
  coExigirDataVencimentoLote = 54;
  coCfopBonificacao          = 55;

//Constantes do frid de cobranca
  coTpcobCobrancaId         = 0;
  coTpcobNomeTipoCobranca   = 1;
  coTpcobValorCobranca      = 2;
  coTpcobDocumento          = 3;
  coTpcobParcela            = 4;
  coTpcobQtdeParcelas       = 5;
  coTpcobDataVencimento     = 6;

function CarregarXML(pPreEntradaId: Integer): TRetornoTelaFinalizar<RecEntradaXML>;
var
  vCaminhoArquivoXml: string;
  vPreEntrada: TArray<RecPreEntradasNotasFiscais>;
  vForm: TFormCarregarXmlEntradaNotasFiscais;
begin
  if pPreEntradaId > 0 then begin
    vPreEntrada := _PreEntradasNotasFiscais.BuscarPreEntradasNotasFiscais(Sessao.getConexaoBanco, 0, [pPreEntradaId]);
    if vPreEntrada = nil then begin
      _Biblioteca.Exclamar('A pr�-entrada n�o foi encontrada!' + sLineBreak + 'Pr�-Entrada: ' + NFormat(pPreEntradaId));
      Result.RetTela := trCancelado;
      Exit;
    end;

    vCaminhoArquivoXml :=
      Sessao.getCaminhoPastaPreEntradas + '\pre_entrada_' + IntToStr(vPreEntrada[0].PreEntradaId) + '_nota' + IntToStr(vPreEntrada[0].NumeroNota) + '.xml';

    if
      not _Biblioteca.salvarStringEmAquivo(
        vPreEntrada[0].XmlTexto,
        vCaminhoArquivoXml
      )
    then begin
      Result.RetTela := trCancelado;
      Exit;
    end;
  end;

  vForm := TFormCarregarXmlEntradaNotasFiscais.Create(nil);
  vForm.FPreEntradaId := pPreEntradaId;

  if vCaminhoArquivoXml <> '' then
    vForm.FrCaminhoXML.CaminhoArquivo := vCaminhoArquivoXml;

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.Capa := vForm.getCapa;
    Result.Dados.Titulos := vForm.getFaturas;

    if not vForm.ckNaoEscriturarItens.Checked then
      Result.Dados.Itens := vForm.getItensGrid;
  end;
end;

procedure TFormCarregarXmlEntradaNotasFiscais.AddProdutoGrid(
  pCodigoOriginal: string;
  pCodigoBarras: string;
  pNome: string;
  pCST: string;
  pPrecoUnitario: Double;
  pQuantidade: Double;
  pUnidade: string;
  pValorDesconto: Double;
  pOutrasDespesas: Double;
  pValorTotal: Double;
  pCFOP: string;
  pNCM: string;
  pBaseCalculoICMS: Double;
  pIndiceRedBaseCalcICMS: Double;
  pPercICMS: Double;
  pValorICMS: Double;
  pBaseCalcICMSST: Double;
  pIndiceRedBaseCalcICMSST: Double;
  pPercICMSST: Double;
  pValorICMSST: Double;
  pBaseCalculoPIS: Double;
  pPercPIS: Double;
  pValorPIS: Double;
  pBaseCalcCOFINS: Double;
  pPercCOFINS: Double;
  pValorCOFINS: Double;
  pPercIVA: Double;
  pPrecoPauta: Double;
  pPercIPI: Double;
  pValorIPI: Double;
  pCest: string;
  pCodigoOrigem: Integer;
  pCstPis: string;
  pCstCofins: string
);
var
  vLinha: Integer;
begin
  if sgItens.Cells[coNome, 1] = '' then
    vLinha := 1
  else begin
    vLinha := sgItens.RowCount;
    sgItens.RowCount := sgItens.RowCount + 1;
  end;

  sgItens.Cells[coSelecionado, vLinha]             := charNaoSelecionado;
  sgItens.Cells[coCodigoOriginal, vLinha]          := pCodigoOriginal;
  sgItens.Cells[coNome, vLinha]                    := pNome;
  sgItens.Cells[coCST, vLinha]                     := pCST;
  sgItens.Cells[coPrecoUnitario, vLinha]           := NFormat(pPrecoUnitario, 6);
  sgItens.Cells[coQuantidade, vLinha]              := NFormat(pQuantidade);
  sgItens.Cells[coUnidade, vLinha]                 := pUnidade;
  sgItens.Cells[coValorDesconto, vLinha]           := NFormat(pValorDesconto);
  sgItens.Cells[coOutrasDespesas, vLinha]          := NFormat(pOutrasDespesas);
  sgItens.Cells[coValorTotal, vLinha]              := NFormat(pValorTotal);
  sgItens.Cells[coCFOP, vLinha]                    := pCFOP;
  sgItens.Cells[coNCM, vLinha]                     := pNCM;
  sgItens.Cells[coBaseCalculoICMS, vLinha]         := NFormat(pBaseCalculoICMS);
  sgItens.Cells[coIndiceRedBaseCalcICMS, vLinha]   := NFormat(pIndiceRedBaseCalcICMS, 5);
  sgItens.Cells[coPercICMS, vLinha]                := NFormat(pPercICMS);
  sgItens.Cells[coValorICMS, vLinha]               := NFormat(pValorICMS);
  sgItens.Cells[coBaseCalcICMSST, vLinha]          := NFormat(pBaseCalcICMSST);
  sgItens.Cells[coIndiceRedBaseCalcICMSST, vLinha] := NFormat(pIndiceRedBaseCalcICMSST, 5);
  sgItens.Cells[coPercICMSST, vLinha]              := NFormat(pPercICMSST);
  sgItens.Cells[coValorICMSST, vLinha]             := NFormat(pValorICMSST);
  sgItens.Cells[coBaseCalculoPIS, vLinha]          := NFormat(pBaseCalculoPIS);
  sgItens.Cells[coPercPIS, vLinha]                 := NFormat(pPercPIS);
  sgItens.Cells[coValorPIS, vLinha]                := NFormat(pValorPIS);
  sgItens.Cells[coBaseCalcCOFINS, vLinha]          := NFormat(pBaseCalcCOFINS);
  sgItens.Cells[coPercCOFINS, vLinha]              := NFormat(pPercCOFINS);
  sgItens.Cells[coValorCOFINS, vLinha]             := NFormat(pValorCOFINS);
  sgItens.Cells[coPercIVA, vLinha]                 := NFormat(pPercIVA, 4);
  sgItens.Cells[coPrecoPauta, vLinha]              := NFormat(pPrecoPauta);
  sgItens.Cells[coPercIPI, vLinha]                 := NFormat(pPercIPI);
  sgItens.Cells[coValorIPI, vLinha]                := NFormat(pValorIPI);
  sgItens.Cells[coCodigoBarras, vLinha]            := pCodigoBarras;
  sgItens.Cells[coCest, vLinha]                    := pCest;
  sgItens.Cells[coCodigoOrigem, vLinha]            := NFormat(pCodigoOrigem);
  sgItens.Cells[coCstPis, vLinha]                  := pCstPis;
  sgItens.Cells[coCstCofins, vLinha]               := pCstCofins;
end;

procedure TFormCarregarXmlEntradaNotasFiscais.AlterarDadosProdutoAltis(
  pStaticTextXml: TStaticText;
  pStaticTextAltis: TStaticText;
  pDescricao: string
);
var
  vRetBanco: RecRetornoBD;
  vProdutos: TArray<RecProdutos>;
  vCampo: string;

  vProdutoId: Integer;
  vTributacaoFederal: RecTributacoesFederal;
begin
  // Se a fonte est� preta � porque n�o h� diferen�a
  if pStaticTextAltis.Font.Color = clBlack then
    Exit;

  if not _Biblioteca.Perguntar('Deseja realmente alterar ' + pDescricao + ' do cadastro do produto?') then
    Exit;

  if pStaticTextAltis = stNomeCompraAltis then
    vCampo := 'NOME_COMPRA'
  else if pStaticTextAltis = stCodigoNcmAltis then
    vCampo := 'CODIGO_NCM'
  else if pStaticTextAltis = stCestAltis then
    vCampo := 'CEST'
  else if pStaticTextAltis = stUnidadeAltis then
    vCampo := 'UNIDADE_VENDA'
  else if pStaticTextAltis = stCodigoOrigemAltis then
    vCampo := 'ORIGEM_PRODUTO'
  else if pStaticTextAltis = stCodigoOriginalAltis then
    vCampo := 'CODIGO_ORIGINAL_FABRICANTE'
  else if pStaticTextAltis = stFabricanteAltis then begin
    vCampo := 'FORNECEDOR_ID';
    pStaticTextXml.Caption := RetornaNumeros(pStaticTextXml.Caption);
  end
  else if pStaticTextAltis = stCodigoBarrasAltis then
    vCampo := 'CODIGO_BARRAS';

  if vCampo = '' then
    Exit;

  vRetBanco := _Produtos.AtualizarProdutoXml(Sessao.getConexaoBanco, SFormatInt(sgItens.Cells[coProdutoId, sgItens.Row]), vCampo, Trim(pStaticTextXml.Caption));
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  vProdutoId := SFormatInt(sgItens.Cells[coProdutoId, sgItens.Row]);

  vProdutos := _Produtos.BuscarProdutos(Sessao.getConexaoBanco, 0, [vProdutoId], True, '');

  _Biblioteca.Informar('Altera��o realizada com sucesso.');

  vTributacaoFederal := _ProdutosGruposTribEmpresas.getTributacoesFederal(Sessao.getConexaoBanco, FEmpresaEntrada.EmpresaId, vProdutoId);
  if vTributacaoFederal.ProdutoId = -1 then begin
    _Biblioteca.Exclamar('As tributa��es para o produto n�o foram encontradas, verifique a parte fiscal no cadastro do produto!');
    Exit;
  end;

  AtualizarInformacoesProdutoAltis(
    vProdutos[0].nome_compra,
    vProdutos[0].codigo_ncm,
    vProdutos[0].cest,
    vProdutos[0].unidade_venda,
    vProdutos[0].CodigoOriginalFabricante,
    vProdutos[0].codigo_barras,
    vTributacaoFederal.OrigemProdutoCompra,
    vProdutos[0].produto_id,
    vProdutos[0].marca_id,
    vProdutos[0].nome_marca,
    vProdutos[0].fornecedor_id,
    vProdutos[0].nome_fabricante,
    vProdutos[0].peso,
    vProdutos[0].TipoControleEstoque,
    vProdutos[0].ExigirDataFabricacaoLote,
    vProdutos[0].ExigirDataVencimentoLote,
    sgItens.Row,
    False
  );
end;

procedure TFormCarregarXmlEntradaNotasFiscais.AtualizarInformacoesProdutoAltis(
  pNomeAltis: string;
  pNCMAltis: string;
  pCest: string;
  pUndAltis: string;
  pCodOriginalAltis: string;
  pCodBarrasAltis: string;
  pCodOrigemAltis: Integer;
  pProdutoAltisId: Integer;
  pMarcaAltisId: Integer;
  pNomeMarcaAltisId: string;
  pFabricanteId: Integer;
  pNomeFabricante: string;
  pPeso: Double;
  pTipoControleEstoque: string;
  pExigirDataFabricacaoLote: string;
  pExigirDataVencimentoLote: string;
  pLinha: Integer;
  pVinculandoAut:Boolean
);
begin
  sgItens.Cells[coProdutoId, pLinha]                := NFormat(pProdutoAltisId);
  sgItens.Cells[coNomeAltis, pLinha]                := pNomeAltis;
  sgItens.Cells[coNCMAltis, pLinha]                 := pNCMAltis;
  sgItens.Cells[coCestAltis, pLinha]                := pCest;
  sgItens.Cells[coUnidadeAltis, pLinha]             := pUndAltis;
  sgItens.Cells[coCodBarrasAltis, pLinha]           := IIfStr(pCodBarrasAltis = '', 'SEM GTIN', pCodBarrasAltis);
  sgItens.Cells[coCodOrigemAltis, pLinha]           := NFormat(pCodOrigemAltis);
  sgItens.Cells[coMarcaAltisId, pLinha]             := NFormat(pMarcaAltisId);
  sgItens.Cells[coNomeMarcaAltisId, pLinha]         := pNomeMarcaAltisId;
  sgItens.Cells[coPeso, pLinha]                     := NFormatEstoque(pPeso);
  sgItens.Cells[coTipoControleEstoque, pLinha]      := pTipoControleEstoque;
  sgItens.Cells[coExigirDataFabricacaoLote, pLinha] := pExigirDataFabricacaoLote;
  sgItens.Cells[coExigirDataVencimentoLote, pLinha] := pExigirDataVencimentoLote;

  if not pVinculandoAut then begin
    sgItens.Cells[coCodOriginalAltis, pLinha] := pCodOriginalAltis;
    sgItens.Cells[coFabricanteId, pLinha]     := NFormat(pFabricanteId);
    sgItens.Cells[coNomeFabricante, pLinha]   := pNomeFabricante;
  end;

//  if not pVinculandoAut and (sgItens.Cells[coCodBarrasAltis, pLinha]) = '') then
//    sgItens.Cells[coCodBarrasAltis, pLinha]   := IIfStr(pCodBarrasAltis = '', 'SEM GTIN', pCodBarrasAltis);

  sgItensClick(nil);
end;

procedure TFormCarregarXmlEntradaNotasFiscais.VincularProdutosAltis;


  procedure vincularCFOPs;
  var
    i: Integer;
    j: Integer;

    vCfopsIds: TArray<string>;
    vCfops: TArray<RecCFOPs>;
  begin
    vCfopsIds := nil;
    for i := 1 to sgItens.RowCount -1 do
      _Biblioteca.AddNoVetorSemRepetir( vCfopsIds, _Biblioteca.RetornaNumeros(sgItens.Cells[coCFOP, i]) + '-0' );

    vCfops := _CFOP.BuscarCFOPsComando(Sessao.getConexaoBanco, ' where CFOP_CORRESPONDENTE_ENTRADA_ID is not null and ' + FiltroInStr('CFOP_PESQUISA_ID', vCfopsIds) );
    if vCfopsIds <> nil then begin
      for i := 1 to sgItens.RowCount -1 do begin
        for j := Low(vCfops) to High(vCfops) do begin
          if sgItens.Cells[coCFOP, i] <> vCfops[j].CfopId then
            Continue;

          sgItens.Cells[coCFOPEntrada, i]     := vCfops[j].CfopCorrespondenteId;
          sgItens.Cells[coCfopBonificacao, i] := vCfops[j].EntradaMercadoriasBonific;
          Break;
        end;
      end;
    end;
  end;

  procedure VincularViaCodigoOriginal;
  var
    i: Integer;
    j: Integer;

    vProdutosIds: TArray<Integer>;
    vProdutos: TArray<RecProdutos>;
    vCodigosOriginais: TArray<string>;
    vTributacaoFederal: RecTributacoesFederal;
    vProdutosFornecedores: TArray<RecProdutosFornCodOriginais>;
  begin
    vCodigosOriginais := nil;
    for i := 1 to sgItens.RowCount -1 do
      _Biblioteca.AddNoVetorSemRepetir( vCodigosOriginais, sgItens.Cells[coCodigoOriginal, i] );

    vProdutosFornecedores :=
      _ProdutosFornCodOriginais.BuscarCodigosOriginaisComando(
        Sessao.getConexaoBanco,
        'where ' + FiltroInStr('CODIGO_ORIGINAL', vCodigosOriginais) + ' ' +
        'and FORNECEDOR_ID  = ' + IntToStr(FrFornecedor.GetFornecedor().cadastro_id)
      );

    if vProdutosFornecedores = nil then
      Exit;

    for i := 1 to sgItens.RowCount -1 do begin
      for j := Low(vProdutosFornecedores) to High(vProdutosFornecedores) do begin
        if sgItens.Cells[coCodigoOriginal, i] <> vProdutosFornecedores[j].CodigoOriginal then
          Continue;

        sgItens.Cells[coProdutoId, i]        := NFormat(vProdutosFornecedores[j].ProdutoId);
        sgItens.Cells[coCodOriginalAltis, i] := vProdutosFornecedores[j].CodigoOriginal;
        sgItens.Cells[coFabricanteId, i]     := NFormat(vProdutosFornecedores[j].FornecedorId);
        sgItens.Cells[coNomeFabricante, i]   := vProdutosFornecedores[j].NomeFornecedor;

        _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, vProdutosFornecedores[j].ProdutoId );
        Break;
      end;
    end;

    (* Se n�o encontrou produtos pelo c�digo original do fornecedor *)
    if vProdutosIds = nil then
      Exit;

    vProdutos := _Produtos.BuscarProdutosComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('PRO.PRODUTO_ID', vProdutosIds) );
    for i := Low(vProdutos) to High(vProdutos) do begin
      j := sgItens.Localizar([coProdutoId], [NFormat(vProdutos[i].produto_id)]);
      if j < 1 then
        Continue;

      vTributacaoFederal := _ProdutosGruposTribEmpresas.getTributacoesFederal(Sessao.getConexaoBanco, FEmpresaEntrada.EmpresaId, vProdutosIds[i]);
      if vTributacaoFederal.ProdutoId = -1 then begin
        _Biblioteca.Exclamar('As tributa��es para o produto ' + vProdutos[i].nome_compra + 'n�o foram encontradas, verifique a parte fiscal no cadastro do produto!');
        Abort;
      end;

      AtualizarInformacoesProdutoAltis(
        vProdutos[i].nome_compra,
        vProdutos[i].codigo_ncm,
        vProdutos[i].cest,
        vProdutos[i].unidade_venda,
        vProdutos[i].CodigoOriginalFabricante,
        vProdutos[i].codigo_barras,
        vTributacaoFederal.OrigemProdutoCompra,
        vProdutos[i].produto_id,
        vProdutos[i].marca_id,
        vProdutos[i].nome_marca,
        vProdutos[i].fornecedor_id,
        vProdutos[i].nome_fabricante,
        vProdutos[i].peso,
        vProdutos[i].TipoControleEstoque,
        vProdutos[i].ExigirDataFabricacaoLote,
        vProdutos[i].ExigirDataVencimentoLote,
        j,
        True
      );
    end;
  end;


  procedure VincularViaCodigoBarras;
  var
    i: Integer;
    j: Integer;

    vProdutosIds: TArray<Integer>;
    vProdutos: TArray<RecProdutos>;
    vCodigosBarras: TArray<string>;
    vTributacaoFederal: RecTributacoesFederal;
    vProdutosCodigoBarras: TArray<RecProdutoCodigoBarras>;
  begin
    vCodigosBarras := nil;
    for i := 1 to sgItens.RowCount -1 do begin
      if sgItens.Cells[coProdutoId, i] <> '' then
        Continue;

      if sgItens.Cells[coCodigoBarras, i] = 'SEM GTIN' then
        Continue;

      _Biblioteca.AddNoVetorSemRepetir( vCodigosBarras, sgItens.Cells[coCodigoBarras, i] );
    end;

    // Se n�o tem nenhum c�digo de barras para pesquisar, saindo fora
    if vCodigosBarras = nil then
      Exit;

    vProdutosCodigoBarras := _Produtos.buscarProdutosCodigoBarras(Sessao.getConexaoBanco, vCodigosBarras);

    // Se n�o encontrou nenhum c�digo de barras, saindo fora
    if vProdutosCodigoBarras = nil then
      Exit;

    for i := 1 to sgItens.RowCount -1 do begin
      for j := Low(vProdutosCodigoBarras) to High(vProdutosCodigoBarras) do begin
        if sgItens.Cells[coCodigoBarras, i] <> vProdutosCodigoBarras[j].CodigoBarras then
          Continue;

        sgItens.Cells[coProdutoId, i]        := NFormat(vProdutosCodigoBarras[j].ProdutoId);
        sgItens.Cells[coCodOriginalAltis, i] := vProdutosCodigoBarras[j].CodigoOriginal;
        sgItens.Cells[coFabricanteId, i]     := NFormat(vProdutosCodigoBarras[j].FornecedorId);
        sgItens.Cells[coNomeFabricante, i]   := vProdutosCodigoBarras[j].NomeFornecedor;

        _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, vProdutosCodigoBarras[j].ProdutoId );
        Break;
      end;
    end;

    (* Se encontrou n�o encontrou produtos pelo c�digo de barras saindo fora *)
    if vProdutosIds = nil then
      Exit;

    vProdutos := _Produtos.BuscarProdutosComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('PRO.PRODUTO_ID', vProdutosIds) );
    for i := Low(vProdutos) to High(vProdutos) do begin
      j := sgItens.Localizar([coProdutoId], [NFormat(vProdutos[i].produto_id)]);
      if j < 1 then
        Continue;

      vTributacaoFederal := _ProdutosGruposTribEmpresas.getTributacoesFederal(Sessao.getConexaoBanco, FEmpresaEntrada.EmpresaId, vProdutosIds[i]);
      if vTributacaoFederal.ProdutoId = -1 then begin
        _Biblioteca.Exclamar('As tributa��es para o produto ' + vProdutos[i].nome_compra + 'n�o foram encontradas, verifique a parte fiscal no cadastro do produto!');
        Abort;
      end;

      AtualizarInformacoesProdutoAltis(
        vProdutos[i].nome_compra,
        vProdutos[i].codigo_ncm,
        vProdutos[i].cest,
        vProdutos[i].unidade_venda,
        vProdutos[i].CodigoOriginalFabricante,
        vProdutos[i].codigo_barras,
        vTributacaoFederal.OrigemProdutoCompra,
        vProdutos[i].produto_id,
        vProdutos[i].marca_id,
        vProdutos[i].nome_marca,
        vProdutos[i].fornecedor_id,
        vProdutos[i].nome_fabricante,
        vProdutos[i].peso,
        vProdutos[i].TipoControleEstoque,
        vProdutos[i].ExigirDataFabricacaoLote,
        vProdutos[i].ExigirDataVencimentoLote,
        j,
        True
      );
    end;
  end;

begin
  vincularCFOPs;
  VincularViaCodigoOriginal;
  VincularViaCodigoBarras;
end;

procedure TFormCarregarXmlEntradaNotasFiscais.Finalizar(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vProdutosCodigosOriginais: TArray<_ProdutosFornCodOriginais.RecProdutosFornCodOriginais>;
begin
  inherited;

  if ckNaoEscriturarItens.Checked then
    Exit;

  vProdutosCodigosOriginais := nil;
  SetLength(vProdutosCodigosOriginais, sgItens.RowCount -1);
  for i := 1 to sgItens.RowCount -1 do begin
    vProdutosCodigosOriginais[i - 1].ProdutoId      := SFormatInt(sgItens.Cells[coProdutoId, i]);
    vProdutosCodigosOriginais[i - 1].FornecedorId   := FrFornecedor.GetFornecedor.cadastro_id;
    vProdutosCodigosOriginais[i - 1].CodigoOriginal := sgItens.Cells[coCodigoOriginal, i];
  end;

  vRetBanco :=
    _ProdutosFornCodOriginais.AtualizarProdutosCodigosOriginais(
      Sessao.getConexaoBanco,
      FrFornecedor.GetFornecedor().cadastro_id,
      vProdutosCodigosOriginais
    );

  Sessao.AbortarSeHouveErro( vRetBanco );
end;

procedure TFormCarregarXmlEntradaNotasFiscais.FormCreate(Sender: TObject);
begin
  inherited;
  FrCaminhoXML.setFiltro('Extensible Markup Language (*.XML)|*.xml|');
  FrCaminhoXML.OnAposPesquisarArquivo := FrCaminhoXMLOnAposPesquisarXML;
  _Biblioteca.Habilitar([
    sgItens,
    FrFornecedor,
    sbPesquisaProduto,
    sgCobrancas],
    False
  );

  _Biblioteca.LimparCampos([
    stNomeCompraXML,
    stCodigoNCMXml,
    stCestXml,
    stUnidadeXml,
    stCodigoOriginalXml,
    stCodigoBarrasXml,
    stCodigoOrigemXml,
    stNomeCompraAltis,
    stCodigoNCMAltis,
    stCestAltis,
    stUnidadeAltis,
    stCodigoOriginalAltis,
    stCodigoBarrasAltis,
    stCodigoOrigemAltis,
    stFornecedorXML,
    stNomeFabricanteXML,
    stFabricanteAltis,
    stNomeFabricanteAltis,
    sgCobrancas]
  );
end;

procedure TFormCarregarXmlEntradaNotasFiscais.FormShow(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([pcDados], True);
end;

procedure TFormCarregarXmlEntradaNotasFiscais.FrCaminhoXMLOnAposPesquisarXML(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vPosic: Integer;

  vItensAux: TArray<RecItemNFe>;
  vItensNota: TArray<RecItemNFe>;

  vCadastroFornecedor: TArray<RecCadastros>;
  vDadosFornecedor: TArray<RecFornecedores>;
  vRetTela: TRetornoTelaFinalizar<Integer>;

  vTipoCobranca: TArray<RecTiposCobranca>;

  vAchou: Boolean;
begin
  sgItens.ClearGrid();
  sgCobrancas.ClearGrid();

  if not _BibliotecaNFE.BuscarDadosNFeEntrada(FrCaminhoXML.getCaminhoArquivo, FDadosNota, vItensNota, FFaturas) then begin
    Exclamar('N�o foi poss�vel carregar o XML da NFe!');
    Exit;
  end;

  FEmpresaEntrada := Sessao.getEmpresa(FDadosNota.CnpjDestinatario);
  if FEmpresaEntrada = nil then begin
    Exclamar('Este XML n�o pertence a empresa selecionada!');
    Exit;
  end;

  if _EntradasNotasFiscais.ExisteEntradaNFe(Sessao.getConexaoBanco, FDadosNota.ChaveNFe) then begin
    Exclamar('Est� no ja esta lan�ada no sistema!');
    Exit;
  end;

  vCadastroFornecedor := _Cadastros.BuscarCadastros(Sessao.getConexaoBanco, 3, [ FDadosNota.Cnpj ]);
  if vCadastroFornecedor = nil then begin
    if not _Biblioteca.Perguntar('O fornecedor desta NFe n�o est� cadastrado, deseja cadastra-lo agora?') then begin
      FrCaminhoXML.Clear;
      SetarFoco(FrCaminhoXML);
      Exit;
    end;

    vRetTela :=
      CadastroFornecedor.CadastrarFornecedorEntradaNota(
        FDadosNota.Cnpj,
        FDadosNota.RazaoSocial,
        FDadosNota.NomeFantasia,
        FDadosNota.InscricaoEstadual,
        FDadosNota.Bairro,
        FDadosNota.Cep,
        FDadosNota.CodigoIBGeMunicipio,
        FDadosNota.Estado,
        FDadosNota.Logradouro,
        FDadosNota.Complemento,
        FDadosNota.Cidade,
        FDadosNota.CodigoIBGeMunicipio,
        FDadosNota.Telefone,
        FDadosNota.SuperSimples
      );

    if vRetTela.BuscaCancelada then
      Exit;

    FrFornecedor.InserirDadoPorChave(vRetTela.Dados);
  end
  else begin
    if vCadastroFornecedor[0].e_fornecedor = 'S' then begin
      FrFornecedor.InserirDadoPorChave(vCadastroFornecedor[0].cadastro_id);

      if FrFornecedor.EstaVazio then begin
        _Biblioteca.Informar(
          'O fornecedor desta n�o est� parametrizado como "Revenda" ou "Uso e consumo", ' +
          'por favor v� at� a tela de fornecedores e realize o ajuste do cadastro.' + sLineBreak +
          'Fornecedor: ' + getInformacao(vCadastroFornecedor[0].cadastro_id, vCadastroFornecedor[0].razao_social)
        );
        Exit;
      end;

      FrFornecedor.Modo(False, False);
    end
    else begin
      _Biblioteca.Informar(
        'Este CNPJ/CPF n�o esta cadastrado como fornecedor, ' +
        'cadastre o mesmo como fornecedor na tela de cadastro de fornecedores.'
      );

      Exit;
    end;
  end;

  vDadosFornecedor := _Fornecedores.BuscarFornecedores(Sessao.getConexaoBanco, 0, [vCadastroFornecedor[0].cadastro_id], False, False, false);

  _Biblioteca.Habilitar([pcDados,sgCobrancas], True);
  _Biblioteca.Habilitar([sgItens, sbPesquisaProduto], True);
  _Biblioteca.Habilitar([FrCaminhoXML], False, False);

  eRazaoSocialDestinatario.Text := FDadosNota.NomeDestinatario;
  eCnpjDestinatario.Text := FDadosNota.CnpjDestinatario;
  eInscEstadualDestinatario.Text := FDadosNota.InscEstadualDestinatario;

  if eCnpjDestinatario.Text <> Sessao.getEmpresaLogada.Cnpj then
    eRazaoSocialDestinatario.Font.Color := clRed;

  eRazaoSocial.Text := FDadosNota.RazaoSocial;
  eCNPJ.Text        := FDadosNota.Cnpj;
  eInscricaoEstadual.Text := FDadosNota.InscricaoEstadual;
  eTelefone.Text    := FDadosNota.Telefone;
  eEndereco.Text    := FDadosNota.Logradouro + ' ' + FDadosNota.Complemento;
  eComplemento.Text := FDadosNota.Bairro + ' ' + FDadosNota.Cidade + ' ' + FDadosNota.Estado + ' CEP: ' + FDadosNota.Cep;

  stInformacoesComplementares.Caption := FDadosNota.InformacoesComplementares;

  stInformacoesComplementares.Height := 15*(trunc(Length(stInformacoesComplementares.Caption)/150) + 1);
  eNatuezaOperacao.Text := FDadosNota.NaturezaOperacao;

  eNumNota.Text  := NFormatN(FDadosNota.NumeroNota);
  eSerieNF.Text  := FDadosNota.SerieNota;
  eModeloNF.Text := FDadosNota.ModeloNota;
  eDataEmissao.Text := DFormatN(FDadosNota.DataHoraEmissao);
  eDataSaida.Text   := DFormatN(FDadosNota.DataHoraSaida);
  mskChaveAcessoNFe.Text  := FDadosNota.ChaveNFe;
  eBaseCalcIcms.Text  := NFormatN(FDadosNota.BaseCalculoIcms,2);
  eValorIcms.Text     := NFormatN(FDadosNota.ValorIcms,2);
  eBaseCalcST.Text    := NFormatN(FDadosNota.BaseCalculoIcmsSt,2);
  eValorST.Text       := NFormatN(FDadosNota.ValorIcmsSt,2);
  eValorIPI.Text      := NFormatN(FDadosNota.ValorIpi,2);
  eValorProdutos.Text := NFormatN(FDadosNota.ValorTotalProdutos,2);
  eValorFrete.Text    := NFormatN(FDadosNota.ValorFrete,2);
  eValorOutras.Text   := NFormatN(FDadosNota.ValorOutrasDespesas,2);
  eValorDesconto.Text := NFormatN(FDadosNota.ValorDesconto,2);
  eValorTotal.Text    := NFormatN(FDadosNota.ValorTotal,2);
  ePesoBruto.Text     := NFormatN(FDadosNota.PesoBruto,2);
  ePesoLiquido.Text   := NFormatN(FDadosNota.PesoLiquido,2);
  eValorPis.Text      := NFormatN(FDadosNota.ValorPis,2);
  eValorCofins.Text   := NFormatN(FDadosNota.ValorCofins,2);
  eTipoFrete.Text     := FDadosNota.ModFrete;

  if FFaturas <> nil then
  begin
    if vDadosFornecedor[0].TipoCobrancaEntradaNfe <> '' then
      vTipoCobranca := _TiposCobranca.BuscarTiposCobrancas(Sessao.getConexaoBanco,0,[vDadosFornecedor[0].TipoCobrancaEntradaNfe],'')
    else
      vTipoCobranca := _TiposCobranca.BuscarTiposCobrancas(Sessao.getConexaoBanco,0,[Sessao.getParametros.TipoCobEntradaNfeXmlId],'');

    for i := Low(FFaturas) to High(FFaturas) do begin
      if vTipoCobranca <> nil then begin
        sgCobrancas.Cells[coTpcobCobrancaId, i + 1]       := NFormat(vTipoCobranca[0].CobrancaId);
        sgCobrancas.Cells[coTpcobNomeTipoCobranca, i + 1] := vTipoCobranca[0].Nome;
      end
      else begin
        sgCobrancas.Cells[coTpcobCobrancaId, i + 1]       := '';
        sgCobrancas.Cells[coTpcobNomeTipoCobranca, i + 1] := '';
      end;
      sgCobrancas.Cells[coTpcobValorCobranca, i + 1]    := NFormat( FFaturas[i].Valor );
      sgCobrancas.Cells[coTpcobDocumento, i + 1]        := FFaturas[i].Documento;
      sgCobrancas.Cells[coTpcobParcela, i + 1]          := NFormat( FFaturas[i].NumeroDaParcela );;
      sgCobrancas.Cells[coTpcobQtdeParcelas, i + 1]     := NFormat( FFaturas[i].QuantParcelas );
      sgCobrancas.Cells[coTpcobDataVencimento, i + 1]   := DFormat(FFaturas[i].DataVencimento);
    end;
    sgCobrancas.SetLinhasGridPorTamanhoVetor( Length(FFaturas) );
  end;

  (* Agrupando os produtos que s�o iguais *)
  vPosic := -1;
  for i := Low(vItensNota) to High(vItensNota) do begin
    vAchou := False;

    for j := Low(vItensAux) to High(vItensAux) do begin
      vAchou := (vItensNota[i].CodigoOriginal = vItensAux[j].CodigoOriginal) and ((vItensNota[i].CfopId = vItensAux[j].CfopId));
      if vAchou then begin
        vPosic := j;
        Break;
      end;
    end;

    if vAchou then begin
      vItensAux[vPosic].Quantidade := vItensAux[vPosic].Quantidade + vItensNota[i].Quantidade;
      vItensAux[vPosic].ValorTotal := vItensAux[vPosic].ValorTotal + vItensNota[i].ValorTotal;

      vItensAux[vPosic].BaseCalculoIcms := vItensAux[vPosic].BaseCalculoIcms + vItensNota[i].BaseCalculoIcms;
      vItensAux[vPosic].ValorIcms       := vItensAux[vPosic].ValorIcms + vItensNota[i].ValorIcms;

      vItensAux[vPosic].BaseCalculoIcmsSt := vItensAux[vPosic].BaseCalculoIcmsSt + vItensNota[i].BaseCalculoIcmsSt;
      vItensAux[vPosic].ValorIcmsSt       := vItensAux[vPosic].ValorIcmsSt + vItensNota[i].ValorIcmsSt;

      vItensAux[vPosic].ValorIpi                 := vItensAux[vPosic].ValorIpi + vItensNota[i].ValorIpi;
      vItensAux[vPosic].ValorTotalOutrasDespesas := vItensAux[vPosic].ValorTotalOutrasDespesas + vItensNota[i].ValorTotalOutrasDespesas;
      vItensAux[vPosic].ValorTotalDesconto       := vItensAux[vPosic].ValorTotalDesconto + vItensNota[i].ValorTotalDesconto;

      vItensAux[vPosic].ValorFrete               := vItensAux[vPosic].ValorFrete + vItensNota[i].ValorFrete;

      vItensAux[vPosic].BaseCalculoPis := vItensAux[vPosic].BaseCalculoPis + vItensNota[i].BaseCalculoPis;
      vItensAux[vPosic].ValorPis       := vItensAux[vPosic].ValorPis + vItensNota[i].ValorPis;

      vItensAux[vPosic].BaseCalculoCofins := vItensAux[vPosic].BaseCalculoCofins + vItensNota[i].BaseCalculoCofins;
      vItensAux[vPosic].ValorCofins       := vItensAux[vPosic].ValorCofins + vItensNota[i].ValorCofins;

      Continue;
    end;

    SetLength(vItensAux, Length(vItensAux) + 1);
    vItensAux[High(vItensAux)] := vItensNota[i];
  end;

  vItensNota := vItensAux;

  for i := Low(vItensNota) to High(vItensNota) do begin
    AddProdutoGrid(
      vItensNota[i].CodigoOriginal,
      vItensNota[i].CodigoBarras,
      vItensNota[i].NomeProduto,
      vItensNota[i].Cst,
      vItensNota[i].PrecoUnitario,
      vItensNota[i].Quantidade,
      vItensNota[i].Unidade,
      vItensNota[i].ValorTotalDesconto,
      vItensNota[i].ValorTotalOutrasDespesas,
      vItensNota[i].ValorTotal,
      vItensNota[i].CfopId,
      vItensNota[i].CodigoNcm,
      vItensNota[i].BaseCalculoIcms,
      vItensNota[i].IndiceReducaoBaseIcms,
      vItensNota[i].PercentualIcms,
      vItensNota[i].ValorIcms,
      vItensNota[i].BaseCalculoIcmsSt,
      vItensNota[i].IndiceReducaoBaseIcmsSt,
      vItensNota[i].PercentualIcmsSt,
      vItensNota[i].ValorIcmsSt,
      vItensNota[i].BaseCalculoPis,
      vItensNota[i].PercentualPis,
      vItensNota[i].ValorPis,
      vItensNota[i].BaseCalculoCofins,
      vItensNota[i].PercentualCofins,
      vItensNota[i].ValorCofins,
      vItensNota[i].Iva,
      vItensNota[i].PrecoPauta,
      vItensNota[i].PercentualIpi,
      vItensNota[i].ValorIpi,
      vItensNota[i].Cest,
      vItensNota[i].OrigemProduto,
      vItensNota[i].CstPis,
      vItensNota[i].CstCofins
    );
  end;

  VincularProdutosAltis;
  sgItensClick(nil);
end;

procedure TFormCarregarXmlEntradaNotasFiscais.FrCaminhoXMLsbPesquisarArquivoClick(
  Sender: TObject);
begin
  inherited;
  FrCaminhoXML.sbPesquisarArquivoClick(Sender);

end;

function TFormCarregarXmlEntradaNotasFiscais.getCapa: RecEntradaNotaFiscal;
begin
  Result.EntradaViaXML        := True;
  Result.XmlTexto             := FDadosNota.XmlTexto;
  Result.CaminhoXml           := FrCaminhoXML.getCaminhoArquivo;
  Result.empresa_id           := Sessao.getEmpresa(FDadosNota.CnpjDestinatario).EmpresaId;
  Result.chave_nfe            := FDadosNota.ChaveNFe;
  Result.numero_nota          := FDadosNota.NumeroNota;
  Result.serie_nota           := FDadosNota.SerieNota;
  Result.modelo_nota          := FDadosNota.ModeloNota;
  Result.data_hora_emissao    := FDadosNota.DataHoraEmissao;
  Result.fornecedor_id        := FrFornecedor.GetFornecedor().cadastro_id;

  Result.base_calculo_icms    := FDadosNota.BaseCalculoIcms;
  Result.valor_icms           := FDadosNota.ValorIcms;
  Result.base_calculo_icms_st := FDadosNota.BaseCalculoIcmsSt;
  Result.valor_icms_st        := FDadosNota.ValorIcmsSt;

  Result.base_calculo_pis     := FDadosNota.BaseCalculoPis;
  Result.valor_pis            := FDadosNota.ValorPis;

  Result.base_calculo_cofins  := FDadosNota.BaseCalculoCofins;
  Result.valor_cofins         := FDadosNota.ValorCofins;

  Result.peso_bruto           := FDadosNota.PesoBruto;
  Result.peso_liquido         := FDadosNota.PesoLiquido;

  Result.valor_ipi            := FDadosNota.ValorIpi;

  Result.valor_desconto       := FDadosNota.ValorDesconto;
  Result.valor_outras_despesas:= FDadosNota.ValorOutrasDespesas;
  Result.valor_frete          := FDadosNota.ValorFrete;
  Result.ModalidadeFrete      := FDadosNota.TipoFrete;

  Result.valor_total_produtos := FDadosNota.ValorTotalProdutos;
  Result.valor_total          := FDadosNota.ValorTotal;
  Result.atualizar_custo_entrada := 'S';
  Result.PlanoFinanceiroId       := FrFornecedor.GetFornecedor().PlanoFinanceiroRevenda;
end;

function TFormCarregarXmlEntradaNotasFiscais.getItensGrid: TArray<RecEntradaNotaFiscalItem>;
var
  i: Integer;
begin
  SetLength(Result, sgItens.RowCount -1);
  for i := 1 to sgItens.RowCount -1 do begin
    Result[i - 1].produto_id             := SFormatInt(sgItens.Cells[coProdutoId, i]);
    Result[i - 1].nome_produto           := sgItens.Cells[coNomeAltis, i];
    Result[i - 1].item_id                := i;
    Result[i - 1].CfopId                 := sgItens.Cells[coCFOPEntrada, i];
    Result[i - 1].cst                    := sgItens.Cells[coCST, i];
    Result[i - 1].codigo_ncm             := sgItens.Cells[coNCM, i];
    Result[i - 1].valor_total            := SFormatDouble(sgItens.Cells[coValorTotal, i]);
    Result[i - 1].preco_unitario         := SFormatDouble(sgItens.Cells[coPrecoUnitario, i]);
    Result[i - 1].quantidade             := SFormatDouble(sgItens.Cells[coQuantidade, i]);
    Result[i - 1].valor_total_desconto   := SFormatDouble(sgItens.Cells[coValorDesconto, i]);
    Result[i - 1].valor_total_outras_despesas := SFormatDouble(sgItens.Cells[coOutrasDespesas, i]);
    Result[i - 1].codigo_barras          := sgItens.Cells[coCodigoBarras, i];
    Result[i - 1].origem_produto         := SFormatInt( sgItens.Cells[coCodigoOrigem, i] );

    Result[i - 1].indice_reducao_base_icms := SFormatDouble(sgItens.Cells[coIndiceRedBaseCalcICMS, i]);
    Result[i - 1].base_calculo_icms      := SFormatDouble(sgItens.Cells[coBaseCalculoICMS, i]);
    Result[i - 1].percentual_icms        := SFormatDouble(sgItens.Cells[coPercICMS, i]);
    Result[i - 1].valor_icms             := SFormatDouble(sgItens.Cells[coValorICMS, i]);

    Result[i - 1].indice_reducao_base_icms_st := 1;
    Result[i - 1].base_calculo_icms_st   := SFormatDouble(sgItens.Cells[coBaseCalcICMSST, i]);
    Result[i - 1].percentual_icms_st     := SFormatDouble(sgItens.Cells[coPercICMSST, i]);
    Result[i - 1].valor_icms_st          := SFormatDouble(sgItens.Cells[coValorICMSST, i]);

    Result[i - 1].cst_pis                := sgItens.Cells[coCstPis, i];
    Result[i - 1].base_calculo_pis       := SFormatDouble(sgItens.Cells[coBaseCalculoPIS, i]);
    Result[i - 1].percentual_pis         := SFormatDouble(sgItens.Cells[coPercPis, i]);
    Result[i - 1].valor_pis              := SFormatDouble(sgItens.Cells[coValorPis, i]);

    Result[i - 1].cst_cofins             := sgItens.Cells[coCstCofins, i];
    Result[i - 1].base_calculo_cofins    := SFormatDouble(sgItens.Cells[coBaseCalcCOFINS, i]);
    Result[i - 1].percentual_cofins      := SFormatDouble(sgItens.Cells[coPercCofins, i]);
    Result[i - 1].valor_cofins           := SFormatDouble(sgItens.Cells[coValorCofins, i]);

    Result[i - 1].valor_ipi              := SFormatDouble(sgItens.Cells[coValorIPI, i]);
    Result[i - 1].percentual_ipi         := SFormatDouble(sgItens.Cells[coPercIPI, i]);
    Result[i - 1].iva                    := SFormatDouble(sgItens.Cells[coPercIVA, i]);

    Result[i - 1].Peso                   := SFormatDouble(sgItens.Cells[coPeso, i]);

    Result[i - 1].preco_pauta            := SFormatDouble(sgItens.Cells[coPrecoPauta, i]);

    Result[i - 1].QuantidadeEmbalagem    := 1;
    Result[i - 1].UnidadeEntradaId       := sgItens.Cells[coUnidade, i];
    Result[i - 1].UnidadeCompraId        := sgItens.Cells[coUnidade, i];
    Result[i - 1].MultiploCompra         := 1;
    Result[i - 1].TipoControleEstoque    := sgItens.Cells[coTipoControleEstoque, i];

    Result[i - 1].nome_marca             := sgItens.Cells[coNomeMarcaAltisId, i];

    Result[i - 1].ExigirDataFabricacaoLote  := sgItens.Cells[coExigirDataFabricacaoLote, i];
    Result[i - 1].ExigirDataVencimentoLote  := sgItens.Cells[coExigirDataVencimentoLote, i];
    Result[i - 1].EntradaMercadoriasBonific := sgItens.Cells[coCfopBonificacao, i];
  end;
end;

function TFormCarregarXmlEntradaNotasFiscais.getFaturas: TArray<RecTitulosFinanceiros>;
var
  i: Integer;
begin
  SetLength(Result, sgCobrancas.RowCount -1);

  for i := 1 to sgCobrancas.RowCount -1 do begin
    if sgCobrancas.Cells[coTpcobValorCobranca, i] = '' then begin
      Result := nil;
      exit;
    end;
    Result[i - 1].CobrancaId      := SFormatInt(sgCobrancas.Cells[coTpcobCobrancaId, i]);
    Result[i - 1].NomeCobranca    := sgCobrancas.Cells[coTpcobNomeTipoCobranca, i];
    Result[i - 1].Parcela         := SFormatInt(sgCobrancas.Cells[coTpcobParcela, i]);
    Result[i - 1].DataVencimento  := StrToDate(sgCobrancas.Cells[coTpcobDataVencimento, i]);
    Result[i - 1].Documento       := sgCobrancas.Cells[coTpcobDocumento, i];;
    Result[i - 1].NumeroParcelas  := SFormatInt(sgCobrancas.Cells[coTpcobQtdeParcelas, i]);
    Result[i - 1].Valor           := SFormatCurr(sgCobrancas.Cells[coTpcobValorCobranca, i]);
  end;
end;

procedure TFormCarregarXmlEntradaNotasFiscais.MarcarDesmarcarTodos(pMarcar: Boolean);
var
  i: Integer;
begin
  for i := 1 to sgItens.RowCount -1 do
    sgItens.Cells[coSelecionado, i] := IIfStr(pMarcar, charSelecionado, charNaoSelecionado);
end;

procedure TFormCarregarXmlEntradaNotasFiscais.miAlterarCFOPEntradaClick(Sender: TObject);
var
  i: Integer;
  vQtdeSelecionados: Integer;

  vCfop: RecCFOPs;
begin
  inherited;
  vQtdeSelecionados := 0;
  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charSelecionado then
      vQtdeSelecionados := vQtdeSelecionados + 1;
  end;

  if vQtdeSelecionados = 0 then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado para altera��o!');
    Abort;
  end;

  vCfop :=
    RecCFOPs(
      PesquisaCFOP.PesquisarCFOP(
        tpEntrada,
        True,
        IIfStr(Sessao.getEmpresaLogada.EstadoId = FrFornecedor.GetFornecedor.cadastro.estado_id, '1', '2'),
        True,
        False,
        False
      )
    );

  if vCfop = nil then
    Exit;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charSelecionado then begin
      sgItens.Cells[coCFOPEntrada, i]     := vCfop.CfopPesquisaId;
      sgItens.Cells[coCfopBonificacao, i] := vCfop.EntradaMercadoriasBonific;
    end;
  end;
end;

procedure TFormCarregarXmlEntradaNotasFiscais.miCadastrarProdutoSelecionadoClick(Sender: TObject);
var
  vProduto: TArray<RecProdutos>;
  vRetTela: TRetornoTelaFinalizar<Integer>;
  vTributacaoFederal: RecTributacoesFederal;
begin
  inherited;

  vRetTela :=
    CadastroProdutos.CadastrarProdutoEntradaNota(
      FrFornecedor.GetFornecedor().cadastro.cpf_cnpj,
      FrFornecedor.GetFornecedor().cadastro.cadastro_id,
      FrFornecedor.GetFornecedor().cadastro.nome_fantasia,
      sgItens.Cells[coNome, sgItens.Row],
      sgItens.Cells[coNCM, sgItens.Row],
      sgItens.Cells[coCest, sgItens.Row],
      sgItens.Cells[coCodigoBarras, sgItens.Row],
      sgItens.Cells[coCodigoOriginal, sgItens.Row],
      sgItens.Cells[coUnidade, sgItens.Row]
    );

  if vRetTela.BuscaCancelada then
    Exit;

  vProduto := _Produtos.BuscarProdutos(Sessao.getConexaoBanco, 0, [vRetTela.Dados], True, '');

  vTributacaoFederal := _ProdutosGruposTribEmpresas.getTributacoesFederal(Sessao.getConexaoBanco, FEmpresaEntrada.EmpresaId, vProduto[0].produto_id);
  if vTributacaoFederal.ProdutoId = -1 then begin
    _Biblioteca.Exclamar('As tributa��es para o produto n�o foram encontradas, verifique a parte fiscal no cadastro do produto!');
    Exit;
  end;

  AtualizarInformacoesProdutoAltis(
    vProduto[0].nome_compra,
    vProduto[0].codigo_ncm,
    vProduto[0].cest,
    vProduto[0].unidade_venda,
    vProduto[0].CodigoOriginalFabricante,
    vProduto[0].codigo_barras,
    vTributacaoFederal.OrigemProdutoCompra,
    vProduto[0].produto_id,
    vProduto[0].marca_id,
    vProduto[0].nome_marca,
    vProduto[0].fornecedor_id,
    vProduto[0].nome_fabricante,
    vProduto[0].peso,
    vProduto[0].TipoControleEstoque,
    vProduto[0].ExigirDataFabricacaoLote,
    vProduto[0].ExigirDataVencimentoLote,
    sgItens.Row,
    False
  );

  sgItensClick(nil);
end;

procedure TFormCarregarXmlEntradaNotasFiscais.miDesmarcarSelecionadoClick(Sender: TObject);
begin
  inherited;
  sgItens.Cells[coSelecionado, sgItens.Row] := charNaoSelecionado;
end;

procedure TFormCarregarXmlEntradaNotasFiscais.miDesmarcarTodosClick(Sender: TObject);
begin
  inherited;
  MarcarDesmarcarTodos(False);
end;

procedure TFormCarregarXmlEntradaNotasFiscais.miMarcarSelecionadoClick(Sender: TObject);
begin
  inherited;
  sgItens.Cells[coSelecionado, sgItens.Row] := charNaoSelecionado;
end;

procedure TFormCarregarXmlEntradaNotasFiscais.miMarcarTodosClick(Sender: TObject);
begin
  inherited;
  MarcarDesmarcarTodos(True);
end;

procedure TFormCarregarXmlEntradaNotasFiscais.sbPesquisaProdutoClick(Sender: TObject);
var
  vProduto: RecProdutos;
  vTributacaoFederal: RecTributacoesFederal;
begin
  inherited;
  vProduto := RecProdutos( PesquisaProdutos.PesquisarProduto(False, True) );
  if vProduto <> nil then begin

    vTributacaoFederal := _ProdutosGruposTribEmpresas.getTributacoesFederal(Sessao.getConexaoBanco, FEmpresaEntrada.EmpresaId, vProduto.produto_id);
    if vTributacaoFederal.ProdutoId = -1 then begin
      _Biblioteca.Exclamar('As tributa��es para o produto n�o foram encontradas, verifique a parte fiscal no cadastro do produto!');
      Exit;
    end;

    AtualizarInformacoesProdutoAltis(
      vProduto.nome_compra,
      vProduto.codigo_ncm,
      vProduto.cest,
      vProduto.unidade_venda,
      vProduto.CodigoOriginalFabricante,
      vProduto.codigo_barras,
      vTributacaoFederal.OrigemProdutoCompra,
      vProduto.produto_id,
      vProduto.marca_id,
      vProduto.nome_marca,
      vProduto.fornecedor_id,
      vProduto.nome_fabricante,
      vProduto.peso,
      vProduto.TipoControleEstoque,
      vProduto.ExigirDataFabricacaoLote,
      vProduto.ExigirDataVencimentoLote,
      sgItens.Row,
      False
    );
  end;
end;

procedure TFormCarregarXmlEntradaNotasFiscais.sgCobrancasDrawCell(
  Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coTpcobValorCobranca, coTpcobCobrancaId, coTpcobParcela, coTpcobQtdeParcelas, coTpcobDocumento] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgCobrancas.MergeCells([ARow, ACol], [ARow, ACol],[ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCarregarXmlEntradaNotasFiscais.sgItensClick(Sender: TObject);
begin
  inherited;
  if sgItens.Cells[coNome, sgItens.Row] = '' then
    Exit;

  (* Preenchendo as informa��es do XML *)
  stNomeCompraXML.Caption     := ' ' + sgItens.Cells[coNome, sgItens.Row];
  stCodigoNCMXml.Caption      := ' ' + sgItens.Cells[coNCM, sgItens.Row];
  stCestXml.Caption           := ' ' + sgItens.Cells[coCest, sgItens.Row];
  stUnidadeXml.Caption        := ' ' + sgItens.Cells[coUnidade, sgItens.Row];
  stFornecedorXML.Caption     := NFormat(FrFornecedor.GetFornecedor.cadastro_id) + ' ';
  stNomeFabricanteXML.Caption := ' ' + FrFornecedor.GetFornecedor.cadastro.nome_fantasia;
  stCodigoOriginalXml.Caption := ' ' + sgItens.Cells[coCodigoOriginal, sgItens.Row];
  stCodigoBarrasXml.Caption   := ' ' + sgItens.Cells[coCodigoBarras, sgItens.Row];
  stCodigoOrigemXml.Caption   := ' ' + sgItens.Cells[coCodigoOrigem, sgItens.Row];

  (* Preenchendo as informa��es do Hiva para compara��o *)
  stNomeCompraAltis.Caption     := ' ' + sgItens.Cells[coNomeAltis, sgItens.Row];
  stCodigoNCMAltis.Caption      := ' ' + sgItens.Cells[coNCMAltis, sgItens.Row];
  stCestAltis.Caption           := ' ' + sgItens.Cells[coCestAltis, sgItens.Row];
  stUnidadeAltis.Caption        := ' ' + sgItens.Cells[coUnidadeAltis, sgItens.Row];
  stFabricanteAltis.Caption     := sgItens.Cells[coFabricanteId, sgItens.Row] + ' ';
  stNomeFabricanteAltis.Caption := ' ' + sgItens.Cells[coNomeFabricante, sgItens.Row];
  stCodigoOriginalAltis.Caption := ' ' + sgItens.Cells[coCodOriginalAltis, sgItens.Row];
  stCodigoBarrasAltis.Caption   := ' ' + sgItens.Cells[coCodBarrasAltis, sgItens.Row];
  stCodigoOrigemAltis.Caption   := ' ' + sgItens.Cells[coCodOrigemAltis, sgItens.Row];

  VerificarDiferencaXmlAltis;
end;

procedure TFormCarregarXmlEntradaNotasFiscais.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coLegenda,
    coSelecionado,
    coCST]
  then
    vAlinhamento := taCenter
  else if ACol in[
    coProdutoId,
    coPrecoUnitario,
    coQuantidade,
    coValorDesconto,
    coOutrasDespesas,
    coValorTotal,
    coBaseCalculoICMS,
    coIndiceRedBaseCalcICMS,
    coPercICMS,
    coValorICMS,
    coBaseCalcICMSST,
    coIndiceRedBaseCalcICMSST,
    coValorICMSST,
    coBaseCalculoPIS,
    coPercPIS,
    coValorPIS,
    coBaseCalcCOFINS,
    coPercCOFINS,
    coValorCOFINS,
    coPercIVA,
    coPrecoPauta,
    coPercIPI,
    coValorIPI]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCarregarXmlEntradaNotasFiscais.sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol <> coSelecionado) or (sgItens.Cells[ACol, ARow] = '') then
   Exit;

  if sgItens.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else if sgItens.Cells[ACol, ARow] = charSelecionado then
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFormCarregarXmlEntradaNotasFiscais.sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Shift = [ssCtrl]) and (Key = VK_SPACE) then
    MarcarDesmarcarTodos(True)
  else if Key = VK_SPACE then
    sgItens.Cells[coSelecionado, sgItens.Row] := IIfStr(sgItens.Cells[coSelecionado, sgItens.Row] = charSelecionado, charNaoSelecionado, charSelecionado)
  else if Key = VK_F5 then
    sbPesquisaProdutoClick(Sender);
end;

procedure TFormCarregarXmlEntradaNotasFiscais.stCestAltisDblClick(Sender: TObject);
begin
  inherited;
  AlterarDadosProdutoAltis(
    stCestXml,
    stCestAltis,
    'o CEST'
  );
end;

procedure TFormCarregarXmlEntradaNotasFiscais.stCodigoBarrasAltisDblClick(Sender: TObject);
begin
  inherited;
  AlterarDadosProdutoAltis(
    stCodigoBarrasXml,
    stCodigoBarrasAltis,
    'o c�digo de barras'
  );
end;

procedure TFormCarregarXmlEntradaNotasFiscais.stCodigoNcmAltisDblClick(Sender: TObject);
begin
  inherited;
  AlterarDadosProdutoAltis(
    stCodigoNCMXml,
    stCodigoNcmAltis,
    'o NCM'
  );
end;

procedure TFormCarregarXmlEntradaNotasFiscais.stCodigoOriginalAltisDblClick(Sender: TObject);
begin
  inherited;
  if FrFornecedor.GetFornecedor.tipo_fornecedor <> 'I' then begin
    _Biblioteca.Exclamar('O fornecedor desta nota n�o � do tipo industria, altera��o n�o permitida!');
    Exit;
  end;

  AlterarDadosProdutoAltis(
    stCodigoOriginalXml,
    stCodigoOriginalAltis,
    'o c�digo original'
  );
end;

procedure TFormCarregarXmlEntradaNotasFiscais.stFabricanteAltisDblClick(Sender: TObject);
begin
  inherited;
  AlterarDadosProdutoAltis(
    stFornecedorXML,
    stFabricanteAltis,
    'o fornecedor'
  );
end;

procedure TFormCarregarXmlEntradaNotasFiscais.stNomeCompraAltisDblClick(Sender: TObject);
begin
  inherited;
  AlterarDadosProdutoAltis(
    stNomeCompraXML,
    stNomeCompraAltis,
    'o nome de compra'
  );
end;

procedure TFormCarregarXmlEntradaNotasFiscais.stUnidadeAltisDblClick(Sender: TObject);
begin
  inherited;
  AlterarDadosProdutoAltis(
    stUnidadeXml,
    stUnidadeAltis,
    'a unidade de venda'
  );
end;

procedure TFormCarregarXmlEntradaNotasFiscais.VerificarDiferencaXmlAltis;

  procedure VerDiferenca( pStaticText1: TStaticText; pStaticText2: TStaticText );
  begin
    if pStaticText1.Caption <> pStaticText2.Caption then
      pStaticText2.Font.Color := clRed
    else
      pStaticText2.Font.Color := clBlack;
  end;

begin
  VerDiferenca(stNomeCompraXML, stNomeCompraAltis);
  VerDiferenca(stCodigoNCMXml, stCodigoNcmAltis);
  VerDiferenca(stCestXml, stCestAltis);
  VerDiferenca(stUnidadeXml, stUnidadeAltis);
  VerDiferenca(stFornecedorXML, stFabricanteAltis);
  VerDiferenca(stNomeFabricanteXML, stNomeFabricanteAltis);
  VerDiferenca(stCodigoOriginalXml, stCodigoOriginalAltis);
  VerDiferenca(stCodigoBarrasXml, stCodigoBarrasAltis);
  VerDiferenca(stCodigoOrigemXml, stCodigoOrigemAltis);
end;

procedure TFormCarregarXmlEntradaNotasFiscais.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  j: Integer;
begin
  inherited;
  if ckNaoEscriturarItens.Checked and (FrFornecedor.GetFornecedor.UsoConsumo = 'N') then begin
    _Biblioteca.Exclamar('Apenas fornecedores de uso e consumo podem ter notas escrituradas sem itens!');
    SetarFoco(ckNaoEscriturarItens);
    Abort;
  end;

  if not ckNaoEscriturarItens.Checked then begin
    for i := 1 to sgItens.RowCount -1 do begin
      if sgItens.Cells[coProdutoId, i] = '' then begin
        _Biblioteca.Exclamar('O produto ' + sgItens.Cells[coNome, i] + ' n�o foi vinculado ao sistema!');
        sgItens.Col := coNome;
        sgItens.Row := i;
        SetarFoco(sgItens);
        Abort;
      end;

      if sgItens.Cells[coNome, i] = '' then begin
        _Biblioteca.Exclamar('O produto ' + sgItens.Cells[coNome, i] + ' n�o foi vinculado ao sistema!');
        sgItens.Col := coNome;
        sgItens.Row := i;
        SetarFoco(sgItens);
        Abort;
      end;

      if sgItens.Cells[coCFOPEntrada, i] = '' then begin
        _Biblioteca.Exclamar('O produto ' + sgItens.Cells[coNome, i] + ' n�o teve seu CFOP de entrada definido, verifique!');
        sgItens.Col := coCFOPEntrada;
        sgItens.Row := i;
        SetarFoco(sgItens);
        Abort;
      end;

      for j := 1 to sgItens.RowCount -1 do begin
        if i = j then
          Continue;

        if sgItens.Cells[coProdutoId, i] = sgItens.Cells[coProdutoId, j] then begin
          _Biblioteca.Exclamar('O produto ' + sgItens.Cells[coNome, j] + ' j� foi lan�ado na linha ' + IntToStr(i) + ', verifique!');
          sgItens.Col := coNome;
          sgItens.Row := j;
          SetarFoco(sgItens);
          Abort;
        end;
      end;
    end;
  end;
end;

end.
