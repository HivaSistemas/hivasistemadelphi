unit AjusteEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.Mask, _RecordsEstoques,
  EditLukaData, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameProdutos, _AjustesEstoqueItens,
  Frame.MotivosAjusteEstoque, _Biblioteca, _Estoques, _EstoquesDivisao, _CustosProdutos, _RecordsCadastros, _RecordsEspeciais, _AjustesEstoque,
  ComboBoxLuka, _HerancaCadastro, Frame.Lotes, FrameLocais, CheckBoxLuka,
  StaticTextLuka;

type
  TFormAjusteEstoque = class(TFormHerancaCadastroCodigo)
    FrProduto: TFrProdutos;
    lb7: TLabel;
    eQuantidade: TEditLuka;
    sgProdutos: TGridLuka;
    FrMotivoAjusteEstoque: TFrMotivosAjusteEstoque;
    lb1: TLabel;
    eObservacao: TEditLuka;
    stTEF: TStaticText;
    st5: TStaticText;
    stQuantidadeItensGrid: TStaticText;
    cbNatureza: TComboBoxLuka;
    Label2: TLabel;
    FrLote: TFrameLotes;
    FrLocalProduto: TFrLocais;
    stEstoqueDisponivel: TStaticTextLuka;
    st4: TStaticText;
    stEstoqueFisico: TStaticTextLuka;
    StaticText1: TStaticText;
    stEstoqueDisponivelProdutoSelecionadoGrid: TStaticTextLuka;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    stEstoqueFisicoProdutoSelecionadoGrid: TStaticTextLuka;
    stValorTotalProdutosGrid: TStaticTextLuka;
    StaticText5: TStaticText;
    Label3: TLabel;
    ePrecoUnitario: TEditLuka;
    Label4: TLabel;
    eValorTotal: TEditLuka;
    StaticText6: TStaticText;
    stEstoqueDisponivelLocal: TStaticTextLuka;
    StaticText7: TStaticText;
    stEstoqueFisicoLocal: TStaticTextLuka;
    Label5: TLabel;
    Label6: TLabel;
    ckCalcularEstoqueFinal: TCheckBoxLuka;
    sbZerarEstoque: TSpeedButton;
    sbZerarEstoqueNegativo: TSpeedButton;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure eQuantidadeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure sgProdutosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgProdutosClick(Sender: TObject);
    procedure sgProdutosDblClick(Sender: TObject);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure eQuantidadeChange(Sender: TObject);
    procedure ckCalcularEstoqueFinalClick(Sender: TObject);
    procedure sbZerarEstoqueClick(Sender: TObject);
    procedure sbZerarEstoqueNegativoClick(Sender: TObject);
  private
    FLinha: Integer;
    procedure PreencherRegistro(pAjuste: RecAjusteEstoque; pAjusteItens: TArray<RecAjusteEstoqueItens>);
    procedure AdicionarNoProdutoGrid(
      pProdutoId: Integer;
      pNomeProduto: string;
      pMarca: string;
      pUnidade: string;
      pCustoUnitario: Double;
      pValorTotal: Double;
      pLocalId: Integer;
      pNomeLocal: string;
      pQuantidade: Double;
      pNovoEstoqueFisico: Double;
      pLote: string;
      pDataFabricacao: TDate;
      pDataVencimento: TDate;
      pEstoqueFisico: Double;
      pNatureza: string;
      pNaturezaSintetico: string;
      pEstoqueDisponivel: Double;
      pEstoqueInformado: Double
    );

    procedure FrProdutoOnAposPesquisar(Sender: TObject);
    procedure FrProdutoOnAposDeletar(Sender: TObject);

    procedure FrLocalProdutoOnAposPesquisar(Sender: TObject);
    procedure FrLocalProdutoOnAposDeletar(Sender: TObject);

    procedure FrMotivoAjusteOnAposPesquisar(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

uses BuscarProdutosZerarEstoque;

const
  coProduto           = 0;
  coNomeProduto       = 1;
  coMarca             = 2;
  coUnidade           = 3;
  coPrecoUnitario     = 4;
  coLocal             = 5;
  coNatureza          = 6;
  coQuantidade        = 7;
  coEstoqueInformado  = 8;
  coValorTotal        = 9;
  coNovoEstoqueFisico = 10;
  coLote              = 11;

  coDataFabricacao    = 12;
  coDataVencimento    = 13;

  coEstoqueFisico     = 14;
  coEstReservado      = 15;
  coNaturezaSint      = 16;
  coLocalId           = 17;
  coEstoqueDisponivel = 18;

procedure TFormAjusteEstoque.AdicionarNoProdutoGrid(
  pProdutoId: Integer;
  pNomeProduto: string;
  pMarca: string;
  pUnidade: string;
  pCustoUnitario: Double;
  pValorTotal: Double;
  pLocalId: Integer;
  pNomeLocal: string;
  pQuantidade: Double;
  pNovoEstoqueFisico: Double;
  pLote: string;
  pDataFabricacao: TDate;
  pDataVencimento: TDate;
  pEstoqueFisico: Double;
  pNatureza: string;
  pNaturezaSintetico: string;
  pEstoqueDisponivel: Double;
  pEstoqueInformado: Double
);
var
  vLinha: Integer;
  i: Integer;
begin
  if FLinha = 0 then begin
    vLinha := sgProdutos.Localizar([coProduto, coLocalId, coLote], [NFormat(pProdutoId), NFormat(pLocalId), pLote]);
    if vLinha > -1 then begin
      Exclamar('Este produto j� foi adicionado no grid!');
      sgProdutos.Row := vLinha;
      sgProdutos.SetFocus;
      Abort;
    end;

    if sgProdutos.Cells[coProduto, 1] = '' then
      FLinha := 1
    else begin
      FLinha := sgProdutos.RowCount;
      sgProdutos.RowCount := sgProdutos.RowCount + 1;
    end;
  end;

  sgProdutos.Cells[coProduto, FLinha]           := NFormat(pProdutoId);
  sgProdutos.Cells[coNomeProduto, FLinha]       := pNomeProduto;
  sgProdutos.Cells[coMarca, FLinha]             := pMarca;
  sgProdutos.Cells[coUnidade, FLinha]           := pUnidade;
  sgProdutos.Cells[coPrecoUnitario, FLinha]     := NFormat(pCustoUnitario);
  sgProdutos.Cells[coValorTotal, FLinha]        := NFormat(pValorTotal);
  sgProdutos.Cells[coLocal, FLinha]             := NFormat(pLocalId) + ' - ' + pNomeLocal;
  sgProdutos.Cells[coQuantidade, FLinha]        := NFormatNEstoqueCasasDecimais(pQuantidade, 4);
  sgProdutos.Cells[coEstoqueInformado, FLinha]  := NFormatNEstoqueCasasDecimais(pEstoqueInformado, 4);
  sgProdutos.Cells[coNovoEstoqueFisico, FLinha] := NFormatNEstoqueCasasDecimais(pNovoEstoqueFisico, 4);
  sgProdutos.Cells[coLote, FLinha]              := pLote;
  sgProdutos.Cells[coDataFabricacao, FLinha]    := DFormatN(pDataFabricacao);
  sgProdutos.Cells[coDataVencimento, FLinha]    := DFormatN(pDataVencimento);
  sgProdutos.Cells[coEstoqueFisico, FLinha]     := NFormatNEstoqueCasasDecimais(pEstoqueFisico, 4);
  sgProdutos.Cells[coNatureza, FLinha]          := pNatureza;
  sgProdutos.Cells[coNaturezaSint, FLinha]      := pNaturezaSintetico;
  sgProdutos.Cells[coLocalId, FLinha]           := NFormat(pLocalId);
  sgProdutos.Cells[coEstoqueDisponivel, FLinha] := NFormatNEstoqueCasasDecimais(pEstoqueDisponivel, 4);

  stQuantidadeItensGrid.Caption := NFormat(sgProdutos.RowCount -1);
  stValorTotalProdutosGrid.Clear;
  for i := 1 to sgProdutos.RowCount -1 do
    stValorTotalProdutosGrid.Somar(SFormatDouble(sgProdutos.Cells[coValorTotal, i]));

  sgProdutos.Row := FLinha;
  sgProdutosClick(nil);
  FLinha := 0;
end;

procedure TFormAjusteEstoque.BuscarRegistro;
var
  vAjuste: TArray<RecAjusteEstoque>;
  vAjusteItens: TArray<RecAjusteEstoqueItens>;
begin
  vAjuste := _AjustesEstoque.BuscarAjustesEstoque(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vAjuste = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  vAjusteItens := _AjustesEstoqueItens.BuscarAjusteEstoqueItens(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vAjusteItens = nil then begin
    _Biblioteca.Exclamar('Produtos do ajuste n�o encontrado!');
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(vAjuste[0], vAjusteItens);
end;

procedure TFormAjusteEstoque.ckCalcularEstoqueFinalClick(Sender: TObject);
begin
  inherited;
  if ckCalcularEstoqueFinal.Checked then begin
    cbNatureza.Enabled := False;
    cbNatureza.ItemIndex := -1;
  end
  else
    cbNatureza.Enabled := True;
end;

procedure TFormAjusteEstoque.eQuantidadeChange(Sender: TObject);
begin
  inherited;
  eValorTotal.AsCurr := ePrecoUnitario.AsCurr * eQuantidade.AsDouble;
end;

procedure TFormAjusteEstoque.eQuantidadeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  m1, m2, m3: Double;
  quantidadeDesejada: Double;
begin
  inherited;

  if (Key <> VK_RETURN) or (eID.AsInt > 0) then
    Exit;

  if FrProduto.EstaVazio then begin
    Exclamar('O produto n�o foi informado, verifique!');
    FrProduto.SetFocus;
    Exit;
  end;

  if FrLocalProduto.EstaVazio then begin
    Exclamar('O local do produto n�o foi definido!');
    SetarFoco(FrLocalProduto);
    Exit;
  end;

  //Calcular se vai ser feito entrada ou saida com base na quantidade desejada de estoque
  quantidadeDesejada := eQuantidade.AsDouble;
  if ckCalcularEstoqueFinal.Checked then begin

    if stEstoqueFisicoLocal.AsDouble > quantidadeDesejada then begin
      cbNatureza.SetIndicePorValor('S');
      eQuantidade.AsDouble := stEstoqueFisicoLocal.AsDouble - quantidadeDesejada;
    end
    else if stEstoqueFisicoLocal.AsDouble < quantidadeDesejada then begin
      cbNatureza.SetIndicePorValor('E');
      eQuantidade.AsDouble := quantidadeDesejada - (stEstoqueFisicoLocal.AsDouble);
    end;

  end;

  if cbNatureza.Text = '' then begin
    Exclamar('A natureza do ajuste n�o foi informado, verifique!');
    SetarFoco(cbNatureza);
    Exit;
  end;

  m1 := FrProduto.getProduto().multiplo_venda;
  m2 := FrProduto.getProduto().multiplo_venda_2;
  m3 := FrProduto.getProduto().multiplo_venda_3;

  if
    not ValidarMultiplo(eQuantidade.AsDouble, m3, False, ((m1 = 0) and (m2 = 0))) and
    not ValidarMultiplo(eQuantidade.AsDouble, m2, False, (m1 = 0)) and
    not ValidarMultiplo(eQuantidade.AsDouble, m1, ((m2 = 0) and (m3 = 0)))
  then begin
    SetarFoco(eQuantidade);
    Exit;
  end;

  if eQuantidade.AsCurr = 0 then begin
    Exclamar('A quantidade n�o foi inforamada corretamente, verifique!');
    SetarFoco(eQuantidade);
    Exit;
  end;

  if not FrLote.LoteValido then begin
    Exclamar('O lote informado n�o � v�lido, verifique!');
    SetarFoco(FrLote);
    Exit;
  end;

  if eValorTotal.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O produto n�o possui custo definido, por favor verique!');
    SetarFoco(FrProduto);
    Exit;
  end;

  AdicionarNoProdutoGrid(
    FrProduto.getProduto().produto_id,
    FrProduto.getProduto().nome,
    FrProduto.getProduto().nome_marca,
    FrProduto.getProduto().unidade_venda,
    IIfDbl(ePrecoUnitario.AsDouble > 0.00,  ePrecoUnitario.AsDouble, 0.10),
    ((IIfDbl(ePrecoUnitario.AsDouble > 0.00,  ePrecoUnitario.AsDouble, 0.10)) * eQuantidade.AsDouble),
    FrLocalProduto.GetLocais().local_id,
    FrLocalProduto.GetLocais().nome,
    eQuantidade.AsDouble,
    IIfDbl(cbNatureza.GetValor = 'E', stEstoqueFisicoLocal.AsDouble + eQuantidade.AsDouble, stEstoqueFisicoLocal.AsDouble - eQuantidade.AsDouble),
    FrLote.Lote,
    FrLote.DataFabricacao,
    FrLote.DataVencimento,
    stEstoqueFisico.AsDouble,
    cbNatureza.Text,
    cbNatureza.GetValor,
    stEstoqueDisponivel.AsDouble,
    quantidadeDesejada
  );

  _Biblioteca.LimparCampos([FrProduto, eQuantidade, cbNatureza]);
  SetarFoco(FrProduto);
end;

procedure TFormAjusteEstoque.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _AjustesEstoque.ExcluirAjustesEstoque(Sessao.getConexaoBanco, eID.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormAjusteEstoque.FormCreate(Sender: TObject);
begin
  inherited;
  FrProduto.OnAposDeletar := FrProdutoOnAposDeletar;
  FrProduto.OnAposPesquisar := FrProdutoOnAposPesquisar;

  FrLocalProduto.OnAposDeletar := FrLocalProdutoOnAposDeletar;
  FrLocalProduto.OnAposPesquisar := FrLocalProdutoOnAposPesquisar;

  FrMotivoAjusteEstoque.OnAposPesquisar := FrMotivoAjusteOnAposPesquisar;
  FrLote.Modo(False);

  sbZerarEstoque.Enabled := Sessao.AutorizadoRotina('zerar_estoque_produtos');
  sbZerarEstoqueNegativo.Enabled := Sessao.AutorizadoRotina('zerar_estoque_produtos');
  eQuantidade.CasasDecimais := 4;
end;

procedure TFormAjusteEstoque.FrLocalProdutoOnAposDeletar(Sender: TObject);
begin
  stEstoqueDisponivelLocal.Clear;
  stEstoqueFisicoLocal.Clear;

  if FrProduto.EstaVazio then begin
    ePrecoUnitario.Clear;
    eValorTotal.Clear;
  end;
end;

procedure TFormAjusteEstoque.FrLocalProdutoOnAposPesquisar(Sender: TObject);
var
  vEstoquesDivisao: TArray<RecEstoquesDivisao>;
begin
  if FrProduto.EstaVazio then
    Exit;

  vEstoquesDivisao := _EstoquesDivisao.BuscarEstoque(Sessao.getConexaoBanco, 1, [Sessao.getEmpresaLogada.EmpresaId, FrLocalProduto.GetLocais().local_id, FrProduto.getProduto.produto_id]);

  if vEstoquesDivisao <> nil then begin
    stEstoqueDisponivelLocal.AsDouble := vEstoquesDivisao[0].Disponivel;
    stEstoqueFisicoLocal.AsDouble     := vEstoquesDivisao[0].Fisico;
  end;
end;

procedure TFormAjusteEstoque.FrMotivoAjusteOnAposPesquisar(Sender: TObject);
begin
  if FrMotivoAjusteEstoque.GetMotivoAjusteEstoque.PlanoFinanceiroId = '' then begin
    _Biblioteca.Exclamar('O plano financeiro para este motivo de ajuste n�o foi informado, verifique o cadastro de motivos de ajuste de estoque!');
    FrMotivoAjusteEstoque.Clear;
    SetarFoco(FrMotivoAjusteEstoque);
  end;
end;

procedure TFormAjusteEstoque.FrProdutoOnAposDeletar(Sender: TObject);
begin
  _Biblioteca.LimparCampos([stEstoqueDisponivel, stEstoqueFisico]);
  FrLote.SetTipoControleEstoque('N', 'N', 'N');

  if not FrLocalProduto.EstaVazio then
    FrLocalProdutoOnAposDeletar(nil);
end;

procedure TFormAjusteEstoque.FrProdutoOnAposPesquisar(Sender: TObject);
var
  vEstoques: TArray<RecEstoque>;
  vCustosProdutos: TArray<RecCustoProduto>;
begin
  _Biblioteca.LimparCampos([stEstoqueDisponivel, stEstoqueFisico]);

  if Em(FrProduto.getProduto.TipoControleEstoque, ['K', 'A']) then begin
    _Biblioteca.Informar('Produtos que s�o cotrolados como "Kit" n�o pode ter seu estoque ajustado, ajuste os produtos que comp�e o kit.');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  FrLote.SetTipoControleEstoque(FrProduto.getProduto.TipoControleEstoque, FrProduto.getProduto.ExigirDataFabricacaoLote, FrProduto.getProduto.ExigirDataVencimentoLote);

  vEstoques := _Estoques.BuscarEstoque(Sessao.getConexaoBanco, 0, [FrProduto.getProduto.produto_id, Sessao.getEmpresaLogada.EmpresaId]);
  if vEstoques <> nil then begin
    stEstoqueDisponivel.AsDouble := vEstoques[0].Disponivel;
    stEstoqueFisico.AsDouble     := vEstoques[0].Fisico;
  end;

  vCustosProdutos := _CustosProdutos.BuscarCustoProdutos(Sessao.getConexaoBanco, 0, [FrProduto.getProduto.produto_id, Sessao.getEmpresaLogada.EmpresaId]);

  if vCustosProdutos <> nil then
    ePrecoUnitario.AsDouble := vCustosProdutos[0].CustoAjusteEstoque
  else
    ePrecoUnitario.AsDouble := 0.10;

  if not FrLocalProduto.EstaVazio then
    FrLocalProdutoOnAposPesquisar(nil);
end;

procedure TFormAjusteEstoque.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetornoBanco: RecRetornoBD;
  vAjusteItens: TArray<RecAjusteEstoqueItens>;
begin
  inherited;

  SetLength(vAjusteItens, sgProdutos.RowCount -1);
  for i := 1 to sgProdutos.RowCount -1 do begin
    vAjusteItens[i - 1].ajuste_estoque_id := 0;
    vAjusteItens[i - 1].LocalId           := SFormatInt(sgProdutos.Cells[coLocalId, i]);
    vAjusteItens[i - 1].item_id           := i;
    vAjusteItens[i - 1].produto_id        := SFormatInt(sgProdutos.Cells[coProduto, i]);
    vAjusteItens[i - 1].quantidade        := SFormatDouble(sgProdutos.Cells[coQuantidade, i]);
    vAjusteItens[i - 1].quantidadeInformado := SFormatDouble(sgProdutos.Cells[coEstoqueInformado, i]);
    vAjusteItens[i - 1].Lote              := sgProdutos.Cells[coLote, i];
    vAjusteItens[i - 1].Natureza          := sgProdutos.Cells[coNaturezaSint, i];
    vAjusteItens[i - 1].PrecoUnitario     := SFormatDouble(sgProdutos.Cells[coPrecoUnitario, i]);
    vAjusteItens[i - 1].ValorTotal        := SFormatDouble(sgProdutos.Cells[coValorTotal, i]);
    vAjusteItens[i - 1].DataFabricacao    := ToData(sgProdutos.Cells[coDataFabricacao, i]);
    vAjusteItens[i - 1].DataVencimento    := ToData(sgProdutos.Cells[coDataVencimento, i]);
  end;

  vRetornoBanco :=
    _AjustesEstoque.AtualizarAjustesEstoque(
      Sessao.getConexaoBanco,
      eID.AsInt,
      Sessao.getEmpresaLogada.EmpresaId,
      FrMotivoAjusteEstoque.GetMotivoAjusteEstoque().motivo_ajuste_id,
      eObservacao.Text,
      FrMotivoAjusteEstoque.GetMotivoAjusteEstoque().PlanoFinanceiroId,
      'NOR',
      0,
      stValorTotalProdutosGrid.AsDouble,
      'FISICO',
      vAjusteItens,
      False
    );

  if vRetornoBanco.TeveErro then begin
    Exclamar(vRetornoBanco.MensagemErro);
    Abort;
  end;

  if vRetornoBanco.AsInt > 0 then
    _Biblioteca.Informar('Novo ajuste de estoque efetuado com sucesso, c�digo: ' + NFormat(vRetornoBanco.AsInt));
end;

procedure TFormAjusteEstoque.Modo(pEditando: Boolean);
begin
  inherited;
   _Biblioteca.Habilitar([
    //eDataCadastro,
    FrMotivoAjusteEstoque,
    eObservacao,
    FrProduto,
    FrLocalProduto,
    eQuantidade,
    cbNatureza,
    sgProdutos,
    stQuantidadeItensGrid,
    stEstoqueDisponivelProdutoSelecionadoGrid,
    stEstoqueFisicoProdutoSelecionadoGrid,
    sbZerarEstoque,
    stValorTotalProdutosGrid],
    pEditando
  );

  _Biblioteca.SomenteLeitura([
    //eDataCadastro,
    FrMotivoAjusteEstoque,
    eObservacao,
    FrProduto,
    FrLocalProduto,
    eQuantidade,
    cbNatureza,
    sgProdutos,
    stQuantidadeItensGrid,
    stEstoqueDisponivelProdutoSelecionadoGrid,
    stEstoqueFisicoProdutoSelecionadoGrid],
    False
  );

  cbNatureza.Enabled := (eID.AsInt = 0);

  if pEditando then begin
    SetarFoco(FrMotivoAjusteEstoque);
    sgProdutos.MostrarColunas([coNovoEstoqueFisico]);
  end;
end;

procedure TFormAjusteEstoque.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormAjusteEstoque.PreencherRegistro(pAjuste: RecAjusteEstoque; pAjusteItens: TArray<RecAjusteEstoqueItens>);
var
  i: Integer;
begin
  eID.AsInt := pAjuste.ajuste_estoque_id;
  //eDataCadastro.AsData := pAjuste.data_hora_ajuste;
  FrMotivoAjusteEstoque.InserirDadoPorChave(pAjuste.motivo_ajuste_id);
  eObservacao.Text := pAjuste.observacao;
  sgProdutos.OcultarColunas([coNovoEstoqueFisico]);

  for i := Low(pAjusteItens) to High(pAjusteItens) do begin
    AdicionarNoProdutoGrid(
      pAjusteItens[i].produto_id,
      pAjusteItens[i].NomeProduto,
      pAjusteItens[i].NomeMarca,
      pAjusteItens[i].Unidade,
      pAjusteItens[i].PrecoUnitario,
      pAjusteItens[i].ValorTotal,
      pAjusteItens[i].LocalId,
      pAjusteItens[i].NomeLocal,
      pAjusteItens[i].Quantidade,
      0,
      pAjusteItens[i].Lote,
      0,
      0,
      0,
      pAjusteItens[i].NaturezaAnalitico,
      pAjusteItens[i].Natureza,
      0,
      10
    );

  end;

  sbGravar.Enabled := False;

  Informar('N�o � poss�vel editar este ajuste.');
  _Biblioteca.SomenteLeitura
  (
    [
      FrMotivoAjusteEstoque,
      FrProduto,
      FrLocalProduto,
      FrLote,
      cbNatureza,
      eObservacao,
      FrProduto,
      eQuantidade
    ],
    True
  );

  FrMotivoAjusteEstoque.SetFocus;
end;

procedure TFormAjusteEstoque.sbZerarEstoqueClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecProdutosSelecionados>;
  vProdutos: TArray<RecProdutosZerarEstoque>;
  i: Integer;
begin
  inherited;
  vRetorno := BuscarProdutosZerarEstoque.BuscarDadosProdutos;

  if vRetorno.BuscaCancelada then
    Exit;

  FrMotivoAjusteEstoque.InserirDadoPorChave(999);

  vProdutos := vRetorno.Dados.Itens;

  for i := Low(vProdutos) to High(vProdutos) do begin
    AdicionarNoProdutoGrid(
      vProdutos[i].ProdutoId,
      vProdutos[i].ProdutoNome,
      vProdutos[i].MarcaNome,
      vProdutos[i].Unidade,
      IIfDbl(vProdutos[i].PrecoUnitario > 0.0, vProdutos[i].PrecoUnitario, 0.10),
      ((IIfDbl(vProdutos[i].PrecoUnitario > 0.0, vProdutos[i].PrecoUnitario, 0.10)) * IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade)),
      vProdutos[i].LocalId,
      vProdutos[i].LocalNome,
      IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade),
      0, //novo estoque fisico
      vProdutos[i].Lote,
      vProdutos[i].DataFabricacao,
      vProdutos[i].DataVencimento,
      IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade), //estoque fisico
      IIfStr(vProdutos[i].Quantidade > 0, 'Sa�da', 'Entrada'),
      IIf(vProdutos[i].Quantidade > 0, 'S', 'E'),
      IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade), //estoque disponivel
      IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade)  //estoque informado
    );
  end;
end;

procedure TFormAjusteEstoque.sgProdutosClick(Sender: TObject);
begin
  inherited;
  if sgProdutos.Cells[coProduto, 1] = '' then
    Exit;

  stEstoqueDisponivelProdutoSelecionadoGrid.AsDouble := SFormatDouble(sgProdutos.Cells[coEstoqueDisponivel, sgProdutos.Row]);
  stEstoqueFisicoProdutoSelecionadoGrid.AsDouble     := SFormatDouble(sgProdutos.Cells[coEstoqueFisico, sgProdutos.Row]);
end;

procedure TFormAjusteEstoque.sgProdutosDblClick(Sender: TObject);
begin
  inherited;
  if sgProdutos.Cells[coProduto, sgProdutos.Row] = '' then
    Exit;

  FrProduto.InserirDadoPorChave(SFormatInt(sgProdutos.Cells[coProduto, sgProdutos.Row]));
  FrLocalProduto.InserirDadoPorChave( SFormatInt(sgProdutos.Cells[coLocalId, sgProdutos.Row]) );

  if SFormatDouble(sgProdutos.Cells[coQuantidade, sgProdutos.Row]) > SFormatDouble(sgProdutos.Cells[coEstoqueInformado, sgProdutos.Row]) then
    eQuantidade.AsDouble := SFormatDouble(sgProdutos.Cells[coEstoqueInformado, sgProdutos.Row])
  else
    eQuantidade.AsDouble := SFormatDouble(sgProdutos.Cells[coQuantidade, sgProdutos.Row]);

  cbNatureza.SetIndicePorValor(sgProdutos.Cells[coNaturezaSint, sgProdutos.Row]);
  FrLote.Lote := sgProdutos.Cells[coLote, sgProdutos.Row];
  FrLote.DataFabricacao := ToData(sgProdutos.Cells[coDataFabricacao, sgProdutos.Row]);
  FrLote.DataVencimento := ToData(sgProdutos.Cells[coDataVencimento, sgProdutos.Row]);
  eValorTotal.AsDouble  := SFormatDouble(sgProdutos.Cells[coValorTotal, sgProdutos.Row]);
  ePrecoUnitario.AsDouble := SFormatDouble(sgProdutos.Cells[coPrecoUnitario, sgProdutos.Row]);

  FLinha := sgProdutos.Row;
  SetarFoco(FrProduto);
end;

procedure TFormAjusteEstoque.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coProduto, coQuantidade, coEstoqueInformado, coPrecoUnitario, coNovoEstoqueFisico, coValorTotal] then
    vAlinhamento := taRightJustify
  else if ACol = coNatureza then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormAjusteEstoque.sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if (ACol <> coNatureza) or (ARow = 0) then
    Exit;

  AFont.Style := [fsBold];

  if sgProdutos.Cells[coNaturezaSint, ARow] = 'E' then
    AFont.Color := clBlue
  else
    AFont.Color := clRed;
end;

procedure TFormAjusteEstoque.sgProdutosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_DELETE) and (eID.AsInt = 0) then begin
    stValorTotalProdutosGrid.Somar(SFormatDouble(sgProdutos.Cells[coValorTotal, sgProdutos.Row]) * - 1);
    sgProdutos.DeleteRow(sgProdutos.Row);
    FLinha := 0;
    stQuantidadeItensGrid.Caption := NFormat(IIfDbl(sgProdutos.Cells[coProduto, sgProdutos.FixedRows] = '', 0, sgProdutos.RowCount - sgProdutos.FixedRows));

    if (sgProdutos.Cells[coProduto, sgProdutos.FixedRows] = '') then begin
      stEstoqueDisponivelProdutoSelecionadoGrid.Clear;
      stEstoqueFisicoProdutoSelecionadoGrid.Clear;
    end;
  end;
end;

procedure TFormAjusteEstoque.sbZerarEstoqueNegativoClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecProdutosSelecionados>;
  vProdutos: TArray<RecProdutosZerarEstoque>;
  i: Integer;
begin
  inherited;
  vProdutos := _AjustesEstoque.BuscarProdutosEstoqueNegativo(
    Sessao.getConexaoBanco,
    '',
    Sessao.getEmpresaLogada.EmpresaId
  );

  if vRetorno.BuscaCancelada then
    Exit;

  FrMotivoAjusteEstoque.InserirDadoPorChave(999);

  //vProdutos := vRetorno.Dados.Itens;

  for i := Low(vProdutos) to High(vProdutos) do begin
    AdicionarNoProdutoGrid(
      vProdutos[i].ProdutoId,
      vProdutos[i].ProdutoNome,
      vProdutos[i].MarcaNome,
      vProdutos[i].Unidade,
      IIfDbl(vProdutos[i].PrecoUnitario > 0.0, vProdutos[i].PrecoUnitario, 0.10),
      ((IIfDbl(vProdutos[i].PrecoUnitario > 0.0, vProdutos[i].PrecoUnitario, 0.10)) * IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade)),
      vProdutos[i].LocalId,
      vProdutos[i].LocalNome,
      IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade),
      0, //novo estoque fisico
      vProdutos[i].Lote,
      vProdutos[i].DataFabricacao,
      vProdutos[i].DataVencimento,
      IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade), //estoque fisico
      IIfStr(vProdutos[i].Quantidade > 0, 'Sa�da', 'Entrada'),
      IIf(vProdutos[i].Quantidade > 0, 'S', 'E'),
      IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade), //estoque disponivel
      IIfDbl(vProdutos[i].Quantidade < 0, vProdutos[i].Quantidade * -1, vProdutos[i].Quantidade)  //estoque informado
    );
  end;
end;

procedure TFormAjusteEstoque.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrMotivoAjusteEstoque.EstaVazio then begin
    Exclamar('N�o foi informado o motivo do ajuste do estoque, verifique!');
    FrMotivoAjusteEstoque.SetFocus;
    Abort;
  end;

  if SFormatInt(stQuantidadeItensGrid.Caption) = 0 then begin
    Exclamar('N�o foi informado nenhum produto para ajuste, verifique!');
    FrProduto.SetFocus;
    Abort;
  end;
end;

end.
