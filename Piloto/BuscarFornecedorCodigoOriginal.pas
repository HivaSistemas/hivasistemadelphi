unit BuscarFornecedorCodigoOriginal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca,
  EditLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameFornecedores,
  Vcl.Buttons, Vcl.ExtCtrls, _ProdutosFornCodOriginais;

type
  TFormBuscarFornecedorCodigoOriginal = class(TFormHerancaFinalizar)
    FrFornecedor: TFrFornecedores;
    lbllb1: TLabel;
    eCodigoOriginal: TEditLuka;
    procedure FormShow(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(): TRetornoTelaFinalizar<RecProdutosFornCodOriginais>;

implementation

{$R *.dfm}

function Buscar(): TRetornoTelaFinalizar<RecProdutosFornCodOriginais>;
var
  vForm: TFormBuscarFornecedorCodigoOriginal;
begin
  vForm := TFormBuscarFornecedorCodigoOriginal.Create(Application);

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.FornecedorId   := vForm.FrFornecedor.GetFornecedor.cadastro_id;
    Result.Dados.NomeFornecedor := vForm.FrFornecedor.GetFornecedor.cadastro.nome_fantasia;
    Result.Dados.CodigoOriginal := vForm.eCodigoOriginal.Text;
  end;
end;

{ TFormBuscarFornecedorCodigoOriginal }

procedure TFormBuscarFornecedorCodigoOriginal.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(FrFornecedor );
end;

procedure TFormBuscarFornecedorCodigoOriginal.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrFornecedor.EstaVazio then begin
    Exclamar('O fornecedor n�o foi informado corretamente, verifique!');
    SetarFoco(FrFornecedor);
    Abort;
  end;

  if eCodigoOriginal.Trim = '' then begin
    Exclamar('O c�digo original n�o foi informado corretamente, verifique!');
    SetarFoco(eCodigoOriginal);
    Abort;
  end;

end;

end.
