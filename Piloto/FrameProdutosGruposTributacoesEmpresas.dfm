inherited FrProdutosGruposTributacoesEmpresas: TFrProdutosGruposTributacoesEmpresas
  Width = 800
  Height = 332
  ExplicitWidth = 800
  ExplicitHeight = 332
  object sgGrupos: TGridLuka
    Left = 0
    Top = 16
    Width = 800
    Height = 316
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    Align = alClient
    ColCount = 7
    Ctl3D = True
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 3
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goThumbTracking]
    ParentCtl3D = False
    PopupMenu = pmOpcoes
    TabOrder = 0
    OnDblClick = sgGruposDblClick
    OnDrawCell = sgGruposDrawCell
    OnKeyDown = sgGruposKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Sel.?'
      'Empresa'
      'Nome'
      'Grupo trib.estadual compra'
      'Grupo trib.estadual venda'
      'Grupo trib.federal compra'
      'Grupo trib.federal venda')
    OnGetCellPicture = sgGruposGetCellPicture
    Grid3D = False
    RealColCount = 12
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      29
      47
      179
      281
      270
      289
      271)
  end
  object st1: TStaticTextLuka
    Left = 0
    Top = 0
    Width = 800
    Height = 16
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Grupos de tributa'#231#245'es'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object pmOpcoes: TPopupMenu
    Left = 756
    Top = 264
    object miMarcarDesmarcarFocado: TMenuItem
      Caption = 'Marcar/Desmarcar t'#237'tulo focado ( Espa'#231'o )'
      OnClick = miMarcarDesmarcarFocadoClick
    end
    object miMarcarDesmarcarTodos: TMenuItem
      Caption = 'Marcar/Desmarcar todos t'#237'tulos ( CRTL + Espa'#231'o )'
      OnClick = miMarcarDesmarcarTodosClick
    end
    object miN1: TMenuItem
      Caption = '-'
    end
    object miAlterarGruposEmpresaFocada: TMenuItem
      Caption = 'Alterar grupos empresa (Focada)'
      OnClick = miAlterarGruposEmpresaFocadaClick
    end
    object miN3: TMenuItem
      Caption = '-'
    end
    object miAlterarGruposEmpresasSelecionadas: TMenuItem
      Caption = 'Alterar grupos empresas (Selecionadas)'
      OnClick = miAlterarGruposEmpresasSelecionadasClick
    end
  end
end
