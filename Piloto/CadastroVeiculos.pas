unit CadastroVeiculos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Biblioteca,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Mask, MaskEditLuka, ComboBoxLuka, _Sessao, PesquisaVeiculos,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameMotoristas, _Veiculos, _RecordsEspeciais,
  CheckBoxLuka;

type
  TFormCadastroVeiculos = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
    ePlaca: TMaskEditLuka;
    Label2: TLabel;
    eAnoFabricacao: TEditLuka;
    Label6: TLabel;
    cbTipoCombustivel: TComboBoxLuka;
    Label5: TLabel;
    Label3: TLabel;
    eCor: TEditLuka;
    Label4: TLabel;
    eChassi: TEditLuka;
    Label7: TLabel;
    cbTipoVeiculo: TComboBoxLuka;
    Label8: TLabel;
    ePesoMaximo: TEditLuka;
    Label9: TLabel;
    eTara: TEditLuka;
    FrMotorista: TFrMotoristas;
  private
    procedure PreencherRegistro(pVeiculo: RecVeiculos);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroVeiculos }

procedure TFormCadastroVeiculos.BuscarRegistro;
var
  vDados: TArray<RecVeiculos>;
begin
  vDados := _Veiculos.BuscarVeiculos(Sessao.getConexaoBanco, 0, [eId.AsInt], False);
  if vDados = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vDados[0]);
end;

procedure TFormCadastroVeiculos.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormCadastroVeiculos.GravarRegistro(Sender: TObject);
var
  vMotoristaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vMotoristaId := 0;
  if not FrMotorista.EstaVazio then
    vMotoristaId := FrMotorista.getMotorista().CadastroId;

  vRetBanco :=
    _Veiculos.AtualizarVeiculo(
      Sessao.getConexaoBanco,
      eID.AsInt,
      vMotoristaId,
      eNome.Text,
      ToChar(ckAtivo),
      ePlaca.Text,
      eAnoFabricacao.AsInt,
      cbTipoCombustivel.GetValor,
      eChassi.Text,
      eCor.Text,
      cbTipoVeiculo.GetValor,
      ePesoMaximo.AsDouble,
      eTara.AsDouble
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroVeiculos.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eNome,
    ckAtivo,
    ePlaca,
    eAnoFabricacao,
    cbTipoVeiculo,
    cbTipoCombustivel,
    eCor,
    eChassi,
    cbTipoVeiculo,
    ePesoMaximo,
    eTara,
    FrMotorista],
    pEditando
  );

  if pEditando then
    SetarFoco(eNome);
end;

procedure TFormCadastroVeiculos.PesquisarRegistro;
var
  vDados: RecVeiculos;
begin
  vDados := RecVeiculos(PesquisaVeiculos.Pesquisar(False));
  if vDados = nil then
    Exit;

  inherited;
  PreencherRegistro(vDados);
end;

procedure TFormCadastroVeiculos.PreencherRegistro(pVeiculo: RecVeiculos);
begin
  eID.AsInt            := pVeiculo.VeiculoId;
  FrMotorista.InserirDadoPorChave(pVeiculo.MotoristaId);
  eNome.Text           := pVeiculo.Nome;
  ckAtivo.Checked      := pVeiculo.Ativo = 'S';
  ePlaca.Text          := pVeiculo.Placa;
  eAnoFabricacao.AsInt := pVeiculo.Ano;
  cbTipoCombustivel.SetIndicePorValor(pVeiculo.TipoCombustivel);
  eChassi.Text         := pVeiculo.Chasssi;
  eCor.Text            := pVeiculo.Cor;
  cbTipoVeiculo.SetIndicePorValor(pVeiculo.TipoVeiculo);
  ePesoMaximo.AsDouble := pVeiculo.PesoMaximo;
  eTara.AsDouble       := pVeiculo.Tara;
end;

procedure TFormCadastroVeiculos.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('O nome n�o foi informado corretamente, verifique!');
    SetarFoco(eNome);
    Abort;
  end;

  if ePlaca.Text = '___-____' then begin
    _Biblioteca.Exclamar('A placa n�o foi informada corretamente, verifique!');
    SetarFoco(ePlaca);
    Abort;
  end;

  if cbTipoCombustivel.ItemIndex < 0 then begin
    _Biblioteca.Exclamar('O tipo de combust�vel n�o foi informado corretamente, verifique!');
    SetarFoco(cbTipoCombustivel);
    Abort;
  end;

  if cbTipoVeiculo.ItemIndex < 0 then begin
    _Biblioteca.Exclamar('O tipo de ve�culo n�o foi informado corretamente, verifique!');
    SetarFoco(cbTipoVeiculo);
    Abort;
  end;
end;

end.
