unit BuscarDadosSolicitacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  _RecordsCadastros, _Biblioteca, Vcl.StdCtrls, FrameClientes, FrameFuncionarios,
  EditLuka;

type
  TFormBuscarDadosSolicitacao = class(TFormHerancaFinalizar)
    eNumeroSolicitacao: TEditLuka;
    Label1: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosSolicitacoes: TRetornoTelaFinalizar<TArray<Integer>>;

implementation

{$R *.dfm}

function BuscarDadosSolicitacoes: TRetornoTelaFinalizar<TArray<Integer>>;
var
  vForm: TFormBuscarDadosSolicitacao;
begin
  vForm := TFormBuscarDadosSolicitacao.Create(Application);

  if Result.Ok(vForm.ShowModal) then begin
    if vForm.eNumeroSolicitacao.AsInt > 0 then begin
      SetLength(Result.Dados, Length(Result.Dados) + 1);
      Result.Dados[0] := vForm.eNumeroSolicitacao.AsInt;
    end;
  end;

  FreeAndNil(vForm);
end;

end.
