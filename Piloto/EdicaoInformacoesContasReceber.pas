unit EdicaoInformacoesContasReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Sessao, _ContasReceber,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Mask, EditLukaData, EditLuka, _Biblioteca, _RecordsFinanceiros,
  _RecordsEspeciais;

type
  TFormEdicaoInformacoesContasReceber = class(TFormHerancaFinalizar)
    lb6: TLabel;
    eDataVencimento: TEditLukaData;
    lbl18: TLabel;
    eValorDocumento: TEditLuka;
    lb14: TLabel;
    eParcela: TEditLuka;
    eQtdeParcelas: TEditLuka;
    lb1: TLabel;
    procedure FormShow(Sender: TObject);
  private
    FContaReceber: RecContasReceber;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function AlterarDados(pReceberId: Integer): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

function AlterarDados(pReceberId: Integer): TRetornoTelaFinalizar;
var
  vForm: TFormEdicaoInformacoesContasReceber;
  vReceber: TArray<RecContasReceber>;
begin
  Result.RetTela := trCancelado;

  if pReceberId = 0 then
    Exit;

  if not Sessao.AutorizadoRotina('alterar_informacoes_contas_receber') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  vReceber := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 0, [pReceberId]);
  if vReceber = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  if vReceber[0].turno_id > 0 then begin
    _Biblioteca.Exclamar('Este t�tulo n�o pode ser alterado pois pertence ao caixa!');
    Exit;
  end;

//  if Em(vReceber[0].cobranca_id, [Sessao.getParametros.TipoCobrancaGeracaoCredId, Sessao.getParametros.TipoCobRecebEntregaId, Sessao.getParametros.TipoCobAdiantamentoAcuId]) then begin
//    _Biblioteca.Exclamar('N�o � permitido alterar este tipo de documento!');
//    Exit;
//  end;

  vForm := TFormEdicaoInformacoesContasReceber.Create(Application);
  vForm.FContaReceber := vReceber[0];

  vForm.eDataVencimento.AsData := vReceber[0].data_vencimento;
  vForm.eValorDocumento.AsCurr := vReceber[0].ValorDocumento;
  vForm.eParcela.AsInt      := vReceber[0].Parcela;
  vForm.eQtdeParcelas.AsInt := vReceber[0].NumeroParcelas;

  Result.Ok(vForm.ShowModal, True);
end;

{ TFormEdicaoInformacoesContasReceber }

procedure TFormEdicaoInformacoesContasReceber.Finalizar(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco :=
    _ContasReceber.AtualizarInformacoes(
      Sessao.getConexaoBanco,
      FContaReceber.ReceberId,
      eDataVencimento.AsData,
      eValorDocumento.AsCurr,
      eParcela.AsInt,
      eQtdeParcelas.AsInt
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RotinaSucesso;
end;

procedure TFormEdicaoInformacoesContasReceber.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(eDataVencimento);
end;

procedure TFormEdicaoInformacoesContasReceber.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if not eDataVencimento.DataOk then begin
    _Biblioteca.Exclamar('A data de vencimento n�o foi informada corretamente, verifique!');
    SetarFoco(eDataVencimento);
    Abort;
  end;

  if eValorDocumento.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O valor do documento n�o foi informado corretamente, verifique!');
    SetarFoco(eDataVencimento);
    Abort;
  end;

  if eParcela.AsInt = 0 then begin
    _Biblioteca.Exclamar('A parcela n�o foi informada corretamente, verifique!');
    SetarFoco(eParcela);
    Abort;
  end;

  if eQtdeParcelas.AsInt < eParcela.AsInt then begin
    _Biblioteca.Exclamar('A quantidade de parcelas n�o pode ser menor que a parcela, verifique!');
    SetarFoco(eQtdeParcelas);
    Abort;
  end;

  if eValorDocumento.AsCurr <> FContaReceber.ValorDocumento then begin
    if FContaReceber.baixa_origem_id + FContaReceber.BaixaPagarOrigemId > 0 then begin
      _Biblioteca.Exclamar('Um financeiro originado de uma baixa n�o pode ter seu valor alterado!');
      SetarFoco(eValorDocumento);
      Abort;
    end;
  end;
end;

end.
