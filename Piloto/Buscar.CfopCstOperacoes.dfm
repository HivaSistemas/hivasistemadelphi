inherited FormBuscarCfopCstOperacoes: TFormBuscarCfopCstOperacoes
  Caption = 'Busca de CFOP/CST oper.'
  ClientHeight = 126
  ClientWidth = 399
  ExplicitWidth = 405
  ExplicitHeight = 155
  PixelsPerInch = 96
  TextHeight = 14
  object Label1: TLabel [0]
    Left = 5
    Top = 50
    Width = 105
    Height = 14
    Caption = 'Tipo de movimento'
  end
  object Label2: TLabel [1]
    Left = 165
    Top = 50
    Width = 95
    Height = 14
    Caption = 'Tipo de opera'#231#227'o'
  end
  object Label3: TLabel [2]
    Left = 280
    Top = 50
    Width = 18
    Height = 14
    Caption = 'CST'
  end
  inherited pnOpcoes: TPanel
    Top = 89
    Width = 399
    ExplicitTop = 89
    ExplicitWidth = 399
  end
  inline FrCFOP: TFrCFOPs
    Left = 6
    Top = 5
    Width = 385
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 6
    ExplicitTop = 5
    ExplicitWidth = 385
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 360
      Height = 23
      ExplicitWidth = 360
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 385
      ExplicitWidth = 385
      inherited lbNomePesquisa: TLabel
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 280
        ExplicitLeft = 280
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited cbTipoCFOPs: TComboBoxLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited pnPesquisa: TPanel
      Left = 360
      Height = 24
      ExplicitLeft = 360
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object cbTipoMovimento: TComboBoxLuka
    Left = 3
    Top = 63
    Width = 156
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 2
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Venda'
      'Devolu'#231#227'o'
      'Transf.prod. entre emp.'
      'Remessa de conserto'
      'Retorno de entrega'
      'Remessa dep.fechado'
      'Retorno dep.fechado'
      'Devolu'#231#227'o de entrada'
      'Produ'#231#227'o entrada'
      'Produ'#231#227'o sa'#237'da'
      'Ajusta entrada'
      'Ajuste sa'#237'da')
    Valores.Strings = (
      'VIT'
      'DEV'
      'TPE'
      'REM'
      'REN'
      'RDF'
      'RRD'
      'DEN'
      'PEN'
      'PSA'
      'AEE'
      'AES')
    AsInt = 0
  end
  object cbTipoOperacao: TComboBoxLuka
    Left = 165
    Top = 64
    Width = 109
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    ItemIndex = 0
    TabOrder = 3
    Text = 'Interna'
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Interna'
      'Insterestadual'
      'Exterior')
    Valores.Strings = (
      'I'
      'E'
      'X')
    AsInt = 0
    AsString = 'I'
  end
  object cbCST: TComboBoxLuka
    Left = 280
    Top = 64
    Width = 109
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 4
    OnKeyDown = ProximoCampo
    Items.Strings = (
      '00'
      '10'
      '20'
      '30'
      '40'
      '41'
      '50'
      '51'
      '60'
      '70'
      '90')
    AsInt = 0
  end
end
