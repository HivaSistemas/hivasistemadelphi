inherited FormGrupoPrecificacao: TFormGrupoPrecificacao
  Caption = 'Grupo de precifica'#231#227'o entre lojas'
  ClientHeight = 332
  ClientWidth = 419
  OnShow = FormShow
  ExplicitWidth = 425
  ExplicitHeight = 361
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 332
    inherited sbPesquisar: TSpeedButton
      Visible = False
    end
  end
  inline FrEmpresa: TFrEmpresas
    Left = 127
    Top = 7
    Width = 282
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 127
    ExplicitTop = 7
    ExplicitWidth = 282
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 257
      Height = 23
      ExplicitWidth = 393
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 282
      ExplicitWidth = 418
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 177
        ExplicitLeft = 313
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 257
      Height = 24
      ExplicitLeft = 393
      ExplicitHeight = 24
    end
  end
  inline FrEmpresasRetiraAto: TFrameAdicionarEmpresas
    Left = 127
    Top = 58
    Width = 284
    Height = 266
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 127
    ExplicitTop = 58
    ExplicitWidth = 284
    ExplicitHeight = 266
    inherited sgValores: TGridLuka
      Width = 284
      Height = 249
    end
    inherited StaticTextLuka1: TStaticTextLuka
      Width = 284
      Caption = 'Empresas do grupo'
      ExplicitLeft = -2
    end
  end
end
