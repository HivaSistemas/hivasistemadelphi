inherited FormBuscarInformacoesPessoaRecebeuEntrega: TFormBuscarInformacoesPessoaRecebeuEntrega
  Caption = 'Buscar informac'#245'es pessoa recebeu entrega'
  ClientHeight = 245
  ClientWidth = 495
  OnShow = FormShow
  ExplicitWidth = 501
  ExplicitHeight = 274
  PixelsPerInch = 96
  TextHeight = 14
  object lblCPF_CNPJ: TLabel [0]
    Left = 229
    Top = 43
    Width = 18
    Height = 14
    Caption = 'CPF'
  end
  object lbl1: TLabel [1]
    Left = 3
    Top = 43
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  object lbllb12: TLabel [2]
    Left = 380
    Top = 43
    Width = 107
    Height = 14
    Caption = 'Data / hora entrega'
  end
  object lbllb13: TLabel [3]
    Left = 3
    Top = 87
    Width = 65
    Height = 14
    Caption = 'Obseva'#231#245'es'
  end
  object Label1: TLabel [4]
    Left = 3
    Top = 1
    Width = 79
    Height = 14
    Caption = 'Status entrega'
  end
  inherited pnOpcoes: TPanel
    Top = 208
    Width = 495
    TabOrder = 6
    ExplicitTop = 208
    ExplicitWidth = 498
  end
  object eNome: TEditLuka
    Left = 3
    Top = 57
    Width = 212
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCPF: TEditCPF_CNPJ_Luka
    Left = 229
    Top = 57
    Width = 142
    Height = 22
    EditMask = '999.999.999-99'
    MaxLength = 14
    TabOrder = 2
    Text = '   .   .   -  '
    OnKeyDown = ProximoCampo
    Tipo = [tccCPF]
  end
  object eDataEntrega: TEditLukaData
    Left = 380
    Top = 57
    Width = 72
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object eHoraEntrega: TEditHoras
    Left = 451
    Top = 57
    Width = 41
    Height = 22
    EditMask = '!90:00;1;_'
    MaxLength = 5
    TabOrder = 4
    Text = '  :  '
    OnKeyDown = ProximoCampo
  end
  object meObservacoes: TMemo
    Left = 3
    Top = 101
    Width = 489
    Height = 104
    TabOrder = 5
    OnKeyDown = ProximoCampo
  end
  object cbStatusEntrega: TComboBoxLuka
    Left = 3
    Top = 15
    Width = 214
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 0
    OnChange = cbStatusEntregaChange
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Retorno parcial'
      'Retorno total'
      'Entrega total')
    Valores.Strings = (
      'RPA'
      'RTO'
      'ENT')
    AsInt = 0
  end
end
