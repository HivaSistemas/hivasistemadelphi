inherited FrDiversosContatos: TFrDiversosContatos
  Width = 676
  Height = 218
  ExplicitWidth = 676
  ExplicitHeight = 218
  object lb14: TLabel
    Left = 2
    Top = 1
    Width = 46
    Height = 13
    Caption = 'Descri'#231#227'o'
  end
  object lb11: TLabel
    Left = 248
    Top = 1
    Width = 42
    Height = 13
    Caption = 'Telefone'
  end
  object lb1: TLabel
    Left = 443
    Top = 1
    Width = 33
    Height = 13
    Caption = 'Celular'
  end
  object lb9: TLabel
    Left = 248
    Top = 39
    Width = 58
    Height = 13
    Caption = 'Observa'#231#227'o'
  end
  object lb2: TLabel
    Left = 377
    Top = 1
    Width = 29
    Height = 13
    Caption = 'Ramal'
  end
  object lb3: TLabel
    Left = 2
    Top = 39
    Width = 28
    Height = 13
    Caption = 'E-mail'
  end
  object sgOutrosContatos: TGridLuka
    Left = 0
    Top = 77
    Width = 676
    Height = 141
    TabStop = False
    Align = alBottom
    ColCount = 6
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 6
    OnDblClick = sgOutrosContatosDblClick
    OnDrawCell = sgOutrosContatosDrawCell
    OnKeyDown = sgOutrosContatosKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Descri'#231#227'o'
      'Telefone'
      'Ramal'
      'Celular'
      'Email'
      'Observa'#231#227'o')
    Grid3D = False
    RealColCount = 6
    AtivarPopUpSelecao = False
    ColWidths = (
      210
      119
      55
      112
      168
      334)
  end
  object eDescricao: TEditLuka
    Left = 2
    Top = 15
    Width = 238
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 0
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eTelefone: TEditTelefoneLuka
    Left = 248
    Top = 15
    Width = 121
    Height = 21
    EditMask = '(99) 9999-9999;1; '
    MaxLength = 14
    TabOrder = 1
    Text = '(  )     -    '
    OnKeyDown = ProximoCampo
  end
  object eCelular: TEditTelefoneLuka
    Left = 443
    Top = 15
    Width = 117
    Height = 21
    EditMask = '(99) 99999-9999;1; '
    MaxLength = 15
    TabOrder = 3
    Text = '(  )      -    '
    OnKeyDown = ProximoCampo
    Celular = True
  end
  object eRamal: TEditLuka
    Left = 377
    Top = 15
    Width = 58
    Height = 21
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 5
    TabOrder = 2
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmail: TEditLuka
    Left = 2
    Top = 53
    Width = 238
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 4
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eObservacao: TMemo
    Left = 248
    Top = 53
    Width = 424
    Height = 22
    TabOrder = 5
    OnKeyDown = eObservacaoKeyDown
  end
end
