unit GrupoPrecificacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, Vcl.StdCtrls,
  StaticTextLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameEmpresas, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, GridLuka,
  Frame.HerancaInsercaoExclusao, Frame.AdicionarEmpresas, _GruposPrecificacao;

type
  TFormGrupoPrecificacao = class(TFormHerancaCadastro)
    FrEmpresa: TFrEmpresas;
    FrEmpresasRetiraAto: TFrameAdicionarEmpresas;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure EmpresaOnAposPesquisar(Sender: TObject);
  public
    { Public declarations }
  protected
    procedure GravarRegistro(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Modo(pEditando: Boolean); override;
  end;

var
  FormGrupoPrecificacao: TFormGrupoPrecificacao;

implementation

{$R *.dfm}

uses _Sessao, _BibliotecaGenerica, _Biblioteca, _RecordsEspeciais;

{ TFormGrupoPrecificacao }

procedure TFormGrupoPrecificacao.EmpresaOnAposPesquisar(Sender: TObject);

  procedure PreencherGrupos(pFrame: TFrameAdicionarEmpresas; pArrayGrupos: TArray<RecGruposPrecificacao>);
  var
    i: Integer;
  begin
    for i := Low(pArrayGrupos) to High(pArrayGrupos) do
      pFrame.AddEmpresa( pArrayGrupos[i].EmpresaGrupoId, pArrayGrupos[i].NomeFantasia );
  end;

begin
  Modo(True);
  FrEmpresa.Modo(False, False);

  PreencherGrupos(FrEmpresasRetiraAto, _GruposPrecificacao.BuscarGruposPrecificacao(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId]));
end;

procedure TFormGrupoPrecificacao.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresa.OnAposPesquisar := EmpresaOnAposPesquisar;
  FrEmpresa.Modo(True);
end;

procedure TFormGrupoPrecificacao.FormShow(Sender: TObject);
begin
  inherited;
  _BibliotecaGenerica.SetarFoco(FrEmpresa.sgPesquisa);
end;

procedure TFormGrupoPrecificacao.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;

  Sessao.getConexaoBanco.IniciarTransacao;

  vRetorno := _GruposPrecificacao.AtualizarGruposPrecificacao(
    Sessao.getConexaoBanco,
    FrEmpresa.getEmpresa().EmpresaId,
    FrEmpresasRetiraAto.TrazerArrayEmpresas
  );

  if vRetorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  _Biblioteca.Informar(coAlteracaoRegistroSucesso);
  Modo(False);
end;

procedure TFormGrupoPrecificacao.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    FrEmpresasRetiraAto],
    pEditando
  );

  sbGravar.Enabled := pEditando;
  sbDesfazer.Enabled := pEditando;
  if not pEditando then begin
    FrEmpresa.Modo(True);
    _BibliotecaGenerica.SetarFoco(FrEmpresa);
  end;
end;

procedure TFormGrupoPrecificacao.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
