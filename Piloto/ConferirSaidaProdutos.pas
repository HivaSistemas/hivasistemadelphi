unit ConferirSaidaProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca, _EntregasItens,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, Vcl.Grids, GridLuka, _Sessao, _RecordsEspeciais, _RecordsExpedicao,
  Informacoes.Entrega, _ExibirMensagemMemo, _Entregas, _RetiradasItens, _CodigoBarras;

type
  TTipoMovimento = (tpEntrega, tpRetirada);

  TFormConferirSaidaProdutos = class(TFormHerancaFinalizar)
    eCodigoBarras: TEditLuka;
    lb1: TLabel;
    sgItens: TGridLuka;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure eCodigoBarrasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
  private
    FMovimentoId: Integer;
    FTipoMovimento: TTipoMovimento;

    FItens: TArray<RecItemConferencia>;

    function getLinhaProduto(
      const pProdutoId: Integer;
      pLinhaInicial: Integer
    ): Integer;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Conferir(pTipoMovimento: TTipoMovimento; pMovimentoId: Integer): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

const
  coProdutoId       = 0;
  coNomeProduto     = 1;
  coMarca           = 2;
  coQuantidade      = 3;
  coUnidade         = 4;
  coLote            = 5;
  coQtdeConferidos  = 6;
  coQtdeNaoConf     = 7;
  coMultiploVenda   = 8;

  (* Ocultas *)
  coCodigoBarras         = 9;
  coConfSomenteCodBarras = 10;
  coTipoControleEstoque  = 11;

function Conferir(pTipoMovimento: TTipoMovimento; pMovimentoId: Integer): TRetornoTelaFinalizar;
var
  i: Integer;
  vForm: TFormConferirSaidaProdutos;
  vEntregasItens: TArray<RecEntregaItem>;
  vRetiradasItens: TArray<RecRetiradaItem>;
begin
  if pMovimentoId = 0 then
    Exit;

  if pTipoMovimento = tpRetirada then begin
    vRetiradasItens := _RetiradasItens.BuscarRetiradaItens(Sessao.getConexaoBanco, 1, [pMovimentoId]);
    if vRetiradasItens = nil then begin
      Result.RetTela := trOk;
      Exit;
    end;
  end
  else begin
    vEntregasItens := _EntregasItens.BuscarEntregaItens(Sessao.getConexaoBanco, 1, [pMovimentoId]);
    if vEntregasItens = nil then begin
      Result.RetTela := trOk;
      Exit;
    end;
  end;

  vForm := TFormConferirSaidaProdutos.Create(nil);

  if pTipoMovimento = tpRetirada then begin
    SetLength(vForm.FItens, Length(vRetiradasItens));
    for i := Low(vRetiradasItens) to High(vRetiradasItens) do begin
      vForm.FItens[i].ProdutoId                   := vRetiradasItens[i].ProdutoId;
      vForm.FItens[i].ItemId                      := vRetiradasItens[i].ItemId;
      vForm.FItens[i].NomeProduto                 := vRetiradasItens[i].NomeProduto;
      vForm.FItens[i].Marca                       := vRetiradasItens[i].NomeMarca;
      vForm.FItens[i].Quantidade                  := vRetiradasItens[i].Quantidade;
      vForm.FItens[i].Unidade                     := vRetiradasItens[i].Unidade;
      vForm.FItens[i].Lote                        := vRetiradasItens[i].Lote;
      vForm.FItens[i].ConferirSomenteCodigoBarras := vRetiradasItens[i].ConfSomenteCodigoBarras;
      vForm.FItens[i].MultiploVenda               := vRetiradasItens[i].MultiploVenda;
      vForm.FItens[i].CodigoBarras                := vRetiradasItens[i].CodigoBarras;
      vForm.FItens[i].TipoControleEstoque         := vRetiradasItens[i].TipoControleEstoque;
    end;
  end
  else begin
    SetLength(vForm.FItens, Length(vEntregasItens));
    for i := Low(vEntregasItens) to High(vEntregasItens) do begin
      vForm.FItens[i].ProdutoId                   := vEntregasItens[i].ProdutoId;
      vForm.FItens[i].ItemId                      := vEntregasItens[i].ItemId;
      vForm.FItens[i].NomeProduto                 := vEntregasItens[i].NomeProduto;
      vForm.FItens[i].Marca                       := vEntregasItens[i].NomeMarca;
      vForm.FItens[i].Quantidade                  := vEntregasItens[i].Quantidade;
      vForm.FItens[i].Unidade                     := vEntregasItens[i].Unidade;
      vForm.FItens[i].Lote                        := vEntregasItens[i].Lote;
      vForm.FItens[i].ConferirSomenteCodigoBarras := vEntregasItens[i].ConfSomenteCodigoBarras;
      vForm.FItens[i].MultiploVenda               := vEntregasItens[i].MultiploVenda;
      vForm.FItens[i].CodigoBarras                := vEntregasItens[i].CodigoBarras;
      vForm.FItens[i].TipoControleEstoque         := vEntregasItens[i].TipoControleEstoque;
    end;
  end;

  vForm.FMovimentoId   := pMovimentoId;
  vForm.FTipoMovimento := pTipoMovimento;

  for i := Low(vForm.FItens) to High(vForm.FItens) do begin
    vForm.sgItens.Cells[coProdutoId, i + 1]            := NFormat(vForm.FItens[i].ProdutoId);
    vForm.sgItens.Cells[coNomeProduto, i + 1]          := vForm.FItens[i].NomeProduto;
    vForm.sgItens.Cells[coMarca, i + 1]                := vForm.FItens[i].Marca;
    vForm.sgItens.Cells[coQuantidade, i + 1]           := NFormatNEstoque(vForm.FItens[i].Quantidade);
    vForm.sgItens.Cells[coUnidade, i + 1]              := vForm.FItens[i].Unidade;
    vForm.sgItens.Cells[coLote, i + 1]                 := vForm.FItens[i].Lote;
    vForm.sgItens.Cells[coQtdeNaoConf, i + 1]          := NFormatNEstoque(vForm.FItens[i].Quantidade);
    vForm.sgItens.Cells[coCodigoBarras, i + 1]         := vForm.FItens[i].CodigoBarras;
    vForm.sgItens.Cells[coConfSomenteCodBarras, i + 1] := vForm.FItens[i].ConferirSomenteCodigoBarras;
    vForm.sgItens.Cells[coMultiploVenda, i + 1]        := NFormatNEstoque(vForm.FItens[i].MultiploVenda);
    vForm.sgItens.Cells[coTipoControleEstoque, i + 1]  := vForm.FItens[i].TipoControleEstoque;
  end;
  vForm.sgItens.SetLinhasGridPorTamanhoVetor( Length(vForm.FItens) );

  Result.Ok(vForm.ShowModal, True);
end;

{ TFormConferirSaidaProdutos }

procedure TFormConferirSaidaProdutos.eCodigoBarrasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  vPosic: Integer;
  vQtdeGrid: Currency;
  vProdutoId: Integer;
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eCodigoBarras.Trim = '' then begin
    _ExibirMensagemMemo.Mensagem('Conf. de sa�da de produtos', 'Nenhum c�digo de barras v�lido foi d�gitado!', [], True);
    SetarFoco(eCodigoBarras);
    Exit;
  end;

  vProdutoId := _CodigoBarras.getProdutoId(eCodigoBarras.Text);

  vPosic := getLinhaProduto(vProdutoId, -1);
  if vPosic = -1 then
    Exit;

  vQtdeGrid := SFormatCurr(sgItens.Cells[coQtdeConferidos, vPosic]);

  vQtdeGrid := vQtdeGrid + 1;
  sgItens.Cells[coQtdeConferidos, vPosic] := NFormatNEstoque(vQtdeGrid);
  sgItens.Row := vPosic;

  eCodigoBarras.Clear;
  SetarFoco(eCodigoBarras);

  sgItens.Cells[coQtdeNaoConf, vPosic] :=
    NFormatNEstoque(SFormatDouble(sgItens.Cells[coQuantidade, vPosic]) - SFormatDouble(sgItens.Cells[coQtdeConferidos, vPosic]));
end;

procedure TFormConferirSaidaProdutos.Finalizar(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  if FTipoMovimento = tpEntrega then begin
    for i := 1 to sgItens.RowCount -1 do
      FItens[i - 1].NaoSeparados := SFormatDouble( sgItens.Cells[coQtdeNaoConf, i] );

    if FTipoMovimento = tpRetirada then begin


    end
    else begin
      vRetBanco :=
        _Entregas.AtualizarConfereciaProdutos(
          Sessao.getConexaoBanco,
          FMovimentoId,
          FItens
        );
    end;
  end;

  Sessao.AbortarSeHouveErro(vRetBanco);
end;

procedure TFormConferirSaidaProdutos.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(eCodigoBarras);
  sgItens.Col := coQtdeConferidos;
end;

function TFormConferirSaidaProdutos.getLinhaProduto(
  const pProdutoId: Integer;
  pLinhaInicial: Integer
): Integer;
var
  i: Integer;
  vQtdeGrid: Currency;

  vLinha: Integer;
  vLinhaInicial: Integer;
  vLinhaAnterior: Integer;
begin
  Result := -1;
  vLinha := -1;
  vLinhaInicial := IIfInt(pLinhaInicial = -1, 1, pLinhaInicial);
  for i := vLinhaInicial to sgItens.RowCount -1 do begin
    if sgItens.Cells[coProdutoId, i] = NFormat(pProdutoId) then begin
      vLinha := i;
      Break;
    end;
  end;

  // Se n�o encontrou o produto verificando se o c�digo de barras � o c�digo do produto
  if vLinha = -1 then begin
    for i := vLinhaInicial to sgItens.RowCount -1 do begin
      if sgItens.Cells[coProdutoId, i] = NFormat(pProdutoId) then begin
        vLinha := i;
        Break;
      end;
    end;

    if vLinha = -1 then begin
      if pLinhaInicial > -1 then
        Exit;

      _ExibirMensagemMemo.Mensagem('Conf. de sa�da de produtos', 'Produto n�o encontrado!', [], True);
      SetarFoco(eCodigoBarras);
      eCodigoBarras.Clear;
      Exit;
    end;
  end;

  vQtdeGrid := SFormatCurr(sgItens.Cells[coQtdeConferidos, vLinha]);

  // Se a quantidade for igual ao que j� foi conferido e o produto tiver controle por lote ou algo que gere ele diversas vezes
  // buscando a partir da ultima linha que o encontrou se h� outros n�o conferidos
  if (vQtdeGrid < SFormatCurr(sgItens.Cells[coQuantidade, vLinha])) then
    Result := vLinha
  else if sgItens.Cells[coTipoControleEstoque, vLinha] = 'N' then begin
    _ExibirMensagemMemo.Mensagem('Conf. de sa�da de produtos', 'O produto ' + sgItens.Cells[coNomeProduto, vLinha] + ' j� foi completamente conferido!', [], True);
    SetarFoco(eCodigoBarras);
    eCodigoBarras.Clear;
    Exit;
  end
  else begin
    vLinhaAnterior := vLinha;
    vLinha := getLinhaProduto(pProdutoId, vLinha + 1);

    if vLinha = -1 then begin
      _ExibirMensagemMemo.Mensagem('Conf. de sa�da de produtos', 'O produto ' + sgItens.Cells[coNomeProduto, vLinhaAnterior] + ' j� foi completamente conferido!', [], True);
      SetarFoco(eCodigoBarras);
      eCodigoBarras.Clear;
      Result := -2;
      Exit;
    end;

    if vLinha = -2 then
      Exit;

    Result := vLinha;
  end;
end;

procedure TFormConferirSaidaProdutos.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coQtdeConferidos then begin
    TextCell := NFormatNEstoque( SFormatDouble(TextCell) );
    if TextCell = '' then begin
      sgItens.Cells[coQtdeNaoConf, ARow] := sgItens.Cells[coQuantidade, ARow];
      Exit;
    end;

    if not ValidarMultiplo(SFormatDouble(sgItens.Cells[ACol, ARow]), SFormatDouble(sgItens.Cells[coMultiploVenda, ARow])) then begin
      sgItens.Cells[coQtdeNaoConf, ARow] := sgItens.Cells[coQuantidade, ARow];
      TextCell := '';
      Exit;
    end;

    sgItens.Cells[coQtdeNaoConf, ARow] := NFormatNEstoque(SFormatDouble(sgItens.Cells[coQuantidade, ARow]) - SFormatDouble(sgItens.Cells[ACol, ARow]));
  end;
end;

procedure TFormConferirSaidaProdutos.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coProdutoId, coQuantidade, coQtdeConferidos, coQtdeNaoConf] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormConferirSaidaProdutos.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coQtdeConferidos then begin
    if sgItens.Cells[coConfSomenteCodBarras, ARow] = 'N' then begin
      ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
      AFont.Color  := _Biblioteca.coCorFonteEdicao3;
    end;
  end
  else if ACol = coQtdeNaoConf then
    AFont.Color := clRed;
end;

procedure TFormConferirSaidaProdutos.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ARow = 0 then
    Exit;

  CanSelect := ACol = coQtdeConferidos;
  if not CanSelect then
    Exit;

  if sgItens.Cells[coConfSomenteCodBarras, ARow] = 'S' then
    sgItens.Options := sgItens.Options - [goEditing]
  else
    sgItens.Options := sgItens.Options + [goEditing];
end;

procedure TFormConferirSaidaProdutos.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  vTemItensQtde: Boolean;
  vTemItensParcialmenteConferidos: Boolean;
begin
  inherited;

  vTemItensQtde := False;
  vTemItensParcialmenteConferidos := False;
  for i := 1 to sgItens.RowCount -1 do begin
    if SFormatCurr(sgItens.Cells[coQtdeConferidos, i]) > SFormatCurr(sgItens.Cells[coQuantidade, i]) then begin
      _Biblioteca.Exclamar('A quantidade de conferidos � maior que a quantidade da entrega, por favor verifique!');
      SetarFoco(sgItens);
      sgItens.Row := i;
      Abort;
    end;

    if not vTemItensQtde then
      vTemItensQtde := SFormatCurr(sgItens.Cells[coQuantidade, i]) > SFormatCurr(sgItens.Cells[coQtdeNaoConf, i]);

    if not vTemItensParcialmenteConferidos then
      vTemItensParcialmenteConferidos := SFormatCurr(sgItens.Cells[coQtdeNaoConf, i]) > 0;
  end;

  // Se todos os produtos est�o como n�o conferidos n�o passar para frente
  if not vTemItensQtde then begin
    _Biblioteca.Exclamar('Nenhum produto foi conferido, por favor verifique!');
    Abort;
  end;

  if vTemItensParcialmenteConferidos then begin
    if FTipoMovimento = tpRetirada then begin
      _Biblioteca.Exclamar('Existem produtos que n�o foram totalmente conferidos, por favor verifique!');
      Abort;
    end;

    if
      not _Biblioteca.Perguntar(
        'Existem produtos que foram parcialmente conferidos, caso continue estes produtos voltar�o para pend�ncia de entrega para, deseja continuar?'
      )
    then
      Abort;
  end;
end;

end.
