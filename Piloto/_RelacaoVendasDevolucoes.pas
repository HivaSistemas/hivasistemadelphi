unit _RelacaoVendasDevolucoes;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsRelatorios;

function BuscarVendasDevolucoes(pConexao: TConexao; pComando: string): TArray<RecOrcamentosVendas>;
function BuscarProdutosOrcamentos(pConexao: TConexao; const pComando: string): TArray<RecProdutoOrcamento>;
function BuscarFaturamentoPeriodoFuncionario(pConexao: TConexao; pComando: string): TArray<RecOrcamentosVendas>;

implementation

function BuscarFaturamentoPeriodoFuncionario(
  pConexao: TConexao;
  pComando: string
): TArray<RecOrcamentosVendas>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select  ');
    Add('  SUM(VALOR_TOTAL) as VALOR_TOTAL, ');
    Add('  VENDEDOR_ID ');
    Add('from ( ');
    Add('  select ');
    Add('    VALOR_LIQUIDO * -1 as VALOR_TOTAL, ');
    Add('    DATA_HORA_DEVOLUCAO as DATA_MOVIMENTO, ');
    Add('    VENDEDOR_ID ');
    Add('  from VW_RELACAO_DEVOLUCOES ');

    Add('  union all ');

    Add('  select ');
    Add('    VALOR_TOTAL, ');
    Add('    DATA_HORA_RECEBIMENTO as DATA_MOVIMENTO, ');
    Add('    VENDEDOR_ID ');
    Add('  from VW_RELACAO_ORCAMENTOS_VENDAS ');
    Add(') MOVIMENTOS ');

    Add(pComando);

    Add(' GROUP BY VENDEDOR_ID ');
  end;

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ValorTotal := vSql.GetDouble('VALOR_TOTAL');
      Result[i].VendedorId := vSql.GetInt('VENDEDOR_ID');

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarVendasDevolucoes(pConexao: TConexao; pComando: string): TArray<RecOrcamentosVendas>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select  ');
    Add('  ORCAMENTO_ID, ');
    Add('  DEVOLUCAO_ID, ');
    Add('  TIPO_MOVIMENTO, ');
    Add('  CLIENTE_ID, ');
    Add('  NOME_CLIENTE, ');
    Add('  VALOR_TOTAL_PRODUTOS, ');
    Add('  VALOR_OUTRAS_DESPESAS, ');
    Add('  VALOR_DESCONTO, ');
    Add('  VALOR_FRETE, ');
    Add('  VALOR_TOTAL, ');
    Add('  DATA_MOVIMENTO, ');
    Add('  EMPRESA_ID, ');
    Add('  NOME_EMPRESA, ');
    Add('  CONDICAO_ID, ');
    Add('  NOME_CONDICAO_PAGAMENTO, ');
    Add('  VENDEDOR_ID, ');
    Add('  NOME_FUNCIONARIO, ');
    Add('  VALOR_CUSTO_ENTRADA, ');
    Add('  VALOR_CUSTO_FINAL, ');
    Add('  VALOR_DINHEIRO, ');
    Add('  VALOR_CHEQUE, ');
    Add('  VALOR_CARTAO_DEBITO, ');
    Add('  VALOR_CARTAO_CREDITO, ');
    Add('  VALOR_COBRANCA, ');
    Add('  VALOR_FINANCEIRA, ');
    Add('  VALOR_ACUMULATIVO, ');
    Add('  PARCEIRO_ID, ');
    Add('  PARCEIRO_NOME, ');
    Add('  VALOR_CREDITO, ');
    Add('  TRUNC(DATA_MOVIMENTO) as DATA_AGRUPAMENTO ');
    Add('from ( ');
    Add('  select ');
    Add('    ORCAMENTO_ID, ');
    Add('    DEVOLUCAO_ID, ');
    Add('    ''DEV'' as TIPO_MOVIMENTO, ');
    Add('    CLIENTE_ID, ');
    Add('    NOME_CLIENTE, ');
    Add('    VALOR_BRUTO as VALOR_TOTAL_PRODUTOS, ');
    Add('    VALOR_OUTRAS_DESPESAS, ');
    Add('    VALOR_DESCONTO, ');
    Add('    VALOR_FRETE, ');
    Add('    VALOR_LIQUIDO as VALOR_TOTAL, ');
    Add('    DATA_HORA_DEVOLUCAO as DATA_MOVIMENTO, ');
    Add('    EMPRESA_ID, ');
    Add('    NOME_EMPRESA, ');
    Add('    CONDICAO_ID, ');
    Add('    NOME_CONDICAO_PAGAMENTO, ');
    Add('    VENDEDOR_ID, ');
    Add('    NOME_VENDEDOR as NOME_FUNCIONARIO, ');
    Add('    VALOR_CUSTO_ENTRADA, ');
    Add('    VALOR_CUSTO_FINAL, ');
    Add('    VALOR_DINHEIRO, ');
    Add('    VALOR_CHEQUE, ');
    Add('    VALOR_CARTAO_DEBITO, ');
    Add('    VALOR_CARTAO_CREDITO, ');
    Add('    VALOR_COBRANCA, ');
    Add('    VALOR_FINANCEIRA, ');
    Add('    VALOR_ACUMULATIVO, ');
    Add('    VALOR_CREDITO, ');
    Add('    DATA_ENTREGA, ');
    Add('    DATA_HORA_RECEBIMENTO, ');
    Add('    TIPO_ANALISE_CUSTO, ');
    Add('    STATUS, ');
    Add('    TURNO_ID, ');
    Add('    PARCEIRO_NOME, ');
    Add('    PARCEIRO_ID, ');
    Add('    VALOR_PIX, ');
    Add('    ORC_POR_AMBIENTE ');
    Add('  from VW_RELACAO_DEVOLUCOES ');

    Add('  union all ');

    Add('  select ');
    Add('    ORCAMENTO_ID, ');
    Add('    0 as DEVOLUCAO_ID, ');
    Add('    ''VEN'' as TIPO_MOVIMENTO, ');
    Add('    CLIENTE_ID, ');
    Add('    NOME_CLIENTE, ');
    Add('    VALOR_TOTAL_PRODUTOS, ');
    Add('    VALOR_OUTRAS_DESPESAS, ');
    Add('    VALOR_DESCONTO, ');
    Add('    VALOR_FRETE, ');
    Add('    VALOR_TOTAL, ');
    Add('    DATA_HORA_RECEBIMENTO as DATA_MOVIMENTO, ');
    Add('    EMPRESA_ID, ');
    Add('    NOME_EMPRESA, ');
    Add('    CONDICAO_ID, ');
    Add('    NOME_CONDICAO_PAGAMENTO, ');
    Add('    VENDEDOR_ID, ');
    Add('    NOME_VENDEDOR as NOME_FUNCIONARIO, ');
    Add('    VALOR_CUSTO_ENTRADA, ');
    Add('    VALOR_CUSTO_FINAL, ');
    Add('    VALOR_DINHEIRO, ');
    Add('    VALOR_CHEQUE, ');
    Add('    VALOR_CARTAO_DEBITO, ');
    Add('    VALOR_CARTAO_CREDITO, ');
    Add('    VALOR_COBRANCA, ');
    Add('    VALOR_FINANCEIRA, ');
    Add('    VALOR_ACUMULATIVO, ');
    Add('    VALOR_CREDITO, ');
    Add('    DATA_ENTREGA, ');
    Add('    DATA_HORA_RECEBIMENTO, ');
    Add('    TIPO_ANALISE_CUSTO, ');
    Add('    STATUS, ');
    Add('    TURNO_ID, ');
    Add('    PARCEIRO_NOME, ');
    Add('    PARCEIRO_ID, ');
    Add('    VALOR_PIX, ');
    Add('    ORC_POR_AMBIENTE ');
    Add('  from VW_RELACAO_ORCAMENTOS_VENDAS ');
    Add(') MOVIMENTOS ');

//    Add('order by DATA_MOVIMENTO asc ');

    Add(pComando);
  end;

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].OrcamentoId           := vSql.GetInt('ORCAMENTO_ID');
      Result[i].DevolucaoId           := vSql.GetInt('DEVOLUCAO_ID');
      Result[i].ClienteId             := vSql.GetInt('CLIENTE_ID');
      Result[i].NomeCliente           := vSql.GetString('NOME_CLIENTE');
//      Result[i].DataCadastro          := vSql.GetData('DATA_CADASTRO');
      Result[i].VendedorId            := vSql.GetInt('VENDEDOR_ID');
      Result[i].NomeVendedor          := vSql.GetString('NOME_FUNCIONARIO');
      Result[i].CondicaoId            := vSql.GetInt('CONDICAO_ID');
      Result[i].NomeCondicaoPagamento := vSql.GetString('NOME_CONDICAO_PAGAMENTO');
      Result[i].ValorTotalProdutos    := vSql.GetDouble('VALOR_TOTAL_PRODUTOS');
      Result[i].ValorOutrasDespesas   := vSql.GetDouble('VALOR_OUTRAS_DESPESAS');
      Result[i].ValorFrete            := vSql.GetDouble('VALOR_FRETE');
      Result[i].ValorDesconto         := vSql.GetDouble('VALOR_DESCONTO');
      Result[i].ValorTotal            := vSql.GetDouble('VALOR_TOTAL');
      Result[i].EmpresaId             := vSql.GetInt('EMPRESA_ID');
      Result[i].NomeEmpresa           := vSql.GetString('NOME_EMPRESA');
      Result[i].ParceiroId            := vSql.GetInt('PARCEIRO_ID');
      Result[i].ParceiroNome          := vSql.GetString('PARCEIRO_NOME');
//      Result[i].tipoAnaliseCusto      := vSql.GetString('TIPO_ANALISE_CUSTO');
//      Result[i].IndiceDescontoVendaId := vSql.GetInt('INDICE_DESCONTO_VENDA_ID');
      Result[i].ValorCustoEntrada     := vSql.GetDouble('VALOR_CUSTO_ENTRADA');
      Result[i].ValorCustoFinal       := vSql.GetDouble('VALOR_CUSTO_FINAL');
//      Result[i].Status                := vSql.GetString('STATUS');
      Result[i].DataHoraRecebimento   := vSql.GetData('DATA_MOVIMENTO');
      Result[i].DataAgrupamento       := vSql.GetData('DATA_AGRUPAMENTO');
//      Result[i].ClienteGrupoId        := vSql.GetInt('CLIENTE_GRUPO_ID');
//      Result[i].NomeGrupoCliente      := vSql.GetString('NOME_GRUPO_CLIENTE');
      Result[i].TipoMovimento         := vSql.GetString('TIPO_MOVIMENTO');

      if Result[i].TipoMovimento  = 'DEV' then
        Result[i].TipoMovimentoAnalitico := 'Devolu��o'
      else if Result[i].TipoMovimento  = 'VEN' then
        Result[i].TipoMovimentoAnalitico := 'Venda';

//      if Result[i].Status  = 'OE' then
//        Result[i].StatusAnalitico := 'Aberto'
//      else if Result[i].Status  = 'OB' then
//        Result[i].StatusAnalitico := 'Bloqueado'
//      else if Result[i].Status = 'VB' then
//        Result[i].StatusAnalitico := 'Venda bloqueada'
//      else if Result[i].Status = 'VR' then
//        Result[i].StatusAnalitico := 'Aguardando recebimento'
//      else if Result[i].Status = 'VE' then
//        Result[i].StatusAnalitico := 'Aguard. recebimento entrega'
//      else if Result[i].Status = 'RE' then
//        Result[i].StatusAnalitico := 'Recebido'
//      else if Result[i].Status = 'CA' then
//        Result[i].StatusAnalitico := 'Cancelado';

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

function BuscarProdutosOrcamentos(pConexao: TConexao; const pComando: string): TArray<RecProdutoOrcamento>;
var
  i: Integer;
  vSql: TConsulta;
begin
  Result := nil;
  vSql := TConsulta.Create(pConexao);

  with vSql.SQL do begin
    Add('select ');
    Add('  PRODUTO_ID, ');
    Add('  ITEM_ID, ');
    Add('  NOME_PRODUTO, ');
    Add('  MARCA_ID, ');
    Add('  NOME_MARCA, '); // 4
    Add('  PRECO_UNITARIO, ');
    Add('  QUANTIDADE, ');
    Add('  UNIDADE_VENDA, ');
    Add('  VALOR_TOTAL, ');
    Add('  ORCAMENTO_ID ');
    Add('from ');
    Add('  VW_ORCAMENTOS_ITENS_IMPRESSAO ');
    Add(pComando);
  end;

  if vSql.Pesquisar then begin
    SetLength(Result, vSql.GetQuantidadeRegistros);
    for i := 0 to vSql.GetQuantidadeRegistros - 1 do begin
      Result[i].ProdutoId     := vSql.GetInt(0);
      Result[i].ItemId        := vSql.GetInt(1);
      Result[i].NomeProduto   := vSql.GetString(2);
      Result[i].MarcaId       := vSql.GetInt(3);
      Result[i].NomeMarca     := vSql.GetString(4);
      Result[i].PrecoUnitario := vSql.GetDouble(5);
      Result[i].Quantidade    := vSql.GetDouble(6);
      Result[i].UnidadeVenda  := vSql.GetString(7);
      Result[i].ValorTotal    := vSql.GetDouble(8);
      Result[i].OrcamentoId   := vSql.GetInt(9);

      vSql.Next;
    end;
  end;

  vSql.Active := False;
  vSql.Free;
end;

end.
