unit BuscarDadosCartoesTEF;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _RecordsOrcamentosVendas,
  Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Grids, GridLuka, _Biblioteca, System.Math, _ComunicacaoTEF,
  System.StrUtils, _Sessao, _RecordsFinanceiros, SelecionarVariosOpcoes, BuscarTiposCobrancaCartoes,
  _TiposCobranca, _RecordsCadastros, Vcl.Menus, _RecordsEspeciais, _BibliotecaGenerica,
  _GerenciadorCartaoTEF;

type
  TFormBuscarDadosCartoesTEF = class(TFormHerancaFinalizar)
    sgCartoesPassar: TGridLuka;
    stCaracteristicas: TStaticText;
    st4: TStaticText;
    stValorReceber: TStaticText;
    st2: TStaticText;
    stValorAprovados: TStaticText;
    stValorDiferenca: TStaticText;
    st5: TStaticText;
    stValorCartoesInseridos: TStaticText;
    st3: TStaticText;
    pmOpcoes: TPopupMenu;
    miAdicionarCartao: TMenuItem;
    miAlterarFormaRecebimento: TMenuItem;
    miN1: TMenuItem;
    miPassarCartao: TMenuItem;
    miN2: TMenuItem;
    miAlterarTipoCobranca: TMenuItem;
    miN3: TMenuItem;
    miDeletarCartao: TMenuItem;
    miN4: TMenuItem;
    procedure sgCartoesPassarDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure sgCartoesPassarGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgCartoesPassarSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgCartoesPassarArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sbDefinirTipoCobrancaClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure miAdicionarCartaoClick(Sender: TObject);
    procedure miPassarCartaoClick(Sender: TObject);
    procedure miAlterarTipoCobrancaClick(Sender: TObject);
    procedure miDeletarCartaoClick(Sender: TObject);
    procedure miAlterarFormaRecebimentoClick(Sender: TObject);
  private
    FTEF: TTEFDiscado;
    FCartoes: TArray<RecRespostaTEF>;

    FQtdeMaximaParcelas: Integer;
    FCartaoDebitoLiberado: Boolean;
    FCartaoCreditoLiberado: Boolean;

    procedure CalcularDiferenca;
    procedure VerificarValores;
//    function VerificarUltimoCartao: Boolean;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BuscarPagamentosTEF(
  pValorDebito: Double;
  pValorCredito: Double;
  pQtdeMaximaParcelas: Integer;
  pCartaoDebitoLiberado: Boolean;
  pCartaoCreditoLiberado: Boolean
): TRetornoTelaFinalizar< TArray<RecRespostaTEF> >;

procedure CancelarCartoes(pCartoes: TArray<RecRespostaTEF>);

implementation

{$R *.dfm}

const
  coTipoRecebimento  = 0;
  coTipoCartao       = 1;
  coQtdeParcelas     = 2;
  coCobrancaId       = 3;
  coTipoCobranca     = 4;
  coValor            = 5;
  coStatus           = 6;

  coAprovado = 'Aprovado';

function BuscarPagamentosTEF(
  pValorDebito: Double;
  pValorCredito: Double;
  pQtdeMaximaParcelas: Integer;
  pCartaoDebitoLiberado: Boolean;
  pCartaoCreditoLiberado: Boolean
): TRetornoTelaFinalizar< TArray<RecRespostaTEF> >;
var
  vLinha: Integer;
  vForm: TFormBuscarDadosCartoesTEF;
begin
  vForm := TFormBuscarDadosCartoesTEF.Create(Application);

  vForm.stValorReceber.Caption := _Biblioteca.NFormat(pValorDebito + pValorCredito);

  vLinha := 0;
  if pValorDebito > 0 then begin
    Inc(vLinha);

    vForm.sgCartoesPassar.Cells[coTipoRecebimento, vLinha] := 'TEF';
    vForm.sgCartoesPassar.Cells[coTipoCartao, vLinha]      := 'D�bito';
    vForm.sgCartoesPassar.Cells[coQtdeParcelas, vLinha]    := '';
    vForm.sgCartoesPassar.Cells[coValor, vLinha]           := _Biblioteca.NFormatN(pValorDebito);
    vForm.sgCartoesPassar.Cells[coStatus, vLinha]          := 'Pendente';

    SetLength(vForm.FCartoes, Length(vForm.FCartoes) + 1);
  end;

  if pValorCredito > 0 then begin
    Inc(vLinha);

    vForm.sgCartoesPassar.Cells[coTipoRecebimento, vLinha] := 'TEF';
    vForm.sgCartoesPassar.Cells[coTipoCartao, vLinha]      := 'Cr�dito';
    vForm.sgCartoesPassar.Cells[coQtdeParcelas, vLinha]    := _Biblioteca.NFormatN(pQtdeMaximaParcelas);
    vForm.sgCartoesPassar.Cells[coValor, vLinha]           := _Biblioteca.NFormatN(pValorCredito);
    vForm.sgCartoesPassar.Cells[coStatus, vLinha]          := 'Pendente';

    SetLength(vForm.FCartoes, Length(vForm.FCartoes) + 1);
  end;

  vForm.FQtdeMaximaParcelas       := pQtdeMaximaParcelas;
  vForm.sgCartoesPassar.SetLinhasGridPorTamanhoVetor( vLinha );
  vForm.CalcularDiferenca;

  vForm.FCartaoDebitoLiberado  := pCartaoDebitoLiberado;
  vForm.FCartaoCreditoLiberado := pCartaoCreditoLiberado;

  Result.Ok(vForm.ShowModal);
  Result.Dados := vForm.FCartoes;

  vForm.Free;
end;

procedure TFormBuscarDadosCartoesTEF.CalcularDiferenca;
var
  i: Integer;
  vTotalAprovados: Double;
  vTotalInseridos: Double;
begin
  vTotalAprovados := 0;
  vTotalInseridos := 0;
  for i := 1 to sgCartoesPassar.RowCount -1 do begin
    if sgCartoesPassar.Cells[coStatus, i] = coAprovado then
      vTotalAprovados := vTotalAprovados + SFormatDouble(sgCartoesPassar.Cells[coValor, i]);

    vTotalInseridos := vTotalInseridos + SFormatDouble(sgCartoesPassar.Cells[coValor, i]);
  end;
  stValorAprovados.Caption        := _Biblioteca.NFormat(vTotalAprovados);
  stValorCartoesInseridos.Caption := _Biblioteca.NFormat(vTotalInseridos);
  stValorDiferenca.Caption        := _Biblioteca.NFormatN(SFormatDouble(stValorReceber.Caption) - vTotalInseridos);
end;

procedure TFormBuscarDadosCartoesTEF.FormCreate(Sender: TObject);
var
  vGerenciador: TArray<RecGerenciadorCartaoTEF>;
begin
  inherited;

  vGerenciador := _GerenciadorCartaoTEF.BuscarGerenciadorCartaoTEF(Sessao.getConexaoBanco, 0, [Sessao.getParametrosEstacao.GerenciadorCartaoId]);

  FTEF :=
    TTEFDiscado.Create(
      Self,
      Sessao.getParametrosEstacao.DiretorioArquivosRequisicao,
      Sessao.getParametrosEstacao.DiretorioArquivosResposta,
      vGerenciador[0].TextoProcurarBandeira1,
      vGerenciador[0].PosicaoInicialProcBand1,
      vGerenciador[0].QtdeCaracteresCopiarBand1,
      vGerenciador[0].TextoProcurarBandeira2,
      vGerenciador[0].PosicaoInicialProcBand2,
      vGerenciador[0].QtdeCaracteresCopiarBand2,
      vGerenciador[0].TextoProcurarRede1,
      vGerenciador[0].PosicaoInicialProcRede1,
      vGerenciador[0].QtdeCaracteresCopiarRede1,
      vGerenciador[0].TextoProcurarRede2,
      vGerenciador[0].PosicaoInicialProcRede2,
      vGerenciador[0].QtdeCaracteresCopiarRede2,
      vGerenciador[0].TextoProcurarNumeroCartao1,
      vGerenciador[0].PosicIniProcNumeroCartao1,
      vGerenciador[0].QtdeCaracCopiarNrCartao1,
      vGerenciador[0].TextoProcurarNumeroCartao2,
      vGerenciador[0].PosicIniProcNumeroCartao2,
      vGerenciador[0].QtdeCaracCopiarNrCartao2,
      vGerenciador[0].TextoProcurarNsu1,
      vGerenciador[0].PosicaoIniProcurarNsu1,
      vGerenciador[0].QtdeCaracCopiarNsu1,
      vGerenciador[0].TextoProcurarNsu2,
      vGerenciador[0].PosicaoIniProcurarNsu2,
      vGerenciador[0].QtdeCaracCopiarNsu2,
      vGerenciador[0].TextoProcurarCodAutoriz1,
      vGerenciador[0].PosicaoIniProCodAutoriz1,
      vGerenciador[0].QtdeCaracCopCodAutoriz1,
      vGerenciador[0].TextoProcurarCodAutoriz2,
      vGerenciador[0].PosicaoIniProCodAutoriz2,
      vGerenciador[0].QtdeCaracCopCodAutoriz2
    );
end;

procedure TFormBuscarDadosCartoesTEF.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_F1: miAdicionarCartao.Click;
    VK_F2: miAlterarFormaRecebimento.Click;
    VK_F5: miPassarCartao.Click;
    VK_F6: miAlterarTipoCobranca.Click;
  end;
end;

procedure TFormBuscarDadosCartoesTEF.miAdicionarCartaoClick(Sender: TObject);
var
  vLinha: Integer;
  vOpcao: TRetornoTelaFinalizar<Integer>;
begin
  inherited;
  if stValorAprovados.Caption = stValorReceber.Caption then begin
    _Biblioteca.Exclamar('Todos os cart�es j� foram aprovados, n�o � permitido inserir um novo cart�o!');
    Exit;
  end;

  if FCartaoCreditoLiberado and FCartaoDebitoLiberado then begin
    vOpcao := SelecionarVariosOpcoes.Selecionar('Escolha', ['D�bito', 'Cr�dito'], 1);
    if vOpcao.BuscaCancelada then
      Exit;
  end
  else if FCartaoDebitoLiberado then
    vOpcao.Dados := 0  // D�bito
  else
    vOpcao.Dados := 1; // Cr�dito

  SetLength(FCartoes, Length(FCartoes) + 1);

  if sgCartoesPassar.Cells[coTipoRecebimento, 1] = '' then
    vLinha := 1
  else begin
    vLinha := sgCartoesPassar.RowCount;
    sgCartoesPassar.RowCount := sgCartoesPassar.RowCount + 1;
  end;

  sgCartoesPassar.Cells[coTipoRecebimento, vLinha] := 'TEF';
  sgCartoesPassar.Cells[coTipoCartao, vLinha]      := IIfStr(vOpcao.Dados = 0, 'D�bito', 'Cr�dito');
  sgCartoesPassar.Cells[coQtdeParcelas, vLinha]    := _Biblioteca.NFormatN(FQtdeMaximaParcelas);
  sgCartoesPassar.Cells[coValor, vLinha]           := '';
  sgCartoesPassar.Cells[coStatus, vLinha]          := 'Pendente';
  sgCartoesPassar.Row := vLinha;
  sgCartoesPassar.Col := coValor;
end;

procedure TFormBuscarDadosCartoesTEF.miAlterarFormaRecebimentoClick(Sender: TObject);
var
  vPosic: Integer;
  vCobranca: RecTiposCobranca;
  vRetTela: TRetornoTelaFinalizar<RecDadosCartao>;
begin
  inherited;
  if
    (SgCartoesPassar.Cells[coTipoRecebimento, SgCartoesPassar.Row] = 'TEF') and
    (SgCartoesPassar.Cells[coStatus, SgCartoesPassar.Row] = coAprovado)
  then begin
    _Biblioteca.Exclamar('Cart�es TEF j� aprovados n�o podem ter a forma de recebimento alteradas!');
    Exit;
  end;

  VerificarValores;

  vRetTela :=
    BuscarTiposCobrancaCartoes.Buscar(
      SFormatInt(sgCartoesPassar.Cells[coQtdeParcelas, sgCartoesPassar.Row]),
      Copy(sgCartoesPassar.Cells[coTipoCartao, sgCartoesPassar.Row], 1, 1),
      '',
      '',
      ''
    );

  if vRetTela.BuscaCancelada then
    Exit;

  vCobranca := _TiposCobranca.BuscarTiposCobrancas(Sessao.getConexaoBanco, 0, [vRetTela.Dados.CobrancaId], 'R')[0];

  SgCartoesPassar.Cells[coTipoRecebimento, SgCartoesPassar.Row] := 'POS';
  sgCartoesPassar.Cells[coCobrancaId, sgCartoesPassar.Row]      := _Biblioteca.NFormat(vCobranca.CobrancaId);
  sgCartoesPassar.Cells[coTipoCobranca, sgCartoesPassar.Row]    := vCobranca.Nome;

  vPosic := sgCartoesPassar.Row -1;

  SgCartoesPassar.Cells[coStatus, SgCartoesPassar.Row]          := coAprovado;

  FCartoes[vPosic].CobrancaId        := vCobranca.CobrancaId;
  FCartoes[vPosic].Valor             := SFormatDouble(SgCartoesPassar.Cells[coValor, SgCartoesPassar.Row]);
  FCartoes[vPosic].CodigoAutorizacao := vRetTela.Dados.CodigoAutorizacao;
  FCartoes[vPosic].Nsu               := vRetTela.Dados.NsuTef;
  FCartoes[vPosic].NumeroCartao      := vRetTela.Dados.NumeroCartao;
  FCartoes[vPosic].TipoRecebimento   := 'POS';
end;

procedure TFormBuscarDadosCartoesTEF.miAlterarTipoCobrancaClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vCobranca: RecTiposCobranca;
  vRetTela: TRetornoTelaFinalizar<RecDadosCartao>;
begin
  inherited;
  if SgCartoesPassar.Cells[coStatus, SgCartoesPassar.Row] <> coAprovado then begin
    _Biblioteca.Exclamar('Para definir o tipo de cobran�a o cart�o deve ter sido aprovado!');
    Exit;
  end;

  vRetTela :=
    BuscarTiposCobrancaCartoes.Buscar(
      SFormatInt(sgCartoesPassar.Cells[coQtdeParcelas, sgCartoesPassar.Row]),
      Copy(sgCartoesPassar.Cells[coTipoCartao, sgCartoesPassar.Row], 1, 1),
      FCartoes[sgCartoesPassar.Row - 1].NumeroCartao,
      FCartoes[sgCartoesPassar.Row - 1].Nsu,
      FCartoes[sgCartoesPassar.Row - 1].CodigoAutorizacao
    );

  if vRetTela.BuscaCancelada then
    Exit;

  vCobranca := _TiposCobranca.BuscarTiposCobrancas(Sessao.getConexaoBanco, 0, [vRetTela.Dados.CobrancaId], 'R')[0];
  sgCartoesPassar.Cells[coCobrancaId, sgCartoesPassar.Row]   := _Biblioteca.NFormat(vCobranca.CobrancaId);
  sgCartoesPassar.Cells[coTipoCobranca, sgCartoesPassar.Row] := vCobranca.Nome;

  FCartoes[sgCartoesPassar.Row - 1].CobrancaId        := vCobranca.CobrancaId;
  FCartoes[sgCartoesPassar.Row - 1].CodigoAutorizacao := vRetTela.Dados.CodigoAutorizacao;
  FCartoes[sgCartoesPassar.Row - 1].Nsu               := vRetTela.Dados.NsuTef;
  FCartoes[sgCartoesPassar.Row - 1].NumeroCartao      := vRetTela.Dados.NumeroCartao;

  // Se n�o for TEF n�o realizar nenhum v�nculo
  if SgCartoesPassar.Cells[coTipoRecebimento, SgCartoesPassar.Row] = 'TEF' then begin
    vRetBanco :=
      _TiposCobranca.VincularCartaoAutomatico(
        Sessao.getConexaoBanco,
        FCartoes[sgCartoesPassar.Row - 1].CobrancaId,
        FCartoes[sgCartoesPassar.Row - 1].Rede,
        FCartoes[sgCartoesPassar.Row - 1].Bandeira
      );
    Sessao.AbortarSeHouveErro(vRetBanco);
  end;
end;

procedure TFormBuscarDadosCartoesTEF.miDeletarCartaoClick(Sender: TObject);
var
  i: Integer;
  vAuxilar: TArray<RecRespostaTEF>;
begin
  inherited;
  vAuxilar := nil;
  if (SgCartoesPassar.Cells[coStatus, SgCartoesPassar.Row] = coAprovado) and (sgCartoesPassar.Cells[coTipoRecebimento, sgCartoesPassar.Row] = 'TEF') then begin
    _Biblioteca.Exclamar('Este cart�o j� foi passado e aprovado!');
    Exit;
  end;

  for i := Low(FCartoes) to High(FCartoes) do begin
    if i = sgCartoesPassar.Row -1 then
      Continue;

    SetLength(vAuxilar, Length(vAuxilar) + 1);
    vAuxilar[High(vAuxilar)] := FCartoes[i];
  end;

  VerificarValores;
  sgCartoesPassar.DeleteRow( sgCartoesPassar.Row );
end;

procedure TFormBuscarDadosCartoesTEF.miPassarCartaoClick(Sender: TObject);
var
  vPosic: Integer;
  vCobranca: TArray<RecTiposCobranca>;
begin
  inherited;

  if SgCartoesPassar.Cells[coStatus, SgCartoesPassar.Row] = coAprovado then begin
    _Biblioteca.Exclamar('Este cart�o j� foi passado e aprovado!');
    Exit;
  end;

  VerificarValores;

  // Verificando se o gereciador est� 100%
  while not FTEF.VerificarTEFAtivo( False ) do begin
    _Biblioteca.Informar('O gerenciador padr�o n�o est� ativo. Clique em OK para ativ�-lo.');
    FTEF.AtivarTEF;
  end;

  FTEF.EfetuarRecebimento(
    0,
    SFormatDouble( sgCartoesPassar.Cells[coValor, sgCartoesPassar.Row] ),
    '',
    IIfInt(sgCartoesPassar.Cells[coTipoCartao, sgCartoesPassar.Row] = 'D�bito', 0, SFormatInt(sgCartoesPassar.Cells[coQtdeParcelas, sgCartoesPassar.Row]))
  );

  if not FTEF.Resposta.Aprovado then begin
    Exclamar(FTEF.Resposta.Mensagem);
    Exit;
  end;

  // Se n�o conseguiu pegar os registros 29
  if FTEF.Resposta.QtdeRegistros29 = 0 then begin
    Informar(FTEF.Resposta.Mensagem);
    Exit;
  end;

  FTEF.FazerBackupArquivosTEF;

  sgCartoesPassar.Cells[coStatus, sgCartoesPassar.Row] := coAprovado;
  sgCartoesPassar.Options := sgCartoesPassar.Options - [goEditing];

  // Atualizando o cart�o do vetor
  vPosic := sgCartoesPassar.Row -1;

  FCartoes[vPosic] := FTEF.Resposta;
  FCartoes[vPosic].TipoRecebimento := 'TEF';

  FTEF.EnviarConfirmacao(0);
  // Se deu algum problema, informando e dando mensagem
  if not FTEF.Resposta.Aprovado then begin
    Exclamar(FTEF.Resposta.Mensagem);
    Exit;
  end;

  vCobranca :=
    _TiposCobranca.BuscarTiposCobrancas(
      Sessao.getConexaoBanco,
      4,
      [ SFormatInt(sgCartoesPassar.Cells[coQtdeParcelas, sgCartoesPassar.Row]), FTEF.Resposta.Rede, FTEF.Resposta.Bandeira, Copy(sgCartoesPassar.Cells[coTipoCartao, sgCartoesPassar.Row], 1, 1) ],
      'R'
    );

  if vCobranca = nil then begin
    _Biblioteca.Informar('O cart�o que acabou de ser passado n�o foi identificado de maneira autom�tica, ser� necess�rio seleciona-lo manualmente.');
    miAlterarTipoCobrancaClick(Sender);
  end
  else begin
    sgCartoesPassar.Cells[coCobrancaId, sgCartoesPassar.Row]   := _Biblioteca.NFormat(vCobranca[0].CobrancaId);
    sgCartoesPassar.Cells[coTipoCobranca, sgCartoesPassar.Row] := vCobranca[0].Nome;
    FCartoes[vPosic].CobrancaId := vCobranca[0].CobrancaId;
  end;

//  // Verificando se � o �ltimo cart�o a ser passado
//  vUltimoCartao := VerificarUltimoCartao;
//  if vUltimoCartao then
//    ModalResult := MrOk
//  else begin
//    // Jogando o foco para a primeira linha de cart�o que ainda n�o foi passada
//    with SgCartoesPassar do begin
//      for i := 1 to RowCount - 1 do begin
//        if Cells[coStatus, i] = 'Pendente' then begin
//          Row := i;
//          Break;
//        end;
//      end;
//    end;
//  end;
end;

procedure TFormBuscarDadosCartoesTEF.sbDefinirTipoCobrancaClick(Sender: TObject);
var
  vCobranca: RecTiposCobranca;
  vRetTela: TRetornoTelaFinalizar<RecDadosCartao>;
begin
  inherited;
  vRetTela := BuscarTiposCobrancaCartoes.Buscar(
    SFormatInt(sgCartoesPassar.Cells[coQtdeParcelas,
    sgCartoesPassar.Row]),
    Copy(sgCartoesPassar.Cells[coTipoCartao, sgCartoesPassar.Row], 1, 1),
    FCartoes[sgCartoesPassar.Row - 1].NumeroCartao,
    FCartoes[sgCartoesPassar.Row - 1].Nsu,
    FCartoes[sgCartoesPassar.Row - 1].CodigoAutorizacao
  );

  if vRetTela.BuscaCancelada then

    Exit;

  vCobranca := _TiposCobranca.BuscarTiposCobrancas(Sessao.getConexaoBanco, 0, [vRetTela.Dados.CobrancaId], 'R')[0];
  sgCartoesPassar.Cells[coCobrancaId, sgCartoesPassar.Row]   := _Biblioteca.NFormat(vCobranca.CobrancaId);
  sgCartoesPassar.Cells[coTipoCobranca, sgCartoesPassar.Row] := vCobranca.Nome;
end;

procedure TFormBuscarDadosCartoesTEF.sgCartoesPassarArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coValor then
    TextCell := _Biblioteca.NFormatN( _Biblioteca.SFormatDouble(TextCell) );

  CalcularDiferenca;
end;

procedure TFormBuscarDadosCartoesTEF.sgCartoesPassarDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coValor, coQtdeParcelas, coCobrancaId] then
    vAlinhamento := taRightJustify
  else if ACol in[coTipoRecebimento, coTipoCartao, coStatus] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgCartoesPassar.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscarDadosCartoesTEF.sgCartoesPassarGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in[coValor, coQtdeParcelas] then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Color := _Biblioteca.coCorFonteEdicao2;
  end
  else if ACol = coStatus then begin
    AFont.Style := [fsBold];
    if sgCartoesPassar.Cells[coStatus, ARow] = 'Pendente' then
      AFont.Color := clRed
    else
      AFont.Color := clNavy;
  end
  else if ACol = coTipoCartao then begin
    AFont.Style := [fsBold];
    if sgCartoesPassar.Cells[coStatus, ARow] = 'Cr�dito' then
      AFont.Color := clNavy
    else
      AFont.Color := $000096DB;
  end;
end;

procedure TFormBuscarDadosCartoesTEF.sgCartoesPassarSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgCartoesPassar.Cells[coStatus, ARow] = 'Aprovado' then
    sgCartoesPassar.Options := sgCartoesPassar.Options - [goEditing]
  else if ACol in[coValor] then begin
    sgCartoesPassar.Options := sgCartoesPassar.Options + [goEditing];
    sgCartoesPassar.OnKeyPress := NumerosVirgula;
  end
  else if (ACol in[coQtdeParcelas]) and (sgCartoesPassar.Cells[coTipoCartao, ARow] <> 'D�bito') then begin
    sgCartoesPassar.Options := sgCartoesPassar.Options + [goEditing];
    sgCartoesPassar.OnKeyPress := Numeros;
  end
  else
    sgCartoesPassar.Options := sgCartoesPassar.Options - [goEditing];
end;

procedure TFormBuscarDadosCartoesTEF.VerificarRegistro(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  for i := 1 to sgCartoesPassar.RowCount - 1 do begin
    if sgCartoesPassar.Cells[coStatus, i] = 'Pendente' then begin
      Exclamar('Existem cart�es que ainda est�o pendentes!');
      sgCartoesPassar.Row := i;
      _Biblioteca.SetarFoco(sgCartoesPassar);
      Abort;
    end;

    if sgCartoesPassar.Cells[coCobrancaId, i] = '' then begin
      Exclamar('Existem cart�es que n�o tiveram o tipo de cobran�a informado!');
      sgCartoesPassar.Row := i;
      _Biblioteca.SetarFoco(sgCartoesPassar);
      Abort;
    end;
  end;
end;

//function TFormBuscarDadosCartoesTEF.VerificarUltimoCartao: Boolean;
//var
//  i: Integer;
//begin
//  Result := True;
//
//  with SgCartoesPassar do begin
//    for i := FixedRows to RowCount - 1 do begin
//      if Cells[coStatus, i] <> 'Pendente' then
//        Continue;
//
//      Result := False;
//      Break;
//    end;
//  end;
//end;

procedure TFormBuscarDadosCartoesTEF.VerificarValores;
var
  i: Integer;
  vTotalCartoes: Double;
begin
  vTotalCartoes := 0;
  for i := 1 to sgCartoesPassar.RowCount -1 do begin
    if sgCartoesPassar.Cells[coValor, i] = '' then begin
      _Biblioteca.Exclamar('O valor do cart�o a ser passado n�o foi informado corretamente, verifique!');
      sgCartoesPassar.Row := i;
      sgCartoesPassar.Col := coValor;
      _Biblioteca.SetarFoco(sgCartoesPassar);
      Abort;
    end;

    if SFormatInt(sgCartoesPassar.Cells[coQtdeParcelas, i]) > FQtdeMaximaParcelas then begin
      _Biblioteca.Exclamar('A quantidade de parcelas a serem passadas n�o pode ser maior que a quantidade m�xima da permitida na condi��o de pagamento, verifique!');
      sgCartoesPassar.Row := i;
      sgCartoesPassar.Col := coQtdeParcelas;
      _Biblioteca.SetarFoco(sgCartoesPassar);
      Abort;
    end;

    vTotalCartoes := vTotalCartoes + SFormatDouble( sgCartoesPassar.Cells[coValor, i] );
  end;

  if _Biblioteca.NFormat(vTotalCartoes) <> stValorReceber.Caption then begin
    _Biblioteca.Exclamar('O valor total a ser recebido � diferente do valor total dos cart�es inseridos, verifique!');
    _Biblioteca.SetarFoco(sgCartoesPassar);
    Abort;
  end;

  if stValorDiferenca.Caption <> '' then begin
    _Biblioteca.Exclamar('Existe diferen�a nos valores informados, verifique!');
    _Biblioteca.SetarFoco(sgCartoesPassar);
    Abort;
  end;
end;

procedure CancelarCartoes(pCartoes: TArray<RecRespostaTEF>);
var
  i: integer;
  vTef: TTEFDiscado;
begin
  vTef :=
    TTEFDiscado.Create(
      nil,
      Sessao.getParametrosEstacao.DiretorioArquivosRequisicao,
      Sessao.getParametrosEstacao.DiretorioArquivosResposta,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0,
      '',
      0,
      0
    );

  for i := Low(pCartoes) to High(pCartoes) do begin
    if not pCartoes[i].Aprovado then
      Continue;

    if pCartoes[i].Aprovado then begin
      vTef.EfetuarCNC(pCartoes[i].Rede, pCartoes[i].Nsu, pCartoes[i].Finalizacao, pCartoes[i].Valor, pCartoes[i].DataOperacao, pCartoes[i].HoraOperacao);
      vTef.EnviarConfirmacao(0);
    end
    else
      vTef.EfetuarNCN(pCartoes[i].rede, pCartoes[i].nsu, pCartoes[i].finalizacao, pCartoes[i].Valor);

    if not vTef.Resposta.Aprovado then begin
      Exclamar('Falha ao cancelar transa��o TEF!');
      Continue;
    end;
  end;
//  vTef.ApagarBackupArquivosTEF;

  vTef.Free;
end;

end.
