unit _TipoBloqueioEstoque;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _Sessao, _RecordsCadastros;

{$M+}
type
  TTipoBloqueioEstoque = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordTipoBloqueioEstoque: RecTipoBloqueioEstoque;
  end;

function AtualizarTipoBloqueioEstoque(
  pConexao: TConexao;
  pTipoBloqueioId: Integer;
  pDescricao: string;
  pAtivo: string
): RecRetornoBD;

function BuscarTipoBloqueioEstoque(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTipoBloqueioEstoque>;

function ExcluirTipoBloqueioEstoque(
  pConexao: TConexao;
  pTipoBloqueioId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TTipoBloqueioEstoque }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TIPO_BLOQUEIO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o',
      True,
      0,
      'where DESCRICAO like :P1 || ''%'' ' +
      'order by ' +
      '  DESCRICAO '
    );
end;

constructor TTipoBloqueioEstoque.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'TIPO_BLOQUEIO_ESTOQUE');

  FSql :=
    'select ' +
    '  TIPO_BLOQUEIO_ID, ' +
    '  DESCRICAO, ' +
    '  ATIVO ' +
    'from ' +
    '  TIPO_BLOQUEIO_ESTOQUE ';

  setFiltros(getFiltros);

  AddColuna('TIPO_BLOQUEIO_ID', True);
  AddColuna('DESCRICAO');
  AddColuna('ATIVO');
end;

function TTipoBloqueioEstoque.getRecordTipoBloqueioEstoque: RecTipoBloqueioEstoque;
begin
  Result := RecTipoBloqueioEstoque.Create;
  Result.tipo_bloqueio_id := getInt('TIPO_BLOQUEIO_ID', True);
  Result.descricao              := getString('DESCRICAO');
  Result.ativo                  := getString('ATIVO');
end;

function AtualizarTipoBloqueioEstoque(
  pConexao: TConexao;
  pTipoBloqueioId: Integer;
  pDescricao: string;
  pAtivo: string
): RecRetornoBD;
var
  t: TTipoBloqueioEstoque;
  vNovo: Boolean;
  seq: TSequencia;
begin
  Result.Iniciar;
  t := TTipoBloqueioEstoque.Create(pConexao);

  vNovo := pTipoBloqueioId = 0;
  if vNovo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_TIP_BLO_EST_ID');
    pTipoBloqueioId := seq.getProximaSequencia;
    result.AsInt := pTipoBloqueioId;
    seq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    t.setInt('TIPO_BLOQUEIO_ID', pTipoBloqueioId, True);
    t.setString('DESCRICAO', pDescricao);
    t.setString('ATIVO', pAtivo);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarTipoBloqueioEstoque(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTipoBloqueioEstoque>;
var
  i: Integer;
  t: TTipoBloqueioEstoque;
begin
  Result := nil;
  t := TTipoBloqueioEstoque.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordTipoBloqueioEstoque;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirTipoBloqueioEstoque(
  pConexao: TConexao;
  pTipoBloqueioId: Integer
): RecRetornoBD;
var
  t: TTipoBloqueioEstoque;
begin
  Result.TeveErro := False;
  t := TTipoBloqueioEstoque.Create(pConexao);

  try
    t.setInt('TIPO_BLOQUEIO_ID', pTipoBloqueioId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
