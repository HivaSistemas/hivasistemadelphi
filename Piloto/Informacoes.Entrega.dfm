inherited FormInformacoesEntrega: TFormInformacoesEntrega
  Caption = 'Informa'#231#245'es da entrega'
  ClientHeight = 471
  ClientWidth = 666
  ExplicitWidth = 672
  ExplicitHeight = 500
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 2
    Top = 0
    Width = 41
    Height = 14
    Caption = 'Entrega'
  end
  object lb2: TLabel [1]
    Left = 82
    Top = 0
    Width = 39
    Height = 14
    Caption = 'Cliente'
  end
  object lb13: TLabel [2]
    Left = 289
    Top = 0
    Width = 92
    Height = 14
    Caption = 'Empresa entrega'
  end
  object lb17: TLabel [3]
    Left = 2
    Top = 40
    Width = 38
    Height = 14
    Caption = 'Pedido'
  end
  object Label1: TLabel [4]
    Left = 82
    Top = 40
    Width = 57
    Height = 14
    Caption = 'Nota fiscal'
  end
  object lb4: TLabel [5]
    Left = 244
    Top = 40
    Width = 101
    Height = 14
    Caption = 'Data/hora entrega'
  end
  object lb16: TLabel [6]
    Left = 368
    Top = 40
    Width = 147
    Height = 14
    Caption = 'Nome pessoa recebimento'
  end
  object lb3: TLabel [7]
    Left = 166
    Top = 40
    Width = 69
    Height = 14
    Caption = 'Cont.entrega'
  end
  object sbInformacoesOrcamento: TSpeedButtonLuka [8]
    Left = 59
    Top = 58
    Width = 18
    Height = 18
    Hint = 'Abrir informa'#231#245'es do pedido'
    Flat = True
    NumGlyphs = 2
    OnClick = sbInformacoesOrcamentoClick
    TagImagem = 13
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  object sbNotaFiscalId: TSpeedButtonLuka [9]
    Left = 137
    Top = 58
    Width = 18
    Height = 18
    Hint = 'Abrir informa'#231#245'es da nota fiscal'
    Flat = True
    NumGlyphs = 2
    OnClick = sbNotaFiscalIdClick
    TagImagem = 13
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  object sbInformacoesControle: TSpeedButtonLuka [10]
    Left = 221
    Top = 58
    Width = 18
    Height = 18
    Hint = 'Abrir informa'#231#245'es do controle de entrega'
    Flat = True
    NumGlyphs = 2
    OnClick = sbInformacoesControleClick
    TagImagem = 13
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  object lb5: TLabel [11]
    Left = 2
    Top = 80
    Width = 143
    Height = 14
    Caption = 'Usu'#225'rio '#237'niciou separa'#231#227'o'
  end
  object lb6: TLabel [12]
    Left = 208
    Top = 80
    Width = 115
    Height = 14
    Caption = 'Data/hora '#237'nicio sep.'
  end
  object lb7: TLabel [13]
    Left = 330
    Top = 80
    Width = 153
    Height = 14
    Caption = 'Usu'#225'rio finalizou separa'#231#227'o'
  end
  object lb8: TLabel [14]
    Left = 535
    Top = 80
    Width = 121
    Height = 14
    Caption = 'Data/hora finaliz. sep.'
  end
  inherited pnOpcoes: TPanel
    Top = 434
    Width = 666
    ExplicitTop = 434
    ExplicitWidth = 666
  end
  object eEntregaId: TEditLuka
    Left = 2
    Top = 14
    Width = 75
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCliente: TEditLuka
    Left = 82
    Top = 14
    Width = 202
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmpresa: TEditLuka
    Left = 289
    Top = 14
    Width = 187
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object stStatus: TStaticText
    Left = 481
    Top = 19
    Width = 183
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Ag. carregamento'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
  end
  object st3: TStaticText
    Left = 481
    Top = 4
    Width = 183
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Status'
    Color = 38619
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    Transparent = False
  end
  object eOrcamentoId: TEditLuka
    Left = 2
    Top = 54
    Width = 55
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 6
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNotaFiscalId: TEditLuka
    Left = 82
    Top = 54
    Width = 55
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 7
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataHoraEntrega: TEditLukaData
    Left = 244
    Top = 54
    Width = 118
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    ReadOnly = True
    TabOrder = 8
    Text = '  /  /       :  :  '
    TipoData = tdDataHora
  end
  object eNomePessoaRetirou: TEditLuka
    Left = 368
    Top = 54
    Width = 296
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 9
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object sgItens: TGridLuka
    Left = 2
    Top = 117
    Width = 662
    Height = 313
    ColCount = 9
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 10
    OnDrawCell = sgItensDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Lote'
      'Quantidade'
      'Und.'
      'Devolvidos'
      'Retornados'
      'N'#227'o separ.')
    Grid3D = False
    RealColCount = 9
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      64
      236
      126
      103
      74
      64
      70
      75
      72)
  end
  object eControleEntregaId: TEditLuka
    Left = 166
    Top = 54
    Width = 55
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 11
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eUsuarioInicioSeparacao: TEditLuka
    Left = 2
    Top = 94
    Width = 202
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 12
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataHoraInicioSeparacao: TEditLukaData
    Left = 208
    Top = 94
    Width = 118
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    ReadOnly = True
    TabOrder = 13
    Text = '  /  /       :  :  '
    TipoData = tdDataHora
  end
  object eUsuarioFinalizouSeparacao: TEditLuka
    Left = 330
    Top = 94
    Width = 201
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 14
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataHoraFinalizouSeparacao: TEditLukaData
    Left = 535
    Top = 94
    Width = 129
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    ReadOnly = True
    TabOrder = 15
    Text = '  /  /       :  :  '
    TipoData = tdDataHora
  end
end
