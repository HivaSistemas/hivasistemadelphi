inherited FormInformacoesProduto: TFormInformacoesProduto
  Caption = 'Informa'#231#245'es de produto'
  ClientHeight = 430
  ClientWidth = 800
  ExplicitWidth = 806
  ExplicitHeight = 459
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 393
    Width = 800
    ExplicitTop = 393
    ExplicitWidth = 800
  end
  object pcDados: TPageControl
    Left = 0
    Top = 39
    Width = 800
    Height = 354
    ActivePage = tsParametrosCompra
    Align = alClient
    TabOrder = 1
    object tsPrincipal: TTabSheet
      Caption = 'Principais'
      object lb1: TLabel
        Left = 239
        Top = 48
        Width = 64
        Height = 14
        Caption = 'Mult. venda'
      end
      object lb2: TLabel
        Left = 320
        Top = 48
        Width = 26
        Height = 14
        Caption = 'Peso'
      end
      object lb3: TLabel
        Left = 401
        Top = 48
        Width = 41
        Height = 14
        Caption = 'Volume'
      end
      object lb5: TLabel
        Left = 483
        Top = 48
        Width = 92
        Height = 14
        Caption = 'C'#243'digo de barras'
      end
      object Label3: TLabel
        Left = 492
        Top = 94
        Width = 122
        Height = 14
        Caption = 'C'#243'digo balan'#231'a ( PDV )'
      end
      object Label7: TLabel
        Left = 402
        Top = 94
        Width = 86
        Height = 14
        Caption = 'Valor adic. frete'
      end
      inline FrFabricante: TFrFornecedores
        Left = 2
        Top = 0
        Width = 313
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitWidth = 313
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 288
          Height = 23
          TabOrder = 1
          ExplicitWidth = 288
          ExplicitHeight = 23
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 313
          TabOrder = 0
          ExplicitWidth = 313
          inherited lbNomePesquisa: TLabel
            Width = 58
            Caption = 'Fabricante'
            ExplicitWidth = 58
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 208
            ExplicitLeft = 208
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 288
          Height = 24
          ExplicitLeft = 288
          ExplicitHeight = 24
        end
      end
      inline FrUnidadeVenda: TFrUnidades
        Left = 2
        Top = 47
        Width = 226
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 47
        ExplicitWidth = 226
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 201
          Height = 23
          TabOrder = 1
          ExplicitWidth = 201
          ExplicitHeight = 23
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 226
          TabOrder = 0
          ExplicitWidth = 226
          inherited lbNomePesquisa: TLabel
            Width = 100
            Caption = 'Unidade de venda'
            ExplicitWidth = 100
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 121
            ExplicitLeft = 121
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 201
          Height = 24
          ExplicitLeft = 201
          ExplicitHeight = 24
        end
      end
      inline FrMarca: TFrMarcas
        Left = 320
        Top = 0
        Width = 313
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 320
        ExplicitWidth = 313
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 288
          Height = 23
          TabOrder = 1
          ExplicitWidth = 288
          ExplicitHeight = 23
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 313
          TabOrder = 0
          ExplicitWidth = 313
          inherited lbNomePesquisa: TLabel
            Width = 33
            Caption = 'Marca'
            ExplicitWidth = 33
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 208
            ExplicitLeft = 208
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 288
          Height = 24
          ExplicitLeft = 288
          ExplicitHeight = 24
        end
      end
      object eMultiploVenda: TEditLuka
        Left = 239
        Top = 63
        Width = 76
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        ReadOnly = True
        TabOrder = 3
        Text = '0,000'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 3
        NaoAceitarEspaco = False
      end
      object ePeso: TEditLuka
        Left = 320
        Top = 63
        Width = 76
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        ReadOnly = True
        TabOrder = 4
        Text = '0,000'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 3
        NaoAceitarEspaco = False
      end
      object eVolume: TEditLuka
        Left = 401
        Top = 63
        Width = 76
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        ReadOnly = True
        TabOrder = 5
        Text = '0,000'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 3
        NaoAceitarEspaco = False
      end
      object ckAceitarEstoqueNegativo: TCheckBox
        Left = 2
        Top = 200
        Width = 247
        Height = 17
        Caption = '1 - Aceitar estoque negativo'
        Enabled = False
        TabOrder = 7
      end
      object ckInativarProdutoZerarEstoque: TCheckBox
        Left = 2
        Top = 217
        Width = 247
        Height = 17
        Caption = '2 - Inativar produto ao zerar estoque'
        Enabled = False
        TabOrder = 8
      end
      object ckBloquearParaCompras: TCheckBox
        Left = 2
        Top = 234
        Width = 247
        Height = 17
        Caption = '3 - Bloqueado para compras'
        Enabled = False
        TabOrder = 9
      end
      object ckBloquearParaVendas: TCheckBox
        Left = 2
        Top = 251
        Width = 247
        Height = 17
        Caption = '4 - Bloqueado para vendas'
        Enabled = False
        TabOrder = 10
      end
      object eCodigoBarras: TEditLuka
        Left = 481
        Top = 63
        Width = 150
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 14
        ReadOnly = True
        TabOrder = 6
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      inline FrLinhaProduto: TFrameDepartamentosSecoesLinhas
        Left = 2
        Top = 94
        Width = 394
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 11
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 94
        ExplicitWidth = 394
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 369
          Height = 23
          ExplicitWidth = 369
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 394
          ExplicitWidth = 394
          inherited lbNomePesquisa: TLabel
            Width = 93
            Caption = 'Linha do produto'
            ExplicitWidth = 93
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 289
            ExplicitLeft = 289
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 369
          Height = 24
          ExplicitLeft = 369
          ExplicitHeight = 24
        end
        inherited rgNivel: TRadioGroup
          ItemIndex = 3
        end
      end
      object ckProdutoDiversoPDV: TCheckBox
        Left = 253
        Top = 200
        Width = 258
        Height = 17
        Caption = '6 - Produto diversos ( PDV )'
        Enabled = False
        TabOrder = 12
      end
      object ckPermitirDevolucao: TCheckBox
        Left = 2
        Top = 268
        Width = 247
        Height = 17
        Caption = '5 - Permitir devolu'#231#227'o'
        Enabled = False
        TabOrder = 13
      end
      object eCodigoBalanca: TEditLuka
        Left = 492
        Top = 109
        Width = 139
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 5
        ReadOnly = True
        TabOrder = 14
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ckSobEncomenda: TCheckBox
        Left = 253
        Top = 217
        Width = 258
        Height = 17
        Caption = '7 - Sob encomenda'
        Enabled = False
        TabOrder = 15
      end
      object GroupBoxLuka1: TGroupBoxLuka
        Left = 0
        Top = 150
        Width = 206
        Height = 46
        TabOrder = 17
        OpcaoMarcarDesmarcar = False
        object lb14: TLabel
          Left = 104
          Top = 4
          Width = 81
          Height = 14
          Caption = '% Com. a prazo'
        end
        object lb13: TLabel
          Left = 9
          Top = 4
          Width = 77
          Height = 14
          Caption = '% Com. a vista'
        end
        object ePercentualComissaoVista: TEditLuka
          Left = 9
          Top = 18
          Width = 90
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          MaxLength = 5
          ReadOnly = True
          TabOrder = 0
          Text = '0,00'
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          NaoAceitarEspaco = False
        end
        object ePercentualComissaoPrazo: TEditLuka
          Left = 107
          Top = 18
          Width = 90
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          MaxLength = 5
          ReadOnly = True
          TabOrder = 1
          Text = '0,00'
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          NaoAceitarEspaco = False
        end
      end
      object ckSujeitoComissao: TCheckBox
        Left = 0
        Top = 137
        Width = 91
        Height = 17
        Caption = 'Comissionar'
        Enabled = False
        TabOrder = 16
      end
      object ckBloquearEntradaSemPedCompra: TCheckBox
        Left = 253
        Top = 234
        Width = 258
        Height = 17
        Caption = '8 - Bloquear entrada sem pedido de compra'
        Enabled = False
        TabOrder = 18
      end
      object ckExigirSeparacao: TCheckBox
        Left = 253
        Top = 251
        Width = 258
        Height = 17
        Caption = '9 - Exigir separa'#231#227'o'
        Enabled = False
        TabOrder = 19
      end
      object eValorAdicionalFrete: TEditLuka
        Left = 402
        Top = 109
        Width = 86
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        ReadOnly = True
        TabOrder = 20
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object ckExigirModeloNota: TCheckBox
        Left = 253
        Top = 268
        Width = 258
        Height = 17
        Caption = '10 - Exigir modelo nota fiscal'
        Enabled = False
        TabOrder = 21
      end
      object ckSepararSomenteCodigoBarras: TCheckBoxLuka
        Left = 515
        Top = 200
        Width = 258
        Height = 17
        Caption = '11 - Separar somente com c'#243'digo de barras'
        Enabled = False
        TabOrder = 22
        CheckedStr = 'N'
      end
    end
    object tsComplementos: TTabSheet
      Caption = 'Complementos'
      ImageIndex = 1
      object stCaracteristicas: TStaticText
        Left = 0
        Top = 0
        Width = 792
        Height = 15
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Caracter'#237'sticas t'#233'cnicas/garantia'
        Color = 15395562
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
      end
      object eCaracteristicas: TMemo
        Left = 0
        Top = 15
        Width = 792
        Height = 147
        Align = alTop
        MaxLength = 4000
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 1
      end
      object ckRevenda: TCheckBox
        Left = 1
        Top = 168
        Width = 150
        Height = 17
        Caption = '1 - Revenda'
        Enabled = False
        TabOrder = 2
      end
      object ckUsoConsumo: TCheckBox
        Left = 1
        Top = 187
        Width = 150
        Height = 17
        Caption = '2 - Uso e consumo'
        Enabled = False
        TabOrder = 3
      end
      object ckAtivoImobilizado: TCheckBox
        Left = 129
        Top = 168
        Width = 150
        Height = 17
        Caption = '3 - Ativo Imobilizado'
        Enabled = False
        TabOrder = 4
      end
      object ckServico: TCheckBox
        Left = 129
        Top = 187
        Width = 150
        Height = 17
        Caption = '4 - Servi'#231'o'
        Enabled = False
        TabOrder = 5
      end
    end
    object tsParametrosCompra: TTabSheet
      Caption = 'Par'#226'metros de compra'
      ImageIndex = 5
      object lb6: TLabel
        Left = 2
        Top = 1
        Width = 92
        Height = 14
        Caption = 'Nome de compra'
      end
      object lbl2: TLabel
        Left = 439
        Top = 1
        Width = 159
        Height = 14
        Caption = 'C'#243'digo original do fabricante'
      end
      object eNomeCompra: TEditLuka
        Left = 2
        Top = 15
        Width = 431
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 60
        TabOrder = 0
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      inline FrMultiplosCompra: TFrameConversaoUnidadesCompra
        Left = 2
        Top = 41
        Width = 234
        Height = 258
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 41
        ExplicitHeight = 258
        inherited sgValores: TGridLuka
          Height = 241
          ExplicitHeight = 241
        end
        inherited StaticTextLuka1: TStaticTextLuka
          Caption = 'M'#250'ltiplos de compra'
        end
        inherited pmOpcoes: TPopupMenu
          Left = 16
          Top = 208
        end
      end
      inline FrCodigoOriginalFornecedores: TFrCodigoOriginalFornecedores
        Left = 241
        Top = 41
        Width = 364
        Height = 258
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 241
        ExplicitTop = 41
        inherited pmOpcoes: TPopupMenu
          Top = 208
          inherited miInserir: TMenuItem
            Visible = False
          end
        end
      end
      object eCodigoOriginalFabricante: TEditLuka
        Left = 439
        Top = 15
        Width = 170
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 20
        TabOrder = 3
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
    object tsControleEstoque: TTabSheet
      Caption = 'Controle do estoque'
      ImageIndex = 6
      object Label4: TLabel
        Left = 793
        Top = 0
        Width = 64
        Height = 14
        Caption = 'Quantidade'
      end
      object lbNomePesquisa: TLabel
        Left = 3
        Top = 1
        Width = 24
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Tipo'
      end
      object Label5: TLabel
        Left = 3
        Top = 79
        Width = 100
        Height = 14
        Margins.Left = 0
        Margins.Top = 1
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alCustom
        Caption = 'Defini'#231#227'o aut. lote'
      end
      object Label6: TLabel
        Left = 3
        Top = 119
        Width = 98
        Height = 14
        Caption = 'Dias aviso vencto.'
      end
      object lbl3: TLabel
        Left = 3
        Top = 161
        Width = 112
        Height = 14
        Caption = 'Motivo de inativa'#231#227'o'
      end
      object sgProdutosKit: TGridLuka
        Left = 389
        Top = 7
        Width = 401
        Height = 190
        ColCount = 4
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        Enabled = False
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
        TabOrder = 0
        OnDrawCell = sgProdutosKitDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Produto'
          'Nome'
          'Quantidade'
          '% Part.')
        OnGetCellColor = sgProdutosKitGetCellColor
        Grid3D = False
        RealColCount = 5
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          52
          199
          72
          68)
      end
      object ckExigirDataFabricacaoLote: TCheckBoxLuka
        Left = 3
        Top = 43
        Width = 211
        Height = 17
        Caption = 'Exigir data de fabrica'#231#227'o do lote'
        Checked = True
        Enabled = False
        State = cbChecked
        TabOrder = 1
        CheckedStr = 'S'
        Valor = 'N'
      end
      object ckExigirDataVencimentoLote: TCheckBoxLuka
        Left = 3
        Top = 60
        Width = 211
        Height = 17
        Caption = 'Exigir data de vencimento do lote'
        Checked = True
        Enabled = False
        State = cbChecked
        TabOrder = 2
        CheckedStr = 'S'
        Valor = 'C'
      end
      object cbTipoControleEstoque: TComboBoxLuka
        Left = 3
        Top = 16
        Width = 211
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        Enabled = False
        TabOrder = 3
        Items.Strings = (
          'Normal'
          'Lote'
          'Piso'
          'Kit entr. desmembrada'
          'Kit entr. agrupada')
        Valores.Strings = (
          'N'
          'L'
          'P'
          'K'
          'A')
        AsInt = 0
      end
      inline FrUnidadeEntrega: TFrUnidades
        Left = 220
        Top = -1
        Width = 160
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Enabled = False
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 220
        ExplicitTop = -1
        ExplicitWidth = 160
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 135
          Height = 23
          TabOrder = 1
          ExplicitWidth = 135
          ExplicitHeight = 23
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 160
          TabOrder = 0
          ExplicitWidth = 160
          inherited lbNomePesquisa: TLabel
            Width = 109
            Height = 14
            Caption = 'Unidade de entrega'
            ExplicitWidth = 109
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 90
            ExplicitLeft = 90
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 135
          Height = 24
          ExplicitLeft = 135
          ExplicitHeight = 24
        end
      end
      object cbDefinicaoAutomaticaLote: TComboBoxLuka
        Left = 3
        Top = 93
        Width = 377
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        Enabled = False
        TabOrder = 5
        Items.Strings = (
          'Produtos do mesmo lote que atendam a quantidade'
          'Misturar lotes dando prioridade ao vencimento')
        Valores.Strings = (
          'Q'
          'V')
        AsInt = 0
      end
      object eDiasAvisoVencimento: TEditLuka
        Left = 3
        Top = 133
        Width = 98
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 8
        TabOrder = 6
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eMotivoInatividade: TEditLuka
        Left = 3
        Top = 175
        Width = 377
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 8
        TabOrder = 7
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
    object tsFiscal: TTabSheet
      Caption = 'Fiscal'
      ImageIndex = 5
      object lb4: TLabel
        Left = 0
        Top = 0
        Width = 64
        Height = 14
        Caption = 'C'#243'digo NCM'
      end
      object lb7: TLabel
        Left = 155
        Top = 0
        Width = 24
        Height = 14
        Caption = 'CEST'
      end
      object eNCM: TEditLuka
        Left = 0
        Top = 14
        Width = 151
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 8
        NumbersOnly = True
        TabOrder = 0
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eCEST: TEditLuka
        Left = 155
        Top = 14
        Width = 151
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 7
        NumbersOnly = True
        TabOrder = 1
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      inline FrProdutosGruposTributacoes: TFrProdutosGruposTributacoesEmpresas
        Left = 0
        Top = 0
        Width = 792
        Height = 325
        Align = alClient
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitWidth = 792
        ExplicitHeight = 325
        inherited sgGrupos: TGridLuka
          Width = 792
          Height = 309
          ExplicitWidth = 792
          ExplicitHeight = 309
          ColWidths = (
            33
            54
            185
            183
            282
            311
            292)
        end
        inherited st1: TStaticTextLuka
          Width = 792
          ExplicitWidth = 792
        end
        inherited pmOpcoes: TPopupMenu
          Left = 836
          Top = 392
        end
      end
    end
    object tsFotos: TTabSheet
      Caption = 'Fotos'
      ImageIndex = 5
      inline FrFoto1: TFrImagem
        Left = 2
        Top = 19
        Width = 128
        Height = 128
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 19
      end
      inline FrFoto2: TFrImagem
        Left = 137
        Top = 19
        Width = 128
        Height = 128
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 137
        ExplicitTop = 19
      end
      inline FrFoto3: TFrImagem
        Left = 273
        Top = 19
        Width = 128
        Height = 128
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 273
        ExplicitTop = 19
      end
      object stctxtlk1: TStaticTextLuka
        Left = 137
        Top = 4
        Width = 128
        Height = 15
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Foto 2'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object stctxtlk2: TStaticTextLuka
        Left = 273
        Top = 4
        Width = 128
        Height = 15
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Foto 3'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object stctxtlk3: TStaticTextLuka
        Left = 2
        Top = 4
        Width = 128
        Height = 15
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Foto 1'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
    end
  end
  object pPanel: TPanel
    Left = 0
    Top = 0
    Width = 800
    Height = 39
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 3
      Top = 1
      Width = 37
      Height = 14
      Caption = 'C'#243'digo'
    end
    object lb17: TLabel
      Left = 598
      Top = 1
      Width = 76
      Height = 14
      Caption = 'Data cadastro'
    end
    object lbl1: TLabel
      Left = 101
      Top = 1
      Width = 32
      Height = 14
      Caption = 'Nome'
    end
    object ckAtivo: TCheckBoxLuka
      Left = 718
      Top = 17
      Width = 46
      Height = 17
      Caption = 'Ativo'
      Enabled = False
      TabOrder = 0
      CheckedStr = 'N'
    end
    object eDataCadastro: TEditLukaData
      Left = 598
      Top = 15
      Width = 98
      Height = 22
      Hint = 
        'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
        'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
      TabStop = False
      EditMask = '99/99/9999;1; '
      MaxLength = 10
      ReadOnly = True
      TabOrder = 1
      Text = '  /  /    '
    end
    object eID: TEditLuka
      Left = 2
      Top = 15
      Width = 93
      Height = 22
      Alignment = taRightJustify
      CharCase = ecUpperCase
      ReadOnly = True
      TabOrder = 2
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
    object eNomeProduto: TEditLuka
      Left = 101
      Top = 15
      Width = 472
      Height = 22
      CharCase = ecUpperCase
      MaxLength = 60
      ReadOnly = True
      TabOrder = 3
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      NaoAceitarEspaco = False
    end
  end
end
