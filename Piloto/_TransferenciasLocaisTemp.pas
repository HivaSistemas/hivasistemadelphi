unit _TransferenciasLocaisTemp;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils, _TransferenciasLocaisItensTemp;

{$M+}
type
  RecTransferenciasLocaisTemp = record
    TransferenciaLocalId: Integer;
    EmpresaId: Integer;
    NomeEmpresa: string;
    LocalOrigemId: Integer;
    NomeLocal: string;
    UsuarioCadastroId: Integer;
    NomeUsuarioCadastro: string;
    DataHoraCadastro: TDateTime;
    Observacao: string;
    Status: string;
  end;

  TTransferenciasLocaisTemp = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordTransferencia: RecTransferenciasLocaisTemp;
  end;

function AtualizarTransferenciaLocal(
  pConexao: TConexao;
  pAjusteEstoqueId: Integer;
  pEmpresaId: Integer;
  pLocalOrigemId: Integer;
  pObservacao: string;
  pStatus: string;
  pItens: TArray<RecTransferenciasLocaisItensTemp>
): RecRetornoBD;

function BuscarTransferenciasLocaisTemp(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTransferenciasLocaisTemp>;
function BuscarTransferenciasLocaisTempComando(pConexao: TConexao; pComando: string): TArray<RecTransferenciasLocaisTemp>;

function getFiltros: TArray<RecFiltros>;

function ConfirmarTransferenciaLocais(
  pConexao: TConexao;
  pTransferenciaId: Integer
): RecRetornoBD;

implementation

{ TTransferenciasLocaisTemp }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where TRANSFERENCIA_LOCAL_ID = :P1'
    );
end;

constructor TTransferenciasLocaisTemp.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'TRANSFERENCIAS_LOCAIS_TEMP');

  FSql :=
    'select ' +
    '  TRA.TRANSFERENCIA_LOCAL_ID, ' +
    '  TRA.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA, ' +
    '  TRA.USUARIO_CADASTRO_ID, ' +
    '  TRA.LOCAL_ORIGEM_ID, ' +
    '  TRA.STATUS, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  FUN.NOME as NOME_USUARIO_CADASTRO, ' +
    '  TRA.DATA_HORA_CADASTRO, ' +
    '  TRA.OBSERVACAO ' +
    'from ' +
    '  TRANSFERENCIAS_LOCAIS_TEMP TRA ' +

    'inner join FUNCIONARIOS FUN ' +
    'on TRA.USUARIO_CADASTRO_ID = FUN.FUNCIONARIO_ID ' +

    'inner join EMPRESAS EMP ' +
    'on TRA.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on TRA.LOCAL_ORIGEM_ID = LOC.LOCAL_ID ';

  setFiltros(getFiltros);

  AddColuna('TRANSFERENCIA_LOCAL_ID', True);
  AddColuna('EMPRESA_ID');
  AddColunaSL('NOME_FANTASIA');
  AddColuna('LOCAL_ORIGEM_ID');
  AddColuna('STATUS');
  AddColunaSL('NOME_LOCAL');
  AddColunaSL('USUARIO_CADASTRO_ID');
  AddColunaSL('NOME_USUARIO_CADASTRO');
  AddColunaSL('DATA_HORA_CADASTRO');
  AddColuna('OBSERVACAO');
end;

function TTransferenciasLocaisTemp.getRecordTransferencia: RecTransferenciasLocaisTemp;
begin
  Result.TransferenciaLocalId := getInt('TRANSFERENCIA_LOCAL_ID', True);
  Result.EmpresaId            := getInt('EMPRESA_ID');
  Result.NomeEmpresa          := getString('NOME_FANTASIA');
  Result.LocalOrigemId        := getInt('LOCAL_ORIGEM_ID');
  Result.NomeLocal            := getString('NOME_LOCAL');
  Result.UsuarioCadastroId    := getInt('USUARIO_CADASTRO_ID');
  Result.NomeUsuarioCadastro  := getString('NOME_USUARIO_CADASTRO');
  Result.DataHoraCadastro     := getData('DATA_HORA_CADASTRO');
  Result.Observacao           := getString('OBSERVACAO');
  Result.Status               := getString('STATUS');
end;

function AtualizarTransferenciaLocal(
  pConexao: TConexao;
  pAjusteEstoqueId: Integer;
  pEmpresaId: Integer;
  pLocalOrigemId: Integer;
  pObservacao: string;
  pStatus: string;
  pItens: TArray<RecTransferenciasLocaisItensTemp>
): RecRetornoBD;
var
  vTransf: TTransferenciasLocaisTemp;
  vItens: TTransferenciasLocaisItensTemp;

  i: Integer;
  novo: Boolean;
  vExec: TExecucao;
begin
  Result.TeveErro := False;
  pConexao.SetRotina('REALIZAR_TRANSF_LOCAIS');

  vExec := TExecucao.Create(pConexao);
  vTransf := TTransferenciasLocaisTemp.Create(pConexao);
  vItens := TTransferenciasLocaisItensTemp.Create(pConexao);

  novo := pAjusteEstoqueId = 0;
  if novo then begin
    pAjusteEstoqueId := TSequencia.Create(pConexao, 'SEQ_TRANSFERENCIA_LOCAL_ID').GetProximaSequencia;
    result.AsInt := pAjusteEstoqueId;
  end;

  try
    pConexao.IniciarTransacao;

    vTransf.setInt('TRANSFERENCIA_LOCAL_ID', pAjusteEstoqueId, True);
    vTransf.setInt('EMPRESA_ID', pEmpresaId);
    vTransf.setInt('LOCAL_ORIGEM_ID', pLocalOrigemId);
    vTransf.setString('STATUS', pStatus);
    vTransf.setStringN('OBSERVACAO', pObservacao);


    if novo then
      vTransf.Inserir
    else
      vTransf.Atualizar;

    vItens.setInt('TRANSFERENCIA_LOCAL_ID', pAjusteEstoqueId, True);
    for i := Low(pItens) to High(pItens) do begin
      vItens.setInt('LOCAL_DESTINO_ID', pItens[i].LocalDestinoId, True);
      vItens.setInt('ITEM_ID', pItens[i].ItemId, True);
      vItens.setString('LOTE', pItens[i].Lote, True);
      vItens.setInt('PRODUTO_ID', pItens[i].ProdutoId);
      vItens.setDouble('QUANTIDADE', pItens[i].quantidade);

      vItens.Inserir;
    end;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
  vExec.Free;
  vItens.Free;
  vTransf.Free;
end;

function BuscarTransferenciasLocaisTemp(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecTransferenciasLocaisTemp>;
var
  i: Integer;
  t: TTransferenciasLocaisTemp;
begin
  Result := nil;
  t := TTransferenciasLocaisTemp.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordTransferencia;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarTransferenciasLocaisTempComando(pConexao: TConexao; pComando: string): TArray<RecTransferenciasLocaisTemp>;
var
  i: Integer;
  t: TTransferenciasLocaisTemp;
begin
  Result := nil;
  t := TTransferenciasLocaisTemp.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordTransferencia;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ConfirmarTransferenciaLocais(
  pConexao: TConexao;
  pTransferenciaId: Integer
): RecRetornoBD;
var
  vExecucao: TExecucao;
begin
  Result.Iniciar;

  vExecucao := TExecucao.Create(pConexao);
  pConexao.SetRotina('CONFIRMAR_TRANSF_LOCAIS');

  try
    pConexao.IniciarTransacao;

    vExecucao.SQL.Add('');
    vExecucao.SQL.Add('insert into TRANSFERENCIAS_LOCAIS( ');
    vExecucao.SQL.Add('  TRANSFERENCIA_LOCAL_ID, ');
    vExecucao.SQL.Add('  DATA_HORA_ALTERACAO, ');
    vExecucao.SQL.Add('  DATA_HORA_CADASTRO, ');
    vExecucao.SQL.Add('  EMPRESA_ID, ');
    vExecucao.SQL.Add('  ESTACAO_ALTERACAO, ');
    vExecucao.SQL.Add('  LOCAL_ORIGEM_ID, ');
    vExecucao.SQL.Add('  OBSERVACAO, ');
    vExecucao.SQL.Add('  ROTINA_ALTERACAO, ');
    vExecucao.SQL.Add('  USUARIO_CADASTRO_ID, ');
    vExecucao.SQL.Add('  USUARIO_SESSAO_ID ');
    vExecucao.SQL.Add(') ');
    vExecucao.SQL.Add('select ');
    vExecucao.SQL.Add('  TRANSFERENCIA_LOCAL_ID, ');
    vExecucao.SQL.Add('  DATA_HORA_ALTERACAO, ');
    vExecucao.SQL.Add('  DATA_HORA_CADASTRO, ');
    vExecucao.SQL.Add('  EMPRESA_ID, ');
    vExecucao.SQL.Add('  ESTACAO_ALTERACAO, ');
    vExecucao.SQL.Add('  LOCAL_ORIGEM_ID, ');
    vExecucao.SQL.Add('  OBSERVACAO, ');
    vExecucao.SQL.Add('  ROTINA_ALTERACAO, ');
    vExecucao.SQL.Add('  USUARIO_CADASTRO_ID, ');
    vExecucao.SQL.Add('  USUARIO_SESSAO_ID ');
    vExecucao.SQL.Add('from ');
    vExecucao.SQL.Add('  TRANSFERENCIAS_LOCAIS_TEMP ');
    vExecucao.SQL.Add('where TRANSFERENCIA_LOCAL_ID = :P1 ');
    vExecucao.Executar([pTransferenciaId]);

    vExecucao.Clear;
    vExecucao.SQL.Add('update TRANSFERENCIAS_LOCAIS_TEMP set STATUS = ''B'' where TRANSFERENCIA_LOCAL_ID = :P1');
    vExecucao.Executar([pTransferenciaId]);

    vExecucao.Clear;
    vExecucao.SQL.Add('insert into TRANSFERENCIAS_LOCAIS_ITENS( ');
    vExecucao.SQL.Add('  DATA_HORA_ALTERACAO, ');
    vExecucao.SQL.Add('  ESTACAO_ALTERACAO, ');
    vExecucao.SQL.Add('  ITEM_ID, ');
    vExecucao.SQL.Add('  LOCAL_DESTINO_ID, ');
    vExecucao.SQL.Add('  LOTE, ');
    vExecucao.SQL.Add('  PRODUTO_ID, ');
    vExecucao.SQL.Add('  QUANTIDADE, ');
    vExecucao.SQL.Add('  ROTINA_ALTERACAO, ');
    vExecucao.SQL.Add('  TRANSFERENCIA_LOCAL_ID, ');
    vExecucao.SQL.Add('  USUARIO_SESSAO_ID ');
    vExecucao.SQL.Add(') ');
    vExecucao.SQL.Add('select ');
    vExecucao.SQL.Add('  DATA_HORA_ALTERACAO, ');
    vExecucao.SQL.Add('  ESTACAO_ALTERACAO, ');
    vExecucao.SQL.Add('  ITEM_ID, ');
    vExecucao.SQL.Add('  LOCAL_DESTINO_ID, ');
    vExecucao.SQL.Add('  LOTE, ');
    vExecucao.SQL.Add('  PRODUTO_ID, ');
    vExecucao.SQL.Add('  QUANTIDADE, ');
    vExecucao.SQL.Add('  ROTINA_ALTERACAO, ');
    vExecucao.SQL.Add('  TRANSFERENCIA_LOCAL_ID, ');
    vExecucao.SQL.Add('  USUARIO_SESSAO_ID ');
    vExecucao.SQL.Add('from ');
    vExecucao.SQL.Add('  TRANSFERENCIAS_LOC_ITENS_TMP ');
    vExecucao.SQL.Add('where TRANSFERENCIA_LOCAL_ID = :P1 ');
    vExecucao.Executar([pTransferenciaId]);

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;
end;

end.
