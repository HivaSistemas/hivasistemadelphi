unit CancelarItensCompras;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _Biblioteca,
  Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _ComprasItens, _RecordsEspeciais;

type
  TFormCancelarItensCompras = class(TFormHerancaFinalizar)
    sgItens: TGridLuka;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
  private
    FCompraId: Integer;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Cancelar(pCompraId: Integer): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

{ TFormCancelarItensCompras }

const
  coProdutoId      = 0;
  coNome           = 1;
  coMarca          = 2;
  coUnidade        = 3;
  coQuantidade     = 4;
  coEntregues      = 5;
  coCancelados     = 6;
  coSaldo          = 7;
  coQtdeCancelar   = 8;
  coMultiploCompra = 9;
  coItemId         = 10;

function Cancelar(pCompraId: Integer): TRetornoTelaFinalizar;
var
  i: Integer;
  vForm: TFormCancelarItensCompras;
  vItens: TArray<RecComprasItens>;
begin
  if pCompraId = 0 then
    Exit;

  vItens := _ComprasItens.BuscarComprasItens(Sessao.getConexaoBanco, 1, [pCompraId]);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vForm := TFormCancelarItensCompras.Create(nil);
  vForm.FCompraId := pCompraId;

  for i := Low(vItens) to High(vItens) do begin
    vForm.sgItens.Cells[coProdutoId, i + 1]      := NFormat(vItens[i].ProdutoId);
    vForm.sgItens.Cells[coNome, i + 1]           := vItens[i].NomeProduto;
    vForm.sgItens.Cells[coMarca, i + 1]          := NFormat(vItens[i].MarcaId) + ' - ' + vItens[i].NomeMarca;
    vForm.sgItens.Cells[coUnidade, i + 1]        := vItens[i].UnidadeCompraId;
    vForm.sgItens.Cells[coQuantidade, i + 1]     := NFormatEstoque(vItens[i].Quantidade);
    vForm.sgItens.Cells[coEntregues, i + 1]      := NFormatEstoque(vItens[i].Entregues);
    vForm.sgItens.Cells[coCancelados, i + 1]     := NFormatEstoque(vItens[i].Cancelados);
    vForm.sgItens.Cells[coSaldo, i + 1]          := NFormatEstoque(vItens[i].Saldo);
    vForm.sgItens.Cells[coMultiploCompra, i + 1] := NFormat(vItens[i].MultiploCompra, 4);
    vForm.sgItens.Cells[coQtdeCancelar, i + 1]   := '';
    vForm.sgItens.Cells[coItemId, i + 1]         := NFormat(vItens[i].ItemId);
  end;
  vForm.sgItens.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vItens) );

  Result.Ok(vForm.ShowModal, True);
end;

procedure TFormCancelarItensCompras.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  TextCell := NFormatN( SFormatDouble(TextCell), 4 );

  if TextCell = '' then
    Exit;

  if not _Biblioteca.ValidarMultiplo( SFormatDouble(TextCell), SFormatDouble( sgItens.Cells[coMultiploCompra, ARow] ) ) then
    TextCell := '';

  if SFormatCurr( TextCell ) > SFormatCurr( sgItens.Cells[coSaldo, ARow] ) then begin
    _Biblioteca.Exclamar('A quantidade a cancelar n�o pode ser maior que o saldo dispon�vel, verifique!');
    TextCell := '';
  end;
end;

procedure TFormCancelarItensCompras.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coProdutoId,
    coQuantidade,
    coEntregues,
    coCancelados,
    coSaldo,
    coQtdeCancelar]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCancelarItensCompras.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coQtdeCancelar then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end;
end;

procedure TFormCancelarItensCompras.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coQtdeCancelar then
    sgItens.Options := sgItens.Options + [goEditing]
  else
    sgItens.Options := sgItens.Options - [goEditing];
end;

procedure TFormCancelarItensCompras.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

procedure TFormCancelarItensCompras.Finalizar(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vItens: TArray<RecComprasItens>;
begin
  vItens := nil;
  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coQtdeCancelar, i] = '' then
      Continue;

    SetLength(vItens, Length(vItens) + 1);

    vItens[High(vItens)].CompraId := FCompraId;
    vItens[High(vItens)].ItemId   := SFormatInt( sgItens.Cells[coItemId, i] );
    vItens[High(vItens)].QuantidadeCancelar := SFormatDouble( sgItens.Cells[coQtdeCancelar, i] );
  end;

  if vItens = nil then begin
    _Biblioteca.Exclamar('Nenhum item foi definido para cancelamento!');
    Abort;
  end;

  vRetBanco := _ComprasItens.CancelarItensCompra(Sessao.getConexaoBanco, FCompraId, vItens);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  RotinaSucesso;

  inherited;
end;

end.
