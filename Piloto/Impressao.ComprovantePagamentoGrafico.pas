unit Impressao.ComprovantePagamentoGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos, QRExport, QRPDFFilt, QRWebFilt, QRCtrls,
  QuickRpt, Vcl.ExtCtrls, _HerancaRelatorioSimples, _Sessao, _Biblioteca, _Orcamentos, _RecordsOrcamentosVendas,
  _OrcamentosPagamentos, _OrcamentosPagamentosCheques, _RecordsFinanceiros, _OrcamentosItens;

type
  RecCobrancas = record
    CobrancaId: Integer;
    NomeCobranca: string;
    Valor: Double;
    Dados: TArray<RecTitulosFinanceiros>;
  end;

  TFormImpressaoComprovanteGrafico = class(TFormHerancaRelatoriosGraficos)
    qrTitulo: TQRBand;
    QRShape2: TQRShape;
    qr2: TQRLabel;
    qrOrcamentoId: TQRLabel;
    qr8: TQRLabel;
    qrCliente: TQRLabel;
    qr4: TQRLabel;
    qrTelefoneCliente: TQRLabel;
    qr14: TQRLabel;
    qrCelularCliente: TQRLabel;
    qr9: TQRLabel;
    qrVendedor: TQRLabel;
    qr17: TQRLabel;
    qrCondicaoPagamento: TQRLabel;
    qrReportDinheiro: TQuickRep;
    ColumnHeaderBand1: TQRBand;
    qr16: TQRLabel;
    qrValorDinheiro: TQRLabel;
    qrReportCheque: TQuickRep;
    qrJuntar: TQRCompositeReport;
    qrReportCartao: TQuickRep;
    QRBand3: TQRBand;
    qrTipoCobrancaCartao: TQRLabel;
    qrValorCartao: TQRLabel;
    qrReportCobranca: TQuickRep;
    qrReportCredito: TQuickRep;
    QRBand6: TQRBand;
    qr27: TQRLabel;
    qrValorCredito: TQRLabel;
    qrReportFinal: TQuickRep;
    qrRodape: TQRBand;
    qrVersaoSistema: TQRLabel;
    TitleBand1: TQRBand;
    qr6: TQRLabel;
    qrValorCheque: TQRLabel;
    QRLabel2: TQRLabel;
    qrlOrcamentoIdNF: TQRLabel;
    QRLabel4: TQRLabel;
    qrlVendedorNF: TQRLabel;
    QRLabel6: TQRLabel;
    qrlClienteNF: TQRLabel;
    QRLabel8: TQRLabel;
    qrlTelefoneNF: TQRLabel;
    QRLabel10: TQRLabel;
    qrlCelularNF: TQRLabel;
    QRLabel12: TQRLabel;
    qrlCondicaoPagamentoNF: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    qrlTotalSerPagoNF: TQRLabel;
    qrlTotalDescontoNF: TQRLabel;
    qrlTotalOutrasDespesasNF: TQRLabel;
    qrlTotalProdutosNF: TQRLabel;
    QRLabel22: TQRLabel;
    qrNFReportDinheiro: TQuickRep;
    QRBand7: TQRBand;
    qrl1: TQRLabel;
    qrlNFValorDinheiro: TQRLabel;
    qrNFReportCartoes: TQuickRep;
    QRBand8: TQRBand;
    qrl2: TQRLabel;
    qrlNFValorCartoes: TQRLabel;
    qrl4: TQRLabel;
    qrl5: TQRLabel;
    QRBand9: TQRBand;
    qrlNFTipoCobrancaCartao: TQRLabel;
    qrlNFValorCobrancaCartao: TQRLabel;
    qrNFReportCheques: TQuickRep;
    QRBand10: TQRBand;
    qrl3: TQRLabel;
    qrl9: TQRLabel;
    qrl10: TQRLabel;
    qrl12: TQRLabel;
    QRBand11: TQRBand;
    qrlNFDataVencimentoCheque: TQRLabel;
    qrlNFNumeroCheque: TQRLabel;
    qrlNFValorCheque: TQRLabel;
    QRBand12: TQRBand;
    qrl21: TQRLabel;
    qrlNFValorCheques: TQRLabel;
    qrNFReportCobrancas: TQuickRep;
    QRBand13: TQRBand;
    qrl6: TQRLabel;
    qrlNFValorCobrancas: TQRLabel;
    QRBand14: TQRBand;
    qrlNFTipoCobranca: TQRLabel;
    qrlNFValorCobranca: TQRLabel;
    qrl13: TQRLabel;
    qrl14: TQRLabel;
    QRSubDetail2: TQRSubDetail;
    qrlNFDataVencimentoCobranca: TQRLabel;
    qrlNFValorCobrancaAnalitico: TQRLabel;
    qrNFReportCreditos: TQuickRep;
    qrNFReportPix: TQuickRep;
    QRBand15: TQRBand;
    qrl7: TQRLabel;
    qrlNFValorCreditos: TQRLabel;
    //qrlNFValorPix: TQRLabel;
    qrl8: TQRLabel;
    qrlNFValorFrete: TQRLabel;
    qrbndDetailBand2: TQRBand;
    qrlNFNomeProduto: TQRLabel;
    qrlNFQuantidade: TQRLabel;
    qrlCabProdutoId: TQRLabel;
    qrl11: TQRLabel;
    qrlNFMarca: TQRLabel;
    qrl15: TQRLabel;
    qrlNFUnidade: TQRLabel;
    qrl16: TQRLabel;
    qrlNFPrecoUnitario: TQRLabel;
    qrl17: TQRLabel;
    qrlNFValorTotal: TQRLabel;
    QRShape1: TQRShape;
    qrl18: TQRLabel;
    qrNFReportAcumulado: TQuickRep;
    qrbnd1: TQRBand;
    qrl19: TQRLabel;
    qrlNfValorAcumulado: TQRLabel;
    qr36: TQRLabel;
    qrNFDataCadastro: TQRLabel;
    qr38: TQRLabel;
    qrNFDataHoraRecebimento: TQRLabel;
    QuickRep1: TQuickRep;
    QRBand16: TQRBand;
    QRLabel1: TQRLabel;
    qrObservacao: TQRLabel;
    QRBand17: TQRBand;
    QRShape7: TQRShape;
    QRLabel28: TQRLabel;
    qrNomeProdutoA4: TQRLabel;
    QRLabel33: TQRLabel;
    qrMarcaA4: TQRLabel;
    QRLabel36: TQRLabel;
    qrQuantidadeA4: TQRLabel;
    qrUnidadeA4: TQRLabel;
    QRLabel42: TQRLabel;
    qrlNFPrecoUnitarioA4: TQRLabel;
    QRLabel44: TQRLabel;
    qrlNFValorTotalA4: TQRLabel;
    qrluniA4: TQRLabel;
    QRBand18: TQRBand;
    QRLabel3: TQRLabel;
    qrlNFValorPix: TQRLabel;
    qrReportPix: TQuickRep;
    QRBand19: TQRBand;
    QRLabel7: TQRLabel;
    qrValorPix: TQRLabel;
    qpObservacoesGrafico: TQuickRep;
    QRBand20: TQRBand;
    QRLabel5: TQRLabel;
    qrObservacoesGrafico: TQRLabel;
    qpObservacoesPedidoNF: TQuickRep;
    QRBand21: TQRBand;
    QRLabel9: TQRLabel;
    qrObservacoesPedidoNF: TQRLabel;
    qrReportAcumulado: TQuickRep;
    QRBand22: TQRBand;
    QRLabel11: TQRLabel;
    qrValorAcumulado: TQRLabel;
    qrInformacaoComplementar: TQuickRep;
    QRBand23: TQRBand;
    QRLabel13: TQRLabel;
    qrlNFComplemento: TQRLabel;
    qpInformacaoComplementarGrafico: TQuickRep;
    QRBand24: TQRBand;
    QRLabel18: TQRLabel;
    qrInformacaoComplementarGrafico: TQRLabel;
    QRShape3: TQRShape;
    QRLabel19: TQRLabel;
    qrCodigoOriginalA4: TQRLabel;
    QRLabel20: TQRLabel;
    qrlNFCodigoOriginal: TQRLabel;
    QRLabel21: TQRLabel;
    qrEnderecoCliente: TQRLabel;
    QRLabel24: TQRLabel;
    qrBairroCliente: TQRLabel;
    QRLabel26: TQRLabel;
    qrCidadeCliente: TQRLabel;
    QRLabel29: TQRLabel;
    qrCEPCliente: TQRLabel;
    QRBand25: TQRBand;
    QRShape5: TQRShape;
    qr28: TQRLabel;
    qr30: TQRLabel;
    qr31: TQRLabel;
    qr29: TQRLabel;
    qrTotalSerPago: TQRLabel;
    qrValorDesconto: TQRLabel;
    qrValorOutrasDespesas: TQRLabel;
    qrTotalProdutos: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape4: TQRShape;
    QRShape6: TQRShape;
    QRLabel23: TQRLabel;
    QRLabel27: TQRLabel;
    qrInsEstadualCliente: TQRLabel;
    qrCnpjCliente: TQRLabel;
    QRBand5: TQRBand;
    qrTipoCobranca: TQRLabel;
    qr34: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    qrVencimentoCobranca: TQRLabel;
    qrValorCobranca: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel31: TQRLabel;
    qrDataRecebimento: TQRLabel;
    qrPesoTotal111: TQRLabel;
    qrPesoTotal: TQRLabel;
    QRLabel30: TQRLabel;
    qrlPesoTotalNF: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure qrReportChequeBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrReportChequeNeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrReportCartaoBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrReportCartaoNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrReportCobrancaNeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrReportCobrancaBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure QRBand5BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRSubDetail1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRSubDetail1NeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrJuntarAddReports(Sender: TObject);
    procedure qrNFReportCartoesBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrNFReportCartoesNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand9BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrNFReportChequesBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrNFReportChequesNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand11BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrNFReportCobrancasNeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrNFReportCobrancasBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure QRBand14BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRSubDetail2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrbndDetailBand2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRBand17BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qrRelatorioA4BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrRelatorioA4NeedData(Sender: TObject; var MoreData: Boolean);
  private
    FOrcamentoId: Integer;

    FPosicItens: Integer;
    FItens: TArray<RecOrcamentoItens>;
    FValorCartao: Double;
    FValorCobranca: Double;

    FPosicCheque: Integer;
    FCheques: TArray<RecTitulosFinanceiros>;

    FPosicCartao: Integer;
    FCartoes: TArray<RecTitulosFinanceiros>;

    FPosicCobrancas: Integer;
    FPosicItensCobr: Integer;
    FCobrancas: TArray<RecCobrancas>;
  public
    { Public declarations }
  end;

procedure Imprimir(const pOrcamentoId: Integer);

implementation

{$R *.dfm}

procedure Imprimir(const pOrcamentoId: Integer);
var
  vOrcamento: TArray<RecOrcamentos>;
  vForm: TFormImpressaoComprovanteGrafico;
  vPesoTotal: Double;
  i: Integer;

  vImpressora: RecImpressora;
begin
  if not Sessao.AutorizadoRotina('reimprimir_comprovante_pagamento') then begin
    _Biblioteca.Exclamar('Voc� n�o possui autoriza��o para reimprimir o comprovante de pagamento!');
    Exit;
  end;

  if pOrcamentoId = 0 then
    Exit;

  vOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 3, [pOrcamentoId]);
  if vOrcamento = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vImpressora := Sessao.getImpressora( toComprovantePagamento );
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;

  vForm := TFormImpressaoComprovanteGrafico.Create(Application, vImpressora);

  vForm.FItens := _OrcamentosItens.BuscarOrcamentosItens(Sessao.getConexaoBanco, 0, [pOrcamentoId]);
  vForm.FOrcamentoId := pOrcamentoId;
  vForm.PreencherCabecalho(Sessao.getEmpresaLogada.EmpresaId);

  vPesoTotal := 0;
  for I := Low(vForm.FItens) to High(vForm.FItens) do begin
    vPesoTotal := vPesoTotal + vForm.FItens[i].PesoTotal;
  end;

  if vForm.ImpressaoGrafica then begin
    vForm.qrOrcamentoId.Caption       := NFormat(vOrcamento[0].orcamento_id);
    vForm.qrVendedor.Caption          := NFormat(vOrcamento[0].vendedor_id) + ' - ' + vOrcamento[0].nome_vendedor;
    vForm.qrCondicaoPagamento.Caption := NFormat(vOrcamento[0].condicao_id) + ' - ' + vOrcamento[0].nome_condicao_pagto;
    vForm.qrCliente.Caption           := NFormat(vOrcamento[0].cliente_id) + ' - ' + vOrcamento[0].nome_cliente;
    vForm.qrEnderecoCliente.Caption   := vOrcamento[0].LogradouroCliente + ' ' + vOrcamento[0].ComplementoCliente + ' ' + vOrcamento[0].NumeroCliente;
    vForm.qrBairroCliente.Caption     := vOrcamento[0].NomeBairroCliente;
    vForm.qrCidadeCliente.Caption     := vOrcamento[0].NomeCidadeCliente + ' - ' + vOrcamento[0].UfCliente;
    vForm.qrCEPCliente.Caption        := vOrcamento[0].CepCliente;
    vForm.qrDataRecebimento.Caption   := DFormat(vOrcamento[0].data_hora_recebimento) + ' �s ' + HFormat(vOrcamento[0].data_hora_recebimento);
    vForm.qrTelefoneCliente.Caption   := vOrcamento[0].telefone_principal;
    vForm.qrCnpjCliente.Caption       := vOrcamento[0].cpfCnpjCliente;
    vForm.qrInsEstadualCliente.Caption := vOrcamento[0].InscricaoEstadualCliente;
    vForm.qrCelularCliente.Caption    := vOrcamento[0].telefone_celular;
    vForm.qrTotalProdutos.Caption     := NFormat(vOrcamento[0].valor_total_produtos);
    vForm.qrPesoTotal.Caption         := NFormat(vPesoTotal);
    vForm.qrValorOutrasDespesas.Caption := NFormat(vOrcamento[0].valor_outras_despesas);
    vForm.qrValorDesconto.Caption     := NFormat(vOrcamento[0].valor_desconto);
    vForm.qrTotalSerPago.Caption      := NFormat(vOrcamento[0].valor_total);
    vForm.qrValorDinheiro.Caption     := NFormatN(vOrcamento[0].valor_dinheiro);
    vForm.qrValorAcumulado.Caption    := NFormatN(vOrcamento[0].ValorAcumulativo);
    vForm.qrValorCheque.Caption       := NFormatN(vOrcamento[0].valor_cheque);
    vForm.FValorCartao                := vOrcamento[0].ValorCartaoDebito + vOrcamento[0].ValorCartaoCredito;
    vForm.FValorCobranca              := vOrcamento[0].valor_cobranca;
    vForm.qrValorCredito.Caption      := NFormatN(vOrcamento[0].valor_credito);
    vForm.qrValorPix.Caption          := NFormatN(vOrcamento[0].valor_pix);
    vForm.qrObservacoesGrafico.Caption := vOrcamento[0].ObservacoesCaixa;
    vForm.qrInformacaoComplementarGrafico.Caption := vOrcamento[0].infComplCompPagamentos;
  end
  else begin
    vForm.qrlOrcamentoIdNF.Caption         := NFormat(vOrcamento[0].orcamento_id);
    vForm.qrlVendedorNF.Caption            := NFormat(vOrcamento[0].vendedor_id) + ' - ' + vOrcamento[0].nome_vendedor;
    vForm.qrlCondicaoPagamentoNF.Caption   := NFormat(vOrcamento[0].condicao_id) + ' - ' + vOrcamento[0].nome_condicao_pagto;
    vForm.qrlClienteNF.Caption             := NFormat(vOrcamento[0].cliente_id) + ' - ' + vOrcamento[0].nome_cliente;
    vForm.qrlTelefoneNF.Caption            := vOrcamento[0].telefone_principal;
    vForm.qrlCelularNF.Caption             := vOrcamento[0].telefone_celular;
    vForm.qrNFDataCadastro.Caption         := DFormat(vOrcamento[0].data_cadastro);
    vForm.qrlPesoTotalNF.Caption           := NFormat(vPesoTotal);
    vForm.qrNFDataHoraRecebimento.Caption  := DHFormat(vOrcamento[0].data_hora_recebimento);
    vForm.qrlTotalProdutosNF.Caption       := NFormat(vOrcamento[0].valor_total_produtos);
    vForm.qrlTotalOutrasDespesasNF.Caption := NFormat(vOrcamento[0].valor_outras_despesas);
    vForm.qrlNFValorFrete.Caption          := NFormat(vOrcamento[0].ValorFrete + vOrcamento[0].ValorFreteItens);
    vForm.qrlTotalDescontoNF.Caption       := NFormat(vOrcamento[0].valor_desconto);
    vForm.qrlTotalSerPagoNF.Caption        := NFormat(vOrcamento[0].valor_total);
    vForm.qrlNFValorDinheiro.Caption       := NFormatN(vOrcamento[0].valor_dinheiro);
    vForm.qrlNFValorCartoes.Caption        := NFormatN(vOrcamento[0].ValorCartaoDebito + vOrcamento[0].ValorCartaoCredito);
    vForm.qrlNFValorCheques.Caption        := NFormatN(vOrcamento[0].valor_cheque);
    vForm.qrlNFValorCobrancas.Caption      := NFormatN(vOrcamento[0].valor_cobranca);
    vForm.qrlNFValorCreditos.Caption       := NFormatN(vOrcamento[0].valor_credito);
    vForm.qrlNFValorPix.Caption            := NFormatN(vOrcamento[0].valor_pix);
    vForm.qrlNfValorAcumulado.Caption      := NFormatN(vOrcamento[0].ValorAcumulativo);
    vForm.qrlNFComplemento.Caption          := vOrcamento[0].infComplCompPagamentos;
    vForm.qrObservacoesPedidoNF.Caption    := vOrcamento[0].ObservacoesCaixa;
  end;

  vForm.qrJuntar.PrinterSettings.PrinterIndex := vImpressora.getIndex;

  if vImpressora.AbrirPreview then
    vForm.qrJuntar.Preview
  else
    vForm.qrJuntar.Print;

  vForm.Free;
end;

procedure TFormImpressaoComprovanteGrafico.FormCreate(Sender: TObject);
begin
  inherited;
  FCheques   := nil;
  FCartoes   := nil;
  FCobrancas := nil;
end;

procedure TFormImpressaoComprovanteGrafico.QRBand11BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrlNFDataVencimentoCheque.Caption := DFormat(FCheques[FPosicCheque].DataVencimento);
  qrlNFNumeroCheque.Caption         := NFormat(FCheques[FPosicCheque].NumeroCheque);
  qrlNFValorCheque.Caption         := NFormat(FCheques[FPosicCheque].Valor);
end;

procedure TFormImpressaoComprovanteGrafico.QRBand14BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FPosicItensCobr := -1;
  qrlNFTipoCobranca.Caption  := NFormat(FCobrancas[FPosicCobrancas].CobrancaId) + ' - ' + FCobrancas[FPosicCobrancas].NomeCobranca;
  qrlNFValorCobranca.Caption := NFormat(FCobrancas[FPosicCobrancas].Valor);
end;

procedure TFormImpressaoComprovanteGrafico.QRBand17BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  quantidade: Double;
begin
  inherited;
 qrNomeProdutoA4.Caption    := _Biblioteca.NFormat(FItens[FPosicItens].produto_id) + ' - ' + FItens[FPosicItens].nome;
  if Length(qrNomeProdutoA4.Caption) > 38 then
    qrNomeProdutoA4.Font.Size := 7
  else
    qrNomeProdutoA4.Font.Size := 7;

  qrMarcaA4.Caption          := FItens[FPosicItens].nome_marca;
  qrCodigoOriginalA4.Caption := FItens[FPosicItens].CodigoOriginalFabricante;
  if FItens[FPosicItens].UnidadeEntregaId <> '' then begin

    //Ajustando quandidade   1
    quantidade := FItens[FPosicItens].Quantidade / FItens[FPosicItens].multiplo_venda;
    if Frac(quantidade) <> 0 then
      qrQuantidadeA4.Caption   := _Biblioteca.NFormatEstoque(quantidade)
    else
      qrQuantidadeA4.Caption   := IntToStr(Trunc(quantidade));
    //  fim 1

    qrUnidadeA4.Caption        := FItens[FPosicItens].UnidadeEntregaId;
  end
  else begin
    // Ajustando quandidade   2
    quantidade := FItens[FPosicItens].Quantidade;
    if Frac(quantidade) <> 0 then
      qrQuantidadeA4.Caption   := _Biblioteca.NFormatEstoque(quantidade)
    else
      qrQuantidadeA4.Caption   := IntToStr(Trunc(quantidade));
   // fim 2

    qrUnidadeA4.Caption        := FItens[FPosicItens].unidade_venda;
  end;

  qrlNFPrecoUnitarioA4.Caption := NFormat(FItens[FPosicItens].preco_unitario);
  qrlNFValorTotalA4.Caption    := NFormat(FItens[FPosicItens].valor_total);

end;

procedure TFormImpressaoComprovanteGrafico.QRBand3BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrTipoCobrancaCartao.Caption := NFormat(FCartoes[FPosicCartao].CobrancaId) + ' - ' + FCartoes[FPosicCartao].NomeCobranca;
  qrValorCartao.Caption        := NFormat(FCartoes[FPosicCartao].valor);
end;

procedure TFormImpressaoComprovanteGrafico.QRBand5BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FPosicItensCobr := -1;
  qrTipoCobranca.Caption := NFormat(FCobrancas[FPosicCobrancas].CobrancaId) + ' - ' + FCobrancas[FPosicCobrancas].NomeCobranca;
  //qrValorTipoCobranca.Caption := NFormat(FCobrancas[FPosicCobrancas].Valor);
end;

procedure TFormImpressaoComprovanteGrafico.QRBand9BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrlNFTipoCobrancaCartao.Caption  := NFormat(FCartoes[FPosicCartao].CobrancaId) + ' - ' + FCartoes[FPosicCartao].NomeCobranca;
  qrlNFValorCobrancaCartao.Caption := NFormat(FCartoes[FPosicCartao].valor);
end;

procedure TFormImpressaoComprovanteGrafico.qrbndDetailBand2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrlNFNomeProduto.Caption    := _Biblioteca.NFormat(FItens[FPosicItens].produto_id) + ' - ' + FItens[FPosicItens].nome;
  if Length(qrlNFNomeProduto.Caption) > 38 then
    qrlNFNomeProduto.Font.Size := 6
  else
    qrlNFNomeProduto.Font.Size := 7;

  qrlNFMarca.Caption          := FItens[FPosicItens].nome_marca;
  qrlNFCodigoOriginal.Caption := FItens[FPosicItens].CodigoOriginalFabricante;

  if FItens[FPosicItens].UnidadeEntregaId <> '' then begin
    qrlNFQuantidade.Caption     := _Biblioteca.NFormatEstoque(FItens[FPosicItens].Quantidade / FItens[FPosicItens].multiplo_venda);
    qrlNFUnidade.Caption        := FItens[FPosicItens].UnidadeEntregaId;
  end
  else begin
    qrlNFQuantidade.Caption     := _Biblioteca.NFormatEstoque(FItens[FPosicItens].Quantidade);
    qrlNFUnidade.Caption        := FItens[FPosicItens].unidade_venda;
  end;

  qrlNFPrecoUnitario.Caption := NFormat(FItens[FPosicItens].preco_unitario);
  qrlNFValorTotal.Caption    := NFormat(FItens[FPosicItens].valor_total);
end;

procedure TFormImpressaoComprovanteGrafico.qrJuntarAddReports(Sender: TObject);
var
  i: Integer;
  vCobrancaId: Integer;
  vCobrancas: TArray<RecTitulosFinanceiros>;
begin
  inherited;
  if ImpressaoGrafica then begin
    qrJuntar.Reports.Add(qrRelatorioA4);

    if qrValorDinheiro.Caption <> '' then
      qrJuntar.Reports.Add(qrReportDinheiro);

    if qrValorCheque.Caption <> '' then begin
      qrJuntar.Reports.Add(qrReportCheque);
      if FCheques = nil then
        FCheques := _OrcamentosPagamentosCheques.BuscarOrcamentosPagamentosCheques(Sessao.getConexaoBanco, 0, [FOrcamentoId]);
    end;

    if FValorCartao > 0 then begin
      qrJuntar.Reports.Add(qrReportCartao);
      if FCartoes = nil then
        FCartoes := _OrcamentosPagamentos.BuscarOrcamentosPagamentos(Sessao.getConexaoBanco, 4, [FOrcamentoId]);
    end;

    if FValorCobranca > 0 then begin
      vCobrancaId := -1;
      qrJuntar.Reports.Add(qrReportCobranca);
      if FCobrancas = nil then begin
        vCobrancas := _OrcamentosPagamentos.BuscarOrcamentosPagamentos(Sessao.getConexaoBanco, 1, [FOrcamentoId]);
        for i := Low(vCobrancas) to High(vCobrancas) do begin
          if vCobrancaId <> vCobrancas[i].CobrancaId then begin
            vCobrancaId := vCobrancas[i].CobrancaId;
            SetLength(FCobrancas, Length(FCobrancas) + 1);
            FCobrancas[High(FCobrancas)].Valor        := vCobrancas[i].valor;
            FCobrancas[High(FCobrancas)].CobrancaId   := vCobrancas[i].CobrancaId;
            FCobrancas[High(FCobrancas)].NomeCobranca := vCobrancas[i].NomeCobranca;
          end
          else
            FCobrancas[High(FCobrancas)].Valor := FCobrancas[High(FCobrancas)].Valor + vCobrancas[i].valor;

          SetLength(FCobrancas[High(FCobrancas)].Dados, Length(FCobrancas[High(FCobrancas)].Dados) + 1);
          FCobrancas[High(FCobrancas)].Dados[High(FCobrancas[High(FCobrancas)].Dados)] := vCobrancas[i];
        end;
      end;
    end;

    if qrValorPix.Caption <> '' then
      qrJuntar.Reports.Add( qrReportPix );

    if qrValorAcumulado.Caption <> '' then
      qrJuntar.Reports.Add( qrReportAcumulado );

    if qrValorCredito.Caption <> '' then
      qrJuntar.Reports.Add(qrReportCredito);

    if qrObservacoesGrafico.Caption <> '' then
      qrJuntar.Reports.Add( qpObservacoesGrafico );

    if qrInformacaoComplementarGrafico.Caption <> '' then
      qrJuntar.Reports.Add( qpInformacaoComplementarGrafico );

    qrJuntar.Reports.Add(qrReportFinal);
  end
  else begin
    qrJuntar.Reports.Add( qrRelatorioNF );

    if qrlNFValorDinheiro.Caption <> '' then
      qrJuntar.Reports.Add( qrNFReportDinheiro );

    if qrlNFValorCartoes.Caption <> '' then begin
      qrJuntar.Reports.Add(qrNFReportCartoes);
      if FCartoes = nil then
        FCartoes := _OrcamentosPagamentos.BuscarOrcamentosPagamentos(Sessao.getConexaoBanco, 4, [FOrcamentoId]);
    end;

    if qrlNFValorCheques.Caption <> '' then begin
      qrJuntar.Reports.Add(qrNFReportCheques);
      if FCheques = nil then
        FCheques := _OrcamentosPagamentosCheques.BuscarOrcamentosPagamentosCheques(Sessao.getConexaoBanco, 0, [FOrcamentoId]);
    end;

    if qrlNFValorCobrancas.Caption <> '' then begin
      vCobrancaId := -1;
      qrJuntar.Reports.Add(qrNFReportCobrancas);
      if FCobrancas = nil then begin
        vCobrancas := _OrcamentosPagamentos.BuscarOrcamentosPagamentos(Sessao.getConexaoBanco, 1, [FOrcamentoId]);
        for i := Low(vCobrancas) to High(vCobrancas) do begin
          if vCobrancaId <> vCobrancas[i].CobrancaId then begin
            vCobrancaId := vCobrancas[i].CobrancaId;
            SetLength(FCobrancas, Length(FCobrancas) + 1);
            FCobrancas[High(FCobrancas)].Valor        := vCobrancas[i].valor;
            FCobrancas[High(FCobrancas)].CobrancaId   := vCobrancas[i].CobrancaId;
            FCobrancas[High(FCobrancas)].NomeCobranca := vCobrancas[i].NomeCobranca;
          end
          else
            FCobrancas[High(FCobrancas)].Valor := FCobrancas[High(FCobrancas)].Valor + vCobrancas[i].valor;

          SetLength(FCobrancas[High(FCobrancas)].Dados, Length(FCobrancas[High(FCobrancas)].Dados) + 1);
          FCobrancas[High(FCobrancas)].Dados[High(FCobrancas[High(FCobrancas)].Dados)] := vCobrancas[i];
        end;
      end;
    end;

    if qrlNFValorCreditos.Caption <> '' then
      qrJuntar.Reports.Add( qrNFReportCreditos );

    if qrlNFValorPix.Caption <> '' then
      qrJuntar.Reports.Add( qrNFReportPix );

    if qrlNfValorAcumulado.Caption <> '' then
      qrJuntar.Reports.Add( qrNFReportAcumulado );

    if qrObservacoesPedidoNF.Caption <> '' then
      qrJuntar.Reports.Add( qpObservacoesPedidoNF );

    if qrlNFComplemento.Caption <> '' then
      qrJuntar.Reports.Add( qrInformacaoComplementar );
  end;
end;

procedure TFormImpressaoComprovanteGrafico.qrNFReportCartoesBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicCartao := -1;
end;

procedure TFormImpressaoComprovanteGrafico.qrNFReportCartoesNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicCartao);
  MoreData := FPosicCartao < Length(FCartoes);
end;

procedure TFormImpressaoComprovanteGrafico.qrNFReportChequesBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicCheque := -1;
end;

procedure TFormImpressaoComprovanteGrafico.qrNFReportChequesNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicCheque);
  MoreData := FPosicCheque < Length(FCheques);
end;

procedure TFormImpressaoComprovanteGrafico.qrNFReportCobrancasBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicCobrancas := -1;
end;

procedure TFormImpressaoComprovanteGrafico.qrNFReportCobrancasNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicCobrancas);
  MoreData := FPosicCobrancas < Length(FCobrancas);
end;

procedure TFormImpressaoComprovanteGrafico.qrRelatorioA4BeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicItens := -1;
end;

procedure TFormImpressaoComprovanteGrafico.qrRelatorioA4NeedData(
  Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicItens);
  MoreData := FPosicItens < Length(FItens);
end;

procedure TFormImpressaoComprovanteGrafico.qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicItens := -1;
end;

procedure TFormImpressaoComprovanteGrafico.qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicItens);
  MoreData := FPosicItens < Length(FItens);
end;

procedure TFormImpressaoComprovanteGrafico.qrReportCartaoBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicCartao := -1;
end;

procedure TFormImpressaoComprovanteGrafico.qrReportCartaoNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicCartao);
  MoreData := FPosicCartao < Length(FCartoes);
end;

procedure TFormImpressaoComprovanteGrafico.qrReportChequeBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicCheque := -1;
end;

procedure TFormImpressaoComprovanteGrafico.qrReportChequeNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicCheque);
  MoreData := FPosicCheque < Length(FCheques);
end;

procedure TFormImpressaoComprovanteGrafico.qrReportCobrancaBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicCobrancas := -1;
end;

procedure TFormImpressaoComprovanteGrafico.qrReportCobrancaNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicCobrancas);
  MoreData := FPosicCobrancas < Length(FCobrancas);
end;

procedure TFormImpressaoComprovanteGrafico.QRSubDetail1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrVencimentoCobranca.Caption := DFormat(FCobrancas[FPosicCobrancas].Dados[FPosicItensCobr].DataVencimento);
  qrValorCobranca.Caption      := NFormat(FCobrancas[FPosicCobrancas].Dados[FPosicItensCobr].valor);
end;

procedure TFormImpressaoComprovanteGrafico.QRSubDetail1NeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicItensCobr);
  MoreData := FPosicItensCobr < Length(FCobrancas[FPosicCobrancas].Dados);
end;

procedure TFormImpressaoComprovanteGrafico.QRSubDetail2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrlNFDataVencimentoCobranca.Caption := DFormat(FCobrancas[FPosicCobrancas].Dados[FPosicItensCobr].DataVencimento);
  qrlNFValorCobrancaAnalitico.Caption := NFormat(FCobrancas[FPosicCobrancas].Dados[FPosicItensCobr].valor);
end;

end.
