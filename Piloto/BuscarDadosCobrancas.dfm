inherited FormBuscarDadosCobranca: TFormBuscarDadosCobranca
  Caption = 'Buscar dados de cobran'#231'as'
  ClientHeight = 348
  ClientWidth = 587
  OnShow = FormShow
  ExplicitWidth = 593
  ExplicitHeight = 377
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 311
    Width = 587
    ExplicitTop = 311
    ExplicitWidth = 587
    inherited sbFinalizar: TSpeedButton
      Left = 256
      ExplicitLeft = 256
    end
  end
  object stCondicaoPagamento: TStaticText
    Left = 4
    Top = 16
    Width = 579
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 5921326
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    Transparent = False
  end
  object stNomeCondicao: TStaticTextLuka
    Left = 4
    Top = 0
    Width = 579
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Condi'#231#227'o de pagamento'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  inline FrDadosCobranca: TFrDadosCobranca
    Left = 5
    Top = 34
    Width = 578
    Height = 277
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 5
    ExplicitTop = 34
    ExplicitHeight = 277
    inherited pnInformacoesPrincipais: TPanel
      inherited lbllb10: TLabel
        Width = 39
        Height = 14
        ExplicitWidth = 39
        ExplicitHeight = 14
      end
      inherited lbllb9: TLabel
        Width = 29
        Height = 14
        ExplicitWidth = 29
        ExplicitHeight = 14
      end
      inherited lbllb6: TLabel
        Width = 28
        Height = 14
        ExplicitWidth = 28
        ExplicitHeight = 14
      end
      inherited lbllb2: TLabel
        Width = 64
        Height = 14
        ExplicitWidth = 64
        ExplicitHeight = 14
      end
      inherited lbl2: TLabel
        Width = 62
        Height = 14
        ExplicitWidth = 62
        ExplicitHeight = 14
      end
      inherited eRepetir: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited ePrazo: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eDataVencimento: TEditLukaData
        Height = 22
        ExplicitHeight = 22
      end
      inherited FrTiposCobranca: TFrTiposCobranca
        inherited PnTitulos: TPanel
          inherited lbNomePesquisa: TLabel
            Height = 15
          end
        end
      end
      inherited eDocumento: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
    inherited pnInformacoesBoleto: TPanel
      inherited lbllb31: TLabel
        Width = 79
        Height = 14
        ExplicitWidth = 79
        ExplicitHeight = 14
      end
      inherited lbl1: TLabel
        Width = 92
        Height = 14
        ExplicitWidth = 92
        ExplicitHeight = 14
      end
      inherited eNossoNumero: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eCodigoBarras: TMaskEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
    inherited pn1: TPanel
      Height = 164
      ExplicitHeight = 164
      inherited sgCobrancas: TGridLuka
        Top = 2
        Height = 160
        ExplicitTop = 2
        ExplicitHeight = 160
      end
      inherited stDiferenca: TStaticText
        Font.Color = clRed
      end
      inherited StaticTextLuka4: TStaticTextLuka
        Color = 38619
        Font.Color = clBlack
      end
      inherited stPrazoMedioCalc: TStaticText
        Top = 144
        ExplicitTop = 144
      end
    end
    inherited pnInformacoesCheque: TPanel
      inherited lbllb1: TLabel
        Width = 33
        Height = 14
        ExplicitWidth = 33
        ExplicitHeight = 14
      end
      inherited lbllb3: TLabel
        Width = 43
        Height = 14
        ExplicitWidth = 43
        ExplicitHeight = 14
      end
      inherited lbllb4: TLabel
        Width = 79
        Height = 14
        ExplicitWidth = 79
        ExplicitHeight = 14
      end
      inherited lbllb5: TLabel
        Width = 71
        Height = 14
        ExplicitWidth = 71
        ExplicitHeight = 14
      end
      inherited lbl3: TLabel
        Width = 85
        Height = 14
        ExplicitWidth = 85
        ExplicitHeight = 14
      end
      inherited lbl4: TLabel
        Width = 48
        Height = 14
        ExplicitWidth = 48
        ExplicitHeight = 14
      end
      inherited lbCPF_CNPJ: TLabel
        Width = 52
        Height = 14
        ExplicitWidth = 52
        ExplicitHeight = 14
      end
      inherited eBanco: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eAgencia: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eContaCorrente: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eNumeroCheque: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eNomeEmitente: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eTelefoneEmitente: TEditTelefoneLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eCPF_CNPJ: TEditCPF_CNPJ_Luka
        Height = 22
        ExplicitHeight = 22
      end
    end
  end
end
