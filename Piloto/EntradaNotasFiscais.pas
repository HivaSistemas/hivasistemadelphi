unit EntradaNotasFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  _HerancaCadastroCodigo, Vcl.StdCtrls, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _Biblioteca, _Sessao, FrameCFOPs,
  _FrameHenrancaPesquisas, FrameFornecedores, Vcl.ComCtrls, ComboBoxLuka, Vcl.Mask, EditLukaData, RadioGroupLuka, _RecordsEstoques,
  Vcl.Grids, GridLuka, _Produtos, FrameProdutos, FrameDiretorioArquivo, _BibliotecaNFE, _RecordsCadastros, PesquisaProdutos, CadastroProdutos,
  System.StrUtils, _RecordsEspeciais, _EntradasNotasFiscais, _EntradasNotasFiscaisItens, FrameTiposCobranca, System.DateUtils,
  System.Math, _EntradasNotasFiscFinanceiro, _EntradaNotasFiscaisCalculos, Pesquisa.EntradaNotasFiscais, _EntradasNfItensLotes,
  _HerancaCadastro, CarregarXmlEntradaNotasFiscais, StaticTextLuka, _RecordsFinanceiros, FrameDadosCobranca, FrameLocais,
  Frame.HerancaInsercaoExclusao, FrameBuscarLotes, FrameBuscarPedidosCompras, FrameEmpresas, _ConhecimentosFretesItens,
  SpeedButtonLuka, PesquisaCompras, FramePlanosFinanceiros, FrameCentroCustos, _EntradasNFItensCompras, FrameMultiplosCompra, _GruposTribEstadualCompra,
  Vcl.Menus, PesquisaLocaisProdutos, CheckBoxLuka, InformacoesPrecoLiquidoItemEntrada, _ParametrosEmpresa, _ProdutosGruposTribEmpresas,
  _ProdutosOrdensLocEntregas, BuscarDadosOutrosCustosEntradas, _ComprasItens, ConhecimentosFretes, ConferenciaCustosTributacoesEntradas;

type
  TFormEntradaNotasFiscais = class(TFormHerancaCadastroCodigo)
    FrFornecedor: TFrFornecedores;
    eNumeroNotaFiscal: TEditLuka;
    lb1: TLabel;
    lb2: TLabel;
    pcDados: TPageControl;
    tsPrincipais: TTabSheet;
    tsProdutos: TTabSheet;
    cbModeloNota: TComboBoxLuka;
    lb3: TLabel;
    cbSerieNota: TComboBoxLuka;
    lb4: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    eBaseCalculoICMS: TEditLuka;
    eValorICMS: TEditLuka;
    lb8: TLabel;
    eBaseCalculoICMSST: TEditLuka;
    lb9: TLabel;
    eValorICMSST: TEditLuka;
    lb10: TLabel;
    eValorTotalProdutos: TEditLuka;
    lb11: TLabel;
    eOutrasDespesas: TEditLuka;
    lb12: TLabel;
    lb14: TLabel;
    eValorIPI: TEditLuka;
    eValorFrete: TEditLuka;
    lb15: TLabel;
    eDataContabil: TEditLukaData;
    eDataEmissaoNota: TEditLukaData;
    rgTipoRateioFrete: TRadioGroupLuka;
    eValorDesconto: TEditLuka;
    lb16: TLabel;
    lb17: TLabel;
    eValorTotalNota: TEditLuka;
    lb19: TLabel;
    eQuantidade: TEditLuka;
    FrProduto: TFrProdutos;
    eValorUnitario: TEditLuka;
    lb20: TLabel;
    sgProdutos: TGridLuka;
    eValorDescontoProd: TEditLuka;
    lb21: TLabel;
    lb22: TLabel;
    eTotalProduto: TEditLuka;
    lb23: TLabel;
    ePercentualICMS: TEditLuka;
    ePercentualIPI: TEditLuka;
    lb24: TLabel;
    lb25: TLabel;
    ckAtualizarCustoCompra: TCheckBox;
    ckAtualizarCustoEntrada: TCheckBox;
    lb32: TLabel;
    eIndiceReducaoBaseCalculoICMS: TEditLuka;
    eChaveAcessoNFe: TMaskEdit;
    lbl1: TLabel;
    eValorOutDespesasProd: TEditLuka;
    cbCst: TComboBoxLuka;
    eBaseCalcICMSProd: TEditLuka;
    lbl2: TLabel;
    eValorICMSProd: TEditLuka;
    lbl3: TLabel;
    eValorIPIProd: TEditLuka;
    lbl4: TLabel;
    eBaseCalcICMSSTProd: TEditLuka;
    lbl5: TLabel;
    eValorICMSStProd: TEditLuka;
    lbl6: TLabel;
    lbl7: TLabel;
    cbStatusEntrada: TComboBoxLuka;
    StaticTextLuka1: TStaticTextLuka;
    FrLocalProduto: TFrLocais;
    FrBuscarLotes: TFrBuscarLotes;
    st5: TStaticText;
    stQtdeTotal: TStaticText;
    stValorTotal: TStaticText;
    st1: TStaticText;
    FrBuscarPedidosCompras: TFrBuscarPedidosCompras;
    eDataPrimeiraParcela: TEditLukaData;
    lbl8: TLabel;
    FrEmpresa: TFrEmpresas;
    lbl9: TLabel;
    cbModalidadeFrete: TComboBoxLuka;
    lblTipoMovimento: TLabel;
    eConhecimentoId: TEditLuka;
    eValorConhecimento: TEditLuka;
    sbInformacoesConhecimento: TSpeedButtonLuka;
    FrPlanoFinanceiro: TFrPlanosFinanceiros;
    FrCentroCusto: TFrCentroCustos;
    FrDadosCobranca: TFrDadosCobranca;
    FrMultiplosCompra: TFrMultiplosCompra;
    st2: TStaticText;
    stPesoTotal: TStaticText;
    eValorFreteItem: TEditLuka;
    lbl10: TLabel;
    FrCFOPCapa: TFrCFOPs;
    FrCFOPProduto: TFrCFOPs;
    pmOpcoes: TPopupMenu;
    miExcluirProduto: TMenuItem;
    miDefinirLocalEntradaTodosProdutos: TMenuItem;
    lb13: TLabel;
    eIndiceReducaoICMSST: TEditLuka;
    miN1: TMenuItem;
    miInformaesPrecoFinalLiquido: TMenuItem;
    eValorOutrosCustos: TEditLuka;
    lb18: TLabel;
    sbCarregarXML: TSpeedButtonLuka;
    sbOutrosCustos: TSpeedButtonLuka;
    sbLancarConhecimentoFrete: TSpeedButtonLuka;
    lb26: TLabel;
    eValorOutrosCustosFinanceiros: TEditLuka;
    lb27: TLabel;
    eValorBonificacao: TEditLuka;
    miDefinirLocalEntradaProdutosNaoDefinidos: TMenuItem;
    miN2: TMenuItem;
    lb28: TLabel;
    ePercIcmsST: TEditLuka;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure eValorIPIProdKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CalcularTotalProduto(Sender: TObject; var Key: Char);
    procedure sgProdutosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure sgProdutosDblClick(Sender: TObject);
    procedure CalcularTotalNota(Sender: TObject);
    procedure cbSerieNotaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ckAtualizarCustoEntradaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbCarregarXMLClick(Sender: TObject);
    procedure sgProdutosClick(Sender: TObject);
    procedure eValorTotalNotaChange(Sender: TObject);
    procedure rgTipoRateioFreteClick(Sender: TObject);
    procedure FrCFOPCapaOnAposPesquisar(Sender: TObject);
    procedure miExcluirProdutoClick(Sender: TObject);
    procedure miDefinirLocalEntradaTodosProdutosClick(Sender: TObject);
    procedure miInformaesPrecoFinalLiquidoClick(Sender: TObject);
    procedure eBaseCalcICMSProdKeyPress(Sender: TObject; var Key: Char);
    procedure ePercentualIPIKeyPress(Sender: TObject; var Key: Char);
    procedure eValorOutrosCustosChange(Sender: TObject);
    procedure FrBuscarLotesExit(Sender: TObject);
    procedure sbOutrosCustosClick(Sender: TObject);
    procedure sbLancarConhecimentoFreteClick(Sender: TObject);
    procedure eValorOutrosCustosFinanceirosChange(Sender: TObject);
    procedure FrBuscarPedidosComprasmiInserirClick(Sender: TObject);
    procedure SpeedButtonLuka1Click(Sender: TObject);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FCarregouXML: Boolean;
    FPreEntradaId: Integer;
    FLotes: TArray<RecEntradasNfItensLotes>;
    FDadosCarregado: RecEntradaNotaFiscal;
    FParametrosEmpresa: RecParametrosEmpresa;
    FTitulosOutrosCustos: TArray<RecTitulosFinanceiros>;
    FDadosConhecimento: TArray<RecConhecimentosFretesItens>;
    vItensPedidos: TArray<RecComprasItens>;
    vDadosCompras: TArray<RecDadosCompras>;
    FPedidosCompra: TArray<RecDadosCompras>;

    procedure MonitoraFramePedidos(Sender: TObject; TipoMudanca: string);

    procedure FrProdutoOnAposPesquisar(Sender: TObject);

    procedure IniciarComponentes;

    procedure PreencherRegistro(
      pNota: RecEntradaNotaFiscal;
      pNotaItens: TArray<RecEntradaNotaFiscalItem>;
      pNotaFinanceiro: TArray<RecTitulosFinanceiros>;
      pCarregandoXML: Boolean
    );

    procedure ValidarItensDivergentes;
    procedure ReprocessarItensPedidos;

    procedure AdicionarProdutoNoGrid(
      pProdutoID: Integer;
      pNome: string;
      pMarca: string;
      pCFOPID: string;
      pQuantidade: Double;
      pUnidade: string;
      pValorUnitario: Double;
      pValorTotalProduto: Double;
      pBaseICMS: Double;
      pIndiceReducaoBaseICMS: Double;
      pIndiceReducaoBaseICMSST: Double;
      pPercentualICMS: Double;
      pValorICMS: Double;
      pBaseICMSST: Double;
      pPercentualICMSST: Double;
      pValorICMSST: Double;
      pValorIPI: Double;
      pPercentualIPI: Double;
      pBasePis: Double;
      pPercentualPis: Double;
      pValorPis: Double;
      pCstPis: string;
      pBaseCofins: Double;
      pPercentualCofins: Double;
      pValorCofins: Double;
      pCstCofins: string;
      pCST: string;
      pNCM: string;
      pCodigoBarras: string;
      pValorDesconto: Double;
      pValorFrete: Double;
      pValorOutrasDesp: Double;
      pLocalId: Integer;
      pNomeLocal: string;
      pIva: Double;
      pPrecoPauta: Double;
      pInformacoesAdicionais: string;
      pPeso: Double;
      pValorICMSFrete: Double;
      pQuantidadeEmbalagem: Double;
      pUnidadeCompra: string;
      pMultiploCompra: Double;
      pExigirDataFabricacaoLote: string;
      pExigirDataVencimentoLote: string;
      pTipoControleEstoque: string;
      pCfopBonificacao: string;
      pMultiploVenda: Double
    ); overload;

    function getComponentesCabecalho: TArray<TControl>;
    function getComponentesItens: TArray<TControl>;

    procedure BuscarConhecimentoFrete;
    procedure TotalizarProdutos;

    procedure RatearFreteProdutos;
    procedure RatearOutrosCustos;
    procedure CalcularImpostosProdutos;
    procedure CalcularValoresCustosProdutos;

    procedure FrEmpresaOnAposPesquisar(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

procedure RelizarEntradaViaPreEntrada(pPreEntradaId: Integer);

implementation

uses
  EntradaCarregarPedidos;

{$R *.dfm}

{ TFormEntradaNotasFiscais }

const
  // Grid de produtos
  coLegenda                              = 0;
  coProdutoId                            = 1;
  coNomeProduto                          = 2;
  coMarca                                = 3;
  coCFOP                                 = 4;
  coQuantidade                           = 5;
  coUnidade                              = 6;
  coQtdeEntradaAltis                     = 7;
  coUnidadeEntradaAltis                  = 8;
  coValorUnitario                        = 9;
  coValorTotal                           = 10;
  coValorDesconto                        = 11;
  coValorOutrasDespesas                  = 12;
  coValorFrete                           = 13;
  coCST                                  = 14;
  coBaseICMSUsuario                      = 15;
  coPercentualICMSUsuario                = 16;
  coValorICMSUsuario                     = 17;
  coBaseICMSSTUsuario                    = 18;
  coPercentualICMSSTUsuario              = 19;
  coValorICMSSTUsuario                   = 20;
  coPercentualIPIUsuario                 = 21;
  coValorIPIUsuario                      = 22;
  coLocalId                              = 23;
  coNomeLocal                            = 24;
  coPrecoFinal                           = 25;
  coPrecoLiquido                         = 26;

  (* Ocultas *)
  coCstPis                               = 27;
  coCstCofins                            = 28;
  coBasePis                              = 29;
  coPercentualPis                        = 30;
  coValorPis                             = 31;
  coBaseCofins                           = 32;
  coPercentualCofins                     = 33;
  coValorCofins                          = 34;
  coNCM                                  = 35;
  coCodigoBarras                         = 36;
  coIndiceReducaoBaseCalculoICMSUsuario  = 37;
  coIndiceReducaoBaseCalcICMSSTUsuario   = 38;
  coIvaUsuario                           = 39;
  coPrecoPautaUsuario                    = 40;
  coInformacoesAdicionais                = 41;
  coPeso                                 = 42;

  (* Forma��o do custo *)
  coValorDifalCusto                      = 43;
  coValorOutrosCustos                    = 44;
  coValorICMSCusto                       = 45;
  coValorICMSFreteCusto                  = 46;
  coValorPisCusto                        = 47;
  coValorCofinsCusto                     = 48;
  coValorPisFreteCusto                   = 49;
  coValorCofinsFreteCusto                = 50;

  coMultiploCompra                       = 51;
  coQuantidadeEmbalagem                  = 52;
  coTipoControleEstoque                  = 53;
  coExigirDataFabricacaoLote             = 54;
  coExigirDataVencimentoLote             = 55;
  coCfopBonificacao                      = 56;
  coPrecoTabelaAtual                     = 57;
  coMultiploVenda                        = 58;
  coDivergenciaPedidoCompra              = 59;

procedure RelizarEntradaViaPreEntrada(pPreEntradaId: Integer);
var
  vForm: TFormEntradaNotasFiscais;
begin
  if pPreEntradaId = 0 then
    Exit;

  if not Sessao.AutorizadoTela('tformentradanotasfiscais') then begin
    Exclamar('Voc� n�o possui permiss�es necess�rias para acessar a tela de entradas!');
    Exit;
  end;

  vForm := TFormEntradaNotasFiscais.Create(Application);
  vForm.Show;
  vForm.FPreEntradaId := pPreEntradaId;
  vForm.sbCarregarXMLClick(nil);
end;

procedure TFormEntradaNotasFiscais.AdicionarProdutoNoGrid(
  pProdutoID: Integer;
  pNome: string;
  pMarca: string;
  pCFOPID: string;
  pQuantidade: Double;
  pUnidade: string;
  pValorUnitario: Double;
  pValorTotalProduto: Double;
  pBaseICMS: Double;
  pIndiceReducaoBaseICMS: Double;
  pIndiceReducaoBaseICMSST: Double;
  pPercentualICMS: Double;
  pValorICMS: Double;
  pBaseICMSST: Double;
  pPercentualICMSST: Double;
  pValorICMSST: Double;
  pValorIPI: Double;
  pPercentualIPI: Double;
  pBasePis: Double;
  pPercentualPis: Double;
  pValorPis: Double;
  pCstPis: string;
  pBaseCofins: Double;
  pPercentualCofins: Double;
  pValorCofins: Double;
  pCstCofins: string;
  pCST: string;
  pNCM: string;
  pCodigoBarras: string;
  pValorDesconto: Double;
  pValorFrete: Double;
  pValorOutrasDesp: Double;
  pLocalId: Integer;
  pNomeLocal: string;
  pIva: Double;
  pPrecoPauta: Double;
  pInformacoesAdicionais: string;
  pPeso: Double;
  pValorICMSFrete: Double;
  pQuantidadeEmbalagem: Double;
  pUnidadeCompra: string;
  pMultiploCompra: Double;
  pExigirDataFabricacaoLote: string;
  pExigirDataVencimentoLote: string;
  pTipoControleEstoque: string;
  pCfopBonificacao: string;
  pMultiploVenda: Double
);
var
  vLinha: Integer;
  vAlterandoProdutoXML: Boolean;
  vTemDivergencia: Boolean;
begin
  vAlterandoProdutoXML := False;
  vLinha := sgProdutos.Localizar([coProdutoId], [NFormat(pProdutoID)]);
  if vLinha = -1 then begin
    if sgProdutos.Cells[coProdutoId, 1] = '' then
      vLinha := 1
    else begin
      vLinha := sgProdutos.RowCount;
      sgProdutos.RowCount := sgProdutos.RowCount + 1;
    end;
  end
  else
    vAlterandoProdutoXML := FCarregouXML;

  vTemDivergencia := FrBuscarPedidosCompras.VincularItemIdEQuantidade(
    pProdutoID,
    vLinha,
    pQuantidade
  );

  sgProdutos.Cells[coLocalId, vLinha]                      := NFormatN(pLocalId);
  sgProdutos.Cells[coNomeLocal, vLinha]                    := pNomeLocal;
  sgProdutos.Cells[coPeso, vLinha]                         := NFormatN(pPeso, 3);
  sgProdutos.Cells[coQtdeEntradaAltis, vLinha]             := NFormatEstoque(pQuantidade * pQuantidadeEmbalagem);
  sgProdutos.Cells[coUnidadeEntradaAltis, vLinha]          := pUnidadeCompra;
  sgProdutos.Cells[coMultiploCompra, vLinha]               := NFormat(pMultiploCompra, 4);
  sgProdutos.Cells[coMultiploVenda, vLinha]                := NFormat(pMultiploVenda, 4);
  sgProdutos.Cells[coQuantidadeEmbalagem, vLinha]          := NFormatEstoque(pQuantidadeEmbalagem);
  sgProdutos.Cells[coTipoControleEstoque, vLinha]          := pTipoControleEstoque;
  sgProdutos.Cells[coExigirDataFabricacaoLote, vLinha]     := pExigirDataFabricacaoLote;
  sgProdutos.Cells[coExigirDataVencimentoLote, vLinha]     := pExigirDataVencimentoLote;
  sgProdutos.Cells[coBaseICMSUsuario, vLinha]              := NFormat(pBaseICMS);
  sgProdutos.Cells[coIndiceReducaoBaseCalculoICMSUsuario, vLinha] := NFormat(pIndiceReducaoBaseICMS, 6);
  sgProdutos.Cells[coPercentualICMSUsuario, vLinha]        := NFormat(pPercentualICMS);
  sgProdutos.Cells[coValorICMSUsuario, vLinha]             := NFormat(pValorICMS);
  sgProdutos.Cells[coCfopBonificacao, vLinha]              := pCfopBonificacao;

  sgProdutos.Cells[coBaseICMSSTUsuario, vLinha]                  := NFormat(pBaseICMSST);
  sgProdutos.Cells[coIndiceReducaoBaseCalcICMSSTUsuario, vLinha] := NFormat(pIndiceReducaoBaseICMSST, 6);
  sgProdutos.Cells[coPercentualICMSSTUsuario, vLinha]            := NFormat(pPercentualICMSST);
  sgProdutos.Cells[coValorICMSSTUsuario, vLinha]                 := NFormat(pValorICMSST);

  sgProdutos.Cells[coDivergenciaPedidoCompra, vLinha]            := IIfStr(vTemDivergencia, 'S', 'N');

  sgProdutos.Cells[coCST, vLinha] := pCST;

  if not vAlterandoProdutoXML then begin
    sgProdutos.Cells[coProdutoId, vLinha]                    := NFormat(pProdutoID);
    sgProdutos.Cells[coNomeProduto, vLinha]                  := pNome;
    sgProdutos.Cells[coMarca, vLinha]                        := pMarca;
    sgProdutos.Cells[coCFOP, vLinha]                         := pCFOPID;
    sgProdutos.Cells[coQuantidade, vLinha]                   := NFormatEstoque(pQuantidade);
    sgProdutos.Cells[coUnidade, vLinha]                      := pUnidade;
    sgProdutos.Cells[coValorUnitario, vLinha]                := NFormat(pValorUnitario);
    sgProdutos.Cells[coValorTotal, vLinha]                   := NFormat(pValorTotalProduto);
    sgProdutos.Cells[coValorIPIUsuario, vLinha]              := NFormat(pValorIPI);
    sgProdutos.Cells[coPercentualIPIUsuario, vLinha]         := NFormat(pPercentualIPI);
    sgProdutos.Cells[coBasePis, vLinha]                      := NFormat(pBasePis);
    sgProdutos.Cells[coPercentualPis, vLinha]                := NFormat(pPercentualPis);
    sgProdutos.Cells[coValorPis, vLinha]                     := NFormat(pValorPis);
    sgProdutos.Cells[coCstPis, vLinha]                       := pCstPis;
    sgProdutos.Cells[coBaseCofins, vLinha]                   := NFormat(pBaseCofins);
    sgProdutos.Cells[coPercentualCofins, vLinha]             := NFormat(pPercentualCofins);
    sgProdutos.Cells[coValorCofins, vLinha]                  := NFormat(pValorCofins);
    sgProdutos.Cells[coLocalId, vLinha]                      := NFormatN(pLocalId);
    sgProdutos.Cells[coNomeLocal, vLinha]                    := pNomeLocal;
    sgProdutos.Cells[coCstCofins, vLinha]                    := pCstCofins;
    sgProdutos.Cells[coNCM, vLinha]                          := pNCM;
    sgProdutos.Cells[coCodigoBarras, vLinha]                 := pCodigoBarras;
    sgProdutos.Cells[coValorDesconto, vLinha]                := NFormat(pValorDesconto);
    sgProdutos.Cells[coValorFrete, vLinha]                   := NFormat(pValorFrete);
    sgProdutos.Cells[coValorOutrasDespesas, vLinha]          := NFormat(pValorOutrasDesp);
    sgProdutos.Cells[coIvaUsuario, vLinha]                   := NFormatN(pIva);
    sgProdutos.Cells[coPrecoPautaUsuario, vLinha]            := NFormatN(pPrecoPauta);
    sgProdutos.Cells[coInformacoesAdicionais, vLinha]        := pInformacoesAdicionais;
    sgProdutos.Cells[coValorICMSFreteCusto, vLinha]          := NFormatN(pValorICMSFrete);
  end;

  TotalizarProdutos;
end;

procedure TFormEntradaNotasFiscais.BuscarConhecimentoFrete;
begin
  FDadosConhecimento := _ConhecimentosFretesItens.BuscarItens(Sessao.getConexaoBanco, 1, [FrFornecedor.GetFornecedor.cadastro_id, eNumeroNotaFiscal.AsInt, cbModeloNota.AsString, cbSerieNota.AsString]);
  if FDadosConhecimento <> nil then begin
    eConhecimentoId.AsInt       := FDadosConhecimento[0].ConhecimentoId;
    eValorConhecimento.AsDouble := FDadosConhecimento[0].ValorCohecimentoFrete;
  end;
end;

procedure TFormEntradaNotasFiscais.BuscarRegistro;
var
  pProdutoId: Integer;
  pQuantidade: Double;
  i: Integer;
  vNota: TArray<RecEntradaNotaFiscal>;
  vTemDivergencia: Boolean;
begin
  vNota := _EntradasNotasFiscais.BuscarEntradaNotaFiscais(Sessao.getConexaoBanco, 0, [eID.AsInt]);
  if vNota = nil then begin
    NenhumRegistro;
    Exit;
  end;

  inherited;

  PreencherRegistro(
    vNota[0],
    _EntradasNotasFiscaisItens.BuscarEntradaNotaFiscalItens(Sessao.getConexaoBanco, 0, [eID.AsInt]),
    _EntradasNotasFiscFinanceiro.BuscarEntradaNotaFiscalFinanceiro(Sessao.getConexaoBanco, 1, [eID.AsInt, 'N']),
    False
  );

  BuscarConhecimentoFrete;
  TotalizarProdutos;
end;

procedure TFormEntradaNotasFiscais.CalcularImpostosProdutos;
var
  i: Integer;
  vQtdeEntrada: Double;
  vQtdeEntradaAltis: Double;
  vImpostos: TCalculoImpostos;
begin
  vImpostos := TCalculoImpostos.Create;

  for i := 1 to sgProdutos.RowCount -1 do begin
    if sgProdutos.Cells[coProdutoId, i] = '' then
      Continue;

    vQtdeEntrada      := SFormatDouble(sgProdutos.Cells[coQuantidade, i]);
    vQtdeEntradaAltis := SFormatDouble(sgProdutos.Cells[coQtdeEntradaAltis, i]);

    vImpostos.setDados(
      FrEmpresa.getEmpresa.EstadoId,
      FrFornecedor.GetFornecedor().cadastro.estado_id,
      SFormatDouble(sgProdutos.Cells[coValorUnitario, i]),
      vQtdeEntrada,
      vQtdeEntradaAltis,
      SFormatDouble(sgProdutos.Cells[coValorDesconto, i]),
      SFormatDouble(sgProdutos.Cells[coValorOutrasDespesas, i]),
      SFormatDouble(sgProdutos.Cells[coValorICMSUsuario, i]),
      SFormatDouble(sgProdutos.Cells[coValorICMSSTUsuario, i]),
      SFormatDouble(sgProdutos.Cells[coValorFrete, i]),
      SFormatDouble(sgProdutos.Cells[coPercentualIPIUsuario, i]),
      FrEmpresa.getEmpresa().EmpresaId
    );

    sgProdutos.Cells[coValorDifalCusto, i]   := NFormatN( vImpostos.ValorDifal, 6 );
    sgProdutos.Cells[coValorICMSCusto, i]    := NFormatN( vImpostos.ValorIcms, 6 );
    sgProdutos.Cells[coValorPisCusto, i]     := NFormatN( vImpostos.ValorPis, 6 );
    sgProdutos.Cells[coValorCofinsCusto, i]  := NFormatN( vImpostos.ValorCofins, 6 );

//  coValorPisFreteCusto                   = 45;
//  coValorCofinsFreteCusto                = 46;
  end;

  vImpostos.Free;
end;

procedure TFormEntradaNotasFiscais.CalcularTotalNota(Sender: TObject);
begin
  eValorTotalNota.AsDouble :=
    eValorTotalProdutos.AsCurr +
    eValorICMSST.AsCurr +
    eOutrasDespesas.AsCurr +
    eValorIPI.AsCurr +
    eValorFrete.AsCurr -
    eValorDesconto.AsCurr;
end;

procedure TFormEntradaNotasFiscais.CalcularTotalProduto(Sender: TObject; var Key: Char);
begin
  eTotalProduto.AsDouble := eQuantidade.AsDouble * eValorUnitario.AsDouble;
end;

procedure TFormEntradaNotasFiscais.CalcularValoresCustosProdutos;
var
  i: Integer;
  vQuantidade: Double;
begin
  if sgProdutos.Cells[coProdutoId, 1] = '' then
    Exit;

  for i := 1 to sgProdutos.RowCount -1 do begin
    vQuantidade := SFormatDouble(sgProdutos.Cells[coQtdeEntradaAltis, i]);

    sgProdutos.Cells[coPrecoFinal, i] :=
      NFormat(
        Sessao.getCalculosSistema.CalcEntradas.CalcularPrecoFinal(
          SFormatDouble(sgProdutos.Cells[coValorTotal, i]),
          SFormatDouble(sgProdutos.Cells[coValorOutrasDespesas, i]),
          SFormatDouble(sgProdutos.Cells[coValorICMSSTUsuario, i]),
          SFormatDouble(sgProdutos.Cells[coValorIPIUsuario, i]),
          SFormatDouble(sgProdutos.Cells[coValorFrete, i]),
          SFormatDouble(sgProdutos.Cells[coValorDesconto, i]),
          SFormatDouble(sgProdutos.Cells[coValorDifalCusto, i]),
          SFormatDouble(sgProdutos.Cells[coValorOutrosCustos, i]),
          vQuantidade
        )
      );

    sgProdutos.Cells[coPrecoLiquido, i] :=
      NFormat(
        Sessao.getCalculosSistema.CalcEntradas.CalcularPrecoLiquido(
          SFormatDouble(sgProdutos.Cells[coPrecoFinal, i]),
          SFormatDouble(sgProdutos.Cells[coValorICMSCusto, i]),
          SFormatDouble(sgProdutos.Cells[coValorPisCusto, i]),
          SFormatDouble(sgProdutos.Cells[coValorCofinsCusto, i]),
          SFormatDouble(sgProdutos.Cells[coValorICMSFreteCusto, i]),
          0,
          0,
          SFormatDouble(sgProdutos.Cells[coValorICMSSTUsuario, i]) > 0,
          vQuantidade,
          FParametrosEmpresa
        )
      );
  end;
end;

procedure TFormEntradaNotasFiscais.cbSerieNotaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if FrEmpresa.EstaVazio then begin
    _Biblioteca.Exclamar('A empresa da entrada deve ser informada, verifique!');
    SetarFoco(FrEmpresa);
    Exit;
  end;

  if FrFornecedor.EstaVazio then begin
    _Biblioteca.Exclamar('O fornecedor da entrada deve ser informado, verifique!');
    SetarFoco(FrFornecedor);
    Exit;
  end;

  if eNumeroNotaFiscal.AsInt = 0 then begin
    _Biblioteca.Exclamar('O numero da nota fiscal deve ser informado, verifique!');
    SetarFoco(eNumeroNotaFiscal);
    Exit;
  end;

  if cbModeloNota.Text = '' then begin
    _Biblioteca.Exclamar('O modelo da nota fiscal deve ser informado, verifique!');
    SetarFoco(cbModeloNota);
    Exit;
  end;

  if cbSerieNota.Text = '' then begin
    _Biblioteca.Exclamar('A s�rie da nota fiscal deve ser informado, verifique!');
    SetarFoco(cbSerieNota);
    Exit;
  end;

  if _EntradasNotasFiscais.ExisteEntradaNFe(Sessao.getConexaoBanco, FrFornecedor.GetFornecedor().cadastro_id, eNumeroNotaFiscal.AsInt, cbModeloNota.Text, cbSerieNota.Text) then begin
    _Biblioteca.Exclamar('J� existe uma entrada de nota fiscal com os dados informados!');
    SetarFoco(FrFornecedor);
    Exit;
  end;

  _Biblioteca.Habilitar(getComponentesCabecalho, False, False);
  _Biblioteca.Habilitar(getComponentesItens, True, False);
  SetarFoco(eChaveAcessoNFe);

  IniciarComponentes;
  BuscarConhecimentoFrete;
end;

procedure TFormEntradaNotasFiscais.ckAtualizarCustoEntradaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  SetarFoco(FrProduto, Key);
end;

procedure TFormEntradaNotasFiscais.eBaseCalcICMSProdKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  eValorICMSProd.AsDouble := Arredondar(eBaseCalcICMSProd.AsDouble * ePercentualICMS.AsDouble * 0.01, 2);
end;

procedure TFormEntradaNotasFiscais.ePercentualIPIKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  eValorIPIProd.AsDouble := Arredondar(eTotalProduto.AsDouble * ePercentualIPI.AsDouble * 0.01, 2);
end;

procedure TFormEntradaNotasFiscais.eValorIPIProdKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if FrProduto.EstaVazio then begin
    Exclamar('Nenhum produto foi informado!');
    SetarFoco(FrProduto);
    Exit;
  end;

  if eQuantidade.AsCurr = 0 then begin
    Exclamar('A quantidade n�o foi informada corretamente, verifique');
    SetarFoco(eQuantidade);
    Exit;
  end;

  if eValorUnitario.AsCurr = 0 then begin
    Exclamar('O valor unit�rio n�o foi informado corretamente, verifique');
    SetarFoco(eValorUnitario);
    Exit;
  end;

  if FrCFOPProduto.EstaVazio then begin
    Exclamar('O CFOP do produto n�o foi informado corretamente, verifique!');
    SetarFoco(FrCFOPProduto);
    Exit;
  end;

  AdicionarProdutoNoGrid(
    FrProduto.getProduto().produto_id,
    FrProduto.getProduto().nome,
    FrProduto.getProduto().nome_marca,
    FrCFOPProduto.GetCFOP().CfopPesquisaId,
    eQuantidade.AsDouble,
    FrProduto.getProduto().unidade_venda,
    eValorUnitario.AsDouble,
    eTotalProduto.AsDouble,
    eBaseCalcICMSProd.AsDouble,
    eIndiceReducaoBaseCalculoICMS.AsDouble,
    eIndiceReducaoICMSST.AsDouble,
    ePercentualICMS.AsDouble,
    eValorICMSProd.AsDouble,
    eBaseCalcICMSSTProd.AsDouble,
    ePercIcmsST.AsDouble,
    eValorICMSStProd.AsDouble,
    eValorIPIProd.AsDouble,
    ePercentualIPI.AsDouble,
    0,
    0,
    0,
    '50',
    0,
    0,
    0,
    '50',
    cbCst.Text,
    '',
    '',
    eValorDescontoProd.AsDouble,
    0,
    eValorOutDespesasProd.AsDouble,
    FrLocalProduto.GetLocais().local_id,
    FrLocalProduto.GetLocais().nome,
    0,
    0,
    '',
    FrProduto.getProduto().peso,
    0,
    FrMultiplosCompra.QuantidadeEmbalagem,
    FrMultiplosCompra.UnidadeId,
    FrMultiplosCompra.Multiplo,
    FrProduto.getProduto().ExigirDataFabricacaoLote,
    FrProduto.getProduto().ExigirDataVencimentoLote,
    FrProduto.getProduto().TipoControleEstoque,
    FrCFOPProduto.GetCFOP().EntradaMercadoriasBonific,
    FrProduto.getProduto().multiplo_venda
  );

  LimparCampos([
    FrProduto,
    FrLocalProduto,
    eQuantidade,
    eValorUnitario,
    eValorDescontoProd,
    eValorOutDespesasProd,
    FrCFOPProduto,
    eTotalProduto,
    cbCst,
    eBaseCalcICMSProd,
    eIndiceReducaoBaseCalculoICMS,
    ePercentualICMS,
    eValorICMSProd,
    eBaseCalcICMSSTProd,
    eIndiceReducaoICMSST,
    eValorICMSStProd,
    ePercentualIPI,
    eValorIPIProd,
    eValorFreteItem,
    ePercIcmsST
  ]);

  SetarFoco(FrProduto);
  sgProdutos.Repaint;
  sgProdutosClick(Sender);
end;

procedure TFormEntradaNotasFiscais.eValorOutrosCustosChange(Sender: TObject);
begin
  inherited;
  RatearOutrosCustos;
  CalcularValoresCustosProdutos;
end;

procedure TFormEntradaNotasFiscais.eValorOutrosCustosFinanceirosChange(Sender: TObject);
begin
  inherited;
  eValorTotalNotaChange(Sender);
  eValorOutrosCustosChange(Sender);
end;

procedure TFormEntradaNotasFiscais.eValorTotalNotaChange(Sender: TObject);
begin
  inherited;
  FrDadosCobranca.ValorPagar :=
    eValorTotalNota.AsDouble +
    eValorOutrosCustosFinanceiros.AsCurr -
    eValorBonificacao.AsCurr;
end;

procedure TFormEntradaNotasFiscais.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _EntradasNotasFiscais.ExcluirEntradaNotaFiscal(Sessao.getConexaoBanco, eID.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  RotinaSucesso;
  inherited;
end;

procedure TFormEntradaNotasFiscais.FormCreate(Sender: TObject);
begin
  // Tem que ser antes da heran�a por causa do met�do Modo
  FPreEntradaId := -1;

  inherited;
  FrProduto.OnAposPesquisar   := FrProdutoOnAposPesquisar;

  FrDadosCobranca.setEditDataBase(eDataPrimeiraParcela);
  FrDadosCobranca.setTelaEntrada;
  FrDadosCobranca.Natureza := 'P';
  FrBuscarLotes.Tela       := ttEntradas;

  FrCFOPCapa.OnAposPesquisar  := FrCFOPCapaOnAposPesquisar;
  FrEmpresa.OnAposPesquisar   := FrEmpresaOnAposPesquisar;
  Sessao.SetComboBoxSeriesNotas(cbSerieNota);

  FrBuscarPedidosCompras.OnChange := MonitoraFramePedidos;

  FPedidosCompra := nil;
end;

procedure TFormEntradaNotasFiscais.FrBuscarLotesExit(Sender: TObject);
begin
  inherited;
  FrBuscarLotes.ProdutoId := SFormatInt( sgProdutos.Cells[coProdutoId, sgProdutos.Row] );
  FLotes                  := FrBuscarLotes.Lotes;
end;

procedure TFormEntradaNotasFiscais.FrBuscarPedidosComprasmiInserirClick(
  Sender: TObject);
begin
  inherited;
  FrBuscarPedidosCompras.miInserirClick(Sender);

end;

procedure TFormEntradaNotasFiscais.FrCFOPCapaOnAposPesquisar(Sender: TObject);
begin
  if FrPlanoFinanceiro.EstaVazio then
    FrPlanoFinanceiro.InserirDadoPorChave( FrCFOPCapa.GetCFOP.PlanoFinanceiroEntradaId, False );
end;

procedure TFormEntradaNotasFiscais.FrEmpresaOnAposPesquisar(Sender: TObject);
begin
  FParametrosEmpresa := _ParametrosEmpresa.BuscarParametrosEmpresas(Sessao.getConexaoBanco, 0, [FrEmpresa.getEmpresa.EmpresaId])[0];
end;

procedure TFormEntradaNotasFiscais.FrProdutoOnAposPesquisar(Sender: TObject);
var
  vTribFederal: RecTributacoesFederal;
  vTribEstadual: RecTributacaoEstadual;
begin
  vTribFederal := _ProdutosGruposTribEmpresas.getTributacoesFederal(Sessao.getConexaoBanco, FrEmpresa.getEmpresa.EmpresaId, FrProduto.getProduto().produto_id);
  if vTribFederal.ProdutoId = -1 then begin
    Exclamar('Os par�metros fiscais de compra n�o foram informados no cadastro do produto!');
    Exit;
  end;

  vTribEstadual := _ProdutosGruposTribEmpresas.getTributacaoEstadual(Sessao.getConexaoBanco, FrEmpresa.getEmpresa.EmpresaId, FrEmpresa.getEmpresa.EstadoId, FrProduto.getProduto.produto_id );

  if FrFornecedor.GetFornecedor.tipo_fornecedor = 'R' then
    cbCST.Text := NFormat(vTribFederal.OrigemProdutoCompra) + vTribEstadual.CstRevendaCompra
  else if FrFornecedor.GetFornecedor.tipo_fornecedor = 'D' then
    cbCST.Text := NFormat(vTribFederal.OrigemProdutoCompra) + vTribEstadual.CstDistribuidoraCompra
  else
    cbCST.Text := NFormat(vTribFederal.OrigemProdutoCompra) + vTribEstadual.CstIndustriaCompra;

  ePercentualICMS.AsDouble               := vTribEstadual.PercIcmsVenda;
  eIndiceReducaoBaseCalculoICMS.AsDouble := vTribEstadual.IndiceReducaoBaseIcmsVenda;
  eIndiceReducaoICMSST.AsDouble          := vTribEstadual.IndiceReducaoBaseIcmsVenda;
  FrMultiplosCompra.SetProdutoId( FrProduto.getProduto.produto_id );

end;

function TFormEntradaNotasFiscais.getComponentesCabecalho: TArray<TControl>;
begin
  Result :=
    [FrEmpresa,
    FrFornecedor,
    eNumeroNotaFiscal,
    cbModeloNota,
    cbSerieNota];
end;

function TFormEntradaNotasFiscais.getComponentesItens: TArray<TControl>;
begin
  Result :=
    [pcDados,
    eDataContabil,
    eDataPrimeiraParcela,
    eChaveAcessoNFe,
    eDataEmissaoNota,
    FrCFOPCapa,
    eBaseCalculoICMS,
    eValorICMS,
    eBaseCalculoICMSST,
    eValorICMSST,
    eValorTotalProdutos,
    eValorDesconto,
    eOutrasDespesas,
    eValorIPI,
    eValorFrete,
    eValorTotalNota,
    eValorBonificacao,
    cbModalidadeFrete,
    rgTipoRateioFrete,
    cbStatusEntrada,
    ckAtualizarCustoCompra,
    ckAtualizarCustoEntrada,
    eValorOutrosCustosFinanceiros,
    FrPlanoFinanceiro,
    FrCentroCusto,
    FrDadosCobranca,
    eConhecimentoId,
    eValorConhecimento,
    sbInformacoesConhecimento,
    eValorOutrosCustos,
    sbOutrosCustos,
    // Aba Produtos
    FrProduto,
    FrLocalProduto,
    eQuantidade,
    FrMultiplosCompra,
    eValorUnitario,
    eValorDescontoProd,
    eValorOutDespesasProd,
    eTotalProduto,
    FrCFOPProduto,
    cbCST,
    eBaseCalcICMSProd,
    eIndiceReducaoBaseCalculoICMS,
    ePercentualICMS,
    ePercIcmsST,
    eValorICMSProd,
    eBaseCalcICMSSTProd,
    eValorICMSStProd,
    ePercentualIPI,
    eValorIPIProd,
    sgProdutos,
    stPesoTotal,
    stQtdeTotal,
    stValorTotal,
    FrBuscarLotes,
    FrBuscarPedidosCompras];
end;

procedure TFormEntradaNotasFiscais.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetorno: RecRetornoBD;
  vNotaItens: TArray<RecEntradaNotaFiscalItem>;
  ItensPedidoCompraAux: TArray<RecEntradasNFItensCompras>;

  vRetTela: TRetornoTelaFinalizar;

  vBasePis: Double;
  vValorPis: Double;
  vBaseCofins: Double;
  vValorCofins: Double;
  vPesoTotal: Double;

  vCentroCustoId: Integer;
  vChaveAcessoNFe: string;
begin
  inherited;

  vBasePis := 0;
  vValorPis := 0;
  vPesoTotal := 0;
  vBaseCofins := 0;
  vValorCofins := 0;

  vNotaItens := nil;
  if sgProdutos.Cells[coProdutoId, 1] <> '' then begin
    SetLength(vNotaItens, sgProdutos.RowCount - 1);
    for i := sgProdutos.FixedRows to sgProdutos.RowCount - 1 do begin
      vNotaItens[i - 1].produto_id             := SFormatInt(sgProdutos.Cells[coProdutoId, i]);
      vNotaItens[i - 1].nome_produto           := sgProdutos.Cells[coNomeProduto, i];
      vNotaItens[i - 1].nome_marca             := sgProdutos.Cells[coMarca, i];
      vNotaItens[i - 1].item_id                := i;
      vNotaItens[i - 1].CfopId                 := sgProdutos.Cells[coCFOP, i];
      vNotaItens[i - 1].cst                    := sgProdutos.Cells[coCST, i];
      vNotaItens[i - 1].codigo_ncm             := sgProdutos.Cells[coNCM, i];
      vNotaItens[i - 1].valor_total            := SFormatDouble(sgProdutos.Cells[coValorTotal, i]);
      vNotaItens[i - 1].preco_unitario         := SFormatDouble(sgProdutos.Cells[coValorUnitario, i]);
      vNotaItens[i - 1].quantidade             := SFormatDouble(sgProdutos.Cells[coQuantidade, i]);
      vNotaItens[i - 1].valor_total_desconto   := SFormatDouble(sgProdutos.Cells[coValorDesconto, i]);
      vNotaItens[i - 1].valor_total_outras_despesas := SFormatDouble(sgProdutos.Cells[coValorOutrasDespesas, i]);
      vNotaItens[i - 1].codigo_barras          := sgProdutos.Cells[coCodigoBarras, i];
      vNotaItens[i - 1].origem_produto         := 0;

      vNotaItens[i - 1].indice_reducao_base_icms := SFormatDouble(sgProdutos.Cells[coIndiceReducaoBaseCalculoICMSUsuario, i]);
      vNotaItens[i - 1].base_calculo_icms      := SFormatDouble(sgProdutos.Cells[coBaseICMSUsuario, i]);
      vNotaItens[i - 1].percentual_icms        := SFormatDouble(sgProdutos.Cells[coPercentualICMSUsuario, i]);
      vNotaItens[i - 1].valor_icms             := SFormatDouble(sgProdutos.Cells[coValorICMSUsuario, i]);

      vNotaItens[i - 1].indice_reducao_base_icms_st := SFormatDouble(sgProdutos.Cells[coIndiceReducaoBaseCalcICMSSTUsuario, i]);
      vNotaItens[i - 1].base_calculo_icms_st   := SFormatDouble(sgProdutos.Cells[coBaseICMSSTUsuario, i]);
      vNotaItens[i - 1].percentual_icms_st     := SFormatDouble(sgProdutos.Cells[coPercentualICMSSTUsuario, i]);
      vNotaItens[i - 1].valor_icms_st          := SFormatDouble(sgProdutos.Cells[coValorICMSSTUsuario, i]);

      vNotaItens[i - 1].cst_pis                := sgProdutos.Cells[coCstPis, i];
      vNotaItens[i - 1].base_calculo_pis       := SFormatDouble(sgProdutos.Cells[coBasePis, i]);
      vNotaItens[i - 1].percentual_pis         := SFormatDouble(sgProdutos.Cells[coPercentualPis, i]);
      vNotaItens[i - 1].valor_pis              := SFormatDouble(sgProdutos.Cells[coValorPis, i]);

      vBasePis := vBasePis + vNotaItens[i - 1].base_calculo_pis;
      vValorPis := vValorPis + vNotaItens[i - 1].valor_pis;

      vNotaItens[i - 1].cst_cofins             := sgProdutos.Cells[coCstCofins, i];
      vNotaItens[i - 1].base_calculo_cofins    := SFormatDouble(sgProdutos.Cells[coBaseCofins, i]);
      vNotaItens[i - 1].percentual_cofins      := SFormatDouble(sgProdutos.Cells[coPercentualCofins, i]);
      vNotaItens[i - 1].valor_cofins           := SFormatDouble(sgProdutos.Cells[coValorCofins, i]);

      vNotaItens[i - 1].Peso                   := SFormatDouble(sgProdutos.Cells[coPeso, i]);

      vBaseCofins := vBaseCofins + vNotaItens[i - 1].base_calculo_cofins;
      vValorCofins := vValorCofins + vNotaItens[i - 1].valor_cofins;

      vPesoTotal := vPesoTotal + vNotaItens[i - 1].Peso;

      vNotaItens[i - 1].valor_ipi              := SFormatDouble(sgProdutos.Cells[coValorIPIUsuario, i]);
      vNotaItens[i - 1].percentual_ipi         := SFormatDouble(sgProdutos.Cells[coPercentualIPIUsuario, i]);
      vNotaItens[i - 1].iva                    := SFormatDouble(sgProdutos.Cells[coIvaUsuario, i]);

      vNotaItens[i - 1].preco_pauta            := SFormatDouble(sgProdutos.Cells[coPrecoPautaUsuario, i]);
      vNotaItens[i - 1].informacoes_adicionais := sgProdutos.Cells[coInformacoesAdicionais, i];

      vNotaItens[i - 1].LocalEntradaId        := SFormatInt(sgProdutos.Cells[coLocalId, i]);

      vNotaItens[i - 1].unidade                := sgProdutos.Cells[coUnidade, i];
      vNotaItens[i - 1].UnidadeEntradaId       := sgProdutos.Cells[coUnidade, i];
      vNotaItens[i - 1].QuantidadeEntradaAltis := SFormatDouble(sgProdutos.Cells[coQtdeEntradaAltis, i]);
      vNotaItens[i - 1].UnidadeCompraId        := sgProdutos.Cells[coUnidadeEntradaAltis, i];
      vNotaItens[i - 1].MultiploCompra         := SFormatDouble(sgProdutos.Cells[coMultiploCompra, i]);
      vNotaItens[i - 1].MultiploVenda          := SFormatDouble(sgProdutos.Cells[coMultiploVenda, i]);
      vNotaItens[i - 1].QuantidadeEmbalagem    := SFormatDouble(sgProdutos.Cells[coQuantidadeEmbalagem, i]);

      vNotaItens[i - 1].ValorFreteCusto       := _Biblioteca.Arredondar(SFormatDouble(sgProdutos.Cells[coValorFrete, i]) /  vNotaItens[i - 1].QuantidadeEntradaAltis, 6);
      vNotaItens[i - 1].ValorIPICusto         := _Biblioteca.Arredondar(vNotaItens[i - 1].valor_ipi / vNotaItens[i - 1].QuantidadeEntradaAltis, 6);
      vNotaItens[i - 1].ValorICMSStCusto      := _Biblioteca.Arredondar(vNotaItens[i - 1].valor_icms_st / vNotaItens[i - 1].QuantidadeEntradaAltis, 6);
      vNotaItens[i - 1].ValorOutrosCusto      := _Biblioteca.Arredondar(SFormatDouble(sgProdutos.Cells[coValorOutrosCustos, i]) / vNotaItens[i - 1].QuantidadeEntradaAltis, 6);
      vNotaItens[i - 1].ValorDifalCusto       := SFormatDouble(sgProdutos.Cells[coValorDifalCusto, i]);
      vNotaItens[i - 1].PrecoFinal            := SFormatDouble(sgProdutos.Cells[coPrecoFinal, i]);

      vNotaItens[i - 1].ValorICMSCusto        := SFormatDouble(sgProdutos.Cells[coValorICMSCusto, i]);
      vNotaItens[i - 1].ValorICMSFreteCusto   := _Biblioteca.Arredondar(SFormatDouble(sgProdutos.Cells[coValorICMSFreteCusto, i]) / vNotaItens[i - 1].QuantidadeEntradaAltis, 6);
      vNotaItens[i - 1].ValorPisCusto         := SFormatDouble(sgProdutos.Cells[coValorPisCusto, i]);
      vNotaItens[i - 1].ValorCofinsCusto      := SFormatDouble(sgProdutos.Cells[coValorCofinsCusto, i]);
  //    vNotaItens[i - 1].ValorPisFreteCusto    := _Biblioteca.Arredondar(SFormatDouble(sgProdutos.Cells[coValorCofinsCusto, i]) / vNotaItens[i - 1].quantidade, 2);
  //    vNotaItens[i - 1].ValorCofinsFreteCusto := _Biblioteca.Arredondar(SFormatDouble(sgProdutos.Cells[coValorCofinsCusto, i]) / vNotaItens[i - 1].quantidade, 2);
      vNotaItens[i - 1].PrecoLiquido          := SFormatDouble(sgProdutos.Cells[coPrecoLiquido, i]);
    end;
  end;

  vRetTela := ConferenciaCustosTributacoesEntradas.Conferir(FrEmpresa.getEmpresa().EmpresaId, vNotaItens);
  if vRetTela.BuscaCancelada then
    Abort;

  vCentroCustoId := 0;
  if not FrCentroCusto.EstaVazio then
    vCentroCustoId := FrCentroCusto.GetCentro().CentroCustoId;

  vChaveAcessoNFe := '';
  if _Biblioteca.RetornaNumeros(eChaveAcessoNFe.Text) <> '' then
    vChaveAcessoNFe := eChaveAcessoNFe.Text;

  ItensPedidoCompraAux := nil;
  for i := Low(FrBuscarPedidosCompras.ItensPedidoCompra) to High(FrBuscarPedidosCompras.ItensPedidoCompra) do begin
    if FrBuscarPedidosCompras.ItensPedidoCompra[i].Quantidade = 0 then
      Continue;

    SetLength(ItensPedidoCompraAux, Length(ItensPedidoCompraAux) + 1);
    ItensPedidoCompraAux[High(ItensPedidoCompraAux)] := FrBuscarPedidosCompras.ItensPedidoCompra[i];
  end;

  vRetorno :=
    _EntradasNotasFiscais.AtualizarEntradaNotaFiscal(
      Sessao.getConexaoBanco,
      eID.AsInt,
      FrEmpresa.getEmpresa.EmpresaId,
      FrFornecedor.GetFornecedor().cadastro_id,
      cbStatusEntrada.AsString,
      FrCFOPCapa.GetCFOP().CfopPesquisaId,
      FrPlanoFinanceiro.getDados().PlanoFinanceiroId,
      vCentroCustoId,
      eConhecimentoId.AsInt,
      eValorConhecimento.AsDouble,
      eBaseCalculoICMS.AsCurr,
      eNumeroNotaFiscal.AsInt,
      vPesoTotal,
      vPesoTotal,
      vBaseCofins,
      vValorCofins,
      eValorIPI.AsCurr,
      vBasePis,
      vValorPis,
      eBaseCalculoICMSST.AsCurr,
      eValorICMSST.AsCurr,
      eValorICMS.AsCurr,
      eValorTotalNota.AsCurr,
      eValorTotalProdutos.AsCurr,
      eValorDesconto.AsCurr,
      eOutrasDespesas.AsCurr,
      eValorOutrosCustosFinanceiros.AsCurr,
      eValorFrete.AsCurr,
      rgTipoRateioFrete.GetValor,
      cbModalidadeFrete.AsInt,
      cbSerieNota.Text,
      cbModeloNota.Text,
      eDataEmissaoNota.AsData,
      eDataContabil.AsData,
      eDataPrimeiraParcela.AsData,
      vChaveAcessoNFe,
      ToChar(ckAtualizarCustoCompra),
      ToChar(ckAtualizarCustoEntrada),
      FDadosCarregado.CaminhoXml,
      FDadosCarregado.XmlTexto,
      vNotaItens,
      FrDadosCobranca.Titulos,
      FTitulosOutrosCustos,
      ItensPedidoCompraAux,
      FLotes
    );

  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetorno.AsInt);
end;

procedure TFormEntradaNotasFiscais.Modo(pEditando: Boolean);
begin
  inherited;
  // Se a entrada estiver sendo cancelada
  if (pEditando = False) and (FPreEntradaId > -1) then begin
    FEditandoHeranca := False;
    Close;
    Exit;
  end;

  FCarregouXML := False;

  FLotes := nil;
  FDadosConhecimento := nil;
  FTitulosOutrosCustos := nil;
  FDadosCarregado.entrada_id := -1;

  _Biblioteca.Habilitar(getComponentesCabecalho, pEditando);
  _Biblioteca.Habilitar(getComponentesItens, False);
  _Biblioteca.Habilitar([sbCarregarXML, sbLancarConhecimentoFrete], not pEditando);

  if pEditando then
    SetarFoco(FrEmpresa);
end;

procedure TFormEntradaNotasFiscais.MonitoraFramePedidos(
  Sender: TObject;
  TipoMudanca: string
);
begin
  if TipoMudanca = FrameBuscarPedidosCompras.TAcaoPedido.AlterouQuantidade then
    ValidarItensDivergentes
  else if TipoMudanca = FrameBuscarPedidosCompras.TAcaoPedido.InseriuPedido then
    // TODO: implementar
  else if TipoMudanca = FrameBuscarPedidosCompras.TAcaoPedido.RemoveuPedido then
    ValidarItensDivergentes;

end;

procedure TFormEntradaNotasFiscais.PesquisarRegistro;
var
  vRetTela: TRetornoTelaFinalizar<RecEntradaNotaFiscal>;
begin
  vRetTela := Pesquisa.EntradaNotasFiscais.Pesquisar(ttEntrada);
  if vRetTela.BuscaCancelada then
    Exit;

  inherited;
  PreencherRegistro(
    vRetTela.Dados,
    _EntradasNotasFiscaisItens.BuscarEntradaNotaFiscalItens(Sessao.getConexaoBanco, 0, [vRetTela.Dados.entrada_id]),
    _EntradasNotasFiscFinanceiro.BuscarEntradaNotaFiscalFinanceiro(Sessao.getConexaoBanco, 1, [vRetTela.Dados.entrada_id, 'N']),
    False
  );

  BuscarConhecimentoFrete;
  TotalizarProdutos;
end;

procedure TFormEntradaNotasFiscais.PreencherRegistro(
  pNota: RecEntradaNotaFiscal;
  pNotaItens: TArray<RecEntradaNotaFiscalItem>;
  pNotaFinanceiro: TArray<RecTitulosFinanceiros>;
  pCarregandoXML: Boolean
);
type
  RecCFOPsTotais = record
    CfopId: string;
    Total: Currency;
  end;

var
  i: Integer;
  j: Integer;
  vAchou: Boolean;
  vPosicCfop: Integer;
  vCfopid: string;
  vTotal: Currency;
  vCfops: TArray<RecCFOPsTotais>;
  vLocalProd: TArray<RecProdutosOrdensLocEntregas>;
  pProdutoId: Integer;
  pQuantidade: Double;
  vTemDivergencia: Boolean;
begin
  if (pNota.Status <> '') and (pNota.Status <> 'ACM') then begin
    _Biblioteca.Exclamar('N�o � permitido editar uma entrada de nota fiscal com o estoque j� consolidado!');
    Modo(False);
    Abort;
  end;

  eID.AsInt := pNota.entrada_id;
  FrEmpresa.InserirDadoPorChave(pNota.empresa_id);

  FrFornecedor.InserirDadoPorChave(pNota.fornecedor_id, False);
  if FrFornecedor.EstaVazio then begin
    _Biblioteca.Informar(
      'O fornecedor desta n�o est� parametrizado como "Revenda" ou "Uso e consumo", ' +
      'por favor v� at� a tela de fornecedores e realize o ajuste do cadastro.'
    );
    Modo(False);
    Abort;
  end;

  eNumeroNotaFiscal.AsInt := pNota.numero_nota;
  cbModeloNota.SetIndicePorValor(pNota.modelo_nota);
  cbSerieNota.AsString      := pNota.serie_nota;
  cbStatusEntrada.AsString  := pNota.Status;
  eChaveAcessoNFe.Text      := pNota.chave_nfe;
  eDataContabil.AsData      := pNota.DataEntrada;
  eDataEmissaoNota.AsData   := pNota.data_hora_emissao;
  eDataPrimeiraParcela.AsData := pNota.DataPrimeiraParcela;
  FrCFOPCapa.InserirDadoPorChave(pNota.CfopId, False);
  eBaseCalculoICMS.AsDouble    := pNota.base_calculo_icms;
  eValorICMS.AsDouble          := pNota.valor_icms;
  eBaseCalculoICMSST.AsDouble  := pNota.base_calculo_icms_st;
  eValorICMSST.AsDouble        := pNota.valor_icms_st;
  eValorTotalProdutos.AsDouble := pNota.valor_total_produtos;
  eValorDesconto.AsDouble      := pNota.valor_desconto;
  eOutrasDespesas.AsDouble     := pNota.valor_outras_despesas;
  eValorIPI.AsDouble           := pNota.valor_ipi;
  eValorFrete.AsDouble         := pNota.valor_frete;
  eValorTotalNota.AsDouble     := pNota.valor_total;
  cbModalidadeFrete.AsInt      := pNota.ModalidadeFrete;
  eValorOutrosCustosFinanceiros.AsCurr := pNota.ValorOutrosCustosFinanc;
  FrPlanoFinanceiro.InserirDadoPorChave( pNOta.PlanoFinanceiroId, False );
  FrCentroCusto.InserirDadoPorChave( pNOta.CentroCustoId );
  eConhecimentoId.AsInt := pNota.ConhecimentoFreteId;

  eValorConhecimento.AsDouble := pNota.ValorConhecimentoFrete;
  rgTipoRateioFrete.SetIndicePorValor(pNota.TipoRateioFrete);

  ckAtualizarCustoCompra.Checked := (pNota.atualizar_custo_compra = 'S');
  ckAtualizarCustoCompra.Enabled := (not ckAtualizarCustoCompra.Checked);

  ckAtualizarCustoEntrada.Checked := (pNota.atualizar_custo_entrada = 'S');
  ckAtualizarCustoEntrada.Enabled := (not ckAtualizarCustoEntrada.Checked);

  vCfops := nil;
  for i := Low(pNotaItens) to High(pNotaItens) do begin
    if pCarregandoXML then begin
      vLocalProd := _ProdutosOrdensLocEntregas.BuscarOrdensLocais(Sessao.getConexaoBanco,0,[FrEmpresa.getEmpresa().EmpresaId, 'E',pNotaItens[i].produto_id]);
      if vLocalProd <> nil then begin
        pNotaItens[i].LocalEntradaId := vLocalProd[0].LocalId;
        pNotaItens[i].NomeLocal      := vLocalProd[0].NomeLocal;
      end;
    end;

    AdicionarProdutoNoGrid(
      pNotaItens[i].produto_id,
      pNotaItens[i].nome_produto,
      pNotaItens[i].nome_marca,
      pNotaItens[i].CfopId,
      pNotaItens[i].quantidade,
      pNotaItens[i].UnidadeEntradaId,
      pNotaItens[i].preco_unitario,
      pNotaItens[i].valor_total,
      pNotaItens[i].base_calculo_icms,
      pNotaItens[i].indice_reducao_base_icms,
      pNotaItens[i].indice_reducao_base_icms_st,
      pNotaItens[i].percentual_icms,
      pNotaItens[i].valor_icms,
      pNotaItens[i].base_calculo_icms_st,
      pNotaItens[i].percentual_icms_st,
      pNotaItens[i].valor_icms_st,
      pNotaItens[i].valor_ipi,
      pNotaItens[i].percentual_ipi,
      pNotaItens[i].base_calculo_pis,
      pNotaItens[i].percentual_pis,
      pNotaItens[i].valor_pis,
      pNotaItens[i].cst_pis,
      pNotaItens[i].base_calculo_cofins,
      pNotaItens[i].percentual_cofins,
      pNotaItens[i].valor_cofins,
      pNotaItens[i].cst_cofins,
      pNotaItens[i].cst,
      pNotaItens[i].codigo_ncm,
      pNotaItens[i].codigo_barras,
      pNotaItens[i].valor_total_desconto,
      pNotaItens[i].ValorFreteCusto,
      pNotaItens[i].valor_total_outras_despesas,
      pNotaItens[i].LocalEntradaId,
      pNotaItens[i].NomeLocal,
      pNotaItens[i].iva,
      pNotaItens[i].preco_pauta,
      pNotaItens[i].informacoes_adicionais,
      pNotaItens[i].Peso,
      pNotaItens[i].ValorICMSFreteCusto,
      pNotaItens[i].QuantidadeEmbalagem,
      pNotaItens[i].UnidadeCompraId,
      pNotaItens[i].MultiploCompra,
      pNotaItens[i].ExigirDataFabricacaoLote,
      pNotaItens[i].ExigirDataVencimentoLote,
      pNotaItens[i].TipoControleEstoque,
      pNotaItens[i].EntradaMercadoriasBonific,
      pNotaItens[i].MultiploVenda
    );

    vAchou := False;
    vPosicCfop := -1;
    for j := Low(vCfops) to High(vCfops) do begin
      vAchou := pNotaItens[i].CfopId = vCfops[j].CfopId;
      if vAchou then begin
        vPosicCfop := j;
        Break;
      end;
    end;

    if not vAchou then begin
      SetLength(vCfops, Length(vCfops) + 1);

      vCfops[High(vCfops)].CfopId := pNotaItens[i].CfopId;
      vCfops[High(vCfops)].Total  := pNotaItens[i].valor_total;
    end
    else
      vCfops[vPosicCfop].Total := vCfops[vPosicCfop].Total + pNotaItens[i].valor_total;
  end;

  vCfopid := '';
  vTotal  := 0;
  for j := Low(vCfops) to High(vCfops) do begin
    if vTotal < vCfops[j].Total then begin
      vCfopid := vCfops[j].CfopId;
      vTotal  := vCfops[j].Total;
    end;
  end;

  FrCFOPCapa.InserirDadoPorChave(vCfopid);

  FrDadosCobranca.Titulos := pNotaFinanceiro;
  // Deixar comentado por enquanto, mudar essa forma de carregar
  FrBuscarPedidosCompras.ItensPedidoCompra := _EntradasNFItensCompras.BuscarEntradasNFItensCompras(Sessao.getConexaoBanco, 0, [pNota.entrada_id]);
  FrBuscarPedidosCompras.BuscarPedidosCompraCarregados();
  FLotes := _EntradasNfItensLotes.BuscarEntradasNfItensLotes(Sessao.getConexaoBanco, 0, [pNota.entrada_id]);
  FTitulosOutrosCustos := _EntradasNotasFiscFinanceiro.BuscarEntradaNotaFiscalFinanceiro(Sessao.getConexaoBanco, 1, [pNota.entrada_id, 'O']);

  vTotal := 0;
  for i := Low(FTitulosOutrosCustos) to High(FTitulosOutrosCustos) do
    vTotal := vTotal + FTitulosOutrosCustos[i].Valor;

  eValorOutrosCustos.AsCurr := vTotal;

  _Biblioteca.Habilitar(getComponentesCabecalho, False, False);
  _Biblioteca.Habilitar(getComponentesItens, True, False);
  IniciarComponentes;
  sgProdutosClick(nil);
  SetarFoco(eChaveAcessoNFe);

  if pCarregandoXML then begin
    if (not FrCFOPCapa.EstaVazio) and (FrCFOPCapa.GetCFOP().PlanoFinanceiroEntradaId <> '') then
      FrPlanoFinanceiro.InserirDadoPorChave(FrCFOPCapa.GetCFOP().PlanoFinanceiroEntradaId, False)
    else
      FrPlanoFinanceiro.InserirDadoPorChave(pNota.PlanoFinanceiroId, False)
  end;

  FDadosCarregado := pNota;

  for i := sgProdutos.FixedRows to sgProdutos.RowCount - 1 do begin
    pProdutoId := SFormatInt(sgProdutos.Cells[coProdutoId, i]);
    pQuantidade := SFormatDouble(sgProdutos.Cells[coQuantidade, i]);

    vTemDivergencia := FrBuscarPedidosCompras.VerificarDivergenciaAdicionandoNovoPedido(
      pProdutoId,
      pQuantidade
    );

    sgProdutos.Cells[coDivergenciaPedidoCompra, i] := IIfStr(vTemDivergencia, 'S', 'N');
  end;

  sgProdutos.Repaint;
end;

procedure TFormEntradaNotasFiscais.IniciarComponentes;
var
  vInicial: string;
begin
  if FrFornecedor.GetFornecedor().cadastro.estado_id = FrEmpresa.getEmpresa.EstadoId then
    vInicial := '1'
  else
    vInicial := '2';

  FrCFOPCapa.SetInicioCFOPPesquisa(vInicial);
  FrCFOPProduto.SetInicioCFOPPesquisa(vInicial);
  rgTipoRateioFrete.SetIndicePorValor( FrFornecedor.GetFornecedor.TipoRateioFrete );
  FrBuscarPedidosCompras.FornecedorId := FrFornecedor.GetFornecedor().cadastro_id;
  FrBuscarPedidosCompras.EmpresaId := FrEmpresa.getEmpresa().EmpresaId;
end;

procedure TFormEntradaNotasFiscais.miDefinirLocalEntradaTodosProdutosClick(Sender: TObject);
var
  i: Integer;
  vLocal: RecLocaisProdutos;
begin
  inherited;
  vLocal := RecLocaisProdutos(PesquisaLocaisProdutos.PesquisarLocalProduto);
  if vLocal = nil then
    Exit;

  for i := 1 to sgProdutos.RowCount -1 do begin
    if (Sender = miDefinirLocalEntradaProdutosNaoDefinidos) and (sgProdutos.Cells[coLocalId, i] <> '') then
      Continue;

    sgProdutos.Cells[coLocalId, i]   := NFormat(vLocal.local_id);
    sgProdutos.Cells[coNomeLocal, i] := vLocal.nome;
  end;
end;

procedure TFormEntradaNotasFiscais.miExcluirProdutoClick(Sender: TObject);
var
  produtoId: Integer;
begin
  inherited;
  if sgProdutos.Cells[coProdutoId, sgProdutos.Row] = '' then
    Exit;

  if not _Biblioteca.Perguntar('Deseja realmente excluir este produto?') then
    Exit;

  produtoId := _Biblioteca.SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]);
  sgProdutos.DeleteRow(sgProdutos.Row);
  FrBuscarPedidosCompras.DesvincularItemEQuantidade(produtoId);
  TotalizarProdutos;
end;

procedure TFormEntradaNotasFiscais.miInformaesPrecoFinalLiquidoClick(Sender: TObject);
var
  vQuantidade: Double;
begin
  inherited;

  vQuantidade := SFormatDouble( sgProdutos.Cells[coQtdeEntradaAltis, sgProdutos.Row] );

  InformacoesPrecoLiquidoItemEntrada.Informar(
    SFormatDouble( sgProdutos.Cells[coValorTotal, sgProdutos.Row] ) / vQuantidade,
    SFormatDouble( sgProdutos.Cells[coValorOutrasDespesas, sgProdutos.Row] ) / vQuantidade,
    SFormatDouble( sgProdutos.Cells[coValorDesconto, sgProdutos.Row] ) / vQuantidade,
    SFormatDouble( sgProdutos.Cells[coValorFrete, sgProdutos.Row] ) / vQuantidade,
    SFormatDouble( sgProdutos.Cells[coValorIPIUsuario, sgProdutos.Row] ) / vQuantidade,
    SFormatDouble( sgProdutos.Cells[coValorICMSSTUsuario, sgProdutos.Row] ) / vQuantidade,
    SFormatDouble( sgProdutos.Cells[coValorOutrosCustos, sgProdutos.Row] ) / vQuantidade,
    SFormatDouble( sgProdutos.Cells[coValorDifalCusto, sgProdutos.Row] ) / vQuantidade,
    SFormatDouble( sgProdutos.Cells[coPrecoFinal, sgProdutos.Row] ),
    SFormatDouble( sgProdutos.Cells[coValorICMSCusto, sgProdutos.Row] ),
    SFormatDouble( sgProdutos.Cells[coValorICMSFreteCusto, sgProdutos.Row] ) / vQuantidade,
    SFormatDouble( sgProdutos.Cells[coValorPisCusto, sgProdutos.Row] ),
    SFormatDouble( sgProdutos.Cells[coValorCofinsCusto, sgProdutos.Row] ),
    SFormatDouble( sgProdutos.Cells[coPrecoLiquido, sgProdutos.Row] )
  );
end;

procedure TFormEntradaNotasFiscais.RatearFreteProdutos;
var
  i: Integer;
  vIndice: Double;
  ValorRatear: Double;
  vValorICMSFreteRatear: Double;
begin
  if rgTipoRateioFrete.GetValor = 'M' then
    Exit;

  ValorRatear := eValorFrete.AsDouble + eValorConhecimento.AsDouble;
  if NFormatN(ValorRatear) = '' then
    Exit;

  vValorICMSFreteRatear := 0;
  if FDadosConhecimento <> nil then begin
    if FParametrosEmpresa.RegimeTributario = 'SN' then
      vValorICMSFreteRatear := 0
    else
      vValorICMSFreteRatear := FDadosConhecimento[0].ValorICMSFrete;
  end;

  for i := 1 to sgProdutos.RowCount -1 do begin
    sgProdutos.Cells[coValorFrete, i] := '';

    if rgTipoRateioFrete.GetValor = 'Q' then begin
      if stQtdeTotal.Caption = '' then
        Exit;

      vIndice := SFormatDouble(sgProdutos.Cells[coQtdeEntradaAltis, i]) / SFormatDouble( stQtdeTotal.Caption );
    end
    else if rgTipoRateioFrete.GetValor = 'P' then begin
      if stPesoTotal.Caption = '' then
        Exit;

      vIndice := SFormatDouble(sgProdutos.Cells[coPeso, i]) * SFormatDouble(sgProdutos.Cells[coQtdeEntradaAltis, i]) / SFormatDouble( stPesoTotal.Caption );
    end
    else begin
      if stValorTotal.Caption = '' then
        Exit;

      vIndice := SFormatDouble(sgProdutos.Cells[coValorTotal, i]) / SFormatDouble( stValorTotal.Caption );
    end;

    sgProdutos.Cells[coValorFrete, i]          := NFormatN( Arredondar(ValorRatear * vIndice, 2) );
    sgProdutos.Cells[coValorICMSFreteCusto, i] := NFormatN( Arredondar(vValorICMSFreteRatear * vIndice, 2) );
  end;
end;

procedure TFormEntradaNotasFiscais.RatearOutrosCustos;
var
  i: Integer;
  vIndice: Double;
  vFaltaRatear: Double;
  vLinhaItemMaisSignificativo: Integer;
  vValorItemMaisSignificativo: Double;
begin
  for i := 1 to sgProdutos.RowCount -1 do
    sgProdutos.Cells[coValorOutrosCustos, i] := '';

  // Se n�o houver valor a ratear
  if eValorOutrosCustos.AsCurr + eValorOutrosCustosFinanceiros.AsCurr = 0 then
    Exit;

  // Se n�o houver produto lan�ado
  if sgProdutos.Cells[coProdutoId, 1] = '' then
    Exit;

  if SFormatCurr(stValorTotal.Caption) = 0 then
    Exit;

  vFaltaRatear := eValorOutrosCustos.AsCurr + eValorOutrosCustosFinanceiros.AsCurr;
  vLinhaItemMaisSignificativo := -1;
  vValorItemMaisSignificativo := 0;
  for i := 1 to sgProdutos.RowCount -1 do begin
    if SFormatCurr( sgProdutos.Cells[coValorTotal, i] ) > vValorItemMaisSignificativo then begin
      vValorItemMaisSignificativo := SFormatCurr( sgProdutos.Cells[coValorTotal, i] );
      vLinhaItemMaisSignificativo := i;
    end;

    vIndice := SFormatDouble( sgProdutos.Cells[coValorTotal, i] ) / SFormatDouble( stValorTotal.Caption );

    sgProdutos.Cells[coValorOutrosCustos, i] := NFormat( (eValorOutrosCustos.AsCurr + eValorOutrosCustosFinanceiros.AsCurr) * vIndice );
    vFaltaRatear := vFaltaRatear - SFormatDouble( sgProdutos.Cells[coValorOutrosCustos, i] );
  end;

  if NFormatN(vFaltaRatear) <> '' then
    sgProdutos.Cells[coValorOutrosCustos, vLinhaItemMaisSignificativo] := NFormat(SFormatDouble(sgProdutos.Cells[coValorOutrosCustos, vLinhaItemMaisSignificativo]) + vFaltaRatear);
end;

procedure TFormEntradaNotasFiscais.ReprocessarItensPedidos;
var
  i: Integer;
  vTemDivergencia: Boolean;
  pProdutoId: Integer;
  pQuantidade: Double;
begin
  for i := sgProdutos.FixedRows to sgProdutos.RowCount - 1 do begin
    pProdutoId := SFormatInt(sgProdutos.Cells[coProdutoId, i]);
    pQuantidade := SFormatDouble(sgProdutos.Cells[coQuantidade, i]);

    vTemDivergencia := FrBuscarPedidosCompras.VerificarDivergenciaAdicionandoNovoPedido(
      pProdutoId,
      pQuantidade
    );

    sgProdutos.Cells[coDivergenciaPedidoCompra, i] := IIfStr(vTemDivergencia, 'S', 'N');
  end;

  sgProdutos.Repaint;
end;

procedure TFormEntradaNotasFiscais.rgTipoRateioFreteClick(Sender: TObject);
begin
  inherited;
  RatearFreteProdutos;
end;

procedure TFormEntradaNotasFiscais.sbCarregarXMLClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar<RecEntradaXML>;
begin
  inherited;
  vRetTela := CarregarXmlEntradaNotasFiscais.CarregarXML(FPreEntradaId);
  if vRetTela.BuscaCancelada then begin
    if FPreEntradaId > 0 then begin
      if Self.Showing then
        Close;
    end;
    Exit;
  end;

  Modo(True);

  FCarregouXML := True;
  FDadosCarregado := vRetTela.Dados.Capa;
  PreencherRegistro(vRetTela.Dados.Capa, vRetTela.Dados.Itens, nil, True);
  FrDadosCobranca.Titulos := vRetTela.Dados.Titulos;
  FrDadosCobranca.Modo(True,False);
  eDataContabil.AsData := Sessao.getDataHora;

  BuscarConhecimentoFrete;
  TotalizarProdutos;

  eValorICMSST.AsCurr        := vRetTela.Dados.Capa.valor_icms_st;
  eValorIPI.AsCurr           := vRetTela.Dados.Capa.valor_ipi;
  eValorTotalProdutos.AsCurr := vRetTela.Dados.Capa.valor_total_produtos;

  eId.Clear;

  if cbSerieNota.GetValor = '' then begin
    _Biblioteca.Exclamar(
      'A s�rie da nota n�o foi encontrada, verifique se a mesma encontra-se cadastrada!' + sLineBreak + 'S�rie ' + vRetTela.Dados.Capa.serie_nota
    );
    Modo(False);
  end;
end;

procedure TFormEntradaNotasFiscais.sbLancarConhecimentoFreteClick(Sender: TObject);
begin
  inherited;
  Sessao.AbrirTela(TFormConhecimentosFretes);
end;

procedure TFormEntradaNotasFiscais.sbOutrosCustosClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar<RecOutrosCustos>;
begin
  inherited;

  vRetTela := BuscarDadosOutrosCustosEntradas.Buscar(FTitulosOutrosCustos);
  if vRetTela.BuscaCancelada then
    Exit;

  eValorOutrosCustos.AsCurr := vRetTela.Dados.ValorTotal;
  FTitulosOutrosCustos      := vRetTela.Dados.Titulos;  
end;

procedure TFormEntradaNotasFiscais.sgProdutosClick(Sender: TObject);
begin
  inherited;
  FrBuscarPedidosCompras.ProdutoId := SFormatInt( sgProdutos.Cells[coProdutoId, sgProdutos.Row] );
  FrBuscarPedidosCompras.ItemId    := sgProdutos.Row;
  FrBuscarPedidosCompras.QuantidadeBuscar := SFormatCurr( sgProdutos.Cells[coQuantidade, sgProdutos.Row] );

  FrBuscarLotes.setTipoControleEstoque(
    sgProdutos.Cells[coTipoControleEstoque, sgProdutos.Row],
    sgProdutos.Cells[coExigirDataFabricacaoLote, sgProdutos.Row] = 'S',
    sgProdutos.Cells[coExigirDataVencimentoLote, sgProdutos.Row] = 'S'
  );

  FrBuscarLotes.ProdutoId      := SFormatInt( sgProdutos.Cells[coProdutoId, sgProdutos.Row] );
  FrBuscarLotes.MultiploCompra := SFormatDouble( sgProdutos.Cells[coMultiploVenda, sgProdutos.Row] );
  FrBuscarLotes.Lotes          := FLotes;
end;

procedure TFormEntradaNotasFiscais.sgProdutosDblClick(Sender: TObject);
begin
  inherited;
  if sgProdutos.Cells[coProdutoId, sgProdutos.Row] = '' then
    Exit;

  FrProduto.InserirDadoPorChave(SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]));
  FrLocalProduto.InserirDadoPorChave( SFormatInt(sgProdutos.Cells[coLocalId, sgProdutos.Row]) );

  FrMultiplosCompra.setUnidadeDefinida(
    sgProdutos.Cells[coUnidadeEntradaAltis, sgProdutos.Row],
    SFormatDouble(sgProdutos.Cells[coMultiploCompra, sgProdutos.Row]),
    SFormatDouble(sgProdutos.Cells[coQuantidadeEmbalagem, sgProdutos.Row])
  );

  eQuantidade.AsDouble     := SFormatDouble(sgProdutos.Cells[coQuantidade, sgProdutos.Row]);
  eValorUnitario.AsDouble  := SFormatDouble(sgProdutos.Cells[coValorUnitario, sgProdutos.Row]);
  eTotalProduto.AsDouble   := SFormatDouble(sgProdutos.Cells[coValorTotal, sgProdutos.Row]);
  eValorDescontoProd.AsDouble  := SFormatDouble(sgProdutos.Cells[coValorDesconto, sgProdutos.Row]);
  eValorOutDespesasProd.AsDouble := SFormatDouble(sgProdutos.Cells[coValorOutrasDespesas, sgProdutos.Row]);
  FrCFOPProduto.InserirDadoPorChave(sgProdutos.Cells[coCFOP, sgProdutos.Row], False);
  cbCST.SetIndicePorValor( sgProdutos.Cells[coCST, sgProdutos.Row] );

  eBaseCalcICMSProd.AsDouble             := SFormatDouble(sgProdutos.Cells[coBaseICMSUsuario, sgProdutos.Row]);
  eIndiceReducaoBaseCalculoICMS.AsDouble := SFormatDouble(sgProdutos.Cells[coIndiceReducaoBaseCalculoICMSUsuario, sgProdutos.Row]);
  ePercentualICMS.AsDouble               := SFormatDouble(sgProdutos.Cells[coPercentualICMSUsuario, sgProdutos.Row]);
  eValorICMSProd.AsDouble                := SFormatDouble(sgProdutos.Cells[coValorICMSUsuario, sgProdutos.Row]);

  eBaseCalcICMSSTProd.AsDouble  := SFormatDouble(sgProdutos.Cells[coBaseICMSStUsuario, sgProdutos.Row]);
  eIndiceReducaoICMSST.AsDouble := SFormatDouble(sgProdutos.Cells[coIndiceReducaoBaseCalcICMSStUsuario, sgProdutos.Row]);
  ePercIcmsST.AsDouble          := SFormatDouble(sgProdutos.Cells[coPercentualICMSStUsuario, sgProdutos.Row]);
  eValorICMSSTProd.AsDouble     := SFormatDouble(sgProdutos.Cells[coValorICMSStUsuario, sgProdutos.Row]);

  ePercentualIPI.AsDouble  := SFormatDouble(sgProdutos.Cells[coPercentualIPIUsuario, sgProdutos.Row]);
  eValorIPIProd.AsDouble   := SFormatDouble(sgProdutos.Cells[coValorIPIUsuario, sgProdutos.Row]);

  SetarFoco(FrProduto);
end;

procedure TFormEntradaNotasFiscais.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if
    ACol in[
      coProdutoId,
      coQuantidade,
      coValorUnitario,
      coValorTotal,
      coValorDesconto,
      coValorOutrasDespesas,
      coQtdeEntradaAltis,
      coValorFrete,
      coBaseICMSUsuario,
      coPercentualICMSUsuario,
      coValorICMSUsuario,
      coBaseICMSSTUsuario,
      coPercentualICMSSTUsuario,
      coValorICMSSTUsuario,
      coValorIPIUsuario,
      coPercentualIPIUsuario,
      coLocalId,
      coPrecoFinal,
      coPrecoLiquido]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormEntradaNotasFiscais.sgProdutosGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgProdutos.Cells[coDivergenciaPedidoCompra, ARow] = 'S' then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicaoPadrao;
    AFont.Color  := _Biblioteca.coCorFonteEdicao5;
  end
  else begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicaoPadrao;
    AFont.Color  := _Biblioteca.coCorFonteEdicaoPadrao;
  end;
end;

procedure TFormEntradaNotasFiscais.sgProdutosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sgProdutosDblClick(nil)
  else if Key = VK_DELETE then
    miExcluirProdutoClick(Sender);
end;

procedure TFormEntradaNotasFiscais.SpeedButtonLuka1Click(Sender: TObject);
var
  i: Integer;
  vRetTela: TRetornoTelaFinalizar<RecDadosCompras>;
begin
  inherited;
  vItensPedidos := _ComprasItens.BuscarComprasItens(
    Sessao.getConexaoBanco,
    0,
    [7965]
  );

  for I := Low(vItensPedidos) to High(vItensPedidos) do begin

  end;
end;

procedure TFormEntradaNotasFiscais.TotalizarProdutos;
var
  i: Integer;
  vQtdeTotal: Double;
  vPesoTotal: Double;

  vValorTotal: Currency;
  vTotalDesconto: Currency;
  vTotalOutrasDesp: Currency;
  vTotalST: Currency;
  vTotalIPI: Currency;
  vTotalBonificacao: Currency;
begin
  vValorTotal := 0;
  vQtdeTotal  := 0;
  vPesoTotal  := 0;

  vTotalDesconto    := 0;
  vTotalOutrasDesp  := 0;
  vTotalST          := 0;
  vTotalIPI         := 0;
  vTotalBonificacao := 0;

  for i := 1 to sgProdutos.RowCount -1 do begin
    vValorTotal := vValorTotal + SFormatDouble( sgProdutos.Cells[coValorTotal, i] );
    vQtdeTotal  := vQtdeTotal + SFormatDouble( sgProdutos.Cells[coQuantidade, i] );
    vPesoTotal  := vPesoTotal + SFormatDouble( sgProdutos.Cells[coPeso, i] ) * SFormatDouble( sgProdutos.Cells[coQtdeEntradaAltis, i] );

    vTotalDesconto   := vTotalDesconto + SFormatDouble( sgProdutos.Cells[coValorDesconto, i] );
    vTotalOutrasDesp := vTotalOutrasDesp + SFormatDouble( sgProdutos.Cells[coValorOutrasDespesas, i] );

    vTotalST  := vTotalST + SFormatDouble( sgProdutos.Cells[coValorICMSSTUsuario, i] );
    vTotalIPI := vTotalIPI + SFormatDouble( sgProdutos.Cells[coValorIPIUsuario, i] );

    if sgProdutos.Cells[coCfopBonificacao, i] = 'S' then begin
      vTotalBonificacao :=
        vTotalBonificacao +
        SFormatCurr( sgProdutos.Cells[coValorTotal, i] ) +
        SFormatCurr( sgProdutos.Cells[coValorICMSSTUsuario, i] ) +
        SFormatCurr( sgProdutos.Cells[coValorIPIUsuario, i] );
    end;
  end;

  stValorTotal.Caption := NFormatN(vValorTotal);
  stQtdeTotal.Caption  := NFormatN(vQtdeTotal, 3);
  stPesoTotal.Caption  := NFormatN(vPesoTotal, 3);

  eValorTotalProdutos.AsDouble  := vValorTotal;
  eValorDesconto.AsDouble       := vTotalDesconto;
  eOutrasDespesas.AsDouble      := vTotalOutrasDesp;
  eValorICMSST.AsDouble         := vTotalST;
  eValorIPI.AsDouble            := vTotalIPI;
  eValorBonificacao.AsCurr      := vTotalBonificacao;

  RatearOutrosCustos;
  RatearFreteProdutos;
  CalcularImpostosProdutos;
  CalcularValoresCustosProdutos;
  CalcularTotalNota(nil);
end;

procedure TFormEntradaNotasFiscais.ValidarItensDivergentes;
var
  i: Integer;
  j: Integer;
  totalItemPedidos: Double;
  saldoItemPedidos: Double;
  quantidadeItem: Double;
begin
  for i := sgProdutos.FixedRows to sgProdutos.RowCount - 1 do begin
    //Se n�o tiver pedido de compras carregado limpa todas as divergencias
    if FrBuscarPedidosCompras.ItensPedidoCompra = nil then
      sgProdutos.Cells[coDivergenciaPedidoCompra, i] := 'N'
    else begin
      totalItemPedidos := 0;
      saldoItemPedidos := 0;
      quantidadeItem := SFormatDouble(sgProdutos.Cells[coQuantidade, i]);
      for j := Low(FrBuscarPedidosCompras.ItensPedidoCompra) to High(FrBuscarPedidosCompras.ItensPedidoCompra) do begin
        if SFormatInt(sgProdutos.Cells[coProdutoId, i]) <> FrBuscarPedidosCompras.ItensPedidoCompra[j].ProdutoId then
          Continue;

        totalItemPedidos := totalItemPedidos + FrBuscarPedidosCompras.ItensPedidoCompra[j].Quantidade;
        saldoItemPedidos := saldoItemPedidos + FrBuscarPedidosCompras.ItensPedidoCompra[j].Saldo;
      end;

      if (quantidadeItem > saldoItemPedidos) then
        sgProdutos.Cells[coDivergenciaPedidoCompra, i] := 'S'

    end;
  end;

  sgProdutos.Repaint;
end;

procedure TFormEntradaNotasFiscais.VerificarRegistro(Sender: TObject);
var
  i: Integer;

  procedure ValidarExisteDiferencaLotes(
    pProdutoId: Integer;
    pNomeProduto: string;
    pLinha: Integer;
    pQuantidadeEntrada: Double;
    pExigirDataFabricao: Boolean;
    pExigirDataVencimento: Boolean
  );
  var
    j: Integer;
    vQtde: Double;
  begin
    vQtde := 0;

    for j := Low(FLotes) to High(FLotes) do begin
      if pProdutoId <> FLotes[j].ProdutoId then
        Continue;

      if pExigirDataFabricao and (DFormatN(FLotes[j].DataFabricacao) = '') then begin
        _Biblioteca.Exclamar('A data de fabrica��o do lote para o produto ' + pNomeProduto + ' n�o foi informado corretamente!');
        sgProdutos.Row := pLinha;
        sgProdutos.Col := coQtdeEntradaAltis;
        SetarFoco(sgProdutos);
        Abort;
      end;

      if pExigirDataVencimento and (DFormatN(FLotes[j].DataVencimento) = '') then begin
        _Biblioteca.Exclamar('A data de vencimento do lote para o produto ' + pNomeProduto + ' n�o foi informado corretamente!');
        sgProdutos.Row := pLinha;
        sgProdutos.Col := coQtdeEntradaAltis;
        SetarFoco(sgProdutos);
        Abort;
      end;

      vQtde := vQtde + FLotes[j].Quantidade;
    end;

    if NFormatNEstoque(pQuantidadeEntrada) <> NFormatNEstoque(vQtde) then begin
      _Biblioteca.Exclamar('Existe diferen�a de quantidades nos lotes informados para o produto ' + pNomeProduto);
      sgProdutos.Row := pLinha;
      sgProdutos.Col := coQtdeEntradaAltis;
      SetarFoco(sgProdutos);
      Abort;
    end;
  end;

begin
  inherited;

  FrBuscarLotesExit(Sender);

  if cbModeloNota.Text = '55' then begin
    if not ValidarChaveNFe(eChaveAcessoNFe.Text) then
      Abort;
  end;

  if FrFornecedor.EstaVazio then begin
    Exclamar('O fornecedor n�o foi informado, verique!');
    SetarFoco(FrFornecedor);
    Abort;
  end;

  if eNumeroNotaFiscal.Text = '' then begin
    Exclamar('O numero da nota n�o foi informado, verique!');
    SetarFoco(eNumeroNotaFiscal);
    Abort;
  end;

  if not eDataEmissaoNota.DataOk then begin
    Exclamar('A data de emiss�o da nota n�o � v�lida, verifique!');
    SetarFoco(eDataEmissaoNota);
    Abort;
  end;

  if eValorTotalProdutos.AsDouble = 0 then begin
    Exclamar('O total dos produtos deve ser maior que 0, verifique!');
    SetarFoco(eTotalProduto);
    Abort;
  end;

  if eValorTotalNota.AsDouble = 0 then begin
    Exclamar('O total da nota fiscal deve ser maior que 0, verifique!');
    SetarFoco(eValorTotalNota);
    Abort;
  end;

  if FrCFOPCapa.EstaVazio then begin
    Exclamar('O CFOP da nota n�o foi informado, verifique!');
    SetarFoco(FrCFOPCapa);
    Abort;
  end;

  if rgTipoRateioFrete.ItemIndex = -1 then begin
    Exclamar('O tipo de rateio de frete n�o foi informado, verifique!');
    SetarFoco(rgTipoRateioFrete);
    Abort;
  end;

  if cbStatusEntrada.Text = '' then begin
    Exclamar('O status da entrada n�o foi definido, verifique!');
    SetarFoco(cbStatusEntrada);
    Abort;
  end;

  if FrPlanoFinanceiro.EstaVazio then begin
    Exclamar('O plano financeiro da entrada deve ser informado, verifique!');
    SetarFoco(FrPlanoFinanceiro);
    Abort;
  end;

  // Se n�o estiver aguardando chegada da mercadoria
  if (cbStatusEntrada.GetValor <> 'ACM') and ((FDadosCarregado.entrada_id = -1) or (FDadosCarregado.OrigemEntrada <> 'TPE')) and (FrCFOPCapa.GetCFOP().EntradaMercadoriasBonific = 'N') then begin
    if FrDadosCobranca.ExisteDiferenca then begin
      Exclamar('Existe diferen�a nos t�tulos financeiros lan�ados, verifique!');
      SetarFoco(FrDadosCobranca);
      Abort;
    end;
  end;

  // Se n�o tiver produtos e n�o for CFOP que permite tal opera��o
  if (sgProdutos.Cells[coProdutoId, 1] = '') and (FrCFOPCapa.GetCFOP.NaoExigirItensEntrada = 'N') then begin
    Exclamar('N�o foi informado nenhum item para entrada, verifique!');
    SetarFoco(sgProdutos);
    Abort;
  end;

  if (sgProdutos.Cells[coProdutoId, 1] <> '') or (FrCFOPCapa.GetCFOP.NaoExigirItensEntrada = 'N') then begin
    for i := 1 to sgProdutos.RowCount -1 do begin
      if sgProdutos.Cells[coLocalId, i] = '' then begin
        Exclamar('O local de entrada para o produto ' + sgProdutos.Cells[coNomeProduto, i] + ' n�o foi definido, veriique!');
        sgProdutos.Row := i;
        SetarFoco(sgProdutos);
        Abort;
      end;

      if Em(sgProdutos.Cells[coTipoControleEstoque, i], ['L', 'G', 'P']) then begin
        ValidarExisteDiferencaLotes(
          SFormatInt(sgProdutos.Cells[coProdutoId, i]),
          sgProdutos.Cells[coNomeProduto, i],
          i,
          SFormatDouble(sgProdutos.Cells[coQtdeEntradaAltis, i]),
          sgProdutos.Cells[coExigirDataFabricacaoLote, i] = 'S',
          sgProdutos.Cells[coExigirDataVencimentoLote, i] = 'S'
        );
      end;
    end;
  end;
end;

end.
