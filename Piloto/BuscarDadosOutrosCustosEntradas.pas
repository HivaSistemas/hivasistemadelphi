unit BuscarDadosOutrosCustosEntradas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, _FrameHerancaPrincipal, FrameDadosCobrancaOutrosCustos,
  Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, _RecordsFinanceiros;

type
  RecOutrosCustos = record
    ValorTotal: Currency;
    Titulos: TArray<RecTitulosFinanceiros>;
  end;

  TFormBuscarDadosOutrosCustosEntradas = class(TFormHerancaFinalizar)
    FrOutrosCustos: TFrDadosCobrancaOutrosCustos;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function Buscar(pTitulos: TArray<RecTitulosFinanceiros>): TRetornoTelaFinalizar<RecOutrosCustos>;

implementation

{$R *.dfm}

function Buscar(pTitulos: TArray<RecTitulosFinanceiros>): TRetornoTelaFinalizar<RecOutrosCustos>;
var
  vForm: TFormBuscarDadosOutrosCustosEntradas;
begin
  vForm := TFormBuscarDadosOutrosCustosEntradas.Create(nil);
  vForm.FrOutrosCustos.Titulos := pTitulos;

  if Result.Ok(vForm.ShowModal) then begin
    Result.Dados.ValorTotal := vForm.FrOutrosCustos.ValorTotal;
    Result.Dados.Titulos    := vForm.FrOutrosCustos.Titulos;
  end;
end;

procedure TFormBuscarDadosOutrosCustosEntradas.FormShow(Sender: TObject);
begin
  inherited;
  _Biblioteca.SetarFoco(FrOutrosCustos.FrFornecedor);
end;

end.
