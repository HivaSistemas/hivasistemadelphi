inherited FrTipoNotaGerar: TFrTipoNotaGerar
  Width = 155
  Height = 140
  ExplicitWidth = 155
  ExplicitHeight = 140
  object grpDocumentosEmitir: TGroupBox
    Left = 0
    Top = -3
    Width = 153
    Height = 140
    Caption = 'Tipos de documento'
    TabOrder = 0
    object ckRecibo: TCheckBoxLuka
      Left = 8
      Top = 30
      Width = 145
      Height = 17
      Caption = 'Comprovante Pag.'
      TabOrder = 0
      CheckedStr = 'N'
    end
    object ckEmitirNFCe: TCheckBoxLuka
      Left = 8
      Top = 50
      Width = 137
      Height = 38
      Caption = 'Cupom Fiscal (NFC-e)'
      TabOrder = 1
      CheckedStr = 'N'
    end
    object ckEmitirNFe: TCheckBoxLuka
      Left = 8
      Top = 94
      Width = 121
      Height = 17
      Caption = 'Nota Fiscal (NF-e)'
      TabOrder = 2
      CheckedStr = 'N'
    end
  end
end
