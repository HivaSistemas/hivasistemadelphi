unit ImpressorasEstacao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, Vcl.Buttons, Vcl.ExtCtrls, HerancaImpressorasEstacaoUsuario,
  Vcl.StdCtrls, EditLuka, Vcl.ComCtrls, ComboBoxLuka, _Sessao, _Biblioteca, _RecordsEspeciais, _ImpressorasEstacoes,
  CheckBoxLuka;

type
  TFormImpressorasEstacao = class(TFormHerancaImpressoraEstacaoUsuario)
    eEstacao: TEditLuka;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure ExcluirRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExibirLogs; override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormImpressorasEstacao }

procedure TFormImpressorasEstacao.BuscarRegistro;
var
  vImpressoras: TArray<RecImpressorasEstacoes>;
begin
  inherited;
  vImpressoras := _ImpressorasEstacoes.BuscarImpressoras(Sessao.getConexaoBanco, 0, [eEstacao.Text]);
  if vImpressoras = nil then
    Exit;

  PreencherRegistros(
    vImpressoras[0].TipoImpressoraNFe,
    vImpressoras[0].ImpressoraNFe,
    vImpressoras[0].AbrirPreviewNFe,

    vImpressoras[0].TipoImpressoraNFCe,
    vImpressoras[0].ImpressoraNFCe,
    vImpressoras[0].AbrirPreviewNFCe,

    vImpressoras[0].TipoImpressoraCompPagamento,
    vImpressoras[0].ImpressoraCompPagamento,
    vImpressoras[0].AbrirPreviewCompPagamento,

    vImpressoras[0].TipoImpressoraComproEntrega,
    vImpressoras[0].ImpressoraComproEntrega,
    vImpressoras[0].AbrirPreviewComproEntrega,

    vImpressoras[0].TipoImpressoraListaSeparac,
    vImpressoras[0].ImpressoraListaSeparac,
    vImpressoras[0].AbrirPreviewListaSeparacao,

    vImpressoras[0].TipoImpressoraOrcamento,
    vImpressoras[0].ImpressoraOrcamento,
    vImpressoras[0].AbrirPreviewOrcamento,

    vImpressoras[0].TipoImpCompPagtoFinanceiro,
    vImpressoras[0].ImpCompPagtoFinanceiro,
    vImpressoras[0].AbrirPrevCompPagtoFinanc,

    vImpressoras[0].TipoImpressoraCompDevolucao,
    vImpressoras[0].ImpressoraCompDevolucao,
    vImpressoras[0].AbrirPrevCompDevolucao,

    vImpressoras[0].TipoImpressoraCompCaixa,
    vImpressoras[0].ImpressoraCompCaixa,
    vImpressoras[0].AbrirPrevCompCaixa,
    vImpressoras[0].ModeloBalanca,
    vImpressoras[0].PortaBalanca,

    vImpressoras[0].TipoImpressoraCtzPromo,
    vImpressoras[0].ImpressoraCtzPromo,
    vImpressoras[0].AbrirPrevCtzPromo
  );
end;

procedure TFormImpressorasEstacao.ExcluirRegistro;
begin
  inherited;

end;

procedure TFormImpressorasEstacao.ExibirLogs;
begin
  inherited;

end;

procedure TFormImpressorasEstacao.FormCreate(Sender: TObject);
begin
  inherited;
  eEstacao.Text := _Biblioteca.NomeComputador;
  setIndexesDefault;
  BuscarRegistro;
end;

procedure TFormImpressorasEstacao.FormShow(Sender: TObject);
begin
  inherited;
  _Biblioteca.SetarFoco(cbCaminhoImpressoraNFe);
end;

procedure TFormImpressorasEstacao.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vRetBanco :=
    _ImpressorasEstacoes.AtualizarImpressoras(
      Sessao.getConexaoBanco,
      eEstacao.Text,

      cbTipoImpressoraNFe.GetValor,
      cbCaminhoImpressoraNFe.Text,
      ckAbrirPreviewNFe.CheckedStr,

      cbTipoImpressoraNFCe.GetValor,
      cbCaminhoImpressoraNFCe.Text,
      ckAbrirPreviewNFCe.CheckedStr,

      cbTipoImpressoraCompPagamento.GetValor,
      cbCaminhoImpressoraCompPagamento.Text,
      ckAbrirPreviewCompPagamento.CheckedStr,

      cbTipoImpressoraComproEntrega.GetValor,
      cbCaminhoImpressoraComproEntrega.Text,
      ckAbrirPreviewCompEntrega.CheckedStr,

      cbTipoImpressoraListaSeparac.GetValor,
      cbCaminhoImpressoraListaSeparac.Text,
      ckAbrirPreviewListaSeparacao.CheckedStr,

      cbTipoImpressoraOrcamento.GetValor,
      cbCaminhoImpressoraOrcamento.Text,
      ckAbrirPreviewOrcamento.CheckedStr,

      cbTipoImpressoraCompPagtoFinanc.GetValor,
      cbCaminhoImpressoraCompPagtoFinanc.Text,
      ckAbrirPreviewCompPagtoFinanc.CheckedStr,

      cbTipoImpressoraComprovanteDevolucao.GetValor,
      cbCaminhoImpressoraComprovanteDevolucao.Text,
      ckAbrirPreviewComprovanteDevolucao.CheckedStr,

      cbTipoImpressoraCompCaixa.GetValor,
      cbCaminhoImpressoraCompCaixa.Text,
      ckAbrirPreviewCompCaixa.CheckedStr,
      cbxModeloBalanca.Text,
      cbxPortaBalanca.Text,

      cbTipoImpressoraCartazPromocional.GetValor,
      cbCaminhoImpressoraCartazPromocional.Text,
      ckAbrirPreviewCartazPromocional.CheckedStr
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  RotinaSucesso;
  sbGravar.Enabled := True;
end;

procedure TFormImpressorasEstacao.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.


