inherited FormMapaProducao: TFormMapaProducao
  Caption = 'Mapa de produ'#231#227'o'
  ClientHeight = 205
  ClientWidth = 663
  ExplicitWidth = 669
  ExplicitHeight = 234
  PixelsPerInch = 96
  TextHeight = 14
  object lb7: TLabel [1]
    Left = 576
    Top = 49
    Width = 64
    Height = 14
    Caption = 'Quantidade'
  end
  inherited pnOpcoes: TPanel
    Height = 205
    ExplicitHeight = 205
    inherited sbDesfazer: TSpeedButton
      Top = 46
      ExplicitTop = 46
    end
    inherited sbExcluir: TSpeedButton
      Visible = False
    end
    inherited sbPesquisar: TSpeedButton
      Visible = False
    end
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 284
    Top = 8
    TabOrder = 5
    Visible = False
    ExplicitLeft = 284
    ExplicitTop = 8
  end
  inline FrProduto: TFrProdutos
    Left = 128
    Top = 49
    Width = 233
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 128
    ExplicitTop = 49
    ExplicitWidth = 233
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 208
      Height = 23
      TabOrder = 4
      ExplicitWidth = 208
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 2
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 5
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 7
    end
    inherited CkMultiSelecao: TCheckBox
      TabOrder = 3
    end
    inherited PnTitulos: TPanel
      Width = 233
      TabOrder = 0
      ExplicitWidth = 233
      inherited lbNomePesquisa: TLabel
        Width = 42
        Caption = 'Produto'
        ExplicitWidth = 42
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 128
        ExplicitLeft = 128
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 208
      Height = 24
      TabOrder = 1
      ExplicitLeft = 208
      ExplicitHeight = 24
      inherited sbPesquisa: TSpeedButton
        Visible = False
      end
    end
    inherited ckSomenteAtivos: TCheckBox
      TabOrder = 6
    end
  end
  inline FrLocalProduto: TFrLocais
    Left = 368
    Top = 49
    Width = 201
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 368
    ExplicitTop = 49
    ExplicitWidth = 201
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 176
      Height = 23
      TabOrder = 5
      ExplicitWidth = 176
      ExplicitHeight = 23
    end
    inherited CkMultiSelecao: TCheckBox
      TabOrder = 3
    end
    inherited PnTitulos: TPanel
      Width = 201
      TabOrder = 4
      ExplicitWidth = 201
      inherited lbNomePesquisa: TLabel
        Width = 28
        Caption = 'Local'
        ExplicitWidth = 28
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 96
        ExplicitLeft = 96
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 176
      Height = 24
      ExplicitLeft = 176
      ExplicitHeight = 24
    end
  end
  object sgProdutosProducao: TGridLuka
    Left = 127
    Top = 112
    Width = 526
    Height = 85
    ColCount = 4
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 6
    OnDrawCell = sgProdutosProducaoDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Qtde. Necess'#225'ria'
      'Qtde. Dispon'#237'vel')
    Grid3D = False
    RealColCount = 4
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      64
      232
      99
      97)
  end
  object eQuantidade: TEditLuka
    Left = 575
    Top = 64
    Width = 78
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Color = clMenuBar
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Text = '0,0000'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    PadraoEstoque = True
    NaoAceitarEspaco = False
  end
  object StaticText2: TStaticText
    Left = 127
    Top = 97
    Width = 526
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Mapa de produ'#231#227'o'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
  end
end
