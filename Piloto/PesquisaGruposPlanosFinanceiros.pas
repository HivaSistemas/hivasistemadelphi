unit PesquisaGruposPlanosFinanceiros;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _GruposPlanosFinanceiros,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Sessao, _Biblioteca,
  Vcl.ExtCtrls;

type
  TFormPesquisaGruposPlanosFinanceiros = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pSomenteAtivos: Boolean; pSomenteNivel3: Boolean): TObject;

implementation

{$R *.dfm}

const
  coDescricao = 2;

var
  FSomenteNivel3: Boolean;

function Pesquisar(pSomenteAtivos: Boolean; pSomenteNivel3: Boolean): TObject;
var
  vObj: TObject;
begin
  FSomenteNivel3 := pSomenteNivel3;
  vObj := _HerancaPesquisas.Pesquisar(TFormPesquisaGruposPlanosFinanceiros, _GruposPlanosFinanceiros.GetFiltros, [pSomenteAtivos]);
  if vObj = nil then
    Result := nil
  else
    Result := RecGruposPlanosFinanceiros(vObj);
end;

{ TFormPesquisaPlanosFinanceiros }

procedure TFormPesquisaGruposPlanosFinanceiros.BuscarRegistros;
var
  i: Integer;
  vDados: TArray<RecGruposPlanosFinanceiros>;
begin
  inherited;

  vDados :=
    _GruposPlanosFinanceiros.BuscarGruposPlanosFinanceiros(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]);

  if vDados = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vDados);

  for i := Low(vDados) to High(vDados) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := vDados[i].GrupoFinanceiroId;
    sgPesquisa.Cells[coDescricao, i + 1]    := vDados[i].Descricao;
  end;
  sgPesquisa.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vDados) );
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaGruposPlanosFinanceiros.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

end.
