unit ConhecimentosFretes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _Sessao, _ComunicacaoNFE,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, ComboBoxLuka, _FrameHerancaPrincipal, _Biblioteca,
  _FrameHenrancaPesquisas, FrameTransportadoras, FrameFornecedores, Vcl.Mask, _RecordsFinanceiros,
  EditLukaData, Vcl.Grids, GridLuka, FrameCFOPs, FramePortadores, StaticTextLuka, _EntradasNotasFiscais,
  FrameTiposCobranca, _RecordsEspeciais, _ConhecimentosFretes, _ConhecimentosFretesItens,
  _ConhecFretesFinanceiros, FrameEmpresas, PesquisaConhecimentosFretes, _PreEntrCteNfeReferenciadas,
  MaskEditLuka, FrameDadosCobranca, CheckBoxLuka, _PreEntradasCte,
  RadioGroupLuka;

type
  TFormConhecimentosFretes = class(TFormHerancaCadastroCodigo)
    FrTransportadora: TFrTransportadora;
    eNumeroConhecimento: TEditLuka;
    lbl1: TLabel;
    lbllb3: TLabel;
    cbModeloConhecimento: TComboBoxLuka;
    cbSerieConhecimento: TComboBoxLuka;
    lbllb2: TLabel;
    FrFornecedor: TFrFornecedores;
    lbllb1: TLabel;
    eNumeroNotaFiscal: TEditLuka;
    lbl2: TLabel;
    cbModeloNota: TComboBoxLuka;
    cbSerieNota: TComboBoxLuka;
    lbl3: TLabel;
    lbllb6: TLabel;
    eDataEmissaoNota: TEditLukaData;
    lbllb13: TLabel;
    ePesoNota: TEditLuka;
    lbl4: TLabel;
    eQtdeNota: TEditLuka;
    lbl5: TLabel;
    eValorNota: TEditLuka;
    sgNotasConhecimento: TGridLuka;
    FrCFOP: TFrCFOPs;
    lbl6: TLabel;
    eDataEmissao: TEditLukaData;
    lbl7: TLabel;
    eDataContabil: TEditLukaData;
    StaticTextLuka2: TStaticTextLuka;
    stQtdeNotas: TStaticText;
    StaticTextLuka1: TStaticTextLuka;
    stTotalPeso: TStaticText;
    StaticTextLuka3: TStaticTextLuka;
    stTotalQtde: TStaticText;
    StaticTextLuka4: TStaticTextLuka;
    stValorTotal: TStaticText;
    lbllb7: TLabel;
    lbllb8: TLabel;
    eBaseCalculoICMS: TEditLuka;
    ePercICMS: TEditLuka;
    eValorICMS: TEditLuka;
    lbl8: TLabel;
    eValorFrete: TEditLuka;
    lbl9: TLabel;
    StaticTextLuka5: TStaticTextLuka;
    lbl12: TLabel;
    FrEmpresa: TFrEmpresas;
    eChaveConhecimento: TMaskEditLuka;
    FrDadosCobranca: TFrDadosCobranca;
    rgTipoRateioConhecimento: TRadioGroupLuka;
    procedure FormCreate(Sender: TObject);
    procedure sgNotasConhecimentoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure eValorNotaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgNotasConhecimentoDblClick(Sender: TObject);
    procedure sgNotasConhecimentoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorFreteChange(Sender: TObject);
  private
    FPreEntradaId: Integer;

    procedure PreencherRegistro(
      pConhecimento: RecConhecimentosFretes;
      pNotas: TArray<RecConhecimentosFretesItens>;
      pFinanceiros: TArray<RecTitulosFinanceiros>
    );

    procedure AddGridNotas(
      pFornecedorId: Integer;
      pNomeFornecedor: string;
      pNumeroNota: Integer;
      pModeloNota: string;
      pSerieNota: string;
      pDataEmissao: TDateTime;
      pPeso: Double;
      pQtde: Double;
      pValorTotalNota: Double
    );

    procedure AtualizarTotais;

    procedure FrCFOPOnAposPesquisar(Sender: TObject);
    procedure FrCFOPOnAposDeletar(Sender: TObject);
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure BuscarRegistro; override;
    procedure ExcluirRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

procedure RelizarEntradaViaPreEntrada(pPreEntradaId: Integer);

implementation

{$R *.dfm}

const
  cnFornecedor  = 0;
  cnNumeroNF    = 1;
  cnModelo      = 2;
  cnSerie       = 3;
  cnDataEmissao = 4;
  cnPeso        = 5;
  cnQuantidade  = 6;
  cnValorNota   = 7;
  (* Ocultas *)
  cnFornecedorId = 8;

procedure RelizarEntradaViaPreEntrada(pPreEntradaId: Integer);
var
  i: Integer;
  vForm: TFormConhecimentosFretes;
  vPreEntrada: TArray<RecPreEntradasCte>;

  vNotas: TArray<RecConhecimentosFretesItens>;
begin
  if pPreEntradaId = 0 then
    Exit;

  if not Sessao.AutorizadoTela('tformconhecimentosfretes') then begin
    Exclamar('Voc� n�o possui permiss�es necess�rias para acessar a tela de entradas!');
    Exit;
  end;

  vPreEntrada := _PreEntradasCte.BuscarPreEntradasCte(Sessao.getConexaoBanco, 0, [pPreEntradaId]);
  if vPreEntrada = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vNotas := _ComunicacaoNFE.getNotasConhecimentoFrete(pPreEntradaId);

  vForm := TFormConhecimentosFretes.Create(Application);
  vForm.Show;
  vForm.Modo(True);
  vForm.FPreEntradaId := pPreEntradaId;

  vForm.FrTransportadora.InserirDadoPorChave(vPreEntrada[0].CadastroId, False);
  vForm.eNumeroConhecimento.AsInt     := vPreEntrada[0].NumeroCte;
  vForm.cbModeloConhecimento.AsString := '57';
  vForm.cbSerieConhecimento.AsString  := vPreEntrada[0].SerieCte;

  vForm.FrEmpresa.InserirDadoPorChave(vPreEntrada[0].EmpresaId, False);
  vForm.eBaseCalculoICMS.AsDouble := vPreEntrada[0].BaseCalculoIcms;
  vForm.ePercICMS.AsDouble        := vPreEntrada[0].PercentualIcms;
  vForm.eValorICMS.AsDouble       := vPreEntrada[0].ValorIcms;
  vForm.eValorFrete.AsDouble      := vPreEntrada[0].ValorTotalCte;

  vForm.FrCFOP.InserirDadoPorChave( vPreEntrada[0].CfopEntradaId, False );

  vForm.eDataEmissao.AsData  := vPreEntrada[0].DataHoraEmissao;
  vForm.eDataContabil.AsData := vPreEntrada[0].DataHoraEmissao;

  vForm.eChaveConhecimento.Text := _Biblioteca.FormatarChaveNFe(vPreEntrada[0].ChaveAcessoCte);

  for i := Low(vNotas) to High(vNotas) do begin
    vForm.AddGridNotas(
      vNotas[i].FornecedorNotaId,
      vNotas[i].NomeFornecedor,
      vNotas[i].NumeroNota,
      vNotas[i].ModeloNota,
      vNotas[i].SerieNota,
      vNotas[i].DataEmissaoNota,
      vNotas[i].PesoNota,
      vNotas[i].QuantidadeNota,
      vNotas[i].ValorTotalNota
    );
  end;

  vForm.AtualizarTotais;
end;

procedure TFormConhecimentosFretes.AddGridNotas(
  pFornecedorId: Integer;
  pNomeFornecedor: string;
  pNumeroNota: Integer;
  pModeloNota: string;
  pSerieNota: string;
  pDataEmissao: TDateTime;
  pPeso: Double;
  pQtde: Double;
  pValorTotalNota: Double
);
var
  vLinha: Integer;
begin
  vLinha := sgNotasConhecimento.Localizar([cnFornecedorId, cnNumeroNF, cnModelo, cnSerie], [NFormat(pFornecedorId), NFormat(pNumeroNota), pModeloNota, pSerieNota]);
  if vLinha = -1 then begin
    if sgNotasConhecimento.Cells[cnFornecedor, 1] = '' then
      vLinha := 1
    else begin
      vLinha := sgNotasConhecimento.RowCount;
      sgNotasConhecimento.RowCount := sgNotasConhecimento.RowCount + 1;
    end;
  end;

  sgNotasConhecimento.Cells[cnFornecedor, vLinha]   := NFormat(pFornecedorId) + ' - ' + pNomeFornecedor;
  sgNotasConhecimento.Cells[cnNumeroNF, vLinha]     := NFormat(pNumeroNota);
  sgNotasConhecimento.Cells[cnModelo, vLinha]       := pModeloNota;
  sgNotasConhecimento.Cells[cnSerie, vLinha]        := pSerieNota;
  sgNotasConhecimento.Cells[cnDataEmissao, vLinha]  := DFormat(pDataEmissao);
  sgNotasConhecimento.Cells[cnPeso, vLinha]         := NFormatN(pPeso, 3);
  sgNotasConhecimento.Cells[cnQuantidade, vLinha]   := NFormatN(pQtde, 3);
  sgNotasConhecimento.Cells[cnValorNota, vLinha]    := NFormat(pValorTotalNota);
  sgNotasConhecimento.Cells[cnFornecedorId, vLinha] := NFormat(pFornecedorId);

  stQtdeNotas.Caption := NFormat(sgNotasConhecimento.RowCount -1);
  sgNotasConhecimento.Row := vLinha;
end;

procedure TFormConhecimentosFretes.AtualizarTotais;
var
  i: Integer;

  vTotalPeso: Double;
  vTotalQtde: Double;
  vValorTotal: Double;
begin
  vTotalPeso := 0;
  vTotalQtde := 0;
  vValorTotal := 0;
  for i := 1 to sgNotasConhecimento.RowCount -1 do begin
    vTotalPeso := vTotalPeso + SFormatDouble(sgNotasConhecimento.Cells[cnPeso, i]);
    vTotalQtde := vTotalQtde + SFormatDouble(sgNotasConhecimento.Cells[cnQuantidade, i]);
    vValorTotal := vValorTotal + SFormatDouble(sgNotasConhecimento.Cells[cnValorNota, i]);
  end;

  stTotalPeso.Caption := NFormatN(vTotalPeso, 3);
  stTotalQtde.Caption := NFormatN(vTotalQtde, 3);
  stValorTotal.Caption := NFormatN(vValorTotal);
end;

procedure TFormConhecimentosFretes.BuscarRegistro;
var
  vDados: TArray<RecConhecimentosFretes>;
begin
  if eID.AsInt = 0 then
    Exit;

  vDados := _ConhecimentosFretes.BuscarConhecimento(Sessao.getConexaoBanco, 0, [eID.AsInt]);
  if vDados = nil then begin
    NenhumRegistro;
    Exit;
  end;

  inherited;

  PreencherRegistro(
    vDados[0],
    _ConhecimentosFretesItens.BuscarItens(Sessao.getConexaoBanco, 0, [eID.AsInt]),
    _ConhecFretesFinanceiros.BuscarItens(Sessao.getConexaoBanco, 0, [eID.AsInt])
  );
end;

procedure TFormConhecimentosFretes.eValorFreteChange(Sender: TObject);
begin
  inherited;
  FrDadosCobranca.ValorPagar := 0;
  if FrCFOP.EstaVazio or (FrCFOP.GetCFOP.EntradaMercadoriasBonific = 'N') then
    FrDadosCobranca.ValorPagar := eValorFrete.AsDouble;
end;

procedure TFormConhecimentosFretes.eValorNotaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if FrFornecedor.EstaVazio then begin
    _Biblioteca.Exclamar('O fornecedor n�o foi informado corretamente, verifique!');
    SetarFoco(FrFornecedor);
    Exit;
  end;

  if eNumeroNotaFiscal.AsInt = 0 then begin
    _Biblioteca.Exclamar('O n�mero da nota fiscal n�o foi informado corretamente, verifique!');
    SetarFoco(eNumeroNotaFiscal);
    Exit;
  end;

  if cbModeloNota.ItemIndex < 0 then begin
    _Biblioteca.Exclamar('O modelo da nota fiscal n�o foi informado corretamente, verifique!');
    SetarFoco(cbModeloNota);
    Exit;
  end;

  if cbSerieNota.ItemIndex < 0 then begin
    _Biblioteca.Exclamar('A s�rie da nota fiscal n�o foi informado corretamente, verifique!');
    SetarFoco(cbSerieNota);
    Exit;
  end;

  if not eDataEmissaoNota.DataOk then begin
    _Biblioteca.Exclamar('A data de emiss�o da nota fiscal n�o foi informado corretamente, verifique!');
    SetarFoco(eDataEmissaoNota);
    Exit;
  end;

  if eQtdeNota.AsCurr = 0 then begin
    _Biblioteca.Exclamar('A quantidade da nota fiscal n�o foi informado corretamente, verifique!');
    SetarFoco(eQtdeNota);
    Exit;
  end;

  if eValorNota.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O valor total da nota fiscal n�o foi informado corretamente, verifique!');
    SetarFoco(eValorNota);
    Exit;
  end;

  AddGridNotas(
    FrFornecedor.GetFornecedor().cadastro_id,
    FrFornecedor.GetFornecedor().cadastro.razao_social,
    eNumeroNotaFiscal.AsInt,
    cbModeloNota.GetValor,
    cbSerieNota.GetValor,
    eDataEmissaoNota.AsData,
    ePesoNota.AsDouble,
    eQtdeNota.AsDouble,
    eValorNota.AsDouble
  );

  AtualizarTotais;

  _Biblioteca.LimparCampos([eNumeroNotaFiscal, eDataEmissaoNota, ePesoNota, eQtdeNota, eValorNota]);
  SetarFoco(FrFornecedor);
end;

procedure TFormConhecimentosFretes.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _ConhecimentosFretes.ExcluirConhecimento(Sessao.getConexaoBanco, eID.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormConhecimentosFretes.FormCreate(Sender: TObject);
begin
  // Tem que ser antes da heran�a por causa do met�do Modo
  FPreEntradaId := -1;

  inherited;
  Sessao.SetComboBoxSeriesNotas(cbSerieConhecimento);
  Sessao.SetComboBoxSeriesNotas(cbSerieNota);

  FrCFOP.OnAposPesquisar := FrCFOPOnAposPesquisar;
  FrCFOP.OnAposDeletar   := FrCFOPOnAposDeletar;
end;

procedure TFormConhecimentosFretes.FrCFOPOnAposDeletar(Sender: TObject);
begin
  eValorFreteChange(Sender);
end;

procedure TFormConhecimentosFretes.FrCFOPOnAposPesquisar(Sender: TObject);
begin
  eValorFreteChange(Sender);
end;

procedure TFormConhecimentosFretes.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vNotas: TArray<RecConhecimentosFretesItens>;
begin
  inherited;

  SetLength(vNotas, sgNotasConhecimento.RowCount -1);
  for i := 1 to sgNotasConhecimento.RowCount -1 do begin
    vNotas[i - 1].FornecedorNotaId := SFormatInt(sgNotasConhecimento.Cells[cnFornecedorId, i]);
    vNotas[i - 1].NumeroNota       := SFormatInt(sgNotasConhecimento.Cells[cnNumeroNF, i]);
    vNotas[i - 1].ModeloNota       := sgNotasConhecimento.Cells[cnModelo, i];
    vNotas[i - 1].SerieNota        := sgNotasConhecimento.Cells[cnSerie, i];
    vNotas[i - 1].DataEmissaoNota  := ToData(sgNotasConhecimento.Cells[cnDataEmissao, i]);
    vNotas[i - 1].PesoNota         := SFormatDouble(sgNotasConhecimento.Cells[cnPeso, i]);
    vNotas[i - 1].QuantidadeNota   := SFormatDouble(sgNotasConhecimento.Cells[cnQuantidade, i]);
    vNotas[i - 1].ValorTotalNota   := SFormatDouble(sgNotasConhecimento.Cells[cnValorNota, i]);

    if rgTipoRateioConhecimento.GetValor = 'V' then
      vNotas[i - 1].Indice := vNotas[i - 1].ValorTotalNota / SFormatDouble(stValorTotal.Caption)
    else if rgTipoRateioConhecimento.GetValor = 'P' then
      vNotas[i - 1].Indice := vNotas[i - 1].PesoNota / SFormatDouble(stTotalPeso.Caption)
    else
      vNotas[i - 1].Indice := vNotas[i - 1].QuantidadeNota / SFormatDouble(stTotalQtde.Caption);
  end;

  vRetBanco :=
    _ConhecimentosFretes.AtualizarConhecimento(
      Sessao.getConexaoBanco,
      eID.AsInt,
      FrEmpresa.getEmpresa().EmpresaId,
      FrTransportadora.GetDado().CadastroId,
      eNumeroConhecimento.AsInt,
      cbModeloConhecimento.GetValor,
      cbSerieConhecimento.GetValor,
      eBaseCalculoICMS.AsDouble,
      ePercICMS.AsDouble,
      eValorICMS.AsDouble,
      eValorFrete.AsDouble,
      eChaveConhecimento.Text,
      FrCFOP.GetCFOP().CfopPesquisaId,
      eDataEmissao.AsData,
      eDataContabil.AsData,
      0,
      rgTipoRateioConhecimento.GetValor,
      vNotas,
      FrDadosCobranca.Titulos
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormConhecimentosFretes.Modo(pEditando: Boolean);
begin
  inherited;

  // Se a entrada estiver sendo cancelada
  if (pEditando = False) and (FPreEntradaId > -1) then begin
    FEditandoHeranca := False;
    Close;
    Exit;
  end;

  _Biblioteca.Habilitar([
    FrTransportadora,
    FrEmpresa,
    eNumeroConhecimento,
    cbModeloConhecimento,
    cbSerieConhecimento,
    FrFornecedor,
    eNumeroNotaFiscal,
    cbModeloNota,
    cbSerieNota,
    eDataEmissaoNota,
    ePesoNota,
    eQtdeNota,
    eValorNota,
    sgNotasConhecimento,
    stQtdeNotas,
    stTotalPeso,
    stTotalQtde,
    stValorTotal,
    eBaseCalculoICMS,
    ePercICMS,
    eValorICMS,
    eValorFrete,
    eChaveConhecimento,
    FrCFOP,
    eDataEmissao,
    eDataContabil,
    FrDadosCobranca,
    rgTipoRateioConhecimento],
    pEditando
  );

  if pEditando then begin
    rgTipoRateioConhecimento.SetIndicePorValor('V');
    SetarFoco(FrTransportadora);
  end;
end;

procedure TFormConhecimentosFretes.PesquisarRegistro;
var
  vRetTela: TRetornoTelaFinalizar<RecConhecimentosFretes>;
begin
  vRetTela := PesquisaConhecimentosFretes.Pesquisar;
  if vRetTela.BuscaCancelada then
    Exit;

  inherited;

  PreencherRegistro(
    vRetTela.Dados,
    _ConhecimentosFretesItens.BuscarItens(Sessao.getConexaoBanco, 0, [vRetTela.Dados.ConhecimentoId]),
    _ConhecFretesFinanceiros.BuscarItens(Sessao.getConexaoBanco, 0, [vRetTela.Dados.ConhecimentoId])
  );
end;

procedure TFormConhecimentosFretes.PreencherRegistro(
  pConhecimento: RecConhecimentosFretes;
  pNotas: TArray<RecConhecimentosFretesItens>;
  pFinanceiros: TArray<RecTitulosFinanceiros>
);
var
  i: Integer;
begin
  if _EntradasNotasFiscais.ExisteEntradaConhecimentoVinculado(Sessao.getConexaoBanco, pConhecimento.ConhecimentoId) then begin
    _Biblioteca.Exclamar('N�o � permitido editar um conhecimento de frete que j� foi utilizado em entradas de notas fiscais, verifique!');
    Modo(False);
    Abort;
  end;

  eID.AsInt := pConhecimento.ConhecimentoId;

  FrTransportadora.InserirDadoPorChave(pConhecimento.TransportadoraId);
  eNumeroConhecimento.AsInt := pConhecimento.Numero;
  cbModeloConhecimento.SetIndicePorValor(pConhecimento.Modelo);
  cbSerieConhecimento.SetIndicePorValor(pConhecimento.Serie);

  FrEmpresa.InserirDadoPorChave(pConhecimento.EmpresaId);

  eBaseCalculoICMS.AsDouble := pConhecimento.BaseCalculoIcms;
  ePercICMS.AsDouble        := pConhecimento.PercentualIcms;
  eValorICMS.AsDouble       := pConhecimento.ValorIcms;
  eValorFrete.AsDouble      := pConhecimento.ValorFrete;

  FrCFOP.InserirDadoPorChave(pConhecimento.CfopId);

  eDataEmissao.AsData := pConhecimento.DataEmissao;
  eDataContabil.AsData := pConhecimento.DataContabil;

  eChaveConhecimento.Text := pConhecimento.ChaveConhecimento;

  for i := Low(pNotas) to High(pNotas) do begin
    AddGridNotas(
      pNotas[i].FornecedorNotaId,
      pNotas[i].NomeFornecedor,
      pNotas[i].NumeroNota,
      pNotas[i].ModeloNota,
      pNotas[i].SerieNota,
      pNotas[i].DataEmissaoNota,
      pNotas[i].PesoNota,
      pNotas[i].QuantidadeNota,
      pNotas[i].ValorTotalNota
    );
  end;

  FrDadosCobranca.Titulos := pFinanceiros;
  AtualizarTotais;
end;

procedure TFormConhecimentosFretes.sgNotasConhecimentoDblClick(Sender: TObject);
begin
  inherited;
  if sgNotasConhecimento.Cells[cnFornecedorId, sgNotasConhecimento.Row] = '' then
    Exit;

  FrFornecedor.InserirDadoPorChave( SFormatInt(sgNotasConhecimento.Cells[cnFornecedorId, sgNotasConhecimento.Row]) );
  eNumeroNotaFiscal.AsInt := SFormatInt(sgNotasConhecimento.Cells[cnNumeroNF, sgNotasConhecimento.Row]);
  cbModeloNota.SetIndicePorValor( sgNotasConhecimento.Cells[cnModelo, sgNotasConhecimento.Row] );
  cbSerieNota.SetIndicePorValor( sgNotasConhecimento.Cells[cnSerie, sgNotasConhecimento.Row] );
  eDataEmissaoNota.AsData := ToData(sgNotasConhecimento.Cells[cnDataEmissao, sgNotasConhecimento.Row]);
  ePesoNota.AsDouble := SFormatDouble(sgNotasConhecimento.Cells[cnPeso, sgNotasConhecimento.Row]);
  eQtdeNota.AsDouble := SFormatDouble(sgNotasConhecimento.Cells[cnQuantidade, sgNotasConhecimento.Row]);
  eValorNota.AsDouble := SFormatDouble(sgNotasConhecimento.Cells[cnValorNota, sgNotasConhecimento.Row]);
end;

procedure TFormConhecimentosFretes.sgNotasConhecimentoDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    cnNumeroNF,
    cnModelo,
    cnSerie,
    cnPeso,
    cnQuantidade,
    cnValorNota]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgNotasConhecimento.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormConhecimentosFretes.sgNotasConhecimentoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then begin
    sgNotasConhecimento.DeleteRow(sgNotasConhecimento.Row);
    AtualizarTotais;
  end;
end;

procedure TFormConhecimentosFretes.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrTransportadora.EstaVazio then begin
    _Biblioteca.Exclamar('A transportadora n�o foi informada corretamente, verifique!');
    SetarFoco(FrTransportadora);
    Abort;
  end;

  if eNumeroConhecimento.AsInt = 0 then begin
    _Biblioteca.Exclamar('O n�mero do conhecimento de frete n�o foi informado corretamente, verifique!');
    SetarFoco(eNumeroConhecimento);
    Abort;
  end;

  if cbModeloConhecimento.ItemIndex < 0 then begin
    _Biblioteca.Exclamar('O modelo do conhecimento de frete n�o foi informado corretamente, verifique!');
    SetarFoco(cbModeloConhecimento);
    Abort;
  end;

  if cbSerieConhecimento.ItemIndex < 0 then begin
    _Biblioteca.Exclamar('A s�rie do conhecimento de frete n�o foi informado corretamente, verifique!');
    SetarFoco(cbSerieConhecimento);
    Abort;
  end;

  if stQtdeNotas.Caption = '' then begin
    _Biblioteca.Exclamar('Nenhuma nota fiscal foi lan�ada, verifique!');
    SetarFoco(FrFornecedor);
    Abort;
  end;

  if FrEmpresa.EstaVazio then begin
    _Biblioteca.Exclamar('A empresa do conhecimento de frete n�o foi informada corretamente, verifique!');
    SetarFoco(FrEmpresa);
    Abort;
  end;

  if eValorFrete.AsCurr = 0 then begin
    _Biblioteca.Exclamar('O valor do conhecimento de frete n�o foi informado corretamente, verifique!');
    SetarFoco(eValorFrete);
    Abort;
  end;

  if FrCFOP.EstaVazio then begin
    _Biblioteca.Exclamar('O CFOP do conhecimento de frete n�o foi informado corretamente, verifique!');
    SetarFoco(FrCFOP);
    Abort;
  end;

  if not eDataEmissao.DataOk then begin
    _Biblioteca.Exclamar('A data de emiss�o do conhecimento de frete n�o foi informado corretamente, verifique!');
    SetarFoco(eDataEmissao);
    Abort;
  end;

  if not eDataContabil.DataOk then begin
    _Biblioteca.Exclamar('A data cont�bil do conhecimento de frete n�o foi informado corretamente, verifique!');
    SetarFoco(eDataContabil);
    Abort;
  end;

  if FrDadosCobranca.ExisteDiferenca then begin
    _Biblioteca.Exclamar('Os t�tulos do conhecimento de frete n�o foi informado corretamente, verifique!');
    SetarFoco(FrDadosCobranca);
    Abort;
  end;
end;

end.
