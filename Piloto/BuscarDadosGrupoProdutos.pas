unit BuscarDadosGrupoProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  Frame.DepartamentosSecoesLinhas, _RecordsCadastros, _Biblioteca;

type
  TFormDadosGrupoProduto = class(TFormHerancaFinalizar)
    FrLinhaProduto: TFrameDepartamentosSecoesLinhas;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosGrupoProdutoss: TRetornoTelaFinalizar<RecProdutoDeptoSecaoLinha>;

implementation

{$R *.dfm}

{ BuscarDadosGrupoProdutos }

function BuscarDadosGrupoProdutoss: TRetornoTelaFinalizar<RecProdutoDeptoSecaoLinha>;
var
  vForm: TFormDadosGrupoProduto;
begin
  vForm := TFormDadosGrupoProduto.Create(Application);

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FrLinhaProduto.GetProdutoDeptoSecaoLinha(0);

//  FreeAndNil(vForm);
end;

end.
