unit VinculacaoCartaoesReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, System.Win.ComObj,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _Biblioteca,
  Frame.Diretorio, FrameDiretorioArquivo, Vcl.Grids, GridLuka, _Imagens, _Sessao,
  Vcl.StdCtrls, CheckBoxLuka, _ContasReceber, SpeedButtonLuka, Informacoes.Orcamento,
  InformacoesAcumulado, Informacoes.ContasReceberBaixa, _RecordsEspeciais;

type
  TFormVinculacaoCartaoesReceber = class(TFormHerancaRelatoriosPageControl)
    FrCaminhoArquivo: TFrCaminhoArquivo;
    sgCartoes: TGridLuka;
    sbGravar: TSpeedButtonLuka;
    procedure sgCartoesGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgCartoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbGravarClick(Sender: TObject);
    procedure sgCartoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgCartoesDblClick(Sender: TObject);
  private
    procedure identificarCartoes;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormVinculacaoCartaoesReceber }

const
  coIdentificado         = 0;
  coMovimentoId          = 1;
  coTipoMovimento        = 2;
  coCliente              = 3;
  coNSU                  = 4;
  coNumeroCartao         = 5;
  coRede                 = 6;
  coBandeira             = 7;
  coValorTransacao       = 8;
  coDataTransacao        = 9;
  coQtdeParcelas         = 10;

  (* Ocultas *)
  coOrigemSintetico  = 11;
  coCobrancaId       = 12;
  coDataCadastro     = 13;
  coValorCartao      = 14;
  coItemIdCartao     = 15;

procedure TFormVinculacaoCartaoesReceber.Carregar(Sender: TObject);
const
  cpNSU             = 1;
  cpNumeroCartao    = 2;
  cpRede            = 3;
  cpBandeira        = 4;
  cpValorTransacao  = 13;
  cpDataTransacao   = 14;
  cpStatus          = 16;
  cpQtdeParcelas    = 17;

var
  i: Integer;
  sheet: OleVariant;
  vPlanilha: OleVariant;
  vTemCartao: Boolean;

  function Valor( pValor: string ): Double;
  begin
    Result := StrToFloat( StringReplace(pValor, '.', ',', [rfReplaceAll]) ) ;
  end;

begin
  sgCartoes.ClearGrid();
  //Crio o objeto que gerencia o arquivo excel
  vPlanilha:= CreateOleObject('Excel.Application');

  //Abro o arquivo
  vPlanilha.WorkBooks.open(FrCaminhoArquivo.getCaminhoArquivo);

  //Pego a primeira vPlanilha do arquivo
  sheet := vPlanilha.WorkSheets[1];

  i := 2;
  vTemCartao := False;
  while Trim(sheet.Cells[i, coNSU]) <> '' do begin
    if sheet.cells[i, cpStatus].Value <> 'CONFIRMADA' then
      Continue;

    sgCartoes.Cells[coIdentificado, i - 1]   := charNaoSelecionado;
    sgCartoes.Cells[coNSU, i - 1]            := Trim(sheet.cells[i, cpNSU].Value);
    sgCartoes.Cells[coNumeroCartao, i - 1]   := Trim(sheet.cells[i, cpNumeroCartao].Value);
    sgCartoes.Cells[coRede, i - 1]           := sheet.cells[i, cpRede].Value;
    sgCartoes.Cells[coBandeira, i - 1]       := sheet.cells[i, cpBandeira].Value;
    sgCartoes.Cells[coValorTransacao, i - 1] := NFormatN(SFormatDouble(sheet.cells[i, cpValorTransacao].Value));
    sgCartoes.Cells[coDataTransacao, i - 1]  := sheet.cells[i, cpDataTransacao].Value;
    sgCartoes.Cells[coQtdeParcelas, i - 1]   := sheet.cells[i, cpQtdeParcelas].Value;

    vTemCartao := True;
    Inc(i);
  end;

  //Fecho a vPlanilha
  vPlanilha.WorkBooks.Close;

  sgCartoes.SetLinhasGridPorTamanhoVetor( i - 2 );

  if vTemCartao then
    identificarCartoes;

  SetarFoco(sgCartoes);
end;

procedure TFormVinculacaoCartaoesReceber.identificarCartoes;
var
  i: Integer;
  vDadosCartaoAltis: RecCartoesVinculacao;
begin
  for i := 1 to sgCartoes.RowCount -1 do begin

    vDadosCartaoAltis :=
      _ContasReceber.buscarContasReceberVincularCartao(
        Sessao.getConexaoBanco,
        ToData(sgCartoes.Cells[coDataTransacao, i]),
        SFormatDouble(sgCartoes.Cells[coValorTransacao, i]),
        SFormatInt(sgCartoes.Cells[coQtdeParcelas, i])
      );

    if vDadosCartaoAltis.ClienteId = -1 then
      Continue;

    sgCartoes.Cells[coIdentificado, i] := _Biblioteca.charSelecionado;

    sgCartoes.Cells[coMovimentoId, i] := NFormat(vDadosCartaoAltis.MovimentoId);
    sgCartoes.Cells[coCliente, i]     := getInformacao(vDadosCartaoAltis.ClienteId, vDadosCartaoAltis.NomeCliente);

    sgCartoes.Cells[coTipoMovimento, i]   := vDadosCartaoAltis.TipoMovimento;
    sgCartoes.Cells[coOrigemSintetico, i] := vDadosCartaoAltis.Origem;
    sgCartoes.Cells[coDataCadastro, i]    := DFormat(vDadosCartaoAltis.DataCadastro);
    sgCartoes.Cells[coCobrancaId, i]      := NFormat(vDadosCartaoAltis.CobrancaId);
    sgCartoes.Cells[coValorCartao, i]     := NFormat(vDadosCartaoAltis.ValorCartao);
    sgCartoes.Cells[coItemIdCartao, i]    := NFormat(vDadosCartaoAltis.ItemIdCartao);
  end;
end;

procedure TFormVinculacaoCartaoesReceber.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormVinculacaoCartaoesReceber.sbGravarClick(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vCartoes: TArray<RecCartoesVinculacao>;
begin
  inherited;

  vCartoes := nil;
  for i := 1 to sgCartoes.RowCount -1 do begin
    if sgCartoes.Cells[coIdentificado, i] = charNaoSelecionado then
      Continue;

    SetLength(vCartoes, Length(vCartoes) + 1);
    vCartoes[High(vCartoes)].Origem        := sgCartoes.Cells[coOrigemSintetico, i];
    vCartoes[High(vCartoes)].CobrancaId    := SFormatInt(sgCartoes.Cells[coCobrancaId, i]);
    vCartoes[High(vCartoes)].QtdeParcelas  := SFormatInt(sgCartoes.Cells[coQtdeParcelas, i]);
    vCartoes[High(vCartoes)].ItemIdCartao  := SFormatInt(sgCartoes.Cells[coItemIdCartao, i]);
    vCartoes[High(vCartoes)].MovimentoId   := SFormatInt(sgCartoes.Cells[coMovimentoId, i]);

    vCartoes[High(vCartoes)].Nsu          := sgCartoes.Cells[coNSU, i];
    vCartoes[High(vCartoes)].NumeroCartao := sgCartoes.Cells[coNumeroCartao, i];
  end;

  vRetBanco := _ContasReceber.AtualizarDadosCartoes(Sessao.getConexaoBanco, vCartoes);
  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RotinaSucesso;
end;

procedure TFormVinculacaoCartaoesReceber.sgCartoesDblClick(Sender: TObject);
begin
  inherited;
  if sgCartoes.Cells[coOrigemSintetico, sgCartoes.Row] = 'ORC' then
    Informacoes.Orcamento.Informar( SFormatInt(sgCartoes.Cells[coMovimentoId, sgCartoes.Row]) )
  else if sgCartoes.Cells[coOrigemSintetico, sgCartoes.Row] = 'BXR' then
    Informacoes.ContasReceberBaixa.Informar( SFormatInt(sgCartoes.Cells[coMovimentoId, sgCartoes.Row]) )
  else
    InformacoesAcumulado.Informar( SFormatInt(sgCartoes.Cells[coMovimentoId, sgCartoes.Row]) );
end;

procedure TFormVinculacaoCartaoesReceber.sgCartoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coMovimentoId, coNSU, coValorTransacao, coQtdeParcelas] then
    vAlinhamento := taRightJustify
  else if ACol in[coIdentificado, coTipoMovimento] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgCartoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormVinculacaoCartaoesReceber.sgCartoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coTipoMovimento then begin
    AFont.Style := [fsBold];
    AFont.Color := _Biblioteca.coCorFonteEdicao2;
  end;
end;

procedure TFormVinculacaoCartaoesReceber.sgCartoesGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgCartoes.Cells[coNSU, ARow] = '' then
    Exit;

  if ACol = coIdentificado then begin
    if sgCartoes.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgCartoes.Cells[ACol, ARow] = _Biblioteca.charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormVinculacaoCartaoesReceber.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
