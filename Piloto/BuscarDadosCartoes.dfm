inherited FormBuscarDadosCartoes: TFormBuscarDadosCartoes
  Caption = 'Buscar dados de cart'#245'es'
  ClientHeight = 370
  ClientWidth = 545
  ExplicitWidth = 551
  ExplicitHeight = 399
  PixelsPerInch = 96
  TextHeight = 14
  object st3: TStaticText [0]
    Left = 4
    Top = 1
    Width = 537
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Condi'#231#227'o de pagamento'
    Color = 38619
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 11
    Transparent = False
  end
  object stCondicaoPagamento: TStaticText [1]
    Left = 4
    Top = 16
    Width = 537
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 5921326
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    Transparent = False
  end
  inherited pnOpcoes: TPanel
    Top = 333
    Width = 545
    ExplicitTop = 333
    ExplicitWidth = 545
    inherited sbFinalizar: TSpeedButton
      Left = 0
      Width = 541
      Height = 33
      Align = alClient
      ExplicitLeft = 218
      ExplicitWidth = 541
      ExplicitHeight = 33
    end
  end
  object sgOpcoesCartoes: TGridLuka
    Left = 4
    Top = 49
    Width = 537
    Height = 102
    ColCount = 4
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goRowSelect]
    TabOrder = 1
    OnDblClick = sgOpcoesCartoesDblClick
    OnDrawCell = sgOpcoesCartoesDrawCell
    OnKeyDown = sgOpcoesCartoesKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'C'#243'digo'
      'Tipo de cobran'#231'a'
      'Tipo cart'#227'o'
      'Rede cart'#227'o')
    Grid3D = False
    RealColCount = 4
    AtivarPopUpSelecao = False
    ColWidths = (
      48
      205
      106
      119)
  end
  object sgCartoesEscolhidos: TGridLuka
    Left = 4
    Top = 169
    Width = 537
    Height = 131
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goRowSelect]
    TabOrder = 2
    OnDblClick = sgCartoesEscolhidosDblClick
    OnDrawCell = sgCartoesEscolhidosDrawCell
    OnKeyDown = sgCartoesEscolhidosKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'C'#243'digo'
      'Tipo de cobran'#231'a'
      'Valor'
      'Nr.cart'#227'o'
      'NSU')
    Grid3D = False
    RealColCount = 5
    AtivarPopUpSelecao = False
    ColWidths = (
      46
      229
      93
      79
      64)
  end
  object stCaracteristicas: TStaticText
    Left = 4
    Top = 153
    Width = 537
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = 'Cart'#245'es selecionados'
    Color = 15395562
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    Transparent = False
  end
  object stValorTotal: TStaticText
    Left = 4
    Top = 301
    Width = 179
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Total a ser pago'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    Transparent = False
  end
  object stValorTotalASerPago: TStaticText
    Left = 4
    Top = 316
    Width = 179
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    Transparent = False
  end
  object stTotalDefinido: TStaticText
    Left = 182
    Top = 301
    Width = 181
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Total definido'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
  end
  object stValorTotalDefinido: TStaticText
    Left = 182
    Top = 316
    Width = 181
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    Transparent = False
  end
  object st2: TStaticText
    Left = 362
    Top = 301
    Width = 179
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Diferen'#231'a'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 9
    Transparent = False
  end
  object stDiferenca: TStaticText
    Left = 362
    Top = 316
    Width = 179
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 4868863
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Transparent = False
  end
  object st1: TStaticText
    Left = 4
    Top = 34
    Width = 537
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = 'Cart'#245'es dispon'#237'veis'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
  end
end
