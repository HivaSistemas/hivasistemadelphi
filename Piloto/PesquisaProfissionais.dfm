inherited FormPesquisaProfissionais: TFormPesquisaProfissionais
  Caption = 'Pesquisa de profissionais'
  ClientHeight = 328
  ClientWidth = 622
  ExplicitWidth = 628
  ExplicitHeight = 357
  PixelsPerInch = 96
  TextHeight = 14
  inherited eValorPesquisa: TEditLuka
    Width = 417
    ExplicitWidth = 417
  end
  inherited sgPesquisa: TGridLuka
    Width = 617
    Height = 283
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Raz'#227'o Social'
      'Nome Fanstasia')
    RealColCount = 4
    ExplicitWidth = 617
    ExplicitHeight = 283
    ColWidths = (
      28
      55
      268
      251)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Left = 8
    Top = 298
    Text = '0'
    ExplicitLeft = 8
    ExplicitTop = 298
  end
end
