unit Relacao.Produtos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _RecordsCadastros,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, _FrameHenrancaPesquisas, _Sessao, _Biblioteca,
  Frame.DepartamentosSecoesLinhas, _FrameHerancaPrincipal, FrameDataInicialFinal, _Imagens,
  Vcl.StdCtrls, ComboBoxLuka, Vcl.Grids, GridLuka, _RecordsEspeciais, _Produtos, SelecionarVariosOpcoes,
  Vcl.Menus, AlterarOrdensLocaisEntregasProdutos, FrameProdutos, CheckBoxLuka,
  FrameMarcas, FrameFornecedores, FrameUnidades, BuscaDados,
  FrameGruposTributacoesEstadualCompras, FrameGruposTributacoesEstadualVenda,
  FrameGruposTributacoesFederalCompra, FrameGruposTributacoesFederalVenda, InformacoesProduto,
  EditLuka, FrameTextos, BuscarDadosTexto;

type
  TFormRelacaoProdutos = class(TFormHerancaRelatoriosPageControl)
    FrDataCadastroProduto: TFrDataInicialFinal;
    FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas;
    Label1: TLabel;
    cbTipoControleEstoque: TComboBoxLuka;
    sgItens: TGridLuka;
    cbAtivo: TComboBoxLuka;
    Label2: TLabel;
    pmOpcoes: TPopupMenu;
    miN1: TMenuItem;
    FrProdutos: TFrProdutos;
    cbAceitarEstoqueNegativo: TComboBoxLuka;
    Label3: TLabel;
    Label4: TLabel;
    cbInativarProdutoZerarEstoque: TComboBoxLuka;
    Label5: TLabel;
    cbBloqueadoCompra: TComboBoxLuka;
    Label6: TLabel;
    cbBloqueadoVenda: TComboBoxLuka;
    Label7: TLabel;
    cbPermitirDevolucao: TComboBoxLuka;
    cbSobEncomenda: TComboBoxLuka;
    Label9: TLabel;
    Label10: TLabel;
    cbBloquearEntradaSemPedidoCompra: TComboBoxLuka;
    cbExigirSeparacao: TComboBoxLuka;
    Label11: TLabel;
    Label12: TLabel;
    cbExigirModeloNotaFiscal: TComboBoxLuka;
    cbSepararSomenteComCodigoBarras: TComboBoxLuka;
    Label13: TLabel;
    FrMarca: TFrMarcas;
    FrFabricante: TFrFornecedores;
    FrUnidadeVenda: TFrUnidades;
    miBloqueadoCompras: TMenuItem;
    miSobEncomenda: TMenuItem;
    miBloquearEntradaSemPedidoCompra: TMenuItem;
    miExigirModeloNota: TMenuItem;
    Panel1: TPanel;
    miMarcarDesmarcarSelecionado: TSpeedButton;
    miMarcarDesmarcarTodos: TSpeedButton;
    miAlterarOrdemLocaisEntrega: TSpeedButton;
    miPermitirNaoPermitirEstoqueNegativo: TSpeedButton;
    miAtivarDesativar: TSpeedButton;
    miExigirSeparacao: TSpeedButton;
    miSepararSomenteCodigoBarras: TSpeedButton;
    miPermitirDevolucao: TSpeedButton;
    miBloqueadoVendas: TSpeedButton;
    sbDefinirMarcaProdutos: TSpeedButton;
    sbDefinirUnidadeVenda: TSpeedButton;
    miDefinirGrupoProdutos: TSpeedButton;
    miDefinirEnderecosEstoque: TSpeedButton;
    SpeedButton1: TSpeedButton;
    sbZerarCustoVenda: TSpeedButton;
    SpeedButton3: TSpeedButton;
    PopupMenu1: TPopupMenu;
    teste1: TMenuItem;
    No1: TMenuItem;
    frNCM: TFrTextos;
    frCest: TFrTextos;
    pnComissaoVista: TPanel;
    lb1: TLabel;
    lb2: TLabel;
    Label14: TLabel;
    cbComissaoVista: TComboBoxLuka;
    pnComissaoPrazo: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    cbComissaoPrazo: TComboBoxLuka;
    SpeedButton2: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    eComissaoVista1: TEditLuka;
    eComissaoVista2: TEditLuka;
    eComissaoPrazo1: TEditLuka;
    eComissaoPrazo2: TEditLuka;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    Label8: TLabel;
    cbEmPromocao: TComboBoxLuka;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    Limparunidadedeentrega1: TMenuItem;
    FrGrupoTributacoesEstadualVenda: TFrGruposTributacoesEstadualVenda;
    Label18: TLabel;
    cbAgrupamento: TComboBoxLuka;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure miMarcarDesmarcarSelecionadoClick(Sender: TObject);
    procedure miMarcarDesmarcarTodosClick(Sender: TObject);
    procedure sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure miAlterarOrdemLocaisEntregaClick(Sender: TObject);
    procedure miPermitirNaoPermitirEstoqueNegativoClick(Sender: TObject);
    procedure miAtivarDesativarClick(Sender: TObject);
    procedure miPermitirEstoqueNegativoClick(Sender: TObject);
    procedure miInativarProdutoZeraEstoqueClick(Sender: TObject);
    procedure miBloqueadoComprasClick(Sender: TObject);
    procedure miBloqueadoVendasClick(Sender: TObject);
    procedure miPermitirDevolucaoClick(Sender: TObject);
    procedure miProdutoPDVClick(Sender: TObject);
    procedure miSobEncomendaClick(Sender: TObject);
    procedure miBloquearEntradaSemPedidoCompraClick(Sender: TObject);
    procedure miExigirSeparacaoClick(Sender: TObject);
    procedure miExigirModeloNotaClick(Sender: TObject);
    procedure miSepararSomenteCodigoBarrasClick(Sender: TObject);
    procedure sgItensDblClick(Sender: TObject);
    procedure sbDefinirMarcaProdutosClick(Sender: TObject);
    procedure sbDefinirUnidadeVendaClick(Sender: TObject);
    procedure miDefinirGrupoProdutosClick(Sender: TObject);
    procedure miDefinirEnderecosEstoqueClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sbImprimirClick(Sender: TObject);
    procedure sbZerarCustoVendaClick(Sender: TObject);
    procedure teste1Click(Sender: TObject);
    procedure No1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure cbComissaoVistaChange(Sender: TObject);
    procedure cbComissaoPrazoChange(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure Limparunidadedeentrega1Click(Sender: TObject);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    { Private declarations }
  protected
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses BuscarDadosMarcaProduto, BuscarDadosUnidadeProduto,
  BuscarDadosGrupoProdutos, BuscarDadosEnderecoEstoque, BuscarPercentual,
  _ProdutosGruposTribEmpresas, BuscarDadosTributacaoProdutos;

{ TFormRelacaoProdutos }

const
  coSelecionado         = 0;
  coProdutoId           = 1;
  coNome                = 2;
  coNomeCompra          = 3;
  coMarca               = 4;
  coMultiploVenda       = 5;
  coUnidadeVenda        = 6;
  coAtivo               = 7;
  coTipoControleEstoque = 8;
  coNCM                 = 9;
  coCest                = 10;
  coCodigoBarras        = 11;
  coCodigoOriginal      = 12;
  coPeso                = 13;
  coComissaoVista       = 14;
  coComissaoPrazo       = 15;

  coTipoLinha           = 16;

  // Tipos de linhas
  coLinhaDetalhe   = 'D';
  coLinhaCabAgrup  = 'CAGRU';
//  coLinhaTotVenda  = 'TV';
//  coTotalAgrupador = 'TAGRU';

procedure TFormRelacaoProdutos.Carregar(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vSql: string;
  vProdutos: TArray<RecProdutos>;
  vProdutosAux: TArray<RecProdutos>;
  vSqlPromocao: string;
  vProdutoIds: TArray<Integer>;
  vProdutoIdsSql: string;
  vAddProduto: Boolean;
  vSQLTributacao: string;
  vLinha: Integer;
  vAgrupadorId: string;
begin
  vSql := '';
  sgItens.ClearGrid();

  vSQLTributacao := '';
  if not FrGrupoTributacoesEstadualVenda.EstaVazio then begin
    vSQLTributacao :=
      ' and GTE.GRUPO_TRIB_ESTADUAL_VENDA_ID = ''' + IntToStr(FrGrupoTributacoesEstadualVenda.getGrupo.GrupoTribEstadualVendaId) + ''' ';
  end;

  vSql :=
    '  where GTE.EMPRESA_ID = ''' + IntToStr(Sessao.getEmpresaLogada.EmpresaId) + ''' ' +
    '  and GTE.ESTADO_ID_VENDA = ''' + Sessao.getEmpresaLogada.EstadoId + ''' ' +
    '  and GTE.ESTADO_ID_COMPRA = ''' + Sessao.getEmpresaLogada.EstadoId + ''' ';

  if not FrProdutos.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrProdutos.getSqlFiltros('PRO.PRODUTO_ID'));

  if not FrDataCadastroProduto.NenhumaDataValida then
    _Biblioteca.WhereOuAnd(vSql, FrDataCadastroProduto.getSqlFiltros('PRO.DATA_CADASTRO'));

  if not FrMarca.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrMarca.getSqlFiltros('PRO.MARCA_ID'));

  if not FrDepartamentosSecoesLinhas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrDepartamentosSecoesLinhas.getSqlFiltros('PRO.LINHA_PRODUTO_ID'));

  if not FrFabricante.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrFabricante.getSqlFiltros('PRO.FORNECEDOR_ID'));

  if not FrUnidadeVenda.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrUnidadeVenda.getSqlFiltros('PRO.UNIDADE_VENDA'));

  if not cbTipoControleEstoque.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.TIPO_CONTROLE_ESTOQUE = ''' + cbTipoControleEstoque.GetValor + '''');

  if not cbAtivo.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.ATIVO = ''' + cbAtivo.GetValor + '''');

  if not cbAceitarEstoqueNegativo.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.ACEITAR_ESTOQUE_NEGATIVO = ''' + cbAceitarEstoqueNegativo.GetValor + '''');

  if not cbInativarProdutoZerarEstoque.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.INATIVAR_ZERAR_ESTOQUE = ''' + cbInativarProdutoZerarEstoque.GetValor + '''');

  if not cbBloqueadoCompra.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.BLOQUEADO_COMPRAS = ''' + cbBloqueadoCompra.GetValor + '''');

  if not cbBloqueadoVenda.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.BLOQUEADO_VENDAS = ''' + cbBloqueadoVenda.GetValor + '''');

  if not cbPermitirDevolucao.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.PERMITIR_DEVOLUCAO = ''' + cbPermitirDevolucao.GetValor + '''');

  if not cbPermitirDevolucao.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.PERMITIR_DEVOLUCAO = ''' + cbPermitirDevolucao.GetValor + '''');

 // if not cbProdutosDiversosPDV.NaoFiltrar then
  //  _Biblioteca.WhereOuAnd(vSql, ' PRO.PRODUTO_DIVERSOS_PDV = ''' + cbProdutosDiversosPDV.GetValor + '''');

  if not cbSobEncomenda.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.SOB_ENCOMENDA = ''' + cbSobEncomenda.GetValor + '''');

  if not cbBloquearEntradaSemPedidoCompra.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.BLOQUEAR_ENTRAD_SEM_PED_COMPRA = ''' + cbBloquearEntradaSemPedidoCompra.GetValor + '''');

  if not cbExigirSeparacao.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.EXIGIR_SEPARACAO = ''' + cbExigirSeparacao.GetValor + '''');

  if not cbExigirModeloNotaFiscal.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.EXIGIR_MODELO_NOTA_FISCAL = ''' + cbExigirModeloNotaFiscal.GetValor + '''');

  if not cbSepararSomenteComCodigoBarras.NaoFiltrar then
    _Biblioteca.WhereOuAnd(vSql, ' PRO.EXIGIR_MODELO_NOTA_FISCAL = ''' + cbSepararSomenteComCodigoBarras.GetValor + '''');

  if not frNCM.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, frNCM.TrazerFiltros('PRO.CODIGO_NCM'));

  if not frCest.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, frCest.TrazerFiltros('PRO.CEST'));

  if cbComissaoVista.GetValor <> 'NaoFiltrar' then begin
    if cbComissaoVista.GetValor = 'EN' then begin
      vSql := vSql + ' and ' + 'PRO.PERCENTUAL_COMISSAO_VISTA' + ' > ' + NFormatOracle(eComissaoVista1.asDouble) + ' and PRO.PERCENTUAL_COMISSAO_VISTA < ' + NFormatOracle(eComissaoVista2.asDouble) + ' ';
    end
    else if cbComissaoVista.GetValor = 'MA' then begin
      vSql := vSql + ' and ' + 'PRO.PERCENTUAL_COMISSAO_VISTA' + ' > ' + NFormatOracle(eComissaoVista1.asDouble) + ' ';
    end
    else if cbComissaoVista.GetValor = 'ME' then begin
      vSql := vSql + ' and ' + 'PRO.PERCENTUAL_COMISSAO_VISTA' + ' < ' + NFormatOracle(eComissaoVista1.asDouble) + ' ';
    end;
  end;

  if cbComissaoPrazo.GetValor <> 'NaoFiltrar' then begin
    if cbComissaoPrazo.GetValor = 'EN' then begin
      vSql := vSql + ' and ' + 'PRO.PERCENTUAL_COMISSAO_PRAZO' + ' > ' + NFormatOracle(eComissaoPrazo1.asDouble) + ' and PRO.PERCENTUAL_COMISSAO_PRAZO < ' + NFormatOracle(eComissaoPrazo2.asDouble) + ' ';
    end
    else if cbComissaoPrazo.GetValor = 'MA' then begin
      vSql := vSql + ' and ' + 'PRO.PERCENTUAL_COMISSAO_PRAZO' + ' > ' + NFormatOracle(eComissaoPrazo1.asDouble) + ' ';
    end
    else if cbComissaoPrazo.GetValor = 'ME' then begin
      vSql := vSql + ' and ' + 'PRO.PERCENTUAL_COMISSAO_PRAZO' + ' < ' + NFormatOracle(eComissaoPrazo1.asDouble) + ' ';
    end;
  end;

  if cbEmPromocao.GetValor = 'S' then begin
    vSql := vSql + ' and PRO.PRODUTO_ID IN (select PRODUTO_ID from PRODUTOS_PROMOCOES where PRECO_PROMOCIONAL > 0 AND EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId) + ' )';
  end
  else if cbEmPromocao.GetValor = 'N' then begin
    vSql := vSql + ' and PRO.PRODUTO_ID NOT IN (select PRODUTO_ID from PRODUTOS_PROMOCOES where PRECO_PROMOCIONAL > 0 AND EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId) + ' )';
  end;

  if cbAgrupamento.GetValor <> 'NEN' then begin
    if cbAgrupamento.GetValor = 'NCM' then
      vSql := vSql + vSQLTributacao + ' order by PRO.CODIGO_NCM asc, PRO.NOME asc '
    else if cbAgrupamento.GetValor = 'MAR' then
      vSql := vSql + vSQLTributacao + ' order by MAR.NOME asc, PRO.NOME asc '
    else if cbAgrupamento.GetValor = 'TRI' then
      vSql := vSql + vSQLTributacao + ' order by TRE.GRUPO_TRIB_ESTADUAL_VENDA_ID asc, PRO.NOME asc '

  end
  else
    vSql := vSql + vSQLTributacao + ' order by PRO.NOME';

  vProdutos := _Produtos.BuscarProdutosRelacaoProdutos(Sessao.getConexaoBanco, vSql);
  if vProdutos = nil then begin
    NenhumRegistro;
    Exit;
  end;

//  if cbEmPromocao.GetValor <> 'NaoFiltrar' then begin
//    vProdutoIdsSql := ' (';
//    for i := Low(vProdutos) to High(vProdutos) do begin
//      if i > 0 then
//        vProdutoIdsSql := vProdutoIdsSql + ', ';
//      vProdutoIdsSql := vProdutoIdsSql +  IntToStr(vProdutos[i].produto_id);
//    end;
//
//    vProdutoIdsSql := vProdutoIdsSql + ')';
//
//    vSqlPromocao := ' where PRODUTO_ID in ' + vProdutoIdsSql;
//    vSqlPromocao := vSqlPromocao + ' and EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId);
//
//    if cbEmPromocao.GetValor = 'S' then
//      vSqlPromocao := vSqlPromocao + ' and PRECO_PROMOCIONAL > 0';
//
//    vProdutoIds := _Produtos.BuscarProdutosPromocao(Sessao.getConexaoBanco, vSqlPromocao);
//
//    if vProdutoIds = nil then begin
//      NenhumRegistro;
//      Exit;
//    end;
//
//    for i := Low(vProdutos) to High(vProdutos) do begin
//      if cbEmPromocao.GetValor = 'S' then
//        vAddProduto := False
//      else
//        vAddProduto := True;
//
//      for j := Low(vProdutoIds) to High(vProdutoIds) do begin
//        if cbEmPromocao.GetValor = 'S' then begin
//          if vProdutos[i].produto_id = vProdutoIds[j] then begin
//            vAddProduto := True;
//            Break;
//          end;
//        end;
//
//        if cbEmPromocao.GetValor = 'N' then begin
//          if vProdutos[i].produto_id = vProdutoIds[j] then begin
//            vAddProduto := False;
//            Break;
//          end;
//        end;
//      end;
//
//      if vAddProduto then begin
//        SetLength(vProdutosAux, Length(vProdutosAux) + 1);
//        vProdutosAux[High(vProdutosAux)] := vProdutos[i];
//      end;
//    end;
//
//    if vProdutosAux = nil then begin
//      NenhumRegistro;
//      Exit;
//    end;
//
//    vProdutos := nil;
//    vProdutos := vProdutosAux;
//  end;

  vLinha := 0;
  vAgrupadorId := '';
  for i := Low(vProdutos) to High(vProdutos) do begin
    if cbAgrupamento.GetValor = 'NCM' then begin
      if vAgrupadorId <> vProdutos[i].codigo_ncm then begin
        Inc(vLinha);

        vAgrupadorId                         := vProdutos[i].codigo_ncm;
        sgItens.Cells[coNome, vLinha]        := 'NCM: ' + vProdutos[i].codigo_ncm;
        sgItens.Cells[coTipoLinha, vLinha]   := coLinhaCabAgrup;
        sgItens.Cells[coSelecionado, vLinha] := charNaoPermiteSelecionar;
      end;
    end
    else if cbAgrupamento.GetValor = 'MAR' then begin
      if vAgrupadorId <> IntToStr(vProdutos[i].marca_id) then begin
        Inc(vLinha);

        vAgrupadorId                          := IntToStr(vProdutos[i].marca_id);
        sgItens.Cells[coNome, vLinha]         := 'Marca: ' + vProdutos[i].nome_marca;
        sgItens.Cells[coTipoLinha, vLinha]    := coLinhaCabAgrup;
        sgItens.Cells[coSelecionado, vLinha]  := charNaoPermiteSelecionar;
      end;
    end
    else if cbAgrupamento.GetValor = 'TRI' then begin
      if vAgrupadorId <> IntToStr(vProdutos[i].GrupoTribEstadualVendaId) then begin
        Inc(vLinha);

        vAgrupadorId                          := IntToStr(vProdutos[i].GrupoTribEstadualVendaId);
        sgItens.Cells[coNome, vLinha]         := 'Grupo trib.: ' + vProdutos[i].GrupoTribEstadualVendaNome;
        sgItens.Cells[coTipoLinha, vLinha]    := coLinhaCabAgrup;
        sgItens.Cells[coSelecionado, vLinha]  := charNaoPermiteSelecionar;
      end;
    end
    else if cbAgrupamento.GetValor = 'GPR' then begin
      if vAgrupadorId <> vProdutos[i].LinhaProdutoId then begin
        Inc(vLinha);

        vAgrupadorId                          := vProdutos[i].LinhaProdutoId;
        sgItens.Cells[coNome, vLinha]         := 'Gr. prod.: ' + vProdutos[i].LinhaProdutoId + ' - ' + vProdutos[i].LinhaProdutoNome;
        sgItens.Cells[coTipoLinha, vLinha]    := coLinhaCabAgrup;
        sgItens.Cells[coSelecionado, vLinha]  := charNaoPermiteSelecionar;
      end;
    end;

    Inc(vLinha);
    sgItens.Cells[coSelecionado, vLinha]         := charNaoSelecionado;
    sgItens.Cells[coProdutoId, vLinha]           := _Biblioteca.NFormat(vProdutos[i].produto_id);
    sgItens.Cells[coNome, vLinha]                := vProdutos[i].nome;
    sgItens.Cells[coNomeCompra, vLinha]          := vProdutos[i].nome_compra;
    sgItens.Cells[coMarca, vLinha]               := _Biblioteca.NFormat(vProdutos[i].marca_id) + ' - ' + vProdutos[i].nome_marca;
    sgItens.Cells[coMultiploVenda, vLinha]       := _Biblioteca.NFormatN(vProdutos[i].multiplo_venda, 3);
    sgItens.Cells[coUnidadeVenda, vLinha]        := vProdutos[i].unidade_venda;
    sgItens.Cells[coAtivo, vLinha]               := vProdutos[i].ativo;
    sgItens.Cells[coTipoControleEstoque, vLinha] := vProdutos[i].TipoControleEstoque;
    sgItens.Cells[coNCM, vLinha]                 := vProdutos[i].codigo_ncm;
    sgItens.Cells[coCest, vLinha]                := vProdutos[i].cest;
    sgItens.Cells[coCodigoBarras, vLinha]        := vProdutos[i].codigo_barras;
    sgItens.Cells[coCodigoOriginal, vLinha]      := '';
    sgItens.Cells[coPeso, vLinha]                := _Biblioteca.NFormatN(vProdutos[i].peso, 3);
    sgItens.Cells[coComissaoVista, vLinha]       := _Biblioteca.NFormatN(vProdutos[i].PercentualComissaoVista, 2);
    sgItens.Cells[coComissaoPrazo, vLinha]       := _Biblioteca.NFormatN(vProdutos[i].PercentualComissaoPrazo, 2);

    sgItens.Cells[coTipoLinha, vLinha]           := coLinhaDetalhe;
  end;
  sgItens.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor(vLinha);

  SetarFoco(sgItens);
end;

procedure TFormRelacaoProdutos.cbComissaoPrazoChange(Sender: TObject);
begin
  inherited;
  pnComissaoPrazo.Visible := cbComissaoPrazo.GetValor <> 'NaoFiltrar';

  if cbComissaoPrazo.GetValor = 'EN' then begin
    Label15.Caption := 'Valor entre';
    eComissaoPrazo2.Visible := True;
    Label16.Visible := True;
  end
  else if cbComissaoPrazo.GetValor = 'MA' then begin
    Label15.Caption := 'Valor maior que';
    eComissaoPrazo2.Visible := False;
    Label16.Visible := False;
  end
  else if cbComissaoPrazo.GetValor = 'ME' then begin
    Label15.Caption := 'Valor menor que';
    eComissaoPrazo2.Visible := False;
    Label16.Visible := False;
  end;
end;

procedure TFormRelacaoProdutos.cbComissaoVistaChange(Sender: TObject);
begin
  inherited;
  pnComissaoVista.Visible := cbComissaoVista.GetValor <> 'NaoFiltrar';

  if cbComissaoVista.GetValor = 'EN' then begin
    lb1.Caption := 'Valor entre';
    eComissaoVista2.Visible := True;
    lb2.Visible := True;
  end
  else if cbComissaoVista.GetValor = 'MA' then begin
    lb1.Caption := 'Valor maior que';
    eComissaoVista2.Visible := False;
    lb2.Visible := False;
  end
  else if cbComissaoVista.GetValor = 'ME' then begin
    lb1.Caption := 'Valor menor que';
    eComissaoVista2.Visible := False;
    lb2.Visible := False;
  end;
end;

procedure TFormRelacaoProdutos.FormCreate(Sender: TObject);
begin
  inherited;
  miDefinirEnderecosEstoque.Visible := Sessao.getParametros.UtilEnderecoEstoque = 'S';
end;

procedure TFormRelacaoProdutos.FormShow(Sender: TObject);
begin
  inherited;
  cbTipoControleEstoque.SetIndicePorValor(_Biblioteca.cbNaoFiltrar);
  cbAtivo.SetIndicePorValor(_Biblioteca.cbNaoFiltrar);
  SetarFoco(FrProdutos);
end;

procedure TFormRelacaoProdutos.Limparunidadedeentrega1Click(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecUnidades>;
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetBanco := _Produtos.AtualizarCampoProduto( Sessao.getConexaoBanco, '', vProdutosIds, 'UNIDADE_ENTREGA_ID');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.MenuItem1Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
  vRetornoTela: TRetornoTelaFinalizar<string>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetornoTela := BuscarDadosTexto.BuscarDadosString('NCM', 'NCM', 8);
  if vRetornoTela.BuscaCancelada then
    Exit;

  vRetBanco := _Produtos.Atualizarncm(Sessao.getConexaoBanco, vProdutosIds, vRetornoTela.Dados);
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.MenuItem2Click(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecUnidades>;
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetorno := BuscarDadosUnidadeProduto.BuscarDadosUnidadeProdutos;
  if vRetorno.BuscaCancelada then
    Exit;

  vRetBanco := _Produtos.AtualizarCampoProduto( Sessao.getConexaoBanco, vRetorno.Dados.unidade_id, vProdutosIds, 'UNIDADE_ENTREGA_ID');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.miAlterarOrdemLocaisEntregaClick(Sender: TObject);
var
  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;
  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt(sgItens.Cells[coProdutoId, i]) );
  end;

  if vProdutosIds = nil then begin
    NenhumRegistroSelecionado;
    Exit;
  end;

  if AlterarOrdensLocaisEntregasProdutos.Alterar(vProdutosIds).BuscaCancelada then
    Exit;

  RotinaSucesso;
end;

procedure TFormRelacaoProdutos.miAtivarDesativarClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vMotivoInatividade: string;
  vProdutosIds: TArray<Integer>;
begin
  inherited;
  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Ativar/desativar?', ['Ativar', 'Desativar'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  if vOpcao = 1 then begin
    vMotivoInatividade := BuscaDados.BuscarDados('Motivo da inatividade', '', 800);

    if vMotivoInatividade = '' then begin
      Exclamar('� necess�rio informar o motivo da inatividade para desativar o produto!');
      Abort;
    end;
  end;

  vRetBanco := _Produtos.AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'ATIVO', vMotivoInatividade);
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.miBloqueadoComprasClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Bloquear para compras?', ['Sim', 'N�o'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  vRetBanco := AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'BLOQUEADO_COMPRAS');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;


procedure TFormRelacaoProdutos.miBloqueadoVendasClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Bloquear para vendas?', ['Sim', 'N�o'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  vRetBanco := AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'ACEITAR_ESTOQUE_NEGATIVO');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;


procedure TFormRelacaoProdutos.miBloquearEntradaSemPedidoCompraClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Bloquear entrada sem ped. de compra?', ['Sim', 'N�o'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  vRetBanco := AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'BLOQUEAR_ENTRAD_SEM_PED_COMPRA');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.miDefinirEnderecosEstoqueClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<TArray<Integer>>;
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetorno := BuscarDadosEnderecoEstoque.BuscarDadosEnderecoEstoqueProdutos;
  if vRetorno.BuscaCancelada then
    Exit;

  vRetBanco := _Produtos.AtualizarEnderecosEstoqueProdutos(Sessao.getConexaoBanco, vRetorno.Dados, vProdutosIds);
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.miDefinirGrupoProdutosClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecProdutoDeptoSecaoLinha>;
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetorno := BuscarDadosGrupoProdutos.BuscarDadosGrupoProdutoss;
  if vRetorno.BuscaCancelada then
    Exit;

  vRetBanco := _Produtos.AtualizarCampoProduto( Sessao.getConexaoBanco, vRetorno.Dados.DeptoSecaoLinhaId, vProdutosIds, 'LINHA_PRODUTO_ID');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.miExigirModeloNotaClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Exigir modelo nota fiscal?', ['Sim', 'N�o'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  vRetBanco := AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'EXIGIR_MODELO_NOTA_FISCAL');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.miExigirSeparacaoClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Exigir separa��o?', ['Sim', 'N�o'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  vRetBanco := AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'EXIGIR_SEPARACAO');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.miInativarProdutoZeraEstoqueClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Inativar produto ao zerar estoque', ['Sim', 'N�o'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  vRetBanco := _Produtos.AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'INATIVAR_ZERAR_ESTOQUE');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;


procedure TFormRelacaoProdutos.miMarcarDesmarcarSelecionadoClick(Sender: TObject);
begin
  inherited;
  if sgItens.Cells[coProdutoId, sgItens.Row] = '' then
    Exit;

  sgItens.Cells[coSelecionado, sgItens.Row] := IIfStr( sgItens.Cells[coSelecionado, sgItens.Row] = charNaoSelecionado, charSelecionado, charNaoSelecionado );
end;

procedure TFormRelacaoProdutos.miMarcarDesmarcarTodosClick(Sender: TObject);
var
  i: Integer;
  vMarcar: Boolean;
begin
  inherited;
  if sgItens.Cells[coProdutoId, sgItens.Row] = '' then
    Exit;

  vMarcar := sgItens.Cells[coSelecionado, sgItens.Row] = charNaoSelecionado;
  for i := 1 to sgItens.RowCount -1 do
    sgItens.Cells[coSelecionado, i] := IIfStr( vMarcar , charSelecionado, charNaoSelecionado );
end;

procedure TFormRelacaoProdutos.miPermitirDevolucaoClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Permitir devolu��o?', ['Sim', 'N�o'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  vRetBanco := AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'PERMITIR_DEVOLUCAO');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.miPermitirEstoqueNegativoClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Permitir estoque negativo?', ['Permitir', 'N�o permitir'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  vRetBanco := _Produtos.AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'ACEITAR_ESTOQUE_NEGATIVO');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;


procedure TFormRelacaoProdutos.miPermitirNaoPermitirEstoqueNegativoClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Permitir estoque negativo?', ['Permitir', 'N�o permitir'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  vRetBanco := _Produtos.AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'ACEITAR_ESTOQUE_NEGATIVO');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.miProdutoPDVClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Produtos diversos (PDV)?', ['Sim', 'N�o'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  vRetBanco := AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'PRODUTO_DIVERSOS_PDV');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.miSepararSomenteCodigoBarrasClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Separar somente com c�d. de barras?', ['Sim', 'N�o'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  vRetBanco := AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'CONF_SOMENTE_CODIGO_BARRAS');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.miSobEncomendaClick(Sender: TObject);
var
  vOpcao: Integer;
  vRetBanco: RecRetornoBD;

  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vOpcao := SelecionarVariosOpcoes.Selecionar('Sob encomenda?', ['Sim', 'N�o'], -1).Dados;
  if vOpcao = -1 then
    Exit;

  vRetBanco := AtualizarCampoProduto( Sessao.getConexaoBanco, _Biblioteca.IIfStr(vOpcao = 0, 'S', 'N'), vProdutosIds, 'SOB_ENCOMENDA');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.No1Click(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecUnidades>;
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetBanco := _Produtos.AtualizarZerarCustoProdutos(Sessao.getConexaoBanco, vProdutosIds, 'N');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.sbDefinirMarcaProdutosClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecMarcas>;
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetorno := BuscarDadosMarcaProduto.BuscarDadosMarcaProdutos;
  if vRetorno.BuscaCancelada then
    Exit;

  vRetBanco := _Produtos.AtualizarCampoProduto( Sessao.getConexaoBanco, IntToStr(vRetorno.Dados.marca_id), vProdutosIds, 'MARCA_ID');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.sbDefinirUnidadeVendaClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecUnidades>;
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetorno := BuscarDadosUnidadeProduto.BuscarDadosUnidadeProdutos;
  if vRetorno.BuscaCancelada then
    Exit;

  vRetBanco := _Produtos.AtualizarCampoProduto( Sessao.getConexaoBanco, vRetorno.Dados.unidade_id, vProdutosIds, 'UNIDADE_VENDA');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.sbImprimirClick(Sender: TObject);
begin
  GridToExcel(sgItens, [coProdutoId]);
end;

procedure TFormRelacaoProdutos.sbZerarCustoVendaClick(Sender: TObject);
begin
  inherited;
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Width, TSpeedButton(Sender).Height)) do
    PopupMenu1.Popup(X, Y);
end;

procedure TFormRelacaoProdutos.sgItensDblClick(Sender: TObject);
begin
  inherited;
  InformacoesProduto.Informar( SFormatInt( sgItens.Cells[coProdutoId, sgItens.Row]) );
end;

procedure TFormRelacaoProdutos.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coProdutoId,
    coMultiploVenda,
    coPeso,
    coComissaoPrazo,
    coComissaoVista]
  then
    vAlinhamento := taRightJustify
  else if ACol in[
    coSelecionado,
    coAtivo]
  then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoProdutos.sgItensGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;

  if ARow = 0 then
    Exit;

  if sgItens.Cells[coTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
end;

procedure TFormRelacaoProdutos.sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgItens.Cells[coProdutoId, ARow] = '' then
    Exit;

  if ACol = coSelecionado then begin
    if sgItens.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgItens.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormRelacaoProdutos.sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if sgItens.Cells[coProdutoId, sgItens.Row] = '' then
    Exit;

  if (Shift = [ssCtrl]) and (Key = VK_SPACE) then
    miMarcarDesmarcarTodosClick(Sender)
  else if (Key = VK_SPACE) then
    miMarcarDesmarcarSelecionadoClick(Sender);
end;

procedure TFormRelacaoProdutos.SpeedButton2Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
  vRetornoTela: TRetornoTelaFinalizar<Double>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetornoTela := BuscarDadosPercentual('Percentual de comiss�o a vista');
  if vRetornoTela.BuscaCancelada then
    Exit;

  vRetBanco := _Produtos.AtualizarComissaoAVista(Sessao.getConexaoBanco, vProdutosIds, vRetornoTela.Dados);
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.SpeedButton3Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
  vRetornoTela: TRetornoTelaFinalizar<Double>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetornoTela := BuscarDadosPercentual('Percentual de custo de venda');
  if vRetornoTela.BuscaCancelada then
    Exit;

  vRetBanco := _Produtos.AtualizarCustoVendaProdutos(Sessao.getConexaoBanco, vProdutosIds, vRetornoTela.Dados);
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.SpeedButton4Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
  vRetornoTela: TRetornoTelaFinalizar<Double>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetornoTela := BuscarDadosPercentual('Percentual de comiss�o a prazo');
  if vRetornoTela.BuscaCancelada then
    Exit;

  vRetBanco := _Produtos.AtualizarComissaoAPrazo(Sessao.getConexaoBanco, vProdutosIds, vRetornoTela.Dados);
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.SpeedButton5Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
  vRetornoTela: TRetornoTelaFinalizar<TArray<RecProdutosGruposTribEmpresas>>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetornoTela := BuscarDadosTributacaoProdutos.BuscarDadosTributacao;
  if vRetornoTela.BuscaCancelada then
    Exit;

  vRetBanco := _Produtos.AtualizarTributacaoProdutos(Sessao.getConexaoBanco, vProdutosIds, vRetornoTela.Dados);
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.SpeedButton6Click(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
  vRetornoTela: TRetornoTelaFinalizar<string>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetornoTela := BuscarDadosTexto.BuscarDadosString('CEST', 'CEST', 7);
  if vRetornoTela.BuscaCancelada then
    Exit;

  vRetBanco := _Produtos.AtualizarCEST(Sessao.getConexaoBanco, vProdutosIds, vRetornoTela.Dados);
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoProdutos.SpeedButton7Click(Sender: TObject);
begin
  inherited;
  with TSpeedButton(Sender).ClientToScreen(point(TSpeedButton(Sender).Width, TSpeedButton(Sender).Height)) do
    PopupMenu2.Popup(X, Y);
end;

procedure TFormRelacaoProdutos.teste1Click(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecUnidades>;
  vRetBanco: RecRetornoBD;
  i: Integer;
  vProdutosIds: TArray<Integer>;
begin
  inherited;

  vProdutosIds := nil;

  for i := 1 to sgItens.RowCount -1 do begin
    if sgItens.Cells[coSelecionado, i] = charNaoSelecionado then
      Continue;

    _Biblioteca.AddNoVetorSemRepetir( vProdutosIds, SFormatInt( sgItens.Cells[coProdutoId, i] ) );
  end;

  if vProdutosIds = nil then begin
    _Biblioteca.Exclamar('Nenhum produto foi selecionado!');
    Exit;
  end;

  vRetBanco := _Produtos.AtualizarZerarCustoProdutos(Sessao.getConexaoBanco, vProdutosIds, 'S');
  Sessao.AbortarSeHouveErro( vRetBanco );

  RotinaSucesso;
  Carregar(Sender);
end;

end.
