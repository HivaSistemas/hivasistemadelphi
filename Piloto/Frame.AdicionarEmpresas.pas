unit Frame.AdicionarEmpresas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Buttons, PesquisaEmpresas,
  Vcl.Grids, GridLuka, Vcl.ExtCtrls, Vcl.StdCtrls, _RecordsCadastros, _Biblioteca,
  StaticTextLuka, Frame.HerancaInsercaoExclusao, Vcl.Menus;

type
  TFrameAdicionarEmpresas = class(TFrameHerancaInsercaoExclusao)
    sbAdicionarEmpresa: TSpeedButton;
    procedure sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    { Private declarations }
  public
    procedure Clear; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    function EstaVazio: Boolean; override;
    function TrazerArrayEmpresas: TArray<Integer>;
    procedure AddEmpresa(pEmpresaId: Integer; pNome: string);
  protected
    procedure Inserir; override;
  end;

implementation

{$R *.dfm}

const
  coEmpresaId   = 0;
  coNomeEmpresa = 1;

procedure TFrameAdicionarEmpresas.AddEmpresa(pEmpresaId: Integer; pNome: string);
var
  vLinha: Integer;
begin
  if sgValores.Cells[coEmpresaId, 0] = '' then
    vLinha := 0
  else
    vLinha := sgValores.RowCount;

  sgValores.Cells[coEmpresaId, vLinha] := NFormat(pEmpresaId);
  sgValores.Cells[coNomeEmpresa, vLinha] := pNome;
  sgValores.RowCount := vLinha + 1;

  sgValores.Row := vLinha;
  SetarFoco(sgValores);
end;

procedure TFrameAdicionarEmpresas.Clear;
begin
  inherited;
  sgValores.ClearGrid(1);
end;

function TFrameAdicionarEmpresas.EstaVazio: Boolean;
begin
  Result := sgValores.Cells[coEmpresaId, 0] = '';
end;

procedure TFrameAdicionarEmpresas.Inserir;
var
  vLinha: Integer;
  vEmpresa: RecEmpresas;
begin
  inherited;
  vEmpresa := RecEmpresas(PesquisaEmpresas.PesquisarEmpresa);
  if vEmpresa = nil then
    Exit;

  vLinha := sgValores.Localizar([coEmpresaId], [NFormat(vEmpresa.EmpresaId)]);
  if vLinha > -1 then begin
    sgValores.Row := vLinha;
    SetarFoco(sgValores);
    Exit;
  end;

  AddEmpresa(vEmpresa.EmpresaId, vEmpresa.NomeFantasia);
end;

procedure TFrameAdicionarEmpresas.sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if Acol = coEmpresaId then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgValores.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFrameAdicionarEmpresas.SomenteLeitura(pValue: Boolean);
begin
  inherited;

end;

function TFrameAdicionarEmpresas.TrazerArrayEmpresas: TArray<Integer>;
var
  i: Integer;
begin
  SetLength(Result, sgValores.RowCount);
  for i := 0 to sgValores.RowCount -1 do
    Result[i] := SFormatInt(sgValores.Cells[coEmpresaId, i]);
end;

end.
