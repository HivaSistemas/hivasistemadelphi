inherited FormPesquisaClientes: TFormPesquisaClientes
  Caption = 'Pesquisa de clientes'
  ClientHeight = 472
  ClientWidth = 794
  ExplicitWidth = 802
  ExplicitHeight = 503
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 794
    Height = 426
    ColCount = 11
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goThumbTracking]
    ScrollBars = ssBoth
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Raz'#227'o Social'
      'Nome Fanstasia'
      'CPF/CNPJ'
      'Logradouro'
      'Complemento'
      'Bairro'
      'Cidade/UF'
      'E-mail'
      'Ativo')
    RealColCount = 11
    Indicador = True
    ExplicitWidth = 794
    ExplicitHeight = 426
    ColWidths = (
      28
      55
      199
      177
      155
      170
      160
      133
      134
      152
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Left = 8
    Top = 424
    Text = '3'
    ExplicitLeft = 8
    ExplicitTop = 424
  end
  inherited pnFiltro1: TPanel
    Width = 794
    ExplicitWidth = 794
    inherited lblPesquisa: TLabel
      Left = 205
      Width = 39
      Caption = 'Cliente'
      ExplicitLeft = 205
      ExplicitWidth = 39
    end
    inherited eValorPesquisa: TEditLuka
      Left = 205
      Top = 19
      Width = 584
      ExplicitLeft = 205
      ExplicitTop = 19
      ExplicitWidth = 584
    end
    inherited cbOpcoesPesquisa: TComboBoxLuka
      Top = 19
      ExplicitTop = 19
    end
  end
end
