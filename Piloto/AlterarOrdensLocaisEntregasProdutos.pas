unit AlterarOrdensLocaisEntregasProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas,
  FrameOrdenacaoLocais, _RecordsEspeciais, _ProdutosOrdensLocEntregas, _Sessao;

type
  TFormAlterarOrdensLocaisEntregasProdutos = class(TFormHerancaFinalizar)
    FrEmpresasAlterar: TFrEmpresas;
    FrOrdemAto: TFrOrdenacaoLocais;
    FrOrdemRetirar: TFrOrdenacaoLocais;
    FrOrdemEntregar: TFrOrdenacaoLocais;
    procedure FormShow(Sender: TObject);
  private
    FProdutosIds: TArray<Integer>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Finalizar(Sender: TObject); override;
  end;

function Alterar(pProdutosIds: TArray<Integer>): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

function Alterar(pProdutosIds: TArray<Integer>): TRetornoTelaFinalizar;
var
  vForm: TFormAlterarOrdensLocaisEntregasProdutos;
begin
  vForm := TFormAlterarOrdensLocaisEntregasProdutos.Create(nil);
  vForm.FProdutosIds := pProdutosIds;

  Result.Ok(vForm.ShowModal, True);
end;

{ TFormAlterarOrdensLocaisEntregasProdutos }

procedure TFormAlterarOrdensLocaisEntregasProdutos.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresasAlterar.InserirDadoPorChave( Sessao.getEmpresaLogada.EmpresaId, False );
  SetarFoco(FrEmpresasAlterar);
end;

procedure TFormAlterarOrdensLocaisEntregasProdutos.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrEmpresasAlterar.EstaVazio then begin
    _Biblioteca.Exclamar('Nenhuma empresa foi selecionada para altera��o da ordena��o dos locais!');
    SetarFoco(FrEmpresasAlterar);
    Abort;
  end;

  if FrOrdemAto.EstaVazio and FrOrdemRetirar.EstaVazio and FrOrdemEntregar.EstaVazio then begin
    _Biblioteca.Exclamar('Nenhum local foi definido!');
    SetarFoco(FrOrdemAto);
    Abort;
  end;
end;

procedure TFormAlterarOrdensLocaisEntregasProdutos.Finalizar(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  Sessao.getConexaoBanco.IniciarTransacao;

  if not FrOrdemAto.EstaVazio then begin
    vRetBanco :=
      _ProdutosOrdensLocEntregas.AtualizarProdutosOrdensLocais(
        Sessao.getConexaoBanco,
        FrEmpresasAlterar.GetArrayOfInteger,
        FProdutosIds,
        'A',
        FrOrdemAto.TrazerArrayLocais,
        False
      );

    Sessao.AbortarSeHouveErro( vRetBanco );
  end;

  if not FrOrdemRetirar.EstaVazio then begin
    vRetBanco :=
      _ProdutosOrdensLocEntregas.AtualizarProdutosOrdensLocais(
        Sessao.getConexaoBanco,
        FrEmpresasAlterar.GetArrayOfInteger,
        FProdutosIds,
        'R',
        FrOrdemRetirar.TrazerArrayLocais,
        False
      );

    Sessao.AbortarSeHouveErro( vRetBanco );
  end;

  if not FrOrdemEntregar.EstaVazio then begin
    vRetBanco :=
      _ProdutosOrdensLocEntregas.AtualizarProdutosOrdensLocais(
        Sessao.getConexaoBanco,
        FrEmpresasAlterar.GetArrayOfInteger,
        FProdutosIds,
        'E',
        FrOrdemEntregar.TrazerArrayLocais,
        False
      );

    Sessao.AbortarSeHouveErro( vRetBanco );
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  inherited;
end;

end.
