unit OutrasNotas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsFiscais, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, System.Math, System.StrUtils, _Produtos,
  _FrameHenrancaPesquisas, FrameClientes, Vcl.Grids, GridLuka, FrameCFOPs, _Biblioteca, _NotasFiscais,
  FrameEndereco, ComboBoxLuka, FrameBairros, Vcl.Mask, EditCEPLuka, _RecordsCadastros, _ComunicacaoNFE,
  FrameProdutos, CheckBoxLuka, _CST, _Unidades, _RecordsEspeciais, _RecordsNotasFiscais, _ImpressaoDANFE,
  _NotasFiscaisItens, _ProdutosGruposTribEmpresas, Frame.Cadastros, Vcl.ComCtrls, _RecordsEstoques,
  Frame.HerancaInsercaoExclusao, FrameReferenciasNotasFiscais, _NotasFiscaisReferencias, _RelacaoEstoque,
  System.Win.ComObj, FrameTransportadoras, FrameUnidades;

type
  RecProdutosImportacao = record
    produtoId: Integer;
    cfop: string;
    quantidade: Double;
    precoUnitario: Double;
    cst: string;
  end;

type
  TFormOutrasNotas = class(TFormHerancaCadastroCodigo)
    sgProdutos: TGridLuka;
    cbTipoOperacao: TComboBoxLuka;
    lb1: TLabel;
    lb8: TLabel;
    eLogradouro: TEditLuka;
    eComplemento: TEditLuka;
    lb9: TLabel;
    lb2: TLabel;
    eNumero: TEditLuka;
    eCEP: TEditCEPLuka;
    lb10: TLabel;
    FrBairro: TFrBairros;
    FrProduto: TFrProdutos;
    lb4: TLabel;
    cbUnidade: TComboBoxLuka;
    lb5: TLabel;
    eQuantidade: TEditLuka;
    lb6: TLabel;
    ePrecoUnitario: TEditLuka;
    lb7: TLabel;
    eValorTotalProduto: TEditLuka;
    cbCST: TComboBoxLuka;
    lb11: TLabel;
    lb12: TLabel;
    eIndiceReducaoBaseICMS: TEditLuka;
    lb13: TLabel;
    eValorBaseICMS: TEditLuka;
    lb14: TLabel;
    ePercentualICMS: TEditLuka;
    lb15: TLabel;
    eValorICMS: TEditLuka;
    lb16: TLabel;
    eIndiceReducaoBaseICMSST: TEditLuka;
    lb17: TLabel;
    eValorBaseICMSST: TEditLuka;
    lb18: TLabel;
    ePercentualICMSST: TEditLuka;
    lb19: TLabel;
    eValorICMSST: TEditLuka;
    lb20: TLabel;
    ePercentualIPI: TEditLuka;
    eValorIPI: TEditLuka;
    lb21: TLabel;
    lb24: TLabel;
    eValorDesconto: TEditLuka;
    eValorOutrasDespesas: TEditLuka;
    lb26: TLabel;
    lb37: TLabel;
    ePesoBruto: TEditLuka;
    ePesoLiquido: TEditLuka;
    lb38: TLabel;
    FrCFOPCapa: TFrCFOPs;
    FrCFOPProduto: TFrCFOPs;
    FrCadastro: TFrameCadastros;
    pcOutros: TPageControl;
    tsTotais: TTabSheet;
    tsParametros: TTabSheet;
    lb22: TLabel;
    lb23: TLabel;
    lb25: TLabel;
    lb27: TLabel;
    lb31: TLabel;
    lb32: TLabel;
    lb33: TLabel;
    lb36: TLabel;
    eValorBaseICMSTotal: TEditLuka;
    eValorICMSTotal: TEditLuka;
    eValorDescontoTotal: TEditLuka;
    eValorOutrasDespesasTotal: TEditLuka;
    eValorTotalProdutos: TEditLuka;
    eBaseICMSSTTotal: TEditLuka;
    eValorICMSSTTotal: TEditLuka;
    eValorIPITotal: TEditLuka;
    lb30: TLabel;
    eValorTotalNota: TEditLuka;
    ckEmitirAltis: TCheckBoxLuka;
    ckMovimentarEstoque: TCheckBoxLuka;
    lb34: TLabel;
    cbTipoNota: TComboBoxLuka;
    lb35: TLabel;
    eInformacoesComplementares: TMemo;
    tsReferencias: TTabSheet;
    FrReferenciasNotasFiscais: TFrReferenciasNotasFiscais;
    lb3: TLabel;
    eCodigoProdutoNFe: TEditLuka;
    lbEstoqueFiscal: TLabel;
    eEstoqueFiscal: TEditLuka;
    SpeedButton1: TSpeedButton;
    OpenDialog1: TOpenDialog;
    cbTipoFrete: TComboBoxLuka;
    Label2: TLabel;
    Label3: TLabel;
    eNumeroItemPedido: TEditLuka;
    Label4: TLabel;
    eNumeroPedido: TEditLuka;
    TabSheet1: TTabSheet;
    FrTransportadora: TFrTransportadora;
    FrUnidadeVenda: TFrUnidades;
    ePesoBrutoTotal: TEditLuka;
    lb28: TLabel;
    lb29: TLabel;
    ePesoLiquidoTotal: TEditLuka;
    Label5: TLabel;
    eVolumes: TEditLuka;
    procedure FormCreate(Sender: TObject);
    procedure cbTipoOperacaoChange(Sender: TObject);
    procedure ePrecoUnitarioChange(Sender: TObject);
    procedure ePrecoUnitarioKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure ePercentualICMSChange(Sender: TObject);
    procedure cbCSTChange(Sender: TObject);
    procedure ePercentualICMSSTChange(Sender: TObject);
    procedure sgProdutosDblClick(Sender: TObject);
    procedure sgProdutosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cbTipoOperacaoExit(Sender: TObject);
    procedure eCodigoProdutoNFeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eQuantidadeChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    FLinhaEditando: Integer;
    FOutraNotaId: Integer;

    procedure PreencherRegistro(pNota: RecNotaFiscal);
    procedure FrCadastroOnAposPesquisar(Sender: TObject);
    procedure FrProdutoOnAposPesquisar(Sender: TObject);
    procedure FrCadastroOnAposDeletar(Sender: TObject);

    procedure AdicionarProduto;
    procedure AddNoGrid(
      pLinha: Integer;
      pProdutoId: Integer;
      pNomeProduto: string;
      pUnidade: string;
      pQuantidade: Double;
      pValorUnitario: Currency;
      pValorTotal: Currency;
      pDesconto: Currency;
      pOutrasDespesas: Currency;
      pCFOPId: string;
      pDescricaoCFOP: string;
      pCST: string;
      pBaseICMS: Currency;
      pPercentualICMS: Currency;
      pValorICMS: Currency;
      pIndiceRededucaoICMS: Double;
      pBaseICMSST: Currency;
      pPercentualICMSST: Currency;
      pValorICMSST: Currency;
      pIndiceReducaoICMSST: Double;
      pPercentualIPI: Currency;
      pValorIPI: Currency;
      pBasePIS: Currency;
      pPercentualPIS: Currency;
      pValorPIS: Currency;
      pBaseCOFINS: Currency;
      pPercentualCOFINS: Currency;
      pValorCOFINS: Currency;
      pPesoBruto: Double;
      pPesoLiquido: Double;
      pCodigoBarras: string;
      pNCM: string;
      pCSTPis: string;
      pCSTCofins: string;
      pCodigoProdutoNfe: string;
      pNumeroItemPedido: Integer;
      pNumeroPedido: string
    );

    procedure Totalizar;
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormOutrasNotas }

const
  coProdutoId           = 0;
  coNomeProduto         = 1;
  coUnidade             = 2;
  coQuantidade          = 3;
  coValorUnitario       = 4;
  coValorTotal          = 5;
  coDesconto            = 6;
  coOutrasDespesas      = 7;
  coCFOP                = 8;
  coCST                 = 9;
  coBaseICMS            = 10;
  coPercentualICMS      = 11;
  coValorICMS           = 12;
  coIndiceReducaoICMS   = 13;
  coBaseICMSST          = 14;
  coPercentualICMSST    = 15;
  coValorICMSST         = 16;
  coIndiceReducaoICMSST = 17;
  coPercentualIPI       = 18;
  coValorIPI            = 19;
  coBasePIS             = 20;
  coPercentualPIS       = 21;
  coValorPIS            = 22;
  coBaseCOFINS          = 23;
  coPercentualCOFINS    = 24;
  coValorCOFINS         = 25;
  coNumeroPedido        = 26;
  coNumeroItemPedido    = 27;
  coCodigoBarras        = 28;
  coNCM                 = 29;

  (* Colunas ocultas *)
  coCFOPId              = 30;
  coPesoBruto           = 31;
  coPesoLiquido         = 32;
  coCstPis              = 33;
  coCstCofins           = 34;
  coCodigoProdutoNfe    = 35;

procedure TFormOutrasNotas.AdicionarProduto;
var
  vTributacaoFederal: RecTributacoesFederal;
  vProdutos: TArray<RecProdutoImpostosCalculados>;
begin
  inherited;
  if cbTipoOperacao.GetValor = '' then begin
    _Biblioteca.Exclamar('O tipo de opera��o n�o foi informado corretamente!');
    SetarFoco(cbTipoOperacao);
    Exit;
  end;

  if FrProduto.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar o produto!');
    SetarFoco(FrProduto);
    Exit;
  end;

  if cbUnidade.GetValor = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar a unidade do produto!');
    SetarFoco(cbUnidade);
    Exit;
  end;

  if eQuantidade.AsCurr = 0 then begin
    _Biblioteca.Exclamar('� necess�rio informar a quantidade do produto!');
    SetarFoco(eQuantidade);
    Exit;
  end;

  if ePrecoUnitario.AsCurr = 0 then begin
    _Biblioteca.Exclamar('� necess�rio informar o pre�o do produto!');
    SetarFoco(ePrecoUnitario);
    Exit;
  end;

  if cbCST.GetValor = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar a CST do produto!');
    SetarFoco(cbCST);
    Exit;
  end;

  vTributacaoFederal := _ProdutosGruposTribEmpresas.getTributacoesFederal(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId, FrProduto.getProduto().produto_id);
  if vTributacaoFederal.ProdutoId = -1 then begin
    _Biblioteca.Exclamar('As tributa��es para o produto n�o foram encontradas, verifique a parte fiscal no cadastro do produto!');
    Exit;
  end;

  SetLength(vProdutos, 1);
  vProdutos[0].ProdutoId          := FrProduto.getProduto().produto_id;
  vProdutos[0].ValorTotalProdutos := eValorTotalProduto.AsCurr;
  vProdutos[0].ValorOutrasDesp    := eValorOutrasDespesas.AsCurr;
  vProdutos[0].ValorDesconto      := eValorDesconto.AsCurr;

  _Produtos.BuscarImpostosProduto(
    Sessao.getConexaoBanco,
    vProdutos,
    Sessao.getEmpresaLogada.EmpresaId,
    cbCST.GetValor,
    FrBairro.GetBairro().EstadoId
  );

  AddNoGrid(
    FLinhaEditando,
    FrProduto.getProduto().produto_id,
    FrProduto.getProduto().nome,
    cbUnidade.GetValor,
    eQuantidade.AsDouble,
    ePrecoUnitario.AsDouble,
    eValorTotalProduto.AsDouble,
    eValorDesconto.AsDouble,
    eValorOutrasDespesas.AsDouble,
    FrCFOPProduto.GetCFOP().CfopPesquisaId,
    FrCFOPProduto.GetCFOP().nome,
    cbCST.GetValor,
    eValorBaseICMS.AsDouble,
    ePercentualICMS.AsDouble,
    eValorICMS.AsDouble,
    eIndiceReducaoBaseICMS.AsDouble,
    eValorBaseICMSST.AsDouble,
    ePercentualICMSST.AsDouble,
    eValorICMSST.AsDouble,
    eIndiceReducaoBaseICMSST.AsDouble,
    ePercentualIPI.AsDouble,
    eValorIPI.AsDouble,
    vProdutos[0].BaseCalculoPis,
    vProdutos[0].PercentualPIS,
    vProdutos[0].ValorPIS,
    vProdutos[0].BaseCalculoCofins,
    vProdutos[0].PercentualCofins,
    vProdutos[0].ValorCOFINS,
    ePesoBruto.AsDouble,
    ePesoLiquido.AsDouble,
    FrProduto.getProduto().codigo_barras,
    FrProduto.getProduto().codigo_ncm,
    IIfStr(cbTipoOperacao.GetValor = 'E', vTributacaoFederal.CstPisCompra, vTributacaoFederal.CstPisVenda),
    IIfStr(cbTipoOperacao.GetValor = 'E', vTributacaoFederal.CstCofinsCompra, vTributacaoFederal.CstCofinsVenda),
    eCodigoProdutoNFe.Text,
    eNumeroItemPedido.AsInt,
    eNumeroPedido.Text
  );

  eIndiceReducaoBaseICMS.AsDouble := 1;
  eIndiceReducaoBaseICMSST.AsDouble := 1;

  _Biblioteca.LimparCampos([
    FrProduto,
    FrCFOPProduto,
    cbUnidade,
    eQuantidade,
    ePrecoUnitario,
    eValorTotalProduto,
    cbCST,
    eValorDesconto,
    eValorOutrasDespesas,
    eValorBaseICMS,
    ePercentualICMS,
    eValorICMS,
    eValorBaseICMSST,
    ePercentualICMSST,
    eValorICMSST,
    ePercentualIPI,
    eValorIPI,
    ePesoBruto,
    ePesoLiquido,
    eCodigoProdutoNFe,
    eNumeroItemPedido,
    eNumeroPedido,
    eEstoqueFiscal]
  );

  Totalizar;
  SetarFoco(FrProduto);
end;

procedure TFormOutrasNotas.BuscarRegistro;
var
  vNota: TArray<RecNotaFiscal>;
begin
  vNota := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 4, [eId.AsInt]);
  if vNota = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  if vNota[0].status = 'E' then begin
    _Biblioteca.Exclamar('Esta nota j� foi emitida, n�o � poss�vel edit�-la!');
    SetarFoco(eID);
    Exit;
  end;

  if vNota[0].status = 'C' then begin
    _Biblioteca.Exclamar('Esta nota j� foi cancelada, n�o � poss�vel edit�-la!');
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vNota[0]);
end;

procedure TFormOutrasNotas.cbCSTChange(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([eIndiceReducaoBaseICMS], Em(cbCST.GetValor, ['20', '70', '90']));
  _Biblioteca.Habilitar([eIndiceReducaoBaseICMSST], Em(cbCST.GetValor, ['70', '90']));
  eIndiceReducaoBaseICMS.AsDouble := 1;
  eIndiceReducaoBaseICMSST.AsDouble := 1;

  _Biblioteca.Habilitar([eValorBaseICMSST, ePercentualICMSST, eValorICMSST], Em(cbCST.GetValor, ['10', '70']));
end;

procedure TFormOutrasNotas.cbTipoOperacaoChange(Sender: TObject);
begin
  inherited;
  if cbTipoOperacao.GetValor = 'E' then begin
    FrCFOPCapa.cbTipoCFOPs.ItemIndex := 0;
    FrCFOPProduto.cbTipoCFOPs.ItemIndex := 0;
  end
  else begin
    FrCFOPCapa.cbTipoCFOPs.ItemIndex := 1;
    FrCFOPProduto.cbTipoCFOPs.ItemIndex := 1;
  end;
end;

procedure TFormOutrasNotas.cbTipoOperacaoExit(Sender: TObject);
begin
  inherited;
  if cbTipoOperacao.GetValor <> '' then
    cbTipoOperacao.Enabled := False;
end;

procedure TFormOutrasNotas.eCodigoProdutoNFeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  vTributacaoFederal: RecTributacoesFederal;
  vProdutos: TArray<RecProdutoImpostosCalculados>;
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  AdicionarProduto;
end;

procedure TFormOutrasNotas.ePercentualICMSChange(Sender: TObject);
begin
  inherited;
  eValorICMS.AsDouble := eValorBaseICMS.AsDouble * (ePercentualICMS.AsDouble * 0.01);
end;

procedure TFormOutrasNotas.ePercentualICMSSTChange(Sender: TObject);
begin
  inherited;
  eValorICMSST.AsDouble := eValorBaseICMSST.AsDouble * (ePercentualICMSST.AsDouble * 0.01);
end;

procedure TFormOutrasNotas.ePrecoUnitarioChange(Sender: TObject);
begin
  inherited;
  eValorTotalProduto.AsDouble := eQuantidade.AsDouble * ePrecoUnitario.AsDouble;
end;

procedure TFormOutrasNotas.ePrecoUnitarioKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eQuantidade.AsCurr = 0 then begin
    Exclamar('A quantidade n�o pode ser 0!');
    SetarFoco(eQuantidade);
    Abort;
  end;

  if ePrecoUnitario.AsCurr = 0 then begin
    Exclamar('O pre�o unit�rio n�o pode ser 0!');
    SetarFoco(ePrecoUnitario);
    Abort;
  end;

  ProximoCampo(Sender, Key, Shift);
end;

procedure TFormOutrasNotas.eQuantidadeChange(Sender: TObject);
begin
  inherited;
  if (eQuantidade.AsDouble > 0) and (ePrecoUnitario.AsDouble > 0) then
    eValorTotalProduto.AsDouble := eQuantidade.AsDouble * ePrecoUnitario.AsDouble;
end;

procedure TFormOutrasNotas.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco := _NotasFiscais.ExcluirNotasFiscais(Sessao.getConexaoBanco, eID.AsInt);
  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;
end;

procedure TFormOutrasNotas.FormCreate(Sender: TObject);
var
  i: Integer;
  vCSTs: TArray<RecCST>;
  vUnidades: TArray<RecUnidades>;
begin
  inherited;
  cbCST.Items.Clear;
  cbCST.Valores.Clear;
  vCSTs := _CST.getCSTs(Sessao.getConexaoBanco);
  for i := Low(vCSTs) to High(vCSTs) do begin
    cbCST.Items.Add(vCSTs[i].CST);
    cbCST.Valores.Add(vCSTs[i].CST);
  end;

  cbUnidade.Items.Clear;
  cbUnidade.Valores.Clear;
  vUnidades := _Unidades.BuscarUnidades(Sessao.getConexaoBanco, 2, []);
  for i := Low(vUnidades) to High(vUnidades) do begin
    cbUnidade.Items.Add(vUnidades[i].unidade_id);
    cbUnidade.Valores.Add(vUnidades[i].unidade_id);
  end;

  FrCadastro.OnAposPesquisar := FrCadastroOnAposPesquisar;
  FrCadastro.OnAposDeletar   := FrCadastroOnAposDeletar;

  FrProduto.OnAposPesquisar := FrProdutoOnAposPesquisar;
end;

procedure TFormOutrasNotas.FrCadastroOnAposDeletar(Sender: TObject);
begin
  _Biblioteca.LimparCampos([
    eLogradouro,
    eComplemento,
    FrBairro,
    eNumero,
    eCEP
  ]);
end;

procedure TFormOutrasNotas.FrCadastroOnAposPesquisar(Sender: TObject);
begin
  eLogradouro.Text      := FrCadastro.getCadastro().logradouro;
  eComplemento.Text     := FrCadastro.getCadastro().complemento;
  FrBairro.InserirDadoPorChave(FrCadastro.getCadastro().bairro_id, False);
  eNumero.Text          := FrCadastro.getCadastro().numero;
  eCEP.Text             := FrCadastro.getCadastro().cep;
end;

procedure TFormOutrasNotas.FrProdutoOnAposPesquisar(Sender: TObject);
var
  vSql: string;
  precoVenda, custoCompra: Double;

  VEstoque: Double;
  vEstoques: TArray<RecRelacaoEstoque>;
begin
  if Em(FrProduto.getProduto.TipoControleEstoque, ['K', 'A']) then begin
    _Biblioteca.Exclamar('N�o � permitido a movimenta��o de produtos kit, fa�a a movimenta��o dos produtos que comp�e o kit!');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Abort;
  end;

  if not FrProduto.EstaVazio then begin
    precoVenda  := 0;
    custoCompra := 0;
    vEstoque    := 0;

    vSql :=
      ' where EST.EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId) +
      ' and PRO.PRODUTO_ID = ' + IntToStr(FrProduto.getProduto.produto_id);

    vEstoques := _RelacaoEstoque.BuscarEstoque(Sessao.getConexaoBanco, vSql );
    if vEstoques <> nil then begin
      vEstoque := vEstoques[0].EstoqueFiscal;
      eEstoqueFiscal.AsDouble := vEstoque;
    end;

    cbUnidade.SetIndicePorValor(FrProduto.getProduto.unidade_venda);
    _Produtos.BuscarPrecoVendaCustoCompra(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId, FrProduto.getProduto.produto_id, precoVenda, custoCompra);
    ePrecoUnitario.AsDouble := precoVenda;
  end;
end;

procedure TFormOutrasNotas.PreencherRegistro(pNota: RecNotaFiscal);
var
  i: Integer;
  vItens: TArray<RecNotaFiscalItem>;
begin
  vItens := _NotasFiscaisItens.BuscarNotasFiscaisItens(Sessao.getConexaoBanco, 0, [pNota.nota_fiscal_id]);
  if vItens = nil then
    Exclamar('Os produtos da nota fiscal n�o foram encontrados!');

  FOutraNotaId := pNota.OutraNotaId;

  cbTipoOperacao.SetIndicePorValor( IfThen(Em(pNota.CfopId, ['1', '2', '3']), 'E', 'S') );
  FrCadastro.InserirDadoPorChave(pNota.cadastro_id, False);

  eLogradouro.Text      := pNota.logradouro_destinatario;
  eComplemento.Text     := pNota.complemento_destinatario;
  eNumero.Text          := pNota.numero_destinatario;
  eCEP.Text             := pNota.cep_destinatario;

//  eNumeroPedido.Text   := pNota.numero_pedido;

  FrCFOPCapa.InserirDadoPorChave(pNota.CfopId, False);

  ePesoBrutoTotal.AsDouble     := pNota.peso_bruto;
  ePesoLiquidoTotal.AsDouble   := pNota.peso_liquido;
  eValorBaseICMSTotal.AsDouble := pNota.base_calculo_icms;
  eValorICMSTotal.AsDouble     := pNota.valor_icms;
  eBaseICMSSTTotal.AsDouble    := pNota.base_calculo_icms_st;
  eValorICMSST.AsDouble        := pNota.valor_icms_st;
  eValorIPITotal.AsDouble      := pNota.valor_ipi;
  eValorDescontoTotal.AsDouble := pNota.valor_desconto;
  eValorOutrasDespesasTotal.AsDouble := pNota.valor_outras_despesas;
  eValorTotalProduto.AsDouble  := pNota.valor_total_produtos;
  eValorTotalNota.AsDouble     := pNota.valor_total;

  cbTipoNota.SetIndicePorValor(pNota.tipo_nota);
  cbTipoFrete.SetIndicePorValor(pNota.tipo_frete);
  eInformacoesComplementares.Lines.Text := pNota.informacoes_complementares;

  for i := Low(vItens) to High(vItens) do begin
    AddNoGrid(
      0,
      vItens[i].produto_id,
      vItens[i].nome_produto,
      vItens[i].unidade,
      vItens[i].quantidade,
      vItens[i].preco_unitario,
      vItens[i].valor_total,
      vItens[i].valor_total_desconto,
      vItens[i].valor_total_outras_despesas,
      vItens[i].CfopId,
      '',
      vItens[i].cst,
      vItens[i].base_calculo_icms,
      vItens[i].percentual_icms,
      vItens[i].valor_icms,
      vItens[i].indice_reducao_base_icms,
      vItens[i].base_calculo_icms_st,
      vItens[i].percentual_icms_st,
      vItens[i].valor_icms_st,
      vItens[i].indice_reducao_base_icms_st,
      vItens[i].percentual_ipi,
      vItens[i].valor_ipi,
      vItens[i].base_calculo_pis,
      vItens[i].percentual_pis,
      vItens[i].valor_pis,
      vItens[i].base_calculo_cofins,
      vItens[i].percentual_cofins,
      vItens[i].valor_cofins,
      0,
      0,
      vItens[i].codigo_barras,
      vItens[i].codigo_ncm,
      vItens[i].cst_pis,
      vItens[i].cst_cofins,
      vItens[i].CodigoProdutoNFe,
      vItens[i].numero_item_pedido,
      vItens[i].numero_pedido
    );
  end;

  FrReferenciasNotasFiscais.Referencias :=
    _NotasFiscaisReferencias.BuscarNotasFiscaisReferencias(
      Sessao.getConexaoBanco,
      0,
      [pNota.nota_fiscal_id]
    );
end;

procedure TFormOutrasNotas.AddNoGrid(
  pLinha: Integer;
  pProdutoId: Integer;
  pNomeProduto: string;
  pUnidade: string;
  pQuantidade: Double;
  pValorUnitario: Currency;
  pValorTotal: Currency;
  pDesconto: Currency;
  pOutrasDespesas: Currency;
  pCFOPId: string;
  pDescricaoCFOP: string;
  pCST: string;
  pBaseICMS: Currency;
  pPercentualICMS: Currency;
  pValorICMS: Currency;
  pIndiceRededucaoICMS: Double;
  pBaseICMSST: Currency;
  pPercentualICMSST: Currency;
  pValorICMSST: Currency;
  pIndiceReducaoICMSST: Double;
  pPercentualIPI: Currency;
  pValorIPI: Currency;
  pBasePIS: Currency;
  pPercentualPIS: Currency;
  pValorPIS: Currency;
  pBaseCOFINS: Currency;
  pPercentualCOFINS: Currency;
  pValorCOFINS: Currency;
  pPesoBruto: Double;
  pPesoLiquido: Double;
  pCodigoBarras: string;
  pNCM: string;
  pCSTPis: string;
  pCSTCofins: string;
  pCodigoProdutoNfe: string;
  pNumeroItemPedido: Integer;
  pNumeroPedido: string
);
var
  vLinha: Integer;
begin
  if pLinha > 0 then
    vLinha := pLinha
  else if sgProdutos.Cells[coProdutoId, 1] = '' then
    vLinha := 1
  else begin
    vLinha := sgProdutos.RowCount;
    sgProdutos.RowCount := sgProdutos.RowCount + 1;
  end;

  sgProdutos.Cells[coProdutoId, vLinha]           := NFormat(pProdutoId);
  sgProdutos.Cells[coNomeProduto, vLinha]         := pNomeProduto;
  sgProdutos.Cells[coUnidade, vLinha]             := pUnidade;
  sgProdutos.Cells[coQuantidade, vLinha]          := NFormat(pQuantidade, 4);
  sgProdutos.Cells[coValorUnitario, vLinha]       := NFormat(pValorUnitario);
  sgProdutos.Cells[coValorTotal, vLinha]          := NFormat(pValorTotal);
  sgProdutos.Cells[coDesconto, vLinha]            := NFormatN(pDesconto);
  sgProdutos.Cells[coOutrasDespesas, vLinha]      := NFormatN(pOutrasDespesas);
  sgProdutos.Cells[coCFOP, vLinha]                := pCFOPId + ' - ' + pDescricaoCFOP;
  sgProdutos.Cells[coCST, vLinha]                 := pCST;
  sgProdutos.Cells[coBaseICMS, vLinha]            := NFormatN(pBaseICMS);
  sgProdutos.Cells[coPercentualICMS, vLinha]      := NFormatN(pPercentualICMS);
  sgProdutos.Cells[coValorICMS, vLinha]           := NFormatN(pValorICMS);
  sgProdutos.Cells[coIndiceReducaoICMS, vLinha]   := NFormatN(pIndiceRededucaoICMS);
  sgProdutos.Cells[coBaseICMSST, vLinha]          := NFormatN(pBaseICMSST);
  sgProdutos.Cells[coPercentualICMSST, vLinha]    := NFormatN(pPercentualICMSST);
  sgProdutos.Cells[coValorICMSST, vLinha]         := NFormatN(pValorICMSST);
  sgProdutos.Cells[coIndiceReducaoICMSST, vLinha] := NFormatN(pIndiceReducaoICMSST);
  sgProdutos.Cells[coPercentualIPI, vLinha]       := NFormatN(pPercentualIPI);
  sgProdutos.Cells[coValorIPI, vLinha]            := NFormatN(pValorIPI);
  sgProdutos.Cells[coBasePIS, vLinha]             := NFormatN(pBasePIS);
  sgProdutos.Cells[coPercentualPIS, vLinha]       := NFormatN(pPercentualPIS);
  sgProdutos.Cells[coValorPIS, vLinha]            := NFormatN(pValorPIS);
  sgProdutos.Cells[coBaseCOFINS, vLinha]          := NFormatN(pBaseCOFINS);
  sgProdutos.Cells[coPercentualCOFINS, vLinha]    := NFormatN(pPercentualCOFINS);
  sgProdutos.Cells[coValorCOFINS, vLinha]         := NFormatN(pValorCOFINS);
  sgProdutos.Cells[coCFOPId, vLinha]              := pCFOPId;
  sgProdutos.Cells[coPesoBruto, vLinha]           := NFormatN(pPesoBruto);
  sgProdutos.Cells[coPesoLiquido, vLinha]         := NFormatN(pPesoLiquido);
  sgProdutos.Cells[coCodigoBarras, vLinha]        := pCodigoBarras;
  sgProdutos.Cells[coNCM, vLinha]                 := pNCM;
  sgProdutos.Cells[coCSTPis, vLinha]              := pCSTPis;
  sgProdutos.Cells[coCSTCofins, vLinha]           := pCSTCofins;
  sgProdutos.Cells[coCodigoProdutoNfe, vLinha]    := pCodigoProdutoNfe;
  sgProdutos.Cells[coNumeroItemPedido, vLinha]    := NFormatN(pNumeroItemPedido);
  sgProdutos.Cells[coNumeroPedido, vLinha]        := pNumeroPedido;

  FLinhaEditando := 0;
end;

procedure TFormOutrasNotas.GravarRegistro(Sender: TObject);
type
  RecDadosImpostos = record
    BaseCalculoPis: Double;
    ValorPis: Double;
    BaseCalculoCofins: Double;
    ValorCofins: Double;
  end;

var
  i: Integer;
  vNotaFiscalId: Integer;
  vRetBanco: RecRetornoBD;
  vImpostos: RecDadosImpostos;
  vItens: TArray<RecNotaFiscalItem>;
  vTransportadoraId: Integer;
  vEspecie: string;
begin
  inherited;

  vItens := nil;

  vImpostos.BaseCalculoPis := 0;
  vImpostos.ValorPis       := 0;
  vImpostos.BaseCalculoCofins := 0;
  vImpostos.ValorCofins       := 0;

  Totalizar;
  SetLength(vItens, sgProdutos.RowCount - 1);
  for i := 1 to sgProdutos.RowCount - 1 do begin
    vItens[i - 1].nota_fiscal_id              := 0;
    vItens[i - 1].produto_id                  := SFormatInt(sgProdutos.Cells[coProdutoId, i]);
    vItens[i - 1].nome_produto                := sgProdutos.Cells[coNomeProduto, i];
    vItens[i - 1].item_id                     := i;
    vItens[i - 1].CfopId                      := sgProdutos.Cells[coCFOPId, i];
    vItens[i - 1].orcamento_id                := 0;
    vItens[i - 1].cst                         := sgProdutos.Cells[coCST, i];
    vItens[i - 1].codigo_barras               := sgProdutos.Cells[coCodigoBarras, i];
    vItens[i - 1].origem_produto              := 0;
    vItens[i - 1].unidade                     := sgProdutos.Cells[coUnidade, i];
    vItens[i - 1].codigo_ncm                  := sgProdutos.Cells[coNCM, i];
    vItens[i - 1].cst_cofins                  := sgProdutos.Cells[coCstPis, i];
    vItens[i - 1].percentual_cofins           := SFormatDouble(sgProdutos.Cells[coPercentualCOFINS, i]);
    vItens[i - 1].valor_cofins                := SFormatDouble(sgProdutos.Cells[coValorCOFINS, i]);
    vItens[i - 1].iva                         := 0;
    vItens[i - 1].preco_pauta                 := 0;
    vItens[i - 1].valor_ipi                   := SFormatDouble(sgProdutos.Cells[coValorIPI, i]);
    vItens[i - 1].percentual_ipi              := SFormatDouble(sgProdutos.Cells[coPercentualIPI, i]);
    vItens[i - 1].base_calculo_cofins         := SFormatDouble(sgProdutos.Cells[coBaseCOFINS, i]);
    vItens[i - 1].valor_pis                   := SFormatDouble(sgProdutos.Cells[coValorPIS, i]);
    vItens[i - 1].percentual_pis              := SFormatDouble(sgProdutos.Cells[coPercentualPIS, i]);
    vItens[i - 1].base_calculo_icms_st        := SFormatDouble(sgProdutos.Cells[coBaseICMSST, i]);
    vItens[i - 1].valor_icms_st               := SFormatDouble(sgProdutos.Cells[coValorICMSST, i]);
    vItens[i - 1].base_calculo_pis            := SFormatDouble(sgProdutos.Cells[coBasePIS, i]);
    vItens[i - 1].cst_pis                     := sgProdutos.Cells[coCstCOfins, i];
    vItens[i - 1].valor_icms                  := SFormatDouble(sgProdutos.Cells[coValorICMS, i]);
    vItens[i - 1].percentual_icms             := SFormatDouble(sgProdutos.Cells[coPercentualICMS, i]);
    vItens[i - 1].base_calculo_icms           := SFormatDouble(sgProdutos.Cells[coBaseICMS, i]);
    vItens[i - 1].valor_total                 := SFormatDouble(sgProdutos.Cells[coValorTotal, i]);
    vItens[i - 1].preco_unitario              := SFormatDouble(sgProdutos.Cells[coValorUnitario, i]);
    vItens[i - 1].valor_frete                 := 0;
    vItens[i - 1].valor_seguro                := 0;
    vItens[i - 1].quantidade                  := SFormatDouble(sgProdutos.Cells[coQuantidade, i]);
    vItens[i - 1].valor_total_desconto        := SFormatDouble(sgProdutos.Cells[coDesconto, i]);
    vItens[i - 1].valor_total_outras_despesas := SFormatDouble(sgProdutos.Cells[coOutrasDespesas, i]);
    vItens[i - 1].indice_reducao_base_icms    := SFormatDouble(sgProdutos.Cells[coIndiceReducaoICMS, i]);
    vItens[i - 1].indice_reducao_base_icms_st := SFormatDouble(sgProdutos.Cells[coIndiceReducaoICMSST, i]);
    vItens[i - 1].percentual_icms_st          := SFormatDouble(sgProdutos.Cells[coPercentualICMSST, i]);
    vItens[i - 1].informacoes_adicionais      := '';
    vItens[i - 1].CodigoProdutoNFe            := sgProdutos.Cells[coCodigoProdutoNfe, i];
    vItens[i - 1].numero_item_pedido          := SFormatInt(sgProdutos.Cells[coNumeroItemPedido, i]);
    vItens[i - 1].numero_pedido               := sgProdutos.Cells[coNumeroPedido, i];

    vImpostos.BaseCalculoPis := vImpostos.BaseCalculoPis + vItens[i - 1].base_calculo_pis;
    vImpostos.ValorPis       := vImpostos.ValorPis + vItens[i - 1].valor_pis;
    vImpostos.BaseCalculoCofins := vImpostos.BaseCalculoCofins + vItens[i - 1].base_calculo_cofins;
    vImpostos.ValorCofins       := vImpostos.ValorCofins + vItens[i - 1].valor_cofins;
  end;

  if not FrTransportadora.EstaVazio then
    vTransportadoraId := FrTransportadora.GetDado().CadastroId;

  if not FrUnidadeVenda.EstaVazio then
    vEspecie := FrUnidadeVenda.getUnidade().unidade_id;

  vRetBanco :=
    _NotasFiscais.AtualizarNotasFiscais(
      Sessao.getConexaoBanco,
      eID.AsInt,
      FOutraNotaId,
      FrCadastro.getCadastro().cadastro_id,
      FrCFOPCapa.GetCFOP().CfopPesquisaId,
      Sessao.getEmpresaLogada.EmpresaId,
      0,
      0,
      0,
      0,
      0,
      'OUT',
      0,
      IIfStr(cbTipoNota.GetValor = 'C', '65', '55'),
      IIfStr(cbTipoNota.GetValor = 'C', IntToStr(Sessao.getParametrosEmpresa.SerieNFCe) , IntToStr(Sessao.getParametrosEmpresa.SerieNFe)),
      Sessao.getEmpresaLogada.RazaoSocial,
      Sessao.getEmpresaLogada.NomeFantasia,
      Sessao.getParametrosEmpresa.RegimeTributario,
      Sessao.getEmpresaLogada.Cnpj,
      Sessao.getEmpresaLogada.InscricaoEstadual,
      Sessao.getEmpresaLogada.Logradouro,
      Sessao.getEmpresaLogada.Complemento,
      Sessao.getEmpresaLogada.NomeBairro,
      Sessao.getEmpresaLogada.NomeCidade,
      Sessao.getEmpresaLogada.Numero,
      Sessao.getEmpresaLogada.EstadoId,
      Sessao.getEmpresaLogada.Cep,
      Sessao.getEmpresaLogada.TelefonePrincipal,
      Sessao.getEmpresaLogada.CodigoIBGECidade,
      Sessao.getEmpresaLogada.CodigoIBGEEstado,
      FrCadastro.getCadastro().nome_fantasia,
      FrCadastro.getCadastro().razao_social,
      FrCadastro.getCadastro().tipo_pessoa,
      FrCadastro.getCadastro().cpf_cnpj,
      FrCadastro.getCadastro().inscricao_estadual,
      eLogradouro.Text,
      eComplemento.Text,
      FrBairro.GetBairro.nome,
      FrBairro.GetBairro.nome_cidade,
      eNumero.Text,
      FrBairro.GetBairro.EstadoId,
      eCEP.Text,
      FrBairro.GetBairro.CodigoIbgeCidade,
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      0,
      '', // Nome do cosumidor final
      '', // Telefone do cosumidor final
      cbTipoNota.GetValor,
      FrCFOPCapa.GetCFOP().nome,
      eValorTotalNota.AsCurr,
      eValorTotalProdutos.AsCurr,
      eValorDescontoTotal.AsCurr,
      eValorOutrasDespesasTotal.AsCurr,
      0,
      0,
      eValorBaseICMSTotal.AsCurr,
      eValorICMS.AsCurr,
      eBaseICMSSTTotal.AsCurr,
      eValorICMSST.AsCurr,
      vImpostos.BaseCalculoPis,
      vImpostos.ValorPis,
      vImpostos.BaseCalculoCofins,
      vImpostos.ValorCofins,
      eValorIPITotal.AsCurr,
      ePesoLiquidoTotal.AsCurr,
      ePesoBrutoTotal.AsCurr,
      0,
      0,
      0,
      0,
      0,
      eValorTotalNota.AsDouble,
      0,
      0,
      'N',
      '',
      '',
      0,
      '',
      '',
      '',
      '',
      '',
      'N',
      'N',
      0,
      'S',
      eInformacoesComplementares.Lines.Text,
      vItens,
      FrReferenciasNotasFiscais.Referencias,
      0,
      vTransportadoraId,
      eVolumes.AsDouble,
      vEspecie,
      cbTipoFrete.GetValorInt
    );

  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  vNotaFiscalId := IfThen(eID.AsInt > 0, eID.AsInt, vRetBanco.AsInt);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);

  if not Perguntar('Deseja enviar a NF-e agora?') then
    Exit;

  if not _ComunicacaoNFE.EnviarNFe(vNotaFiscalId) then
    Exclamar('Falha ao enviar NFe, fa�a a emiss�o pela tela de consulta de notas fiscais!');
end;

procedure TFormOutrasNotas.Modo(pEditando: Boolean);
begin
  inherited;

  FOutraNotaId := 0;

  _Biblioteca.Habilitar([
    cbTipoOperacao,
    FrCadastro,
    eLogradouro,
    eComplemento,
    FrBairro,
    eNumero,
    eCEP,
    FrCFOPCapa,
    FrProduto,
    FrCFOPProduto,
    cbUnidade,
    eQuantidade,
    ePrecoUnitario,
    eValorTotalProduto,
    cbCST,
    eValorDesconto,
    eValorOutrasDespesas,
    eIndiceReducaoBaseICMS,
    eValorBaseICMS,
    ePercentualICMS,
    eValorICMS,
    eIndiceReducaoBaseICMSST,
    eValorBaseICMSST,
    ePercentualICMSST,
    eValorICMSST,
    ePercentualIPI,
    eValorIPI,
    sgProdutos,
    ePesoBrutoTotal,
    ePesoLiquidoTotal,
    eValorBaseICMSTotal,
    eValorICMSTotal,
    eBaseICMSSTTotal,
    eValorIPITotal,
    eValorICMSSTTotal,
    eValorDescontoTotal,
    eValorOutrasDespesasTotal,
    eValorTotalProdutos,
    eValorTotalNota,
    cbTipoNota,
    cbTipoFrete,
    ckMovimentarEstoque,
    eInformacoesComplementares,
    ePesoBruto,
    ePesoLiquido,
    ckEmitirAltis,
    pcOutros,
    FrReferenciasNotasFiscais],
    pEditando
  );

  ckMovimentarEstoque.Checked := true;

  eIndiceReducaoBaseICMS.AsDouble := 1;
  eIndiceReducaoBaseICMSST.AsDouble := 1;
  cbTipoFrete.ItemIndex := 2;

  if pEditando then begin
    SetarFoco(cbTipoOperacao);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormOutrasNotas.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormOutrasNotas.sgProdutosDblClick(Sender: TObject);
begin
  inherited;
  if sgProdutos.Cells[coProdutoId, 1] = '' then
    Exit;

  FrProduto.InserirDadoPorChave(SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]), False);
  FrCFOPProduto.InserirDadoPorChave(sgProdutos.Cells[coCFOPId, sgProdutos.Row], False);
  cbUnidade.SetIndicePorValor(sgProdutos.Cells[coUnidade, sgProdutos.Row]);

  eQuantidade.AsDouble              := SFormatDouble(sgProdutos.Cells[coQuantidade, sgProdutos.Row]);
  ePrecoUnitario.AsDouble           := SFormatDouble(sgProdutos.Cells[coValorUnitario, sgProdutos.Row]);
  eValorTotalProduto.AsDouble       := SFormatDouble(sgProdutos.Cells[coValorTotal, sgProdutos.Row]);
  cbCST.SetIndicePorValor(sgProdutos.Cells[coCST, sgProdutos.Row]);
  eValorDesconto.AsDouble           := SFormatDouble(sgProdutos.Cells[coDesconto, sgProdutos.Row]);
  eValorOutrasDespesas.AsDouble     := SFormatDouble(sgProdutos.Cells[coOutrasDespesas, sgProdutos.Row]);
  eIndiceReducaoBaseICMS.AsDouble   := SFormatDouble(sgProdutos.Cells[coIndiceReducaoICMS, sgProdutos.Row]);
  eValorBaseICMS.AsDouble           := SFormatDouble(sgProdutos.Cells[coBaseICMS, sgProdutos.Row]);
  ePercentualICMS.AsDouble          := SFormatDouble(sgProdutos.Cells[coPercentualICMS, sgProdutos.Row]);
  eValorICMS.AsDouble               := SFormatDouble(sgProdutos.Cells[coValorICMS, sgProdutos.Row]);
  eIndiceReducaoBaseICMSST.AsDouble := SFormatDouble(sgProdutos.Cells[coIndiceReducaoICMSST, sgProdutos.Row]);
  eBaseICMSSTTotal.AsDouble         := SFormatDouble(sgProdutos.Cells[coBaseICMSST, sgProdutos.Row]);
  ePercentualICMSST.AsDouble        := SFormatDouble(sgProdutos.Cells[coPercentualICMSST, sgProdutos.Row]);
  eValorICMSST.AsDouble             := SFormatDouble(sgProdutos.Cells[coValorICMSST, sgProdutos.Row]);
  ePercentualIPI.AsDouble           := SFormatDouble(sgProdutos.Cells[coPercentualIPI, sgProdutos.Row]);
  eValorIPI.AsDouble                := SFormatDouble(sgProdutos.Cells[coValorIPI, sgProdutos.Row]);
  ePesoBruto.AsDouble               := SFormatDouble(sgProdutos.Cells[coPesoBruto, sgProdutos.Row]);
  ePesoLiquido.AsDouble             := SFormatDouble(sgProdutos.Cells[coPesoLiquido, sgProdutos.Row]);
  eCodigoProdutoNFe.Text            := sgProdutos.Cells[coCodigoProdutoNFe, sgProdutos.Row];
  eNumeroItemPedido.Text            := sgProdutos.Cells[coNumeroItemPedido, sgProdutos.Row];

  FLinhaEditando := sgProdutos.Row;
end;

procedure TFormOutrasNotas.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coProdutoId,
    coQuantidade,
    coValorUnitario,
    coDesconto,
    coOutrasDespesas,
    coValorTotal,
    coBaseICMS,
    coPercentualICMS,
    coValorICMS,
    coIndiceReducaoICMS,
    coBaseICMSST,
    coPercentualICMSST,
    coValorICMSST,
    coIndiceReducaoICMSST,
    coPercentualIPI,
    coValorIPI,
    coBasePIS,
    coPercentualPIS,
    coValorPIS,
    coBaseCOFINS,
    coPercentualCOFINS,
    coValorCOFINS]
  then
    vAlinhamento := taRightJustify
  else if ACol in[coUnidade, coCST] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormOutrasNotas.sgProdutosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_DELETE then
    Exit;

  sgProdutos.DeleteRow(sgProdutos.Row, sgProdutos.RealColCount);
end;

procedure TFormOutrasNotas.SpeedButton1Click(Sender: TObject);
var
  produtos: TArray<RecProdutosImportacao>;
  i: Integer;
  sheet: OleVariant;
  vPlanilha: OleVariant;
begin
  if OpenDialog1.Execute then begin
    //Crio o objeto que gerencia o arquivo excel
    vPlanilha:= CreateOleObject('Excel.Application');
    vPlanilha.WorkBooks.Open(OpenDialog1.FileName);

    //Pego a primeira vPlanilha do arquivo
    sheet := vPlanilha.WorkSheets[1];

    i := 1;
    while Trim(sheet.Cells[i, 1]) <> '' do begin
      SetLength(produtos, Length(produtos) + 1);
      produtos[High(produtos)].produtoId     := sheet.cells[i, 1].Value;
      produtos[High(produtos)].cfop      := sheet.cells[i, 2].Value;
      produtos[High(produtos)].quantidade      := sheet.cells[i, 3].Value;
      produtos[High(produtos)].precoUnitario  := sheet.cells[i, 4].Value;
      produtos[High(produtos)].cst       := sheet.cells[i, 5].Value;
      Inc(i);
    end;

    //Fecho a vPlanilha
    vPlanilha.WorkBooks.Close;

    for i := 0 to Length(produtos) - 1 do begin
      FrProduto.InserirDadoPorChave(produtos[i].produtoId);
      FrCFOPProduto.InserirDadoPorChave(produtos[i].cfop);
      eQuantidade.AsDouble := produtos[i].quantidade;
      ePrecoUnitario.AsDouble := produtos[i].precoUnitario;
      cbCST.SetIndicePorValor(produtos[i].cst);
      AdicionarProduto;
    end;

    RotinaSucesso;    
  end;
end;

procedure TFormOutrasNotas.Totalizar;
type
  RecTotais = record
    PesoBruto: Double;
    PesoLiquido: Double;
    BaseCalculoICMS: Double;
    ICMS: Double;
    BaseCalculoICMSST: Double;
    ICMSST: Double;
    IPI: Double;
    Desconto: Double;
    OutrasDespesas: Double;
    Produtos: Double;
    BasePIS: Double;
    PIS: Double;
    BaseCOFINS: Double;
    COFINS: DOuble;
  end;

var
  i: Integer;
  vTotais: RecTotais;
begin
  vTotais.PesoBruto         := 0;
  vTotais.PesoLiquido       := 0;
  vTotais.BaseCalculoICMS   := 0;
  vTotais.ICMS              := 0;
  vTotais.BaseCalculoICMSST := 0;
  vTotais.ICMSST            := 0;
  vTotais.IPI               := 0;
  vTotais.Desconto          := 0;
  vTotais.OutrasDespesas    := 0;
  vTotais.Produtos          := 0;
  vTotais.BasePIS           := 0;
  vTotais.PIS               := 0;
  vTotais.BaseCOFINS        := 0;
  vTotais.COFINS            := 0;

  for i := 1 to sgProdutos.RowCount - 1 do begin
    vTotais.PesoBruto         := vTotais.PesoBruto + SFormatDouble(sgProdutos.Cells[coPesoBruto, i]);
    vTotais.PesoLiquido       := vTotais.PesoLiquido + SFormatDouble(sgProdutos.Cells[coPesoLiquido, i]);
    vTotais.BaseCalculoICMS   := vTotais.BaseCalculoICMS + SFormatDouble(sgProdutos.Cells[coBaseICMS, i]);
    vTotais.ICMS              := vTotais.ICMS + SFormatDouble(sgProdutos.Cells[coValorICMS, i]);
    vTotais.BaseCalculoICMSST := vTotais.BaseCalculoICMSST + SFormatDouble(sgProdutos.Cells[coBaseICMSST, i]);
    vTotais.ICMSST            := vTotais.ICMSST + SFormatDouble(sgProdutos.Cells[coValorICMSST, i]);
    vTotais.IPI               := vTotais.IPI + SFormatDouble(sgProdutos.Cells[coValorIPI, i]);
    vTotais.Desconto          := vTotais.Desconto + SFormatDouble(sgProdutos.Cells[coDesconto, i]);
    vTotais.OutrasDespesas    := vTotais.OutrasDespesas + SFormatDouble(sgProdutos.Cells[coOutrasDespesas, i]);
    vTotais.Produtos          := vTotais.Produtos + SFormatDouble(sgProdutos.Cells[coValorTotal, i]);
    vTotais.BasePIS           := vTotais.BasePIS + SFormatDouble(sgProdutos.Cells[coBasePIS, i]);
    vTotais.PIS               := vTotais.PIS + SFormatDouble(sgProdutos.Cells[coValorPIS, i]);
    vTotais.BaseCOFINS        := vTotais.BaseCOFINS + SFormatDouble(sgProdutos.Cells[coBaseCOFINS, i]);
    vTotais.COFINS            := vTotais.COFINS + SFormatDouble(sgProdutos.Cells[coValorCOFINS, i]);
  end;

  ePesoBrutoTotal.AsDouble           := vTotais.PesoBruto;
  ePesoLiquidoTotal.AsDouble         := vTotais.PesoLiquido;
  eValorBaseICMSTotal.AsDouble       := vTotais.BaseCalculoICMS;
  eValorICMSTotal.AsDouble           := vTotais.ICMS;
  eBaseICMSSTTotal.AsDouble          := vTotais.BaseCalculoICMSST;
  eValorICMSSTTotal.AsDouble         := vTotais.ICMSST;
  eValorIPITotal.AsDouble            := vTotais.IPI;
  eValorDescontoTotal.AsDouble       := vTotais.Desconto;
  eValorOutrasDespesasTotal.AsDouble := vTotais.OutrasDespesas;
  eValorTotalProdutos.AsDouble       := vTotais.Produtos;

  eValorTotalNota.AsCurr :=
    vTotais.Produtos -
    vTotais.Desconto +
    vTotais.OutrasDespesas +
    vTotais.IPI +
    vTotais.ICMSST;
end;

procedure TFormOutrasNotas.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if cbTipoOperacao.GetValor = '' then begin
    Exclamar('� necess�rio informar o tipo de opera��o a ser realizada!');
    SetarFoco(cbTipoOperacao);
    Abort;
  end;

  if FrCadastro.EstaVazio then begin
    Exclamar('� necess�rio informar o cliente da opera��o!');
    SetarFoco(FrCadastro);
    Abort;
  end;

  if FrBairro.EstaVazio then begin
    Exclamar('� necess�rio informar o bairro do cliente!');
    SetarFoco(FrBairro);
    Abort;
  end;

  if FrCFOPCapa.EstaVazio then begin
    Exclamar('� necess�rio informar o CFOP da capa da nota fiscal!');
    SetarFoco(FrCFOPCapa);
    Abort;
  end;

  if sgProdutos.Cells[coProdutoId, 1] = '' then begin
    Exclamar('� necess�rio informar ao menos 1 produto!');
    SetarFoco(FrProduto);
    Abort;
  end;

  if cbTipoNota.GetValor = '' then begin
    Exclamar('� necess�rio informar o tipo de nota a ser emitida!');
    SetarFoco(cbTipoNota);
    Abort;
  end;

  if ckEmitirAltis.Checked then begin
    if (cbTipoNota.GetValor = 'N') and (FrCadastro.getCadastro.cadastro_id = Sessao.getParametros.cadastro_consumidor_final_id) then begin
      Exclamar('N�o � permitido gerar NFe para consumidor final!');
      SetarFoco(FrCadastro);
      Abort;
    end;

    eInformacoesComplementares.Lines.Text := Trim( eInformacoesComplementares.Lines.Text );
    if (_Biblioteca.RemoverCaracteresEspeciais(eInformacoesComplementares.Lines.Text) = '') then begin
      Exclamar('� necess�rio informar as "Informa��es complementares"!');
      SetarFoco(eInformacoesComplementares);
      Abort;
    end;
  end;

  if (cbTipoFrete.GetValor = '9') and (not FrTransportadora.EstaVazio) then begin
    Exclamar('A transportadora foi definida. Por favor verifique o tipo de frete informado.');
    SetarFoco(cbTipoFrete);
    Abort;
  end;
end;

end.
