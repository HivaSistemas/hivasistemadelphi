inherited FormPesquisaVeiculos: TFormPesquisaVeiculos
  Caption = 'Pesquisa de ve'#237'culos'
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    ColCount = 8
    FixedCols = 1
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Placa'
      'Motorista'
      'Peso m'#225'x.'
      'Tara'
      'Ativo')
    RealColCount = 8
    ExplicitLeft = 0
    ExplicitTop = 46
    ExplicitWidth = 648
    ExplicitHeight = 248
    ColWidths = (
      28
      43
      162
      69
      119
      77
      57
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
  inherited pnFiltro1: TPanel
    inherited eValorPesquisa: TEditLuka
      Width = 441
      ExplicitWidth = 441
    end
  end
end
