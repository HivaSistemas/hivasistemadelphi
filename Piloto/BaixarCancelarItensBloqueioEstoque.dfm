inherited FormBaixarCancelarItensBloqueioEstoque: TFormBaixarCancelarItensBloqueioEstoque
  Caption = 'Baixar cancelar itens do bloqueio estoque'
  ClientHeight = 380
  ClientWidth = 777
  OnShow = FormShow
  ExplicitWidth = 783
  ExplicitHeight = 409
  PixelsPerInch = 96
  TextHeight = 14
  object lbObservacoes: TLabel [0]
    Left = 3
    Top = 261
    Width = 69
    Height = 14
    Caption = 'Observa'#231#245'es'
  end
  inherited pnOpcoes: TPanel
    Top = 343
    Width = 777
  end
  object sgItens: TGridLuka
    Left = 1
    Top = 1
    Width = 774
    Height = 256
    ColCount = 8
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 2
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    TabOrder = 1
    OnDrawCell = sgItensDrawCell
    OnKeyPress = NumerosVirgula
    OnSelectCell = sgItensSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Produto'
      'Nome'
      'Quantidade'
      'Und.'
      'Lote'
      'Local'
      'Saldo'
      'Qtd.bx/canc.')
    OnArrumarGrid = sgItensArrumarGrid
    Grid3D = False
    RealColCount = 13
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      55
      269
      77
      34
      56
      108
      71
      78)
  end
  object meObservacoes: TMemoAltis
    Left = 1
    Top = 275
    Width = 774
    Height = 66
    CharCase = ecUpperCase
    TabOrder = 2
  end
end
