unit ConsultaNotasFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, Vcl.Grids, GridLuka, _RecordsNotasFiscais,
  Vcl.Buttons, Vcl.ExtCtrls, _NotasFiscais, _Sessao, System.Math, _Biblioteca, System.StrUtils,
  _ComunicacaoNFE, _RecordsNFE, BuscaDados, _ImpressaoDANFE, Vcl.Menus, BuscarDados.InutilizacaoNFe,
  Edicao.CapaNotaFiscal, Edicao.NotaFiscalItens, Informacoes.NotaFiscal, _RecordsEspeciais,
  _ImpressaoDANFE_NFCe, Vcl.StdCtrls, CheckBoxLuka, Impressao.ComprovanteGraficoNFCe;

type
  TFormConsultaNotasFiscais = class(TFormHerancaRelatorios)
    sgNotas: TGridLuka;
    pmRotinas: TPopupMenu;
    miCancelarNFe: TMenuItem;
    miInutilizarNumercaoNFe: TMenuItem;
    miEditarCapaNotaFiscal: TMenuItem;
    miEditarItensNotaFiscal: TMenuItem;
    miN1: TMenuItem;
    miEmitirNotafiscalCupomFiscal: TMenuItem;
    miN2: TMenuItem;
    miN3: TMenuItem;
    miN4: TMenuItem;
    miEmitirNFePendente: TMenuItem;
    procedure sgNotasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgNotasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure miCancelarNFeClick(Sender: TObject);
    procedure pmRotinasPopup(Sender: TObject);
    procedure miInutilizarNumercaoNFeClick(Sender: TObject);
    procedure miEditarCapaNotaFiscalClick(Sender: TObject);
    procedure miEditarItensNotaFiscalClick(Sender: TObject);
    procedure sgNotasDblClick(Sender: TObject);
    procedure miEmitirNotafiscalCupomFiscalClick(Sender: TObject);
    procedure miEmitirNFePendenteClick(Sender: TObject);
  protected
    procedure Carregar(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormHerancaRelatorios1 }

const
  coTipoNota        = 0;
  coStatus          = 1;
  coStatusNfe       = 2;
  coMotivoStatusNfe = 3;
  coNumeroNota      = 4;
  coDataGeracao     = 5;
  coNomeRazaoSocial = 6;
  coValor           = 7;
  coDanfeImpresso   = 8;
  coEmailEnviado    = 9;
  coChaveAcesso     = 10;
  coNotaFiscalId    = 11;
  coTipoMovimento   = 12;
  coOrcamentoId     = 13;
  coRetiradaId      = 14;
  coEntregaId       = 15;
  coDevolucaoId     = 16;

procedure TFormConsultaNotasFiscais.Carregar(Sender: TObject);
var
  i: Integer;
  vNotas: TArray<RecNotaFiscal>;
begin
  inherited;
  vNotas := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 3, []);

  for i := Low(vNotas) to High(vNotas) do begin
    sgNotas.Cells[coTipoNota, i + 1]       := IfThen(vNotas[i].tipo_nota = 'N', 'NF-e', 'NFC-e');

    sgNotas.Cells[coStatus, i + 1] :=
      Decode(
        vNotas[i].status,[
        'E', 'Emitida',
        'C', 'Cancelada',
        'D', 'Denegada',
        'N�o emitida']
      );

    sgNotas.Cells[coStatusNfe, i + 1]       := vNotas[i].status_nfe;
    sgNotas.Cells[coMotivoStatusNfe, i + 1] := vNotas[i].motivo_status_nfe;
    sgNotas.Cells[coNumeroNota, i + 1]      := _Biblioteca.NFormatN(vNotas[i].numero_nota);
    sgNotas.Cells[coDataGeracao, i + 1]     := DFormat(vNotas[i].data_hora_cadastro);
    sgNotas.Cells[coNomeRazaoSocial, i + 1] := IfThen(vNotas[i].tipo_pessoa_destinatario = 'F', vNotas[i].nome_fantasia_destinatario, vNotas[i].razao_social_destinatario);
    sgNotas.Cells[coValor, i + 1]           := _Biblioteca.NFormat(vNotas[i].valor_total);
    sgNotas.Cells[coDanfeImpresso, i + 1]   := _Biblioteca.SimNao(vNotas[i].danfe_impresso);
    sgNotas.Cells[coEmailEnviado, i + 1]    := _Biblioteca.SimNao(vNotas[i].enviou_email_nfe_cliente);
    sgNotas.Cells[coChaveAcesso, i + 1]     := _Biblioteca.RetornaNumeros(vNotas[i].chave_nfe);
    sgNotas.Cells[coNotaFiscalId, i + 1]    := _Biblioteca.NFormat(vNotas[i].nota_fiscal_id);
    sgNotas.Cells[coTipoMovimento, i + 1]   := vNotas[i].TipoMovimento;
    sgNotas.Cells[coOrcamentoId, i + 1]     := _Biblioteca.NFormatN(vNotas[i].orcamento_id);
    sgNotas.Cells[coRetiradaId, i + 1]      := _Biblioteca.NFormatN(vNotas[i].RetiradaId);
    sgNotas.Cells[coEntregaId, i + 1]       := _Biblioteca.NFormatN(vNotas[i].EntregaId);
    sgNotas.Cells[coDevolucaoId, i + 1]     := _Biblioteca.NFormatN(vNotas[i].DevolucaoId);

  end;
  sgNotas.RowCount := IfThen(Length(vNotas) > 1, High(vNotas) + 2, 2);
end;

procedure TFormConsultaNotasFiscais.Imprimir(Sender: TObject);
var
  vNotaFiscalId: Integer;
begin
  inherited;
  vNotaFiscalId := SFormatInt(sgNotas.Cells[coNotaFiscalId, sgNotas.Row]);
  if vNotaFiscalId = 0 then
    Exit;
  if sgNotas.Cells[coStatus, sgNotas.Row] <> 'Emitida' then begin
    Exclamar('� poss�vel somente fazer a impress�o do DANFE de uma nota fiscal eletr�nica j� emitida!');
    Exit;
  end;

  if sgNotas.Cells[coTipoNota, sgNotas.Row] = 'NF-e' then begin
    if _ImpressaoDANFE.Imprimir(vNotaFiscalId) then
      Carregar(Sender);
  end
  else begin
    if Impressao.ComprovanteGraficoNFCe.Imprimir(vNotaFiscalId) then
      Carregar(Sender);
  end;
end;

procedure TFormConsultaNotasFiscais.miCancelarNFeClick(Sender: TObject);
var
  oNFe: TComunicacaoNFe;
  vJustificativa: string;
  vNotaFiscalId: Integer;

  vRetBanco: RecRetornoBD;
begin
  inherited;

  vNotaFiscalId := SFormatInt(sgNotas.Cells[coNotaFiscalId, sgNotas.Row]);
  if vNotaFiscalId = 0 then
    Exit;

  vRetBanco := _NotasFiscais.PodeCancelarNFe(Sessao.getConexaoBanco, vNotaFiscalId);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  vJustificativa := BuscaDados.BuscarDados('Justificativa', '');
  if vJustificativa = '' then begin
    Exclamar('� necess�rio informar a justificativa para cancelamento da NF-e!');
    Exit;
  end;

  if Length(Trim(vJustificativa)) < 15 then begin
    Exclamar('A justificativa para cancelamento da NF-e deve ter no m�nimo 15 caracteres!');
    Exit;
  end;

  oNFe := TComunicacaoNFe.Create(Self);
  if oNFe.CancelarNFe(vNotaFiscalId, vJustificativa) then
    Carregar(Sender);

  oNFe.Free;
end;

procedure TFormConsultaNotasFiscais.miEditarCapaNotaFiscalClick(Sender: TObject);
begin
  inherited;
  case Edicao.CapaNotaFiscal.Editar(SFormatInt(sgNotas.Cells[coNotaFiscalId, sgNotas.Row])).RetTela of
    trOk: begin
      RotinaSucesso;
      Carregar(Sender);
    end;

    trCancelado: begin
      RotinaCanceladaUsuario;
      SetarFoco(sgNotas);
    end;
  end;
end;

procedure TFormConsultaNotasFiscais.miEditarItensNotaFiscalClick(Sender: TObject);
begin
  inherited;
  case Edicao.NotaFiscalItens.Editar(SFormatInt(sgNotas.Cells[coNotaFiscalId, sgNotas.Row])).RetTela of
    trOk: begin
      RotinaSucesso;
      Carregar(Sender);
    end;

    trCancelado: begin
      RotinaCanceladaUsuario;
      SetarFoco(sgNotas);
    end;
  end;
end;

procedure TFormConsultaNotasFiscais.miEmitirNFePendenteClick(Sender: TObject);
var
  vNotaFiscalId: Integer;
begin
  inherited;
  vNotaFiscalId := SFormatInt(sgNotas.Cells[coNotaFiscalId, sgNotas.Row]);
  if vNotaFiscalId = 0 then
    Exit;

  try
    _ComunicacaoNFE.EnviarNFe(vNotaFiscalId);
  finally
    Carregar(Sender);
  end;
end;

procedure TFormConsultaNotasFiscais.miEmitirNotafiscalCupomFiscalClick(Sender: TObject);
//var
//  vRetBanco: RecRespostaBanco;
begin
  inherited;
//  vNotaFiscalId := SFormatInt(sgNotas.Cells[coNotaFiscalId, sgNotas.Row]);
//  if vNotaFiscalId = 0 then
//    Exit;
//
//  vRetBanco := _NotasFiscais.GerarNotaCupomFiscal(Sessao.getConexaoBanco, vNotaFiscalId);
//  if vRetBanco.TeveErro then begin
//    Exclamar(vRetBanco.MensagemErro);
//    Exit;
//  end;

//  if
//    vNFe.EmitirNFe(
//      vNotaFiscalId,
//      0,
//      '',
//      ''
//    )
//  then
//    _ImpressaoDANFE.Imprimir(vNotaFiscalId);


  Carregar(Sender);
end;

procedure TFormConsultaNotasFiscais.miInutilizarNumercaoNFeClick(Sender: TObject);
var
  oNFe: TComunicacaoNFe;
  vDadosInut: TRetornoTelaFinalizar<RecDadosInutilizacao>;
begin
  inherited;
  vDadosInut := BuscarDados.InutilizacaoNFe.BuscarDados();
  if vDadosInut.RetTela = trCancelado then
    Exit;

  oNFe := TComunicacaoNFe.Create(Self);

  if
    oNFe.InutilizarNFe(
      Sessao.getEmpresaLogada.cnpj,
      vDadosInut.Dados.ano,
      vDadosInut.Dados.serie,
      vDadosInut.Dados.numeracao_inicial,
      vDadosInut.Dados.numeracao_final,
      vDadosInut.Dados.justificativa,
      vDadosInut.Dados.modeloNota
    )
  then
    _Biblioteca.Informar('Numera��o inutilizada com sucesso.');

  oNFe.Free;
end;

procedure TFormConsultaNotasFiscais.pmRotinasPopup(Sender: TObject);
begin
  inherited;
  miCancelarNFe.Enabled :=
    (sgNotas.Cells[coStatusNfe, sgNotas.Row] = '100') and
    (sgNotas.Cells[coStatus, sgNotas.Row] = 'Emitida');

  miEmitirNFePendente.Enabled := (sgNotas.Cells[coStatus, sgNotas.Row] = 'N�o emitida');
end;

procedure TFormConsultaNotasFiscais.sgNotasDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.NotaFiscal.Informar(SFormatInt(sgNotas.Cells[coNotaFiscalId, sgNotas.Row]));
end;

procedure TFormConsultaNotasFiscais.sgNotasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coTipoNota, coDanfeImpresso, coEmailEnviado, coStatus] then
    vAlinhamento := taCenter
  else if ACol in[coValor, coStatusNfe, coNumeroNota] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgNotas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormConsultaNotasFiscais.sgNotasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;

  if ARow = 0 then
    Exit;

  if ACol = coTipoNota then begin
    AFont.Style := [fsBold];
    AFont.Color := IfThen(sgNotas.Cells[ACol, ARow] = 'NF-e', clBlue, $000096DB);
  end
  else if ACol = coDanfeImpresso then begin
    AFont.Style := [fsBold];
    AFont.Color := IfThen(sgNotas.Cells[ACol, ARow] = 'N�o', clRed, clBlue);
  end
  else if ACol = coEmailEnviado then begin
    AFont.Style := [fsBold];
    AFont.Color := IfThen(sgNotas.Cells[ACol, ARow] = 'N�o', clRed, clBlue);
  end
  else if ACol = coStatus then begin
    AFont.Style := [fsBold];
    if sgNotas.Cells[ACol, ARow] = 'Emitida' then
      AFont.Color := $000096DB
    else if sgNotas.Cells[ACol, ARow] = 'Cancelada' then
      AFont.Color := clRed
    else if sgNotas.Cells[ACol, ARow] = 'Denegada' then
      AFont.Color := clRed
    else
      AFont.Color := $000080FF;
  end;
end;

end.
