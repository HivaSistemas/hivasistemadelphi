inherited FormRelacaoEstoque: TFormRelacaoEstoque
  Caption = 'Valor de estoque'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    object SpeedButton1: TSpeedButton [2]
      Left = 4
      Top = 214
      Width = 110
      Height = 40
      BiDiMode = bdLeftToRight
      Caption = 'Gerar Planilha'
      Flat = True
      NumGlyphs = 2
      ParentBiDiMode = False
      OnClick = SpeedButton1Click
    end
    object sbAtualizarCMV: TSpeedButton [3]
      Left = 4
      Top = 376
      Width = 110
      Height = 23
      Hint = 
        'Atualizar o CMV do produto com o valor do CMV das outras empresa' +
        's'
      BiDiMode = bdLeftToRight
      Caption = 'Atualizar CMV'
      NumGlyphs = 2
      ParentBiDiMode = False
      Visible = False
      OnClick = sbAtualizarCMVClick
    end
    object sbAjustarVlrEstoque: TSpeedButton [4]
      Left = 4
      Top = 450
      Width = 110
      Height = 25
      Hint = 
        'Atualizar o CMV do produto com o valor do CMV das outras empresa' +
        's'
      BiDiMode = bdLeftToRight
      Caption = 'Ajustar Vlr. Estoque'
      NumGlyphs = 2
      ParentBiDiMode = False
      Visible = False
      OnClick = sbAjustarVlrEstoqueClick
    end
    object lbNovoValor: TLabel [5]
      Left = 4
      Top = 476
      Width = 105
      Height = 14
      Caption = 'Novo valor estoque'
      Visible = False
    end
    object Label3: TLabel [6]
      Left = 6
      Top = 308
      Width = 110
      Height = 14
      Caption = 'Livro Reg. Invent'#225'rio'
    end
    object eValorEstoque: TEditLuka
      Left = 3
      Top = 496
      Width = 107
      Height = 22
      Alignment = taRightJustify
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = '0,00'
      Visible = False
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      PadraoEstoque = True
      NaoAceitarEspaco = False
    end
    object eLivro: TEditLuka
      Left = 7
      Top = 327
      Width = 107
      Height = 22
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      TipoCampo = tcTexto
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 0
      PadraoEstoque = True
      NaoAceitarEspaco = False
    end
  end
  inherited pcDados: TPageControl
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object lb1: TLabel
        Left = 330
        Top = 43
        Width = 154
        Height = 14
        Caption = 'Tipo de controle de estoque'
      end
      object lb2: TLabel
        Left = 330
        Top = 84
        Width = 26
        Height = 14
        Caption = 'Ativo'
      end
      object lb3: TLabel
        Left = 330
        Top = 126
        Width = 129
        Height = 14
        Caption = 'Quantidade de estoque'
      end
      object Label2: TLabel
        Left = 393
        Top = 237
        Width = 24
        Height = 14
        Caption = 'Ano:'
      end
      object Label1: TLabel
        Left = 332
        Top = 236
        Width = 26
        Height = 14
        Caption = 'M'#234's:'
      end
      object Label4: TLabel
        Left = 330
        Top = 169
        Width = 118
        Height = 14
        Caption = 'Produtos pais e filhos'
      end
      inline FrDataCadastroProduto: TFrDataInicialFinal
        Left = 328
        Top = -1
        Width = 217
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 328
        ExplicitTop = -1
        ExplicitWidth = 217
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 156
          Height = 14
          Caption = 'Data de cadastro do produto'
          ExplicitWidth = 156
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas
        Left = 2
        Top = 130
        Width = 319
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 130
        ExplicitWidth = 319
        ExplicitHeight = 81
        inherited sgPesquisa: TGridLuka
          Width = 294
          Height = 64
          ExplicitWidth = 294
          ExplicitHeight = 64
        end
        inherited PnTitulos: TPanel
          Width = 319
          ExplicitWidth = 319
          inherited lbNomePesquisa: TLabel
            Width = 98
            Caption = 'Grupo de estoque'
            ExplicitWidth = 98
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 214
            ExplicitLeft = 214
          end
        end
        inherited pnPesquisa: TPanel
          Left = 294
          Height = 65
          ExplicitLeft = 294
          ExplicitHeight = 65
        end
      end
      object cbTipoControleEstoque: TComboBoxLuka
        Left = 330
        Top = 57
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 5
        TabOrder = 5
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'Normal'
          'Lote'
          'Piso'
          'Grade'
          'Kit'
          'N'#227'o filtrar')
        Valores.Strings = (
          'N'
          'L'
          'P'
          'G'
          'K'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object cbAtivo: TComboBoxLuka
        Left = 330
        Top = 98
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 2
        TabOrder = 6
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'Sim'
          'N'#227'o'
          'N'#227'o filtrar')
        Valores.Strings = (
          'S'
          'N'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      inline FrProdutos: TFrProdutos
        Left = 2
        Top = 45
        Width = 320
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 45
        ExplicitWidth = 320
        inherited sgPesquisa: TGridLuka
          Width = 295
          ExplicitWidth = 295
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          ExplicitLeft = 295
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrEmpresa: TFrEmpresas
        Left = 2
        Top = 0
        Width = 318
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitWidth = 318
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 293
          Height = 24
          ExplicitWidth = 293
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          Width = 318
          ExplicitWidth = 318
          inherited lbNomePesquisa: TLabel
            Width = 47
            Caption = 'Empresa'
            ExplicitWidth = 47
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 213
            ExplicitLeft = 213
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 293
          Height = 25
          ExplicitLeft = 293
          ExplicitHeight = 25
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      object rgTipoCusto: TRadioGroupLuka
        Left = 531
        Top = 1
        Width = 185
        Height = 74
        Caption = ' Tipo de custo '
        ItemIndex = 1
        Items.Strings = (
          'Custo da ultima entrada'
          'Custo de compra'
          'CMV')
        TabOrder = 7
        OnClick = rgTipoCustoClick
        Valores.Strings = (
          'FIN'
          'COM'
          'CMV')
      end
      object rgTipoEstoque: TRadioGroupLuka
        Left = 531
        Top = 81
        Width = 185
        Height = 106
        Caption = ' Tipo de estoque '
        ItemIndex = 0
        Items.Strings = (
          'F'#237'sico'
          'Dispon'#237'vel'
          'Reservado'
          'Fiscal'
          'F'#237'sico x Fiscal')
        TabOrder = 8
        Valores.Strings = (
          'FIS'
          'DIS'
          'RES'
          'FISCAL'
          'FISXFIS')
      end
      inline FrMarcas: TFrMarcas
        Left = 2
        Top = 215
        Width = 320
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 215
        ExplicitWidth = 320
        inherited sgPesquisa: TGridLuka
          Width = 295
          ExplicitWidth = 295
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 39
            ExplicitWidth = 39
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          ExplicitLeft = 295
        end
      end
      object cbQuantidadeEmEstoque: TComboBoxLuka
        Left = 330
        Top = 140
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 3
        TabOrder = 9
        Text = 'N'#227'o filtrar'
        Items.Strings = (
          'Positivo'
          'Negativo'
          'Zerado'
          'N'#227'o filtrar')
        Valores.Strings = (
          'POS'
          'NEG'
          'ZERO'
          'NaoFiltrar')
        AsInt = 0
        AsString = 'NaoFiltrar'
      end
      object eAno: TEditLuka
        Left = 393
        Top = 252
        Width = 122
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 4
        NumbersOnly = True
        TabOrder = 12
        TipoCampo = tcTexto
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        ValorMaximo = 2099.000000000000000000
        NaoAceitarEspaco = False
      end
      object eMes: TEditLuka
        Left = 332
        Top = 252
        Width = 55
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 2
        NumbersOnly = True
        TabOrder = 10
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        ValorMaximo = 12.000000000000000000
        NaoAceitarEspaco = False
      end
      object StaticText4: TStaticText
        Left = 327
        Top = 215
        Width = 188
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Valor de estoque retroativo'
        Color = 15395562
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 11
        Transparent = False
      end
      object cbPaiFilho: TComboBoxLuka
        Left = 330
        Top = 183
        Width = 185
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 13
        Text = 'Trazer todos os produtos'
        Items.Strings = (
          'Trazer todos os produtos'
          'Somente filhos'
          'Somente pais')
        Valores.Strings = (
          'T'
          'F'
          'P')
        AsInt = 0
        AsString = 'T'
      end
      inline FrLocais: TFrLocais
        Left = 2
        Top = 305
        Width = 319
        Height = 79
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 14
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 305
        ExplicitWidth = 319
        ExplicitHeight = 79
        inherited sgPesquisa: TGridLuka
          Width = 294
          Height = 62
          ExplicitWidth = 294
          ExplicitHeight = 62
        end
        inherited CkAspas: TCheckBox
          Top = 60
          ExplicitTop = 60
        end
        inherited CkFiltroDuplo: TCheckBox
          Top = 23
          ExplicitTop = 23
        end
        inherited CkPesquisaNumerica: TCheckBox
          Top = 40
          ExplicitTop = 40
        end
        inherited PnTitulos: TPanel
          Width = 319
          ExplicitWidth = 319
          inherited lbNomePesquisa: TLabel
            Width = 94
            Caption = 'Local do produto:'
            ExplicitWidth = 94
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 214
            ExplicitLeft = 214
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 294
          Height = 63
          ExplicitLeft = 294
          ExplicitHeight = 63
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object sgItens: TGridLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 470
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 10
        Ctl3D = True
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        GradientEndColor = 15395562
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goThumbTracking]
        ParentCtl3D = False
        TabOrder = 0
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Und.'
          'Marca'
          'NCM'
          'Estoque'
          'Estoque fiscal'
          'Custo unit.'
          'Valor'
          'Valor fiscal')
        OnGetCellColor = sgItensGetCellColor
        Grid3D = False
        RealColCount = 60
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          52
          320
          34
          182
          88
          74
          90
          88
          98
          98)
      end
      object TPanel
        Left = 0
        Top = 470
        Width = 884
        Height = 48
        Align = alBottom
        TabOrder = 1
        object stQtdeTotalEstoque: TStaticTextLuka
          Left = 157
          Top = 32
          Width = 156
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,000'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Transparent = False
          AsInt = 0
          TipoCampo = tcNumerico
          CasasDecimais = 3
        end
        object st4: TStaticText
          Left = 157
          Top = 17
          Width = 156
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Qtde. total estoque'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          Transparent = False
        end
        object stValorTotalEstoque: TStaticTextLuka
          Left = 312
          Top = 32
          Width = 147
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = '0,0000'
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          Transparent = False
          AsInt = 0
          TipoCampo = tcNumerico
          CasasDecimais = 4
        end
        object st3: TStaticText
          Left = 312
          Top = 17
          Width = 147
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Valor total estoque'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 3
          Transparent = False
        end
        object st1: TStaticTextLuka
          Left = 1
          Top = 0
          Width = 458
          Height = 17
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          Caption = 'Totais'
          Color = 38619
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 4
          Transparent = False
          AsInt = 0
          TipoCampo = tcTexto
          CasasDecimais = 0
        end
        object st2: TStaticText
          Left = 2
          Top = 17
          Width = 157
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Caption = 'Qtde. produtos'
          Color = 15395562
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 5
          Transparent = False
        end
        object stQtdeProdutos: TStaticTextLuka
          Left = 1
          Top = 32
          Width = 157
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clTeal
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 6
          Transparent = False
          AsInt = 0
          TipoCampo = tcNumerico
          CasasDecimais = 0
        end
        object Panel1: TPanel
          Left = 459
          Top = 0
          Width = 425
          Height = 48
          Caption = 'Panel1'
          TabOrder = 7
          object stTotaisFiscal: TStaticTextLuka
            Left = -33
            Top = 0
            Width = 458
            Height = 17
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            Caption = 'Totais fiscal'
            Color = 38619
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 0
            Transparent = False
            AsInt = 0
            TipoCampo = tcTexto
            CasasDecimais = 0
          end
          object StaticText1: TStaticText
            Left = 0
            Top = 17
            Width = 126
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Qtde. produtos'
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 1
            Transparent = False
          end
          object StaticText2: TStaticText
            Left = 125
            Top = 17
            Width = 156
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Qtde. total estoque'
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 2
            Transparent = False
          end
          object StaticText3: TStaticText
            Left = 278
            Top = 17
            Width = 147
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = 'Valor total estoque'
            Color = 15395562
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 3
            Transparent = False
          end
          object stValorTotalEstoqueFiscal: TStaticTextLuka
            Left = 278
            Top = 32
            Width = 147
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = '0,0000'
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 4
            Transparent = False
            AsInt = 0
            TipoCampo = tcNumerico
            CasasDecimais = 4
          end
          object stQtdeTotalEstoqueFiscal: TStaticTextLuka
            Left = 125
            Top = 32
            Width = 154
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Caption = '0,000'
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 5
            Transparent = False
            AsInt = 0
            TipoCampo = tcNumerico
            CasasDecimais = 3
          end
          object stQtdeProdutosFiscal: TStaticTextLuka
            Left = 0
            Top = 32
            Width = 126
            Height = 16
            Alignment = taCenter
            AutoSize = False
            BevelInner = bvNone
            BevelKind = bkFlat
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clTeal
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentColor = False
            ParentFont = False
            TabOrder = 6
            Transparent = False
            AsInt = 0
            TipoCampo = tcNumerico
            CasasDecimais = 0
          end
        end
      end
    end
  end
  object frxReport: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 44649.918901203700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 776
    Top = 312
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstReceber
        DataSetName = 'frxdstReceber'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 154.960730000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Left = -2.779530000000000000
          Top = 116.165430000000000000
          Width = 721.890230000000000000
          Height = 38.551181102362200000
          Fill.BackColor = cl3DLight
        end
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 52.913420000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'EMPRESA:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 75.590600000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'INSC.EST.:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 75.590600000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 52.913420000000000000
          Width = 453.543600000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 75.590600000000000000
          Width = 200.315090000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 75.590600000000000000
          Width = 207.874150000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."INSCRICAO_ESTADUAL"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = -3.000000000000000000
          Top = 116.165430000000000000
          Width = 68.787404020000000000
          Height = 37.795275590000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 65.252010000000000000
          Top = 116.165430000000000000
          Width = 238.110390000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Discrimina'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 427.086890000000000000
          Top = 116.165430000000000000
          Width = 181.417440000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Valores')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 610.283860000000000000
          Top = 116.165430000000000000
          Width = 107.716523230000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 3.779530000000000000
          Width = 721.890230000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LIVRO REGISTRO DE INVENT'#193'RIO - RI - MODELO P7')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Top = 26.456710000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo4: TfrxMemoView
          Left = -3.779530000000000000
          Top = 30.236240000000000000
          Width = 721.890230000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'REGISTRO DE INVENT'#193'RIO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 304.362400000000000000
          Top = 116.165430000000000000
          Width = 49.133890000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 354.496290000000000000
          Top = 116.165430000000000000
          Width = 71.811070000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 427.086890000000000000
          Top = 135.062979920000000000
          Width = 81.259842520000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Unit'#225'rio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 508.457020000000000000
          Top = 135.063080000000000000
          Width = 100.157492520000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'Total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Page: TfrxMemoView
          Left = 83.149660000000000000
          Top = 94.488196300000000000
          Width = 60.472480000000000000
          Height = 15.118112680000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'ARial'
          Font.Style = []
          Memo.UTF8W = (
            '[(<Page#>) + 1]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 7.559060000000000000
          Top = 94.488250000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'FOLHA:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 147.401670000000000000
          Top = 94.488250000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'LIVRO:')
          ParentFont = False
        end
        object mmValorLivro: TfrxMemoView
          Left = 192.756030000000000000
          Top = 94.488250000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '005-E')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 340.157700000000000000
          Top = 94.488250000000000000
          Width = 177.637910000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'ESTOQUES EXISTENTES EM:')
          ParentFont = False
        end
        object mmDataEstoque: TfrxMemoView
          Left = 517.795610000000000000
          Top = 94.488250000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '31/03/2022')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 234.330860000000000000
        Width = 718.110700000000000000
        DataSet = dstReceber
        DataSetName = 'frxdstReceber'
        RowCount = 0
        object frxdstTranscodigoProd: TfrxMemoView
          Top = 1.000000000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          DataField = 'ProdutoId'
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstReceber."ProdutoId"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 65.385826770000000000
          Top = 1.000000000000000000
          Width = 238.110236220000000000
          Height = 15.118120000000000000
          DataField = 'ProdutoNome'
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstReceber."ProdutoNome"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 508.346456690000000000
          Top = 1.000000000000000000
          Width = 81.259842520000000000
          Height = 15.118120000000000000
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstReceber."CustoTotal"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 304.251968500000000000
          Top = 1.000000000000000000
          Width = 49.133858270000000000
          Height = 15.118120000000000000
          DataField = 'Unidade'
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstReceber."Unidade"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 427.086614170000000000
          Top = 1.000000000000000000
          Width = 81.259842520000000000
          Height = 15.118120000000000000
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstReceber."CustoUnitario"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 354.519685040000000000
          Top = 1.000000000000000000
          Width = 71.811023620000000000
          Height = 15.118120000000000000
          DataField = 'Estoque'
          DataSet = dstReceber
          DataSetName = 'frxdstReceber'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstReceber."Estoque"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 79.748082760000000000
        Top = 313.700990000000000000
        Width = 718.110700000000000000
        object Line1: TfrxLineView
          Top = 3.779530000000000000
          Width = 718.110700000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object mmValorProduto: TfrxMemoView
          Left = 64.252010000000000000
          Top = 7.559060000000000000
          Width = 177.637910000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Line3: TfrxLineView
          Left = -3.779530000000000000
          Top = 3.779530000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object mmTotalUnitario: TfrxMemoView
          Left = 427.086890000000000000
          Top = 7.559060000000000000
          Width = 81.259842520000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object mmTotal: TfrxMemoView
          Left = 509.236550000000000000
          Top = 7.559060000000000000
          Width = 100.157492520000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Line4: TfrxLineView
          Top = 30.236240000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo22: TfrxMemoView
          Top = 34.015770000000000000
          Width = 177.637910000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'OBS:')
          ParentFont = False
        end
        object Line5: TfrxLineView
          Top = 75.590600000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 415.748300000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
    end
  end
  object dstReceber: TfrxDBDataset
    UserName = 'frxdstReceber'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ProdutoId=ProdutoId'
      'ProdutoNome=ProdutoNome'
      'Unidade=Unidade'
      'Estoque=Estoque'
      'CustoUnitario=CustoUnitario'
      'CustoTotal=CustoTotal')
    DataSet = cdsReceber
    BCDToCurrency = False
    Left = 776
    Top = 376
  end
  object cdsReceber: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ProdutoId'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ProdutoNome'
        DataType = ftString
        Size = 200
      end
      item
        Name = 'Unidade'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Estoque'
        DataType = ftFloat
      end
      item
        Name = 'CustoUnitario'
        DataType = ftFloat
      end
      item
        Name = 'CustoTotal'
        DataType = ftFloat
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 776
    Top = 440
    object cdsReceberProdutoId: TStringField
      FieldName = 'ProdutoId'
    end
    object cdsReceberProdutoNome: TStringField
      FieldName = 'ProdutoNome'
      Size = 200
    end
    object cdsReceberUnidade: TStringField
      FieldName = 'Unidade'
    end
    object cdsReceberEstoque: TFloatField
      FieldName = 'Estoque'
    end
    object cdsReceberCustoUnitario: TFloatField
      FieldName = 'CustoUnitario'
    end
    object cdsReceberCustoTotal: TFloatField
      FieldName = 'CustoTotal'
    end
  end
end
