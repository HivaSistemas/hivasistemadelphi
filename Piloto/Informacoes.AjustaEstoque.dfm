inherited FormInformacoesAjusteEstoque: TFormInformacoesAjusteEstoque
  Caption = 'Informa'#231#245'es de ajuste de estoque'
  ClientHeight = 432
  ClientWidth = 792
  ExplicitWidth = 798
  ExplicitHeight = 461
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 4
    Top = 3
    Width = 37
    Height = 14
    Caption = 'C'#243'digo'
  end
  object lbl1: TLabel [1]
    Left = 510
    Top = 3
    Width = 43
    Height = 14
    Caption = 'Usu'#225'rio'
  end
  object lbllb3: TLabel [2]
    Left = 690
    Top = 3
    Width = 63
    Height = 14
    Caption = 'Data ajuste'
  end
  object lb2: TLabel [3]
    Left = 77
    Top = 3
    Width = 90
    Height = 14
    Caption = 'Motivo do ajuste'
  end
  object lb3: TLabel [4]
    Left = 324
    Top = 3
    Width = 47
    Height = 14
    Caption = 'Empresa'
  end
  object lbl26: TLabel [5]
    Left = 4
    Top = 314
    Width = 69
    Height = 14
    Caption = 'Observa'#231#245'es'
  end
  inherited pnOpcoes: TPanel
    Top = 395
    Width = 792
    ExplicitTop = 395
    ExplicitWidth = 792
  end
  object eAjusteId: TEditLuka
    Left = 4
    Top = 18
    Width = 67
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eUsuarioAjuste: TEditLuka
    Left = 510
    Top = 18
    Width = 175
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataAjuste: TEditLukaData
    Left = 690
    Top = 18
    Width = 98
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    ReadOnly = True
    TabOrder = 3
    Text = '  /  /    '
  end
  object eMotivoAjuste: TEditLuka
    Left = 76
    Top = 18
    Width = 243
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 4
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmpresa: TEditLuka
    Left = 324
    Top = 18
    Width = 181
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 5
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object sgProdutos: TGridLuka
    Left = 4
    Top = 44
    Width = 784
    Height = 268
    ColCount = 8
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing, goRowSelect]
    TabOrder = 6
    OnDrawCell = sgProdutosDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Lote'
      'Quantidade'
      'Pre'#231'o unit.'
      'Unidade'
      'Valor Total')
    Grid3D = False
    RealColCount = 10
    AtivarPopUpSelecao = False
    ColWidths = (
      53
      181
      150
      111
      74
      68
      58
      74)
  end
  object eObservacoes: TMemo
    Left = 5
    Top = 328
    Width = 645
    Height = 64
    TabOrder = 7
  end
  object stQuantidadeItensGrid: TStaticText
    Left = 656
    Top = 343
    Width = 132
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    Transparent = False
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 656
    Top = 328
    Width = 132
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Quantidade de itens'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 9
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object StaticTextLuka2: TStaticTextLuka
    Left = 656
    Top = 359
    Width = 132
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Valor total'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stValorTotalProdutosGrid: TStaticTextLuka
    Left = 656
    Top = 376
    Width = 132
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 11
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
end
