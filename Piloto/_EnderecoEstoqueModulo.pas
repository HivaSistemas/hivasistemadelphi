unit _EnderecoEstoqueModulo;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _Sessao, _RecordsCadastros;

{$M+}
type
  TEnderecoEstoqueModulo = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEnderecoEstoqueModulo: RecEnderecoEstoqueModulo;
  end;

function AtualizarEnderecoEstoqueModulo(
  pConexao: TConexao;
  pModuloId: Integer;
  pDescricao: string
): RecRetornoBD;

function BuscarEnderecoEstoqueModulo(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEnderecoEstoqueModulo>;

function ExcluirEnderecoEstoqueModulo(
  pConexao: TConexao;
  pModuloId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEnderecoEstoqueModulo }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where MODULO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o',
      True,
      0,
      'where DESCRICAO like :P1 || ''%'' ' +
      'order by ' +
      '  DESCRICAO '
    );
end;

constructor TEnderecoEstoqueModulo.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENDERECO_EST_MODULO');

  FSql :=
    'select ' +
    '  MODULO_ID, ' +
    '  DESCRICAO ' +
    'from ' +
    '  ENDERECO_EST_MODULO ';

  setFiltros(getFiltros);

  AddColuna('MODULO_ID', True);
  AddColuna('DESCRICAO');
end;

function TEnderecoEstoqueModulo.getRecordEnderecoEstoqueModulo: RecEnderecoEstoqueModulo;
begin
  Result := RecEnderecoEstoqueModulo.Create;
  Result.modulo_id     := getInt('MODULO_ID', True);
  Result.descricao  := getString('DESCRICAO');
end;

function AtualizarEnderecoEstoqueModulo(
  pConexao: TConexao;
  pModuloId: Integer;
  pDescricao: string
): RecRetornoBD;
var
  t: TEnderecoEstoqueModulo;
  vNovo: Boolean;
  seq: TSequencia;
begin
  Result.Iniciar;
  t := TEnderecoEstoqueModulo.Create(pConexao);

  vNovo := pModuloId = 0;
  if vNovo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_END_EST_MOD_ID');
    pModuloId := seq.getProximaSequencia;
    result.AsInt := pModuloId;
    seq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    t.setInt('MODULO_ID', pModuloId, True);
    t.setString('DESCRICAO', pDescricao);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarEnderecoEstoqueModulo(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEnderecoEstoqueModulo>;
var
  i: Integer;
  t: TEnderecoEstoqueModulo;
begin
  Result := nil;
  t := TEnderecoEstoqueModulo.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEnderecoEstoqueModulo;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirEnderecoEstoqueModulo(
  pConexao: TConexao;
  pModuloId: Integer
): RecRetornoBD;
var
  t: TEnderecoEstoqueModulo;
begin
  Result.TeveErro := False;
  t := TEnderecoEstoqueModulo.Create(pConexao);

  try
    t.setInt('MODULO_ID', pModuloId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
