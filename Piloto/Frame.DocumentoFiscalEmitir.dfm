inherited FrameDocumentoFiscalEmitir: TFrameDocumentoFiscalEmitir
  Width = 201
  Height = 92
  ExplicitWidth = 201
  ExplicitHeight = 92
  object gbDocumentosEmitir: TGroupBox
    Left = 5
    Top = 1
    Width = 189
    Height = 86
    Caption = '  Documento a emitir  '
    TabOrder = 0
    object ckRecibo: TCheckBoxLuka
      Left = 8
      Top = 18
      Width = 145
      Height = 17
      Caption = 'Recibo de entrega'
      TabOrder = 0
      CheckedStr = 'N'
    end
    object ckEmitirNFCe: TCheckBoxLuka
      Left = 8
      Top = 39
      Width = 129
      Height = 17
      Caption = 'Cupom Fiscal (NFC-e)'
      TabOrder = 1
      CheckedStr = 'N'
    end
    object ckEmitirNFe: TCheckBoxLuka
      Left = 8
      Top = 60
      Width = 105
      Height = 17
      Caption = 'Nota Fiscal (NF-e)'
      TabOrder = 2
      CheckedStr = 'N'
    end
  end
end
