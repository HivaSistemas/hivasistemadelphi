unit CadastroAdministradorasCartoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.Mask, _Biblioteca, _Sessao,
  EditCpfCnpjLuka, Vcl.StdCtrls, CheckBoxLuka, EditLuka, Vcl.Buttons, _AdministradorasCartoes,
  Vcl.ExtCtrls, _RecordsEspeciais, PesquisaAdministradorasCartoes;

type
  TFormCadastroAdministradorasCartoes = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
    lbCPF_CNPJ: TLabel;
    eCPF_CNPJ: TEditCPF_CNPJ_Luka;
  private
    procedure PreencherRegistro(pAdmin: RecAdministradorasCartoes);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroAdministradorasCartoes }

procedure TFormCadastroAdministradorasCartoes.BuscarRegistro;
var
  vDados: TArray<RecAdministradorasCartoes>;
begin
  vDados := _AdministradorasCartoes.BuscarAdministradorasCartoes(Sessao.getConexaoBanco, 0, [eId.AsInt], False);
  if vDados = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vDados[0]);
end;

procedure TFormCadastroAdministradorasCartoes.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _AdministradorasCartoes.ExcluirAdministradorasCartoes(Sessao.getConexaoBanco, eId.AsInt);
  Sessao.AbortarSeHouveErro(vRetBanco);

  inherited;
end;

procedure TFormCadastroAdministradorasCartoes.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _AdministradorasCartoes.AtualizarAdministradorasCartoes(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text,
      eCPF_CNPJ.Text,
      ckAtivo.CheckedStr
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroAdministradorasCartoes.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eDescricao,
    eCPF_CNPJ,
    ckAtivo],
    pEditando
  );

  if pEditando then begin
    ckAtivo.Checked := True;
    SetarFoco(eDescricao);
  end;
end;

procedure TFormCadastroAdministradorasCartoes.PesquisarRegistro;
var
  vDados: RecAdministradorasCartoes;
begin
  vDados := RecAdministradorasCartoes(PesquisaAdministradorasCartoes.Pesquisar(False));
  if vDados = nil then
    Exit;

  inherited;
  PreencherRegistro(vDados);
end;

procedure TFormCadastroAdministradorasCartoes.PreencherRegistro(pAdmin: RecAdministradorasCartoes);
begin
  eID.AsInt          := pAdmin.AdministradoraId;
  eDescricao.Text    := pAdmin.Descricao;
  ckAtivo.CheckedStr := pAdmin.Ativo;
  eCPF_CNPJ.Text     := pAdmin.Cnpj;
end;

procedure TFormCadastroAdministradorasCartoes.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o da operadora n�o foi informada corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;

  if not eCPF_CNPJ.getCPF_CNPJOk then begin
    _Biblioteca.Exclamar('O CNPJ da administradora n�o foi informada corretamente, verifique!');
    SetarFoco(eCPF_CNPJ);
    Abort;
  end;
end;

end.
