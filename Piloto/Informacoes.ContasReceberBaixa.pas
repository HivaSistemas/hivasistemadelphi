unit Informacoes.ContasReceberBaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Mask, EditLukaData, Vcl.StdCtrls,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Biblioteca, System.Math, System.StrUtils,
  _ContasReceberBaixas, _ContasReceberBaixasItens, _RecordsFinanceiros, _Sessao, Informacoes.TurnoCaixa,
  Vcl.ComCtrls, StaticTextLuka, _ContasRecBaixasPagtosDin, _ContasReceber, InformacoesCreditosUtilizados;

type
  TFormInformacoesContasReceberBaixa = class(TFormHerancaFinalizar)
    pcDados: TPageControl;
    tsGerais: TTabSheet;
    lb7: TLabel;
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    lb14: TLabel;
    lb5: TLabel;
    lb4: TLabel;
    lb8: TLabel;
    lb10: TLabel;
    lb15: TLabel;
    lb16: TLabel;
    lb17: TLabel;
    lb18: TLabel;
    lb6: TLabel;
    sbInfoOrcamentoId: TSpeedButton;
    sbCreditos: TSpeedButton;
    lb9: TLabel;
    sbInformacoesBaixa: TSpeedButton;
    lb19: TLabel;
    eBaixaId: TEditLuka;
    eUsuarioBaixa: TEditLuka;
    eDataPagamento: TEditLukaData;
    eDataBaixa: TEditLukaData;
    eValorDinheiro: TEditLuka;
    eValorCheque: TEditLuka;
    eValorCartao: TEditLuka;
    eValorCobranca: TEditLuka;
    stValores: TStaticText;
    eValorCredito: TEditLuka;
    eObservacoes: TMemo;
    eEmpresaBaixa: TEditLuka;
    eUsuarioEnviouCaixa: TEditLuka;
    eValorBruto: TEditLuka;
    eValorJuros: TEditLuka;
    eValorDesconto: TEditLuka;
    eValorL�quido: TEditLuka;
    eTurnoId: TEditLuka;
    st3: TStaticText;
    stReceberCaixa: TStaticText;
    st1: TStaticTextLuka;
    stTipo: TStaticText;
    eBaixaPagarOrigemId: TEditLuka;
    eValorRetencao: TEditLuka;
    tsContasRecebimentoDinheiro: TTabSheet;
    sgContasDinheiro: TGridLuka;
    tsTitulosBaixados: TTabSheet;
    sgTitulosBaixados: TGridLuka;
    tsTitulosGerados: TTabSheet;
    sgTitulosGerados: TGridLuka;
    eValorMulta: TEditLuka;
    lb20: TLabel;
    eValorAdiantado: TEditLuka;
    lb21: TLabel;
    Label1: TLabel;
    eValorPix: TEditLuka;
    SpeedButton1: TSpeedButton;
    procedure sgTitulosBaixadosDblClick(Sender: TObject);
    procedure sgTitulosBaixadosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbInfoOrcamentoIdClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sgContasDinheiroDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTitulosGeradosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgTitulosGeradosDblClick(Sender: TObject);
    procedure sbCreditosClick(Sender: TObject);
    procedure sbInformacoesBaixaClick(Sender: TObject);
  end;

procedure Informar(pBaixaId: Integer);

implementation

uses
  Informacoes.TituloReceber, InformacoesContasPagarBaixa;

{$R *.dfm}

const
  coReceberId      = 0;
  coCliente        = 1;
  coDocumento      = 2;
  coDataVencimento = 3;
  coValor          = 4;
  coTipoCobranca   = 5;

  (* Grid de contas com os recebimentos em dinheiro *)
  cdContaId      = 0;
  cdDescricao    = 1;
  cdValor        = 2;

  (* Grid de t�tulos gerados *)
  cgReceberId      = 0;
  cgCliente        = 1;
  cgDocumento      = 2;
  cgDataVencimento = 3;
  cgValor          = 4;
  cgTipoCobranca   = 5;

procedure Informar(pBaixaId: Integer);
var
  i: Integer;
  vForm: TFormInformacoesContasReceberBaixa;
  vDadosBaixa: TArray<RecContaReceberBaixa>;
  vItensBaixa: TArray<RecContaReceberBaixaItem>;
  vPagamentosDin: TArray<RecTitulosBaixasPagtoDin>;

  vFinancGerados: TArray<RecContasReceber>;
begin
  if pBaixaId = 0 then
    Exit;

  vDadosBaixa := _ContasReceberBaixas.BuscarContaReceberBaixas(Sessao.getConexaoBanco, 0, [pBaixaId]);
  if vDadosBaixa = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vItensBaixa := _ContasReceberBaixasItens.BuscarContaReceberBaixaItens(Sessao.getConexaoBanco, 0, [pBaixaId]);
  if vDadosBaixa = nil then begin
    Exclamar('Os itens da baixa n�o foram encontrados!');
    Exit;
  end;

  vForm := TFormInformacoesContasReceberBaixa.Create(Application);

  vForm.ReadOnlyTodosObjetos(True);
  vForm.eBaixaId.AsInt         := vDadosBaixa[0].BaixaId;
  vForm.stReceberCaixa.Caption := _Biblioteca.SimNao(vDadosBaixa[0].ReceberCaixa);
  vForm.stReceberCaixa.Font.Color := _Biblioteca.AzulVermelho(vForm.stReceberCaixa.Caption);
  vForm.eUsuarioEnviouCaixa.SetInformacao(vDadosBaixa[0].UsuarioEnvioCaixaId, vDadosBaixa[0].NomeUsuarioEnvCaixa);
  vForm.eUsuarioBaixa.SetInformacao(vDadosBaixa[0].usuario_baixa_id, vDadosBaixa[0].nome_usuario_baixa);
  vForm.eDataPagamento.AsData  := vDadosBaixa[0].data_pagamento;
  vForm.eDataBaixa.AsData      := vDadosBaixa[0].data_hora_baixa;
  vForm.eEmpresaBaixa.Text     := NFormat(vDadosBaixa[0].EmpresaId) + ' - ' + vDadosBaixa[0].nome_empresa_baixa;
  vForm.stTipo.Caption         := vDadosBaixa[0].TipoAnalitico;
  vForm.eBaixaPagarOrigemId.SetInformacao(vDadosBaixa[0].BaixaPagarOrigemId);

  vForm.eValorBruto.AsCurr     := vDadosBaixa[0].ValorTitulos;
  vForm.eValorMulta.AsCurr     := vDadosBaixa[0].ValorMulta;
  vForm.eValorJuros.AsCurr     := vDadosBaixa[0].ValorJuros;
  vForm.eValorRetencao.AsCurr  := vDadosBaixa[0].ValorRetencao;
  vForm.eValorDesconto.AsCurr  := vDadosBaixa[0].valor_desconto;
  vForm.eValorAdiantado.AsCurr := vDadosBaixa[0].ValorAdiantado;
  vForm.eValorL�quido.AsCurr   := vDadosBaixa[0].ValorLiquido;
  vForm.eTurnoId.SetInformacao(vDadosBaixa[0].TurnoId);

  vForm.eValorDinheiro.AsCurr := vDadosBaixa[0].valor_dinheiro;
  vForm.eValorCheque.AsCurr   := vDadosBaixa[0].valor_cheque;
  vForm.eValorCartao.AsCurr   := vDadosBaixa[0].ValorCartaoDebito + vDadosBaixa[0].ValorCartaoCredito;
  vForm.eValorCobranca.AsCurr := vDadosBaixa[0].valor_cobranca;
  vForm.eValorCredito.AsCurr  := vDadosBaixa[0].valor_credito;
  vForm.eValorPix.AsCurr      := vDadosBaixa[0].valor_pix;

  vForm.eObservacoes.Text     := vDadosBaixa[0].observacoes;

  vPagamentosDin := _ContasRecBaixasPagtosDin.BuscarContasRecBaixasPagtoDin(Sessao.getConexaoBanco, 0, [pBaixaId]);
  for i := Low(vPagamentosDin) to High(vPagamentosDin) do begin
    vForm.sgContasDinheiro.Cells[cdContaId, i + 1]   := vPagamentosDin[i].ContaId;
    vForm.sgContasDinheiro.Cells[cdDescricao, i + 1] := vPagamentosDin[i].Descricao;
    vForm.sgContasDinheiro.Cells[cdValor, i + 1]     := NFormat(vPagamentosDin[i].Valor);
  end;
  vForm.sgContasDinheiro.SetLinhasGridPorTamanhoVetor( Length(vPagamentosDin) );

  for i := Low(vItensBaixa) to High(vItensBaixa) do begin
    vForm.sgTitulosBaixados.Cells[coReceberId, i + 1]      := NFormat(vItensBaixa[i].receber_id);
    vForm.sgTitulosBaixados.Cells[coCliente, i + 1]        := NFormat(vItensBaixa[i].CadastroId) + ' - ' + vItensBaixa[i].nome_cliente;
    vForm.sgTitulosBaixados.Cells[coDocumento, i + 1]      := vItensBaixa[i].documento;
    vForm.sgTitulosBaixados.Cells[coDataVencimento, i + 1] := DFormat(vItensBaixa[i].data_vencimento);
    vForm.sgTitulosBaixados.Cells[coValor, i + 1]          := NFormat(vItensBaixa[i].valor_documento);
    vForm.sgTitulosBaixados.Cells[coTipoCobranca, i + 1]   := NFormat(vItensBaixa[i].CobrancaId) + ' - ' + vItensBaixa[i].NomeTipoCobranca;
  end;
  vForm.sgTitulosBaixados.SetLinhasGridPorTamanhoVetor( Length(vItensBaixa) );

  vFinancGerados := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 5, [pBaixaId]);
  for i := Low(vFinancGerados) to High(vFinancGerados) do begin
    vForm.sgTitulosGerados.Cells[cgReceberId, i + 1]      := NFormat(vFinancGerados[i].ReceberId);
    vForm.sgTitulosGerados.Cells[cgCliente, i + 1]        := NFormat(vFinancGerados[i].CadastroId) + ' - ' + vFinancGerados[i].NomeCliente;
    vForm.sgTitulosGerados.Cells[cgDocumento, i + 1]      := vFinancGerados[i].documento;
    vForm.sgTitulosGerados.Cells[cgDataVencimento, i + 1] := DFormat(vFinancGerados[i].data_vencimento);
    vForm.sgTitulosGerados.Cells[cgValor, i + 1]          := NFormat(vFinancGerados[i].ValorDocumento);
    vForm.sgTitulosGerados.Cells[cgTipoCobranca, i + 1]   := NFormat(vFinancGerados[i].cobranca_id) + ' - ' + vFinancGerados[i].nome_tipo_cobranca;
  end;
  vForm.sgTitulosGerados.SetLinhasGridPorTamanhoVetor( Length(vFinancGerados) );

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesContasReceberBaixa.FormCreate(Sender: TObject);
begin
  inherited;
  pcDados.ActivePage := tsGerais;
end;

procedure TFormInformacoesContasReceberBaixa.FormShow(Sender: TObject);
begin
  inherited;
  pcDados.ActivePageIndex := 0;
end;

procedure TFormInformacoesContasReceberBaixa.sbCreditosClick(Sender: TObject);
begin
  inherited;
  InformacoesCreditosUtilizados.Informar( eBaixaId.AsInt, 'R' );
end;

procedure TFormInformacoesContasReceberBaixa.sbInfoOrcamentoIdClick(Sender: TObject);
begin
  inherited;
  Informacoes.TurnoCaixa.Informar(eTurnoId.IdInformacao);
end;

procedure TFormInformacoesContasReceberBaixa.sbInformacoesBaixaClick(Sender: TObject);
begin
  inherited;
  InformacoesContasPagarBaixa.Informar( eBaixaPagarOrigemId.IdInformacao );
end;

procedure TFormInformacoesContasReceberBaixa.sgContasDinheiroDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol = cdValor then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgContasDinheiro.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesContasReceberBaixa.sgTitulosBaixadosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloReceber.Informar( SFormatInt(sgTitulosBaixados.Cells[coReceberId, sgTitulosBaixados.Row]) );
end;

procedure TFormInformacoesContasReceberBaixa.sgTitulosBaixadosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coReceberId, coValor] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgTitulosBaixados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesContasReceberBaixa.sgTitulosGeradosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloReceber.Informar( SFormatInt(sgTitulosGerados.Cells[cgReceberId, sgTitulosGerados.Row]) );
end;

procedure TFormInformacoesContasReceberBaixa.sgTitulosGeradosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[cgReceberId, cgValor] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgTitulosGerados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
