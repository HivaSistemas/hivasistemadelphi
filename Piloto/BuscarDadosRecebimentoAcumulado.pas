unit BuscarDadosRecebimentoAcumulado;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca, _Sessao, _Acumulados,
  Vcl.ExtCtrls, FrameTipoNotaGerar, _FrameHerancaPrincipal, FrameFechamento, _ComunicacaoTEF, _RecordsFinanceiros,
  FrameFechamentoJuros, _RecordsOrcamentosVendas, _RecordsEspeciais, BuscarDadosCartoesTEF, _CondicoesPagamento,
  _RecordsCadastros, Vcl.StdCtrls, Emissao.BoletosBancario, ImpressaoComprovanteCartao, _AcumuladosPagamentos,
  ImpressaoComprovantePagamentoAcumulado, _AcumuladosPagamentosCheques, ImpressaoMeiaPaginaDuplicataMercantil;

type
  TFormBuscarDadosRecebimentoAcumulado = class(TFormHerancaFinalizar)
    FrPagamento: TFrFechamentoJuros;
    FrTipoNotaGerar: TFrTipoNotaGerar;
    ckGerarCreditoTroco: TCheckBox;
  private
    FAcumuladoId: Integer;
    FClienteId: Integer;
    FEmpresaId: Integer;
    FQtdeMaximaParcelas: Integer;

    FResultado: RecRetornoRecebimento;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BuscarDadosRecebimento(pAcumulado: RecAcumulados): TRetornoTelaFinalizar<RecRetornoRecebimento>;

implementation

{$R *.dfm}

{ TFormBuscarDadosRecebimentoAcumulado }

function BuscarDadosRecebimento(pAcumulado: RecAcumulados): TRetornoTelaFinalizar<RecRetornoRecebimento>;
var
  vForm: TFormBuscarDadosRecebimentoAcumulado;
begin
  vForm := TFormBuscarDadosRecebimentoAcumulado.Create(nil);

  vForm.FrPagamento.Modo(True);
  vForm.FrPagamento.TelaChamada := ttRecebimento;
  vForm.FrPagamento.setUtilizaTef( Sessao.getParametrosEstacao.UtilizarTef = 'S' );

  vForm.FrPagamento.ValorFrete := pAcumulado.ValorFrete;

  vForm.FrPagamento.setCondicaoPagamento(pAcumulado.CondicaoId, pAcumulado.NomeCondicaoPagamento, pAcumulado.PrazoMedioCondicaoPagto);
  vForm.FrPagamento.setTotalProdutos(pAcumulado.ValorTotalProdutos);

  vForm.FrPagamento.eValorJuros.AsCurr := pAcumulado.ValorJuros;
  vForm.FrPagamento.eValorJurosChange(nil);

  vForm.FrPagamento.eValorDesconto.AsCurr := pAcumulado.ValorDesconto;
  vForm.FrPagamento.eValorDescontoChange(nil);

  vForm.FrPagamento.eValorOutrasDespesas.AsDouble := pAcumulado.ValorOutrasDespesas;
  vForm.FrPagamento.eValorOutrasDespesasChange(nil);

  vForm.FrPagamento.eValorDinheiro.AsCurr      := pAcumulado.ValorDinheiro;
  vForm.FrPagamento.eValorCheque.AsCurr        := pAcumulado.ValorCheque;
  vForm.FrPagamento.eValorCartaoDebito.AsCurr  := pAcumulado.ValorCartaoDebito;
  vForm.FrPagamento.eValorCartaoCredito.AsCurr := pAcumulado.ValorCartaoCredito;
  vForm.FrPagamento.eValorCobranca.AsCurr      := pAcumulado.ValorCobranca;
  vForm.FrPagamento.eValorFinanceira.AsCurr    := pAcumulado.ValorFinanceira;
  vForm.FrPagamento.eValorCredito.AsCurr       := pAcumulado.ValorCredito;
  vForm.FrPagamento.eValorPix.AsCurr           := pAcumulado.ValorPix;

  vForm.FAcumuladoId        := pAcumulado.AcumuladoId;
  vForm.FClienteId          := pAcumulado.ClienteId;
  vForm.FEmpresaId          := pAcumulado.EmpresaId;
  vForm.FQtdeMaximaParcelas := _CondicoesPagamento.BuscarQtdePrazos(Sessao.getConexaoBanco, pAcumulado.CondicaoId);

  vForm.FrPagamento.CartoesDebito   := _AcumuladosPagamentos.BuscarAcumuladosPagamentos(Sessao.getConexaoBanco, 0, [pAcumulado.AcumuladoId, 'D']);
  vForm.FrPagamento.CartoesCredito  := _AcumuladosPagamentos.BuscarAcumuladosPagamentos(Sessao.getConexaoBanco, 0, [pAcumulado.AcumuladoId, 'C']);
  vForm.FrPagamento.Cobrancas       := _AcumuladosPagamentos.BuscarAcumuladosPagamentos(Sessao.getConexaoBanco, 1, [pAcumulado.AcumuladoId]);
  vForm.FrPagamento.Cheques         := _AcumuladosPagamentosCheques.BuscarAcumuladosPagamentosCheques(Sessao.getConexaoBanco, 0, [pAcumulado.AcumuladoId]);

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FResultado;
end;

procedure TFormBuscarDadosRecebimentoAcumulado.Finalizar(Sender: TObject);
var
  i: Integer;
  vCartoes: TArray<RecTitulosFinanceiros>;
  vRetornoCartoes: TRetornoTelaFinalizar< TArray<RecRespostaTEF> >;

  vValorDinheiro: Double;
  vRetBanco: RecRetornoBD;
begin
  vCartoes := nil;

  // Se houver TEF, chamando a tela que realizar� a passagem destes cart�es
  if (Sessao.getParametrosEstacao.UtilizarTef = 'S') and (FrPagamento.eValorCartaoDebito.AsCurr + FrPagamento.eValorCartaoCredito.AsCurr > 0) then begin
    vRetornoCartoes :=
      BuscarDadosCartoesTEF.BuscarPagamentosTEF(
        FrPagamento.eValorCartaoDebito.AsCurr,
        FrPagamento.eValorCartaoCredito.AsCurr,
        FQtdeMaximaParcelas,
        FrPagamento.eValorCartaoDebito.Enabled,
        FrPagamento.eValorCartaoCredito.Enabled
      );

    if vRetornoCartoes.BuscaCancelada then begin
      BuscarDadosCartoesTEF.CancelarCartoes( vRetornoCartoes.Dados );
      RotinaCanceladaUsuario;
      Abort;
    end;

    vCartoes := nil;
    SetLength(vCartoes, Length(vRetornoCartoes.Dados));
    for i := Low(vRetornoCartoes.Dados) to High(vRetornoCartoes.Dados) do begin
      vCartoes[i].CobrancaId        := vRetornoCartoes.Dados[i].CobrancaId;
      vCartoes[i].NsuTef            := vRetornoCartoes.Dados[i].Nsu;
      vCartoes[i].ItemId            := i;
      vCartoes[i].Valor             := vRetornoCartoes.Dados[i].Valor;
      vCartoes[i].NumeroCartao      := vRetornoCartoes.Dados[i].NumeroCartao;
      vCartoes[i].CodigoAutorizacao := vRetornoCartoes.Dados[i].CodigoAutorizacao;
      vCartoes[i].TipoRecebCartao   := vRetornoCartoes.Dados[i].TipoRecebimento;
    end;
  end
  else
    vCartoes := FrPagamento.Cartoes;

  Sessao.getConexaoBanco.IniciarTransacao;

  if FrPagamento.eValorCredito.AsCurr > 0 then
    vValorDinheiro := FrPagamento.eValorDinheiro.AsCurr
  else
    vValorDinheiro := IIfDbl(not ckGerarCreditoTroco.Checked, FrPagamento.eValorDinheiro.AsCurr - FrPagamento.eValorTroco.AsCurr, FrPagamento.eValorDinheiro.AsCurr);

  // Atualizando os pagamentos efetuados
  vRetBanco :=
    _Acumulados.AtualizarValoresPagamentos(
      Sessao.getConexaoBanco,
      FAcumuladoId,
      vValorDinheiro,
      FrPagamento.eValorCheque.AsCurr,
      FrPagamento.eValorCartaoDebito.AsCurr,
      FrPagamento.eValorCartaoCredito.AsCurr,
      FrPagamento.eValorCobranca.AsCurr,
      FrPagamento.eValorFinanceira.AsCurr,
      FrPagamento.eValorCredito.AsCurr
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  // Atualizando os titulos a serem gerados no financeiro
  vRetBanco :=
    _Acumulados.AtualizarAcumuladoPagamentos(
      Sessao.getConexaoBanco,
      FAcumuladoId,
      FrPagamento.Cheques,
      vCartoes,
      FrPagamento.Cobrancas,
      nil,
      True
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  if Length(FrPagamento.FDadosPix) > 0 then begin
    vRetBanco := _Acumulados.AtualizarAcumuladosPagamentosPix(
      Sessao.getConexaoBanco,
      FAcumuladoId,
      FrPagamento.FDadosPix,
      FEmpresaId,
      FClienteId,
      Sessao.getTurnoCaixaAberto.TurnoId,
      Sessao.getData
    );

    Sessao.AbortarSeHouveErro(vRetBanco);
  end;

  // Gerando o financeiro e baixando a previs�o
  vRetBanco :=
    _Acumulados.ReceberAcumulado(
      Sessao.getConexaoBanco,
      FAcumuladoId,
      Sessao.getTurnoCaixaAberto.TurnoId,
      Sessao.getTurnoCaixaAberto.FuncionarioId,
      ckGerarCreditoTroco.Checked,
      IIfDbl(FrPagamento.eValorCredito.AsCurr > 0, 0, FrPagamento.eValorTroco.AsCurr),
      FrTipoNotaGerar.TipoNotaGerar
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  Sessao.getConexaoBanco.FinalizarTransacao;

  if FrPagamento.TemBoletoBancario then
    Emissao.BoletosBancario.Emitir(FAcumuladoId, tbAcumulado);

  if FrTipoNotaGerar.ckRecibo.Checked then
    ImpressaoComprovantePagamentoAcumulado.Imprimir(FAcumuladoId);

  // Imprimir as vias dos cart�es
  if Sessao.getParametrosEstacao.UtilizarTef = 'S' then begin
    for i := Low(vRetornoCartoes.Dados) to High(vRetornoCartoes.Dados) do
      ImpressaoComprovanteCartao.Imprimir( vRetornoCartoes.Dados[i].Comprovante1Via, vRetornoCartoes.Dados[i].Comprovante2Via );
  end;

  if FrPagamento.TemDuplicataMercantil then
    ImpressaoMeiaPaginaDuplicataMercantil.Emitir(FAcumuladoId, tpAcumulado);

  inherited;
end;

procedure TFormBuscarDadosRecebimentoAcumulado.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if FrPagamento.eValorTroco.AsCurr < 0 then begin
    _Biblioteca.Exclamar('Existe diferen�a no valor a ser pago e as formas de pagamentos definidas!');
    SetarFoco(FrPagamento.eValorDinheiro);
    Abort;
  end;

  if (FrPagamento.eValorTroco.AsCurr > FrPagamento.eValorDinheiro.AsCurr) and (not ckGerarCreditoTroco.Checked) and (FrPagamento.eValorCredito.AsCurr = 0) then begin
    _Biblioteca.Exclamar('O valor do troco n�o pode ser maior que o valor do dinheiro sem que esteja marcado para gerar cr�dito!');
    SetarFoco(FrPagamento.eValorDinheiro);
    Abort;
  end;

  if (FrPagamento.eValorCheque.AsCurr > 0) and (FrPagamento.Cheques = nil) then begin
    _Biblioteca.Exclamar('Os cheques n�o foram definidos corretamente!');
    SetarFoco(FrPagamento.eValorCheque);
    Abort;
  end;

  FrPagamento.verificarCartoes;

  if (FrPagamento.eValorCobranca.AsCurr > 0) and (FrPagamento.Cobrancas = nil) then begin
    _Biblioteca.Exclamar('As cobran�as n�o foram definidas corretamente!');
    SetarFoco(FrPagamento.eValorCobranca);
    Abort;
  end;

  if (FrPagamento.eValorpIX.AsCurr > 0) and (FrPagamento.FDadosPix = nil) then begin
    _Biblioteca.Exclamar('As contas do PIX n�o foram definidas corretamente!');
    SetarFoco(FrPagamento.eValorpIX);
    Abort;
  end;

  if ckGerarCreditoTroco.Checked and (FClienteId = Sessao.getParametros.cadastro_consumidor_final_id) then begin
    _Biblioteca.Exclamar('N�o � permitido gerar cr�dito do troco para consumidor final!');
    SetarFoco(ckGerarCreditoTroco);
    Abort;
  end;

  if FrPagamento.getCreditoMaiorTotalPagarETemFormasPagamento then begin
    _Biblioteca.Exclamar('Os cr�ditos que est�o sendo utilizados j� cobrem o valor do pedido, n�o ser� poss�vel utilizar outras formas de pagamento!');
    SetarFoco(FrPagamento.eValorDinheiro);
    Abort;
  end;

  if FrPagamento.getFormasPagamentoMaiorTotalPagarEUtilizandoCredito then begin
    _Biblioteca.Exclamar('As formas de pagamento j� definidas � maior ou igual ao valor a ser pago pelo pedido, n�o ser� poss�vel utilizar cr�ditos!');
    SetarFoco(FrPagamento.eValorCredito);
    Abort;
  end;

  FrTipoNotaGerar.VerificarRegistro;
end;

end.
