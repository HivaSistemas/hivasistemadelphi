unit CadastroGruposFornecedores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, EditLuka, Vcl.Buttons,
  Vcl.ExtCtrls, _GruposFornecedores, _Sessao, _Biblioteca, _RecordsEspeciais, PesquisaGruposFornecedores,
  CheckBoxLuka;

type
  TFormCadastroGruposFornecedores = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
  private
    procedure PreencherRegistro(pGrupo: RecGruposFornecedores);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroGruposFornecedores }

procedure TFormCadastroGruposFornecedores.BuscarRegistro;
var
  vGrupos: TArray<RecGruposFornecedores>;
begin
  vGrupos := _GruposFornecedores.BuscarGrupos(Sessao.getConexaoBanco, 0, [eId.AsInt], False);
  if vGrupos = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vGrupos[0]);
end;

procedure TFormCadastroGruposFornecedores.ExcluirRegistro;
begin
  Sessao.AbortarSeHouveErro( _GruposFornecedores.ExcluirGrupo(Sessao.getConexaoBanco, eID.AsInt) );
  inherited;
end;

procedure TFormCadastroGruposFornecedores.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vRetBanco :=
    _GruposFornecedores.AtualizarGrupo(
      Sessao.getConexaoBanco,
      eId.AsInt,
      eDescricao.Text,
      ckAtivo.CheckedStr
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroGruposFornecedores.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([eDescricao, ckAtivo], pEditando);

  if pEditando then
    SetarFoco(eDescricao);
end;

procedure TFormCadastroGruposFornecedores.PesquisarRegistro;
var
  vGrupo: RecGruposFornecedores;
begin
  vGrupo := RecGruposFornecedores(PesquisaGruposFornecedores.Pesquisar(False));
  if vGrupo = nil then
    Exit;

  inherited;
  PreencherRegistro(vGrupo);
end;

procedure TFormCadastroGruposFornecedores.PreencherRegistro(pGrupo: RecGruposFornecedores);
begin
  eID.AsInt          := pGrupo.GrupoId;
  eDescricao.Text    := pGrupo.Descricao;
  ckAtivo.CheckedStr := pGrupo.Ativo;
end;

procedure TFormCadastroGruposFornecedores.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o do grupo n�o foi informada corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;
end;

end.
