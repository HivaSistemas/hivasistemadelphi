inherited FormPesquisaCFOP: TFormPesquisaCFOP
  Caption = 'Pesquisa de CFOP'
  ClientHeight = 280
  ClientWidth = 498
  ExplicitWidth = 504
  ExplicitHeight = 309
  PixelsPerInch = 96
  TextHeight = 14
  object lb2: TLabel [0]
    Left = 4
    Top = 205
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited sgPesquisa: TGridLuka
    Width = 498
    Height = 234
    ColCount = 4
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goRowSelect, goThumbTracking]
    OnClick = sgPesquisaClick
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Ativo')
    RealColCount = 4
    ExplicitLeft = 0
    ExplicitTop = 46
    ExplicitWidth = 498
    ExplicitHeight = 234
    ColWidths = (
      28
      55
      322
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Left = 9
    Top = 166
    Text = '0'
    ExplicitLeft = 9
    ExplicitTop = 166
  end
  inherited pnFiltro1: TPanel
    Width = 498
    ExplicitWidth = 498
    inherited eValorPesquisa: TEditLuka
      Top = 19
      Width = 290
      ExplicitTop = 19
      ExplicitWidth = 290
    end
    inherited cbOpcoesPesquisa: TComboBoxLuka
      Top = 19
      ExplicitTop = 19
    end
  end
  object eDescricaoCFOP: TMemo
    Left = 4
    Top = 219
    Width = 491
    Height = 58
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 3
    OnKeyDown = ProximoCampo
  end
end
