inherited FrameHerancaInsercaoExclusao: TFrameHerancaInsercaoExclusao
  Width = 266
  Height = 199
  ExplicitWidth = 266
  ExplicitHeight = 199
  object sgValores: TGridLuka
    Left = 0
    Top = 17
    Width = 266
    Height = 182
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    Align = alClient
    ColCount = 2
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    PopupMenu = pmOpcoes
    TabOrder = 0
    OnKeyDown = sgValoresKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    Grid3D = False
    RealColCount = 2
    AtivarPopUpSelecao = False
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 0
    Top = 0
    Width = 266
    Height = 17
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Descri'#231#227'o'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object pmOpcoes: TPopupMenu
    Left = 176
    Top = 72
    object miInserir: TMenuItem
      Caption = 'Inserir'
      OnClick = miInserirClick
    end
    object miExcluir: TMenuItem
      Caption = 'Excluir'
      OnClick = miExcluirClick
    end
    object miN1: TMenuItem
      Caption = '-'
    end
    object miSubir: TMenuItem
      Caption = 'Subir'
      OnClick = miSubirClick
    end
    object miDescer: TMenuItem
      Caption = 'Descer'
      OnClick = miDescerClick
    end
  end
end
