inherited FormDevolucaoProdutos: TFormDevolucaoProdutos
  Left = 20
  Top = 20
  Caption = 'Devolu'#231#227'o de produtos'
  ClientHeight = 564
  ClientWidth = 1000
  ExplicitWidth = 1006
  ExplicitHeight = 593
  DesignSize = (
    1000
    564)
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 125
    Top = 1
    Width = 38
    Caption = 'Pedido'
    ExplicitLeft = 125
    ExplicitTop = 1
    ExplicitWidth = 38
  end
  object lb2: TLabel [1]
    Left = 234
    Top = 1
    Width = 39
    Height = 14
    Caption = 'Cliente'
  end
  object lb12: TLabel [2]
    Left = 464
    Top = 1
    Width = 52
    Height = 14
    Caption = 'Vendedor'
  end
  object lb3: TLabel [3]
    Left = 663
    Top = 1
    Width = 76
    Height = 14
    Caption = 'Data cadastro'
  end
  object lb4: TLabel [4]
    Left = 125
    Top = 38
    Width = 48
    Height = 14
    Caption = 'Data rec.'
  end
  object sbInfoPedido: TSpeedButton [5]
    Left = 207
    Top = 19
    Width = 19
    Height = 18
    Hint = 'Abrir informa'#231#245'es do cliente'
    Caption = '...'
    Flat = True
    OnClick = sbInfoPedidoClick
  end
  object sbDevolucaoTotal: TSpeedButton [6]
    Left = 765
    Top = 39
    Width = 105
    Height = 31
    Caption = 'Devolu'#231#227'o total'
    Flat = True
    Visible = False
    OnClick = sbDevolucaoTotalClick
  end
  object sbInfoVendedor: TSpeedButton [7]
    Left = 638
    Top = 21
    Width = 19
    Height = 18
    Hint = 'Abrir informa'#231#245'es do vendedor'
    Caption = '...'
    Flat = True
    Visible = False
  end
  object lb1: TLabel [8]
    Left = 234
    Top = 38
    Width = 69
    Height = 14
    Caption = 'Observa'#231#245'es'
  end
  object lb5: TLabel [9]
    Left = 464
    Top = 38
    Width = 95
    Height = 14
    Caption = 'Motivo devolu'#231#227'o'
    Visible = False
  end
  inherited pnOpcoes: TPanel
    Height = 571
    Align = alNone
    TabOrder = 5
    ExplicitHeight = 571
    inherited sbGravar: TSpeedButton
      Left = 3
      Width = 110
      ExplicitLeft = 3
      ExplicitWidth = 110
    end
    inherited sbDesfazer: TSpeedButton
      Left = 3
      Top = 96
      Width = 110
      ExplicitLeft = 3
      ExplicitTop = 96
      ExplicitWidth = 110
    end
    inherited sbExcluir: TSpeedButton
      Left = -112
      Visible = False
      ExplicitLeft = -112
    end
    inherited sbPesquisar: TSpeedButton
      Left = 3
      Width = 110
      ExplicitLeft = 3
      ExplicitWidth = 110
    end
    inherited sbLogs: TSpeedButton
      Left = 3
      Top = 518
      Width = 110
      Height = 40
      ExplicitLeft = 3
      ExplicitTop = 518
      ExplicitWidth = 110
      ExplicitHeight = 40
    end
  end
  inherited eID: TEditLuka
    Left = 125
    Top = 15
    TabOrder = 0
    ExplicitLeft = 125
    ExplicitTop = 15
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 1032
    TabOrder = 6
    Visible = False
    ExplicitLeft = 1032
  end
  object eCliente: TEditLuka
    Left = 234
    Top = 15
    Width = 203
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eVendedor: TEditLuka
    Left = 464
    Top = 15
    Width = 175
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataCadastro: TEditLukaData
    Left = 663
    Top = 15
    Width = 81
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    ReadOnly = True
    TabOrder = 7
    Text = '  /  /    '
  end
  object eDataRecebimento: TEditLukaData
    Left = 125
    Top = 52
    Width = 80
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    ReadOnly = True
    TabOrder = 8
    Text = '  /  /    '
  end
  object sgProdutos: TGridLuka
    Left = 125
    Top = 76
    Width = 871
    Height = 453
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 11
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 2
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    TabOrder = 9
    OnDrawCell = sgProdutosDrawCell
    OnKeyPress = NumerosVirgula
    OnSelectCell = sgProdutosSelectCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Und.'
      'Qtd.venda'
      'Qtd.entregues'
      'Qtd.devolver'
      'Qtd.Pend'#234'ncia'
      'Qtd.devolver'
      'Qtde.sem prev.'
      'Qtd.devolver')
    OnGetCellColor = sgProdutosGetCellColor
    OnArrumarGrid = sgProdutosArrumarGrid
    Grid3D = False
    RealColCount = 21
    Indicador = True
    AtivarPopUpSelecao = False
    ExplicitHeight = 460
    ColWidths = (
      53
      189
      92
      34
      72
      85
      76
      95
      77
      91
      81)
  end
  object stTotalDevolucao: TStaticText
    Left = 869
    Top = 546
    Width = 127
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '170,50'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Transparent = False
    ExplicitTop = 553
  end
  object stTotalDesconto: TStaticText
    Left = 497
    Top = 546
    Width = 125
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '15,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 11
    Transparent = False
    ExplicitTop = 553
  end
  object stValorProdutos: TStaticText
    Left = 125
    Top = 546
    Width = 125
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '150,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    Transparent = False
    ExplicitTop = 553
  end
  object stOutrasDespesas: TStaticText
    Left = 373
    Top = 546
    Width = 125
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '35,50'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 13
    Transparent = False
    ExplicitTop = 553
  end
  object eObservacoes: TEditLuka
    Left = 234
    Top = 52
    Width = 223
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 200
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object stValorFrete: TStaticText
    Left = 249
    Top = 546
    Width = 125
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '150,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 14
    Transparent = False
    ExplicitTop = 553
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 125
    Top = 530
    Width = 130
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelOuter = bvNone
    Caption = 'Valor produtos'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 15
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
    ExplicitTop = 537
  end
  object StaticTextLuka2: TStaticTextLuka
    Left = 254
    Top = 530
    Width = 115
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Frete'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 16
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
    ExplicitTop = 537
  end
  object StaticTextLuka3: TStaticTextLuka
    Left = 368
    Top = 530
    Width = 127
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Outras despesas'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 17
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
    ExplicitTop = 537
  end
  object StaticTextLuka4: TStaticTextLuka
    Left = 491
    Top = 530
    Width = 127
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Total desconto'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 18
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
    ExplicitTop = 537
  end
  object StaticTextLuka5: TStaticTextLuka
    Left = 869
    Top = 530
    Width = 127
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Total da devolu'#231#227'o'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 19
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
    ExplicitTop = 537
  end
  object stTotalSubstitucaoTributaria: TStaticText
    Left = 621
    Top = 546
    Width = 125
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 20
    Transparent = False
    ExplicitTop = 553
  end
  object StaticTextLuka6: TStaticTextLuka
    Left = 617
    Top = 530
    Width = 127
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Substitui'#231#227'o trib.'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 21
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
    ExplicitTop = 537
  end
  object stTotalIPI: TStaticText
    Left = 745
    Top = 546
    Width = 125
    Height = 16
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 22
    Transparent = False
    ExplicitTop = 553
  end
  object StaticTextLuka7: TStaticTextLuka
    Left = 743
    Top = 530
    Width = 127
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    Caption = 'IPI'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 23
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
    ExplicitTop = 537
  end
  object ckDevolverFrete: TCheckBoxLuka
    Left = 753
    Top = 8
    Width = 164
    Height = 17
    Caption = 'Devolver frete'
    TabOrder = 24
    OnClick = ckDevolverFreteClick
    CheckedStr = 'N'
  end
  object ckDevolverOutrasDespesas: TCheckBoxLuka
    Left = 753
    Top = 25
    Width = 164
    Height = 17
    Caption = 'Devolver outras despesas'
    TabOrder = 25
    OnClick = ckDevolverOutrasDespesasClick
    CheckedStr = 'N'
  end
  object eMotivoDevolucao: TEditLuka
    Left = 464
    Top = 52
    Width = 280
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 200
    TabOrder = 4
    Visible = False
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
