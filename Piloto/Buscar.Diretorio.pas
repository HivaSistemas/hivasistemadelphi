unit Buscar.Diretorio;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar,
  Vcl.Grids, Vcl.Samples.DirOutln, Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, Vcl.Outline;

type
  TFormBuscarDiretorio = class(TFormHerancaFinalizar)
    doDiretorio: TDirectoryOutline;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(): TRetornoTelaFinalizar<string>;

implementation

{$R *.dfm}

function Buscar(): TRetornoTelaFinalizar<string>;
var
  vForm: TFormBuscarDiretorio;
begin
  vForm := TFormBuscarDiretorio.Create(Application);

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.doDiretorio.Directory + '\';

  vForm.Free;
end;

procedure TFormBuscarDiretorio.FormCreate(Sender: TObject);
begin
  inherited;
  doDiretorio.Drive := 'c';
  doDiretorio.FullCollapse;
end;

procedure TFormBuscarDiretorio.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
