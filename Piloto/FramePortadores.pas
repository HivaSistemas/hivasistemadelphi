unit FramePortadores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _Portadores,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, _Biblioteca, PesquisaPortadores,
  Vcl.Menus;

type
  TFrPortadores = class(TFrameHenrancaPesquisas)
  public
    function getPortador(pLinha: Integer = -1): RecPortadores;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrPortadores }

function TFrPortadores.AdicionarDireto: TObject;
var
  vPortadores: TArray<RecPortadores>;
begin
  vPortadores := _Portadores.BuscarPortadores(Sessao.getConexaoBanco, 0, [FChaveDigitada] );
  if vPortadores = nil then
    Result := nil
  else
    Result := vPortadores[0];
end;

function TFrPortadores.AdicionarPesquisando: TObject;
begin
  Result := PesquisaPortadores.PesquisarPortador;
end;

function TFrPortadores.getPortador(pLinha: Integer): RecPortadores;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecPortadores(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrPortadores.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecPortadores(FDados[i]).PortadorId = RecPortadores(pSender).PortadorId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrPortadores.MontarGrid;
var
  i: Integer;
  pSender: RecPortadores;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecPortadores(FDados[i]);
      AAdd([pSender.PortadorId, pSender.Descricao]);
    end;
  end;
end;

end.
