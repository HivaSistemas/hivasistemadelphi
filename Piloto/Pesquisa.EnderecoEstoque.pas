unit Pesquisa.EnderecoEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, System.StrUtils, System.Math,
  _Biblioteca, _Sessao, Vcl.ExtCtrls;

type
  TFormPesquisaEnderecoEstoque = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(): TObject;

implementation

{$R *.dfm}

uses _EnderecoEstoque;

const
  coCodigo     = 1;
  coNomeRua    = 2;
  coNomeModulo = 3;
  coNomeNivel  = 4;
  coNomeVao    = 5;

function Pesquisar(): TObject;
var
  obj: TObject;
begin
  obj := _HerancaPesquisas.Pesquisar(TFormPesquisaEnderecoEstoque, _EnderecoEstoque.GetFiltros);
  if obj = nil then
    Result := nil
  else
    Result := RecEnderecosEstoque(obj);
end;

{ TFormPesquisaEnderecoEstoque }

procedure TFormPesquisaEnderecoEstoque.BuscarRegistros;
var
  i: Integer;
  vEnderecoEstoque: TArray<RecEnderecosEstoque>;
begin
  inherited;

  vEnderecoEstoque :=
    _EnderecoEstoque.BuscarEnderecoEstoque(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if vEnderecoEstoque = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vEnderecoEstoque);

  for i := Low(vEnderecoEstoque) to High(vEnderecoEstoque) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]   := IntToStr(vEnderecoEstoque[i].endereco_id);
    sgPesquisa.Cells[coNomeRua, i + 1] := vEnderecoEstoque[i].nome_rua;
    sgPesquisa.Cells[coNomeModulo, i + 1] := vEnderecoEstoque[i].nome_modulo;
    sgPesquisa.Cells[coNomeNivel, i + 1] := vEnderecoEstoque[i].nome_nivel;
    sgPesquisa.Cells[coNomeVao, i + 1] := vEnderecoEstoque[i].nome_vao;
  end;

  sgPesquisa.RowCount := IfThen(Length(vEnderecoEstoque) = 1, 2, High(vEnderecoEstoque) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaEnderecoEstoque.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else if ACol in [coNomeRua, coNomeModulo, coNomeNivel, coNomeVao] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taCenter;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaEnderecoEstoque.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;

  if ARow = 0 then
    Exit;
end;

end.
