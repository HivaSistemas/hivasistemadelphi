unit _AtalhosSistema;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _Sessao, _RecordsCadastros;

{$M+}
type
  TAtalhosSistema = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordAtalhosSistema: RecAtalhosSistema;
  end;

function AtualizarAtalhosSistema(
  pConexao: TConexao;
  pAtalhoSistemaId: Integer;
  pAtalho: string;
  pDescricao: string
): RecRetornoBD;

function BuscarAtalhoSistema(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAtalhosSistema>;

function ExcluirAtalhoSistema(
  pConexao: TConexao;
  pAtalhoSistemaId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TMotivoAjusteEstoque }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where ATALHO_SISTEMA_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o',
      True,
      0,
      'where DESCRICAO like :P1 || ''%'' ' +
      'order by ' +
      '  ATALHO_SISTEMA_ID asc '
    );
end;

constructor TAtalhosSistema.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ATALHOS_SISTEMA');

  FSql :=
    'select ' +
    '  ATALHO_SISTEMA_ID, ' +
    '  ATALHO, ' +
    '  DESCRICAO ' +
    'from ' +
    '  ATALHOS_SISTEMA ';

  setFiltros(getFiltros);

  AddColuna('ATALHO_SISTEMA_ID', True);
  AddColuna('ATALHO');
  AddColuna('DESCRICAO');
end;

function TAtalhosSistema.getRecordAtalhosSistema: RecAtalhosSistema;
begin
  Result.atalho_sistema_id  := getInt('ATALHO_SISTEMA_ID', True);
  Result.atalho             := getString('ATALHO');
  Result.descricao          := getString('DESCRICAO');
end;

function AtualizarAtalhosSistema(
  pConexao: TConexao;
  pAtalhoSistemaId: Integer;
  pAtalho: string;
  pDescricao: string
): RecRetornoBD;
var
  t: TAtalhosSistema;
  vNovo: Boolean;
  seq: TSequencia;
  exec: TExecucao;
begin
  Result.Iniciar;
  t := TAtalhosSistema.Create(pConexao);
  exec := TExecucao.Create(pConexao);

  exec.SQL.Add('delete from ATALHOS_SISTEMA where ATALHO_SISTEMA_ID = :P1');

  try
    pConexao.IniciarTransacao;

    exec.Executar([pAtalhoSistemaId]);

    t.setInt('ATALHO_SISTEMA_ID', pAtalhoSistemaId, True);
    t.setString('ATALHO', pAtalho);
    t.setString('DESCRICAO', pDescricao);
    t.Inserir;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
  exec.Free;
end;

function BuscarAtalhoSistema(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecAtalhosSistema>;
var
  i: Integer;
  t: TAtalhosSistema;
begin
  Result := nil;
  t := TAtalhosSistema.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordAtalhosSistema;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirAtalhoSistema(
  pConexao: TConexao;
  pAtalhoSistemaId: Integer
): RecRetornoBD;
var
  t: TAtalhosSistema;
begin
  Result.TeveErro := False;
  t := TAtalhosSistema.Create(pConexao);

  try
    t.setInt('ATALHO_SISTEMA_ID', pAtalhoSistemaId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
