inherited FormPesquisaTiposCobranca: TFormPesquisaTiposCobranca
  Caption = 'Pesquisa de tipos de cobran'#231'a'
  ClientHeight = 295
  ClientWidth = 661
  ExplicitWidth = 669
  ExplicitHeight = 326
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Top = 39
    Width = 661
    Height = 256
    ColCount = 8
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goThumbTracking]
    ScrollBars = ssBoth
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Tx. ret. m'#234's'
      'Portador'
      'Rede cart'#227'o'
      'Tipo cart'#227'o'
      'Ativo')
    Grid3D = True
    RealColCount = 8
    Indicador = True
    ExplicitTop = 39
    ExplicitWidth = 661
    ExplicitHeight = 256
    ColWidths = (
      28
      43
      198
      69
      170
      79
      93
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Left = 8
    Top = 264
    Text = '0'
    ExplicitLeft = 8
    ExplicitTop = 264
  end
  inherited pnFiltro1: TPanel
    Width = 661
    Height = 39
    ExplicitWidth = 661
    ExplicitHeight = 39
    inherited lblPesquisa: TLabel
      Left = 192
      Top = 1
      Width = 93
      Caption = 'Tipo de cobran'#231'a'
      ExplicitLeft = 192
      ExplicitTop = 1
      ExplicitWidth = 93
    end
    inherited lblOpcoesPesquisa: TLabel
      Top = 1
      ExplicitTop = 1
    end
    inherited eValorPesquisa: TEditLuka
      Left = 192
      Top = 15
      Width = 462
      ExplicitLeft = 192
      ExplicitTop = 15
      ExplicitWidth = 462
    end
    inherited cbOpcoesPesquisa: TComboBoxLuka
      Top = 15
      Width = 185
      ExplicitTop = 15
      ExplicitWidth = 185
    end
  end
end
