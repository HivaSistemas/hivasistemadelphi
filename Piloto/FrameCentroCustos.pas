unit FrameCentroCustos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _CentroCustos,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, PesquisaCentroCustos, _Biblioteca, _Sessao,
  Vcl.Menus;

type
  TFrCentroCustos = class(TFrameHenrancaPesquisas)
  public
    function GetCentro(pLinha: Integer = -1): RecCentrosCustos;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrCentroCustos }

function TFrCentroCustos.AdicionarDireto: TObject;
var
  vDados: TArray<RecCentrosCustos>;
begin
  vDados := _CentroCustos.BuscarCentroCustos(Sessao.getConexaoBanco, 0, [FChaveDigitada] );
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrCentroCustos.AdicionarPesquisando: TObject;
begin
  Result := PesquisaCentroCustos.PesquisarCentro;
end;

function TFrCentroCustos.GetCentro(pLinha: Integer): RecCentrosCustos;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecCentrosCustos(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrCentroCustos.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecCentrosCustos(FDados[i]).CentroCustoId = RecCentrosCustos(pSender).CentroCustoId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrCentroCustos.MontarGrid;
var
  i: Integer;
  vSender: RecCentrosCustos;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecCentrosCustos(FDados[i]);
      AAdd([IntToStr(vSender.CentroCustoId), vSender.nome]);
    end;
  end;
end;

end.
