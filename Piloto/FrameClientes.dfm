inherited FrClientes: TFrClientes
  inherited PnTitulos: TPanel
    inherited lbNomePesquisa: TLabel
      Width = 38
      Caption = 'Clientes'
      ExplicitWidth = 38
    end
  end
  inherited poOpcoes: TPopupMenu
    object miN1: TMenuItem
      Caption = '-'
    end
    object miVidaCliente: TMenuItem
      Caption = 'Vida do cliente'
      ShortCut = 16457
      OnClick = miVidaClienteClick
    end
  end
end
