inherited FrNomeComputador: TFrNomeComputador
  Width = 184
  Height = 45
  ExplicitWidth = 184
  ExplicitHeight = 45
  object lb8: TLabel
    Left = 3
    Top = 3
    Width = 102
    Height = 13
    Caption = 'Nome do computador'
  end
  object sbNomeComputador: TSpeedButton
    Left = 157
    Top = 18
    Width = 23
    Height = 22
    OnClick = sbNomeComputadorClick
  end
  object eNomeComputador: TEditLuka
    Left = 3
    Top = 18
    Width = 154
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 30
    TabOrder = 0
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
  end
end
