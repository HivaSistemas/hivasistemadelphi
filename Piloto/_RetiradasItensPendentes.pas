unit _RetiradasItensPendentes;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _BibliotecaGenerica, System.SysUtils;

{$M+}

type
  RecRetiradasItensPendentes = record
    OrcamentoId: Integer;
    ProdutoId: Integer;
    Nome: string;
    ItemId: Integer;
    Quantidade: Double;
    Entregues: Double;
    Devolvidos: Double;
    MultiploVenda: Double;
    Saldo: Double;
    Unidade: string;
    Marca: string;
    Lote: string;
    NomeEmpresa: string;
    NomeLocal: string;
    LocalId: Integer;
    EmpresaId: Integer;
  end;

  TRetiradasItensPendentes = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function GetRecordRetiradasPendentes: RecRetiradasItensPendentes;
  end;

function BuscarRetiradasItensPendentes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecRetiradasItensPendentes>;

function BuscarRetiradasItensPendentesComando(pConexao: TConexao; pComando: string): TArray<RecRetiradasItensPendentes>;

function GetFiltros: TArray<RecFiltros>;

implementation

{ TOrcamentoTramite }

function GetFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'and EIP.ORCAMENTO_ID = :P1 ' +
      'and case when IOR.TIPO_CONTROLE_ESTOQUE <> ''A'' or IOR.ITEM_KIT_ID is null then ''S'' else ''N'' end = ''S'' ' +
      'order by ' +
      '  PRO.NOME '
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'and SPE.ORCAMENTO_ID = :P1 ' +
      'and case when IOR.TIPO_CONTROLE_ESTOQUE <> ''A'' or IOR.ITEM_KIT_ID is null then ''S'' else ''N'' end = ''S'' ' +
      'and SPE.SALDO > 0 ' +
      'order by ' +
      '  PRO.NOME '
    );

end;

constructor TRetiradasItensPendentes.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'RETIRADAS_ITENS_PENDENTES');

  FSql :=
    'select ' +
    '  EIP.EMPRESA_ID, ' +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA, ' +
    '  EIP.LOCAL_ID, ' +
    '  LOC.NOME as NOME_LOCAL, ' +
    '  EIP.ORCAMENTO_ID, ' +
    '  EIP.PRODUTO_ID, ' +
    '  PRO.NOME, ' +
    '  EIP.ITEM_ID, ' +
    '  EIP.QUANTIDADE, ' +
    '  EIP.ENTREGUES, ' +
    '  EIP.DEVOLVIDOS, ' +
    '  EIP.SALDO, ' +
    '  EIP.LOTE, ' +
    '  PRO.UNIDADE_VENDA, ' +
    '  MAR.NOME as NOME_MARCA, ' +
    '  PRO.MULTIPLO_VENDA ' +
    'from ' +
    '  RETIRADAS_ITENS_PENDENTES EIP ' +

    'inner join ORCAMENTOS_ITENS IOR ' +
    'on EIP.ORCAMENTO_ID = IOR.ORCAMENTO_ID ' +
    'and EIP.PRODUTO_ID = IOR.PRODUTO_ID ' +
    'and EIP.ITEM_ID = IOR.ITEM_ID ' +

    'inner join PRODUTOS PRO ' +
    'on EIP.PRODUTO_ID = PRO.PRODUTO_ID ' +

    'inner join MARCAS MAR ' +
    'on PRO.MARCA_ID = MAR.MARCA_ID ' +

    'inner join EMPRESAS EMP ' +
    'on EIP.EMPRESA_ID = EMP.EMPRESA_ID ' +

    'inner join LOCAIS_PRODUTOS LOC ' +
    'on EIP.LOCAL_ID = LOC.LOCAL_ID ' +

    'where case when IOR.TIPO_CONTROLE_ESTOQUE = ''A'' or IOR.ITEM_KIT_ID is null then ''S'' else ''N'' end = ''S'' ';

  SetFiltros(GetFiltros);

  AddColunaSL('EMPRESA_ID');
  AddColunaSL('NOME_EMPRESA');
  AddColunaSL('LOCAL_ID');
  AddColunaSL('NOME_LOCAL');
  AddColunaSL('ORCAMENTO_ID');
  AddColunaSL('PRODUTO_ID');
  AddColunaSL('NOME');
  AddColunaSL('ITEM_ID');
  AddColunaSL('QUANTIDADE');
  AddColunaSL('ENTREGUES');
  AddColunaSL('DEVOLVIDOS');
  AddColunaSL('SALDO');
  AddColunaSL('UNIDADE_VENDA');
  AddColunaSL('NOME_MARCA');
  AddColunaSL('LOTE');
  AddColunaSL('MULTIPLO_VENDA');
end;

function TRetiradasItensPendentes.GetRecordRetiradasPendentes: RecRetiradasItensPendentes;
begin
  Result.LocalId           := getInt('LOCAL_ID');
  Result.EmpresaId         := getInt('EMPRESA_ID');
  Result.NomeLocal         := getString('NOME_LOCAL');
  Result.NomeEmpresa       := getString('NOME_EMPRESA');
  Result.OrcamentoId       := getInt('ORCAMENTO_ID');
  Result.ProdutoId         := getInt('PRODUTO_ID');
  Result.Nome              := getString('NOME');
  Result.ItemId            := getInt('ITEM_ID');
  Result.Quantidade        := getDouble('QUANTIDADE');
  Result.Entregues         := getDouble('ENTREGUES');
  Result.Devolvidos        := getDouble('DEVOLVIDOS');
  Result.Saldo             := getDouble('SALDO');
  Result.Unidade           := getString('UNIDADE_VENDA');
  Result.Marca             := getString('NOME_MARCA');
  Result.Lote              := getString('LOTE');
  Result.MultiploVenda     := getDouble('MULTIPLO_VENDA');
end;


function BuscarRetiradasItensPendentes(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecRetiradasItensPendentes>;
var
  i: Integer;
  t: TRetiradasItensPendentes;
begin
  Result := nil;
  t := TRetiradasItensPendentes.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordRetiradasPendentes;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function BuscarRetiradasItensPendentesComando(pConexao: TConexao; pComando: string): TArray<RecRetiradasItensPendentes>;
var
  i: Integer;
  t: TRetiradasItensPendentes;
begin
  Result := nil;
  t := TRetiradasItensPendentes.Create(pConexao);

  if t.PesquisarComando(pComando) then begin
    SetLength(Result, t.GetQuantidadeRegistros);
    for i := 0 to t.GetQuantidadeRegistros - 1 do begin
      Result[i] := t.GetRecordRetiradasPendentes;

      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
