  unit FechamentoCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, _Biblioteca, _Sessao, _MovimentosContas,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Mask, EditLukaData, _FrameHerancaPrincipal, System.Math,
  _FrameHenrancaPesquisas, FrameFuncionarios, _TurnosCaixas, _RecordsEspeciais, _ContasReceber,
  FrameContas, _RecordsCaixa, BuscarDados.TitulosFechamento, Informacoes.TurnoCaixa,
  _Orcamentos, _SessaoGenerica, _HerancaCadastroCodigo, _HerancaCadastro, _MovimentosTurnos;

type
  TDadosFechamento = record
    cartoes: TArray<RecFechamento>;
    cheques: TArray<RecFechamento>;
    cobranca: TArray<RecFechamento>;
  end;

  TFormFechamentoCaixa = class(TFormHerancaCadastro)
    FrCaixaAberto: TFrFuncionarios;
    lb17: TLabel;
    eDataAbertura: TEditLukaData;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    lb14: TLabel;
    sbBuscarDadosCheques: TSpeedButton;
    sbBuscarDadosCartoes: TSpeedButton;
    sbBuscarDadosCobranca: TSpeedButton;
    eValorDinheiro: TEditLuka;
    eValorCheque: TEditLuka;
    eValorCartao: TEditLuka;
    eValorCobranca: TEditLuka;
    stSPC: TStaticText;
    eObservacoes: TMemo;
    lb1: TLabel;
    FrContaDestino: TFrContas;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure eValorChequeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorCartaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorCobrancaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbBuscarDadosChequesClick(Sender: TObject);
    procedure sbBuscarDadosCartoesClick(Sender: TObject);
    procedure sbBuscarDadosCobrancaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FDadosTurno: RecTurnosCaixas;
    FDadosFechamento: TDadosFechamento;

    procedure FrCaixaOnAposPesquisar(Sender: TObject);
    procedure PreencherRegistro(pTurno: RecTurnosCaixas);
    procedure Totalizar(pFechamento: TArray<RecFechamento>; pTipo: TTipoBusca);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormFechamentoCaixa }

procedure TFormFechamentoCaixa.BuscarRegistro;
begin
  inherited;
  PreencherRegistro(FDadosTurno);
end;

procedure TFormFechamentoCaixa.eValorCartaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then begin
    if FDadosTurno.valor_cartao > 0 then
      sbBuscarDadosCartoes.Click
    else
      ProximoCampo(Sender, Key, Shift);
  end;
end;

procedure TFormFechamentoCaixa.eValorChequeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then begin
    if FDadosTurno.valor_cheque > 0 then
      sbBuscarDadosCheques.Click
    else
      ProximoCampo(Sender, Key, Shift);
  end;
end;

procedure TFormFechamentoCaixa.eValorCobrancaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then begin
    if FDadosTurno.valor_cobranca > 0 then
      sbBuscarDadosCobranca.Click
    else
      ProximoCampo(Sender, Key, Shift);
  end;
end;

procedure TFormFechamentoCaixa.ExcluirRegistro;
begin
  inherited;
end;

procedure TFormFechamentoCaixa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Destruir(TObject(FDadosTurno));
end;

procedure TFormFechamentoCaixa.FormCreate(Sender: TObject);
begin
  inherited;
  FrCaixaAberto.OnAposPesquisar := FrCaixaOnAposPesquisar;
end;

procedure TFormFechamentoCaixa.FormShow(Sender: TObject);
begin
  inherited;
  FrCaixaAberto.Modo(True);
  SetarFoco(FrCaixaAberto);
end;

procedure TFormFechamentoCaixa.FrCaixaOnAposPesquisar(Sender: TObject);
var
  vTurnos: TArray<RecTurnosCaixas>;
begin
  vTurnos := _TurnosCaixas.BuscarTurnosCaixas( Sessao.getConexaoBanco, 1, [FrCaixaAberto.GetFuncionario().funcionario_id] );
  if vTurnos = nil then begin
    Exclamar('N�o foi encontrado turno em aberto para o funcion�rio escolhido, verifique!');
    FrCaixaAberto.Modo(True);
    FrCaixaAberto.SetFocus;
    Exit;
  end;
  FDadosTurno := vTurnos[0];

  eValorCheque.Enabled := (vTurnos[0].valor_cheque > 0);
  sbBuscarDadosCheques.Enabled := eValorCheque.Enabled;
  eValorCartao.Enabled := (vTurnos[0].valor_cartao > 0);
  sbBuscarDadosCartoes.Enabled := eValorCartao.Enabled;
  eValorCobranca.Enabled := (vTurnos[0].valor_cobranca > 0);
  sbBuscarDadosCobranca.Enabled := eValorCobranca.Enabled;

  eDataAbertura.AsData := vTurnos[0].data_hora_abertura;
  Modo(True);
  FrCaixaAberto.Modo(False, False);
end;

procedure TFormFechamentoCaixa.GravarRegistro(Sender: TObject);
var
  vContaDestinoId: string;
  vRetorno: RecRetornoBD;
  vValorDiferencaDinheiro: Currency;
  vTurnos: TArray<RecTurnosCaixas>;
begin
  inherited;

  vContaDestinoId := '';
  if not FrContaDestino.EstaVazio then
    vContaDestinoId := FrContaDestino.GetConta().Conta;

  vTurnos := _TurnosCaixas.BuscarTurnosCaixas( Sessao.getConexaoBanco, 1, [FrCaixaAberto.GetFuncionario().funcionario_id] );
  if vTurnos = nil then begin
    Exclamar('N�o foi encontrado turno em aberto para o funcion�rio escolhido, verifique!');
    FrCaixaAberto.Modo(True);
    FrCaixaAberto.SetFocus;
    Exit;
  end;
  FDadosTurno := vTurnos[0];

  Sessao.getConexaoBanco.IniciarTransacao;

  // Sa�da do dinheiro no banco/caixa de origem do dinheiro
  vRetorno :=
    _MovimentosTurnos.AtualizarMovimentosTurno(
      Sessao.getConexaoBanco,
      0,
      FDadosTurno.TurnoId,
      vContaDestinoId,
      eValorDinheiro.AsDouble,
      eValorCheque.AsDouble,
      0,
      eValorCartao.AsDouble,
      eValorCobranca.AsDouble,
      'FEC',
      eObservacoes.Text
    );

  Sessao.AbortarSeHouveErro( vRetorno );

  vValorDiferencaDinheiro := eValorDinheiro.AsDouble - (FDadosTurno.valor_dinheiro - FDadosTurno.ValorSaidasDinheiro);
  if vValorDiferencaDinheiro <> 0 then begin
    vRetorno := _TurnosCaixas.LancarDiferencaFechamentoCaixa(Sessao.getConexaoBanco, FDadosTurno.TurnoId, vValorDiferencaDinheiro);
    Sessao.AbortarSeHouveErro( vRetorno );
  end;

  Sessao.AtualizarTurnoCaixaAberto;
  Sessao.getConexaoBanco.FinalizarTransacao;

  RotinaSucesso;
  Informacoes.TurnoCaixa.Informar( FDadosTurno.TurnoId );
  SetarFoco(FrCaixaAberto);
end;

procedure TFormFechamentoCaixa.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    FrContaDestino,
    eValorDinheiro,
    eValorCheque,
    eValorCartao,
    eValorCobranca,
    eObservacoes,
    sbBuscarDadosCartoes,
    sbBuscarDadosCheques,
    sbBuscarDadosCobranca],
    pEditando
  );

  FDadosFechamento.cartoes := nil;
  FDadosFechamento.cheques := nil;
  FDadosFechamento.cobranca := nil;

  if pEditando then
    SetarFoco(FrContaDestino)
  else begin
    FrCaixaAberto.Modo(True);
    SetarFoco(FrCaixaAberto);
    eDataAbertura.Clear;
  end;
end;

procedure TFormFechamentoCaixa.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormFechamentoCaixa.PreencherRegistro(pTurno: RecTurnosCaixas);
begin
  FDadosTurno := pTurno;

  sbBuscarDadosCartoes.Enabled := (FDadosTurno.valor_cartao > 0);
  sbBuscarDadosCheques.Enabled := (FDadosTurno.valor_cheque > 0);
  sbBuscarDadosCobranca.Enabled := (FDadosTurno.valor_cobranca > 0);

  eDataAbertura.AsData := pTurno.data_hora_abertura;
end;

procedure TFormFechamentoCaixa.sbBuscarDadosCartoesClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<TArray<RecFechamento>>;
begin
  inherited;
  vRetorno := BuscarDados.TitulosFechamento.BuscarTitulosFechamento(ttCartoes, FDadosFechamento.cartoes, FDadosTurno.TurnoId);
  if vRetorno.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    Exit;
  end;

  FDadosFechamento.cartoes := vRetorno.Dados;
  Totalizar(FDadosFechamento.cartoes, ttCartoes);
end;

procedure TFormFechamentoCaixa.sbBuscarDadosChequesClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<TArray<RecFechamento>>;
begin
  inherited;
  vRetorno := BuscarDados.TitulosFechamento.BuscarTitulosFechamento(ttCheques, FDadosFechamento.cheques, FDadosTurno.TurnoId);
  if vRetorno.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    Exit;
  end;

  FDadosFechamento.cheques := vRetorno.Dados;
  Totalizar(FDadosFechamento.cheques, ttCheques);
end;

procedure TFormFechamentoCaixa.sbBuscarDadosCobrancaClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<TArray<RecFechamento>>;
begin
  inherited;
  vRetorno := BuscarDados.TitulosFechamento.BuscarTitulosFechamento(ttCobrancas, FDadosFechamento.cobranca, FDadosTurno.TurnoId);
  if vRetorno.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    Exit;
  end;
  FDadosFechamento.cobranca := vRetorno.Dados;
  Totalizar(FDadosFechamento.cobranca, ttCobrancas);
end;

procedure TFormFechamentoCaixa.Totalizar(pFechamento: TArray<RecFechamento>; pTipo: TTipoBusca);
var
  i: Integer;
  vTotal: Double;
begin
  vTotal := 0;
  for i := Low(pFechamento) to High(pFechamento) do
    vTotal := vTotal + pFechamento[i].valor;

  if pTipo = ttCartoes then
    eValorCartao.AsDouble := vTotal
  else if pTipo = ttCheques then
    eValorCheque.AsDouble := vTotal
  else
    eValorCobranca.AsDouble := vTotal;
end;

procedure TFormFechamentoCaixa.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if _MovimentosTurnos.ExisteMovimentoFechamentoTurno(Sessao.getConexaoBanco, FDadosTurno.TurnoId) then begin
    Exclamar('J� existe um registro de fechamento de caixa para este turno, entre em contato com o suporte!');
    Abort;
  end;

  if (eValorDinheiro.AsCurr > 0) and (FrContaDestino.EstaVazio) then begin
    Exclamar('A conta de destino n�o foi informada corretamente, verifique!');
    SetarFoco(FrContaDestino);
    Abort;
  end;

  if (eValorDinheiro.AsCurr + eValorCheque.AsCurr + eValorCartao.AsCurr + eValorCobranca.AsCurr = 0) then begin
    if not _Biblioteca.Perguntar('Deseja gravar sem informar nenhum valor?') then
      Abort;
  end;

  // Validar os documento se n�o houver autoriza��o para passar sem isso! Luka 06/05/2018 00:50
//  if not Sessao.AutorizadoRotina('nao_obrigar_verificar_documentos_financeiro') then begin
//    Exclamar('A conta de destino n�o foi informada corretamente, verifique!');
//    SetarFoco(FrContaDestino);
//    Abort;
//  end;

//  if _Orcamentos.ExistemVendasPendentesTurno(Sessao.getConexaoBanco, FDadosTurno.TurnoId) then begin
//    Exclamar(
//      'Existem vendas que n�o foram finalizadas corretamente! ' + Chr(13) + Chr(10) +
//      'Verifique se todas as notas foram enviadas.'
//    );
//
//    Abort;
//  end;
end;

end.
