unit BuscarDefinicaoLotesVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _Biblioteca,
  Vcl.StdCtrls, StaticTextLuka, Vcl.Buttons, Vcl.ExtCtrls, _EstoquesDivisao, _Sessao,
  _RecordsOrcamentosVendas;

type
  TTela = (ttOrcamentosVendas, ttAgendarItensSemPrevisao);

  TFormBuscarDefinicaoLotesVenda = class(TFormHerancaFinalizar)
    StaticTextLuka1: TStaticTextLuka;
    sgLotes: TGridLuka;
    Panel1: TPanel;
    StaticTextLuka2: TStaticTextLuka;
    stProduto: TStaticText;
    StaticTextLuka3: TStaticTextLuka;
    stQuantidadeRetirarAto: TStaticText;
    stQuantidadeRetirar: TStaticText;
    StaticTextLuka4: TStaticTextLuka;
    StaticTextLuka5: TStaticTextLuka;
    stQuantidadeEntregar: TStaticText;
    procedure sgLotesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgLotesSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgLotesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgLotesArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure FormCreate(Sender: TObject);
  private
    function getQuantidadeTotal(pColuna: Integer): Currency;
    function ValidarQuantidadesDefinidas(pStaticText: TStaticText; pColuna: Integer): Boolean;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(
  pEmpresaId: Integer;
  pProdutoId: Integer;
  pNomeProduto: string;
  pQuantidadeRetirarAto: Double;
  pQuantidadeRetirar: Double;
  pQuantidadeEntregar: Double;
  pMultiploVenda: Double;
  pTela: TTela
): TRetornoTelaFinalizar< TArray<RecDefinicoesLoteVenda> >;

implementation

{$R *.dfm}

const
  coEmpresaId     = 0;
  coNomeEmpresa   = 1;
  coLote          = 2;
  coLocalId       = 3;
  coLocalNome     = 4;
  coDtFabricacao  = 5;
  coDtVencimento  = 6;
  coDispAto       = 7;
  coQtdeRetAto    = 8;
  coDispRetirar   = 9;
  coQtdeRetirar   = 10;
  coDispEntregar  = 11;
  coQtdeEntregar  = 12;

  (* Ocultas *)
  coMultiploVenda = 13;

function Buscar(
  pEmpresaId: Integer;
  pProdutoId: Integer;
  pNomeProduto: string;
  pQuantidadeRetirarAto: Double;
  pQuantidadeRetirar: Double;
  pQuantidadeEntregar: Double;
  pMultiploVenda: Double;
  pTela: TTela
): TRetornoTelaFinalizar< TArray<RecDefinicoesLoteVenda> >;
var
  i: Integer;
  vLotes: TArray<RecLotesDisponiveis>;
  vForm: TFormBuscarDefinicaoLotesVenda;
begin
  Result.Dados := nil;
  vForm := TFormBuscarDefinicaoLotesVenda.Create(nil);

  if pTela = ttAgendarItensSemPrevisao then begin
    vForm.sgLotes.OcultarColunas([coDispAto, coQtdeRetAto, coDispRetirar, coQtdeRetirar]);
    _Biblioteca.Visibilidade([vForm.StaticTextLuka3, vForm.StaticTextLuka4, vForm.stQuantidadeRetirarAto, vForm.stQuantidadeRetirar], False);
    vForm.sgLotes.Col := coQtdeEntregar;
  end;

  vForm.stProduto.Caption              := ' ' + NFormat(pProdutoId) + ' - ' + pNomeProduto;
  vForm.stQuantidadeRetirarAto.Caption := NFormatNEstoque(pQuantidadeRetirarAto);
  vForm.stQuantidadeRetirar.Caption    := NFormatNEstoque(pQuantidadeRetirar);
  vForm.stQuantidadeEntregar.Caption   := NFormatNEstoque(pQuantidadeEntregar);

  if Sessao.getParametros.DefinirLocalManual = 'N' then begin
    vForm.sgLotes.OcultarColunas([coEmpresaId, coNomeEmpresa]);
  end;

  vLotes := _EstoquesDivisao.BuscarLotesDisponiveis(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId, pProdutoId, Sessao.getParametros.DefinirLocalManual);
  for i := Low(vLotes) to High(vLotes) do begin
    vForm.sgLotes.Cells[coLote, i + 1]         := vLotes[i].Lote;
    vForm.sgLotes.Cells[coDtFabricacao, i + 1] := DFormatN(vLotes[i].DataFabricacao);
    vForm.sgLotes.Cells[coDtVencimento, i + 1] := DFormatN(vLotes[i].DataVencimento);
    vForm.sgLotes.Cells[coDispAto, i + 1]      := NFormatEstoque(vLotes[i].DisponivelAto);
    vForm.sgLotes.Cells[coDispRetirar, i + 1]  := NFormatEstoque(vLotes[i].DisponivelRetirar);
    vForm.sgLotes.Cells[coDispEntregar, i + 1] := NFormatEstoque(vLotes[i].DisponivelEntregar);
    vForm.sgLotes.Cells[coMultiploVenda, i + 1] := NFormatEstoque(pMultiploVenda);

    if Sessao.getParametros.DefinirLocalManual = 'S' then begin
      vForm.sgLotes.Cells[coLocalId, i + 1] := NFormat(vLotes[i].LocalId);
      vForm.sgLotes.Cells[coLocalNome, i + 1] := vLotes[i].LocalNome;
      vForm.sgLotes.Cells[coEmpresaId, i + 1] := NFormat(vLotes[i].EmpresaId);
      vForm.sgLotes.Cells[coNomeEmpresa, i + 1] := vLotes[i].EmpresaNome;
    end;

  end;
  vForm.sgLotes.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vLotes) );

  if Result.Ok(vForm.ShowModal, True) then begin
    for i := 1 to vForm.sgLotes.RowCount -1 do begin
      if
        (SFormatCurr(vForm.sgLotes.Cells[coQtdeRetAto, i]) = 0) and
        (SFormatCurr(vForm.sgLotes.Cells[coQtdeRetirar, i]) = 0) and
        (SFormatCurr(vForm.sgLotes.Cells[coQtdeEntregar, i]) = 0)
      then
        Continue;

      SetLength(Result.Dados, Length(Result.Dados) + 1);

      Result.Dados[High(Result.Dados)].Lote                 := vForm.sgLotes.Cells[coLote, i];

      if Sessao.getParametros.DefinirLocalManual = 'S' then begin
        Result.Dados[High(Result.Dados)].EmpresaId            := StrToInt(vForm.sgLotes.Cells[coEmpresaId, i]);
        Result.Dados[High(Result.Dados)].LocalId              := StrToInt(vForm.sgLotes.Cells[coLocalId, i]);
        Result.Dados[High(Result.Dados)].LocalNome            := vForm.sgLotes.Cells[coLocalNome, i];
      end;

      Result.Dados[High(Result.Dados)].QuantidadeRetirarAto := SFormatDouble( vForm.sgLotes.Cells[coQtdeRetAto, i] );
      Result.Dados[High(Result.Dados)].QuantidadeRetirar    := SFormatDouble( vForm.sgLotes.Cells[coQtdeRetirar, i] );
      Result.Dados[High(Result.Dados)].QuantidadeEntregar   := SFormatDouble( vForm.sgLotes.Cells[coQtdeEntregar, i] );
    end;
  end;
end;

procedure TFormBuscarDefinicaoLotesVenda.FormCreate(Sender: TObject);
begin
  inherited;
  if Sessao.getParametros.DefinirLocalManual = 'N' then
    sgLotes.OcultarColunas([coLocalId, coLocalNome]);
end;

function TFormBuscarDefinicaoLotesVenda.getQuantidadeTotal(pColuna: Integer): Currency;
var
  i: Integer;
begin
  Result := 0;
  for i := 1 to sgLotes.RowCount -1 do
    Result := Result + SFormatCurr(sgLotes.Cells[pColuna, i]);
end;

function TFormBuscarDefinicaoLotesVenda.ValidarQuantidadesDefinidas(pStaticText: TStaticText; pColuna: Integer): Boolean;
begin
  Result := True;
  if getQuantidadeTotal(pColuna) > SFormatCurr( pStaticText.Caption ) then begin
    _Biblioteca.Exclamar('A quantidade definida para o lote n�o pode ser maior que a quantidade de entrega!');
    Result := False;
  end;
end;

procedure TFormBuscarDefinicaoLotesVenda.sgLotesArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if not ACol in[
    coQtdeRetAto,
    coQtdeRetirar,
    coQtdeEntregar]
  then
    Exit;

  TextCell := NFormatNEstoque(SFormatDouble(TextCell));
  if TextCell = '' then
    Exit;

//  if not ValidarMultiplo(SFormatDouble(TextCell), SFormatDouble(sgLotes.Cells[coMultiploVenda, sgLotes.Row])) then begin
//    TextCell := '';
//    Exit;
//  end;

  if ACol = coQtdeRetAto then begin
    if not ValidarQuantidadesDefinidas( stQuantidadeRetirarAto, ACol ) then
      TextCell := '';
  end
  else if ACol = coQtdeRetirar then begin
    if not ValidarQuantidadesDefinidas( stQuantidadeRetirar, ACol ) then
      TextCell := '';
  end
  else begin
    if not ValidarQuantidadesDefinidas( stQuantidadeEntregar, ACol ) then
      TextCell := '';
  end;

  if
    (SFormatDouble( sgLotes.Cells[coQtdeRetAto, sgLotes.Row] ) > 0) and
    (SFormatDouble( sgLotes.Cells[coDispAto, sgLotes.Row] ) < SFormatDouble( sgLotes.Cells[coQtdeRetAto, sgLotes.Row] ))
  then begin
    Exclamar('N�o � permitido informar quantidade a retirar no ato maior que a quantidade dispon�vel do local!');
    TextCell := '';
  end;

  if
    (SFormatDouble( sgLotes.Cells[coQtdeRetirar, sgLotes.Row] ) > 0) and
    (SFormatDouble( sgLotes.Cells[coDispRetirar, sgLotes.Row] ) < SFormatDouble( sgLotes.Cells[coQtdeRetirar, sgLotes.Row] ))
  then begin
    Exclamar('N�o � permitido informar quantidade a retirar maior que a quantidade dispon�vel do local!');
    TextCell := '';
  end;

  if
    (SFormatDouble( sgLotes.Cells[coQtdeEntregar, sgLotes.Row] ) > 0) and
    (SFormatDouble( sgLotes.Cells[coDispEntregar, sgLotes.Row] ) < SFormatDouble( sgLotes.Cells[coQtdeEntregar, sgLotes.Row] ))
  then begin
    Exclamar('N�o � permitido informar quantidade a entregar maior que a quantidade dispon�vel do local!');
    TextCell := '';
  end;
end;

procedure TFormBuscarDefinicaoLotesVenda.sgLotesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coDispAto,
    coDispRetirar,
    coDispEntregar,
    coQtdeRetAto,
    coQtdeRetirar,
    coQtdeEntregar,
    coLocalId]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgLotes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormBuscarDefinicaoLotesVenda.sgLotesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in[coDispAto, coQtdeRetAto] then begin
    AFont.Color  := _Biblioteca.coCorFonteEdicao1;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao1;
  end
  else if ACol in[coDispRetirar, coQtdeRetirar] then begin
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
  end
  else if ACol in[coDispEntregar, coQtdeEntregar] then begin
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
  end;
end;

procedure TFormBuscarDefinicaoLotesVenda.sgLotesSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in[
    coQtdeRetAto,
    coQtdeRetirar,
    coQtdeEntregar]
  then
    sgLotes.Options := sgLotes.Options + [goEditing]
  else
    sgLotes.Options := sgLotes.Options - [goEditing];
end;

procedure TFormBuscarDefinicaoLotesVenda.VerificarRegistro(Sender: TObject);
begin
  if getQuantidadeTotal( coQtdeRetAto ) <> SFormatCurr(stQuantidadeRetirarAto.Caption) then begin
    _Biblioteca.Exclamar('A quantidade definida para "retirar no ato" n�o est� correta, verifique!');
    Abort;
  end;

  if getQuantidadeTotal( coQtdeRetirar ) <> SFormatCurr(stQuantidadeRetirar.Caption) then begin
    _Biblioteca.Exclamar('A quantidade definida para "retirar" n�o est� correta, verifique!');
    Abort;
  end;

  if getQuantidadeTotal( coQtdeEntregar ) <> SFormatCurr(stQuantidadeEntregar.Caption) then begin
    _Biblioteca.Exclamar('A quantidade definida para "entregar" n�o est� correta, verifique!');
    Abort;
  end;

  if
    (SFormatDouble( sgLotes.Cells[coQtdeRetAto, sgLotes.Row] ) > 0) and
    (SFormatDouble( sgLotes.Cells[coDispAto, sgLotes.Row] ) < SFormatDouble( sgLotes.Cells[coQtdeRetAto, sgLotes.Row] ))
  then begin
    Exclamar('Por favor pressione "ENTER" ap�s informar quantidade do produto!');
    Abort;
  end;

  if
    (SFormatDouble( sgLotes.Cells[coQtdeRetirar, sgLotes.Row] ) > 0) and
    (SFormatDouble( sgLotes.Cells[coDispRetirar, sgLotes.Row] ) < SFormatDouble( sgLotes.Cells[coQtdeRetirar, sgLotes.Row] ))
  then begin
    Exclamar('Por favor pressione "ENTER" ap�s informar quantidade do produto!');
    Abort;
  end;

  if
    (SFormatDouble( sgLotes.Cells[coQtdeEntregar, sgLotes.Row] ) > 0) and
    (SFormatDouble( sgLotes.Cells[coDispEntregar, sgLotes.Row] ) < SFormatDouble( sgLotes.Cells[coQtdeEntregar, sgLotes.Row] ))
  then begin
    Exclamar('Por favor pressione "ENTER" ap�s informar quantidade do produto!');
    Abort;
  end;

  inherited;
end;

end.
