unit ImpressaoListaContagemEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos, QRExport,
  QRPDFFilt, QRWebFilt, QRCtrls, QuickRpt, Vcl.ExtCtrls, _ListaContagem, _Sessao, _Biblioteca, _RecordsCadastros;

type
  TFormImpressaoListaContagemEstoque = class(TFormHerancaRelatoriosGraficos)
    qrbndColumnHeaderBand1: TQRBand;
    qr23: TQRLabel;
    qr24: TQRLabel;
    qr25: TQRLabel;
    qr27: TQRLabel;
    qrbndDetailBand1: TQRBand;
    qrProdutoId: TQRLabel;
    qrNomeProduto: TQRLabel;
    qrMarca: TQRLabel;
    qrUnidade: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    qrCodigoBarras: TQRLabel;
    QRLabel4: TQRLabel;
    qrCodigoOriginal: TQRLabel;
    QRLabel5: TQRLabel;
    qrLote: TQRLabel;
    QRLabel6: TQRLabel;
    qrEstoqueFisico: TQRLabel;
    QRLabel7: TQRLabel;
    qrNomeEnderecoEstoque: TQRLabel;
    procedure qrbndDetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure qrRelatorioA4BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrRelatorioA4NeedData(Sender: TObject; var MoreData: Boolean);
  private
    FPosicao: Integer;
    FEstoques: TArray<RecRelacaoEstoque>;
  public
    { Public declarations }
  end;

  procedure Imprimir(vEstoques: TArray<RecRelacaoEstoque>);

implementation

{$R *.dfm}

uses _EnderecoEstoque, _Produtos;

procedure Imprimir(vEstoques: TArray<RecRelacaoEstoque>);
var
  vForm: TFormImpressaoListaContagemEstoque;
  vImpressora: RecImpressora;
begin
  vImpressora.TipoImpressora := 'G';
  vImpressora.AbrirPreview   := True;

  vForm := TFormImpressaoListaContagemEstoque.Create(Application, vImpressora);
  vForm.PreencherCabecalho(Sessao.getEmpresaLogada.EmpresaId, True);

  vForm.FEstoques := vEstoques;

  if Sessao.getParametros.UtilEnderecoEstoque = 'N' then begin
    vForm.qrbndDetailBand1.Height := 18;
    VForm.qrEndereco.Visible := False;
    VForm.QRLabel7.Visible := False;
  end;

  vForm.Imprimir;

  vForm.Free;
end;

procedure TFormImpressaoListaContagemEstoque.qrbndDetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
var
  enderecos: TArray<RecEnderecosEstoque>;
begin
  inherited;
  qrProdutoId.Caption      := NFormat(FEstoques[FPosicao].ProdutoId);
  qrNomeProduto.Caption    := FEstoques[FPosicao].NomeProduto;
  qrMarca.Caption          := FEstoques[FPosicao].NomeMarca;
  qrUnidade.Caption        := FEstoques[FPosicao].UnidadeVenda;
  qrCodigoBarras.Caption   := FEstoques[FPosicao].CodigoBarras;
  qrCodigoOriginal.Caption := FEstoques[FPosicao].CodigoOriginal;
  qrLote.Caption           := FEstoques[FPosicao].Lote;
  qrEstoqueFisico.Caption  := FEstoques[FPosicao].EstoqueFisico;

  enderecos := _Produtos.BuscarEnderecosEstoqueCompleto(Sessao.getConexaoBanco, FEstoques[FPosicao].ProdutoId);
  qrNomeEnderecoEstoque.Caption := '';
  if enderecos <> nil then begin
    qrNomeEnderecoEstoque.Caption :=
      'Rua: ' + enderecos[0].nome_rua + ' ' +
      'M�dulo: ' + enderecos[0].nome_modulo + ' ' +
      'V�o: ' + enderecos[0].nome_vao + ' ' +
      'N�vel: ' + enderecos[0].nome_nivel;
  end;
end;

procedure TFormImpressaoListaContagemEstoque.qrRelatorioA4BeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicao := -1;
end;

procedure TFormImpressaoListaContagemEstoque.qrRelatorioA4NeedData(
  Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicao);
  MoreData := FPosicao < Length(FEstoques);
end;

end.
