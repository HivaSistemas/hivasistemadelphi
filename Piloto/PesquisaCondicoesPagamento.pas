unit PesquisaCondicoesPagamento;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _RecordsCadastros, _Sessao,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Biblioteca, _CondicoesPagamento, System.Math,
  Vcl.ExtCtrls;

type

  TFormPesquisaCondicoesPagamento = class(TFormHerancaPesquisas)
    procedure FormCreate(Sender: TObject);
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarCondicaoPagamento(pSomenteAtivos: Boolean; pFechamentoAcumulativo: Boolean; pSomenteEmpresaLogada: Boolean): TObject;

implementation

{$R *.dfm}

var
  FSomenteAtivos: Boolean;
  FFechamentoAcumulativo: Boolean;
  FSomenteEmpresaLogada: Boolean;

const
  cp_nome             = 2;
  cp_indice_acrescimo = 3;
  cp_ativo            = 4;

function PesquisarCondicaoPagamento(pSomenteAtivos: Boolean; pFechamentoAcumulativo: Boolean; pSomenteEmpresaLogada: Boolean): TObject;
var
  r: TObject;
begin
  FSomenteAtivos := pSomenteAtivos;
  FFechamentoAcumulativo := pFechamentoAcumulativo;
  FSomenteEmpresaLogada := pSomenteEmpresaLogada;

  r := _HerancaPesquisas.Pesquisar(TFormPesquisaCondicoesPagamento, _CondicoesPagamento.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := RecCondicoesPagamento(r);
end;

procedure TFormPesquisaCondicoesPagamento.BuscarRegistros;
var
  i: Integer;
  vCondicoesPagamento: TArray<RecCondicoesPagamento>;
begin
  inherited;

  vCondicoesPagamento :=
    _CondicoesPagamento.BuscarCondicoesPagamentos(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FSomenteAtivos,
      FFechamentoAcumulativo,
      FSomenteEmpresaLogada,
      Sessao.getEmpresaLogada.EmpresaId
    );

  if vCondicoesPagamento = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vCondicoesPagamento);

  for i := Low(vCondicoesPagamento) to High(vCondicoesPagamento) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]      := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]           := NFormat(vCondicoesPagamento[i].condicao_id);
    sgPesquisa.Cells[cp_nome, i + 1]             := vCondicoesPagamento[i].nome;
    sgPesquisa.Cells[cp_indice_acrescimo, i + 1] := NFormat(vCondicoesPagamento[i].indice_acrescimo, 4);
    sgPesquisa.Cells[cp_ativo, i + 1]            := vCondicoesPagamento[i].ativo;
  end;

  sgPesquisa.RowCount := IfThen(Length(vCondicoesPagamento) = 1, 2, High(vCondicoesPagamento) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaCondicoesPagamento.FormCreate(Sender: TObject);
begin
  inherited;
  if FSomenteAtivos then
    sgPesquisa.ColWidths[cp_ativo] := -1;
end;

procedure TFormPesquisaCondicoesPagamento.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, cp_ativo]) then
    vAlinhamento := taCenter
  else if ACol in[coCodigo, cp_indice_acrescimo] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
