unit FrameTipoNotaGerar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  _FrameHerancaPrincipal, Vcl.StdCtrls, _Sessao, CheckBoxLuka, _Biblioteca, _ValidacaoTipoNotaGerar;

type
  TFrTipoNotaGerar = class(TFrameHerancaPrincipal)
    grpDocumentosEmitir: TGroupBox;
    ckRecibo: TCheckBoxLuka;
    ckEmitirNFCe: TCheckBoxLuka;
    ckEmitirNFe: TCheckBoxLuka;
  private
    FValidacao: RecValidacaoTipoNotaGerar;

    function getTipoNotaGerar: string;
  public
    procedure setDadosPrincipais( pCadastroId: Integer; pCondicaoPagamentoId: Integer; pOrcamentoId: Integer );
    procedure VerificarRegistro;
  published
    property TipoNotaGerar: string read getTipoNotaGerar;
  end;

implementation

{$R *.dfm}

{ TFrTipoNotaGerar }

procedure TFrTipoNotaGerar.setDadosPrincipais( pCadastroId: Integer; pCondicaoPagamentoId: Integer; pOrcamentoId: Integer );
begin
  ckEmitirNFCe.Enabled := Sessao.getParametrosEmpresa.UtilizaNFCe = 'S';
  ckEmitirNFe.Enabled  := Sessao.getParametrosEmpresa.UtilizaNfe = 'S';

  FValidacao := _ValidacaoTipoNotaGerar.BuscarDadosValidacao( Sessao.getConexaoBanco, pCadastroId, pOrcamentoId, pCondicaoPagamentoId );
  if FValidacao.ExigirNotaProduto or FValidacao.ExigirNotaCliente or FValidacao.ExigirNotaCondicaoPagamento then begin
    _Biblioteca.Habilitar([ckEmitirNFCe, ckEmitirNFe], False);
    if FValidacao.SomenteNFe then
      ckEmitirNFe.Checked := True
    else
      ckEmitirNFCe.Checked := True;
  end;
end;

procedure TFrTipoNotaGerar.VerificarRegistro;
begin
  if ckEmitirNFCe.Checked and ckEmitirNFe.Checked then begin
    _Biblioteca.Exclamar('N�o � poss�vel emitir NFCe e NFe ao mesmo tempo, selecione apenas um tipo!');
    SetarFoco(ckEmitirNFCe);
    Abort;
  end;

  if FValidacao.SomenteNFe and ckEmitirNFCe.Checked then begin
    _Biblioteca.Exclamar('Documentos fiscais para este cliente deve ser somente NFe!');
    SetarFoco(ckEmitirNFe);
    Abort;
  end;
end;

function TFrTipoNotaGerar.getTipoNotaGerar: string;
begin
  Result := 'NI';
  if ckEmitirNFCe.Checked then
    Result := 'NE'
  else if ckEmitirNFe.Checked then
    Result := 'NF';
end;

end.
