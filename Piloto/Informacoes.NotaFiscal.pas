unit Informacoes.NotaFiscal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _RecordsNotasFiscais,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, GroupBoxLuka, Vcl.Mask, EditLukaData, _NotasFiscais,
  Vcl.ComCtrls, Vcl.Grids, GridLuka, _NotasFiscaisItens, _Sessao, _Biblioteca, System.StrUtils,
  System.Math, StaticTextLuka, Informacoes.Retiradas, Informacoes.Entrega, InformacoesAcumulado,
  SpeedButtonLuka, BuscarTextoCartaCorrecao, _NotasFiscaisCartasCorrecoes, _ComunicacaoNFE,
  Vcl.Menus, _FrameHerancaPrincipal, Frame.HerancaInsercaoExclusao, _NotasFiscaisReferencias,
  FrameReferenciasNotasFiscais, InformacoesProduto;

type
  TFormInformacoesNotaFiscal = class(TFormHerancaFinalizar)
    lb1: TLabel;
    eNotaFiscalId: TEditLuka;
    lbl1: TLabel;
    eCadastro: TEditLuka;
    eNumeroNota: TEditLuka;
    lbl2: TLabel;
    stTipoNota: TStaticText;
    lbl3: TLabel;
    eSerieNota: TEditLuka;
    lbl4: TLabel;
    eModeloNota: TEditLuka;
    pcDados: TPageControl;
    tsGerais: TTabSheet;
    lbllb3: TLabel;
    eDataCadastro: TEditLukaData;
    lbl5: TLabel;
    eDataEmissao: TEditLukaData;
    lbMovimentoBase: TLabel;
    eMovimentoBaseId: TEditLuka;
    stStatusNota: TStaticText;
    lbl14: TLabel;
    eCFOPNota: TEditLuka;
    lbl15: TLabel;
    eValorBaseCalculoICMS: TEditLuka;
    lbl16: TLabel;
    eValorICMS: TEditLuka;
    lbl17: TLabel;
    eBaseCalculoICMSST: TEditLuka;
    lbl18: TLabel;
    eValorICMSST: TEditLuka;
    eValorTotalProdutos: TEditLuka;
    lbl19: TLabel;
    lbl20: TLabel;
    eValorFrete: TEditLuka;
    lbl21: TLabel;
    eValorSeguro: TEditLuka;
    lbl22: TLabel;
    eValorDesconto: TEditLuka;
    lbl23: TLabel;
    eValorOutrasDespesas: TEditLuka;
    eValorIPI: TEditLuka;
    lbl24: TLabel;
    eValorTotalNota: TEditLuka;
    lbl25: TLabel;
    meInformacoesComplementares: TMemo;
    lbl26: TLabel;
    tsItens: TTabSheet;
    sgProdutos: TGridLuka;
    lbl28: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    lbl9: TLabel;
    lbl10: TLabel;
    lbl11: TLabel;
    lbl12: TLabel;
    lbl13: TLabel;
    eLogradouro: TEditLuka;
    eComplemento: TEditLuka;
    eNumero: TEditLuka;
    eBairro: TEditLuka;
    eCidade: TEditLuka;
    eEstadoId: TEditLuka;
    eCEP: TEditLuka;
    eEmpresaNota: TEditLuka;
    lbl27: TLabel;
    stDanfeImpresso: TStaticText;
    stEnviouDanfeEmail: TStaticText;
    eChaveAcessoNFe: TMaskEdit;
    lbMovimento: TLabel;
    eMovimentoId: TEditLuka;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    StaticTextLuka3: TStaticTextLuka;
    StaticTextLuka4: TStaticTextLuka;
    StaticTextLuka5: TStaticTextLuka;
    stTipoMovimento: TStaticText;
    tsCancelamento: TTabSheet;
    lbl30: TLabel;
    eDataCancelamento: TEditLukaData;
    eMotivoCancelamento: TEditLuka;
    lbl31: TLabel;
    Label1: TLabel;
    eProtocoloCancelamento: TEditLuka;
    sbInformacoesBase: TSpeedButtonLuka;
    sbInformacoesMovimentoId: TSpeedButtonLuka;
    sbLogs: TSpeedButtonLuka;
    tsCartasCorrecoes: TTabSheet;
    sgCorrecoes: TGridLuka;
    pmCartas: TPopupMenu;
    miInserirCorrecao: TMenuItem;
    miEnviarCorrecao: TMenuItem;
    tsReferencias: TTabSheet;
    FrReferenciasNotasFiscais: TFrReferenciasNotasFiscais;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sbInformacoesMovimentoIdClick(Sender: TObject);
    procedure sbInformacoesBaseClick(Sender: TObject);
    procedure sbLogsClick(Sender: TObject);
    procedure miInserirCorrecaoClick(Sender: TObject);
    procedure miEnviarCorrecaoClick(Sender: TObject);
    procedure tsCartasCorrecoesShow(Sender: TObject);
    procedure sgCorrecoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgCorrecoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgProdutosDblClick(Sender: TObject);
  end;

procedure Informar(const pNotaFiscalId: Integer);

implementation

uses
  Informacoes.Orcamento, Informacoes.Devolucao, Logs;

{$R *.dfm}

const
  coProdutoId                      = 0;
  coNomeProduto                    = 1;
  coCST                            = 2;
  coPrecoUnitario                  = 3;
  coQuantidade                     = 4;
  coUnidade                        = 5;
  coValorDesconto                  = 6;
  coValorOutrasDespesas            = 7;
  coValorTotal                     = 8;
  coBaseCalculoICMS                = 9;
  coIndiceReducaoBaseCalculoICMS   = 10;
  coPercentualICMS                 = 11;
  coValorICMS                      = 12;
  coBaseCalculoICMSST              = 13;
  coIndiceReducaoBaseCalculoICMSST = 14;
  coValorICMSST                    = 15;
  coBaseCalculoPIS                 = 16;
  coPercentualPIS                  = 17;
  coValorPIS                       = 18;
  coBaseCalculoCOFINS              = 19;
  coPercentualCOFINS               = 20;
  coValorCOFINS                    = 21;
  coPercentualIVA                  = 22;
  coPrecoPauta                     = 23;
  coPercentualIPI                  = 24;
  coValorIPI                       = 25;

  (* Colunas ocultas *)
  coInformacoesAdicionais          = 26;
  coItemId                         = 27;

  (* Grid de cartas de corre��es *)
  ccSequencia        = 0;
  ccStatus           = 1;
  ccDataHoraCadastro = 2;
  ccUsuarioCadastro  = 3;
  ccTextoCorrecao    = 4;

procedure Informar(const pNotaFiscalId: Integer);
var
  i: Integer;
  vNotaFiscal: TArray<RecNotaFiscal>;
  vNotaItens: TArray<RecNotaFiscalItem>;

  vForm: TFormInformacoesNotaFiscal;

  vMovimentoId: Integer;
  vMovimentoBaseId: Integer;
  vMovimento: string;
  vMovimentoBase: string;

  function FormatarChaveNFe(pValor: string): string;
  var
    i: Integer;
    vInc: Integer;
  begin  
    vInc := 0;
    for i := 1 to Length(pValor) do begin
      Result := Result + pValor[i];

      Inc(vInc);    
      if vInc = 4 then begin
        Result := Result + '.';
        vInc := 0;      
      end;
    end;
  end;
  
begin
  if pNotaFiscalId = 0 then
    Exit;

  vNotaFiscal := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 0, [pNotaFiscalId]);
  if vNotaFiscal = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vNotaItens := _NotasFiscaisItens.BuscarNotasFiscaisItens(Sessao.getConexaoBanco, 0, [pNotaFiscalId]);
  if vNotaItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vForm := TFormInformacoesNotaFiscal.Create(Application);
  vForm.ReadOnlyTodosObjetos(True);

  vForm.eNotaFiscalId.AsInt    := vNotaFiscal[0].nota_fiscal_id;
  vForm.eCadastro.Text         := NFormat(vNotaFiscal[0].cadastro_id) + ' - ' + vNotaFiscal[0].razao_social_destinatario;
  vForm.eEmpresaNota.Text      := NFormat(vNotaFiscal[0].empresa_id) + ' - ' + vNotaFiscal[0].razao_social_emitente;
  vForm.stTipoNota.Caption     := vNotaFiscal[0].TipoNotaAnalitico;
  vForm.eNumeroNota.AsInt      := vNotaFiscal[0].numero_nota;
  vForm.eModeloNota.Text       := vNotaFiscal[0].ModeloNota;
  vForm.eSerieNota.Text        := vNotaFiscal[0].SerieNota;
  vForm.eCFOPNota.Text         := vNotaFiscal[0].CfopId + ' - ' + vNotaFiscal[0].natureza_operacao;
  vForm.eDataCadastro.AsData   := vNotaFiscal[0].data_hora_cadastro;
  vForm.eDataEmissao.AsData    := vNotaFiscal[0].data_hora_emissao;

  vMovimento :=
    _Biblioteca.Decode(
      vNotaFiscal[0].TipoMovimento,[
      'VRA', 'Retirada',
      'VRE', 'Retirada',
      'VEN', 'Entrega',
      'DRE', 'Devolu��o',
      'DEN', 'Devolu��o',
      'ACU', 'Acumulado',
      'DAC', 'Devolu��o',
      'OUT', 'Outra nota',
      'TPE', 'Trans.prod.',
      'TFI', 'Transf. fisc.',
      'NNE', 'NFe de NFCe',
      'DEF', 'Dev.entr.NF',
      'PEN', 'Prod.entr.NF',
      'PSA', 'Prod.said.NF',
      'AEE', 'Ajus.entr.NF',
      'AES', 'Ajus.said.NF',
      'REN', 'Ret.entrega']
    );

  vMovimentoId :=
    _Biblioteca.Decode(
      vNotaFiscal[0].TipoMovimento,[
      'VRA', vNotaFiscal[0].RetiradaId,
      'VRE', vNotaFiscal[0].RetiradaId,
      'VEN', vNotaFiscal[0].EntregaId,
      'DRE', vNotaFiscal[0].DevolucaoId,
      'DEN', vNotaFiscal[0].DevolucaoId,
      'DEF', 0,
      'ACU', vNotaFiscal[0].AcumuladoId,
      'DAC', vNotaFiscal[0].DevolucaoId,
      'OUT', vNotaFiscal[0].OutraNotaId,
      'TPE', vNotaFiscal[0].TransferenciaProdutosId,
      'TFI', 0,
      'NNE', vNotaFiscal[0].NotaFiscalOrigemCupomId,
      'REN', vNotaFiscal[0].EntregaId,
      'PEN', vNotaFiscal[0].ProducaoId,
      'PSA', vNotaFiscal[0].ProducaoId,
      'AEE', vNotaFiscal[0].AjusteEstoqueId,
      'AES', vNotaFiscal[0].AjusteEstoqueId]
    );

  vMovimentoBase :=
    _Biblioteca.Decode(
      vNotaFiscal[0].TipoMovimento,[
      'VRA', 'Pedido',
      'VRE', 'Pedido',
      'VEN', 'Pedido',
      'DRE', 'Pedido',
      'DEN', 'Pedido',
      'ACU', 'Acumulado',
      'DAC', 'Acumulado',
      'OUT', '',
      'TPE', '',
      'TFI', '',
      'NNE', '',
	    'DEF', 'Dev.entr.',
      'REN', 'Pedido',
      'PEN', 'Produ��o',
      'PSA', 'Produ��o',
      'AEE', 'Ajuste',
      'AES', 'Ajuste']
    );

  vMovimentoBaseId :=
    _Biblioteca.Decode(
      vNotaFiscal[0].TipoMovimento,[
      'VRA', vNotaFiscal[0].orcamento_id,
      'VRE', vNotaFiscal[0].orcamento_id,
      'VEN', vNotaFiscal[0].orcamento_id,
      'DRE', vNotaFiscal[0].orcamento_id,
      'DEN', vNotaFiscal[0].orcamento_id,
      'ACU', vNotaFiscal[0].AcumuladoId,
      'DAC', vNotaFiscal[0].AcumuladoId,
      'OUT', 0,
      'TPE', 0,
      'TFI', 0,
      'NNE', 0,
	    'DEF', vNotaFiscal[0].DevolucaoEntradaId,
      'REN', vNotaFiscal[0].OrcamentoBaseId,
      'PEN', 0,
      'PSA', 0,
      'AEE', 0,
      'AES', 0]
    );

  vMovimentoBase :=  vMovimentoBase;

  vForm.lbMovimento.Caption := ' N� ' + vMovimento;
  vForm.eMovimentoId.SetInformacao(vMovimentoId, '', vNotaFiscal[0].TipoMovimento);

  vForm.lbMovimentoBase.Caption := ' N� ' + vMovimentoBase;
  vForm.eMovimentoBaseId.SetInformacao(vMovimentoBaseId, '', vNotaFiscal[0].TipoMovimento);

  vForm.stStatusNota.Caption         := vNotaFiscal[0].StatusNotaAnalitico;
  vForm.stStatusNota.Font.Color :=
    _Biblioteca.Decode(
      vNotaFiscal[0].status,[
      'E', clBlue,
      'N', $000080FF,
      'C', clRed,
      'D', clRed]      
    );
  
  vForm.eChaveAcessoNFe.Text         := FormatarChaveNFe( _Biblioteca.RetornaNumeros(vNotaFiscal[0].chave_nfe) );
  vForm.eDataCancelamento.AsData     := vNotaFiscal[0].DataHoraCancelamentoNota;
  vForm.eMotivoCancelamento.Text     := vNotaFiscal[0].MotivoCancelamentoNota;
  vForm.stDanfeImpresso.Caption      := _Biblioteca.SimNao(vNotaFiscal[0].danfe_impresso);
  vForm.stDanfeImpresso.Font.Color   := _Biblioteca.AzulVermelho(vNotaFiscal[0].danfe_impresso);
  vForm.stEnviouDanfeEmail.Caption   := _Biblioteca.SimNao(vNotaFiscal[0].enviou_email_nfe_cliente);
  vForm.stEnviouDanfeEmail.Font.Color:= _Biblioteca.AzulVermelho(vNotaFiscal[0].enviou_email_nfe_cliente);
  vForm.eLogradouro.Text             := vNotaFiscal[0].logradouro_destinatario;
  vForm.eComplemento.Text            := vNotaFiscal[0].complemento_destinatario;
  vForm.eNumero.Text                 := vNotaFiscal[0].numero_destinatario;
  vForm.eBairro.Text                 := vNotaFiscal[0].nome_bairro_destinatario;
  vForm.eCidade.Text                 := vNotaFiscal[0].nome_cidade_destinatario;
  vForm.eEstadoId.Text               := vNotaFiscal[0].estado_id_destinatario;
  vForm.eCEP.Text                    := vNotaFiscal[0].cep_destinatario;
  vForm.eProtocoloCancelamento.Text  := vNotaFiscal[0].ProtocoloCancelamentoNFe;
  vForm.stTipoMovimento.Caption      := vNotaFiscal[0].TipoMovimentoAnalitico;

  vForm.eValorBaseCalculoICMS.AsCurr := vNotaFiscal[0].base_calculo_icms;
  vForm.eValorICMS.AsCurr            := vNotaFiscal[0].valor_icms;
  vForm.eBaseCalculoICMSST.AsCurr    := vNotaFiscal[0].base_calculo_icms_st;
  vForm.eValorICMSST.AsCurr          := vNotaFiscal[0].valor_icms_st;
  vForm.eValorTotalProdutos.AsCurr   := vNotaFiscal[0].valor_total_produtos;
  vForm.eValorFrete.AsCurr           := vNotaFiscal[0].valor_frete;
  vForm.eValorSeguro.AsCurr          := vNotaFiscal[0].valor_seguro;
  vForm.eValorDesconto.AsCurr        := vNotaFiscal[0].valor_desconto;
  vForm.eValorOutrasDespesas.AsCurr  := vNotaFiscal[0].valor_outras_despesas;
  vForm.eValorIPI.AsCurr             := vNotaFiscal[0].valor_ipi;
  vForm.eValorTotalNota.AsCurr       := vNotaFiscal[0].valor_total;
  vForm.meInformacoesComplementares.Text := vNotaFiscal[0].informacoes_complementares;

  for i := Low(vNotaItens) to High(vNotaItens) do begin
    vForm.sgProdutos.Cells[coProdutoId, i + 1]                      := NFormat(vNotaItens[i].produto_id);
    vForm.sgProdutos.Cells[coNomeProduto, i + 1]                    := vNotaItens[i].nome_produto;
    vForm.sgProdutos.Cells[coCST, i + 1]                            := vNotaItens[i].cst;
    vForm.sgProdutos.Cells[coPrecoUnitario, i + 1]                  := NFormat(vNotaItens[i].preco_unitario);
    vForm.sgProdutos.Cells[coQuantidade, i + 1]                     := NFormat(vNotaItens[i].quantidade);
    vForm.sgProdutos.Cells[coUnidade, i + 1]                        := vNotaItens[i].unidade;
    vForm.sgProdutos.Cells[coValorDesconto, i + 1]                  := NFormatN(vNotaItens[i].valor_total_desconto);
    vForm.sgProdutos.Cells[coValorOutrasDespesas, i + 1]            := NFormatN(vNotaItens[i].valor_total_outras_despesas);
    vForm.sgProdutos.Cells[coValorTotal, i + 1]                     := NFormatN(vNotaItens[i].valor_total);
    vForm.sgProdutos.Cells[coBaseCalculoICMS, i + 1]                := NFormatN(vNotaItens[i].base_calculo_icms);
    vForm.sgProdutos.Cells[coIndiceReducaoBaseCalculoICMS, i + 1]   := NFormatN(vNotaItens[i].indice_reducao_base_icms, 5);
    vForm.sgProdutos.Cells[coPercentualICMS, i + 1]                 := NFormatN(vNotaItens[i].percentual_icms);
    vForm.sgProdutos.Cells[coValorICMS, i + 1]                      := NFormatN(vNotaItens[i].valor_icms);
    vForm.sgProdutos.Cells[coBaseCalculoICMSST, i + 1]              := NFormatN(vNotaItens[i].base_calculo_icms_st);
    vForm.sgProdutos.Cells[coIndiceReducaoBaseCalculoICMSST, i + 1] := NFormatN(vNotaItens[i].indice_reducao_base_icms_st, 5);
    vForm.sgProdutos.Cells[coValorICMSST, i + 1]                    := NFormatN(vNotaItens[i].valor_icms_st);
    vForm.sgProdutos.Cells[coBaseCalculoPIS, i + 1]                 := NFormatN(vNotaItens[i].base_calculo_pis);
    vForm.sgProdutos.Cells[coPercentualPIS, i + 1]                  := NFormatN(vNotaItens[i].percentual_pis);
    vForm.sgProdutos.Cells[coValorPIS, i + 1]                       := NFormatN(vNotaItens[i].valor_pis);
    vForm.sgProdutos.Cells[coBaseCalculoCOFINS, i + 1]              := NFormatN(vNotaItens[i].base_calculo_cofins);
    vForm.sgProdutos.Cells[coPercentualCOFINS, i + 1]               := NFormatN(vNotaItens[i].percentual_cofins);
    vForm.sgProdutos.Cells[coValorCOFINS, i + 1]                    := NFormatN(vNotaItens[i].valor_cofins);
    vForm.sgProdutos.Cells[coPercentualIVA, i + 1]                  := NFormatN(vNotaItens[i].iva);
    vForm.sgProdutos.Cells[coPrecoPauta, i + 1]                     := NFormatN(vNotaItens[i].preco_pauta);
    vForm.sgProdutos.Cells[coPercentualIPI, i + 1]                  := NFormatN(vNotaItens[i].percentual_ipi);
    vForm.sgProdutos.Cells[coValorIPI, i + 1]                       := NFormatN(vNotaItens[i].valor_ipi);
    vForm.sgProdutos.Cells[coInformacoesAdicionais, i + 1]          := vNotaItens[i].informacoes_adicionais;
    vForm.sgProdutos.Cells[coItemId, i + 1]                         := NFormat(vNotaItens[i].item_id);
  end;
  vForm.sgProdutos.SetLinhasGridPorTamanhoVetor( Length(vNotaItens) );

  vForm.FrReferenciasNotasFiscais.Referencias := _NotasFiscaisReferencias.BuscarNotasFiscaisReferencias(Sessao.getConexaoBanco, 0, [pNotaFiscalId]);

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesNotaFiscal.FormShow(Sender: TObject);
begin
  inherited;
  pcDados.ActivePageIndex := 0;
end;

procedure TFormInformacoesNotaFiscal.miEnviarCorrecaoClick(Sender: TObject);
var
  vNFe: TComunicacaoNFe;
begin
  inherited;
  if sgCorrecoes.Cells[ccStatus, sgCorrecoes.Row] = 'Emitida' then begin
    _Biblioteca.Exclamar('Esta carta de corre��o j� foi emitida!');
    Abort;
  end;

  vNFe := TComunicacaoNFe.Create(nil);

  if
    vNFe.EnviarCorrecao(
      eNotaFiscalId.AsInt,
      eChaveAcessoNFe.Text,
      SFormatInt(sgCorrecoes.Cells[ccSequencia, sgCorrecoes.Row]),
      sgCorrecoes.Cells[ccTextoCorrecao, sgCorrecoes.Row]
    )
  then
    tsCartasCorrecoesShow(Sender);

  vNFe.Free;
end;

procedure TFormInformacoesNotaFiscal.miInserirCorrecaoClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;

  if stStatusNota.Caption <> 'Emitida' then begin
    Exclamar('Somente uma NFe j� emitida pode ter uma carta de corre��o vinculada!');
    Abort;
  end;

  if stTipoNota.Caption <> 'NF-e' then begin
    Exclamar('Somente uma NFe pode ter a uma carta de corre��o vinculada!');
    Abort;
  end;

  vRetTela := BuscarTextoCartaCorrecao.buscarCorrecao(eNotaFiscalId.AsInt);
  if vRetTela.BuscaCancelada then
    Exit;

  tsCartasCorrecoesShow(Sender);
end;

procedure TFormInformacoesNotaFiscal.sbInformacoesBaseClick(Sender: TObject);
begin
  inherited;

  if Em(eMovimentoBaseId.TipoMovimentoInformacao, ['VRA', 'VRE', 'VEN', 'DRE', 'DEN', 'REN']) then
    Informacoes.Orcamento.Informar(eMovimentoBaseId.IdInformacao)
  else if Em(eMovimentoBaseId.TipoMovimentoInformacao, ['ACU', 'DAC']) then
    InformacoesAcumulado.Informar(eMovimentoBaseId.IdInformacao);
end;

procedure TFormInformacoesNotaFiscal.sbInformacoesMovimentoIdClick(Sender: TObject);
begin
  inherited;

  if Em(eMovimentoId.TipoMovimentoInformacao, ['VRA', 'VRE']) then
    Informacoes.Retiradas.Informar(eMovimentoId.IdInformacao)
  else if Em(eMovimentoId.TipoMovimentoInformacao, ['VEN', 'REN']) then
    Informacoes.Entrega.Informar(eMovimentoId.IdInformacao)
  else if Em(eMovimentoId.TipoMovimentoInformacao, ['DRE', 'DEN', 'DAC']) then
    Informacoes.Devolucao.Informar(eMovimentoId.IdInformacao)
  else if eMovimentoId.TipoMovimentoInformacao = 'ACU' then
    InformacoesAcumulado.Informar(eMovimentoId.IdInformacao);
end;

procedure TFormInformacoesNotaFiscal.sbLogsClick(Sender: TObject);
begin
  inherited;
  if pcDados.ActivePage = tsItens then
    Logs.Iniciar('LOGS_NOTAS_FISCAIS_ITENS', ['NOTA_FISCAL_ID', 'ITEM_ID'], 'VW_TIPOS_ALTER_LOGS_NTF_ITENS', [eNotaFiscalId.AsInt, SFormatInt(sgProdutos.Cells[coItemId, sgProdutos.Row])])
  else
    Logs.Iniciar('LOGS_NOTAS_FISCAIS', ['NOTA_FISCAL_ID'], 'VW_TIPOS_ALTER_LOGS_NTF', [eNotaFiscalId.AsInt]);
end;

procedure TFormInformacoesNotaFiscal.sgCorrecoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ccSequencia] then
    vAlinhamento := taRightJustify
  else if ACol = ccStatus then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgCorrecoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesNotaFiscal.sgCorrecoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = ccStatus then begin
    AFont.Style := [fsBold];
    if sgCorrecoes.Cells[ccStatus, ARow] = 'N�o emitida' then
      AFont.Color := clRed
    else
      AFont.Color := clNavy;
  end;
end;

procedure TFormInformacoesNotaFiscal.sgProdutosDblClick(Sender: TObject);
begin
  inherited;
  InformacoesProduto.Informar( SFormatInt(sgProdutos.Cells[coProdutoId, sgProdutos.Row]) );
end;

procedure TFormInformacoesNotaFiscal.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coNomeProduto, coUnidade] then
    vAlinhamento := taLeftJustify
  else if ACol = coCST then
    vAlinhamento := taCenter
  else
    vAlinhamento := taRightJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesNotaFiscal.tsCartasCorrecoesShow(Sender: TObject);
var
  i: Integer;
  vCorrecoes: TArray<RecNotasFiscaisCartasCorrecoes>;
begin
  inherited;

  sgCorrecoes.ClearGrid();
  vCorrecoes :=
    _NotasFiscaisCartasCorrecoes.BuscarNotasFiscaisCartasCorrecoes(
      Sessao.getConexaoBanco,
      0,
      [eNotaFiscalId.AsInt]
    );

  for i := Low(vCorrecoes) to High(vCorrecoes) do begin
    sgCorrecoes.Cells[ccSequencia, i + 1]        := NFormat(vCorrecoes[i].Sequencia);
    sgCorrecoes.Cells[ccStatus, i + 1]           := vCorrecoes[i].StatusAnalitico;
    sgCorrecoes.Cells[ccDataHoraCadastro, i + 1] := DHFormat(vCorrecoes[i].DataHoraCadastro);
    sgCorrecoes.Cells[ccUsuarioCadastro, i + 1]  := getInformacao(vCorrecoes[i].UsuarioCadastroId, vCorrecoes[i].NomeUsuarioCadastro);
    sgCorrecoes.Cells[ccTextoCorrecao, i + 1]    := vCorrecoes[i].TextoCorrecao;
  end;
  sgCorrecoes.SetLinhasGridPorTamanhoVetor( Length(vCorrecoes) );
end;

end.
