unit _CalculosSugestaoCompras;

interface

uses
  System.SysUtils, _SugestaoCompras, _OperacoesBancoDados, _Sessao, _Biblioteca, System.Math;

function CalcularSugestao(
  pEmpresaId: Integer;
  pApenasItensComSugestao: Boolean;
  pDiasSuprimento: Integer;
  pPercentualCrescimento: Double;
  pDatasIniciaisMedias: TArray<TDate>;
  pDatasFinaisMedias: TArray<TDate>;
  pItens: TArray<RecItensSugestao>;
  var pQtdeDiasUteisPeriodoSelecionado: Integer;
  var pQtdeDiasSuprimento: Integer
): TArray<RecItensSugestao>;

implementation

function CalcularSugestao(
  pEmpresaId: Integer;
  pApenasItensComSugestao: Boolean;
  pDiasSuprimento: Integer;
  pPercentualCrescimento: Double;
  pDatasIniciaisMedias: TArray<TDate>;
  pDatasFinaisMedias: TArray<TDate>;
  pItens: TArray<RecItensSugestao>;
  var pQtdeDiasUteisPeriodoSelecionado: Integer;
  var pQtdeDiasSuprimento: Integer
): TArray<RecItensSugestao>;
var
  i: Integer;
  vQtdeDiasUteisPeriodoSelecionado: Integer;
begin
  pDiasSuprimento :=
    _SugestaoCompras.getDiasUteisPeriodo(
      Sessao.getConexaoBanco,
      pEmpresaId,
      Sessao.getData,
      Sessao.getData + pDiasSuprimento
    );

  vQtdeDiasUteisPeriodoSelecionado := 0;
  for i := Low(pDatasIniciaisMedias) to High(pDatasFinaisMedias) do begin
    vQtdeDiasUteisPeriodoSelecionado :=
      vQtdeDiasUteisPeriodoSelecionado +
        _SugestaoCompras.getDiasUteisPeriodo(
          Sessao.getConexaoBanco,
          pEmpresaId,
          pDatasIniciaisMedias[i],
          pDatasFinaisMedias[i]
        );
  end;

  pQtdeDiasUteisPeriodoSelecionado := vQtdeDiasUteisPeriodoSelecionado;
  pQtdeDiasSuprimento := pDiasSuprimento;

  Result := nil;
  for i := Low(pItens) to High(pItens) do begin
    SetLength(Result, Length(Result) + 1);

    Result[High(Result)] := pItens[i];
    Result[High(Result)].MediaVendasDiaria    := pItens[i].VendasPeriodo / vQtdeDiasUteisPeriodoSelecionado;
    Result[High(Result)].QtdeDiasUteisPeriodo := vQtdeDiasUteisPeriodoSelecionado;
    Result[High(Result)].DiasEstoque          := 0;

    if (pItens[i].Estoque > 0) and (Result[High(Result)].MediaVendasDiaria > 0) then
      Result[High(Result)].DiasEstoque        :=  Ceil(pItens[i].Estoque / Result[High(Result)].MediaVendasDiaria);

    Result[High(Result)].SugestaoCompra       := (Result[High(Result)].MediaVendasDiaria * pDiasSuprimento * (1 + pPercentualCrescimento * 0.01)) - pItens[i].Estoque - pItens[i].ComprasPendentes;
    if Result[High(Result)].SugestaoCompra > 0 then
      Result[High(Result)].SugestaoCompra := _Biblioteca.RetornaValorDeAcordoMultiplo( Result[High(Result)].SugestaoCompra, pItens[i].MultiploCompra )
    else
      Result[High(Result)].SugestaoCompra := 0;

    if pApenasItensComSugestao then begin
      if NFormatNEstoque(Result[High(Result)].SugestaoCompra) = '' then
        SetLength(Result, Length(Result) - 1);
    end;
  end;
end;

end.
