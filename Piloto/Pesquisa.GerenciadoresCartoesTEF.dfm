inherited FormPesquisaGerenciadoresCartoesTEF: TFormPesquisaGerenciadoresCartoesTEF
  Caption = 'Pesquisa de gerenciadores de cart'#245'es TEF'
  ClientHeight = 256
  ClientWidth = 477
  ExplicitWidth = 483
  ExplicitHeight = 284
  PixelsPerInch = 96
  TextHeight = 14
  inherited eValorPesquisa: TEditLuka
    Width = 317
    ExplicitWidth = 317
  end
  inherited sgPesquisa: TGridLuka
    Width = 469
    Height = 209
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Gerenciador'
      'Ativo')
    RealColCount = 4
    ExplicitWidth = 469
    ExplicitHeight = 209
    ColWidths = (
      28
      55
      199
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Left = 8
    Top = 224
    Text = '0'
    ExplicitLeft = 8
    ExplicitTop = 224
  end
end
