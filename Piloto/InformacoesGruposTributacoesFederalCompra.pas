unit InformacoesGruposTributacoesFederalCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, ComboBoxLuka, CheckBoxLuka, EditLuka, Vcl.Buttons, Vcl.ExtCtrls;

type
  TFormInformacoesGruposTributacoesFederalCompra = class(TFormHerancaFinalizar)
    Label1: TLabel;
    eID: TEditLuka;
    ckAtivo: TCheckBoxLuka;
    lb25: TLabel;
    lb3: TLabel;
    lb5: TLabel;
    lb1: TLabel;
    cbOrigemProduto: TComboBoxLuka;
    cbCstEntradaPIS: TComboBoxLuka;
    cbCstEntradaCOFINS: TComboBoxLuka;
    eDescricao: TEditLuka;
    procedure FormCreate(Sender: TObject);
  end;

procedure Informar(pGrupoTribFederalCompraId: Integer);

implementation

{$R *.dfm}

uses _GruposTribFederalCompra, _Biblioteca, _Sessao;

procedure Informar(pGrupoTribFederalCompraId: Integer);
var
  vGrupo: TArray<RecGruposTribFederalCompra>;
  vForm: TFormInformacoesGruposTributacoesFederalCompra;
begin
  if pGrupoTribFederalCompraId = 0 then
    Exit;

  vGrupo := _GruposTribFederalCompra.BuscarGruposTribFederalCompra(Sessao.getConexaoBanco, 0, [pGrupoTribFederalCompraId]);
  if vGrupo = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vForm := TFormInformacoesGruposTributacoesFederalCompra.Create(nil);

  vForm.eID.SetInformacao(vGrupo[0].GrupoTribFederalCompraId);
  vForm.eDescricao.SetInformacao(vGrupo[0].Descricao);
  vForm.ckAtivo.CheckedStr          := vGrupo[0].Ativo;
  vForm.cbOrigemProduto.AsInt       := vGrupo[0].OrigemProduto;
  vForm.cbCstEntradaPIS.AsString    := vGrupo[0].CstPis;
  vForm.cbCstEntradaCOFINS.AsString := vGrupo[0].CstCofins;

  vForm.ShowModal;
  vForm.Free;
end;

procedure TFormInformacoesGruposTributacoesFederalCompra.FormCreate(Sender: TObject);
begin
  inherited;
  Sessao.SetComboBoxPisCofins(cbCstEntradaPIS);
  Sessao.SetComboBoxPisCofins(cbCstEntradaCOFINS);
end;

end.
