unit CadastroCFOP;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsCadastros, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, _Cfop, _RecordsEspeciais, PesquisaCFOP,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameCFOPs, _CfopParametrosFiscaisEmpr,
  FramePlanosFinanceiros, CheckBoxLuka, GroupBoxLuka, Vcl.ComCtrls,
  FrameCfopParametrosFiscaisEmpresas;

type
  TFormCadastroCFOP = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
    pcParametros: TPageControl;
    tsPrincipais: TTabSheet;
    tsParametrosCompra: TTabSheet;
    lb2: TLabel;
    eDescricaoCFOP: TMemo;
    GroupBoxLuka1: TGroupBoxLuka;
    ckEntradaMercBonificacao: TCheckBoxLuka;
    ckEntradaMercUsoConsumo: TCheckBoxLuka;
    ckEntradaMercRevenda: TCheckBoxLuka;
    ckEntradaConhecimentoFrete: TCheckBoxLuka;
    FrCFOPEntrada: TFrCFOPs;
    FrPlanoFinanceiroEntrada: TFrPlanosFinanceiros;
    ckEntradaServicos: TCheckBoxLuka;
    ckEntradaMercadorias: TCheckBoxLuka;
    tsFiscal: TTabSheet;
    FrParametrosPorEmpresa: TFrCfopParametrosFiscaisEmpresas;
    ckNaoExigirItensEntrada: TCheckBoxLuka;
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); reintroduce;
    procedure ckEntradaMercadoriasClick(Sender: TObject);
    procedure ckEntradaConhecimentoFreteClick(Sender: TObject);
  private
    procedure PreencherRegistro(pCFOP: RecCFOPs);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroCFOP }

procedure TFormCadastroCFOP.BuscarRegistro;
var
  vCfopId: string;
  vFlutuacao: string;
  vCfop: TArray<RecCFOPs>;
begin
  if Pos('-', eID.Text) < 1 then begin
    _Biblioteca.Exclamar('A flutua��o do CFOP informado � inv�lido!');
    SetarFoco(eID);
    Abort;
  end;

  vCfopId := Copy(eID.Text, 1, 4);
  vFlutuacao := Copy(eID.Text, Pos('-', eID.Text) + 1, 2);

  if Length(_Biblioteca.RetornaNumeros(vCfopId)) <> 4 then begin
    _Biblioteca.Exclamar('O Cfop informado � inv�lido!');
    SetarFoco(eID);
    Abort;
  end;

  if vFlutuacao = '' then begin
    _Biblioteca.Exclamar('A flutua��o do CFOP informado � inv�lido!');
    SetarFoco(eID);
    Abort;
  end;

  vCfop := _Cfop.BuscarCFOPs(Sessao.getConexaoBanco, 0, [eId.Text], tpTodos, False, '', False, False, False);
  Modo(True);
  if vCfop <> nil then
    PreencherRegistro(vCfop[0]);
end;

procedure TFormCadastroCFOP.ckEntradaConhecimentoFreteClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([FrCFOPEntrada], ckEntradaMercadorias.Checked or ckEntradaConhecimentoFrete.Checked);
end;

procedure TFormCadastroCFOP.ckEntradaMercadoriasClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([FrCFOPEntrada, FrPlanoFinanceiroEntrada], ckEntradaMercadorias.Checked or ckEntradaConhecimentoFrete.Checked);
end;

procedure TFormCadastroCFOP.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then begin
    if eID.Text <> '' then
      BuscarRegistro
    else begin
      _Biblioteca.Exclamar('� necess�rio informar o c�digo de CFOP!');
      eID.SetFocus;
    end;
  end;
end;

procedure TFormCadastroCFOP.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Cfop.ExcluirCFOP(Sessao.getConexaoBanco, eId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroCFOP.GravarRegistro(Sender: TObject);
var
  vCfopId: string;
  vFlutuacao: Integer;
  vRetBanco: RecRetornoBD;
  vCfopCorrespondenteId: string;
  vPlanoFinanceiroEntradaId: string;
begin
  inherited;

  vCfopId := Copy(eID.Text, 1, 4);
  vFlutuacao := SFormatInt(Copy(eID.Text, Pos('-', eID.Text) + 1, 2));

  vCfopCorrespondenteId := '';
  if not FrCFOPEntrada.EstaVazio then
    vCfopCorrespondenteId := FrCFOPEntrada.GetCFOP().CfopPesquisaId;

  if not FrPlanoFinanceiroEntrada.EstaVazio then
    vPlanoFinanceiroEntradaId := FrPlanoFinanceiroEntrada.getDados().PlanoFinanceiroId;

  vRetBanco :=
    _Cfop.AtualizarCFOP(
      Sessao.getConexaoBanco,
      vCfopId,
      vFlutuacao,
      vCfopCorrespondenteId,
      eNome.Text,
      eDescricaoCFOP.Text,
      _Biblioteca.ToChar(ckAtivo),
      vPlanoFinanceiroEntradaId,
      ckEntradaMercadorias.CheckedStr,
      ckEntradaMercRevenda.CheckedStr,
      ckEntradaMercUsoConsumo.CheckedStr,
      ckEntradaMercBonificacao.CheckedStr,
      ckEntradaServicos.CheckedStr,
      ckEntradaConhecimentoFrete.CheckedStr,
      ckNaoExigirItensEntrada.CheckedStr,
      FrParametrosPorEmpresa.ParametrosCFOPEmpresas
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroCFOP.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    eNome,
    eDescricaoCFOP,
    ckAtivo,
    FrCFOPEntrada,
    FrPlanoFinanceiroEntrada,
    ckEntradaMercadorias,
    ckEntradaServicos,
    ckEntradaConhecimentoFrete,
    ckEntradaMercRevenda,
    ckEntradaMercBonificacao,
    ckEntradaMercUsoConsumo,
    ckNaoExigirItensEntrada,
    pcParametros,
    FrParametrosPorEmpresa],
    pEditando
  );

  _Biblioteca.Habilitar([FrCFOPEntrada], eID.AsInt > 5000);

  if pEditando then begin
    SetarFoco(eNome);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormCadastroCFOP.PesquisarRegistro;
var
  vCfop: RecCFOPs;
begin
  vCfop := RecCFOPs(PesquisaCFOP.PesquisarCFOP(tpTodos, False, '', False, False, False));
  if vCfop = nil then
    Exit;

  inherited;
  PreencherRegistro(vCfop);
end;

procedure TFormCadastroCFOP.PreencherRegistro(pCFOP: RecCFOPs);
begin
  sbExcluir.Enabled := True;
  eNome.SetFocus;

  eID.Text := pCFOP.CfopPesquisaId;
  eNome.Text := pCFOP.nome;
  eDescricaoCFOP.Lines.Add(pCFOP.descricao);
  ckAtivo.Checked := (pCFOP.ativo = 'S');

  ckEntradaMercadorias.CheckedStr       := pCFOP.EntradaMercadorias;
  ckEntradaMercRevenda.CheckedStr       := pCFOP.EntradaMercadoriasRevenda;
  ckEntradaMercUsoConsumo.CheckedStr    := pCFOP.EntradaMercUsoConsumo;
  ckEntradaMercBonificacao.CheckedStr   := pCFOP.EntradaMercadoriasBonific;
  ckEntradaServicos.CheckedStr          := pCFOP.EntradaServicos;
  ckEntradaConhecimentoFrete.CheckedStr := pCFOP.EntradaConhecimentoFrete;
  ckNaoExigirItensEntrada.CheckedStr    := pCFOP.NaoExigirItensEntrada;

  FrCFOPEntrada.InserirDadoPorChave( pCFOP.CfopCorrespondenteId, False );
  FrPlanoFinanceiroEntrada.InserirDadoPorChave( pCFOP.PlanoFinanceiroEntradaId, False );

  FrParametrosPorEmpresa.ParametrosCFOPEmpresas := _CfopParametrosFiscaisEmpr.BuscarCfopParametrosFiscaisEmpr(Sessao.getConexaoBanco, 0, [pCFOP.CfopPesquisaId]);

  pCFOP.Free;
end;

procedure TFormCadastroCFOP.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o nome do CFOP!');
    SetarFoco(eNome);
    Abort;
  end;

  if eDescricaoCFOP.Text = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar a descri��o do CFOP!');
    SetarFoco(eDescricaoCFOP);
    Abort;
  end;
end;

end.
