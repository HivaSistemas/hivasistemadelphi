unit BuscarDadosFornecedor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameFornecedores, _Biblioteca, _RecordsCadastros;

type
  TFormBuscarDadosFornecedor = class(TFormHerancaFinalizar)
    FrFornecedores: TFrFornecedores;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosFornecedorConta(caption: string = ''): TRetornoTelaFinalizar<RecFornecedores>;

implementation

function BuscarDadosFornecedorConta(caption: string = ''): TRetornoTelaFinalizar<RecFornecedores>;
var
  vForm: TFormBuscarDadosFornecedor;
begin
  vForm := TFormBuscarDadosFornecedor.Create(Application);
  if (caption <> '') then
    vForm.Caption := caption;

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FrFornecedores.GetFornecedor(0);

  FreeAndNil(vForm);
end;

{$R *.dfm}

end.
