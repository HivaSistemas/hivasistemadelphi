inherited FormGruposTributacoesEstadualVenda: TFormGruposTributacoesEstadualVenda
  Caption = 'Grupos de tributa'#231#245'es estadual de venda'
  ClientHeight = 393
  ClientWidth = 899
  ExplicitWidth = 905
  ExplicitHeight = 422
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [1]
    Left = 210
    Top = 5
    Width = 53
    Height = 14
    Caption = 'Descri'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Height = 393
    ExplicitHeight = 393
    object SpeedButton4: TSpeedButton
      Left = 2
      Top = 364
      Width = 112
      Height = 19
      Caption = 'Informa'#231#245'es'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = SpeedButton4Click
    end
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 854
    Top = 2
    ExplicitLeft = 854
    ExplicitTop = 2
  end
  object eDescricao: TEditLuka
    Left = 210
    Top = 19
    Width = 640
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  inline FrICMS: TFrICMS
    Left = 126
    Top = 44
    Width = 770
    Height = 348
    Align = alCustom
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 44
    ExplicitWidth = 770
    ExplicitHeight = 348
    inherited pnGrid: TPanel
      Width = 770
      Height = 348
      ExplicitWidth = 770
      ExplicitHeight = 348
      inherited sgICMS: TGridLuka
        Width = 680
        Height = 348
        ExplicitWidth = 680
        ExplicitHeight = 348
        ColWidths = (
          46
          60
          43
          56
          36
          43
          75
          62
          77
          113
          79
          85
          64)
      end
      inherited Panel1: TPanel
        Left = 680
        Height = 348
        ExplicitLeft = 680
        ExplicitHeight = 348
        inherited eValor: TEditLuka
          Height = 22
          ExplicitHeight = 22
        end
      end
    end
    inherited pmOpcoes: TPopupMenu
      Left = 602
      Top = 239
    end
  end
end
