inherited FormPesquisaContagemEstoque: TFormPesquisaContagemEstoque
  Caption = 'Pesquisa de Contagem de Estoque'
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    ColCount = 5
    OnDrawCell = sgPesquisaDrawCell
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Nome'
      'Data Inicio'
      'Data Fim')
    RealColCount = 5
    ColWidths = (
      28
      55
      271
      139
      125)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Text = '0'
  end
end
