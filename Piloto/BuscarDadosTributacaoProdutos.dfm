inherited FormBuscarDadosTributacao: TFormBuscarDadosTributacao
  Caption = 'Buscar dados de tributa'#231#227'o dos produtos'
  ClientHeight = 274
  ClientWidth = 810
  ExplicitWidth = 816
  ExplicitHeight = 303
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 237
    Width = 810
    inherited sbFinalizar: TSpeedButton
      Left = 353
      ExplicitLeft = 353
    end
  end
  inline FrProdutosGruposTributacoes: TFrProdutosGruposTributacoesEmpresas
    Left = 0
    Top = 0
    Width = 810
    Height = 237
    Align = alClient
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitTop = 16
    ExplicitWidth = 880
    ExplicitHeight = 463
    inherited sgGrupos: TGridLuka
      Width = 810
      Height = 221
      HCol.Strings = (
        'Sel.?'
        'Empresa'
        'Nome'
        'CST de compra'
        'CST de venda'
        'Pis/Cofins de compra'
        'Pis/Cofins de venda')
      ExplicitWidth = 817
      ExplicitHeight = 447
      ColWidths = (
        33
        54
        185
        136
        129
        132
        128)
    end
    inherited st1: TStaticTextLuka
      Width = 810
      Caption = 'Parametros de tributa'#231#245'es de mercadorias'
      ExplicitWidth = 880
    end
    inherited pmOpcoes: TPopupMenu
      Left = 836
      Top = 392
    end
  end
end
