inherited FormInformacoesRetirada: TFormInformacoesRetirada
  Caption = 'Informa'#231#245'es da retirada'
  ClientHeight = 382
  ClientWidth = 657
  ExplicitWidth = 663
  ExplicitHeight = 411
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 2
    Top = 3
    Width = 46
    Height = 14
    Caption = 'Retirada'
  end
  object lb2: TLabel [1]
    Left = 82
    Top = 3
    Width = 39
    Height = 14
    Caption = 'Cliente'
  end
  object lb13: TLabel [2]
    Left = 289
    Top = 3
    Width = 47
    Height = 14
    Caption = 'Empresa'
  end
  object lb17: TLabel [3]
    Left = 179
    Top = 43
    Width = 54
    Height = 14
    Caption = 'N'#186' Pedido'
  end
  object Label1: TLabel [4]
    Left = 261
    Top = 43
    Width = 57
    Height = 14
    Caption = 'Nota fiscal'
  end
  object lb4: TLabel [5]
    Left = 343
    Top = 43
    Width = 102
    Height = 14
    Caption = 'Data/hora retirada'
  end
  object lb16: TLabel [6]
    Left = 481
    Top = 43
    Width = 114
    Height = 14
    Caption = 'Nome pessoa retirou'
  end
  object Label2: TLabel [7]
    Left = 2
    Top = 43
    Width = 28
    Height = 14
    Caption = 'Local'
  end
  object sbInformacoesOrcamento: TSpeedButtonLuka [8]
    Left = 237
    Top = 61
    Width = 18
    Height = 18
    Hint = 'Abrir informa'#231#245'es do pedido'
    Flat = True
    NumGlyphs = 2
    OnClick = sbInformacoesOrcamentoClick
    TagImagem = 13
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  object sbNotaFiscalId: TSpeedButtonLuka [9]
    Left = 319
    Top = 61
    Width = 18
    Height = 18
    Hint = 'Abrir informa'#231#245'es da nota fiscal'
    Flat = True
    NumGlyphs = 2
    OnClick = sbNotaFiscalIdClick
    TagImagem = 13
    PedirCertificacao = False
    PermitirAutOutroUsuario = False
  end
  inherited pnOpcoes: TPanel
    Top = 345
    Width = 657
    ExplicitTop = 345
    ExplicitWidth = 657
  end
  object eRetiradaId: TEditLuka
    Left = 2
    Top = 17
    Width = 76
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCliente: TEditLuka
    Left = 82
    Top = 17
    Width = 203
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmpresa: TEditLuka
    Left = 289
    Top = 17
    Width = 188
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object stTipo: TStaticText
    Left = 481
    Top = 22
    Width = 173
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Retira no ato'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
  end
  object eOrcamentoId: TEditLuka
    Left = 179
    Top = 57
    Width = 54
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 5
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNotaFiscalId: TEditLuka
    Left = 261
    Top = 57
    Width = 57
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 6
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDataHoraRetirada: TEditLukaData
    Left = 343
    Top = 57
    Width = 133
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999 99:99:99;1;_'
    MaxLength = 19
    ReadOnly = True
    TabOrder = 7
    Text = '  /  /       :  :  '
    TipoData = tdDataHora
  end
  object eNomePessoaRetirou: TEditLuka
    Left = 481
    Top = 57
    Width = 173
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 8
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object sgItens: TGridLuka
    Left = 2
    Top = 85
    Width = 652
    Height = 263
    ColCount = 7
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 9
    OnDrawCell = sgItensDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Lote'
      'Und.'
      'Quantidade'
      'Devolvidos'
      '')
    Grid3D = False
    RealColCount = 7
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      64
      234
      126
      97
      32
      75
      68)
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 481
    Top = 6
    Width = 173
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Tipo'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object eLocal: TEditLuka
    Left = 2
    Top = 57
    Width = 171
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 11
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
