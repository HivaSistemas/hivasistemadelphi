inherited FormParametrosEmpresa: TFormParametrosEmpresa
  ActiveControl = FrEmpresa
  Caption = 'Par'#226'metros de loja'
  ClientHeight = 551
  ClientWidth = 712
  OnShow = FormShow
  ExplicitTop = -86
  ExplicitWidth = 718
  ExplicitHeight = 580
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 551
    ExplicitHeight = 551
    inherited sbExcluir: TSpeedButton
      Visible = False
    end
    inherited sbPesquisar: TSpeedButton
      Visible = False
    end
    object Label13: TLabel
      Left = 2
      Top = 466
      Width = 107
      Height = 14
      Alignment = taCenter
      Caption = 'Busca de par'#226'metros:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtBuscarMenu: TButtonedEdit
      Tag = 3
      Left = 2
      Top = 486
      Width = 112
      Height = 26
      ParentCustomHint = False
      TabStop = False
      Align = alCustom
      AutoSelect = False
      AutoSize = False
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      LeftButton.ImageIndex = 11
      ParentFont = False
      ParentShowHint = False
      RightButton.ImageIndex = 11
      RightButton.Visible = True
      ShowHint = True
      TabOrder = 0
      OnChange = edtBuscarMenuChange
      OnEnter = edtBuscarMenuEnter
      OnKeyDown = edtBuscarMenuKeyDown
    end
  end
  inline FrEmpresa: TFrEmpresas
    Left = 125
    Top = 2
    Width = 284
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 2
    ExplicitWidth = 284
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 259
      Height = 23
      ExplicitWidth = 259
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 284
      ExplicitWidth = 284
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 179
        ExplicitLeft = 179
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 259
      Height = 24
      ExplicitLeft = 259
      ExplicitHeight = 24
      inherited sbPesquisa: TSpeedButton
        Visible = False
      end
    end
  end
  object pcParametros: TPageControl
    Left = 125
    Top = 45
    Width = 588
    Height = 505
    ActivePage = tsGeral
    TabOrder = 2
    object tsGeral: TTabSheet
      Caption = 'Geral'
      object ckPermiteAlterarFormaPagtoCaixa: TCheckBox
        Left = 178
        Top = 0
        Width = 304
        Height = 17
        Hint = '2 - Permite caixa alterar formas de pagamento de vendas'
        Caption = '1- Permite caixa alterar formas de pagto. de vendas'
        TabOrder = 0
        OnClick = ckUtilizaNFeClick
      end
      object ckPermiteAlterarFormaPagtoCxFin: TCheckBox
        Left = 178
        Top = 17
        Width = 304
        Height = 17
        Hint = 
          '3 - Permite caixa alterar formas pagamento de recebimentos de t'#237 +
          'tulos do financeiro'
        Caption = '2- Permite caixa alterar formas pagto rec. financeiro'
        TabOrder = 1
        OnClick = ckUtilizaNFeClick
      end
      inline FrLogoEmpresa: TFrImagem
        Left = 10
        Top = 14
        Width = 128
        Height = 128
        Hint = 'Logomarca da empresa'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 10
        ExplicitTop = 14
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 10
        Top = 0
        Width = 126
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Logomarca da emp.'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object ckExirgirDadosCatao: TCheckBox
        Left = 178
        Top = 33
        Width = 304
        Height = 17
        Hint = '3 - teste'
        Caption = '3- Exirgir dados do cart'#227'o no recebimento'
        TabOrder = 4
        OnClick = ckUtilizaNFeClick
      end
      object ckObrigarVendedorSelTipoCobr: TCheckBox
        Left = 178
        Top = 51
        Width = 304
        Height = 17
        Caption = '4- Exirgir dados de conbran'#231'a/cheque na venda'
        TabOrder = 5
        OnClick = ckUtilizaNFeClick
      end
      object ckExigirNomeConsumidorFinalVenda: TCheckBox
        Left = 178
        Top = 69
        Width = 304
        Height = 17
        Caption = '5- Exigir nome consumidor final na venda'
        TabOrder = 6
        OnClick = ckUtilizaNFeClick
      end
      object ckCalcularFreteVendaKM: TCheckBox
        Left = 178
        Top = 87
        Width = 304
        Height = 17
        Caption = '6- Calcular frete da venda por Km'
        TabOrder = 7
        OnClick = ckUtilizaNFeClick
      end
      object ckAlertaEntregaPendente: TCheckBox
        Left = 178
        Top = 105
        Width = 304
        Height = 17
        Caption = '7 - Alertar entrega pendente de cliente'
        TabOrder = 8
        OnClick = ckUtilizaNFeClick
      end
    end
    object tsFiscal: TTabSheet
      Caption = 'Fiscal'
      ImageIndex = 3
      object lb6: TLabel
        Left = 1
        Top = 3
        Width = 96
        Height = 14
        Caption = 'Regime tribut'#225'rio'
      end
      object lb7: TLabel
        Left = 1
        Top = 43
        Width = 128
        Height = 14
        Caption = 'Faixa Simples Nacional'
      end
      object lb18: TLabel
        Left = 1
        Top = 140
        Width = 193
        Height = 14
        Caption = 'Tipo de redirecionamento de notas'
      end
      object cbRegimeTributario: TComboBoxLuka
        Left = 1
        Top = 18
        Width = 153
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 0
        OnChange = cbRegimeTributarioChange
        Items.Strings = (
          '1 - Simples Nacional'
          '2 - Lucro Presumido'
          '3 - Lucro Real')
        Valores.Strings = (
          'SN'
          'LP'
          'LR')
        AsInt = 0
      end
      object cbFaixaSimplesNacional: TComboBoxLuka
        Left = 1
        Top = 58
        Width = 295
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 1
        Items.Strings = (
          'At'#233' 180.000,00  -  4,00 %'
          'De 180.000,01 a 360.000,00  -  7,30 %'
          'De 360.000,01 a 720.000,00  -  9,50 %'
          'De 720.000,01 a 1.800.000,00  - 10,70 %'
          'De 1.800.000,01 a 3.600.000,00  -  14,30 %'
          'De 3.600.000,01 a 4.800.000,00  -  19,00 %')
        AsInt = 0
      end
      object gbPercentuaisImpostos: TGroupBox
        Left = 365
        Top = 5
        Width = 212
        Height = 104
        Caption = ' % Impostos lucro presumido / real '
        TabOrder = 2
        object lb9: TLabel
          Left = 7
          Top = 15
          Width = 16
          Height = 14
          Caption = 'PIS'
        end
        object lb8: TLabel
          Left = 89
          Top = 15
          Width = 38
          Height = 14
          Caption = 'COFINS'
        end
        object lb10: TLabel
          Left = 7
          Top = 57
          Width = 22
          Height = 14
          Caption = 'CSLL'
        end
        object lb11: TLabel
          Left = 89
          Top = 57
          Width = 20
          Height = 14
          Caption = 'IRPJ'
        end
        object ePercentualPIS: TEditLuka
          Left = 7
          Top = 29
          Width = 76
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          MaxLength = 5
          TabOrder = 0
          Text = '0,00'
          OnKeyDown = ProximoCampo
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          NaoAceitarEspaco = False
        end
        object ePercentualCOFINS: TEditLuka
          Left = 89
          Top = 29
          Width = 76
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          MaxLength = 5
          TabOrder = 1
          Text = '0,00'
          OnKeyDown = ProximoCampo
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          NaoAceitarEspaco = False
        end
        object ePercentualCSLL: TEditLuka
          Left = 7
          Top = 71
          Width = 76
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          MaxLength = 5
          TabOrder = 2
          Text = '0,00'
          OnKeyDown = ProximoCampo
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          NaoAceitarEspaco = False
        end
        object ePercentualIRPJ: TEditLuka
          Left = 89
          Top = 71
          Width = 76
          Height = 22
          Alignment = taRightJustify
          CharCase = ecUpperCase
          MaxLength = 5
          TabOrder = 3
          Text = '0,00'
          OnKeyDown = ProximoCampo
          TipoCampo = tcNumerico
          ApenasPositivo = False
          AsInt = 0
          CasasDecimais = 2
          NaoAceitarEspaco = False
        end
      end
      inline FrEmpresaDirecionarNotaEntregar: TFrEmpresas
        Left = 1
        Top = 303
        Width = 295
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 303
        ExplicitWidth = 295
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 270
          Height = 23
          ExplicitWidth = 270
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 295
          ExplicitWidth = 295
          inherited lbNomePesquisa: TLabel
            Width = 193
            Caption = 'Empresa direcionar nota a entregar'
            ExplicitWidth = 193
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 190
            ExplicitLeft = 190
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 270
          Height = 24
          ExplicitLeft = 270
          ExplicitHeight = 24
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrEmpresaDirecionarNotaRetirar: TFrEmpresas
        Left = 1
        Top = 245
        Width = 295
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 245
        ExplicitWidth = 295
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 270
          Height = 23
          ExplicitWidth = 270
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 295
          ExplicitWidth = 295
          inherited lbNomePesquisa: TLabel
            Width = 180
            Caption = 'Empresa direcionar nota a retirar'
            ExplicitWidth = 180
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 190
            ExplicitLeft = 190
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 270
          Height = 24
          ExplicitLeft = 270
          ExplicitHeight = 24
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrEmpresaDirecionarNotaRetirarAto: TFrEmpresas
        Left = 1
        Top = 183
        Width = 295
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 183
        ExplicitWidth = 295
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 270
          Height = 23
          ExplicitWidth = 270
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 295
          ExplicitWidth = 295
          inherited lbNomePesquisa: TLabel
            Width = 187
            Caption = 'Empresa direcionar nota retira ato'
            ExplicitWidth = 187
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 190
            ExplicitLeft = 190
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 270
          Height = 24
          ExplicitLeft = 270
          ExplicitHeight = 24
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      object cbTipoRedirecionamentoNota: TComboBoxLuka
        Left = 1
        Top = 154
        Width = 295
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 6
        Items.Strings = (
          'N'#227'o redirecionar'
          'Redirecionar notas de pessoas jur'#237'dicas'
          'Redirecionar todas as notas fiscais')
        Valores.Strings = (
          'NRE'
          'RPJ'
          'TOD')
        AsInt = 0
      end
      object StaticTextLuka4: TStaticTextLuka
        Left = -2
        Top = 117
        Width = 581
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Direcionamento de notas para empresa por tipo de entrega'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 7
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
    end
    object tsExpedicao: TTabSheet
      Caption = 'Log'#237'stica'
      ImageIndex = 4
      object lb20: TLabel
        Left = 2
        Top = 294
        Width = 128
        Height = 14
        Caption = 'Qtde. min. dias entrega'
      end
      object gbFormasPagamento: TGroupBoxLuka
        Left = 2
        Top = 3
        Width = 182
        Height = 132
        Caption = ' Tipos de log'#237'stica a utilizar?    '
        TabOrder = 0
        OpcaoMarcarDesmarcar = True
        object ckTrabalhaEntregar: TCheckBox
          Left = 14
          Top = 51
          Width = 88
          Height = 17
          Caption = 'Entregar'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object ckTrabalhaRetirar: TCheckBox
          Left = 14
          Top = 34
          Width = 88
          Height = 17
          Caption = 'Retirar'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object ckTrabalhaSemPrevisao: TCheckBox
          Left = 14
          Top = 69
          Width = 88
          Height = 17
          Caption = 'Sem previs'#227'o'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object ckTrabalhaRetirarAto: TCheckBoxLuka
          Left = 14
          Top = 17
          Width = 115
          Height = 17
          Caption = 'Retirar no ato'
          Checked = True
          State = cbChecked
          TabOrder = 3
          CheckedStr = 'S'
        end
        object ckTrabalhaSemPrevisaoEntregar: TCheckBox
          Left = 14
          Top = 86
          Width = 150
          Height = 17
          Caption = 'Sem previs'#227'o - Entregar'
          Checked = True
          State = cbChecked
          TabOrder = 4
        end
      end
      object ckConfirmarSaidaProdutos: TCheckBox
        Left = 2
        Top = 148
        Width = 292
        Height = 17
        Caption = '1 - Confirmar sa'#237'da de produtos'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
      object ckImprimirLoteCompPagamento: TCheckBox
        Left = 2
        Top = 165
        Width = 292
        Height = 17
        Caption = '2 - Imprimir lote no comprovante de entrega'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
      object ckImprimirCodigoOriginalCompPagamento: TCheckBox
        Left = 2
        Top = 182
        Width = 292
        Height = 17
        Caption = '3 - Imprimir c'#243'digo original no comp. de entrega'
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
      object ckImprimirCodBarrasCompEnt: TCheckBox
        Left = 2
        Top = 199
        Width = 292
        Height = 17
        Caption = '4 - Imprimir c'#243'digo de barras no comp. de entrega'
        Checked = True
        State = cbChecked
        TabOrder = 4
      end
      object ckTrabalharControleSeparacao: TCheckBoxLuka
        Left = 2
        Top = 216
        Width = 292
        Height = 17
        Caption = '5 - Trabalhar com controle de separa'#231#227'o'
        TabOrder = 5
        CheckedStr = 'N'
      end
      object eQtdeMinDiasEntrega: TEditLuka
        Left = 2
        Top = 308
        Width = 137
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 3
        TabOrder = 6
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object gbTrabalharConferencia: TGroupBoxLuka
        Left = 205
        Top = 3
        Width = 182
        Height = 132
        Caption = 'Tipos de confer'#234'ncia a utilizar?'
        TabOrder = 7
        OpcaoMarcarDesmarcar = True
        object ckTrabalharConferenciaRetAto: TCheckBoxLuka
          Left = 14
          Top = 20
          Width = 97
          Height = 17
          Caption = 'Retirar no ato'
          Checked = True
          State = cbChecked
          TabOrder = 0
          CheckedStr = 'S'
        end
        object ckTrabalharConferenciaRetirar: TCheckBoxLuka
          Left = 14
          Top = 44
          Width = 82
          Height = 17
          Caption = 'Retirar'
          Checked = True
          State = cbChecked
          TabOrder = 1
          CheckedStr = 'S'
        end
        object ckTrabalharConferenciaEntregar: TCheckBoxLuka
          Left = 14
          Top = 68
          Width = 67
          Height = 17
          Caption = 'Entregar'
          Checked = True
          State = cbChecked
          TabOrder = 2
          CheckedStr = 'S'
        end
      end
      object ckPermBaixarEntRecPendente: TCheckBoxLuka
        Left = 2
        Top = 233
        Width = 396
        Height = 17
        Caption = 
          '6 - Permitir baixar entrega com rec. pendente( Receber na entreg' +
          'a )'
        TabOrder = 8
        CheckedStr = 'N'
      end
      object ckGerarNfAgrupada: TCheckBoxLuka
        Left = 2
        Top = 251
        Width = 396
        Height = 17
        Caption = '7 - Gerar nota agrupada para entrega no ato'
        TabOrder = 9
        CheckedStr = 'N'
      end
      object ckPermitirDevolucaoAposPrazo: TCheckBoxLuka
        Left = 2
        Top = 268
        Width = 396
        Height = 17
        Caption = '8 - Permitir devolu'#231#227'o ap'#243's o prazo configurado'
        TabOrder = 10
        CheckedStr = 'N'
      end
    end
    object tsEstoque: TTabSheet
      Caption = 'Estoque'
      ImageIndex = 5
      object lbl1: TLabel
        Left = 2
        Top = 20
        Width = 78
        Height = 14
        Caption = #205'ndice s'#225'bado'
      end
      object lbl2: TLabel
        Left = 146
        Top = 20
        Width = 85
        Height = 14
        Caption = #205'ndice domingo'
      end
      object cbIndiceSabado: TComboBoxLuka
        Left = 2
        Top = 34
        Width = 140
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 0
        Items.Strings = (
          '0    - N'#227'o abre'
          '0.5 - Meio Per'#237'odo'
          '1    - Integral')
        Valores.Strings = (
          '0,0'
          '0,5'
          '1,0')
        AsInt = 0
      end
      object cbIndiceDomingo: TComboBoxLuka
        Left = 146
        Top = 34
        Width = 140
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 1
        Items.Strings = (
          '0    - N'#227'o abre'
          '0.5 - Meio Per'#237'odo'
          '1    - Integral')
        Valores.Strings = (
          '0,0'
          '0,5'
          '1,0')
        AsInt = 0
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 2
        Top = 2
        Width = 284
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Sugest'#227'o de compras'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      inline FrPlanosFinanceiros1: TFrPlanosFinanceiros
        Left = 2
        Top = 65
        Width = 289
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 65
        ExplicitWidth = 289
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 264
          Height = 23
          ExplicitWidth = 264
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 289
          ExplicitWidth = 289
          inherited lbNomePesquisa: TLabel
            Width = 252
            Caption = 'Plano financeiro baixa de pedidos de compras'
            ExplicitWidth = 252
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 184
            ExplicitLeft = 184
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 264
          Height = 24
          ExplicitLeft = 264
          ExplicitHeight = 24
        end
      end
      object rgTipoCustoAjusteEstoque: TRadioGroupLuka
        Left = 3
        Top = 144
        Width = 185
        Height = 108
        Caption = 'Custo para corre'#231#227'o de estoque'
        ItemIndex = 2
        Items.Strings = (
          'Pre'#231'o final'
          'Pre'#231'o de compra'
          'Custo m'#233'dio')
        TabOrder = 4
        Valores.Strings = (
          'FIN'
          'COM'
          'CMV')
      end
      object StaticTextLuka3: TStaticTextLuka
        Left = 2
        Top = 114
        Width = 284
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Corre'#231#227'o de estoque'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
    end
    object tsDocumentosFiscais: TTabSheet
      Caption = 'NF-e / NFC-e'
      ImageIndex = 1
      object lbNumeroUltimaNFeEmitida: TLabel
        Left = 2
        Top = 22
        Width = 133
        Height = 14
        Caption = 'Numero ult. NFe emitida'
      end
      object lb2: TLabel
        Left = 148
        Top = 22
        Width = 28
        Height = 14
        Caption = 'S'#233'rie'
      end
      object Label2: TLabel
        Left = 148
        Top = 180
        Width = 28
        Height = 14
        Caption = 'S'#233'rie'
      end
      object Label3: TLabel
        Left = 2
        Top = 180
        Width = 139
        Height = 14
        Caption = 'Numero ult. NFCe emitida'
      end
      object Label4: TLabel
        Left = 222
        Top = 22
        Width = 53
        Height = 14
        Caption = 'Ambiente'
      end
      object Label5: TLabel
        Left = 222
        Top = 180
        Width = 53
        Height = 14
        Caption = 'Ambiente'
      end
      object Label6: TLabel
        Left = 1
        Top = 262
        Width = 225
        Height = 14
        Caption = 'Informa'#231#245'es complementares NFe / NFCe'
      end
      object lb17: TLabel
        Left = 2
        Top = 220
        Width = 139
        Height = 14
        Caption = 'Numero ult. NSU dist. DFE'
      end
      object lb21: TLabel
        Left = 148
        Top = 220
        Width = 137
        Height = 14
        Caption = 'Numero ult. NSU dist. CTe'
      end
      object Label7: TLabel
        Left = 2
        Top = 62
        Width = 87
        Height = 14
        Caption = 'S'#233'rie Certificado'
      end
      object Label8: TLabel
        Left = 220
        Top = 62
        Width = 93
        Height = 14
        Caption = 'Senha Certificado'
      end
      object btnCertificado: TSpeedButton
        Left = 373
        Top = 22
        Width = 172
        Height = 90
        Caption = 'Vazio...'
        Glyph.Data = {
          AE700000424DAE7000000000000036000000280000007A0000003B0000000100
          20000000000078700000C40E0000C40E00000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000009F7F3F60BFBF7F040000000000000000000000000000
          000000000000FFFFFF01A57F326A000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007F7F
          3F048888770F0000000000000000A682352B9E6908F8A5823C33000000000000
          0000000000000000000000000000B48734229F6906FBA5782839000000000000
          0000A2A2730BBFAA6A0C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000D4AA55069D6E15E2A1711AA4A3782846A27011C79F6902FFA273
          178D0000000000000000000000000000000000000000A3731A75A16902FFA06C
          0DD6A87F2D3EA16F139B9F6E13EABF9F7F080000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000A16F159BA16903FFA16905FF9E69
          03FFA06901FFA56E0BDCFFFFFF01000000000000000000000000000000009E6F
          14CC9F6902FFA26902FFA06904FF9F6902FFA57212B000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000AD7F2A42A169
          03FF9E6803FFA26902FFA06902FF9F6905FFB687343100000000000000000000
          0000AA884C1EA06903FCA16901FF9E6901FFA26902FF9F6903FFA9751C590000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000B6916D07A26C0BEEA06802FFA26901FFA16902FF9C6903FFA57218860000
          00000000000000000000A2731C6CA06901FFA06800FFA06901FFA06901FF9F6B
          09F5CCAA660F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000A06F159CA26901FF9C6901FFA16901FF9E69
          02FFA36E0ED6FFFFFF010000000000000000A16F0FC2A06800FFA06901FFA068
          00FF9F6901FFA27114BC00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000A47C324CA06904FF9F69
          01FFA16901FFA16901FF9E6903FEA7864326CC999905B7843D199C6906FCA069
          01FFA06800FFA06800FFA26902FFA2731B660000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007F7F
          3F04A8761B70A16B09EFA06903FFA16903FFA56B07F1A98B4D21A57214C4BB99
          551E9E6B0AE89F6904FFA16902FF9B6A09F4A5751B8091916D07000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A3813C43AE8A5023A0701A9AA36905FEA06E187FA375
          1E759D6901FFA373198BA7752366A26903FFA57318A8BD8F4127A17A2E470000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000AB771B9C9E6C0EE6A378255FB387
          3B2FAF8F4F209F6B08F4A26902FFA26904F7B0853C2AA37F3238A6762356A06D
          0CDEA46901AC0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000A9771C999E69
          02FFA26902FF9F731EB5997A3049A777227799712767A87922769E792B52A179
          27A6A26903FF9E6902FF9D6901AC000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000AB781B9BA16B0AE8A3752364BB99551E9C731F68A36F0E9EA26B07A8A46D
          09A09F741F70BF9F5F20A07624549F6C0AE0A06902AC00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000FFFFFF01C6C68D09C6A9
          8D09BFBF7F08B9B98B0BA5823F64AA884C1EA3721E8E9C6907F39F6902FFA269
          03FF9F6904FF9F6902FFA16902FF9A6807F89F701998B093581AA3741D67B9B9
          8B0BB9B98B0BB9A28B0BB0B0890D000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007F7F
          7F02A16E14BE9E6904FFA16902FF9E6B0ED59F874F20A37010C5A16903FFA169
          01FFA06901FF9F6801FFA16901FFA26901FFA06901FFA16901FFA46901FF9A6C
          14D1B4965A22A26E0EC4A06902FF9F6903FF9F6D0FD1BFBF7F04000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000AA7F2F30A26A04FCA16A05FAAF843A30A06F17B09F69
          01FFA16901FFA06901FFA06901FFA06800FFA06901FFA06901FFA06901FFA369
          01FFA06901FFA16902FF9D6D11C2AC7F34229E6908F39D6903FFAC7F30440000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000A0721A9AA47012A6A67F
          2A4E9F6902FFA06901FF9F6902FFA06800FFA06800FFA06901FFA06901FFA069
          01FFA06901FF9F6901FF9C6902FFA16901FFA26903FFAC781C6CA872188A9D6F
          15B2000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000AD84
          3D19B3842F36A26F11C29F6901FFA26901FFA16901FFA06800FFA06901FFA069
          01FFA06800FFA06800FFA06901FFA06800FFA16901FFA16901FF9D6902FFA56D
          0CD3A7863C26A582442500000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000BB99551EAB771D4FA7782657A879
          2558A8762558A97B2657A9782657A8792858A9782657A8792558A8762558A978
          2657A9782657A8792858B07C2258AD792258AB792956AB792356A885493B0000
          0000A781363D9F6F1AC29B8558179D6806F8A06901FFA06901FFA06901FFA069
          01FFA06901FFA06901FFA06901FFA06901FFA06901FFA06901FFA06901FFA069
          01FFA06901FFA06905FCC3A1661EA27219C7A97D324B00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000FF0000019E752382A26A07F99F69
          01FFA06901FFA06901FFA06901FFA06901FFA06901FFA06901FFA06901FFA069
          01FFA06901FFA06901FFA06901FFA06901FF9D6902FFA16902FF9F6B0BFBA274
          1F8AA985482AA47014ACA06906FDA06A05EAB78E472BA06901FFA06800FFA068
          00FFA06800FFA06800FFA06901FFA06901FFA06901FFA06800FFA06800FFA068
          00FFA06901FFA06901FFA26901FF9F6902FF9D75243FA47114D3996804FEA370
          12B3AE8643260000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000A1752162A369
          02FF9E6903FFA36901FFA16901FFA06901FFA06901FFA06901FFA06901FFA069
          01FFA06901FFA06901FFA06901FFA06901FFA06901FFA06901FFA06902FFA16A
          02FFA27010FAA2762469AA823C33A26E11C79F6904FFA26D0CE8B9904B2C9F69
          02FFA06800FFA16901FF9F6901FFA06901FFA06800FFA06901FFA06901FFA069
          01FFA06901FFA06901FFA06901FFA06800FFA16903FF9F6901FFA1690344A170
          12D1A36903FF9D6D14D2A87D263B000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000A8751BD7A16901FF9F6902FFA271179BAA771E7CA8771E7CAB781C7DAA77
          1C7CA9781C7DAA771C7CA9781C7DA9761C7DA9761C7DA9761C7DA9761C7DA976
          1C7DA9761C7DA9761C7DA9761C7DA8761A7FB48D4648AAAA55039C752755A070
          17DAA6906317A16904FAA16901FFA06901FF9F6901FFA06901FFA06901FFA068
          00FFA06800FFA06901FFA06901FFA06901FFA06901FFA06800FF9F6901FF9F69
          05FFBB9D5A22A47317DBA3752164D4AA7F060000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A9781CFF9E6902FFA26E10F1FFFFFF01000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000AD915B1CBB8C3E31A36D0DCBA06901FF9F6901FFA26901FFA069
          01FFA06800FFA06901FFA06800FFA06901FFA06800FFA06800FFA06800FFA068
          00FFA16902FFA06D0BDEAC8B5A1FA1865D260000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000A8781BFFA16901FFA46F10EE0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000A1741C86A1701496A67A27629D6903FFA269
          01FF9F6901FFA06901FFA06800FFA06901FFA06901FFA06800FFA06901FFA069
          01FFA06800FFA06800FFA06902FFA5751A7E9F721A7DA270169D000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000A9781BFFA069
          01FFA16F10EB0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000B0822D279C6907F9A06907F39E7C
          44259F6D11C1A16903FF9F6903FFA06901FFA16801FF9E6901FFA06901FFA069
          01FFA06901FFA16902FFA16902FFA16901FFA06C0DD3AC8B5A1FA46C08EFA169
          07FBAD8134350000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000A9781BFFA06901FFA37010EC0000000000000000000000009F7835489D71
          15D4A57112D8A67111D8A57111D8A57112D8A47011D9A3700FDBA77213D59271
          305E0000000000000000000000000000000000000000000000019F6F1AB0A269
          04FFA46A04FF9E6D12C1AA88441EA56D0ED89E6903FFA16901FF9F6802FFA169
          01FFA06901FFA06800FFA06800FF9F6903FFA06903FF9F6C0AE1B08941279C6E
          17B89E6807FF9F6903FF9D6E16C4FFFFFF010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A9781BFFA06901FFA16F11EB00000000000000000000
          00009C6E14BDA36A02FF9E6902FF9F6902FFA06901FFA06901FF9F6901FF9F69
          03FF9F6903FFA27011E000000000000000000000000000000000000000007F7F
          7F02B0915331B8924636B8924636B88D4636AA884258A98D4B1BA27218A59B68
          05FEA36901FF9F6901FFA06901FFA06800FFA06800FF9F6903FE9F6F16B3B18B
          4D219F782C5BB38D4B36B88D4636B88D4236B38D4B36BFBFBF04000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000A9781BFFA06901FFA26F10EB0000
          00000000000000000000A278256CA26908F6A06904F8A46A04F8A36B04F7A46B
          04F7A46B04F7A36B06F7A46A05F79C762B870000000000000000000000000000
          0000000000000000000000000000000000000000000000000000A7771E989F6D
          0ED996743844A082412BA97013839C6E18BDA47416D49E6F19C0A3701386A37D
          3435A37A3238A36E12CEA16905AC000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000A9781BFFA069
          01FFA16F11EB00000000000000000000000000000000B6B69107B299990ACCB2
          990AA9A9A909A9A9A909A9A9A909C6A9A909BF9F9F0800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000A7771A9AA36A02FF9D6903FE9D711CB29D7F464CA5721855A36C0D4BAC71
          0F53A6823954A37622A4A06A05FE9E6903FFA16902AC00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000A9781BFFA06901FFA06D10ED000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A7771C9A9D6A07F6A1741D78B98B450BAE8A48239E69
          09F89C6803FFA06A07FCA17C3431B6914807A2731E6CA16B0AF29E6702AC0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A9781BFFA06901FFA26F10EB00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000A581384DA7833D1D000000000000
          000000000000A3701783A06906FFAB74169E000000000000000000000000B185
          42179E74234F0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000AAAAAA03878787227578784E7B7B7B408D8D8D120000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000A9781BFFA06901FFA16F10EB0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000A3915B1CA57014D8B895571D7F7F7F020000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000AAAAAA06717373CC6D6F70FD6E7071FF6E70
          71FF6F7071F2737575948D8D8D09000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000A9781BFFA069
          01FFA26F10EB0000000000000000000000009B793C2EA27118A8A37212B0A572
          14AEA47314AFA47314AFA47314AFA37314AFA57314AEA57314AEA47314AFA473
          14AFA57314AEA47314AEA47312AFA47314AFA47312AFA47314AFA57314AEA873
          14AEA87418A8A57A2F360000000000000000FFFFFF01AD802B97B2996614A679
          2982AAAAAA030000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000757575897373
          7385757575627274746D707172CA6E7071FF7375759A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000A9781BFFA06901FFA26F10EB000000000000000000000000A06F15B5A369
          03FF9C6803FFA26901FFA16901FFA16901FFA16901FFA16901FFA16901FFA169
          01FFA16901FFA16901FFA16901FFA16901FFA16901FFA16901FFA16901FFA169
          01FFA16901FFA06901FF9D6904FFA17116CA000000000000000000000000A673
          12EAB1883F7DA06C0EE6AAAAAA03000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000008888880F707171E96F7071F88D8D
          7F12000000000000000000000000000000000000000000000000000000000000
          0000000000007F7F7F020000000000000000000000007F7F7F08BFBFBF040000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A9781BFFA06901FFA26F10ED00000000000000000000
          0000A3751F899F6905FFA16805FFA06901FF9E6901FF9F6901FF9F6901FF9F69
          01FF9F6901FF9F6901FF9F6901FF9F6901FF9F6901FF9F6901FF9F6901FF9F69
          01FF9F6901FF9F6901FF9F6901FFA06901FFA16903FF99712097000000000000
          000000000000A6720EECA06906FFA46D08ECAAAAAA0300000000000000000000
          000000000000000000007678788C6F7172D46E7172D46F7172D4737474CC7173
          749B7878784AAAAA550300000000000000000000000079797941707172D16F71
          72D4797B796500000000AAAAAA0675777762717272A0727474957777772F6E71
          71B96E7071FF818181350000000084848419727474C86E7172D4717373940000
          00000000000175757559717272C2707272E88787822F7F7F7F1E717373B86E70
          71F4707172E57477776D78787846707273D06F7172D4787878337C7C7C2B7172
          73CD6E7172D47476768100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000A9781BFFA06901FFA27010EA0000
          00000000000000000000FFAAAA03A5874322AE864926AE8F4A29B3894B25B389
          4B25B3894B25B3894B25B3894B25B3894B25B3894B25B3894B25B3894B25B389
          4B25B3894B25B3894B25B3894B25B3894B25B3894B25B1864624B4874322BFBF
          7F04000000000000000000000000A6720EEC9D6901FFA16C08EDCCCC99050000
          0000000000000000000000000000000000006F7171DE6E7071FF6F7071F26E70
          72ED707172F36D6F70FD6D6F70FE727373B77F7F7F0E00000000000000007072
          727D6E7071FF6F7071E77A7C7A628D8D8D09707173C36E7071FF6E7071FB6F70
          72F66E7070F56F7071EB6E7071FF7D7D7D39000000007C7C7C4A6E7071FF6E6F
          70F47474749100000000757878556D6F70FE6D6F70FD6F7171D98787822F7274
          74C16E7071FF737375B773757587717373D9727374E66E7071FF6F7071E07A7A
          75327678785F6E7071FF6E7071F17678787F0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000A9781BFFA069
          01FFA26F10ED0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000A6720EECA16901FFA26C
          08EDBFBF7F0400000000000000000000000000000000000000006E7172DF6F70
          71FF7C7C7C2BFFFFFF01B69191077F7F7F3C717272CF6E7071FF727373B0FFFF
          FF01000000007072727D6E7071FF7577777500000000757777716E7071FF7172
          72CB83837B217373730B787878616D6F70FE6E7071FF7D7D7D39000000007A7D
          7A4B6E7071FF727475AB0000000000000000717373BA6D6F70FF7376766E7F7F
          7F026D6D6D076E7071F26F7172F88888880F0000000083837B1F707272EF6E70
          71FF7474743B000000007676765F6E7071FF7375759A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000A8781BFFA06901FFA37010EC000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000A672
          0EECA06901FFA46C07ECAAAAAA03000000000000000000000000000000000000
          00006E7172DF6E7071FF7979792A0000000000000000000000007F7F7F186E71
          71EF6D6F70FE7979793F000000007171737C6E7071FF75777773000000007273
          73C76E7071FF7979793F0000000000000000FFFFFF01717272CF6E7071FF7D7D
          7D39000000007C7F7C4A6E7071FF737575AA00000000000000006E6F71C46D6F
          70FF8585852C000000007F7F7F02727373C26E7071FE7274747689897F1A9F9F
          9F08747575C16E7071FF7276763A000000007676765F6E7071FF767676970000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A9791BFFA06901FFA37010EC00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000A6720EECA06901FFA46C07ECAAAAAA0300000000000000000000
          000000000000000000006E7172DF6E7071FF7676762900000000000000000000
          000000000000727474996E7071FF7274748C000000007072727D6E7071FF7678
          787400000000707272E56E7071FF8484841B0000000000000000000000007274
          74B46E7071FF7D7D7D39000000007A7D7A4B6E7071FF737575AA000000000000
          00006E6F71C46D6F70FF7F857F2A000000000000000089897F1A727474AB6E70
          71F76D6F70FB6F7172F46F7071FC6D6F70FF7276763A000000007676765F6E70
          71FF767676970000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000A9781BFFA06901FFA37010EC0000
          00000000000000000000A6904D17A178237BA4751784A5741783A7741983A574
          1783A7741983A5741783A5741783A5741783A5741783A7741983A5741783A574
          1783A5741783A7741783A7741783A5741783A5741783A9751984A576207D9784
          4B1B000000000000000000000000A6720EECA06901FFA46C07ECAAAAAA030000
          0000000000000000000000000000000000006E7172DF6E7071FF7979792A0000
          0000000000000000000000000000777777716E7071FF737474B3000000007072
          727D6E7071FF7577777300000000727374CD6E7071FF7A7A7A32000000000000
          0000FFFFFF01707272CA6E7071FF7D7D7D39000000007C7C7C4A6E7071FF7375
          75AA00000000000000006E6F71C46E7071FF8282822900000000000000000000
          0000000000018D8D8D12767676367A7A7A51727373D46D6F70FF7D7D78370000
          00007676765F6E7071FF76767697000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000A9791AFFA069
          01FFA26F10EC000000000000000000000000A27216AA9E6803FF9F6902FFA069
          01FFA16901FFA16901FFA16901FFA16901FFA16901FFA16901FFA16901FFA169
          01FFA16901FFA16901FFA16901FFA16901FFA16901FFA16901FFA16901FFA269
          02FF9F6904FFA67213C0000000000000000000000000A6720EECA06901FFA46C
          07ECAAAAAA0300000000000000000000000000000000000000006E7172DF6E70
          71FF7676762900000000000000000000000000000000787878706E7071FF7374
          76B58D8D8D127273739A6E7071FF77777775000000007375757E6E7071FF7173
          73B38A8A7F18A9A98D097A7A7A626E7071FE6E7071FF7E807E5D99999914797B
          7B696E7071FF737575AA000000008989750D6E7070C76E7071FF818181377F7F
          7F0E7F7F7F027A7A764B727474D275787866000000009191910E6E7072E46E70
          71F98A8A8A18000000007676765F6E7071FF7676769700000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000A9781BFFA06901FFA26F10ED0000000000000000000000009C6F1D9EA269
          03FFA26902FFA16901FF9F6901FF9F6901FF9F6901FF9F6901FF9F6901FF9F69
          01FF9F6901FF9F6901FF9F6901FF9F6901FF9F6901FF9F6901FF9E6901FF9F69
          01FF9F6901FF9D6901FFA16902FFA27117B0000000000000000000000000A672
          0EECA06901FFA46C07ECAAAAAA03000000000000000000000000000000000000
          00006E7172DF6E7071FF7979792A000000000000000000000000000000007273
          739A6E7071FF7173738B707274916E7071FF6E7071FF74767665000000007F7F
          7F10707273C96E7071FF6E7070F96F7171F56E7070F5707171CE6E7071FF6E70
          71FF7B7D7B886E7071FF6F7071FF737575A3FFFF7F02737474DF6D6F70FD6E70
          71FF6E7071F96E7071F4787878137F7F7A386E6F70F96E7071F5717373D46F71
          71E86D6F70FE7375759200000000000000007676765F6E7071FF767676970000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A9791AFFA06901FFA27010EA00000000000000000000
          0000BF94550CAD751D4EA7792552AB792552AE792252AE792252AE792252AE79
          2252AE792252AE792252AE792252AE792252AE792252AE792252AE792252AE79
          2252AE792552AE792552AE792552A7792252A6751D4EC3964B11000000000000
          000000000000A6720EECA06901FFA46C07ECAAAAAA0300000000000000000000
          000000000000000000006E7172DF6E7071FF7676762900000000000000000000
          00007B7B7B1D6F7071F06D6F70FD7B7B773E797979437678787D757575738787
          871100000000000000007F7F7F0E7474746D727373A572747495787878397777
          77207779797878787A7D818181437678787D777979787C7C7C25FFFFFF01797C
          7971707172DF6E7071FF7678789478787A7B7F7F7F0A000000007D7D7D3B7173
          738D747676B17273739A767673567F7F7F0400000000000000007678765F6E70
          71FF747676970000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000A9781BFFA06901FFA26F10ED0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000FFFFFF01A7700BEEA06901FFA46C07ECAAAAAA030000
          0000000000000000000000000000000000006E7071E06E7071FF767676290000
          00009F9F7F087B7B7B40707172D66E7071FF717374B1FFFFFF0100000000BFBF
          BF04BFBF9F08FFFFFF0200000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000007F7F7F02C6C6A909BFBF
          BF040000000000000000707273C06E7071FF8585852A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000767676616E7071FF74767697000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000A9791AFFA069
          01FFA16F10EB0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000A7875720B694503CB994523EB994
          523EB695543FB695543FB691503FBA95503FBA95503FA46E0EF2A06901FFA46C
          07ECAAAAAA03000000000000000000000000BFBFBF04707272D86D6F70F96E70
          71FF6E7070E26E7070E06F7172F56D6F70FE6D6F70FD717272B48888880F0000
          0000000000007B7B79696F7072F67F7F7F440000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000007878
          78226F7072F67373737900000000000000007173739B6F7072C9878787220000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000737676566F7071EC6E7071FF7576769600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000A8781BFFA06901FFA26F10EB000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000AD8B5116A17119BC9F6805FD9F69
          03FFA06902FF9F6902FF9F6902FFA06902FF9E6903FF9C6803FF9C6803FFA069
          02FFA06901FFA46C07ECAAAAAA03000000000000000000000000FFFFFF036F71
          72C46F7071CA6F7071CA6F7070CA6F7072C96F7272C47273739A787878447F7F
          7F02000000000000000000000000797C79566E7071C87F7F7A38000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000007373731F6F7072C9717474650000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000717474516E7070C9707171C8787878440000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000A9791AFFA06901FFA26F11EB00000000000000000000
          0000DAB66D07B0842F51B1812755AE812455AE812455AE812455B1812755B583
          2957AE822B52B09C620D000000000000000000000000AAAA55039F6D13C8A269
          02FF9D6902FF9E6901FF9E6901FFA16901FF9D6901FF9F6901FF9F6901FFA169
          01FF9F6902FFA26901FF9E6903FFA26C0AECAAAAAA0300000000000000000000
          00000000000000000000000000000000000000000000AAAAAA038F7F7F109C9C
          890DFFFFFF01000000000000000000000000000000000000000000000000AAAA
          AA039988880FA2A2A20B00000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000AAAA
          AA03000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000019C9C890DA391910EFFFFFF01000000000000
          000000000000000000007F7F7F0E9999990A0000000000000000000000000000
          0000000000000000000000000000AAAAAA038F8F7F1099999905000000000000
          00000000000000000000000000000000000000000000000000009F9F9F088787
          7811BFBFBF04000000000000000000000000A9781CFFA06901FFA06D10ED0000
          000000000000000000009E701A9AA46902FF9B6804FF9F6803FFA06901FFA069
          01FFA06902FF9D6803FFA26904FFA27116B90000000000000000000000009C7D
          3A399F6804FFA16901FF9E6C09E99C6D16919B6E198A9E6E198A9E6E198AA26E
          198AA06E198AA17520AEA16902FFA16902FF9E6804FFA66F0DDCFFFF7F020000
          000000000000000000000000000000000000000000007F7F7F16727474917072
          73E46E7071FA6E6F70F96F7171D9717373897F7F7F160000000000000000BFBF
          7F0475777771717273DA6F7071FA6E7071F4717373C37878784400000000FFFF
          BF04707172E0707172CF00000000000000000000000000000000000000007777
          7740717272BF6F7171EA7477776B00000000757575866E7172DF6E7172DF7F7F
          7F1E707373566E7172DF7779778600000000000000007F7F7F04727475B46E71
          72DF717273D1AAAAAA03FFFFFF0175757550707272C86F7172F86F7071F97073
          73D275777762FFFFFF027F7F7F18737474B56F7172F86F7071F27676788A7B7B
          7B42707273DB6E7172DF777777530000000078787859717272DA6F7172FA6F71
          71E9767676677678786C707172DE707172DF74747423000000007979792C7173
          74B36F7071EE6E7071FA707272E87474748A7878781100000000A9791AFFA069
          01FFA26F10EB000000000000000000000000A37015AF9E6903FFA26902FFA069
          02FF9F6902FF9F6903FF9F6902FFA56901FFA06902FFA17116CF000000000000
          000000000000AD7C2B5EA06902FFA16901FF9F711B7000000000000000000000
          000000000000000000009D8554159E6D12CFA36A02FF9E6902FFA06D0CE5AE86
          35260000000000000000000000000000000000000000000000007C7C7C316F71
          71E56E7071FF6E7071FA707272E3707172E16E7070F66E7071FF717273E67B7B
          7B3CFFFFFF01727474A26E7071FF6F7171E57173739D7072729E6F7071E57071
          72D391919107D4D4AA066F7172FF6E6F70EB0000000000000000000000000000
          00007979792C6E6F70F66E7071FF707172D67878785BFFFFFF01707274DE6E70
          71FF717273CD7F7F7F1A7073735F6E7071FF7577779800000000000000007F7F
          7F0A6E7070F66E7071FC727375B7AAAAAA037779776B6E7071FC6D6F70FB7274
          74BD737576AA707172DC6E7071FF7A7A7A53727375AC6E7071FF737575BB7777
          777A727373CB717273E86E7071FF6F7171DC7B7B7848767A7A496E6F70FD6F71
          71F7727373A57273739A707272E8707172F06E7071FF707171D37777771E7A7A
          7A326F7072ED6E7071FD727373BB70727295717274D56F7172FF707172D1917F
          7F0EA9781CFFA06901FFA16F10EB000000000000000000000000A0844B1BA470
          1376A4721A7FA372147DA472167FA472167FA272167FA2721A7FA6711279AE82
          3129000000000000000000000000AE7E28659F6902FFA06901FFA6771E6D0000
          0000000000000000000000000000A98548159D6B0ECEA06903FF9E6903FFA16C
          0CE4AE8635260000000000000000000000000000000000000000000000007886
          78136F7171DE6E7071FF717373BA7F7F7F287F7F7F02FFFFFF01868686137476
          767D6F7072ED7F7F7F2C7979793F6E7071FE707172D68C8C7F14000000000000
          00007F7F7F0C7A7A7A3000000000D4D4AA066F7172FF6E7071EA000000000000
          000000000000000000007274747D6E7071FF717274A2AAAAAA0300000000FFFF
          FF01707273E16D6F70FB72727214000000007073735F6E7071FF777777980000
          0000000000007373730B6E7070F66F7172EEFFFFFF02858585176E7071F26E70
          70F57B7B7B3A00000000000000007F7F7F06787878669F9F9F086F7171E56F70
          71FF97978D1B0000000078787811717373EB6E7071FF7679795A000000007172
          72C86E7071FF777777600000000000000000767676366E7071FB6E7071FF7878
          7826FFFFFF01717273C66F7071FF73767663FFFFFF0100000000919191077274
          74A96E7071FF73757584A9791AFFA06901FFA26F10EB00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000AE7E2A65A06901FFA069
          01FFA2761E6C000000000000000000000000AD8B45169F6D0DD2A36902FF9D69
          02FFA16C0CE5B586352600000000000000000000000000000000000000000000
          0000000000007275757A6E7071FF707172D57F7F7F0A00000000000000000000
          000000000000000000008A8A8A18000000007375758D6E7071FF77777782CCB2
          B20AB0B09C0DB9B9A20BB0B09C0DB9B9A20BFFFF7F02D4D4D4066F7071FF6E70
          72ED000000000000000000000000000000006F7171896E7071FF7779796B0000
          000000000000FFFFFF01707273E16D6F70FB78787813000000007070735F6E70
          71FF7577779800000000000000007373730B6E7072F66F7273EDFFFFFF017676
          79546E7071FF717274AB00000000000000000000000000000000000000000000
          00007375759F6E7071FF737575948282822B9B9B9B12737474A86E7071FF7B7B
          7B599191910E6D6F70FB6F7071F0B69191070000000000000000000000007273
          74D46E7071FF787878247F7F7F1C6D6F70FE707373E27F7F7F02000000000000
          0000000000007C7C7C2D6F7172FF757573E3A8781CFFA06901FFA26F10EB0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000AD7F
          2B64A06901FFA16901FFA2771E6B0000000000000000AD8B5116A06C0CD39D68
          04FF9F6904FFA26C0DE4AE863526000000000000000000000000000000000000
          0000000000000000000000000000727374D06E7071FF75777762000000000000
          0000000000000000000000000000000000000000000000000000757676A16E70
          71FF6F7172FC707273FA707273FA6F7172FA6F7172FA6F7172FA7D817D3FD4D4
          AA066F7172FF6E7071FE8C8C8C14000000000000000000000000707272886E70
          71FF767979690000000000000000FFFFFF01707273E16D6F70FB787878130000
          00007073735F6E7071FF7777779800000000000000007373730B6E7072F66F72
          73EDFFFFFF01737676636E7071FF747676970000000000000000000000000000
          000000000000000000007F7F7F0E73747490707272F16E7071FE6F7172FB6E70
          71FD6D6F70FF797C79587F7F7F146D6F70FD707173EB7F7F7F02000000000000
          000000000000717373B86E7071FF75757525797F7F2C6E7071FF707272CA0000
          0000000000000000000000000000848484196F7071FF717273FFA9791AFFA069
          01FFA37010EC0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000AD7C2864A06901FFA06901FFA2771E6B00000000AD8B45169E6C
          10CFA26902FFA06903FFA26C0CE4B28533280000000000000000000000000000
          000000000000000000000000000000000000A9A9A9096F7171FA6E7071FF8E87
          8722000000000000000000000000000000000000000000000000000000000000
          0000737575756E7071FF737575AE7A7A7A517C7C7C507C7C7C50717374E26E70
          71FF7C7C7C2BD4D4AA066E7071FF6E7071FF7575757700000000000000000000
          00006F7173896E7071FF767679690000000000000000FFFFFF01707273E16D6F
          70FB78787813000000007070735F6E7071FF7577779800000000000000007373
          730B6E6F70F66F7272EDFFFFFF017A7A7A386E7071FE707272C77F7F7F020000
          0000000000007F7F7F1074747418AAAAAA0300000000000000009F9F9F08757C
          75277B7B7B44727373BB6E7071FF7C7C7C56B6B691076E7071EB6E7071F98383
          831F00000000000000009999990A6F7172EC6E7071FF787878248989890D6F70
          71F16E7071F485857917000000000000000000000000767979546E7071FF7777
          73C0A8791BFFA06901FFA26F11ED000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000AD7F2B64A06901FFA16901FF9B721F69A685
          4217A06C0DD39F6904FF9D6902FFA26C0CE4BB8C352600000000000000000000
          0000000000000000000000000000000000000000000000000000A9A9A9096F71
          72FA6E7071FF8787872200000000000000000000000000000000000000000000
          000000000000000000007F7F7F1E6F7172F16F7273E482827C29FFFFFF017D7D
          7D376E6F70F66F7272DDCCCCCC05838383406F7071FF707273FA717273EE7273
          7398787B7B61878787116F7173926E7071FF76767676858579178D8D71098484
          8434707172EA6D6F70FB787878137F7F7F0A7072726D6D6F70FF747575A07F7F
          7F189494940C7B7F7B446F7071FA6F7172EEFFFFFF027F7F7F04727373C46D6F
          70FD7274748E909085178F8F8F10717373B86D6F70FD9C9C9C1A757575347273
          74DD787A7A83000000009494940C717272D46E6F70FD7C7C7C27000000007375
          75946E7071FF707173BC7F7F7F248A8A7F18727272976F7071FF6E7071FF7272
          722600000000737575896E7071FF717273C47979792EAF9F9F10767676496F70
          71EB6D6F70FB767A7649A8781BFFA06901FFA26E10EB00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000AE7E2A65A06901FFA069
          01FFA77D2B8E9E6C0CD4A16904FFA16902FFA26C0CE5AE863526000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000727373D26E7071FF767676540000000000000000000000000000
          00000000000000000000000000000000000000000000737676586E7071F26E6F
          70F66F7272E46E7071F96F7071F377777742CCCCB20A6F7172FF6E7071FF7273
          74DA7878785D6E7070FC717272FF918773FC6E6F70FE6D6F70FF6E7071FE6E70
          70FD74747448727474E46E7071FF6E7071F778787811707373666E7071FE6E70
          71FF6E6F70FE6D6F70FD7C7C7C7B6F7172FF6E7071FF707172E0FFFF00010000
          000078787824707272D06E7071FF6E7071FD6E7071FC6E7071FF707173CEBFBF
          7F0483837B216E7071F36D6F70FB727374DF6F7172EC6E7071FF727474A20000
          00010000000079797915707172D36E7071FF6E7071FE6D6F70FD6E7072F66E70
          72F66E7071FF75757525000000008D8D8D09717274A96E7071FF6E7071FE6F70
          71FC6E7071FE6E7071F87777776B00000000A7791DFFA16901FFA26D0CF5B691
          6D07000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000AC7E
          2B639F6901FFA06902FF9E6905FFA26902FFA16904FF9F6B0DE4B27F2C280000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000737373796E7071FF707273C77F7F7F080000
          000000000000000000000000000077797978747575DB85858143000000000000
          000083837B21737373797375759F7274747D7C7C7C2500000000CCCC99057878
          7B7278787B727A7A753400000000787D783773757384958C796F727375BB6E70
          71FF747676AF78787B72787F78227B7B7B657678787277777751000000007A7A
          7A30737474A66E7071FF727474C87678787284847F3678787A70767878727878
          78390000000000000000000000009F9F9F087777775C71737390717272977676
          76678989890D00000000000000007C7C7C297274747F737575A37273738F7679
          7652AAAA7F060000000000000000000000007F7F7F0C7375756A7273739A7274
          75867C7C7C27737475CC6E7071FF727272260000000000000000FFFFFF017979
          793F72727486727474A0727474747C7C7C2500000000000000009F6E14AD9C68
          05FFA16902FFA26D0ED6A57216BFA6741ABEA67519BFA6741ABFA67519BFA675
          19BFA67519BFA67519BFA67519BFA6751ABFA67519BFA67519BFA67519BFA675
          19BFA67419BE9E7017DBA36901FF9D6902FFA16901FF9E6902FFA16C0CE5B789
          3427000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000008989890D717272D66E6F
          70FF727373B77A7A7A30AAAAAA03CCCCCC05787D7837707173D96E7071FF7D7D
          7941000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000737575846E7071FF76797969000000000000000000000000BFAAAA0CBBBB
          AA0F0000000000000000767676496E7071FF707272C6A2A2A20B000000000000
          0000BBAA990FB9B9A20B00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000737576CF6E7071FF75757525000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000A87D2F3B9D6A06F7A36901FF9F6902FF9D6901FF9F6901FFA06901FF9F69
          01FF9F6901FF9F6901FF9F6901FF9F6901FF9F6901FF9F6901FF9F6901FF9F69
          01FF9F6901FF9F6901FF9F6901FFA26902FFA06901FF9E6901FFA16903FFA16B
          0CE3AC8230250000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000075757525707272D66E7071FF6E7071FF6E6F70F26E7071F06E7071FF6E70
          71FF717373C59F9F9F0800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000767676616F7172B9787C7C4C00000000000000000000
          0000737474BE6F7071F0FFFFFF02000000009494940C707172D66E7071FF6F71
          72EE7A7A788EAAAAAA06707272F0747575C10000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000787978BD6E7071FA6E7071FF7575
          7525000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000A37D3143A47214E1A26902FFA16901FF9F69
          01FF9F6901FFA06901FFA06901FFA06901FFA06901FFA06901FFA06901FFA069
          01FFA06901FFA06901FFA06901FFA06901FFA06901FFA06901FF9F6901FFA168
          01FFA06E0CE9A97C342700000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000008B8B8B0B74747474757573D26F7172FF7373
          72FF727474C8757878668D8D8D09000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000007476768C717272B07F7F7F0200000000000000007979
          7917727474917B7972DA827E758F99999905727474AD7375758D000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000767676996F71
          71B9717272A28D8D8D0900000000000000000000000000000000000000000000
          000000000000000000000000000000000000}
        Layout = blGlyphTop
        PopupMenu = ppmCertificado
        OnClick = btnCertificadoClick
      end
      object Label9: TLabel
        Left = 2
        Top = 103
        Width = 86
        Height = 14
        Caption = 'Impressora NFe'
      end
      object Label10: TLabel
        Left = 294
        Top = 219
        Width = 52
        Height = 14
        Caption = 'CSC NFC-e'
      end
      object Label11: TLabel
        Left = 421
        Top = 219
        Width = 46
        Height = 14
        Caption = 'Id Token'
      end
      object Label12: TLabel
        Left = 373
        Top = 179
        Width = 124
        Height = 14
        Caption = 'Modelo de Impressora'
      end
      object ckUtilizaNFe: TCheckBox
        Left = 2
        Top = 6
        Width = 97
        Height = 17
        Caption = 'Utiliza NFe'
        TabOrder = 0
        OnClick = ckUtilizaNFeClick
        OnKeyDown = ProximoCampo
      end
      object eNumeroUltimaNFeEmitida: TEditLuka
        Left = 2
        Top = 36
        Width = 140
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 1
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eSerieNFe: TEditLuka
        Left = 148
        Top = 36
        Width = 68
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 3
        TabOrder = 2
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eSerieNFCe: TEditLuka
        Left = 148
        Top = 194
        Width = 68
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 3
        TabOrder = 9
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eNumeroUltimaNFCeEmitida: TEditLuka
        Left = 2
        Top = 194
        Width = 140
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 8
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object ckUtilizaNFCe: TCheckBox
        Left = 2
        Top = 162
        Width = 97
        Height = 17
        Caption = 'Utiliza NFC-e'
        TabOrder = 7
        OnClick = ckUtilizaNFCeClick
        OnKeyDown = ProximoCampo
      end
      object cbTipoAmbienteNfe: TComboBoxLuka
        Left = 222
        Top = 36
        Width = 145
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 3
        OnKeyDown = ProximoCampo
        Items.Strings = (
          'Homologa'#231#227'o'
          'Produ'#231#227'o')
        Valores.Strings = (
          'H'
          'P')
        AsInt = 0
      end
      object cbTipoAmbienteNfce: TComboBoxLuka
        Left = 222
        Top = 194
        Width = 145
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 10
        OnKeyDown = ProximoCampo
        Items.Strings = (
          'Homologa'#231#227'o'
          'Produ'#231#227'o')
        Valores.Strings = (
          'H'
          'P')
        AsInt = 0
      end
      object meInformacoesComplementaresDocsEletronicos: TMemo
        Left = 1
        Top = 278
        Width = 365
        Height = 139
        MaxLength = 200
        TabOrder = 16
        OnKeyDown = ProximoCampo
      end
      object eUltimoNsuConsultadoDistNFe: TEditLuka
        Left = 2
        Top = 234
        Width = 140
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 12
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eUltimoNsuConsultadoDistCTe: TEditLuka
        Left = 148
        Top = 234
        Width = 140
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 13
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object edtSeriaCertificado: TEditLuka
        Left = 3
        Top = 75
        Width = 211
        Height = 22
        CharCase = ecUpperCase
        TabOrder = 4
        OnKeyDown = ProximoCampo
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object edtSenhaCertificado: TEdit
        Left = 221
        Top = 75
        Width = 121
        Height = 22
        PasswordChar = '*'
        TabOrder = 5
        OnKeyDown = ProximoCampo
      end
      object cbxImpressoras: TComboBoxLuka
        Left = 2
        Top = 118
        Width = 393
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 6
        OnKeyDown = ProximoCampo
        AsInt = 0
      end
      object edtCSCNFCe: TEdit
        Left = 294
        Top = 234
        Width = 121
        Height = 22
        TabOrder = 14
        OnKeyDown = ProximoCampo
      end
      object edtIdToken: TEdit
        Left = 421
        Top = 234
        Width = 121
        Height = 22
        TabOrder = 15
        OnKeyDown = ProximoCampo
      end
      object edtModeloImpressora: TEdit
        Left = 373
        Top = 194
        Width = 169
        Height = 22
        TabOrder = 11
        OnKeyDown = ProximoCampo
      end
    end
    object tsVendas: TTabSheet
      Caption = 'Or'#231'amentos / vendas'
      ImageIndex = 2
      object lb3: TLabel
        Left = 177
        Top = 44
        Width = 124
        Height = 14
        Caption = 'Cor lucratividade > 60%'
      end
      object lb1: TLabel
        Left = 177
        Top = 1
        Width = 124
        Height = 14
        Caption = 'Cor lucratividade > 30%'
      end
      object lb4: TLabel
        Left = 1
        Top = 44
        Width = 124
        Height = 14
        Caption = 'Cor lucratividade > 10%'
      end
      object lb5: TLabel
        Left = 1
        Top = 1
        Width = 118
        Height = 14
        Caption = 'Cor lucratividade > 0%'
      end
      object lb12: TLabel
        Left = 1
        Top = 90
        Width = 213
        Height = 14
        Caption = 'Regra de defini'#231#227'o de pre'#231'os na venda:'
      end
      object lb14: TLabel
        Left = 376
        Top = 3
        Width = 137
        Height = 14
        Caption = 'Dias validade or'#231'amento'
      end
      object lb19: TLabel
        Left = 376
        Top = 45
        Width = 124
        Height = 14
        Caption = 'Vlr. frete padr'#227'o venda'
      end
      object lb23: TLabel
        Left = 376
        Top = 90
        Width = 172
        Height = 14
        Caption = 'Qtde.dias p/ bloq.de t'#237't.abertos'
      end
      object Label1: TLabel
        Left = 177
        Top = 143
        Width = 335
        Height = 14
        Caption = 'Informa'#231#245'es complementares do comprovante de pagamento'
      end
      object cbCorLucratividade1: TColorBox
        Left = 177
        Top = 59
        Width = 132
        Height = 22
        DefaultColorColor = clBlue
        NoneColorColor = clBlue
        Selected = clBlue
        Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbPrettyNames]
        TabOrder = 0
      end
      object cbCorLucratividade2: TColorBox
        Left = 177
        Top = 16
        Width = 132
        Height = 22
        DefaultColorColor = clGreen
        NoneColorColor = clGreen
        Selected = clGreen
        Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbPrettyNames]
        TabOrder = 1
      end
      object cbCorLucratividade3: TColorBox
        Left = 1
        Top = 59
        Width = 132
        Height = 22
        DefaultColorColor = clYellow
        NoneColorColor = clYellow
        Selected = clYellow
        Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbPrettyNames]
        TabOrder = 2
      end
      object cbCorLucratividade4: TColorBox
        Left = 1
        Top = 16
        Width = 132
        Height = 22
        DefaultColorColor = clRed
        NoneColorColor = clRed
        Selected = clRed
        Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbPrettyNames]
        TabOrder = 3
      end
      inline FrCondicaoPagamentoPDV: TFrCondicoesPagamento
        Left = 1
        Top = 290
        Width = 283
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 290
        ExplicitWidth = 283
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 283
          Height = 23
          Align = alNone
          ExplicitWidth = 283
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 283
          ExplicitWidth = 283
          inherited lbNomePesquisa: TLabel
            Width = 173
            Caption = 'Condi'#231#227'o de pagamento do PDV'
            ExplicitWidth = 173
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 178
            ExplicitLeft = 178
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 258
          Height = 24
          ExplicitLeft = 258
          ExplicitHeight = 24
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrCondPagtoPadraoPesq: TFrCondicoesPagamento
        Left = 1
        Top = 401
        Width = 283
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 401
        ExplicitWidth = 283
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 283
          Height = 23
          Align = alNone
          ExplicitWidth = 283
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 283
          ExplicitWidth = 283
          inherited lbNomePesquisa: TLabel
            Width = 276
            Caption = 'Condi'#231#227'o de pagto. padr'#227'o para pesq. de produtos'
            ExplicitWidth = 276
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Left = 41
            Top = 9
            ExplicitLeft = 41
            ExplicitTop = 9
          end
          inherited pnSuprimir: TPanel
            Left = 178
            ExplicitLeft = 178
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 258
          Height = 25
          ExplicitLeft = 258
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrCondicaoPagtoPadraoVendaAssistida: TFrCondicoesPagamento
        Left = 1
        Top = 345
        Width = 283
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 6
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 345
        ExplicitWidth = 283
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 283
          Height = 23
          Align = alNone
          ExplicitWidth = 283
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 283
          ExplicitWidth = 283
          inherited lbNomePesquisa: TLabel
            Width = 263
            Caption = 'Condi'#231#227'o de pagto. padr'#227'o para venda assistida'
            ExplicitWidth = 263
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Left = 135
            Top = 14
            ExplicitLeft = 135
            ExplicitTop = 14
          end
          inherited pnSuprimir: TPanel
            Left = 178
            ExplicitLeft = 178
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 258
          Height = 24
          ExplicitLeft = 258
          ExplicitHeight = 24
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrCondicaoPagamentoConsProd2: TFrCondicoesPagamento
        Left = 297
        Top = 290
        Width = 283
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 7
        TabStop = True
        ExplicitLeft = 297
        ExplicitTop = 290
        ExplicitWidth = 283
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Left = -2
          Width = 285
          Height = 23
          Align = alNone
          ExplicitLeft = -2
          ExplicitWidth = 285
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 283
          ExplicitWidth = 283
          inherited lbNomePesquisa: TLabel
            Width = 267
            Caption = '2'#186' op'#231#227'o de condi'#231#227'o de pagto na cons. produtos'
            ExplicitWidth = 267
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 178
            ExplicitLeft = 178
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 258
          Height = 24
          ExplicitLeft = 258
          ExplicitHeight = 24
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrCondicaoPagamentoConsProd3: TFrCondicoesPagamento
        Left = 297
        Top = 345
        Width = 281
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 8
        TabStop = True
        ExplicitLeft = 297
        ExplicitTop = 345
        ExplicitWidth = 281
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 281
          Height = 23
          Align = alNone
          ExplicitWidth = 281
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 281
          ExplicitWidth = 281
          inherited lbNomePesquisa: TLabel
            Width = 267
            Caption = '3'#186' op'#231#227'o de condi'#231#227'o de pagto na cons. produtos'
            ExplicitWidth = 267
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 176
            ExplicitLeft = 176
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 256
          Height = 24
          ExplicitLeft = 256
          ExplicitHeight = 24
          inherited sbPesquisa: TSpeedButton
            Top = 4
            Visible = False
            ExplicitTop = 4
          end
        end
      end
      inline FrCondicaoPagamentoConsProd4: TFrCondicoesPagamento
        Left = 297
        Top = 402
        Width = 283
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 9
        TabStop = True
        ExplicitLeft = 297
        ExplicitTop = 402
        ExplicitWidth = 283
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 283
          Height = 23
          Align = alNone
          ExplicitWidth = 283
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 283
          ExplicitWidth = 283
          inherited lbNomePesquisa: TLabel
            Width = 267
            Caption = '4'#186' op'#231#227'o de condi'#231#227'o de pagto na cons. produtos'
            ExplicitWidth = 267
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 178
            ExplicitLeft = 178
            inherited ckSuprimir: TCheckBox
              Left = 62
              Top = -1
              Visible = False
              ExplicitLeft = 62
              ExplicitTop = -1
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 258
          Height = 24
          ExplicitLeft = 258
          ExplicitHeight = 24
          inherited sbPesquisa: TSpeedButton
            Top = 6
            Visible = False
            ExplicitTop = 6
          end
        end
      end
      object cbTipoPrecoUtilizarVenda: TComboBoxLuka
        Left = 1
        Top = 104
        Width = 304
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 10
        Items.Strings = (
          'Utilizar o menor pre'#231'o fora o pre'#231'o varejo'
          'Utilizar o maior pre'#231'o fora o pre'#231'o varejo'
          'Deixar que o vendedor defina manualmente'
          'Deixar que o vendedor defina apresentando op'#231#245'es automaticamente')
        Valores.Strings = (
          'MEP'
          'MAP'
          'DMA'
          'APA')
        AsInt = 0
      end
      object rgTipoCustoVisualizarLucro: TRadioGroupLuka
        Left = 0
        Top = 143
        Width = 169
        Height = 108
        Hint = 'Tipo de custo para calcular a lucratividade'
        Caption = 'Custo p/ vis. na lucratividade:'
        ItemIndex = 2
        Items.Strings = (
          'Pre'#231'o final'
          'Pre'#231'o compra'
          'CMV')
        TabOrder = 11
        Valores.Strings = (
          'FIN'
          'COM'
          'CMV')
      end
      object eQtdeDiasValidadeOrcamento: TEditLuka
        Left = 376
        Top = 17
        Width = 137
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 3
        TabOrder = 12
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorFretePadraoVenda: TEditLuka
        Left = 376
        Top = 59
        Width = 137
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 3
        TabOrder = 13
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eQtdeDiasBloqVendaTitVenc: TEditLuka
        Left = 376
        Top = 104
        Width = 172
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 3
        TabOrder = 14
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object StaticTextLuka5: TStaticTextLuka
        Left = 3
        Top = 259
        Width = 581
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Parametros de condi'#231#245'es de pagamento pr'#233'-definidas'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 15
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object mmInformacoesComplementaresComprovantePagamento: TMemo
        Left = 177
        Top = 161
        Width = 400
        Height = 90
        MaxLength = 200
        TabOrder = 16
      end
    end
    object tsFinanceiro: TTabSheet
      Caption = 'Caixa / Financeiro'
      ImageIndex = 6
      object lb15: TLabel
        Left = 0
        Top = 0
        Width = 84
        Height = 14
        Caption = '% Juros mensal'
      end
      object lb16: TLabel
        Left = 144
        Top = 0
        Width = 44
        Height = 14
        Caption = '% Multa'
      end
      object lb22: TLabel
        Left = 294
        Top = 53
        Width = 170
        Height = 14
        Caption = 'N'#250'mero estabelecimento Cielo'
      end
      object lb13: TLabel
        Left = 0
        Top = 53
        Width = 240
        Height = 14
        Caption = 'Valor min. gerar t'#237'tulo a receber dif. de caixa'
      end
      object ePercentualJurosMensal: TEditLuka
        Left = 0
        Top = 14
        Width = 101
        Height = 22
        Hint = 'Percentual de juros para multa no contas a receber'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 5
        TabOrder = 0
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object ePercentualMulta: TEditLuka
        Left = 144
        Top = 14
        Width = 101
        Height = 22
        Hint = '% Multa por atraso'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 5
        TabOrder = 1
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eNumeroEstabelecimentoCielo: TEditLuka
        Left = 294
        Top = 67
        Width = 183
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 20
        TabOrder = 2
        OnKeyDown = ProximoCampo
        OnKeyPress = Numeros
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorMinDifTurnoContasRec: TEditLuka
        Left = 0
        Top = 67
        Width = 240
        Height = 22
        Hint = 'Valor minimo para gerar um t'#237'tulo a receber diferen'#231'a de caixa.'
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 5
        TabOrder = 3
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object ckHabilitarRecebimentoPix: TCheckBox
        Left = 3
        Top = 95
        Width = 304
        Height = 17
        Caption = '1 - Habilitar recebimento pix'
        TabOrder = 4
        OnClick = ckUtilizaNFeClick
      end
      object ckBaixaContasReceberPixTurnoAberto: TCheckBox
        Left = 3
        Top = 110
        Width = 364
        Height = 17
        Caption = '2 - Baixa de contas a receber via Pix ir para o turno aberto'
        TabOrder = 5
        OnClick = ckUtilizaNFeClick
      end
    end
  end
  object lbParametros: TListBox
    Left = 4
    Top = 341
    Width = 112
    Height = 121
    Anchors = [akLeft, akBottom]
    ItemHeight = 14
    TabOrder = 3
    Visible = False
    OnDblClick = lbParametrosDblClick
    OnKeyDown = lbParametrosKeyDown
  end
  object ppmCertificado: TPopupMenu
    Left = 584
    Top = 192
    object SalvarCertificado1: TMenuItem
      Caption = 'Salvar Certificado'
      OnClick = SalvarCertificado1Click
    end
  end
end
