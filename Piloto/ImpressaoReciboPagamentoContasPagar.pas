unit ImpressaoReciboPagamentoContasPagar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos, QRExport, _RecordsFinanceiros,
  QRPDFFilt, QRWebFilt, QRCtrls, QuickRpt, Vcl.ExtCtrls, _Biblioteca, _Sessao, _ContasPagarBaixas,
  _ContasPagarBaixasItens;

type
  TFormImpressaoReciboPagamentoContasPagar = class(TFormHerancaRelatoriosGraficos)
    qrTexto: TQRMemo;
    qrbndDetailBand1: TQRBand;
    qrmeFormasPagto: TQRMemo;
    qrl18: TQRLabel;
    qrPagarId: TQRLabel;
    qrDocumento: TQRLabel;
    qrlCabProdutoId: TQRLabel;
    qr2: TQRLabel;
    qr6: TQRLabel;
    qrDataVencimento: TQRLabel;
    qr9: TQRLabel;
    qrValorDocumento: TQRLabel;
    QRShape2: TQRShape;
    qrEmpresaPagadora: TQRLabel;
    qrCpnjPagadora: TQRLabel;
    qr14: TQRLabel;
    qrMultaJuros: TQRLabel;
    qr16: TQRLabel;
    qrValorDesconto: TQRLabel;
    qr18: TQRLabel;
    qrValorAdiantado: TQRLabel;
    qr20: TQRLabel;
    qrSaldoLiquido: TQRLabel;
    qr23: TQRLabel;
    qr22: TQRLabel;
    qr24: TQRLabel;
    qrBaixaId: TQRLabel;
    qrDataPagamento: TQRLabel;
    qrDataBaixa: TQRLabel;
    qrCpfCnpjBeneficiario: TQRLabel;
    qrBeneficiario: TQRLabel;
    QRShape1: TQRShape;
    procedure qrbndDetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioA4BeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioA4NeedData(Sender: TObject; var MoreData: Boolean);
  private
    FPosicao: Integer;
    FTitulos: TArray<RecContasPagarBaixasItens>;
  public
    { Public declarations }
  end;

procedure Imprimir(pBaixaPagarId: Integer);

implementation

{$R *.dfm}

procedure Imprimir(pBaixaPagarId: Integer);
var
  vBaixa: TArray<RecContasPagarBaixas>;
  vForm: TFormImpressaoReciboPagamentoContasPagar;

  vImpressora: RecImpressora;

  procedure AddFormaPagamento(pForma: string; pValor: Double);
  begin
    if pValor > 0 then
      vForm.qrmeFormasPagto.Lines.Add( _Biblioteca.RPad(pForma, 17, '.') + '.: R$ ' + NFormat(pValor) );
  end;

begin
  if pBaixaPagarId = 0 then
    Exit;

  vBaixa := _ContasPagarBaixas.BuscarContasPagarBaixas(Sessao.getConexaoBanco, 0, [pBaixaPagarId]);
  if vBaixa = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vImpressora := Sessao.getImpressora( toComprovantePagtoFinanceiro );
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;

  vForm := TFormImpressaoReciboPagamentoContasPagar.Create(Application, vImpressora);

  vForm.PreencherCabecalho( vBaixa[0].empresa_id );
  vForm.FTitulos := _ContasPagarBaixasItens.BuscarContasPagarBaixasItenss(Sessao.getConexaoBanco, 1, [pBaixaPagarId]);

  vForm.qrTexto.Lines.Text := StringReplace(vForm.qrTexto.Lines.Text, ':NOME_BENEFICIARIO', vBaixa[0].NomeFornecedor, [rfReplaceAll]);
  vForm.qrTexto.Lines.Text := StringReplace(vForm.qrTexto.Lines.Text, ':CPF_CNPJ_BENEFICIARIO', vBaixa[0].CpfCpnjFornecedor, [rfReplaceAll]);
  vForm.qrTexto.Lines.Text := StringReplace(vForm.qrTexto.Lines.Text, ':RAZAO_SOCIAL', vBaixa[0].RazaoSocialEmpresaBaixa, [rfReplaceAll]);
  vForm.qrTexto.Lines.Text := StringReplace(vForm.qrTexto.Lines.Text, ':CNPJ', vBaixa[0].CnpjEmpresaBaixa, [rfReplaceAll]);
  vForm.qrTexto.Lines.Text := StringReplace(vForm.qrTexto.Lines.Text, ':VALOR', NFormat(vBaixa[0].ValorLiquido), [rfReplaceAll]);
  vForm.qrTexto.Lines.Text := StringReplace(vForm.qrTexto.Lines.Text, ':DINHEIRO_POR_EXTENSO', getDinheiroPorExtenso(vBaixa[0].ValorLiquido), [rfReplaceAll]);

  vForm.qrBaixaId.Caption       := NFormat(vBaixa[0].baixa_id);
  vForm.qrDataPagamento.Caption := DFormat(vBaixa[0].data_pagamento);
  vForm.qrDataBaixa.Caption     := DFormat(vBaixa[0].data_hora_baixa);

  vForm.qrmeFormasPagto.Lines.Clear;
  AddFormaPagamento('Dinheiro', vBaixa[0].valor_dinheiro);
  AddFormaPagamento('Cobran�a', vBaixa[0].valor_cobranca);
  AddFormaPagamento('Cr�dito',  vBaixa[0].valor_credito);

  vForm.qrEmpresaPagadora.Caption := vBaixa[0].RazaoSocialEmpresaBaixa;
  vForm.qrCpnjPagadora.Caption    := vBaixa[0].CnpjEmpresaBaixa;

  vForm.qrBeneficiario.Caption        := vBaixa[0].NomeFornecedor;
  vForm.qrCpfCnpjBeneficiario.Caption := vBaixa[0].CpfCpnjFornecedor;

  vForm.Imprimir;

  vForm.Free;
end;

procedure TFormImpressaoReciboPagamentoContasPagar.qrbndDetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrPagarId.Caption         := _Biblioteca.NFormat(FTitulos[FPosicao].PagarId);
  qrDocumento.Caption       := FTitulos[FPosicao].Documento;
  qrDataVencimento.Caption  := _Biblioteca.DFormat(FTitulos[FPosicao].DataVencimento);
  qrValorDocumento.Caption  := _Biblioteca.NFormatN(FTitulos[FPosicao].ValorDocumento);
  qrMultaJuros.Caption      := _Biblioteca.NFormatN(FTitulos[FPosicao].ValorJuros);
  qrValorDesconto.Caption   := _Biblioteca.NFormatN(FTitulos[FPosicao].valor_desconto);
  qrValorAdiantado.Caption  := '';
  qrSaldoLiquido.Caption    := _Biblioteca.NFormatN(FTitulos[FPosicao].ValorLiquido);
end;

procedure TFormImpressaoReciboPagamentoContasPagar.qrRelatorioA4BeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicao := -1;
end;

procedure TFormImpressaoReciboPagamentoContasPagar.qrRelatorioA4NeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicao);
  MoreData := FPosicao < Length(FTitulos);
end;

end.
