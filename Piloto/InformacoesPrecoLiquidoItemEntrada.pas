unit InformacoesPrecoLiquidoItemEntrada;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka;

type
  TFormInformacoesPrecoLiquidoItemEntrada = class(TFormHerancaFinalizar)
    ePrecoUnitario: TEditLuka;
    lbl15: TLabel;
    eOutrasDespesas: TEditLuka;
    lb1: TLabel;
    eDesconto: TEditLuka;
    lb2: TLabel;
    eFreteProporcional: TEditLuka;
    lb3: TLabel;
    eIPI: TEditLuka;
    lb4: TLabel;
    eICMSSt: TEditLuka;
    lb5: TLabel;
    eICMS: TEditLuka;
    lb6: TLabel;
    eICMSFrete: TEditLuka;
    lb7: TLabel;
    ePIS: TEditLuka;
    lb8: TLabel;
    eCOFINS: TEditLuka;
    lb9: TLabel;
    eOutrosCustos: TEditLuka;
    lb10: TLabel;
    ePrecoLiquido: TEditLuka;
    lb11: TLabel;
    ePrecoFinal: TEditLuka;
    lb12: TLabel;
    ePisFrete: TEditLuka;
    lb13: TLabel;
    eCOFINSFrete: TEditLuka;
    lb14: TLabel;
    eValorDifal: TEditLuka;
    lb15: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Informar(
  pPrecoUnitario: Double;
  pOutrasDespesas: Double;
  pDesconto: Double;
  pFreteProporcional: Double;
  pIPI: Double;
  pICMSST: Double;
  pOutrosCustos: Double;
  pDifal: Double;
  pPrecoFinal: Double;
  pICMS: Double;
  pICMSFrete: Double;
  pPIS: Double;
  pCOFINS: Double;
  pPrecoLiquido: Double
);

implementation

{$R *.dfm}

procedure Informar(
  pPrecoUnitario: Double;
  pOutrasDespesas: Double;
  pDesconto: Double;
  pFreteProporcional: Double;
  pIPI: Double;
  pICMSST: Double;
  pOutrosCustos: Double;
  pDifal: Double;
  pPrecoFinal: Double;
  pICMS: Double;
  pICMSFrete: Double;
  pPIS: Double;
  pCOFINS: Double;
  pPrecoLiquido: Double
);
var
  vForm: TFormInformacoesPrecoLiquidoItemEntrada;
begin
  vForm := TFormInformacoesPrecoLiquidoItemEntrada.Create(nil);

  vForm.ePrecoUnitario.AsDouble     := pPrecoUnitario;
  vForm.eOutrasDespesas.AsDouble    := pOutrasDespesas;
  vForm.eDesconto.AsDouble          := pDesconto;
  vForm.eFreteProporcional.AsDouble := pFreteProporcional;
  vForm.eIPI.AsDouble               := pIPI;
  vForm.eICMSSt.AsDouble            := pICMSST;
  vForm.eOutrosCustos.AsDouble      := pOutrosCustos;
  vForm.eValorDifal.AsDouble        := pDifal;
  vForm.ePrecoFinal.AsDouble        := pPrecoFinal;
  vForm.eICMS.AsDouble              := pICMS;
  vForm.eICMSFrete.AsDouble         := pICMSFrete;
  vForm.ePIS.AsDouble               := pPIS;
  vForm.eCOFINS.AsDouble            := pCOFINS;
  vForm.ePrecoLiquido.AsDouble      := pPrecoLiquido;

  vForm.ShowModal;

  vForm.Free;
end;

end.
