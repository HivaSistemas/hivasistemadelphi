unit BuscarDadosCartoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _TiposCobrancaCondicaoPagamento,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, Vcl.StdCtrls, _RecordsCadastros, _Sessao, System.Math,
  _Biblioteca, BuscaDados, _RecordsFinanceiros, BuscarDadosTransacaoCartao;

type
  TFormBuscarDadosCartoes = class(TFormHerancaFinalizar)
    sgOpcoesCartoes: TGridLuka;
    sgCartoesEscolhidos: TGridLuka;
    stCaracteristicas: TStaticText;
    st1: TStaticText;
    stValorTotal: TStaticText;
    stValorTotalASerPago: TStaticText;
    stTotalDefinido: TStaticText;
    stValorTotalDefinido: TStaticText;
    st2: TStaticText;
    stDiferenca: TStaticText;
    st3: TStaticText;
    stCondicaoPagamento: TStaticText;
    procedure sgOpcoesCartoesDblClick(Sender: TObject);
    procedure sgOpcoesCartoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgCartoesEscolhidosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure CalculaDiferencaPagamento;
    procedure sgCartoesEscolhidosDblClick(Sender: TObject);
    procedure sgCartoesEscolhidosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgOpcoesCartoesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FClienteId: Integer;
    FVisualizaBuscaCartao : Boolean;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BuscarCartoes(
  const pCondicaoId: Integer;
  const pNomeCondicao: string;
  const pValorABuscar: Double;
  const pCartoes: TArray<RecTitulosFinanceiros>;
  const pSomenteLeitura: Boolean;
  const pClienteId: Integer;
  const pTipoCartao: string;
  pVisualizaBuscaCartao: Boolean = true
): TRetornoTelaFinalizar< TArray<RecTitulosFinanceiros> >;

implementation

{$R *.dfm}

{ TFormBuscarDadosCartoes }

const
  // Grid de op��es de cart�o
  coTipoCobrancaId       = 0;
  coNomeTipoCobranca     = 1;
  coTipoCartaoAnalit     = 2;
  coRedeCartao           = 3;
  coTipoRecebCartao      = 4;
  coTipoCartao           = 5;
  coUtilizarLimCred      = 6;
  coNaoPermitirConsFinal = 7;

  // Grid de cart�es escolhidos
  ceTipoCobrancaId    = 0;
  ceNomeTipoCobranca  = 1;
  ceValor             = 2;
  ceNumeroCartao      = 3;
  ceNsu               = 4;
  ceCodigoAutorizacao = 5;

  (* Ocultas *)
  ceTipoRecebCartao   = 6;
  ceTipoCartao        = 7;
  ceUtilizarLimCred   = 8;

function BuscarCartoes(
  const pCondicaoId: Integer;
  const pNomeCondicao: string;
  const pValorABuscar: Double;
  const pCartoes: TArray<RecTitulosFinanceiros>;
  const pSomenteLeitura: Boolean;
  const pClienteId: Integer;
  const pTipoCartao: string;
  pVisualizaBuscaCartao: Boolean = true
): TRetornoTelaFinalizar< TArray<RecTitulosFinanceiros> >;
var
  i: Integer;
  vForm: TFormBuscarDadosCartoes;
  vCartoesLiberados: TArray<RecTiposCobrancaLiberadosCondicao>;
begin
  Result.Dados := pCartoes;
  vCartoesLiberados := _TiposCobrancaCondicaoPagamento.BuscarCartoesLiberadosCondicao(Sessao.getConexaoBanco, pCondicaoId, pTipoCartao);
  if vCartoesLiberados = nil then begin
    _Biblioteca.Exclamar('Nenhum cart�o configurado na condi��o de pagamento definida!');
    Exit;
  end;

  vForm := TFormBuscarDadosCartoes.Create(Application);
  vForm.stValorTotalASerPago.Caption := _Biblioteca.NFormat(pValorABuscar);
  vForm.stCondicaoPagamento.Caption := IIfStr(pCondicaoId > 0, NFormat(pCondicaoId) + ' - ') + pNomeCondicao;
  vForm.FClienteId := pClienteId;
  vForm.FVisualizaBuscaCartao := pVisualizaBuscaCartao;
  for i := Low(vCartoesLiberados) to High(vCartoesLiberados) do begin
    vForm.sgOpcoesCartoes.Cells[coTipoCobrancaId, i + 1]   := IntToStr(vCartoesLiberados[i].tipo_cobranca_id);
    vForm.sgOpcoesCartoes.Cells[coNomeTipoCobranca, i + 1] := vCartoesLiberados[i].nome_tipo_cobranca;
    vForm.sgOpcoesCartoes.Cells[coTipoCartaoAnalit, i + 1] := vCartoesLiberados[i].TipoCartaoAnalitico;
    vForm.sgOpcoesCartoes.Cells[coRedeCartao, i + 1]       := vCartoesLiberados[i].rede_cartao;
    vForm.sgOpcoesCartoes.Cells[coTipoRecebCartao, i + 1]  := vCartoesLiberados[i].TipoRecebimentoCartao;
    vForm.sgOpcoesCartoes.Cells[coTipoCartao, i + 1]       := vCartoesLiberados[i].TipoCartao;
    vForm.sgOpcoesCartoes.Cells[coUtilizarLimCred, i + 1]  := vCartoesLiberados[i].UtilizarLimiteCredCliente;
  end;
  vForm.sgOpcoesCartoes.RowCount := IfThen(Length(vCartoesLiberados) > 1, High(vCartoesLiberados) + 2, 2);

  for i := Low(pCartoes) to High(pCartoes) do begin
    vForm.sgCartoesEscolhidos.Cells[ceTipoCobrancaId, i + 1]   := IntToStr(pCartoes[i].CobrancaId);
    vForm.sgCartoesEscolhidos.Cells[ceNomeTipoCobranca, i + 1] := pCartoes[i].NomeCobranca;
    vForm.sgCartoesEscolhidos.Cells[ceValor, i + 1]            := NFormatN(pCartoes[i].valor);
    vForm.sgCartoesEscolhidos.Cells[ceTipoRecebCartao, i + 1]  := pCartoes[i].tipoRecebimentoCartao;
    vForm.sgCartoesEscolhidos.Cells[ceTipoCartao, i + 1]       := pCartoes[i].TipoCartao;
    vForm.sgCartoesEscolhidos.Cells[ceUtilizarLimCred, i + 1]  := pCartoes[i].UtilizarLimiteCredCliente;
  end;
  vForm.sgCartoesEscolhidos.RowCount := IfThen(Length(pCartoes) > 1, High(pCartoes) + 2, 2);
  vForm.CalculaDiferencaPagamento;

  if (pCondicaoId = 0) and (pNomeCondicao = '') then
    vForm.Height := 375;

  _Biblioteca.Habilitar([
    vForm.sgCartoesEscolhidos,
    vForm.sgOpcoesCartoes],
    not pSomenteLeitura,
    False
  );

  if vForm.ShowModal = mrOk then begin
    SetLength(Result.Dados, vForm.sgCartoesEscolhidos.RowCount - 1);
    for i := 1 to vForm.sgCartoesEscolhidos.RowCount - 1 do begin
      Result.Dados[i - 1].CobrancaId                := SFormatInt(vForm.sgCartoesEscolhidos.Cells[ceTipoCobrancaId, i]);
      Result.Dados[i - 1].NomeCobranca              := vForm.sgCartoesEscolhidos.Cells[ceNomeTipoCobranca, i];
      Result.Dados[i - 1].valor                     := SFormatDouble(vForm.sgCartoesEscolhidos.Cells[ceValor, i]);
      Result.Dados[i - 1].tipoRecebimentoCartao     := 'POS';
      Result.Dados[i - 1].TipoRecebCartao           := 'POS';
      Result.Dados[i - 1].TipoCartao                := vForm.sgCartoesEscolhidos.Cells[ceTipoCartao, i];
      Result.Dados[i - 1].UtilizarLimiteCredCliente := vForm.sgCartoesEscolhidos.Cells[ceUtilizarLimCred, i];
      Result.Dados[i - 1].NumeroCartao              := vForm.sgCartoesEscolhidos.Cells[ceNumeroCartao, i];
      Result.Dados[i - 1].NsuTef                    := vForm.sgCartoesEscolhidos.Cells[ceNsu, i];
      Result.Dados[i - 1].CodigoAutorizacao         := vForm.sgCartoesEscolhidos.Cells[ceCodigoAutorizacao, i];
    end;
  end;

  FreeAndNil(vForm);
end;

procedure TFormBuscarDadosCartoes.CalculaDiferencaPagamento;
var
  i: Integer;
begin
  stValorTotalDefinido.Caption := '0,00';
  for i := 1 to sgCartoesEscolhidos.RowCount do
    stValorTotalDefinido.Caption := NFormat(SFormatDouble(stValorTotalDefinido.Caption) + SFormatDouble(sgCartoesEscolhidos.Cells[ceValor, i]));

  stDiferenca.Caption := NFormatN( SFormatDouble(stValorTotalASerPago.Caption) - SFormatDouble(stValorTotalDefinido.Caption) );
end;

procedure TFormBuscarDadosCartoes.sgCartoesEscolhidosDblClick(Sender: TObject);
var
  vValor: Currency;

begin
  inherited;
  if sgCartoesEscolhidos.Cells[ceTipoCobrancaId, sgCartoesEscolhidos.Row] = '' then
    Exit;

  vValor := SFormatDouble(sgCartoesEscolhidos.Cells[ceValor, sgCartoesEscolhidos.Row]);
  vValor := BuscaDados.BuscarDados('Valor cart�o', vValor);

  if vValor = 0 then
    Exit;

  sgCartoesEscolhidos.Cells[ceValor, sgCartoesEscolhidos.Row]             := NFormatN(vValor);

  CalculaDiferencaPagamento;
end;

procedure TFormBuscarDadosCartoes.sgCartoesEscolhidosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  if ACol in[ceTipoCobrancaId, ceValor] then
    sgCartoesEscolhidos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taRightJustify, Rect)
  else
    sgCartoesEscolhidos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

procedure TFormBuscarDadosCartoes.sgCartoesEscolhidosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if Key = VK_DELETE then begin
    sgCartoesEscolhidos.DeleteRow(sgCartoesEscolhidos.Row);
    CalculaDiferencaPagamento;
  end;
end;

procedure TFormBuscarDadosCartoes.sgOpcoesCartoesDblClick(Sender: TObject);
var
  vValor: Currency;
  vLinha: Integer;
  vDadosCartao: TRetornoTelaFinalizar<RecDadosTransacao>;
begin
  inherited;
  if FClienteId = Sessao.getParametros.cadastro_consumidor_final_id then begin
    if sgOpcoesCartoes.Cells[coNaoPermitirConsFinal, sgOpcoesCartoes.Row] = 'S' then begin
      Exclamar('Este cart�o n�o � permitido para consumidor final!');
      Exit;
    end;
  end;

  vValor := BuscaDados.BuscarDados('Valor cart�o', 0);

  if vValor = 0 then
    Exit;
   // Essa linha n�o obriga informar os dados do cart�o no recebimento de venda em cartao Ezequiel
  if Sessao.getParametrosEmpresa.ExirgirDadosCatao = 'S' then begin
   vDadosCartao := BuscarDadosTransacaoCartao.Buscar('', '', '');
  if vDadosCartao.BuscaCancelada then
    Exit;
  end;

  if sgCartoesEscolhidos.Cells[ceTipoCobrancaId, 1] = '' then
    vLinha := 1
  else
    vLinha := sgCartoesEscolhidos.RowCount;

  sgCartoesEscolhidos.Cells[ceTipoCobrancaId, vLinha]   := sgOpcoesCartoes.Cells[coTipoCobrancaId, sgOpcoesCartoes.Row];
  sgCartoesEscolhidos.Cells[ceNomeTipoCobranca, vLinha] := sgOpcoesCartoes.Cells[coNomeTipoCobranca, sgOpcoesCartoes.Row];
  sgCartoesEscolhidos.Cells[ceValor, vLinha]            := NFormatN(vValor);
  sgCartoesEscolhidos.Cells[ceTipoRecebCartao, vLinha]  := sgOpcoesCartoes.Cells[coTipoRecebCartao, sgOpcoesCartoes.Row];
  sgCartoesEscolhidos.Cells[ceTipoCartao, vLinha]       := sgOpcoesCartoes.Cells[coTipoCartao, sgOpcoesCartoes.Row];
  sgCartoesEscolhidos.Cells[ceUtilizarLimCred, vLinha]  := sgOpcoesCartoes.Cells[coUtilizarLimCred, sgOpcoesCartoes.Row];

  sgCartoesEscolhidos.Cells[ceNumeroCartao, vLinha]      := vDadosCartao.Dados.NumeroCartao;
  sgCartoesEscolhidos.Cells[ceCodigoAutorizacao, vLinha] := vDadosCartao.Dados.CodigoAutorizacao;
  sgCartoesEscolhidos.Cells[ceNsu, vLinha]               := vDadosCartao.Dados.Nsu;

  sgCartoesEscolhidos.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);
  CalculaDiferencaPagamento;
end;

procedure TFormBuscarDadosCartoes.sgOpcoesCartoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  if ACol in[coTipoCobrancaId] then
    sgOpcoesCartoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taRightJustify, Rect)
  else
    sgOpcoesCartoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

procedure TFormBuscarDadosCartoes.sgOpcoesCartoesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sgOpcoesCartoesDblClick(Sender);
end;

procedure TFormBuscarDadosCartoes.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if stDiferenca.Caption <> '' then begin
    _Biblioteca.Exclamar('Existe uma diferen�a nos valores, por favor verifique!');
    Abort;
  end;
end;

end.
