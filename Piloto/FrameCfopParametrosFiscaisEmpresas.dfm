inherited FrCfopParametrosFiscaisEmpresas: TFrCfopParametrosFiscaisEmpresas
  Width = 498
  Height = 167
  ExplicitWidth = 498
  ExplicitHeight = 167
  object st1: TStaticTextLuka
    Left = 0
    Top = 0
    Width = 498
    Height = 16
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Par'#226'metros fiscais do CFOP por empresa'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 0
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object sgParametros: TGridLuka
    Left = 0
    Top = 16
    Width = 498
    Height = 151
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    Align = alClient
    ColCount = 3
    Ctl3D = True
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 2
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goThumbTracking]
    ParentCtl3D = False
    TabOrder = 1
    OnDrawCell = sgParametrosDrawCell
    OnKeyDown = sgParametrosKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Empresa'
      'Nome'
      'Calcular PIS/COFINS?')
    OnGetCellPicture = sgParametrosGetCellPicture
    Grid3D = False
    RealColCount = 3
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      51
      219
      111)
  end
end
