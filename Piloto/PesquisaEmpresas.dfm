inherited FormPesquisaEmpresas: TFormPesquisaEmpresas
  Caption = 'Pesquisa de loja'
  ClientHeight = 297
  ExplicitHeight = 328
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Height = 251
    ColCount = 6
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'C'#243'digo'
      'Raz'#227'o social'
      'Nome Fantasia'
      'CNPJ'
      'Ativo')
    RealColCount = 6
    ExplicitHeight = 251
    ColWidths = (
      28
      55
      199
      193
      155
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Top = 248
    Text = '0'
    ExplicitTop = 248
  end
  inherited pnFiltro1: TPanel
    inherited eValorPesquisa: TEditLuka
      Top = 19
      ExplicitTop = 19
    end
    inherited cbOpcoesPesquisa: TComboBoxLuka
      Top = 19
      ExplicitTop = 19
    end
  end
end
