inherited FormImpressaoRecebimentoNaEntregaGrafico: TFormImpressaoRecebimentoNaEntregaGrafico
  Caption = 'FormImpressaoRecebimentoNaEntregaGrafico'
  ExplicitLeft = -34
  ExplicitTop = -358
  ExplicitWidth = 1030
  ExplicitHeight = 754
  PixelsPerInch = 96
  TextHeight = 13
  inherited qrRelatorioNF: TQuickRep
    Left = 223
    Top = 8
    Height = 671
    BeforePrint = qrRelatorioNFBeforePrint
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    OnNeedData = qrRelatorioNFNeedData
    Page.Values = (
      0.000000000000000000
      1109.596354166667000000
      0.000000000000000000
      750.755208333333500000
      0.000000000000000000
      0.000000000000000000
      0.000000000000000000)
    ExplicitLeft = 223
    ExplicitTop = 8
    ExplicitHeight = 671
    inherited qrBandTitulo: TQRBand
      Height = 308
      Size.Values = (
        509.322916666666700000
        750.755208333333500000)
      ExplicitHeight = 308
      inherited qrlNFNomeEmpresa: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          16.536458333333330000
          744.140625000000000000)
        FontSize = 6
      end
      inherited qrNFCnpj: TQRLabel
        Top = 31
        Size.Values = (
          33.072916666666670000
          6.614583333333332000
          51.263020833333340000
          744.140625000000000000)
        FontSize = 7
        ExplicitTop = 31
      end
      inherited qrNFEndereco: TQRLabel
        Size.Values = (
          34.726562500000000000
          6.614583333333332000
          85.989583333333340000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFCidadeUf: TQRLabel
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          122.369791666666700000
          744.140625000000000000)
        FontSize = 7
      end
      inherited qrNFTipoDocumento: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          211.666666666666700000
          719.335937500000000000)
        Caption = 'RECEBIMENTO NA ENTREGA '
        FontSize = 7
      end
      inherited qrSeparador2: TQRShape
        Size.Values = (
          3.307291666666666000
          3.307291666666666000
          281.119791666666700000
          724.296875000000000000)
      end
      inherited qrNFTitulo: TQRLabel
        Size.Values = (
          33.072916666666670000
          1.653645833333333000
          246.393229166666700000
          719.335937500000000000)
        FontSize = 7
      end
      inherited qrlNFEmitidoEm: TQRLabel
        Size.Values = (
          23.151041666666670000
          370.416666666666700000
          3.307291666666666000
          357.187500000000000000)
        FontSize = -6
      end
      inherited qrlNFTelefoneEmpresa: TQRLabel
        Top = 101
        Size.Values = (
          42.994791666666670000
          6.614583333333332000
          167.018229166666700000
          744.140625000000000000)
        FontSize = 7
        ExplicitTop = 101
      end
      object qrlClienteNF: TQRLabel
        Left = 52
        Top = 191
        Width = 386
        Height = 16
        Size.Values = (
          26.458333333333330000
          85.989583333333340000
          315.846354166666700000
          638.307291666666800000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CLIENTE MASTER GOLD PLATINUM'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrl4: TQRLabel
        Left = 3
        Top = 208
        Width = 80
        Height = 16
        Size.Values = (
          26.458333333333330000
          4.960937500000000000
          343.958333333333400000
          132.291666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cond.pagto:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlCondicaoPagamentoNF: TQRLabel
        Left = 84
        Top = 208
        Width = 354
        Height = 16
        Size.Values = (
          26.458333333333330000
          138.906250000000000000
          343.958333333333400000
          585.390625000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '15 - BOLETO 30/60/90/120/150'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrl5: TQRLabel
        Left = 3
        Top = 223
        Width = 124
        Height = 16
        Size.Values = (
          26.458333333333330000
          4.960937500000000000
          368.763020833333400000
          205.052083333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrl6: TQRLabel
        Left = 3
        Top = 255
        Width = 124
        Height = 16
        Size.Values = (
          26.458333333333330000
          4.960937500000000000
          421.679687500000000000
          205.052083333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor outras desp.: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrl7: TQRLabel
        Left = 3
        Top = 272
        Width = 124
        Height = 16
        Size.Values = (
          26.458333333333330000
          4.960937500000000000
          449.791666666666700000
          205.052083333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desconto: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrl9: TQRLabel
        Left = 3
        Top = 289
        Width = 124
        Height = 16
        Size.Values = (
          26.458333333333330000
          4.960937500000000000
          477.903645833333400000
          205.052083333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total ser pago: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlTotalSerPagoNF: TQRLabel
        Left = 128
        Top = 289
        Width = 172
        Height = 16
        Size.Values = (
          26.458333333333330000
          211.666666666666700000
          477.903645833333400000
          284.427083333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total ser pago: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlTotalDescontoNF: TQRLabel
        Left = 128
        Top = 272
        Width = 172
        Height = 16
        Size.Values = (
          26.458333333333330000
          211.666666666666700000
          449.791666666666700000
          284.427083333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor desconto: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlTotalOutrasDespesasNF: TQRLabel
        Left = 128
        Top = 255
        Width = 172
        Height = 16
        Size.Values = (
          26.458333333333330000
          211.666666666666700000
          421.679687500000000000
          284.427083333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor outras desp: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlTotalProdutosNF: TQRLabel
        Left = 128
        Top = 223
        Width = 172
        Height = 16
        Size.Values = (
          26.458333333333330000
          211.666666666666700000
          368.763020833333400000
          284.427083333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total produtos: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrl8: TQRLabel
        Left = 3
        Top = 238
        Width = 124
        Height = 16
        Size.Values = (
          26.458333333333330000
          4.960937500000000000
          393.567708333333400000
          205.052083333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor frete.: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlNFValorFrete: TQRLabel
        Left = 128
        Top = 238
        Width = 172
        Height = 16
        Size.Values = (
          26.458333333333330000
          211.666666666666700000
          393.567708333333400000
          284.427083333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Valor outras desp: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrl1: TQRLabel
        Left = 4
        Top = 191
        Width = 48
        Height = 16
        Size.Values = (
          26.458333333333330000
          6.614583333333332000
          315.846354166666700000
          79.375000000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Cliente:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object QRLabel2: TQRLabel
        Left = 2
        Top = 173
        Width = 77
        Height = 16
        Size.Values = (
          26.458333333333330000
          3.307291666666666000
          286.080729166666700000
          127.330729166666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pedido: '
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlOrcamentoIdNF: TQRLabel
        Left = 74
        Top = 173
        Width = 70
        Height = 16
        Size.Values = (
          26.458333333333330000
          122.369791666666700000
          286.080729166666700000
          115.755208333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '146'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object QRLabel4: TQRLabel
        Left = 145
        Top = 173
        Width = 68
        Height = 16
        Size.Values = (
          26.458333333333330000
          239.778645833333300000
          286.080729166666700000
          112.447916666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Vendedor:'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
      object qrlVendedorNF: TQRLabel
        Left = 214
        Top = 173
        Width = 224
        Height = 16
        Size.Values = (
          26.458333333333330000
          353.880208333333400000
          286.080729166666700000
          370.416666666666700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'VENDEDOR MASTER GOLD PLATINUM'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = 9
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = -7
      end
    end
    inherited PageFooterBand1: TQRBand
      Top = 393
      Height = 148
      Size.Values = (
        244.739583333333400000
        750.755208333333500000)
      BandType = rbSummary
      ExplicitTop = 393
      ExplicitHeight = 148
      inherited qrlNFSistema: TQRLabel
        Top = 126
        Size.Values = (
          23.151041666666670000
          582.083333333333400000
          208.359375000000000000
          138.906250000000000000)
        Caption = 'Hiva 2.0.0.1'
        FontSize = -6
        ExplicitTop = 126
      end
      object qrNFmeFormasPagto: TQRMemo
        Left = 4
        Top = 25
        Width = 433
        Height = 86
        Size.Values = (
          142.213541666666700000
          6.614583333333332000
          41.341145833333340000
          716.028645833333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        AutoStretch = True
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          'Dinheiro........: R$ 1.000,00'
          'Cheque........: R$ 1.000,00'
          'Cart'#227'o cr'#233'dito: R$ 1.000,00'
          'Cart'#227'o d'#233'bito.: R$ 1.000,00')
        ParentFont = False
        Transparent = False
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 8
      end
      object qrl18: TQRLabel
        Left = 1
        Top = 1
        Width = 435
        Height = 18
        Size.Values = (
          29.765625000000000000
          1.653645833333333000
          1.653645833333333000
          719.335937500000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'TIPOS DE RECEBIMENTO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
    object ColumnHeaderBand1: TQRBand
      Left = 0
      Top = 308
      Width = 454
      Height = 22
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        36.380208333333340000
        750.755208333333500000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbColumnHeader
      object qrl11: TQRLabel
        Left = 1
        Top = 2
        Width = 438
        Height = 18
        Size.Values = (
          29.765625000000000000
          1.653645833333333000
          3.307291666666666000
          724.296875000000000000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = '------------------------- PRODUTOS ----------------------------'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
    object DetailBand1: TQRBand
      Left = 0
      Top = 330
      Width = 454
      Height = 63
      AlignToBottom = False
      BeforePrint = DetailBand1BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        104.179687500000000000
        750.755208333333500000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object qrlNFNomeProduto: TQRLabel
        Left = 74
        Top = 1
        Width = 362
        Height = 20
        Size.Values = (
          33.072916666666670000
          123.031250000000000000
          1.322916666666667000
          597.958333333333200000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '7 - CIMENTO CIPLAN 50KG 7 - CIMENTO CIPLAN CIMENTO'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFQuantidade: TQRLabel
        Left = 206
        Top = 42
        Width = 87
        Height = 20
        Size.Values = (
          33.674242424242430000
          340.350378787878800000
          69.753787878787890000
          143.115530303030300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '99.999,999'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlCabProdutoId: TQRLabel
        Left = 2
        Top = 1
        Width = 71
        Height = 19
        Size.Values = (
          31.750000000000000000
          3.968750000000000000
          1.322916666666667000
          117.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Produto:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrl12: TQRLabel
        Left = 2
        Top = 22
        Width = 71
        Height = 19
        Size.Values = (
          31.750000000000000000
          3.968750000000000000
          37.041666666666670000
          117.739583333333300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Marca...:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFMarca: TQRLabel
        Left = 74
        Top = 22
        Width = 362
        Height = 20
        Size.Values = (
          33.072916666666670000
          123.031250000000000000
          37.041666666666670000
          597.958333333333400000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = '1 - AMIGO KENIM'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrl15: TQRLabel
        Left = 158
        Top = 42
        Width = 47
        Height = 20
        Size.Values = (
          33.674242424242430000
          260.975378787878800000
          69.753787878787890000
          78.172348484848500000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Qtde.:'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFUnidade: TQRLabel
        Left = 295
        Top = 42
        Width = 32
        Height = 20
        Size.Values = (
          33.674242424242430000
          487.073863636363700000
          69.753787878787890000
          52.916666666666660000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'CXA'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrl16: TQRLabel
        Left = 2
        Top = 42
        Width = 81
        Height = 20
        Size.Values = (
          33.674242424242430000
          3.607954545454545000
          69.753787878787890000
          134.696969696969700000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Pre'#231'o unit.'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFPrecoUnitario: TQRLabel
        Left = 85
        Top = 42
        Width = 69
        Height = 20
        Size.Values = (
          33.674242424242430000
          140.710227272727200000
          69.753787878787890000
          114.251893939393900000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '9.999,99'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrl17: TQRLabel
        Left = 329
        Top = 42
        Width = 44
        Height = 20
        Size.Values = (
          33.674242424242430000
          543.598484848484900000
          69.753787878787890000
          72.159090909090910000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = 'Total'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
      object qrlNFValorTotal: TQRLabel
        Left = 375
        Top = 42
        Width = 61
        Height = 20
        Size.Values = (
          33.674242424242430000
          620.568181818181900000
          69.753787878787890000
          101.022727272727300000)
        XLColumn = 0
        XLNumFormat = nfGeneral
        Alignment = taRightJustify
        AlignToBand = False
        AutoSize = False
        Caption = '9.999,99'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        VerticalAlignment = tlTop
        FontSize = 7
      end
    end
  end
  inherited qrRelatorioA4: TQuickRep
    Top = 696
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    ExplicitTop = 696
    inherited qrCabecalho: TQRBand
      Size.Values = (
        227.541666666666700000
        1899.708333333333000000)
      inherited qrCaptionEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          44.979166666666670000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          7.937500000000000000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qrEndereco: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          44.979166666666670000
          1513.416666666667000000)
        FontSize = 7
      end
      inherited qrLogoEmpresa: TQRImage
        Size.Values = (
          211.666666666666700000
          13.229166666666670000
          2.645833333333333000
          211.666666666666700000)
      end
      inherited qrCaptionEmpresa: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmitidoEm: TQRLabel
        Size.Values = (
          29.104166666666670000
          1529.291666666667000000
          0.000000000000000000
          370.416666666666700000)
        FontSize = 5
      end
      inherited qr1: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrBairro: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
      end
      inherited qr3: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          82.020833333333320000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCidadeUf: TQRLabel
        Width = 215
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          82.020833333333320000
          568.854166666666700000)
        FontSize = 7
        ExplicitWidth = 215
      end
      inherited qr5: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          7.937500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrCNPJ: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          7.937500000000000000
          248.708333333333300000)
        FontSize = 7
      end
      inherited qr7: TQRLabel
        Size.Values = (
          34.395833333333330000
          222.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrTelefone: TQRLabel
        Size.Values = (
          34.395833333333330000
          386.291666666666700000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
      end
      inherited qrFax: TQRLabel
        Size.Values = (
          34.395833333333330000
          767.291666666666800000
          119.062500000000000000
          187.854166666666700000)
        FontSize = 7
      end
      inherited qr10: TQRLabel
        Size.Values = (
          34.395833333333330000
          603.250000000000000000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qr11: TQRLabel
        Size.Values = (
          34.395833333333330000
          965.729166666666800000
          119.062500000000000000
          158.750000000000000000)
        FontSize = 7
      end
      inherited qrEmail: TQRLabel
        Size.Values = (
          34.395833333333330000
          1129.770833333333000000
          119.062500000000000000
          769.937500000000000000)
        FontSize = 7
      end
      inherited qr13: TQRLabel
        Size.Values = (
          52.916666666666670000
          711.729166666666800000
          164.041666666666700000
          529.166666666666700000)
        FontSize = 12
      end
      inherited qrLabelCep: TQRLabel
        Size.Values = (
          39.687500000000000000
          1656.291666666667000000
          82.020833333333320000
          74.083333333333320000)
        FontSize = 9
      end
      inherited qrCEP: TQRLabel
        Size.Values = (
          39.687500000000000000
          1735.666666666667000000
          82.020833333333320000
          171.979166666666700000)
        FontSize = 9
      end
    end
    inherited qrbndRodape: TQRBand
      Size.Values = (
        111.125000000000000000
        1899.708333333333000000)
      inherited qrUsuarioImpressao: TQRLabel
        Size.Values = (
          31.750000000000000000
          7.937500000000000000
          74.083333333333340000
          354.541666666666700000)
        FontSize = 7
      end
      inherited qrSistema: TQRLabel
        Size.Values = (
          31.750000000000000000
          1529.291666666667000000
          74.083333333333340000
          370.416666666666700000)
        FontSize = 7
      end
    end
  end
end
