inherited FrFechamentoJuros: TFrFechamentoJuros
  Height = 287
  ExplicitHeight = 287
  inherited gbFechamento: TGroupBox
    Height = 287
    ExplicitHeight = 287
    inherited lb19: TLabel
      Top = 103
      ExplicitTop = 103
    end
    inherited lb20: TLabel
      Top = 62
      ExplicitTop = 62
    end
    inherited lb21: TLabel
      Top = 83
      ExplicitTop = 83
    end
    inherited lb22: TLabel
      Top = 63
      ExplicitTop = 63
    end
    inherited lb7: TLabel
      Top = 83
      ExplicitTop = 83
    end
    inherited lb11: TLabel
      Top = 141
      ExplicitTop = 141
    end
    inherited lb12: TLabel
      Top = 161
      ExplicitTop = 161
    end
    inherited lb13: TLabel
      Top = 181
      ExplicitTop = 181
    end
    inherited lb14: TLabel
      Top = 221
      ExplicitTop = 221
    end
    inherited sbBuscarDadosCheques: TSpeedButton
      Top = 157
      ExplicitTop = 157
    end
    inherited sbBuscarDadosCartoesDebito: TSpeedButton
      Top = 177
      ExplicitTop = 177
    end
    inherited sbBuscarDadosCobranca: TSpeedButton
      Top = 217
      ExplicitTop = 217
    end
    inherited lb1: TLabel
      Left = 295
      Visible = False
      ExplicitLeft = 295
    end
    inherited lb3: TLabel
      Top = 43
      ExplicitTop = 43
    end
    inherited lb4: TLabel
      Top = 201
      ExplicitTop = 201
    end
    inherited sbBuscarDadosCartoesCredito: TSpeedButton
      Top = 197
      ExplicitTop = 197
    end
    object lb5: TLabel [21]
      Left = 11
      Top = 22
      Width = 99
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Valor juros'
    end
    object lb6: TLabel [22]
      Left = 248
      Top = 22
      Width = 11
      Height = 13
      Caption = '%'
    end
    inherited eValorTotalASerPago: TEditLuka
      Top = 95
      TabOrder = 7
      ExplicitTop = 95
    end
    inherited ePercentualOutrasDespesas: TEditLuka
      Top = 55
      TabOrder = 4
      ExplicitTop = 55
    end
    inherited eValorDesconto: TEditLuka
      Top = 75
      TabOrder = 5
      ExplicitTop = 75
    end
    inherited eValorOutrasDespesas: TEditLuka
      Top = 55
      TabOrder = 3
      ExplicitTop = 55
    end
    inherited ePercentualDesconto: TEditLuka
      Top = 75
      TabOrder = 6
      ExplicitTop = 75
    end
    inherited eValorDinheiro: TEditLuka
      Top = 134
      TabOrder = 8
      ExplicitTop = 134
    end
    inherited eValorCheque: TEditLuka
      Top = 154
      TabOrder = 9
      ExplicitTop = 154
    end
    inherited eValorCartaoDebito: TEditLuka
      Top = 174
      TabOrder = 10
      ExplicitTop = 174
    end
    inherited eValorCobranca: TEditLuka
      Top = 214
      TabOrder = 12
      ExplicitTop = 214
    end
    inherited eValorCredito: TEditLuka
      TabOrder = 13
    end
    inherited stSPC: TStaticText
      Top = 119
      TabOrder = 17
      ExplicitTop = 119
    end
    inherited eValorAcumulativo: TEditLuka
      Left = 376
      TabOrder = 15
      Visible = False
      ExplicitLeft = 376
    end
    inherited eValorFinanceira: TEditLuka
      TabOrder = 16
    end
    inherited eValorFrete: TEditLuka
      Top = 35
      TabOrder = 2
      ExplicitTop = 35
    end
    inherited eValorCartaoCredito: TEditLuka
      Top = 194
      TabOrder = 11
      ExplicitTop = 194
    end
    object eValorJuros: TEditLuka
      Left = 112
      Top = 15
      Width = 97
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      TabOrder = 0
      Text = '0,00'
      OnChange = eValorJurosChange
      OnKeyDown = ProximoCampo
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object ePercentualJuros: TEditLuka
      Left = 208
      Top = 15
      Width = 38
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      MaxLength = 5
      TabOrder = 1
      Text = '0,00'
      OnChange = ePercentualJurosChange
      OnKeyDown = ProximoCampo
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
  end
end
