inherited FormInformacoesPedidoCompra: TFormInformacoesPedidoCompra
  Caption = 'Informa'#231#245'es do pedido de compra'
  ClientHeight = 458
  ClientWidth = 925
  ExplicitWidth = 931
  ExplicitHeight = 487
  PixelsPerInch = 96
  TextHeight = 14
  object lbl9: TLabel [0]
    Left = 4
    Top = 3
    Width = 37
    Height = 14
    Caption = 'C'#243'digo'
  end
  object lbl10: TLabel [1]
    Left = 89
    Top = 3
    Width = 61
    Height = 14
    Caption = 'Fornecedor'
  end
  object lbl27: TLabel [2]
    Left = 305
    Top = 3
    Width = 47
    Height = 14
    Caption = 'Empresa'
  end
  object lbl11: TLabel [3]
    Left = 471
    Top = 3
    Width = 59
    Height = 14
    Caption = 'Comprador'
  end
  inherited pnOpcoes: TPanel
    Top = 421
    Width = 925
    ExplicitTop = 421
    ExplicitWidth = 925
  end
  object eID: TEditLuka
    Left = 4
    Top = 17
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object pgcDados: TPageControl
    Left = 0
    Top = 46
    Width = 925
    Height = 375
    ActivePage = tsPrincipais
    Align = alBottom
    TabOrder = 2
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      object lbl1: TLabel
        Left = 1
        Top = -1
        Width = 97
        Height = 14
        Caption = 'Data faturamento'
      end
      object lbl2: TLabel
        Left = 101
        Top = -1
        Width = 91
        Height = 14
        Caption = 'Previs'#227'o entrega'
      end
      object lbl8: TLabel
        Left = 197
        Top = -2
        Width = 69
        Height = 14
        Caption = 'Observa'#231#245'es'
      end
      object eDataFaturamento: TEditLukaData
        Left = 1
        Top = 13
        Width = 95
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 0
        Text = '  /  /    '
      end
      object ePrevisaoEntrega: TEditLukaData
        Left = 101
        Top = 13
        Width = 92
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 1
        Text = '  /  /    '
      end
      inline FrPlanoFinanceiro: TFrPlanosFinanceiros
        Left = 1
        Top = 57
        Width = 270
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 57
        ExplicitWidth = 270
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 245
          Height = 23
          ExplicitWidth = 245
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 270
          ExplicitWidth = 270
          inherited lbNomePesquisa: TLabel
            Width = 88
            Caption = 'Plano financeiro'
            ExplicitWidth = 88
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 165
            ExplicitLeft = 165
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 245
          Height = 24
          ExplicitLeft = 245
          ExplicitHeight = 24
        end
      end
      object meObservacoes: TMemoAltis
        Left = 197
        Top = 13
        Width = 642
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 800
        TabOrder = 3
      end
      object StaticTextLuka3: TStaticTextLuka
        Left = -1
        Top = 39
        Width = 493
        Height = 15
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Previs'#227'o financeira ( Opcional )'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        Transparent = False
        Visible = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      inline FrDadosCobranca: TFrDadosCobranca
        Left = 0
        Top = 103
        Width = 578
        Height = 243
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        TabStop = True
        ExplicitTop = 103
        ExplicitHeight = 243
        inherited pnInformacoesPrincipais: TPanel
          inherited lbllb10: TLabel
            Width = 39
            Height = 14
            ExplicitWidth = 39
            ExplicitHeight = 14
          end
          inherited lbllb9: TLabel
            Width = 29
            Height = 14
            ExplicitWidth = 29
            ExplicitHeight = 14
          end
          inherited lbllb6: TLabel
            Width = 28
            Height = 14
            ExplicitWidth = 28
            ExplicitHeight = 14
          end
          inherited lbllb2: TLabel
            Width = 64
            Height = 14
            ExplicitWidth = 64
            ExplicitHeight = 14
          end
          inherited lbl2: TLabel
            Width = 62
            Height = 14
            ExplicitWidth = 62
            ExplicitHeight = 14
          end
          inherited eRepetir: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited ePrazo: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eValorCobranca: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eDataVencimento: TEditLukaData
            Height = 22
            ExplicitHeight = 22
          end
          inherited FrTiposCobranca: TFrTiposCobranca
            inherited PnTitulos: TPanel
              Enabled = False
              inherited lbNomePesquisa: TLabel
                Height = 15
              end
              inherited pnSuprimir: TPanel
                Left = 119
                ExplicitLeft = 119
              end
            end
          end
          inherited eDocumento: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
        end
        inherited pnInformacoesBoleto: TPanel
          inherited lbllb31: TLabel
            Width = 79
            Height = 14
            ExplicitWidth = 79
            ExplicitHeight = 14
          end
          inherited lbl1: TLabel
            Width = 92
            Height = 14
            ExplicitWidth = 92
            ExplicitHeight = 14
          end
          inherited eNossoNumero: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eCodigoBarras: TMaskEditLuka
            Height = 22
            ExplicitHeight = 22
          end
        end
        inherited pn1: TPanel
          Height = 130
          ExplicitHeight = 130
          inherited sgCobrancas: TGridLuka
            Top = 3
            Height = 125
            CorLinhaFoco = 38619
            CorColunaFoco = clActiveCaption
            ExplicitTop = 3
            ExplicitHeight = 125
          end
          inherited ckObrigarDataBase: TCheckBoxLuka
            Checked = True
            State = cbChecked
            CheckedStr = 'S'
          end
        end
        inherited pnInformacoesCheque: TPanel
          inherited lbllb1: TLabel
            Width = 33
            Height = 14
            ExplicitWidth = 33
            ExplicitHeight = 14
          end
          inherited lbllb3: TLabel
            Width = 43
            Height = 14
            ExplicitWidth = 43
            ExplicitHeight = 14
          end
          inherited lbllb4: TLabel
            Width = 79
            Height = 14
            ExplicitWidth = 79
            ExplicitHeight = 14
          end
          inherited lbllb5: TLabel
            Width = 71
            Height = 14
            ExplicitWidth = 71
            ExplicitHeight = 14
          end
          inherited lbl3: TLabel
            Width = 85
            Height = 14
            ExplicitWidth = 85
            ExplicitHeight = 14
          end
          inherited lbl4: TLabel
            Width = 48
            Height = 14
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited lbCPF_CNPJ: TLabel
            Width = 52
            Height = 14
            ExplicitWidth = 52
            ExplicitHeight = 14
          end
          inherited eBanco: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eAgencia: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eContaCorrente: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eNumeroCheque: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eNomeEmitente: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eTelefoneEmitente: TEditTelefoneLuka
            Height = 22
            OnKeyDown = nil
            ExplicitHeight = 22
          end
          inherited eCPF_CNPJ: TEditCPF_CNPJ_Luka
            Height = 22
            ExplicitHeight = 22
          end
        end
      end
    end
    object tsProdutos: TTabSheet
      Caption = 'Produtos'
      ImageIndex = 1
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 360
        Width = 129
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Qtde. produtos'
        Color = 10066176
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object txtQuantidadeItensGrid: TStaticText
        Left = 0
        Top = 376
        Width = 129
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        Transparent = False
      end
      object txtValorTotal: TStaticText
        Left = 128
        Top = 376
        Width = 129
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 128
        Top = 360
        Width = 129
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Valor total l'#237'quido'
        Color = 10066176
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object pnl1: TPanel
        Left = 0
        Top = 312
        Width = 917
        Height = 34
        Align = alBottom
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 4
        object stQuantidadeItensGrid: TStaticText
          Left = 0
          Top = 17
          Width = 129
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Transparent = False
        end
        object stValorTotal: TStaticText
          Left = 128
          Top = 17
          Width = 129
          Height = 16
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          BevelKind = bkFlat
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          Transparent = False
        end
        object StaticTextLuka6: TStaticTextLuka
          Left = 0
          Top = 0
          Width = 129
          Height = 17
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          Caption = 'Qtde. produtos'
          Color = 38619
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          Transparent = False
          AsInt = 0
          TipoCampo = tcTexto
          CasasDecimais = 0
        end
        object StaticTextLuka7: TStaticTextLuka
          Left = 128
          Top = 0
          Width = 129
          Height = 17
          Alignment = taCenter
          AutoSize = False
          BevelInner = bvNone
          Caption = 'Valor total l'#237'quido'
          Color = 38619
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 3
          Transparent = False
          AsInt = 0
          TipoCampo = tcTexto
          CasasDecimais = 0
        end
      end
      object StaticTextLuka4: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 917
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Itens do pedido de compra selecionado'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 17
        Width = 917
        Height = 295
        Align = alClient
        ColCount = 14
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 6
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Und.'
          'Quantidade'
          'Pre'#231'o unit.'
          'Valor total'
          'Valor desconto'
          'Valor out.desp.'
          'Valor l'#237'quido'
          'Entregues'
          'Baixados'
          'Cancelados'
          'Saldo')
        Grid3D = False
        RealColCount = 14
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          269
          154
          30
          82
          65
          81
          88
          89
          81
          78
          63
          71
          64)
      end
    end
    object tsBaixas: TTabSheet
      Caption = 'Baixas'
      ImageIndex = 2
      OnShow = tsBaixasShow
      object sgBaixas: TGridLuka
        Left = 0
        Top = 0
        Width = 917
        Height = 346
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 3
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        PopupMenu = pmOpcoesBaixas
        TabOrder = 0
        OnDblClick = sgFinanceirosDblClick
        OnDrawCell = sgBaixasDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'C'#243'digo'
          'Data/hora baixa'
          'Usu'#225'rio baixa')
        Grid3D = False
        RealColCount = 5
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          57
          143
          233)
      end
    end
    object tsFinanceiro: TTabSheet
      Caption = 'Financeiro'
      ImageIndex = 3
      OnShow = tsFinanceiroShow
      object StaticTextLuka8: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 917
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'T'#237'tulos gerados'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgFinanceiros: TGridLuka
        Left = 0
        Top = 17
        Width = 917
        Height = 329
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 1
        OnDblClick = sgFinanceirosDblClick
        OnDrawCell = sgFinanceirosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'C'#243'digo'
          'Tipo de cobran'#231'a'
          'Valor'
          'Status'
          'Data vencimento')
        Grid3D = False
        RealColCount = 5
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          57
          257
          99
          97
          103)
      end
    end
  end
  object eFornecedor: TEditLuka
    Left = 89
    Top = 17
    Width = 211
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmpresa: TEditLuka
    Left = 305
    Top = 17
    Width = 161
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 4
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eComprador: TEditLuka
    Left = 471
    Top = 17
    Width = 242
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 5
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object StaticTextLuka5: TStaticTextLuka
    Left = 719
    Top = 7
    Width = 200
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Status'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stStatus: TStaticText
    Left = 719
    Top = 23
    Width = 200
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Ag. chegada das mercadorias'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = 33023
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
  end
  object pmOpcoesBaixas: TPopupMenu
    Left = 857
    Top = 342
    object miCancelarBaixaFocada: TMenuItem
      Caption = 'Cancelar baixa focada'
      OnClick = miCancelarBaixaFocadaClick
    end
  end
end
