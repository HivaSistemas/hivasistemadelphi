inherited FormPesquisaVendas: TFormPesquisaVendas
  Caption = 'Pesquisa de produtos para venda'
  ClientHeight = 554
  ClientWidth = 854
  OnShow = FormShow
  ExplicitWidth = 860
  ExplicitHeight = 583
  PixelsPerInch = 96
  TextHeight = 14
  object lblOpcoesPesquisa: TLabel [0]
    Left = 4
    Top = -1
    Width = 109
    Height = 14
    Caption = 'Filtros de pesquisa:'
  end
  object lblPesquisa: TLabel [1]
    Left = 5
    Top = 38
    Width = 56
    Height = 14
    Caption = 'Descri'#231#227'o:'
  end
  object lbMarca: TLabel [2]
    Left = 223
    Top = 38
    Width = 33
    Height = 14
    Caption = 'Marca'
  end
  inherited pnOpcoes: TPanel
    Top = 517
    Width = 854
    ExplicitTop = 517
    ExplicitWidth = 854
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 75
    Width = 854
    Height = 442
    ActivePage = TabSheet1
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Produtos'
      object lbNomeCondicaoPagto2: TLabel
        Left = 456
        Top = 343
        Width = 218
        Height = 16
        AutoSize = False
        Caption = '[]'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lbNomeCondicaoPagto3: TLabel
        Left = 456
        Top = 363
        Width = 218
        Height = 16
        AutoSize = False
        Caption = '[]'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lbNomeCondicaoPagto4: TLabel
        Left = 456
        Top = 381
        Width = 218
        Height = 16
        AutoSize = False
        Caption = '[]'
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lbPrecoCondicaoPagto2: TLabel
        Left = 681
        Top = 343
        Width = 76
        Height = 16
        AutoSize = False
        Caption = 'R$ 9.999,00'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lbPrecoCondicaoPagto3: TLabel
        Left = 681
        Top = 363
        Width = 76
        Height = 16
        AutoSize = False
        Caption = 'R$ 15,00'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lbPrecoCondicaoPagto4: TLabel
        Left = 681
        Top = 381
        Width = 76
        Height = 16
        AutoSize = False
        Caption = 'R$ 15,00'
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 392
        Top = 343
        Width = 58
        Height = 16
        AutoSize = False
        Caption = 'Condi'#231#227'o 1:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 392
        Top = 363
        Width = 58
        Height = 16
        AutoSize = False
        Caption = 'Condi'#231#227'o 2:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 392
        Top = 381
        Width = 58
        Height = 16
        AutoSize = False
        Caption = 'Condi'#231#227'o 3:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbEstoqueGrupo: TLabel
        Left = 305
        Top = 395
        Width = 22
        Height = 16
        Alignment = taRightJustify
        AutoSize = False
        Caption = '[]'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object Label2: TLabel
        Left = 233
        Top = 396
        Width = 66
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Est. grupo:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object Label1: TLabel
        Left = 20
        Top = 343
        Width = 80
        Height = 16
        Hint = 
          'Estoque disponpivel de todas as empresas (Exceto a empresa logad' +
          'a)'
        AutoSize = False
        Caption = 'Est.tod.emp.:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbEstoqueTodasEmpresas: TLabel
        Left = 87
        Top = 343
        Width = 63
        Height = 16
        Hint = 
          'Estoque disponpivel de todas as empresas (Exceto a empresa logad' +
          'a)'
        Alignment = taRightJustify
        AutoSize = False
        Caption = '[]'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object sbTabelaPrecos: TSpeedButtonLuka
        Left = 480
        Top = 394
        Width = 18
        Height = 20
        Hint = 'Tabela de pre'#231'os'
        Flat = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = sbTabelaPrecosClick
        TagImagem = 13
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object sbInformacoesEstoque: TSpeedButtonLuka
        Left = 1
        Top = 341
        Width = 18
        Height = 18
        Hint = 'Informa'#231#245'es do estoque de todas as empresas'
        Flat = True
        NumGlyphs = 2
        OnClick = sbInformacoesEstoqueClick
        TagImagem = 13
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object lb1: TLabel
        Left = 20
        Top = 363
        Width = 67
        Height = 16
        AutoSize = False
        Caption = 'C'#243'd. barras:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb3: TLabel
        Left = 20
        Top = 381
        Width = 109
        Height = 16
        AutoSize = False
        Caption = 'C'#243'd. ref. da ind'#250'stria:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbCodigoBarras: TLabel
        Left = 79
        Top = 363
        Width = 137
        Height = 16
        AutoSize = False
        Caption = '7890001524234'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object lbCodigoOriginal: TLabel
        Left = 127
        Top = 381
        Width = 154
        Height = 16
        AutoSize = False
        Caption = 'orig02182123'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 392
        Top = 400
        Width = 89
        Height = 16
        AutoSize = False
        Caption = 'Tabela de pre'#231'os:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object miLegendas: TSpeedButton
        Left = 4
        Top = 397
        Width = 99
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'Legendas'
        Flat = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000130B0000130B00000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000110
          1514011016140000000000000000000000000000000001101614011016140000
          000000000000000000000000000001101614011015140000000003506964069F
          D1CE06A0D1CF02465C58000102010001010102465C5806A0D1CF06A0D1CF0246
          5C58000102010001010102465C5806A0D1CF069FD1CE0350696407B6F0ED07C4
          FFFF07C5FFFF06A0D3D001101413010F141306A0D3D007C5FFFF07C5FFFF06A0
          D3D001101413010F141306A0D3D007C5FFFF07C4FFFF07B6F0ED07B6F0ED07C4
          FFFF07C5FFFF06A0D3D0010F14130110141306A0D3D007C5FFFF07C5FFFF06A0
          D3D0010F14130110141306A0D3D007C5FFFF07C4FFFF07B6F0ED03506964069F
          D1CE06A0D2CF02465C58000101010001020102465D5806A0D2CF06A0D2CF0246
          5C58000101010001020102465D5806A0D2CF069FD1CE03506964000000000110
          1514011016140000000000000000000000000000000001101614011016140000
          0000000000000000000000000000011016140110161400000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        ParentFont = False
        ParentBiDiMode = False
        OnClick = miLegendasClick
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 0
        Width = 846
        Height = 337
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 9
        DefaultRowHeight = 19
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        PopupMenu = pmOpcoesGrid
        TabOrder = 0
        OnClick = sgItensClick
        OnDblClick = sgItensDblClick
        OnDrawCell = sgItensDrawCell
        OnKeyDown = sgPesquisaKeyDown
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Sel.'
          'Leg.'
          'Produto'
          'Nome'
          'Marca'
          'Est. dispon'#237'vel'
          'Unidade'
          'Pre'#231'o'
          'Pre'#231'o promo'#231#227'o')
        OnGetCellColor = sgItensGetCellColor
        OnGetCellPicture = sgItensGetCellPicture
        Grid3D = False
        RealColCount = 15
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          25
          81
          52
          247
          121
          93
          59
          69
          101)
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Informa'#231#245'es extras'
      ImageIndex = 1
      object lb2: TLabel
        Left = 0
        Top = 0
        Width = 128
        Height = 14
        Caption = 'Refer'#234'ncias do produto'
      end
      inline FrImagemProduto: TFrImagem
        Left = 423
        Top = 3
        Width = 285
        Height = 287
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 423
        ExplicitTop = 3
        ExplicitWidth = 285
        ExplicitHeight = 287
        inherited pnImagem: TPanel
          Width = 285
          Height = 287
          ExplicitWidth = 285
          ExplicitHeight = 287
          inherited imFoto: TImage
            Width = 281
            Height = 283
            ExplicitWidth = 156
            ExplicitHeight = 156
          end
        end
        inherited pmOpcoes: TPopupMenu
          Top = 16
          inherited miCarregarfoto: TMenuItem
            Visible = False
          end
          inherited miApagarfoto: TMenuItem
            Visible = False
          end
          object miProximafoto: TMenuItem
            Caption = 'Pr'#243'xima'
            OnClick = miProximafotoClick
          end
          object miAnterior: TMenuItem
            Caption = 'Anterior'
            OnClick = miAnteriorClick
          end
        end
      end
      object eCaracteristicas: TMemo
        Left = 0
        Top = 14
        Width = 421
        Height = 276
        ReadOnly = True
        TabOrder = 1
      end
    end
  end
  inline FrCondicaoPagamento: TFrCondicoesPagamento
    Left = 459
    Top = 35
    Width = 358
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 459
    ExplicitTop = 35
    ExplicitWidth = 358
    ExplicitHeight = 40
    inherited CkAspas: TCheckBox [0]
    end
    inherited CkFiltroDuplo: TCheckBox [1]
    end
    inherited CkPesquisaNumerica: TCheckBox [2]
    end
    inherited CkMultiSelecao: TCheckBox [3]
    end
    inherited PnTitulos: TPanel [4]
      Width = 358
      ExplicitWidth = 358
      inherited lbNomePesquisa: TLabel
        Width = 174
        Caption = 'Condi'#231#227'o de pagamento padr'#227'o'
        ExplicitWidth = 174
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 253
        ExplicitLeft = 253
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel [5]
      Left = 333
      Height = 24
      ExplicitLeft = 333
      ExplicitHeight = 24
      inherited sbPesquisa: TSpeedButton
        Left = -2
        Top = 2
        Visible = False
        ExplicitLeft = -2
        ExplicitTop = 2
      end
    end
    inherited ckSomenteAtivos: TCheckBox [6]
      Checked = True
      State = cbChecked
    end
    inherited sgPesquisa: TGridLuka [7]
      Width = 331
      Height = 23
      Align = alNone
      ExplicitWidth = 331
      ExplicitHeight = 23
    end
  end
  object cbOpcoesPesquisa: TComboBoxLuka
    Tag = 999
    Left = 4
    Top = 14
    Width = 199
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clSkyBlue
    Ctl3D = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 3
    OnChange = cbOpcoesPesquisaChange
    OnKeyDown = ProximoCampo
    AsInt = 0
  end
  object eProduto: TEditLuka
    Tag = 999
    Left = 4
    Top = 52
    Width = 272
    Height = 22
    Anchors = [akLeft, akTop, akRight]
    CharCase = ecUpperCase
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 4
    OnKeyDown = eProdutoKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eMarca: TEditLuka
    Tag = 999
    Left = 223
    Top = 52
    Width = 225
    Height = 22
    Anchors = [akLeft, akTop, akRight]
    CharCase = ecUpperCase
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 5
    OnKeyDown = eMarcaKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ckSomenteProdutosPromocao: TCheckBoxLuka
    Left = 568
    Top = 8
    Width = 278
    Height = 17
    Caption = 'Somente produtos com promo'#231#227'o ativa'
    TabOrder = 6
    CheckedStr = 'N'
  end
  object pmOpcoesGrid: TPopupMenu
    Left = 682
    Top = 160
  end
end
