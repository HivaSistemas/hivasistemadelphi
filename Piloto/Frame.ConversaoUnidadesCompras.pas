unit Frame.ConversaoUnidadesCompras;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Frame.HerancaInsercaoExclusao, _Biblioteca, _Sessao,
  Vcl.Buttons, SpeedButtonLuka, Vcl.ExtCtrls, Vcl.Grids, GridLuka, Buscar.UnidadeMultiplo, System.Math,
  _RecordsCadastros, Vcl.StdCtrls, StaticTextLuka, Vcl.Menus;

type
  TFrameConversaoUnidadesCompra = class(TFrameHerancaInsercaoExclusao)
    procedure sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    procedure SetMultiplos(pMultiplos: TArray<RecProdutoMultiploCompra>);
    function getMultiplos: TArray<RecProdutoMultiploCompra>;
  protected
    procedure Inserir; override;
  published
    property Multiplos: TArray<RecProdutoMultiploCompra> read getMultiplos write SetMultiplos;
  end;

implementation

{$R *.dfm}

{ TFrameConversaoUnidadesCompra }

const
  coUnd      = 0;
  coMultiplo = 1;
  coQtdeEmbalagem = 2;

function TFrameConversaoUnidadesCompra.getMultiplos: TArray<RecProdutoMultiploCompra>;
var
  i: Integer;
begin
  Result := nil;

  if EstaVazio then
    Exit;

  SetLength(Result, sgValores.RowCount - 1);
  for i := sgValores.FixedRows to sgValores.RowCount - 1 do begin
    Result[i - 1].UnidadeId := sgValores.Cells[coUnd, i];
    Result[i - 1].Multiplo  := SFormatDouble(sgValores.Cells[coMultiplo, i]);
    Result[i - 1].QuantidadeEmbalagem := SFormatDouble(sgValores.Cells[coQtdeEmbalagem, i]);
  end;
end;

procedure TFrameConversaoUnidadesCompra.Inserir;
var
  vLinha: Integer;
  vRet: TRetornoTelaFinalizar<TUnidadeMultiplo>;
begin
  inherited;

  vRet := Buscar.UnidadeMultiplo.Buscar();
  if vRet.RetTela <> trOk then begin
    Self.SetFocus;
    Exit;
  end;

  if sgValores.Cells[coUnd, 1] = '' then
    vLinha := 1
  else begin
    vLinha := sgValores.Localizar([coUnd, coMultiplo, coQtdeEmbalagem], [vRet.Dados.Unidade, NFormat(vRet.Dados.Multiplo, 4), NFormat(vRet.Dados.QuantidadeEmbalagem, 4)]);
    if vLinha > 0 then begin
      sgValores.Row := vLinha;
      Exit;
    end;

    vLinha := sgValores.RowCount;
  end;

  sgValores.Cells[coUnd, vLinha]      := vRet.Dados.Unidade;
  sgValores.Cells[coMultiplo, vLinha] := NFormat(vRet.Dados.Multiplo, 4);
  sgValores.Cells[coQtdeEmbalagem, vLinha] := NFormat(vRet.Dados.QuantidadeEmbalagem, 4);

  sgValores.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);
  sgValores.Row := vLinha;
  Self.SetFocus;
end;

procedure TFrameConversaoUnidadesCompra.SetMultiplos(pMultiplos: TArray<RecProdutoMultiploCompra>);
var
  i: Integer;
begin
  for i := Low(pMultiplos) to High(pMultiplos) do begin
    sgValores.Cells[coUnd, i + 1]      := pMultiplos[i].UnidadeId;
    sgValores.Cells[coMultiplo, i + 1] := NFormat(pMultiplos[i].Multiplo, 4);
    sgValores.Cells[coQtdeEmbalagem, i + 1] := NFormat(pMultiplos[i].QuantidadeEmbalagem, 4);
  end;
  sgValores.RowCount := IfThen(Length(pMultiplos) > 1, High(pMultiplos) + 2, 2);
end;

procedure TFrameConversaoUnidadesCompra.sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = coUnd then
    vAlinhamento := taCenter
  else
    vAlinhamento := taRightJustify;

  sgValores.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
