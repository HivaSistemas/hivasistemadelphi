unit Informacoes.EstoqueProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.ExtCtrls, _Sessao, _EstoquesDivisao,
  Vcl.Grids, GridLuka, Vcl.StdCtrls, StaticTextLuka, Vcl.Buttons, _Biblioteca, _Estoques,
  _RecordsEstoques;

type
  TFormInformacoesEstoqueProduto = class(TFormHerancaFinalizar)
    StaticTextLuka2: TStaticTextLuka;
    sgEmpresas: TGridLuka;
    spSeparador: TSplitter;
    StaticTextLuka1: TStaticTextLuka;
    sgLotes: TGridLuka;
    procedure sgEmpresasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgLotesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEmpresasClick(Sender: TObject);
  private
    FItens: TArray<RecEstoquesDivisao>;
  end;

procedure Informar(pProdutoId: Integer);

implementation

{$R *.dfm}

const
  ceEmpresaId  = 0;
  ceNome       = 1;
  ceFisico     = 2;
  ceReservado  = 3;
  ceBloqueado  = 4;
  ceDisponivel = 5;
  ceCompras    = 6;
  ceEntradas   = 7;

  cdLocal      = 0;
  cdLote       = 1;
  cdDataFab    = 2;
  cdDataVencto = 3;
  cdFisico     = 4;
  cdReservado  = 5;
  cdBloqueado  = 6;
  cdDisponivel = 7;

procedure Informar(pProdutoId: Integer);
var
  i: Integer;
  vEstoques: TArray<RecEstoque>;
  vForm: TFormInformacoesEstoqueProduto;
begin
  if pProdutoId = 0 then
    Exit;

  vEstoques := _Estoques.BuscarEstoque(Sessao.getConexaoBanco, 1, [pProdutoId]);

  vForm := TFormInformacoesEstoqueProduto.Create(nil);
  vForm.FItens := _EstoquesDivisao.BuscarEstoque(Sessao.getConexaoBanco, 0, [pProdutoId]);

  for i := Low(vEstoques) to High(vEstoques) do begin
    vForm.sgEmpresas.Cells[ceEmpresaId, i + 1]  := _Biblioteca.NFormat(vEstoques[i].empresa_id);
    vForm.sgEmpresas.Cells[ceNome, i + 1]       := vEstoques[i].NomeEmpresa;
    vForm.sgEmpresas.Cells[ceFisico, i + 1]     := _Biblioteca.NFormatNEstoque(vEstoques[i].Fisico);
    vForm.sgEmpresas.Cells[ceReservado, i + 1]  := _Biblioteca.NFormatNEstoque(vEstoques[i].Reservado);
    vForm.sgEmpresas.Cells[ceBloqueado, i + 1]  := _Biblioteca.NFormatNEstoque(vEstoques[i].Bloqueado);
    vForm.sgEmpresas.Cells[ceDisponivel, i + 1] := _Biblioteca.NFormatNEstoque(vEstoques[i].Disponivel);
    vForm.sgEmpresas.Cells[ceCompras, i + 1]    := _Biblioteca.NFormatNEstoque(vEstoques[i].ComprasPendentes);
    vForm.sgEmpresas.Cells[ceEntradas, i + 1]   := _Biblioteca.NFormatNEstoque(0);
  end;
  vForm.sgEmpresas.SetLinhasGridPorTamanhoVetor( Length(vEstoques) );
  vForm.sgEmpresasClick(nil);

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormInformacoesEstoqueProduto.sgEmpresasClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vEmpresaId: Integer;
begin
  inherited;
  vLinha := 0;
  sgLotes.ClearGrid();
  vEmpresaId := SFormatInt( sgEmpresas.Cells[ceEmpresaId, sgEmpresas.Row] );
  for i := Low(FItens) to High(FItens) do begin
    if vEmpresaId <> FItens[i].EmpresaId then
      Continue;

    Inc(vLinha);

    sgLotes.Cells[cdLocal, vLinha]      := NFormat(FItens[i].LocalId) + ' - ' + FItens[i].NomeLocal;
    sgLotes.Cells[cdLote, vLinha]       := FItens[i].Lote;
    sgLotes.Cells[cdDataFab, vLinha]    := DFormatN(FItens[i].DataFabricacao);
    sgLotes.Cells[cdDataVencto, vLinha] := DFormatN(FItens[i].DataVencimento);
    sgLotes.Cells[cdFisico, vLinha]     := _Biblioteca.NFormatNEstoque(FItens[i].Fisico);
    sgLotes.Cells[cdReservado, vLinha]  := _Biblioteca.NFormatNEstoque(FItens[i].Reservado);
    sgLotes.Cells[cdBloqueado, vLinha]  := _Biblioteca.NFormatNEstoque(FItens[i].Bloqueado);
    sgLotes.Cells[cdDisponivel, vLinha] := _Biblioteca.NFormatNEstoque(FItens[i].Disponivel);
  end;
  sgLotes.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormInformacoesEstoqueProduto.sgEmpresasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    ceEmpresaId,
    ceFisico,
    ceReservado,
    ceBloqueado,
    ceDisponivel,
    ceCompras,
    ceEntradas]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgEmpresas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesEstoqueProduto.sgLotesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    cdFisico,
    cdReservado,
    cdBloqueado,
    cdDisponivel]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgLotes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
