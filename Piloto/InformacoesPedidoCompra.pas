unit InformacoesPedidoCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, FrameMultiplosCompra,
  Vcl.Grids, GridLuka, _FrameHenrancaPesquisas, FrameProdutos, _RecordsEspeciais,
  _FrameHerancaPrincipal, FrameDadosCobranca, Vcl.StdCtrls, StaticTextLuka,
  MemoAltis, Vcl.Mask, EditLukaData, Vcl.ComCtrls, EditLuka, Vcl.Buttons, _ComprasBaixas,
  Vcl.ExtCtrls, _Compras, _ComprasItens, _ComprasPrevisoes, _Biblioteca, _Sessao,
  FramePlanosFinanceiros, _RecordsFinanceiros, _ContasPagar, Informacoes.TituloPagar,
  Vcl.Menus;

type
  TFormInformacoesPedidoCompra = class(TFormHerancaFinalizar)
    lbl9: TLabel;
    eID: TEditLuka;
    pgcDados: TPageControl;
    tsPrincipais: TTabSheet;
    tsProdutos: TTabSheet;
    StaticTextLuka1: TStaticTextLuka;
    txtQuantidadeItensGrid: TStaticText;
    txtValorTotal: TStaticText;
    StaticTextLuka2: TStaticTextLuka;
    eFornecedor: TEditLuka;
    lbl10: TLabel;
    lbl27: TLabel;
    eEmpresa: TEditLuka;
    lbl11: TLabel;
    eComprador: TEditLuka;
    StaticTextLuka5: TStaticTextLuka;
    stStatus: TStaticText;
    pnl1: TPanel;
    stQuantidadeItensGrid: TStaticText;
    stValorTotal: TStaticText;
    StaticTextLuka6: TStaticTextLuka;
    StaticTextLuka7: TStaticTextLuka;
    lbl1: TLabel;
    lbl2: TLabel;
    lbl8: TLabel;
    eDataFaturamento: TEditLukaData;
    ePrevisaoEntrega: TEditLukaData;
    FrPlanoFinanceiro: TFrPlanosFinanceiros;
    meObservacoes: TMemoAltis;
    StaticTextLuka3: TStaticTextLuka;
    FrDadosCobranca: TFrDadosCobranca;
    StaticTextLuka4: TStaticTextLuka;
    sgItens: TGridLuka;
    tsBaixas: TTabSheet;
    tsFinanceiro: TTabSheet;
    StaticTextLuka8: TStaticTextLuka;
    sgFinanceiros: TGridLuka;
    sgBaixas: TGridLuka;
    pmOpcoesBaixas: TPopupMenu;
    miCancelarBaixaFocada: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure tsFinanceiroShow(Sender: TObject);
    procedure sgFinanceirosDblClick(Sender: TObject);
    procedure sgBaixasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miCancelarBaixaFocadaClick(Sender: TObject);
    procedure tsBaixasShow(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgFinanceirosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    FCompraId: Integer;
  public
    { Public declarations }
  end;

procedure Informar(pCompraId: Integer);

implementation

{$R *.dfm}

const
  ciProdutoId        = 0;
  ciNomeProduto      = 1;
  ciMarca            = 2;
  ciUnidade          = 3;
  ciQuantidade       = 4;
  ciPrecoUnitario    = 5;
  ciValorTotal       = 6;
  ciValorDesconto    = 7;
  civalorOutDespesas = 8;
  ciValorLiquido     = 9;
  ciEntregues        = 10;
  ciBaixados         = 11;
  ciCancelados       = 12;
  ciSaldo            = 13;

  (* Grid de baixas de compras *)
  cbBaixaId       = 0;
  cbDataHoraBaixa = 1;
  cbUsuarioBaixa  = 2;

  (* Grid financeiros *)
  cpPagarId        = 0;
  cpTipoCobranca   = 1;
  cpValor          = 2;
  cpStatus         = 3;
  cpDataVencimento = 4;

procedure Informar(pCompraId: Integer);
var
  i: Integer;
  vLinha: Integer;
  vTotal: Double;
  vForm: TFormInformacoesPedidoCompra;
  vCompra: TArray<RecCompras>;
  vItens: TArray<RecComprasItens>;
begin
  if pCompraId = 0 then
    Exit;

  vCompra := _Compras.BuscarCompras( Sessao.getConexaoBanco, 0, [pCompraId] );
  if vCompra = nil then begin
    _Biblioteca.Exclamar('Pedido de compra n�o encontrado!');
    Exit;
  end;

  vForm := TFormInformacoesPedidoCompra.Create(nil);
  vForm.FCompraId := pCompraId;

  vItens := _ComprasItens.BuscarComprasItens( Sessao.getConexaoBanco, 0, [pCompraId] );

  vForm.eId.AsInt := vCompra[0].CompraId;
  vForm.eEmpresa.SetInformacao(vCompra[0].EmpresaId, vCompra[0].NomeEmpresa);
  vForm.eFornecedor.SetInformacao( vCompra[0].FornecedorId, vCompra[0].NomeFornecedor );
  vForm.eComprador.SetInformacao( vCompra[0].CompradorId, vCompra[0].NomeComprador );
  vForm.stStatus.Caption  := vCompra[0].StatusAnalitico;

  vForm.eDataFaturamento.AsData := vCompra[0].DataFaturamento;
  vForm.ePrevisaoEntrega.AsData := vCompra[0].PrevisaoEntrega;
  vForm.meObservacoes.Lines.Text := vCompra[0].Observacoes;
  vForm.FrPlanoFinanceiro.InserirDadoPorChave( vCompra[0].PlanoFinanceiroId, False );

  vForm.FrDadosCobranca.Titulos := _ComprasPrevisoes.BuscarComprasPrevisoes(Sessao.getConexaoBanco, 0, [vForm.eId.AsInt]);
  vForm.FrDadosCobranca.SomenteLeitura(True);

  vLinha := 0;
  for i := Low(vItens) to High(vItens) do begin

    vLinha := (i +1);
    vForm.sgItens.Cells[ciProdutoId, vLinha]   := NFormat(vItens[i].ProdutoId);
    vForm.sgItens.Cells[ciNomeProduto, vLinha] := vItens[i].NomeProduto;
    vForm.sgItens.Cells[ciMarca, vLinha]       := NFormat(vItens[i].MarcaId) + ' - ' + vItens[i].NomeMarca;
    vForm.sgItens.Cells[ciUnidade, vLinha]     := vItens[i].UnidadeCompraId;
    vForm.sgItens.Cells[ciQuantidade, vLinha]  := NFormat(vItens[i].Quantidade);
    vForm.sgItens.Cells[ciPrecoUnitario, vLinha] := NFormat(vItens[i].PrecoUnitario);
    vForm.sgItens.Cells[ciValorTotal, vLinha]    := NFormat(vItens[i].ValorTotal);
    vForm.sgItens.Cells[ciValorDesconto, vLinha] := NFormat(vItens[i].ValorDesconto);
    vForm.sgItens.Cells[civalorOutDespesas, vLinha] := NFormat(vItens[i].ValorOutrasDespesas);
    vForm.sgItens.Cells[ciValorLiquido, vLinha]     := NFormat(vItens[i].ValorLiquido);
    vForm.sgItens.Cells[ciEntregues, vLinha]  := NFormat(vItens[i].Entregues);
    vForm.sgItens.Cells[ciBaixados, vLinha]   := NFormat(vItens[i].Baixados);
    vForm.sgItens.Cells[ciCancelados, vLinha] := NFormat(vItens[i].Cancelados);
    vForm.sgItens.Cells[ciSaldo, vLinha] := NFormat(vItens[i].Saldo);
  end;

  vForm.stQuantidadeItensGrid.Caption := NFormat(vLinha);

  vTotal := 0;

  for i := 1 to vForm.sgItens.RowCount -1 do
    vTotal := vTotal + SFormatDouble(vForm.sgItens.Cells[ciValorLiquido, i]);

  vForm.stValorTotal.Caption := NFormatN(vTotal);

  vForm.ShowModal;

  vForm.Free;
end;


procedure TFormInformacoesPedidoCompra.FormCreate(Sender: TObject);
begin
  inherited;
  ReadOnlyTodosObjetos(True);
  pgcDados.ActivePageIndex := 0;
end;

procedure TFormInformacoesPedidoCompra.miCancelarBaixaFocadaClick(Sender: TObject);
var
  vBaixaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vBaixaId := SFormatInt(sgBaixas.Cells[cbBaixaId, sgBaixas.Row]);
  if vBaixaId = 0 then
    Exit;

  vRetBanco := _ComprasBaixas.CancelarCompraBaixa(Sessao.getConexaoBanco, vBaixaId);
  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RotinaSucesso;
  tsBaixasShow(Sender);
end;

procedure TFormInformacoesPedidoCompra.sgBaixasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol = cbBaixaId then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgBaixas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesPedidoCompra.sgFinanceirosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloPagar.Informar( SFormatInt(sgFinanceiros.Cells[cpPagarId, sgFinanceiros.Row]) );
end;

procedure TFormInformacoesPedidoCompra.sgFinanceirosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[cpPagarId, cpValor] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgFinanceiros.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesPedidoCompra.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[ciNomeProduto, ciMarca] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesPedidoCompra.tsBaixasShow(Sender: TObject);
var
  i: Integer;
  vBaixas: TArray<RecComprasBaixas>;
begin
  inherited;
  sgBaixas.ClearGrid();

  vBaixas := _ComprasBaixas.BuscarComprasBaixas(Sessao.getConexaoBanco, 0, [FCompraId]);
  for i := Low(vBaixas) to High(vBaixas) do begin
    sgBaixas.Cells[cbBaixaId, i + 1]       := NFormat(vBaixas[i].BaixaId);
    sgBaixas.Cells[cbDataHoraBaixa, i + 1] := DHFormat(vBaixas[i].DataHoraBaixa);
    sgBaixas.Cells[cbusuarioBaixa, i + 1]  := getInformacao(vBaixas[i].UsuarioBaixaId, vBaixas[i].NomeUsuarioBaixa);
  end;
  sgBaixas.SetLinhasGridPorTamanhoVetor( Length(vBaixas) );
end;

procedure TFormInformacoesPedidoCompra.tsFinanceiroShow(Sender: TObject);
var
  i: Integer;
  vFinanceiros: TArray<RecContaPagar>;
begin
  inherited;
  sgFinanceiros.ClearGrid();

  vFinanceiros := _ContasPagar.BuscarContasPagar(Sessao.getConexaoBanco, 6, [FcompraId]);
  for i := Low(vFinanceiros) to High(vFinanceiros) do begin
    sgFinanceiros.Cells[cpPagarId, i + 1]        := NFormat(vFinanceiros[i].PagarId);
    sgFinanceiros.Cells[cpTipoCobranca, i + 1]   := NFormat(vFinanceiros[i].cobranca_id) + ' - ' + vFinanceiros[i].nome_tipo_cobranca;
    sgFinanceiros.Cells[cpValor, i + 1]          := NFormat(vFinanceiros[i].ValorDocumento);
    sgFinanceiros.Cells[cpStatus, i + 1]         := vFinanceiros[i].StatusAnalitico;
    sgFinanceiros.Cells[cpDataVencimento, i + 1] := DFormat(vFinanceiros[i].data_vencimento);
  end;
  sgFinanceiros.SetLinhasGridPorTamanhoVetor( Length(vFinanceiros) );
end;

end.
