unit VidaCliente;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.ComCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameClientes, Vcl.Grids, GridLuka, _RelacaoOrcamentosVendas, _RecordsRelatorios, _Sessao, _Biblioteca,
  _RelacaoDevolucoes, _RelacaoVendasProdutos, _RelacaoNotasFiscais, _NotasFiscais, _RelacaoContasReceber,
  _RelacaoEntregasRealizadas, _RelacaoEntregasPendentes, Informacoes.Orcamento, Informacoes.Devolucao, Informacoes.NotaFiscal,
  Informacoes.TituloReceber, Informacoes.Entrega, Informacoes.Retiradas,
  PageControlAltis, Vcl.StdCtrls, EditLuka, Vcl.Mask, EditLukaData,
  EditTelefoneLuka, _Clientes, FrameCondicoesPagamento, _RecordsCadastros, _ClientesCondicPagtoRestrit;

type
  TFormVidaCliente = class(TFormHerancaFinalizar)
    tsOrcamentos: TTabSheet;
    tsPedidos: TTabSheet;
    tsNotasFiscais: TTabSheet;
    tsFinanceiros: TTabSheet;
    tsAbertos: TTabSheet;
    tsBaixados: TTabSheet;
    tsEntregas: TTabSheet;
    tsPendentes: TTabSheet;
    tsRealizadas: TTabSheet;
    sgNotasFiscais: TGridLuka;
    sgTitulosAbertos: TGridLuka;
    sgTitulosBaixados: TGridLuka;
    sgEntregasPendentes: TGridLuka;
    sgEntregasRealizadas: TGridLuka;
    sgOrcamentos: TGridLuka;
    pcDados: TPageControlAltis;
    pcPedidos: TPageControlAltis;
    tsDevolucoes: TTabSheet;
    sgDevolucoes: TGridLuka;
    tsMixProdutos: TTabSheet;
    sgMixProdutos: TGridLuka;
    tsVendas: TTabSheet;
    sgPedidos: TGridLuka;
    pcFinanceiros: TPageControlAltis;
    pcEntregas: TPageControlAltis;
    tsGerais: TTabSheet;
    eDataCadastro: TEditLukaData;
    Label2: TLabel;
    lbDataNascimento: TLabel;
    eDataNascimento: TEditLukaData;
    Label1: TLabel;
    eDataUltimaVenda: TEditLukaData;
    eValorUltimaEntrega: TEditLuka;
    lb23: TLabel;
    eQuantidadeDiasSemComprar: TEditLuka;
    lb8: TLabel;
    Label6: TLabel;
    eQuantidadeOrcamentosEmAberto: TEditLuka;
    Label3: TLabel;
    eQuantidadePedidos: TEditLuka;
    Label4: TLabel;
    Label8: TLabel;
    eQuantidadeDevolucoes: TEditLuka;
    Label10: TLabel;
    eQuantidadeRetiradas: TEditLuka;
    eQuantidadeEntregas: TEditLuka;
    Label11: TLabel;
    eQuantidadeEntregasRetornadas: TEditLuka;
    Label12: TLabel;
    eQuantidadeFinanceirosAbertos: TEditLuka;
    Label13: TLabel;
    Label14: TLabel;
    eQuantidadeFinanceirosBaixados: TEditLuka;
    stSPC: TStaticText;
    StaticText1: TStaticText;
    eTelefone: TEditTelefoneLuka;
    lb11: TLabel;
    eCelular: TEditTelefoneLuka;
    lb12: TLabel;
    eDataUltimaEntrega: TEditLukaData;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    eLimiteCredito: TEditLuka;
    lb15: TLabel;
    eValorUtilizadoLimiteCredito: TEditLuka;
    Label15: TLabel;
    eValorRestanteLimiteCredito: TEditLuka;
    Label16: TLabel;
    eDataAprovacaoLimite: TEditLukaData;
    Label18: TLabel;
    eDataValidadeLimite: TEditLukaData;
    Label19: TLabel;
    Label20: TLabel;
    StaticText4: TStaticText;
    eDataUltimaRetirada: TEditLukaData;
    Label21: TLabel;
    eValorUltimaRetirada: TEditLuka;
    Label5: TLabel;
    eValorFinanceirosEmAberto: TEditLuka;
    Label7: TLabel;
    eValorFinanceirosBaixados: TEditLuka;
    frCliente: TFrClientes;
    FrCondicoesPagtoRestritas: TFrCondicoesPagamento;

    procedure FormCreate(Sender: TObject);
    procedure sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure sgOrcamentosDblClick(Sender: TObject);
    procedure sgPedidosDblClick(Sender: TObject);
    procedure sgDevolucoesDblClick(Sender: TObject);
    procedure sgNotasFiscaisDblClick(Sender: TObject);
    procedure sgTitulosAbertosDblClick(Sender: TObject);
    procedure sgTitulosBaixadosDblClick(Sender: TObject);
    procedure sgEntregasPendentesDblClick(Sender: TObject);
    procedure sgEntregasRealizadasDblClick(Sender: TObject);
    procedure sgPedidosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgDevolucoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgMixProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgNotasFiscaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTitulosAbertosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTitulosBaixadosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEntregasPendentesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEntregasRealizadasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEntregasPendentesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgEntregasRealizadasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgTitulosAbertosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgTitulosBaixadosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgNotasFiscaisGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgPedidosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgDevolucoesGetCellColor(Sender: TObject; ARow, ACol: Integer;  AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgOrcamentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgMixProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure tsFinanceirosShow(Sender: TObject);
    procedure tsEntregasShow(Sender: TObject);
    procedure tsPedidosShow(Sender: TObject);
    procedure tsVendasShow(Sender: TObject);
    procedure tsDevolucoesShow(Sender: TObject);
    procedure tsMixProdutosShow(Sender: TObject);
    procedure tsNotasFiscaisShow(Sender: TObject);
    procedure tsAbertosShow(Sender: TObject);
    procedure tsBaixadosShow(Sender: TObject);
    procedure tsPendentesShow(Sender: TObject);
    procedure tsRealizadasShow(Sender: TObject);
  private
    (* Procedimentos dos frames *)
    procedure FrClienteOnAposPesquisar(Sender: TObject);
    procedure FrClienteOnAposDeletar(Sender: TObject);
  protected
    procedure Modo(pHabilitar: Boolean);

  public
    { Public declarations }
  end;

procedure AbrirVidaCliente(pClienteId: Integer);

implementation

{$R *.dfm}

const
  coOrcamentoId 	      = 0;
  coEmpresa	            = 1;
  coStatus              = 2;
  coDataCadastro	      = 3;
  coVendedor	          = 4;
  coCondicaoPagamento   = 5;
  coValorTotalProdutos  = 6;
  coValorDesconto       = 7;
  coValorOutrasDespesas = 8;
  coValorTotal	        = 9;

  cvPedidoId           = 0;
  cvEmpresa            = 1;
  cvDataEmissao        = 2;
  cvVendedor           = 3;
  cvCondicaoPagamento  = 4;
  cvValorTotal         = 5;
  cvValorProdutos      = 6;
  cvValorDesconto      = 7;
  cvValorOutrasDespesa = 8;

  cdDevolucaoId       = 0;
  cdEmpresa           = 1;
  cdPedidoId	        = 2;
  cdValorBruto        = 3;
  cdValorOutDesp      = 4;
  cdValorDesconto     = 5;
  cdValorFrete        = 6;
  cdValorSt           = 7;
  cdValorIPI          = 8;
  cdValorLiquido      = 9;
  cdDataHoraDevolucao = 10;
  cdUsuarioDevolucao  = 11;

  cpProdutoId        = 0;
  cpNomeProduto      = 1;
  cpMarca            = 2;
  cpQuantidade       = 3;
  cpUnidade          = 4;
  cpPrecoMedio       = 5;
  cpValorTotal       = 6;
  cpPercParticipacao = 7;
  cdPercentualMargem = 8;

  ntNotaFiscalId           = 0;
  ntDataCadastro           = 1;
  ntEmpresa                = 2;
  ntTipoNota               = 3;
  ntTipoMovimentoAnalitico = 4;
  ntNumeroNota             = 5;
  ntValorTotalNota         = 6;
  ntStatus                 = 7;
  ntTipoMovimentoSintetico = 8;

  faReceberId       = 0;
  faDocumento       = 1;
  faParcela         = 2;
  faValor           = 3;
  faMulta           = 4;
  faJuros           = 5;
  faRetencao        = 6;
  faValorLiquido    = 7;
  faDataCadastro    = 8;
  faDataVencimento  = 9;
  faDiasAtraso      = 10;
  faEmpresa         = 11;
  faStatusAnalitico = 12;
  faTipoCobranca    = 13;
  faPortador        = 14;
  faStatusSintetico = 15;
  faFormaPagamento  = 16;
  faNoCaixa         = 17;

  fbReceberId       = 0;
  fbDocumento       = 1;
  fbParcela         = 2;
  fbValor           = 3;
  fbMulta           = 4;
  fbJuros           = 5;
  fbRetencao        = 6;
  fbValorLiquido    = 7;
  fbDataCadastro    = 8;
  fbDataVencimento  = 9;
  fbDiasAtraso      = 10;
  fbEmpresa         = 11;
  fbStatusAnalitico = 12;
  fbTipoCobranca    = 13;
  fbPortador        = 14;
  fbStatusSintetico = 15;
  fbFormaPagamento  = 16;
  fbNoCaixa         = 17;

  epPedidoId        = 0;
  epTipoMovimento   = 1;
  epDataEmissao     = 2;
  epEmpresa         = 3;
  epLocal           = 4;
  epPrevisaoEntrega = 5;

  ebEntregaId       = 0;
  ebTipoMovimento   = 1;
  ebPedidoID        = 2;
  ebDataEmissao     = 3;
  ebDataEntrega     = 4;
  ebEmpresa         = 5;
  ebLocal           = 6;
  ebRecebedor       = 7;

procedure AbrirVidaCliente(pClienteId: Integer);
var
  vForm: TFormVidaCliente;
begin
  if pClienteId = 0 then
    Exit;

  if not Sessao.AutorizadoTela('tformvidacliente') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  vForm := TFormVidaCliente.Create(Application);

  vForm.frCliente.InserirDadoPorChave(pClienteId, True);

  vForm.abrirModalnformacoes;
end;

procedure TFormVidaCliente.FormCreate(Sender: TObject);
begin
  inherited;
  FrCliente.OnAposDeletar   := FrClienteOnAposDeletar;
  FrCliente.OnAposPesquisar := FrClienteOnAposPesquisar;

  tsFinanceiros.TabVisible     := Sessao.AutorizadoRotina('tformrelacaocontasreceber');
  tsNotasFiscais.TabVisible    := Sessao.AutorizadoRotina('tformrelacaonotasfiscais');
end;

procedure TFormVidaCliente.FormShow(Sender: TObject);
begin
  inherited;
  pcDados.ActivePageIndex := 0;

  if frCliente.EstaVazio then
    SetarFoco(frCliente)
  else
    SetarFoco(eDataCadastro);

  pcDados.Repaint;
end;

procedure TFormVidaCliente.FrClienteOnAposDeletar(Sender: TObject);
begin
  Modo(False);
end;

procedure TFormVidaCliente.FrClienteOnAposPesquisar(Sender: TObject);
var
  i: Integer;
  vComando: string;
  vOrcamentos: TArray<RecOrcamentosVendas>;
  vDadosGeraisCliente: TArray<RecDadosVidaCliente>;
  vCondicoesPagtoRestritasIds: TArray<Integer>;
  vCondicoesPagtoRestritas: TArray<RecClientesCondicPagtoRestrit>;
begin
  Modo(True);
  vComando := '';
  vCondicoesPagtoRestritasIds := nil;
  pcDados.ActivePageIndex := 0;

  if FrCliente.EstaVazio then
    Exit;

  vDadosGeraisCliente := _Clientes.BuscarDadosVidaCliente(Sessao.getConexaoBanco, FrCliente.getCliente.cadastro_id);

  if not (vDadosGeraisCliente = nil) then begin
    eDataNascimento.AsData                := vDadosGeraisCliente[0].DataNascimento;
    eTelefone.Text                        := vDadosGeraisCliente[0].TelefonePrincipal;
    eCelular.Text                         := vDadosGeraisCliente[0].TelefoneCelular;
    eDataCadastro.AsData                  := vDadosGeraisCliente[0].DataCadastro;
    eLimiteCredito.AsDouble               := vDadosGeraisCliente[0].LimiteCredito;
    eDataValidadeLimite.AsData            := vDadosGeraisCliente[0].DataValidadeLimiteCredito;
    eDataAprovacaoLimite.AsData           := vDadosGeraisCliente[0].DataAprovacaoLimiteCredito;
    eValorUtilizadoLimiteCredito.AsDouble := vDadosGeraisCliente[0].ValorUtilizadoLimiteCredito;
    eValorRestanteLimiteCredito.AsDouble  := vDadosGeraisCliente[0].SaldoRestante;
    eValorFinanceirosEmAberto.AsDouble    := vDadosGeraisCliente[0].ValorFinanceiroAberto;
    eQuantidadeFinanceirosAbertos.AsInt   := vDadosGeraisCliente[0].QtdeFinanceirosAbertos;
    eValorFinanceirosBaixados.AsDouble    := vDadosGeraisCliente[0].ValorFinanceiroBaixado;
    eQuantidadeFinanceirosBaixados.AsInt  := vDadosGeraisCliente[0].QtdeFinanceirosBaixados;
    eQuantidadeOrcamentosEmAberto.AsInt   := vDadosGeraisCliente[0].QtdeOrcamentosAbertos;
    eQuantidadePedidos.AsInt              := vDadosGeraisCliente[0].QtdePedidos;
    eQuantidadeDevolucoes.AsInt           := vDadosGeraisCliente[0].QtdeDevolucoes;
    eDataUltimaVenda.AsData               := vDadosGeraisCliente[0].DataUltimaVenda;
    eQuantidadeDiasSemComprar.AsInt       := vDadosGeraisCliente[0].QtdeDiasSemComprar;
    eDataUltimaEntrega.AsDataHora         := vDadosGeraisCliente[0].DataUltimaEntrega;
    eValorUltimaEntrega.AsDouble          := vDadosGeraisCliente[0].ValorUltimaEntrega;
    eQuantidadeEntregas.AsInt             := vDadosGeraisCliente[0].QtdeEntregas;
    eQuantidadeEntregasRetornadas.AsInt   := vDadosGeraisCliente[0].QtdeEntregasRetornadas;
    eDataUltimaRetirada.AsData            := vDadosGeraisCliente[0].DataUltimaRetirada;
    eValorUltimaRetirada.AsDouble         := vDadosGeraisCliente[0].ValorUltimaRetirada;
    eQuantidadeRetiradas.AsInt            := vDadosGeraisCliente[0].QtdeRetiradas;

    vCondicoesPagtoRestritas := _ClientesCondicPagtoRestrit.BuscarClientesCondicPagtoRestrit(Sessao.getConexaoBanco, 0, [FrCliente.getCliente.cadastro_id]);

    if vCondicoesPagtoRestritas <> nil then begin
      SetLength(vCondicoesPagtoRestritasIds, Length(vCondicoesPagtoRestritas));
      for i := Low(vCondicoesPagtoRestritas) to High(vCondicoesPagtoRestritas) do
        vCondicoesPagtoRestritasIds[i] := vCondicoesPagtoRestritas[i].CondicaoId;
    end;

    if not (vCondicoesPagtoRestritasIds = nil) then
      FrCondicoesPagtoRestritas.InserirDadoPorChaveTodos(vCondicoesPagtoRestritasIds, False);
  end;

  _Biblioteca.WhereOuAnd(vComando, FrCliente.getSqlFiltros('CLIENTE_ID'));
  _Biblioteca.WhereOuAnd(vComando, 'STATUS in (''OE'', ''OB'') ');
  vComando := vComando + ' order by ORCAMENTO_ID ';

  vOrcamentos := _RelacaoOrcamentosVendas.BuscarOrcamentos(Sessao.getConexaoBanco, vComando);

  if not (vOrcamentos = nil) then begin

    for i := Low(vOrcamentos) to High(vOrcamentos) do begin

      sgOrcamentos.Cells[coOrcamentoId, i + 1]          := NFormat(vOrcamentos[i].OrcamentoId);
      sgOrcamentos.Cells[coEmpresa, i + 1]              := NFormat(vOrcamentos[i].EmpresaId) + ' - ' + vOrcamentos[i].NomeEmpresa;
      sgOrcamentos.Cells[coStatus, i + 1]               := vOrcamentos[i].StatusAnalitico;
      sgOrcamentos.Cells[coDataCadastro, i + 1]         := DFormat(vOrcamentos[i].DataCadastro);

      if vOrcamentos[i].VendedorId > 0 then
        sgOrcamentos.Cells[coVendedor, i + 1]           := NFormat(vOrcamentos[i].VendedorId) + ' - ' + vOrcamentos[i].NomeVendedor;

      sgOrcamentos.Cells[coCondicaoPagamento, i + 1]    := NFormat(vOrcamentos[i].CondicaoId) + ' - ' + vOrcamentos[i].NomeCondicaoPagamento;
      sgOrcamentos.Cells[coValorTotalProdutos  , i + 1] := NFormatN(vOrcamentos[i].ValorTotalProdutos);
      sgOrcamentos.Cells[coValorDesconto, i + 1]        := NFormatN(vOrcamentos[i].ValorDesconto);
      sgOrcamentos.Cells[coValorOutrasDespesas, i + 1]  := NFormatN(vOrcamentos[i].ValorOutrasDespesas);
      sgOrcamentos.Cells[coValorTotal, i + 1]           := NFormatN(vOrcamentos[i].ValorTotal);
    end;

    sgOrcamentos.SetLinhasGridPorTamanhoVetor( Length(vOrcamentos) );
  end;

  SetarFoco(eDataCadastro);
end;

procedure TFormVidaCliente.Modo(pHabilitar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    pcDados,
    pcPedidos,
    pcFinanceiros,
    pcEntregas,
    sgOrcamentos,
    sgPedidos,
    sgDevolucoes,
    sgMixProdutos,
    sgNotasFiscais,
    sgTitulosAbertos,
    sgTitulosBaixados,
    sgEntregasPendentes,
    sgEntregasRealizadas],
    pHabilitar
  );
end;

procedure TFormVidaCliente.sgDevolucoesDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Devolucao.Informar(SFormatInt(sgDevolucoes.Cells[cdDevolucaoId, sgDevolucoes.Row]));
end;

procedure TFormVidaCliente.sgDevolucoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [cdDevolucaoId , cdPedidoId, cdValorBruto, cdValorOutDesp, cdValorDesconto, cdValorFrete, cdValorSt, cdValorIPI, cdValorLiquido] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgDevolucoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormVidaCliente.sgDevolucoesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = cdValorBruto then
    AFont.Color := clBlue
  else if ACol = cdValorDesconto then
    AFont.Color := clRed
  else if ACol = cdValorOutDesp then
    AFont.Color := clPurple
  else if ACol = cdValorLiquido then
    AFont.Color := $000096DB;
end;

procedure TFormVidaCliente.sgEntregasPendentesDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar( SFormatInt(sgEntregasPendentes.Cells[epPedidoId, sgEntregasPendentes.Row]) );
end;

procedure TFormVidaCliente.sgEntregasPendentesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = epPedidoId then
    vAlinhamento := taRightJustify
  else if ACol = epTipoMovimento then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgEntregasPendentes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormVidaCliente.sgEntregasPendentesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = epTipoMovimento then begin
    AFont.Style := [fsBold];
    if sgEntregasPendentes.Cells[ACol, ARow] = 'Retirar' then
      AFont.Color := clBlue
    else if sgEntregasPendentes.Cells[ACol, ARow] = 'Entregar' then
      AFont.Color := $000096DB
    else
      AFont.Color := clGreen;
  end;
end;

procedure TFormVidaCliente.sgEntregasRealizadasDblClick(Sender: TObject);
begin
  inherited;
  if sgEntregasRealizadas.Cells[ebTipoMovimento, sgEntregasRealizadas.Row] = 'Entregar' then
    Informacoes.Entrega.Informar( SFormatInt(sgEntregasRealizadas.Cells[ebEntregaId, sgEntregasRealizadas.Row]) )
  else
    Informacoes.Retiradas.Informar( SFormatInt(sgEntregasRealizadas.Cells[ebEntregaId, sgEntregasRealizadas.Row]) );
end;

procedure TFormVidaCliente.sgEntregasRealizadasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [ebEntregaId , ebPedidoID] then
    vAlinhamento := taRightJustify
  else if ACol = ebTipoMovimento then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgEntregasRealizadas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;
procedure TFormVidaCliente.sgEntregasRealizadasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = ebTipoMovimento then begin
    AFont.Style := [fsBold];

    if sgEntregasRealizadas.Cells[ebTipoMovimento, ARow] = 'Retirar' then
      AFont.Color := clBlue
    else if sgEntregasRealizadas.Cells[ebTipoMovimento, ARow] = 'Entregar' then
      AFont.Color := $000096DB
    else
      AFont.Color := clGreen;
  end;
end;

procedure TFormVidaCliente.sgMixProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [cpProdutoId , cpQuantidade, cpPrecoMedio, cpValorTotal, cpPercParticipacao, cdPercentualMargem] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgMixProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormVidaCliente.sgMixProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = cpPrecoMedio then
    AFont.Color := clOlive
  else if ACol = cdPercentualMargem then
    AFont.Color := clBlue
  else if ACol = cpPercParticipacao then
    AFont.Color := clGreen
  else if ACol = cpValorTotal then
    AFont.Color := $000096DB
  else if ACol = cpUnidade then
    AFont.Color := clPurple;
end;

procedure TFormVidaCliente.sgNotasFiscaisDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.NotaFiscal.Informar(SFormatInt(sgNotasFiscais.Cells[ntNotaFiscalId, sgNotasFiscais.Row]));
end;

procedure TFormVidaCliente.sgNotasFiscaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [ntNotaFiscalId , ntValorTotalNota] then
    vAlinhamento := taRightJustify
  else if ACol in [ntTipoNota, ntTipoMovimentoAnalitico, ntStatus]  then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgNotasFiscais.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormVidaCliente.sgNotasFiscaisGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = ntTipoNota then begin
    AFont.Style := [fsBold];
    AFont.Color := _Biblioteca.IIfInt(sgNotasFiscais.Cells[ACol, ARow] = 'NF-e', clBlue, $000096DB);
  end
  else if ACol = ntTipoMovimentoAnalitico then begin
    AFont.Style := [fsBold];
    AFont.Color := _NotasFiscais.getCorTipoMovimento(sgNotasFiscais.Cells[ntTipoMovimentoSintetico, ARow]);
  end
  else if ACol = ntStatus then begin
    AFont.Style := [fsBold];
    if sgNotasFiscais.Cells[ACol, ARow] = 'Emitida' then
      AFont.Color := $000096DB
    else if sgNotasFiscais.Cells[ACol, ARow] = 'Cancelada' then
      AFont.Color := clRed
    else if sgNotasFiscais.Cells[ACol, ARow] = 'Denegada' then
      AFont.Color := clRed
    else
      AFont.Color := $000080FF;
  end;
end;
procedure TFormVidaCliente.sgOrcamentosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(SFormatInt(sgOrcamentos.Cells[coOrcamentoId, sgOrcamentos.Row]));
end;

procedure TFormVidaCliente.sgOrcamentosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [coOrcamentoID , coValorTotalProdutos, coValorDesconto, coValorOutrasDespesas, coValorTotal] then
    vAlinhamento := taRightJustify
  else if ACol = coStatus  then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgOrcamentos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormVidaCliente.sgOrcamentosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coCondicaoPagamento then
    AFont.Color := clNavy
  else if ACol = coValorTotal then
    AFont.Color := clBlue
  else if ACol = coValorDesconto then
    AFont.Color := clRed
  else if ACol = coValorOutrasDespesas then
    AFont.Color := clPurple
  else if ACol = coValorTotalProdutos then
    AFont.Color := $000096DB;

  if ACol = coStatus then begin
    AFont.Style := [fsBold];
    if sgOrcamentos.Cells[ACol, ARow] = 'Aberto' then
      AFont.Color := clGreen
    else if sgOrcamentos.Cells[ACol, ARow] = 'Bloqueado' then
      AFont.Color := clRed
    else
      AFont.Color := $000096DB;
  end;
end;

procedure TFormVidaCliente.sgPedidosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Orcamento.Informar(SFormatInt(sgOrcamentos.Cells[cvPedidoId, sgOrcamentos.Row]));
end;

procedure TFormVidaCliente.sgPedidosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [cvPedidoID , cvValorProdutos, cvValorDesconto, cvValorOutrasDespesa, cvValorTotal] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPedidos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormVidaCliente.sgPedidosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = cvCondicaoPagamento then
    AFont.Color := clNavy
  else if ACol = cvValorTotal then
    AFont.Color := clBlue
  else if ACol = cvValorDesconto then
    AFont.Color := clRed
  else if ACol = cvValorOutrasDespesa then
    AFont.Color := clPurple
  else if ACol = cvValorProdutos then
    AFont.Color := $000096DB;
end;

procedure TFormVidaCliente.sgTitulosAbertosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloReceber.Informar( SFormatInt(sgTitulosAbertos.Cells[faReceberId, sgTitulosAbertos.Row]) );
end;

procedure TFormVidaCliente.sgTitulosAbertosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [faReceberId , faParcela, faValor, faMulta, faJuros, faRetencao, faValorLiquido, faDiasAtraso] then
    vAlinhamento := taRightJustify
  else if ACol in [faStatusAnalitico, faTipoCobranca] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgTitulosAbertos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormVidaCliente.sgTitulosAbertosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = faStatusAnalitico then begin
    AFont.Style := [fsBold];
    if sgTitulosAbertos.Cells[faStatusSintetico, ARow] = 'A' then
      AFont.Color := $000096DB
    else
      AFont.Color := clBlue;
  end
  else if ACol in[faDiasAtraso, faRetencao] then
    AFont.Color := clRed
  else if ACol = fbValor then begin
    if sgTitulosAbertos.Cells[faFormaPagamento, ARow] = 'CREP' then
      AFont.Color := clRed
    else
      AFont.Color := $000096DB;
  end;

  if (sgTitulosAbertos.Cells[faStatusSintetico, ARow] = 'A') and (sgTitulosAbertos.Cells[faNoCaixa, ARow] = 'S') then
    ABrush.Color := $00AAAAFF;
end;

procedure TFormVidaCliente.sgTitulosBaixadosDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.TituloReceber.Informar( SFormatInt(sgTitulosBaixados.Cells[fbReceberId, sgTitulosBaixados.Row]) );
end;

procedure TFormVidaCliente.sgTitulosBaixadosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in [fbReceberId , fbParcela, fbValor, fbMulta, fbJuros, fbRetencao, fbValorLiquido, fbDiasAtraso] then
    vAlinhamento := taRightJustify
  else if ACol in [fbStatusAnalitico, fbTipoCobranca] then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgTitulosBaixados.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormVidaCliente.sgTitulosBaixadosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = fbStatusAnalitico then begin
    AFont.Style := [fsBold];
    if sgTitulosBaixados.Cells[fbStatusSintetico, ARow] = 'A' then
      AFont.Color := $000096DB
    else
      AFont.Color := clBlue;
  end
  else if ACol in[fbDiasAtraso, fbRetencao] then
    AFont.Color := clRed
  else if ACol = fbValor then begin
    if sgTitulosBaixados.Cells[fbFormaPagamento, ARow] = 'CREP' then
      AFont.Color := clRed
    else
      AFont.Color := $000096DB;
  end;

  if (sgTitulosBaixados.Cells[fbStatusSintetico, ARow] = 'A') and (sgTitulosBaixados.Cells[fbNoCaixa, ARow] = 'S') then
    ABrush.Color := $00AAAAFF;
end;

procedure TFormVidaCliente.tsAbertosShow(Sender: TObject);
var
  i: Integer;
  vComando: string;
  vTitulosAbertos: TArray<RecContasReceber>;
begin
  inherited;
  if pcFinanceiros.CarregouAba or frCliente.EstaVazio then
    Exit;

  vComando := '';
  _Biblioteca.WhereOuAnd(vComando, FrCliente.getSqlFiltros('CON.CADASTRO_ID'));
  _Biblioteca.WhereOuAnd(vComando, ' CON.STATUS = ''A'' ');
  vComando := vComando + ' order by CON.DATA_VENCIMENTO ';

  vTitulosAbertos := _RelacaoContasReceber.BuscarContasReceberComando(Sessao.getConexaoBanco, vComando);

  if not (vTitulosAbertos = nil) then begin

    for i := Low(vTitulosAbertos) to High(vTitulosAbertos) do begin

      sgTitulosAbertos.Cells[faReceberId, i + 1]       := NFormat(vTitulosAbertos[i].receber_id);
      sgTitulosAbertos.Cells[faDocumento, i + 1]       := vTitulosAbertos[i].documento;
      sgTitulosAbertos.Cells[faParcela, i + 1]         := NFormat(vTitulosAbertos[i].parcela) + '/' + NFormat(vTitulosAbertos[i].numero_parcelas);
      sgTitulosAbertos.Cells[faValor, i + 1]           := NFormatN(vTitulosAbertos[i].valor_documento);
      sgTitulosAbertos.Cells[faRetencao, i + 1]        := NFormatN(vTitulosAbertos[i].ValorRetencao);
      sgTitulosAbertos.Cells[faValorLiquido, i + 1]    := NFormatN(vTitulosAbertos[i].getValorLiquido);
      sgTitulosAbertos.Cells[faDataCadastro, i + 1]    := DFormat(vTitulosAbertos[i].data_cadastro);
      sgTitulosAbertos.Cells[faDataVencimento, i + 1]  := DFormat(vTitulosAbertos[i].data_vencimento);
      sgTitulosAbertos.Cells[faDiasAtraso, i + 1]      := NFormatN(vTitulosAbertos[i].dias_atraso);
      sgTitulosAbertos.Cells[faEmpresa, i + 1]         := NFormat(vTitulosAbertos[i].empresa_id) + ' - ' + vTitulosAbertos[i].nome_empresa;
      sgTitulosAbertos.Cells[faStatusAnalitico, i + 1] := _Biblioteca.IIfStr(vTitulosAbertos[i].status = 'A', 'Em aberto', 'Baixado');
      sgTitulosAbertos.Cells[faStatusSintetico, i + 1] := vTitulosAbertos[i].status;
      sgTitulosAbertos.Cells[faTipoCobranca, i + 1]    := NFormat(vTitulosAbertos[i].cobranca_id) + ' - ' + vTitulosAbertos[i].nome_tipo_cobranca;
      sgTitulosAbertos.Cells[faPortador, i + 1]        := vTitulosAbertos[i].PortadorId + ' - ' + vTitulosAbertos[i].NomePortador;
      sgTitulosAbertos.Cells[faNoCaixa, i + 1]         := vTitulosAbertos[i].NoCaixa;

      if vTitulosAbertos[i].forma_pagamento <> 'CRT' then begin

        vTitulosAbertos[i].ValorMulta :=
          _Biblioteca.getValorMulta(
            vTitulosAbertos[i].valor_documento,
            Sessao.getParametrosEmpresa.PercentualMulta,
            vTitulosAbertos[i].dias_atraso
          );

        vTitulosAbertos[i].ValorJuros :=
          _Biblioteca.getValorJuros(
            vTitulosAbertos[i].valor_documento,
            Sessao.getParametrosEmpresa.PercentualJurosMensal,
            vTitulosAbertos[i].dias_atraso
          );
      end;

      sgTitulosAbertos.Cells[faMulta, i + 1]          := NFormatN(vTitulosAbertos[i].ValorMulta);
      sgTitulosAbertos.Cells[faJuros, i + 1]          := NFormatN(vTitulosAbertos[i].ValorJuros);
      sgTitulosAbertos.Cells[faFormaPagamento, i + 1] := vTitulosAbertos[i].forma_pagamento;
    end;

    sgTitulosAbertos.SetLinhasGridPorTamanhoVetor( Length(vTitulosAbertos) );
  end;

  pcFinanceiros.CarregouAba := True;
end;

procedure TFormVidaCliente.tsBaixadosShow(Sender: TObject);
var
  i: Integer;
  vComando: string;
  vTitulosBaixados: TArray<RecContasReceber>;
begin
  inherited;
  if pcFinanceiros.CarregouAba or frCliente.EstaVazio then
    Exit;

  vComando := '';
  _Biblioteca.WhereOuAnd(vComando, FrCliente.getSqlFiltros('CON.CADASTRO_ID'));
  _Biblioteca.WhereOuAnd(vComando, ' CON.STATUS = ''B'' ');
  vComando := vComando + ' order by CON.DATA_VENCIMENTO ';

  vTitulosBaixados := _RelacaoContasReceber.BuscarContasReceberComando(Sessao.getConexaoBanco, vComando);

  if not (vTitulosBaixados = nil) then begin

    for i := Low(vTitulosBaixados) to High(vTitulosBaixados) do begin

      sgTitulosBaixados.Cells[fbReceberId, i + 1]       := NFormat(vTitulosBaixados[i].receber_id);
      sgTitulosBaixados.Cells[fbDocumento, i + 1]       := vTitulosBaixados[i].documento;
      sgTitulosBaixados.Cells[fbParcela, i + 1]         := NFormat(vTitulosBaixados[i].parcela) + '/' + NFormat(vTitulosBaixados[i].numero_parcelas);
      sgTitulosBaixados.Cells[fbValor, i + 1]           := NFormatN(vTitulosBaixados[i].valor_documento);
      sgTitulosBaixados.Cells[fbRetencao, i + 1]        := NFormatN(vTitulosBaixados[i].ValorRetencao);
      sgTitulosBaixados.Cells[fbValorLiquido, i + 1]    := NFormatN(vTitulosBaixados[i].getValorLiquido);
      sgTitulosBaixados.Cells[fbDataCadastro, i + 1]    := DFormat(vTitulosBaixados[i].data_cadastro);
      sgTitulosBaixados.Cells[fbDataVencimento, i + 1]  := DFormat(vTitulosBaixados[i].data_vencimento);
      sgTitulosBaixados.Cells[fbDiasAtraso, i + 1]      := NFormatN(vTitulosBaixados[i].dias_atraso);
      sgTitulosBaixados.Cells[fbEmpresa, i + 1]         := NFormat(vTitulosBaixados[i].empresa_id) + ' - ' + vTitulosBaixados[i].nome_empresa;
      sgTitulosBaixados.Cells[fbStatusAnalitico, i + 1] := _Biblioteca.IIfStr(vTitulosBaixados[i].status = 'A', 'Em aberto', 'Baixado');
      sgTitulosBaixados.Cells[fbStatusSintetico, i + 1] := vTitulosBaixados[i].status;
      sgTitulosBaixados.Cells[fbTipoCobranca, i + 1]    := NFormat(vTitulosBaixados[i].cobranca_id) + ' - ' + vTitulosBaixados[i].nome_tipo_cobranca;
      sgTitulosBaixados.Cells[fbPortador, i + 1]        := vTitulosBaixados[i].PortadorId + ' - ' + vTitulosBaixados[i].NomePortador;
      sgTitulosBaixados.Cells[fbNoCaixa, i + 1]          := vTitulosBaixados[i].NoCaixa;

      if vTitulosBaixados[i].forma_pagamento <> 'CRT' then begin

        vTitulosBaixados[i].ValorMulta :=
          _Biblioteca.getValorMulta(
            vTitulosBaixados[i].valor_documento,
            Sessao.getParametrosEmpresa.PercentualMulta,
            vTitulosBaixados[i].dias_atraso
          );

        vTitulosBaixados[i].ValorJuros :=
          _Biblioteca.getValorJuros(
            vTitulosBaixados[i].valor_documento,
            Sessao.getParametrosEmpresa.PercentualJurosMensal,
            vTitulosBaixados[i].dias_atraso
          );
      end;

      sgTitulosBaixados.Cells[fbMulta, i + 1]          := NFormatN(vTitulosBaixados[i].ValorMulta);
      sgTitulosBaixados.Cells[fbJuros, i + 1]          := NFormatN(vTitulosBaixados[i].ValorJuros);
      sgTitulosBaixados.Cells[fbFormaPagamento, i + 1] := vTitulosBaixados[i].forma_pagamento;
    end;

    sgTitulosBaixados.SetLinhasGridPorTamanhoVetor( Length(vTitulosBaixados) );
  end;

  pcFinanceiros.CarregouAba := True;
end;

procedure TFormVidaCliente.tsDevolucoesShow(Sender: TObject);
var
  i: Integer;
  vComando: string;
  vDevolucoes: TArray<RecDevolucoes>;
begin
  inherited;
  if pcPedidos.CarregouAba or frCliente.EstaVazio then
    Exit;

  vComando := '';
  _Biblioteca.WhereOuAnd(vComando, FrCliente.getSqlFiltros('CLIENTE_ID'));
  vComando := vComando + ' order by DEVOLUCAO_ID ';

  vDevolucoes := _RelacaoDevolucoes.BuscarDevolucoes(Sessao.getConexaoBanco, vComando);

  if not (vDevolucoes = nil) then begin

    for i := Low(vDevolucoes) to High(vDevolucoes) do begin
      sgDevolucoes.Cells[cdDevolucaoId, i + 1]       := NFormat(vDevolucoes[i].DevolucaoId);
      sgDevolucoes.Cells[cdPedidoID, i + 1]          := NFormat(vDevolucoes[i].OrcamentoId);
      sgDevolucoes.Cells[cdValorBruto, i + 1]        := NFormat(vDevolucoes[i].ValorBruto);
      sgDevolucoes.Cells[cdValorOutDesp, i + 1]      := NFormatN(vDevolucoes[i].ValorOutrasDepesas);
      sgDevolucoes.Cells[cdValorDesconto, i + 1]     := NFormatN(vDevolucoes[i].ValorDesconto);
      sgDevolucoes.Cells[cdValorLiquido, i + 1]      := NFormat(vDevolucoes[i].ValorLiquido);
      sgDevolucoes.Cells[cdDataHoraDevolucao, i + 1] := DHFormat(vDevolucoes[i].DataHoraDevolucao);
      sgDevolucoes.Cells[cdUsuarioDevolucao, i + 1]  := NFormat(vDevolucoes[i].UsuarioCadastroId) + ' - ' + vDevolucoes[i].NomeUsuarioDevolucao;
      sgDevolucoes.Cells[cdEmpresa, i + 1]           := NFormat(vDevolucoes[i].EmpresaId) + ' - ' + vDevolucoes[i].NomeEmpresa;
    end;

    sgDevolucoes.SetLinhasGridPorTamanhoVetor( Length(vDevolucoes) );
  end;

  pcPedidos.CarregouAba := True;
end;

procedure TFormVidaCliente.tsEntregasShow(Sender: TObject);
begin
  inherited;
  pcEntregas.ActivePage := tsPendentes;
  sgEntregasPendentes.SetFocus;
end;

procedure TFormVidaCliente.tsFinanceirosShow(Sender: TObject);
begin
  inherited;
  tsAbertosShow(Sender);
end;

procedure TFormVidaCliente.tsMixProdutosShow(Sender: TObject);
var
  i: Integer;
  vVendasProdutos: TArray<RecMixProdutos>;
begin
  inherited;
  if pcPedidos.CarregouAba or FrCliente.EstaVazio then
    Exit;

  vVendasProdutos := _RelacaoVendasProdutos.BuscarMixProdutosCliente(Sessao.getConexaoBanco, FrCliente.getCliente.cadastro_id);

  if not (vVendasProdutos = nil) then begin

    for i := Low(vVendasProdutos) to High(vVendasProdutos) do begin
      sgMixProdutos.Cells[cpProdutoId, i + 1]          := NFormat(vVendasProdutos[i].ProdutoId);
      sgMixProdutos.Cells[cpNomeProduto, i + 1]        := vVendasProdutos[i].NomeProduto;
      sgMixProdutos.Cells[cpMarca,  i + 1]             := vVendasProdutos[i].NomeMarca;
      sgMixProdutos.Cells[cpQuantidade,  i + 1]        := NFormatN(vVendasProdutos[i].Quantidade);
      sgMixProdutos.Cells[cpUnidade,  i + 1]           := vVendasProdutos[i].UnidadeVenda;
      sgMixProdutos.Cells[cpPrecoMedio,  i + 1]        := NFormatN(vVendasProdutos[i].PrecoMedio);
      sgMixProdutos.Cells[cpValorTotal,  i + 1]        := NFormatN(vVendasProdutos[i].ValorTotal);
      sgMixProdutos.Cells[cpPercParticipacao,  i + 1]  := NFormatN(vVendasProdutos[i].PercentualParticipacao, 4);
      sgMixProdutos.Cells[cdPercentualMargem ,  i + 1] := NFormatN(vVendasProdutos[i].PercentualMargem, 4);
    end;
    sgMixProdutos.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vVendasProdutos) );
  end;

  pcPedidos.CarregouAba := True;
end;

procedure TFormVidaCliente.tsNotasFiscaisShow(Sender: TObject);
var
  i: Integer;
  vComando: string;
  vNotas: TArray<RecRelacaoNotasFiscais>;
begin
  inherited;
  vComando := '';
   _Biblioteca.WhereOuAnd(vComando, FrCliente.getSqlFiltros('NFI.CADASTRO_ID'));
   vComando := vComando + ' order by NFI.DATA_HORA_CADASTRO ';

  vNotas := _RelacaoNotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, vComando);

  if not (vNotas = nil) then begin

    for i := Low(vNotas) to High(vNotas) do begin
      sgNotasFiscais.Cells[ntNotaFiscalId, i + 1]           := _Biblioteca.NFormat(vNotas[i].notaFiscalId);
      sgNotasFiscais.Cells[ntDataCadastro, i + 1]           := _Biblioteca.DFormat(vNotas[i].dataCadastro);
      sgNotasFiscais.Cells[ntEmpresa, i + 1]                := vNotas[i].empresa;
      sgNotasFiscais.Cells[ntTipoNota,  i + 1]              := vNotas[i].tipoNotaAnalitico;
      sgNotasFiscais.Cells[ntTipoMovimentoAnalitico, i + 1] := _NotasFiscais.getTipoMovimentoAnalitico(vNotas[i].tipoMovimento);
      sgNotasFiscais.Cells[ntTipoMovimentoSintetico, i + 1] := vNotas[i].tipoMovimento;
      sgNotasFiscais.Cells[ntNumeroNota, i + 1]             := _Biblioteca.NFormatN(vNotas[i].numeroNota);
      sgNotasFiscais.Cells[ntValorTotalNota, i + 1]         := _Biblioteca.NFormat(vNotas[i].valorTotal);
      sgNotasFiscais.Cells[ntStatus, i + 1]                 := vNotas[i].statusAnalitico;
    end;

    sgNotasFiscais.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vNotas) );
  end;

end;

procedure TFormVidaCliente.tsPedidosShow(Sender: TObject);
begin
  inherited;
  tsVendasShow(Sender);
end;

procedure TFormVidaCliente.tsPendentesShow(Sender: TObject);
var
  i: Integer;
  vComando: string;
  vEntregasPendentes: TArray<RecEntregasPendentes>;
begin
  inherited;
  if pcEntregas.CarregouAba or frCliente.EstaVazio then
    Exit;

  vComando := '';
  _Biblioteca.WhereOuAnd(vComando, FrCliente.getSqlFiltros('CLIENTE_ID') );
  vComando := vComando + ' order by PREVISAO_ENTREGA ';

  vEntregasPendentes := _RelacaoEntregasPendentes.BuscarEntregas(Sessao.getConexaoBanco, vComando);

  if not (vEntregasPendentes = nil) then begin

    for i := Low(vEntregasPendentes) to High(vEntregasPendentes) do begin

      sgEntregasPendentes.Cells[epPedidoID, i + 1]        := _Biblioteca.NFormat(vEntregasPendentes[i].OrcamentoId);
      sgEntregasPendentes.Cells[epTipoMovimento, i + 1]   := vEntregasPendentes[i].TipoMovimentoAnalitico;
      sgEntregasPendentes.Cells[epDataEmissao, i + 1]     := _Biblioteca.DHFormatN(vEntregasPendentes[i].DataHoraCadastro);
      sgEntregasPendentes.Cells[epEmpresa, i + 1]         := _Biblioteca.NFormat(vEntregasPendentes[i].EmpresaId) + ' - ' + vEntregasPendentes[i].NomeEmpresa;
      sgEntregasPendentes.Cells[epLocal, i + 1]           := _Biblioteca.NFormat(vEntregasPendentes[i].LocalId) + ' - ' + vEntregasPendentes[i].NomeLocal;
      sgEntregasPendentes.Cells[epPrevisaoEntrega, i + 1] := _Biblioteca.DHFormatN(vEntregasPendentes[i].PrevisaoEntrega);
    end;

    sgEntregasPendentes.SetLinhasGridPorTamanhoVetor( Length(vEntregasPendentes) );
  end;

  pcEntregas.CarregouAba := True;
end;

procedure TFormVidaCliente.tsRealizadasShow(Sender: TObject);
var
  i: Integer;
  vComando: string;
  vEntregasRealizadas: TArray<RecEntregasRealizadas>;
begin
  inherited;
  if pcEntregas.CarregouAba or frCliente.EstaVazio then
    Exit;

  vComando := '';
  _Biblioteca.WhereOuAnd(vComando, FrCliente.getSqlFiltros('CLIENTE_ID') );
  vComando := vComando + ' order by DATA_HORA_ENTREGA ';

  vEntregasRealizadas := _RelacaoEntregasRealizadas.BuscarEntregas(Sessao.getConexaoBanco, vComando);

  if not (vEntregasRealizadas = nil) then begin

    for i := Low(vEntregasRealizadas) to High(vEntregasRealizadas) do begin
      sgEntregasRealizadas.Cells[ebEntregaID, i + 1]     := _Biblioteca.NFormat(vEntregasRealizadas[i].MovimentoId);
      sgEntregasRealizadas.Cells[ebTipoMovimento, i + 1] := vEntregasRealizadas[i].TipoMovimentoAnalitico;
      sgEntregasRealizadas.Cells[ebPedidoId, i + 1]      := _Biblioteca.NFormat(vEntregasRealizadas[i].OrcamentoId);
      sgEntregasRealizadas.Cells[ebDataEmissao, i + 1]   := _Biblioteca.DHFormatN(vEntregasRealizadas[i].DataHoraCadastro);
      sgEntregasRealizadas.Cells[ebDataEntrega, i + 1]   := _Biblioteca.DHFormatN(vEntregasRealizadas[i].DataHoraEntrega);
      sgEntregasRealizadas.Cells[ebEmpresa, i + 1]       := _Biblioteca.NFormat(vEntregasRealizadas[i].EmpresaId) + ' - ' + vEntregasRealizadas[i].NomeEmpresa;
      sgEntregasRealizadas.Cells[ebLocal, i + 1]         := _Biblioteca.NFormat(vEntregasRealizadas[i].LocalId) + ' - ' + vEntregasRealizadas[i].NomeLocal;
      sgEntregasRealizadas.Cells[ebRecebedor, i + 1]     := vEntregasRealizadas[i].NomePessoaRecebeu;
    end;

    sgEntregasRealizadas.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vEntregasRealizadas) );
  end;

  pcEntregas.CarregouAba := True;
end;

procedure TFormVidaCliente.tsVendasShow(Sender: TObject);
var
  i: Integer;
  vComando: string;
  vPedidos: TArray<RecOrcamentosVendas>;
begin
  inherited;
  if pcPedidos.CarregouAba or frCliente.EstaVazio then
    Exit;

  vComando := '';
  _Biblioteca.WhereOuAnd(vComando, FrCliente.getSqlFiltros('CLIENTE_ID'));
  _Biblioteca.WhereOuAnd(vComando, 'STATUS in (''VB'', ''VR'', ''VE'', ''RE'', ''CA'') ');
  vComando := vComando + ' order by ORCAMENTO_ID ';

  vPedidos := _RelacaoOrcamentosVendas.BuscarOrcamentos(Sessao.getConexaoBanco, vComando);

  if not (vPedidos = nil) then begin

    for i := Low(vPedidos) to High(vPedidos) do begin

      sgPedidos.Cells[cvPedidoID, i + 1]           := NFormat(vPedidos[i].OrcamentoId);
      sgPedidos.Cells[cvEmpresa, i + 1]            := NFormat(vPedidos[i].EmpresaId) + ' - ' + vPedidos[i].NomeEmpresa;
      sgPedidos.Cells[cvDataEmissao, i + 1]        := DFormat(vPedidos[i].DataCadastro);

      if vPedidos[i].VendedorId > 0 then
        sgPedidos.Cells[cvVendedor, i + 1]         := NFormat(vPedidos[i].VendedorId) + ' - ' + vPedidos[i].NomeVendedor;

      sgPedidos.Cells[cvCondicaoPagamento, i + 1]  := NFormat(vPedidos[i].CondicaoId) + ' - ' + vPedidos[i].NomeCondicaoPagamento;
      sgPedidos.Cells[cvValorProdutos  , i + 1]    := NFormatN(vPedidos[i].ValorTotalProdutos);
      sgPedidos.Cells[cvValorDesconto, i + 1]      := NFormatN(vPedidos[i].ValorDesconto);
      sgPedidos.Cells[cvValorOutrasDespesa, i + 1] := NFormatN(vPedidos[i].ValorOutrasDespesas);
      sgPedidos.Cells[cvValorTotal, i + 1]         := NFormatN(vPedidos[i].ValorTotal);
    end;

    sgPedidos.SetLinhasGridPorTamanhoVetor( Length(vPedidos) );
  end;

  pcPedidos.CarregouAba := True;
end;

end.
