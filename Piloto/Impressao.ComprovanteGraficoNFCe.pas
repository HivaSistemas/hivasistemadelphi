unit Impressao.ComprovanteGraficoNFCe;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos, QRExport, QRPDFFilt, QRWebFilt, QRCtrls,
  QuickRpt, Vcl.ExtCtrls, _HerancaRelatorioSimples, _Sessao, _Biblioteca, _Orcamentos, _RecordsOrcamentosVendas,
  _OrcamentosPagamentos, _OrcamentosPagamentosCheques, _RecordsFinanceiros, _OrcamentosItens, _RecordsNotasFiscais,
  _NotasFiscais, _NotasFiscaisItens, _BibliotecaNFE, Vcl.StdCtrls;

type
  RecCobrancas = record
    CobrancaId: Integer;
    NomeCobranca: string;
    Valor: Double;
    Dados: TArray<RecTitulosFinanceiros>;
  end;

  TFormComprovanteGraficoNFCe = class(TFormHerancaRelatoriosGraficos)
    qrTitulo: TQRBand;
    QRShape2: TQRShape;
    qr2: TQRLabel;
    qrOrcamentoId: TQRLabel;
    qr8: TQRLabel;
    qrCliente: TQRLabel;
    qr4: TQRLabel;
    qrTelefoneCliente: TQRLabel;
    qr14: TQRLabel;
    qrCelularCliente: TQRLabel;
    qr9: TQRLabel;
    qrVendedor: TQRLabel;
    qr17: TQRLabel;
    qrCondicaoPagamento: TQRLabel;
    qrJuntar: TQRCompositeReport;
    qr28: TQRLabel;
    qr30: TQRLabel;
    qr31: TQRLabel;
    qr29: TQRLabel;
    qrTotalSerPago: TQRLabel;
    qrValorDesconto: TQRLabel;
    qrValorOutrasDespesas: TQRLabel;
    qrTotalProdutos: TQRLabel;
    qr32: TQRLabel;
    QRLabel2: TQRLabel;
    qrlOrcamentoIdNF: TQRLabel;
    QRLabel4: TQRLabel;
    qrlVendedorNF: TQRLabel;
    QRLabel6: TQRLabel;
    qrlClienteNF: TQRLabel;
    QRLabel8: TQRLabel;
    qrlTelefoneNF: TQRLabel;
    QRLabel10: TQRLabel;
    qrlCelularNF: TQRLabel;
    QRLabel12: TQRLabel;
    qrlCondicaoPagamentoNF: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    qrlTotalSerPagoNF: TQRLabel;
    qrlTotalDescontoNF: TQRLabel;
    qrlTotalOutrasDespesasNF: TQRLabel;
    qrlTotalProdutosNF: TQRLabel;
    QRLabel22: TQRLabel;
    qrl8: TQRLabel;
    qrlNFValorFrete: TQRLabel;
    qrbndDetailBand2: TQRBand;
    qrlNFNomeProduto: TQRLabel;
    qrlNFQuantidade: TQRLabel;
    qrlCabProdutoId: TQRLabel;
    qrl11: TQRLabel;
    qrlNFMarca: TQRLabel;
    qrl15: TQRLabel;
    qrlNFUnidade: TQRLabel;
    qrl16: TQRLabel;
    qrlNFPrecoUnitario: TQRLabel;
    qrl17: TQRLabel;
    qrlNFValorTotal: TQRLabel;
    QRShape1: TQRShape;
    qrl18: TQRLabel;
    qr36: TQRLabel;
    qrNFDataCadastro: TQRLabel;
    qr38: TQRLabel;
    qrNFDataHoraRecebimento: TQRLabel;
    qrlCodigo: TQRLabel;
    qrlDescricao: TQRLabel;
    qrlCodigoValor: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel5: TQRLabel;
    qrlFormaPagto: TQRLabel;
    qrlFormaPagtoValor: TQRLabel;
    QRLabel7: TQRLabel;
    qrlTroco: TQRLabel;
    qrlqtdItens: TQRLabel;
    qrlValorTotal: TQRLabel;
    QRLabel9: TQRLabel;
    qrlChaveAcesso: TQRLabel;
    qrlNumeroChaveAcesso: TQRLabel;
    qrlNomeConsumidor: TQRLabel;
    qrlDocConsumidor: TQRLabel;
    qrlDadosEmissao: TQRLabel;
    qrlProtocoloAutorizacao: TQRLabel;
    qrlDataAutorizacao: TQRLabel;
    qrIQrCode: TQRImage;
    qrlInformacoesComplementares: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape3: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    procedure qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrbndDetailBand2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrJuntarAddReports(Sender: TObject);
  private
    FOrcamentoId: Integer;
    FNotaFiscalId: Integer;

    FPosicItens: Integer;
    FItens: TArray<RecImpressaoDANFENFCe>;
  public
    procedure ImprimirQrCorde(xml: string);
  end;

function Imprimir(pNotaFiscalId: Integer): Boolean;

implementation

uses
  pngimage, HTTPApp, WinInet;

{$R *.dfm}

const
  UrlQrcode = 'https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=';
//  UrlQrCode =
//    'http://chart.apis.google.com/chart?chs=%dx%d&cht=qr&chld=%s&chl=%s';

function Imprimir(pNotaFiscalId: Integer): Boolean;
var
  vForm: TFormComprovanteGraficoNFCe;

  vImpressora: RecImpressora;
  vDadosNota: TArray<RecNotaFiscal>;
  vDadosOrcamento: TArray<RecOrcamentos>;
  vXML: string;
begin
  Result := False;

  vDadosNota := _NotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, 0, [pNotaFiscalId]);
  if vDadosNota = nil then begin
    _Biblioteca.Exclamar('NFC-e n�o encontrado para impress�o!');
    Exit;
  end;

  if vDadosNota[0].status <> 'E' then begin
    _Biblioteca.Exclamar('A NFe deve estar emitida para a impress�o do DANFE!');
    Exit;
  end;

  vImpressora := Sessao.getImpressora(toNFCe);
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.Exclamar('Impressora de NFCe n�o configurada!');
    Exit;
  end;

  vDadosOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 0, [vDadosNota[0].orcamento_id]);

  try
    vForm := TFormComprovanteGraficoNFCe.Create(Application, vImpressora);
    vForm.FItens := _NotasFiscaisItens.BuscarProdutosImpressaoDANFENFCe(Sessao.getConexaoBanco, pNotaFiscalId);
    if vForm.FItens = nil then begin
      _Biblioteca.Exclamar('Itens da NFC-e n�o encontrado para impress�o!');
      Exit;
    end;

    vXML := _NotasFiscais.BuscarXMLNFe(Sessao.getConexaoBanco, pNotaFiscalId);

    vForm.qrlNFNomeEmpresa.Caption := Sessao.getEmpresaLogada.RazaoSocial;
    vForm.qrNFCnpj.Caption := 'CNPJ: ' + Sessao.getEmpresaLogada.Cnpj + ' IE: ' + Sessao.getEmpresaLogada.InscricaoEstadual;
    vForm.qrNFEndereco.Caption := Sessao.getEmpresaLogada.Logradouro + ' ' + Sessao.getEmpresaLogada.Complemento;
    vForm.qrNFCidadeUf.Caption := Sessao.getEmpresaLogada.NomeBairro + '-' + Sessao.getEmpresaLogada.EstadoId;

    vForm.qrlqtdItens.Caption := NFormat(Length(vForm.FItens));
    vForm.qrlValorTotal.Caption := NFormat(vDadosNota[0].valor_total);

    if (vDadosOrcamento[0].valor_dinheiro > 0) or (vDadosOrcamento[0].valor_pix > 0) then begin
      vForm.qrlFormaPagto.Caption := 'DINHEIRO/PIX';
      vForm.qrlFormaPagtoValor.Caption := NFormat(vDadosOrcamento[0].valor_dinheiro + vDadosOrcamento[0].valor_pix);
    end;

    if vDadosOrcamento[0].valor_cheque > 0 then begin
      vForm.qrlFormaPagto.Caption := 'CHEQUE';
      vForm.qrlFormaPagtoValor.Caption :=  NFormat(vDadosOrcamento[0].valor_cheque);
    end;

    if vDadosOrcamento[0].ValorCartaoDebito > 0 then begin
      vForm.qrlFormaPagto.Caption := 'CART�O DE DEBITO';
      vForm.qrlFormaPagtoValor.Caption :=  NFormat(vDadosOrcamento[0].ValorCartaoDebito);
    end;

      if vDadosOrcamento[0].ValorCartaoCredito > 0 then begin
      vForm.qrlFormaPagto.Caption := 'CART�O DE CREDITO';
      vForm.qrlFormaPagtoValor.Caption :=  NFormat(vDadosOrcamento[0].ValorCartaoCredito);
    end;

    if vDadosOrcamento[0].valor_cobranca > 0 then begin
      vForm.qrlFormaPagto.Caption := 'COBRAN�A';
      vForm.qrlFormaPagtoValor.Caption :=  NFormat(vDadosOrcamento[0].valor_cobranca);
    end;

    if vDadosOrcamento[0].valor_credito > 0 then begin
      vForm.qrlFormaPagto.Caption := 'CR�DITO';
      vForm.qrlFormaPagtoValor.Caption :=  NFormat(vDadosOrcamento[0].valor_credito);
    end;

    if vDadosOrcamento[0].valorTroco > 0 then begin
      vForm.qrlTroco.Caption :=  NFormat(vDadosOrcamento[0].valorTroco);
    end;

    vForm.qrlChaveAcesso.Caption := _BibliotecaNFE.GetURLQRCode(vDadosNota[0].estado_id_emitente, IIf(Sessao.getParametrosEmpresa.tipoAmbienteNFCe = 'P', tpProducao, tpHomologacao));
    vForm.qrlNumeroChaveAcesso.Caption := RetornaNumeros(vDadosNota[0].chave_nfe);

    if vDadosNota[0].nome_fantasia_destinatario <> 'CONSUMIDOR FINAL' then
      vForm.qrlNomeConsumidor.Caption := vDadosNota[0].nome_fantasia_destinatario
    else
      vForm.qrlNomeConsumidor.Caption := vDadosNota[0].nome_consumidor_final;

    vForm.qrlDocConsumidor.Caption := vDadosNota[0].cpf_cnpj_destinatario;

    vForm.qrlDadosEmissao.Caption := 'N�mero ' + LPad(NFormat(vDadosNota[0].numero_nota), 6, '0') + ' S�rie' +
        LPad(vDadosNota[0].SerieNota, 2) + ' Emiss�o ' + DFormat(vDadosNota[0].data_hora_emissao);

    vForm.qrlProtocoloAutorizacao.Caption := 'Protocolo de autoriza��o: ' + vDadosNota[0].protocolo_nfe;
    vForm.qrlDataAutorizacao.Caption := 'Data de autoriza��o: ' + DFormat(vDadosNota[0].data_hora_emissao);

    vXML := Copy(vXML, Pos('<![CDATA[', vXML) + 9, Pos(']]>', vXML) - Pos('<![CDATA[', vXML) - 9);
    vForm.ImprimirQrCorde(vXML);

    vForm.qrlInformacoesComplementares.Caption := vDadosNota[0].informacoes_complementares;

    vForm.qrJuntar.PrinterSettings.PrinterIndex := vImpressora.getIndex;
    if vImpressora.AbrirPreview then
      vForm.qrJuntar.Preview
    else
      vForm.qrJuntar.Print;

    Result := true;
  except
    on e: Exception do
      Exclamar('Erro ao imprimir o DANFE da NFCe, caso necess�rio reemita na tela "Rela��o de notas fiscais"! ' + e.Message);
  end;
  vForm.Free;
end;

procedure TFormComprovanteGraficoNFCe.qrbndDetailBand2BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrlCodigoValor.Caption := NFormat(FItens[FPosicItens].produtoId);
  qrlNFNomeProduto.Caption := FItens[FPosicItens].nome;
  qrlNFQuantidade.Caption := NFormat(FItens[FPosicItens].quantidade, 3);
  qrlNFUnidade.Caption := FItens[FPosicItens].unidade;
  qrlNFPrecoUnitario.Caption := NFormat(FItens[FPosicItens].precoUnitario);
  qrlNFValorTotal.Caption := NFormat(FItens[FPosicItens].precoUnitario * FItens[FPosicItens].quantidade);
end;

procedure TFormComprovanteGraficoNFCe.qrJuntarAddReports(Sender: TObject);
begin
  inherited;
  qrJuntar.Reports.Add( qrRelatorioNF );
end;

procedure TFormComprovanteGraficoNFCe.qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicItens := -1;
end;

procedure TFormComprovanteGraficoNFCe.qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicItens);
  MoreData := FPosicItens < Length(FItens);
end;

procedure WinInet_HttpGet(const Url: string; Stream: TStream);
const
  BuffSize = 1024 * 1024;
var
  hInter: HINTERNET;
  UrlHandle: HINTERNET;
  BytesRead: DWORD;
  Buffer: Pointer;
begin
  hInter := InternetOpen('', INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if Assigned(hInter) then
  begin
    Stream.Seek(0, 0);
    GetMem(Buffer, BuffSize);
    try
      UrlHandle := InternetOpenUrl(hInter, PChar(Url), nil, 0,
        INTERNET_FLAG_RELOAD, 0);
      if Assigned(UrlHandle) then
      begin
        repeat
          InternetReadFile(UrlHandle, Buffer, BuffSize, BytesRead);
          if BytesRead > 0 then
            Stream.WriteBuffer(Buffer^, BytesRead);
        until BytesRead = 0;
        InternetCloseHandle(UrlHandle);
      end;
    finally
      FreeMem(Buffer);
    end;
    InternetCloseHandle(hInter);
  end
end;

procedure GetQrCode(Width, Height: Word;
  Correction_Level: string; const Data: string;
  StreamImage: TMemoryStream);
Var
  EncodedURL: string;
begin
  EncodedURL := UrlQrcode + Data;
  //EncodedURL := Format(UrlQrCode, [Width, Height, 'H', HTTPEncode(Data)]);
  WinInet_HttpGet(EncodedURL, StreamImage);
end;

procedure TFormComprovanteGraficoNFCe.ImprimirQrCorde(xml: string);
var
  ImageStream: TMemoryStream;
  PngImage: TPngImage;
begin
  qrIQrCode.Picture := nil;
  ImageStream := TMemoryStream.Create;
  PngImage := TPngImage.Create;
  try
    try
      GetQrCode(150, 150, 'H', xml, ImageStream);
      if ImageStream.Size > 0 then
      begin
        ImageStream.Position := 0;
        PngImage.LoadFromStream(ImageStream);
        qrIQrCode.Picture.Assign(PngImage);
      end;
    except
      on E: exception do
        ShowMessage(E.Message);
    end;
  finally
    ImageStream.Free;
    PngImage.Free;
  end;
end;

end.
