unit _CondicoesPagamentoEmpresa;

interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils;

{$M+}
type
  RecCondicoesPagamentoEmpresa = record
    CondicaoId: Integer;
    EmpresaId: Integer;
  end;

  TCondicoesPagamentoEmpresa = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordOrdem: RecCondicoesPagamentoEmpresa;
  end;

function BuscarCondicoesPagamentoEmpresa(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCondicoesPagamentoEmpresa>;

function getFiltros: TArray<RecFiltros>;

implementation

{ TCondicoesPagamentoPrazos }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 1);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where CONDICAO_ID = :P1 ' +
      'order by ' +
      '  EMPRESA_ID asc '
    );
end;

constructor TCondicoesPagamentoEmpresa.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'CONDICOES_PAGAMENTO_EMPRESA');

  FSql :=
    'select ' +
    '  CONDICAO_ID, ' +
    '  EMPRESA_ID ' +
    'from ' +
    '  CONDICOES_PAGAMENTO_EMPRESA ';

  setFiltros(getFiltros);

  AddColuna('CONDICAO_ID', True);
  AddColuna('EMPRESA_ID', True);
end;

function TCondicoesPagamentoEmpresa.getRecordOrdem: RecCondicoesPagamentoEmpresa;
begin
  Result.CondicaoId := getInt('CONDICAO_ID', True);
  Result.EmpresaId  := getInt('EMPRESA_ID', True);
end;

function BuscarCondicoesPagamentoEmpresa(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecCondicoesPagamentoEmpresa>;
var
  i: Integer;
  t: TCondicoesPagamentoEmpresa;
begin
  Result := nil;
  t := TCondicoesPagamentoEmpresa.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordOrdem;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

end.
