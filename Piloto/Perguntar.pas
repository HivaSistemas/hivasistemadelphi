unit Perguntar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.Buttons, _Biblioteca,
  Vcl.StdCtrls, Vcl.Imaging.jpeg, Vcl.ExtCtrls, Vcl.Imaging.pngimage;

type
  TFormPerguntar = class(TFormHerancaPrincipal)
    Image1: TImage;
    sbSim: TPanel;
    sbNao: TPanel;
    ePergunta: TMemo;
    procedure sbSimClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  public
    { Public declarations }
  end;

function CriarPergunta(pCaptionForm: string; pPergunta: string): TRetornoTelaFinalizar;

implementation

{$R *.dfm}

function CriarPergunta(pCaptionForm: string; pPergunta: string): TRetornoTelaFinalizar;
var
  vForm: TFormPerguntar;
begin
  vForm := TFormPerguntar.Create(nil);

  vForm.sbSim.Color := _Biblioteca.getCorVerdePadraoAltis;
  vForm.Caption := vForm.Caption + ' ' + pCaptionForm;
  vForm.ePergunta.Lines.Text := pPergunta;

  Result.Ok(vForm.ShowModal);
end;

procedure TFormPerguntar.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = Ord('S') then
    sbSimClick(sbSim);

  if Key = Ord('N') then
    sbSimClick(sbNao);
end;

procedure TFormPerguntar.sbSimClick(Sender: TObject);
begin
  inherited;
  if Sender = sbSim then
    ModalResult := mrOk
  else
    ModalResult := mrCancel;
end;

end.
