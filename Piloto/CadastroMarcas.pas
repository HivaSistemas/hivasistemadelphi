unit CadastroMarcas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsCadastros,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, _Sessao, _RecordsEspeciais, _Marcas,
  PesquisaMarcas, _HerancaCadastro, CheckBoxLuka;

type
  TFormCadastroMarcas = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
  private
    procedure PreencherRegistro(pMarca: RecMarcas);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroMarcas }

procedure TFormCadastroMarcas.BuscarRegistro;
var
  vMarcas: TArray<RecMarcas>;
begin
  vMarcas := _Marcas.BuscarMarcas(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vMarcas = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vMarcas[0]);
end;

procedure TFormCadastroMarcas.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Marcas.ExcluirMarca(Sessao.getConexaoBanco, eId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroMarcas.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco :=
    _Marcas.AtualizarMarca(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      _Biblioteca.ToChar(ckAtivo)
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  if vRetBanco.AsInt > 0 then
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + IntToStr(vRetBanco.AsInt))
  else
    _Biblioteca.Informar(coAlteracaoRegistroSucesso);
end;

procedure TFormCadastroMarcas.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eNome, ckAtivo], pEditando);

  if pEditando then begin
    SetarFoco(eNome);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormCadastroMarcas.PesquisarRegistro;
var
  vMarca: RecMarcas;
begin
  vMarca := RecMarcas(PesquisaMarcas.PesquisarMarca);
  if vMarca = nil then
    Exit;

  inherited;
  PreencherRegistro(vMarca);
end;

procedure TFormCadastroMarcas.PreencherRegistro(pMarca: RecMarcas);
begin
  eID.AsInt := pMarca.marca_id;
  eNome.Text := pMarca.nome;
  ckAtivo.Checked := (pMarca.ativo = 'S');

  pMarca.Free;
end;

procedure TFormCadastroMarcas.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o nome da marca!');
    SetarFoco(eNome);
    Abort;
  end;
end;

end.
