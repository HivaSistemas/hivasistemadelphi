inherited FormOutrasNotas: TFormOutrasNotas
  Caption = 'Lan'#231'amento de notas manuais'
  ClientHeight = 578
  ClientWidth = 1013
  ExplicitWidth = 1019
  ExplicitHeight = 607
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 124
    Top = 1
    ExplicitLeft = 124
    ExplicitTop = 1
  end
  object lb1: TLabel [1]
    Left = 195
    Top = 1
    Width = 95
    Height = 14
    Caption = 'Tipo de opera'#231#227'o'
  end
  object lb8: TLabel [2]
    Left = 535
    Top = 1
    Width = 61
    Height = 14
    Caption = 'Logradouro'
  end
  object lb9: TLabel [3]
    Left = 780
    Top = 1
    Width = 76
    Height = 14
    Caption = 'Complemento'
  end
  object lb2: TLabel [4]
    Left = 414
    Top = 42
    Width = 43
    Height = 14
    Caption = 'N'#250'mero'
  end
  object lb10: TLabel [5]
    Left = 488
    Top = 42
    Width = 20
    Height = 14
    Caption = 'Cep'
  end
  object lb4: TLabel [6]
    Left = 665
    Top = 84
    Width = 47
    Height = 14
    Caption = 'Unidade'
  end
  object lb5: TLabel [7]
    Left = 724
    Top = 84
    Width = 64
    Height = 14
    Caption = 'Quantidade'
  end
  object lb6: TLabel [8]
    Left = 795
    Top = 84
    Width = 59
    Height = 14
    Caption = 'Pr. unit'#225'rio'
  end
  object lb7: TLabel [9]
    Left = 870
    Top = 84
    Width = 65
    Height = 14
    Caption = 'Tot. produto'
  end
  object lb11: TLabel [10]
    Left = 965
    Top = 83
    Width = 18
    Height = 14
    Caption = 'CST'
  end
  object lb12: TLabel [11]
    Left = 275
    Top = 124
    Width = 74
    Height = 14
    Caption = 'Ind. red. ICMS'
  end
  object lb13: TLabel [12]
    Left = 359
    Top = 124
    Width = 56
    Height = 14
    Caption = 'Base ICMS'
  end
  object lb14: TLabel [13]
    Left = 430
    Top = 124
    Width = 38
    Height = 14
    Caption = '% ICMS'
  end
  object lb15: TLabel [14]
    Left = 482
    Top = 124
    Width = 57
    Height = 14
    Caption = 'Valor ICMS'
  end
  object lb16: TLabel [15]
    Left = 553
    Top = 124
    Width = 89
    Height = 14
    Caption = 'Ind. red. ICMS ST'
  end
  object lb17: TLabel [16]
    Left = 656
    Top = 124
    Width = 71
    Height = 14
    Caption = 'Base ICMS ST'
  end
  object lb18: TLabel [17]
    Left = 736
    Top = 124
    Width = 53
    Height = 14
    Caption = '% ICMS ST'
  end
  object lb19: TLabel [18]
    Left = 803
    Top = 124
    Width = 72
    Height = 14
    Caption = 'Valor ICMS ST'
  end
  object lb20: TLabel [19]
    Left = 885
    Top = 124
    Width = 26
    Height = 14
    Caption = '% IPI'
  end
  object lb21: TLabel [20]
    Left = 931
    Top = 124
    Width = 45
    Height = 14
    Caption = 'Valor IPI'
  end
  object lb24: TLabel [21]
    Left = 124
    Top = 124
    Width = 51
    Height = 14
    Caption = 'Desconto'
  end
  object lb26: TLabel [22]
    Left = 200
    Top = 124
    Width = 55
    Height = 14
    Caption = 'Out. desp.'
  end
  object lb37: TLabel [23]
    Left = 124
    Top = 163
    Width = 58
    Height = 14
    Caption = 'Peso bruto'
  end
  object lb38: TLabel [24]
    Left = 200
    Top = 163
    Width = 68
    Height = 14
    Caption = 'Peso l'#237'quido'
  end
  object lb3: TLabel [25]
    Left = 471
    Top = 163
    Width = 131
    Height = 14
    Caption = 'C'#243'd.produto sair na NFe'
  end
  object lbEstoqueFiscal: TLabel [26]
    Left = 626
    Top = 163
    Width = 79
    Height = 14
    Caption = 'Estoque Fiscal'
  end
  object Label3: TLabel [27]
    Left = 359
    Top = 162
    Width = 97
    Height = 14
    Caption = 'N. Item no Pedido'
  end
  object Label4: TLabel [28]
    Left = 276
    Top = 163
    Width = 70
    Height = 14
    Caption = 'N'#250'm. pedido'
  end
  inherited pnOpcoes: TPanel
    Height = 578
    TabOrder = 35
    ExplicitHeight = 578
    inherited sbDesfazer: TSpeedButton
      Top = 46
      ExplicitTop = 46
    end
    inherited sbExcluir: TSpeedButton
      Top = 102
      ExplicitTop = 102
    end
    inherited sbPesquisar: TSpeedButton
      Top = 270
      Visible = False
      ExplicitTop = 270
    end
    object SpeedButton1: TSpeedButton
      Left = 2
      Top = 386
      Width = 112
      Height = 44
      Caption = 'Importar Produtos'
      NumGlyphs = 2
      OnClick = SpeedButton1Click
    end
  end
  inherited eID: TEditLuka
    Left = 124
    Top = 15
    Width = 66
    TabOrder = 0
    ExplicitLeft = 124
    ExplicitTop = 15
    ExplicitWidth = 66
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 1015
    TabOrder = 36
    Visible = False
    ExplicitLeft = 1015
  end
  object sgProdutos: TGridLuka
    Left = 122
    Top = 202
    Width = 886
    Height = 267
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alCustom
    ColCount = 28
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 34
    OnDblClick = sgProdutosDblClick
    OnDrawCell = sgProdutosDrawCell
    OnKeyDown = sgProdutosKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome do produto'
      'Unid.'
      'Qtde'
      'Pre'#231'o unit.'
      'Valor total'
      'Desconto'
      'Out. desp.'
      'CFOP'
      'CST'
      'Base ICMS'
      '% ICMS'
      'Valor ICMS'
      'Ind. red. ICMS'
      'Base ICMS ST'
      '% ICMS ST'
      'Valor ICMS ST'
      'Ind. red. ICMS ST'
      '% IPI'
      'Valor IPI'
      'Base PIS'
      '% PIS'
      'Valor PIS'
      'Base COFINS'
      '% COFINS'
      'Valor COFINS'
      'N. Pedido'
      'N. Item no Pedido')
    Grid3D = False
    RealColCount = 35
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      46
      172
      44
      54
      76
      81
      68
      71
      178
      32
      67
      49
      69
      84
      85
      68
      82
      103
      40
      68
      75
      40
      66
      80
      62
      82
      90
      104)
  end
  object cbTipoOperacao: TComboBoxLuka
    Left = 195
    Top = 15
    Width = 96
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    ItemIndex = 0
    TabOrder = 1
    Text = 'Entrada'
    OnChange = cbTipoOperacaoChange
    OnExit = cbTipoOperacaoExit
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Entrada'
      'Sa'#237'da')
    Valores.Strings = (
      'E'
      'S')
    AsInt = 0
    AsString = 'E'
  end
  object eLogradouro: TEditLuka
    Left = 535
    Top = 15
    Width = 240
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 100
    TabOrder = 3
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eComplemento: TEditLuka
    Left = 780
    Top = 15
    Width = 225
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 100
    TabOrder = 4
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNumero: TEditLuka
    Left = 414
    Top = 56
    Width = 68
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 6
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCEP: TEditCEPLuka
    Left = 488
    Top = 56
    Width = 66
    Height = 22
    EditMask = '00000-999;1;_'
    MaxLength = 9
    TabOrder = 7
    Text = '     -   '
    OnKeyDown = ProximoCampo
  end
  inline FrBairro: TFrBairros
    Left = 124
    Top = 40
    Width = 285
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 124
    ExplicitTop = 40
    ExplicitWidth = 285
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 260
      Height = 23
      TabOrder = 1
      ExplicitWidth = 260
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 3
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 5
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 7
    end
    inherited PnTitulos: TPanel
      Width = 285
      TabOrder = 0
      ExplicitWidth = 285
      inherited lbNomePesquisa: TLabel
        Caption = 'Bairro'
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 180
        ExplicitLeft = 180
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 260
      Height = 24
      TabOrder = 2
      ExplicitLeft = 260
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      TabOrder = 6
    end
  end
  inline FrProduto: TFrProdutos
    Left = 124
    Top = 82
    Width = 285
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 9
    TabStop = True
    ExplicitLeft = 124
    ExplicitTop = 82
    ExplicitWidth = 285
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 260
      Height = 23
      TabOrder = 1
      ExplicitWidth = 260
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 3
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 5
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 7
    end
    inherited PnTitulos: TPanel
      Width = 285
      TabOrder = 0
      ExplicitWidth = 285
      inherited lbNomePesquisa: TLabel
        Width = 42
        Caption = 'Produto'
        ExplicitWidth = 42
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 180
        ExplicitLeft = 180
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 260
      Height = 24
      TabOrder = 2
      ExplicitLeft = 260
      ExplicitHeight = 24
      inherited sbPesquisa: TSpeedButton
        Top = -2
        ExplicitTop = -2
      end
    end
    inherited ckSomenteAtivos: TCheckBox
      TabOrder = 6
    end
  end
  object cbUnidade: TComboBoxLuka
    Left = 665
    Top = 98
    Width = 54
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 11
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'UND')
    AsInt = 0
  end
  object eQuantidade: TEditLuka
    Left = 724
    Top = 98
    Width = 66
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 12
    Text = '0,0000'
    OnChange = eQuantidadeChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object ePrecoUnitario: TEditLuka
    Left = 795
    Top = 98
    Width = 70
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 13
    Text = '0,00'
    OnChange = ePrecoUnitarioChange
    OnKeyDown = ePrecoUnitarioKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorTotalProduto: TEditLuka
    Left = 870
    Top = 98
    Width = 90
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 14
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object cbCST: TComboBoxLuka
    Left = 965
    Top = 98
    Width = 45
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 15
    OnChange = cbCSTChange
    OnKeyDown = ProximoCampo
    Items.Strings = (
      '00'
      '10'
      '20'
      '40'
      '41'
      '60'
      '70'
      '90')
    AsInt = 0
  end
  object eIndiceReducaoBaseICMS: TEditLuka
    Left = 275
    Top = 138
    Width = 79
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 18
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object eValorBaseICMS: TEditLuka
    Left = 359
    Top = 138
    Width = 66
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 19
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object ePercentualICMS: TEditLuka
    Left = 430
    Top = 138
    Width = 47
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 20
    Text = '0,00'
    OnChange = ePercentualICMSChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorICMS: TEditLuka
    Left = 482
    Top = 138
    Width = 66
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 21
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eIndiceReducaoBaseICMSST: TEditLuka
    Left = 553
    Top = 138
    Width = 98
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 22
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object eValorBaseICMSST: TEditLuka
    Left = 656
    Top = 138
    Width = 75
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 23
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object ePercentualICMSST: TEditLuka
    Left = 736
    Top = 138
    Width = 62
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 24
    Text = '0,00'
    OnChange = ePercentualICMSSTChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorICMSST: TEditLuka
    Left = 803
    Top = 138
    Width = 77
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 25
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object ePercentualIPI: TEditLuka
    Left = 885
    Top = 138
    Width = 41
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 26
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorIPI: TEditLuka
    Left = 931
    Top = 138
    Width = 79
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 27
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorDesconto: TEditLuka
    Left = 124
    Top = 138
    Width = 70
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 16
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorOutrasDespesas: TEditLuka
    Left = 200
    Top = 138
    Width = 70
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 17
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object ePesoBruto: TEditLuka
    Left = 124
    Top = 177
    Width = 70
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 28
    Text = '0,000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 3
    NaoAceitarEspaco = False
  end
  object ePesoLiquido: TEditLuka
    Left = 200
    Top = 177
    Width = 70
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    TabOrder = 29
    Text = '0,000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 3
    NaoAceitarEspaco = False
  end
  inline FrCFOPCapa: TFrCFOPs
    Left = 560
    Top = 40
    Width = 357
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    TabStop = True
    ExplicitLeft = 560
    ExplicitTop = 40
    ExplicitWidth = 357
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 332
      Height = 24
      ExplicitWidth = 332
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 357
      ExplicitWidth = 357
      inherited lbNomePesquisa: TLabel
        Width = 26
        Caption = 'CFOP'
        ExplicitWidth = 26
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 252
        ExplicitLeft = 252
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited cbTipoCFOPs: TComboBoxLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited pnPesquisa: TPanel
      Left = 332
      Height = 25
      ExplicitLeft = 332
      ExplicitHeight = 25
    end
  end
  inline FrCFOPProduto: TFrCFOPs
    Left = 414
    Top = 82
    Width = 247
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    TabStop = True
    ExplicitLeft = 414
    ExplicitTop = 82
    ExplicitWidth = 247
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 222
      Height = 24
      ExplicitWidth = 222
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 247
      ExplicitWidth = 247
      inherited lbNomePesquisa: TLabel
        Width = 26
        Caption = 'CFOP'
        ExplicitWidth = 26
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 142
        ExplicitLeft = 142
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited cbTipoCFOPs: TComboBoxLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited pnPesquisa: TPanel
      Left = 222
      Height = 25
      ExplicitLeft = 222
      ExplicitHeight = 25
    end
  end
  inline FrCadastro: TFrameCadastros
    Left = 295
    Top = -2
    Width = 235
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 295
    ExplicitTop = -2
    ExplicitWidth = 235
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 210
      Height = 24
      ExplicitWidth = 210
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 235
      ExplicitWidth = 235
      inherited lbNomePesquisa: TLabel
        Width = 54
        ExplicitWidth = 54
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 130
        ExplicitLeft = 130
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 210
      Height = 25
      ExplicitLeft = 210
      ExplicitHeight = 25
    end
  end
  object pcOutros: TPageControl
    Left = 125
    Top = 469
    Width = 885
    Height = 109
    ActivePage = tsParametros
    TabOrder = 37
    object tsTotais: TTabSheet
      Caption = 'Totais da nota fiscal'
      object lb22: TLabel
        Left = 0
        Top = 0
        Width = 83
        Height = 14
        Caption = 'Base c'#225'lc. ICMS'
      end
      object lb23: TLabel
        Left = 94
        Top = 0
        Width = 57
        Height = 14
        Caption = 'Valor ICMS'
      end
      object lb25: TLabel
        Left = 448
        Top = 0
        Width = 59
        Height = 14
        Caption = 'Valor desc.'
      end
      object lb27: TLabel
        Left = 525
        Top = 0
        Width = 85
        Height = 14
        Caption = 'Valor out. desp.'
      end
      object lb31: TLabel
        Left = 619
        Top = 0
        Width = 80
        Height = 14
        Caption = 'Valor produtos'
      end
      object lb32: TLabel
        Left = 171
        Top = 0
        Width = 98
        Height = 14
        Caption = 'Base c'#225'lc. ICMS ST'
      end
      object lb33: TLabel
        Left = 279
        Top = 0
        Width = 72
        Height = 14
        Caption = 'Valor ICMS ST'
      end
      object lb36: TLabel
        Left = 373
        Top = 0
        Width = 45
        Height = 14
        Caption = 'Valor IPI'
      end
      object lb30: TLabel
        Left = 0
        Top = 40
        Width = 64
        Height = 14
        Caption = 'Total nota'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb35: TLabel
        Left = 134
        Top = 40
        Width = 163
        Height = 14
        Caption = 'Informa'#231#245'es complementares'
      end
      object eValorBaseICMSTotal: TEditLuka
        Left = 0
        Top = 14
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 0
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorICMSTotal: TEditLuka
        Left = 94
        Top = 14
        Width = 73
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 1
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorDescontoTotal: TEditLuka
        Left = 448
        Top = 14
        Width = 73
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 5
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorOutrasDespesasTotal: TEditLuka
        Left = 525
        Top = 14
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 6
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorTotalProdutos: TEditLuka
        Left = 619
        Top = 14
        Width = 98
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 7
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eBaseICMSSTTotal: TEditLuka
        Left = 171
        Top = 14
        Width = 104
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 2
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorICMSSTTotal: TEditLuka
        Left = 279
        Top = 14
        Width = 90
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 3
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorIPITotal: TEditLuka
        Left = 373
        Top = 14
        Width = 71
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 4
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorTotalNota: TEditLuka
        Left = 0
        Top = 54
        Width = 128
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        MaxLength = 10
        ParentFont = False
        TabOrder = 8
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eInformacoesComplementares: TMemo
        Left = 134
        Top = 54
        Width = 741
        Height = 22
        MaxLength = 1000
        ScrollBars = ssVertical
        TabOrder = 9
      end
    end
    object tsParametros: TTabSheet
      Caption = 'Par'#226'metros de emiss'#227'o'
      ImageIndex = 1
      object lb34: TLabel
        Left = 0
        Top = 0
        Width = 69
        Height = 14
        Caption = 'Tipo de nota'
      end
      object Label2: TLabel
        Left = 184
        Top = 0
        Width = 70
        Height = 14
        Caption = 'Tipo de frete'
      end
      object ckEmitirAltis: TCheckBoxLuka
        Left = 0
        Top = 41
        Width = 161
        Height = 17
        Caption = 'Gerar nota pelo sistema'
        TabOrder = 1
        OnKeyDown = ProximoCampo
        CheckedStr = 'N'
      end
      object ckMovimentarEstoque: TCheckBoxLuka
        Left = 0
        Top = 58
        Width = 161
        Height = 17
        Caption = 'Movimentar estoque'
        Checked = True
        State = cbChecked
        TabOrder = 2
        Visible = False
        OnKeyDown = ProximoCampo
        CheckedStr = 'S'
      end
      object cbTipoNota: TComboBoxLuka
        Left = 0
        Top = 13
        Width = 96
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 0
        OnKeyDown = ProximoCampo
        Items.Strings = (
          'NFC-e'
          'NF-e')
        Valores.Strings = (
          'C'
          'N')
        AsInt = 0
      end
      object cbTipoFrete: TComboBoxLuka
        Left = 184
        Top = 13
        Width = 236
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 3
        Text = 'Frete por conta do Remetente (CIF)'
        OnKeyDown = ProximoCampo
        Items.Strings = (
          'Frete por conta do Remetente (CIF)'
          'Frete por conta do Destinat'#225'rio (FOB)'
          'Sem ocorr'#234'ncia de transporte')
        Valores.Strings = (
          '0'
          '1'
          '9')
        AsInt = 0
        AsString = '0'
      end
    end
    object tsReferencias: TTabSheet
      Caption = 'Refer'#234'ncias'
      ImageIndex = 2
      inline FrReferenciasNotasFiscais: TFrReferenciasNotasFiscais
        Left = 0
        Top = 0
        Width = 877
        Height = 80
        Align = alClient
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 877
        ExplicitHeight = 80
        inherited sgValores: TGridLuka
          Width = 877
          Height = 63
          ExplicitWidth = 877
          ExplicitHeight = 63
          ColWidths = (
            104
            506)
        end
        inherited StaticTextLuka1: TStaticTextLuka
          Width = 877
          ExplicitWidth = 877
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Informa'#231#245'es do transporte'
      ImageIndex = 3
      object lb28: TLabel
        Left = 0
        Top = 0
        Width = 58
        Height = 14
        Caption = 'Peso bruto'
      end
      object lb29: TLabel
        Left = 79
        Top = 0
        Width = 68
        Height = 14
        Caption = 'Peso l'#237'quido'
      end
      object Label5: TLabel
        Left = 160
        Top = 0
        Width = 64
        Height = 14
        Caption = 'Quantidade'
      end
      inline FrTransportadora: TFrTransportadora
        Left = 249
        Top = 0
        Width = 359
        Height = 40
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 249
        ExplicitWidth = 359
        ExplicitHeight = 40
        inherited sgPesquisa: TGridLuka
          Width = 334
          Height = 23
          ExplicitWidth = 334
          ExplicitHeight = 23
        end
        inherited PnTitulos: TPanel
          Width = 359
          ExplicitWidth = 359
          inherited lbNomePesquisa: TLabel
            Width = 83
            Caption = 'Transportadora'
            ExplicitWidth = 83
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 254
            ExplicitLeft = 254
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 334
          Height = 24
          ExplicitLeft = 334
          ExplicitHeight = 24
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrUnidadeVenda: TFrUnidades
        Left = 616
        Top = -1
        Width = 255
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 616
        ExplicitTop = -1
        ExplicitWidth = 255
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 230
          Height = 24
          TabOrder = 1
          ExplicitWidth = 230
          ExplicitHeight = 24
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 255
          TabOrder = 0
          ExplicitWidth = 255
          inherited lbNomePesquisa: TLabel
            Width = 42
            Caption = 'Esp'#233'cie'
            ExplicitWidth = 42
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 150
            ExplicitLeft = 150
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 230
          Height = 25
          ExplicitLeft = 230
          ExplicitHeight = 25
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      object ePesoBrutoTotal: TEditLuka
        Left = 0
        Top = 14
        Width = 75
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 2
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object ePesoLiquidoTotal: TEditLuka
        Left = 79
        Top = 14
        Width = 75
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        MaxLength = 10
        TabOrder = 3
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eVolumes: TEditLuka
        Left = 160
        Top = 14
        Width = 75
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 4
        Text = '0,00'
        OnKeyDown = ProximoCampo
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
    end
  end
  object eCodigoProdutoNFe: TEditLuka
    Left = 471
    Top = 177
    Width = 150
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 60
    TabOrder = 32
    OnKeyDown = eCodigoProdutoNFeKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEstoqueFiscal: TEditLuka
    Left = 627
    Top = 176
    Width = 118
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 10
    ReadOnly = True
    TabOrder = 33
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eNumeroItemPedido: TEditLuka
    Left = 359
    Top = 176
    Width = 106
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 60
    TabOrder = 31
    OnKeyDown = eCodigoProdutoNFeKeyDown
    TipoCampo = tcTexto
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNumeroPedido: TEditLuka
    Left = 276
    Top = 177
    Width = 79
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 20
    TabOrder = 30
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object OpenDialog1: TOpenDialog
    Left = 528
    Top = 320
  end
end
