inherited FrPagamentoFinanceiro: TFrPagamentoFinanceiro
  Width = 303
  Height = 295
  ExplicitWidth = 303
  ExplicitHeight = 295
  object grpFechamento: TGroupBox
    Left = 0
    Top = 0
    Width = 303
    Height = 295
    Align = alClient
    TabOrder = 0
    object lbllb11: TLabel
      Left = 50
      Top = 121
      Width = 60
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Dinheiro'
    end
    object lbllb12: TLabel
      Left = 50
      Top = 141
      Width = 60
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Cheque'
    end
    object lbllb13: TLabel
      Left = 40
      Top = 161
      Width = 70
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Cart'#227'o d'#233'bito'
    end
    object lbllb14: TLabel
      Left = 50
      Top = 201
      Width = 60
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Cobran'#231'a'
    end
    object lbllb15: TLabel
      Left = 50
      Top = 221
      Width = 60
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Cr'#233'dito'
    end
    object lbllb10: TLabel
      Left = 3
      Top = 262
      Width = 107
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Dif. pagamentos'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lbllb21: TLabel
      Left = 11
      Top = 57
      Width = 99
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Desconto'
    end
    object lbllb7: TLabel
      Left = 234
      Top = 57
      Width = 11
      Height = 13
      Caption = '%'
    end
    object lbllb19: TLabel
      Left = 11
      Top = 78
      Width = 99
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Total a ser pago'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbllb1: TLabel
      Left = 11
      Top = 37
      Width = 99
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Juros'
    end
    object lb4: TLabel
      Left = 32
      Top = 181
      Width = 78
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Cart'#227'o cr'#233'dito'
    end
    object lb1: TLabel
      Left = 11
      Top = 17
      Width = 99
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Multa'
    end
    object sbBuscarDadosDinheiro: TImage
      Left = 281
      Top = 117
      Width = 19
      Height = 18
      Cursor = crHandPoint
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000100000
        001008060000001FF3FF610000000473424954080808087C0864880000000970
        4859730000006F0000006F01F1A2DC430000001974455874536F667477617265
        007777772E696E6B73636170652E6F72679BEE3C1A000000CE4944415478DA63
        FCFFFF3F0336A038ED73022323E37C101BA826F17E16EF026CEA187119A03CED
        4BC37F46867A882286C6BB593C0DA306D0CB00A5E95F6E02293506ECE0D6BD4C
        1E75BC06284EFDECCFC8C4B80EC86442D3FCEFFFBFFF41F7B3793712F402D015
        B9406A129A701ED0F6C9448781D2F4CFBD40E92208EF7FDFBD4CDE62AC619050
        D7DDC0F01F18588C0C8DF31B4BE001C508440A33BE2C05B11F64F04403AD81DB
        9458DF03D783D3007C8046069003400624D574BBFC6564580FF4330F297A8101
        F285F93F4320007D289A4180A704AC0000000049454E44AE426082}
      Visible = False
      OnClick = sbBuscarDadosDinheiroClick
    end
    object sbBuscarDadosCheques: TImage
      Left = 281
      Top = 138
      Width = 19
      Height = 17
      Cursor = crHandPoint
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000100000
        001008060000001FF3FF610000000473424954080808087C0864880000000970
        4859730000006F0000006F01F1A2DC430000001974455874536F667477617265
        007777772E696E6B73636170652E6F72679BEE3C1A000000CE4944415478DA63
        FCFFFF3F0336A038ED73022323E37C101BA826F17E16EF026CEA187119A03CED
        4BC37F46867A882286C6BB593C0DA306D0CB00A5E95F6E02293506ECE0D6BD4C
        1E75BC06284EFDECCFC8C4B80EC86442D3FCEFFFBFFF41F7B3793712F402D015
        B9406A129A701ED0F6C9448781D2F4CFBD40E92208EF7FDFBD4CDE62AC619050
        D7DDC0F01F18588C0C8DF31B4BE001C508440A33BE2C05B11F64F04403AD81DB
        9458DF03D783D3007C8046069003400624D574BBFC6564580FF4330F297A8101
        F285F93F4320007D289A4180A704AC0000000049454E44AE426082}
      OnClick = sbBuscarDadosChequesClick
    end
    object sbBuscarDadosCartoesDebito: TImage
      Left = 281
      Top = 158
      Width = 19
      Height = 17
      Cursor = crHandPoint
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000100000
        001008060000001FF3FF610000000473424954080808087C0864880000000970
        4859730000006F0000006F01F1A2DC430000001974455874536F667477617265
        007777772E696E6B73636170652E6F72679BEE3C1A000000CE4944415478DA63
        FCFFFF3F0336A038ED73022323E37C101BA826F17E16EF026CEA187119A03CED
        4BC37F46867A882286C6BB593C0DA306D0CB00A5E95F6E02293506ECE0D6BD4C
        1E75BC06284EFDECCFC8C4B80EC86442D3FCEFFFBFFF41F7B3793712F402D015
        B9406A129A701ED0F6C9448781D2F4CFBD40E92208EF7FDFBD4CDE62AC619050
        D7DDC0F01F18588C0C8DF31B4BE001C508440A33BE2C05B11F64F04403AD81DB
        9458DF03D783D3007C8046069003400624D574BBFC6564580FF4330F297A8101
        F285F93F4320007D289A4180A704AC0000000049454E44AE426082}
      OnClick = sbBuscarDadosCartoesDebitoClick
    end
    object sbBuscarDadosCartoesCredito: TImage
      Left = 281
      Top = 178
      Width = 19
      Height = 17
      Cursor = crHandPoint
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000100000
        001008060000001FF3FF610000000473424954080808087C0864880000000970
        4859730000006F0000006F01F1A2DC430000001974455874536F667477617265
        007777772E696E6B73636170652E6F72679BEE3C1A000000CE4944415478DA63
        FCFFFF3F0336A038ED73022323E37C101BA826F17E16EF026CEA187119A03CED
        4BC37F46867A882286C6BB593C0DA306D0CB00A5E95F6E02293506ECE0D6BD4C
        1E75BC06284EFDECCFC8C4B80EC86442D3FCEFFFBFFF41F7B3793712F402D015
        B9406A129A701ED0F6C9448781D2F4CFBD40E92208EF7FDFBD4CDE62AC619050
        D7DDC0F01F18588C0C8DF31B4BE001C508440A33BE2C05B11F64F04403AD81DB
        9458DF03D783D3007C8046069003400624D574BBFC6564580FF4330F297A8101
        F285F93F4320007D289A4180A704AC0000000049454E44AE426082}
      OnClick = sbBuscarDadosCartoesCreditoClick
    end
    object sbBuscarDadosCobranca: TImage
      Left = 281
      Top = 198
      Width = 19
      Height = 18
      Cursor = crHandPoint
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000100000
        001008060000001FF3FF610000000473424954080808087C0864880000000970
        4859730000006F0000006F01F1A2DC430000001974455874536F667477617265
        007777772E696E6B73636170652E6F72679BEE3C1A000000CE4944415478DA63
        FCFFFF3F0336A038ED73022323E37C101BA826F17E16EF026CEA187119A03CED
        4BC37F46867A882286C6BB593C0DA306D0CB00A5E95F6E02293506ECE0D6BD4C
        1E75BC06284EFDECCFC8C4B80EC86442D3FCEFFFBFFF41F7B3793712F402D015
        B9406A129A701ED0F6C9448781D2F4CFBD40E92208EF7FDFBD4CDE62AC619050
        D7DDC0F01F18588C0C8DF31B4BE001C508440A33BE2C05B11F64F04403AD81DB
        9458DF03D783D3007C8046069003400624D574BBFC6564580FF4330F297A8101
        F285F93F4320007D289A4180A704AC0000000049454E44AE426082}
      OnClick = sbBuscarDadosCobrancaClick
    end
    object sbBuscarDadosCreditos: TImage
      Left = 281
      Top = 218
      Width = 19
      Height = 18
      Cursor = crHandPoint
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000100000
        001008060000001FF3FF610000000473424954080808087C0864880000000970
        4859730000006F0000006F01F1A2DC430000001974455874536F667477617265
        007777772E696E6B73636170652E6F72679BEE3C1A000000CE4944415478DA63
        FCFFFF3F0336A038ED73022323E37C101BA826F17E16EF026CEA187119A03CED
        4BC37F46867A882286C6BB593C0DA306D0CB00A5E95F6E02293506ECE0D6BD4C
        1E75BC06284EFDECCFC8C4B80EC86442D3FCEFFFBFFF41F7B3793712F402D015
        B9406A129A701ED0F6C9448781D2F4CFBD40E92208EF7FDFBD4CDE62AC619050
        D7DDC0F01F18588C0C8DF31B4BE001C508440A33BE2C05B11F64F04403AD81DB
        9458DF03D783D3007C8046069003400624D574BBFC6564580FF4330F297A8101
        F285F93F4320007D289A4180A704AC0000000049454E44AE426082}
      OnClick = sbBuscarDadosCreditosClick
    end
    object Label1: TLabel
      Left = 11
      Top = 241
      Width = 99
      Height = 14
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Pix/Transfer'#234'ncia'
    end
    object sbBuscarDadosPix: TImage
      Left = 281
      Top = 237
      Width = 19
      Height = 18
      Cursor = crHandPoint
      Picture.Data = {
        0954506E67496D61676589504E470D0A1A0A0000000D49484452000000100000
        001008060000001FF3FF610000000473424954080808087C0864880000000970
        4859730000006F0000006F01F1A2DC430000001974455874536F667477617265
        007777772E696E6B73636170652E6F72679BEE3C1A000000CE4944415478DA63
        FCFFFF3F0336A038ED73022323E37C101BA826F17E16EF026CEA187119A03CED
        4BC37F46867A882286C6BB593C0DA306D0CB00A5E95F6E02293506ECE0D6BD4C
        1E75BC06284EFDECCFC8C4B80EC86442D3FCEFFFBFFF41F7B3793712F402D015
        B9406A129A701ED0F6C9448781D2F4CFBD40E92208EF7FDFBD4CDE62AC619050
        D7DDC0F01F18588C0C8DF31B4BE001C508440A33BE2C05B11F64F04403AD81DB
        9458DF03D783D3007C8046069003400624D574BBFC6564580FF4330F297A8101
        F285F93F4320007D289A4180A704AC0000000049454E44AE426082}
      OnClick = sbBuscarDadosPixClick
    end
    object eValorDinheiro: TEditLuka
      Left = 112
      Top = 114
      Width = 82
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      TabOrder = 6
      Text = '0,00'
      OnChange = checarDefinidos
      OnDblClick = eValorDinheiroDblClick
      OnKeyDown = eValorDinheiroKeyDown
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object eValorCheque: TEditLuka
      Left = 112
      Top = 134
      Width = 82
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      TabOrder = 7
      Text = '0,00'
      OnChange = checarDefinidos
      OnDblClick = eValorDinheiroDblClick
      OnKeyDown = eValorChequeKeyDown
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object eValorCartaoDebito: TEditLuka
      Left = 112
      Top = 153
      Width = 82
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      TabOrder = 8
      Text = '0,00'
      OnChange = checarDefinidos
      OnDblClick = eValorDinheiroDblClick
      OnKeyDown = eValorCartaoDebitoKeyDown
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object eValorCobranca: TEditLuka
      Left = 112
      Top = 193
      Width = 82
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      TabOrder = 10
      Text = '0,00'
      OnChange = checarDefinidos
      OnDblClick = eValorDinheiroDblClick
      OnKeyDown = eValorCobrancaKeyDown
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object eValorCredito: TEditLuka
      Left = 112
      Top = 213
      Width = 82
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      ReadOnly = True
      TabOrder = 11
      Text = '0,00'
      OnChange = checarDefinidos
      OnKeyDown = eValorCreditoKeyDown
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object stPagamento: TStaticText
      Left = -4
      Top = 96
      Width = 208
      Height = 15
      Alignment = taCenter
      AutoSize = False
      Caption = 'Formas de pagamentos'
      Color = 15395562
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 5
      Transparent = False
    end
    object eValorDiferencaPagamentos: TEditLuka
      Left = 112
      Top = 260
      Width = 82
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      CharCase = ecUpperCase
      ReadOnly = True
      TabOrder = 12
      Text = '0,00'
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object eValorDesconto: TEditLuka
      Left = 112
      Top = 50
      Width = 82
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      TabOrder = 2
      Text = '0,00'
      OnChange = eValorDescontoChange
      OnKeyDown = ProximoCampo
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object ePercentualDesconto: TEditLuka
      Left = 190
      Top = 50
      Width = 38
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      MaxLength = 5
      TabOrder = 3
      Text = '0,00'
      OnChange = ePercentualDescontoChange
      OnKeyDown = ProximoCampo
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object eValorTotalASerPago: TEditLuka
      Left = 112
      Top = 70
      Width = 82
      Height = 22
      Alignment = taRightJustify
      CharCase = ecUpperCase
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
      Text = '0,00'
      OnChange = checarDefinidos
      OnKeyDown = ProximoCampo
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = True
    end
    object eValorJuros: TEditLuka
      Left = 112
      Top = 30
      Width = 82
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      TabOrder = 1
      Text = '0,00'
      OnChange = eValorMultaChange
      OnKeyDown = ProximoCampo
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object eValorCartaoCredito: TEditLuka
      Left = 112
      Top = 173
      Width = 82
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      TabOrder = 9
      Text = '0,00'
      OnChange = checarDefinidos
      OnDblClick = eValorDinheiroDblClick
      OnKeyDown = eValorCartaoCreditoKeyDown
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object stValorTotal: TStaticText
      Left = 210
      Top = 96
      Width = 90
      Height = 15
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'Definido'
      Color = 15395562
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 13
      Transparent = False
    end
    object stDinheiroDefinido: TStaticText
      Left = 210
      Top = 117
      Width = 65
      Height = 18
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'N'#195'O'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 14
      Transparent = False
      Visible = False
    end
    object stChequeDefinido: TStaticText
      Left = 210
      Top = 138
      Width = 65
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'N'#195'O'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 15
      Transparent = False
    end
    object stCartaoDebitoDefinido: TStaticText
      Left = 210
      Top = 158
      Width = 65
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'N'#195'O'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 16
      Transparent = False
    end
    object stCobrancaDefinido: TStaticText
      Left = 210
      Top = 198
      Width = 65
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'N'#195'O'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 17
      Transparent = False
    end
    object stCreditoDefinido: TStaticText
      Left = 210
      Top = 218
      Width = 65
      Height = 17
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'N'#195'O'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 18
      Transparent = False
    end
    object stCartaoCreditoDefinido: TStaticText
      Left = 210
      Top = 178
      Width = 65
      Height = 18
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'N'#195'O'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 19
      Transparent = False
    end
    object eValorMulta: TEditLuka
      Left = 112
      Top = 10
      Width = 82
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      TabOrder = 0
      Text = '0,00'
      OnChange = eValorMultaChange
      OnKeyDown = ProximoCampo
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object eValorPix: TEditLuka
      Left = 112
      Top = 233
      Width = 82
      Height = 21
      Alignment = taRightJustify
      CharCase = ecUpperCase
      TabOrder = 20
      Text = '0,00'
      OnChange = checarDefinidos
      OnDblClick = eValorPixDblClick
      OnKeyDown = eValorPixKeyDown
      TipoCampo = tcNumerico
      ApenasPositivo = False
      AsInt = 0
      CasasDecimais = 2
      NaoAceitarEspaco = False
    end
    object stPixDefinido: TStaticText
      Left = 210
      Top = 237
      Width = 65
      Height = 18
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      BevelKind = bkFlat
      Caption = 'N'#195'O'
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 21
      Transparent = False
    end
  end
end
