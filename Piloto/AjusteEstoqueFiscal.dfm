inherited FormAjusteEstoqueFiscal: TFormAjusteEstoqueFiscal
  Caption = 'Corre'#231#227'o de estoque fiscal'
  ClientHeight = 528
  ClientWidth = 1001
  ExplicitWidth = 1007
  ExplicitHeight = 557
  PixelsPerInch = 96
  TextHeight = 14
  object lb7: TLabel [1]
    Left = 534
    Top = 91
    Width = 64
    Height = 14
    Caption = 'Quantidade'
  end
  object lb1: TLabel [2]
    Left = 501
    Top = 2
    Width = 63
    Height = 14
    Caption = 'Observa'#231#227'o'
  end
  object Label2: TLabel [3]
    Left = 127
    Top = 44
    Width = 49
    Height = 14
    Caption = 'Natureza'
  end
  object Label3: TLabel [4]
    Left = 431
    Top = 91
    Width = 76
    Height = 14
    Caption = 'Pre'#231'o unit'#225'rio'
  end
  object Label4: TLabel [5]
    Left = 632
    Top = 91
    Width = 57
    Height = 14
    Caption = 'Valor total'
  end
  object Label5: TLabel [6]
    Left = 521
    Top = 110
    Width = 7
    Height = 14
    Caption = 'X'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel [7]
    Left = 618
    Top = 110
    Width = 6
    Height = 14
    Caption = '='
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited pnOpcoes: TPanel
    Height = 528
    ExplicitHeight = 528
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 947
    Top = 8
    Visible = False
    ExplicitLeft = 947
    ExplicitTop = 8
  end
  inline FrProduto: TFrProdutos
    Left = 226
    Top = 42
    Width = 331
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 226
    ExplicitTop = 42
    ExplicitWidth = 331
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 306
      Height = 23
      TabOrder = 4
      ExplicitWidth = 306
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 2
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 5
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 7
    end
    inherited CkMultiSelecao: TCheckBox
      TabOrder = 3
    end
    inherited PnTitulos: TPanel
      Width = 331
      TabOrder = 0
      ExplicitWidth = 331
      inherited lbNomePesquisa: TLabel
        Width = 42
        Caption = 'Produto'
        ExplicitWidth = 42
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 226
        ExplicitLeft = 226
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 306
      Height = 24
      TabOrder = 1
      ExplicitLeft = 306
      ExplicitHeight = 24
      inherited sbPesquisa: TSpeedButton
        Visible = False
      end
    end
    inherited ckSomenteAtivos: TCheckBox
      TabOrder = 6
    end
  end
  object eQuantidade: TEditLuka
    Left = 534
    Top = 106
    Width = 77
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Color = clMenuBar
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Text = '0,0000'
    OnChange = eQuantidadeChange
    OnKeyDown = eQuantidadeKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    PadraoEstoque = True
    NaoAceitarEspaco = False
  end
  object sgProdutos: TGridLuka
    Left = 125
    Top = 137
    Width = 869
    Height = 350
    Align = alCustom
    ColCount = 13
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 5
    OnDblClick = sgProdutosDblClick
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Und.'
      'Pre'#231'o unit.'
      'Natureza'
      'Quantidade'
      'Qtde. informada'
      'Valor Total'
      'Novo est.f'#237's.'
      'Lote'
      'Dt. fabrica'#231#227'o'
      'Dt. vencimento')
    Grid3D = False
    RealColCount = 14
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      54
      215
      99
      36
      69
      76
      83
      100
      93
      76
      77
      71
      87)
  end
  inline FrMotivoAjusteEstoque: TFrMotivosAjusteEstoque
    Left = 213
    Top = 1
    Width = 285
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 213
    ExplicitTop = 1
    ExplicitWidth = 285
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 260
      Height = 23
      TabOrder = 1
      ExplicitWidth = 260
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 3
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 5
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 7
    end
    inherited PnTitulos: TPanel
      Width = 285
      TabOrder = 0
      ExplicitWidth = 285
      inherited lbNomePesquisa: TLabel
        Width = 155
        Caption = 'Motivo de ajuste do estoque'
        ExplicitWidth = 155
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 180
        ExplicitLeft = 180
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 260
      Height = 24
      TabOrder = 2
      ExplicitLeft = 260
      ExplicitHeight = 24
      inherited sbPesquisa: TSpeedButton
        Visible = False
      end
    end
    inherited ckSomenteAtivos: TCheckBox
      TabOrder = 6
    end
  end
  object eObservacao: TEditLuka
    Left = 501
    Top = 16
    Width = 468
    Height = 22
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 200
    ParentFont = False
    TabOrder = 7
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object cbNatureza: TComboBoxLuka
    Left = 127
    Top = 58
    Width = 92
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    ItemIndex = 0
    TabOrder = 8
    Text = 'Entrada'
    OnKeyDown = ProximoCampo
    Items.Strings = (
      'Entrada'
      'Sa'#237'da')
    Valores.Strings = (
      'E'
      'S')
    AsInt = 0
    AsString = 'E'
  end
  inline FrLote: TFrameLotes
    Left = 133
    Top = 91
    Width = 287
    Height = 41
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 9
    TabStop = True
    ExplicitLeft = 133
    ExplicitTop = 91
    inherited lbLote: TLabel
      Width = 23
      Height = 14
      ExplicitWidth = 23
      ExplicitHeight = 14
    end
    inherited lbDataFabricacao: TLabel
      Width = 75
      Height = 14
      ExplicitWidth = 75
      ExplicitHeight = 14
    end
    inherited lbDataVencimento: TLabel
      Width = 56
      Height = 14
      ExplicitWidth = 56
      ExplicitHeight = 14
    end
    inherited lbBitola: TLabel
      Width = 33
      Height = 14
      ExplicitWidth = 33
      ExplicitHeight = 14
    end
    inherited lbTonalidade: TLabel
      Width = 62
      Height = 14
      ExplicitWidth = 62
      ExplicitHeight = 14
    end
    inherited eLote: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited eDataFabricacao: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
    inherited eDataVencimento: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
    inherited eBitola: TEditLuka
      Height = 22
      OnKeyDown = ProximoCampo
      ExplicitHeight = 22
    end
    inherited eTonalidade: TEditLuka
      Height = 22
      OnKeyDown = ProximoCampo
      ExplicitHeight = 22
    end
  end
  object stEstoqueDisponivel: TStaticTextLuka
    Left = 869
    Top = 112
    Width = 124
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,000'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 3
  end
  object st4: TStaticText
    Left = 869
    Top = 98
    Width = 124
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Est. d'#237'sp. emp.'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 11
    Transparent = False
  end
  object stEstoqueFiscal: TStaticTextLuka
    Left = 744
    Top = 112
    Width = 126
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,000'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 3
  end
  object StaticText1: TStaticText
    Left = 744
    Top = 98
    Width = 126
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Est. fiscal emp.'
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 13
    Transparent = False
  end
  object ePrecoUnitario: TEditLuka
    Left = 430
    Top = 106
    Width = 87
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 14
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    PadraoEstoque = True
    NaoAceitarEspaco = False
  end
  object eValorTotal: TEditLuka
    Left = 632
    Top = 106
    Width = 69
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 15
    Text = '0,0000'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    PadraoEstoque = True
    NaoAceitarEspaco = False
  end
  object ckCalcularEstoqueFinal: TCheckBoxLuka
    Left = 563
    Top = 49
    Width = 241
    Height = 17
    Caption = 'Calcular estoque final automaticamente'
    TabOrder = 16
    CheckedStr = 'N'
  end
  object st5: TStaticText
    Left = 718
    Top = 492
    Width = 109
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Qtde de itens'
    Color = 38619
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 17
    Transparent = False
  end
  object stQuantidadeItensGrid: TStaticText
    Left = 718
    Top = 508
    Width = 109
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 18
    Transparent = False
  end
  object stValorTotalProdutosGrid: TStaticTextLuka
    Left = 849
    Top = 508
    Width = 144
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 19
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object StaticText5: TStaticText
    Left = 849
    Top = 492
    Width = 144
    Height = 16
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Valor total'
    Color = 38619
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 20
    Transparent = False
  end
end
