unit ImpressaoRelacaoControleEntregasGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaImpressaoRelatoriosGrafico, _Biblioteca,
  QRExport, QRPDFFilt, QRCtrls, qrpctrls, QuickRpt, Vcl.ExtCtrls, QRWebFilt;

type
  RecDadosImpressao = record
    Motorista: string;
    QtdeEntregas: Integer;
    QtdeTotalItens: Integer;
    ValorTotalEntregas: Currency;
    PesoTotal: Double;
  end;

  TFormImpressaoRelacaoControleEntregasGrafico = class(TFormHerancaImpressaoRelatoriosGrafico)
    qrbndColumnHeaderBand1: TQRBand;
    qrbndDetailBand1: TQRBand;
    qr8: TQRLabel;
    qr18: TQRLabel;
    qr19: TQRLabel;
    qr3: TQRLabel;
    qr4: TQRLabel;
    qrMotorista: TQRLabel;
    qrQtdeEntregas: TQRLabel;
    qrQtdeItens: TQRLabel;
    qrValorTotalEntregas: TQRLabel;
    qrPesoTotal: TQRLabel;
    procedure qrRelatorioA4BeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioA4NeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrbndDetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    FPosic: Integer;
    FDados: TArray<RecDadosImpressao>;
  public
    { Public declarations }
  end;

procedure Imprimir(
  AOwner: TComponent;
  pDados: TArray<RecDadosImpressao>;
  pFiltrosUtilizados: TArray<string>
);

implementation

{$R *.dfm}

procedure Imprimir(
  AOwner: TComponent;
  pDados: TArray<RecDadosImpressao>;
  pFiltrosUtilizados: TArray<string>
);
var
  vForm: TFormImpressaoRelacaoControleEntregasGrafico;
begin
  vForm := TFormImpressaoRelacaoControleEntregasGrafico.Create(AOwner, pFiltrosUtilizados);

  vForm.FDados := pDados;

  vForm.Preview;

  vForm.Free;
end;

procedure TFormImpressaoRelacaoControleEntregasGrafico.qrbndDetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrMotorista.Caption          := FDados[FPosic].Motorista;
  qrQtdeEntregas.Caption       := NFormatN(FDados[FPosic].QtdeEntregas);
  qrQtdeItens.Caption          := NFormatN(FDados[FPosic].QtdeTotalItens);
  qrValorTotalEntregas.Caption := NFormatN(FDados[FPosic].ValorTotalEntregas);
  qrPesoTotal.Caption          := NFormatNEstoque(FDados[FPosic].PesoTotal);
end;

procedure TFormImpressaoRelacaoControleEntregasGrafico.qrRelatorioA4BeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosic := -1;
end;

procedure TFormImpressaoRelacaoControleEntregasGrafico.qrRelatorioA4NeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosic);
  MoreData := FPosic < Length(FDados);
end;

end.
