unit FrameCadastrosTelefones;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls, _CadastrosTelefones,
  ComboBoxLuka, EditLuka, Vcl.Mask, EditTelefoneLuka, Vcl.Grids, GridLuka, _Biblioteca;

type
  TFrCadastrosTelefones = class(TFrameHerancaPrincipal)
    lb14: TLabel;
    lb11: TLabel;
    lb9: TLabel;
    lb3: TLabel;
    sgTelefones: TGridLuka;
    eTelefone: TEditTelefoneLuka;
    eRamal: TEditLuka;
    eObservacao: TMemo;
    cbTipoTelefone: TComboBoxLuka;
    procedure eObservacaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgTelefonesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTelefonesDblClick(Sender: TObject);
    procedure sgTelefonesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FLinhaAlterando: Integer;
    procedure setCadastrosTelefones(const pContatos: TArray<RecCadastrosTelefones>);
    function getCadastrosTelefones: TArray<RecCadastrosTelefones>;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure Clear; override;
  published
    property Telefones: TArray<RecCadastrosTelefones> read getCadastrosTelefones write setCadastrosTelefones;
  end;

implementation

{$R *.dfm}

{ TFrCadastrosTelefones }

const
  coTipo        = 0;
  coTelefone    = 1;
  coRamal       = 2;
  coObservacao  = 3;

  (* Ocultas *)
  coTipoTelefoneSint = 4;

procedure TFrCadastrosTelefones.Clear;
begin
  inherited;
  _Biblioteca.LimparCampos([ eTelefone, eRamal, eObservacao, sgTelefones]);
end;

constructor TFrCadastrosTelefones.Create(AOwner: TComponent);
begin
  inherited;
  FLinhaAlterando := -1;
end;

procedure TFrCadastrosTelefones.eObservacaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key <> VK_RETURN then
    Exit;

  if cbTipoTelefone.GetValor = '' then begin
    _Biblioteca.Exclamar('O tipo de telefone n�o foi informado corretamente!');
    SetarFoco(cbTipoTelefone);
    Abort;
  end;

  if not eTelefone.getTelefoneOk then begin
    _Biblioteca.Exclamar('O telefone n�o foi informado corretamente!');
    SetarFoco(eTelefone);
    Abort;
  end;

  if (sgTelefones.RowCount = 2) and (sgTelefones.Cells[coTipo, sgTelefones.Row] = '') and (FLinhaAlterando = -1) then
    FLinhaAlterando := 1
  else if (FLinhaAlterando = -1) then begin
    sgTelefones.RowCount := sgTelefones.RowCount + 1;
    FLinhaAlterando      := sgTelefones.RowCount - 1;
  end;

  sgTelefones.Cells[coTipo, FLinhaAlterando]             := cbTipoTelefone.Text;
  sgTelefones.Cells[coTelefone, FLinhaAlterando]         := eTelefone.Text;
  sgTelefones.Cells[coRamal, FLinhaAlterando]            := eRamal.Text;
  sgTelefones.Cells[coObservacao, FLinhaAlterando]       := eObservacao.Text;
  sgTelefones.Cells[coTipoTelefoneSint, FLinhaAlterando] := cbTipoTelefone.GetValor;

  FLinhaAlterando := -1;
  _Biblioteca.LimparCampos([eTelefone, eRamal, eObservacao]);
  _Biblioteca.SetarFoco(cbTipoTelefone);

  ProximoCampo(Sender, Key, Shift);
end;

function TFrCadastrosTelefones.getCadastrosTelefones: TArray<RecCadastrosTelefones>;
var
  i: Integer;
begin
  Result := nil;

  if sgTelefones.Cells[coTipo, 1] = '' then
    Exit;

  SetLength(Result, sgTelefones.RowCount - 1);
  for i := sgTelefones.FixedRows to sgTelefones.RowCount - 1 do begin
    Result[i - sgTelefones.FixedRows].TipoTelefone  := sgTelefones.Cells[coTipoTelefoneSint, i];
    Result[i - sgTelefones.FixedRows].Telefone      := sgTelefones.Cells[coTelefone, i];
    Result[i - sgTelefones.FixedRows].Ramal         := StrToIntDef(sgTelefones.Cells[coRamal, i], 0);
    Result[i - sgTelefones.FixedRows].Observacao    := sgTelefones.Cells[coObservacao, i];
    Result[i - sgTelefones.FixedRows].Ordem         := i - 1;
  end;
end;

procedure TFrCadastrosTelefones.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eTelefone,
    eRamal,
    eObservacao,
    sgTelefones],
    pEditando,
    pLimpar
  );
end;

procedure TFrCadastrosTelefones.setCadastrosTelefones(const pContatos: TArray<RecCadastrosTelefones>);
var
  i: Integer;
begin
  for i := Low(pContatos) to High(pContatos) do begin
    sgTelefones.Cells[coTipo, i + 1]             := pContatos[i].TipoTelefoneAnalitico;
    sgTelefones.Cells[coTelefone, i + 1]         := pContatos[i].Telefone;
    sgTelefones.Cells[coRamal, i + 1]            := NFormatN(pContatos[i].Ramal);
    sgTelefones.Cells[coObservacao, i + 1]       := pContatos[i].Observacao;
    sgTelefones.Cells[coTipoTelefoneSint, i + 1] := pContatos[i].TipoTelefone;
  end;

  sgTelefones.SetLinhasGridPorTamanhoVetor( Length(pContatos) );
end;

procedure TFrCadastrosTelefones.sgTelefonesDblClick(Sender: TObject);
begin
  inherited;
  if sgTelefones.Cells[coTipo, sgTelefones.Row] = '' then
    Exit;

  FLinhaAlterando := sgTelefones.Row;

  cbTipoTelefone.AsString := sgTelefones.Cells[coTipoTelefoneSint, FLinhaAlterando];
  eTelefone.Text          := sgTelefones.Cells[coTelefone, FLinhaAlterando];
  eRamal.Text             := sgTelefones.Cells[coRamal, FLinhaAlterando];
  eObservacao.Lines.Text  := sgTelefones.Cells[coObservacao, FLinhaAlterando];

  SetarFoco(cbTipoTelefone);
end;

procedure TFrCadastrosTelefones.sgTelefonesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol = coTipo then
    vAlinhamento := taCenter
  else if ACol in[coTelefone, coObservacao] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgTelefones.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFrCadastrosTelefones.sgTelefonesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
    sgTelefones.DeleteRow(sgTelefones.Row);
end;

end.
