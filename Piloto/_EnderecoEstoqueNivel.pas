unit _EnderecoEstoqueNivel;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _Sessao, _RecordsCadastros;

{$M+}
type
  TEnderecoEstoqueNivel = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEnderecoEstoqueNivel: RecEnderecoEstoqueNivel;
  end;

function AtualizarEnderecoEstoqueNivel(
  pConexao: TConexao;
  pNivelId: Integer;
  pDescricao: string
): RecRetornoBD;

function BuscarEnderecoEstoqueNivel(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEnderecoEstoqueNivel>;

function ExcluirEnderecoEstoqueNivel(
  pConexao: TConexao;
  pNivelId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEnderecoEstoqueNivel }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where NIVEL_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o',
      True,
      0,
      'where DESCRICAO like :P1 || ''%'' ' +
      'order by ' +
      '  DESCRICAO '
    );
end;

constructor TEnderecoEstoqueNivel.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENDERECO_EST_NIVEL');

  FSql :=
    'select ' +
    '  NIVEL_ID, ' +
    '  DESCRICAO ' +
    'from ' +
    '  ENDERECO_EST_NIVEL ';

  setFiltros(getFiltros);

  AddColuna('NIVEL_ID', True);
  AddColuna('DESCRICAO');
end;

function TEnderecoEstoqueNivel.getRecordEnderecoEstoqueNivel: RecEnderecoEstoqueNivel;
begin
  Result := RecEnderecoEstoqueNivel.Create;
  Result.nivel_id   := getInt('NIVEL_ID', True);
  Result.descricao  := getString('DESCRICAO');
end;

function AtualizarEnderecoEstoqueNivel(
  pConexao: TConexao;
  pNivelId: Integer;
  pDescricao: string
): RecRetornoBD;
var
  t: TEnderecoEstoqueNivel;
  vNovo: Boolean;
  seq: TSequencia;
begin
  Result.Iniciar;
  t := TEnderecoEstoqueNivel.Create(pConexao);

  vNovo := pNivelId = 0;
  if vNovo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_END_EST_NIV_ID');
    pNivelId := seq.getProximaSequencia;
    result.AsInt := pNivelId;
    seq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    t.setInt('NIVEL_ID', pNivelId, True);
    t.setString('DESCRICAO', pDescricao);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarEnderecoEstoqueNivel(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEnderecoEstoqueNivel>;
var
  i: Integer;
  t: TEnderecoEstoqueNivel;
begin
  Result := nil;
  t := TEnderecoEstoqueNivel.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEnderecoEstoqueNivel;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirEnderecoEstoqueNivel(
  pConexao: TConexao;
  pNivelId: Integer
): RecRetornoBD;
var
  t: TEnderecoEstoqueNivel;
begin
  Result.TeveErro := False;
  t := TEnderecoEstoqueNivel.Create(pConexao);

  try
    t.setInt('NIVEL_ID', pNivelId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
