inherited FormArquivos: TFormArquivos
  Caption = 'Inserir anexos'
  ClientHeight = 243
  ClientWidth = 388
  ExplicitWidth = 394
  ExplicitHeight = 272
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Left = 114
    Top = 206
    Width = 271
    Align = alNone
    ExplicitLeft = 114
    ExplicitTop = 206
    ExplicitWidth = 271
    inherited sbFinalizar: TSpeedButton
      Left = 0
      Width = 267
      Height = 33
      Align = alClient
      ExplicitLeft = 0
      ExplicitWidth = 273
      ExplicitHeight = 33
    end
  end
  object sgArquivos: TGridLuka
    Left = 114
    Top = 0
    Width = 271
    Height = 206
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    ColCount = 3
    DefaultRowHeight = 19
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    PopupMenu = pmArquivos
    TabOrder = 1
    OnDrawCell = sgArquivosDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'C'#243'digo'
      'Data/hora cadastro'
      'Arquivo')
    Grid3D = False
    RealColCount = 3
    AtivarPopUpSelecao = False
    ColWidths = (
      45
      121
      270)
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 113
    Height = 244
    BevelKind = bkTile
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 2
    object miInserirNovoArquivo: TSpeedButton
      Left = 0
      Top = 6
      Width = 109
      Height = 30
      Caption = 'Anexar docs'
      Flat = True
      NumGlyphs = 2
      OnClick = miInserirNovoArquivoClick
    end
    object miDeletarArquivoFocado: TSpeedButton
      Left = 0
      Top = 42
      Width = 109
      Height = 32
      Caption = 'Excluir anexo'
      Flat = True
      NumGlyphs = 2
      OnClick = miDeletarArquivoFocadoClick
    end
    object miSalvarArquivoFocado: TSpeedButton
      Left = -10
      Top = 122
      Width = 130
      Height = 31
      Caption = 'Salvar anexo'
      Flat = True
      NumGlyphs = 2
      Visible = False
    end
  end
  object pmArquivos: TPopupMenu
    Left = 288
    Top = 96
    object miN1: TMenuItem
      Caption = '-'
    end
    object Salvararquivo1: TMenuItem
      Caption = 'Salvar arquivo'
      OnClick = Salvararquivo1Click
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 176
    Top = 136
  end
end
