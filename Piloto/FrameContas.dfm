inherited FrContas: TFrContas
  inherited CkAspas: TCheckBox
    Checked = True
    State = cbChecked
  end
  inherited PnTitulos: TPanel
    inherited lbNomePesquisa: TLabel
      Width = 34
      Caption = 'Contas'
      ExplicitWidth = 34
    end
  end
  object rgTipo: TRadioGroupLuka [8]
    Left = 247
    Top = 14
    Width = 132
    Height = 64
    Caption = ' Tipo '
    ItemIndex = 2
    Items.Strings = (
      'Caixas'
      'Bancos'
      'Ambos')
    TabOrder = 8
    Visible = False
    Valores.Strings = (
      'C'
      'B'
      ' ')
  end
  object ckAutorizados: TCheckBox [9]
    Left = 144
    Top = 56
    Width = 97
    Height = 17
    Caption = 'Autorizados'
    TabOrder = 9
    Visible = False
  end
end
