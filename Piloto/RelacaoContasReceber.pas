unit RelacaoContasReceber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, Vcl.ComCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, FrameEmpresas, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameClientes,
  Vcl.StdCtrls, GroupBoxLuka, FrameDataInicialFinal, FrameTiposCobranca, _RelacaoContasReceber,
  _RecordsRelatorios, _Sessao, _Biblioteca, Vcl.Grids, GridLuka, System.StrUtils, System.Math,
  Informacoes.TituloReceber, Vcl.Menus, _Imagens, BaixarTitulosReceber, _ContasReceber,
  VCLTee.TeEngine, VCLTee.Series, VCLTee.TeeProcs, VCLTee.Chart, _RecordsEspeciais,
  _ContasReceberBaixas, InclusaoTitulosReceber, EnviarTitulosReceberCaixa, DefinirContaCustodiaContasReceber,
  VclTee.TeeGDIPlus, Frame.Inteiros, FramePortadores, FrameCentroCustos, Informacoes.TituloPagar,
  Frame.Cadastros, Emissao.BoletosBancario, StaticTextLuka, CheckBoxLuka, _ContasPagar, _RecordsFinanceiros,
  FrameTextos, EditLuka, FrameVendedores, ComboBoxLuka, EdicaoInformacoesContasReceber,
  FrameFaixaNumeros, AdiantamentoTituloFinanceiro, FrameContasCustodia, ImpressaoMeiaPaginaDuplicataMercantil,
  FrameListaNegra, FrameGruposClientes, Data.DB, MemDS, DBAccess, Ora,
  Datasnap.DBClient, frxClass, frxDBSet, OraCall, FramePlanosFinanceiros, BuscaDados,
  SpeedButtonLuka, _ContasBoletos;

type
  RecDadosCobrancas = record
    cobranca_id: Integer;
    NomeTipoCobranca: string;
    valor_titulos: Currency;
    ValorRetencao: Currency;
    valor_juros: Currency;
    valor_total: Currency;
    qtde_titulos: Integer;
  end;

  TFormRelacaoContasReceber = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrDataCadastro: TFrDataInicialFinal;
    FrDataVencimento: TFrDataInicialFinal;
    gbStatusFinanceiro: TGroupBoxLuka;
    ckTitulosEmAberto: TCheckBox;
    ckTitulosBaixados: TCheckBox;
    rgOrdenacao: TRadioGroup;
    gbFormasPagamento: TGroupBoxLuka;
    ckTipoDinheiro: TCheckBox;
    ckTipoCartao: TCheckBox;
    ckTipoCheque: TCheckBox;
    ckTipoCobranca: TCheckBox;
    pmRotinas: TPopupMenu;
    pcResultado: TPageControl;
    tsAnalitico: TTabSheet;
    sgTitulos: TGridLuka;
    tsSinteticoTipoCobranca: TTabSheet;
    pnAnalitico: TPanel;
    st3: TStaticText;
    st5: TStaticText;
    st1: TStaticText;
    st4: TStaticText;
    st6: TStaticText;
    st7: TStaticText;
    sgTitulosTipoCobranca: TGridLuka;
    pnGraficoTiposCobranca: TPanel;
    chTiposCobranca: TChart;
    rgTipoRepresentacaoGrafico: TRadioGroup;
    FrDataBaixa: TFrDataInicialFinal;
    FrDataPagamento: TFrDataInicialFinal;
    FrCodigoPedido: TFrameInteiros;
    FrPortadores: TFrPortadores;
    FrCentrosCustos: TFrCentroCustos;
    psrsSeries1: TPieSeries;
    FrTiposCobranca: TFrTiposCobranca;
    FrClientes: TFrameCadastros;
    FrCodigoTitulo: TFrameInteiros;
    ckAcumulado: TCheckBox;
    st8: TStaticText;
    FrCodigoBaixa: TFrameInteiros;
    FrCodigoAcumulado: TFrameInteiros;
    st2: TStaticText;
    stValorCheque: TStaticTextLuka;
    stValorCartao: TStaticTextLuka;
    stValorCobranca: TStaticTextLuka;
    stValorAcumulado: TStaticTextLuka;
    stValorTotal: TStaticTextLuka;
    stQuantidadeTitulos: TStaticTextLuka;
    stValorChequeSelecionados: TStaticTextLuka;
    stValorCartaoSelecionados: TStaticTextLuka;
    stValorCobrancaSelecionados: TStaticTextLuka;
    stValorAcumuladosSelecionados: TStaticTextLuka;
    stValorTotalSelecionados: TStaticTextLuka;
    stQtdeTitulosSelecionados: TStaticTextLuka;
    ckTrazerCreditosPagar: TCheckBoxLuka;
    FrDocumento: TFrTextos;
    lb9: TLabel;
    ePercMulta: TEditLuka;
    ePercJuros: TEditLuka;
    lb1: TLabel;
    FrNossoNumero: TFrTextos;
    FrVendedores: TFrVendedores;
    lb2: TLabel;
    cbSituacaoRemessa: TComboBoxLuka;
    FrValorDocumento: TFrFaixaNumeros;
    FrContasCustodia: TFrContasCustodia;
    FrListaNegra: TFrListaNegra;
    FrDataEmissao: TFrDataInicialFinal;
    FrGruposClientes: TFrGruposClientes;
    miBaixarTitulo: TSpeedButton;
    miBaixarTitulosSelecionados: TSpeedButton;
    Panel1: TPanel;
    miMarcarDesmarcarTItuloFocado: TSpeedButton;
    miRealizarAdiantamentoTituloFocado: TSpeedButton;
    miMarcarDesmarcarTodosTitulos: TSpeedButton;
    miAlterarInformacoes: TSpeedButton;
    miDefinirContaCustodia: TSpeedButton;
    miExcluirTitulo: TSpeedButton;
    miReimprimirDuplicataMercantilVenda: TSpeedButton;
    miEmitirBoleto: TSpeedButton;
    miEnviarParaRecebimentoCaixa: TSpeedButton;
    miIncluirNovoTitulo: TSpeedButton;
    frxReport: TfrxReport;
    dstReceber: TfrxDBDataset;
    cdsReceber: TClientDataSet;
    cdsReceberCodigo: TStringField;
    cdsReceberDocumento: TStringField;
    cdsReceberNomeCliente: TStringField;
    cdsRecebersituacao: TStringField;
    cdsReceberValor: TFloatField;
    cdsReceberMulta: TFloatField;
    cdsReceberJuros: TFloatField;
    cdsReceberAdiantamento: TFloatField;
    cdsReceberValorLiquido: TFloatField;
    cdsReceberDtVencto: TDateField;
    cdsReceberDtCadastro: TDateField;
    cdsReceberEmpresa: TStringField;
    cdsReceberTipoCobranca: TStringField;
    cdsReceberDiasAtraso: TIntegerField;
    cdsReceberParcela: TStringField;
    Label1: TLabel;
    eNomeEmitenteCheque: TEditLuka;
    miAlteraVendedor: TSpeedButton;
    SpeedButton1: TSpeedButton;
    Label2: TLabel;
    cbAgrupamento: TComboBoxLuka;
    FrPlanosFinanceiros: TFrPlanosFinanceiros;
    miAlterarCampoDocumento: TSpeedButton;
    SpeedButton2: TSpeedButton;
    TabSheet1: TTabSheet;
    gbOrigem: TGroupBoxLuka;
    ckNfe: TCheckBoxLuka;
    ckNFCe: TCheckBoxLuka;
    CheckBoxLuka1: TCheckBoxLuka;
    CheckBoxLuka3: TCheckBoxLuka;
    CheckBoxLuka4: TCheckBoxLuka;
    CheckBoxLuka5: TCheckBoxLuka;
    CheckBoxLuka6: TCheckBoxLuka;
    CheckBoxLuka10: TCheckBoxLuka;
    SpeedButton3: TSpeedButton;
    StaticText1: TStaticText;
    stValorRetencaoSelecionados: TStaticTextLuka;
    stValorRetencao: TStaticTextLuka;
    ckProcessarArquivoRetorno: TCheckBoxLuka;
    sbPesquisaCaminhoArquivo: TSpeedButtonLuka;
    Label3: TLabel;
    eCaminhoArquivo: TEditLuka;
    cdsOcorrencias: TClientDataSet;
    cdsOcorrenciasRAZAO_SOCIAL: TStringField;
    cdsOcorrenciasDOCUMENTO_ID: TStringField;
    cdsOcorrenciasNOSSO_NUMERO: TStringField;
    cdsOcorrenciasVLR_ABATIMENTO: TFloatField;
    cdsOcorrenciasVLR_TITULO: TFloatField;
    cdsOcorrenciasPAGAMENTO: TStringField;
    cdsOcorrenciasDESC_OCORRENCIA: TStringField;
    cdsOcorrenciasDETALHES_OCORRENCIA: TStringField;
    cdsOcorrenciasCOD_OCORRENCIA: TStringField;
    cdsOcorrenciasDIFERENCA: TFloatField;
    cdsOcorrenciasVLR_ORIGINAL: TFloatField;
    cdsSumario: TClientDataSet;
    cdsSumarioCOD_OCORRENCIA: TIntegerField;
    cdsSumarioOCORRENCIA: TStringField;
    cdsSumarioQTD: TIntegerField;
    cdsSumarioTOTAL: TFloatField;
    cdsPortador: TClientDataSet;
    cdsPortadorPORTADOR_ID: TIntegerField;
    cdsPortadorPORTADOR_NOME: TStringField;
    cdsPortadorCONTA_CORRENTE_ID: TStringField;
    cdsPortadorCONTA_CORRENTE: TStringField;
    cdsPortadorVLR_PAGO: TFloatField;
    cdsPortadorVLR_JUROS: TFloatField;
    cdsPortadorTOTAL_BRUTO: TFloatField;
    cdsPortadorVLR_TAXAS: TFloatField;
    cdsPortadorTOTAL_LIQUIDO: TFloatField;
    frxDBOcorrencias: TfrxDBDataset;
    frxDBTitulos: TfrxDBDataset;
    frxDBSumario: TfrxDBDataset;
    frxDBPortador: TfrxDBDataset;
    frxOcorrencias: TfrxReport;
    frxDiferencas: TfrxReport;
    cdsTitulos: TClientDataSet;
    cdsTitulosCADASTRO_ID: TStringField;
    cdsTitulosRAZAO_SOCIAL: TStringField;
    cdsTitulosDOCUMENTO_ID: TStringField;
    cdsTitulosNOSSO_NUMERO: TStringField;
    cdsTitulosCODIGO_BANCO: TStringField;
    cdsTitulosDT_EMISSAO: TStringField;
    cdsTitulosDT_VENCIMENTO: TStringField;
    cdsTitulosVALOR_TITULO: TFloatField;
    cdsTitulosVLR_ADIANTAMENTO: TFloatField;
    SpeedButton4: TSpeedButton;
    procedure sgTitulosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgTitulosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgTitulosDblClick(Sender: TObject);
    procedure sgTitulosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure sgTitulosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure pmRotinasPopup(Sender: TObject);
    procedure miBaixarTituloClick(Sender: TObject);
    procedure miAlterarInformacoes0(Sender: TObject);
    procedure sgTitulosTipoCobrancaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure rgTipoRepresentacaoGraficoClick(Sender: TObject);
    procedure miIncluirNovoTituloClick(Sender: TObject);
    procedure miEnviarParaRecebimentoCaixaClick(Sender: TObject);
    procedure miMarcarDesmarcarTItuloFocadoClick(Sender: TObject);
    procedure miMarcarDesmarcarTodosTitulosClick(Sender: TObject);
    procedure sgTitulosTipoCobrancaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure miExcluirTituloClick(Sender: TObject);
    procedure miEmitirBoletoClick(Sender: TObject);
    procedure miAlterarDataVencimentoClick(Sender: TObject);
    procedure miAlterarParcelaNumeroParcelasClick(Sender: TObject);
    procedure miAlterarInformacoesClick(Sender: TObject);
    procedure miRealizarAdiantamentoTituloFocadoClick(Sender: TObject);
    procedure miDefinirContaCustodiaClick(Sender: TObject);
    procedure miReimprimirDuplicataMercantilVendaClick(Sender: TObject);
    procedure ckTipoChequeClick(Sender: TObject);
    procedure miAlteraVendedorClick(Sender: TObject);
    procedure miAlterarCampoDocumentoClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure sbPesquisaCaminhoArquivoClick(Sender: TObject);
    procedure ckProcessarArquivoRetornoClick(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
  private
    FTitulos: TArray<_RecordsRelatorios.RecContasReceber>;
    FDadosCobrancas: TArray<RecDadosCobrancas>;
    procedure ImprimirRetorno(pCdsOcorrencias: TClientDataSet;
                             pEmpresaID: Integer;
                             pPortadorID: string;
                             pVlrPago, pVlrJuros, pVlrTaxas: Double);

    procedure TotalizarSelecionados;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses untDmRelatorio, PesquisaFuncionarios, _RecordsCadastros, BuscarCentroCusto,
  _EmitirBoletos, EditarInformacoesMemo;

{ TFormRelacaoContasReceber }

const
(* Grid t�tulos *)
  tiSelecionado     = 0;
  tiReceberId       = 1;
  tiDocumento       = 2;
  tiCliente         = 3;
  tiStatusAnalitico = 4;
  tiValorPedido     = 5;
  tiParcela         = 6;
  tiValorDocumento  = 7;
  tiValorAdiantado  = 8;
  tiValorMulta      = 9;
  tiValorJuros      = 10;
  tiValorRetencao   = 11;
  tiSaldoLiquido    = 12;
  tiDataVencimento  = 13;
  tiDataCadastro    = 14;
  tiDiasAtraso      = 15;
  tiEmpresa         = 16;
  tiTipoCobranca    = 17;
  tiPortador        = 18;

  // Campos ocultos
  tiStatusSintetico = 19;
  tiFormaPagamento  = 20;
  tiClienteId       = 21;
  tiNoCaixa         = 22;
  tiBoleto          = 23;
  tiOrigem          = 24;
  tiTipoLinha       = 25;
  tiObservacoes     = 26;

(* Grid de tipos de cobran�a *)
  tcCobranca        = 0;
  tcValorDocumentos = 1;
  tcValorMulta      = 2;
  tcValorJuros      = 3;
  tcValorRetencoes  = 4;
  tcValorTotal      = 5;
  tcQtdeTitulos     = 6;

  coLinhaDetalhe   = 'D';
  coLinhaCabAgrup  = 'CAGRU';
  coLinhaTotVenda  = 'TV';
  coTotalAgrupador = 'TAGRU';

procedure TFormRelacaoContasReceber.Carregar(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vLinha: Integer;
  vComando: string;
  vAdionou: Boolean;
  vAgrupadorId: Integer;
  vTotalContasPagar: Integer;

  vCadastrosIds: TArray<Integer>;
  vTiposCobrancasIds: TArray<Integer>;
  vContasPagar: TArray<_RecordsFinanceiros.RecContaPagar>;


  vBoletos: TBoletosACBr;
  vParametrosImpressao: TParametrosImpressao;
  vNossoNumeroBoletos: TArray<string>;

  vTotais: record
    totalPedidos: Double;
    totalValorBruto: Double;
    totalAdiantamento: Double;
    totalMulta: Double;
    totalJuros: Double;
    totalReten��o: Double;
    totalValorLiquido: Double;
  end;

  vTotaisAgrupador: record
    totalPedidos: Double;
    totalValorBruto: Double;
    totalAdiantamento: Double;
    totalMulta: Double;
    totalJuros: Double;
    totalReten��o: Double;
    totalValorLiquido: Double;
  end;

  procedure TotalizarGrupo;
  begin
    Inc(vLinha);

    sgTitulos.Cells[tiCliente, vLinha]        := 'TOTAL DO CLIENTE: ';
    sgTitulos.Cells[tiValorPedido, vLinha]    := NFormatN(vTotaisAgrupador.totalPedidos);
    sgTitulos.Cells[tiValorDocumento, vLinha] := NFormatN(vTotaisAgrupador.totalValorBruto);
    sgTitulos.Cells[tiValorAdiantado, vLinha] := NFormatN(vTotaisAgrupador.totalAdiantamento);
    sgTitulos.Cells[tiValorMulta, vLinha]     := NFormatN(vTotaisAgrupador.totalMulta);
    sgTitulos.Cells[tiValorJuros, vLinha]     := NFormatN(vTotaisAgrupador.totalJuros);
    sgTitulos.Cells[tiValorRetencao, vLinha]  := NFormatN(vTotaisAgrupador.totalReten��o);
    sgTitulos.Cells[tiSaldoLiquido, vLinha]   := NFormatN(vTotaisAgrupador.totalValorLiquido);
    sgTitulos.Cells[tiTipoLinha, vLinha]      := coTotalAgrupador;
    sgTitulos.Cells[tiSelecionado, vLinha]    := '---';

    vTotaisAgrupador.totalPedidos      := 0;
    vTotaisAgrupador.totalValorBruto   := 0;
    vTotaisAgrupador.totalAdiantamento := 0;
    vTotaisAgrupador.totalMulta        := 0;
    vTotaisAgrupador.totalJuros        := 0;
    vTotaisAgrupador.totalReten��o     := 0;
    vTotaisAgrupador.totalValorLiquido := 0;
    Inc(vLinha);
  end;
begin
  inherited;
  vComando := '';
  vContasPagar := nil;
  vCadastrosIds := nil;
  FDadosCobrancas := nil;
  vTiposCobrancasIds := nil;
  vTotalContasPagar := 0;

  if ckProcessarArquivoRetorno.Checked then
  begin

    vBoletos := TBoletosACBr.Create;
    vParametrosImpressao:= TParametrosImpressao.Create;
    try
      vParametrosImpressao.ContasBoletosId := FrPortadores.getPortador().ContasBoletoId;
      vParametrosImpressao.CaminhoArqRetorno := eCaminhoArquivo.Text;

      vBoletos.PortadorId := FrPortadores.getPortador().PortadorId;
      vBoletos.LerArquivoRetorno(vParametrosImpressao);
      ImprimirRetorno(vBoletos.CdsOcorrencias,
                     Sessao.getEmpresaLogada.EmpresaId,
                     FrPortadores.getPortador().PortadorId,
                     vBoletos.VlrPagoTot,
                     vBoletos.VlrJurosTot,
                     vBoletos.VlrTaxasTot);

      vNossoNumeroBoletos := nil;
      vBoletos.CdsTitulos.First;
      SetLength(vNossoNumeroBoletos, vBoletos.CdsTitulos.RecordCount - 1);

      while not vBoletos.CdsTitulos.Eof do
      begin
        if vBoletos.CdsTitulos.FieldByName('PAGAMENTO').AsString = 'Sim' then
          vNossoNumeroBoletos[vBoletos.CdsTitulos.RecNo - 1] := vBoletos.CdsTitulos.FieldByName('NOSSO_NUMERO').AsString;

        vBoletos.CdsTitulos.Next;
      end;

    finally
      vParametrosImpressao.Free;
      vBoletos.Free;
    end;

    if Length(vNossoNumeroBoletos) = 0 then
      vComando := ' where 1 = 2'
    else
      vComando :=
        'where CON.PORTADOR_ID =  ' + FrPortadores.getPortador().PortadorId + ' ' +
        'and ' + FiltroInStr('CON.NOSSO_NUMERO ', vNossoNumeroBoletos) + ' ' +
        'and CON.EMPRESA_ID = ' + IntToStr(Sessao.getEmpresaLogada.EmpresaId) + ' ' +
        'order by ' +
        '  CON.DATA_VENCIMENTO desc ';

  end else
  begin
    if not FrCodigoPedido.EstaVazio then
      WhereOuAnd(vComando, FrCodigoPedido.getSqlFiltros('CON.ORCAMENTO_ID'));

    if not FrClientes.EstaVazio then
      WhereOuAnd(vComando, FrClientes.getSqlFiltros('CON.CADASTRO_ID'));

    if not FrEmpresas.EstaVazio then
      WhereOuAnd(vComando, FrEmpresas.getSqlFiltros('CON.EMPRESA_ID'));

    if not FrTiposCobranca.EstaVazio then
      WhereOuAnd(vComando, FrTiposCobranca.getSqlFiltros('CON.COBRANCA_ID'));

    if not FrDataCadastro.EstaVazio then
      WhereOuAnd(vComando, FrDataCadastro.getSqlFiltros('CON.DATA_CADASTRO'));

    if not FrDataVencimento.EstaVazio then
      WhereOuAnd(vComando, FrDataVencimento.getSqlFiltros('CON.DATA_VENCIMENTO'));

    if not FrDataEmissao.EstaVazio then
      WhereOuAnd(vComando, FrDataEmissao.getSqlFiltros('CON.DATA_EMISSAO'));

    if not FrDataBaixa.EstaVazio then
      WhereOuAnd(vComando, FrDataBaixa.getSqlFiltros('BAX.DATA_HORA_BAIXA'));

    if not FrDataPagamento.EstaVazio then
      WhereOuAnd(vComando, FrDataPagamento.getSqlFiltros('BAX.DATA_PAGAMENTO'));

    if not FrPortadores.EstaVazio then
      WhereOuAnd(vComando, FrPortadores.getSqlFiltros('CON.PORTADOR_ID'));

    if not FrCentrosCustos.EstaVazio then
      WhereOuAnd(vComando, FrCentrosCustos.getSqlFiltros('CON.CENTRO_CUSTO_ID'));

    if not gbOrigem.NenhumMarcado then
      WhereOuAnd(vComando, gbOrigem.GetSql('CON.ORIGEM'));

    if not FrCodigoTitulo.EstaVazio then
      WhereOuAnd(vComando, FrCodigoTitulo.getSqlFiltros('CON.RECEBER_ID'));

    if not FrCodigoBaixa.EstaVazio then
      WhereOuAnd(vComando, FrCodigoBaixa.getSqlFiltros('CON.BAIXA_ID'));

    if not FrCodigoAcumulado.EstaVazio then
      WhereOuAnd(vComando, FrCodigoAcumulado.getSqlFiltros('CON.ACUMULADO_ID'));

    if not FrDocumento.EstaVazio then
      WhereOuAnd(vComando, FrDocumento.TrazerFiltros('CON.DOCUMENTO'));

    if not FrVendedores.EstaVazio then
      WhereOuAnd(vComando, FrVendedores.getSqlFiltros('CON.VENDEDOR_ID'));

    if not FrNossoNumero.EstaVazio then
      WhereOuAnd(vComando, FrNossoNumero.TrazerFiltros('to_number(CON.NOSSO_NUMERO)'));

    _Biblioteca.WhereOuAnd(vComando, FrValorDocumento.getSQL('CON.VALOR_DOCUMENTO'));

    if cbSituacaoRemessa.GetValor <> 'NaoFiltrar' then begin
      if cbSituacaoRemessa.GetValor = 'NaoGerado' then
        WhereOuAnd(vComando, 'CON.REMESSA_ID is null')
      else
        WhereOuAnd(vComando, 'CON.REMESSA_ID is not null');
    end;

    WhereOuAnd(vComando, ' TCO.FORMA_PAGAMENTO in(');

    vAdionou := False;
    if ckTipoDinheiro.Checked then begin
      vComando := vComando + '''DIN''';
      vAdionou := True;
    end;

    if ckTipoCheque.Checked then begin
      vComando := vComando + IfThen(vAdionou, ',') + '''CHQ''';
      vAdionou := True;
    end;

    if ckTipoCobranca.Checked then begin
      vComando := vComando + IfThen(vAdionou, ',') + '''COB''';
      vAdionou := True;
    end;

    if ckTipoCartao.Checked then
      vComando := vComando + IfThen(vAdionou, ',') + '''CRT''';

    if ckAcumulado.Checked then
      vComando := vComando + IfThen(vAdionou, ',') + '''ACU''';

    vComando := vComando + ') ';

    if not gbStatusFinanceiro.TodosMarcados then begin
      if ckTitulosEmAberto.Checked then
        WhereOuAnd(vComando, ' CON.STATUS = ''A'' ')
      else if ckTitulosBaixados.Checked then
        WhereOuAnd(vComando, ' CON.STATUS = ''B'' ');
    end;

    FrListaNegra.getSqlFiltros('CLI.LISTA_NEGRA_ID', vComando, True);
    FrContasCustodia.getSqlFiltros('CON.CONTA_CUSTODIA_ID', vComando, True);
    FrGruposClientes.getSqlFiltros('CLI.CLIENTE_GRUPO_ID', vComando, True);

    if not FrPlanosFinanceiros.EstaVazio then
      WhereOuAnd(vComando, FrPlanosFinanceiros.getSqlFiltros('CON.PLANO_FINANCEIRO_ID'));

    if (ckTipoCheque.Checked) and (eNomeEmitenteCheque.Text <> '') then
      WhereOuAnd(vComando, ' CON.NOME_EMITENTE LIKE ''%' + eNomeEmitenteCheque.Text + '%'' ');

    vComando := vComando + ' order by ';

    if cbAgrupamento.GetValor = 'CLI' then
      vComando := vComando + ' CON.CADASTRO_ID, CON.DATA_CADASTRO '
    else begin
      case rgOrdenacao.ItemIndex of
        0: vComando := vComando + ' CON.CADASTRO_ID ';
        1: vComando := vComando + ' CAD.RAZAO_SOCIAL ';
        2: vComando := vComando + ' CON.DATA_CADASTRO ';
        3: vComando := vComando + ' CON.DATA_VENCIMENTO ';
        4: vComando := vComando + ' CON.COBRANCA_ID ';
        5: vComando := vComando + ' CON.STATUS ';
      end;
    end;
  end;
  FTitulos := _RelacaoContasReceber.BuscarContasReceberComando(
    Sessao.getConexaoBanco,
    vComando
  );
  if FTitulos = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vAgrupadorId := -1;

  vTotais.totalPedidos      := 0;
  vTotais.totalValorBruto   := 0;
  vTotais.totalAdiantamento := 0;
  vTotais.totalMulta        := 0;
  vTotais.totalJuros        := 0;
  vTotais.totalReten��o     := 0;
  vTotais.totalValorLiquido := 0;

  vTotaisAgrupador.totalPedidos      := 0;
  vTotaisAgrupador.totalValorBruto   := 0;
  vTotaisAgrupador.totalAdiantamento := 0;
  vTotaisAgrupador.totalMulta        := 0;
  vTotaisAgrupador.totalJuros        := 0;
  vTotaisAgrupador.totalReten��o     := 0;
  vTotaisAgrupador.totalValorLiquido := 0;

  vLinha := 0;
  for i := Low(FTitulos) to High(FTitulos) do begin
    if cbAgrupamento.GetValor = 'CLI' then begin
      if vAgrupadorId <> FTitulos[i].cliente_id then begin
        Inc(vLinha);
        if i > 0 then begin
          if ckTrazerCreditosPagar.Checked then begin

            vContasPagar :=
              _ContasPagar.BuscarContasPagarComando(
                Sessao.getConexaoBanco,
                'where COP.CADASTRO_ID = ' + IntToStr(vAgrupadorId) + ' ' +
                'and COP.STATUS = ''A'' ' +
                'and COP.COBRANCA_ID in(' + IntToStr(Sessao.getParametros.TipoCobrancaGerCredImpId) + ', ' + IntToStr(Sessao.getParametros.TipoCobrancaGeracaoCredId) + ') '
              );

            vTotalContasPagar := vTotalContasPagar + Length(vContasPagar);

            for j := Low(vContasPagar) to High(vContasPagar) do begin
              Inc(vLinha);

              sgTitulos.Cells[tiSelecionado, vLinha]     := '---';
              sgTitulos.Cells[tiReceberId, vLinha]       := NFormat(vContasPagar[j].PagarId);
              sgTitulos.Cells[tiDocumento, vLinha]       := vContasPagar[j].documento;
              sgTitulos.Cells[tiCliente, vLinha]         := getInformacao(vContasPagar[j].CadastroId, vContasPagar[j].nome_fornecedor);
              sgTitulos.Cells[tiParcela, vLinha]         := NFormat(vContasPagar[j].parcela) + '/' + NFormat(vContasPagar[j].numero_parcelas);
              sgTitulos.Cells[tiValorDocumento, vLinha]  := NFormatN(vContasPagar[j].ValorDocumento);
              sgTitulos.Cells[tiValorJuros, vLinha]      := '';
              sgTitulos.Cells[tiSaldoLiquido, vLinha]    := NFormatN(vContasPagar[j].ValorDocumento);
              sgTitulos.Cells[tiDataCadastro, vLinha]    := DFormat(vContasPagar[j].data_cadastro);
              sgTitulos.Cells[tiDataVencimento, vLinha]  := DFormat(vContasPagar[j].data_vencimento);
              sgTitulos.Cells[tiDiasAtraso, vLinha]      := NFormatN(vContasPagar[j].dias_atraso);
              sgTitulos.Cells[tiEmpresa, vLinha]         := NFormat(vContasPagar[j].empresa_id) + ' - ' + vContasPagar[j].nome_empresa;
              sgTitulos.Cells[tiStatusAnalitico, vLinha] := 'Ab';
              sgTitulos.Cells[tiTipoCobranca, vLinha]    := NFormat(vContasPagar[j].cobranca_id) + ' - ' + vContasPagar[j].nome_tipo_cobranca;
              sgTitulos.Cells[tiPortador, vLinha]        := getInformacao(vContasPagar[j].PortadorId, vContasPagar[j].NomePortador);
              sgTitulos.Cells[tiStatusSintetico, vLinha] := vContasPagar[j].status;
              sgTitulos.Cells[tiFormaPagamento, vLinha]  := 'CREP';
              sgTitulos.Cells[tiClienteId, vLinha]       := NFormat(vContasPagar[j].CadastroId);

              stValorCobranca.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, vLinha]) * -1 );
              stValorRetencao.Somar( SFormatDouble(sgTitulos.Cells[tiValorRetencao, vLinha]) * -1 );

              vTotais.totalPedidos      := vTotais.totalPedidos - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorPedido, vLinha]);
              vTotais.totalValorBruto   := vTotais.totalValorBruto - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorDocumento, vLinha]);
              vTotais.totalAdiantamento := vTotais.totalAdiantamento - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorAdiantado, vLinha]);
              vTotais.totalMulta        := vTotais.totalMulta - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorMulta, vLinha]);
              vTotais.totalJuros        := vTotais.totalJuros - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorJuros, vLinha]);
              vTotais.totalReten��o     := vTotais.totalReten��o - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorRetencao, vLinha]);
              vTotais.totalValorLiquido := vTotais.totalValorLiquido - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, vLinha]);

              vTotaisAgrupador.totalPedidos      := vTotaisAgrupador.totalPedidos - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorPedido, vLinha]);
              vTotaisAgrupador.totalValorBruto   := vTotaisAgrupador.totalValorBruto - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorDocumento, vLinha]);
              vTotaisAgrupador.totalAdiantamento := vTotaisAgrupador.totalAdiantamento - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorAdiantado, vLinha]);
              vTotaisAgrupador.totalMulta        := vTotaisAgrupador.totalMulta - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorMulta, vLinha]);
              vTotaisAgrupador.totalJuros        := vTotaisAgrupador.totalJuros - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorJuros, vLinha]);
              vTotaisAgrupador.totalReten��o     := vTotaisAgrupador.totalReten��o - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorRetencao, vLinha]);
              vTotaisAgrupador.totalValorLiquido := vTotaisAgrupador.totalValorLiquido - _Biblioteca.SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, vLinha]);
            end;
          end;

          TotalizarGrupo;
        end;

        sgTitulos.Cells[tiCliente, vLinha]   := getInformacao(FTitulos[i].cliente_id, FTitulos[i].nome_cliente);
        sgTitulos.Cells[tiTipoLinha, vLinha] := coLinhaCabAgrup;
        sgTitulos.Cells[tiSelecionado, vLinha] := '---';

        vAgrupadorId := FTitulos[i].cliente_id;
      end;
    end;

    Inc(vLinha);

    sgTitulos.Cells[tiReceberId, vLinha]       := NFormat(FTitulos[i].receber_id);
    sgTitulos.Cells[tiDocumento, vLinha]       := FTitulos[i].documento;
    sgTitulos.Cells[tiCliente, vLinha]         := getInformacao(FTitulos[i].cliente_id, FTitulos[i].nome_cliente);
    sgTitulos.Cells[tiParcela, vLinha]         := NFormat(FTitulos[i].parcela) + '/' + NFormat(FTitulos[i].numero_parcelas);
    sgTitulos.Cells[tiValorPedido, vLinha]     := NFormatN(FTitulos[i].ValorPedido);
    sgTitulos.Cells[tiValorDocumento, vLinha]  := NFormatN(FTitulos[i].valor_documento);
    sgTitulos.Cells[tiValorAdiantado, vLinha]  := NFormatN(FTitulos[i].ValorAdiantado);
    sgTitulos.Cells[tiDataCadastro, vLinha]    := DFormat(FTitulos[i].data_cadastro);
    sgTitulos.Cells[tiDataVencimento, vLinha]  := DFormat(FTitulos[i].data_vencimento);
    sgTitulos.Cells[tiDiasAtraso, vLinha]      := NFormatN(FTitulos[i].dias_atraso);
    sgTitulos.Cells[tiEmpresa, vLinha]         := NFormat(FTitulos[i].empresa_id) + ' - ' + FTitulos[i].nome_empresa;
    sgTitulos.Cells[tiStatusAnalitico, vLinha] := IfThen(FTitulos[i].Status = 'A', 'Ab', 'Bx');
    sgTitulos.Cells[tiTipoCobranca, vLinha]    := NFormat(FTitulos[i].cobranca_id) + ' - ' + FTitulos[i].nome_tipo_cobranca;
    sgTitulos.Cells[tiPortador, vLinha]        := FTitulos[i].PortadorId + ' - ' + FTitulos[i].NomePortador;
    sgTitulos.Cells[tiStatusSintetico, vLinha] := FTitulos[i].status;
    sgTitulos.Cells[tiFormaPagamento, vLinha]  := FTitulos[i].forma_pagamento;
    sgTitulos.Cells[tiClienteId, vLinha]       := NFormat(FTitulos[i].cliente_id);
    sgTitulos.Cells[tiNoCaixa, vLinha]         := FTitulos[i].NoCaixa;
    sgTitulos.Cells[tiBoleto, vLinha]          := FTitulos[i].BoletoBancario;
    sgTitulos.Cells[tiOrigem, vLinha]          := FTitulos[i].Origem;
    sgTitulos.Cells[tiObservacoes, vLinha]     := FTitulos[i].Observacoes;
    sgTitulos.Cells[tiTipoLinha, vLinha]       := coLinhaDetalhe;

    if FTitulos[i].forma_pagamento <> 'CRT' then begin
      FTitulos[i].ValorMulta :=
        _Biblioteca.getValorMulta(
          FTitulos[i].valor_documento,
          ePercMulta.AsDouble,
          FTitulos[i].dias_atraso
        );

      FTitulos[i].ValorJuros :=
        _Biblioteca.getValorJuros(
          FTitulos[i].valor_documento,
          ePercJuros.AsDouble,
          FTitulos[i].dias_atraso
        );
    end;

    sgTitulos.Cells[tiValorMulta, vLinha]      := NFormatN(FTitulos[i].ValorMulta);
    sgTitulos.Cells[tiValorJuros, vLinha]      := NFormatN(FTitulos[i].ValorJuros);
    sgTitulos.Cells[tiValorRetencao, vLinha]   := NFormatN(FTitulos[i].ValorRetencao);
    sgTitulos.Cells[tiSaldoLiquido, vLinha]    := NFormatN(FTitulos[i].getValorLiquido);

    if FTitulos[i].forma_pagamento = 'CHQ' then
      stValorCheque.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, vLinha]) )
    else if FTitulos[i].forma_pagamento = 'CRT' then
      stValorCartao.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, vLinha]) )
    else if FTitulos[i].forma_pagamento = 'ACU'  then
      stValorAcumulado.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, vLinha]) )
    else
      stValorCobranca.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, vLinha]) );

    stValorRetencao.Somar(SFormatDouble(sgTitulos.Cells[tiValorRetencao, vLinha]));

    _Biblioteca.AddNoVetorSemRepetir( vTiposCobrancasIds, FTitulos[i].cobranca_id );
    _Biblioteca.AddNoVetorSemRepetir( vCadastrosIds, FTitulos[i].cliente_id );

    vTotais.totalPedidos      := vTotais.totalPedidos + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorPedido, vLinha]);
    vTotais.totalValorBruto   := vTotais.totalValorBruto + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorDocumento, vLinha]);
    vTotais.totalAdiantamento := vTotais.totalAdiantamento + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorAdiantado, vLinha]);
    vTotais.totalMulta        := vTotais.totalMulta + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorMulta, vLinha]);
    vTotais.totalJuros        := vTotais.totalJuros + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorJuros, vLinha]);
    vTotais.totalReten��o     := vTotais.totalReten��o + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorRetencao, vLinha]);
    vTotais.totalValorLiquido := vTotais.totalValorLiquido + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, vLinha]);

    vTotaisAgrupador.totalPedidos      := vTotaisAgrupador.totalPedidos + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorPedido, vLinha]);
    vTotaisAgrupador.totalValorBruto   := vTotaisAgrupador.totalValorBruto + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorDocumento, vLinha]);
    vTotaisAgrupador.totalAdiantamento := vTotaisAgrupador.totalAdiantamento + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorAdiantado, vLinha]);
    vTotaisAgrupador.totalMulta        := vTotaisAgrupador.totalMulta + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorMulta, vLinha]);
    vTotaisAgrupador.totalJuros        := vTotaisAgrupador.totalJuros + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorJuros, vLinha]);
    vTotaisAgrupador.totalReten��o     := vTotaisAgrupador.totalReten��o + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiValorRetencao, vLinha]);
    vTotaisAgrupador.totalValorLiquido := vTotaisAgrupador.totalValorLiquido + _Biblioteca.SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, vLinha]);

    if
      (sgTitulos.Cells[tiTipoLinha, vLinha] <> coLinhaDetalhe) or
      (FTitulos[i].NoCaixa = 'S') or
      (FTitulos[i].status = 'B') or
      (Em(FTitulos[i].cobranca_id, [Sessao.getParametros.TipoCobRecebEntregaId, Sessao.getParametros.TipoCobAcumulativoId]))
    then
      sgTitulos.Cells[tiSelecionado, vLinha] := charNaoPermiteSelecionar
    else
      sgTitulos.Cells[tiSelecionado, vLinha] := charNaoSelecionado;

  end;

  if cbAgrupamento.GetValor <> 'NEN' then begin
    TotalizarGrupo;
    Inc(vLinha);
  end;

  SetLength(FDadosCobrancas, Length(vTiposCobrancasIds));
  for i := Low(vTiposCobrancasIds) to High(vTiposCobrancasIds) do begin
    FDadosCobrancas[i].cobranca_id   := vTiposCobrancasIds[i];
    FDadosCobrancas[i].qtde_titulos  := 0;
    FDadosCobrancas[i].valor_titulos := 0;
    FDadosCobrancas[i].valor_juros   := 0;
    FDadosCobrancas[i].valor_total   := 0;
    for j := Low(FTitulos) to High(FTitulos) do begin
      if vTiposCobrancasIds[i] <> FTitulos[j].cobranca_id then
        Continue;

      FDadosCobrancas[i].NomeTipoCobranca := FTitulos[j].nome_tipo_cobranca;
      FDadosCobrancas[i].valor_titulos      := FDadosCobrancas[i].valor_titulos + FTitulos[j].valor_documento;
      (*FDadosCobrancas[i].valor_juros      := FDadosCobrancas[i].valor_juros + FTitulos[j].valor_juros*);
      FDadosCobrancas[i].ValorRetencao      := FDadosCobrancas[i].ValorRetencao + FTitulos[j].ValorRetencao;
      FDadosCobrancas[i].valor_total        := FDadosCobrancas[i].valor_total + FTitulos[j].valor_documento;

      Inc(FDadosCobrancas[i].qtde_titulos);
    end;
  end;

  for i := Low(FDadosCobrancas) to High(FDadosCobrancas) do begin
    sgTitulosTipoCobranca.Cells[tcCobranca, i + 1]        := NFormat(FDadosCobrancas[i].cobranca_id) + ' - ' + FDadosCobrancas[i].NomeTipoCobranca;
    sgTitulosTipoCobranca.Cells[tcValorDocumentos, i + 1] := NFormat(FDadosCobrancas[i].valor_titulos);
    sgTitulosTipoCobranca.Cells[tcValorJuros, i + 1]      := NFormat(FDadosCobrancas[i].valor_juros);
    sgTitulosTipoCobranca.Cells[tcValorRetencoes, i + 1]  := NFormat(FDadosCobrancas[i].ValorRetencao);
    sgTitulosTipoCobranca.Cells[tcValorTotal, i + 1]      := NFormat(FDadosCobrancas[i].valor_total - FDadosCobrancas[i].ValorRetencao);
    sgTitulosTipoCobranca.Cells[tcQtdeTitulos, i + 1]     := NFormat(FDadosCobrancas[i].qtde_titulos);
  end;
  sgTitulosTipoCobranca.SetLinhasGridPorTamanhoVetor( Length(FDadosCobrancas) );

  //N�o tem como colocar no agrupador
  if (ckTrazerCreditosPagar.Checked) and (cbAgrupamento.GetValor <> 'CLI') then begin
    vContasPagar :=
      _ContasPagar.BuscarContasPagarComando(
        Sessao.getConexaoBanco,
        'where ' + FiltroInInt('COP.CADASTRO_ID', vCadastrosIds) +
        'and COP.STATUS = ''A'' ' +
        'and COP.COBRANCA_ID in(' + IntToStr(Sessao.getParametros.TipoCobrancaGerCredImpId) + ', ' + IntToStr(Sessao.getParametros.TipoCobrancaGeracaoCredId) + ') '
      );

    for i := Low(vContasPagar) to High(vContasPagar) do begin
      sgTitulos.Cells[tiSelecionado, Length(FTitulos) + i + 1]     := '---';
      sgTitulos.Cells[tiReceberId, Length(FTitulos) + i + 1]       := NFormat(vContasPagar[i].PagarId);
      sgTitulos.Cells[tiDocumento, Length(FTitulos) + i + 1]       := vContasPagar[i].documento;
      sgTitulos.Cells[tiCliente, Length(FTitulos) + i + 1]         := getInformacao(vContasPagar[i].CadastroId, vContasPagar[i].nome_fornecedor);
      sgTitulos.Cells[tiParcela, Length(FTitulos) + i + 1]         := NFormat(vContasPagar[i].parcela) + '/' + NFormat(vContasPagar[i].numero_parcelas);
      sgTitulos.Cells[tiValorDocumento, Length(FTitulos) + i + 1]  := NFormatN(vContasPagar[i].ValorDocumento);
      sgTitulos.Cells[tiValorJuros, Length(FTitulos) + i + 1]      := '';
      sgTitulos.Cells[tiSaldoLiquido, Length(FTitulos) + i + 1]    := NFormatN(vContasPagar[i].ValorDocumento);
      sgTitulos.Cells[tiDataCadastro, Length(FTitulos) + i + 1]    := DFormat(vContasPagar[i].data_cadastro);
      sgTitulos.Cells[tiDataVencimento, Length(FTitulos) + i + 1]  := DFormat(vContasPagar[i].data_vencimento);
      sgTitulos.Cells[tiDiasAtraso, Length(FTitulos) + i + 1]      := NFormatN(vContasPagar[i].dias_atraso);
      sgTitulos.Cells[tiEmpresa, Length(FTitulos) + i + 1]         := NFormat(vContasPagar[i].empresa_id) + ' - ' + vContasPagar[i].nome_empresa;
      sgTitulos.Cells[tiStatusAnalitico, Length(FTitulos) + i + 1] := 'Ab';
      sgTitulos.Cells[tiTipoCobranca, Length(FTitulos) + i + 1]    := NFormat(vContasPagar[i].cobranca_id) + ' - ' + vContasPagar[i].nome_tipo_cobranca;
      sgTitulos.Cells[tiPortador, Length(FTitulos) + i + 1]        := getInformacao(vContasPagar[i].PortadorId, vContasPagar[i].NomePortador);
      sgTitulos.Cells[tiStatusSintetico, Length(FTitulos) + i + 1] := vContasPagar[i].status;
      sgTitulos.Cells[tiFormaPagamento, Length(FTitulos) + i + 1]  := 'CREP';
      sgTitulos.Cells[tiClienteId, Length(FTitulos) + i + 1]       := NFormat(vContasPagar[i].CadastroId);

      if FTitulos[i].forma_pagamento = 'CHQ' then
        stValorCheque.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, Length(FTitulos) + i + 1]) * -1 )
      else if FTitulos[i].forma_pagamento = 'CRT' then
        stValorCartao.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, Length(FTitulos) + i + 1]) * -1 )
      else if FTitulos[i].forma_pagamento = 'ACU'  then
        stValorAcumulado.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, Length(FTitulos) + i + 1]) * -1 )
      else
        stValorCobranca.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, Length(FTitulos) + i + 1]) * -1 );

      stValorRetencao.Somar(SFormatDouble(sgTitulos.Cells[tiValorRetencao, Length(FTitulos) + i + 1]) * -1);
    end;
  end;
  if cbAgrupamento.GetValor = 'CLI' then
    sgTitulos.SetLinhasGridPorTamanhoVetor(vLinha)
  else
    sgTitulos.SetLinhasGridPorTamanhoVetor( Length(FTitulos) + Length(vContasPagar) );

  stValorTotal.Somar( stValorCheque.AsDouble + stValorCartao.AsDouble + stValorCobranca.AsDouble + stValorAcumulado.AsDouble);

  if cbAgrupamento.GetValor = 'CLI' then
    stQuantidadeTitulos.Caption := NFormat(Length(FTitulos) + vTotalContasPagar)
  else
    stQuantidadeTitulos.Caption := NFormat(Length(FTitulos) + Length(vContasPagar));

  SetarFoco(sgTitulos);
  rgTipoRepresentacaoGraficoClick(nil);

end;

procedure TFormRelacaoContasReceber.ckProcessarArquivoRetornoClick(
  Sender: TObject);
begin
  inherited;
  Label3.Enabled := ckProcessarArquivoRetorno.Checked;
  eCaminhoArquivo.Enabled := ckProcessarArquivoRetorno.Checked;
  sbPesquisaCaminhoArquivo.Enabled := ckProcessarArquivoRetorno.Checked;
end;

procedure TFormRelacaoContasReceber.ckTipoChequeClick(Sender: TObject);
begin
  inherited;
  eNomeEmitenteCheque.Enabled := ckTipoCheque.Checked;
end;

procedure TFormRelacaoContasReceber.FormCreate(Sender: TObject);
begin
  inherited;
  pcDados.ActivePageIndex := 0;
  pcResultado.ActivePageIndex := 0;

  ePercMulta.AsDouble := Sessao.getParametrosEmpresa.PercentualMulta;
  ePercJuros.AsDouble := Sessao.getParametrosEmpresa.PercentualJurosMensal;
end;

procedure TFormRelacaoContasReceber.FormShow(Sender: TObject);
begin
  inherited;
  ActiveControl := FrEmpresas.SgPesquisa;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId);
end;

procedure TFormRelacaoContasReceber.Imprimir(Sender: TObject);
var
  vCnt: Integer;
  totalReceber: Double;
begin
  cdsReceber.Close;
  cdsReceber.CreateDataSet;
  cdsReceber.Open;
  inherited;
  totalReceber := 0.0;
  for vCnt := sgTitulos.RowCount - 1 downto 1 do
  begin
    cdsReceber.Insert;
    cdsReceberCodigo.AsString       := sgTitulos.Cells[tiReceberId,vCnt];
    cdsReceberDocumento.AsString    := sgTitulos.Cells[tiDocumento,vCnt];
    cdsReceberNomeCliente.AsString  := sgTitulos.Cells[tiCliente,vCnt];
    cdsRecebersituacao.AsString     := sgTitulos.Cells[tiStatusAnalitico,vCnt];
    cdsReceberParcela.AsString      := sgTitulos.Cells[tiParcela,vCnt];
    cdsReceberValor.AsFloat         := SFormatDouble(sgTitulos.Cells[tiValorDocumento,vCnt]);
    cdsReceberMulta.AsFloat         := SFormatDouble(sgTitulos.Cells[tiValorMulta,vCnt]);
    cdsReceberJuros.AsFloat         := SFormatDouble(sgTitulos.Cells[tiValorJuros,vCnt]);
    cdsReceberAdiantamento.AsFloat  := SFormatDouble(sgTitulos.Cells[tiValorAdiantado,vCnt]);
    cdsReceberValorLiquido.AsFloat  := SFormatDouble(sgTitulos.Cells[tiSaldoLiquido,vCnt]);
    cdsReceberDtVencto.AsDateTime   := ToData(sgTitulos.Cells[tiDataVencimento,vCnt]);
    cdsReceberDtCadastro.AsDateTime := ToData(sgTitulos.Cells[tiDataCadastro,vCnt]);
    cdsReceberEmpresa.AsString      := sgTitulos.Cells[tiEmpresa,vCnt];
    cdsReceberTipoCobranca.AsString := sgTitulos.Cells[tiTipoCobranca,vCnt];
    cdsReceberDiasAtraso.AsInteger  := StrToIntDef(sgTitulos.Cells[tiDiasAtraso,vCnt],0);
    totalReceber := totalReceber + SFormatDouble(sgTitulos.Cells[tiSaldoLiquido,vCnt]);
    cdsReceber.Post;
  end;

  TfrxMemoView(frxReport.FindComponent('mmValorProduto')).Text :=   'Valor Total Liquido.....: ' + NFormat(totalReceber);
  frxReport.ShowReport;
end;

procedure TFormRelacaoContasReceber.ImprimirRetorno(
  pCdsOcorrencias: TClientDataSet; pEmpresaID: Integer; pPortadorID: string;
  pVlrPago, pVlrJuros, pVlrTaxas: Double);
var
  vBancoBoletos: TArray<RecContasBoletos>;
begin
  if pCdsOcorrencias.IsEmpty then
  begin
    Exit;
  end;

  try
    vBancoBoletos := _ContasBoletos.BuscarContasBoletos(Sessao.getConexaoBanco,0,[FrPortadores.getPortador().ContasBoletoId]);
    cdsOcorrencias.Close;
    cdsOcorrencias.CreateDataSet;
    cdsOcorrencias.CloneCursor(pCdsOcorrencias, True);

    cdsSumario.Close;
    cdsSumario.CreateDataSet;
    pCdsOcorrencias.First;
    while not pCdsOcorrencias.Eof do
    begin
      if cdsSumario.Locate('COD_OCORRENCIA', pCdsOcorrencias.FieldByName('COD_OCORRENCIA').AsString, []) then
      begin
        cdsSumario.Edit;
        cdsSumario.FieldByName('TOTAL').AsFloat := cdsSumario.FieldByName('TOTAL').AsFloat +
                                                   pCdsOcorrencias.FieldByName('VLR_TITULO').AsFloat +
                                                   pCdsOcorrencias.FieldByName('VLR_ABATIMENTO').AsFloat;
        cdsSumario.FieldByName('QTD').AsInteger := cdsSumario.FieldByName('QTD').AsInteger + 1;
        cdsSumario.Post;
      end
      else
      begin
        cdsSumario.Insert;
        cdsSumario.FieldByName('COD_OCORRENCIA').AsString  := pCdsOcorrencias.FieldByName('COD_OCORRENCIA').AsString;
        cdsSumario.FieldByName('OCORRENCIA').AsString      := pCdsOcorrencias.FieldByName('DESC_OCORRENCIA').AsString;
        cdsSumario.FieldByName('TOTAL').AsFloat            := pCdsOcorrencias.FieldByName('VLR_TITULO').AsFloat + pCdsOcorrencias.FieldByName('VLR_ABATIMENTO').AsFloat;
        cdsSumario.FieldByName('QTD').AsInteger            := cdsSumario.FieldByName('QTD').AsInteger + 1;
        cdsSumario.Post;
      end;
      pCdsOcorrencias.Next;
    end;

    cdsPortador.Close;
    cdsPortador.CreateDataSet;
    cdsPortador.Append;
    cdsPortadorPORTADOR_ID.AsString := pPortadorID;
    cdsPortadorPORTADOR_NOME.AsString := FrPortadores.getPortador().Descricao;
    cdsPortadorCONTA_CORRENTE_ID.AsInteger := FrPortadores.getPortador().ContasBoletoId;
    cdsPortadorCONTA_CORRENTE.AsString := vBancoBoletos[0].Descricao;
    cdsPortadorVLR_PAGO.AsFloat := pVlrPago;
    cdsPortadorVLR_JUROS.AsFloat := pVlrJuros;
    cdsPortadorVLR_TAXAS.AsFloat := pVlrTaxas;
    cdsPortadorTOTAL_BRUTO.AsFloat := (pVlrPago + pVlrJuros);
    cdsPortadorTOTAL_LIQUIDO.AsFloat := (pVlrPago + pVlrJuros) - pVlrTaxas;
    cdsPortador.Post;

    frxOcorrencias.ShowReport();

    cdsOcorrencias.Filter := 'DIFERENCA > 0';
    cdsOcorrencias.Filtered := True;
    if (not cdsOcorrencias.IsEmpty) and
       (Perguntar('Deseja emitir o relat�rio de diferen�as?')) then
    begin
      frxDiferencas.ShowReport();
    end;
  except
    on E: Exception do
    begin
      _Biblioteca.Informar('Houve um problema ao gerar o relat�rio de processamento de retorno. Erro: ' + E.Message);
    end;
  end;
end;

procedure TFormRelacaoContasReceber.miAlterarCampoDocumentoClick(
  Sender: TObject);
var
  vDocumento: string;
  vRetBanco: RecRetornoBD;
begin
  inherited;
//  if not Sessao.AutorizadoRotina('alterar_documento_conta_pagar') then begin
//    _Biblioteca.NaoAutorizadoRotina;
//    Exit;
//  end;

  if SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]) = 0 then
    Exit;

  vDocumento := BuscaDados.BuscarDados('Documento', sgTitulos.Cells[tiDocumento, sgTitulos.Row], 24);
  if vDocumento = '' then
    Exit;

  vRetBanco :=
    _ContasReceber.AtualizarCampoDocumento(
      Sessao.getConexaoBanco,
      SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]),
      vDocumento
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.miAlterarDataVencimentoClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;

  vRetTela := EdicaoInformacoesContasReceber.AlterarDados(SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]));
  if vRetTela.BuscaCancelada then
    Exit;

  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.miAlterarInformacoesClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;

  vRetTela := EdicaoInformacoesContasReceber.AlterarDados(SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]));
  if vRetTela.BuscaCancelada then
    Exit;

  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.miAlterarParcelaNumeroParcelasClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;

  vRetTela := EdicaoInformacoesContasReceber.AlterarDados(SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]));
  if vRetTela.BuscaCancelada then
    Exit;

  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.miAlteraVendedorClick(Sender: TObject);
var
  vFuncionario: RecFuncionarios;
  vRetBanco: RecRetornoBD;

begin
  inherited;

  vFuncionario := RecFuncionarios(PesquisaFuncionarios.PesquisarFuncionario(False, False, False, False, 0, False, True, False));
  if vFuncionario = nil then
    Exit;

  if not _Biblioteca.Perguntar('Aten��o, deseja realmente alterar o vendedor?') then
    Exit;

  vRetBanco := _ContasReceber.AtualizarVendedor(Sessao.getConexaoBanco, SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]),vFuncionario.funcionario_id );
  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.Informar('Vendedor alterado com sucesso.');
  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.miBaixarTituloClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
  vTitulo: TArray<Integer>;
  vConsumidorFinal: Boolean;
begin
  inherited;
  if sgTitulos.Cells[tiStatusSintetico, sgTitulos.Row] = 'B' then begin
    Exclamar('Este t�tulo j� foi baixado!');
    SetarFoco(sgTitulos);
    Exit;
  end;

  if sgTitulos.Cells[tiNoCaixa, sgTitulos.Row] = 'S' then begin
    Exclamar('Este t�tulo pertence ao caixa!');
    SetarFoco(sgTitulos);
    Exit;
  end;

  if sgTitulos.Cells[tiFormaPagamento, sgTitulos.Row] = 'ACU' then begin
    Exclamar('Este t�tulo � um acumulado, baixa n�o permitida!');
    SetarFoco(sgTitulos);
    Exit;
  end;

  if sgTitulos.Cells[tiSelecionado, sgTitulos.Row] = charNaoPermiteSelecionar then begin
    Exclamar('Este t�tulo n�o pode ser baixado, verique se n�o � uma previs�o!');
    SetarFoco(sgTitulos);
    Exit;
  end;

  vConsumidorFinal := SFormatInt( sgTitulos.Cells[tiClienteId, sgTitulos.Row] ) = Sessao.getParametros.cadastro_consumidor_final_id;

  SetLength( vTitulo, 1 );
  vTitulo[0] := SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]);

  vRetTela := BaixarTitulosReceber.BaixarTitulos(
    [SFormatInt(sgTitulos.Cells[tiClienteId, sgTitulos.Row])],
    vTitulo,
    not vConsumidorFinal,
    //sgTitulos.Cells[tiFormaPagamento, sgTitulos.Row] = 'CRT'
    false
  );

  if vRetTela.BuscaCancelada then
    Exit;

  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.miAlterarInformacoes0(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vClienteAdicionado: Boolean;
  vTitulosIds: TArray<Integer>;
  vRetTela: TRetornoTelaFinalizar;

  vClienteIds: TArray<Integer>;
  vClienteId: Integer;
  vTemClienteDiferente: Boolean;
  vTemConsumidorFinal: Boolean;

  vTemFormaPagamentoDiferenteCartao: Boolean;
  vTemCartao: Boolean;
begin
  inherited;

  vClienteIds := nil;
  vTitulosIds := nil;
  vTemClienteDiferente := False;
  vTemConsumidorFinal := False;

  vTemCartao := False;
  vTemFormaPagamentoDiferenteCartao := False;
  for i := sgTitulos.FixedRows to sgTitulos.RowCount -1 do begin
    vClienteAdicionado := false;
    if
      (sgTitulos.Cells[tiStatusSintetico, i] = 'B') or
      (Em(sgTitulos.Cells[tiSelecionado, i], ['---', charNaoSelecionado])) or
      (sgTitulos.Cells[tiSelecionado, i] <> charSelecionado)
    then
      Continue;

    SetLength(vTitulosIds, Length(vTitulosIds) + 1);
    vTitulosIds[High(vTitulosIds)] := SFormatInt(sgTitulos.Cells[tiReceberId, i]);

    for j := Low(vClienteIds) to High(vClienteIds) do begin
      if vClienteIds[j] = SFormatInt(sgTitulos.Cells[tiClienteId, i]) then begin
        vClienteAdicionado := true;
        Break;
      end;
    end;

    if not vClienteAdicionado then begin
      SetLength(vClienteIds, Length(vClienteIds) + 1);
      vClienteIds[High(vClienteIds)] := SFormatInt(sgTitulos.Cells[tiClienteId, i]);
    end;

    if vClienteId = 0 then
      vClienteId := SFormatInt(sgTitulos.Cells[tiClienteId, i])
    else if not vTemClienteDiferente then
      vTemClienteDiferente := vClienteId <> SFormatInt(sgTitulos.Cells[tiClienteId, i]);

    if not vTemConsumidorFinal then
      vTemConsumidorFinal := vClienteId = Sessao.getParametros.cadastro_consumidor_final_id;

    if (not vTemCartao) and (sgTitulos.Cells[tiFormaPagamento, i] = 'CRT') then
      vTemCartao := True;

    if (not vTemFormaPagamentoDiferenteCartao) and (sgTitulos.Cells[tiFormaPagamento, i] <> 'CRT') then
      vTemFormaPagamentoDiferenteCartao := True;

    if vTemCartao and vTemFormaPagamentoDiferenteCartao then begin
      _Biblioteca.Exclamar('N�o � permitido baixar cart�es com outras formas de pagamentos ao mesmo tempo!');
      Abort;
    end;
  end;

  if vTitulosIds = nil then begin
    _Biblioteca.NenhumRegistroSelecionado;
    Exit;
  end;

  vRetTela := BaixarTitulosReceber.BaixarTitulos( vClienteIds, vTitulosIds, (not vTemClienteDiferente) and (not vTemConsumidorFinal), vTemCartao );
  if vRetTela.BuscaCancelada then
    Exit;

  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.miDefinirContaCustodiaClick(Sender: TObject);
var
  i: Integer;
  vTitulosIds: TArray<Integer>;
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;
  vTitulosIds := nil;

  for i := sgTitulos.FixedRows to sgTitulos.RowCount -1 do begin
    if
      (sgTitulos.Cells[tiStatusSintetico, i] = 'B') or
      (Em(sgTitulos.Cells[tiSelecionado, i], ['---', charNaoSelecionado]))
    then
      Continue;

    SetLength(vTitulosIds, Length(vTitulosIds) + 1);
    vTitulosIds[High(vTitulosIds)] := SFormatInt(sgTitulos.Cells[tiReceberId, i]);
  end;

  if vTitulosIds = nil then begin
    _Biblioteca.NenhumRegistroSelecionado;
    Exit;
  end;

  vRetTela := DefinirContaCustodiaContasReceber.Definir( vTitulosIds );
  if vRetTela.BuscaCancelada then
    Exit;

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.miEmitirBoletoClick(Sender: TObject);
var
  i: Integer;
  vReceberIds: TArray<Integer>;
begin
  inherited;

  for i := 1 to sgTitulos.RowCount -1 do begin
    if (sgTitulos.Cells[tiFormaPagamento, i] <> 'COB') or (sgTitulos.Cells[tiBoleto, i] <> 'S') or (sgTitulos.Cells[tiSelecionado, i] <> charSelecionado) then
      Continue;

    SetLength(vReceberIds, Length(vReceberIds) + 1);
    vReceberIds[High(vReceberIds)] := SFormatInt( sgTitulos.Cells[tiReceberId, i] );
  end;

  if vReceberIds = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  Emissao.BoletosBancario.Emitir(vReceberIds);
end;

procedure TFormRelacaoContasReceber.miEnviarParaRecebimentoCaixaClick(Sender: TObject);
var
  i: Integer;
  vClienteId: Integer;
  vTitulosIds: TArray<Integer>;
begin
  inherited;

  vClienteId := 0;
  vTitulosIds := nil;
  for i := sgTitulos.FixedRows to sgTitulos.RowCount -1 do begin
    if
      (sgTitulos.Cells[tiSelecionado, i] <> charSelecionado) or
      (sgTitulos.Cells[tiStatusSintetico, i] = 'B')
    then
      Continue;

    SetLength(vTitulosIds, Length(vTitulosIds) + 1);
    vTitulosIds[High(vTitulosIds)] := SFormatInt(sgTitulos.Cells[tiReceberId, i]);

    if vClienteId = 0 then
      vClienteId := SFormatInt(sgTitulos.Cells[tiClienteId, i])
    else if vClienteId <> SFormatInt(sgTitulos.Cells[tiClienteId, i]) then begin
      Exclamar('T�tulos de clientes diferentes n�o pode ser mandados juntos para o caixa!');
      SetarFoco(sgTitulos);
      Abort;
    end;
  end;

  case EnviarTitulosReceberCaixa.EnviarTitulos( vTitulosIds ).RetTela of
    trOk: begin
      Carregar(Sender);
      RotinaSucesso;
    end;

    trCancelado: RotinaCanceladaUsuario;
  end;
end;

procedure TFormRelacaoContasReceber.miExcluirTituloClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  if sgTitulos.Cells[tiOrigem, sgTitulos.Row] <> 'MAN' then begin
    _Biblioteca.Exclamar('N�o � permitida a exclus�o de t�tulos que n�o � de origem manual!');
    Abort;
  end;

  if not _Biblioteca.Perguntar('Aten��o, deseja realmente excluir o financeiro selecionado? Este processo � irrevers�vel.') then
    Exit;

  vRetBanco := _ContasReceber.ExcluirContasReceber(Sessao.getConexaoBanco, SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]) );
  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.Informar('Conta a receber exclu�do com sucesso.');
  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.miIncluirNovoTituloClick(Sender: TObject);
begin
  inherited;
  Sessao.AbrirTela(TFormInclusaoTitulosReceber);
end;

procedure TFormRelacaoContasReceber.miMarcarDesmarcarTItuloFocadoClick(Sender: TObject);
begin
  inherited;
  MarcarDesmarcarTodos(sgTitulos, tiSelecionado, VK_SPACE, []);
  TotalizarSelecionados;
end;

procedure TFormRelacaoContasReceber.miMarcarDesmarcarTodosTitulosClick(Sender: TObject);
begin
  inherited;
  MarcarDesmarcarTodos(sgTitulos, tiSelecionado, VK_SPACE, [ssCtrl]);
  TotalizarSelecionados;
end;

procedure TFormRelacaoContasReceber.miRealizarAdiantamentoTituloFocadoClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;
  vRetTela := AdiantamentoTituloFinanceiro.AdiantarTitulo(SFormatInt( sgTitulos.Cells[tiReceberId, sgTitulos.Row]));
  if vRetTela.BuscaCancelada then
    Exit;

  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.miReimprimirDuplicataMercantilVendaClick(Sender: TObject);
begin
  inherited;
  ImpressaoMeiaPaginaDuplicataMercantil.Emitir(SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]), tpFinanceiro);
end;

procedure TFormRelacaoContasReceber.pmRotinasPopup(Sender: TObject);
var
  i: Integer;
  vHabilitarBaixas: Boolean;
  vHabilitarReceberCaixa: Boolean;
  vHabilitarEmissaoBoleto: Boolean;
begin
  inherited;
  vHabilitarBaixas := True;
  vHabilitarReceberCaixa := True;
  vHabilitarEmissaoBoleto := True;
  for i := sgTitulos.FixedRows to sgTitulos.RowCount -1 do begin
    if
      (sgTitulos.Cells[tiStatusSintetico, i] = 'B') or
      (Em(sgTitulos.Cells[tiSelecionado, i], ['---', charNaoSelecionado]))
    then
      Continue;

    // Se for previs�o do acumulado, n�o permitir baixa
    if sgTitulos.Cells[tiFormaPagamento, i] = 'ACU' then
      vHabilitarBaixas := False;

    if sgTitulos.Cells[tiStatusSintetico, sgTitulos.Row] <> 'A' then
      vHabilitarBaixas := False;

    // Se n�o for boleto, n�o permitir emiss�o de boletos
    if (sgTitulos.Cells[tiFormaPagamento, i] <> 'COB') or (sgTitulos.Cells[tiBoleto, i] = 'N') then
      vHabilitarEmissaoBoleto := False;

    // Se houver cart�o, n�o permitir enviar para o caixa
    if sgTitulos.Cells[tiFormaPagamento, i] = 'CRT' then
      vHabilitarReceberCaixa := False;
  end;

  miBaixarTitulo.Enabled := vHabilitarBaixas;
  miEmitirBoleto.Enabled := vHabilitarEmissaoBoleto;

  miBaixarTitulosSelecionados.Enabled  := vHabilitarBaixas;
  miEnviarParaRecebimentoCaixa.Enabled := vHabilitarBaixas and vHabilitarReceberCaixa;
end;

procedure TFormRelacaoContasReceber.rgTipoRepresentacaoGraficoClick(Sender: TObject);
var
  i: Integer;
const
  trValorTotal  = 0;
  trQtdeTitulos = 1;
begin
  inherited;
  chTiposCobranca.Series[0].Clear;
  chTiposCobranca.Series[0].Marks.FontSeriesColor := True;
  chTiposCobranca.Title.Text.Text := 'Representa��o gr�fica(' + IfThen(rgTipoRepresentacaoGrafico.ItemIndex = trValorTotal, 'Valor total', 'Quantidade de t�tulos') + ')';

  for i := Low(FDadosCobrancas) to High(FDadosCobrancas) do begin
    if rgTipoRepresentacaoGrafico.ItemIndex = trQtdeTitulos then begin
      chTiposCobranca.Series[0].Add(
        FDadosCobrancas[i].qtde_titulos / Length(FTitulos) * 100,
        '  ' + NFormat(FDadosCobrancas[i].cobranca_id) + ' - ' + FDadosCobrancas[i].NomeTipoCobranca
      );
    end
    else begin
      if stValorTotal.Caption <> '' then
        chTiposCobranca.Series[0].Add(100)
      else begin
        chTiposCobranca.Series[0].Add(
          FDadosCobrancas[i].valor_total / SFormatDouble(stValorTotal.Caption) * 100,
          '  ' + NFormat(FDadosCobrancas[i].cobranca_id) + ' - ' + FDadosCobrancas[i].NomeTipoCobranca
        );
      end;
    end;
  end;
  chTiposCobranca.Repaint;
end;

procedure TFormRelacaoContasReceber.sbPesquisaCaminhoArquivoClick(
  Sender: TObject);
var
  vDialog: TOpenDialog;
begin
  inherited;
  vDialog := TOpenDialog.Create(Self);

  vDialog.DefaultExt := '*.ret';
  vDialog.Filter := '';
  vDialog.Filter := 'Arquivo de retorno|*.ret';

  if vDialog.Execute then
    eCaminhoArquivo.Text := vDialog.FileName;

  vDialog.Free;
end;

procedure TFormRelacaoContasReceber.sgTitulosDblClick(Sender: TObject);
begin
  inherited;
  if sgTitulos.Cells[tiFormaPagamento, sgTitulos.Row] = 'CREP' then
    Informacoes.TituloPagar.Informar( SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]) )
  else
    Informacoes.TituloReceber.Informar( SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]) );
end;

procedure TFormRelacaoContasReceber.sgTitulosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if ACol in[tiReceberId, tiValorDocumento, tiValorAdiantado, tiParcela, tiValorPedido, tiValorMulta, tiValorJuros, tiValorRetencao, tiSaldoLiquido, tiDiasAtraso] then
    alinhamento := taRightJustify
  else if ACol in[tiSelecionado, tiStatusAnalitico] then
    alinhamento := taCenter
  else
    alinhamento := taLeftJustify;

  sgTitulos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

procedure TFormRelacaoContasReceber.sgTitulosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;

  if ARow = 0 then
    Exit;

  if ACol = tiStatusAnalitico then begin
    AFont.Style := [fsBold];
    if sgTitulos.Cells[tiStatusSintetico, ARow] = 'A' then
      AFont.Color := $000096DB
    else
      AFont.Color := clBlue;
  end
  else if ACol in[tiDiasAtraso, tiValorRetencao] then
    AFont.Color := clRed
  else if ACol = tiSaldoLiquido then begin
    if sgTitulos.Cells[tiFormaPagamento, ARow] = 'CREP' then
      AFont.Color := clRed
    else
      AFont.Color := $000096DB;
  end;

  if (sgTitulos.Cells[tiStatusSintetico, ARow] = 'A') and (sgTitulos.Cells[tiNoCaixa, ARow] = 'S') then
    ABrush.Color := $00AAAAFF;

  if sgTitulos.Cells[tiTipoLinha, ARow] = coLinhaTotVenda then begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if sgTitulos.Cells[tiTipoLinha, ARow] = coLinhaCabAgrup then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao2;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao2;
  end
  else if sgTitulos.Cells[tiTipoLinha, ARow] = coTotalAgrupador then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao3;
    AFont.Style  := [fsBold];
    AFont.Color  := _Biblioteca.coCorFonteEdicao3;
  end;
end;

procedure TFormRelacaoContasReceber.sgTitulosGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol <> tiSelecionado) or (sgTitulos.Cells[ACol, ARow] = '') then
   Exit;

  if sgTitulos.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else if sgTitulos.Cells[ACol, ARow] = charSelecionado then
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFormRelacaoContasReceber.sgTitulosKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  MarcarDesmarcarTodos(sgTitulos, tiSelecionado, Key, Shift);
  if Key = VK_SPACE then
    TotalizarSelecionados;
end;

procedure TFormRelacaoContasReceber.sgTitulosTipoCobrancaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol = tcCobranca then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgTitulosTipoCobranca.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoContasReceber.sgTitulosTipoCobrancaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = tcValorRetencoes then
    AFont.Color := clRed
  else if ACol = tcValorTotal then
    AFont.Color := $000096DB;
end;

procedure TFormRelacaoContasReceber.SpeedButton2Click(Sender: TObject);
var
  vReceberId: Integer;
  vRetBanco: RecRetornoBD;
  vRetTela: TRetornoTelaFinalizar<Integer>;
begin
  inherited;
  vReceberId := SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]);
  if vReceberId = 0 then
    Exit;

  vRetTela := BuscarCentroCusto.Buscar;
  if vRetTela.BuscaCancelada then
    Exit;

  vRetBanco := _ContasReceber.AtualizarCentroCusto( Sessao.getConexaoBanco, vReceberId, vRetTela.Dados );
  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.SpeedButton3Click(Sender: TObject);
var
  vReceberId: Integer;
  vRetTela: TRetornoTelaFinalizar<Double>;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vReceberId := SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]);
  if vReceberId = 0 then
    Exit;

  vRetTela := BuscaDados.BuscarDados('Valor de reten��o', SFormatDouble(sgTitulos.Cells[tiValorRetencao, sgTitulos.Row]), 0);
  if vRetTela.BuscaCancelada then
    Exit;

  vRetBanco :=
    _ContasReceber.AtualizarValorRetencao(
      Sessao.getConexaoBanco,
      vReceberId,
      vRetTela.Dados
    );

  Sessao.AbortarSeHouveErro( vRetBanco );

  _Biblioteca.RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.SpeedButton4Click(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;

  vRetTela := EditarInformacoesMemo.AlterarDados(
    SFormatInt(sgTitulos.Cells[tiReceberId, sgTitulos.Row]),
    sgTitulos.Cells[tiObservacoes, sgTitulos.Row],
    trContasReceber,
    200,
    'Editar observa��es'
  );

  if vRetTela.BuscaCancelada then
    Exit;

  Carregar(Sender);
end;

procedure TFormRelacaoContasReceber.TotalizarSelecionados;
var
  i: Integer;
begin
  _Biblioteca.LimparCampos([
    stValorChequeSelecionados,
    stValorCartaoSelecionados,
    stValorCobrancaSelecionados,
    stValorAcumuladosSelecionados,
    stValorRetencaoSelecionados,
    stValorTotalSelecionados,
    stQtdeTitulosSelecionados
  ]);

  for i := 1 to sgTitulos.RowCount -1 do begin
    if sgTitulos.Cells[tiSelecionado, i] <> charSelecionado then
      Continue;

    if sgTitulos.Cells[tiFormaPagamento, i] = 'CHQ' then
      stValorChequeSelecionados.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, i]) )
    else if sgTitulos.Cells[tiFormaPagamento, i] = 'CRT' then
      stValorCartaoSelecionados.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, i]) )
    else if sgTitulos.Cells[tiFormaPagamento, i] = 'COB' then
      stValorCobrancaSelecionados.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, i]) )
    else
      stValorAcumuladosSelecionados.Somar( SFormatDouble(sgTitulos.Cells[tiSaldoLiquido, i]) );

    stValorRetencaoSelecionados.Somar(SFormatDouble(sgTitulos.Cells[tiValorRetencao, i]));
    stQtdeTitulosSelecionados.Somar( 1 );
  end;

  stValorTotalSelecionados.Somar(
    stValorChequeSelecionados.AsDouble +
    stValorCartaoSelecionados.AsDouble +
    stValorCobrancaSelecionados.AsDouble +
    stValorAcumuladosSelecionados.AsDouble
  );
end;

procedure TFormRelacaoContasReceber.VerificarRegistro(Sender: TObject);
begin
  inherited;
  _Biblioteca.LimparCampos([
    sgTitulos,
    sgTitulosTipoCobranca,
    stValorCheque,
    stValorCartao,
    stValorCobranca,
    stValorAcumulado,
    stValorTotal,
    stValorChequeSelecionados,
    stValorCartaoSelecionados,
    stValorCobrancaSelecionados,
    stValorAcumuladosSelecionados,
    stValorRetencaoSelecionados,
    stValorRetencao,
    stValorTotalSelecionados,
    stQtdeTitulosSelecionados
  ]);

  if ckProcessarArquivoRetorno.Checked then
  begin
    if FrPortadores.EstaVazio then begin
      _Biblioteca.Exclamar('Nenhum portador foi informado, verifique!');
      SetarFoco(FrPortadores);
      Abort;
    end;

    if eCaminhoArquivo.Text = '' then begin
      _Biblioteca.Exclamar('Nenhum arquivo foi informado, verifique!');
      SetarFoco(eCaminhoArquivo);
      Abort;
    end;

  end else
  begin
    if gbStatusFinanceiro.NenhumMarcado then begin
      Exclamar('N�o foi marcado nenhum status para pesquisar os t�tulos, verifique!');
      ckTitulosEmAberto.SetFocus;
      Abort;
    end;

    if gbFormasPagamento.NenhumMarcado then begin
      Exclamar('Nenhum tipo de documento foi marcado para pesquisar os t�tulos, verifique!');
      ckTipoDinheiro.SetFocus;
      Abort;
    end;

    if gbOrigem.NenhumMarcado then begin
      Exclamar('� necess�rio selecionar pelo menos uma origem');
      gbOrigem.SetFocus;
      Abort;
    end;
  end;
end;

end.
