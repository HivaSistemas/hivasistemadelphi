unit Exclamar;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPrincipal, Vcl.StdCtrls,
  Vcl.Imaging.pngimage, Vcl.ExtCtrls, Vcl.Buttons;

type
  TFormExclamar = class(TFormHerancaPrincipal)
    Image1: TImage;
    sbSim: TPanel;
    eExclamacao: TMemo;
    procedure sbOkClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure CriarExclamacao(pCaptionForm: string; pExclamacao: string);

implementation

{$R *.dfm}

procedure CriarExclamacao(pCaptionForm: string; pExclamacao: string);
var
  vForm: TFormExclamar;
begin
  vForm := TFormExclamar.Create(nil);

  vForm.Caption := vForm.Caption + ' ' + pCaptionForm;
  vForm.eExclamacao.Lines.Text := pExclamacao;

  vForm.ShowModal;

  vForm.Free;
end;

procedure TFormExclamar.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sbOkClick(Sender);

  if Key = Ord('O') then
    sbOkClick(Sender);
end;

procedure TFormExclamar.sbOkClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

end.
