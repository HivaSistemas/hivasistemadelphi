inherited FrNumeros: TFrNumeros
  Width = 134
  Height = 72
  ExplicitWidth = 134
  ExplicitHeight = 72
  object sgNumeros: TGridLuka
    Left = 0
    Top = 14
    Width = 134
    Height = 58
    Align = alClient
    ColCount = 1
    DefaultRowHeight = 19
    DrawingStyle = gdsClassic
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goRangeSelect]
    TabOrder = 0
    OnDrawCell = sgNumerosDrawCell
    OnKeyDown = sgNumerosKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    Grid3D = False
    RealColCount = 1
    AtivarPopUpSelecao = False
    ColWidths = (
      106)
  end
  object pnDescricao: TPanel
    Left = 0
    Top = 0
    Width = 134
    Height = 14
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = ' C'#243'digo'
    TabOrder = 1
  end
end
