unit Informacoes.ProdutosComposicaoKit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _ProdutosKit, _Sessao,
  Vcl.Grids, GridLuka, Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca;

type
  TFormInformacoesComposicaoKit = class(TFormHerancaFinalizar)
    sgProdutos: TGridLuka;
    st3: TStaticText;
    stProduto: TStaticText;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  end;

procedure Informar(pProdutoId: Integer; pNomeProduto: string);

implementation

{$R *.dfm}

const
  coProdutoId  = 0;
  coNome       = 1;
  coQuantidade = 2;

procedure Informar(pProdutoId: Integer; pNomeProduto: string);
var
  i: Integer;
  vForm: TFormInformacoesComposicaoKit;
  vProdutosKit: TArray<RecProdutosKit>;
begin
  if pProdutoId = 0 then
    Exit;

  vForm := TFormInformacoesComposicaoKit.Create(nil);

  vProdutosKit := _ProdutosKit.BuscarProdutosKit(Sessao.getConexaoBanco, 0, [pProdutoId]);
  for i := Low(vProdutosKit) to High(vProdutosKit) do begin
    vForm.sgProdutos.Cells[coProdutoId, i + 1]  := NFormat(vProdutosKit[i].ProdutoId);
    vForm.sgProdutos.Cells[coNome, i + 1]       := vProdutosKit[i].NomeProduto;
    vForm.sgProdutos.Cells[coQuantidade, i + 1] := NFormat(vProdutosKit[i].Quantidade, 4);
  end;
  vForm.sgProdutos.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor(Length(vProdutosKit));

  vForm.stProduto.Caption := NFormat(pProdutoId) + ' - ' + pNomeProduto;

  vForm.Show;
end;

procedure TFormInformacoesComposicaoKit.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coProdutoId, coQuantidade] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
