unit DefinirPrecosVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _FormacaoCustosPrecos,
  Vcl.Grids, GridLuka, FrameDataInicialFinal, Frame.DepartamentosSecoesLinhas, _Sessao, _Biblioteca,
  FrameProdutos, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, _BibliotecaGenerica,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, _Imagens, _RecordsEspeciais,
  Vcl.StdCtrls, RadioGroupLuka, SpeedButtonLuka, Vcl.Menus, AlterarDadosPrecificacaoProdutos,
  CheckBoxLuka;

type
  TFormDefinirPrecosVenda = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrProdutos: TFrProdutos;
    FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas;
    FrDataCadastroProduto: TFrDataInicialFinal;
    sgItens: TGridLuka;
    rgTipoCustoFormacao: TRadioGroupLuka;
    sbGravar: TSpeedButtonLuka;
    pmOpcoes: TPopupMenu;
    miMarcarSelecionado: TMenuItem;
    miMarcarTodos: TMenuItem;
    N1: TMenuItem;
    miAlterarDadosColunaSelecionados: TMenuItem;
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensGetEditMask(Sender: TObject; ACol, ARow: Integer; var Value: string);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
    procedure sbGravarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure miMarcarSelecionadoClick(Sender: TObject);
    procedure miMarcarTodosClick(Sender: TObject);
    procedure sgItensKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure miAlterarDadosColunaSelecionadosClick(Sender: TObject);
  private
    procedure CalcularCustoComercial(pLinha: Integer);
  protected
    procedure Carregar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses _GruposPrecificacao;

const
  coSelecionado      = 0;
  coLegenda          = 1;
  coProdutoId        = 2;
  coNome             = 3;
  coCustoCompra      = 4;
  coPercCustoVenda   = 5;
  coPercMargVarejo   = 6;
  coPrecoVarejo      = 7;
  coCustoComercial   = 8;
  coQtdeMinimaVarejo = 9;
  coDataInicioPreco = 10;
  coPrecoAtual      = 11;
  coDataPreAtual    = 12;

{ TFormDefinirPrecosVenda }

procedure TFormDefinirPrecosVenda.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vItens: TArray<RecItensFormacaoCusto>;
begin
  inherited;
  sgItens.ClearGrid();

  if not FrProdutos.EstaVazio then
    vSql := vSql + ' and ' + FrProdutos.getSqlFiltros('PRO.PRODUTO_ID');

  if not FrDepartamentosSecoesLinhas.EstaVazio then
    vSql := vSql + ' and ' + FrDepartamentosSecoesLinhas.getSqlFiltros('PRO.LINHA_PRODUTO_ID');

  if not FrDataCadastroProduto.NenhumaDataValida then
    vSql := vSql + ' and ' + FrDataCadastroProduto.getSqlFiltros('PRO.DATA_CADASTRO');

  vSql := vSql + ' order by PRO.NOME ';

  vItens := _FormacaoCustosPrecos.BuscarPrecoVarejo(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId, vSql);
  if vItens = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vItens) to High(vItens) do begin
    sgItens.Cells[coSelecionado, i + 1]     := charNaoSelecionado;
    sgItens.Cells[coProdutoId, i + 1]       := _Biblioteca.NFormat(vItens[i].ProdutoId);
    sgItens.Cells[coNome, i + 1]            :=  vItens[i].Nome;
    sgItens.Cells[coCustoCompra, i + 1]     := _Biblioteca.NFormatN(vItens[i].CustoCompraComercial, 4);
    sgItens.Cells[coPercCustoVenda, i + 1]  := _Biblioteca.NFormatN(vItens[i].PercentualCustoVenda, 5);
    sgItens.Cells[coPercMargVarejo, i + 1]  := _Biblioteca.NFormatN(vItens[i].PercMargemPrecoVarejo, 5);
    sgItens.Cells[coPrecoVarejo, i + 1]     := _Biblioteca.NFormatN(vItens[i].PrecoVarejo, 4);
    sgItens.Cells[coQtdeMinimaVarejo, i + 1] := _Biblioteca.NFormatN(vItens[i].QuantidadeMinPrecoVarejo, 4);
    sgItens.Cells[coPrecoAtual, i + 1]      := _Biblioteca.NFormatN(vItens[i].PrecoVarejo, 4);
    sgItens.Cells[coDataPreAtual, i + 1]    := _Biblioteca.DFormatN(vItens[i].DataPrecoVarejo);

    CalcularCustoComercial(i + 1);
  end;
  sgItens.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor(Length(vItens));
  sgItens.Col := coPrecoVarejo;
  _Biblioteca.SetarFoco(sgItens);
end;

procedure TFormDefinirPrecosVenda.FormShow(Sender: TObject);
var
  i: Integer;
  pArrayGrupos: TArray<RecGruposPrecificacao>;
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);

  pArrayGrupos := _GruposPrecificacao.BuscarGruposPrecificacao(Sessao.getConexaoBanco, 0, [Sessao.getEmpresaLogada.EmpresaId]);
  for i := Low(pArrayGrupos) to High(pArrayGrupos) do
    FrEmpresas.InserirDadoPorChave(pArrayGrupos[i].EmpresaGrupoId);

  _BibliotecaGenerica.SetarFoco(FrProdutos);
end;

procedure TFormDefinirPrecosVenda.miAlterarDadosColunaSelecionadosClick(
  Sender: TObject);
begin
  inherited;

  AlterarDadosPrecificacaoProdutos.AlterarDados(
    sgItens,
    coSelecionado,
    charSelecionado,
    'Alterar precifica��o dos produtos selecionados',
    [coPrecoVarejo],
    [coQtdeMinimaVarejo],
    [coDataInicioPreco],
    [coPercMargVarejo]
  );

end;

procedure TFormDefinirPrecosVenda.miMarcarSelecionadoClick(Sender: TObject);
begin
  inherited;
  if sgItens.Cells[coProdutoId, sgItens.Row] = '' then
    Exit;

  sgItens.Cells[coSelecionado, sgItens.Row] := IIfStr(sgItens.Cells[coSelecionado, sgItens.Row] = charNaoSelecionado, charSelecionado, charNaoSelecionado);
end;

procedure TFormDefinirPrecosVenda.miMarcarTodosClick(Sender: TObject);
var
  i: Integer;
  vMarcar: Boolean;
begin
  if sgItens.Cells[coProdutoId, 1] = '' then
    Exit;

  vMarcar := sgItens.Cells[coSelecionado, 1] = charNaoSelecionado;
  for i := 1 to sgItens.RowCount -1 do
    sgItens.Cells[coSelecionado, i] := IIfStr(vMarcar, charSelecionado, charNaoSelecionado);
end;

procedure TFormDefinirPrecosVenda.sbGravarClick(Sender: TObject);
var
  i: Integer;
  vPrecos: TArray<RecItensFormacaoCusto>;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vPrecos := nil;
  for i := 1 to sgItens.RowCount -1 do begin
    SetLength(vPrecos, Length(vPrecos) + 1);
    vPrecos[High(vPrecos)].ProdutoId                := SFormatInt( sgItens.Cells[coProdutoId, i] );
    vPrecos[High(vPrecos)].PercMargemPrecoVarejo    := SFormatDouble( sgItens.Cells[coPercMargVarejo, i] );
    vPrecos[High(vPrecos)].PrecoVarejo              := SFormatDouble( sgItens.Cells[coPrecoVarejo, i] );
    vPrecos[High(vPrecos)].QuantidadeMinPrecoVarejo := SFormatDouble( sgItens.Cells[coQtdeMinimaVarejo, i] );
  end;

  Sessao.getConexaoBanco.IniciarTransacao;

  vRetBanco := _FormacaoCustosPrecos.AtualizarPrecoVarejo(Sessao.getConexaoBanco, FrEmpresas.GetArrayOfInteger, vPrecos);
  if vRetBanco.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;
  RotinaSucesso;
  Carregar(nil);
end;

procedure TFormDefinirPrecosVenda.sgItensArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
var
  vQtdeDecimais: Integer;

  procedure CalcularPrecoSugeridoVenda(pColuna: Integer; pPercMargem: Double; pCustoCompra: Double; pLinha: Integer);
  begin
    sgItens.Cells[pColuna, pLinha] := _Biblioteca.NFormatN(
      Sessao.getCalculosSistema.CalcFormacaoCusto.getPrecoSugerido(
        pPercMargem,
        SFormatDouble(sgItens.Cells[coPercCustoVenda, pLinha]),
        pCustoCompra
      ), 4
    );
  end;

begin
  inherited;
  if ARow = 0 then
    Exit;

  vQtdeDecimais := 2;
  // Apenas ajustando a data digitada
  if ACol in[coDataInicioPreco] then begin
    TextCell := _Biblioteca.FormatarData(TextCell);
    Exit;
  end;

  if ACol in[coPrecoVarejo, coQtdeMinimaVarejo] then
    vQtdeDecimais := 4
  else if ACol = coPercMargVarejo then
    vQtdeDecimais := 5;

  TextCell := _BibliotecaGenerica.NFormatN( SFormatDouble(sgItens.Cells[ACol, ARow]), vQtdeDecimais );

  if ACol in[coPercMargVarejo] then
    CalcularPrecoSugeridoVenda(ACol + 1, SFormatDouble(TextCell), SFormatDouble( sgItens.Cells[coCustoCompra, ARow] ), ARow);

  CalcularCustoComercial(ARow);
end;

procedure TFormDefinirPrecosVenda.CalcularCustoComercial(pLinha: Integer);

  procedure CalcularCustoComercial(pColunaPreco: Integer; pColunaCustoComercial: Integer; pCustoCompra: Double; pLinha: Integer);
  var
    vCustoComercial: Double;
  begin
    vCustoComercial := pCustoCompra + ( SFormatDouble(sgItens.Cells[coPercCustoVenda, pLinha]) * 0.01 * SFormatDouble(sgItens.Cells[pColunaPreco, pLinha]) );
    sgItens.Cells[pColunaCustoComercial, pLinha] := _BibliotecaGenerica.NFormatN(vCustoComercial, 4);
  end;

  procedure CalcularPercentualMargem(pColunaPreco: Integer; pColunaCustoComercial: Integer; pLinha: Integer);
  begin
    // Calculo o % de lucro l�quido
    if SFormatCurr(sgItens.Cells[pColunaPreco, pLinha]) <> 0 then begin
      sgItens.Cells[coPercMargVarejo, pLinha] :=
        _BibliotecaGenerica.NFormatN(
          (SFormatDouble(sgItens.Cells[pColunaPreco, pLinha]) - SFormatDouble(sgItens.Cells[pColunaCustoComercial, pLinha])) / SFormatDouble(sgItens.Cells[pColunaPreco, pLinha]) * 100,
          5
        );
    end;
  end;

begin
  // AQUI SE INICIA O PROPRIAMENTE DITO CUSTO COMERCIAL, NOTE QUE A VARIAVEL QUE RECEBE O VALOR MUDOU
  // Adicionando o percentual de custo da venda
  (* Varejo *)
  CalcularCustoComercial(coPrecoVarejo, coCustoComercial, SFormatDouble(sgItens.Cells[coCustoCompra, pLinha]), pLinha);
  CalcularPercentualMargem(coPrecoVarejo, coCustoComercial, pLinha);
end;

procedure TFormDefinirPrecosVenda.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[
    coProdutoId,
    coCustoCompra,
    coPercCustoVenda,
    coPercMargVarejo,
    coPrecoVarejo,
    coCustoComercial,
    coQtdeMinimaVarejo,
    coPrecoAtual]
  then
    vAlinhamento := taRightJustify
  else if ACol = coSelecionado then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormDefinirPrecosVenda.sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in[
    coPercMargVarejo,
    coPrecoVarejo,
    coCustoComercial,
    coQtdeMinimaVarejo,
    coPrecoAtual,
    coDataPreAtual,
    coDataInicioPreco]
  then begin
    AFont.Color := _Biblioteca.coCorFonteEdicao1;
    ABrush.Color := _Biblioteca.coCorCelulaEdicao1;
  end;
end;

procedure TFormDefinirPrecosVenda.sgItensGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgItens.Cells[ACol, ARow] = '' then
   Exit;

  if ACol = coSelecionado then begin
    if sgItens.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgItens.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormDefinirPrecosVenda.sgItensGetEditMask(Sender: TObject; ACol, ARow: Integer; var Value: string);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in [coDataInicioPreco] then
    Value := '99/99/9999;1;_';
end;

procedure TFormDefinirPrecosVenda.sgItensKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) and (Key = VK_SPACE) then begin
    miMarcarTodosClick(Sender);
    Exit;
  end
  else if Key = VK_SPACE then begin
    miMarcarSelecionadoClick(Sender);
    Exit;
  end;

  inherited;
end;

procedure TFormDefinirPrecosVenda.sgItensSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol in[
    coPercMargVarejo,
    coPrecoVarejo,
    coDataInicioPreco,
    coQtdeMinimaVarejo]
  then
    sgItens.Options := sgItens.Options + [goEditing]
  else
    sgItens.Options := sgItens.Options - [goEditing];
end;

procedure TFormDefinirPrecosVenda.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if FrEmpresas.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio definir as empresas para atualiza��o dos pre�os!');
    _BibliotecaGenerica.SetarFoco(FrEmpresas);
    Abort;
  end;
end;

end.
