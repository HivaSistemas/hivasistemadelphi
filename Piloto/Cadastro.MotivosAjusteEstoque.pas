unit Cadastro.MotivosAjusteEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _MotivosAjusteEstoque,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca, _RecordsEspeciais, _RecordsCadastros,
  Pesquisa.MotivosAjusteEstoque, _HerancaCadastro, _FrameHerancaPrincipal,
  _FrameHenrancaPesquisas, FramePlanosFinanceiros, CheckBoxLuka;

type
  TFormCadastroMotivosAjusteEstoque = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
    FrPlanoFinanceiro: TFrPlanosFinanceiros;
    ckIncideFinanceiro: TCheckBoxLuka;
  private
    procedure PreencherRegistro(pMotivo: RecMotivoAjusteEstoque);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroMotivosAjusteEstoque }

procedure TFormCadastroMotivosAjusteEstoque.BuscarRegistro;
var
  vMotivo: TArray<RecMotivoAjusteEstoque>;
begin
  vMotivo := _MotivosAjusteEstoque.BuscarMotivoAjusteEstoques(Sessao.getConexaoBanco, 0, [eId.AsInt]);
  if vMotivo = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Exit;
  end;

  inherited;
  PreencherRegistro(vMotivo[0]);
end;

procedure TFormCadastroMotivosAjusteEstoque.ExcluirRegistro;
var
  vRetorno: RecRetornoBD;
begin
  vRetorno := _MotivosAjusteEstoque.ExcluirMotivoAjusteEstoque(Sessao.getConexaoBanco, eId.AsInt);
  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroMotivosAjusteEstoque.GravarRegistro(Sender: TObject);
var
  vRetorno: RecRetornoBD;
begin
  inherited;
  vRetorno :=
    _MotivosAjusteEstoque.AtualizarMotivoAjusteEstoque(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text,
      FrPlanoFinanceiro.getDados().PlanoFinanceiroId,
      ckAtivo.CheckedStr,
      ckIncideFinanceiro.CheckedStr
    );

  if vRetorno.TeveErro then begin
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetorno.AsInt);
end;

procedure TFormCadastroMotivosAjusteEstoque.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eDescricao, ckAtivo, FrPlanoFinanceiro, ckIncideFinanceiro], pEditando);

  if pEditando then begin
    SetarFoco(eDescricao);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormCadastroMotivosAjusteEstoque.PesquisarRegistro;
var
  vMotivo: RecMotivoAjusteEstoque;
begin
  vMotivo := RecMotivoAjusteEstoque(Pesquisa.MotivosAjusteEstoque.Pesquisar());
  if vMotivo = nil then
    Exit;

  inherited;
  PreencherRegistro(vMotivo);
end;

procedure TFormCadastroMotivosAjusteEstoque.PreencherRegistro(pMotivo: RecMotivoAjusteEstoque);
begin
  eID.AsInt                     := pMotivo.motivo_ajuste_id;
  eDescricao.Text               := pMotivo.descricao;
  ckAtivo.CheckedStr            := pMotivo.ativo;
  ckIncideFinanceiro.CheckedStr := pMotivo.IncideFinanceiro;

  pMotivo.Free;
end;

procedure TFormCadastroMotivosAjusteEstoque.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o n�o foi informado corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;

  if FrPlanoFinanceiro.EstaVazio then begin
    _Biblioteca.Exclamar('O plano financeiro n�o foi bem informado, verifique!');
    SetarFoco(FrPlanoFinanceiro);
    Abort;
  end;
end;

end.
