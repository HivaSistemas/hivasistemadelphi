unit Frame.EnderecoEstoqueVao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _Sessao, _Biblioteca,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _EnderecoEstoqueVao, Pesquisa.EnderecoEstoqueVao,
  _RecordsCadastros, System.Math, Vcl.Buttons, Vcl.Menus;

type
  TFrEnderecoEstoqueVao = class(TFrameHenrancaPesquisas)
  public
    function GetEnderecoEstoqueVao(pLinha: Integer = -1): RecEnderecoEstoqueVao;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrameEnderecoEstoqueVao }

function TFrEnderecoEstoqueVao.AdicionarDireto: TObject;
var
  vMotivos: TArray<RecEnderecoEstoqueVao>;
begin
  vMotivos := _EnderecoEstoqueVao.BuscarEnderecoEstoqueVao(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vMotivos = nil then
    Result := nil
  else
    Result := vMotivos[0];
end;

function TFrEnderecoEstoqueVao.AdicionarPesquisando: TObject;
begin
  Result := Pesquisa.EnderecoEstoqueVao.Pesquisar();
end;

function TFrEnderecoEstoqueVao.GetEnderecoEstoqueVao(pLinha: Integer): RecEnderecoEstoqueVao;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecEnderecoEstoqueVao(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrEnderecoEstoqueVao.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecEnderecoEstoqueVao(FDados[i]).Vao_id = RecEnderecoEstoqueVao(pSender).Vao_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrEnderecoEstoqueVao.MontarGrid;
var
  i: Integer;
  vSender: RecEnderecoEstoqueVao;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecEnderecoEstoqueVao(FDados[i]);
      AAdd([IntToStr(vSender.Vao_id), vSender.descricao]);
    end;
  end;
end;

end.
