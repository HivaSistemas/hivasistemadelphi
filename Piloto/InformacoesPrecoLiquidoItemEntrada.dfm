inherited FormInformacoesPrecoLiquidoItemEntrada: TFormInformacoesPrecoLiquidoItemEntrada
  Caption = 'Informa'#231#245'es do pre'#231'o l'#237'quido do item da entrada'
  ClientHeight = 199
  ClientWidth = 431
  ExplicitWidth = 437
  ExplicitHeight = 228
  PixelsPerInch = 96
  TextHeight = 14
  object lbl15: TLabel [0]
    Left = 3
    Top = 2
    Width = 76
    Height = 14
    Caption = 'Pre'#231'o unit'#225'rio'
  end
  object lb1: TLabel [1]
    Left = 110
    Top = 2
    Width = 92
    Height = 14
    Caption = 'Outras despesas'
  end
  object lb2: TLabel [2]
    Left = 217
    Top = 2
    Width = 51
    Height = 14
    Caption = 'Desconto'
  end
  object lb3: TLabel [3]
    Left = 324
    Top = 2
    Width = 101
    Height = 14
    Caption = 'Frete proporcional'
  end
  object lb4: TLabel [4]
    Left = 3
    Top = 42
    Width = 14
    Height = 14
    Caption = 'IPI'
  end
  object lb5: TLabel [5]
    Left = 110
    Top = 42
    Width = 41
    Height = 14
    Caption = 'ICMS ST'
  end
  object lb6: TLabel [6]
    Left = 110
    Top = 82
    Width = 26
    Height = 14
    Caption = 'ICMS'
  end
  object lb7: TLabel [7]
    Left = 217
    Top = 82
    Width = 57
    Height = 14
    Caption = 'ICMS Frete'
  end
  object lb8: TLabel [8]
    Left = 324
    Top = 82
    Width = 16
    Height = 14
    Caption = 'PIS'
  end
  object lb9: TLabel [9]
    Left = 3
    Top = 122
    Width = 38
    Height = 14
    Caption = 'COFINS'
  end
  object lb10: TLabel [10]
    Left = 217
    Top = 42
    Width = 74
    Height = 14
    Caption = 'Outros custos'
  end
  object lb11: TLabel [11]
    Left = 324
    Top = 122
    Width = 63
    Height = 14
    Caption = 'Pre'#231'o l'#237'quido'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb12: TLabel [12]
    Left = 3
    Top = 82
    Width = 56
    Height = 14
    Caption = 'Pre'#231'o final'
  end
  object lb13: TLabel [13]
    Left = 110
    Top = 122
    Width = 47
    Height = 14
    Caption = 'PIS Frete'
  end
  object lb14: TLabel [14]
    Left = 217
    Top = 122
    Width = 69
    Height = 14
    Caption = 'COFINS Frete'
  end
  object lb15: TLabel [15]
    Left = 324
    Top = 42
    Width = 27
    Height = 14
    Caption = 'Difal'
  end
  inherited pnOpcoes: TPanel
    Top = 162
    Width = 431
    TabOrder = 15
    ExplicitTop = 162
    ExplicitWidth = 431
  end
  object ePrecoUnitario: TEditLuka
    Left = 3
    Top = 16
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object eOutrasDespesas: TEditLuka
    Left = 110
    Top = 16
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object eDesconto: TEditLuka
    Left = 217
    Top = 16
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object eFreteProporcional: TEditLuka
    Left = 326
    Top = 14
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object eIPI: TEditLuka
    Left = 3
    Top = 56
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 4
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object eICMSSt: TEditLuka
    Left = 110
    Top = 56
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 5
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object eICMS: TEditLuka
    Left = 110
    Top = 96
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 8
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object eICMSFrete: TEditLuka
    Left = 217
    Top = 96
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 9
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object ePIS: TEditLuka
    Left = 324
    Top = 96
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 10
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object eCOFINS: TEditLuka
    Left = 3
    Top = 136
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 11
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object eOutrosCustos: TEditLuka
    Left = 217
    Top = 56
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 6
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object ePrecoLiquido: TEditLuka
    Left = 324
    Top = 136
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 14
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object ePrecoFinal: TEditLuka
    Left = 3
    Top = 96
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 7
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object ePisFrete: TEditLuka
    Left = 110
    Top = 136
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 12
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object eCOFINSFrete: TEditLuka
    Left = 217
    Top = 136
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 13
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
  object eValorDifal: TEditLuka
    Left = 324
    Top = 56
    Width = 103
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 16
    Text = '0,000000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 6
    NaoAceitarEspaco = False
  end
end
