inherited FrBuscarLotes: TFrBuscarLotes
  Width = 280
  Height = 140
  ExplicitWidth = 280
  ExplicitHeight = 140
  inherited sgValores: TGridLuka
    Width = 280
    Height = 123
    ColCount = 6
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    PopupMenu = nil
    OnDrawCell = sgValoresDrawCell
    OnGetEditMask = sgValoresGetEditMask
    OnSelectCell = sgValoresSelectCell
    CorLinhaFoco = 38619
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Qtde.'
      'Lote'
      'Bitola'
      'Tonalidade'
      'Dt.fabric.'
      'Dt.vencto.')
    OnArrumarGrid = sgValoresArrumarGrid
    RealColCount = 6
    Indicador = True
    ExplicitWidth = 280
    ExplicitHeight = 123
    ColWidths = (
      57
      61
      67
      72
      64
      64)
  end
  inherited StaticTextLuka1: TStaticTextLuka
    Width = 280
    Caption = 'Lotes'
    ExplicitWidth = 280
  end
  inherited pmOpcoes: TPopupMenu
    Left = 168
    inherited miInserir: TMenuItem
      Visible = False
    end
    inherited miN1: TMenuItem
      Visible = False
    end
    inherited miSubir: TMenuItem
      Visible = False
    end
    inherited miDescer: TMenuItem
      Visible = False
    end
  end
end
