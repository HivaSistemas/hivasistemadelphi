unit BuscarDadosMarcaProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameMarcas, _RecordsCadastros,
  _Biblioteca;

type
  TFormBuscarDadosMarcaProduto = class(TFormHerancaFinalizar)
    FrMarca: TFrMarcas;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosMarcaProdutos: TRetornoTelaFinalizar<RecMarcas>;

implementation

{$R *.dfm}

{ BuscarDadosMarcaProduto }

function BuscarDadosMarcaProdutos: TRetornoTelaFinalizar<RecMarcas>;
var
  vForm: TFormBuscarDadosMarcaProduto;
begin
  vForm := TFormBuscarDadosMarcaProduto.Create(Application);

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FrMarca.GetMarca(0);

//  FreeAndNil(vForm);
end;

end.
