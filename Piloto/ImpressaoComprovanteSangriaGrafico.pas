unit ImpressaoComprovanteSangriaGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos,
  QRCtrls, QRExport, QRPDFFilt, QRWebFilt, QuickRpt, Vcl.ExtCtrls, _Sessao, _MovimentosTurnos, _Biblioteca;

type
  TFormImpressaoComprovanteSangriaGrafico = class(TFormHerancaRelatoriosGraficos)
    qrValoresSangria: TQRMemo;
    QRShape2: TQRShape;
    qrl5: TQRLabel;
    qrObservacoes: TQRMemo;
    qrUsuarioSangria: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure Imprimir(pMovimentoId: Integer);

implementation

{$R *.dfm}

procedure Imprimir(pMovimentoId: Integer);
var
  vRecImpressora: RecImpressora;
  vMovimentos: TArray<RecMovimentosTurnos>;
  vForm: TFormImpressaoComprovanteSangriaGrafico;

  procedure AddValorSangria( pValor: Currency; pDescricao: string );
  begin
    if pValor = 0 then
      Exit;

    vForm.qrValoresSangria.Lines.Add(pDescricao + ' R$ ' + NFormat(pValor) );
  end;

begin

  if pMovimentoId = 0 then
    Exit;

  vRecImpressora := _Sessao.Sessao.getImpressora( toComprovanteCaixa );
  if vRecImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;

  vMovimentos := _MovimentosTurnos.BuscarMovimentosTurnos(Sessao.getConexaoBanco, 1, [pMovimentoId]);
  if vMovimentos = nil then
    Exit;

  vForm := TFormImpressaoComprovanteSangriaGrafico.Create(nil, vRecImpressora);
  vForm.PreencherCabecalho( Sessao.getEmpresaLogada.EmpresaId, False );

  vForm.qrValoresSangria.Lines.Clear;

  AddValorSangria(vMovimentos[0].ValorDinheiro, 'Dinheiro.: ');
  AddValorSangria(vMovimentos[0].ValorCheque,   'Cheque...: ');
  AddValorSangria(vMovimentos[0].ValorCartao,   'Cart�o...: ');
  AddValorSangria(vMovimentos[0].ValorCobranca, 'Cobran�a.: ');

  vForm.qrObservacoes.Lines.Clear;
  vForm.qrObservacoes.Lines.Text := 'Obs: ' + vMovimentos[0].Observacao;

  vForm.qrUsuarioSangria.Caption := vMovimentos[0].Funcionario;

  vForm.Imprimir;

  vForm.Free;
end;

end.
