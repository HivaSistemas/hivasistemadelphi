inherited FormCadastroProdutosRelacionados: TFormCadastroProdutosRelacionados
  Caption = 'Produtos relacionados'
  ClientWidth = 569
  OnShow = FormShow
  ExplicitWidth = 575
  ExplicitHeight = 292
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 472
    Top = 50
    Width = 77
    Height = 14
    Caption = 'Qtde sugerida'
  end
  inherited pnOpcoes: TPanel
    TabOrder = 3
    inherited sbDesfazer: TSpeedButton
      Top = 50
      ExplicitTop = 50
    end
    inherited sbExcluir: TSpeedButton
      Left = -112
      Visible = False
      ExplicitLeft = -112
    end
    inherited sbPesquisar: TSpeedButton
      Left = -112
      Enabled = False
      Visible = False
      ExplicitLeft = -112
    end
  end
  inherited eID: TEditLuka
    TabOrder = 5
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 599
    TabOrder = 6
    Visible = False
    ExplicitLeft = 599
  end
  inline FrProdutoPrincipal: TFrProdutos
    Left = 124
    Top = 4
    Width = 442
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 0
    TabStop = True
    ExplicitLeft = 124
    ExplicitTop = 4
    ExplicitWidth = 442
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 417
      Height = 23
      ExplicitWidth = 417
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 442
      ExplicitWidth = 442
      inherited lbNomePesquisa: TLabel
        Width = 94
        Caption = 'Produto principal'
        ExplicitWidth = 94
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 337
        ExplicitLeft = 337
      end
    end
    inherited pnPesquisa: TPanel
      Left = 417
      Height = 24
      ExplicitLeft = 417
      ExplicitHeight = 24
    end
  end
  inline FrProdutoRelacionado: TFrProdutos
    Left = 124
    Top = 49
    Width = 346
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 124
    ExplicitTop = 49
    ExplicitWidth = 346
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 321
      Height = 23
      ExplicitWidth = 321
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 346
      ExplicitWidth = 346
      inherited lbNomePesquisa: TLabel
        Width = 117
        Caption = 'Produtos relacionado'
        ExplicitWidth = 117
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 241
        ExplicitLeft = 241
      end
    end
    inherited pnPesquisa: TPanel
      Left = 321
      Height = 24
      ExplicitLeft = 321
      ExplicitHeight = 24
    end
  end
  object eQuantidadeSugestao: TEditLuka
    Left = 472
    Top = 65
    Width = 93
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 2
    Text = '0,0000'
    OnKeyDown = eQuantidadeSugestaoKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object sgProdutosRelacionados: TGridLuka
    Left = 124
    Top = 91
    Width = 441
    Height = 169
    ColCount = 3
    DefaultRowHeight = 19
    DrawingStyle = gdsClassic
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRowSelect]
    TabOrder = 4
    OnDblClick = sgProdutosRelacionadosDblClick
    OnDrawCell = sgProdutosRelacionadosDrawCell
    OnKeyDown = sgProdutosRelacionadosKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Qtde. sugerida')
    Grid3D = False
    RealColCount = 3
    AtivarPopUpSelecao = False
    ColWidths = (
      54
      274
      93)
  end
end
