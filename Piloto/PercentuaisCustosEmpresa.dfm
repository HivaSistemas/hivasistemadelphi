inherited FormPercentuaisCustoEmpresa: TFormPercentuaisCustoEmpresa
  ActiveControl = FrEmpresa.sgPesquisa
  Caption = 'Custo fixo loja'
  ClientHeight = 365
  ClientWidth = 552
  ExplicitWidth = 558
  ExplicitHeight = 394
  PixelsPerInch = 96
  TextHeight = 14
  object lb5: TLabel [0]
    Left = 223
    Top = 100
    Width = 28
    Height = 14
    Alignment = taRightJustify
    Caption = '% PIS'
  end
  object Label1: TLabel [1]
    Left = 201
    Top = 126
    Width = 50
    Height = 14
    Alignment = taRightJustify
    Caption = '% COFINS'
  end
  object Label2: TLabel [2]
    Left = 219
    Top = 152
    Width = 32
    Height = 14
    Alignment = taRightJustify
    Caption = '% IRPJ'
  end
  object Label3: TLabel [3]
    Left = 217
    Top = 178
    Width = 34
    Height = 14
    Alignment = taRightJustify
    Caption = '% CSLL'
  end
  object Label4: TLabel [4]
    Left = 175
    Top = 204
    Width = 76
    Height = 14
    Alignment = taRightJustify
    Caption = '% Frete venda'
  end
  object Label5: TLabel [5]
    Left = 188
    Top = 230
    Width = 63
    Height = 14
    Alignment = taRightJustify
    Caption = '% Custo fixo'
  end
  object Label6: TLabel [6]
    Left = 186
    Top = 256
    Width = 65
    Height = 14
    Alignment = taRightJustify
    Caption = '% Comissao'
  end
  inherited pnOpcoes: TPanel
    Height = 365
    TabOrder = 8
    ExplicitHeight = 365
    inherited sbExcluir: TSpeedButton
      Left = -120
      Visible = False
      ExplicitLeft = -120
    end
    inherited sbPesquisar: TSpeedButton
      Left = -120
      Visible = False
      ExplicitLeft = -120
    end
  end
  inline FrEmpresa: TFrEmpresas
    Left = 125
    Top = 5
    Width = 426
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 0
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 5
    ExplicitWidth = 426
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 401
      Height = 23
      ExplicitWidth = 401
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 426
      ExplicitWidth = 426
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 356
        ExplicitLeft = 356
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 401
      Height = 24
      ExplicitLeft = 401
      ExplicitHeight = 24
    end
  end
  object ePercentualPis: TEditLuka
    Left = 257
    Top = 92
    Width = 136
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 14
    TabOrder = 1
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object ePercentualCOFINS: TEditLuka
    Left = 257
    Top = 118
    Width = 136
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 14
    TabOrder = 2
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object ePercentualIRPJ: TEditLuka
    Left = 257
    Top = 144
    Width = 136
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 14
    TabOrder = 3
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object ePercentualCSLL: TEditLuka
    Left = 257
    Top = 170
    Width = 136
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 14
    TabOrder = 4
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object ePercentualFreteVenda: TEditLuka
    Left = 257
    Top = 196
    Width = 136
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 14
    TabOrder = 5
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object ePercentualCustoFixo: TEditLuka
    Left = 257
    Top = 222
    Width = 136
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 14
    TabOrder = 6
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object ePercentualComissao: TEditLuka
    Left = 257
    Top = 248
    Width = 136
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 14
    TabOrder = 7
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
end
