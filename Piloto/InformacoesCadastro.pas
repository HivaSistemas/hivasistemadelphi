unit InformacoesCadastro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, EditLukaData,
  Vcl.StdCtrls, Vcl.ExtCtrls, RadioGroupLuka, Vcl.Mask, EditCpfCnpjLuka,
  EditLuka, Vcl.Buttons, FrameDiversosEnderecos, FrameCadastrosTelefones,
  EditTelefoneLuka, _FrameHerancaPrincipal, FrameEndereco, ComboBoxLuka,
  Vcl.ComCtrls, CheckBoxLuka, _RelacaoCadastros, _RecordsCadastros, _Biblioteca,
  _Sessao, _Cadastros, _CadastrosTelefones, _DiversosEnderecos;

type
  TFormInformacoesCadastro = class(TFormHerancaFinalizar)
    Label1: TLabel;
    eID: TEditLuka;
    eCPF_CNPJ: TEditCPF_CNPJ_Luka;
    lbCPF_CNPJ: TLabel;
    eRazaoSocial: TEditLuka;
    lbRazaoSocialNome: TLabel;
    rgTipoPessoa: TRadioGroupLuka;
    eDataCadastro: TEditLukaData;
    Label2: TLabel;
    eNomeFantasia: TEditLuka;
    lbNomeFantasiaApelido: TLabel;
    pcDados: TPageControl;
    tsPrincipais: TTabSheet;
    lb4: TLabel;
    lbDataNascimento: TLabel;
    lb6: TLabel;
    lb8: TLabel;
    lb10: TLabel;
    lb7: TLabel;
    lb9: TLabel;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    cbEstadoCivil: TComboBoxLuka;
    rgSexo: TRadioGroupLuka;
    eDataNascimento: TEditLukaData;
    eInscricaoEstadual: TEditLuka;
    eOrgaoExpedidor: TEditLuka;
    eCNAE: TMaskEdit;
    eRG: TEditLuka;
    FrEndereco: TFrEndereco;
    eEmail: TEditLuka;
    eTelefone: TEditTelefoneLuka;
    eCelular: TEditTelefoneLuka;
    eFax: TEditTelefoneLuka;
    tsContatos: TTabSheet;
    FrTelefones: TFrCadastrosTelefones;
    tsEnderecos: TTabSheet;
    FrDiversosEnderecos: TFrDiversosEnderecos;
    tsObservacoes: TTabSheet;
    eObservacoes: TMemo;
    pPanel: TPanel;
    ckAtivo: TCheckBoxLuka;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  procedure Informar(pCadastroId: Integer);

implementation

{$R *.dfm}

procedure Informar(pCadastroId: Integer);
var
  vForm: TFormInformacoesCadastro;
  vCadastro: TArray<RecCadastros>;
  vTelefones: TArray<RecCadastrosTelefones>;
  vEnderecos: TArray<RecDiversosEnderecos>;
begin
  if pCadastroId = 0 then
    Exit;

  vCadastro := _Cadastros.BuscarCadastros(Sessao.getConexaoBanco, 0, [pCadastroId]);

  if vCadastro = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vTelefones := _CadastrosTelefones.BuscarCadastrosTelefones(Sessao.getConexaoBanco, 0, [pCadastroId]);
  vEnderecos := _DiversosEnderecos.BuscarDiversosEnderecos(Sessao.getConexaoBanco, 0, [pCadastroId], False);

  vForm := TFormInformacoesCadastro.Create(Application);
  vForm.ReadOnlyTodosObjetos(True);

  if Length(RetornaNumeros(vCadastro[0].cpf_cnpj)) = 11 then
    vForm.eCPF_CNPJ.EditMask := '999.999.999-99'
  else
    vForm.eCPF_CNPJ.EditMask := '99.999.999/9999-99';

  vForm.eID.AsInt               := vCadastro[0].cadastro_id;
  vForm.eDataCadastro.AsData    := vCadastro[0].data_cadastro;
  vForm.eRazaoSocial.Text       := vCadastro[0].razao_social;
  vForm.eNomeFantasia.Text      := vCadastro[0].nome_fantasia;
  vForm.eCPF_CNPJ.Text          := vCadastro[0].cpf_cnpj;
  vForm.eRG.Text                := vCadastro[0].rg;
  vForm.eOrgaoExpedidor.Text    := vCadastro[0].orgao_expedidor_rg;
  vForm.eInscricaoEstadual.Text := vCadastro[0].inscricao_estadual;

  vForm.eDataNascimento.AsData  := vCadastro[0].data_nascimento;
  vForm.eEmail.Text             := vCadastro[0].e_mail;
  vForm.eCNAE.Text              := vCadastro[0].cnae;
  vForm.eTelefone.Text          := vCadastro[0].TelefonePrincipal;
  vForm.eCelular.Text           := vCadastro[0].TelefoneCelular;
  vForm.eFax.Text               := vCadastro[0].TelefoneFax;
  vForm.FrTelefones.Telefones   := vTelefones;
  vForm.ckAtivo.Checked         := (vCadastro[0].ativo = 'S');
  vForm.eObservacoes.Lines.Text := vCadastro[0].Observacoes;

  vForm.rgSexo.SetIndicePorValor(vCadastro[0].sexo);
  vForm.rgTipoPessoa.SetIndicePorValor(vCadastro[0].tipo_pessoa);
  vForm.cbEstadoCivil.SetIndicePorValor(vCadastro[0].estado_civil);
  vForm.FrEndereco.setLogradouro(vCadastro[0].logradouro);
  vForm.FrEndereco.setComplemento(vCadastro[0].complemento);
  vForm.FrEndereco.setNumero(vCadastro[0].numero);
  vForm.FrEndereco.setBairroId(vCadastro[0].bairro_id);
  vForm.FrEndereco.setPontoReferencia(vCadastro[0].ponto_referencia);
  vForm.FrEndereco.setCep(vCadastro[0].cep);
  vForm.FrDiversosEnderecos.setDiversosEnderecos(vEnderecos);

  vForm.ShowModal;

  FreeAndNil(vForm);
  vCadastro[0].Free;
end;

end.

