inherited FormBuscarVersao: TFormBuscarVersao
  Caption = 'Buscar dados vers'#227'o'
  ClientHeight = 82
  ClientWidth = 262
  ExplicitWidth = 268
  ExplicitHeight = 111
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 45
    Width = 262
    inherited sbFinalizar: TSpeedButton
      Left = 73
      ExplicitLeft = 73
    end
  end
  object eVersao: TEditLuka
    Left = 8
    Top = 8
    Width = 246
    Height = 22
    CharCase = ecUpperCase
    Color = clMenuBar
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    TipoCampo = tcTexto
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
end
