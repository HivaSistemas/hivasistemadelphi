inherited FormCadastroCreditoManualReceber: TFormCadastroCreditoManualReceber
  Caption = 'Lan'#231'amento de cr'#233'dito a receber'
  ClientHeight = 312
  ExplicitHeight = 341
  PixelsPerInch = 96
  TextHeight = 14
  inherited lbl2: TLabel
    Left = 435
    ExplicitLeft = 435
  end
  inherited pnOpcoes: TPanel
    Height = 312
    ExplicitHeight = 312
    inherited sbDesfazer: TSpeedButton
      Top = 48
      ExplicitTop = 48
    end
    inherited sbPesquisar: TSpeedButton
      Top = 94
      ExplicitTop = 94
    end
  end
  inherited FrEmpresa: TFrEmpresas
    Width = 241
    ExplicitWidth = 241
    inherited sgPesquisa: TGridLuka
      Width = 216
      ExplicitWidth = 216
    end
    inherited PnTitulos: TPanel
      Width = 241
      ExplicitWidth = 241
      inherited lbNomePesquisa: TLabel
        Height = 15
      end
      inherited pnSuprimir: TPanel
        Left = 136
        ExplicitLeft = 136
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 216
      ExplicitLeft = 216
    end
  end
  inherited eValorCreditoGerar: TEditLuka
    Font.Color = clBlue
  end
  inherited FrPlanoFinanceiro: TFrPlanosFinanceiros
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Height = 15
      end
      inherited pnSuprimir: TPanel
        Left = 143
        ExplicitLeft = 143
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
  end
  inherited FrCentroCusto: TFrCentroCustos
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Height = 15
      end
      inherited pnSuprimir: TPanel
        Left = 93
        ExplicitLeft = 93
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
  end
  inherited ckImportacao: TCheckBox
    Left = 602
    Top = 289
    Width = 99
    Caption = 'Cr'#233'dito manual'
    ExplicitLeft = 602
    ExplicitTop = 289
    ExplicitWidth = 99
  end
  inherited meObservacoes: TMemoAltis
    Left = 435
    Width = 267
    Height = 184
    ExplicitLeft = 435
    ExplicitWidth = 267
    ExplicitHeight = 184
  end
  inherited FrCadastro: TFrameCadastros
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 61
        Height = 15
        Caption = 'Fornecedor'
        ExplicitWidth = 61
      end
      inherited pnSuprimir: TPanel
        Left = 144
        ExplicitLeft = 144
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
  end
  inherited FrPagamentoFinanceiro: TFrPagamentoFinanceiro
    Left = 126
    ExplicitLeft = 126
    inherited grpFechamento: TGroupBox
      inherited sbBuscarDadosDinheiro: TImage
        Visible = True
      end
      inherited stPagamento: TStaticText
        Width = 280
        Color = clSilver
        ExplicitWidth = 280
      end
      inherited stValorTotal: TStaticText
        Color = clSilver
      end
      inherited stDinheiroDefinido: TStaticText
        Visible = True
      end
      inherited eValorPix: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
  end
end
