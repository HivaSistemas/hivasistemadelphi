inherited FormCadastroVeiculos: TFormCadastroVeiculos
  Caption = 'Cadastro de ve'#237'culos'
  ClientHeight = 255
  ClientWidth = 594
  ExplicitWidth = 600
  ExplicitHeight = 284
  PixelsPerInch = 96
  TextHeight = 14
  object Label7: TLabel [0]
    Left = 126
    Top = 97
    Width = 82
    Height = 14
    Margins.Left = 0
    Margins.Top = 1
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alCustom
    Caption = 'Tipo de ve'#237'culo'
  end
  inherited Label1: TLabel
    Top = 13
    ExplicitTop = 13
  end
  object lb1: TLabel [2]
    Left = 212
    Top = 13
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  object Label2: TLabel [3]
    Left = 473
    Top = 13
    Width = 29
    Height = 14
    Caption = 'Placa'
  end
  object Label6: TLabel [4]
    Left = 126
    Top = 53
    Width = 21
    Height = 14
    Caption = 'Ano'
  end
  object Label5: TLabel [5]
    Left = 212
    Top = 53
    Width = 109
    Height = 14
    Margins.Left = 0
    Margins.Top = 1
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alCustom
    Caption = 'Tipo de combust'#237'vel'
  end
  object Label3: TLabel [6]
    Left = 359
    Top = 54
    Width = 17
    Height = 14
    Caption = 'Cor'
  end
  object Label4: TLabel [7]
    Left = 473
    Top = 54
    Width = 36
    Height = 14
    Caption = 'Chassi'
  end
  object Label8: TLabel [8]
    Left = 275
    Top = 97
    Width = 74
    Height = 14
    Caption = 'Peso m'#225'x.(Kg)'
  end
  object Label9: TLabel [9]
    Left = 359
    Top = 97
    Width = 23
    Height = 14
    Caption = 'Tara'
  end
  object cbTipoVeiculo: TComboBoxLuka [10]
    Left = 126
    Top = 112
    Width = 141
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 9
    Items.Strings = (
      'Carro'#231'a'
      'Moto'
      'Carro'
      'Caminhonete'
      'Caminh'#227'o')
    Valores.Strings = (
      'CAC'
      'MOT'
      'CAR'
      'CAI'
      'CAM')
    AsInt = 0
  end
  inherited pnOpcoes: TPanel
    Height = 255
    TabOrder = 2
    ExplicitHeight = 255
  end
  inherited eID: TEditLuka
    Top = 27
    TabOrder = 0
    ExplicitTop = 27
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 547
    TabOrder = 4
    ExplicitLeft = 547
  end
  object eNome: TEditLuka
    Left = 212
    Top = 27
    Width = 255
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ePlaca: TMaskEditLuka
    Left = 473
    Top = 27
    Width = 65
    Height = 22
    CharCase = ecUpperCase
    EditMask = '!LLL-0000;1;_'
    MaxLength = 8
    TabOrder = 3
    Text = '   -    '
    OnKeyDown = ProximoCampo
  end
  object eAnoFabricacao: TEditLuka
    Left = 126
    Top = 68
    Width = 80
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    MaxLength = 8
    TabOrder = 5
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    ValorMaximo = 2050.000000000000000000
    NaoAceitarEspaco = False
  end
  object cbTipoCombustivel: TComboBoxLuka
    Left = 212
    Top = 68
    Width = 141
    Height = 22
    BevelKind = bkFlat
    Style = csOwnerDrawFixed
    Color = clWhite
    TabOrder = 6
    Items.Strings = (
      #193'lcool'
      'Gasolina'
      'Flex'
      'Diesel'
      'GNV')
    Valores.Strings = (
      'A'
      'G'
      'F'
      'D'
      'N')
    AsInt = 0
  end
  object eCor: TEditLuka
    Left = 359
    Top = 68
    Width = 108
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 15
    TabOrder = 7
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eChassi: TEditLuka
    Left = 473
    Top = 68
    Width = 116
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 20
    TabOrder = 8
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object ePesoMaximo: TEditLuka
    Left = 275
    Top = 112
    Width = 78
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 10
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    ValorMaximo = 99999.000000000000000000
    NaoAceitarEspaco = False
  end
  object eTara: TEditLuka
    Left = 359
    Top = 112
    Width = 78
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 11
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = True
    AsInt = 0
    CasasDecimais = 0
    ValorMaximo = 99999.000000000000000000
    NaoAceitarEspaco = False
  end
  inline FrMotorista: TFrMotoristas
    Left = 126
    Top = 141
    Width = 311
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 141
    ExplicitWidth = 311
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 286
      Height = 23
      ExplicitWidth = 286
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 311
      ExplicitWidth = 311
      inherited lbNomePesquisa: TLabel
        Width = 53
        Caption = 'Motorista'
        ExplicitWidth = 53
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 241
        ExplicitLeft = 241
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 286
      Height = 24
      ExplicitLeft = 286
      ExplicitHeight = 24
    end
  end
end
