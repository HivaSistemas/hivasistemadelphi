inherited FormInclusaoTitulosReceber: TFormInclusaoTitulosReceber
  Caption = 'Inclus'#227'o de t'#237'tulos a receber'
  ClientHeight = 417
  ClientWidth = 709
  ExplicitWidth = 715
  ExplicitHeight = 446
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [1]
    Left = 125
    Top = 86
    Width = 60
    Height = 14
    Caption = 'Valor t'#237'tulo'
  end
  object lbllb12: TLabel [2]
    Left = 476
    Top = 86
    Width = 65
    Height = 14
    Caption = 'Dt. emiss'#227'o'
  end
  object lbl2: TLabel [3]
    Left = 126
    Top = 126
    Width = 69
    Height = 14
    Caption = 'Observa'#231#245'es'
  end
  object lb2: TLabel [4]
    Left = 562
    Top = 86
    Width = 63
    Height = 14
    Caption = 'Dt. cont'#225'bil'
  end
  object Label2: TLabel [5]
    Left = 245
    Top = 86
    Width = 79
    Height = 14
    Caption = 'Valor reten'#231#227'o'
  end
  object Label3: TLabel [6]
    Left = 358
    Top = 86
    Width = 63
    Height = 14
    Caption = '% Reten'#231#227'o'
  end
  inherited pnOpcoes: TPanel
    Height = 417
    TabOrder = 12
    ExplicitHeight = 374
    inherited sbDesfazer: TSpeedButton
      Top = 48
      ExplicitTop = 48
    end
    inherited sbExcluir: TSpeedButton
      Visible = False
    end
    inherited sbPesquisar: TSpeedButton
      Top = 94
      Visible = False
      ExplicitTop = 94
    end
    inherited sbLogs: TSpeedButton
      Left = -119
      ExplicitLeft = -119
    end
  end
  inherited eID: TEditLuka
    Width = 59
    TabOrder = 0
    ExplicitWidth = 59
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 765
    TabOrder = 13
    Visible = False
    ExplicitLeft = 765
  end
  inline FrCliente: TFrClientes
    Left = 191
    Top = 2
    Width = 258
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 191
    ExplicitTop = 2
    ExplicitWidth = 258
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 233
      Height = 23
      ExplicitWidth = 250
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 258
      ExplicitWidth = 275
      inherited lbNomePesquisa: TLabel
        Width = 39
        Caption = 'Cliente'
        ExplicitWidth = 39
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 153
        ExplicitLeft = 170
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 233
      Height = 24
      ExplicitLeft = 250
      ExplicitHeight = 24
    end
  end
  inline FrEmpresa: TFrEmpresas
    Left = 457
    Top = 2
    Width = 246
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 457
    ExplicitTop = 2
    ExplicitWidth = 246
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 221
      Height = 23
      ExplicitWidth = 250
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 246
      ExplicitWidth = 275
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Empresa'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 141
        ExplicitLeft = 170
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 221
      Height = 24
      ExplicitLeft = 250
      ExplicitHeight = 24
    end
  end
  object eValorDocumento: TEditLuka
    Left = 126
    Top = 100
    Width = 111
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    Text = '0,00'
    OnChange = eValorDocumentoChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  inline FrCentroCustos: TFrCentroCustos
    Left = 378
    Top = 45
    Width = 273
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 378
    ExplicitTop = 45
    ExplicitWidth = 273
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 248
      Height = 23
      ExplicitWidth = 248
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 273
      ExplicitWidth = 273
      inherited lbNomePesquisa: TLabel
        Width = 90
        ExplicitWidth = 90
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 168
        ExplicitLeft = 168
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 248
      Height = 24
      ExplicitLeft = 248
      ExplicitHeight = 24
    end
  end
  inline FrDadosCobranca: TFrDadosCobranca
    Left = 125
    Top = 168
    Width = 578
    Height = 244
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 11
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 168
    ExplicitHeight = 244
    inherited pnInformacoesPrincipais: TPanel
      inherited lbllb10: TLabel
        Width = 39
        Height = 14
        ExplicitWidth = 39
        ExplicitHeight = 14
      end
      inherited lbllb9: TLabel
        Width = 29
        Height = 14
        ExplicitWidth = 29
        ExplicitHeight = 14
      end
      inherited lbllb6: TLabel
        Width = 28
        Height = 14
        ExplicitWidth = 28
        ExplicitHeight = 14
      end
      inherited lbllb2: TLabel
        Width = 64
        Height = 14
        ExplicitWidth = 64
        ExplicitHeight = 14
      end
      inherited lbl2: TLabel
        Width = 62
        Height = 14
        ExplicitWidth = 62
        ExplicitHeight = 14
      end
      inherited eRepetir: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited ePrazo: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eDataVencimento: TEditLukaData
        Height = 22
        ExplicitHeight = 22
      end
      inherited eDocumento: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
    inherited pnInformacoesBoleto: TPanel
      inherited lbllb31: TLabel
        Width = 79
        Height = 14
        ExplicitWidth = 79
        ExplicitHeight = 14
      end
      inherited lbl1: TLabel
        Width = 92
        Height = 14
        ExplicitWidth = 92
        ExplicitHeight = 14
      end
      inherited eNossoNumero: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eCodigoBarras: TMaskEditLuka
        Height = 22
        ExplicitHeight = 22
      end
    end
    inherited pn1: TPanel
      Height = 131
      ExplicitHeight = 131
      inherited sgCobrancas: TGridLuka
        Height = 127
        ExplicitHeight = 127
      end
    end
    inherited pnInformacoesCheque: TPanel
      inherited lbllb1: TLabel
        Width = 33
        Height = 14
        ExplicitWidth = 33
        ExplicitHeight = 14
      end
      inherited lbllb3: TLabel
        Width = 43
        Height = 14
        ExplicitWidth = 43
        ExplicitHeight = 14
      end
      inherited lbllb4: TLabel
        Width = 79
        Height = 14
        ExplicitWidth = 79
        ExplicitHeight = 14
      end
      inherited lbllb5: TLabel
        Width = 71
        Height = 14
        ExplicitWidth = 71
        ExplicitHeight = 14
      end
      inherited lbl3: TLabel
        Width = 85
        Height = 14
        ExplicitWidth = 85
        ExplicitHeight = 14
      end
      inherited lbl4: TLabel
        Width = 48
        Height = 14
        ExplicitWidth = 48
        ExplicitHeight = 14
      end
      inherited lbCPF_CNPJ: TLabel
        Width = 52
        Height = 14
        ExplicitWidth = 52
        ExplicitHeight = 14
      end
      inherited eBanco: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eAgencia: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eContaCorrente: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eNumeroCheque: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eNomeEmitente: TEditLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eTelefoneEmitente: TEditTelefoneLuka
        Height = 22
        ExplicitHeight = 22
      end
      inherited eCPF_CNPJ: TEditCPF_CNPJ_Luka
        Height = 22
        ExplicitHeight = 22
      end
    end
  end
  inline FrPlanoFinanceiro: TFrPlanosFinanceiros
    Left = 126
    Top = 45
    Width = 251
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 126
    ExplicitTop = 45
    ExplicitWidth = 251
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 226
      Height = 24
      ExplicitWidth = 226
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 251
      ExplicitWidth = 251
      inherited lbNomePesquisa: TLabel
        Width = 100
        ExplicitWidth = 100
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 146
        ExplicitLeft = 146
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 226
      Height = 25
      ExplicitLeft = 226
      ExplicitHeight = 25
    end
  end
  object eDataEmissao: TEditLukaData
    Left = 476
    Top = 100
    Width = 79
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object meObservacoes: TMemoAltis
    Left = 125
    Top = 140
    Width = 578
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 10
    OnKeyDown = ProximoCampo
  end
  object eDataContabil: TEditLukaData
    Left = 562
    Top = 100
    Width = 80
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 9
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object eValorRetencao: TEditLuka
    Left = 242
    Top = 100
    Width = 111
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
    Text = '0,00'
    OnChange = eValorRetencaoChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object ePercentualRetencao: TEditLuka
    Left = 359
    Top = 100
    Width = 111
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    Text = '0,00'
    OnChange = ePercentualRetencaoChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
end
