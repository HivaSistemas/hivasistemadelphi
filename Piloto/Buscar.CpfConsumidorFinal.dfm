inherited FormBuscarCpfConsumidorFinal: TFormBuscarCpfConsumidorFinal
  Caption = 'Consumidor Final'
  ClientHeight = 88
  ClientWidth = 138
  ExplicitWidth = 144
  ExplicitHeight = 117
  PixelsPerInch = 96
  TextHeight = 14
  object lblcpfconsumidorfinal: TLabel [0]
    Left = 3
    Top = 8
    Width = 113
    Height = 14
    Caption = 'CPF Consumidor final'
  end
  inherited pnOpcoes: TPanel
    Left = 3
    Top = 52
    Width = 135
    Height = 36
    Align = alNone
    ExplicitLeft = 3
    ExplicitTop = 52
    ExplicitWidth = 135
    ExplicitHeight = 36
    inherited sbFinalizar: TSpeedButton
      Left = 0
      Width = 131
      Height = 32
      Align = alClient
      ExplicitLeft = -2
      ExplicitTop = -2
      ExplicitWidth = 131
    end
  end
  object edtCPFConsumidorFinal: TEditCPF_CNPJ_Luka
    Left = 3
    Top = 24
    Width = 135
    Height = 22
    EditMask = '999.999.999-99'
    MaxLength = 14
    TabOrder = 1
    Text = '   .   .   -  '
    Tipo = [tccCPF]
  end
end
