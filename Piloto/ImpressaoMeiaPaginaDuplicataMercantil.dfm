�
 TFORMIMPRESSAOMEIAPAGINADUPLICATAMERCANTIL 0z�  TPF0�*TFormImpressaoMeiaPaginaDuplicataMercantil)FormImpressaoMeiaPaginaDuplicataMercantilCaption)FormImpressaoMeiaPaginaDuplicataMercantilClientWidth?ExplicitTop�ExplicitWidthOExplicitHeight�PixelsPerInch`
TextHeight � 	TQuickRepqrRelatorioA4LeftHeightcFunctions.DATA00'' Page.PaperSizeA4Page.Values       �@      ��
@       �@      @�
@       �@       �@           ExplicitLeftExplicitHeightc �TQRBandqrCabecalhoHeight� Size.Values��������@������v�	@ BandTyperbTitleExplicitHeight�  �TQRLabelqrCaptionEnderecoSize.Values�QUUUU��@      @�@k������@      ��@ FontSize  �TQRLabel	qrEmpresaSize.Values�QUUUU��@ZUUUU%�@       �@�����6�@ FontSize  �TQRLabel
qrEnderecoSize.Values�QUUUU��@ZUUUU%�@k������@aUUUU-�	@ FontSize  �TQRImageqrLogoEmpresaSize.Values�������@��������@TOUUUUU� @�������@   �TQRLabelqrCaptionEmpresaSize.Values�QUUUU��@      @�@       �@      ��@ FontSize  �TQRLabelqrEmitidoEmSize.Values�\UUUU��@aUUUU)�	@          ZUUUU5�@ FontSize  �TQRLabelqr1Size.Values�QUUUU��@      @�@)�����
�@      ��@ FontSize  �TQRLabelqrBairroSize.Values�QUUUU��@ZUUUU%�@)�����
�@�����6�@ FontSize  �TQRLabelqr3Size.Values�QUUUU��@�����n�@)�����
�@      ��@ FontSize  �TQRLabel
qrCidadeUfSize.Values�QUUUU��@𞪪��8�	@)�����
�@�����6�@ FontSize  �TQRLabelqr5Size.Values�QUUUU��@�����n�@       �@      ��@ FontSize  �TQRLabelqrCNPJSize.Values�QUUUU��@𞪪��8�	@       �@�KUUUU��@ FontSize  �TQRLabelqr7Size.Values�QUUUU��@      @�@       �@      ��@ FontSize  �TQRLabel
qrTelefoneSize.Values�QUUUU��@ZUUUU%�@       �@�����ڻ@ FontSize  �TQRLabelqrFaxSize.Values�QUUUU��@�����ҿ@       �@�����ڻ@ FontSize  �TQRLabelqr10Size.Values�QUUUU��@      Ж@       �@      ��@ FontSize  �TQRLabelqr11Size.Values�QUUUU��@�����n�@       �@      ��@ FontSize  �TQRLabelqrEmailLeftwSize.ValuesUUUUUU��@      �@�������@      |�@ FontSizeExplicitLeftw  �TQRLabelqr13Left� Width9Size.Valuesk�������@ZUUUU��@�����
�@�KUUUU	�@ CaptionDUPLICATA  DE VENDA MERCANTILFontSizeExplicitLeft� ExplicitWidth9  TQRLabelqr2LeftTopWWidth,HeightSize.Values�\UUUU��@��������@      0�@hUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionCliente:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrNomeClienteLeft3TopWWidth� HeightSize.Values�\UUUU��@      ��@      0�@ZUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption 2.500 - LUCAS RODRIGUES DE SOUZAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqr6Left� TopWWidth8HeightSize.Values�\UUUU��@������@      0�@�����*�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption	CPF/CNPJ:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrCpfCnpjClienteLeft)TopWWidthXHeightSize.Values�\UUUU��@      t�@      0�@hUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption020.550.654-45ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrCaptionVendedorLeft�TopWWidth8HeightEnabledSize.Values�\UUUU��@𞪪��P�	@      0�@�����*�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption	Vendedor:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
qrVendedorLeft�TopWWidth� HeightEnabledSize.Values�\UUUU��@aUUUU�	@      0�@�����J�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption"25 - MARCOS PAULO APOLINARIO SILVAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqr14LeftTopfWidth5HeightSize.Values�\UUUU��@��������@      ��@�����:�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption
   Endereço:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrEnderecoClienteLeft=TopfWidthLHeightSize.Values�\UUUU��@�KUUUUe�@      ��@      x�	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionS   RUA 12 QD 27A APTO 500 BLOCO PIAUI, SETOR NOVA SUIÇA, GOIANIA - GO; CEP: 74550-550ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqr30LeftoTopvWidth9HeightSize.Values      ��@�4��8�'�	@J��8���@�8��8�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption
Qtde.parc.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqr29Left@TopvWidth)HeightSize.Values      ��@
��8��q�	@J��8���@���8��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionParcelaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqr21Left�TopvWidthTHeightSize.Values      ��@�4��8���	@J��8���@��8���@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption   Código HivaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrCabecalhoMovimentoLeft�TopvWidthIHeightSize.Values      ��@
��8��߇	@J��8���@x�q�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionPedidoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqr19Left6TopvWidthaHeightSize.Values      ��@�KUUUU�@J��8���@<��8���@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionData de vencimentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqr18Left� TopvWidthaHeightSize.Values      ��@��8��Z�@J��8���@<��8���@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   Data de emissãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqr17Left� TopvWidthLHeightSize.Values      ��@��q�q�@J��8���@��q�q��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionValor R$ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqr16LeftTopvWidthvHeightSize.Values      ��@��������@J��8���@J��8���@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	DocumentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrDocumentoLeftTop� WidthvHeightSize.Values��q�q�@��������@J��8�c�@J��8���@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	DocumentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrValorLeft� Top� WidthLHeightSize.Values��q�q�@��q�q�@J��8�c�@��q�q��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionValor R$ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrDataEmissaoLeft� Top� WidthaHeightSize.Values��q�q�@��8��Z�@J��8�c�@<��8���@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   Data de emissãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrDataVencimentoLeft6Top� WidthaHeightSize.Values��q�q�@�KUUUU�@J��8�c�@<��8���@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionData de vencimentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrPedidoLeft�Top� WidthIHeightSize.Values��q�q�@
��8��߇	@J��8�c�@x�q�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionPedidoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrCodigoAltisLeft�Top� WidthTHeightSize.Values��q�q�@�i�qȡ	@���8���@���8���@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaption   Código HivaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	qrParcelaLeft@Top� Width)HeightSize.Values��q�q�@
��8��q�	@J��8�c�@���8��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionPedidoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrQtdeParcelasLeftoTop� Width9HeightSize.Values��q�q�@�4��8�'�	@J��8�c�@�8��8�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandAutoSizeCaptionPedidoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   �TQRBandqrbndRodapeTop� HeightmSize.Values������n�@������v�	@ BandType	rbSummaryExplicitTop� ExplicitHeightm �TQRLabelqrUsuarioImpressaoLeftTop#Size.Values       �@RFUUUUU�@      |�@ZUUUUE�@ FontSizeExplicitLeftExplicitTop#  �TQRLabel	qrSistemaLeft3Top!Size.Values       �@UUUUUU3�	@UUUUUU)�@UUUUUU5�@ CaptionHiva 3.0FontSizeExplicitLeft3ExplicitTop!  TQRShapeQRShape2Left� Top� Width�HeightSize.Values�\UUUU��@��������@�RUUUU��@      ��	@ XLColumn XLNumFormat	nfGeneralShape
qrsHorLine
VertAdjust   TQRLabel
qrlClienteLeft� Top� Width'HeightSize.Values      ��@�RUUUUY�@�RUUUUy�@�RUUUU!�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeBiDiModebdLeftToRightParentBiDiModeCaptionClienteColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrlCpfCnpjClienteLeftTop� WidthHeightSize.Values      ��@�������@      ��@������j�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandCaptionCpf:ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRMemoqrInstrucaoLeftTop� Width�Height"Size.Valuesʨ�����@       �@      `�@aUUUU��	@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold Lines.Strings�   Reconheço a exatidão desta DUPLICATA DE VENDA MERCANTIL na importância acima que pagarei a :RAZAO_SOCIAL_EMPRESA, ou a sua ordem, na praça e vencimentos indicados. 
ParentFontTransparentFullJustifyMaxBreakChars FontSize  TQRLabelQRLabel1Left�Top[Width-HeightSize.Values      ��@      �����������@UUUUUUư
@ XLColumn XLNumFormat	nfGeneral	AlignmenttaRightJustifyAlignToBandCaption  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ColorclWhiteFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize    �	TQuickRepqrRelatorioNFLeftToplWidth�Height�Functions.DATA00'' Page.Values               ��	@          ������W�@                               ExplicitLeftExplicitToplExplicitWidth�ExplicitHeight� � TQRLabelQRLabel2LeftTop� Width-HeightSize.ValuesTUUUUU5�@������J�@������J�@      Ԕ@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionCliente:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelNameclientTMLeft/Top� Width� HeightSize.ValuesTUUUUU5�@UUUUUUq�@������J�@TUUUUUا@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionEzequiel Eugenio do PradoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelQRLabel3Left� Top� Width>HeightSize.ValuesTUUUUU5�@     \�@������J�@TUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	CPF/CNPJ:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelCpfCnpjclienteTMLeft;Top� Width� HeightSize.ValuesTUUUUU5�@     �9�@������J�@TUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption020.550.654-45ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelQRLabel4LeftTop� WidthHeightSize.ValuesTUUUUU5�@������J�@      ޏ@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionEnd.:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelqrEnderecoClienteTMLeftTop� Width�HeightSize.ValuesTUUUUU5�@TUUUUU�@      ޏ@     �߮@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionS   RUA 12 QD 27A APTO 500 BLOCO PIAUI, SETOR NOVA SUIÇA, GOIANIA - GO; CEP: 74550-550ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelQRLabel5LeftTop� WidthIHeightSize.Values��������@������J�@TUUUUUS�@������n�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	DocumentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelQRLabel6LeftjTop� Width9HeightSize.Values��������@UUUUUUI�@TUUUUUS�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionValor R$ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelQRLabel7LeftTop� WidthNHeightSize.Values��������@UUUUUU��@TUUUUUS�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaption
Data venc.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �	TQRLabelQRLabel8Left� Top� WidthSHeightSize.Values��������@      ޏ@TUUUUUS�@������@�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaption   Data emissãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �
TQRLabel	QRLabel11Left�Top� Width9HeightSize.Values��������@     �x�@TUUUUUS�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaption
Qtde.parc.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabel	QRLabel12LeftUTop� Width-HeightSize.Values��������@�����*��@TUUUUUS�@      Ԕ@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionParcelaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelqrDocumentoTMLeftTop� Width_HeightSize.Values��������@������J�@TUUUUU5�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	DocumentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabel	qrValorTMLeftjTop� WidthAHeightSize.Values��������@UUUUUUI�@TUUUUU5�@UUUUUU��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionValor R$ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelqrDataEmissaoTMLeft� Top� WidthSHeightSize.Values��������@      ޏ@TUUUUU5�@������@�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaption   Data emissãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelqrDataVencimentoTMLeftTop� WidthNHeightSize.Values��������@UUUUUU��@TUUUUU5�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaption
Data venc.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelqrParcelaTMLeftUTop� Width-HeightSize.Values��������@�����*��@TUUUUU5�@      Ԕ@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionParcelaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelqrQtdeParcelasTMLeft�Top� Width9HeightSize.Values��������@     �x�@TUUUUU5�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaption
Qtde.parc.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRMemoqrInstrucaoTMLeftTopWidth�HeightUSize.ValuesUUUUUU��@���������?�������@     �F�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold Lines.Strings�   Reconheço a exatidão desta DUPLICATA MERCANTIL na importância acima que pagarei a :RAZAO_SOCIAL_EMPRESA, ou a sua ordem, na praça e vencimentos indicados. 
ParentFontTransparentFullJustifyMaxBreakChars FontSize  �TQRShapeQRShape1Left#Top�Width�HeightSize.Values�������@��������@TUUUUU!�@��������@ XLColumn XLNumFormat	nfGeneralShape
qrsHorLine
VertAdjust   �TQRLabelqrlClienteTMLeft� Top�Width� HeightSize.ValuesTUUUUU5�@��������@TUUUU�J�@UUUUUU]�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaptionEzequiel Eugenio do PradoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRLabelqrlCpfCnpjClienteTMLeft� TopWidth� HeightSize.ValuesTUUUUU5�@������ņ@    ���@������d�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaption020.550.654-45ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  �TQRBandQRBand1Left�Top�Width�Height� AlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values������J�@������W�@ PreCaluculateBandHeightKeepOnOnePageBandTyperbTitle TQRLabelQRLabel9LeftTop#Width;HeightSize.ValuesTUUUUU��@��������@��������@TUUUUU!�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption
   Endereço:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrNomeEmpresaNaoFiscalLeft=TopWidthXHeightSize.Values      p�@��������@      ��@������6�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionHiva Sistemas LtdaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrendTMLeft?Top#Width�HeightSize.ValuesTUUUUU��@     \�@��������@      ;�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption%Alameda Botafogo qd36b Lote 15 Nr 251ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel21LeftTopWidth7HeightSize.Values      p�@��������@      ��@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption	Empresa: ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel23LeftTop2Width+HeightSize.ValuesTUUUUU��@��������@UUUUUU]�@������6�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionBairro: ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel
qrbairroTMLeft,Top2Width�HeightSize.ValuesTUUUUU��@UUUUUU��@UUUUUU]�@      ;�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionSetor Pedro LudovicoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel25LeftTopCWidth>HeightSize.Values      p�@��������@��������@TUUUUU�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionCidade/UF: ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrcidadeufTMLeftATopCWidthbHeightSize.Values      p�@UUUUUU��@��������@      Y�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption   Goiânia / GOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel27LeftTopWidth&HeightSize.Values      p�@��������@������Z�@������Z�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionCNPJ: ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrCpfCnpjTMLeft.TopWidth� HeightSize.Values      p�@������"�@������Z�@      �@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption00.000.000/0001-00ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel29LeftTopSWidth7HeightSize.Values      p�@��������@������@�@�������@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption
Telefone: ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabelqrfoneempresaTMLeft7TopSWidth\HeightSize.Values      p�@�������@������@�@������"�@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaption(62) 3000-0000ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel33Left� TopSWidth0HeightSize.Values      p�@      ޏ@������@�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionE-mail: ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	qremailTMLeft� TopSWidth� HeightSize.Values      p�@UUUUUUı@������@�@      ��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaLeftJustifyAlignToBandAutoSizeCaptionHivasolucoes@gmail.comColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize  TQRLabel	QRLabel35Left<TopuWidthUHeightSize.Values�������@      p�@      z�@�����*��@ XLColumn XLNumFormat	nfGeneral	AlignmenttaCenterAlignToBandAutoSizeCaption**DUPLICATA MERCANTIL**ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesVerticalAlignmenttlTopFontSize   �TQRShapeQRShape3Left�TopaWidth�HeightSize.Values      ��@�������� �TUUUUUg�@      ��@ XLColumn XLNumFormat	nfGeneralShape
qrsHorLine
VertAdjust   �TQRShapeQRShape4Left�Top� Width�HeightSize.Values      ��@�������� �UUUUUU��@      ��@ XLColumn XLNumFormat	nfGeneralShape
qrsHorLine
VertAdjust   �TQRBandqrBandTituloTop� Width�Size.Values��������@������W�@ ExplicitTop� ExplicitWidth� �TQRLabelqrlNFNomeEmpresaLeftcTopSize.Values      �@      ��@������Z�@      	�@ FontSizeExplicitLeftcExplicitTop  �TQRLabelqrNFCnpjSize.Valuesk�����J�@��������@�\UUUU�@      	�@ FontSize  �TQRLabelqrNFEnderecoSize.Values      �@��������@k�������@      	�@ FontSize  �TQRLabelqrNFCidadeUfSize.Valuesk�������@��������@hUUUU��@      	�@ FontSize  �TQRLabelqrNFTipoDocumentoSize.Valuesk�����J�@���������?�������@     �ճ@ FontSize  �TQRShapeqrSeparador2Size.Values�������� @�������� @ZUUUU��@      �@   �TQRLabel
qrNFTituloSize.Valuesk�����J�@���������?�����d�@     �ճ@ FontSize  �TQRLabelqrlNFEmitidoEmSize.Values�\UUUU5�@ZUUUU5�@�������� @      ��@ FontSize�  �TQRLabelqrlNFTelefoneEmpresaSize.Valuesk�������@��������@������@      	�@ FontSize   �TQRBandPageFooterBand1TopOWidth�Height Size.Values          ������W�@ ExplicitTopOExplicitWidth�ExplicitHeight  �TQRLabelqrlNFSistemaLeftpSize.ValuesTUUUUU5�@������"�@                �@ FontSize�ExplicitLeftp    TQRCompositeReportQRCompositeReport1Options PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.MemoryLimit@B PrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrinterSettings.Orientation
poPortraitPrinterSettings.PaperSizeLetter	PageCount Left�Top   