unit DefinirMultiploCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _RecordsCadastros,
  Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca;

type
  TFormDefinirMultiploCompra = class(TFormHerancaFinalizar)
    sgUnidades: TGridLuka;
    procedure sgUnidadesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgUnidadesDblClick(Sender: TObject);
    procedure sgUnidadesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  end;

function Buscar( pMultiplos: TArray<RecProdutoMultiploCompra> ): TRetornoTelaFinalizar<RecProdutoMultiploCompra>;

implementation

{$R *.dfm}

const
  coUnidadeId = 0;
  coMultiplo  = 1;
  coQtdeEmbalagem = 2;

function Buscar( pMultiplos: TArray<RecProdutoMultiploCompra> ): TRetornoTelaFinalizar<RecProdutoMultiploCompra>;
var
  i: Integer;
  vForm: TFormDefinirMultiploCompra;
begin
  vForm := TFormDefinirMultiploCompra.Create(nil);

  for i := Low(pMultiplos) to High(pMultiplos) do begin
    vForm.sgUnidades.Cells[coUnidadeId, i + 1] := pMultiplos[i].UnidadeId;
    vForm.sgUnidades.Cells[coMultiplo, i + 1]  := NFormat( pMultiplos[i].Multiplo, 4 );
    vForm.sgUnidades.Cells[coQtdeEmbalagem, i + 1] := NFormat( pMultiplos[i].QuantidadeEmbalagem, 4 );
  end;
  vForm.sgUnidades.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(pMultiplos) );

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.UnidadeId := vForm.sgUnidades.Cells[coUnidadeId, vForm.sgUnidades.Row];
    Result.Dados.Multiplo  := SFormatDouble(vForm.sgUnidades.Cells[coMultiplo, vForm.sgUnidades.Row]);
    Result.Dados.QuantidadeEmbalagem := SFormatDouble(vForm.sgUnidades.Cells[coQtdeEmbalagem, vForm.sgUnidades.Row]);
  end;
end;

procedure TFormDefinirMultiploCompra.sgUnidadesDblClick(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk;
end;

procedure TFormDefinirMultiploCompra.sgUnidadesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgUnidades.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], IIf(ACol = coUnidadeId, taLeftJustify, taRightJustify), Rect);
end;

procedure TFormDefinirMultiploCompra.sgUnidadesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sgUnidadesDblClick(nil);
end;

end.
