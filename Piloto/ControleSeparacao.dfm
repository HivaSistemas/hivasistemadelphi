inherited FormControleSeparacoes: TFormControleSeparacoes
  Caption = 'Controle de separa'#231#245'es'
  ClientHeight = 471
  ClientWidth = 894
  OnShow = FormShow
  ExplicitWidth = 900
  ExplicitHeight = 500
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 471
    ExplicitHeight = 471
  end
  object pc1: TPageControl
    Left = 122
    Top = 0
    Width = 772
    Height = 471
    ActivePage = tsFiltros
    Align = alClient
    TabOrder = 1
    object tsFiltros: TTabSheet
      Caption = 'Filtros'
      ImageIndex = 3
      inline FrClientes: TFrClientes
        Left = 1
        Top = 130
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 130
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 45
            Height = 15
            ExplicitWidth = 45
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 0
        Top = 215
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitTop = 215
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 48
            Height = 15
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
        inherited ckSomenteAtivos: TCheckBox
          Width = 98
          ExplicitWidth = 98
        end
      end
      inline FrOrcamentos: TFrNumeros
        Left = 418
        Top = 1
        Width = 134
        Height = 72
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 418
        ExplicitTop = 1
        inherited sgNumeros: TGridLuka
          ExplicitTop = 14
          ExplicitHeight = 58
        end
        inherited pnDescricao: TPanel
          Caption = ' C'#243'digo do pedido'
        end
      end
      inline FrEmpresa: TFrEmpresas
        Left = 1
        Top = 0
        Width = 403
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 1
        ExplicitWidth = 403
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 378
          Height = 24
          ExplicitWidth = 378
          ExplicitHeight = 24
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 47
            Height = 15
            Caption = 'Empresa'
            ExplicitWidth = 47
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          Height = 25
          ExplicitLeft = 378
          ExplicitHeight = 25
        end
      end
      inline FrLocaisPendencia: TFrLocais
        Left = 1
        Top = 45
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 45
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 112
            Height = 15
            Caption = 'Locais da pend'#234'ncia'
            ExplicitWidth = 112
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
    end
    object tsPendencias: TTabSheet
      Caption = 'Pend'#234'ncias de retiradas/entregas'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 664
      ExplicitHeight = 392
      object splSeparador: TSplitter
        Left = 0
        Top = 176
        Width = 764
        Height = 6
        Cursor = crVSplit
        Align = alTop
        MinSize = 6
        ResizeStyle = rsUpdate
        ExplicitWidth = 664
      end
      object st1: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 764
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Pend'#234'ncias de entregas'
        Color = 10066176
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
        TipoCampo = tcTexto
        CasasDecimais = 0
        ExplicitTop = 16
        ExplicitWidth = 664
      end
      object sgOrcamentos: TGridLuka
        Left = 0
        Top = 17
        Width = 764
        Height = 159
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 1
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Leg.'
          'Tipo'
          'Pedido'
          'Cliente'
          'Local'
          'Data cadastro'
          'Vendedor'
          'Data/hora prev. entrega')
        Grid3D = False
        RealColCount = 10
        Indicador = True
        AtivarPopUpSelecao = False
        ExplicitWidth = 884
        ColWidths = (
          28
          87
          49
          143
          112
          90
          140
          137)
      end
      object st2: TStaticTextLuka
        Left = 0
        Top = 182
        Width = 764
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Itens da pend'#234'ncia selecionada'
        Color = 10066176
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        TipoCampo = tcTexto
        CasasDecimais = 0
        ExplicitTop = 375
        ExplicitWidth = 664
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 199
        Width = 764
        Height = 243
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 6
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 3
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Saldo'
          'Und.'
          'Lote')
        Grid3D = False
        RealColCount = 15
        Indicador = True
        AtivarPopUpSelecao = False
        ExplicitWidth = 664
        ExplicitHeight = 193
        ColWidths = (
          56
          216
          171
          65
          36
          98)
      end
    end
    object tsEmSeparacao: TTabSheet
      Caption = 'Em separa'#231#227'o'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 794
      ExplicitHeight = 459
    end
    object tsSeparados: TTabSheet
      Caption = 'Separados'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 794
      ExplicitHeight = 459
    end
  end
end
