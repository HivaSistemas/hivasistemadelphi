unit GruposTributacoesEstadualVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, _GruposTribEstadualVenda,
  _FrameHerancaPrincipal, FrameICMS, Vcl.StdCtrls, CheckBoxLuka, EditLuka, _GruposTribEstadualVendaEst,
  Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _Biblioteca, _RecordsEspeciais, PesquisaGruposTributacoesEstadualVenda;

type
  TFormGruposTributacoesEstadualVenda = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eDescricao: TEditLuka;
    FrICMS: TFrICMS;
    SpeedButton4: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
  private
    procedure PreencherRegistro(pGrupo: RecGruposTribEstadualVenda);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormGruposTributacoesEstadualVenda }

procedure TFormGruposTributacoesEstadualVenda.BuscarRegistro;
var
  vGrupo: TArray<RecGruposTribEstadualVenda>;
begin
  vGrupo := _GruposTribEstadualVenda.BuscarGruposTribEstadualVenda(Sessao.getConexaoBanco, 0, [eID.AsInt]);
  if vGrupo = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(eID);
    Abort;
  end;

  inherited;
  PreencherRegistro(vGrupo[0]);
end;

procedure TFormGruposTributacoesEstadualVenda.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _GruposTribEstadualVenda.ExcluirGruposTribEstadualVenda(Sessao.getConexaoBanco, eID.AsInt);
  Sessao.AbortarSeHouveErro(vRetBanco);

  inherited;
end;

procedure TFormGruposTributacoesEstadualVenda.FormCreate(Sender: TObject);
begin
  inherited;
  FrICMS.sgICMS.MostrarColunas([9]);
end;

procedure TFormGruposTributacoesEstadualVenda.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vRetBanco :=
    _GruposTribEstadualVenda.AtualizarGruposTribEstadualVenda(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eDescricao.Text,
      ckAtivo.CheckedStr,
      FrICMS.ICMS
    );

  Sessao.AbortarSeHouveErro(vRetBanco);

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormGruposTributacoesEstadualVenda.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eDescricao,
    FrICMS],
    pEditando
  );

  if pEditando then
    SetarFoco(eDescricao);
end;

procedure TFormGruposTributacoesEstadualVenda.PesquisarRegistro;
var
  vGrupo: RecGruposTribEstadualVenda;
begin
  vGrupo := RecGruposTribEstadualVenda(PesquisaGruposTributacoesEstadualVenda.Pesquisar);
  if vGrupo = nil then
    Exit;

  inherited;
  PreencherRegistro(vGrupo);
end;

procedure TFormGruposTributacoesEstadualVenda.PreencherRegistro(pGrupo: RecGruposTribEstadualVenda);
begin
  eID.AsInt                   := pGrupo.GrupoTribEstadualVendaId;
  eDescricao.Text             := pGrupo.Descricao;
  ckAtivo.CheckedStr          := pGrupo.Ativo;
  FrICMS.ICMS                 := _GruposTribEstadualVendaEst.BuscarGruposTribEstadualVendaEst(Sessao.getConexaoBanco, 0, [pGrupo.GrupoTribEstadualVendaId]);
end;

procedure TFormGruposTributacoesEstadualVenda.SpeedButton4Click(
  Sender: TObject);
begin
  inherited;
  Informar('Informa��o de como tem que ser cadastrado a tributa��o e como funciona a tributa��o interestadual');
end;

procedure TFormGruposTributacoesEstadualVenda.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eDescricao.Trim = '' then begin
    _Biblioteca.Exclamar('A descri��o do grupo de tributa��o n�o foi informada corretamente, verifique!');
    SetarFoco(eDescricao);
    Abort;
  end;

  FrICMS.VerificarRegistro;
end;

end.
