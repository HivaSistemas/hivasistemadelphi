unit FrameDataInicialFinal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls,
  Vcl.Mask, EditLukaData, _Biblioteca;

type
  TFrDataInicialFinal = class(TFrameHerancaPrincipal)
    eDataFinal: TEditLukaData;
    eDataInicial: TEditLukaData;
    Label1: TLabel;
    lb1: TLabel;
    procedure eDataFinalKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;

    function GetFiltrosUtilizados: TArray<string>;

    function DatasValidas: Boolean;
    function NenhumaDataValida: Boolean;
    function getSqlFiltros(pColuna: string): string; overload;
    function getSqlFiltros(pColuna: string; var pSqlAtual: string; pWhereOuAnd: Boolean = True): string; overload;
    function getSQLDataInicialMenor(pColuna: string): string;
    function getDataInicial: TDateTime;
    function getDataFinal: TDateTime;
    procedure SetFocus; override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure Clear; override;
    function EstaVazio: Boolean; override;
  end;

implementation

{$R *.dfm}

{ TFrDataInicialFinal }

procedure TFrDataInicialFinal.Clear;
begin
  inherited;
  _Biblioteca.LimparCampos([eDataInicial, eDataFinal]);
end;

constructor TFrDataInicialFinal.Create(AOwner: TComponent);
begin
  inherited;
  eDataFinal.OnKeyDown := eDataFinalKeyDown;
end;

function TFrDataInicialFinal.DatasValidas: Boolean;
begin
  Result := eDataInicial.DataOk and eDataFinal.DataOk;
end;

procedure TFrDataInicialFinal.eDataFinalKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
 // inherited;

  // Simulando um tab para sair do frame
  if Key = VK_RETURN then
    keybd_event(VK_TAB, 0, 0, 0);
end;

function TFrDataInicialFinal.EstaVazio: Boolean;
begin
  Result := NenhumaDataValida;
end;

function TFrDataInicialFinal.getDataFinal: TDateTime;
begin
  Result := eDataFinal.AsData;
end;

function TFrDataInicialFinal.getDataInicial: TDateTime;
begin
  Result := eDataInicial.AsData;
end;

function TFrDataInicialFinal.GetFiltrosUtilizados: TArray<string>;
begin
  Result := nil;

  if NenhumaDataValida then
    Exit;

  SetLength(Result, 2);

  Result[0] := Label1.Caption;
  Result[1] := '    ';
  if eDataInicial.DataOk and eDataFinal.DataOk then
    Result[1] := Result[1] + eDataInicial.Text + ' � ' + eDataFinal.Text
  else if eDataInicial.DataOk then
    Result[1] := Result[1] + eDataInicial.Text
  else if eDataFinal.DataOk then
    Result[1] := Result[1] + eDataFinal.Text;
end;

function TFrDataInicialFinal.getSqlFiltros(pColuna: string; var pSqlAtual: string; pWhereOuAnd: Boolean = True): string;
begin
  Result := getSqlFiltros(pColuna);

  if (Result <> '') and pWhereOuAnd then
    _Biblioteca.WhereOuAnd(pSqlAtual, Result);
end;

function TFrDataInicialFinal.getSqlFiltros(pColuna: string): string;
begin
  Result := '';

  if EstaVazio then
    Exit;

  if (eDataInicial.DataOk and eDataFinal.DataOk) and (eDataInicial.AsData > eDataFinal.AsData) then begin
    _Biblioteca.Exclamar('A data inicial para o campo "' + Label1.Caption + '" n�o pode ser maior que a data final, por favor verifique!');
    SetarFoco(eDataInicial);
    Abort;
  end;

  if eDataInicial.DataOk and eDataFinal.DataOk then begin
    if eDataInicial.AsData = eDataFinal.AsData then
      Result := ' trunc(' + pColuna + ') = ' + ToDateOracle(eDataInicial.AsData)
    else
      Result := ' trunc(' + pColuna + ') between ' + ToDateOracle(eDataInicial.AsData) + ' and ' + ToDateOracle(eDataFinal.AsData)
  end
  else if eDataInicial.DataOk then
    Result := ' trunc(' + pColuna + ') >= ' + ToDateOracle(eDataInicial.AsData)
  else if eDataFinal.DataOk then
    Result := ' trunc(' + pColuna + ') <= ' + ToDateOracle(eDataFinal.AsData);
end;

function TFrDataInicialFinal.getSQLDataInicialMenor(pColuna: string): string;
begin
  Result := '';
  if eDataInicial.DataOk then
    Result := ' trunc(' + pColuna + ') < ' + ToDateOracle(eDataInicial.AsData);
end;

procedure TFrDataInicialFinal.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eDataInicial, eDataFinal], pEditando, pLimpar);
end;

function TFrDataInicialFinal.NenhumaDataValida: Boolean;
begin
  Result := (not eDataInicial.DataOk) and (not eDataFinal.DataOk);
end;

procedure TFrDataInicialFinal.SetFocus;
begin
  inherited;
  SetarFoco(eDataInicial);
end;

end.
