unit CadastroFuncionarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsCadastros, _Sessao,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, FrameImagem, _RecordsEspeciais,
  Vcl.Mask, EditLukaData, _FrameHenrancaPesquisas, FrameCagosFuncionarios, _Biblioteca,
  Vcl.ComCtrls, EditCpfCnpjLuka, FrameBairros, FrameEndereco, _Funcionarios, PesquisaFuncionarios,
  Vcl.Menus, Frame.Cadastros, GroupBoxLuka, CheckBoxLuka, Pesquisa.Cadastros,
  FrameEmpresas, Vcl.Grids, GridLuka, _ParametrosEmpresa,
  FrameEmpresasFaturamento;

type
  TFormCadastroFuncionarios = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
    eDataNascimento: TEditLukaData;
    lb2: TLabel;
    lb3: TLabel;
    eApelido: TEditLuka;
    pcDados: TPageControl;
    tsPrincipais: TTabSheet;
    f: TTabSheet;
    eCPF: TEditCPF_CNPJ_Luka;
    lb4: TLabel;
    lb5: TLabel;
    eRG: TEditLuka;
    lb6: TLabel;
    eOrgaoExpedidor: TEditLuka;
    lb9: TLabel;
    eEmail: TEditLuka;
    eServidorEmail: TEditLuka;
    eNomeUsuarioEmail: TEditLuka;
    eSenhaEmail: TEditLuka;
    ePortaServidorEmail: TEditLuka;
    ePercentualComissaoVista: TEditLuka;
    lb13: TLabel;
    lb14: TLabel;
    ePercentualComissaoPrazo: TEditLuka;
    lb15: TLabel;
    ePercentualDescontoAdicVenda: TEditLuka;
    lb16: TLabel;
    ePercentualDescontoAdicFinanceiro: TEditLuka;
    sbResetarSenha: TSpeedButton;
    eDataCadastro: TEditLukaData;
    lb17: TLabel;
    FrEndereco: TFrEndereco;
    FrFoto: TFrImagem;
    FrCadastro: TFrameCadastros;
    FrCargoFuncionario: TFrCargosFuncionarios;
    gbFuncoesUsuario: TGroupBoxLuka;
    ckVendedor: TCheckBoxLuka;
    ckCaixa: TCheckBoxLuka;
    ckComprador: TCheckBoxLuka;
    tsDadosTrabalhistas: TTabSheet;
    lb18: TLabel;
    eSalario: TEditLuka;
    eNumeroCarteira: TEditLuka;
    lb19: TLabel;
    eSerieCarteira: TEditLuka;
    lb20: TLabel;
    ePisPasep: TEditLuka;
    lb21: TLabel;
    eDataAdmissao: TEditLukaData;
    lb7: TLabel;
    eDataDesligamento: TEditLukaData;
    lb22: TLabel;
    ckMotorista: TCheckBoxLuka;
    ePercDescItemPrecoManual: TEditLuka;
    lb23: TLabel;
    ckAjudante: TCheckBoxLuka;
    ckAcessaAltisW: TCheckBoxLuka;
    pcEmpresas: TTabSheet;
    FrEmpresas: TFrEmpresas;
    ckFinalizarSessao: TCheckBoxLuka;
    ckDevolucaoSomenteFiscal: TCheckBoxLuka;
    TabSheet1: TTabSheet;
    sgFaixasAcrescimo: TGridLuka;
    sbSalvarMeta: TSpeedButton;
    sbCancelarMeta: TSpeedButton;
    frEmpresa: TFrEmpresasFaturamento;
    ckAcessoHivaMobile: TCheckBox;
    procedure ePortaServidorEmailKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbResetarSenhaClick(Sender: TObject);
    procedure eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); reintroduce;
    procedure FormCreate(Sender: TObject);
    procedure sgFaixasAcrescimoDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgFaixasAcrescimoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sgFaixasAcrescimoSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure sbSalvarMetaClick(Sender: TObject);
    procedure sbCancelarMetaClick(Sender: TObject);
  private
    procedure PreencherRegistro(pFuncionario: RecFuncionarios);
    procedure EmpresaOnAposPesquisar(Sender: TObject);
    procedure EmpresaOnAposDeletar(Sender: TObject);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

const
  mtFaixa        = 0;
  mtValorInicial = 1;
  mtValorFinal   = 2;
  mtPercentual   = 3;
  mtColunaMaxima = 4;

{ TFormHerancaCadastro1 }

procedure TFormCadastroFuncionarios.BuscarRegistro;
var
  vFuncionarios: TArray<RecFuncionarios>;
begin
  vFuncionarios := _Funcionarios.BuscarFuncionarios(Sessao.getConexaoBanco, 0, [eId.AsInt], False, False, False, False, False);
  if vFuncionarios = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vFuncionarios[0]);
end;

procedure TFormCadastroFuncionarios.eIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  vCadastro: RecCadastros;
  vFuncionario: TArray<RecFuncionarios>;
begin
//  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eID.AsInt = 0 then begin
    vCadastro := RecCadastros(Pesquisa.Cadastros.Pesquisar(True));
    if vCadastro = nil then
      Exit;

    if vCadastro.cadastro_id = Sessao.getParametros.cadastro_consumidor_final_id then begin
      _Biblioteca.Exclamar('N�o � permitido o uso do cadastro de consumidor final!');
      Exit;
    end;

    if vCadastro.tipo_pessoa = 'J' then begin
      _Biblioteca.Exclamar('N�o � permitido o de pessoa jur�dica no cadastro de funcion�rios!');
      Exit;
    end;

    vFuncionario := _Funcionarios.BuscarFuncionarios(Sessao.getConexaoBanco, 4, [vCadastro.cpf_cnpj], False, False, False, False, False);
    if vFuncionario <> nil then begin
      eID.AsInt := vFuncionario[0].funcionario_id;
      BuscarRegistro;
      Exit;
    end;

    Modo(True);
    eNome.Text := vCadastro.razao_social;
    eApelido.Text := vCadastro.nome_fantasia;
    eCPF.Text := vCadastro.cpf_cnpj;
    eRG.Text := vCadastro.rg;
    eOrgaoExpedidor.Text := vCadastro.orgao_expedidor_rg;
    eDataNascimento.AsData := vCadastro.data_nascimento;
    FrEndereco.setLogradouro( vCadastro.logradouro );
    FrEndereco.setComplemento( vCadastro.complemento );
    FrEndereco.setNumero( vCadastro.numero );
    FrEndereco.setBairroId( vCadastro.bairro_id );
    FrEndereco.setPontoReferencia( vCadastro.ponto_referencia );
    FrEndereco.setCep( vCadastro.cep );
    FrCadastro.InserirDadoPorChave( vCadastro.cadastro_id, False );
    SetarFoco(FrCargoFuncionario);
    Exit;
  end
  else
    BuscarRegistro;
end;

procedure TFormCadastroFuncionarios.EmpresaOnAposDeletar(Sender: TObject);
begin
  sgFaixasAcrescimo.ClearGrid();
  sbSalvarMeta.Enabled := true;
  sbCancelarMeta.Enabled := true;
end;

procedure TFormCadastroFuncionarios.EmpresaOnAposPesquisar(Sender: TObject);
var
  vFaixas: TArray<RecParametrosComissao>;
  i: Integer;
  vLinha: Integer;
begin
  FrEmpresa.Modo(False, False);

  vFaixas := _ParametrosEmpresa.BuscarParametrosMetasFuncionario(
    Sessao.getConexaoBanco,
    FrEmpresa.getEmpresa().EmpresaId,
    eID.AsInt
  );

  vLinha := 0;
  if vFaixas = nil then begin
    sgFaixasAcrescimo.Cells[mtFaixa, sgFaixasAcrescimo.row]        := '1';
    sgFaixasAcrescimo.Cells[mtValorInicial, sgFaixasAcrescimo.row] := '0.01';
    sgFaixasAcrescimo.Col := mtValorFinal;
  end
  else begin
    for i := Low(vFaixas) to High(vFaixas) do begin
      Inc(vLinha);
      sgFaixasAcrescimo.Cells[mtFaixa, vLinha]        := IntToStr(vFaixas[i].Faixa);
      sgFaixasAcrescimo.Cells[mtValorInicial, vLinha] := _Biblioteca.NFormat(vFaixas[i].ValorInicial);
      sgFaixasAcrescimo.Cells[mtValorFinal, vLinha]   := _Biblioteca.NFormat(vFaixas[i].ValorFinal);
      sgFaixasAcrescimo.Cells[mtPercentual, vLinha]   := _Biblioteca.NFormat(vFaixas[i].Percentual);
    end;

    Inc(vLinha);
    sgFaixasAcrescimo.RowCount := vLinha + 1;
    sgFaixasAcrescimo.Row := sgFaixasAcrescimo.RowCount - 1;
    sgFaixasAcrescimo.Cells[mtFaixa, sgFaixasAcrescimo.Row] := IntToStr(sgFaixasAcrescimo.RowCount - 1);
    sgFaixasAcrescimo.Col := mtValorInicial;
  end;

  sbSalvarMeta.Enabled := true;
  sbCancelarMeta.Enabled := true;
  sgFaixasAcrescimo.Enabled := true;
end;

procedure TFormCadastroFuncionarios.ePortaServidorEmailKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    SetarFoco(ePercentualComissaoVista);
end;

procedure TFormCadastroFuncionarios.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Funcionarios.ExcluirFuncionario(Sessao.getConexaoBanco, eId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroFuncionarios.FormCreate(Sender: TObject);
begin
  inherited;
  ckDevolucaoSomenteFiscal.Visible := Sessao.getUsuarioLogado.funcionario_id = 1;
  frEmpresa.OnAposPesquisar := EmpresaOnAposPesquisar;
  frEmpresa.OnAposDeletar := EmpresaOnAposDeletar;
end;

procedure TFormCadastroFuncionarios.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vEmpresasId: TArray<Integer>;
begin
  inherited;

  vEmpresasId := FrEmpresas.GetArrayOfInteger;

  vRetBanco :=
    _Funcionarios.AtualizarFuncionario(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      eApelido.Text,
      eDataNascimento.AsData,
      FrCargoFuncionario.getDados().CargoId,
      eCPF.Text,
      eRG.Text,
      eOrgaoExpedidor.Text,
      eDataAdmissao.AsData,
      FrEndereco.getLogradouro,
      FrEndereco.getComplemento,
      FrEndereco.getNumero,
      FrEndereco.getBairroId,
      FrEndereco.getCep,
      eEmail.Text,
      eServidorEmail.Text,
      eNomeUsuarioEmail.Text,
      eServidorEmail.Text,
      ePortaServidorEmail.AsInt,
      FrCadastro.getCadastro().cadastro_id,
      FrFoto.getImagem,
      ePercentualDescontoAdicVenda.AsDouble,
      ePercentualDescontoAdicFinanceiro.AsDouble,
      ePercentualComissaoVista.AsDouble,
      ePercentualComissaoPrazo.AsDouble,
      ePercDescItemPrecoManual.AsDouble,
      eSalario.AsDouble,
      _Biblioteca.ToChar(ckAtivo),
      _Biblioteca.ToChar(ckVendedor),
      _Biblioteca.ToChar(ckComprador),
      _Biblioteca.ToChar(ckCaixa),
      _Biblioteca.ToChar(ckMotorista),
      _Biblioteca.ToChar(ckAjudante),
      eNumeroCarteira.Text,
      eSerieCarteira.Text,
      ePisPasep.Text,
      eDataDesligamento.AsData,
      ckAcessaAltisW.CheckedStr,
      vEmpresasId,
      ckFinalizarSessao.CheckedStr,
      ckDevolucaoSomenteFiscal.CheckedStr,
      ToChar(ckAcessoHivaMobile)
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroFuncionarios.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    eNome,
    eApelido,
    eCPF,
    eRG,
    eOrgaoExpedidor,
    eDataNascimento,
    eDataAdmissao,
    FrFoto,
    FrCargoFuncionario,
    pcDados,
    FrEndereco,
    ePercentualDescontoAdicVenda,
    ePercentualDescontoAdicFinanceiro,
    ePercDescItemPrecoManual,
    ckAtivo,
    FrCadastro,
    ckVendedor,
    ckCaixa,
    ckComprador,
    ckMotorista,
    ckAjudante,
    eNumeroCarteira,
    eSerieCarteira,
    ePisPasep,
    eDataDesligamento,
    eEmail,
    eServidorEmail,
    eNomeUsuarioEmail,
    eSenhaEmail,
    ePortaServidorEmail,
    FrEmpresas,
    ckAcessaAltisW,
    ckFinalizarSessao,
    ckAcessoHivaMobile,
    FrEmpresa,
    ckDevolucaoSomenteFiscal],
    pEditando
  );

  if pEditando then begin
    SetarFoco(eNome);
    ckAtivo.Checked := True;

    if eID.AsInt = 0 then begin
      FrEmpresa.Enabled := False;
      sgFaixasAcrescimo.Enabled := False;
      sbSalvarMeta.Enabled := False;
      sbCancelarMeta.Enabled := False;
    end;
  end;
end;

procedure TFormCadastroFuncionarios.PesquisarRegistro;
var
  vFuncionario: RecFuncionarios;
begin
  vFuncionario := RecFuncionarios(PesquisaFuncionarios.PesquisarFuncionario(False, False, False, False, 0, False, True, False));
  if vFuncionario = nil then
    Exit;

  inherited;
  PreencherRegistro(vFuncionario);
end;

procedure TFormCadastroFuncionarios.PreencherRegistro(pFuncionario: RecFuncionarios);
var
  i: Integer;
  vEmpresas: TArray<Integer>;
begin
  eID.AsInt      := pFuncionario.funcionario_id;
  eNome.Text     := pFuncionario.nome;
  eApelido.Text  := pFuncionario.apelido;
  eCPF.Text      := pFuncionario.cpf;
  eRG.Text       := pFuncionario.rg;
  eOrgaoExpedidor.Text := pFuncionario.orgao_expedidor_rg;
  FrCargoFuncionario.InserirDadoPorChave(pFuncionario.CargoId, False);
  eDataNascimento.AsData := pFuncionario.data_nascimento;
  eDataAdmissao.AsData := pFuncionario.data_admissao;
  FrFoto.setFoto(pFuncionario.foto);
  FrEndereco.setLogradouro(pFuncionario.logradouro);
  FrEndereco.setComplemento(pFuncionario.complemento);
  FrEndereco.setNumero(pFuncionario.numero);
  FrEndereco.setBairroId(pFuncionario.bairro_id);
  FrEndereco.setCep(pFuncionario.cep);
  FrCadastro.InserirDadoPorChave(pFuncionario.cadastro_id, False);
  eEmail.Text                                := pFuncionario.e_mail;
  eServidorEmail.Text                        := pFuncionario.servidor_smtp;
  eNomeUsuarioEmail.Text                     := pFuncionario.usuario_e_mail;
  eSenhaEmail.Text                           := pFuncionario.senha_e_mail;
  ePortaServidorEmail.AsInt                  := pFuncionario.porta_e_mail;
  ePercentualComissaoVista.AsDouble          := pFuncionario.percentual_comissao_a_vista;
  ePercentualComissaoPrazo.AsDouble          := pFuncionario.percentual_comissao_a_prazo;
  ePercentualDescontoAdicVenda.AsDouble      := pFuncionario.PercentualDescontoAdicVenda;
  ePercentualDescontoAdicFinanceiro.AsDouble := pFuncionario.PercentualDescontoAdicFinan;
  ePercDescItemPrecoManual.AsDouble          := pFuncionario.PercDescItemPrecoManual;
  ckVendedor.CheckedStr                      := pFuncionario.Vendedor;
  ckComprador.CheckedStr                     := pFuncionario.Comprador;
  ckMotorista.CheckedStr                     := pFuncionario.Motorista;
  ckCaixa.CheckedStr                         := pFuncionario.Caixa;
  ckAjudante.CheckedStr                      := pFuncionario.Ajudante;
  eNumeroCarteira.Text                       := pFuncionario.NumeroCarteiraTrabalho;
  eSerieCarteira.Text                        := pFuncionario.SerieCarteiraTrabalho;
  ePisPasep.Text                             := pFuncionario.PisPasep;
  eDataDesligamento.AsData                   := pFuncionario.DataDesligamento;
  ckAcessaAltisW.CheckedStr                  := pFuncionario.AcessaAltisW;
  ckFinalizarSessao.CheckedStr               := pFuncionario.FinalizarSessao;
  ckDevolucaoSomenteFiscal.CheckedStr        := pFuncionario.DevolucaoSomenteFiscal;
  ckAcessoHivaMobile.Checked                 := pFuncionario.AcessoHivaMobile = 'S';

  eDataCadastro.AsData := pFuncionario.data_cadastro;
  ckAtivo.Checked := (pFuncionario.ativo = 'S');

  vEmpresas := _Funcionarios.BuscarEmpresasFuncionario(Sessao.getConexaoBanco, pFuncionario.funcionario_id);
  if vEmpresas <> nil then begin
    for i := Low(vEmpresas) to High(vEmpresas) do begin
      FrEmpresas.InserirDadoPorChave(vEmpresas[i]);
    end;
  end;

  pFuncionario.Free;
end;

procedure TFormCadastroFuncionarios.sbCancelarMetaClick(Sender: TObject);
begin
  inherited;
  FrEmpresa.Clear;
  sgFaixasAcrescimo.ClearGrid();
  sbSalvarMeta.Enabled := false;
  sbCancelarMeta.Enabled := false;
end;

procedure TFormCadastroFuncionarios.sbResetarSenhaClick(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vRetBanco := _Funcionarios.AtualizarSenhaFuncionario(Sessao.getConexaoBanco, eID.AsInt, Sessao.getCriptografia.getCriptografiaSenhaPadrao, False);
  if vRetBanco.TeveErro then
    Exclamar(vRetBanco.MensagemErro)
  else
    _Biblioteca.Informar('A senha do funcion�rios foi redefinida para 123456.');
end;

procedure TFormCadastroFuncionarios.sbSalvarMetaClick(Sender: TObject);
var
  i, vLinhaAtual, vLinhaAnterior: Integer;
  vRetorno: RecRetornoBD;
  vFaixas: TArray<RecParametrosComissao>;
begin
  inherited;
  for i := sgFaixasAcrescimo.FixedRows to sgFaixasAcrescimo.RowCount - 1 do begin
    if (sgFaixasAcrescimo.Cells[mtValorFinal, i] = '') and (sgFaixasAcrescimo.Cells[mtPercentual, i] = '')   then
      Continue;

    vLinhaAtual := i;
    vLinhaAnterior := i - 1;

    if SFormatDouble(sgFaixasAcrescimo.Cells[mtValorInicial, vLinhaAtual]) = 0 then begin
      Exclamar('O valor inicial da faixa ' + sgFaixasAcrescimo.Cells[mtFaixa, i] + ' � inv�lido');
      sgFaixasAcrescimo.Col := mtValorInicial;
      sgFaixasAcrescimo.Row := vLinhaAtual;
      Abort;
    end;

    if SFormatDouble(sgFaixasAcrescimo.Cells[mtValorInicial, vLinhaAtual]) >= SFormatDouble(sgFaixasAcrescimo.Cells[mtValorFinal, vLinhaAtual]) then begin
      Exclamar('O valor inicial da faixa ' + sgFaixasAcrescimo.Cells[mtFaixa, i] + ' � inv�lido');
      sgFaixasAcrescimo.Col := mtValorInicial;
      sgFaixasAcrescimo.Row := vLinhaAtual;
      Abort;
    end;

    if SFormatDouble(sgFaixasAcrescimo.Cells[mtValorFinal, vLinhaAtual]) = 0 then begin
      Exclamar('O valor final da faixa ' + sgFaixasAcrescimo.Cells[mtFaixa, i] + ' � inv�lido');
      sgFaixasAcrescimo.Col := mtValorFinal;
      sgFaixasAcrescimo.Row := vLinhaAtual;
      Abort;
    end;

    if SFormatDouble(sgFaixasAcrescimo.Cells[mtValorInicial, vLinhaAtual]) <= SFormatDouble(sgFaixasAcrescimo.Cells[mtValorFinal, vLinhaAnterior]) then begin
      if sgFaixasAcrescimo.Cells[mtFaixa, vLinhaAtual] <> '1' then begin
        Exclamar('O valor inicial da faixa ' + sgFaixasAcrescimo.Cells[mtFaixa, i] + ' � menor que o valor final da faixa anterior');
        sgFaixasAcrescimo.Col := mtValorInicial;
        sgFaixasAcrescimo.Row := vLinhaAtual;
        Abort;
      end;
    end;

    if SFormatDouble(sgFaixasAcrescimo.Cells[mtPercentual, vLinhaAtual]) = 0 then begin
      Exclamar('O percentual da faixa ' + sgFaixasAcrescimo.Cells[mtFaixa, i] + ' � inv�lido');
      sgFaixasAcrescimo.Col := mtValorInicial;
      sgFaixasAcrescimo.Row := vLinhaAtual;
      Abort;
    end;

    if SFormatDouble(sgFaixasAcrescimo.Cells[mtPercentual, vLinhaAtual]) <= SFormatDouble(sgFaixasAcrescimo.Cells[mtPercentual, vLinhaAnterior]) then begin
      if sgFaixasAcrescimo.Cells[mtFaixa, vLinhaAtual] <> '1' then begin
        Exclamar('O percentual da faixa ' + sgFaixasAcrescimo.Cells[mtFaixa, i] + ' � menor que o percentual da faixa anterior');
        sgFaixasAcrescimo.Col := mtValorInicial;
        sgFaixasAcrescimo.Row := vLinhaAtual;
        Abort;
      end;
    end;
  end;

  for i := sgFaixasAcrescimo.FixedRows to sgFaixasAcrescimo.RowCount - 1 do begin
    if sgFaixasAcrescimo.Cells[mtValorFinal, i] = '' then
      Continue;

    SetLength(vFaixas, Length(vFaixas) + 1);

    vFaixas[i - 1].Faixa          := SFormatInt(sgFaixasAcrescimo.Cells[mtFaixa, i]);
    vFaixas[i - 1].ValorInicial   := SFormatDouble(sgFaixasAcrescimo.Cells[mtValorInicial, i]);
    vFaixas[i - 1].ValorFinal     := SFormatDouble(sgFaixasAcrescimo.Cells[mtValorFinal, i]);
    vFaixas[i - 1].Percentual     := SFormatDouble(sgFaixasAcrescimo.Cells[mtPercentual, i]);
  end;

  vRetorno := _ParametrosEmpresa.AtualizarParametrosComissaoMetaFuncionario(
    Sessao.getConexaoBanco,
    FrEmpresa.getEmpresa().EmpresaId,
    vFaixas,
    eID.AsInt
  );

  if vRetorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Exit;
  end;

  _Biblioteca.Informar(coAlteracaoRegistroSucesso);

  FrEmpresa.Clear;
  sgFaixasAcrescimo.ClearGrid();
  sbSalvarMeta.Enabled := false;
  sbCancelarMeta.Enabled := false;
  frEmpresa.Enabled := true;
  _Biblioteca.SetarFoco(FrEmpresa);
end;

procedure TFormCadastroFuncionarios.sgFaixasAcrescimoDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited ;

  if ACol in[mtFaixa, mtValorInicial, mtValorFinal, mtPercentual] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgFaixasAcrescimo.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormCadastroFuncionarios.sgFaixasAcrescimoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  valorAtual: string;
  valorFinalAnterior: Double;
begin
  inherited;
  if Key = VK_RETURN then begin

    valorAtual := _Biblioteca.NFormat(SFormatDouble(sgFaixasAcrescimo.Cells[sgFaixasAcrescimo.Col, sgFaixasAcrescimo.Row]), 2);
    sgFaixasAcrescimo.Cells[sgFaixasAcrescimo.Col, sgFaixasAcrescimo.Row] := valorAtual;
    if valorAtual <> '0.00' then begin

      if sgFaixasAcrescimo.Col = mtValorInicial then begin
        sgFaixasAcrescimo.Col := mtValorFinal;
      end
      else if sgFaixasAcrescimo.Col = mtValorFinal then begin
        sgFaixasAcrescimo.Col := mtPercentual;
      end
      else if sgFaixasAcrescimo.Col = mtPercentual then begin

        // Validando se n�o estou na ultima linha
        if sgFaixasAcrescimo.Row = sgFaixasAcrescimo.RowCount - 1 then begin
          // Pegando valor final da linha atual para setar valor inicial da proxima linha
          valorFinalAnterior := _Biblioteca.SFormatDouble(sgFaixasAcrescimo.Cells[mtValorFinal, sgFaixasAcrescimo.Row]) + 0.01;

          // Acrescentando uma linha no grid e entrando nela
          sgFaixasAcrescimo.RowCount := sgFaixasAcrescimo.RowCount + 1;
          sgFaixasAcrescimo.Row := sgFaixasAcrescimo.RowCount - 1;

          // Adicionando valores na nova linha e setando posi��o
          sgFaixasAcrescimo.Cells[mtFaixa, sgFaixasAcrescimo.RowCount - 1] := IntToStr(sgFaixasAcrescimo.RowCount - 1);
          sgFaixasAcrescimo.Cells[mtValorInicial, sgFaixasAcrescimo.Row] := _Biblioteca.NFormat(valorFinalAnterior);
          sgFaixasAcrescimo.Col := mtValorFinal;
        end
        else begin
          valorFinalAnterior := _Biblioteca.SFormatDouble(sgFaixasAcrescimo.Cells[mtValorFinal, sgFaixasAcrescimo.Row]) + 0.01;
          sgFaixasAcrescimo.Cells[mtValorInicial, sgFaixasAcrescimo.Row] := _Biblioteca.NFormat(valorFinalAnterior);
          sgFaixasAcrescimo.Row := sgFaixasAcrescimo.Row + 1;
          sgFaixasAcrescimo.Col := mtValorFinal;
        end;
      end;
    end;
  end
  else if (Key = VK_DELETE) and (ssCtrl in Shift) then begin
    if sgFaixasAcrescimo.Row = sgFaixasAcrescimo.RowCount - 1 then begin
      if
        (sgFaixasAcrescimo.Row = 1) and
        ((sgFaixasAcrescimo.RowCount -1) = sgFaixasAcrescimo.Row)
      then begin
        sgFaixasAcrescimo.Col := mtValorFinal;
        sgFaixasAcrescimo.Options := sgFaixasAcrescimo.Options + [goEditing];
      end;

      sgFaixasAcrescimo.DeleteRow(sgFaixasAcrescimo.Row, mtColunaMaxima);
    end
    else
      Exclamar('S� � permitido remover a ultima linha da tabela.');

  end;
end;

procedure TFormCadastroFuncionarios.sgFaixasAcrescimoSelectCell(Sender: TObject;
  ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := ACol in[mtValorInicial, mtValorFinal, mtPercentual];

  if not CanSelect then
    Exit;

  if ACol = mtValorInicial then begin
    sgFaixasAcrescimo.OnKeyPress := NumerosVirgula;
    sgFaixasAcrescimo.Options := sgFaixasAcrescimo.Options + [goEditing];
  end
  else if ACol = mtValorInicial then begin
    sgFaixasAcrescimo.OnKeyPress := NumerosVirgula;
    sgFaixasAcrescimo.Options := sgFaixasAcrescimo.Options + [goEditing];
  end
  else if ACol = mtPercentual then begin
    sgFaixasAcrescimo.OnKeyPress := NumerosVirgula;
    sgFaixasAcrescimo.Options := sgFaixasAcrescimo.Options + [goEditing];
  end;
end;

procedure TFormCadastroFuncionarios.VerificarRegistro(Sender: TObject);
var
  vNaoEUsuarioAltis: Boolean;
begin
  inherited;
  vNaoEUsuarioAltis := eID.AsInt <> 1;

  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('O nome do funcion�rio n�o foi informado corretamente, verifique!');
    SetarFoco(eNome);
    Abort;
  end;

  if (not eCPF.getCPF_CNPJOk) and vNaoEUsuarioAltis then begin
    _Biblioteca.Exclamar('O CPF do funcion�rio n�o foi informado corretamente, verifique!');
    SetarFoco(eCPF);
    Abort;
  end;

  if FrCargoFuncionario.EstaVazio then begin
    _Biblioteca.Exclamar('O cargo do funcion�rio n�o foi informado corretamente, verifique!');
    SetarFoco(FrCargoFuncionario);
    Abort;
  end;

  FrEndereco.VerificarDados;

  if FrCadastro.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar o cadastro de cliente/fornecedor do funcion�rio!');
    SetarFoco(FrCadastro);
    Abort;
  end;

  if vNaoEUsuarioAltis and (eCPF.Text <> FrCadastro.getCadastro.cpf_cnpj) then begin
    _Biblioteca.Exclamar('O CPF informado para o funcion�rio � diferente do CPF do cadastro, verifique! ' + chr(13) + 'CPF Cadastro: ' + FrCadastro.getCadastro().cpf_cnpj);
    SetarFoco(eCPF);
    Abort;
  end;

  if vNaoEUsuarioAltis and (FrCadastro.getCadastro().cadastro_id = Sessao.getParametros.cadastro_consumidor_final_id) then begin
    _Biblioteca.Exclamar('O cadastro do consumidor final n�o pode ser vinculado ao cadastro de funcion�rio!');
    SetarFoco(FrCadastro);
    Abort;
  end;

  if FrEmpresas.EstaVazio then begin
    _Biblioteca.Exclamar('N�o foram informadas as empresas permitidas para o usu�rio!');
    SetarFoco(FrEmpresas);
    Abort;
  end;
end;

end.
