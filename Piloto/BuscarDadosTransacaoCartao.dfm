inherited FormBuscarDadosTransacaoCartao: TFormBuscarDadosTransacaoCartao
  Caption = 'Busca de dados de venda cart'#227'o'
  ClientHeight = 107
  ClientWidth = 367
  ExplicitWidth = 373
  ExplicitHeight = 136
  PixelsPerInch = 96
  TextHeight = 14
  object lbBusca: TLabel [0]
    Left = 105
    Top = 15
    Width = 121
    Height = 14
    Caption = 'C'#243'digo de autoriza'#231#227'o'
  end
  object lb1: TLabel [1]
    Left = 278
    Top = 15
    Width = 22
    Height = 14
    Caption = 'NSU'
  end
  object lb2: TLabel [2]
    Left = 4
    Top = 15
    Width = 81
    Height = 14
    Caption = '4 '#250'lt.dig.cart'#227'o'
  end
  inherited pnOpcoes: TPanel
    Top = 70
    Width = 367
    TabOrder = 3
    ExplicitTop = 70
    ExplicitWidth = 367
  end
  object eCodigoAutorizacao: TEditLuka
    Left = 105
    Top = 29
    Width = 169
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 50
    TabOrder = 1
    OnKeyDown = ProximoCampo
    OnKeyPress = Numeros
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eNSU: TEditLuka
    Left = 278
    Top = 29
    Width = 85
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 20
    TabOrder = 2
    OnKeyDown = eNSUKeyDown
    OnKeyPress = Numeros
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eDigitosCartao: TEditLuka
    Left = 4
    Top = 29
    Width = 97
    Height = 22
    CharCase = ecUpperCase
    MaxLength = 20
    TabOrder = 0
    OnKeyDown = ProximoCampo
    OnKeyPress = Numeros
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
