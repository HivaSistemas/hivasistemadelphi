unit FrameGruposTributacoesEstadualCompras;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _GruposTribEstadualCompra,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Biblioteca, _Sessao, PesquisaGruposTributacoesEstadualCompra,
  Vcl.Menus;

type
  TFrGruposTributacoesEstadualCompra = class(TFrameHenrancaPesquisas)
  public
    function getGrupo(pLinha: Integer = -1): RecGruposTribEstadualCompra;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrGruposTributacoesEstadualCompra }

function TFrGruposTributacoesEstadualCompra.AdicionarDireto: TObject;
var
  vDados: TArray<RecGruposTribEstadualCompra>;
begin
  vDados := _GruposTribEstadualCompra.BuscarGruposTribEstadualCompra(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrGruposTributacoesEstadualCompra.AdicionarPesquisando: TObject;
begin
  Result := PesquisaGruposTributacoesEstadualCompra.Pesquisar;
end;

function TFrGruposTributacoesEstadualCompra.AdicionarPesquisandoTodos: TArray<TObject>;
begin

end;

function TFrGruposTributacoesEstadualCompra.getGrupo(pLinha: Integer): RecGruposTribEstadualCompra;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecGruposTribEstadualCompra(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrGruposTributacoesEstadualCompra.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecGruposTribEstadualCompra(FDados[i]).GrupoTribEstadualCompraId = RecGruposTribEstadualCompra(pSender).GrupoTribEstadualCompraId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrGruposTributacoesEstadualCompra.MontarGrid;
var
  i: Integer;
  vSender: RecGruposTribEstadualCompra;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecGruposTribEstadualCompra(FDados[i]);
      AAdd([IntToStr(vSender.GrupoTribEstadualCompraId), vSender.Descricao]);
    end;
  end;
end;

end.
