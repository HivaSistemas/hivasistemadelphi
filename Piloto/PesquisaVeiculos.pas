unit PesquisaVeiculos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Veiculos, _Sessao,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Biblioteca, Vcl.ExtCtrls;

type
  TFormPesquisaVeiculos = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pSomenteAtivos: Boolean): TObject;

implementation

{$R *.dfm}

var
  FSomenteAtivos: Boolean;

const
  coNome       = 2;
  coPlaca      = 3;
  coMotorista  = 4;
  coPesoMaximo = 5;
  coTara       = 6;
  coAtivo      = 7;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
var
  obj: TObject;
begin
  FSomenteAtivos := pSomenteAtivos;
  obj := _HerancaPesquisas.Pesquisar(TFormPesquisaVeiculos, _Veiculos.GetFiltros);
  if obj = nil then
    Result := nil
  else
    Result := RecVeiculos(obj);
end;

{ TFormPesquisaVeiculos }

procedure TFormPesquisaVeiculos.BuscarRegistros;
var
  i: Integer;
  vDados: TArray<RecVeiculos>;
begin
  inherited;

  vDados :=
    _Veiculos.BuscarVeiculos(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FSomenteAtivos
    );

  if vDados = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vDados);

  for i := Low(vDados) to High(vDados) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := NFormat(vDados[i].VeiculoId);
    sgPesquisa.Cells[coNome, i + 1]         := vDados[i].Nome;
    sgPesquisa.Cells[coPlaca, i + 1]        := vDados[i].Placa;
    sgPesquisa.Cells[coMotorista, i + 1]    := NFormat(vDados[i].MotoristaId) + ' - ' + vDados[i].NomeMotorista;
    sgPesquisa.Cells[coPesoMaximo, i + 1]   := NFormatN(vDados[i].PesoMaximo);
    sgPesquisa.Cells[coTara, i + 1]         := NFormatN(vDados[i].Tara);
    sgPesquisa.Cells[coAtivo, i + 1]        := vDados[i].Ativo;
  end;
  sgPesquisa.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vDados) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaVeiculos.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coSelecionado, coAtivo] then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
