unit BuscarDados.InutilizacaoNFe;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, _Biblioteca,
  Vcl.ExtCtrls, Vcl.StdCtrls, ComboBoxLuka, EditLuka, _Sessao, System.DateUtils;

type
  RecDadosInutilizacao = record
    numeracao_inicial: Integer;
    numeracao_final: Integer;
    serie: Integer;
    ano: Integer;
    justificativa: string;
    modeloNota: string;
  end;

  TFormBuscarDadosInutilizacaoNFe = class(TFormHerancaFinalizar)
    eNumeracaoInicial: TEditLuka;
    eNumeracaoFinal: TEditLuka;
    eJustificativa: TEditLuka;
    cbAnoInutilizacao: TComboBoxLuka;
    lb1: TLabel;
    lb2: TLabel;
    eSerie: TEditLuka;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    Label1: TLabel;
    cbModeloNota: TComboBoxLuka;
    procedure FormCreate(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function BuscarDados(): TRetornoTelaFinalizar<RecDadosInutilizacao>;

implementation

{$R *.dfm}

function BuscarDados(): TRetornoTelaFinalizar<RecDadosInutilizacao>;
var
  vForm: TFormBuscarDadosInutilizacaoNFe;
begin
  vForm := TFormBuscarDadosInutilizacaoNFe.Create(Application);

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.modeloNota        := vForm.cbModeloNota.GetValor;
    Result.Dados.numeracao_inicial := vForm.eNumeracaoInicial.AsInt;
    Result.Dados.numeracao_final   := vForm.eNumeracaoFinal.AsInt;
    Result.Dados.serie             := vForm.eSerie.AsInt;
    Result.Dados.ano               := vForm.cbAnoInutilizacao.GetValorInt;
    Result.Dados.justificativa     := vForm.eJustificativa.Text;
  end;
end;

{ TFormBuscarDadosInutilizacaoNFe }

procedure TFormBuscarDadosInutilizacaoNFe.FormCreate(Sender: TObject);
var
  i: Integer;
  vAno: Word;
begin
  inherited;
  cbAnoInutilizacao.Items.Clear;
  cbAnoInutilizacao.Valores.Clear;

  vAno := YearOf(Sessao.getDataHora);
  for i := 0 to 5 do begin
    cbAnoInutilizacao.Items.Add( IntToStr(vAno - i) );
    cbAnoInutilizacao.Valores.Add( IntToStr(vAno - i) );
  end;
end;

procedure TFormBuscarDadosInutilizacaoNFe.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eNumeracaoInicial.AsInt = 0 then begin
    Exclamar('A numera��o inicial n�o foi informada corretamente, verifique!');
    eNumeracaoInicial.SetFocus;
    Abort;
  end;

  if eNumeracaoFinal.AsInt = 0 then begin
    Exclamar('A numera��o final n�o foi informada corretamente, verifique!');
    eNumeracaoFinal.SetFocus;
    Abort;
  end;

  if eSerie.Text = '' then begin
    Exclamar('A s�rie das notas fiscais a serem inutilizadas n�o foi inforamada corretamente, verifique!');
    eSerie.SetFocus;
    Abort;
  end;

  if cbAnoInutilizacao.Text = '' then begin
    Exclamar('O ano da numera��o das notas fiscais a serem inutilizadas n�o foi informado corretamente, verifique!');
    cbAnoInutilizacao.SetFocus;
    Abort;
  end;

  if Length(eJustificativa.Text) < 15 then begin
    Exclamar('A justificativa para a inutiliza��o deve ter ao menos 15 caracteres, verifique!');
    cbAnoInutilizacao.SetFocus;
    Abort;
  end;
end;

end.
