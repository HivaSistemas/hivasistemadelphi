inherited FormDefinirTransporte: TFormDefinirTransporte
  Caption = 'Defini'#231#227'o de transporte'
  ClientHeight = 190
  ClientWidth = 638
  OnShow = FormShow
  ExplicitWidth = 644
  ExplicitHeight = 219
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 153
    Width = 638
    ExplicitTop = 153
    ExplicitWidth = 638
    inherited sbFinalizar: TSpeedButton
      Left = 3
      Top = -7
      ExplicitLeft = 3
      ExplicitTop = -7
    end
  end
  inline FrVeiculos: TFrVeiculos
    Left = 5
    Top = 16
    Width = 308
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 5
    ExplicitTop = 16
    ExplicitWidth = 308
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 283
      Height = 23
      ExplicitWidth = 283
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 308
      ExplicitWidth = 308
      inherited lbNomePesquisa: TLabel
        Width = 39
        Caption = 'Ve'#237'culo'
        ExplicitWidth = 39
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 203
        ExplicitLeft = 203
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 283
      Height = 24
      ExplicitLeft = 283
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  inline FrMotorista: TFrMotoristas
    Left = 325
    Top = 16
    Width = 308
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 325
    ExplicitTop = 16
    ExplicitWidth = 308
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 283
      Height = 23
      ExplicitWidth = 283
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 308
      ExplicitWidth = 308
      inherited lbNomePesquisa: TLabel
        Width = 53
        Caption = 'Motorista'
        ExplicitWidth = 53
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 203
        ExplicitLeft = 203
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 283
      Height = 24
      ExplicitLeft = 283
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object StaticTextLuka1: TStaticTextLuka
    Left = 13
    Top = 109
    Width = 132
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Qtde. produtos'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stQuantidadeProdutos: TStaticText
    Left = 13
    Top = 125
    Width = 132
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '120'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
  end
  object stPesoTotal: TStaticText
    Left = 153
    Top = 125
    Width = 132
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '1.500,250'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    Transparent = False
  end
  object StaticTextLuka2: TStaticTextLuka
    Left = 153
    Top = 109
    Width = 132
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Peso total (Kg)'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object StaticTextLuka3: TStaticTextLuka
    Left = 153
    Top = 69
    Width = 132
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Qtde. entregas'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stQuantidadeEntregas: TStaticText
    Left = 153
    Top = 85
    Width = 132
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '120'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    Transparent = False
  end
  object StaticTextLuka4: TStaticTextLuka
    Left = 13
    Top = 69
    Width = 132
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Peso m'#225'x. ve'#237'culo (Kg)'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 9
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stPesoMaximoVeiculo: TStaticText
    Left = 13
    Top = 85
    Width = 132
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '20.0000,000'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Transparent = False
  end
  inline FrAjudantes: TFrFuncionarios
    Left = 325
    Top = 63
    Width = 306
    Height = 81
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 11
    TabStop = True
    ExplicitLeft = 325
    ExplicitTop = 63
    ExplicitWidth = 306
    ExplicitHeight = 81
    inherited sgPesquisa: TGridLuka
      Width = 281
      Height = 64
      ExplicitWidth = 281
      ExplicitHeight = 64
    end
    inherited CkFiltroDuplo: TCheckBox
      Left = 3
      Top = 32
      ExplicitLeft = 3
      ExplicitTop = 32
    end
    inherited PnTitulos: TPanel
      Width = 306
      ExplicitWidth = 306
      inherited lbNomePesquisa: TLabel
        Width = 55
        Caption = 'Ajudantes'
        ExplicitWidth = 55
        ExplicitHeight = 14
      end
      inherited CkChaveUnica: TCheckBox
        Checked = False
        State = cbUnchecked
      end
      inherited pnSuprimir: TPanel
        Left = 201
        ExplicitLeft = 201
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 281
      Height = 65
      ExplicitLeft = 281
      ExplicitHeight = 65
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited ckAjudante: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
end
