unit MixVendas;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _Biblioteca,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, _Sessao, System.Math, _RecordsCadastros,
  _Produtos, _CustosProdutos, _Orcamentos, _OrcamentosItens, _RecordsOrcamentosVendas,
  _FormacaoCustosPrecos, _CondicoesPagamento, StaticTextLuka;

type
//  TProdutosMix = record
//    ProdutoId: Integer;
//    Nome: string;
//    Marca: string;
//    Unidade: string;
//    Quantidade: Double;
//    ValorUnitario: Currency;
//    ValorOutrasDesp: Currency;
//    ValorDesconto: Currency;
//    ValorTotalFrete: Currency;
//  end;

  TFormMixVenda = class(TFormHerancaFinalizar)
    sgProdutos: TGridLuka;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    lb15: TLabel;
    eTotalCustoEntrada: TEditLuka;
    eTotalImpostos: TEditLuka;
    eTotalEncargos: TEditLuka;
    eTotalCustoFinal: TEditLuka;
    stSPC: TStaticText;
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    eTotalProdutos: TEditLuka;
    eTotalOutrasDesp: TEditLuka;
    eTotalDesconto: TEditLuka;
    eTotalLiquido: TEditLuka;
    st1: TStaticText;
    eTotalPercOutrasDesp: TEditLuka;
    lb7: TLabel;
    eTotalPercDesconto: TEditLuka;
    lb6: TLabel;
    lb5: TLabel;
    lb8: TLabel;
    eTotalLucroBruto: TEditLuka;
    eTotalLucroLiquido: TEditLuka;
    eTotalPercLucroLiquido: TEditLuka;
    lb16: TLabel;
    eTotalPercLucroBruto: TEditLuka;
    lb17: TLabel;
    lb9: TLabel;
    stResultadoVenda: TStaticText;
    st3: TStaticText;
    stCliente: TStaticText;
    st4: TStaticText;
    stCondicaoPagamento: TStaticText;
    Label1: TLabel;
    eValorFrete: TEditLuka;
    lb10: TLabel;
    eTotalCustoVenda: TEditLuka;
    lb14: TLabel;
    eTotalPromocional: TEditLuka;
    StaticText1: TStaticText;
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure eTotalOutrasDespChange(Sender: TObject);
    procedure eTotalLiquidoChange(Sender: TObject);
    procedure eTotalDescontoChange(Sender: TObject);
    procedure sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure FormShow(Sender: TObject);
    procedure eTotalPercDescontoChange(Sender: TObject);
  private
    FClienteId: Integer;
    FOrcamentoJaGravado: Boolean;
    FCondicaoPagamentoId: Integer;
    FCondicaoPagamento: RecCondicoesPagamento;

    (* Calculos por produto *)
    procedure RatearOutrasDepesas;
    procedure RatearDesconto;

    procedure CalcularProduto(pLinha: Integer);
    procedure CalcularLucroProduto(pLinha: Integer);

    (* Totalizações *)
    procedure TotalizarCustos;

    procedure Totalizar;
    procedure TotalizarLucro;
    procedure TotalizarValorASerPago;
  end;

procedure Negociacao(pOrcamentoId: Integer); overload;

procedure Mix(
  pClienteId: Integer;
  pNomeCliente: string;
  pCondicaoPagamentoId: Integer;
  pNomeCondicaoPagamento: string;
  var pProdutos: TArray<RecOrcamentoItens>;
  pAbrirTela: Boolean;
  pReprocessarCusto: Boolean = False
); overload;

implementation

{$R *.dfm}

uses
  _BibliotecaGenerica, ValidarUsuarioAutorizado;

const
  coProdutoId           = 0;
  coNome                = 1;
  coResultado           = 2;
 // coMarca               = 3;
 // coUnidade             = 4;
  coQuantidade          = 3;
  coValorUnitario       = 4;
  coOutrasDesp          = 5;
  coDesconto            = 6;
  coValorLiquido        = 7;
  coCustoEntrada        = 8;
  coCustUnit            = 9;
  coImpostos            = 10;
  coEncargos            = 11;
  coCustoVendaCondPagto = 12;
  coValorCustoFinal     = 13;
  coValorLucroBruto     = 14;
  coPercLucroBruto      = 15;
  coValorLucroLiquido   = 16;
  coPercLucroLiquido    = 17;

  (* Ocultas *)
  coPercCustoVenda      = 18;
  coValorFrete          = 19;
  coTipoPreco           = 20;

procedure Negociacao(pOrcamentoId: Integer);
var
  i: Integer;
  vForm: TFormMixVenda;
  vOrc: TArray<RecOrcamentos>;
  vItens: TArray<RecOrcamentoItens>;
begin
  if not Sessao.AutorizadoRotina('visualizar_negociacao') then begin
    _Biblioteca.NaoAutorizadoRotina;
    Exit;
  end;

  if pOrcamentoId = 0 then
    Exit;

  vOrc := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 0, [pOrcamentoId]);
  if vOrc = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vForm := TFormMixVenda.Create(Application);

  vForm.FOrcamentoJaGravado  := True;
  vForm.FClienteId           := vOrc[0].cliente_id;
  vForm.FCondicaoPagamentoId := vOrc[0].condicao_id;

  vForm.stCliente.Caption           := getInformacao(vOrc[0].cliente_id, vOrc[0].nome_cliente);
  vForm.stCondicaoPagamento.Caption := getInformacao(vOrc[0].condicao_id, vOrc[0].nome_condicao_pagto);

  vItens := _OrcamentosItens.BuscarOrcamentosItens(Sessao.getConexaoBanco, 0, [pOrcamentoId]);
  for i := Low(vItens) to High(vItens) do begin
    vForm.sgProdutos.Cells[coProdutoId, i + 1]     := _Biblioteca.NFormat(vItens[i].produto_id);
    vForm.sgProdutos.Cells[coNome, i + 1]          := vItens[i].Nome;
    //vForm.sgProdutos.Cells[coMarca, i + 1]         := vItens[i].nome_marca;
   // vForm.sgProdutos.Cells[coUnidade, i + 1]       := vItens[i].unidade_venda;
    vForm.sgProdutos.Cells[coQuantidade, i + 1]    := _Biblioteca.NFormat(vItens[i].Quantidade);
    vForm.sgProdutos.Cells[coValorUnitario, i + 1] := _Biblioteca.NFormat(vItens[i].preco_unitario);
    vForm.sgProdutos.Cells[coOutrasDesp, i + 1]    := _Biblioteca.NFormatN(vItens[i].valor_total_outras_despesas);
    vForm.sgProdutos.Cells[coDesconto, i + 1]      := _Biblioteca.NFormatN(vItens[i].valor_total_desconto);
    vForm.sgProdutos.Cells[coValorFrete, i + 1]    := _Biblioteca.NFormatN(vItens[i].ValorTotalFrete);

    vForm.sgProdutos.Cells[coValorLiquido, i + 1]  := _Biblioteca.NFormat(vItens[i].valor_total - vItens[i].valor_total_desconto + vItens[i].valor_total_outras_despesas);

    vForm.sgProdutos.Cells[coCustoEntrada, i + 1]        := _Biblioteca.NFormatN(vItens[i].CustoEntrada);
    vForm.sgProdutos.Cells[coImpostos, i + 1]            := _Biblioteca.NFormatN(vItens[i].ValorImpostos);
    vForm.sgProdutos.Cells[coEncargos, i + 1]            := _Biblioteca.NFormatN(vItens[i].ValorEncargos);
    vForm.sgProdutos.Cells[coCustoVendaCondPagto, i + 1] := _Biblioteca.NFormatN(vItens[i].ValorCustoVenda);
    vForm.sgProdutos.Cells[coValorCustoFinal, i + 1]     := _Biblioteca.NFormatN(vItens[i].ValorCustoFinal);
    vForm.sgProdutos.Cells[coTipoPreco, i + 1]           := vItens[i].TipoPrecoUtilizado;

    vForm.CalcularLucroProduto( i + 1 );
  end;
  vForm.sgProdutos.SetLinhasGridPorTamanhoVetor( Length(vItens) );
  vForm.Totalizar;
  vForm.TotalizarLucro;

  _Biblioteca.SomenteLeitura([vForm.eValorFrete, vForm.eTotalOutrasDesp, vForm.eTotalPercOutrasDesp, vForm.eTotalDesconto, vForm.eTotalPercDesconto, vForm.eTotalLiquido], True);

  vForm.ShowModal;

  vForm.Free;
end;

procedure Mix(
  pClienteId: Integer;
  pNomeCliente: string;
  pCondicaoPagamentoId: Integer;
  pNomeCondicaoPagamento: string;
  var pProdutos: TArray<RecOrcamentoItens>;
  pAbrirTela: Boolean;
  pReprocessarCusto: Boolean = False
);
var
  i: Integer;
  vForm: TFormMixVenda;
  vRetTelaEntregasAto: TRetornoTelaFinalizar;
begin
  if (pAbrirTela and (not Sessao.AutorizadoRotina('visualizar_negociacao'))) then begin

    vRetTelaEntregasAto := ValidarUsuarioAutorizado.validaUsuarioAutorizado;
    if vRetTelaEntregasAto.RetTela <> trOk then begin
      _Biblioteca.NaoAutorizadoRotina;
      Exit;
    end;
  end;

  vForm := TFormMixVenda.Create(Application);

  vForm.FClienteId          := pClienteId;
  vForm.FOrcamentoJaGravado := False;
  vForm.FCondicaoPagamento := _CondicoesPagamento.BuscarCondicoesPagamentos(Sessao.getConexaoBanco, 0, [pCondicaoPagamentoId], True, False, False)[0];

  vForm.stCliente.Caption := _Biblioteca.NFormat(pClienteId) + ' - ' + pNomeCliente;
  vForm.stCondicaoPagamento.Caption := _Biblioteca.NFormat(pCondicaoPagamentoId) + ' - ' + pNomeCondicaoPagamento;

  for i := Low(pProdutos) to High(pProdutos) do begin
    vForm.sgProdutos.Cells[coProdutoId, i + 1]     := _Biblioteca.NFormat(pProdutos[i].produto_id);
    vForm.sgProdutos.Cells[coNome, i + 1]          := pProdutos[i].Nome;
    //vForm.sgProdutos.Cells[coMarca, i + 1]         := pProdutos[i].nome_marca;
   // vForm.sgProdutos.Cells[coUnidade, i + 1]       := pProdutos[i].unidade_venda;
    vForm.sgProdutos.Cells[coQuantidade, i + 1]    := _Biblioteca.NFormat(pProdutos[i].Quantidade);
    vForm.sgProdutos.Cells[coValorUnitario, i + 1] := _Biblioteca.NFormat(pProdutos[i].preco_unitario);
    vForm.sgProdutos.Cells[coOutrasDesp, i + 1]    := _Biblioteca.NFormatN(pProdutos[i].valor_total_outras_despesas);
    vForm.sgProdutos.Cells[coDesconto, i + 1]      := _Biblioteca.NFormatN(pProdutos[i].valor_total_desconto);
    vForm.sgProdutos.Cells[coValorLiquido, i + 1]  := _Biblioteca.NFormat(pProdutos[i].Quantidade * pProdutos[i].preco_unitario + pProdutos[i].valor_total_outras_despesas - pProdutos[i].valor_total_desconto);
    vForm.sgProdutos.Cells[coValorFrete, i + 1]    := _Biblioteca.NFormatN(pProdutos[i].ValorTotalFrete);
    vForm.sgProdutos.Cells[coTipoPreco, i + 1]     := pProdutos[i].TipoPrecoUtilizado;

    vForm.CalcularProduto(i + 1);
  end;
  vForm.sgProdutos.SetLinhasGridPorTamanhoVetor( Length(pProdutos) );

  if not pReprocessarCusto then
    vForm.Totalizar;

  if pAbrirTela then
    vForm.ShowModal
  else begin
    for i := 1 to vForm.sgProdutos.RowCount -1 do begin
      pProdutos[i - 1].ValorImpostos   := SFormatDouble(vForm.sgProdutos.Cells[coImpostos, i]);
      pProdutos[i - 1].ValorEncargos   := SFormatDouble(vForm.sgProdutos.Cells[coEncargos, i]);
      pProdutos[i - 1].ValorCustoVenda := SFormatDouble(vForm.sgProdutos.Cells[coCustoVendaCondPagto, i]);
      pProdutos[i - 1].ValorCustoFinal := SFormatDouble(vForm.sgProdutos.Cells[coValorCustoFinal, i]);
      pProdutos[i - 1].CustoEntrada    := SFormatDouble(vForm.sgProdutos.Cells[coCustoEntrada, i]);
    end;
  end;

  vForm.Free;
end;

procedure TFormMixVenda.CalcularLucroProduto(pLinha: Integer);
var
  vValorLiquido: Double;
begin
  vValorLiquido := SFormatDouble( sgProdutos.Cells[coValorLiquido, pLinha] ) + SFormatDouble( sgProdutos.Cells[coValorFrete, pLinha] );
  // AtualizarLucroBruto;
  sgProdutos.Cells[coValorLucroBruto, pLinha] := _Biblioteca.NFormatN(vValorLiquido - SFormatDouble(sgProdutos.Cells[coCustoEntrada, pLinha]));
  sgProdutos.Cells[coPercLucroBruto, pLinha]  := _Biblioteca.NFormatN( Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(vValorLiquido, SFormatDouble(sgProdutos.Cells[coValorLucroBruto, pLinha]), 4), 4 );

  // AtualizarLucroLiquido;
  sgProdutos.Cells[coValorLucroLiquido, pLinha] := _Biblioteca.NFormatN( vValorLiquido - SFormatDouble(sgProdutos.Cells[coValorCustoFinal, pLinha]) );
  sgProdutos.Cells[coPercLucroLiquido, pLinha]  := _Biblioteca.NFormatN( Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(vValorLiquido, SFormatDouble(sgProdutos.Cells[coValorLucroLiquido, pLinha]), 4), 4);
end;

procedure TFormMixVenda.CalcularProduto(pLinha: Integer);
var
  vProdutoId: Integer;
  vQuantidade: Double;
  vValorLiquido: Double;

  procedure AtualizarCustoEntrada;
  var
    vCusto: TArray<RecCustoProduto>;
  begin
    sgProdutos.Cells[coCustoEntrada, pLinha] := '';

    vCusto := _CustosProdutos.BuscarCustoProdutos(Sessao.getConexaoBanco, 0, [vProdutoId, Sessao.getEmpresaLogada.EmpresaId]);
    if vCusto <> nil then begin
      sgProdutos.Cells[coCustoEntrada, pLinha]      := _Biblioteca.NFormatN( vCusto[0].CustoCompraComercial * vQuantidade );
      sgProdutos.Cells[coCustUnit, pLinha]      := _Biblioteca.NFormatN( vCusto[0].CustoCompraComercial );
    end;
  end;

  procedure AtualizarImpostos;
  var
    vPerc: Double;
  begin
    vPerc := _FormacaoCustosPrecos.BuscarPercentualCustoVenda(Sessao.getConexaoBanco, Sessao.getEmpresaLogada.EmpresaId, vProdutoId);
    sgProdutos.Cells[coPercCustoVenda, pLinha] := _Biblioteca.NFormatN(vPerc, 6);

    sgProdutos.Cells[coImpostos, pLinha] := _Biblioteca.NFormatN(vValorLiquido * vPerc * 0.01, 2);
  end;

  procedure AtualizarEncargos;
  begin
    sgProdutos.Cells[coEncargos, pLinha] := _Biblioteca.NFormatN(vValorLiquido * FCondicaoPagamento.PercentualEncagos * 0.01, 2);
  end;

  procedure AtualizarCustoVendaCondPagamento;
  begin
    sgProdutos.Cells[coCustoVendaCondPagto, pLinha] := _Biblioteca.NFormatN(vValorLiquido * FCondicaoPagamento.PercentualCustoVenda * 0.01, 2);
  end;

begin
  vProdutoId  := SFormatInt(sgProdutos.Cells[coProdutoId, pLinha]);
  vQuantidade := SFormatDouble(sgProdutos.Cells[coQuantidade, pLinha]);
  vValorLiquido := SFormatDouble(sgProdutos.Cells[coValorLiquido, pLinha]) + SFormatDouble(sgProdutos.Cells[coValorFrete, pLinha]);

  AtualizarCustoEntrada;
  AtualizarImpostos;
  AtualizarEncargos;
  AtualizarCustoVendaCondPagamento;

  sgProdutos.Cells[coValorCustoFinal, pLinha] :=
    _Biblioteca.NFormatN(
      SFormatDouble(sgProdutos.Cells[coCustoEntrada, pLinha]) +
      SFormatDouble(sgProdutos.Cells[coImpostos, pLinha]) +
      SFormatDouble(sgProdutos.Cells[coCustoVendaCondPagto, pLinha]) +
      SFormatDouble(sgProdutos.Cells[coEncargos, pLinha])
    );

  CalcularLucroProduto(pLinha);
end;

procedure TFormMixVenda.eTotalDescontoChange(Sender: TObject);
begin
  inherited;
  if FOrcamentoJaGravado then
    Exit;

  if eTotalDesconto.Focused or (Sender = nil) then begin
    eTotalPercDesconto.AsDouble :=
      Sessao.getCalculosSistema.CalcOrcamento.getPercDesconto(
        eTotalProdutos.AsDouble - eTotalPromocional.AsDouble,
        eTotalOutrasDesp.AsDouble,
        eTotalDesconto.AsDouble
      );

    if eTotalDesconto.Focused then
      TotalizarValorASerPago;
  end;
  RatearDesconto;
  TotalizarLucro;
end;

procedure TFormMixVenda.eTotalLiquidoChange(Sender: TObject);
begin
  inherited;
  if FOrcamentoJaGravado then
    Exit;

  if eTotalLiquido.Focused then begin
    _Biblioteca.LimparCampos([eTotalDesconto, eTotalOutrasDesp, eTotalPercDesconto, eTotalPercOutrasDesp]);

    if eTotalLiquido.AsCurr > eTotalProdutos.AsCurr then begin
      eTotalOutrasDesp.AsDouble := eTotalLiquido.AsDouble - eTotalProdutos.AsDouble - eValorFrete.AsCurr;
      eTotalOutrasDespChange(nil);
    end
    else begin
      eTotalDesconto.AsDouble := (eTotalProdutos.AsDouble + eTotalOutrasDesp.AsDouble + eValorFrete.AsCurr) - eTotalLiquido.AsDouble;
      eTotalDescontoChange(nil);
    end;
  end;
  TotalizarLucro;
end;

procedure TFormMixVenda.eTotalOutrasDespChange(Sender: TObject);
begin
  inherited;
  if FOrcamentoJaGravado then
    Exit;

  if eTotalOutrasDesp.Focused or (Sender = nil) then begin
    eTotalPercOutrasDesp.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercOutrasDespesas(eTotalProdutos.AsDouble, eTotalOutrasDesp.AsDouble);

    if eTotalOutrasDesp.Focused then begin
      eTotalDescontoChange(nil);
      TotalizarValorASerPago;
    end;
  end;

  RatearOutrasDepesas;
  TotalizarLucro;
end;

procedure TFormMixVenda.eTotalPercDescontoChange(Sender: TObject);
begin
  inherited;
  if FOrcamentoJaGravado then
    Exit;

  if not eTotalPercDesconto.Focused then
    Exit;

  eTotalDesconto.AsDouble := _BibliotecaGenerica.ZeroSeNegativo((eTotalProdutos.AsDouble - eTotalPromocional.AsDouble + eTotalOutrasDesp.AsDouble)* eTotalPercDesconto.AsDouble * 0.01);

  TotalizarValorASerPago;
  RatearDesconto;
  TotalizarLucro;
end;

procedure TFormMixVenda.FormShow(Sender: TObject);
begin
  inherited;
  //sgProdutos.Col := coMarca;
end;

procedure TFormMixVenda.RatearDesconto;
var
  i: Integer;
  vValorItem: Double;
begin
  if FOrcamentoJaGravado then
    Exit;

  for i := 1 to sgProdutos.RowCount -1 do begin
    vValorItem := SFormatDouble(sgProdutos.Cells[coQuantidade, i]) * (SFormatDouble(sgProdutos.Cells[coValorUnitario, i]));
    sgProdutos.Cells[coDesconto, i] := _Biblioteca.NFormatN( vValorItem / eTotalProdutos.AsDouble * eTotalDesconto.AsDouble );

    vValorItem := vValorItem + SFormatDouble(sgProdutos.Cells[coOutrasDesp, i]) - SFormatDouble(sgProdutos.Cells[coDesconto, i]);
    sgProdutos.Cells[coValorLiquido, i] := _Biblioteca.NFormatN(vValorItem, 2);

    CalcularProduto(i);
  end;
  TotalizarCustos;
  TotalizarLucro;
end;

procedure TFormMixVenda.RatearOutrasDepesas;
var
  i: Integer;
  vValorItem: Double;
begin
  if FOrcamentoJaGravado then
    Exit;

  for i := 1 to sgProdutos.RowCount -1 do begin
    vValorItem := SFormatDouble(sgProdutos.Cells[coQuantidade, i]) * (SFormatDouble(sgProdutos.Cells[coValorUnitario, i]));
    sgProdutos.Cells[coOutrasDesp, i] := _Biblioteca.NFormatN( vValorItem / eTotalProdutos.AsDouble * eTotalOutrasDesp.AsDouble );

    vValorItem := vValorItem + SFormatDouble(sgProdutos.Cells[coOutrasDesp, i]) - SFormatDouble(sgProdutos.Cells[coDesconto, i]);
    sgProdutos.Cells[coValorLiquido, i] := _Biblioteca.NFormatN(vValorItem, 2);

    CalcularProduto(i);
  end;
  TotalizarCustos;
  TotalizarLucro;
end;

procedure TFormMixVenda.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  //if ACol in[coNome, coMarca] then
   // vAlinhamento := taLeftJustify
  //else if ACol in[coUnidade] then
  //  vAlinhamento := taCenter
  //else
    vAlinhamento := taRightJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormMixVenda.sgProdutosGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol <> coResultado then
    Exit;

  if SFormatDouble(sgProdutos.Cells[coPercLucroLiquido, ARow]) > 60 then
    ABrush.Color := Sessao.getParametrosEmpresa.CorLucratividade1
  else if SFormatDouble(sgProdutos.Cells[coPercLucroLiquido, ARow]) > 30 then
    ABrush.Color := Sessao.getParametrosEmpresa.CorLucratividade2
  else if SFormatDouble(sgProdutos.Cells[coPercLucroLiquido, ARow]) > 10 then
    ABrush.Color := Sessao.getParametrosEmpresa.CorLucratividade3
  else if SFormatDouble(sgProdutos.Cells[coPercLucroLiquido, ARow]) > 0 then
    ABrush.Color := Sessao.getParametrosEmpresa.CorLucratividade4
  else
    ABrush.Color := clRed;
end;

procedure TFormMixVenda.sgProdutosSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  CanSelect := not(ACol in[coResultado]);
end;

procedure TFormMixVenda.Totalizar;
var
  i: Integer;

  vTotalCustoEntrada: Double;
  vTotalImpostos: Double;
  vTotalEncargos: Double;
  vTotalCustoVendaCondPagto: Double;

  vTotalProdutos: Double;
  vTotalPromocional: Double;
  vTotalFrete: Double;
  vTotalOutrasDesp: Double;
  vTotalDesconto: Double;
begin
  vTotalCustoEntrada := 0;
  vTotalImpostos     := 0;
  vTotalEncargos     := 0;
  vTotalCustoVendaCondPagto := 0;

  vTotalProdutos    := 0;
  vTotalPromocional := 0;
  vTotalFrete       := 0;
  vTotalOutrasDesp  := 0;
  vTotalDesconto    := 0;

  for i := sgProdutos.FixedRows to sgProdutos.RowCount - 1 do begin
    vTotalCustoEntrada         := vTotalCustoEntrada + SFormatDouble(sgProdutos.Cells[coCustoEntrada, i]);
    vTotalImpostos             := vTotalImpostos + SFormatDouble(sgProdutos.Cells[coImpostos, i]);
    vTotalEncargos             := vTotalEncargos + SFormatDouble(sgProdutos.Cells[coEncargos, i]);
    vTotalCustoVendaCondPagto  := vTotalCustoVendaCondPagto + SFormatDouble(sgProdutos.Cells[coCustoVendaCondPagto, i]);

    vTotalProdutos   := vTotalProdutos + Arredondar(SFormatDouble(sgProdutos.Cells[coValorUnitario, i]) * SFormatDouble(sgProdutos.Cells[coQuantidade, i]), 2);
    vTotalFrete      := vTotalFrete + SFormatDouble(sgProdutos.Cells[coValorFrete, i]);
    vTotalOutrasDesp := vTotalOutrasDesp + SFormatDouble(sgProdutos.Cells[coOutrasDesp, i]);
    vTotalDesconto   := vTotalDesconto + SFormatDouble(sgProdutos.Cells[coDesconto, i]);

    if sgProdutos.Cells[coTipoPreco, i] = 'PRO' then
      vTotalPromocional := vTotalPromocional + Arredondar(SFormatDouble(sgProdutos.Cells[coValorUnitario, i]) * SFormatDouble(sgProdutos.Cells[coQuantidade, i]), 2);
  end;

  eTotalCustoEntrada.AsCurr := vTotalCustoEntrada;
  eTotalImpostos.AsCurr     := vTotalImpostos;
  eTotalEncargos.AsCurr     := vTotalEncargos;
  eTotalCustoVenda.AsCurr   := vTotalCustoVendaCondPagto;
  eTotalCustoFinal.AsCurr   := vTotalCustoEntrada + vTotalImpostos + vTotalEncargos + vTotalCustoVendaCondPagto;

  eTotalProdutos.AsCurr         := vTotalProdutos;
  eTotalPromocional.AsCurr      := vTotalPromocional;
  eValorFrete.AsCurr            := vTotalFrete;

  eTotalOutrasDesp.AsCurr       := vTotalOutrasDesp;
  eTotalOutrasDespChange(nil);

  eTotalDesconto.AsCurr         := vTotalDesconto;
  eTotalDescontoChange(nil);

  eTotalLiquido.AsCurr          := vTotalProdutos + vTotalOutrasDesp + vTotalFrete - vTotalDesconto;
end;

procedure TFormMixVenda.TotalizarCustos;
var
  i: Integer;

  vTotalImpostos: Double;
  vTotalEncargos: Double;
  vTotalCustoVendaCondPagto: Double;
begin
  vTotalImpostos := 0;
  vTotalEncargos := 0;
  vTotalCustoVendaCondPagto := 0;

  for i := sgProdutos.FixedRows to sgProdutos.RowCount - 1 do begin
    vTotalImpostos             := vTotalImpostos + SFormatDouble(sgProdutos.Cells[coImpostos, i]);
    vTotalEncargos             := vTotalEncargos + SFormatDouble(sgProdutos.Cells[coEncargos, i]);
    vTotalCustoVendaCondPagto  := vTotalCustoVendaCondPagto + SFormatDouble(sgProdutos.Cells[coCustoVendaCondPagto, i]);
  end;

  eTotalImpostos.AsCurr   := vTotalImpostos;
  eTotalEncargos.AsCurr   := vTotalEncargos;
  eTotalCustoVenda.AsCurr := vTotalCustoVendaCondPagto;

  eTotalCustoFinal.AsCurr := eTotalCustoEntrada.AsCurr + eTotalImpostos.AsCurr + eTotalEncargos.AsCurr + eTotalCustoVenda.AsCurr;
end;

procedure TFormMixVenda.TotalizarLucro;
begin
  eTotalLucroBruto.AsCurr       := eTotalLiquido.AsCurr - eTotalCustoEntrada.AsCurr;
  eTotalPercLucroBruto.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercLucroBruto(eTotalLiquido.AsCurr, eTotalLucroBruto.AsCurr, 4);

  eTotalLucroLiquido.AsDouble     := eTotalLiquido.AsCurr - eTotalCustoFinal.AsCurr;
  eTotalPercLucroLiquido.AsDouble := Sessao.getCalculosSistema.CalcOrcamento.getPercLucroLiquido(eTotalLiquido.AsCurr, eTotalLucroLiquido.AsCurr, 4);

  if eTotalPercLucroLiquido.AsDouble > 60 then
    stResultadoVenda.Color := Sessao.getParametrosEmpresa.CorLucratividade1
  else if eTotalPercLucroLiquido.AsDouble > 30 then
    stResultadoVenda.Color := Sessao.getParametrosEmpresa.CorLucratividade2
  else if eTotalPercLucroLiquido.AsDouble > 10 then
    stResultadoVenda.Color := Sessao.getParametrosEmpresa.CorLucratividade3
  else if eTotalPercLucroLiquido.AsDouble > 0 then
    stResultadoVenda.Color := Sessao.getParametrosEmpresa.CorLucratividade4
  else
    stResultadoVenda.Color := clRed;

  sgProdutos.Repaint;
  sgProdutos.Refresh;
end;

procedure TFormMixVenda.TotalizarValorASerPago;
begin
  eTotalLiquido.AsDouble := eTotalProdutos.AsCurr + eValorFrete.AsCurr + eTotalOutrasDesp.AsDouble - eTotalDesconto.AsDouble;
end;

end.

