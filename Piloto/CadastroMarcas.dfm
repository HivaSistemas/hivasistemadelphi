inherited FormCadastroMarcas: TFormCadastroMarcas
  Caption = 'Cadastro de marcas'
  ClientHeight = 206
  ClientWidth = 451
  ExplicitWidth = 457
  ExplicitHeight = 235
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 126
    Top = 58
    Width = 32
    Height = 14
    Caption = 'Nome'
  end
  inherited pnOpcoes: TPanel
    Height = 206
    ExplicitHeight = 191
  end
  inherited eID: TEditLuka
    TabOrder = 3
  end
  object eNome: TEditLuka
    Left = 126
    Top = 73
    Width = 317
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 1
    OnKeyDown = ProximoCampo
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
end
