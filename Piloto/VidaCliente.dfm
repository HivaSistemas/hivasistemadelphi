inherited FormVidaCliente: TFormVidaCliente
  ActiveControl = frCliente
  Caption = 'Hist'#243'rico do cliente'
  ClientHeight = 462
  ClientWidth = 793
  OnShow = FormShow
  ExplicitWidth = 799
  ExplicitHeight = 491
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 425
    Width = 793
    ExplicitTop = 425
    ExplicitWidth = 787
  end
  object pcDados: TPageControlAltis
    Left = 0
    Top = 44
    Width = 793
    Height = 378
    ActivePage = tsGerais
    Align = alCustom
    TabOrder = 1
    CarregouAba = False
    object tsGerais: TTabSheet
      Caption = 'Principal'
      ImageIndex = -1
      object Label2: TLabel
        Left = 3
        Top = 21
        Width = 76
        Height = 14
        Caption = 'Data cadastro'
      end
      object lbDataNascimento: TLabel
        Left = 100
        Top = 21
        Width = 91
        Height = 14
        Caption = 'Data anivers'#225'rio'
      end
      object Label1: TLabel
        Left = 405
        Top = 63
        Width = 100
        Height = 14
        Caption = 'Data '#250'ltima venda'
      end
      object lb23: TLabel
        Left = 118
        Top = 186
        Width = 111
        Height = 14
        Caption = 'Valor '#250'ltima entrega'
      end
      object lb8: TLabel
        Left = 523
        Top = 63
        Width = 129
        Height = 14
        Caption = 'Qtde. dias sem comprar'
      end
      object Label6: TLabel
        Left = 3
        Top = 186
        Width = 109
        Height = 14
        Caption = 'Data '#250'ltima entrega'
      end
      object Label3: TLabel
        Left = 405
        Top = 21
        Width = 107
        Height = 14
        Caption = 'Qtde.orc. em aberto'
      end
      object Label4: TLabel
        Left = 524
        Top = 21
        Width = 74
        Height = 14
        Caption = 'Qtde.pedidos'
      end
      object Label8: TLabel
        Left = 651
        Top = 21
        Width = 94
        Height = 14
        Caption = 'Qtde. devolu'#231#245'es'
      end
      object Label10: TLabel
        Left = 244
        Top = 307
        Width = 81
        Height = 14
        Caption = 'Qtde. retiradas'
      end
      object Label11: TLabel
        Left = 245
        Top = 186
        Width = 80
        Height = 14
        Caption = 'Qtde. entregas'
      end
      object Label12: TLabel
        Left = 3
        Top = 229
        Width = 143
        Height = 14
        Caption = 'Qtde. entregas retornadas'
      end
      object Label13: TLabel
        Left = 405
        Top = 130
        Width = 137
        Height = 14
        Caption = 'Qtde. financeiros abertos'
      end
      object Label14: TLabel
        Left = 405
        Top = 173
        Width = 145
        Height = 14
        Caption = 'Qtde. financeiros baixados'
      end
      object lb11: TLabel
        Left = 197
        Top = 21
        Width = 48
        Height = 14
        Caption = 'Telefone'
      end
      object lb12: TLabel
        Left = 285
        Top = 21
        Width = 39
        Height = 14
        Caption = 'Celular'
      end
      object lb15: TLabel
        Left = 3
        Top = 63
        Width = 92
        Height = 14
        Caption = 'Limite de cr'#233'dito'
      end
      object Label15: TLabel
        Left = 102
        Top = 63
        Width = 79
        Height = 14
        Caption = 'Valor utilizado'
      end
      object Label16: TLabel
        Left = 201
        Top = 63
        Width = 80
        Height = 14
        Caption = 'Saldo restante'
      end
      object Label18: TLabel
        Left = 3
        Top = 105
        Width = 121
        Height = 14
        Caption = 'Data aprova'#231#227'o limite'
      end
      object Label19: TLabel
        Left = 131
        Top = 105
        Width = 113
        Height = 14
        Caption = 'Data validade limite'
      end
      object Label20: TLabel
        Left = 3
        Top = 307
        Width = 110
        Height = 14
        Caption = 'Data '#250'ltima retirada'
      end
      object Label21: TLabel
        Left = 118
        Top = 307
        Width = 112
        Height = 14
        Caption = 'Valor '#250'ltima retirada'
      end
      object Label5: TLabel
        Left = 602
        Top = 130
        Width = 136
        Height = 14
        Caption = 'Valor financeiros abertos'
      end
      object Label7: TLabel
        Left = 602
        Top = 173
        Width = 144
        Height = 14
        Caption = 'Valor financeiros baixados'
      end
      object eDataCadastro: TEditLukaData
        Left = 3
        Top = 35
        Width = 93
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        TabStop = False
        EditMask = '99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = '  /  /    '
      end
      object eDataNascimento: TEditLukaData
        Left = 100
        Top = 35
        Width = 93
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '  /  /    '
      end
      object eDataUltimaVenda: TEditLukaData
        Left = 405
        Top = 77
        Width = 111
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        TabOrder = 2
        Text = '  /  /    '
      end
      object eValorUltimaEntrega: TEditLuka
        Left = 118
        Top = 201
        Width = 120
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eQuantidadeDiasSemComprar: TEditLuka
        Left = 523
        Top = 77
        Width = 155
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object stSPC: TStaticText
        Left = -1
        Top = 166
        Width = 375
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Dados de entregas'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
      end
      object StaticText1: TStaticText
        Left = -4
        Top = 0
        Width = 378
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Dados do cliente'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 6
        Transparent = False
      end
      object eTelefone: TEditTelefoneLuka
        Left = 197
        Top = 35
        Width = 85
        Height = 22
        EditMask = '(99) 9999-9999;1; '
        MaxLength = 14
        TabOrder = 7
        Text = '(  )     -    '
      end
      object eCelular: TEditTelefoneLuka
        Left = 286
        Top = 35
        Width = 88
        Height = 22
        EditMask = '(99) 99999-9999;1; '
        MaxLength = 15
        TabOrder = 8
        Text = '(  )      -    '
        Celular = True
      end
      object eDataUltimaEntrega: TEditLukaData
        Left = 3
        Top = 201
        Width = 109
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        TabStop = False
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 9
        Text = '  /  /    '
      end
      object StaticText2: TStaticText
        Left = 405
        Top = 0
        Width = 380
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Dados de vendas'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 10
        Transparent = False
      end
      object StaticText3: TStaticText
        Left = 405
        Top = 109
        Width = 380
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Dados do financeiro'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 11
        Transparent = False
      end
      object eLimiteCredito: TEditLuka
        Left = 3
        Top = 77
        Width = 93
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 12
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorUtilizadoLimiteCredito: TEditLuka
        Left = 102
        Top = 77
        Width = 93
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 13
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorRestanteLimiteCredito: TEditLuka
        Left = 201
        Top = 77
        Width = 109
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 14
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object StaticText4: TStaticText
        Left = 3
        Top = 286
        Width = 371
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Dados de retiradas'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 15
        Transparent = False
      end
      object eDataUltimaRetirada: TEditLukaData
        Left = 3
        Top = 321
        Width = 109
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        TabStop = False
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 16
        Text = '  /  /    '
      end
      object eDataAprovacaoLimite: TEditLukaData
        Left = 3
        Top = 120
        Width = 122
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        TabStop = False
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 17
        Text = '  /  /    '
      end
      object eDataValidadeLimite: TEditLukaData
        Left = 131
        Top = 120
        Width = 122
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        TabStop = False
        EditMask = '99/99/9999;1; '
        MaxLength = 10
        ReadOnly = True
        TabOrder = 18
        Text = '  /  /    '
      end
      object eValorUltimaRetirada: TEditLuka
        Left = 118
        Top = 321
        Width = 120
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 19
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eQuantidadeDevolucoes: TEditLuka
        Left = 649
        Top = 35
        Width = 120
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 20
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eQuantidadeEntregas: TEditLuka
        Left = 244
        Top = 201
        Width = 130
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 21
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eQuantidadeEntregasRetornadas: TEditLuka
        Left = 3
        Top = 243
        Width = 142
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 22
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eQuantidadeFinanceirosAbertos: TEditLuka
        Left = 405
        Top = 145
        Width = 170
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 23
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eQuantidadeFinanceirosBaixados: TEditLuka
        Left = 405
        Top = 187
        Width = 170
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 24
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eQuantidadeOrcamentosEmAberto: TEditLuka
        Left = 405
        Top = 35
        Width = 111
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 25
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eQuantidadePedidos: TEditLuka
        Left = 523
        Top = 35
        Width = 120
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 26
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eQuantidadeRetiradas: TEditLuka
        Left = 244
        Top = 321
        Width = 130
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 27
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorFinanceirosEmAberto: TEditLuka
        Left = 602
        Top = 145
        Width = 167
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 28
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorFinanceirosBaixados: TEditLuka
        Left = 602
        Top = 187
        Width = 167
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 29
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      inline FrCondicoesPagtoRestritas: TFrCondicoesPagamento
        Left = 405
        Top = 221
        Width = 364
        Height = 122
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 30
        TabStop = True
        ExplicitLeft = 405
        ExplicitTop = 221
        ExplicitWidth = 364
        ExplicitHeight = 122
        inherited sgPesquisa: TGridLuka
          Width = 339
          Height = 105
          TabOrder = 1
          ExplicitWidth = 339
          ExplicitHeight = 105
        end
        inherited CkAspas: TCheckBox
          TabOrder = 3
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 5
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 7
        end
        inherited PnTitulos: TPanel
          Width = 364
          TabOrder = 0
          ExplicitWidth = 364
          inherited lbNomePesquisa: TLabel
            Width = 243
            Height = 14
            Caption = 'Condi'#231#245'es de pagamento restritas liberadas'
            ExplicitWidth = 243
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Left = 320
            Checked = False
            State = cbUnchecked
            ExplicitLeft = 320
          end
          inherited pnSuprimir: TPanel
            Left = 294
            ExplicitLeft = 294
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 339
          Height = 106
          TabOrder = 2
          ExplicitLeft = 339
          ExplicitHeight = 106
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
          TabOrder = 6
        end
      end
    end
    object tsOrcamentos: TTabSheet
      Caption = 'Or'#231'amentos'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 777
      ExplicitHeight = 0
      object sgOrcamentos: TGridLuka
        Left = 0
        Top = 0
        Width = 785
        Height = 349
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 9
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        TabOrder = 0
        OnDblClick = sgOrcamentosDblClick
        OnDrawCell = sgOrcamentosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Or'#231'amento'
          'Empresa'
          'Status'
          'Data de cadastro'
          'Vendedor'
          'Cond.pagamento'
          'Valor produtos'
          'Valor desconto'
          'Valor out. desp.'
          'Valor total')
        OnGetCellColor = sgOrcamentosGetCellColor
        Grid3D = False
        RealColCount = 15
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          68
          111
          93
          98
          97
          136
          84
          90
          89)
      end
    end
    object tsPedidos: TTabSheet
      Caption = 'Pedidos'
      ImageIndex = 1
      OnShow = tsPedidosShow
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 777
      ExplicitHeight = 0
      object pcPedidos: TPageControlAltis
        Left = 0
        Top = 0
        Width = 785
        Height = 349
        ActivePage = tsVendas
        Align = alClient
        TabOrder = 0
        CarregouAba = False
        ExplicitWidth = 777
        object tsVendas: TTabSheet
          Caption = 'Vendas'
          OnShow = tsVendasShow
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 769
          ExplicitHeight = 0
          object sgPedidos: TGridLuka
            Left = 0
            Top = 0
            Width = 777
            Height = 320
            Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
            Align = alClient
            ColCount = 9
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
            TabOrder = 0
            OnDblClick = sgPedidosDblClick
            OnDrawCell = sgPedidosDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 16768407
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clMenuHighlight
            HCol.Strings = (
              'Pedido'
              'Empresa'
              'Data de emiss'#227'o'
              'Vendedor'
              'Cond.pagamento'
              'Valor total'
              'Valor produtos'
              'Valor desconto'
              'Valor out. desp.')
            OnGetCellColor = sgPedidosGetCellColor
            Grid3D = False
            RealColCount = 15
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              68
              133
              98
              115
              123
              92
              91
              90
              93)
          end
        end
        object tsDevolucoes: TTabSheet
          Caption = 'Devolu'#231#245'es'
          ImageIndex = 1
          OnShow = tsDevolucoesShow
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 769
          ExplicitHeight = 0
          object sgDevolucoes: TGridLuka
            Left = 0
            Top = 0
            Width = 777
            Height = 320
            Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
            Align = alClient
            ColCount = 12
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
            TabOrder = 0
            OnDblClick = sgDevolucoesDblClick
            OnDrawCell = sgDevolucoesDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 16768407
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clMenuHighlight
            HCol.Strings = (
              'Devolu'#231#227'o'
              'Empresa'
              'Pedido'
              'Valor bruto'
              'Valor out.desp.'
              'Valor desc.'
              'Valor frete'
              'Valor ST'
              'Valor IPI'
              'Valor l'#237'quido'
              'Data/hora devolu'#231#227'o'
              'Usu'#225'rio devolu'#231#227'o')
            OnGetCellColor = sgDevolucoesGetCellColor
            Grid3D = False
            RealColCount = 15
            OrdenarOnClick = True
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              69
              170
              78
              91
              96
              84
              88
              84
              80
              131
              122
              148)
          end
        end
        object tsMixProdutos: TTabSheet
          Caption = 'Mix de produtos'
          ImageIndex = 2
          OnShow = tsMixProdutosShow
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 769
          ExplicitHeight = 0
          object sgMixProdutos: TGridLuka
            Left = 0
            Top = 0
            Width = 777
            Height = 320
            Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
            Align = alClient
            ColCount = 9
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
            TabOrder = 0
            OnDrawCell = sgMixProdutosDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 16768407
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clMenuHighlight
            HCol.Strings = (
              'Produto'
              'Nome'
              'Marca'
              'Quantidade'
              'Und.'
              'Pre'#231'o m'#233'dio'
              'Valor total'
              '% participa'#231#227'o'
              '% Margem')
            OnGetCellColor = sgMixProdutosGetCellColor
            Grid3D = False
            RealColCount = 15
            OrdenarOnClick = True
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              69
              163
              128
              75
              40
              81
              77
              95
              69)
          end
        end
      end
    end
    object tsNotasFiscais: TTabSheet
      Caption = 'Notas fiscais'
      ImageIndex = 2
      OnShow = tsNotasFiscaisShow
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 777
      ExplicitHeight = 0
      object sgNotasFiscais: TGridLuka
        Left = 0
        Top = 0
        Width = 785
        Height = 349
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        OnDblClick = sgNotasFiscaisDblClick
        OnDrawCell = sgNotasFiscaisDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Nota fiscal'
          'Data cadastro'
          'Empresa'
          'Tipo nota'
          'Tipo movimento'
          'N'#250'mero nota'
          'Valor total nota'
          'Status')
        OnGetCellColor = sgNotasFiscaisGetCellColor
        Grid3D = False
        RealColCount = 21
        OrdenarOnClick = True
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          72
          96
          143
          117
          126
          93
          101
          107)
      end
    end
    object tsFinanceiros: TTabSheet
      Caption = 'Financeiros'
      ImageIndex = 3
      OnShow = tsFinanceirosShow
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 777
      ExplicitHeight = 0
      object pcFinanceiros: TPageControlAltis
        Left = 0
        Top = 0
        Width = 785
        Height = 349
        ActivePage = tsBaixados
        Align = alClient
        TabOrder = 0
        CarregouAba = False
        ExplicitWidth = 777
        object tsAbertos: TTabSheet
          Caption = 'Abertos'
          OnShow = tsAbertosShow
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 769
          ExplicitHeight = 0
          object sgTitulosAbertos: TGridLuka
            Left = 0
            Top = 0
            Width = 777
            Height = 320
            Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
            Align = alClient
            ColCount = 15
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
            TabOrder = 0
            OnDblClick = sgTitulosAbertosDblClick
            OnDrawCell = sgTitulosAbertosDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 16768407
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clMenuHighlight
            HCol.Strings = (
              'C'#243'digo'
              'Documento'
              'Parcela'
              'Valor'
              'Multa'
              'Juros'
              'Reten'#231#227'o'
              'Valor l'#237'quido'
              'Data cad.'
              'Data Vencto.'
              'Dias atraso'
              'Empresa'
              'Status'
              'Tipo de cobran'#231'a'
              'Portador')
            OnGetCellColor = sgTitulosAbertosGetCellColor
            Grid3D = False
            RealColCount = 20
            OrdenarOnClick = True
            Indicador = True
            AtivarPopUpSelecao = False
            ExplicitWidth = 769
            ColWidths = (
              61
              106
              63
              69
              61
              73
              75
              94
              85
              93
              87
              131
              90
              124
              146)
          end
        end
        object tsBaixados: TTabSheet
          Caption = 'Baixados'
          ImageIndex = 1
          OnShow = tsBaixadosShow
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 769
          ExplicitHeight = 0
          object sgTitulosBaixados: TGridLuka
            Left = 0
            Top = 0
            Width = 777
            Height = 320
            Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
            Align = alClient
            ColCount = 15
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
            TabOrder = 0
            OnDblClick = sgTitulosBaixadosDblClick
            OnDrawCell = sgTitulosBaixadosDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 16768407
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clMenuHighlight
            HCol.Strings = (
              'C'#243'digo'
              'Documento'
              'Parcela'
              'Valor'
              'Multa'
              'Juros'
              'Reten'#231#227'o'
              'Valor l'#237'quido'
              'Data cad.'
              'Data Vencto.'
              'Dias atraso'
              'Empresa'
              'Status'
              'Tipo de cobran'#231'a'
              'Portador')
            OnGetCellColor = sgTitulosBaixadosGetCellColor
            Grid3D = False
            RealColCount = 20
            OrdenarOnClick = True
            Indicador = True
            AtivarPopUpSelecao = False
            ColWidths = (
              61
              96
              63
              69
              61
              73
              75
              94
              85
              93
              87
              131
              95
              124
              139)
          end
        end
      end
    end
    object tsEntregas: TTabSheet
      Caption = 'Entregas'
      ImageIndex = 4
      OnShow = tsEntregasShow
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 777
      ExplicitHeight = 0
      object pcEntregas: TPageControlAltis
        Left = 0
        Top = 0
        Width = 785
        Height = 349
        ActivePage = tsRealizadas
        Align = alClient
        TabOrder = 0
        CarregouAba = False
        ExplicitWidth = 777
        object tsPendentes: TTabSheet
          Caption = 'Pendentes'
          OnShow = tsPendentesShow
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 769
          ExplicitHeight = 0
          object sgEntregasPendentes: TGridLuka
            Left = 0
            Top = 0
            Width = 777
            Height = 320
            Align = alClient
            ColCount = 6
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
            TabOrder = 0
            OnDblClick = sgEntregasPendentesDblClick
            OnDrawCell = sgEntregasPendentesDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 16768407
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clMenuHighlight
            HCol.Strings = (
              'Pedido'
              'Tipo mov.'
              'Data emiss'#227'o'
              'Empresa'
              'Local'
              'Previs'#227'o ent.')
            OnGetCellColor = sgEntregasPendentesGetCellColor
            Grid3D = False
            RealColCount = 15
            Indicador = True
            AtivarPopUpSelecao = False
            ExplicitWidth = 769
            ColWidths = (
              68
              99
              116
              142
              149
              124)
          end
        end
        object tsRealizadas: TTabSheet
          Caption = 'Realizadas'
          ImageIndex = 1
          OnShow = tsRealizadasShow
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 769
          ExplicitHeight = 0
          object sgEntregasRealizadas: TGridLuka
            Left = 0
            Top = 0
            Width = 777
            Height = 320
            Align = alClient
            ColCount = 8
            DefaultRowHeight = 19
            DrawingStyle = gdsGradient
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
            TabOrder = 0
            OnDblClick = sgEntregasRealizadasDblClick
            OnDrawCell = sgEntregasRealizadasDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 16768407
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clMenuHighlight
            HCol.Strings = (
              'Entrega'
              'Tipo mov.'
              'Pedido'
              'Data emiss'#227'o'
              'Data entrega'
              'Empresa'
              'Local'
              'Merc. entregue p/')
            OnGetCellColor = sgEntregasRealizadasGetCellColor
            Grid3D = False
            RealColCount = 15
            Indicador = True
            AtivarPopUpSelecao = False
            ExplicitWidth = 785
            ColWidths = (
              57
              99
              76
              113
              113
              131
              118
              145)
          end
        end
      end
    end
  end
  inline frCliente: TFrClientes
    Left = 2
    Top = -1
    Width = 311
    Height = 42
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 2
    ExplicitTop = -1
    ExplicitWidth = 311
    ExplicitHeight = 42
    inherited sgPesquisa: TGridLuka
      Width = 286
      Height = 25
      ExplicitWidth = 286
      ExplicitHeight = 25
    end
    inherited CkPesquisaNumerica: TCheckBox
      Checked = False
      State = cbUnchecked
    end
    inherited PnTitulos: TPanel
      Width = 311
      ExplicitWidth = 311
      inherited lbNomePesquisa: TLabel
        Width = 39
        Height = 14
        Caption = 'Cliente'
        ExplicitWidth = 39
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 241
        ExplicitLeft = 241
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 286
      Height = 26
      ExplicitLeft = 286
      ExplicitHeight = 26
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
end
