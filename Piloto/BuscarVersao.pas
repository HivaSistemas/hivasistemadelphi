unit BuscarVersao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, _RecordsCadastros, _Biblioteca;

type
  TFormBuscarVersao = class(TFormHerancaFinalizar)
    eVersao: TEditLuka;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosVersao(caption: string = ''): TRetornoTelaFinalizar<string>;

implementation

{$R *.dfm}

function BuscarDadosVersao(caption: string = ''): TRetornoTelaFinalizar<string>;
var
  vForm: TFormBuscarVersao;
begin
  vForm := TFormBuscarVersao.Create(Application);
  if (caption <> '') then
    vForm.Caption := caption;

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.eVersao.Text;

  FreeAndNil(vForm);
end;

end.
