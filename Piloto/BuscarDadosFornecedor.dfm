inherited FormBuscarDadosFornecedor: TFormBuscarDadosFornecedor
  Caption = 'Buscar dados do fornecedor'
  ClientHeight = 106
  ClientWidth = 388
  ExplicitWidth = 394
  ExplicitHeight = 135
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 69
    Width = 388
    inherited sbFinalizar: TSpeedButton
      Left = 129
      ExplicitLeft = 129
    end
  end
  inline FrFornecedores: TFrFornecedores
    Left = 3
    Top = 7
    Width = 380
    Height = 50
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 3
    ExplicitTop = 7
    ExplicitWidth = 380
    ExplicitHeight = 50
    inherited sgPesquisa: TGridLuka
      Width = 377
      Height = 33
      ExplicitWidth = 243
    end
    inherited PnTitulos: TPanel
      Width = 380
      ExplicitWidth = 306
      inherited lbNomePesquisa: TLabel
        Width = 74
        ExplicitWidth = 74
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 275
        ExplicitLeft = 201
        inherited ckSuprimir: TCheckBox
          Width = 61
          Visible = False
          ExplicitLeft = 40
          ExplicitWidth = 61
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 377
      Width = 3
      Height = 34
      Visible = False
      ExplicitLeft = 303
      ExplicitWidth = 3
      inherited sbPesquisa: TSpeedButton
        Left = -19
        ExplicitLeft = -21
      end
    end
  end
end
