unit PesquisaListaNegra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Sessao, _Biblioteca,
  ComboBoxLuka, EditLuka, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _ListaNegra;

type
  TFormPesquisaListaNegra = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer;  Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(pSomenteAtivos: Boolean): TObject;

implementation

{$R *.dfm}

const
  coNome   = 2;
  coAtivo  = 3;

function Pesquisar(pSomenteAtivos: Boolean): TObject;
var
  vDado: TObject;
begin
  vDado := _HerancaPesquisas.Pesquisar(TFormPesquisaListaNegra, _ListaNegra.GetFiltros, [pSomenteAtivos]);
  if vDado = nil then
    Result := nil
  else
    Result := RecListaNegra(vDado);
end;

{ TFormPesquisaListaNegra }

procedure TFormPesquisaListaNegra.BuscarRegistros;
var
  i: Integer;
  vRotas: TArray<RecListaNegra>;
begin
  inherited;

  vRotas :=
    _ListaNegra.BuscarListaNegra(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text],
      FAuxiliares[0]
    );

  if vRotas = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vRotas);

  for i := Low(vRotas) to High(vRotas) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := _Biblioteca.charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := NFormat(vRotas[i].ListaId);
    sgPesquisa.Cells[coNome, i + 1]         := vRotas[i].Nome;
    sgPesquisa.Cells[coAtivo, i + 1]        := _Biblioteca.SimNao(vRotas[i].Ativo);
  end;
  sgPesquisa.SetLinhasGridPorTamanhoVetor( Length(vRotas) );
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaListaNegra.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coSelecionado, coAtivo] then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
