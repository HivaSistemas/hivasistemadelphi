unit CadastroClientes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, System.StrUtils, _Clientes,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, RadioGroupLuka, Vcl.Mask, EditCpfCnpjLuka, _RecordsCadastros,
  EditLukaData, ComboBoxLuka, _FrameHerancaPrincipal, FrameEndereco, _Sessao, System.Math,
  Vcl.ComCtrls, _Biblioteca, EditTelefoneLuka, Vcl.Grids, GridLuka, _DiversosEnderecos,
  _FrameHenrancaPesquisas, FrameBairros, EditCEPLuka, FrameDiversosEnderecos, _DiversosContatos,
  FrameDiversosContatos, FrameCondicoesPagamento, _RecordsEspeciais, PesquisaClientes,
  FrameVendedores, _ClientesCondicPagtoRestrit, PesquisaFornecedores, Cadastros,
  Vcl.Menus, CheckBoxLuka, FrameIndicesDescontosVenda, FrameCadastrosTelefones,
  FrameListaNegra, FrameGruposClientes, StaticTextLuka, FrameArquivos,
  _ClientesFiliais;

type
  TFormCadastroClientes = class(TFormCadastros)
    cbTipoCliente: TComboBoxLuka;
    lb3: TLabel;
    tsSerasaSpc: TTabSheet;
    lbCodigo_Spc: TLabel;
    lbData_pesquisa_Financeiro: TLabel;
    lbAtendente_Financeiro: TLabel;
    lb14: TLabel;
    lbAtendente_Serasa: TLabel;
    lbData_Restricao_Spc: TLabel;
    lbLocal_Spc: TLabel;
    lbData_Restricao_Serasa: TLabel;
    lbLocal_Serasa: TLabel;
    stSPC: TStaticText;
    txtStSerasa: TStaticText;
    ckRestricaoSerasa: TCheckBox;
    ckRestricaoSPC: TCheckBox;
    eCodigoSPC: TEditLuka;
    eAtendenteSPC: TEditLuka;
    eDataPesquisaSPC: TEditLukaData;
    eDataRestricaoSPC: TEditLukaData;
    eDataPesquisaSerasa: TEditLukaData;
    eAtendenteSerasa: TEditLuka;
    eDataRestricaoSerasa: TEditLukaData;
    eLocalRestricaoSerasa: TEditLuka;
    tsParametros: TTabSheet;
    lb15: TLabel;
    lb16: TLabel;
    lb18: TLabel;
    Label3: TLabel;
    FrCondicaoPagamentoPadraoVenda: TFrCondicoesPagamento;
    FrCondicoesPagtoRestritas: TFrCondicoesPagamento;
    FrVendedorPadrao: TFrVendedores;
    ckEmitirSomenteNFE: TCheckBox;
    eLimiteCredito: TEditLuka;
    eLimiteCreditoMensal: TEditLuka;
    eDataAprovacaoLimiteCredito: TEditLukaData;
    ckNaoGerarComissao: TCheckBox;
    ckExigirNotaFaturamento: TCheckBox;
    cbTipoPreco: TComboBoxLuka;
    eDataValidadeLimiteCredito: TEditLukaData;
    lb1: TLabel;
    ckExigirModeloNotaFiscal: TCheckBox;
    eQtdeDiasVencimentoAcumulado: TEditLuka;
    lb2: TLabel;
    FrIndiceDescontoVenda: TFrIndicesDescontosVenda;
    ckIgnorarRedirRegraNotaFisc: TCheckBox;
    FrListaNegra: TFrListaNegra;
    lb5: TLabel;
    eObservacoesListaNegra: TEditLuka;
    ckIgnorarBloqueiosVendas: TCheckBoxLuka;
    FrGrupoCliente: TFrGruposClientes;
    eLocalRestricaoSPC: TEditLuka;
    lb17: TLabel;
    Label4: TLabel;
    eObservacoesVenda: TMemo;
    eUsuarioBanco: TMemo;
    eSenhaBanco: TMemo;
    eIpServidor: TMemo;
    eServicoOracle: TMemo;
    eQtdEstacoes: TMemo;
    eResponsavelEmp: TMemo;
    eValorAdesao: TMemo;
    eValorMens: TMemo;
    DadosHiva: TTabSheet;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    //eIpServidor: TMemo;
    //eServicoOracle: TMemo;
    //eSenhaBanco: TMemo;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    //eResponsavelEmp: TMemo;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    //eValorMens: TMemo;
    Label14: TLabel;
    //eValorAdesao: TMemo;
    StaticTextLuka1: TStaticTextLuka;
    StaticTextLuka2: TStaticTextLuka;
    StaticTextLuka3: TStaticTextLuka;
    //eQtdEstacoes: TMemo;
    Label15: TLabel;
    eDataAdesao: TEditLukaData;
    eDatavenc: TMemo;
    Label16: TLabel;
    ePorta: TMemo;
    ckCalcularIndiceDeducao: TCheckBox;
    FrArquivos: TFrArquivos;
    StaticTextLuka4: TStaticTextLuka;
    eDataValidade: TEditLukaData;
    Label17: TLabel;
    StaticTextLuka5: TStaticTextLuka;
    eCPF_CNPJ_Filial: TEditCPF_CNPJ_Luka;
    sgFiliais: TGridLuka;
    Label18: TLabel;
    rgRegimeEmpresa: TRadioGroup;
    ckBloquearVendas: TCheckBox;
    ckExportarWeb: TCheckBox;
    procedure ckRestricaoSerasaClick(Sender: TObject);
    procedure ckRestricaoSPCClick(Sender: TObject);
    procedure eEmailKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ckRestricaoSPCKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbPesquisarCadastrosClick(Sender: TObject);
    procedure rgTipoPessoaClick(Sender: TObject);
    procedure eNomeFantasiaEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbTipoClienteChange(Sender: TObject);
    procedure eCPF_CNPJ_FilialExit(Sender: TObject);
    procedure sgFiliaisKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    FGravandoPorTela: Boolean;
    FRespostaCadastroId: Integer;
    procedure PreencherRegistro(pCliente: RecClientes; pCondicoesPagtoRestritasIds: TArray<Integer>; pCLienteFiliais: TArray<Rec_ClientesFiliais>);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Modo(pEditando: Boolean); override;
  end;

function cadastrarNovoCliente(pCpfCnpj: string): TRetornoTelaFinalizar<Integer>;

implementation

uses
  _Autorizacoes ;

{$R *.dfm}

function cadastrarNovoCliente(pCpfCnpj: string): TRetornoTelaFinalizar<Integer>;
var
  vForm: TFormCadastroClientes;
begin
  vForm := TFormCadastroClientes.Create(nil);
  vForm.FormStyle := fsNormal;
  vForm.Visible := False;
  vForm.Modo(True);
  vForm.FGravandoPorTela := True;

  vForm.rgTipoPessoa.SetIndicePorValor( IIfStr(Length(pCpfCnpj) = 11, 'F', 'J') );
  vForm.eCPF_CNPJ.Text := _Biblioteca.FormatarCpfCnpf(pCpfCnpj);
  if not vForm.eCPF_CNPJ.getCPF_CNPJOk then begin
    _Biblioteca.Exclamar('O CPF/CNPJ � inv�lido!');
    vForm.eCPF_CNPJ.Clear;
    _Biblioteca.SetarFoco( vForm.eCPF_CNPJ );
  end
  else
    _Biblioteca.SetarFoco( vForm.eRazaoSocial );

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FRespostaCadastroId;

  vForm.Free;
end;

procedure TFormCadastroClientes.BuscarRegistro;
var
  vClientes: TArray<RecClientes>;

  i: Integer;
  vCondicoesPagtoRestritasIds: TArray<Integer>;
  vCondicoesPagtoRestritas: TArray<RecClientesCondicPagtoRestrit>;
  vClienteFilial: TArray<Rec_ClientesFiliais>;
begin
  inherited;

  vClientes := _Clientes.BuscarClientes(Sessao.getConexaoBanco, 0, [eId.AsInt], False);
  if vClientes <> nil then begin
    vCondicoesPagtoRestritas := _ClientesCondicPagtoRestrit.BuscarClientesCondicPagtoRestrit(Sessao.getConexaoBanco, 0, [eID.AsInt]);
    if vCondicoesPagtoRestritas <> nil then begin
      SetLength(vCondicoesPagtoRestritasIds, Length(vCondicoesPagtoRestritas));
      for i := Low(vCondicoesPagtoRestritas) to High(vCondicoesPagtoRestritas) do
        vCondicoesPagtoRestritasIds[i] := vCondicoesPagtoRestritas[i].CondicaoId;
    end;
    vClienteFilial := _ClientesFiliais.Buscar_ClientesFiliais(Sessao.getConexaoBanco, 0, [eID.AsInt]);
    PreencherRegistro(vClientes[0], vCondicoesPagtoRestritasIds, vClienteFilial);
  end;
end;

procedure TFormCadastroClientes.cbTipoClienteChange(Sender: TObject);
begin
  inherited;
  if (cbTipoCliente.GetValor = 'NC') and (Trim(eInscricaoEstadual.Text) = '') then
    eInscricaoEstadual.Text := 'ISENTO'
  else if (cbTipoCliente.GetValor <> 'NC') and (Trim(eInscricaoEstadual.Text) = 'ISENTO') then
    eInscricaoEstadual.Clear;
end;

procedure TFormCadastroClientes.ckRestricaoSerasaClick(Sender: TObject);
begin
  inherited;
  eDataRestricaoSerasa.Enabled := ckRestricaoSerasa.Checked;
  eLocalRestricaoSerasa.Enabled := ckRestricaoSerasa.Checked;
end;

procedure TFormCadastroClientes.ckRestricaoSPCClick(Sender: TObject);
begin
  inherited;
  eDataRestricaoSPC.Enabled := ckRestricaoSPC.Checked;
  eLocalRestricaoSPC.Enabled := ckRestricaoSPC.Checked;
end;

procedure TFormCadastroClientes.ckRestricaoSPCKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if ckRestricaoSPC.Checked then
    ProximoCampo(Sender, Key, Shift)
  else
    SetarFoco(FrCondicaoPagamentoPadraoVenda, Key);
end;

procedure TFormCadastroClientes.eCPF_CNPJ_FilialExit(Sender: TObject);
var
  vLinha: integer;
begin
  inherited;
  if Trim(eCPF_CNPJ_Filial.Text) = '' then
    Exit;

  eCPF_CNPJ_Filial.Text := RetornaNumeros(eCPF_CNPJ_Filial.Text);

  if Length(eCPF_CNPJ_Filial.Text) = 11 then
    eCPF_CNPJ_Filial.Tipo := [tccCPF]
  else
    eCPF_CNPJ_Filial.Tipo := [tccCNPJ];

  if not eCPF_CNPJ_Filial.getCPF_CNPJOk then
  begin
    _Biblioteca.Exclamar('O CPF/CNPJ � inv�lido!');
    eCPF_CNPJ_Filial.EditMask := '';
    eCPF_CNPJ_Filial.Clear;
    _Biblioteca.SetarFoco(eCPF_CNPJ_Filial);
    exit;
  end;

  vLinha := sgFiliais.Localizar([0], [eCPF_CNPJ_Filial.Text]);
  if vLinha = -1 then begin

    if sgFiliais.Cells[0,1] = '' then
      vLinha := 1
    else begin
      vLinha := sgFiliais.RowCount;
      sgFiliais.RowCount := sgFiliais.RowCount + 1;
    end;
  end else
  begin
    sgFiliais.Row := vLinha;
    _Biblioteca.Exclamar('CPF/CNPJ j� inserido!');
    eCPF_CNPJ_Filial.EditMask := '';
    eCPF_CNPJ_Filial.Clear;
    _Biblioteca.SetarFoco(eCPF_CNPJ_Filial);
    exit;
  end;

  sgFiliais.Cells[0, vLinha] := eCPF_CNPJ_Filial.Text;
  eCPF_CNPJ_Filial.EditMask := '';
  eCPF_CNPJ_Filial.Clear;
  _Biblioteca.SetarFoco(eCPF_CNPJ_Filial);
end;

procedure TFormCadastroClientes.eEmailKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  SetarFoco(FrDiversosEnderecos, Key);
end;

procedure TFormCadastroClientes.eNomeFantasiaEnter(Sender: TObject);
begin
  inherited;
  if eNomeFantasia.Text = '' then
    eNomeFantasia.Text := eRazaoSocial.Text;
end;

procedure TFormCadastroClientes.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _Clientes.ExcluirCliente(Sessao.getConexaoBanco, eId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroClientes.FormCreate(Sender: TObject);
begin
  inherited;
  tsParametros.TabVisible := Sessao.AutorizadoRotina('parametros_adicionais_cliente');
  DadosHiva.TabVisible  := Sessao.getEmpresaLogada.Cnpj = '33.433.422/0001-00';
  ckAtivo.Visible := Sessao.AutorizadoRotina('ativar_desativar_cliente');
end;



procedure TFormCadastroClientes.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;

  vCondicaoPadraoId: Integer;
  vVendedorPadraoId: Integer;
  vIndiceDescontoVendaId: Integer;
  vListaNegraId: Integer;
  vGrupoClienteId: Integer;
  vRecClienteFilial: TArray<Rec_ClientesFiliais>;
  vCnt: Integer;
  vRegimeEmpresa: string;
begin
  inherited;

  for vCnt := 1 to sgFiliais.RowCount - 1 do
  begin
    if sgFiliais.Cells[0,vCnt] = '' then
      continue;
    SetLength(vRecClienteFilial,vCnt);
    vRecClienteFilial[vCnt -1].CadastroId := 0;
    vRecClienteFilial[vCnt -1].CpfCnpj := sgFiliais.Cells[0,vCnt];
  end;

  vCondicaoPadraoId := 0;
  if not FrCondicaoPagamentoPadraoVenda.EstaVazio then
    vCondicaoPadraoId := FrCondicaoPagamentoPadraoVenda.getDados.condicao_id;

  vVendedorPadraoId := 0;
  if not FrVendedorPadrao.EstaVazio then
    vVendedorPadraoId := FrVendedorPadrao.getVendedor.funcionario_id;

  vIndiceDescontoVendaId := 0;
  if not FrIndiceDescontoVenda.EstaVazio then
    vIndiceDescontoVendaId := FrIndiceDescontoVenda.getDados().IndiceId;

  vListaNegraId := 0;
  if not FrListaNegra.EstaVazio then
    vListaNegraId := FrListaNegra.getDados().ListaId;

  vGrupoClienteId := 0;
  if not FrGrupoCliente.EstaVazio then
    vGrupoClienteId := FrGrupoCliente.getDados().ClienteGrupoId;

  vRegimeEmpresa := '';
  if rgRegimeEmpresa.ItemIndex <> -1 then begin
    case rgRegimeEmpresa.ItemIndex of
      0: vRegimeEmpresa := 'SN';
      1: vRegimeEmpresa := 'LR';
      2: vRegimeEmpresa := 'LP';
    end;
  end;

  vRetBanco :=
    _Clientes.AtualizarCliente(
      Sessao.getConexaoBanco,
      FPesquisouCadastro,
      eID.AsInt,
      FCadastro,
      FTelefones,
      FEnderecos,
      cbTipoCliente.GetValor,
      eCodigoSPC.Text,
      eDataPesquisaSPC.AsData,
      eLocalRestricaoSPC.Text,
      eDataPesquisaSerasa.AsData,
      eLocalRestricaoSerasa.Text,
      eAtendenteSPC.Text,
      _Biblioteca.ToChar(ckRestricaoSPC),
      eDataRestricaoSPC.AsData,
      eAtendenteSerasa.Text,
      _Biblioteca.ToChar(ckRestricaoSerasa),
      eDataRestricaoSerasa.AsData,
      vCondicaoPadraoId,
      vVendedorPadraoId,
      _Biblioteca.ToChar(ckEmitirSomenteNFE),
      _Biblioteca.ToChar(ckExigirModeloNotaFiscal),
      FrCondicoesPagtoRestritas.GetArrayOfInteger,
      eLimiteCredito.AsCurr,
      eLimiteCreditoMensal.AsCurr,
      eDataAprovacaoLimiteCredito.AsData,
      ToChar(ckNaoGerarComissao),
      _Biblioteca.ToChar(ckAtivo),
      ToChar(ckExigirNotaFaturamento),
      cbTipoPreco.GetValor,
      eDataValidadeLimiteCredito.AsData,
      eQtdeDiasVencimentoAcumulado.AsInt,
      ToChar(ckIgnorarRedirRegraNotaFisc.Checked),
      vIndiceDescontoVendaId,
      vListaNegraId,
      eObservacoesListaNegra.Text,
      ckIgnorarBloqueiosVendas.CheckedStr,
      vGrupoClienteId,
      eObservacoesVenda.Lines.Text,
      eUsuarioBanco.lines.Text,
      eSenhaBanco.lines.Text,
      eIpServidor.lines.Text,
      eServicoOracle.lines.Text,
      eQtdEstacoes.lines.Text,
      eResponsavelEmp.lines.Text,
      eValorAdesao.lines.Text,
      eValorMens.lines.Text,
      eDataAdesao.AsData,
      eDataVenc.lines.Text,
      ePorta.lines.Text,
      ToChar(ckCalcularIndiceDeducao),
      Sessao.getCriptografia.Criptografar(eDataValidade.Text,False),
      FrArquivos.Arquivos,
      Sessao.getUsuarioLogado.cadastro_id,
      vRecClienteFilial,
      vRegimeEmpresa,
      toChar(ckBloquearVendas),
      ToChar(ckExportarWeb)
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  if vRetBanco.AsInt > 0 then begin
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + IntToStr(vRetBanco.AsInt));

    if FGravandoPorTela then begin
      FRespostaCadastroId := vRetBanco.AsInt;
      ModalResult := mrOk;
    end;
  end
  else
    _Biblioteca.Informar(coAlteracaoRegistroSucesso);
end;

procedure TFormCadastroClientes.Modo(pEditando: Boolean);
begin
  inherited;

  FGravandoPorTela := False;

  _Biblioteca.Habilitar([
    cbTipoCliente,
    FrGrupoCliente,

    (* Aba Serasa/SPC*)
    eDataPesquisaSerasa,
    eAtendenteSerasa,
    ckRestricaoSerasa,
    eDataRestricaoSerasa,
    eLocalRestricaoSerasa,

    eCodigoSPC,
    eDataPesquisaSPC,
    eAtendenteSPC,
    ckRestricaoSPC,
    eDataRestricaoSPC,
    eLocalRestricaoSPC,
    eObservacoesVenda,
    eUsuarioBanco,
    eSenhaBanco,
    eIpServidor,
    eServicoOracle,
    eQtdEstacoes,
    eResponsavelEmp,
    eValorAdesao,
    eValorMens,
    eDataAdesao,
    eDataVenc,
    ePorta,
    FrArquivos,

    (* Aba Par�metros do cliente *)
    FrCondicaoPagamentoPadraoVenda,
    FrVendedorPadrao,
    FrCondicoesPagtoRestritas,
    ckEmitirSomenteNFE,
    ckExigirModeloNotaFiscal,
    ckNaoGerarComissao,
    eLimiteCredito,
    eLimiteCreditoMensal,
    eDataAprovacaoLimiteCredito,
    ckExigirNotaFaturamento,
    ckExportarWeb,
    ckBloquearVendas,
    cbTipoPreco,
    eDataValidadeLimiteCredito,
    eQtdeDiasVencimentoAcumulado,
    ckIgnorarRedirRegraNotaFisc,
    FrIndiceDescontoVenda,
    eObservacoesListaNegra,
    FrListaNegra,
    ckCalcularIndiceDeducao,
    ckIgnorarBloqueiosVendas,
    eDataValidade,
    eCPF_CNPJ_Filial,
    sgFiliais],
    pEditando
  );

  ckRestricaoSerasaClick(nil);
  ckRestricaoSPCClick(nil);

  if pEditando then begin
    cbTipoPreco.SetIndicePorValor('V');
    eQtdeDiasVencimentoAcumulado.AsInt := 15;
  end;
end;

procedure TFormCadastroClientes.PreencherRegistro(pCliente: RecClientes; pCondicoesPagtoRestritasIds: TArray<Integer>; pCLienteFiliais: TArray<Rec_ClientesFiliais>);
var
  i: Integer;
  vDataValidade: TDateTime;
begin
  cbTipoCliente.SetIndicePorValor( pCliente.tipo_cliente );

  eDataPesquisaSPC.AsData := pCliente.data_pesquisa_spc;
  eDataPesquisaSerasa.AsData := pCliente.data_pesquisa_serasa;
  eAtendenteSPC.Text := pCliente.atendente_spc;
  eAtendenteSerasa.Text := pCliente.atendente_serasa;
  eCodigoSPC.Text := pCliente.codigo_spc;
  ckRestricaoSPC.Checked := (pCliente.restricao_spc = 'S');
  ckRestricaoSerasa.Checked := (pCliente.restricao_serasa = 'S');
  eDataRestricaoSPC.AsData := pCliente.data_restricao_spc;
  eDataRestricaoSerasa.AsData := pCliente.data_restricao_serasa;

  FrGrupoCliente.InserirDadoPorChave(pCliente.GrupoClienteId, False);
  FrCondicaoPagamentoPadraoVenda.InserirDadoPorChave(pCliente.condicao_id, False);
  FrVendedorPadrao.InserirDadoPorChave(pCliente.vendedor_id, False);

  eObservacoesVenda.Lines.Text      := pCliente.ObservacoesVenda;
  eUsuarioBanco.Text                := pCliente.UsuarioBanco;
  eSenhaBanco.Text                  := pCliente.SenhaBanco;
  eIpServidor.Text                  := pCliente.IpServidor;
  eServicoOracle.Text               := pCliente.ServicoOracle;
  eQtdEstacoes.Text                 := pCliente.QtdEstacoes;
  eResponsavelEmp.Text              := pCliente.ResponsavelEmp;
  eValorAdesao.Text                 := pCliente.ValorAdesao;
  eValorMens.Text                   := pCliente.ValorMens;
  eDataAdesao.AsData                := pCliente.DataAdesao;
  eDataVenc.Text                    := pCliente.DataVenc;
  ePorta.Text                       := pCliente.Porta;

  eLimiteCredito.AsCurr              := pCliente.LimiteCredito;
  eLimiteCreditoMensal.AsCurr        := pCliente.LimiteCreditoMensal;
  eDataAprovacaoLimiteCredito.AsData := pCliente.DataAprovacaoLimiteCredito;
  ckEmitirSomenteNFE.Checked         := (pCliente.emitir_somente_nfe = 'S');
  ckExigirModeloNotaFiscal.Checked   := pCliente.ExigirModeloNotaFiscal = 'S';
  ckNaoGerarComissao.Checked         := (pCliente.NaoGerarComissao = 'S');
  ckExigirNotaFaturamento.Checked    := (pCliente.ExigirNotaFaturamento = 'S');
  ckExportarWeb.Checked              := (pCliente.ExportarWeb = 'S');
  ckBloquearVendas.Checked           := (pCliente.BloquearVendas = 'S');
  ckCalcularIndiceDeducao.Checked    := (pCliente.CalcularIndiceDeducao = 'S');
  cbTipoPreco.SetIndicePorValor(pCliente.TipoPreco);
  eDataValidadeLimiteCredito.AsData  := pCliente.DataValidadeLimiteCredito;
  eQtdeDiasVencimentoAcumulado.AsInt := pCliente.QtdeDiasVencimentoAcumulado;
  ckIgnorarRedirRegraNotaFisc.Checked := pCliente.IgnorarRedirRegraNotaFisc = 'S';
  ckAtivo.Checked                    := (pCliente.ativo = 'S');
  FrListaNegra.InserirDadoPorChave(pCliente.ListaNegraId, False);
  eObservacoesListaNegra.Text        := pCliente.ObservacoesListaNegra;
  ckIgnorarBloqueiosVendas.CheckedStr := pCliente.IgnorarBloqueiosVenda;

  if pCliente.RegimeEmpresa = '' then
    rgRegimeEmpresa.ItemIndex := -1
  else begin
    if pCliente.RegimeEmpresa = 'SN' then
      rgRegimeEmpresa.ItemIndex := 0
    else if pCliente.RegimeEmpresa = 'LR' then
      rgRegimeEmpresa.ItemIndex := 1
    else if pCliente.RegimeEmpresa = 'LP' then
      rgRegimeEmpresa.ItemIndex := 2;

  end;

  if not pCliente.Validade.IsEmpty then
    vDataValidade := StrToDateDef(Sessao.getCriptografia.Descriptografar(pCliente.Validade,False),StrToDate('31/12/1899'));
  eDataValidade.AsData := vDataValidade;

  for i := 0 to Length(pCondicoesPagtoRestritasIds) - 1 do
    FrCondicoesPagtoRestritas.InserirDadoPorChave(pCondicoesPagtoRestritasIds[i]);

  if length(pCLienteFiliais) > 0 then
  begin
    sgFiliais.RowCount := length(pCLienteFiliais) + 1;
    for i := 0 to Length(pCLienteFiliais) - 1 do
      sgFiliais.Cells[0, i+1] := pCLienteFiliais[i].CpfCnpj;

  end;

//  FrCondicoesPagtoRestritas.InserirDadoPorChaveTodos(pCondicoesPagtoRestritasIds, False);
  FrIndiceDescontoVenda.InserirDadoPorChave( pCliente.IndiceDescontoVendaId, False );

  FrArquivos.Origem := 'CLI';
  FrArquivos.Id     := pCliente.cadastro_id;
  FrArquivos.buscarArquivos;

  pCliente.Free;
end;

procedure TFormCadastroClientes.rgTipoPessoaClick(Sender: TObject);
begin
  inherited;
  cbTipoCliente.Clear;
  cbTipoCliente.Valores.Clear;
  if rgTipoPessoa.GetValor = 'F' then begin
    cbTipoCliente.Items.Add('N�o contribuinte');
    cbTipoCliente.Items.Add('Produtor rural');

    cbTipoCliente.Valores.Add('NC');
    cbTipoCliente.Valores.Add('PR');
  end
  else begin
    cbTipoCliente.Items.Add('N�o contribuinte');
    cbTipoCliente.Items.Add('Contribuinte');
    cbTipoCliente.Items.Add('Org�o p�blico');
    cbTipoCliente.Items.Add('Revenda');
    cbTipoCliente.Items.Add('Construtora');
    cbTipoCliente.Items.Add('Cl�nica / Hospital');
    cbTipoCliente.Items.Add('Produtor rural');

    cbTipoCliente.Valores.Add('NC');
    cbTipoCliente.Valores.Add('CO');
    cbTipoCliente.Valores.Add('OP');
    cbTipoCliente.Valores.Add('RE');
    cbTipoCliente.Valores.Add('CT');
    cbTipoCliente.Valores.Add('CH');
    cbTipoCliente.Valores.Add('PR');
  end;
end;

procedure TFormCadastroClientes.sbPesquisarCadastrosClick(Sender: TObject);
begin
  inherited;
  BuscarRegistro;
end;

procedure TFormCadastroClientes.sgFiliaisKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
    sgFiliais.DeleteRow(sgFiliais.Row);
end;

procedure TFormCadastroClientes.VerificarRegistro(Sender: TObject);
var
  i: Integer;
  j: Integer;
  tamanho: Integer;
  camposObrigatorios: TArray<RecCamposObrigatoriosClientes>;
begin
  inherited;

  if cbTipoCliente.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('� necess�rio informar o tipo de cliente!');
    SetarFoco(cbTipoCliente);
    Abort;
  end;

  if cbTipoPreco.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('� necess�rio informar o tipo de pre�o a ser utilizado pelo cliente!');
    SetarFoco(cbTipoPreco);
    Abort;
  end;

  if eDataValidadeLimiteCredito.DataOk and (eLimiteCredito.AsCurr + eLimiteCreditoMensal.AsCurr = 0) then begin
    _Biblioteca.Exclamar('� necess�rio informar o valor do limite de cr�dito quando se informa a data de validade do limite!');
    SetarFoco(eLimiteCredito);
    Abort;
  end;

  if (not FrListaNegra.EstaVazio) and (eObservacoesListaNegra.Trim = '') then begin
    _Biblioteca.Exclamar('� necess�rio informar a observa��o da lista negra!');
    SetarFoco(FrListaNegra);
    Abort;
  end;

  if FrListaNegra.EstaVazio and (eObservacoesListaNegra.Trim <> '') then begin
    _Biblioteca.Exclamar('N�o � permitido informar observa��o da lista negra quando o cliente n�o se encontrar em nenhuma lista!');
    SetarFoco(eObservacoesListaNegra);
    Abort;
  end;

  camposObrigatorios := _Autorizacoes.BuscarCamposClienteObrigatorios(Sessao.getConexaoBanco);
  for j := Low(camposObrigatorios) to High(camposObrigatorios) do begin
    for i := 0 to Self.ComponentCount - 1 do begin

      if Self.Components[i].Name = camposObrigatorios[j].nome then begin
        if (Self.Components[i].ClassName = TEditLuka.ClassName) then begin
          if TEditLuka(Self.Components[i]).Text = '' then begin
            if (camposObrigatorios[j].tipo_cliente = 'A') then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TEditLuka(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'F') and (eCPF_CNPJ.Tipo = [tccCPF]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TEditLuka(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'J') and (eCPF_CNPJ.Tipo = [tccCNPJ]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TEditLuka(Self.Components[i]));
              Abort;
            end;
          end;
        end
        else if Self.Components[i].ClassName = TMaskEdit.ClassName then begin
          if _Biblioteca.RetornaNumeros(TMaskEdit(Self.Components[i]).Text) = '' then begin
            if (camposObrigatorios[j].tipo_cliente = 'A') then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TMaskEdit(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'F') and (eCPF_CNPJ.Tipo = [tccCPF]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TMaskEdit(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'J') and (eCPF_CNPJ.Tipo = [tccCNPJ]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TMaskEdit(Self.Components[i]));
              Abort;
            end;
          end;
        end
        else if Self.Components[i].ClassName = TEditLukaData.ClassName then begin
          if not TEditLukaData(Self.Components[i]).DataOk then begin
            if (camposObrigatorios[j].tipo_cliente = 'A') then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TEditLukaData(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'F') and (eCPF_CNPJ.Tipo = [tccCPF]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TEditLukaData(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'J') and (eCPF_CNPJ.Tipo = [tccCNPJ]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TEditLukaData(Self.Components[i]));
              Abort;
            end;
          end;
        end
        else if Self.Components[i].ClassName = TComboBoxLuka.ClassName then begin
          if TComboBoxLuka(Self.Components[i]).GetValor = '' then begin
            if (camposObrigatorios[j].tipo_cliente = 'A') then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TComboBoxLuka(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'F') and (eCPF_CNPJ.Tipo = [tccCPF]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TComboBoxLuka(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'J') and (eCPF_CNPJ.Tipo = [tccCNPJ]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TComboBoxLuka(Self.Components[i]));
              Abort;
            end;
          end;
        end
        else if Self.Components[i].ClassName = TEditTelefoneLuka.ClassName then begin
          if not TEditTelefoneLuka(Self.Components[i]).getTelefoneOk then begin
            if (camposObrigatorios[j].tipo_cliente = 'A') then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TEditTelefoneLuka(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'F') and (eCPF_CNPJ.Tipo = [tccCPF]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TEditTelefoneLuka(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'J') and (eCPF_CNPJ.Tipo = [tccCNPJ]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TEditTelefoneLuka(Self.Components[i]));
              Abort;
            end;
          end;
        end
        else if Self.Components[i].ClassName = TFrGruposClientes.ClassName then begin
          if TFrGruposClientes(Self.Components[i]).EstaVazio then begin
            if (camposObrigatorios[j].tipo_cliente = 'A') then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TFrGruposClientes(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'F') and (eCPF_CNPJ.Tipo = [tccCPF]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TFrGruposClientes(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'J') and (eCPF_CNPJ.Tipo = [tccCNPJ]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TFrGruposClientes(Self.Components[i]));
              Abort;
            end;
          end;
        end
        else if Self.Components[i].ClassName = TFrVendedores.ClassName then begin
          if TFrVendedores(Self.Components[i]).EstaVazio then begin
            if (camposObrigatorios[j].tipo_cliente = 'A') then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TFrVendedores(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'F') and (eCPF_CNPJ.Tipo = [tccCPF]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TFrVendedores(Self.Components[i]));
              Abort;
            end;

            if (camposObrigatorios[j].tipo_cliente = 'J') and (eCPF_CNPJ.Tipo = [tccCNPJ]) then begin
              _Biblioteca.Exclamar('O campo "' + camposObrigatorios[j].descricao + '" n�o foi preenchido corretamente');
              SetarFoco(TFrVendedores(Self.Components[i]));
              Abort;
            end;
          end;
        end;

        //ShowMessage(TEditLuka(Self.Components[i]).Text);
      end;

    end;
  end;

//  ShowMessage('validou corretamente');

end;

end.
