inherited FrCodigoBarrasAuxiliar: TFrCodigoBarrasAuxiliar
  inherited sgValores: TGridLuka
    ColCount = 1
    OnDrawCell = sgValoresDrawCell
    OnKeyPress = sgValoresKeyPress
    HCol.Strings = (
      'C'#243'digo barras')
    ColWidths = (
      236)
  end
  inherited StaticTextLuka1: TStaticTextLuka
    Caption = 'C'#243'digo de barras auxiliar'
  end
end
