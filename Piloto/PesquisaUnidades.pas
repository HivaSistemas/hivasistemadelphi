unit PesquisaUnidades;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Sessao, _Biblioteca,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, System.Math, _RecordsCadastros, _Unidades,
  Vcl.ExtCtrls;

type
  TFormPesquisaUnidades = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarUnidade: TObject;

implementation

{$R *.dfm}

{ TFormPesquisaUnidades }

const
  cp_nome  = 2;
  cp_ativo = 3;

function PesquisarUnidade: TObject;
var
  r: TObject;
begin
  r := _HerancaPesquisas.Pesquisar(TFormPesquisaUnidades, _Unidades.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := RecUnidades(r);
end;

procedure TFormPesquisaUnidades.BuscarRegistros;
var
  i: Integer;
  unidades: TArray<RecUnidades>;
begin
  inherited;

  unidades :=
    _Unidades.BuscarUnidades(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if unidades = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(unidades);

  for i := Low(unidades) to High(unidades) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1] := unidades[i].unidade_id;
    sgPesquisa.Cells[cp_nome, i + 1] := unidades[i].descricao;
    sgPesquisa.Cells[cp_ativo, i + 1] := unidades[i].ativo;
  end;

  sgPesquisa.RowCount := IfThen(Length(unidades) = 1, 2, High(unidades) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaUnidades.sgPesquisaDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;

  if ACol = cp_ativo then
    alinhamento := taCenter
  else
    alinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

end.
