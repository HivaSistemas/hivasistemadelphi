unit Frame.EnderecoEstoqueRua;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _Sessao, _Biblioteca,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _EnderecoEstoqueRua, Pesquisa.EnderecoEstoqueRua,
  _RecordsCadastros, System.Math, Vcl.Buttons, Vcl.Menus;

type
  TFrEnderecoEstoqueRua = class(TFrameHenrancaPesquisas)
  public
    function GetEnderecoEstoqueRua(pLinha: Integer = -1): RecEnderecoEstoqueRua;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrameEnderecoEstoqueRua }

function TFrEnderecoEstoqueRua.AdicionarDireto: TObject;
var
  vMotivos: TArray<RecEnderecoEstoqueRua>;
begin
  vMotivos := _EnderecoEstoqueRua.BuscarEnderecoEstoqueRua(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vMotivos = nil then
    Result := nil
  else
    Result := vMotivos[0];
end;

function TFrEnderecoEstoqueRua.AdicionarPesquisando: TObject;
begin
  Result := Pesquisa.EnderecoEstoqueRua.Pesquisar();
end;

function TFrEnderecoEstoqueRua.GetEnderecoEstoqueRua(pLinha: Integer): RecEnderecoEstoqueRua;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecEnderecoEstoqueRua(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrEnderecoEstoqueRua.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecEnderecoEstoqueRua(FDados[i]).Rua_id = RecEnderecoEstoqueRua(pSender).Rua_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrEnderecoEstoqueRua.MontarGrid;
var
  i: Integer;
  vSender: RecEnderecoEstoqueRua;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecEnderecoEstoqueRua(FDados[i]);
      AAdd([IntToStr(vSender.Rua_id), vSender.descricao]);
    end;
  end;
end;

end.
