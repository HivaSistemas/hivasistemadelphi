inherited FormRelacaoNotasFiscaisPreEntradas: TFormRelacaoNotasFiscaisPreEntradas
  Caption = 'Rela'#231#227'o de notas fiscais para pr'#233'-entrada'
  ClientHeight = 485
  ClientWidth = 994
  ExplicitWidth = 1000
  ExplicitHeight = 514
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Height = 485
    ExplicitHeight = 596
  end
  object pnNotasFiscais: TPanel
    Left = 122
    Top = 0
    Width = 872
    Height = 485
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 596
    object sgNotasFiscais: TGridLuka
      Left = 0
      Top = 0
      Width = 872
      Height = 485
      Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
      Align = alClient
      DefaultRowHeight = 19
      DrawingStyle = gdsGradient
      FixedColor = 15395562
      FixedCols = 0
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
      PopupMenu = pmOperacoesManifesto
      TabOrder = 0
      OnDrawCell = sgNotasFiscaisDrawCell
      IncrementExpandCol = 0
      IncrementExpandRow = 0
      CorLinhaFoco = 16768407
      CorFundoFoco = 16774625
      CorLinhaDesfoque = 14869218
      CorFundoDesfoque = 16382457
      CorSuperiorCabecalho = clWhite
      CorInferiorCabecalho = 13553358
      CorSeparadorLinhas = 12040119
      CorColunaFoco = clMenuHighlight
      HCol.Strings = (
        'CNPJ emitente'
        'Raz'#227'o social emitente'
        'Data emiss'#227'o'
        'Valor total'
        'Chave da NFe')
      Grid3D = False
      RealColCount = 5
      AtivarPopUpSelecao = False
      ExplicitLeft = -16
      ExplicitTop = -8
      ExplicitHeight = 596
      ColWidths = (
        128
        244
        81
        87
        312)
    end
  end
  object pmOperacoesManifesto: TPopupMenu
    Left = 894
    Top = 405
    object miManifestarCienciaOperacao: TMenuItem
      Caption = 'Manifestar ci'#234'ncia da opera'#231#227'o'
    end
    object miConfirmacaoOperacao: TMenuItem
      Caption = 'Confirma'#231#227'o da opera'#231#227'o'
    end
    object miDesconhecimentoOperacao: TMenuItem
      Caption = 'Desconhecimento da opera'#231#227'o'
    end
    object miOperacaoNaoRealizada: TMenuItem
      Caption = 'Opera'#231#227'o n'#227'o realizada'
    end
  end
end
