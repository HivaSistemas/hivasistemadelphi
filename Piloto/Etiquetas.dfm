inherited FormEtiquetas: TFormEtiquetas
  Caption = 'Etiquetas'
  ExplicitTop = -164
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    object sbGravar: TSpeedButton [2]
      Left = 0
      Top = 256
      Width = 117
      Height = 35
      Hint = 'Gravar ou atualizar configura'#231#245'es da tela'
      Caption = 'Salvar Filtros'
      Flat = True
      Glyph.Data = {
        36180000424D3618000000000000360000002800000040000000200000000100
        18000000000000180000600F0000600F00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFF1F1F14040400000000000000000000000000000000000
        000000000000001313134D4D4D25252500000000000000000000000000000000
        00000000000000001D1D1DCACACAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFF3F3F3C7C7C8BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFC8C8C9D2D2D3C4C4C4BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFCECECEFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF9494940000000000000000000000000000000000000000
        00000000333333EEEEEEFFFFFFFDFDFD6C6C6C00000000000000000000000000
        0000000000000000000000474747FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFD3D3D3BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFDADADAFEFEFEFFFFFFFBFBFBCCCCCCBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF6767670000000000000000000000000000000000000000
        00171717E4E4E4E6F5FB47B3E4BDE4F5FCFCFC42424200000000000000000000
        00000000000000000000001C1C1CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFC8C8C8BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFCFCFD0FEFEFEEFEFEFD1D1D1F9F9F9F8F8F8C5C5C5BFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFD7D7D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF4242420000000000000000000000000000000000000404
        04C0C0C0F9FDFE3BAEE30095DA129CDDDFF2FAEAEAEA1E1E1E00000000000000
        0000000000000000000000010101F8F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFEFEFEC0C0C0BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFC7C7
        C7FAFAFAF7F7F7C3C3C4BFBFBFCECECEFEFEFEEFEFEFC0C0C1BFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFCECECEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF1E1E1E0000000000000000000000000000000000008B8B
        8BFFFFFF6BC1EA0095DA0095DA0095DA30A9E1F6FBFECACACA03030300000000
        0000000000000000000000000000D4D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF6F6F6BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFC0C0C0F2F2
        F2FCFCFDCBCBCBBFBFBFBFBFBFBFBFBFDADADAFFFFFFE2E2E2BFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFC5C5C5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF8F8F8010101000000000000000000000000000000080808FAFA
        FAAEDDF30095DA0095DA0095DA0095DA0095DA6CC2EAFFFFFF49494900000000
        0000000000000000000000000000B0B0B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFECECECBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFD1D1D1FFFF
        FFDADADABFBFBFBFBFBFBFBFBFBFBFBFBFBFBFEBEBEBFEFEFEC1C1C1BFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFFCFCFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFD3D3D3000000000000000000000000000000000000060606F6F6
        F6C4E6F650B6E631A9E10095DA1DA1DE4FB6E596D3F0FFFFFF41414100000000
        00000000000000000000000000008C8C8CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE3E3E4BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFCFCFD0FFFF
        FFE5E5E5D3D3D3C7C7C7BFBFBFCCCCCCD3D3D3F0F0F0FCFCFDC0C0C1BFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFF3F3F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFAEAEAE0000000000000000000000000000000000000000006D6D
        6DFBFBFBFFFFFF9FD7F10095DA5FBCE8FFFFFFFFFFFFAAAAAA00000000000000
        0000000000000000000000000000696969FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFDADADABFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFEAEA
        EAFFFFFFFFFFFFD7D7D7BFBFBFE7E7E7FFFFFFFEFEFEDBDBDBBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFEAEAEAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF8A8A8A0000000000000000000000000000000000000000000000
        001B1B1BFFFFFF9FD7F10095DA5FBCE8FFFFFF5D5D5D00000000000000000000
        0000000000000000000000000000444444FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFD1D1D1BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFD6D6D6FFFFFFD7D7D7BFBFBFE7E7E7FFFFFFC6C6C6BFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF6464640000000000000000000000000000000000000000000000
        00000000FFFFFF9FD7F10095DA5FBCE8FFFFFF3F3F3F00000000000000000000
        0000000000000000000000000000202020FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFC8C8C8BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFCFCFCFFFFFFFD7D7D7BFBFBFE7E7E7FFFFFFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFD8D8D8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF4040400000000000000000000000000000000000000000000000
        00000000DEDEDED1ECF81BA0DE99D5F0FFFFFF22222200000000000000000000
        0000000000000000000000000000020202F9F9F9FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFEFEFEC0C0C0BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFC8C8C8FFFFFFE5E5E5C6C6C6F3F3F3F7F7F7BFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFCFCFD0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF1C1C1C0000000000000000000000000000000000000000000000
        000000005F5F5FFEFEFEFFFFFFFFFFFFA3A3A300000000000000000000000000
        0000000000000000000000000000000000D9D9D9FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFCFCFCBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFE7E7E7FFFFFFFFFFFFFFFFFFD7D7D8BFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFC6C6C6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF0202020000000000000000000000000000000000000000000000
        0000000000000034343477777751515101010100000000000000000000000000
        0000000000000000000000000000000000BFBFBFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFAFAFABFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFD3D3D3DDDDDDCDCDCDBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFC4C4C4AFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAF
        AFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAFAF
        AFAFAFAFAFAFAFAFAFAFAFAFAFAFB3B3B3FBFBFBFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFECECECEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB
        EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB
        EBEBEBEBEBEBEBEBEBEBEBEBEBEBF0F0F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF4B4B4B3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F
        3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F
        3F3F3F3F3F3F3F3F3F3F3FB7B7B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFF5F5F5CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF
        CFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF
        CFCFCFCFCFCFCFCFCFCFCFF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000009F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFEFEFEFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFF7F7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFF6F6F60000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000C6C6C6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFECECECBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFC3C3C3FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFEBEBEB0000000000000000000000000000000000000000
        000000000000000000001313134A4A4A4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F
        4F4F4F4F4F565656A5A5A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFE9E9E9BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFBFBFBFC8C8C9D2D2D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3D3
        D3D3D3D3D3D7D7D7F1F1F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFDFDFDF0000000000000000000000000000000000000000
        00000000000000525252F5F5F5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFE7E7E7BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFBFBFBFE2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFEBEBEB0000000000000000000000000000000000000000
        000000001C1C1CF3F3F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFE9E9E9BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFBFBFBFD4D4D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF8383830808080000000000000000000000000000
        00161616BFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFDFDFDD2D2D2C0C0C0BFBFBFBFBFBFBFBFBFBFBFBFBFBF
        BFCCCCCCF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
      OnClick = sbGravarClick
    end
  end
  inherited pcDados: TPageControl
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 6
      ExplicitTop = 29
      ExplicitWidth = 884
      ExplicitHeight = 518
      object Label1: TLabel
        Left = 368
        Top = 43
        Width = 169
        Height = 14
        Caption = 'Tipo de c'#243'digo para impress'#227'o'
      end
      object Label2: TLabel
        Left = 368
        Top = 1
        Width = 153
        Height = 14
        Caption = 'Caminho impressora Zebra.:'
      end
      object imImagemEtiqueta: TImage
        Left = 368
        Top = 325
        Width = 513
        Height = 188
      end
      object Label3: TLabel
        Left = 368
        Top = 85
        Width = 163
        Height = 14
        Caption = 'Tipo de pre'#231'o para impress'#227'o'
      end
      inline FrDataCadastroProduto: TFrDataInicialFinal
        Left = 630
        Top = 8
        Width = 217
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 6
        TabStop = True
        ExplicitLeft = 630
        ExplicitTop = 8
        ExplicitWidth = 217
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 156
          Height = 14
          Caption = 'Data de cadastro do produto'
          ExplicitWidth = 156
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDepartamentosSecoesLinhas: TFrameDepartamentosSecoesLinhas
        Left = 0
        Top = 268
        Width = 320
        Height = 80
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitTop = 268
        ExplicitWidth = 320
        ExplicitHeight = 80
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 63
          ExplicitWidth = 295
          ExplicitHeight = 63
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 102
            Caption = 'Grupo de produtos'
            ExplicitWidth = 102
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 64
          ExplicitLeft = 295
          ExplicitHeight = 64
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited rgNivel: TRadioGroup
          Left = 111
          Top = 47
          ItemIndex = 3
          ExplicitLeft = 111
          ExplicitTop = 47
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 0
        Top = 94
        Width = 320
        Height = 80
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitTop = 94
        ExplicitWidth = 320
        ExplicitHeight = 80
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 63
          ExplicitWidth = 295
          ExplicitHeight = 63
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 64
          ExplicitLeft = 295
          ExplicitHeight = 64
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrMarcas: TFrMarcas
        Left = 0
        Top = 181
        Width = 320
        Height = 80
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitTop = 181
        ExplicitWidth = 320
        ExplicitHeight = 80
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 63
          ExplicitWidth = 295
          ExplicitHeight = 63
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 39
            ExplicitWidth = 39
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 64
          ExplicitLeft = 295
          ExplicitHeight = 64
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrFornecedores: TFrFornecedores
        Left = 0
        Top = 354
        Width = 320
        Height = 80
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitTop = 354
        ExplicitWidth = 320
        ExplicitHeight = 80
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 63
          ExplicitWidth = 295
          ExplicitHeight = 63
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 74
            ExplicitWidth = 74
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 64
          ExplicitLeft = 295
          ExplicitHeight = 64
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrEmpresas: TFrEmpresas
        Left = 0
        Top = 0
        Width = 320
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitWidth = 320
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 24
          ExplicitWidth = 295
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 25
          ExplicitLeft = 295
          ExplicitHeight = 25
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrCondicoesPagamento: TFrCondicoesPagamento
        Left = 0
        Top = 47
        Width = 320
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        TabStop = True
        ExplicitTop = 47
        ExplicitWidth = 320
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 24
          ExplicitWidth = 295
          ExplicitHeight = 24
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 132
            Caption = 'Condi'#231#227'o de pagamento'
            ExplicitWidth = 132
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 25
          ExplicitLeft = 295
          ExplicitHeight = 25
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      object rgModelosEtiquetas: TRadioGroupLuka
        AlignWithMargins = True
        Left = 368
        Top = 155
        Width = 257
        Height = 164
        Caption = 'Modelos de etiquetas:'
        ItemIndex = 0
        Items.Strings = (
          'Modelo 01 - H 10 cm x L 05 cm'
          'Modelo 02 - H 3.3 cm x L 2.2 cm / 3 CL'
          'Modelo 03 - H 7,5 cm x L 3,5 cm'
          'Modelo 04 - H 10,4 cm x L 3,00 cm'
          'Modelo 05 - H 3 cm x L 5 cm / 2 CL'
          'Modelo 06 - H 15 cm x L 10 cm (Em teste)'
          'Modelo 07 - A4 (Promo'#231#227'o)'
          'Modelo 08 - H 7,5 cm x L 3,5 cm (2 pre'#231'os)')
        TabOrder = 9
        OnClick = rgModelosEtiquetasClick
      end
      object cbTipoCodigoImpressao: TComboBoxLuka
        Left = 368
        Top = 59
        Width = 215
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        TabOrder = 7
        Items.Strings = (
          'C'#243'digo de barras'
          'C'#243'digo do produto (Hiva)')
        Valores.Strings = (
          'F'
          'A')
        AsInt = 0
      end
      object eCaminhoImpressao: TEditLuka
        Left = 368
        Top = 19
        Width = 215
        Height = 22
        CharCase = ecUpperCase
        MaxLength = 100
        TabOrder = 8
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      inline frDataPromocao: TFrDataInicialFinal
        Left = 630
        Top = 104
        Width = 217
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 10
        TabStop = True
        ExplicitLeft = 630
        ExplicitTop = 104
        ExplicitWidth = 217
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 100
          Height = 14
          Caption = 'Data de promo'#231#227'o'
          ExplicitWidth = 100
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object ckbImprimirPrecoVenda: TCheckBox
        Left = 368
        Top = 129
        Width = 197
        Height = 17
        Caption = 'Imprimir Pre'#231'o de Venda'
        Checked = True
        State = cbChecked
        TabOrder = 11
        Visible = False
      end
      inline FrCodigoEntrada: TFrNumeros
        Left = 630
        Top = 156
        Width = 118
        Height = 72
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 12
        TabStop = True
        ExplicitLeft = 630
        ExplicitTop = 156
        ExplicitWidth = 118
        inherited sgNumeros: TGridLuka
          Width = 118
          ExplicitWidth = 118
        end
        inherited pnDescricao: TPanel
          Width = 118
          Caption = 'C'#243'digo da entrada'
          ExplicitWidth = 118
        end
      end
      inline FrNumeroNota: TFrNumeros
        Left = 758
        Top = 156
        Width = 117
        Height = 72
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 13
        TabStop = True
        ExplicitLeft = 758
        ExplicitTop = 156
        ExplicitWidth = 117
        inherited sgNumeros: TGridLuka
          Width = 117
          ExplicitWidth = 117
        end
        inherited pnDescricao: TPanel
          Width = 117
          Caption = 'N'#250'mero da nota'
          ExplicitWidth = 117
        end
      end
      inline frDataAlteracaoPreco: TFrDataInicialFinal
        Left = 632
        Top = 55
        Width = 217
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 14
        TabStop = True
        ExplicitLeft = 632
        ExplicitTop = 55
        ExplicitWidth = 217
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 148
          Height = 14
          Caption = 'Data de altera'#231#227'o de pre'#231'o'
          ExplicitWidth = 148
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object cbTipoPrecoImpressao: TComboBoxLuka
        Left = 368
        Top = 101
        Width = 215
        Height = 22
        BevelKind = bkFlat
        Style = csOwnerDrawFixed
        Color = clWhite
        ItemIndex = 0
        TabOrder = 15
        Text = 'Pre'#231'o de venda'
        Items.Strings = (
          'Pre'#231'o de venda'
          'Pre'#231'o de promo'#231#227'o'
          'Nenhum')
        Valores.Strings = (
          'V'
          'P'
          'N')
        AsInt = 0
        AsString = 'V'
      end
      inline FrCondicoesPagamento2: TFrCondicoesPagamento2
        Left = 0
        Top = 436
        Width = 322
        Height = 41
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 16
        TabStop = True
        ExplicitTop = 436
        ExplicitWidth = 322
        ExplicitHeight = 41
        inherited sgPesquisa: TGridLuka
          Width = 297
          Height = 24
        end
        inherited PnTitulos: TPanel
          Width = 322
          inherited lbNomePesquisa: TLabel
            Width = 141
            ExplicitWidth = 141
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 217
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 297
          Height = 25
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object sgItens: TGridLuka
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 878
        Height = 512
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 10
        Ctl3D = True
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 3
        RowCount = 2
        GradientEndColor = 15395562
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goColSizing, goThumbTracking]
        ParentCtl3D = False
        PopupMenu = pmOpcoes
        TabOrder = 0
        OnDrawCell = sgItensDrawCell
        OnKeyDown = sgItensKeyDown
        OnKeyPress = Numeros
        OnSelectCell = sgItensSelectCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Sel.?'
          'Produto'
          'Nome'
          'Pre'#231'o de venda'
          'Pre'#231'o de promo'#231#227'o'
          'Marca'
          'Und. venda'
          'C'#243'd. barras'
          'C'#243'd. original'
          'Quantidade')
        OnGetCellColor = sgItensGetCellColor
        OnGetCellPicture = sgItensGetCellPicture
        OnArrumarGrid = sgItensArrumarGrid
        Grid3D = False
        RealColCount = 10
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          32
          55
          217
          91
          122
          105
          71
          110
          106
          75)
      end
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 928
    Top = 472
    object miMarcarDesmarcarSelecionado: TMenuItem
      Caption = 'Marcar/desm. selecionado ( Espa'#231'o )'
      OnClick = miMarcarDesmarcarSelecionadoClick
    end
    object miMarcarDesmarcarTodos: TMenuItem
      Caption = 'Marcar/desmacar todos ( CTRL + Espa'#231'o )'
      OnClick = miMarcarDesmarcarTodosClick
    end
  end
end
