unit FrameGruposFornecedores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Grids, GridLuka, _Sessao, _GruposFornecedores, _Biblioteca, PesquisaGruposFornecedores,
  Vcl.Menus;

type
  TFrGruposFornecedores = class(TFrameHenrancaPesquisas)
  public
    function GetGrupo(pLinha: Integer = -1): RecGruposFornecedores;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrGruposFornecedores }

function TFrGruposFornecedores.AdicionarDireto: TObject;
var
  vDados: TArray<RecGruposFornecedores>;
begin
  vDados := _GruposFornecedores.BuscarGrupos(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrGruposFornecedores.AdicionarPesquisando: TObject;
begin
  Result := PesquisaGruposFornecedores.Pesquisar(ckSomenteAtivos.Checked);
end;

function TFrGruposFornecedores.GetGrupo(pLinha: Integer): RecGruposFornecedores;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecGruposFornecedores(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrGruposFornecedores.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecGruposFornecedores(FDados[i]).GrupoId = RecGruposFornecedores(pSender).GrupoId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrGruposFornecedores.MontarGrid;
var
  i: Integer;
  vSender: RecGruposFornecedores;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecGruposFornecedores(FDados[i]);
      AAdd([IntToStr(vSender.GrupoId), vSender.Descricao]);
    end;
  end;
end;

end.
