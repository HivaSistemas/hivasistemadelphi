inherited FormBuscarTiposAcompanhamentoOrcamentos: TFormBuscarTiposAcompanhamentoOrcamentos
  Caption = 'Buscar tipos de acompanhamento or'#231'amentos'
  ClientHeight = 132
  ClientWidth = 344
  ExplicitWidth = 350
  ExplicitHeight = 161
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 95
    Width = 344
    inherited sbFinalizar: TSpeedButton
      Left = 113
      ExplicitLeft = 113
    end
  end
  inline FrTipoAcompanhamento: TFrTipoAcompanhamentoOrcamento
    Left = 7
    Top = 5
    Width = 325
    Height = 81
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 7
    ExplicitTop = 5
    ExplicitWidth = 325
    inherited sgPesquisa: TGridLuka
      Width = 300
      ExplicitWidth = 300
    end
    inherited PnTitulos: TPanel
      Width = 325
      ExplicitWidth = 325
      inherited lbNomePesquisa: TLabel
        Width = 187
        Caption = 'Tipo acompanhamento or'#231'amento'
        ExplicitWidth = 187
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 220
        ExplicitLeft = 220
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 300
      ExplicitLeft = 300
    end
  end
end
