unit SangriaCaixa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms,
  Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.Mask, _RecordsCaixa, _Biblioteca, EditLukaData, FrameContas, _FrameHerancaPrincipal,
  _Sessao, _MovimentosTurnos, _FrameHenrancaPesquisas, FrameFuncionarios, Vcl.StdCtrls, EditLuka, _RecordsEspeciais,
  Vcl.Buttons, Vcl.ExtCtrls, _Funcionarios, _TurnosCaixas, BuscarDados.TitulosFechamento, _ContasReceber,
  _HerancaCadastro, ImpressaoComprovanteSangriaGrafico, CheckBoxLuka;

type
  TDadosSangria = record
    cartoes: TArray<RecFechamento>;
    cheques: TArray<RecFechamento>;
    cobranca: TArray<RecFechamento>;
  end;

  TFormSangriaCaixa = class(TFormHerancaCadastroCodigo)
    FrCaixaAberto: TFrFuncionarios;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    lb14: TLabel;
    sbBuscarDadosCheques: TSpeedButton;
    sbBuscarDadosCartoes: TSpeedButton;
    sbBuscarDadosCobranca: TSpeedButton;
    lb1: TLabel;
    eValorDinheiro: TEditLuka;
    eValorCheque: TEditLuka;
    eValorCartao: TEditLuka;
    eValorCobranca: TEditLuka;
    stValoresSangria: TStaticText;
    eObservacoes: TMemo;
    lb2: TLabel;
    eTurnoId: TEditLuka;
    FrContaDestino: TFrContas;
    procedure FormCreate(Sender: TObject);
    procedure eValorChequeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorCartaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorCobrancaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbBuscarDadosChequesClick(Sender: TObject);
    procedure sbBuscarDadosCobrancaClick(Sender: TObject);
    procedure sbBuscarDadosCartoesClick(Sender: TObject);
  private
    FTurnoId: Integer;
    FDadosSangria: TDadosSangria;

    procedure FrCaixaOnAposPesquisar(Sender: TObject);
    procedure Totalizar(pFechamento: TArray<RecFechamento>; pTipo: TTipoBusca);
    procedure PreencherRegistro(pSangria: RecMovimentoSangria);
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
  end;

implementation

{$R *.dfm}

{ TFormSangriaCaixa }

procedure TFormSangriaCaixa.BuscarRegistro;
var
  sangria: TArray<RecMovimentoSangria>;
begin
  sangria := _MovimentosTurnos.BuscarMovimentoSangria(Sessao.getConexaoBanco, eID.AsInt);

  if sangria = nil then begin
    _Biblioteca.Exclamar(coNenhumRegistroEncontrado);
    eID.SetFocus;
    Abort;
  end;

  inherited;
  PreencherRegistro(sangria[0]);
end;

procedure TFormSangriaCaixa.eValorCartaoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sbBuscarDadosCartoes.Click;
end;

procedure TFormSangriaCaixa.eValorChequeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sbBuscarDadosCheques.Click;
end;

procedure TFormSangriaCaixa.eValorCobrancaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    sbBuscarDadosCobranca.Click;
end;

procedure TFormSangriaCaixa.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _MovimentosTurnos.ExcluirSangria(Sessao.getConexaoBanco, eID.AsInt, eTurnoId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormSangriaCaixa.FormCreate(Sender: TObject);
begin
  inherited;
  FrCaixaAberto.OnAposPesquisar := FrCaixaOnAposPesquisar;
  FrCaixaAberto.Modo(True);
end;

procedure TFormSangriaCaixa.FrCaixaOnAposPesquisar(Sender: TObject);
var
  vTurnos: TArray<RecTurnosCaixas>;
begin
  vTurnos := _TurnosCaixas.BuscarTurnosCaixas( Sessao.getConexaoBanco, 1, [FrCaixaAberto.GetFuncionario().funcionario_id] );
  if vTurnos = nil then begin
    Exclamar('N�o foi encontrado turno em aberto para o funcion�rio escolhido, verifique!');
    SetarFoco(FrCaixaAberto);
    Exit;
  end;

  FTurnoId := vTurnos[0].TurnoId;
  eTurnoId.AsInt := FTurnoId;

  eValorCheque.Enabled := (vTurnos[0].valor_cheque > 0) and (eID.AsInt = 0);
  sbBuscarDadosCheques.Enabled := eValorCheque.Enabled;
  eValorCartao.Enabled := (vTurnos[0].valor_cartao > 0) and (eID.AsInt = 0);
  sbBuscarDadosCartoes.Enabled := eValorCartao.Enabled;
  eValorCobranca.Enabled := (vTurnos[0].valor_cobranca > 0) and (eID.AsInt = 0);
  sbBuscarDadosCobranca.Enabled := eValorCobranca.Enabled;

  Destruir(TArray<TObject>(vTurnos));

  // Modo(True);
  FrCaixaAberto.Modo(False, False);
  SetarFoco(FrContaDestino);
end;

procedure TFormSangriaCaixa.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vSangriaId: Integer;
  vRetorno: RecRetornoBD;
  vContaDestinoId: string;
begin
  inherited;

  vContaDestinoId := '';
  if not FrContaDestino.EstaVazio then
    vContaDestinoId := FrContaDestino.GetConta().Conta;

  Sessao.getConexaoBanco.IniciarTransacao;

  vRetorno :=
    _MovimentosTurnos.AtualizarMovimentosTurno(
      Sessao.getConexaoBanco,
      0,
      FTurnoId,
      vContaDestinoId,
      eValorDinheiro.AsCurr,
      eValorCheque.AsCurr,
      0,
      eValorCartao.AsCurr,
      eValorCobranca.AsCurr,
      'SAN',
      eObservacoes.Text
    );

   if vRetorno.TeveErro then begin
    Sessao.getConexaoBanco.VoltarTransacao;
    _Biblioteca.Exclamar(vRetorno.MensagemErro);
    Abort;
  end;

  vSangriaId := vRetorno.AsInt;

  if eValorCartao.AsCurr > 0 then begin
    for i := Low(FDadosSangria.cartoes) to High(FDadosSangria.cartoes) do begin
      vRetorno :=
        _ContasReceber.AtualizarBancoCaixaTitulos(
          Sessao.getConexaoBanco,
          FDadosSangria.cartoes[i].orcamento_id,
          FDadosSangria.cartoes[i].BaixaOrigemId,
          FDadosSangria.cartoes[i].ItemIdCrtOrcamento,
          vSangriaId,
          'CRT',
          0
        );

      if vRetorno.TeveErro then begin
        Sessao.getConexaoBanco.VoltarTransacao;
        _Biblioteca.Exclamar(vRetorno.MensagemErro);
        Abort;
      end;
    end;
  end;

  if eValorCheque.AsCurr > 0 then begin
    for i := Low(FDadosSangria.cheques) to High(FDadosSangria.cheques) do begin
      { Gravando os t�tulos na sa�da }
      vRetorno :=
        _ContasReceber.AtualizarBancoCaixaTitulos(
          Sessao.getConexaoBanco,
          0,
          0,
          0,
          vSangriaId,
          'CHQ',
          FDadosSangria.cheques[i].receber_id
        );

      if vRetorno.TeveErro then begin
        Sessao.getConexaoBanco.VoltarTransacao;
        _Biblioteca.Exclamar(vRetorno.MensagemErro);
        Abort;
      end;
    end;
  end;

  if eValorCobranca.AsCurr > 0 then begin
    for i := Low(FDadosSangria.cobranca) to High(FDadosSangria.cobranca) do begin
      vRetorno :=
        _ContasReceber.AtualizarBancoCaixaTitulos(
          Sessao.getConexaoBanco,
          0,
          0,
          0,
          vSangriaId,
          'COB',
          FDadosSangria.cobranca[i].receber_id
        );

      if vRetorno.TeveErro then begin
        Sessao.getConexaoBanco.VoltarTransacao;
        _Biblioteca.Exclamar(vRetorno.MensagemErro);
        Abort;
      end;
    end;
  end;

  Sessao.getConexaoBanco.FinalizarTransacao;

  if vRetorno.AsInt > 0 then
    _Biblioteca.Informar(coNovoRegistroSucessoCodigo + NFormat(vSangriaId));

  ImpressaoComprovanteSangriaGrafico.Imprimir( vSangriaId );
end;

procedure TFormSangriaCaixa.Modo(pEditando: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    FrCaixaAberto,
    FrContaDestino,
    eValorDinheiro,
    eValorCobranca,
    eValorCheque,
    eValorCartao,
    sbBuscarDadosCartoes,
    sbBuscarDadosCheques,
    sbBuscarDadosCobranca],
    (pEditando) and (eID.AsInt = 0)
  );

  FDadosSangria.cartoes := nil;
  FDadosSangria.cheques := nil;
  FDadosSangria.cobranca := nil;

  _Biblioteca.Habilitar([eTurnoId], False);

  sbGravar.Enabled := (pEditando) and (eID.AsInt = 0);

  if pEditando then begin
    eID.Enabled := False;
    SetarFoco(FrCaixaAberto);
  end
  else begin
    eID.Enabled := True;
    SetarFoco(eID);
  end;
end;

procedure TFormSangriaCaixa.PesquisarRegistro;
begin
  inherited;

end;

procedure TFormSangriaCaixa.PreencherRegistro(pSangria: RecMovimentoSangria);
begin
  FrCaixaAberto.InserirDadoPorChave(pSangria.funcionarioId);
  FrContaDestino.InserirDadoPorChave(pSangria.contaId);
  eTurnoId.AsInt := pSangria.turnoId;
  eObservacoes.Text := pSangria.observacoes;
  eValorDinheiro.AsDouble := pSangria.valorDinheiro;
  eValorCheque.AsDouble := pSangria.valorCheque;
  eValorCartao.AsDouble := pSangria.valorCartao;
  eValorCobranca.AsDouble := pSangria.valorCobranca;
  sbExcluir.Enabled := True;
  sbGravar.Enabled := False;
  sbPesquisar.Enabled := False;
  sbDesfazer.Enabled := True;
end;

procedure TFormSangriaCaixa.sbBuscarDadosCartoesClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<TArray<RecFechamento>>;
begin
  inherited;
  if FrCaixaAberto.EstaVazio then begin
    Exclamar('O caixa para realizar a sangria n�o foi informado');
    SetarFoco(FrCaixaAberto);
    Abort;
  end;

  vRetorno := BuscarDados.TitulosFechamento.BuscarTitulosFechamento(ttCartoes, FDadosSangria.cartoes, FTurnoId);
  if vRetorno.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    SetarFoco(eValorCartao);
    Exit;
  end;

  FDadosSangria.cartoes := vRetorno.Dados;
  Totalizar(FDadosSangria.cartoes, ttCartoes);
end;

procedure TFormSangriaCaixa.sbBuscarDadosChequesClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<TArray<RecFechamento>>;
begin
  inherited;
  if FrCaixaAberto.EstaVazio then begin
    Exclamar('O caixa para realizar a sangria n�o foi informado');
    SetarFoco(FrCaixaAberto);
    Abort;
  end;

  vRetorno := BuscarDados.TitulosFechamento.BuscarTitulosFechamento(ttCheques, FDadosSangria.cheques, FTurnoId);
  if vRetorno.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    SetarFoco(eValorCheque);
    Exit;
  end;

  FDadosSangria.cheques := vRetorno.Dados;
  Totalizar(FDadosSangria.cheques, ttCheques);
end;

procedure TFormSangriaCaixa.sbBuscarDadosCobrancaClick(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<TArray<RecFechamento>>;
begin
  inherited;
  if FrCaixaAberto.EstaVazio then begin
    Exclamar('O caixa para realizar a sangria n�o foi informado');
    SetarFoco(FrCaixaAberto);
    Abort;
  end;

  vRetorno := BuscarDados.TitulosFechamento.BuscarTitulosFechamento(ttCobrancas, FDadosSangria.cobranca, FTurnoId);
  if vRetorno.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    SetarFoco(eValorCobranca);
    Exit;
  end;

  FDadosSangria.cobranca := vRetorno.Dados;
  Totalizar(FDadosSangria.cobranca, ttCobrancas);
end;

procedure TFormSangriaCaixa.Totalizar(pFechamento: TArray<RecFechamento>; pTipo: TTipoBusca);
var
  i: Integer;
  vTotal: Double;
begin
  vTotal := 0;
  for i := Low(pFechamento) to High(pFechamento) do
    vTotal := vTotal + pFechamento[i].valor;

  if pTipo = ttCartoes then
    eValorCartao.AsDouble := vTotal
  else if pTipo = ttCheques then
    eValorCheque.AsDouble := vTotal
  else
    eValorCobranca.AsDouble := vTotal;
end;

procedure TFormSangriaCaixa.VerificarRegistro(Sender: TObject);
var
  valorDinheiroTurno: Double;
begin
  inherited;

  if FrCaixaAberto.EstaVazio then begin
    Exclamar('N�o foi informado o caixa para ser realizada a opera��o, verifique!');
    SetarFoco(FrCaixaAberto);
    Abort;
  end;

  if (FrContaDestino.EstaVazio) and (eValorDinheiro.AsCurr > 0) then begin
    Exclamar('A conta de destino da sangria n�o foi informada corretamente, verifique!');
    SetarFoco(FrContaDestino);
    Abort;
  end;

  if
    (eValorDinheiro.AsCurr = 0) and
    (eValorCheque.AsCurr = 0) and
    (eValorCobranca.AsCurr = 0) and
    (eValorCartao.AsCurr = 0)
  then begin
    Exclamar('Nenhum valor foi informado, verifique!');
    eValorDinheiro.SetFocus;
    Abort;
  end;

  if eValorDinheiro.AsCurr > 0 then begin
    valorDinheiroTurno := _MovimentosTurnos.BuscarValorDinheiroTurno(Sessao.getConexaoBanco, eTurnoId.AsInt);

    if eValorDinheiro.AsDouble > valorDinheiroTurno then begin
      Exclamar('O valor do dinheiro informado � maior do que o dinheiro em caixa!');
      eValorDinheiro.SetFocus;
      Abort;
    end;
  end;

end;

end.
