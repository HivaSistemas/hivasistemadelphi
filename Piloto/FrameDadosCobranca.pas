unit FrameDadosCobranca;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, _Biblioteca, _RecordsCadastros,
  _FrameHenrancaPesquisas, FrameTiposCobranca, Vcl.Mask, EditLukaData, System.DateUtils,
  Vcl.StdCtrls, EditLuka, Vcl.Grids, GridLuka, StaticTextLuka, CheckBoxLuka, _RecordsFinanceiros,
  Vcl.ExtCtrls, MaskEditLuka, EditTelefoneLuka, EditCpfCnpjLuka;

type
  TFrDadosCobranca = class(TFrameHerancaPrincipal)
    pnInformacoesPrincipais: TPanel;
    lbllb10: TLabel;
    lbllb9: TLabel;
    lbllb6: TLabel;
    lbllb2: TLabel;
    eRepetir: TEditLuka;
    ePrazo: TEditLuka;
    eValorCobranca: TEditLuka;
    eDataVencimento: TEditLukaData;
    FrTiposCobranca: TFrTiposCobranca;
    pnInformacoesBoleto: TPanel;
    eNossoNumero: TEditLuka;
    lbllb31: TLabel;
    eCodigoBarras: TMaskEditLuka;
    lbl1: TLabel;
    pn1: TPanel;
    sgCobrancas: TGridLuka;
    stDiferenca: TStaticText;
    st2: TStaticText;
    stValorTotalDefinido: TStaticText;
    stTotalDefinido: TStaticText;
    stValorTotal: TStaticText;
    stValorTotalASerPago: TStaticText;
    StaticTextLuka4: TStaticTextLuka;
    stQtdeTitulos: TStaticText;
    ckObrigarDataBase: TCheckBoxLuka;
    ckMostrarCodigoBarras: TCheckBoxLuka;
    eDocumento: TEditLuka;
    lbl2: TLabel;
    pnInformacoesCheque: TPanel;
    lbllb1: TLabel;
    lbllb3: TLabel;
    lbllb4: TLabel;
    lbllb5: TLabel;
    eBanco: TEditLuka;
    eAgencia: TEditLuka;
    eContaCorrente: TEditLuka;
    eNumeroCheque: TEditLuka;
    eNomeEmitente: TEditLuka;
    eTelefoneEmitente: TEditTelefoneLuka;
    lbl3: TLabel;
    lbl4: TLabel;
    st1: TStaticText;
    stPrazoMedioCalc: TStaticText;
    lbCPF_CNPJ: TLabel;
    eCPF_CNPJ: TEditCPF_CNPJ_Luka;
    ckDataFixa: TCheckBoxLuka;
    procedure eDocumentoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorCobancaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgCobrancasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgCobrancasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgCobrancasDblClick(Sender: TObject);
    procedure eNossoNumeroKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eTelefoneEmitenteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eNumeroChequeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eCPF_CNPJExit(Sender: TObject);
  private
    FNatureza: string;
    FFormaPagamento: string;
    FTelaEntradaNotaFiscal: Boolean;

    FPrazoMedioPermitido: Double;
    FDataBase: TDateTime;
    FEditDataBase: TEditLukaData;

    FLinhaEditando: Integer;
    FDiasPrazo: TArray<RecTipoCobrancaDiasPrazo>;

    procedure FrTiposCobrancaOnAposPesquisar(Sender: TObject);
    procedure FrTiposCobrancaOnAposDeletar(Sender: TObject);

    function getValorPagar: Double;
    function getValorDefinido: Currency;
    function getExisteDiferenca: Boolean;
    procedure setValorPagar(pValor: Double);

    function getTitulos: TArray<RecTitulosFinanceiros>;
    procedure setTitulos(pTitulos: TArray<RecTitulosFinanceiros>);

    function getComponentesPrazo: TArray<TControl>;
    function getComponentesBoleto: TArray<TControl>;
    function getComponentesCheque: TArray<TControl>;
    function getComponentesComum: TArray<TControl>;

    procedure AddGrid;

    procedure setFormaPagamento(pValor: string);
    procedure setNatureza(pValor: string);
  public
    FDadosRetencao: Double;
    constructor Create(AOwner: TComponent); override;

    procedure Clear; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;

    procedure SetFocus; override;

    function EmEdicao: Boolean;
    function EstaVazio: Boolean; override;

    procedure verificarPrazoMedio;
    procedure CalculaDiferencaPagamento;
    procedure setEditDataBase(var pEdit: TEditLukaData);
    procedure setTelaEntrada;
  published
    property Natureza: string read FNatureza write setNatureza;
    property DataBase: TDateTime read FDataBase write FDataBase;
    property FormaPagamento: string read FFormaPagamento write setFormaPagamento;
    property ValorPagar: Double read getValorPagar write setValorPagar;
    property ValorDefinido: Currency read getValorDefinido;
    property ExisteDiferenca: Boolean read getExisteDiferenca;
    property PrazoMedioPermitido: Double read FPrazoMedioPermitido write FPrazoMedioPermitido;
    property Titulos: TArray<RecTitulosFinanceiros> read getTitulos write setTitulos;
  end;

implementation

{$R *.dfm}

{ TFrDadosCobranca }

uses
  _TiposCobrancaDiasPrazo, _Sessao;

const
  coCobrancaId         = 0;
  coNomeTipoCobranca   = 1;
  coValorCobranca      = 2;
  coValorRetencao      = 3;
  coDocumento          = 4;
  coParcela            = 5;
  coQtdeParcelas       = 6;
  coDataVencimento     = 7;
  coCodigoBarras       = 8;
  coNossoNumero        = 9;
  coBanco              = 10;
  coAgencia            = 11;
  coContaCorrente      = 12;
  coNumeroCheque       = 13;
  coNomeEmitente       = 14;
  coCpfCnpjEmitente    = 15;
  coTelefone           = 16;

  (* Ocultas *)
  coPortadorId         = 16;
  coPermitirPrazoMedio = 17;
  coPrazoMedio         = 18;

procedure TFrDadosCobranca.AddGrid;
var
  i: Integer;
  vLinha: Integer;
  vRepeticao: Integer;

  vDataAtual: TDateTime;
  vDataVencimento: TDateTime;
  vDataVencimentoOriginal: TDateTime;

  vValorProporcional: Currency;
  vValorRestante: Currency;
  vValorRetencao: Currency;
begin
  inherited;

  if FrTiposCobranca.EstaVazio then begin
    _Biblioteca.Exclamar('� necess�rio informar o tipo de cobran�a!');
    SetarFoco(FrTiposCobranca);
    Exit;
  end;

  if eDataVencimento.Enabled and (not eDataVencimento.DataOk) and (FDiasPrazo = nil) then begin
    _Biblioteca.Exclamar('A data de vencimento n�o foi definida corretamente!');
    SetarFoco(eDataVencimento);
    Exit;
  end;

  if ckObrigarDataBase.Checked then begin
    if Assigned(FEditDataBase) then begin
      if not FEditDataBase.DataOk then begin
        _Biblioteca.Exclamar('A data base ainda n�o foi informada, informe antes de continuar!');
        SetarFoco(FEditDataBase);
        Exit;
      end;
      FDataBase := FEditDataBase.AsData;
    end
    else if FDataBase = 0 then begin
      _Biblioteca.Exclamar('A data base ainda n�o foi informada, informe antes de continuar!');
      Exit;
    end;
  end;

  if eDocumento.Visible and (eDocumento.Trim = '') then begin
    _Biblioteca.Exclamar('O documento n�o foi informado corretamente, verifique!');
    SetarFoco(eDocumento);
    Exit;
  end;

  if FFormaPagamento = 'CHQ' then begin
    if FNatureza = 'R' then begin
      if eBanco.Text = '' then begin
        _Biblioteca.Exclamar('O c�digo do banco a que o cheque pertence n�o foi informado corretamente, verifique!');
        SetarFoco(eBanco);
        Exit;
      end;

      if eAgencia.Text = '' then begin
        _Biblioteca.Exclamar('A ag�ncia a que o cheque pertence n�o foi informado corretamente, verifique!');
        SetarFoco(eAgencia);
        Exit;
      end;

      if eContaCorrente.Text = '' then begin
        _Biblioteca.Exclamar('A conta corrente a que o cheque pertence n�o foi informado corretamente, verifique!');
        SetarFoco(eContaCorrente);
        Exit;
      end;

      if eNumeroCheque.AsInt = 0 then begin
        _Biblioteca.Exclamar('O n�mero do cheque n�o foi informado corretamente, verifique!');
        SetarFoco(eNumeroCheque);
        Exit;
      end;
    end;

    if eNumeroCheque.AsInt = 0 then begin
      _Biblioteca.Exclamar('O n�mero do cheque n�o foi informado corretamente, verifique!');
      SetarFoco(eNumeroCheque);
      Exit;
    end;
  end;

  vDataAtual := 0;
  vRepeticao := 1;
  vDataVencimento := Sessao.getData;
  if eRepetir.AsInt > 0 then begin
    vRepeticao := eRepetir.AsInt;
    vDataVencimento := eDataVencimento.AsData;
    if ckDataFixa.Checked then begin
      vDataVencimentoOriginal := vDataVencimento;
    end;
  end
  else if (FDiasPrazo <> nil) then begin
    if FLinhaEditando = -1 then begin
      vRepeticao      := Length(FDiasPrazo);

      if not ckDataFixa.Checked then begin
        vDataAtual      := IIfDbl(eDataVencimento.DataOk, eDataVencimento.AsData, Sessao.getData);
        vDataVencimento := vDataAtual;
      end
      else begin
        vDataAtual      := IIfDbl(eDataVencimento.DataOk, eDataVencimento.AsData, Sessao.getData);
        vDataVencimento := vDataAtual;
        vDataVencimentoOriginal := vDataVencimento;
      end;
    end
    else begin
      vRepeticao := 1;
      vDataVencimento := eDataVencimento.AsData;
    end;
  end
  else if FLinhaEditando > -1 then begin
    vRepeticao := IIfInt(eRepetir.AsInt = 0, 1, eRepetir.AsInt);
    vDataVencimento := eDataVencimento.AsData;
  end;

  vValorProporcional := Arredondar( eValorCobranca.AsCurr / vRepeticao, 2 );
  vValorRestante := ( eValorCobranca.AsCurr - (vValorProporcional * vRepeticao) );

  vValorRetencao := 0;
  if FDadosRetencao > 0 then
    vValorRetencao := (FDadosRetencao / vRepeticao);

  for i := 1 to vRepeticao do begin
    if FLinhaEditando > -1 then
      vLinha := FLinhaEditando
    else if sgCobrancas.Cells[coDataVencimento, 1] = '' then
      vLinha := 1
    else begin
      vLinha := sgCobrancas.RowCount - 1;
      Inc(vLinha);
    end;

    sgCobrancas.Cells[coDataVencimento, vLinha]     := DFormat(vDataVencimento);
    sgCobrancas.Cells[coValorCobranca, vLinha]      := _Biblioteca.NFormat(vValorProporcional + IIfDbl(i = 1, vValorRestante) ); // Adiciona o valor que iria sobrar na primeira parcela
    sgCobrancas.Cells[coValorRetencao, vLinha]      := _Biblioteca.NFormat(vValorRetencao);
    sgCobrancas.Cells[coDocumento, vLinha]          := eDocumento.Text;
    sgCobrancas.Cells[coNomeTipoCobranca, vLinha]   := FrTiposCobranca.GetTipoCobranca.Nome;
    sgCobrancas.Cells[coCobrancaId, vLinha]         := NFormat(FrTiposCobranca.GetTipoCobranca.CobrancaId);
    sgCobrancas.Cells[coCodigoBarras, vLinha]       := eCodigoBarras.Text;
    sgCobrancas.Cells[coNossoNumero, vLinha]        := eNossoNumero.Text;
    sgCobrancas.Cells[coBanco, vLinha]              := eBanco.Text;
    sgCobrancas.Cells[coAgencia, vLinha]            := eAgencia.Text;
    sgCobrancas.Cells[coContaCorrente, vLinha]      := eContaCorrente.Text;
    sgCobrancas.Cells[coNumeroCheque, vLinha]       := NFormat(eNumeroCheque.AsInt + i - 1);
    sgCobrancas.Cells[coNomeEmitente, vLinha]       := eNomeEmitente.Text;
    sgCobrancas.Cells[coCPFCNPJEmitente, vLinha]    := eCPF_CNPJ.Text;
    sgCobrancas.Cells[coTelefone, vLinha]           := eTelefoneEmitente.Text;
    sgCobrancas.Cells[coPortadorId, vLinha]         := FrTiposCobranca.GetTipoCobranca.PortadorId;
    sgCobrancas.Cells[coPermitirPrazoMedio, vLinha] := FrTiposCobranca.GetTipoCobranca.PermitirPrazoMedio;

    if FLinhaEditando = -1 then begin
      sgCobrancas.Cells[coParcela, vLinha] := NFormat(i);
      sgCobrancas.Cells[coQtdeParcelas, vLinha] := NFormat(vRepeticao);
    end;

    if not ckDataFixa.Checked then begin
      if eRepetir.AsInt > 0 then
        vDataVencimento := IncDay(vDataVencimento, ePrazo.AsInt)
      else if (FDiasPrazo <> nil) and (i <= High(FDiasPrazo)) then
        vDataVencimento := IncDay(vDataAtual, FDiasPrazo[i].Dias - FDiasPrazo[0].Dias)
      else if ePrazo.AsInt > 0 then
        vDataVencimento := IncDay(vDataVencimento, ePrazo.AsInt);
    end
    else begin
      vDataVencimento := IncMonth(vDataVencimentoOriginal, i)
    end;

    if FLinhaEditando = -1 then
      sgCobrancas.RowCount := vLinha + 1;
  end;
  FLinhaEditando := -1;

  _Biblioteca.LimparCampos([
    eCodigoBarras,
    eNossoNumero,
    eDataVencimento,
    eValorCobranca,
    eRepetir,
    ePrazo,
    FrTiposCobranca,
    eDocumento]
  );

  _Biblioteca.Habilitar([eDataVencimento], False);
  _Biblioteca.Habilitar([FrTiposCobranca], True);
  SetarFoco(FrTiposCobranca);
  CalculaDiferencaPagamento;
  FLinhaEditando := -1;
end;

procedure TFrDadosCobranca.CalculaDiferencaPagamento;
var
  i: Integer;

  vDataAtual: TDateTime;
  vQtdeTitulos: Integer;
  vDiasPrazoMedio: Double;

begin
  stDiferenca.Caption := _Biblioteca.NFormatN( SFormatDouble(stValorTotalASerPago.Caption) - SFormatDouble(stValorTotalDefinido.Caption) );

  if sgCobrancas.Cells[coCobrancaId, 1] = '' then
    Exit;

  vQtdeTitulos := 0;
  vDiasPrazoMedio := 0;
  vDataAtual := Sessao.getData;

  stValorTotalDefinido.Caption := '0,00';
  for i := 1 to sgCobrancas.RowCount -1 do begin
    if sgCobrancas.Cells[coCobrancaId, i] <> '' then
      Inc(vQtdeTitulos);

    stValorTotalDefinido.Caption := _Biblioteca.NFormat( SFormatDouble(stValorTotalDefinido.Caption) + SFormatDouble(sgCobrancas.Cells[coValorCobranca, i]) );
    vDiasPrazoMedio := vDiasPrazoMedio + ToDataHora(sgCobrancas.Cells[coDataVencimento, i]) - vDataAtual;
  end;

  stQtdeTitulos.Caption    := NFormatN(vQtdeTitulos);
  stPrazoMedioCalc.Caption := NFormatN(vDiasPrazoMedio / vQtdeTitulos);
  stDiferenca.Caption      := _Biblioteca.NFormatN( SFormatDouble(stValorTotalASerPago.Caption) - SFormatDouble(stValorTotalDefinido.Caption) );
end;

procedure TFrDadosCobranca.Clear;
begin
  inherited;
  FDataBase := 0;
  _Biblioteca.Visibilidade([pnInformacoesCheque, pnInformacoesBoleto], False);
  _Biblioteca.LimparCampos([FrTiposCobranca, ePrazo, eRepetir, eDataVencimento, eValorCobranca, sgCobrancas, stPrazoMedioCalc]);
end;

constructor TFrDadosCobranca.Create(AOwner: TComponent);
begin
  inherited;
  FrTiposCobranca.OnAposPesquisar := FrTiposCobrancaOnAposPesquisar;
  FrTiposCobranca.OnAposDeletar := FrTiposCobrancaOnAposDeletar;

  eValorCobranca.OnKeyDown := ProximoCampo;
  eDocumento.OnKeyDown     := eDocumentoKeyDown;
  eCodigoBarras.OnKeyDown  := ProximoCampo;
  eNossoNumero.OnKeyDown   := eNossoNumeroKeyDown;
  eNumeroCheque.OnKeyDown  := eNumeroChequeKeyDown;
  eValorCobranca.OnKeyDown := eValorCobancaKeyDown;
  eCPF_CNPJ.OnExit         := eCPF_CNPJExit;

  FLinhaEditando := -1;
  FTelaEntradaNotaFiscal := False;
end;

function TFrDadosCobranca.EmEdicao: Boolean;
begin
  Result := FrTiposCobranca.sgPesquisa.Enabled;
end;

procedure TFrDadosCobranca.eNossoNumeroKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  AddGrid;
end;

procedure TFrDadosCobranca.eNumeroChequeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key <> VK_RETURN then
    Exit;

  if FNatureza = 'R' then begin
    ProximoCampo(Sender, Key, Shift);
    Exit;
  end;

  AddGrid;
end;

function TFrDadosCobranca.EstaVazio: Boolean;
begin
  Result := False;
end;

procedure TFrDadosCobranca.eTelefoneEmitenteKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  AddGrid;
end;

procedure TFrDadosCobranca.eValorCobancaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then begin
    if FNatureza = 'R' then begin
      if pnInformacoesCheque.Visible then
        SetarFoco(eBanco)
      else if pnInformacoesBoleto.Visible and eCodigoBarras.Enabled then
        SetarFoco(eCodigoBarras)
      else
        AddGrid;
    end
    else
      ProximoCampo(Sender, Key, Shift);
  end;
end;

procedure TFrDadosCobranca.eCPF_CNPJExit(Sender: TObject);
begin
  inherited;
  if _Biblioteca.RetornaNumeros(eCPF_CNPJ.Text) = '' then
    Exit;

  if not _Biblioteca.getCNPJ_CPFOk(eCPF_CNPJ.Text) then begin
    _Biblioteca.Exclamar('Informe um CPF/CNPJ v�lido!');
    SetarFoco(eCPF_CNPJ);
    eCPF_CNPJ.EditMask := '';
    eCPF_CNPJ.Clear;
  end;

  if Length(_Biblioteca.RetornaNumeros(eCPF_CNPJ.Text)) = 11 then
    eCPF_CNPJ.Tipo := [tccCPF]
  else
    eCPF_CNPJ.Tipo := [tccCNPJ];
end;

procedure TFrDadosCobranca.eDocumentoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key <> VK_RETURN then
    Exit;

  if pnInformacoesBoleto.Visible and eNossoNumero.Enabled then
    SetarFoco(eCodigoBarras)
  else if pnInformacoesCheque.Visible and eBanco.Enabled then
    SetarFoco(eBanco)
  else if pnInformacoesCheque.Visible and eNumeroCheque.Enabled then
    SetarFoco(eNumeroCheque)
  else
    AddGrid;
end;

procedure TFrDadosCobranca.FrTiposCobrancaOnAposDeletar(Sender: TObject);
begin
  LimparCampos([ePrazo, eRepetir]);
  FDiasPrazo := nil;

  lbllb2.Caption := 'Vencimento';

  _Biblioteca.Visibilidade([pnInformacoesCheque, pnInformacoesBoleto], False);
end;

procedure TFrDadosCobranca.FrTiposCobrancaOnAposPesquisar(Sender: TObject);
begin
  _Biblioteca.Visibilidade([pnInformacoesCheque], FrTiposCobranca.GetTipoCobranca().BoletoBancario = 'S');
  _Biblioteca.Visibilidade([pnInformacoesCheque], FrTiposCobranca.GetTipoCobranca().forma_pagamento = 'CHQ');

  if (FNatureza = 'P') and (FFormaPagamento = 'CHQ') then begin
    _Biblioteca.Habilitar(getComponentesCheque, False);
    _Biblioteca.Habilitar([eNumeroCheque], True);
  end;

  if FLinhaEditando > -1 then
    Exit;

  FLinhaEditando := -1;
  FDiasPrazo := _TiposCobrancaDiasPrazo.BuscarTipoCobrancaDiasPrazos(Sessao.getConexaoBanco, 0, [FrTiposCobranca.GetTipoCobranca().CobrancaId]);

  _Biblioteca.Habilitar(getComponentesPrazo, (FDiasPrazo = nil) or (FTelaEntradaNotaFiscal) or (FrTiposCobranca.GetTipoCobranca.PermitirPrazoMedio = 'S') );
  _Biblioteca.Habilitar(getComponentesBoleto, FrTiposCobranca.GetTipoCobranca().BoletoBancario = 'S');
  _Biblioteca.Habilitar(getComponentesCheque, FrTiposCobranca.GetTipoCobranca().forma_pagamento = 'CHQ');

  if FDiasPrazo <> nil then begin
    lbllb2.Caption := '1� Vencto';
    eDataVencimento.AsData := Sessao.getData + FDiasPrazo[0].Dias;
  end;
end;

function TFrDadosCobranca.getComponentesBoleto: TArray<TControl>;
begin
  Result := [eCodigoBarras, eNossoNumero];
end;

function TFrDadosCobranca.getComponentesCheque: TArray<TControl>;
begin
  Result := [eBanco, eAgencia, eContaCorrente, eNumeroCheque, eNomeEmitente, eCPF_CNPJ, eTelefoneEmitente];
end;

function TFrDadosCobranca.getComponentesComum: TArray<TControl>;
begin
  Result := [FrTiposCobranca, eValorCobranca, eDocumento, sgCobrancas, stValorTotalASerPago, stValorTotalDefinido, stDiferenca, stQtdeTitulos, stPrazoMedioCalc];
end;

function TFrDadosCobranca.getComponentesPrazo: TArray<TControl>;
begin
  Result := [ePrazo, eRepetir, eDataVencimento, ckDataFixa];
end;

function TFrDadosCobranca.getExisteDiferenca: Boolean;
begin
  Result := stDiferenca.Caption <> '';
end;

function TFrDadosCobranca.getTitulos: TArray<RecTitulosFinanceiros>;
var
  i: Integer;
begin
  Result := nil;

  if sgCobrancas.Cells[coCobrancaId, 1] = '' then
    Exit;

  SetLength(Result, sgCobrancas.RowCount -1);
  for i := 1 to sgCobrancas.RowCount -1 do begin
    Result[i - 1].ItemId         := i;
    Result[i - 1].CobrancaId     := SFormatInt( sgCobrancas.Cells[coCobrancaId, i] );
    Result[i - 1].NomeCobranca   := sgCobrancas.Cells[coNomeTipoCobranca, i];
    Result[i - 1].DataVencimento := ToData( sgCobrancas.Cells[coDataVencimento, i] );
    Result[i - 1].Valor          := SFormatDouble( sgCobrancas.Cells[coValorCobranca, i] );
    Result[i - 1].ValorRetencao  := SFormatDouble( sgCobrancas.Cells[coValorRetencao, i] );
    Result[i - 1].Parcela        := SFormatInt( sgCobrancas.Cells[coParcela, i] );
    Result[i - 1].NumeroParcelas := SFormatInt( sgCobrancas.Cells[coQtdeParcelas, i] );
    Result[i - 1].Documento      := sgCobrancas.Cells[coDocumento, i];

    Result[i - 1].NossoNumero    := sgCobrancas.Cells[coNossoNumero, i];
    Result[i - 1].CodigoBarras   := sgCobrancas.Cells[coCodigoBarras, i];

    Result[i - 1].Banco               := sgCobrancas.Cells[coBanco, i];
    Result[i - 1].Agencia             := sgCobrancas.Cells[coAgencia, i];
    Result[i - 1].ContaCorrente       := sgCobrancas.Cells[coContaCorrente, i];
    Result[i - 1].NumeroCheque        := SFormatInt(sgCobrancas.Cells[coNumeroCheque, i]);
    Result[i - 1].NomeEmitente        := sgCobrancas.Cells[coNomeEmitente, i];
    Result[i - 1].CpfCnpjEmitente     := sgCobrancas.Cells[coCpfCnpjEmitente, i];
    Result[i - 1].TelefoneEmitente    := sgCobrancas.Cells[coTelefone, i];
    Result[i - 1].PortadorId          := sgCobrancas.Cells[coPortadorId, i];
    Result[i - 1].PermitirPrazoMedio  := sgCobrancas.Cells[coPermitirPrazoMedio, i];

    if FNatureza = 'R' then
      Result[i - 1].Documento := 'MAN/' + sgCobrancas.Cells[coCobrancaId, i];
  end;
end;

function TFrDadosCobranca.getValorDefinido: Currency;
begin
  Result := SFormatCurr(stValorTotalDefinido.Caption);
end;

function TFrDadosCobranca.getValorPagar: Double;
begin
  Result := SFormatDouble( stValorTotalASerPago.Caption );
end;

procedure TFrDadosCobranca.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  pnInformacoesBoleto.Visible := ckMostrarCodigoBarras.Checked;

  _Biblioteca.Habilitar(getComponentesComum, pEditando, pLimpar);
  _Biblioteca.Habilitar(getComponentesPrazo, False, pLimpar);
  _Biblioteca.Habilitar(getComponentesBoleto, False, pLimpar);

  FDataBase := 0;

  if not pEditando then
    _Biblioteca.Visibilidade([pnInformacoesCheque, pnInformacoesBoleto], False);
end;

procedure TFrDadosCobranca.setEditDataBase(var pEdit: TEditLukaData);
begin
  FEditDataBase := pEdit;
end;

procedure TFrDadosCobranca.SetFocus;
begin
  inherited;
  SetarFoco(FrTiposCobranca);
end;

procedure TFrDadosCobranca.setFormaPagamento(pValor: string);
begin
  FFormaPagamento := pValor;
  FrTiposCobranca.setFormaPagamento(FFormaPagamento);

  if FFormaPagamento = 'CHQ' then begin
    sgCobrancas.OcultarColunas([coCodigoBarras, coNossoNumero]);
    _Biblioteca.Visibilidade([pnInformacoesCheque], True);
  end
  else if FFormaPagamento = 'COB' then
    _Biblioteca.Visibilidade([pnInformacoesBoleto], True);
end;

procedure TFrDadosCobranca.setNatureza(pValor: string);
begin
  FNatureza := pValor;
  FrTiposCobranca.setNatureza(FNatureza);

  if FNatureza = 'R' then
    sgCobrancas.OcultarColunas([coDocumento]);

  _Biblioteca.Visibilidade([lbl2, eDocumento], FNatureza = 'P');
end;

procedure TFrDadosCobranca.setTelaEntrada;
begin
  FTelaEntradaNotaFiscal := True;
end;

procedure TFrDadosCobranca.setTitulos(pTitulos: TArray<RecTitulosFinanceiros>);
var
  i: Integer;
begin
  if pTitulos = nil then
    Exit;

  for i := Low(pTitulos) to High(pTitulos) do begin
    sgCobrancas.Cells[coCobrancaId, i + 1]       := NFormat(pTitulos[i].CobrancaId);
    sgCobrancas.Cells[coNomeTipoCobranca, i + 1] := pTitulos[i].NomeCobranca;
    sgCobrancas.Cells[coDataVencimento, i + 1]   := DFormat(pTitulos[i].DataVencimento);
    sgCobrancas.Cells[coValorCobranca, i + 1]    := NFormat( pTitulos[i].Valor );
    sgCobrancas.Cells[coValorRetencao, i + 1]    := NFormat( pTitulos[i].ValorRetencao );
    sgCobrancas.Cells[coParcela, i + 1]          := NFormat( pTitulos[i].Parcela );
    sgCobrancas.Cells[coQtdeParcelas, i + 1]     := NFormat( pTitulos[i].NumeroParcelas );
    sgCobrancas.Cells[coDocumento, i + 1]        := pTitulos[i].Documento;

    sgCobrancas.Cells[coNossoNumero, i + 1]      := pTitulos[i].NossoNumero;
    sgCobrancas.Cells[coCodigoBarras, i + 1]     := pTitulos[i].CodigoBarras;

    sgCobrancas.Cells[coBanco, i + 1]         := pTitulos[i].Banco;
    sgCobrancas.Cells[coAgencia, i + 1]       := pTitulos[i].Agencia;
    sgCobrancas.Cells[coContaCorrente, i + 1] := pTitulos[i].ContaCorrente;
    sgCobrancas.Cells[coNumeroCheque, i + 1]  := NFormat(pTitulos[i].NumeroCheque);
    sgCobrancas.Cells[coNomeEmitente, i + 1]  := pTitulos[i].NomeEmitente;
    sgCobrancas.Cells[coTelefone, i + 1]      := pTitulos[i].TelefoneEmitente;
    sgCobrancas.Cells[coPortadorId, i + 1]    := pTitulos[i].PortadorId;
    sgCobrancas.Cells[coPermitirPrazoMedio, i + 1] := pTitulos[i].PermitirPrazoMedio;
  end;
  sgCobrancas.SetLinhasGridPorTamanhoVetor( Length(pTitulos) );

  CalculaDiferencaPagamento;
end;

procedure TFrDadosCobranca.setValorPagar(pValor: Double);
begin
  stValorTotalASerPago.Caption := NFormatN(pValor);
  CalculaDiferencaPagamento;
end;

procedure TFrDadosCobranca.sgCobrancasDblClick(Sender: TObject);
begin
  inherited;
  if sgCobrancas.Cells[coDataVencimento, sgCobrancas.Row] = '' then
    Exit;

  FLinhaEditando := sgCobrancas.Row;

  eDataVencimento.AsData  := ToData(sgCobrancas.Cells[coDataVencimento, FLinhaEditando]);
  FrTiposCobranca.InserirDadoPorChave(StrToInt(sgCobrancas.Cells[coCobrancaId, FLinhaEditando]), False);
  eValorCobranca.AsDouble := SFormatDouble(sgCobrancas.Cells[coValorCobranca, FLinhaEditando]);
  eDocumento.Text         := sgCobrancas.Cells[coDocumento, FLinhaEditando];
  eCodigoBarras.Text      := sgCobrancas.Cells[coCodigoBarras, FLinhaEditando];
  eNossoNumero.Text       := sgCobrancas.Cells[coNossoNumero, FLinhaEditando];

  eBanco.Text            := sgCobrancas.Cells[coBanco, FLinhaEditando];
  eAgencia.Text          := sgCobrancas.Cells[coAgencia, FLinhaEditando];
  eContaCorrente.Text    := sgCobrancas.Cells[coContaCorrente, FLinhaEditando];
  eNumeroCheque.Text     := sgCobrancas.Cells[coNumeroCheque, FLinhaEditando];
  eNomeEmitente.Text     := sgCobrancas.Cells[coNomeEmitente, FLinhaEditando];
  eCPF_CNPJ.Text         := sgCobrancas.Cells[coCpfCnpjEmitente, FLinhaEditando];
  eTelefoneEmitente.Text := sgCobrancas.Cells[coTelefone, FLinhaEditando];

  _Biblioteca.Habilitar([eDataVencimento], sgCobrancas.Cells[coPermitirPrazoMedio, FLinhaEditando] = 'S', False);
  _Biblioteca.Habilitar([FrTiposCobranca], False, False);
  if eDataVencimento.Enabled then
    SetarFoco(eDataVencimento)
  else
    SetarFoco(eValorCobranca);
end;

procedure TFrDadosCobranca.sgCobrancasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coValorCobranca, coValorRetencao, coCobrancaId, coParcela, coQtdeParcelas] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgCobrancas.MergeCells([ARow, ACol], [ARow, ACol],[ARow, ACol], vAlinhamento, Rect);
end;

procedure TFrDadosCobranca.sgCobrancasKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then begin
    sgCobrancas.DeleteRow(sgCobrancas.Row);
    CalculaDiferencaPagamento;
  end;
end;

procedure TFrDadosCobranca.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  _Biblioteca.Visibilidade([pnInformacoesPrincipais, pnInformacoesCheque, pnInformacoesBoleto, stValorTotal, stValorTotalASerPago, st2, stDiferenca], not pValue);
end;

procedure TFrDadosCobranca.verificarPrazoMedio;
begin
//  if (PrazoMedioPermitido > 0) and (SFormatCurr(stPrazoMedioCalc.Caption) > PrazoMedioPermitido) then begin
//    _Biblioteca.Exclamar(
//      'O prazo m�dio calculado � maior que o permitido, por favor verifique! ' + sLineBreak +
//      'Prazo m�dio permitido: ' + NFormatN(FPrazoMedioPermitido) + sLineBreak +
//      'Prazo m�dio c�lculado: ' + stPrazoMedioCalc.Caption
//    );
//    Abort;
//  end;
end;

end.
