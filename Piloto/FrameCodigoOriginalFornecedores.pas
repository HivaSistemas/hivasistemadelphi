unit FrameCodigoOriginalFornecedores;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Frame.HerancaInsercaoExclusao, _Biblioteca, BuscarFornecedorCodigoOriginal,
  Vcl.Menus, Vcl.StdCtrls, StaticTextLuka, Vcl.Grids, GridLuka, _ProdutosFornCodOriginais;

type
  TFrCodigoOriginalFornecedores = class(TFrameHerancaInsercaoExclusao)
    procedure sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    function  getCodigosOriginais: TArray<RecProdutosFornCodOriginais>;
    procedure setCodigosOriginais(pCodigos: TArray<RecProdutosFornCodOriginais>);
  protected
    procedure Inserir; override;
  published
    property Codigos: TArray<RecProdutosFornCodOriginais> read getCodigosOriginais write setCodigosOriginais;
  end;

implementation

{$R *.dfm}

{ TFrCodigoOriginalFornecedores }

const
  coFornecedor     = 0;
  coCodigoOriginal = 1;
  coFornecedorId   = 2;

function TFrCodigoOriginalFornecedores.getCodigosOriginais: TArray<RecProdutosFornCodOriginais>;
var
  i: Integer;
begin
  Result := nil;

  if sgValores.Cells[coFornecedor, 1] = '' then
    Exit;

  SetLength(Result, sgValores.RowCount -1);
  for i := 1 to sgValores.RowCount -1 do begin
    Result[i - 1].FornecedorId   := SFormatInt(sgValores.Cells[coFornecedorId, i]);
    Result[i - 1].CodigoOriginal := sgValores.Cells[coCodigoOriginal, i];
  end;
end;

procedure TFrCodigoOriginalFornecedores.Inserir;
var
  vLinha: Integer;
  vRet: TRetornoTelaFinalizar<RecProdutosFornCodOriginais>;
begin
  inherited;

  vRet := BuscarFornecedorCodigoOriginal.Buscar;
  if vRet.RetTela <> trOk then begin
    Self.SetFocus;
    Exit;
  end;

  if sgValores.Cells[coFornecedor, 1] = '' then
    vLinha := 1
  else begin
    vLinha := sgValores.Localizar([coFornecedorId, coCodigoOriginal], [NFormat(vRet.Dados.FornecedorId), vRet.Dados.CodigoOriginal]);
    if vLinha > 0 then begin
      sgValores.Row := vLinha;
      Exit;
    end;

    vLinha := sgValores.RowCount;
  end;

  sgValores.Cells[coFornecedor, vLinha]     := NFormat(vRet.Dados.FornecedorId) + ' - ' + vRet.Dados.NomeFornecedor;
  sgValores.Cells[coCodigoOriginal, vLinha] := vRet.Dados.CodigoOriginal;
  sgValores.Cells[coFornecedorId, vLinha]   := NFormat(vRet.Dados.FornecedorId);

  sgValores.RowCount := IIfInt(vLinha > 1, vLinha + 1, 2);
  sgValores.Row := vLinha;
  Self.SetFocus;
end;

procedure TFrCodigoOriginalFornecedores.setCodigosOriginais(pCodigos: TArray<RecProdutosFornCodOriginais>);
var
  i: Integer;
begin
  sgValores.ClearGrid();
  for i := Low(pCodigos) to High(pCodigos) do begin
    sgValores.Cells[coFornecedor, i + 1]     := NFormat(pCodigos[i].FornecedorId) + ' - ' + pCodigos[i].NomeFornecedor;
    sgValores.Cells[coCodigoOriginal, i + 1] := pCodigos[i].CodigoOriginal;
    sgValores.Cells[coFornecedorId, i + 1]   := NFormat(pCodigos[i].FornecedorId);
  end;
  sgValores.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(pCodigos) );
end;

procedure TFrCodigoOriginalFornecedores.sgValoresDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgValores.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

end.
