inherited FormRelacaoBloqueiosEstoques: TFormRelacaoBloqueiosEstoques
  Caption = 'Rela'#231#227'o de bloqueios de estoques'
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Left = 1
      Top = 480
      ExplicitLeft = 1
      ExplicitTop = 480
    end
    object miBaixarItensBloqueioSelecionado: TSpeedButton [2]
      Left = 0
      Top = 120
      Width = 117
      Height = 35
      Caption = 'Bx. bloqueio'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miBaixarItensBloqueioSelecionadoClick
    end
    object miCancelarItensBloqueioFocado: TSpeedButton [3]
      Left = 1
      Top = 175
      Width = 117
      Height = 35
      Caption = 'Cancelar bloqueio'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miBaixarItensBloqueioSelecionadoClick
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Left = 6
      Top = 512
      ExplicitLeft = 6
      ExplicitTop = 512
    end
    object StaticTextLuka1: TStaticTextLuka
      Left = 0
      Top = 106
      Width = 118
      Height = 8
      Align = alCustom
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      Color = clSilver
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 1
      Transparent = False
      AsInt = 0
      TipoCampo = tcTexto
      CasasDecimais = 0
    end
    object StaticTextLuka2: TStaticTextLuka
      Left = 0
      Top = 241
      Width = 118
      Height = 8
      Align = alCustom
      Alignment = taCenter
      AutoSize = False
      BevelInner = bvNone
      Color = clSilver
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 2
      Transparent = False
      AsInt = 0
      TipoCampo = tcTexto
      CasasDecimais = 0
    end
  end
  inherited pcDados: TPageControl
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      inline FrProdutos: TFrProdutos
        Left = 2
        Top = 88
        Width = 318
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 88
        ExplicitWidth = 318
        inherited sgPesquisa: TGridLuka
          Width = 293
          ExplicitWidth = 293
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 318
          ExplicitWidth = 318
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 213
            ExplicitLeft = 213
          end
        end
        inherited pnPesquisa: TPanel
          Left = 293
          ExplicitLeft = 293
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrEmpresa: TFrEmpresas
        Left = 2
        Top = 0
        Width = 318
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 2
        ExplicitWidth = 318
        inherited sgPesquisa: TGridLuka
          Width = 293
          ExplicitWidth = 293
        end
        inherited PnTitulos: TPanel
          Width = 318
          ExplicitWidth = 318
          inherited lbNomePesquisa: TLabel
            Width = 47
            Caption = 'Empresa'
            ExplicitWidth = 47
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 213
            ExplicitLeft = 213
            inherited ckSuprimir: TCheckBox
              Visible = False
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 293
          ExplicitLeft = 293
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckSomenteAtivos: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      inline FrDataCadastroProduto: TFrDataInicialFinal
        Left = 376
        Top = 15
        Width = 236
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 376
        ExplicitTop = 15
        ExplicitWidth = 236
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 96
          Height = 14
          Caption = 'Data do bloqueio'
          ExplicitWidth = 96
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Left = 111
          Height = 22
          ExplicitLeft = 111
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrLocaisBloqueio: TFrLocais
        Left = 0
        Top = 176
        Width = 318
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitTop = 176
        ExplicitWidth = 318
        inherited sgPesquisa: TGridLuka
          Width = 293
          ExplicitWidth = 293
        end
        inherited PnTitulos: TPanel
          Width = 318
          ExplicitWidth = 318
          inherited lbNomePesquisa: TLabel
            Width = 104
            Caption = 'Locais de bloqueio'
            ExplicitWidth = 104
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 213
            ExplicitLeft = 213
          end
        end
        inherited pnPesquisa: TPanel
          Left = 293
          ExplicitLeft = 293
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrUsuariosCadastro: TFrFuncionarios
        Left = 0
        Top = 349
        Width = 320
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitTop = 349
        ExplicitWidth = 320
        ExplicitHeight = 81
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 64
          ExplicitWidth = 295
          ExplicitHeight = 64
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 110
            Caption = 'Usu'#225'rio de inser'#231#227'o'
            ExplicitWidth = 110
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 65
          ExplicitLeft = 295
          ExplicitHeight = 65
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckBuscarSomenteAtivos: TCheckBox
          Checked = False
          State = cbUnchecked
        end
      end
      inline FrDataIbxbloqueio: TFrDataInicialFinal
        Left = 376
        Top = 72
        Width = 236
        Height = 42
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 376
        ExplicitTop = 72
        ExplicitWidth = 236
        ExplicitHeight = 42
        inherited Label1: TLabel
          Width = 146
          Height = 14
          Caption = 'Data da baixa do bloqueio'
          ExplicitWidth = 146
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Left = 111
          Height = 22
          ExplicitLeft = 111
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrUsuariodaBaixa: TFrFuncionarios
        Left = 0
        Top = 433
        Width = 320
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 6
        TabStop = True
        ExplicitTop = 433
        ExplicitWidth = 320
        ExplicitHeight = 81
        inherited sgPesquisa: TGridLuka
          Width = 295
          Height = 64
          ExplicitWidth = 295
          ExplicitHeight = 64
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 163
            Caption = 'Usu'#225'rio da baixa do bloqueio'
            ExplicitWidth = 163
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          Height = 65
          ExplicitLeft = 295
          ExplicitHeight = 65
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckBuscarSomenteAtivos: TCheckBox
          Checked = False
          State = cbUnchecked
        end
      end
      inline FrTipoBloqueioEstoque1: TFrTipoBloqueioEstoque
        Left = 2
        Top = 263
        Width = 320
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 7
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 263
        ExplicitWidth = 320
        inherited sgPesquisa: TGridLuka
          Width = 295
          ExplicitWidth = 295
        end
        inherited PnTitulos: TPanel
          Width = 320
          ExplicitWidth = 320
          inherited lbNomePesquisa: TLabel
            Width = 94
            ExplicitWidth = 94
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 215
            ExplicitLeft = 215
          end
        end
        inherited pnPesquisa: TPanel
          Left = 295
          ExplicitLeft = 295
        end
      end
      object gbStatus: TGroupBoxLuka
        Left = 376
        Top = 120
        Width = 194
        Height = 43
        Caption = '  Status  '
        TabOrder = 8
        OpcaoMarcarDesmarcar = False
        object ckAbertos: TCheckBoxLuka
          Left = 16
          Top = 16
          Width = 66
          Height = 17
          Caption = 'Abertos'
          Checked = True
          State = cbChecked
          TabOrder = 0
          CheckedStr = 'S'
          Valor = 'A'
        end
        object ckBaixados: TCheckBoxLuka
          Left = 85
          Top = 16
          Width = 84
          Height = 17
          Caption = 'Baixados'
          Checked = True
          State = cbChecked
          TabOrder = 1
          CheckedStr = 'S'
          Valor = 'B'
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object splSeparador: TSplitter
        Left = 0
        Top = 212
        Width = 884
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ResizeStyle = rsUpdate
      end
      object st1: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Bloqueios'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgBloqueios: TGridLuka
        Left = 0
        Top = 17
        Width = 884
        Height = 195
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 10
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        PopupMenu = pmOpcoes
        TabOrder = 1
        OnClick = sgBloqueiosClick
        OnDrawCell = sgBloqueiosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'C'#243'digo'
          'Empresa'
          'Status'
          'Custo'
          'Tipo de bloqueio'
          'Data/hora cadastro'
          'Usu'#225'rio cadastro'
          'Data/hora baixa'
          'Usu'#225'rio baixa'
          'Observa'#231#245'es')
        OnGetCellColor = sgBloqueiosGetCellColor
        Grid3D = False
        RealColCount = 15
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          43
          215
          110
          82
          179
          119
          150
          111
          136
          444)
      end
      object st2: TStaticTextLuka
        Left = 0
        Top = 218
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Itens do bloqueio'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 235
        Width = 884
        Height = 249
        Align = alClient
        ColCount = 12
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 3
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Custo'
          'Quantidade'
          'Custo Total'
          'Baixados'
          'Cancelados'
          'Saldo'
          'Und.'
          'Lote'
          'Local')
        Grid3D = False
        RealColCount = 13
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          55
          269
          154
          76
          76
          84
          68
          80
          70
          36
          82
          188)
      end
      object Panel1: TPanel
        Left = 0
        Top = 484
        Width = 884
        Height = 34
        Align = alBottom
        BevelKind = bkTile
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 4
        object sbAlterarTipoAcompanhamento: TSpeedButton
          Left = 9
          Top = 1
          Width = 192
          Height = 26
          BiDiMode = bdRightToLeft
          Caption = 'Alterar tipo de bloqueio'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = sbAlterarTipoAcompanhamentoClick
        end
      end
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 808
    Top = 112
  end
  object frxReport: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 45006.909558622700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 480
    Top = 96
    Datasets = <
      item
        DataSet = dsCapaBloqueio
        DataSetName = 'dsCapaBloqueio'
      end
      item
        DataSet = dsItensBloqueio
        DataSetName = 'dsItensBloqueio'
      end
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 71.811070000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133890000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000000000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000000000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133890000000000000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133890000000000000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 445.984540000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 109.606370000000000000
        Top = 151.181200000000000000
        Width = 718.110700000000000000
        DataSet = dsCapaBloqueio
        DataSetName = 'dsCapaBloqueio'
        RowCount = 0
        object Memo18: TfrxMemoView
          Left = 2.779530000000000000
          Top = 2.000000000000000000
          Width = 117.165430000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'C'#243'digo: [dsCapaBloqueio."codigo"]')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Top = 90.708720000000000000
          Width = 718.110700000000000000
          Height = 18.897650000000000000
          Fill.BackColor = cl3DLight
        end
        object dsEntregacliente: TfrxMemoView
          Left = 127.504020000000000000
          Top = 2.000000000000000000
          Width = 476.220780000000000000
          Height = 18.897650000000000000
          DataSet = dsCapaBloqueio
          DataSetName = 'dsCapaBloqueio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Empresa: [dsCapaBloqueio."empresa"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 2.779530000000000000
          Top = 91.708720000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 48.133890000000000000
          Top = 91.708720000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 237.110390000000000000
          Top = 91.708720000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Marca')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 395.850650000000000000
          Top = 91.708720000000000000
          Width = 41.574830000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 668.976810000000000000
          Top = 91.708720000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Width = 718.110236220472000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo5: TfrxMemoView
          Left = 2.779530000000000000
          Top = 22.897650000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DataSet = dsCapaBloqueio
          DataSetName = 'dsCapaBloqueio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Status: [dsCapaBloqueio."status"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 273.346630000000000000
          Top = 22.677180000000000000
          Width = 113.385900000000000000
          Height = 18.897637800000000000
          DisplayFormat.FormatStr = '#,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[dsCapaBloqueio."custo"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 415.748300000000000000
          Top = 20.897650000000000000
          Width = 298.582870000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Tipo de bloqueio: [dsCapaBloqueio."tipo_bloqueio"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 2.779530000000000000
          Top = 45.354360000000000000
          Width = 328.819110000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data/hora cadastro: [dsCapaBloqueio."hora_cadastro"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 336.378170000000000000
          Top = 45.354360000000000000
          Width = 377.953000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Usu'#225'rio cadastro: [dsCapaBloqueio."usuario_cadastro"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 2.779530000000000000
          Top = 68.031540000000000000
          Width = 328.819110000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Data/hora baixa: [dsCapaBloqueio."hora_baixa"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 336.378170000000000000
          Top = 68.031540000000000000
          Width = 377.953000000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Usu'#225'rio baixa: [dsCapaBloqueio."usuario_baixa"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 343.937230000000000000
          Top = 91.708720000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Custo')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 437.425480000000000000
          Top = 91.708720000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Custo Total')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 498.118430000000000000
          Top = 91.708720000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Baixados')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 614.063390000000000000
          Top = 91.708720000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 550.590910000000000000
          Top = 91.708720000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Cancelados')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 210.094620000000000000
          Top = 22.677180000000000000
          Width = 60.472480000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Custo:')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 283.464750000000000000
        Width = 718.110700000000000000
        DataSet = dsItensBloqueio
        DataSetName = 'dsItensBloqueio'
        RowCount = 0
        object dsItensproduto: TfrxMemoView
          Left = 2.779530000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DataSet = dsItensBloqueio
          DataSetName = 'dsItensBloqueio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[dsItensBloqueio."produto"]')
          ParentFont = False
        end
        object dsItensnome: TfrxMemoView
          Left = 48.133890000000000000
          Width = 185.196970000000000000
          Height = 18.897650000000000000
          DataField = 'nome'
          DataSet = dsItensBloqueio
          DataSetName = 'dsItensBloqueio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[dsItensBloqueio."nome"]')
          ParentFont = False
        end
        object dsItensmarca: TfrxMemoView
          Left = 237.110390000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          DataField = 'marca'
          DataSet = dsItensBloqueio
          DataSetName = 'dsItensBloqueio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[dsItensBloqueio."marca"]')
          ParentFont = False
        end
        object dsItensqtde: TfrxMemoView
          Left = 388.291590000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'qtde'
          DataSet = dsItensBloqueio
          DataSetName = 'dsItensBloqueio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[dsItensBloqueio."qtde"]')
          ParentFont = False
        end
        object dsItenslote: TfrxMemoView
          Left = 672.756340000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DataField = 'lote'
          DataSet = dsItensBloqueio
          DataSetName = 'dsItensBloqueio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[dsItensBloqueio."lote"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 336.378170000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataSet = dsItensBloqueio
          DataSetName = 'dsItensBloqueio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[dsItensBloqueio."custo"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 441.205010000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataSet = dsItensBloqueio
          DataSetName = 'dsItensBloqueio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[dsItensBloqueio."custos_total"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 501.897960000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DataSet = dsItensBloqueio
          DataSetName = 'dsItensBloqueio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[dsItensBloqueio."baixados"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 614.063390000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          DataSet = dsItensBloqueio
          DataSetName = 'dsItensBloqueio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[dsItensBloqueio."saldos"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 554.370440000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataSet = dsItensBloqueio
          DataSetName = 'dsItensBloqueio'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[dsItensBloqueio."cancelados"]')
          ParentFont = False
        end
      end
      object Memo15: TfrxMemoView
        Left = 370.393940000000000000
        Top = -49.133890000000000000
        Width = 75.590600000000000000
        Height = 11.338590000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
        ParentFont = False
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 60.472480000000000000
        Top = 362.834880000000000000
        Width = 718.110700000000000000
        object Memo42: TfrxMemoView
          Left = 306.141930000000000000
          Top = 34.015770000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '#,##0.000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<dsCapaBloqueio."custo">)]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          Top = 7.559060000000000000
          Width = 718.110700000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo41: TfrxMemoView
          Left = 215.433210000000000000
          Top = 34.015770000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Total.....:')
          ParentFont = False
        end
      end
    end
  end
  object dsCapaBloqueio: TfrxUserDataSet
    RangeBegin = rbCurrent
    RangeEnd = reCurrent
    UserName = 'dsCapaBloqueio'
    OnFirst = dsCapaBloqueioFirst
    OnNext = dsCapaBloqueioNext
    OnPrior = dsCapaBloqueioPrior
    Fields.Strings = (
      'codigo'
      'empresa'
      'status'
      'custo'
      'tipo_bloqueio'
      'hora_cadastro'
      'usuario_cadastro'
      'hora_baixa'
      'usuario_baixa')
    OnGetValue = dsCapaBloqueioGetValue
    Left = 480
    Top = 144
  end
  object dsItensBloqueio: TfrxUserDataSet
    RangeBegin = rbCurrent
    RangeEnd = reCurrent
    UserName = 'dsItensBloqueio'
    OnCheckEOF = dsItensBloqueioCheckEOF
    OnFirst = dsItensBloqueioFirst
    OnNext = dsItensBloqueioNext
    OnPrior = dsItensBloqueioPrior
    Fields.Strings = (
      'produto'
      'nome'
      'marca'
      'custo'
      'qtde'
      'custos_total'
      'baixados'
      'cancelados'
      'saldos'
      'unid'
      'lote'
      'local')
    OnGetValue = dsItensBloqueioGetValue
    Left = 480
    Top = 200
  end
end
