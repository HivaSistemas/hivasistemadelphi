unit Buscar.Enderecos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _RecordsCadastros,
  Vcl.Buttons, Vcl.ExtCtrls, _Biblioteca, _Sessao, _DiversosEnderecos;

type
  TFormBuscarEnderecos = class(TFormHerancaFinalizar)
    sgEnderecos: TGridLuka;
    procedure sgEnderecosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEnderecosDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function Buscar(pCadastroId: Integer; pSomenteEnderecoEntrega: Boolean): TRetornoTelaFinalizar<RecDiversosEnderecos>;

implementation

{$R *.dfm}

const
  coTipoEndereco          = 0;
  coLogradouro            = 1;
  coComplemento           = 2;
  coNumero                = 3;
  coBairro                = 4;
  coPontoReferencia       = 5;
  coCep                   = 6;
  coInscricaoEstadual     = 7;
  coTipoEnderecoSintetico = 8;
  coBairroId              = 9;

function Buscar(pCadastroId: Integer; pSomenteEnderecoEntrega: Boolean): TRetornoTelaFinalizar<RecDiversosEnderecos>;
var
  i: Integer;
  vForm: TFormBuscarEnderecos;
  vEnderecos: TArray<RecDiversosEnderecos>;
begin
  Result.RetTela := trCancelado;

  vEnderecos := _DiversosEnderecos.BuscarDiversosEnderecos(Sessao.getConexaoBanco, 0, [pCadastroId], pSomenteEnderecoEntrega);
  if vEnderecos = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vForm := TFormBuscarEnderecos.Create(nil);

  for i := Low(vEnderecos) to High(vEnderecos) do begin
    vForm.sgEnderecos.Cells[coTipoEndereco, i + 1]      := vEnderecos[i].TipoEnderecoAnalitico;
    vForm.sgEnderecos.Cells[coLogradouro, i + 1]        := vEnderecos[i].logradouro;
    vForm.sgEnderecos.Cells[coComplemento, i + 1]       := vEnderecos[i].complemento;
    vForm.sgEnderecos.Cells[coNumero, i + 1]            := vEnderecos[i].numero;
    vForm.sgEnderecos.Cells[coBairro, i + 1]            := NFormat(vEnderecos[i].bairro_id) + ' - ' + vEnderecos[i].nome_bairro + ' / ' + vEnderecos[i].nome_cidade;
    vForm.sgEnderecos.Cells[coPontoReferencia, i + 1]   := vEnderecos[i].ponto_referencia;
    vForm.sgEnderecos.Cells[coCep, i + 1]               := vEnderecos[i].cep;
    vForm.sgEnderecos.Cells[coInscricaoEstadual, i + 1] := vEnderecos[i].InscricaoEstadual;
    vForm.sgEnderecos.Cells[coBairroId, i + 1]          := NFormat(vEnderecos[i].bairro_id);
  end;
  vForm.sgEnderecos.SetLinhasGridPorTamanhoVetor( Length(vEnderecos) );

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.logradouro        := vForm.sgEnderecos.Cells[coLogradouro, vForm.sgEnderecos.Row];
    Result.Dados.complemento       := vForm.sgEnderecos.Cells[coComplemento, vForm.sgEnderecos.Row];
    Result.Dados.numero            := vForm.sgEnderecos.Cells[coNumero, vForm.sgEnderecos.Row];
    Result.Dados.bairro_id         := SFormatInt(vForm.sgEnderecos.Cells[coBairroId, vForm.sgEnderecos.Row]);
    Result.Dados.ponto_referencia  := vForm.sgEnderecos.Cells[coPontoReferencia, vForm.sgEnderecos.Row];
    Result.Dados.cep               := vForm.sgEnderecos.Cells[coCep, vForm.sgEnderecos.Row];
    Result.Dados.InscricaoEstadual := vForm.sgEnderecos.Cells[coInscricaoEstadual, vForm.sgEnderecos.Row];
  end;
end;

procedure TFormBuscarEnderecos.FormShow(Sender: TObject);
begin
  inherited;
  SetarFoco(sgEnderecos);
end;

procedure TFormBuscarEnderecos.sgEnderecosDblClick(Sender: TObject);
begin
  inherited;
  sbFinalizarClick(Sender);
end;

procedure TFormBuscarEnderecos.sgEnderecosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgEnderecos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

end.
