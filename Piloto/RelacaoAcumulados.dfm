inherited FormRelacaoAcumulados: TFormRelacaoAcumulados
  Caption = 'Rela'#231'ao de acumulados'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    inherited sbImprimir: TSpeedButton
      Top = 55
      ExplicitTop = 55
    end
  end
  inherited pcDados: TPageControl
    ActivePage = tsResultado
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      inline FrClientes: TFrClientes
        Left = 1
        Top = 84
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 84
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 45
            ExplicitWidth = 45
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrEmpresas: TFrEmpresas
        Left = 1
        Top = 0
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        TabStop = True
        ExplicitLeft = 1
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrDataFechamento: TFrDataInicialFinal
        Left = 416
        Top = 0
        Width = 217
        Height = 41
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        TabStop = True
        ExplicitLeft = 416
        ExplicitWidth = 217
        inherited Label1: TLabel
          Width = 111
          Height = 14
          Caption = 'Data de fechamento'
          ExplicitWidth = 111
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrCondicoesPagamento: TFrCondicoesPagamento
        Left = 0
        Top = 170
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        ExplicitTop = 170
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 138
            ExplicitWidth = 138
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      object gbFormasPagamento: TGroupBoxLuka
        Left = 418
        Top = 86
        Width = 225
        Height = 81
        Caption = '  Formas de pagamento    '
        TabOrder = 4
        OpcaoMarcarDesmarcar = True
        object ckCartao: TCheckBox
          Left = 14
          Top = 58
          Width = 88
          Height = 17
          Caption = 'Cart'#227'o'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object ckFinanceira: TCheckBox
          Left = 106
          Top = 17
          Width = 108
          Height = 17
          Caption = 'Financeira'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object ckCheque: TCheckBox
          Left = 14
          Top = 37
          Width = 88
          Height = 17
          Caption = 'Cheque'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
        object ckCobranca: TCheckBox
          Left = 106
          Top = 37
          Width = 88
          Height = 17
          Caption = 'Cobran'#231'a'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        object ckDinheiro: TCheckBoxLuka
          Left = 14
          Top = 17
          Width = 85
          Height = 17
          Caption = 'Dinheiro'
          Checked = True
          State = cbChecked
          TabOrder = 4
          CheckedStr = 'S'
        end
        object ckPix: TCheckBox
          Left = 106
          Top = 58
          Width = 88
          Height = 17
          Caption = 'Pix'
          Checked = True
          State = cbChecked
          TabOrder = 5
        end
      end
      inline FrPedido: TFrameInteiros
        Left = 416
        Top = 213
        Width = 132
        Height = 84
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 213
        inherited Panel1: TPanel
          Caption = 'C'#243'digo do pedido'
        end
      end
      inline FrCodigoTurno: TFrameInteiros
        Left = 558
        Top = 213
        Width = 132
        Height = 84
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 6
        TabStop = True
        ExplicitLeft = 558
        ExplicitTop = 213
        inherited Panel1: TPanel
          Caption = 'C'#243'digo do turno'
        end
      end
      inline FrVendedores: TFrVendedores
        Left = 0
        Top = 425
        Width = 402
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 7
        TabStop = True
        Visible = False
        ExplicitTop = 425
        ExplicitWidth = 402
        inherited sgPesquisa: TGridLuka
          Width = 377
          ExplicitWidth = 377
        end
        inherited PnTitulos: TPanel
          Width = 402
          ExplicitWidth = 402
          inherited lbNomePesquisa: TLabel
            Width = 65
            ExplicitWidth = 65
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 297
            ExplicitLeft = 297
          end
        end
        inherited pnPesquisa: TPanel
          Left = 377
          ExplicitLeft = 377
        end
      end
      inline FrDataRecebimento: TFrDataInicialFinal
        Left = 416
        Top = 42
        Width = 217
        Height = 41
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 8
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 42
        ExplicitWidth = 217
        inherited Label1: TLabel
          Width = 115
          Height = 14
          Caption = 'Data de recebimento'
          ExplicitWidth = 115
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 0
        Top = 255
        Width = 401
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 9
        TabStop = True
        ExplicitTop = 255
        ExplicitWidth = 401
        inherited sgPesquisa: TGridLuka
          Width = 376
          ExplicitWidth = 376
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 401
          ExplicitWidth = 401
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 296
            ExplicitLeft = 296
          end
        end
        inherited pnPesquisa: TPanel
          Left = 376
          ExplicitLeft = 376
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
      object gbStatus: TGroupBoxLuka
        Left = 418
        Top = 172
        Width = 225
        Height = 39
        Caption = '  Status do acumulado  '
        TabOrder = 10
        OpcaoMarcarDesmarcar = True
        object ckStatusEmAberto: TCheckBox
          Left = 14
          Top = 16
          Width = 88
          Height = 17
          Caption = 'Fechado'
          TabOrder = 0
        end
        object ckStatusRecebido: TCheckBox
          Left = 124
          Top = 16
          Width = 73
          Height = 17
          Caption = 'Recebido'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
      end
      inline FrAcumulado: TFrameInteiros
        Left = 416
        Top = 303
        Width = 132
        Height = 84
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 11
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 303
        inherited Panel1: TPanel
          Caption = 'C'#243'digo do acumulado'
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object splSeparador: TSplitter
        Left = 0
        Top = 212
        Width = 884
        Height = 6
        Cursor = crVSplit
        Align = alTop
        MinSize = 6
        ResizeStyle = rsUpdate
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Acumulados'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgAcumulados: TGridLuka
        Left = 0
        Top = 17
        Width = 884
        Height = 195
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 16
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goFixedRowClick]
        PopupMenu = pmOpcoesAcumulados
        TabOrder = 1
        OnClick = sgAcumuladosClick
        OnDblClick = sgAcumuladosDblClick
        OnDrawCell = sgAcumuladosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Acumulado'
          'Cliente'
          'Status'
          'Data\hora fechamento'
          'Data\hora recebimento'
          'Condi'#231#227'o de pagamento'
          'Valor produtos'
          'Valor total'
          'Valor out. desp.'
          'Valor desconto'
          'Valor juros'
          'Valor luc.bruto'
          '% Lucro bruto'
          'Valor luc.l'#237'quido'
          '% Lucro l'#237'quido'
          'Empresa')
        OnGetCellColor = sgAcumuladosGetCellColor
        Grid3D = False
        RealColCount = 16
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          69
          243
          148
          132
          137
          139
          124
          92
          92
          91
          75
          86
          81
          98
          92
          162)
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 0
        Top = 218
        Width = 884
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Itens do acumulado'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object sgItens: TGridLuka
        Left = 2
        Top = 236
        Width = 884
        Height = 206
        ColCount = 13
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 3
        OnDrawCell = sgItensDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Pre'#231'o unit.'
          'Quantidade'
          'Und.'
          'Valor total'
          'Valor out.desp.'
          'Valor desc.'
          'Valor luc.bruto'
          '% Lucro bruto'
          'Valor luc.l'#237'quido'
          '% Lucro l'#237'quido')
        Grid3D = False
        RealColCount = 13
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          269
          154
          69
          74
          33
          75
          89
          70
          87
          78
          98
          88)
      end
      object Panel1: TPanel
        Left = 0
        Top = 444
        Width = 884
        Height = 74
        BevelKind = bkTile
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 4
        object miReimprimirComprovantePagamento: TSpeedButton
          Left = 9
          Top = 2
          Width = 200
          Height = 26
          BiDiMode = bdRightToLeft
          Caption = 'Reimprimir comp. de pagamento'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miReimprimirComprovantePagamentoClick
        end
        object miReimprimirDuplicatasMercantil: TSpeedButton
          Left = 9
          Top = 34
          Width = 139
          Height = 26
          BiDiMode = bdRightToLeft
          Caption = 'Reimprimir duplicata'
          Flat = True
          NumGlyphs = 2
          ParentBiDiMode = False
          OnClick = miReimprimirDuplicatasMercantilClick
        end
      end
    end
  end
  object pmOpcoesAcumulados: TPopupMenu
    Left = 936
    Top = 472
    object miN1: TMenuItem
      Caption = '-'
    end
    object miAlterarIndiceDescontoVenda: TMenuItem
      Caption = 'Alterar '#237'ndice de desconto de venda'
      Visible = False
      OnClick = miAlterarIndiceDescontoVendaClick
    end
  end
  object frxReport: TfrxReport
    Version = '5.2.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 45394.463961678240000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 208
    Top = 168
    Datasets = <
      item
        DataSet = frxClientes
        DataSetName = 'frxdstClientes'
      end
      item
        DataSet = dstAcumulados
        DataSetName = 'frxdstAcumulados'
      end
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstItensAcumulados
        DataSetName = 'frxdstItensAcumulados'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 68.031540000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000000000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133890000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000000000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133890000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000000000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000000000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000000000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000000000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133890000000000000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133890000000000000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 147.401670000000000000
        Width = 718.110700000000000000
        DataSet = frxClientes
        DataSetName = 'frxdstClientes'
        RowCount = 0
        object Shape1: TfrxShapeView
          Top = 1.779530000000000000
          Width = 718.110700000000000000
          Height = 26.456710000000000000
          Fill.BackColor = clScrollBar
        end
        object Memo25: TfrxMemoView
          Left = 51.779532440000000000
          Top = 5.000000000000000000
          Width = 495.118430000000000000
          Height = 15.118120000000000000
          DataSet = frxClientes
          DataSetName = 'frxdstClientes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxdstClientes."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 2.645666850000000000
          Top = 5.000000000000000000
          Width = 45.354335590000000000
          Height = 15.118120000000000000
          DataSet = frxClientes
          DataSetName = 'frxdstClientes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstClientes."CLIENTE_ID"] -')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 38.173252760000000000
        Top = 389.291590000000000000
        Width = 718.110700000000000000
        object Line1: TfrxLineView
          Top = 3.779529999999970000
          Width = 718.110700000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object mmValorProduto: TfrxMemoView
          Left = 3.779530000000000000
          Top = 11.338590000000000000
          Width = 498.897960000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Courier'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Valor Total.....: ')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 449.764070000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 196.535560000000000000
        Width = 718.110700000000000000
        DataSet = dstAcumulados
        DataSetName = 'frxdstAcumulados'
        RowCount = 0
        object Shape3: TfrxShapeView
          Width = 718.110700000000000000
          Height = 41.574830000000000000
          Fill.BackColor = cl3DLight
        end
        object frxdstAcumuladosSTATUS: TfrxMemoView
          Left = 491.338900000000000000
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          DataField = 'STATUS'
          DataSet = dstAcumulados
          DataSetName = 'frxdstAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstAcumulados."STATUS"]')
          ParentFont = False
        end
        object frxdstAcumuladosACUMULADO_ID: TfrxMemoView
          Left = 74.370130000000000000
          Top = 3.779530000000000000
          Width = 79.370130000000000000
          Height = 15.118120000000000000
          DataField = 'ACUMULADO_ID'
          DataSet = dstAcumulados
          DataSetName = 'frxdstAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstAcumulados."ACUMULADO_ID"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Acumulado:')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 443.205010000000000000
          Top = 3.779530000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Status:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 102.504020000000000000
          Top = 22.677180000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_OUTRAS_DESPESAS'
          DataSet = dstAcumulados
          DataSetName = 'frxdstAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstAcumulados."VALOR_OUTRAS_DESPESAS"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 4.897650000000000000
          Top = 22.677180000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Outras despesas:')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 270.905690000000000000
          Top = 22.677180000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_DESCONTO'
          DataSet = dstAcumulados
          DataSetName = 'frxdstAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstAcumulados."VALOR_DESCONTO"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 177.637910000000000000
          Top = 22.677180000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor Desconto:')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 422.086890000000000000
          Top = 22.677180000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_JUROS'
          DataSet = dstAcumulados
          DataSetName = 'frxdstAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstAcumulados."VALOR_JUROS"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 347.716760000000000000
          Top = 22.677180000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor Juros:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 573.268090000000000000
          Top = 22.677180000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          DataField = 'VALOR_TOTAL_PRODUTOS'
          DataSet = dstAcumulados
          DataSetName = 'frxdstAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstAcumulados."VALOR_TOTAL_PRODUTOS"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 498.897960000000000000
          Top = 22.677180000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor Total:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 276.244280000000000000
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          DataSet = dstAcumulados
          DataSetName = 'frxdstAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstAcumulados."DATA_HORA_RECEBIMENTO"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 161.519790000000000000
          Top = 3.779530000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Data do acerto:')
          ParentFont = False
        end
      end
      object SubdetailData1: TfrxSubdetailData
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        DataSet = dstItensAcumulados
        DataSetName = 'frxdstItensAcumulados'
        RowCount = 0
        object Shape5: TfrxShapeView
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          Fill.BackColor = clMenu
        end
        object frxdstItensAcumuladosPRODUTO_ID: TfrxMemoView
          Left = 3.779530000000000000
          Top = 2.000000000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'PRODUTO_ID'
          DataSet = dstItensAcumulados
          DataSetName = 'frxdstItensAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstItensAcumulados."PRODUTO_ID"]')
          ParentFont = False
        end
        object frxdstItensAcumuladosNOME_PRODUTO: TfrxMemoView
          Left = 52.913420000000000000
          Top = 2.000000000000000000
          Width = 309.921460000000000000
          Height = 18.897650000000000000
          DataField = 'NOME_PRODUTO'
          DataSet = dstItensAcumulados
          DataSetName = 'frxdstItensAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstItensAcumulados."NOME_PRODUTO"]')
          ParentFont = False
        end
        object frxdstItensAcumuladosPRECO_UNITARIO: TfrxMemoView
          Left = 362.834880000000000000
          Top = 2.000000000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          DataField = 'PRECO_UNITARIO'
          DataSet = dstItensAcumulados
          DataSetName = 'frxdstItensAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstItensAcumulados."PRECO_UNITARIO"]')
          ParentFont = False
        end
        object frxdstItensAcumuladosQUANTIDADE: TfrxMemoView
          Left = 430.866420000000000000
          Top = 2.000000000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DataField = 'QUANTIDADE'
          DataSet = dstItensAcumulados
          DataSetName = 'frxdstItensAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstItensAcumulados."QUANTIDADE"]')
          ParentFont = False
        end
        object frxdstItensAcumuladosVALOR_TOTAL: TfrxMemoView
          Left = 646.299630000000000000
          Top = 2.000000000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_TOTAL'
          DataSet = dstItensAcumulados
          DataSetName = 'frxdstItensAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstItensAcumulados."VALOR_TOTAL"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 480.000310000000000000
          Top = 2.000000000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_TOTAL_DESCONTO'
          DataSet = dstItensAcumulados
          DataSetName = 'frxdstItensAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstItensAcumulados."VALOR_TOTAL_DESCONTO"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 555.590910000000000000
          Top = 2.000000000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          DataField = 'VALOR_TOTAL_OUTRAS_DESPESAS'
          DataSet = dstItensAcumulados
          DataSetName = 'frxdstItensAcumulados'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstItensAcumulados."VALOR_TOTAL_OUTRAS_DESPESAS"]')
          ParentFont = False
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 260.787570000000000000
        Width = 718.110700000000000000
        KeepChild = True
        object Shape4: TfrxShapeView
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          Fill.BackColor = clMenu
        end
        object Memo22: TfrxMemoView
          Left = 3.779530000000000000
          Top = 2.000000000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 52.913420000000000000
          Top = 2.000000000000000000
          Width = 309.921460000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 362.834880000000000000
          Top = 2.000000000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o Unt.')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 427.086890000000000000
          Top = 2.000000000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde.')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 646.299630000000000000
          Top = 2.000000000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Total')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 480.000310000000000000
          Top = 2.000000000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr Desconto')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 555.590910000000000000
          Top = 2.000000000000000000
          Width = 86.929190000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr Outras Desp.')
          ParentFont = False
        end
      end
    end
  end
  object qAcumulados: TOraQuery
    SQL.Strings = (
      'select '
      '  ACU.CLIENTE_ID,'
      '  ACU.ACUMULADO_ID, '
      '  ACU.CONDICAO_ID, '
      '  CON.NOME as NOME_CONDICAO_PAGAMENTO, '
      '  ACU.DATA_HORA_FECHAMENTO, '
      '  ACU.DATA_HORA_RECEBIMENTO, '
      '  ACU.VALOR_OUTRAS_DESPESAS,'
      '  ACU.VALOR_DESCONTO,'
      '  ACU.VALOR_JUROS,'
      '  ACU.VALOR_TOTAL_PRODUTOS,'
      '  case'
      '    when ACU.STATUS = '#39'CA'#39' then '#39'Cancelado'#39
      '    when ACU.STATUS = '#39'AR'#39' then '#39'Aguardando Recebimento'#39
      '    when ACU.STATUS = '#39'RE'#39' then '#39'Recebido'#39
      '  end as STATUS,'
      '  ACU.VALOR_TOTAL'
      'from '
      '  ACUMULADOS ACU '
      ''
      'inner join CADASTROS CAD '
      'on ACU.CLIENTE_ID = CAD.CADASTRO_ID '
      ''
      'inner join FUNCIONARIOS FFE '
      'on ACU.USUARIO_FECHAMENTO_ID = FFE.FUNCIONARIO_ID '
      ''
      'inner join CONDICOES_PAGAMENTO CON '
      'on ACU.CONDICAO_ID = CON.CONDICAO_ID '
      ''
      'inner join EMPRESAS EMP '
      'on ACU.EMPRESA_ID = EMP.EMPRESA_ID '
      ''
      'left join FUNCIONARIOS FRE '
      'on ACU.USUARIO_RECEBIMENTO_ID = FRE.FUNCIONARIO_ID '
      ''
      'left join INDICES_DESCONTOS_VENDA IDV '
      'on ACU.INDICE_DESCONTO_VENDA_ID = IDV.INDICE_ID')
    MasterSource = dsClientesAcumulados
    MasterFields = 'CLIENTE_ID'
    DetailFields = 'CLIENTE_ID'
    Left = 210
    Top = 345
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'CLIENTE_ID'
        Value = nil
      end>
    object qAcumuladosACUMULADO_ID: TFloatField
      FieldName = 'ACUMULADO_ID'
      Required = True
    end
    object qAcumuladosCONDICAO_ID: TIntegerField
      FieldName = 'CONDICAO_ID'
      Required = True
    end
    object qAcumuladosNOME_CONDICAO_PAGAMENTO: TStringField
      FieldName = 'NOME_CONDICAO_PAGAMENTO'
      Size = 60
    end
    object qAcumuladosDATA_HORA_FECHAMENTO: TDateTimeField
      FieldName = 'DATA_HORA_FECHAMENTO'
      Required = True
    end
    object qAcumuladosDATA_HORA_RECEBIMENTO: TDateTimeField
      FieldName = 'DATA_HORA_RECEBIMENTO'
    end
    object qAcumuladosSTATUS: TStringField
      FieldName = 'STATUS'
      Required = True
      FixedChar = True
      Size = 2
    end
    object qAcumuladosVALOR_TOTAL: TFloatField
      FieldName = 'VALOR_TOTAL'
      Required = True
      currency = True
    end
    object qAcumuladosCLIENTE_ID: TFloatField
      FieldName = 'CLIENTE_ID'
      Required = True
    end
    object qAcumuladosVALOR_OUTRAS_DESPESAS: TFloatField
      FieldName = 'VALOR_OUTRAS_DESPESAS'
      Required = True
      currency = True
    end
    object qAcumuladosVALOR_DESCONTO: TFloatField
      FieldName = 'VALOR_DESCONTO'
      Required = True
      currency = True
    end
    object qAcumuladosVALOR_JUROS: TFloatField
      FieldName = 'VALOR_JUROS'
      Required = True
      currency = True
    end
    object qAcumuladosVALOR_TOTAL_PRODUTOS: TFloatField
      FieldName = 'VALOR_TOTAL_PRODUTOS'
      Required = True
      currency = True
    end
  end
  object dstAcumulados: TfrxDBDataset
    UserName = 'frxdstAcumulados'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ACUMULADO_ID=ACUMULADO_ID'
      'CONDICAO_ID=CONDICAO_ID'
      'NOME_CONDICAO_PAGAMENTO=NOME_CONDICAO_PAGAMENTO'
      'DATA_HORA_FECHAMENTO=DATA_HORA_FECHAMENTO'
      'DATA_HORA_RECEBIMENTO=DATA_HORA_RECEBIMENTO'
      'STATUS=STATUS'
      'VALOR_TOTAL=VALOR_TOTAL'
      'CLIENTE_ID=CLIENTE_ID'
      'VALOR_OUTRAS_DESPESAS=VALOR_OUTRAS_DESPESAS'
      'VALOR_DESCONTO=VALOR_DESCONTO'
      'VALOR_JUROS=VALOR_JUROS'
      'VALOR_TOTAL_PRODUTOS=VALOR_TOTAL_PRODUTOS')
    DataSet = qAcumulados
    BCDToCurrency = False
    Left = 210
    Top = 298
  end
  object dsClientesAcumulados: TDataSource
    DataSet = qClientes
    Left = 134
    Top = 401
  end
  object qClientes: TOraQuery
    SQL.Strings = (
      'select distinct'
      'ACU.CLIENTE_ID,'
      'CAD.NOME_FANTASIA,'
      'CAD.RAZAO_SOCIAL '
      'from '
      'ACUMULADOS ACU '
      ''
      'inner join CADASTROS CAD'
      'ON CAD.CADASTRO_ID = ACU.CLIENTE_ID'
      ''
      'where 1 = 1')
    Left = 136
    Top = 344
    object qClientesCLIENTE_ID: TFloatField
      FieldName = 'CLIENTE_ID'
      Required = True
    end
    object qClientesNOME_FANTASIA: TStringField
      FieldName = 'NOME_FANTASIA'
      Size = 100
    end
    object qClientesRAZAO_SOCIAL: TStringField
      FieldName = 'RAZAO_SOCIAL'
      Size = 100
    end
  end
  object frxClientes: TfrxDBDataset
    UserName = 'frxdstClientes'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CLIENTE_ID=CLIENTE_ID'
      'NOME_FANTASIA=NOME_FANTASIA'
      'RAZAO_SOCIAL=RAZAO_SOCIAL')
    DataSet = qClientes
    BCDToCurrency = False
    Left = 134
    Top = 297
  end
  object dsItensAcumulados: TDataSource
    DataSet = qAcumulados
    Left = 302
    Top = 401
  end
  object qItensAcumulados: TOraQuery
    SQL.Strings = (
      'select '
      '  ITE.ACUMULADO_ID, '
      '  ITE.PRODUTO_ID, '
      '  PRO.NOME as NOME_PRODUTO, '
      '  ITE.PRECO_UNITARIO, '
      '  ITE.QUANTIDADE, '
      '  ITE.VALOR_TOTAL, '
      '  ITE.VALOR_TOTAL_DESCONTO, '
      '  ITE.VALOR_TOTAL_OUTRAS_DESPESAS, '
      '  ITE.VALOR_TOTAL_FRETE, '
      '  PRO.MARCA_ID, '
      '  MAR.NOME as NOME_MARCA, '
      '  PRO.UNIDADE_VENDA, '
      ''
      '  ITE.PRECO_FINAL, '
      '  ITE.PRECO_FINAL_MEDIO, '
      '  ITE.PRECO_LIQUIDO, '
      '  ITE.PRECO_LIQUIDO_MEDIO, '
      '  ITE.CMV, '
      '  ITE.CUSTO_ULTIMO_PEDIDO, '
      '  ITE.CUSTO_PEDIDO_MEDIO, '
      '  ITE.VALOR_IMPOSTOS, '
      '  ITE.VALOR_ENCARGOS, '
      '  ITE.VALOR_CUSTO_VENDA, '
      '  ITE.VALOR_CUSTO_FINAL '
      'from '
      '  ACUMULADOS_ITENS ITE '
      ''
      'inner join PRODUTOS PRO '
      'on ITE.PRODUTO_ID = PRO.PRODUTO_ID '
      ''
      'inner join MARCAS MAR '
      'on PRO.MARCA_ID = MAR.MARCA_ID')
    MasterSource = dsItensAcumulados
    MasterFields = 'ACUMULADO_ID'
    DetailFields = 'ACUMULADO_ID'
    Left = 301
    Top = 345
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ACUMULADO_ID'
        Value = nil
      end>
    object qItensAcumuladosACUMULADO_ID: TFloatField
      FieldName = 'ACUMULADO_ID'
      Required = True
    end
    object qItensAcumuladosPRODUTO_ID: TFloatField
      FieldName = 'PRODUTO_ID'
      Required = True
    end
    object qItensAcumuladosNOME_PRODUTO: TStringField
      FieldName = 'NOME_PRODUTO'
      Size = 60
    end
    object qItensAcumuladosPRECO_UNITARIO: TFloatField
      FieldName = 'PRECO_UNITARIO'
      Required = True
      currency = True
    end
    object qItensAcumuladosQUANTIDADE: TFloatField
      FieldName = 'QUANTIDADE'
      Required = True
    end
    object qItensAcumuladosVALOR_TOTAL: TFloatField
      FieldName = 'VALOR_TOTAL'
      Required = True
      currency = True
    end
    object qItensAcumuladosVALOR_TOTAL_DESCONTO: TFloatField
      FieldName = 'VALOR_TOTAL_DESCONTO'
      Required = True
      currency = True
    end
    object qItensAcumuladosVALOR_TOTAL_OUTRAS_DESPESAS: TFloatField
      FieldName = 'VALOR_TOTAL_OUTRAS_DESPESAS'
      Required = True
      currency = True
    end
    object qItensAcumuladosVALOR_TOTAL_FRETE: TFloatField
      FieldName = 'VALOR_TOTAL_FRETE'
      Required = True
      currency = True
    end
    object qItensAcumuladosMARCA_ID: TIntegerField
      FieldName = 'MARCA_ID'
    end
    object qItensAcumuladosNOME_MARCA: TStringField
      FieldName = 'NOME_MARCA'
      Size = 40
    end
    object qItensAcumuladosUNIDADE_VENDA: TStringField
      FieldName = 'UNIDADE_VENDA'
      Size = 5
    end
    object qItensAcumuladosPRECO_FINAL: TFloatField
      FieldName = 'PRECO_FINAL'
      Required = True
      currency = True
    end
    object qItensAcumuladosPRECO_FINAL_MEDIO: TFloatField
      FieldName = 'PRECO_FINAL_MEDIO'
      Required = True
      currency = True
    end
    object qItensAcumuladosPRECO_LIQUIDO: TFloatField
      FieldName = 'PRECO_LIQUIDO'
      Required = True
      currency = True
    end
    object qItensAcumuladosPRECO_LIQUIDO_MEDIO: TFloatField
      FieldName = 'PRECO_LIQUIDO_MEDIO'
      Required = True
      currency = True
    end
    object qItensAcumuladosCMV: TFloatField
      FieldName = 'CMV'
      Required = True
    end
    object qItensAcumuladosCUSTO_ULTIMO_PEDIDO: TFloatField
      FieldName = 'CUSTO_ULTIMO_PEDIDO'
      Required = True
    end
    object qItensAcumuladosCUSTO_PEDIDO_MEDIO: TFloatField
      FieldName = 'CUSTO_PEDIDO_MEDIO'
      Required = True
    end
    object qItensAcumuladosVALOR_IMPOSTOS: TFloatField
      FieldName = 'VALOR_IMPOSTOS'
      Required = True
    end
    object qItensAcumuladosVALOR_ENCARGOS: TFloatField
      FieldName = 'VALOR_ENCARGOS'
      Required = True
    end
    object qItensAcumuladosVALOR_CUSTO_VENDA: TFloatField
      FieldName = 'VALOR_CUSTO_VENDA'
      Required = True
    end
    object qItensAcumuladosVALOR_CUSTO_FINAL: TFloatField
      FieldName = 'VALOR_CUSTO_FINAL'
      Required = True
    end
  end
  object dstItensAcumulados: TfrxDBDataset
    UserName = 'frxdstItensAcumulados'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ACUMULADO_ID=ACUMULADO_ID'
      'PRODUTO_ID=PRODUTO_ID'
      'NOME_PRODUTO=NOME_PRODUTO'
      'PRECO_UNITARIO=PRECO_UNITARIO'
      'QUANTIDADE=QUANTIDADE'
      'VALOR_TOTAL=VALOR_TOTAL'
      'VALOR_TOTAL_DESCONTO=VALOR_TOTAL_DESCONTO'
      'VALOR_TOTAL_OUTRAS_DESPESAS=VALOR_TOTAL_OUTRAS_DESPESAS'
      'VALOR_TOTAL_FRETE=VALOR_TOTAL_FRETE'
      'MARCA_ID=MARCA_ID'
      'NOME_MARCA=NOME_MARCA'
      'UNIDADE_VENDA=UNIDADE_VENDA'
      'PRECO_FINAL=PRECO_FINAL'
      'PRECO_FINAL_MEDIO=PRECO_FINAL_MEDIO'
      'PRECO_LIQUIDO=PRECO_LIQUIDO'
      'PRECO_LIQUIDO_MEDIO=PRECO_LIQUIDO_MEDIO'
      'CMV=CMV'
      'CUSTO_ULTIMO_PEDIDO=CUSTO_ULTIMO_PEDIDO'
      'CUSTO_PEDIDO_MEDIO=CUSTO_PEDIDO_MEDIO'
      'VALOR_IMPOSTOS=VALOR_IMPOSTOS'
      'VALOR_ENCARGOS=VALOR_ENCARGOS'
      'VALOR_CUSTO_VENDA=VALOR_CUSTO_VENDA'
      'VALOR_CUSTO_FINAL=VALOR_CUSTO_FINAL')
    DataSet = qItensAcumulados
    BCDToCurrency = False
    Left = 302
    Top = 298
  end
end
