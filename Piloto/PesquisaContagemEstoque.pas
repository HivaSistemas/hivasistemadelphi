unit PesquisaContagemEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Sessao, System.Math,
  ComboBoxLuka, EditLuka, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Inventario;

type
  TFormPesquisaContagemEstoque = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPesquisaContagemEstoque: TFormPesquisaContagemEstoque;

function PesquisarContagemEstoque: TObject;

implementation

{$R *.dfm}

uses _Biblioteca;

const
  cp_Nome       = 2;
  cp_DataInicio = 3;
  cp_DataFinal  = 4;

{ TFormPesquisaContagemEstoque }

function PesquisarContagemEstoque: TObject;
var
  r: TObject;
begin
  r := _HerancaPesquisas.Pesquisar(TFormPesquisaContagemEstoque, _Inventario.GetFiltros);
  if r = nil then
    Result := nil
  else
    Result := RecInventario(r);
end;

procedure TFormPesquisaContagemEstoque.BuscarRegistros;
var
  i: Integer;
  vInventario: TArray<RecInventario>;
begin
  inherited;

  vInventario :=
    _Inventario.BuscarInventario(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]);

  if vInventario = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vInventario);

  for i := Low(vInventario) to High(vInventario) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]      := vInventario[i].InventarioId.ToString;
    sgPesquisa.Cells[cp_Nome, i + 1]       := vInventario[i].Nome;
    sgPesquisa.Cells[cp_DataInicio, i + 1] := DateTimeToStr(vInventario[i].DataInicio);
    sgPesquisa.Cells[cp_DataFinal, i + 1]  := DateTimeToStr(vInventario[i].DataFinal);

  end;

  sgPesquisa.RowCount := IfThen(Length(vInventario) = 1, 2, High(vInventario) + 2);
  SetarFoco(sgPesquisa);
end;

procedure TFormPesquisaContagemEstoque.sgPesquisaDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado]) then
    vAlinhamento := taCenter
  else if ACol = coCodigo then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
