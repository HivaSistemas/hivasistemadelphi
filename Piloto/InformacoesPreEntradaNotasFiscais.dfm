inherited FormInformacoesPreEntradaNotasFiscais: TFormInformacoesPreEntradaNotasFiscais
  Caption = 'Informa'#231#245'es de entrada futura'
  ClientHeight = 310
  ClientWidth = 663
  OnShow = FormShow
  ExplicitWidth = 669
  ExplicitHeight = 339
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 2
    Top = 0
    Width = 64
    Height = 14
    Caption = 'Pr'#233'-entrada'
  end
  object lb2: TLabel [1]
    Left = 82
    Top = 0
    Width = 75
    Height = 14
    Caption = 'CNPJ Emitente'
  end
  object lb13: TLabel [2]
    Left = 197
    Top = 0
    Width = 122
    Height = 14
    Caption = 'Raz'#227'o social emitente'
  end
  object lb11: TLabel [3]
    Left = 375
    Top = 0
    Width = 42
    Height = 14
    Caption = 'Nr. nota'
  end
  object lb12: TLabel [4]
    Left = 441
    Top = 0
    Width = 28
    Height = 14
    Caption = 'S'#233'rie'
  end
  inherited pnOpcoes: TPanel
    Top = 273
    Width = 663
    ExplicitTop = 273
    ExplicitWidth = 663
  end
  object ePreEntradaId: TEditLuka
    Left = 2
    Top = 14
    Width = 75
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCNPJEmitente: TEditLuka
    Left = 81
    Top = 14
    Width = 112
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    Text = '02.250.123/0001-40'
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eRazaoSocialEmitente: TEditLuka
    Left = 197
    Top = 14
    Width = 172
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    Text = 'CIPLAN CIMENTOS S/A'
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object stStatusAltis: TStaticText
    Left = 496
    Top = 19
    Width = 165
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Ag.manifesta'#231#227'o'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    Transparent = False
  end
  object st1: TStaticTextLuka
    Left = 496
    Top = 2
    Width = 165
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Status'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 5
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object eNumeroNota: TEditLuka
    Left = 375
    Top = 14
    Width = 60
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 6
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eSerieNota: TEditLuka
    Left = 441
    Top = 14
    Width = 50
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 7
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object pcDados: TPageControl
    Left = 2
    Top = 36
    Width = 659
    Height = 236
    ActivePage = tsPrincipais
    TabOrder = 8
    object tsPrincipais: TTabSheet
      Caption = 'Principais'
      object lb5: TLabel
        Left = 0
        Top = 40
        Width = 184
        Height = 14
        Caption = 'Usu'#225'rio prestou ci'#234'ncia opera'#231#227'o'
      end
      object lb6: TLabel
        Left = 206
        Top = 40
        Width = 98
        Height = 14
        Caption = 'Data/hora ci'#234'ncia'
      end
      object lb3: TLabel
        Left = 0
        Top = 80
        Width = 177
        Height = 14
        Caption = 'Usu'#225'rio Opera'#231#227'o n'#227'o realizada'
      end
      object lb4: TLabel
        Left = 206
        Top = 80
        Width = 117
        Height = 14
        Caption = 'Data/hora oper.'#241'.rea.'
      end
      object lb7: TLabel
        Left = 330
        Top = 40
        Width = 197
        Height = 14
        Caption = 'Usu'#225'rio desconhecimento opera'#231#227'o'
      end
      object lb8: TLabel
        Left = 534
        Top = 40
        Width = 108
        Height = 14
        Caption = 'Data/hora desconh.'
      end
      object lb9: TLabel
        Left = 330
        Top = 80
        Width = 111
        Height = 14
        Caption = 'Usu'#225'rio confirma'#231#227'o'
      end
      object lb10: TLabel
        Left = 534
        Top = 80
        Width = 113
        Height = 14
        Caption = 'Data/hora confirma'#231'.'
      end
      object lb14: TLabel
        Left = 172
        Top = -1
        Width = 60
        Height = 14
        Caption = 'Nr. entrada'
      end
      object sbInformaocoesEntrada: TSpeedButtonLuka
        Left = 234
        Top = 17
        Width = 18
        Height = 18
        Hint = 'Abrir informa'#231#245'es do turno'
        Flat = True
        NumGlyphs = 2
        TagImagem = 13
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object lb15: TLabel
        Left = 258
        Top = -1
        Width = 47
        Height = 14
        Caption = 'Empresa'
      end
      object lb16: TLabel
        Left = 436
        Top = -1
        Width = 106
        Height = 14
        Caption = 'Data/hora emiss'#227'o'
      end
      object lb17: TLabel
        Left = 557
        Top = -1
        Width = 85
        Height = 14
        Caption = 'Valor total nota'
      end
      object lb18: TLabel
        Left = 0
        Top = 120
        Width = 169
        Height = 14
        Caption = 'Motivo opera'#231#227'o n'#227'o realizada'
      end
      object lb19: TLabel
        Left = 0
        Top = 160
        Width = 97
        Height = 14
        Caption = 'Chave acesso NFe'
      end
      object eUsuarioCienciaOperacao: TEditLuka
        Left = 0
        Top = 54
        Width = 197
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 0
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataHoraCiencia: TEditLukaData
        Left = 206
        Top = 54
        Width = 115
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        ReadOnly = True
        TabOrder = 1
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object eOperacaoNaoRealizada: TEditLuka
        Left = 0
        Top = 94
        Width = 197
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 2
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataHoraOperacaoNaoRealizada: TEditLukaData
        Left = 206
        Top = 94
        Width = 115
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        ReadOnly = True
        TabOrder = 3
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object eUsuarioDesconhecimento: TEditLuka
        Left = 330
        Top = 54
        Width = 198
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 4
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataHoraDesconhecimento: TEditLukaData
        Left = 531
        Top = 54
        Width = 117
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        ReadOnly = True
        TabOrder = 5
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object eUsuarioConfirmacao: TEditLuka
        Left = 330
        Top = 94
        Width = 199
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 6
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataHoraConfirmacao: TEditLukaData
        Left = 531
        Top = 94
        Width = 118
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        ReadOnly = True
        TabOrder = 7
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object st2: TStaticTextLuka
        Left = 1
        Top = 1
        Width = 165
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Status SEFAZ'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 8
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object stStatusSefaz: TStaticText
        Left = 1
        Top = 18
        Width = 165
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Autorizada'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 9
        Transparent = False
      end
      object eEntradaId: TEditLuka
        Left = 172
        Top = 13
        Width = 60
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 10
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eEmpresa: TEditLuka
        Left = 258
        Top = 13
        Width = 172
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 11
        Text = 'CIPLAN CIMENTOS S/A'
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eDataHoraEmissao: TEditLukaData
        Left = 436
        Top = 13
        Width = 115
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        ReadOnly = True
        TabOrder = 12
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object eValorTotalNota: TEditLuka
        Left = 557
        Top = 13
        Width = 91
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 13
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eMotivoOperacaoNaoRealizada: TEditLuka
        Left = 0
        Top = 134
        Width = 321
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 14
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eChaveAcessoNFe: TEditLuka
        Left = 0
        Top = 174
        Width = 321
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 15
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
    end
    object tsXML: TTabSheet
      Caption = 'XML'
      ImageIndex = 1
      object wbXML: TWebBrowser
        Left = 0
        Top = 0
        Width = 651
        Height = 207
        Align = alClient
        TabOrder = 0
        ExplicitLeft = -24
        ExplicitTop = 1
        ControlData = {
          4C00000048430000651500000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
  end
end
