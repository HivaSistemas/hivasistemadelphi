unit AnaliseProdutoLotes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastro, _Biblioteca, _EstoquesDivisao,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameProdutos, Vcl.Buttons, _Sessao,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _RecordsEspeciais, Vcl.Menus;

type
  TFormAnaliseProdutoLotes = class(TFormHerancaCadastro)
    FrProduto: TFrProdutos;
    sgLotes: TGridLuka;
    pmOpcoes: TPopupMenu;
    miAtivarDesativarPontaEstLoteSel: TMenuItem;
    miAtivarDesativarPontaEstTodosLotes: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure sgLotesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgLotesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgLotesSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure miAtivarDesativarPontaEstLoteSelClick(Sender: TObject);
    procedure miAtivarDesativarPontaEstTodosLotesClick(Sender: TObject);
    procedure sgLotesArrumarGrid(Sender: TObject; ARow, ACol: Integer;
      var TextCell: string);
  private
    procedure ProdutoOnAposPesquisar(Sender: TObject);
  protected
    procedure Modo(pEditando: Boolean); override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure GravarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

{ TFormAnaliseProdutoLotes }

const
  coLote             = 0;
  coPontaEstoque     = 1;
  coMultPontaEstoque = 2;
  coFisico           = 3;
  coDisponivel       = 4;
  coReservado        = 5;
  coDataFabricacao   = 6;
  coDatavencimento   = 7;
  coTipoLinha        = 8;
  coEmpresaId        = 9;
  coProdutoId        = 10;

procedure TFormAnaliseProdutoLotes.FormCreate(Sender: TObject);
begin
  inherited;
  FrProduto.OnAposPesquisar := ProdutoOnAposPesquisar;
end;

procedure TFormAnaliseProdutoLotes.GravarRegistro(Sender: TObject);
var
  i: Integer;
  vRetBanco: RecRetornoBD;
  vItens: TArray<RecEstoquesDivisao>;
begin
  vItens := nil;
  for i := 1 to sgLotes.RowCount -1 do begin
    if sgLotes.Cells[coTipoLinha, i] = 'C' then
      Continue;

    SetLength(vItens, Length(vItens) + 1);
    vItens[High(vItens)].EmpresaId            := SFormatInt(sgLotes.Cells[coEmpresaId, i]);
    vItens[High(vItens)].ProdutoId            := SFormatInt(sgLotes.Cells[coProdutoId, i]);
    vItens[High(vItens)].Lote                 := sgLotes.Cells[coLote, i];
    vItens[High(vItens)].PontaEstoque         := _Biblioteca.ToChar(sgLotes.Cells[coPontaEstoque, i]);
    vItens[High(vItens)].MultiploPontaEstoque := SFormatDouble(sgLotes.Cells[coMultPontaEstoque, i]);
  end;

  vRetBanco := _EstoquesDivisao.AtualizarLotesPontaEstoque(Sessao.getConexaoBanco, vItens);
  if vRetBanco.TeveErro then begin
    Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  RotinaSucesso;
  inherited;
end;

procedure TFormAnaliseProdutoLotes.miAtivarDesativarPontaEstLoteSelClick(Sender: TObject);
begin
  inherited;
  if sgLotes.Cells[coPontaEstoque, sgLotes.Row] = 'Sim' then
    sgLotes.Cells[coPontaEstoque, sgLotes.Row] := 'N�o'
  else if sgLotes.Cells[coPontaEstoque, sgLotes.Row] = 'N�o' then
    sgLotes.Cells[coPontaEstoque, sgLotes.Row] := 'Sim';
end;

procedure TFormAnaliseProdutoLotes.miAtivarDesativarPontaEstTodosLotesClick(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  for i := 1 to sgLotes.RowCount -1 do begin
    if sgLotes.Cells[coTipoLinha, i] = 'C' then
      Continue;

    if sgLotes.Cells[coPontaEstoque, i] = 'Sim' then
      sgLotes.Cells[coPontaEstoque, i] := 'N�o'
    else if sgLotes.Cells[coPontaEstoque, i] = 'N�o' then
      sgLotes.Cells[coPontaEstoque, i] := 'Sim';
  end;
end;

procedure TFormAnaliseProdutoLotes.Modo(pEditando: Boolean);
begin
  inherited;
   _Biblioteca.Habilitar([
      sgLotes],
      pEditando
   );

  if not pEditando then begin
    FrProduto.Modo(True);
    _Biblioteca.SetarFoco(FrProduto);
  end;
end;

procedure TFormAnaliseProdutoLotes.ProdutoOnAposPesquisar(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vEmpresaAnteriorId: Integer;
  vDivisaoEstoque: TArray<RecEstoquesDivisao>;
begin
  if not Em(FrProduto.getProduto.TipoControleEstoque, ['L', 'P', 'G']) then begin
    _Biblioteca.Exclamar('O produto selecionado n�o tem seu estoque controlado por "LOTE", "PISO" ou "GRADE"!');
    SetarFoco(FrProduto);
    FrProduto.Clear;
    Exit;
  end;

  Modo(True);
  FrProduto.Modo(False, False);

  vDivisaoEstoque := _EstoquesDivisao.BuscarEstoque(Sessao.getConexaoBanco, 0, [FrProduto.getProduto.produto_id]);

  vLinha := 1;
  vEmpresaAnteriorId := -1;
  for i := Low(vDivisaoEstoque) to High(vDivisaoEstoque) do begin
    if vEmpresaAnteriorId <> vDivisaoEstoque[i].EmpresaId then begin
      sgLotes.Cells[coLote, vLinha] := NFormat(vDivisaoEstoque[i].EmpresaId) + ' - ' + vDivisaoEstoque[i].NomeEmpresa;
      sgLotes.Cells[coTipoLinha, vLinha] := 'C';
      vEmpresaAnteriorId := vDivisaoEstoque[i].EmpresaId;
      Inc(vLinha);
    end;

    sgLotes.Cells[coLote, vLinha]             := vDivisaoEstoque[i].Lote;
    sgLotes.Cells[coPontaEstoque, vLinha]     := _Biblioteca.SimNao(vDivisaoEstoque[i].PontaEstoque);
    sgLotes.Cells[coMultPontaEstoque, vLinha] := _Biblioteca.NFormatN(vDivisaoEstoque[i].MultiploPontaEstoque, 3);
    sgLotes.Cells[coFisico, vLinha]           := _Biblioteca.NFormatNEstoque(vDivisaoEstoque[i].Fisico);
    sgLotes.Cells[coDisponivel, vLinha]       := _Biblioteca.NFormatNEstoque(vDivisaoEstoque[i].Disponivel);
    sgLotes.Cells[coReservado, vLinha]        := _Biblioteca.NFormatNEstoque(vDivisaoEstoque[i].Reservado);
    sgLotes.Cells[coDataFabricacao, vLinha]   := _Biblioteca.DFormatN(vDivisaoEstoque[i].DataFabricacao);
    sgLotes.Cells[coDatavencimento, vLinha]   := _Biblioteca.DFormatN(vDivisaoEstoque[i].DataVencimento);
    sgLotes.Cells[coTipoLinha, vLinha]        := 'D';
    sgLotes.Cells[coEmpresaId, vLinha]        := _Biblioteca.NFormat(vDivisaoEstoque[i].EmpresaId);
    sgLotes.Cells[coProdutoId, vLinha]        := _Biblioteca.NFormat(vDivisaoEstoque[i].ProdutoId);
    Inc(vLinha);
  end;
  sgLotes.RowCount := vLinha;
end;

procedure TFormAnaliseProdutoLotes.sgLotesArrumarGrid(Sender: TObject; ARow, ACol: Integer; var TextCell: string);
begin
  inherited;
  if ACol <> coMultPontaEstoque then
    Exit;

  if ARow = 0 then
    Exit;

  TextCell := _Biblioteca.NFormatN( SFormatDouble(sgLotes.Cells[coMultPontaEstoque, ARow]), 3 );
end;

procedure TFormAnaliseProdutoLotes.sgLotesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if (sgLotes.Cells[coTipoLinha, ARow] = 'C') then
    vAlinhamento := taLeftJustify
  else if ACol in[coMultPontaEstoque, coFisico, coDisponivel, coReservado] then
    vAlinhamento := taRightJustify
  else if ACol = coPontaEstoque then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  if sgLotes.Cells[coTipoLinha, ARow] = 'C' then
    sgLotes.MergeCells([ARow, ACol], [ARow, coLote], [ARow, coReservado], vAlinhamento, Rect)
  else
    sgLotes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormAnaliseProdutoLotes.sgLotesGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (sgLotes.Cells[coTipoLinha, ARow] = 'C') then begin
    ABrush.Color := _Biblioteca.getCorVerdePadraoAltis;
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
  end
  else if ACol = coPontaEstoque then
    _Biblioteca.SetarColunaGridSimNao(AFont, sgLotes.Cells[coPontaEstoque, ARow])
  else if ACol = coMultPontaEstoque then begin
    ABrush.Color := _Biblioteca.coCorCelulaEdicao1;
    AFont.Color  := _Biblioteca.coCorFonteEdicao1;
  end;
end;

procedure TFormAnaliseProdutoLotes.sgLotesSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if (ACol = coMultPontaEstoque) and (sgLotes.Cells[coPontaEstoque, ARow] = 'Sim') then
    sgLotes.Options := sgLotes.Options + [goEditing]
  else
    sgLotes.Options := sgLotes.Options - [goEditing];
end;

procedure TFormAnaliseProdutoLotes.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
