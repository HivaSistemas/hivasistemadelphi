inherited FormAdiantamentoTituloFinanceiro: TFormAdiantamentoTituloFinanceiro
  Caption = 'Adiantamento de contas a receber'
  ClientHeight = 418
  ClientWidth = 639
  ExplicitWidth = 645
  ExplicitHeight = 447
  PixelsPerInch = 96
  TextHeight = 14
  object lb8: TLabel [0]
    Left = 343
    Top = 1
    Width = 80
    Height = 14
    Caption = 'Data de pagto.'
  end
  object lb22: TLabel [1]
    Left = 5
    Top = 1
    Width = 77
    Height = 14
    Caption = 'Valor do t'#237'tulo'
  end
  object lb1: TLabel [2]
    Left = 213
    Top = 1
    Width = 97
    Height = 14
    Caption = 'Valor l'#237'q. do titulo'
  end
  object lb4: TLabel [3]
    Left = 109
    Top = 1
    Width = 70
    Height = 14
    Caption = 'Valor adiant.'
  end
  object lb2: TLabel [4]
    Left = 313
    Top = 40
    Width = 69
    Height = 14
    Caption = 'Observa'#231#245'es'
  end
  object lb3: TLabel [5]
    Left = 435
    Top = 1
    Width = 78
    Height = 14
    Caption = 'Valor a adiantar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited pnOpcoes: TPanel
    Top = 381
    Width = 639
    TabOrder = 5
    ExplicitTop = 381
    ExplicitWidth = 639
  end
  object eDataPagamento: TEditLukaData
    Left = 343
    Top = 15
    Width = 85
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
    OnKeyDown = ProximoCampo
  end
  object eValorTitulo: TEditLuka
    Left = 5
    Top = 15
    Width = 100
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 0
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorTotal: TEditLuka
    Left = 213
    Top = 15
    Width = 125
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorAdiantado: TEditLuka
    Left = 109
    Top = 15
    Width = 100
    Height = 22
    TabStop = False
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    Text = '0,00'
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  inline FrPagamento: TFrPagamentoFinanceiro
    Left = 4
    Top = 40
    Width = 303
    Height = 335
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 4
    ExplicitTop = 40
    ExplicitHeight = 335
    inherited grpFechamento: TGroupBox
      Height = 335
      ExplicitHeight = 335
      inherited lbllb11: TLabel
        Top = 63
        ExplicitTop = 63
      end
      inherited lbllb12: TLabel
        Top = 83
        ExplicitTop = 83
      end
      inherited lbllb13: TLabel
        Left = 32
        Top = 103
        Width = 78
        ExplicitLeft = 32
        ExplicitTop = 103
        ExplicitWidth = 78
      end
      inherited lbllb14: TLabel
        Top = 165
        ExplicitTop = 165
      end
      inherited lbllb15: TLabel
        Top = 185
        ExplicitTop = 185
      end
      inherited lbllb10: TLabel
        Top = 205
        ExplicitTop = 205
      end
      inherited lbllb21: TLabel
        Left = 355
        Top = 61
        ExplicitLeft = 355
        ExplicitTop = 61
      end
      inherited lbllb7: TLabel
        Left = 593
        Top = 61
        Width = 9
        Height = 14
        ExplicitLeft = 593
        ExplicitTop = 61
        ExplicitWidth = 9
        ExplicitHeight = 14
      end
      inherited lbllb19: TLabel
        Top = 19
        ExplicitTop = 19
      end
      inherited lbllb1: TLabel
        Left = 355
        Top = 41
        ExplicitLeft = 355
        ExplicitTop = 41
      end
      inherited lb4: TLabel
        Top = 123
        ExplicitTop = 123
      end
      inherited lb1: TLabel
        Left = 355
        Top = 21
        ExplicitLeft = 355
        ExplicitTop = 21
      end
      inherited sbBuscarDadosDinheiro: TImage
        Top = 59
        ExplicitTop = 59
      end
      inherited sbBuscarDadosCheques: TImage
        Top = 79
        ExplicitTop = 79
      end
      inherited sbBuscarDadosCartoesDebito: TImage
        Top = 99
        ExplicitTop = 99
      end
      inherited sbBuscarDadosCartoesCredito: TImage
        Top = 119
        ExplicitTop = 119
      end
      inherited sbBuscarDadosCobranca: TImage
        Top = 162
        ExplicitTop = 162
      end
      inherited sbBuscarDadosCreditos: TImage
        Top = 182
        OnClick = FrPagamentosbBuscarDadosCreditosClick
        ExplicitTop = 182
      end
      inherited Label1: TLabel
        Top = 145
        ExplicitTop = 145
      end
      inherited sbBuscarDadosPix: TImage
        Top = 141
        ExplicitTop = 141
      end
      inherited eValorDinheiro: TEditLuka
        Top = 56
        Height = 22
        ExplicitTop = 56
        ExplicitHeight = 22
      end
      inherited eValorCheque: TEditLuka
        Top = 76
        Height = 22
        ExplicitTop = 76
        ExplicitHeight = 22
      end
      inherited eValorCartaoDebito: TEditLuka
        Top = 96
        Height = 22
        ExplicitTop = 96
        ExplicitHeight = 22
      end
      inherited eValorCobranca: TEditLuka
        Top = 158
        Height = 22
        ExplicitTop = 158
        ExplicitHeight = 22
      end
      inherited eValorCredito: TEditLuka
        Top = 178
        Height = 22
        ExplicitTop = 178
        ExplicitHeight = 22
      end
      inherited stPagamento: TStaticText
        Top = 38
        ExplicitTop = 38
      end
      inherited eValorDiferencaPagamentos: TEditLuka
        Top = 198
        Height = 22
        ExplicitTop = 198
        ExplicitHeight = 22
      end
      inherited eValorDesconto: TEditLuka
        Left = 456
        Top = 54
        Height = 22
        ExplicitLeft = 456
        ExplicitTop = 54
        ExplicitHeight = 22
      end
      inherited ePercentualDesconto: TEditLuka
        Left = 552
        Top = 54
        Height = 22
        ExplicitLeft = 552
        ExplicitTop = 54
        ExplicitHeight = 22
      end
      inherited eValorTotalASerPago: TEditLuka
        Top = 13
        ExplicitTop = 13
      end
      inherited eValorJuros: TEditLuka
        Left = 456
        Top = 33
        Height = 22
        ExplicitLeft = 456
        ExplicitTop = 33
        ExplicitHeight = 22
      end
      inherited eValorCartaoCredito: TEditLuka
        Top = 116
        Height = 22
        OnChange = nil
        ExplicitTop = 116
        ExplicitHeight = 22
      end
      inherited stValorTotal: TStaticText
        Top = 38
        ExplicitTop = 38
      end
      inherited stDinheiroDefinido: TStaticText
        Top = 60
        ExplicitTop = 60
      end
      inherited stChequeDefinido: TStaticText
        Top = 80
        ExplicitTop = 80
      end
      inherited stCartaoDebitoDefinido: TStaticText
        Top = 100
        ExplicitTop = 100
      end
      inherited stCobrancaDefinido: TStaticText
        Top = 162
        ExplicitTop = 162
      end
      inherited stCreditoDefinido: TStaticText
        Top = 182
        ExplicitTop = 182
      end
      inherited stCartaoCreditoDefinido: TStaticText
        Top = 120
        ExplicitTop = 120
      end
      inherited eValorMulta: TEditLuka
        Left = 456
        Top = 12
        Height = 22
        ExplicitLeft = 456
        ExplicitTop = 12
        ExplicitHeight = 22
      end
      inherited eValorPix: TEditLuka
        Top = 137
        Height = 22
        ExplicitTop = 137
        ExplicitHeight = 22
      end
      inherited stPixDefinido: TStaticText
        Top = 141
        ExplicitTop = 141
      end
    end
  end
  object eObservacoes: TEditLuka
    Left = 313
    Top = 54
    Width = 321
    Height = 22
    CharCase = ecUpperCase
    TabOrder = 7
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eValorAdiantar: TEditLuka
    Left = 435
    Top = 15
    Width = 125
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Text = '0,00'
    OnChange = eValorAdiantarChange
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
end
