unit Frame.ICMSCompra;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Grids, _Biblioteca,
  GridLuka, Vcl.StdCtrls, EditLuka, Vcl.Buttons, Vcl.ExtCtrls, _GruposTribEstadualCompEst, System.Math;

type
  TFrICMSCompra = class(TFrameHerancaPrincipal)
    pnBotoes: TPanel;
    lb1: TLabel;
    lbEstadoSelecionado: TLabel;
    sbPreencher: TSpeedButton;
    eValor: TEditLuka;
    pnGrid: TPanel;
    sgICMS: TGridLuka;
    procedure sgICMSSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure eValorKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbPreencherClick(Sender: TObject);
    procedure sgICMSDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
  private
    procedure setICMS(const pValor: TArray<RecGruposTribEstadualCompEst>);
    function  getICMS: TArray<RecGruposTribEstadualCompEst>;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Clear; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure VerificarRegistro;
  published
    property ICMS: TArray<RecGruposTribEstadualCompEst> read getICMS write setICMS;
  end;

implementation

{$R *.dfm}

const
  coEstado                 = 0;
  coCstRevenda             = 1;
  coCstDistribuidora       = 2;
  coCstIndustria           = 3;
  coPercentualAliquotaIcms = 4;
  coPercentualIva          = 5;
  coIndiceReducaoBaseIcms  = 6;
  coPrecoPauta             = 7;

{ TFrICMSCompra }

procedure TFrICMSCompra.Clear;
var
  vLinha: Integer;
  vColuna: Integer;
begin
  inherited;
  for vLinha := sgICMS.FixedRows to sgICMS.RowCount - 1 do begin
    for vColuna := sgICMS.FixedCols to sgICMS.ColCount do
      sgICMS.Cells[vColuna, vLinha] := '';
  end;
end;

constructor TFrICMSCompra.Create(AOwner: TComponent);
begin
  inherited;
  Align := alClient;
  sgICMS.FixedCols := 1;

  sgICMS.Cells[coCstRevenda, 0]             := 'CST';
  sgICMS.Cells[coPercentualAliquotaIcms, 0] := '%';

  sgICMS.OcultarColunas([coPercentualAliquotaIcms]);

  sgICMS.Col := coCstRevenda;
  sgICMS.Invalidate;
  Invalidate;
end;

procedure TFrICMSCompra.eValorKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) and (eValor.Text <> '') then
    sbPreencher.Click;
end;

function TFrICMSCompra.getICMS: TArray<RecGruposTribEstadualCompEst>;
var
  vLinha: Integer;
begin
  SetLength(Result, 27);
  for vLinha := sgICMS.FixedRows to sgICMS.RowCount - 1 do begin
    Result[vLinha - sgICMS.FixedRows].EstadoId              := sgICMS.Cells[coEstado, vLinha];
    Result[vLinha - sgICMS.FixedRows].CstRevenda            := sgICMS.Cells[coCstRevenda, vLinha];
    Result[vLinha - sgICMS.FixedRows].CstDistribuidora      := sgICMS.Cells[coCstDistribuidora, vLinha];
    Result[vLinha - sgICMS.FixedRows].CstIndustria          := sgICMS.Cells[coCstIndustria, vLinha];
    Result[vLinha - sgICMS.FixedRows].CstRevenda            := sgICMS.Cells[coCstRevenda, vLinha];
    Result[vLinha - sgICMS.FixedRows].IndiceReducaoBaseIcms := SFormatDouble(sgICMS.Cells[coIndiceReducaoBaseIcms, vLinha]);
    Result[vLinha - sgICMS.FixedRows].Iva                   := SFormatDouble(sgICMS.Cells[coPercentualIva, vLinha]);
    Result[vLinha - sgICMS.FixedRows].PrecoPauta            := SFormatDouble(sgICMS.Cells[coPrecoPauta, vLinha]);
  end;
end;

procedure TFrICMSCompra.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  lbEstadoSelecionado.Caption := '';
  sgICMS.Enabled := pEditando;

  if pLimpar then
    Clear;
end;

procedure TFrICMSCompra.sbPreencherClick(Sender: TObject);
var
  vLinha: Integer;
begin
  inherited;
  if (eValor.Text = '') then
    Exit;

  for vLinha := sgICMS.FixedRows to sgICMS.RowCount - 1 do
    sgICMS.Cells[sgICMS.Col, vLinha] := eValor.Text;

  if not(sgICMS.Col = coPrecoPauta) then
    sgICMS.Col := sgICMS.Col + 1
  else
    sgICMS.Col := coCstRevenda;

  eValor.Clear;
  SetarFoco(eValor);
end;

procedure TFrICMSCompra.setICMS(const pValor: TArray<RecGruposTribEstadualCompEst>);
var
  vLinha: Integer;
begin
  for vLinha := Low(pValor) to High(pValor) do begin
    sgICMS.Cells[coEstado, vLinha + sgICMS.FixedRows]                 := pValor[vLinha].EstadoId;
    sgICMS.Cells[coCstRevenda, vLinha + sgICMS.FixedRows]             := pValor[vLinha].CstRevenda;
    sgICMS.Cells[coCstDistribuidora, vLinha + sgICMS.FixedRows]       := pValor[vLinha].CstDistribuidora;
    sgICMS.Cells[coCstIndustria, vLinha + sgICMS.FixedRows]           := pValor[vLinha].CstIndustria;
    sgICMS.Cells[coIndiceReducaoBaseIcms, vLinha + sgICMS.FixedRows]  := NFormatN(pValor[vLinha].IndiceReducaoBaseIcms, 4);
    sgICMS.Cells[coPercentualIva, vLinha + sgICMS.FixedRows]          := NFormatN(pValor[vLinha].Iva);
    sgICMS.Cells[coPrecoPauta, vLinha + sgICMS.FixedRows]             := NFormatN(pValor[vLinha].PrecoPauta);
  end;
end;

procedure TFrICMSCompra.sgICMSDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ARow = 0 then begin
    if
      ACol in[
        coCstRevenda,
        coCstDistribuidora,
        coCstIndustria
      ]
    then
      sgICMS.MergeCells([ARow, ACol], [ARow, coCstRevenda], [ARow, coCstIndustria], taCenter, Rect)
    else if ACol in[coPercentualAliquotaIcms, coPercentualIva] then
      sgICMS.MergeCells([ARow, ACol], [ARow, coPercentualAliquotaIcms], [ARow, coPercentualIva], taCenter, Rect)
    else
      sgICMS.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taCenter, Rect);
  end
  else begin
    if
      ACol in[
        coCstRevenda,
        coCstDistribuidora,
        coCstIndustria]
    then
      vAlinhamento := taCenter
    else if ACol = coEstado then
      vAlinhamento := taLeftJustify
    else
      vAlinhamento := taRightJustify;

    sgICMS.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
  end;
end;

procedure TFrICMSCompra.sgICMSSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  if (ACol = coEstado) or FSomenteLeitura then
    sgICMS.Options := sgICMS.Options - [goEditing]
  else
    sgICMS.Options := sgICMS.Options + [goEditing];

  if ACol in[coPercentualAliquotaIcms ,coPercentualIva, coPrecoPauta, coIndiceReducaoBaseIcms] then begin
    sgICMS.OnKeyPress := NumerosPonto;
    eValor.OnKeyPress := NumerosPonto;
    eValor.CasasDecimais := IfThen(ACol = coIndiceReducaoBaseIcms, 4, 2);
    eValor.Clear;
  end
  else begin
    sgICMS.OnKeyPress := Numeros;
    eValor.OnKeyPress := Numeros;
    eValor.CasasDecimais := 0;
  end;

  lbEstadoSelecionado.Caption :=
    _Biblioteca.Decode(
      sgICMS.Cells[coEstado, ARow],
      [
        'AC', 'Acre',
        'AL', 'Alagoas',
        'AP', 'Amap�',
        'AM', 'Amazonas',
        'BA', 'Bahia',
        'CE', 'Cear�',
        'DF', 'Distrito Federal',
        'ES', 'Espirito Santo',
        'GO', 'Goi�s',
        'MA', 'Maranh�o',
        'MT', 'Mato Grosso',
        'MS', 'Mato Grosso do Sul',
        'MG', 'Minas Gerais',
        'PA', 'Par�',
        'PB', 'Para�ba',
        'PR', 'Paran�',
        'PE', 'Pernambuco',
        'PI', 'Piau�',
        'RJ', 'Rio de Janeiro',
        'RN', 'Rio Grande do Norte',
        'RS', 'Rio Grande do Sul',
        'RO', 'Rond�nia',
        'RR', 'Roraima',
        'SC', 'Santa Catarina',
        'SP', 'S�o Paulo',
        'SE', 'Sergipe',
        'TO', 'Tocantins'
      ]
    );
end;

procedure TFrICMSCompra.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  if pValue then
    sgICMS.Options := sgICMS.Options - [goEditing]
  else
    sgICMS.Options := sgICMS.Options + [goEditing];
end;

procedure TFrICMSCompra.VerificarRegistro;
var
  i: Integer;

  procedure VerificarColuna(pColuna: Integer; pLinha: Integer; pColunaMensagem: string; pUF: string);
  begin

    if sgICMS.Cells[pColuna, pLinha] = '' then begin
      _Biblioteca.Exclamar('A coluna "' + pColunaMensagem + '" para o estado ' + pUF + ' n�o foi informada corretamente, verifique!');
      sgICMS.Row := pLinha;
      sgICMS.Col := pColuna;
      SetarFoco(sgICMS);
      Abort;
    end
    else if not Em(sgICMS.Cells[pColuna, pLinha], ['00','10','20','30','40','41','50','51','60','70','90']) then begin
      _Biblioteca.Exclamar(
        'A coluna "' + pColunaMensagem + '" para o estado ' + pUF + ' est� com o CST inexistente, verifique!' + chr(13) +
        'CST Preenchido: ' + sgICMS.Cells[pColuna, pLinha] + chr(13) +
        'CSTs existentes: ' + '00, 10, 20, 30, 40, 41, 50, 51, 60, 70, 90'
      );
      sgICMS.Row := pLinha;
      sgICMS.Col := pColuna;
      SetarFoco(sgICMS);
      Abort;
    end;
  end;

begin

  for i := sgICMS.FixedRows to sgICMS.RowCount -1 do begin
    VerificarColuna(coCstRevenda, i, '"Revenda"', sgICMS.Cells[coEstado, i]);
    VerificarColuna(coCstDistribuidora, i, '"Distribuidor"', sgICMS.Cells[coEstado, i]);
    VerificarColuna(coCstIndustria, i, '"Industria"', sgICMS.Cells[coEstado, i]);
  end;
end;

end.
