unit CadastroTiposCobranca;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaCadastroCodigo, Vcl.StdCtrls, _RecordsCadastros,
  EditLuka, Vcl.Buttons, Vcl.ExtCtrls, ComboBoxLuka, _Sessao, _Biblioteca, _RecordsEspeciais,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameContas, _TiposCobranca,
  System.StrUtils, System.Math, RadioGroupLuka, PesquisaTiposCobranca, _TiposCobrancaDiasPrazo,
  GroupBoxLuka, FrameNumeros, Frame.GerenciadoresCartoesTEF, CheckBoxLuka, Logs, _BibliotecaGenerica,
  Vcl.Grids, GridLuka, Frame.HerancaInsercaoExclusao, Vcl.ComCtrls, FramePortadores,
  FrameAdministradorasCartoes, StaticTextLuka;

type
  TFormCadastroTiposCobranca = class(TFormHerancaCadastroCodigo)
    lb1: TLabel;
    eNome: TEditLuka;
    rgFormaPagamento: TRadioGroupLuka;
    PageControl1: TPageControl;
    tsPrincipais: TTabSheet;
    tsCartao: TTabSheet;
    rgTipoCartao: TRadioGroupLuka;
    cbRedeCartao: TComboBoxLuka;
    lb2: TLabel;
    ckBoletoBancario: TCheckBoxLuka;
    ckUtilizarLimiteCredito: TCheckBoxLuka;
    cbTipoRecebimentoCartao: TComboBoxLuka;
    lb5: TLabel;
    lb3: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    eTaxaRetencaoMes: TEditLuka;
    FrDiasPrazo: TFrNumeros;
    cbTipoPagamentoComissao: TComboBoxLuka;
    ePercentualComissao: TEditLuka;
    FrPortador: TFrPortadores;
    lbl1: TLabel;
    cbIncidenciaFinanceira: TComboBoxLuka;
    ckPermitirPrazoMedio: TCheckBoxLuka;
    sbCopiarTipoCobranca: TSpeedButton;
    lb4: TLabel;
    eBandeira: TEditLuka;
    lb8: TLabel;
    eRede: TEditLuka;
    ckPermitirVinculoAutTEF: TCheckBoxLuka;
    lb9: TLabel;
    ePrazoMedio: TEditLuka;
    lb10: TLabel;
    cbRedeCartaoNFe: TComboBoxLuka;
    FrAdministradoraCartao: TFrAdministradorasCartoes;
    ckEmitirDuplicMercantilVenda: TCheckBoxLuka;
    procedure cbRedeCartaoChange(Sender: TObject);
    procedure rgFormaPagamentoClick(Sender: TObject);
    procedure sbCopiarTipoCobrancaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure PreencherRegistro(pTipoCobranca: RecTiposCobranca);
    procedure FrDiasPrazoOnAposAdicionar(Sender: TObject);
    procedure FrDiasPrazoOnAposDeletar(Sender: TObject);
    procedure CalcularPrazoMedio;
  protected
    procedure BuscarRegistro; override;
    procedure GravarRegistro(Sender: TObject); override;
    procedure ExcluirRegistro; override;
    procedure VerificarRegistro(Sender: TObject); override;
    procedure PesquisarRegistro; override;
    procedure Modo(pEditando: Boolean); override;
    procedure ExibirLogs; override;
  end;

implementation

{$R *.dfm}

{ TFormCadastroTiposCobranca }

procedure TFormCadastroTiposCobranca.BuscarRegistro;
var
  vTiposCobrancas: TArray<RecTiposCobranca>;
begin
  vTiposCobrancas := _TiposCobranca.BuscarTiposCobrancas(Sessao.getConexaoBanco, 0, [eId.AsInt], '');
  if vTiposCobrancas = nil then begin
    NenhumRegistro;
    _Biblioteca.SetarFoco(eID);
    Exit;
  end;

  inherited;
  PreencherRegistro(vTiposCobrancas[0]);
end;

procedure TFormCadastroTiposCobranca.CalcularPrazoMedio;
var
  i: Integer;
  vQtdeDias: Integer;
  vDias: TArray<Integer>;
begin
  vQtdeDias := 0;
  vDias := FrDiasPrazo.Valores;

  for i := Low(vDias) to High(vDias) do
    vQtdeDias := vQtdeDias + vDias[i];

  ePrazoMedio.AsInt := Round(vQtdeDias / _BibliotecaGenerica.zvl(Length(vDias), 1));
end;

procedure TFormCadastroTiposCobranca.cbRedeCartaoChange(Sender: TObject);
begin
  inherited;
  if cbRedeCartao.ItemIndex = 0 then
    rgTipoCartao.ItemIndex := -1;
end;

procedure TFormCadastroTiposCobranca.ExcluirRegistro;
var
  vRetBanco: RecRetornoBD;
begin
  vRetBanco := _TiposCobranca.ExcluirTiposCobranca(Sessao.getConexaoBanco, eId.AsInt);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  inherited;
end;

procedure TFormCadastroTiposCobranca.ExibirLogs;
begin
  inherited;
  Logs.Iniciar('LOGS_TIPOS_COBRANCA', ['COBRANCA_ID'], 'VW_TIPOS_ALTER_LOGS_TP_COBRANC', [eId.AsInt])
end;

procedure TFormCadastroTiposCobranca.FormCreate(Sender: TObject);
begin
  inherited;
  FrDiasPrazo.OnAposAdicionar := FrDiasPrazoOnAposAdicionar;
  FrDiasPrazo.OnAposDeletar   := FrDiasPrazoOnAposDeletar;
end;

procedure TFormCadastroTiposCobranca.FrDiasPrazoOnAposAdicionar(Sender: TObject);
begin
  CalcularPrazoMedio;
end;

procedure TFormCadastroTiposCobranca.FrDiasPrazoOnAposDeletar(Sender: TObject);
begin
  CalcularPrazoMedio;
end;

procedure TFormCadastroTiposCobranca.GravarRegistro(Sender: TObject);
var
  vRetBanco: RecRetornoBD;
  vAdministradoraCartaoId: Integer;
begin
  inherited;

  vAdministradoraCartaoId := 0;
  if not FrAdministradoraCartao.EstaVazio then
    vAdministradoraCartaoId := FrAdministradoraCartao.getDados().AdministradoraId;

  if rgFormaPagamento.GetValor <> 'CRT' then begin
    cbRedeCartao.ItemIndex := -1;
    cbTipoRecebimentoCartao.ItemIndex := -1;
    cbRedeCartaoNFe.ItemIndex := -1;
  end;

  vRetBanco :=
    _TiposCobranca.AtualizarTiposCobranca(
      Sessao.getConexaoBanco,
      eID.AsInt,
      eNome.Text,
      eTaxaRetencaoMes.AsDouble,
      cbRedeCartao.GetValor,
      rgTipoCartao.GetValor,
      rgFormaPagamento.GetValor,
      cbTipoRecebimentoCartao.GetValor,
      cbTipoPagamentoComissao.GetValor,
      ePercentualComissao.AsDouble,
      ckUtilizarLimiteCredito.CheckedStr,
      _Biblioteca.ToChar(ckBoletoBancario),
      _Biblioteca.ToChar(ckAtivo),
      FrPortador.getPortador().PortadorId,
      cbIncidenciaFinanceira.GetValor,
      ckPermitirPrazoMedio.CheckedStr,
      ckPermitirVinculoAutTEF.CheckedStr,
      cbRedeCartaoNFe.AsString,
      FrDiasPrazo.getValores,
      ePrazoMedio.AsDouble,
      vAdministradoraCartaoId,
      ckEmitirDuplicMercantilVenda.CheckedStr
    );

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Abort;
  end;

  _Biblioteca.InformarAlteracaoRegistro(vRetBanco.AsInt);
end;

procedure TFormCadastroTiposCobranca.Modo(pEditando: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([
    eNome,
    eTaxaRetencaoMes,
    cbRedeCartao,
    rgTipoCartao,
    FrDiasPrazo,
    rgFormaPagamento,
    cbTipoRecebimentoCartao,
    cbTipoPagamentoComissao,
    ePercentualComissao,
    ckUtilizarLimiteCredito,
    ckBoletoBancario,
    cbIncidenciaFinanceira,
    FrPortador,
    ckPermitirPrazoMedio,
    ckAtivo,
    ePrazoMedio,
    FrAdministradoraCartao,
    cbRedeCartaoNFe,
    PageControl1,
    ckEmitirDuplicMercantilVenda],
    pEditando
  );

  _Biblioteca.Habilitar([eID], not pEditando);
  _Biblioteca.Habilitar([sbCopiarTipoCobranca], not pEditando, False);

  if pEditando then begin
    _Biblioteca.SetarFoco(eNome);
    ckAtivo.Checked := True;
  end;
end;

procedure TFormCadastroTiposCobranca.PesquisarRegistro;
var
  vTiposCobranca: RecTiposCobranca;
begin
  vTiposCobranca := RecTiposCobranca(PesquisaTiposCobranca.PesquisarTiposCobranca(''));
  if vTiposCobranca = nil then
    Exit;

  inherited;
  PreencherRegistro(vTiposCobranca);
end;

procedure TFormCadastroTiposCobranca.PreencherRegistro(pTipoCobranca: RecTiposCobranca);
var
  i: Integer;
  vDiasPrazo: TArray<RecTipoCobrancaDiasPrazo>;
begin
  eID.AsInt := pTipoCobranca.CobrancaId;

  rgFormaPagamento.SetIndicePorValor(pTipoCobranca.forma_pagamento);
  rgFormaPagamentoClick(nil);

  eNome.Text := pTipoCobranca.Nome;
  cbIncidenciaFinanceira.SetIndicePorValor( pTipoCobranca.IncidenciaFinanceira );
  cbRedeCartao.SetIndicePorValor(pTipoCobranca.rede_cartao);
  eTaxaRetencaoMes.AsDouble := pTipoCobranca.taxa_retencao_mes;
  rgTipoCartao.SetIndicePorValor(pTipoCobranca.tipo_cartao);
  cbTipoRecebimentoCartao.SetIndicePorValor(pTipoCobranca.tipo_recebimento_cartao);
  ckEmitirDuplicMercantilVenda.CheckedStr := pTipoCobranca.EmitirDuplicMercantilVenda;

  vDiasPrazo := _TiposCobrancaDiasPrazo.BuscarTipoCobrancaDiasPrazos(Sessao.getConexaoBanco, 0, [pTipoCobranca.CobrancaId]);
  for i := Low(vDiasPrazo) to High(vDiasPrazo) do
    FrDiasPrazo.Adicionar(vDiasPrazo[i].Dias);

  cbTipoPagamentoComissao.SetIndicePorValor(pTipoCobranca.TipoPagamentoComissao);
  ePercentualComissao.AsDouble        := pTipoCobranca.PercentualComissao;
  ckUtilizarLimiteCredito.CheckedStr  := pTipoCobranca.UtilizarLimiteCredCliente;
  ckBoletoBancario.CheckedStr         := pTipoCobranca.BoletoBancario;
  ckPermitirPrazoMedio.CheckedStr     := pTipoCobranca.PermitirPrazoMedio;
  ckPermitirVinculoAutTEF.CheckedStr  := pTipoCobranca.PermitirVinculoAutTEF;
  ckAtivo.Checked                     := (pTipoCobranca.ativo = 'S');
  FrPortador.InserirDadoPorChave( pTipoCobranca.PortadorId, False );
  FrAdministradoraCartao.InserirDadoPorChave( pTipoCobranca.AdministradoraCartaoId, False );
  cbRedeCartaoNFe.AsString            := pTipoCobranca.BandeiraCartaoNFe;
  eBandeira.Text                      := pTipoCobranca.BandeiraRetornoTEF;
  eRede.Text                          := pTipoCobranca.RedeRetornoTEF;

  pTipoCobranca.Free;
end;

procedure TFormCadastroTiposCobranca.rgFormaPagamentoClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([cbRedeCartao, rgTipoCartao, cbTipoRecebimentoCartao], rgFormaPagamento.GetValor = 'CRT');
  _Biblioteca.Habilitar([ckBoletoBancario], rgFormaPagamento.GetValor = 'COB');

  if rgFormaPagamento.GetValor = 'CRT' then
    cbTipoRecebimentoCartao.SetIndicePorValor('P');
end;

procedure TFormCadastroTiposCobranca.sbCopiarTipoCobrancaClick(Sender: TObject);
var
  vCobranca: RecTiposCobranca;
begin
  vCobranca := RecTiposCobranca(PesquisaTiposCobranca.PesquisarTiposCobranca(''));
  if vCobranca = nil then
    Exit;

  Modo(True);
  PreencherRegistro(vCobranca);

  eID.Clear;
end;

procedure TFormCadastroTiposCobranca.VerificarRegistro(Sender: TObject);
begin
  inherited;

  if eNome.Trim = '' then begin
    _Biblioteca.Exclamar('� necess�rio informar o nome do tipo de cobran�a!');
    _Biblioteca.SetarFoco(eNome);
    Abort;
  end;

  if rgFormaPagamento.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('� necess�rio informar a forma de pagamento deste tipo de cobran�a!');
    _Biblioteca.SetarFoco(rgFormaPagamento);
    Abort;
  end;

  if cbIncidenciaFinanceira.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('� necess�rio informar a incid�ncia financeira deste tipo de cobran�a!');
    _Biblioteca.SetarFoco(cbIncidenciaFinanceira);
    Abort;
  end;

  if
    FrDiasPrazo.EstaVazio and
    (rgFormaPagamento.ItemIndex > 0)
  then begin
    _Biblioteca.Exclamar('A quantidade de dias de prazo para esse tipo de cobran�a deve ser informado!');
    _Biblioteca.SetarFoco(FrDiasPrazo);
    Abort;
  end;

  if cbTipoPagamentoComissao.ItemIndex = -1 then begin
    _Biblioteca.Exclamar('� necess�rio informar o tipo de pagamento de comiss�o, verifique!');
    _Biblioteca.SetarFoco(cbTipoPagamentoComissao);
    Abort;
  end;

  if (rgFormaPagamento.GetValor = 'CRT') and (rgTipoCartao.GetValor = '') then begin
    _Biblioteca.Exclamar('� necess�rio informar o tipo de cart�o!');
    _Biblioteca.SetarFoco(rgTipoCartao);
    Abort;
  end;

  if ckUtilizarLimiteCredito.Checked and (rgFormaPagamento.GetValor = 'CRT') then begin
    if
      not Perguntar(
        'A forma de pagamento foi definida como cart�o, por�m a op��o de "1 - Utilizar do limite de cr�dito do cliente" foi marcada sendo que esta forma de ' +
        'pagamento n�o � um limite que a loja controla diretamente, deseja realmente continuar?'
      )
    then begin
      _Biblioteca.SetarFoco(ckUtilizarLimiteCredito);
      Abort;
    end;
  end;

  if rgFormaPagamento.GetValor = 'CRT' then begin
    if cbRedeCartao.ItemIndex = -1 then begin
      Exclamar('� necess�rio informar a rede do cart�o!');
      _Biblioteca.SetarFoco(cbRedeCartao);
      Abort;
    end;

    if cbTipoRecebimentoCartao.ItemIndex = -1 then begin
      Exclamar('� necess�rio informar o tipo de recebimento do cart�o!');
      _Biblioteca.SetarFoco(cbTipoRecebimentoCartao);
      Abort;
    end;

    if cbRedeCartaoNFe.ItemIndex = -1 then begin
      Exclamar('� necess�rio informar a bandeira do cart�o!');
      _Biblioteca.SetarFoco(eBandeira);
      Abort;
    end;
  end;

  if FrPortador.EstaVazio then begin
    _Biblioteca.Exclamar('O portador do tipo de cobran�a n�o foi informado corretamente, verifique!');
    _Biblioteca.SetarFoco(FrPortador);
    Abort;
  end;

  if ckPermitirPrazoMedio.Checked and (not rgFormaPagamento.ItemIndex in[1,3]) then begin
    _Biblioteca.Exclamar('Somente as formas de pagamento "Cheque" e "Cobran�a" podem aceitar vendas no prazo m�dio!');
    _Biblioteca.SetarFoco(ckPermitirPrazoMedio);
    Abort;
  end;

  if rgFormaPagamento.GetValor = 'ACU' then begin
    if cbIncidenciaFinanceira.GetValor <> 'R' then begin
      _Biblioteca.Exclamar('A forma de pagamento acumulativo s� pode ter a incid�ncia financeira como "Contas a receber"!');
      _Biblioteca.SetarFoco(cbIncidenciaFinanceira);
      Abort;
    end;
  end;

  if ckEmitirDuplicMercantilVenda.Checked then begin
    if rgFormaPagamento.GetValor <> 'COB' then begin
      _Biblioteca.Exclamar('O par�metro "4 - Emitir duplic. merc.de venda" somente pode ser utilizado quando a forma de pagamento for "Cobran�a", verifique!');
      _Biblioteca.SetarFoco(ckEmitirDuplicMercantilVenda);
      Abort;
    end;

    if cbIncidenciaFinanceira.GetValor = 'P' then begin
      _Biblioteca.Exclamar(
        'O par�metro "4 - Emitir duplic. merc.de venda" n�o pode ser utilizado quando o par�metro "Incid�ncia financeira" ' +
        'for somente "Contas a pagar", verifique!'
      );

      _Biblioteca.SetarFoco(ckEmitirDuplicMercantilVenda);
      Abort;
    end;
  end;
end;

end.
