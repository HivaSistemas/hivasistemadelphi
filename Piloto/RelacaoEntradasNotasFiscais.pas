unit RelacaoEntradasNotasFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _EntradasNotasFiscais, _EntradasNotasFiscaisItens,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, FrameProdutos, FrameFornecedores, _Sessao, _Biblioteca,
  _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameEmpresas, FrameNumeros, _RecordsEspeciais,
  FrameDataInicialFinal, _RecordsEstoques, Vcl.StdCtrls, StaticTextLuka, InformacoesEntradaNotaFiscal,
  Vcl.Grids, GridLuka, Vcl.Menus, ComboBoxLuka, CheckBoxLuka,
  FramePlanosFinanceiros, Data.DB, OraCall, DBAccess, Ora, MemDS, Vcl.DBGrids,
  CRGrid, Datasnap.DBClient, frxClass, frxDBSet;

type
  TFormRelacaoEntradaNotasFiscais = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrFornecedores: TFrFornecedores;
    FrDataContabil: TFrDataInicialFinal;
    FrDataCadastro: TFrDataInicialFinal;
    FrNumeroNota: TFrNumeros;
    FrCodigoEntrada: TFrNumeros;
    sgEntradas: TGridLuka;
    splSeparador: TSplitter;
    StaticTextLuka1: TStaticTextLuka;
    sgItens: TGridLuka;
    FrProdutos: TFrProdutos;
    pmOpcoes: TPopupMenu;
    cbAgrupamento: TComboBoxLuka;
    lb1: TLabel;
    FrPlanosFinanceiros: TFrPlanosFinanceiros;
    miDesconsolidarEstoque: TSpeedButton;
    FrDataEmissao: TFrDataInicialFinal;
    frxSintetico: TfrxReport;
    dstPagar: TfrxDBDataset;
    cdsPagar: TClientDataSet;
    cdsPagarEntrada: TStringField;
    cdsPagarFornecedor: TStringField;
    cdsPagarNumeroNota: TStringField;
    cdsPagarCFOP: TStringField;
    cdsPagarDataLancamento: TDateField;
    cdsPagarValorTotal: TFloatField;
    cdsPagarEmpresa: TStringField;
    cdsPagarUsuario: TStringField;
    frxAnalitico: TfrxReport;
    cdsItensProduto: TClientDataSet;
    dsItensEntrada: TfrxDBDataset;
    cdsItensProdutoproduto: TIntegerField;
    cdsItensProdutonome: TStringField;
    cdsItensProdutoCST: TStringField;
    cdsItensProdutoprc_unitario: TFloatField;
    cdsItensProdutounidade: TStringField;
    cdsItensProdutovlr_desc: TFloatField;
    cdsItensProdutooutras_despesas: TFloatField;
    cdsItensProdutovlr_total: TFloatField;
    cdsItensProdutoqtde: TFloatField;
    cdsItensProdutoentradaId: TStringField;
    procedure sgEntradasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure miDesconsolidarEstoqueClick(Sender: TObject);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgEntradasClick(Sender: TObject);
    procedure sgEntradasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgEntradasDblClick(Sender: TObject);
  private
    FItens: TArray<RecEntradaNotaFiscalItem>;
    vComando: string;
    vComandoPadrao: string;
    vTotalEntradas: Double;

  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses SelecionarVariosOpcoes;

{ TFormRelacaoEntradaNotasFiscais }

const
  ceEntradaId       = 0;
  ceFornecedor      = 1;
  ceStatusAnalitico = 2;
  ceNumeroNota      = 3;
  ceValorTotalNota  = 4;
  ceDataCadastro    = 5;
  ceDataContabil    = 6;
  ceCFOP            = 7;
  ceBaseICMS        = 8;
  ceValorICMS       = 9;
  ceBaseICMSST      = 10;
  ceValorICMSST     = 11;
  ceValorIPI        = 12;
  ceValorPIS        = 13;
  ceValorCOFINS     = 14;
  ceEmpresa         = 15;
  ceUsuarioCadastro = 16;
  ceStatus          = 17;

  (* Grid de produtos *)
  ciProdutoId         = 0;
  ciNome              = 1;
  ciCST               = 2;
  ciPrecoUnitario     = 3;
  ciQuantidade        = 4;
  ciQtdeEmbalagem     = 5;
  ciUnidade           = 6;
  ciValorDesconto     = 7;
  ciOutrasDespesas    = 8;
  ciValorTotal        = 9;
  ciBaseCalculoICMS   = 10;
  ciIndReducaoICMS    = 11;
  ciPercICMS          = 12;
  ciValorICMS         = 13;
  ciBaseCalculoICMSST = 14;
  ciIndReducaoICMSST  = 15;
  ciValorICMSST       = 16;
  ciCSTPIS            = 17;
  ciPercPIS           = 18;
  ciValorPIS          = 19;
  ciCSTCOFINS         = 20;
  ciPercCOFINS        = 21;
  ciValorCOFINS       = 22;
  ciPercIVA           = 23;
  ciPrecoPauta        = 24;
  ciPercIPI           = 25;
  ciValorIPI          = 26;

procedure TFormRelacaoEntradaNotasFiscais.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vEntradasIds: TArray<Integer>;
  vEntradas: TArray<RecEntradaNotaFiscal>;
  vTotalValorNota: Double;
  vTotalBaseCalculoIcms: Double;
  vTotalValorIcms: Double;
  vTotalBaseCalculoIcmsSt: Double;
  vTotalValorImsSt: Double;
  vTotalValorIpi: Double;
  vTotalValorPis: Double;
  vTotalValorCofins: Double;
  vLinha: Integer;
begin
  inherited;

  vSql := '';
  FItens := nil;
  vEntradasIds := nil;
  sgEntradas.ClearGrid();
  sgItens.ClearGrid();

  vTotalEntradas          := 0;
  vTotalValorNota         := 0;
  vTotalBaseCalculoIcms   := 0;
  vTotalValorIcms         := 0;
  vTotalBaseCalculoIcmsSt := 0;
  vTotalValorImsSt        := 0;
  vTotalValorIpi          := 0;
  vTotalValorPis          := 0;
  vTotalValorCofins       := 0;

  if not FrEmpresas.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrEmpresas.getSqlFiltros('ENT.EMPRESA_ID'));

  if not FrFornecedores.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrFornecedores.getSqlFiltros('ENT.FORNECEDOR_ID'));

  if not FrCodigoEntrada.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrCodigoEntrada.TrazerFiltros('ENT.ENTRADA_ID'));

  if not FrNumeroNota.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrNumeroNota.TrazerFiltros('ENT.NUMERO_NOTA'));

  if not FrProdutos.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, 'ENTRADA_ID in(select ENTRADA_ID from ENTRADAS_NOTAS_FISCAIS_ITENS where ' + FrProdutos.getSqlFiltros('PRODUTO_ID') + ') ');

  if not FrDataCadastro.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrDataCadastro.getSqlFiltros('trunc(ENT.DATA_HORA_CADASTRO)'));

  if not FrDataemissao.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrDataEmissao.getSqlFiltros('trunc(ENT.DATA_HORA_EMISSAO)'));

  if not FrDataContabil.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrDataContabil.getSqlFiltros('trunc(ENT.DATA_HORA_EMISSAO)'));

  if not FrPlanosFinanceiros.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrPlanosFinanceiros.getSqlFiltros('ENT.PLANO_FINANCEIRO_ID'));

  vSql := vSql + 'order by ENT.ENTRADA_ID ';
  vComando := vSql;

  vEntradas := _EntradasNotasFiscais.BuscarEntradaNotaFiscaisComando(Sessao.getConexaoBanco, vSql);
  if vEntradas = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vEntradas) to High(vEntradas) do begin
    sgEntradas.Cells[ceEntradaId, i + 1]       := NFormat(vEntradas[i].entrada_id);
    sgEntradas.Cells[ceFornecedor, i + 1]      := NFormat(vEntradas[i].fornecedor_id) + ' - ' + vEntradas[i].nome_fantasia;
    sgEntradas.Cells[ceStatusAnalitico, i + 1] := vEntradas[i].StatusAnalitico;
    sgEntradas.Cells[ceNumeroNota, i + 1]      := NFormat(vEntradas[i].numero_nota);
    sgEntradas.Cells[ceValorTotalNota, i + 1]  := NFormat(vEntradas[i].valor_total);
    sgEntradas.Cells[ceDataCadastro, i + 1]    := DFormatN(vEntradas[i].data_hora_cadastro);
    sgEntradas.Cells[ceDataContabil, i + 1]    := DFormatN(vEntradas[i].DataEntrada);
    sgEntradas.Cells[ceCFOP, i + 1]            := vEntradas[i].CfopId;
    sgEntradas.Cells[ceBaseICMS, i + 1]        := NFormatN(vEntradas[i].base_calculo_icms);
    sgEntradas.Cells[ceValorICMS, i + 1]       := NFormatN(vEntradas[i].valor_icms);
    sgEntradas.Cells[ceBaseICMSST, i + 1]      := NFormatN(vEntradas[i].base_calculo_icms_st);
    sgEntradas.Cells[ceValorICMSST, i + 1]     := NFormatN(vEntradas[i].valor_icms_st);
    sgEntradas.Cells[ceValorIPI, i + 1]        := NFormatN(vEntradas[i].valor_ipi);
    sgEntradas.Cells[ceValorPIS, i + 1]        := NFormatN(vEntradas[i].valor_pis);
    sgEntradas.Cells[ceValorCOFINS, i + 1]     := NFormatN(vEntradas[i].valor_cofins);
    sgEntradas.Cells[ceEmpresa, i + 1]         := NFormat(vEntradas[i].empresa_id) + ' - ' + vEntradas[i].nome_fantasia;
    sgEntradas.Cells[ceUsuarioCadastro, i + 1] := NFormat(vEntradas[i].UsuarioCadastroId) + ' - ' + vEntradas[i].NomeUsuarioCadastro;
    sgEntradas.Cells[ceStatus, i + 1]          := vEntradas[i].Status;

    _Biblioteca.AddNoVetorSemRepetir(vEntradasIds, vEntradas[i].entrada_id);

    vTotalValorNota         := vTotalValorNota + vEntradas[i].valor_total;
    vTotalBaseCalculoIcms   := vTotalBaseCalculoIcms + vEntradas[i].base_calculo_icms;
    vTotalValorIcms         := vTotalValorIcms + vEntradas[i].valor_icms;
    vTotalBaseCalculoIcmsSt := vTotalBaseCalculoIcmsSt + vEntradas[i].base_calculo_icms_st;
    vTotalValorImsSt        := vTotalValorImsSt + vEntradas[i].valor_icms_st;
    vTotalValorIpi          := vTotalValorIpi + vEntradas[i].valor_ipi;
    vTotalValorPis          := vTotalValorPis + vEntradas[i].valor_pis;
    vTotalValorCofins       := vTotalValorCofins + vEntradas[i].valor_cofins;
  end;

  vLinha := Length(vEntradas) + 1;
  sgEntradas.SetLinhasGridPorTamanhoVetor(vLinha);

  sgEntradas.Cells[ceNumeroNota, vLinha]      := 'TOTAIS: ';
  sgEntradas.Cells[ceValorTotalNota, vLinha]  := NFormatN(vTotalValorNota);
  sgEntradas.Cells[ceBaseICMS, vLinha]        := NFormatN(vTotalBaseCalculoIcms);
  sgEntradas.Cells[ceValorICMS, vLinha]       := NFormatN(vTotalValorIcms);
  sgEntradas.Cells[ceBaseICMSST, vLinha]      := NFormatN(vTotalBaseCalculoIcmsSt);
  sgEntradas.Cells[ceValorICMSST, vLinha]     := NFormatN(vTotalValorImsSt);
  sgEntradas.Cells[ceValorIPI, vLinha]        := NFormatN(vTotalValorIpi);
  sgEntradas.Cells[ceValorPIS, vLinha]        := NFormatN(vTotalValorPis);
  sgEntradas.Cells[ceValorCOFINS, vLinha]     := NFormatN(vTotalValorCofins);

  vTotalEntradas := vTotalValorNota;

  FItens := _EntradasNotasFiscaisItens.BuscarEntradaNotaFiscalItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.ENTRADA_ID', vEntradasIds));

  dsItensEntrada.RangeEnd := reCount;
  dsItensEntrada.RangeEndCount := High(FItens);

  sgEntradasClick(Sender);
  SetarFoco(sgEntradas);
end;

procedure TFormRelacaoEntradaNotasFiscais.FormShow(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId, False);
  SetarFoco(FrEmpresas);
end;

procedure TFormRelacaoEntradaNotasFiscais.Imprimir(Sender: TObject);
var
  vCnt: Integer;
  totalPagar: Double;
  vOpcao: TRetornoTelaFinalizar<Integer>;
begin
  vOpcao := SelecionarVariosOpcoes.Selecionar('Tipo relatório', ['Sintético', 'Análitico'], 0);
  if vOpcao.BuscaCancelada then
    Exit;

  cdsPagar.Close;
  cdsPagar.CreateDataSet;
  cdsPagar.Open;
  inherited;

  for vCnt := sgEntradas.RowCount - 2 downto 1 do
  begin
    cdsPagar.Insert;
    cdsPagarEntrada.AsString    := IntToStr(SFormatInt(sgEntradas.Cells[ceEntradaId,vCnt]));
    cdsPagarFornecedor.AsString := sgEntradas.Cells[ceFornecedor,vCnt];
    cdsPagarNumeroNota.AsString := sgEntradas.Cells[ceNumeroNota,vCnt];
    cdsPagarValorTotal.AsFloat  := SFormatDouble(sgEntradas.Cells[ceValorTotalNota,vCnt]);
    cdsPagarCFOP.AsString       := sgEntradas.Cells[ceCFOP,vCnt];
    cdsPagarEmpresa.AsString    := sgEntradas.Cells[ceEmpresa,vCnt];
    cdsPagarUsuario.AsString    := sgEntradas.Cells[ceUsuarioCadastro,vCnt];
    cdsPagarDataLancamento.AsDateTime := ToData(sgEntradas.Cells[ceDataCadastro,vCnt]);
    cdsPagar.Post;
  end;
  cdsPagar.First;
  case vOpcao.Dados of
    0://Sintético
    begin

      TfrxMemoView(frxSintetico.FindComponent('mmValorProduto')).Text :=   'Valor Total.....: ' + NFormat(vTotalEntradas);
      frxSintetico.ShowReport;
    end;
    1: //Análitico
    begin

      cdsItensProduto.Close;
      cdsItensProduto.CreateDataSet;
      cdsItensProduto.Open;
      inherited;

      for vCnt := High(FItens) downto Low(FItens) do
      begin
        cdsItensProduto.Insert;
        cdsItensProdutoentradaId.AsInteger    := FItens[vCnt].entrada_id;
        cdsItensProdutoproduto.AsInteger      := FItens[vCnt].produto_id;
        cdsItensProdutonome.AsString          := FItens[vCnt].nome_produto;
        cdsItensProdutoCST.AsString           := FItens[vCnt].cst;
        cdsItensProdutoprc_unitario.AsFloat   := FItens[vCnt].preco_unitario;
        cdsItensProdutoqtde.AsFloat           := FItens[vCnt].quantidade;
        cdsItensProdutounidade.AsString       := FItens[vCnt].unidade;
        cdsItensProdutovlr_desc.AsFloat       := FItens[vCnt].valor_total_desconto;
        cdsItensProdutooutras_despesas.AsFloat:= FItens[vCnt].valor_total_outras_despesas;
        cdsItensProdutovlr_total.AsFloat      := FItens[vCnt].valor_total;
        cdsItensProduto.Post;
      end;
      cdsItensProduto.First;
      TfrxMemoView(frxAnalitico.FindComponent('mmValorProduto')).Text :=   'Valor Total.....: ' + NFormat(vTotalEntradas);
      frxAnalitico.ShowReport;
//      frxReport1.ShowReport;


    end;
  end;
     cdsPagar.SaveToFile('c:\temp\capa.xml',dfXML);
     cdsItensProduto.SaveToFile('c:\temp\item.xml',dfXML);
end;

procedure TFormRelacaoEntradaNotasFiscais.miDesconsolidarEstoqueClick(Sender: TObject);
var
  vEntradaId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vEntradaId := SFormatInt(sgEntradas.Cells[ceEntradaId, sgEntradas.Row]);
  if vEntradaId = 0 then
    Exit;

  if sgEntradas.Cells[ceStatus, sgEntradas.Row] <> 'BAI' then begin
    _Biblioteca.Exclamar('Somente entrada baixada pode ter a desconsolidação do estoque!');
    Exit;
  end;

  vRetBanco := _EntradasNotasFiscais.DesconsolidarEstoque(Sessao.getConexaoBanco, vEntradaId);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoEntradaNotasFiscais.sgEntradasClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vEntradaId: Integer;
begin
  inherited;
  vLinha := 0;
  sgItens.ClearGrid();

  vEntradaId := SFormatInt(sgEntradas.Cells[ceEntradaId, sgEntradas.Row]);
  if vEntradaId = 0 then
    Exit;

  for i := Low(FItens) to High(FItens) do begin
    if vEntradaId <> FItens[i].entrada_id then
      Continue;

    Inc(vLinha);

    sgItens.Cells[ciProdutoId, vLinha]         := NFormat(FItens[i].produto_id);
    sgItens.Cells[ciNome, vLinha]              := FItens[i].nome_produto;
    sgItens.Cells[ciCST, vLinha]               := FItens[i].cst;
    sgItens.Cells[ciPrecoUnitario, vLinha]     := NFormat(FItens[i].preco_unitario);
    sgItens.Cells[ciQuantidade, vLinha]        := NFormat(FItens[i].quantidade);
    sgItens.Cells[ciQtdeEmbalagem, vLinha]     := NFormat(FItens[i].QuantidadeEmbalagem);
    sgItens.Cells[ciUnidade, vLinha]           := FItens[i].unidade;
    sgItens.Cells[ciValorDesconto, vLinha]     := NFormatN(FItens[i].valor_total_desconto);
    sgItens.Cells[ciOutrasDespesas, vLinha]    := NFormatN(FItens[i].valor_total_outras_despesas);
    sgItens.Cells[ciValorTotal, vLinha]        := NFormat(FItens[i].valor_total);
    sgItens.Cells[ciBaseCalculoICMS, vLinha]   := NFormatN(FItens[i].base_calculo_icms);
    sgItens.Cells[ciIndReducaoICMS, vLinha]    := NFormatN(FItens[i].indice_reducao_base_icms, 5);
    sgItens.Cells[ciPercICMS, vLinha]          := NFormatN(FItens[i].percentual_icms);
    sgItens.Cells[ciValorICMS, vLinha]         := NFormatN(FItens[i].valor_icms);
    sgItens.Cells[ciBaseCalculoICMSST, vLinha] := NFormatN(FItens[i].base_calculo_icms_st);
    sgItens.Cells[ciIndReducaoICMSST, vLinha]  := NFormatN(FItens[i].indice_reducao_base_icms_st, 5);
    sgItens.Cells[ciValorICMSST, vLinha]       := NFormatN(FItens[i].valor_icms_st);
    sgItens.Cells[ciCSTPIS, vLinha]            := FItens[i].cst_pis;
    sgItens.Cells[ciPercPIS, vLinha]           := NFormatN(FItens[i].percentual_pis);
    sgItens.Cells[ciValorPIS, vLinha]          := NFormatN(FItens[i].valor_pis);
    sgItens.Cells[ciCSTCOFINS, vLinha]         := FItens[i].cst_cofins;
    sgItens.Cells[ciPercCOFINS, vLinha]        := NFormatN(FItens[i].percentual_cofins);
    sgItens.Cells[ciValorCOFINS, vLinha]       := NFormatN(FItens[i].valor_cofins);
    sgItens.Cells[ciPercIVA, vLinha]           := NFormatN(FItens[i].iva);
    sgItens.Cells[ciPrecoPauta, vLinha]        := NFormatN(FItens[i].preco_pauta);
    sgItens.Cells[ciPercIPI, vLinha]           := NFormatN(FItens[i].percentual_ipi);
    sgItens.Cells[ciValorIPI, vLinha]          := NFormatN(FItens[i].valor_ipi);
  end;
  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
end;

procedure TFormRelacaoEntradaNotasFiscais.sgEntradasDblClick(Sender: TObject);
begin
  inherited;
  InformacoesEntradaNotaFiscal.Informar( SFormatInt(sgEntradas.Cells[ceEntradaId, sgEntradas.Row]) );
end;

procedure TFormRelacaoEntradaNotasFiscais.sgEntradasDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[ceEntradaId, ceNumeroNota, ceValorTotalNota, ceBaseICMS, ceValorICMS, ceBaseICMSST, ceValorICMSST, ceValorIPI, ceValorPIS, ceValorCOFINS] then
    vAlinhamento := taRightJustify
  else if ACol = ceStatusAnalitico then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgEntradas.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoEntradaNotasFiscais.sgEntradasGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = ceStatusAnalitico then begin
    AFont.Style := [fsBold];
    AFont.Color := _EntradasNotasFiscais.getCorStatus( sgEntradas.Cells[ceStatus, ARow] );
  end;

  if ARow = sgEntradas.RowCount -1 then
  begin
    AFont.Color := clWhite;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end;
end;

procedure TFormRelacaoEntradaNotasFiscais.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ciNome, ciUnidade] then
    vAlinhamento := taLeftJustify
  else if ACol = ciCST then
    vAlinhamento := taCenter
  else
    vAlinhamento := taRightJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoEntradaNotasFiscais.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
