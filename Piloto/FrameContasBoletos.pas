unit FrameContasBoletos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Menus,_ContasBoletos,_Biblioteca,
  Vcl.Buttons, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, PesquisaContasBoletos;

type
  TFrContasBoletos = class(TFrameHenrancaPesquisas)
  private
    { Private declarations }
  public
    function getDados(pLinha: Integer = -1): RecContasBoletos;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

var
  FrContasBoletos: TFrContasBoletos;

implementation

{$R *.dfm}

{ TFrContasBoletos }

function TFrContasBoletos.AdicionarDireto: TObject;
var
  vDados: TArray<RecContasBoletos>;
begin
  vDados := _ContasBoletos.BuscarContasBoletos(Sessao.getConexaoBanco, 0, [FChaveDigitada]);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrContasBoletos.AdicionarPesquisando: TObject;
begin
  Result := PesquisaContasBoletos.PesquisarContasBoletos('',ckSomenteAtivos.Checked);
end;

function TFrContasBoletos.getDados(pLinha: Integer): RecContasBoletos;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecContasBoletos(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrContasBoletos.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecContasBoletos(FDados[i]).ContaboletoId = RecContasBoletos(pSender).ContaboletoId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrContasBoletos.MontarGrid;
var
  i: Integer;
  vSender: RecContasBoletos;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      vSender := RecContasBoletos(FDados[i]);
      AAdd([IntToStr(vSender.ContaboletoId), vSender.Descricao]);
    end;
  end;
end;

end.
