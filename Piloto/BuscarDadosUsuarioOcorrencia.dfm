inherited FormBuscarDadosUsuarioOcorrencia: TFormBuscarDadosUsuarioOcorrencia
  Caption = 'Buscar dados usu'#225'rio ocorr'#234'ncia'
  ClientHeight = 101
  ClientWidth = 395
  ExplicitWidth = 401
  ExplicitHeight = 130
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 64
    Width = 395
    ExplicitTop = 64
    ExplicitWidth = 417
    inherited sbFinalizar: TSpeedButton
      Left = 137
      ExplicitLeft = 137
    end
  end
  inline FrFuncionarios: TFrFuncionarios
    Left = 6
    Top = 4
    Width = 380
    Height = 50
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 6
    ExplicitTop = 4
    ExplicitWidth = 380
    ExplicitHeight = 50
    inherited sgPesquisa: TGridLuka
      Width = 355
      Height = 33
      ExplicitWidth = 355
      ExplicitHeight = 33
    end
    inherited PnTitulos: TPanel
      Width = 380
      ExplicitWidth = 380
      inherited lbNomePesquisa: TLabel
        Width = 71
        ExplicitWidth = 71
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 275
        ExplicitLeft = 275
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 355
      Height = 34
      ExplicitLeft = 355
      ExplicitHeight = 34
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
end
