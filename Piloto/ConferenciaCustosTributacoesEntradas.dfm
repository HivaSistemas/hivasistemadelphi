inherited FormConferenciaCustosTributacoesEntradas: TFormConferenciaCustosTributacoesEntradas
  Caption = 'Confer'#234'ncia de custos na entrada'
  ClientHeight = 471
  ClientWidth = 794
  ExplicitWidth = 800
  ExplicitHeight = 500
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 434
    Width = 794
    ExplicitTop = 434
    ExplicitWidth = 794
  end
  object sgItens: TGridLuka
    Left = 1
    Top = 1
    Width = 791
    Height = 431
    ColCount = 8
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 1
    OnDrawCell = sgItensDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Quantidade'
      'Und.'
      'Pre'#231'o tabela'
      'Pre'#231'o tab.atual'
      '%Varia'#231#227'o')
    OnGetCellColor = sgItensGetCellColor
    Grid3D = False
    RealColCount = 13
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      51
      244
      116
      72
      33
      77
      91
      69)
  end
end
