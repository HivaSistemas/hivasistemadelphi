inherited FormInformacoesAcumulado: TFormInformacoesAcumulado
  Caption = 'Informa'#231#245'es do acumulado'
  ClientHeight = 571
  ClientWidth = 844
  OnShow = FormShow
  ExplicitTop = -43
  ExplicitWidth = 850
  ExplicitHeight = 600
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 5
    Top = 1
    Width = 61
    Height = 14
    Caption = 'Acumulado'
  end
  object lb2: TLabel [1]
    Left = 84
    Top = 1
    Width = 39
    Height = 14
    Caption = 'Cliente'
  end
  object lb13: TLabel [2]
    Left = 358
    Top = 1
    Width = 47
    Height = 14
    Caption = 'Empresa'
  end
  inherited pnOpcoes: TPanel
    Top = 534
    Width = 844
    ExplicitTop = 534
    ExplicitWidth = 844
  end
  object eAcumuladoId: TEditLuka
    Left = 5
    Top = 15
    Width = 76
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 1
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eCliente: TEditLuka
    Left = 84
    Top = 15
    Width = 269
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 2
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object eEmpresa: TEditLuka
    Left = 358
    Top = 15
    Width = 275
    Height = 22
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    TipoCampo = tcTexto
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 0
    NaoAceitarEspaco = False
  end
  object pcDados: TPageControl
    Left = 0
    Top = 38
    Width = 844
    Height = 496
    ActivePage = tsGerais
    Align = alBottom
    TabOrder = 4
    object tsGerais: TTabSheet
      Caption = 'Gerais'
      ExplicitLeft = 8
      object lb3: TLabel
        Left = 191
        Top = 43
        Width = 110
        Height = 14
        Caption = 'Data/hora do acerto'
      end
      object lb4: TLabel
        Left = 191
        Top = 88
        Width = 128
        Height = 14
        Caption = 'Data/hora recebimento'
      end
      object lb5: TLabel
        Left = 456
        Top = 152
        Width = 27
        Height = 14
        Caption = 'Total'
      end
      object lb6: TLabel
        Left = 2
        Top = 152
        Width = 79
        Height = 14
        Caption = 'Total produtos'
      end
      object lb7: TLabel
        Left = 96
        Top = 152
        Width = 69
        Height = 14
        Caption = 'Outras desp.'
      end
      object lb8: TLabel
        Left = 189
        Top = 152
        Width = 51
        Height = 14
        Caption = 'Desconto'
      end
      object lb9: TLabel
        Left = 83
        Top = 170
        Width = 10
        Height = 14
        Caption = '+'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb10: TLabel
        Left = 182
        Top = 170
        Width = 5
        Height = 14
        Caption = '-'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb11: TLabel
        Left = 440
        Top = 167
        Width = 13
        Height = 19
        Caption = '='
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb15: TLabel
        Left = 2
        Top = 43
        Width = 93
        Height = 14
        Caption = 'Usu'#225'rio inser'#231#227'o'
      end
      object lb16: TLabel
        Left = 2
        Top = 88
        Width = 115
        Height = 14
        Caption = 'Usu'#225'rio recebimento'
      end
      object lb17: TLabel
        Left = 330
        Top = 88
        Width = 30
        Height = 14
        Caption = 'Turno'
      end
      object lb18: TLabel
        Left = 2
        Top = 3
        Width = 132
        Height = 14
        Caption = 'Condi'#231#227'o de pagamento'
      end
      object lb22: TLabel
        Left = 263
        Top = 170
        Width = 10
        Height = 14
        Caption = '+'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb23: TLabel
        Left = 275
        Top = 152
        Width = 57
        Height = 14
        Caption = 'Valor frete'
      end
      object lb19: TLabel
        Left = 357
        Top = 152
        Width = 58
        Height = 14
        Caption = 'Valor juros'
      end
      object lb20: TLabel
        Left = 343
        Top = 170
        Width = 10
        Height = 14
        Caption = '+'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lb25: TLabel
        Left = 113
        Top = 198
        Width = 157
        Height = 14
        Caption = #205'ndice de desconto de venda'
        Visible = False
      end
      object sbInformacoesTurno: TSpeedButtonLuka
        Left = 386
        Top = 106
        Width = 18
        Height = 18
        Hint = 'Abrir informa'#231#245'es do turno de recebimento acumulado'
        Flat = True
        NumGlyphs = 2
        OnClick = sbInformacoesTurnoClick
        TagImagem = 13
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object SpeedButtonLuka1: TSpeedButtonLuka
        Left = 1
        Top = 201
        Width = 117
        Height = 35
        Caption = 'Negocia'#231#245'es'
        Flat = True
        Visible = False
        TagImagem = 14
        PedirCertificacao = False
        PermitirAutOutroUsuario = False
      end
      object eDataCadastro: TEditLukaData
        Left = 191
        Top = 57
        Width = 131
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        ReadOnly = True
        TabOrder = 0
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object eDataRecebimento: TEditLukaData
        Left = 191
        Top = 102
        Width = 133
        Height = 22
        Hint = 
          'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
          'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
        EditMask = '99/99/9999 99:99:99;1;_'
        MaxLength = 19
        ReadOnly = True
        TabOrder = 1
        Text = '  /  /       :  :  '
        TipoData = tdDataHora
      end
      object eTotalAcumulado: TEditLuka
        Left = 456
        Top = 166
        Width = 70
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eTotalProdutos: TEditLuka
        Left = 2
        Top = 166
        Width = 79
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eOutrasDespesas: TEditLuka
        Left = 96
        Top = 166
        Width = 83
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorDesconto: TEditLuka
        Left = 189
        Top = 166
        Width = 70
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = '|'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 5
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eUsuarioCadastro: TEditLuka
        Left = 2
        Top = 57
        Width = 183
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 6
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eUsuarioRecebimento: TEditLuka
        Left = 2
        Top = 102
        Width = 183
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 7
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eTurnoId: TEditLuka
        Left = 330
        Top = 102
        Width = 57
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 8
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object stSPC: TStaticText
        Left = 2
        Top = 134
        Width = 551
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Totais do acumulado'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 9
        Transparent = False
      end
      object eCondicaoPagamento: TEditLuka
        Left = 2
        Top = 17
        Width = 321
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 10
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object eValorFrete: TEditLuka
        Left = 275
        Top = 166
        Width = 65
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 11
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      object eValorJuros: TEditLuka
        Left = 357
        Top = 166
        Width = 78
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = '|'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 12
        Text = '0,00'
        TipoCampo = tcNumerico
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 2
        NaoAceitarEspaco = False
      end
      inline FrFechamento: TFrFechamentoJuros
        Left = 544
        Top = 0
        Width = 290
        Height = 338
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 13
        TabStop = True
        ExplicitLeft = 544
        ExplicitHeight = 338
        inherited gbFechamento: TGroupBox
          Height = 338
          Caption = ''
          ExplicitHeight = 297
          inherited lb20: TLabel
            Width = 9
            Height = 14
            ExplicitWidth = 9
            ExplicitHeight = 14
          end
          inherited lb7: TLabel
            Width = 9
            Height = 14
            ExplicitWidth = 9
            ExplicitHeight = 14
          end
          inherited lb11: TLabel
            Top = 156
            ExplicitTop = 156
          end
          inherited lb12: TLabel
            Top = 176
            ExplicitTop = 176
          end
          inherited lb13: TLabel
            Top = 196
            ExplicitTop = 196
          end
          inherited lb14: TLabel
            Top = 236
            ExplicitTop = 236
          end
          inherited lb15: TLabel
            Top = 276
            ExplicitTop = 276
          end
          inherited sbBuscarDadosCheques: TSpeedButton
            Top = 172
            ExplicitTop = 172
          end
          inherited sbBuscarDadosCartoesDebito: TSpeedButton
            Top = 192
            ExplicitTop = 192
          end
          inherited sbBuscarDadosCobranca: TSpeedButton
            Top = 232
            ExplicitTop = 232
          end
          inherited lb10: TLabel
            Top = 297
            Visible = False
            ExplicitTop = 297
          end
          inherited lb2: TLabel
            Top = 209
            ExplicitTop = 209
          end
          inherited sbBuscarCreditos: TSpeedButton
            Top = 272
            ExplicitTop = 272
          end
          inherited lb4: TLabel
            Top = 216
            ExplicitTop = 216
          end
          inherited sbBuscarDadosCartoesCredito: TSpeedButton
            Top = 212
            ExplicitTop = 212
          end
          inherited lb6: TLabel
            Width = 9
            Height = 14
            ExplicitWidth = 9
            ExplicitHeight = 14
          end
          inherited Label1: TLabel
            Top = 256
            ExplicitTop = 256
          end
          inherited sbBuscarDadosCobrancaPix: TSpeedButton
            Top = 252
            ExplicitTop = 252
          end
          inherited ePercentualOutrasDespesas: TEditLuka
            Top = 56
            Height = 22
            ExplicitTop = 56
            ExplicitHeight = 22
          end
          inherited eValorDesconto: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eValorOutrasDespesas: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited ePercentualDesconto: TEditLuka
            Top = 76
            Height = 22
            ExplicitTop = 76
            ExplicitHeight = 22
          end
          inherited eValorDinheiro: TEditLuka
            Top = 149
            Height = 22
            ExplicitTop = 149
            ExplicitHeight = 22
          end
          inherited eValorCheque: TEditLuka
            Top = 169
            Height = 22
            ExplicitTop = 169
            ExplicitHeight = 22
          end
          inherited eValorCartaoDebito: TEditLuka
            Top = 189
            Height = 22
            ExplicitTop = 189
            ExplicitHeight = 22
          end
          inherited eValorCobranca: TEditLuka
            Top = 229
            Height = 22
            ExplicitTop = 229
            ExplicitHeight = 22
          end
          inherited eValorCredito: TEditLuka
            Top = 269
            Height = 22
            ExplicitTop = 269
            ExplicitHeight = 22
          end
          inherited stSPC: TStaticText
            Top = 134
            Color = 38619
            ExplicitTop = 134
          end
          inherited eValorTroco: TEditLuka
            Top = 289
            Visible = False
            ExplicitTop = 289
          end
          inherited eValorAcumulativo: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eValorFinanceira: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eValorFrete: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited eValorCartaoCredito: TEditLuka
            Top = 209
            Height = 22
            ExplicitTop = 209
            ExplicitHeight = 22
          end
          inherited eValorPix: TEditLuka
            Top = 248
            Height = 22
            ExplicitTop = 248
            ExplicitHeight = 22
          end
          inherited eValorJuros: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
          inherited ePercentualJuros: TEditLuka
            Height = 22
            ExplicitHeight = 22
          end
        end
      end
      inline FrEndereco: TFrEndereco
        Left = 2
        Top = 343
        Width = 663
        Height = 88
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 14
        TabStop = True
        ExplicitLeft = 2
        ExplicitTop = 343
        ExplicitWidth = 663
        ExplicitHeight = 88
        inherited eCEP: TEditCEPLuka
          Width = 72
          Height = 22
          ExplicitWidth = 72
          ExplicitHeight = 22
        end
        inherited eLogradouro: TEditLuka
          Height = 22
          ExplicitHeight = 22
        end
        inherited eComplemento: TEditLuka
          Height = 22
          ExplicitHeight = 22
        end
        inherited FrBairro: TFrBairros
          Width = 355
          ExplicitWidth = 355
          inherited sgPesquisa: TGridLuka
            Width = 330
            ExplicitWidth = 355
          end
          inherited PnTitulos: TPanel
            Width = 355
            inherited lbNomePesquisa: TLabel
              Height = 15
            end
            inherited pnSuprimir: TPanel
              Left = 250
            end
          end
          inherited pnPesquisa: TPanel
            Left = 330
            Visible = False
            ExplicitLeft = 325
          end
        end
        inherited ckValidarComplemento: TCheckBox
          Width = 137
          Checked = True
          State = cbChecked
          ExplicitWidth = 137
        end
        inherited ckValidarBairro: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckValidarCEP: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited ckValidarEndereco: TCheckBox
          Width = 127
          Checked = True
          State = cbChecked
          ExplicitWidth = 127
        end
        inherited ePontoReferencia: TEditLuka
          Height = 22
          ExplicitHeight = 22
        end
        inherited ckValidarPontoReferencia: TCheckBox
          Width = 180
          ExplicitWidth = 180
        end
        inherited eNumero: TEditLuka
          Width = 72
          Height = 22
          ExplicitWidth = 72
          ExplicitHeight = 22
        end
      end
      object eIndiceDescontoVenda: TEditLuka
        Left = 113
        Top = 212
        Width = 198
        Height = 22
        CharCase = ecUpperCase
        ReadOnly = True
        TabOrder = 15
        Visible = False
        TipoCampo = tcTexto
        ApenasPositivo = False
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object st3: TStaticText
        Left = 354
        Top = 6
        Width = 173
        Height = 16
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Situa'#231#227'o do acumulado'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 16
        Transparent = False
      end
      object stStatus: TStaticText
        Left = 354
        Top = 22
        Width = 173
        Height = 17
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        BevelKind = bkFlat
        Caption = 'Para recebimento no caixa'
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 17
        Transparent = False
      end
      object StaticText1: TStaticText
        Left = 2
        Top = 299
        Width = 541
        Height = 15
        Alignment = taCenter
        AutoSize = False
        Caption = 'Endere'#231'o de entrega'
        Color = 38619
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 18
        Transparent = False
      end
    end
    object tsPedidos: TTabSheet
      Caption = 'Pedidos'
      ImageIndex = 4
      object sgPedidos: TGridLuka
        Left = 0
        Top = 0
        Width = 836
        Height = 467
        Align = alClient
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        OnDblClick = sgPedidosDblClick
        OnDrawCell = sgPedidosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Pedido'
          'Data/hora recebimento'
          'Vendedor'
          'Valor'
          'Valor devolvido')
        Grid3D = False
        RealColCount = 10
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          50
          143
          182
          88
          97)
      end
    end
    object tsItens: TTabSheet
      Caption = 'Produtos'
      ImageIndex = 1
      object sgProdutos: TGridLuka
        Left = 0
        Top = 0
        Width = 836
        Height = 467
        Align = alClient
        ColCount = 10
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        OnDrawCell = sgProdutosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Pre'#231'o unit.'
          'Quantidade'
          'Und.'
          'Valor desc.'
          'Outras desp.'
          'Valor L'#237'quido'
          'Valor frete')
        Grid3D = False
        RealColCount = 10
        Indicador = True
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          244
          116
          75
          72
          43
          82
          83
          91
          85)
      end
    end
    object tsFinanceiros: TTabSheet
      Caption = 'Financeiros'
      ImageIndex = 5
      OnShow = tsFinanceirosShow
      object sgReceber: TGridLuka
        Left = 0
        Top = 0
        Width = 836
        Height = 467
        Align = alClient
        ColCount = 8
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDblClick = sgReceberDblClick
        OnDrawCell = sgReceberDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Receber'
          'Tipo cobran'#231'a'
          'Status'
          'Documento'
          'Data vencimento'
          'Valor documento'
          'Parcela'
          'Nr.Parc.')
        OnGetCellColor = sgReceberGetCellColor
        Grid3D = False
        RealColCount = 10
        AtivarPopUpSelecao = False
        ColWidths = (
          54
          241
          74
          109
          101
          103
          53
          64)
      end
    end
    object tsObservacoes: TTabSheet
      Caption = 'Observa'#231#245'es'
      ImageIndex = 7
      object lb21: TLabel
        Left = 3
        Top = 0
        Width = 221
        Height = 14
        Caption = 'Obseva'#231#245'es para a nota fiscal eletr'#244'nica'
      end
      object sbAdicionarObservacaoNFe: TSpeedButton
        Left = 588
        Top = 55
        Width = 101
        Height = 28
        Caption = 'Adic. observa'#231#227'o'
      end
      object eObservacoesNotaFiscalEletronica: TMemo
        Left = 3
        Top = 14
        Width = 584
        Height = 69
        MaxLength = 800
        ReadOnly = True
        TabOrder = 0
      end
    end
    object tsNotasFiscais: TTabSheet
      Caption = 'Notas fiscais'
      ImageIndex = 4
      object sgNotasFiscais: TGridLuka
        Left = 0
        Top = 0
        Width = 836
        Height = 467
        Align = alClient
        ColCount = 7
        DefaultRowHeight = 19
        DrawingStyle = gdsGradient
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        TabOrder = 0
        OnDblClick = sgNotasFiscaisDblClick
        OnDrawCell = sgNotasFiscaisDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 16768407
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clMenuHighlight
        HCol.Strings = (
          'Nota'
          'N'#250'mero'
          'Tipo'
          'Data emiss'#227'o'
          'Tipo movimento'
          'Valor total'
          'Empresa')
        Grid3D = False
        RealColCount = 10
        AtivarPopUpSelecao = False
        ColWidths = (
          56
          98
          63
          93
          155
          114
          176)
      end
    end
  end
end
