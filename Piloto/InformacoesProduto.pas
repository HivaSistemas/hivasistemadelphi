unit InformacoesProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, StaticTextLuka, FrameImagem,
  FrameProdutosGruposTributacoesEmpresas, FrameUnidades, Vcl.StdCtrls,
  ComboBoxLuka, CheckBoxLuka, Vcl.Grids, GridLuka, _FrameHenrancaPesquisas,
  FrameProdutos, FrameCodigoOriginalFornecedores, _FrameHerancaPrincipal,
  Frame.HerancaInsercaoExclusao, Frame.ConversaoUnidadesCompras, EditLuka,
  Vcl.ComCtrls, GroupBoxLuka, Frame.DepartamentosSecoesLinhas, FrameMarcas,
  FrameFornecedores, Vcl.Mask, EditLukaData, _Produtos,
  _Sessao, _ProdutosKit, _ProdutosGruposTribEmpresas, _ProdutosMultiplosCompras,
  _ProdutosFornCodOriginais, _Biblioteca, System.Math, _RecordsCadastros;

type
  TFormInformacoesProduto = class(TFormHerancaFinalizar)
    Label1: TLabel;
    eID: TEditLuka;
    eNomeProduto: TEditLuka;
    lbl1: TLabel;
    eDataCadastro: TEditLukaData;
    lb17: TLabel;
    ckAtivo: TCheckBoxLuka;
    pcDados: TPageControl;
    tsPrincipal: TTabSheet;
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb5: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    FrFabricante: TFrFornecedores;
    FrUnidadeVenda: TFrUnidades;
    FrMarca: TFrMarcas;
    eMultiploVenda: TEditLuka;
    ePeso: TEditLuka;
    eVolume: TEditLuka;
    ckAceitarEstoqueNegativo: TCheckBox;
    ckInativarProdutoZerarEstoque: TCheckBox;
    ckBloquearParaCompras: TCheckBox;
    ckBloquearParaVendas: TCheckBox;
    eCodigoBarras: TEditLuka;
    FrLinhaProduto: TFrameDepartamentosSecoesLinhas;
    ckProdutoDiversoPDV: TCheckBox;
    ckPermitirDevolucao: TCheckBox;
    eCodigoBalanca: TEditLuka;
    ckSobEncomenda: TCheckBox;
    GroupBoxLuka1: TGroupBoxLuka;
    lb14: TLabel;
    lb13: TLabel;
    ePercentualComissaoVista: TEditLuka;
    ePercentualComissaoPrazo: TEditLuka;
    ckSujeitoComissao: TCheckBox;
    ckBloquearEntradaSemPedCompra: TCheckBox;
    ckExigirSeparacao: TCheckBox;
    eValorAdicionalFrete: TEditLuka;
    ckExigirModeloNota: TCheckBox;
    ckSepararSomenteCodigoBarras: TCheckBoxLuka;
    tsComplementos: TTabSheet;
    stCaracteristicas: TStaticText;
    eCaracteristicas: TMemo;
    ckRevenda: TCheckBox;
    ckUsoConsumo: TCheckBox;
    ckAtivoImobilizado: TCheckBox;
    ckServico: TCheckBox;
    tsParametrosCompra: TTabSheet;
    lb6: TLabel;
    lbl2: TLabel;
    eNomeCompra: TEditLuka;
    FrMultiplosCompra: TFrameConversaoUnidadesCompra;
    FrCodigoOriginalFornecedores: TFrCodigoOriginalFornecedores;
    eCodigoOriginalFabricante: TEditLuka;
    tsControleEstoque: TTabSheet;
    Label4: TLabel;
    lbNomePesquisa: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    lbl3: TLabel;
    sgProdutosKit: TGridLuka;
    ckExigirDataFabricacaoLote: TCheckBoxLuka;
    ckExigirDataVencimentoLote: TCheckBoxLuka;
    cbTipoControleEstoque: TComboBoxLuka;
    FrUnidadeEntrega: TFrUnidades;
    cbDefinicaoAutomaticaLote: TComboBoxLuka;
    eDiasAvisoVencimento: TEditLuka;
    eMotivoInatividade: TEditLuka;
    tsFiscal: TTabSheet;
    lb4: TLabel;
    lb7: TLabel;
    eNCM: TEditLuka;
    eCEST: TEditLuka;
    FrProdutosGruposTributacoes: TFrProdutosGruposTributacoesEmpresas;
    tsFotos: TTabSheet;
    FrFoto1: TFrImagem;
    FrFoto2: TFrImagem;
    FrFoto3: TFrImagem;
    stctxtlk1: TStaticTextLuka;
    stctxtlk2: TStaticTextLuka;
    stctxtlk3: TStaticTextLuka;
    pPanel: TPanel;
    procedure sgProdutosKitDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgProdutosKitGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    procedure AddProdutoGridKit(pProdutoId: Integer; pNomeProduto: string; pQuantidade: Double; pParticipacao: Double; pCarregandoProduto: Boolean);
  public
    { Public declarations }
  end;

  procedure Informar(pProdutoId: Integer);

implementation

{$R *.dfm}

const
  coKitProdutoId    = 0;
  coNomeProdutoKit  = 1;
  coQuantidadeKit   = 2;
  coParticipacaoKit = 3;

procedure Informar(pProdutoId: Integer);
var
  i: Integer;
  vProduto: TArray<RecProdutos>;
  vProdutosKit: TArray<RecProdutosKit>;
  vForm: TFormInformacoesProduto;
begin
  if pProdutoId = 0 then
    Exit;

  vProduto := _Produtos.BuscarProdutos(Sessao.getConexaoBanco, 0, [pProdutoId], False, '');

  if vProduto = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vForm := TFormInformacoesProduto.Create(Application);
  vForm.ReadOnlyTodosObjetos(True);

  vForm.eID.AsInt            := vProduto[0].produto_id;
  vForm.eNomeProduto.Text    := vProduto[0].nome;
  vForm.eDataCadastro.AsData := vProduto[0].data_cadastro;
  vForm.ckAtivo.Checked      := (vProduto[0].ativo = 'S');

  // Principais
  vForm.FrFabricante.InserirDadoPorChave(vProduto[0].fornecedor_id, False);
  vForm.FrMarca.InserirDadoPorChave(vProduto[0].marca_id, False);
  vForm.FrUnidadeVenda.InserirDadoPorChave(vProduto[0].unidade_venda);
  vForm.eMultiploVenda.AsDouble := vProduto[0].multiplo_venda;
  vForm.ePeso.AsDouble := vProduto[0].peso;
  vForm.eVolume.AsDouble := vProduto[0].volume;
  vForm.FrLinhaProduto.InserirDadoPorChave(vProduto[0].LinhaProdutoId);
  vForm.eCodigoBarras.Text := vProduto[0].codigo_barras;
  vForm.eCodigoBalanca.Text := vProduto[0].codigoBalanca;
  vForm.eValorAdicionalFrete.AsDouble := vProduto[0].ValorAdicionalFrete;
  vForm.ckAceitarEstoqueNegativo.Checked := (vProduto[0].aceitar_estoque_negativo = 'S');
  vForm.ckInativarProdutoZerarEstoque.Checked := (vProduto[0].inativar_zerar_estoque = 'S');
  vForm.ckBloquearParaCompras.Checked := (vProduto[0].bloqueado_compras = 'S');
  vForm.ckBloquearParaVendas.Checked := (vProduto[0].bloqueado_vendas = 'S');
  vForm.ckSujeitoComissao.Checked := (vProduto[0].SujeitoComissao = 'S');
  vForm.ePercentualComissaoVista.AsDouble := vProduto[0].PercentualComissaoVista;
  vForm.ePercentualComissaoPrazo.AsDouble := vProduto[0].PercentualComissaoPrazo;
  vForm.ckPermitirDevolucao.Checked := (vProduto[0].permitir_devolucao = 'S');
  vForm.ckProdutoDiversoPDV.Checked := (vProduto[0].produtoDiversosPDV = 'S');
  vForm.ckSobEncomenda.Checked := (vProduto[0].SobEncomenda = 'S');
  vForm.ckBloquearEntradaSemPedCompra.Checked := (vProduto[0].BloquearEntradSemPedCompra = 'S');
  vForm.ckExigirSeparacao.Checked := (vProduto[0].ExigirSeparacao = 'S');
  vForm.ckExigirModeloNota.Checked := (vProduto[0].NaoExigirModeloNotaFiscal = 'S');
  vForm.ckSepararSomenteCodigoBarras.CheckedStr := vProduto[0].ConfSomenteCodigoBarras;

  vForm.FrFoto1.setFoto(vProduto[0].foto_1);
  vForm.FrFoto2.setFoto(vProduto[0].foto_2);
  vForm.FrFoto3.setFoto(vProduto[0].foto_3);

  // Complementos
  vForm.eCaracteristicas.Text := vProduto[0].caracteristicas;
  vForm.ckRevenda.Checked := (vProduto[0].Revenda = 'S');
  vForm.ckUsoConsumo.Checked := (vProduto[0].UsoConsumo = 'S');
  vForm.ckAtivoImobilizado.Checked := (vProduto[0].AtivoImobilizado = 'S');
  vForm.ckServico.Checked := (vProduto[0].Servico = 'S');

  // Parametros de compra
  vForm.eNomeCompra.Text               := vProduto[0].nome_compra;
  vForm.eCodigoOriginalFabricante.Text := vProduto[0].CodigoOriginalFabricante;
  vForm.FrMultiplosCompra.Multiplos := _ProdutosMultiplosCompras.BuscarProdutoMultiploCompras(Sessao.getConexaoBanco, 0, [vProduto[0].produto_id]);
  vForm.FrCodigoOriginalFornecedores.Codigos := _ProdutosFornCodOriginais.BuscarCodigosOriginais(Sessao.getConexaoBanco, 0, [vProduto[0].produto_id]);

  // Controle de estoque
  vForm.cbTipoControleEstoque.SetIndicePorValor(vProduto[0].TipoControleEstoque);
  vForm.ckExigirDataFabricacaoLote.Checked := vProduto[0].ExigirDataFabricacaoLote = 'S';
  vForm.ckExigirDataVencimentoLote.Checked := vProduto[0].ExigirDataVencimentoLote = 'S';
  vForm.cbDefinicaoAutomaticaLote.SetIndicePorValor( vProduto[0].TipoDefAutomaticaLote );
  vForm.eDiasAvisoVencimento.AsInt := vProduto[0].DiasAvisoVencimento;
  vForm.eMotivoInatividade.Text    := vProduto[0].MotivoInatividade;
  vForm.FrUnidadeEntrega.InserirDadoPorChave( vProduto[0].UnidadeEntregaId, False );

  if Em(vForm.cbTipoControleEstoque.GetValor, ['K', 'A']) then begin
    vProdutosKit := _ProdutosKit.BuscarProdutosKit(Sessao.getConexaoBanco, 0, [vForm.eID.AsInt]);
    for i := Low(vProdutosKit) to High(vProdutosKit) do begin
      vForm.AddProdutoGridKit(
        vProdutosKit[i].ProdutoId,
        vProdutosKit[i].NomeProduto,
        vProdutosKit[i].Quantidade,
        vProdutosKit[i].PercParticipacao,
        True
      );
    end;
  end;

  // Fiscal
  vForm.eNCM.Text := vProduto[0].codigo_ncm;
  vForm.eCEST.Text := vProduto[0].cest;

  vForm.FrProdutosGruposTributacoes.ProdutosGruposTribEmpresas := _ProdutosGruposTribEmpresas.BuscarProdutosGruposTribEmpresas(Sessao.getConexaoBanco, 0, [vProduto[0].produto_id]);

  vForm.ShowModal;
  FreeAndNil(vForm);
  vProduto[0].Free;
end;

procedure TFormInformacoesProduto.AddProdutoGridKit(pProdutoId: Integer; pNomeProduto: string; pQuantidade: Double; pParticipacao: Double; pCarregandoProduto: Boolean);
var
  vLinha: Integer;
begin
  if sgProdutosKit.Cells[coKitProdutoId, 1] = '' then
    vLinha := 1
  else
    vLinha := sgProdutosKit.RowCount;

  sgProdutosKit.Cells[coKitProdutoId, vLinha]    := NFormat(pProdutoId);
  sgProdutosKit.Cells[coNomeProdutoKit, vLinha]  := pNomeProduto;
  sgProdutosKit.Cells[coQuantidadeKit, vLinha]   := NFormat(pQuantidade, 4);
  sgProdutosKit.Cells[coParticipacaoKit, vLinha] := NFormat(pParticipacao, 4);

  sgProdutosKit.RowCount := IfThen(vLinha > 1, vLinha + 1, 2);
end;

procedure TFormInformacoesProduto.sgProdutosKitDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[coKitProdutoId, coQuantidadeKit, coParticipacaoKit] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgProdutosKit.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormInformacoesProduto.sgProdutosKitGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = coParticipacaoKit then
    AFont.Color := _Biblioteca.corEdicaoGrid;
end;

end.


