inherited FormFechamentoCaixa: TFormFechamentoCaixa
  Caption = 'Fechamento de caixa'
  ClientHeight = 221
  ClientWidth = 538
  OnShow = FormShow
  ExplicitWidth = 544
  ExplicitHeight = 250
  PixelsPerInch = 96
  TextHeight = 14
  object lb17: TLabel [0]
    Left = 453
    Top = 10
    Width = 76
    Height = 14
    Caption = 'Data abertura'
  end
  object lb11: TLabel [1]
    Left = 131
    Top = 133
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Dinheiro'
  end
  object lb12: TLabel [2]
    Left = 131
    Top = 156
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Cheque'
  end
  object lb13: TLabel [3]
    Left = 131
    Top = 179
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Cart'#227'o'
  end
  object lb14: TLabel [4]
    Left = 131
    Top = 202
    Width = 60
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Cobran'#231'a'
  end
  object sbBuscarDadosCheques: TSpeedButton [5]
    Left = 292
    Top = 152
    Width = 18
    Height = 18
    Caption = '...'
    OnClick = sbBuscarDadosChequesClick
  end
  object sbBuscarDadosCartoes: TSpeedButton [6]
    Left = 292
    Top = 174
    Width = 18
    Height = 18
    Caption = '...'
    OnClick = sbBuscarDadosCartoesClick
  end
  object sbBuscarDadosCobranca: TSpeedButton [7]
    Left = 292
    Top = 198
    Width = 18
    Height = 18
    Caption = '...'
    OnClick = sbBuscarDadosCobrancaClick
  end
  object lb1: TLabel [8]
    Left = 319
    Top = 108
    Width = 69
    Height = 14
    Caption = 'Observa'#231#245'es'
  end
  inherited pnOpcoes: TPanel
    Height = 221
    TabOrder = 8
    ExplicitHeight = 221
    inherited sbDesfazer: TSpeedButton
      Top = 50
      ExplicitTop = 50
    end
    inherited sbExcluir: TSpeedButton
      Left = -112
      Visible = False
      ExplicitLeft = -112
    end
    inherited sbPesquisar: TSpeedButton
      Top = 138
      Visible = False
      ExplicitTop = 138
    end
  end
  inline FrCaixaAberto: TFrFuncionarios
    Left = 130
    Top = 9
    Width = 316
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 0
    TabStop = True
    ExplicitLeft = 130
    ExplicitTop = 9
    ExplicitWidth = 316
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 291
      Height = 23
      TabOrder = 1
      ExplicitWidth = 291
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 2
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 4
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 5
    end
    inherited CkMultiSelecao: TCheckBox
      TabOrder = 3
    end
    inherited PnTitulos: TPanel
      Width = 316
      TabOrder = 0
      ExplicitWidth = 316
      inherited lbNomePesquisa: TLabel
        Width = 29
        Caption = 'Caixa'
        ExplicitWidth = 29
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 211
        ExplicitLeft = 211
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 291
      Height = 24
      ExplicitLeft = 291
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited ckCaixa: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object eDataAbertura: TEditLukaData
    Left = 453
    Top = 25
    Width = 80
    Height = 22
    Hint = 
      'Pressione "A" para definir o dia atual, "I" para o primeiro dia ' +
      'do m'#234's atual e "F" para o '#250'ltimo dia do m'#234's atual.'
    TabStop = False
    Enabled = False
    EditMask = '99/99/9999;1; '
    MaxLength = 10
    ReadOnly = True
    TabOrder = 1
    Text = '  /  /    '
  end
  object eValorDinheiro: TEditLuka
    Left = 193
    Top = 126
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 2
    Text = '0,00'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorCheque: TEditLuka
    Left = 193
    Top = 149
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 3
    Text = '0,00'
    OnKeyDown = eValorChequeKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorCartao: TEditLuka
    Left = 193
    Top = 172
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 4
    Text = '0,00'
    OnKeyDown = eValorCartaoKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object eValorCobranca: TEditLuka
    Left = 193
    Top = 195
    Width = 97
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    ReadOnly = True
    TabOrder = 5
    Text = '0,00'
    OnKeyDown = eValorCobrancaKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 2
    NaoAceitarEspaco = False
  end
  object stSPC: TStaticText
    Left = 128
    Top = 104
    Width = 182
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = 'Valores de fechamento'
    Color = 15790320
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 7
    Transparent = False
  end
  object eObservacoes: TMemo
    Left = 319
    Top = 123
    Width = 214
    Height = 93
    TabOrder = 6
  end
  inline FrContaDestino: TFrContas
    Left = 130
    Top = 55
    Width = 316
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 9
    TabStop = True
    ExplicitLeft = 130
    ExplicitTop = 55
    ExplicitWidth = 316
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 291
      Height = 23
      ExplicitWidth = 291
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 316
      ExplicitWidth = 316
      inherited lbNomePesquisa: TLabel
        Width = 75
        Caption = 'Conta destino'
        ExplicitWidth = 75
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 211
        ExplicitLeft = 211
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 291
      Height = 24
      ExplicitLeft = 291
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
    inherited rgTipo: TRadioGroupLuka
      ItemIndex = 0
    end
  end
end
