unit BuscarDadosUsuarioOcorrencia;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, Frame.Cadastros,
  _RecordsCadastros, _Biblioteca, Vcl.StdCtrls, FrameClientes, FrameFuncionarios;

type
  TFormBuscarDadosUsuarioOcorrencia = class(TFormHerancaFinalizar)
    FrFuncionarios: TFrFuncionarios;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosUsuario(caption: string = ''): TRetornoTelaFinalizar<RecFuncionarios>;

implementation

{$R *.dfm}

function BuscarDadosUsuario(caption: string = ''): TRetornoTelaFinalizar<RecFuncionarios>;
var
  vForm: TFormBuscarDadosUsuarioOcorrencia;
begin
  vForm := TFormBuscarDadosUsuarioOcorrencia.Create(Application);

  if caption <> '' then
    VForm.Caption := caption;

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.FrFuncionarios.GetFuncionario(0);

  FreeAndNil(vForm);
end;

end.
