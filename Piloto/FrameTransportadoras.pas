unit FrameTransportadoras;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.Buttons, _Transportadoras,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, _Biblioteca, PesquisaTransportadoras,
  Vcl.Menus;

type
  TFrTransportadora = class(TFrameHenrancaPesquisas)
  public
    function GetDado(pLinha: Integer = -1): RecTransportadoras;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrTransportadora }

function TFrTransportadora.AdicionarDireto: TObject;
var
  vDados: TArray<RecTransportadoras>;
begin
  vDados := _Transportadoras.BuscarTransportadoras(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked);
  if vDados = nil then
    Result := nil
  else
    Result := vDados[0];
end;

function TFrTransportadora.AdicionarPesquisando: TObject;
begin
  Result := PesquisaTransportadoras.Pesquisar;
end;

function TFrTransportadora.GetDado(pLinha: Integer): RecTransportadoras;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecTransportadoras(FDados[IIfInt(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrTransportadora.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecTransportadoras(FDados[i]).CadastroId = RecTransportadoras(pSender).CadastroId then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrTransportadora.MontarGrid;
var
  i: Integer;
  pSender: RecTransportadoras;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecTransportadoras(FDados[i]);
      AAdd([IntToStr(pSender.CadastroId), pSender.cadastro.nome_fantasia]);
    end;
  end;
end;

end.
