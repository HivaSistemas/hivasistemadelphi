 unit ImpressaoComprovanteEntregaGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos,
  QRCtrls, QRExport, QRPDFFilt, QRWebFilt, QuickRpt, Vcl.ExtCtrls, _Entregas,
  _BibliotecaGenerica, _EntregasItens, _RecordsExpedicao, _Sessao, _Biblioteca,
  _Retiradas, _RetiradasItens, QRBarcodeComum, _Orcamentos, _CondicoesPagamento,
  System.Math, _RecordsEspeciais, Vcl.Imaging.jpeg;

type
  TTipoMovimento = (tpEntrega, tpRetirada);

  TFormImpressaoComprovanteEntregaGrafico = class(TFormHerancaRelatoriosGraficos)
    qrl2: TQRLabel;
    qrlNFTitQuantidadeTotalItens: TQRLabel;
    qrlNFQuantidadeTotalItens: TQRLabel;
    qrlNFVendedor: TQRLabel;
    qrlNFCliente: TQRLabel;
    qrlNFPedido: TQRLabel;
    qrlNFCodigoMovimento: TQRLabel;
    qrlNFBarCodePedido: TQRBarcode;
    qrl5: TQRLabel;
    QRShape2: TQRShape;
    qrlNFEnderecoEntrega: TQRMemo;
    qrlNFTelefone: TQRLabel;
    qrlNFRecebimentoEntrega: TQRLabel;
    qrlNFCabecalhoCondPagamentoEntrega: TQRLabel;
    qrlNFCondicaoPagamentoEntrega: TQRLabel;
    qrlNFObservacoes: TQRLabel;
    qrNfLocal: TQRLabel;
    qrlNFCabecalhoValorReceberEntrega: TQRLabel;
    qrlNFValorReceberEntrega: TQRLabel;
    QRBand3: TQRBand;
    qrPedidoA4: TQRLabel;
    QRLabel19: TQRLabel;
    qrClienteA4: TQRLabel;
    qrTelefoneA4: TQRLabel;
    QRLabel23: TQRLabel;
    qrVendedorA4: TQRLabel;
    qrEntregaA4: TQRLabel;
    qrEnderecoEntregaA4: TQRMemo;
    QRBand4: TQRBand;
    QRLabel28: TQRLabel;
    qrNomeProdutoA4: TQRLabel;
    QRLabel33: TQRLabel;
    qrMarcaA4: TQRLabel;
    QRLabel36: TQRLabel;
    qrQuantidadeA4: TQRLabel;
    qrUnidadeA4: TQRLabel;
    QRLabel40: TQRLabel;
    qrLoteA4: TQRLabel;
    QRLabel42: TQRLabel;
    qrCodigoBarrasA4: TQRLabel;
    QRLabel44: TQRLabel;
    qrCodigoOriginalA4: TQRLabel;
    qrLocalA4: TQRLabel;
    QRLabel34: TQRLabel;
    qrQuantidadeTotalItensA4: TQRLabel;
    qrRecebimentoEntregaA4: TQRLabel;
    qrCabecalhoRecebimentoEntregaA4: TQRLabel;
    qrCondicaoPagamentoEntregaA4: TQRLabel;
    qrCabecalhoValorReceberEntregaA4: TQRLabel;
    qrValorReceberEntregaA4: TQRLabel;
    QRLabel15: TQRLabel;
    qrlNfunidadeA4: TQRLabel;
    QuickRep2: TQuickRep;
    QRBand5: TQRBand;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    qr2: TQRLabel;
    qrNomeCliente: TQRLabel;
    qr6: TQRLabel;
    qrCpfCnpjCliente: TQRLabel;
    qrCaptionVendedor: TQRLabel;
    qrVendedor: TQRLabel;
    qr14: TQRLabel;
    qrEnderecoCliente: TQRLabel;
    qr30: TQRLabel;
    qr29: TQRLabel;
    qr21: TQRLabel;
    qrCabecalhoMovimento: TQRLabel;
    qr19: TQRLabel;
    qr18: TQRLabel;
    qr17: TQRLabel;
    QRLabel54: TQRLabel;
    QRBand6: TQRBand;
    QRBand7: TQRBand;
    QRShape8: TQRShape;
    QRLabel59: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel70: TQRLabel;
    QRBand8: TQRBand;
    QRLabel62: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel31: TQRLabel;
    qrObservacoesA4: TQRLabel;
    qrRelatorioMatricial: TQuickRep;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    qrEmpresaMatricial: TQRLabel;
    qrEnderecoMatricial: TQRLabel;
    qrLogoEmpresaMatricial: TQRImage;
    QRLabel4: TQRLabel;
    qrEmitidoEmMatricial: TQRLabel;
    QRLabel6: TQRLabel;
    qrBairroMatricial: TQRLabel;
    QRLabel8: TQRLabel;
    qrCidadeUFMatricial: TQRLabel;
    QRLabel10: TQRLabel;
    qrCNPJMatricial: TQRLabel;
    QRLabel12: TQRLabel;
    qrTelefoneMatricialCabecalho: TQRLabel;
    qrFaxMatricial: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    qrEmailMatricial: TQRLabel;
    qrDescricaoMatricial: TQRLabel;
    QRBand2: TQRBand;
    qrLocalMatricial: TQRLabel;
    QRLabel29: TQRLabel;
    qrQuantidadeTotalItensMatricial: TQRLabel;
    qrRecebimentoEntregaMatricial: TQRLabel;
    qrCabecalhoRecebimentoEntregaMatricial: TQRLabel;
    qrCondicaoPagamentoEntregaMatricial: TQRLabel;
    qrCabecalhoValorReceberEntregaMatricial: TQRLabel;
    qrValorReceberEntregaMatricial: TQRLabel;
    qrObservacoesMatricial: TQRLabel;
    QRBand9: TQRBand;
    QRShape3: TQRShape;
    qrPedidoMatricial: TQRLabel;
    QRLabel30: TQRLabel;
    qrClienteMatricial: TQRLabel;
    qrTelefoneMatricial: TQRLabel;
    QRLabel32: TQRLabel;
    qrVendedorMatricial: TQRLabel;
    qrEntregaMatricial: TQRLabel;
    qrEnderecoEntregaMatricial: TQRMemo;
    QRBand10: TQRBand;
    QRShape4: TQRShape;
    QRLabel35: TQRLabel;
    qrNomeProdutoMatricial: TQRLabel;
    QRLabel37: TQRLabel;
    qrMarcaMatricial: TQRLabel;
    QRLabel49: TQRLabel;
    qrQuantidadeMatricial: TQRLabel;
    qrUnidadeMatricial: TQRLabel;
    QRLabel50: TQRLabel;
    qrLoteMatricial: TQRLabel;
    QRLabel55: TQRLabel;
    qrCodigoBarrasMatricial: TQRLabel;
    QRLabel56: TQRLabel;
    qrCodigoOriginalMatricial: TQRLabel;
    QRLabel57: TQRLabel;
    qrlNfunidadeMatricial: TQRLabel;
    qrEnderecoEstoqueA4: TQRLabel;
    QRLabel3: TQRLabel;
    QRShape5: TQRShape;
    QRShape7: TQRShape;
    QRShape6: TQRShape;
    qrLabelEnderecoEstoqueMatricial: TQRLabel;
    qrEnderecoEstoqueMatricial: TQRLabel;
    QRShape9: TQRShape;
    qrbndNFDetail: TQRBand;
    qrlNFNomeProduto: TQRLabel;
    qrlNFCabUnidade: TQRLabel;
    qrlNFLote: TQRLabel;
    qrlNFQuantidade: TQRLabel;
    qrlCabProdutoId: TQRLabel;
    qrl1: TQRLabel;
    qrlNFMarca: TQRLabel;
    qrl4: TQRLabel;
    qrlNFUnidade: TQRLabel;
    qrl3: TQRLabel;
    qrlNFCodigoBarras: TQRLabel;
    qrl7: TQRLabel;
    qrlNFCodigoOriginal: TQRLabel;
    QRShape1: TQRShape;
    QRLabel5: TQRLabel;
    qrEnderecoEstoqueNF: TQRLabel;
    qrlNFApelido: TQRLabel;
    QRLabel7: TQRLabel;
    qrApelidoMatricial: TQRLabel;
    QRLabel9: TQRLabel;
    qrApelidoA4: TQRLabel;
    QRShape10: TQRShape;
    qrDataHoraRetornoLabelA4: TQRLabel;
    qrLinhaAssinaturaMotoristaA4: TQRShape;
    qrLinhaAssinaturaConferenteA4: TQRShape;
    qrAssinaturaMotoristaA4: TQRLabel;
    qrAssinaturaConferenteA4: TQRLabel;
    qrDataHoraSaidaLabelA4: TQRLabel;
    qrLinhaAssinaturaClienteA4: TQRShape;
    qrAssinaturaClienteA4: TQRLabel;
    qrDataHoraSaidaA4: TQRLabel;
    qrDataHoraRetornoA4: TQRLabel;
    QRLabel2: TQRLabel;
    qrPesoA4: TQRLabel;
    QRLabel11: TQRLabel;
    qrPesoMatricial: TQRLabel;
    procedure qrbndNFDetailBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QuickRep1NeedData(Sender: TObject; var MoreData: Boolean);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure qrRelatorioA4BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrRelatorioA4NeedData(Sender: TObject; var MoreData: Boolean);
    procedure qrRelatorioMatricialBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure qrRelatorioMatricialNeedData(Sender: TObject;
      var MoreData: Boolean);
    procedure QRBand10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand1AfterPrint(Sender: TQRCustomBand; BandPrinted: Boolean);
    procedure qrRelatorioMatricialAfterPrint(Sender: TObject);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    FImpressao: Integer;
    FPosicao: Integer;
    FFirstPage: Boolean;
    FTipoMovimento: TTipoMovimento;
    FItensEnt: TArray<RecEntregaItem>;
    FItensRet: TArray<RecRetiradaItem>;
  public
    { Public declarations }
  end;

procedure Imprimir(pId: Integer; pTipoMovimento: TTipoMovimento; pTelaOrcamento: Boolean = False);
procedure InicializacoesImpressaoMatricial(vForm: TFormImpressaoComprovanteEntregaGrafico);

implementation

{$R *.dfm}

uses _RecordsCadastros, _Produtos;

procedure Imprimir(pId: Integer; pTipoMovimento: TTipoMovimento; pTelaOrcamento: Boolean = False);
type
  RecInformacoesComprovante = record
    Id: Integer;
    EmpresaId: Integer;
    LocalId: Integer;
    NomeLocal: string;
    OrcamentoId: Integer;
    ClienteId: Integer;
    NomeCliente: string;
    Apelido: string;
    VendedorId: Integer;
    NomeVendedor: string;
    EnderecoEntrega: string;
    TelefonePrincipal: string;
    TelefoneCelular: string;
    QuantidadeProdutos: Integer;
    ValorReceberEntrega: Double;
    CondicaoId: Integer;
    NomeCondicao: string;
    ReceberNaEntrega: string;
    ObservacoesExpedicao: string;
  end;
var
  vRetBanco: RecRetornoBD;
  vImpressora: RecImpressora;
  vEntregas: TArray<RecEntrega>;
  vRetiradas: TArray<RecRetirada>;
  vForm: TFormImpressaoComprovanteEntregaGrafico;
  vInformacoes: RecInformacoesComprovante;
  vQtdeViasJaImpressasCompEntrega: Integer;
  vCondicaoRecebimentoNaEntrega: Boolean;

  procedure DescerComponentes(pControls: TArray<TControl>; pValorDescer: Integer);
  var
    i: Integer;
  begin
    for i := Low(pControls) to High(pControls) do
      pControls[i].Top := pControls[i].Top + pValorDescer;
  end;

begin

  if pTipoMovimento = tpEntrega then
    vQtdeViasJaImpressasCompEntrega := _Entregas.getQtdeViasImpressasComprovanteEntrega(Sessao.getConexaoBanco, pId)
  else
    vQtdeViasJaImpressasCompEntrega := _Retiradas.getQtdeViasImpressasComprovanteRetirada(Sessao.getConexaoBanco, pId);

  if (vQtdeViasJaImpressasCompEntrega > 0) and (not Sessao.AutorizadoRotina('reimprimir_comprovante_entrega')) then begin
    _Biblioteca.Exclamar('Voc� n�o possui autoriza��o para reimprimir o comprovante de entrega!');
    Exit;
  end;

  vImpressora := Sessao.getImpressora(toComprovanteEntrega);
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;

  if pTipoMovimento = tpEntrega then
    vRetBanco := _Entregas.IncrementarQtdeViasCompEntregaImpressos(Sessao.getConexaoBanco, pId, Sessao.getParametros.UtilizaControleManifesto = 'S')
  else
    vRetBanco := _Retiradas.IncrementarQtdeViasCompRetiradaImpressos(Sessao.getConexaoBanco, pId);

  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  if pTipoMovimento = tpEntrega then begin
    vEntregas := _Entregas.BuscarEntregas(Sessao.getConexaoBanco, 1, [pId]);
    if vEntregas = nil then begin
      _Biblioteca.Exclamar('Entrega n�o encontrada! Entrega: ' + _Biblioteca.NFormat(pId));
      Exit;
    end;
    vInformacoes.Id                   := pId;
    vInformacoes.EmpresaId            := vEntregas[0].EmpresaId;
    vInformacoes.LocalId              := vEntregas[0].LocalId;
    vInformacoes.NomeLocal            := vEntregas[0].NomeLocal;
    vInformacoes.OrcamentoId          := vEntregas[0].OrcamentoId;
    vInformacoes.ClienteId            := vEntregas[0].ClienteId;
    vInformacoes.NomeCliente          := vEntregas[0].NomeCliente;
    vInformacoes.Apelido              := vEntregas[0].Apelido;
    vInformacoes.VendedorId           := vEntregas[0].VendedorId;
    vInformacoes.NomeVendedor         := vEntregas[0].NomeVendedor;
    vInformacoes.TelefonePrincipal    := vEntregas[0].TelefonePrincipal;
    vInformacoes.TelefoneCelular      := vEntregas[0].TelefoneCelular;
    vInformacoes.ValorReceberEntrega  := vEntregas[0].ValorReceberEntrega;
    vInformacoes.CondicaoId           := vEntregas[0].CondicaoId;
    vInformacoes.NomeCondicao         := vEntregas[0].NomeCondicaoPagamento;
    vInformacoes.ReceberNaEntrega     := vEntregas[0].ReceberNaEntrega;
    vInformacoes.ObservacoesExpedicao := vEntregas[0].ObservacoesExpedicao;

    vInformacoes.EnderecoEntrega := 'End: ' + vEntregas[0].Logradouro + ' ' + vEntregas[0].Complemento + ' nr' + vEntregas[0].Numero + ' ' + vEntregas[0].NomeBairro + ', ' + vEntregas[0].Cidade + '-' + vEntregas[0].UF + IIfStr(vEntregas[0].PontoReferencia <> '', '. Ponto ref. ' + vEntregas[0].PontoReferencia);
  end
  else begin
    if pTelaOrcamento then
      vRetiradas := _Retiradas.BuscarRetiradas(Sessao.getConexaoBanco, 1, [pId])
    else
      vRetiradas := _Retiradas.BuscarRetiradas(Sessao.getConexaoBanco, 0, [pId]);

    if vRetiradas = nil then begin
      _Biblioteca.Exclamar('Retirada n�o encontrada! Retirada: ' + _Biblioteca.NFormat(pId));
      Exit;
    end;

    vInformacoes.Id                  := pId;
    vInformacoes.EmpresaId           := vRetiradas[0].EmpresaId;
    vInformacoes.LocalId             := vRetiradas[0].LocalId;
    vInformacoes.NomeLocal           := vRetiradas[0].NomeLocal;
    vInformacoes.OrcamentoId         := vRetiradas[0].OrcamentoId;
    vInformacoes.ClienteId           := vRetiradas[0].ClienteId;
    vInformacoes.NomeCliente         := vRetiradas[0].NomeCliente;
    vInformacoes.Apelido             := vRetiradas[0].Apelido;
    vInformacoes.VendedorId          := vRetiradas[0].VendedorId;
    vInformacoes.NomeVendedor        := vRetiradas[0].NomeVendedor;
    vInformacoes.TelefonePrincipal   := vRetiradas[0].TelefonePrincipal;
    vInformacoes.TelefoneCelular     := vRetiradas[0].TelefoneCelular;
    vInformacoes.CondicaoId          := vRetiradas[0].CondicaoId;
    vInformacoes.NomeCondicao        := vRetiradas[0].NomeCondicaoPagamento;
    vInformacoes.ValorReceberEntrega := 0;

    vInformacoes.EnderecoEntrega := '';
  end;

  vForm := TFormImpressaoComprovanteEntregaGrafico.Create(nil, vImpressora);
  vForm.PreencherCabecalho(vInformacoes.EmpresaId, False);
  vForm.qrNFCidadeUf.Caption         := '*********************';
  vForm.qrlNFTelefoneEmpresa.Caption := '*********************';

  vForm.qrlNFSistema.Caption := _Sessao.Sessao.getVersaoSistema;

  if vForm.ImpressaoGrafica then begin
    if pTipoMovimento = tpEntrega then begin
      vForm.qr13.Caption := 'COMPROVANTE DE ENTREGA';
      vForm.FItensEnt := _EntregasItens.BuscarEntregaItens(Sessao.getConexaoBanco, 0, [pId]);
      vInformacoes.QuantidadeProdutos := Length(vForm.FItensEnt);

      vForm.qrLinhaAssinaturaConferenteA4.Enabled := Sessao.getParametros.UtilizaControleManifesto = 'N';
      vForm.qrAssinaturaConferenteA4.Enabled := Sessao.getParametros.UtilizaControleManifesto = 'N';
      vForm.qrLinhaAssinaturaMotoristaA4.Enabled := Sessao.getParametros.UtilizaControleManifesto = 'N';
      vForm.qrAssinaturaMotoristaA4.Enabled := Sessao.getParametros.UtilizaControleManifesto = 'N';
      vForm.qrLinhaAssinaturaClienteA4.Enabled := Sessao.getParametros.UtilizaControleManifesto = 'N';
      vForm.qrAssinaturaClienteA4.Enabled := Sessao.getParametros.UtilizaControleManifesto = 'N';
      vForm.qrDataHoraSaidaLabelA4.Enabled := Sessao.getParametros.UtilizaControleManifesto = 'N';
      vForm.qrDataHoraSaidaA4.Enabled := Sessao.getParametros.UtilizaControleManifesto = 'N';
      vForm.qrDataHoraRetornoLabelA4.Enabled := Sessao.getParametros.UtilizaControleManifesto = 'N';
      vForm.qrDataHoraRetornoA4.Enabled := Sessao.getParametros.UtilizaControleManifesto = 'N';
    end
    else begin
      vForm.qr13.Caption := 'COMPROVANTE DE RETIRADA';

      if pTelaOrcamento then
        vForm.FItensRet := _RetiradasItens.BuscarRetiradaItens(Sessao.getConexaoBanco, 0, [vRetiradas[0].RetiradaId])
      else
        vForm.FItensRet := _RetiradasItens.BuscarRetiradaItens(Sessao.getConexaoBanco, 0, [pId]);

      vInformacoes.QuantidadeProdutos := Length(vForm.FItensRet);
    end;

    vForm.qrLocalA4.Caption            := 'Local: ' + getInformacao(vInformacoes.LocalId, vInformacoes.NomeLocal);

    vForm.FTipoMovimento := pTipoMovimento;

    vForm.qrCondicaoPagamentoEntregaA4.Caption := getInformacao(vInformacoes.CondicaoId, vInformacoes.NomeCondicao);
    vForm.qrQuantidadeTotalItensA4.Caption := _Biblioteca.NFormat(vInformacoes.QuantidadeProdutos);
    vForm.qrEntregaA4.Caption              := IIfStr(pTipoMovimento = tpEntrega, 'Entrega.: ', 'Retirada.:') + _Biblioteca.NFormat(vInformacoes.Id);
    vForm.qrPedidoA4.Caption               := 'Pedido..: ' + _Biblioteca.NFormat(vInformacoes.OrcamentoId);
    vForm.qrClienteA4.Caption              := _Biblioteca.NFormat(vInformacoes.ClienteId) + ' - ' + vInformacoes.NomeCliente;
    vForm.qrApelidoA4.Caption              := vInformacoes.Apelido;
    vForm.qrVendedorA4.Caption             := _Biblioteca.NFormat(vInformacoes.VendedorId) + ' - ' + vInformacoes.NomeVendedor;
    vForm.qrTelefoneA4.Caption             := 'Telefone: ' + vInformacoes.TelefonePrincipal + ' Celular: ' + vInformacoes.TelefoneCelular;
    vForm.qrEmitidoEm.Caption              := 'Emitido em ' + DHFormat(Sessao.getDataHora);
    vForm.qrEnderecoEntregaA4.Lines.Text   := vInformacoes.EnderecoEntrega;

    vCondicaoRecebimentoNaEntrega :=
      (pTipoMovimento = tpEntrega) and
      (vInformacoes.ReceberNaEntrega = 'S') and
      (_Orcamentos.PedidoTemFinanceiroAbertoReceberNaEntrega(Sessao.getConexaoBanco, vInformacoes.OrcamentoId));

    if vCondicaoRecebimentoNaEntrega then begin
      vForm.qrRecebimentoEntregaA4.Enabled             := True;
      vForm.qrCabecalhoRecebimentoEntregaA4.Enabled  := True;
      vForm.qrCabecalhoValorReceberEntregaA4.Enabled   := True;
      vForm.qrValorReceberEntregaA4.Enabled            := True;

      vForm.qrValorReceberEntregaA4.Caption      := 'R$ ' + _Biblioteca.NFormat(vInformacoes.ValorReceberEntrega);
    end;

    if vInformacoes.ObservacoesExpedicao <> '' then begin
      vForm.qrObservacoesA4.Enabled := True;
      vForm.qrObservacoesA4.Caption := _Biblioteca.RemoveCaracteres('Observa��o entrega: ' + vInformacoes.ObservacoesExpedicao, [chr(13), chr(10)]);
    end;

    try
      vForm.Imprimir;
    finally
      vForm.Free;
    end;
  end
  else if vForm.ImpressaoMatricial then begin
    InicializacoesImpressaoMatricial(vForm);

    if pTipoMovimento = tpEntrega then begin
      vForm.qrDescricaoMatricial.Caption := 'COMPROVANTE DE ENTREGA';
      vForm.FItensEnt := _EntregasItens.BuscarEntregaItens(Sessao.getConexaoBanco, 0, [pId]);
      vInformacoes.QuantidadeProdutos := Length(vForm.FItensEnt);
    end
    else begin
      vForm.qrDescricaoMatricial.Caption := 'COMPROVANTE DE RETIRADA';

      if pTelaOrcamento then
        vForm.FItensRet := _RetiradasItens.BuscarRetiradaItens(Sessao.getConexaoBanco, 0, [vRetiradas[0].RetiradaId])
      else
        vForm.FItensRet := _RetiradasItens.BuscarRetiradaItens(Sessao.getConexaoBanco, 0, [pId]);

      vInformacoes.QuantidadeProdutos := Length(vForm.FItensRet);
    end;

    vForm.qrLocalMatricial.Caption            := 'Local: ' + getInformacao(vInformacoes.LocalId, vInformacoes.NomeLocal);

    vForm.FTipoMovimento := pTipoMovimento;

    vForm.qrCondicaoPagamentoEntregaMatricial.Caption := getInformacao(vInformacoes.CondicaoId, vInformacoes.NomeCondicao);
    vForm.qrQuantidadeTotalItensMatricial.Caption := _Biblioteca.NFormat(vInformacoes.QuantidadeProdutos);
    vForm.qrEntregaMatricial.Caption      := IIfStr(pTipoMovimento = tpEntrega, 'Entrega.: ', 'Retirada.:') + _Biblioteca.NFormat(vInformacoes.Id);
    vForm.qrEmitidoEmMatricial.Caption            := 'Emitido em ' + DHFormat(Sessao.getDataHora);
    vForm.qrPedidoMatricial.Caption               := 'Pedido..: ' + _Biblioteca.NFormat(vInformacoes.OrcamentoId);
    vForm.qrClienteMatricial.Caption              := _Biblioteca.NFormat(vInformacoes.ClienteId) + ' - ' + vInformacoes.NomeCliente;
    vForm.qrApelidoMatricial.Caption              := vInformacoes.Apelido;
    vForm.qrVendedorMatricial.Caption             := _Biblioteca.NFormat(vInformacoes.VendedorId) + ' - ' + vInformacoes.NomeVendedor;
    vForm.qrTelefoneMatricial.Caption             := 'Telefone: ' + vInformacoes.TelefonePrincipal + ' Celular: ' + vInformacoes.TelefoneCelular;
    vForm.qrEnderecoEntregaMatricial.Lines.Text   := vInformacoes.EnderecoEntrega;

    vCondicaoRecebimentoNaEntrega :=
      (pTipoMovimento = tpEntrega) and
      (vInformacoes.ReceberNaEntrega = 'S') and
      (_Orcamentos.PedidoTemFinanceiroAbertoReceberNaEntrega(Sessao.getConexaoBanco, vInformacoes.OrcamentoId));

    if vCondicaoRecebimentoNaEntrega then begin
      vForm.qrRecebimentoEntregaMatricial.Enabled             := True;
      vForm.qrCabecalhoRecebimentoEntregaMatricial.Enabled  := True;
      vForm.qrCabecalhoValorReceberEntregaMatricial.Enabled   := True;
      vForm.qrValorReceberEntregaMatricial.Enabled            := True;

      vForm.qrValorReceberEntregaMatricial.Caption      := 'R$ ' + _Biblioteca.NFormat(vInformacoes.ValorReceberEntrega);
    end;

    if vInformacoes.ObservacoesExpedicao <> '' then begin
      vForm.qrObservacoesMatricial.Enabled := True;
      vForm.qrObservacoesMatricial.Caption := _Biblioteca.RemoveCaracteres('Observa��o entrega: ' + vInformacoes.ObservacoesExpedicao, [chr(13), chr(10)]);
    end;

    try
      vForm.qrRelatorioMatricial.PrinterSettings.PrinterIndex := vImpressora.getIndex;
      vForm.qrRelatorioMatricial.Prepare;

      if vImpressora.AbrirPreview then
        vForm.qrRelatorioMatricial.PreviewModal
      else
        vForm.qrRelatorioMatricial.Print;
    finally
      vForm.Free;
    end;
  end
  else begin
    if pTipoMovimento = tpEntrega then begin
      vForm.qrNFTipoDocumento.Caption := 'COMPROVANTE DE ENTREGA';
      vForm.FItensEnt := _EntregasItens.BuscarEntregaItens(Sessao.getConexaoBanco, 0, [pId]);
      vInformacoes.QuantidadeProdutos := Length(vForm.FItensEnt);
    end
    else begin
      vForm.qrNFTipoDocumento.Caption := 'COMPROVANTE DE RETIRADA';

      if pTelaOrcamento then
        vForm.FItensRet := _RetiradasItens.BuscarRetiradaItens(Sessao.getConexaoBanco, 0, [vRetiradas[0].RetiradaId])
      else
        vForm.FItensRet := _RetiradasItens.BuscarRetiradaItens(Sessao.getConexaoBanco, 0, [pId]);

      vInformacoes.QuantidadeProdutos := Length(vForm.FItensRet);
    end;

    vForm.qrNfLocal.Caption            := 'Local: ' + getInformacao(vInformacoes.LocalId, vInformacoes.NomeLocal);

    vForm.FTipoMovimento := pTipoMovimento;

    vForm.qrlNFCondicaoPagamentoEntrega.Caption := getInformacao(vInformacoes.CondicaoId, vInformacoes.NomeCondicao);
    vForm.qrlNFQuantidadeTotalItens.Caption := _Biblioteca.NFormat(vInformacoes.QuantidadeProdutos);
    vForm.qrlNFCodigoMovimento.Caption      := IIfStr(pTipoMovimento = tpEntrega, 'Entrega.: ', 'Retirada.:') + _Biblioteca.NFormat(vInformacoes.Id);
    vForm.qrlNFPedido.Caption               := 'Pedido..: ' + _Biblioteca.NFormat(vInformacoes.OrcamentoId);
    vForm.qrlNFCliente.Caption              := 'Cliente.: ' + _Biblioteca.NFormat(vInformacoes.ClienteId) + ' - ' + vInformacoes.NomeCliente;
    vForm.qrlNFApelido.Caption              := 'Apelido.:  ' + vInformacoes.Apelido;
    vForm.qrlNFEmitidoEm.Caption            := 'Emitido em ' + DHFormat(Sessao.getDataHora);
    vForm.qrlNFVendedor.Caption             := 'Vendedor: ' + _Biblioteca.NFormat(vInformacoes.VendedorId) + ' - ' + vInformacoes.NomeVendedor;
    vForm.qrlNFTelefone.Caption             := 'Telefone: ' + vInformacoes.TelefonePrincipal + ' Celular: ' + vInformacoes.TelefoneCelular;
    vForm.qrlNFEnderecoEntrega.Lines.Text   := vInformacoes.EnderecoEntrega;
    vForm.qrlNFBarCodePedido.BarText        := _Biblioteca.LPad(IntToStr(pId), 12, '0');

    vCondicaoRecebimentoNaEntrega :=
      (pTipoMovimento = tpEntrega) and
      (vInformacoes.ReceberNaEntrega = 'S') and
      (_Orcamentos.PedidoTemFinanceiroAbertoReceberNaEntrega(Sessao.getConexaoBanco, vInformacoes.OrcamentoId));

    if vCondicaoRecebimentoNaEntrega then begin
      vForm.qrlNFRecebimentoEntrega.Enabled             := True;
      vForm.qrlNFCabecalhoCondPagamentoEntrega.Enabled  := True;
      vForm.qrlNFCabecalhoValorReceberEntrega.Enabled   := True;
      vForm.qrlNFValorReceberEntrega.Enabled            := True;

      vForm.qrlNFValorReceberEntrega.Caption      := 'R$ ' + _Biblioteca.NFormat(vInformacoes.ValorReceberEntrega);
    end;

    if vInformacoes.ObservacoesExpedicao <> '' then begin
      vForm.qrlNFObservacoes.Enabled := True;
      vForm.qrlNFObservacoes.Caption := _Biblioteca.RemoveCaracteres('Obs: ' + vInformacoes.ObservacoesExpedicao, [chr(13), chr(10)]);
      vForm.qrlNFObservacoes.Height  := Ceil(Length(vForm.qrlNFObservacoes.Caption) / 48) * 25;

      DescerComponentes([vForm.qrlNFRecebimentoEntrega, vForm.qrlNFCabecalhoCondPagamentoEntrega, vForm.qrlNFCondicaoPagamentoEntrega, vForm.qrlNFBarCodePedido, vForm.QRShape2, vForm.qrl5, vForm.qrlNFSistema], vForm.qrlNFObservacoes.Height + 10);

      vForm.PageFooterBand1.Height := vForm.PageFooterBand1.Height + vForm.qrlNFObservacoes.Height;
      vForm.PageFooterBand1.Repaint;
    end;

    if Sessao.getParametrosEmpresa.ImprimirLoteCompEntrega = 'N' then begin
      vForm.qrlNFCabUnidade.Enabled := False;
      vForm.qrlNFLote.Enabled := False;
    end;

    try
      vForm.qrRelatorioNF.Height := vForm.qrBandTitulo.Height + (vForm.qrbndNFDetail.Height * vInformacoes.QuantidadeProdutos) + vForm.PageFooterBand1.Height + 10;

      vForm.Imprimir;
    finally
      vForm.Free;
    end;

  end;

end;

procedure TFormImpressaoComprovanteEntregaGrafico.QRBand10BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  enderecosEstoque: TArray<RecEnderecosEstoque>;
  i: Integer;
  txtEndereco: string;
begin
  inherited;
  if FTipoMovimento = tpEntrega then begin
    qrNomeProdutoMatricial.Caption := _Biblioteca.NFormat(FItensEnt[FPosicao].ProdutoId) + ' - ' + FItensEnt[FPosicao].NomeProduto;
    qrMarcaMatricial.Caption := FItensEnt[FPosicao].NomeMarca;

    if FItensEnt[FPosicao].UnidadeEntregaId <> '' then begin
      qrQuantidadeMatricial.Caption := FloatToStr(FItensEnt[FPosicao].Quantidade / FItensEnt[FPosicao].MultiploVenda);
      qrUnidadeMatricial.Caption := FItensEnt[FPosicao].UnidadeEntregaId;
    end
    else begin
      qrQuantidadeMatricial.Caption := FloatToStr(FItensEnt[FPosicao].Quantidade);
      qrUnidadeMatricial.Caption := FItensEnt[FPosicao].Unidade;
    end;

    if FItensEnt[FPosicao].PesoTotal > 0 then
      qrPesoMatricial.Caption := _Biblioteca.NFormat(FItensEnt[FPosicao].PesoTotal, 2)
    else
      qrPesoMatricial.Caption := '';

    qrLoteMatricial.Caption := FItensEnt[FPosicao].Lote;
    qrCodigoOriginalMatricial.Caption := FItensEnt[FPosicao].CodigoBarras;
    qrCodigoOriginalMatricial.Caption := FItensEnt[FPosicao].CodigoOriginalFabricante;

    if Sessao.getParametros.UtilEnderecoEstoque = 'S' then
      enderecosEstoque := _Produtos.BuscarEnderecosEstoqueCompleto(Sessao.getConexaoBanco, FItensEnt[FPosicao].ProdutoId);
  end
  else begin
    qrNomeProdutoMatricial.Caption := _Biblioteca.NFormat(FItensRet[FPosicao].ProdutoId) + ' - ' + FItensRet[FPosicao].NomeProduto;
    if Length(qrNomeProdutoMatricial.Caption) > 38 then
      qrNomeProdutoMatricial.Font.Size := 8
    else
      qrNomeProdutoMatricial.Font.Size := 9;

    qrMarcaMatricial.Caption := FItensRet[FPosicao].NomeMarca;

    if FItensRet[FPosicao].UnidadeEntregaId <> '' then begin
      qrQuantidadeMatricial.Caption := FloatToStr(FItensRet[FPosicao].Quantidade / FItensRet[FPosicao].MultiploVenda);
      qrUnidadeMatricial.Caption := FItensRet[FPosicao].UnidadeEntregaId;
    end
    else begin
      qrQuantidadeMatricial.Caption := FloatToStr(FItensRet[FPosicao].Quantidade);
      qrUnidadeMatricial.Caption := FItensRet[FPosicao].Unidade;
    end;

    if FItensRet[FPosicao].PesoTotal > 0 then
      qrPesoMatricial.Caption := _Biblioteca.NFormat(FItensRet[FPosicao].PesoTotal, 2)
    else
      qrPesoMatricial.Caption := '';

    qrLoteMatricial.Caption := FItensRet[FPosicao].Lote;
    qrCodigoBarrasMatricial.Caption := FItensRet[FPosicao].CodigoBarras;
    qrCodigoOriginalMatricial.Caption := FItensRet[FPosicao].CodigoOriginalFabricante;

    if Sessao.getParametros.UtilEnderecoEstoque = 'S' then
      enderecosEstoque := _Produtos.BuscarEnderecosEstoqueCompleto(Sessao.getConexaoBanco, FItensRet[FPosicao].ProdutoId);
  end;

  if Sessao.getParametros.UtilEnderecoEstoque = 'S' then begin
    txtEndereco := '';
    for i := 0 to Length(enderecosEstoque) - 1 do begin
      if i > 0 then
        txtEndereco := txtEndereco + #10#13;

      txtEndereco :=
        txtEndereco +
        'Rua: ' + enderecosEstoque[i].nome_rua + ' ' +
        'M�dulo: ' + enderecosEstoque[i].nome_modulo + ' ' +
        'V�o: ' + enderecosEstoque[i].nome_vao + ' ' +
        'N�vel: ' + enderecosEstoque[i].nome_nivel;

    end;

    qrEnderecoEstoqueMatricial.Caption := txtEndereco;
  end
  else begin
    qrEnderecoEstoqueMatricial.Caption := '';
    qrLabelEnderecoEstoqueMatricial.Caption := '';
    QRBand10.Height := 68;
  end;
end;

procedure TFormImpressaoComprovanteEntregaGrafico.QRBand1AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  Inc(FImpressao);
  if FImpressao > 4 then
    QRBand1.Enabled := False;
end;

procedure TFormImpressaoComprovanteEntregaGrafico.QRBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FFirstPage;
  FFirstPage := False;
end;

procedure TFormImpressaoComprovanteEntregaGrafico.QRBand4BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  enderecosEstoque: TArray<RecEnderecosEstoque>;
  i: Integer;
  txtEndereco: string;
begin
  inherited;
  if FTipoMovimento = tpEntrega then begin
    qrNomeProdutoA4.Caption := _Biblioteca.NFormat(FItensEnt[FPosicao].ProdutoId) + ' - ' + FItensEnt[FPosicao].NomeProduto;
    if Length(qrNomeProdutoA4.Caption) > 38 then
      qrNomeProdutoA4.Font.Size := 8
    else
      qrNomeProdutoA4.Font.Size := 9;

    qrMarcaA4.Caption := FItensEnt[FPosicao].NomeMarca;

    if FItensEnt[FPosicao].UnidadeEntregaId <> '' then begin
      qrQuantidadeA4.Caption := FloatToStr(FItensEnt[FPosicao].Quantidade / FItensEnt[FPosicao].MultiploVenda);
      qrUnidadeA4.Caption := FItensEnt[FPosicao].UnidadeEntregaId;
    end
    else begin
      qrQuantidadeA4.Caption := FloatToStr(FItensEnt[FPosicao].Quantidade);
      qrUnidadeA4.Caption := FItensEnt[FPosicao].Unidade;
    end;

    if FItensEnt[FPosicao].PesoTotal > 0 then
      qrPesoA4.Caption := _Biblioteca.NFormat(FItensEnt[FPosicao].PesoTotal, 2)
    else
      qrPesoA4.Caption := '';

    qrLoteA4.Caption := FItensEnt[FPosicao].Lote;
    qrCodigoOriginalA4.Caption := FItensEnt[FPosicao].CodigoBarras;
    qrCodigoOriginalA4.Caption := FItensEnt[FPosicao].CodigoOriginalFabricante;

    if Sessao.getParametros.UtilEnderecoEstoque = 'S' then
      enderecosEstoque := _Produtos.BuscarEnderecosEstoqueCompleto(Sessao.getConexaoBanco, FItensEnt[FPosicao].ProdutoId);
  end
  else begin
    qrNomeProdutoA4.Caption := _Biblioteca.NFormat(FItensRet[FPosicao].ProdutoId) + ' - ' + FItensRet[FPosicao].NomeProduto;
    if Length(qrNomeProdutoA4.Caption) > 38 then
      qrNomeProdutoA4.Font.Size := 8
    else
      qrNomeProdutoA4.Font.Size := 9;

    qrMarcaA4.Caption := FItensRet[FPosicao].NomeMarca;

    if FItensRet[FPosicao].UnidadeEntregaId <> '' then begin
      qrQuantidadeA4.Caption := FloatToStr(FItensRet[FPosicao].Quantidade / FItensRet[FPosicao].MultiploVenda);
      qrUnidadeA4.Caption := FItensRet[FPosicao].UnidadeEntregaId;
    end
    else begin
      qrQuantidadeA4.Caption := FloatToStr(FItensRet[FPosicao].Quantidade);
      qrUnidadeA4.Caption := FItensRet[FPosicao].Unidade;
    end;

    if FItensRet[FPosicao].PesoTotal > 0 then
      qrPesoA4.Caption := _Biblioteca.NFormat(FItensRet[FPosicao].PesoTotal, 2)
    else
      qrPesoA4.Caption := '';

    qrLoteA4.Caption := FItensRet[FPosicao].Lote;
    qrCodigoBarrasA4.Caption := FItensRet[FPosicao].CodigoBarras;
    qrCodigoOriginalA4.Caption := FItensRet[FPosicao].CodigoOriginalFabricante;

    if Sessao.getParametros.UtilEnderecoEstoque = 'S' then
      enderecosEstoque := _Produtos.BuscarEnderecosEstoqueCompleto(Sessao.getConexaoBanco, FItensRet[FPosicao].ProdutoId);
  end;

  if Sessao.getParametros.UtilEnderecoEstoque = 'S' then begin
    txtEndereco := '';
    for i := 0 to Length(enderecosEstoque) - 1 do begin
      if i > 0 then
        txtEndereco := txtEndereco + #10#13;

      txtEndereco :=
        txtEndereco +
        'Rua: ' + enderecosEstoque[i].nome_rua + ' ' +
        'M�dulo: ' + enderecosEstoque[i].nome_modulo + ' ' +
        'V�o: ' + enderecosEstoque[i].nome_vao + ' ' +
        'N�vel: ' + enderecosEstoque[i].nome_nivel;
    end;

    qrEnderecoEstoqueA4.Caption := txtEndereco;
  end
  else begin
    qrEnderecoEstoqueA4.Caption := '';
    QRLabel3.Caption := '';
    QRBand4.Height := 70;
  end;
end;

procedure TFormImpressaoComprovanteEntregaGrafico.qrbndNFDetailBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
var
  enderecosEstoque: TArray<RecEnderecosEstoque>;
  i: Integer;
  txtEndereco: string;
  multiplo_usado: Double;
  adicionarAltura: Integer;
begin
  inherited;
  if FTipoMovimento = tpEntrega then begin
    qrlNFNomeProduto.Caption := _Biblioteca.NFormat(FItensEnt[FPosicao].ProdutoId) + ' - ' + FItensEnt[FPosicao].NomeProduto;
    if Length(qrlNFNomeProduto.Caption) > 38 then
      qrlNFNomeProduto.Font.Size := 6
    else
      qrlNFNomeProduto.Font.Size := 7;

    qrlNFMarca.Caption := FItensEnt[FPosicao].NomeMarca;

    if FItensEnt[FPosicao].UnidadeEntregaId <> '' then begin
      if True then

      qrlNFQuantidade.Caption := FloatToStr(FItensEnt[FPosicao].Quantidade / FItensEnt[FPosicao].MultiploVenda);
      qrlNFUnidade.Caption := FItensEnt[FPosicao].UnidadeEntregaId;
    end
    else begin
      qrlNFQuantidade.Caption := FloatToStr(FItensEnt[FPosicao].Quantidade);
      qrlNFUnidade.Caption := FItensEnt[FPosicao].Unidade;
    end;

    qrlNFLote.Caption := FItensEnt[FPosicao].Lote;
    qrlNFCodigoBarras.Caption := FItensEnt[FPosicao].CodigoBarras;
    qrlNFCodigoOriginal.Caption := FItensEnt[FPosicao].CodigoOriginalFabricante;

    if Sessao.getParametros.UtilEnderecoEstoque = 'S' then
      enderecosEstoque := _Produtos.BuscarEnderecosEstoqueCompleto(Sessao.getConexaoBanco, FItensEnt[FPosicao].ProdutoId);
  end
  else begin
    qrlNFNomeProduto.Caption := _Biblioteca.NFormat(FItensRet[FPosicao].ProdutoId) + ' - ' + FItensRet[FPosicao].NomeProduto;
    if Length(qrlNFNomeProduto.Caption) > 38 then
      qrlNFNomeProduto.Font.Size := 6
    else
      qrlNFNomeProduto.Font.Size := 7;

    qrlNFMarca.Caption := FItensRet[FPosicao].NomeMarca;

    if FItensRet[FPosicao].UnidadeEntregaId <> '' then begin
      qrlNFQuantidade.Caption := FloatToStr(FItensRet[FPosicao].Quantidade / FItensRet[FPosicao].MultiploVenda);
      qrlNFUnidade.Caption := FItensRet[FPosicao].UnidadeEntregaId;
    end
    else begin
      qrlNFQuantidade.Caption := FloatToStr(FItensRet[FPosicao].Quantidade);
      qrlNFUnidade.Caption := FItensRet[FPosicao].Unidade;
    end;

    qrlNFLote.Caption := FItensRet[FPosicao].Lote;
    qrlNFCodigoBarras.Caption := FItensRet[FPosicao].CodigoBarras;
    qrlNFCodigoOriginal.Caption := FItensRet[FPosicao].CodigoOriginalFabricante;

    if Sessao.getParametros.UtilEnderecoEstoque = 'S' then
      enderecosEstoque := _Produtos.BuscarEnderecosEstoqueCompleto(Sessao.getConexaoBanco, FItensRet[FPosicao].ProdutoId);
  end;

  if Sessao.getParametros.UtilEnderecoEstoque = 'S' then begin
    txtEndereco := '';
    for i := 0 to Length(enderecosEstoque) - 1 do begin
      if i > 0 then
        txtEndereco := txtEndereco + #10#13;

      txtEndereco :=
        txtEndereco +
        'Rua: ' + enderecosEstoque[i].nome_rua + ' ' +
        'M�dulo: ' + enderecosEstoque[i].nome_modulo + ' ' +
        'V�o: ' + enderecosEstoque[i].nome_vao + ' ' +
        'N�vel: ' + enderecosEstoque[i].nome_nivel;
    end;

    if txtEndereco = '' then
      qrbndNFDetail.Height := 120
    else begin
      adicionarAltura := i * 18;
      qrbndNFDetail.Height := 144 + adicionarAltura - 22;
    end;

    qrEnderecoEstoqueNF.Caption := txtEndereco;
  end
  else begin
    qrEnderecoEstoqueNF.Caption := '';
    QRLabel5.Caption := '';
    qrbndNFDetail.Height := 100;
  end;
  //22
  //144
end;

procedure TFormImpressaoComprovanteEntregaGrafico.qrRelatorioA4BeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicao := -1;
end;

procedure TFormImpressaoComprovanteEntregaGrafico.qrRelatorioA4NeedData(
  Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicao);
  if FTipoMovimento = tpEntrega then
    MoreData := FPosicao < Length(FItensEnt)
  else
    MoreData := FPosicao < Length(FItensRet);
end;

procedure TFormImpressaoComprovanteEntregaGrafico.qrRelatorioMatricialAfterPrint(
  Sender: TObject);
begin
  inherited;
  FFirstPage := False;
end;

procedure TFormImpressaoComprovanteEntregaGrafico.qrRelatorioMatricialBeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FFirstPage := True;
  FPosicao := -1;
end;

procedure TFormImpressaoComprovanteEntregaGrafico.qrRelatorioMatricialNeedData(
  Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicao);
  if FTipoMovimento = tpEntrega then
    MoreData := FPosicao < Length(FItensEnt)
  else
    MoreData := FPosicao < Length(FItensRet);
end;

procedure TFormImpressaoComprovanteEntregaGrafico.qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicao := -1;
end;

procedure TFormImpressaoComprovanteEntregaGrafico.qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicao);
  if FTipoMovimento = tpEntrega then
    MoreData := FPosicao < Length(FItensEnt)
  else
    MoreData := FPosicao < Length(FItensRet);
end;

procedure TFormImpressaoComprovanteEntregaGrafico.QuickRep1BeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicao := -1;
end;

procedure TFormImpressaoComprovanteEntregaGrafico.QuickRep1NeedData(
  Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicao);
  if FTipoMovimento = tpEntrega then
    MoreData := FPosicao < Length(FItensEnt)
  else
    MoreData := FPosicao < Length(FItensRet);
end;

procedure InicializacoesImpressaoMatricial(vForm: TFormImpressaoComprovanteEntregaGrafico);
var
  vDataHora: TDateTime;
begin
  vForm.FImpressao := 0;
  vDataHora := Sessao.getDataHora;

  vForm.qrEmpresaMatricial.Caption  := Sessao.getEmpresaLogada.RazaoSocial;
  vForm.qrCNPJMatricial.Caption     := Sessao.getEmpresaLogada.Cnpj;
  vForm.qrEnderecoMatricial.Caption := Sessao.getEmpresaLogada.Logradouro + ' ' + Sessao.getEmpresaLogada.Complemento + ' n� ' + Sessao.getEmpresaLogada.Numero;
  vForm.qrBairroMatricial.Caption   := Sessao.getEmpresaLogada.NomeBairro;
  vForm.qrCidadeUFMatricial.Caption := Sessao.getEmpresaLogada.NomeCidade + ' / ' + Sessao.getEmpresaLogada.EstadoId;
  vForm.qrTelefoneMatricialCabecalho.Caption := Sessao.getEmpresaLogada.TelefonePrincipal;
  vForm.qrFaxMatricial.Caption      := Sessao.getEmpresaLogada.TelefoneFax;
  vForm.qrEmailMatricial.Caption    := Sessao.getEmpresaLogada.EMail;
end;

end.

