unit ConfirmarDevolucoes;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatorios, Vcl.Buttons, _Sessao, _Biblioteca,
  Vcl.ExtCtrls, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, FrameLocais, _Devolucoes,
  Vcl.StdCtrls, EditLuka, FrameDataInicialFinal, StaticTextLuka, Vcl.Grids, _DevolucoesItens,
  GridLuka, _RecordsOrcamentosVendas, Vcl.Menus, _RecordsEspeciais, Informacoes.Devolucao,
  DefinirEntregasRetiradasDevolucao, CheckBoxLuka;

type
  TFormConfirmarDevolucoes = class(TFormHerancaRelatorios)
    FrDataDevolucao: TFrDataInicialFinal;
    ePedido: TEditLuka;
    Label1: TLabel;
    sgDevolucoes: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    sgItens: TGridLuka;
    StaticTextLuka2: TStaticTextLuka;
    pmOpcoes: TPopupMenu;
    miConfirmarDevolucao: TSpeedButton;
    miCancelarDevolucao: TSpeedButton;
    procedure sgDevolucoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgDevolucoesClick(Sender: TObject);
    procedure miCancelarDevolucaoClick(Sender: TObject);
    procedure miConfirmarDevolucaoClick(Sender: TObject);
    procedure sgDevolucoesDblClick(Sender: TObject);
  private
    FItens: TArray<RecDevolucoesItens>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

const
  (* Capa *)
  coDevolucao         = 0;
  coCliente           = 1;
  coPedidoId          = 2;
  coDataHoraDevolucao = 3;

  (* Itens *)
  ciProdutoId  = 0;
  ciNome       = 1;
  ciMarca      = 2;
  ciUnidade    = 3;
  ciQuantidade = 4;


{ TFormConfirmarDevolucoes }

procedure TFormConfirmarDevolucoes.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vDevolucoes: TArray<RecDevolucoes>;
  vDevolucoesIds: TArray<Integer>;
begin
  inherited;
  FItens := nil;
  vDevolucoesIds := nil;

  sgItens.ClearGrid();
  sgDevolucoes.ClearGrid();

  vSql := 'where DEV.STATUS = ''A'' ';

  if not FrDataDevolucao.NenhumaDataValida then
    vSql := vSql + ' and ' + FrDataDevolucao.getSqlFiltros('trunc(DEV.DATA_HORA_DEVOLUCAO)');

  if ePedido.AsInt > 0 then
    vSql := vSql + ' and DEV.ORCAMENTO_ID = ' + IntToStr(ePedido.AsInt);

  vSql := vSql + ' order by DEV.DEVOLUCAO_ID desc ';
  vDevolucoes := _Devolucoes.BuscarDevolucoesComando(Sessao.getConexaoBanco, vSql);
  if vDevolucoes = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vDevolucoes) to High(vDevolucoes) do begin
    sgDevolucoes.Cells[coPedidoId, i + 1]          := NFormat(vDevolucoes[i].orcamento_id);
    sgDevolucoes.Cells[coCliente, i + 1]           := NFormat(vDevolucoes[i].ClienteId) + ' - ' + vDevolucoes[i].NomeCliente;
    sgDevolucoes.Cells[coDevolucao, i + 1]         := NFormat(vDevolucoes[i].devolucao_id);
    sgDevolucoes.Cells[coDataHoraDevolucao, i + 1] := DHFormat(vDevolucoes[i].data_hora_devolucao);
    _Biblioteca.AddNoVetorSemRepetir(vDevolucoesIds, vDevolucoes[i].devolucao_id);
  end;
  sgDevolucoes.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vDevolucoes) );

  FItens := _DevolucoesItens.BuscarDevolucoesItensComando(Sessao.getConexaoBanco, 'where ' + FiltroInInt('ITE.DEVOLUCAO_ID', vDevolucoesIds) );
  SetarFoco(sgDevolucoes);
  sgDevolucoesClick(nil);
end;

procedure TFormConfirmarDevolucoes.Imprimir(Sender: TObject);
begin
  inherited;

end;

procedure TFormConfirmarDevolucoes.miCancelarDevolucaoClick(Sender: TObject);
var
  vDevolucaoId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;
  vDevolucaoId := SFormatInt(sgDevolucoes.Cells[coDevolucao, sgDevolucoes.Row]);
  if vDevolucaoId = 0 then begin
    _Biblioteca.Exclamar('Nenhuma devolu��o selecionada!');
    Abort;
  end;

  vRetBanco := _Devolucoes.CancelarDevolucao( Sessao.getConexaoBanco, vDevolucaoId );
  Sessao.AbortarSeHouveErro( vRetBanco );

  Carregar(Sender);
end;

procedure TFormConfirmarDevolucoes.miConfirmarDevolucaoClick(Sender: TObject);
var
  vDevolucaoId: Integer;
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;
  vDevolucaoId := SFormatInt(sgDevolucoes.Cells[coDevolucao, sgDevolucoes.Row]);
  if vDevolucaoId = 0 then begin
    NenhumRegistro;
    Exit;
  end;

  vRetTela := DefinirEntregasRetiradasDevolucao.Definir(vDevolucaoId, SFormatInt(sgDevolucoes.Cells[coPedidoId, sgDevolucoes.Row]));
  if vRetTela.BuscaCancelada then
    Exit;

  RotinaSucesso;
  Carregar(nil);
end;

procedure TFormConfirmarDevolucoes.sgDevolucoesClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vDevolucaoId: Integer;
begin
  inherited;
  sgItens.ClearGrid();

  vLinha := 0;
  vDevolucaoId := SFormatInt(sgDevolucoes.Cells[coDevolucao, sgDevolucoes.Row]);
  for i := Low(FItens) to High(FItens) do begin
    if vDevolucaoId <> FItens[i].DevolucaoId then
      Continue;

    Inc(vLinha);
    sgItens.Cells[ciProdutoId, vLinha]  := NFormat(FItens[i].ProdutoId);
    sgItens.Cells[ciNome, vLinha]       := FItens[i].NomeProduto;
    sgItens.Cells[ciMarca, vLinha]      := FItens[i].NomeMarca;
    sgItens.Cells[ciUnidade, vLinha]    := FItens[i].UnidadeVenda;
    sgItens.Cells[ciQuantidade, vLinha] := NFormatEstoque(FItens[i].QuantidadeDevolvidosEntregues + FItens[i].QuantidadeDevolvidosPendencias + FItens[i].QuantidadeDevolvSemPrevisao);
  end;
  sgItens.RowCount := IIfInt(vLinha > 1, vLinha + 1, 2);
end;

procedure TFormConfirmarDevolucoes.sgDevolucoesDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.Devolucao.Informar( SFormatInt(sgDevolucoes.Cells[coDevolucao, sgDevolucoes.Row]) );
end;

procedure TFormConfirmarDevolucoes.sgDevolucoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    coPedidoId,
    coDevolucao]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgDevolucoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormConfirmarDevolucoes.sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[
    ciProdutoId,
    ciQuantidade]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormConfirmarDevolucoes.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
