unit BuscarInformacoesPessoaRecebeuEntrega;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca,
  ComboBoxLuka, EditHoras, EditLukaData, Vcl.Mask, EditCpfCnpjLuka, EditLuka,
  Vcl.Buttons, Vcl.ExtCtrls;

type
  RecInfoEntrega = record
    Status: string;
    NomePessoa: string;
    CPFPessoa: string;
    DataHoraRetirada: TDateTime;
    Observacoes: string;
  end;

  TFormBuscarInformacoesPessoaRecebeuEntrega = class(TFormHerancaFinalizar)
    lblCPF_CNPJ: TLabel;
    lbl1: TLabel;
    lbllb12: TLabel;
    lbllb13: TLabel;
    eNome: TEditLuka;
    eCPF: TEditCPF_CNPJ_Luka;
    eDataEntrega: TEditLukaData;
    eHoraEntrega: TEditHoras;
    meObservacoes: TMemo;
    cbStatusEntrega: TComboBoxLuka;
    Label1: TLabel;
    procedure cbStatusEntregaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(pInfo: RecInfoEntrega): TRetornoTelaFinalizar<RecInfoEntrega>;

implementation

{$R *.dfm}


function Buscar(pInfo: RecInfoEntrega): TRetornoTelaFinalizar<RecInfoEntrega>;
var
  vForm: TFormBuscarInformacoesPessoaRecebeuEntrega;
begin
  vForm := TFormBuscarInformacoesPessoaRecebeuEntrega.Create(nil);

  vForm.cbStatusEntrega.SetIndicePorValor(pInfo.Status);
  vForm.eNome.Text := pInfo.NomePessoa;
  vForm.eCPF.Text := pInfo.CPFPessoa;
  vForm.eDataEntrega.AsData := pInfo.DataHoraRetirada;
  vForm.eHoraEntrega.AsHora := pInfo.DataHoraRetirada;
  vForm.meObservacoes.Lines.Text := pInfo.Observacoes;

  if vForm.cbStatusEntrega.ItemIndex = -1 then begin
    vForm.cbStatusEntrega.ItemIndex := 0;
    vForm.cbStatusEntregaChange(nil);
  end;
  SetarFoco(vForm.cbStatusEntrega);

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.Status           := vForm.cbStatusEntrega.GetValor;
    Result.Dados.NomePessoa       := vForm.eNome.Text;
    Result.Dados.CPFPessoa        := vForm.eCPF.Text;
    Result.Dados.DataHoraRetirada := ToDataHora(vForm.eDataEntrega.AsData, vForm.eHoraEntrega.AsHora);
    Result.Dados.Observacoes      := vForm.meObservacoes.Lines.Text;
  end;
end;

{ TFormBuscarInformacoesPessoaRecebeuEntrega }

procedure TFormBuscarInformacoesPessoaRecebeuEntrega.cbStatusEntregaChange(Sender: TObject);
begin
  inherited;
  _Biblioteca.Habilitar([eNome, eCPF, eDataEntrega, eHoraEntrega, meObservacoes], cbStatusEntrega.GetValor <> 'RTO');
end;

procedure TFormBuscarInformacoesPessoaRecebeuEntrega.FormShow(Sender: TObject);
begin
  inherited;
  cbStatusEntrega.SetIndicePorValor( 'ENT' );
  SetarFoco(eNome);
end;

procedure TFormBuscarInformacoesPessoaRecebeuEntrega.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if eNome.Enabled and (eNome.Trim = '') then begin
    _Biblioteca.Exclamar('O nome da pessoa que recebeu deve ser informado!');
    SetarFoco(eNome);
    Abort;
  end;
end;

end.
