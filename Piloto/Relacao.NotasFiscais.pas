unit Relacao.NotasFiscais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl, _RecordsEspeciais,
  Vcl.ComCtrls, Vcl.Buttons, Vcl.ExtCtrls, _FrameHerancaPrincipal, _ComunicacaoNFE, _Imagens,
  _FrameHenrancaPesquisas, FrameEmpresas, FrameProdutos, Frame.Cadastros, BuscaDados,
  FrameDataInicialFinal, CheckBoxLuka, Vcl.StdCtrls, GroupBoxLuka, Vcl.Grids, _Sessao, System.Math,
  GridLuka, Vcl.Menus, _NotasFiscais, _Biblioteca, Edicao.CapaNotaFiscal, Edicao.NotaFiscalItens,
  BuscarDados.InutilizacaoNFe, Informacoes.NotaFiscal, _RelacaoNotasFiscais, _RecordsNotasFiscais,
  Frame.Inteiros, Buscar.Diretorio, StaticTextLuka, FrameNumeros, ComboBoxLuka, Winapi.ShellAPI,
  FrameCFOPs, Impressao.ComprovanteGraficoNFCe, FrameIndicesDescontosVenda, BuscarDadosClienteEmissaoNota,
  FrameFaixaNumeros, FrameLocais, _ParametrosEmpresa, RadioGroupLuka, BuscarDadosEmpresaEmissaoNota,
  BuscarDadosTipoPagamentoNota;

type
  TFormRelacaoNotasFiscais = class(TFormHerancaRelatoriosPageControl)
    FrEmpresas: TFrEmpresas;
    FrCadastros: TFrameCadastros;
    FrProdutos: TFrProdutos;
    FrDatasCadastro: TFrDataInicialFinal;
    FrDatasEmissao: TFrDataInicialFinal;
    FrDatasCancelamento: TFrDataInicialFinal;
    gbTiposNota: TGroupBoxLuka;
    ckNfe: TCheckBoxLuka;
    pmRotinas: TPopupMenu;
    sgNotasFiscais: TGridLuka;
    spSeparador: TSplitter;
    gbStatus: TGroupBoxLuka;
    ckNaoEmitida: TCheckBoxLuka;
    ckCancelada: TCheckBoxLuka;
    sgProdutos: TGridLuka;
    FrCodigoNota: TFrameInteiros;
    FrNumeroNota: TFrameInteiros;
    ckNFCe: TCheckBoxLuka;
    ckEmitida: TCheckBoxLuka;
    ckDenegada: TCheckBoxLuka;
    StaticTextLuka1: TStaticTextLuka;
    FrCodigoOrcamento: TFrameInteiros;
    cbNumeroNotaGerado: TComboBoxLuka;
    Label1: TLabel;
    gbTipoMovimento: TGroupBoxLuka;
    ckVendaRetirarAto: TCheckBoxLuka;
    ckVendaRetirar: TCheckBoxLuka;
    ckVendaEntregar: TCheckBoxLuka;
    ckDevolucoesRetiradas: TCheckBoxLuka;
    ckDevolucoesEntregas: TCheckBoxLuka;
    ckTransfProdEmp: TCheckBoxLuka;
    ckDevolucaoAcumulados: TCheckBoxLuka;
    ckAcumulados: TCheckBoxLuka;
    ckOutrasNotas: TCheckBoxLuka;
    FrCodigoAcumulado: TFrNumeros;
    FrCodigoRetirada: TFrNumeros;
    FrCodigoEntrega: TFrNumeros;
    FrCodigoDevolucao: TFrNumeros;
    ckNfeNfce: TCheckBoxLuka;
    ckRetornoParcialTotalEntrega: TCheckBoxLuka;
    FrCFOP: TFrCFOPs;
    ckTransferenciaFiscal: TCheckBoxLuka;
    Panel1: TPanel;
    miInutilizarNumercaoNFe: TSpeedButton;
    miEditarCapaNotaFiscal: TSpeedButton;
    miEditarItensNotaFiscal: TSpeedButton;
    miCancelarNFe: TSpeedButton;
    miEmitirNFePendente: TSpeedButton;
    miImprimirDANFE: TSpeedButton;
    miGerarXMLs: TSpeedButton;
    miGerarPDFDanfe: TSpeedButton;
    miMarcarDesmarcarFocado: TSpeedButton;
    miMarcarDesmarcarTodos: TSpeedButton;
    miEmitirNFeNFCe: TSpeedButton;
    FrIndiceDescontoVenda: TFrIndicesDescontosVenda;
    sbAplicarIndiceDeducao: TSpeedButton;
    ckDevolucaoEntradaNF: TCheckBoxLuka;
    FrValorTotal: TFrFaixaNumeros;
    miAlterarEmpresaEmitente: TSpeedButton;
    miEmitirNfsSelecionadas: TSpeedButton;
    lb1: TLabel;
    cbOrdenacao: TComboBoxLuka;
    gbFormasPagamento: TGroupBoxLuka;
    ckCartao: TCheckBox;
    ckFinanceira: TCheckBox;
    ckCheque: TCheckBox;
    ckCobranca: TCheckBox;
    ckAcumulado: TCheckBox;
    ckDinheiro: TCheckBoxLuka;
    ckPix: TCheckBox;
    SpeedButton1: TSpeedButton;
    ckProducaoEstoqueEntrada: TCheckBoxLuka;
    ckProducaoEstoqueSaida: TCheckBoxLuka;
    FrLocalProduto: TFrLocais;
    ckAjusteEstoqueEntrada: TCheckBoxLuka;
    ckAjusteEstoqueSaida: TCheckBoxLuka;
    rgTipoPessoa: TRadioGroupLuka;
    TabSheet1: TTabSheet;
    gbClasseFiscal: TGroupBoxLuka;
    CheckBoxLuka1: TCheckBoxLuka;
    CheckBoxLuka2: TCheckBoxLuka;
    CheckBoxLuka3: TCheckBoxLuka;
    CheckBoxLuka4: TCheckBoxLuka;
    CheckBoxLuka5: TCheckBoxLuka;
    CheckBoxLuka6: TCheckBoxLuka;
    CheckBoxLuka7: TCheckBoxLuka;
    CheckBoxLuka8: TCheckBoxLuka;
    CheckBoxLuka9: TCheckBoxLuka;
    CheckBoxLuka10: TCheckBoxLuka;
    CheckBoxLuka11: TCheckBoxLuka;
    CheckBoxLuka12: TCheckBoxLuka;
    frProducaoEstoque: TFrameInteiros;
    SpeedButton2: TSpeedButton;
    frCodigoProducao: TFrameInteiros;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    sbAlterarTiposPagamento: TSpeedButton;
    procedure miEditarCapaNotaFiscalClick(Sender: TObject);
    procedure miEditarItensNotaFiscalClick(Sender: TObject);
    procedure miCancelarNFeClick(Sender: TObject);
    procedure miInutilizarNumercaoNFeClick(Sender: TObject);
    procedure miEmitirNFePendenteClick(Sender: TObject);
    procedure sgNotasFiscaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgNotasFiscaisGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgNotasFiscaisDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sgNotasFiscaisClick(Sender: TObject);
    procedure sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miGerarXMLsClick(Sender: TObject);
    procedure miImprimirDANFEClick(Sender: TObject);
    procedure miValidadorNFeClick(Sender: TObject);
    procedure miConsultaNFeCompletaClick(Sender: TObject);
    procedure miConsultaInutilizacoesClick(Sender: TObject);
    procedure miEmitirNFeNFCeClick(Sender: TObject);
    procedure miGerarPDFDanfeClick(Sender: TObject);
    procedure miConsultaNFeCompletaSEFAZGoClick(Sender: TObject);
    procedure miMarcarDesmarcarFocadoClick(Sender: TObject);
    procedure miMarcarDesmarcarTodosClick(Sender: TObject);
    procedure sgNotasFiscaisGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
    procedure sgNotasFiscaisKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbAplicarIndiceDeducaoClick(Sender: TObject);
    procedure miAlterarEmpresaEmitenteClick(Sender: TObject);
    procedure miEmitirNfsSelecionadasClick(Sender: TObject);
    procedure sbImprimirClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure sbAlterarTiposPagamentoClick(Sender: TObject);
  private
    FNotaItens: TArray<RecNotaFiscalItem>;
  protected
    procedure Carregar(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

implementation

{$R *.dfm}

uses
  _NotasFiscaisItens, _ImpressaoDANFE, _ImpressaoDANFE_NFCe, _HerancaFinalizar,
  _RecordsCadastros, _Empresas ;

const
  coSelecionado            = 0;
  coNotaFiscalId           = 1;
  coDataCadastro           = 2;
  coTipoNotaAnalitico      = 3;
  coTipoMovimentoAnalitico = 4;
  coRazaoSocial            = 5;
  coNumeroNota             = 6;
  coValorTotal             = 7;
  coValorTotProdutos       = 8;
  coValorOutDespesas       = 9;
  coValorDesconto          = 10;
  coBaseCalcICMS           = 11;
  coValorICMS              = 12;
  coBaseCalcICMSST         = 13;
  coValorICMSST            = 14;
  coValorIPI               = 15;
  coCFOP                   = 16;
  coStatus                 = 17;

  (* Colunas ocultas *)
  coEmpresa                = 18;
  coTipoNota               = 19;
  coTipoMovimento          = 20;
  coTipoLinha              = 21;
  coCadastroId             = 22;
  coEmpresaId              = 23;

  {Tipos de linha}
  coLinhaDetalhe  = 'D';
  coLinhaTotal    = 'T';

  {Grid de itens}
  ciProdutoId                      = 0;
  ciNomeProduto                    = 1;
  ciCST                            = 2;
  ciPrecoUnitario                  = 3;
  ciQuantidade                     = 4;
  ciUnidade                        = 5;
  ciValorDesconto                  = 6;
  ciValorOutrasDespesas            = 7;
  ciValorTotal                     = 8;
  ciBaseCalculoICMS                = 9;
  ciIndiceReducaoBaseCalculoICMS   = 10;
  ciPercentualICMS                 = 11;
  ciValorICMS                      = 12;
  ciBaseCalculoICMSST              = 13;
  ciIndiceReducaoBaseCalculoICMSST = 14;
  ciValorICMSST                    = 15;
  ciBaseCalculoPIS                 = 16;
  ciPercentualPIS                  = 17;
  ciValorPIS                       = 18;
  ciBaseCalculoCOFINS              = 19;
  ciPercentualCOFINS               = 20;
  ciValorCOFINS                    = 21;
  ciPercentualIVA                  = 22;
  ciPrecoPauta                     = 23;
  ciPercentualIPI                  = 24;
  ciValorIPI                       = 25;

  (* Colunas ocultas *)
  ciInformacoesAdicionais          = 26;

procedure TFormRelacaoNotasFiscais.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vSqlFormasPagto: string;
  vSqlRetiradas, vSqlEntregas: string;
  vLinha: Integer;
  vNotasIds: TArray<Integer>;
  vNotas: TArray<RecRelacaoNotasFiscais>;

  vTotais: record
    TotalValorTotal: Double;
    TotalValorTotalProdutos: Double;
    TotalValorOutrasDespesas: Double;
    TotalValorDesconto: Double;
    TotalBaseCalculoIcms: Double;
    TotalValorIcms: Double;
    TotalBaseCalculoIcmsSt: Double;
    TotalValorIcmsSt: Double;
    TotalValorIpi: Double;
  end;

  procedure SqlFormaPagamento(pColuna: string; pCheckBox: TCheckBox);
  begin
    if pCheckBox.Checked then
      AddOrSeNecessario(vSqlFormasPagto, ' ' + pColuna + IIfStr(ckProducaoEstoqueEntrada.Checked or ckProducaoEstoqueSaida.Checked, ' >= ', ' > ') + ' 0');
  end;
begin
  inherited;
  vLinha := 0;
  vNotasIds := nil;
  FNotaItens := nil;
  vSqlFormasPagto := '';

  sgNotasFiscais.ClearGrid();
  sgProdutos.ClearGrid();

  if not FrCodigoNota.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrCodigoNota.getSqlFiltros('NFI.NOTA_FISCAL_ID'))
  else begin
    if not FrNumeroNota.EstaVazio then
     _Biblioteca.WhereOuAnd(vSql, FrNumeroNota.getSqlFiltros('NFI.NUMERO_NOTA'));

    if cbNumeroNotaGerado.GetValor <> _Biblioteca.cbNaoFiltrar then
      _Biblioteca.WhereOuAnd(vSql, ' NFI.NUMERO_NOTA is ' + IIfStr(cbNumeroNotaGerado.GetValor = 'S', 'not ') + 'null ');

    if not gbStatus.NenhumMarcado then
      _Biblioteca.WhereOuAnd(vSql, gbStatus.GetSql('NFI.STATUS'));

    if not gbTiposNota.NenhumMarcado then
      _Biblioteca.WhereOuAnd(vSql, gbTiposNota.GetSql('NFI.TIPO_NOTA'));

    if not gbTipoMovimento.NenhumMarcado then
      _Biblioteca.WhereOuAnd(vSql, gbTipoMovimento.GetSql('NFI.TIPO_MOVIMENTO'));

    if not FrEmpresas.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrEmpresas.getSqlFiltros('NFI.EMPRESA_ID'));

    if not FrCadastros.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrCadastros.getSqlFiltros('NFI.CADASTRO_ID'));

    if not FrDatasCadastro.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vSql, FrDatasCadastro.getSqlFiltros('trunc(NFI.DATA_HORA_CADASTRO)'));

    if not FrDatasEmissao.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vSql, FrDatasEmissao.getSqlFiltros('trunc(NFI.DATA_HORA_EMISSAO)'));

    if not FrDatasCancelamento.NenhumaDataValida then
      _Biblioteca.WhereOuAnd(vSql, FrDatasCancelamento.getSqlFiltros('trunc(NFI.DATA_HORA_CANCELAMENTO_NFE)'));

    if not FrProdutos.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, ' NFI.NOTA_FISCAL_ID in( select NOTA_FISCAL_ID from NOTAS_FISCAIS_ITENS where ' + FrProdutos.getSqlFiltros('PRODUTO_ID') + ') ');

    if not gbClasseFiscal.TodosMarcados then
      _Biblioteca.WhereOuAnd(vSql, ' NFI.NOTA_FISCAL_ID in( select NOTA_FISCAL_ID from NOTAS_FISCAIS_ITENS where ' + gbClasseFiscal.GetSql('CST') + ') ');

    if not FrCodigoOrcamento.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrCodigoOrcamento.getSqlFiltros('NFI.ORCAMENTO_ID'));

    if not FrCodigoAcumulado.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrCodigoAcumulado.TrazerFiltros('NFI.ACUMULADO_ID'));

    if not frProducaoEstoque.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, frProducaoEstoque.getSqlFiltros('NFI.PRODUCAO_ESTOQUE_ID'));

    if not FrCodigoRetirada.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrCodigoRetirada.TrazerFiltros('NFI.RETIRADA_ID'));

    if not FrCodigoEntrega.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrCodigoEntrega.TrazerFiltros('NFI.ENTREGA_ID'));

    if not FrCodigoDevolucao.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrCodigoDevolucao.TrazerFiltros('NFI.DEVOLUCAO_ID'));

    if not frCodigoProducao.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, frCodigoProducao.getSqlFiltros('NFI.PRODUCAO_ESTOQUE_ID'));

    if not FrCFOP.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, ' NFI.NOTA_FISCAL_ID in( select NOTA_FISCAL_ID from NOTAS_FISCAIS_ITENS where ' + FrCFOP.getSqlFiltros('CFOP_ID') + ' ) ');

    if rgTipoPessoa.GetValor <> 'T' then
      _Biblioteca.WhereOuAnd(vSql, ' CAD.TIPO_PESSOA = ''' + rgTipoPessoa.GetValor + '''');

    if (ckDinheiro.Checked) and (ckPix.Checked) and (ckCheque.Checked) and (ckCartao.Checked) and (ckFinanceira.Checked) and
       (ckCobranca.Checked) and (ckAcumulado.Checked) then
    //N�o filtra por pagamento, s� se estiver marcado somente alguns tipos
    else begin
      if (ckDinheiro.Checked) or (ckPix.Checked) or (ckCheque.Checked) or (ckCartao.Checked) or (ckFinanceira.Checked) or
         (ckCobranca.Checked) or (ckAcumulado.Checked) then
      begin
        SqlFormaPagamento('NFI.VALOR_RECEBIDO_DINHEIRO', ckDinheiro);
        SqlFormaPagamento('NFI.VALOR_RECEBIDO_PIX', ckPix);
        SqlFormaPagamento('NFI.VALOR_RECEBIDO_CHEQUE', ckCheque);
        SqlFormaPagamento('NFI.VALOR_RECEBIDO_CARTAO_DEB', ckCartao);
        SqlFormaPagamento('NFI.VALOR_RECEBIDO_CARTAO_CRED', ckCartao);
        SqlFormaPagamento('NFI.VALOR_RECEBIDO_FINANCEIRA', ckFinanceira);
        SqlFormaPagamento('NFI.VALOR_RECEBIDO_COBRANCA + NFI.VALOR_RECEBIDO_CREDITO', ckCobranca);
        SqlFormaPagamento('NFI.VALOR_RECEBIDO_ACUMULADO', ckAcumulado);
        vSql := vSql + ' and ( ' + vSqlFormasPagto + ' )';
      end;
    end;

    _Biblioteca.WhereOuAnd(vSql, FrValorTotal.getSQL('NFI.VALOR_TOTAL'));

    if not FrLocalProduto.EstaVazio then begin
      vSqlRetiradas := FrLocalProduto.getSqlFiltros('REE.LOCAL_ID');
      vSqlEntregas := FrLocalProduto.getSqlFiltros('ENN.LOCAL_ID');
      _Biblioteca.WhereOuAnd(vSql, ' (' + vSqlRetiradas + ' OR ' + vSqlEntregas + ') ');
    end;
  end;

  if cbOrdenacao.GetValor = 'DAT' then
    vSql := vSql + ' order by NFI.DATA_HORA_CADASTRO '
  else
    vSql := vSql + ' order by NFI.TIPO_NOTA, NFI.NUMERO_NOTA ';
//  vSql := vSql + ' order by NFI.NOTA_FISCAL_ID ';

  vNotas := _RelacaoNotasFiscais.BuscarNotasFiscais(Sessao.getConexaoBanco, vSql);
  if vNotas = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vNotas) to High(vNotas) do begin
    Inc(vLinha);

    sgNotasFiscais.Cells[coSelecionado, vLinha]            := _Biblioteca.charNaoSelecionado;
    sgNotasFiscais.Cells[coNotaFiscalId, vLinha]           := _Biblioteca.NFormat(vNotas[i].notaFiscalId);
    sgNotasFiscais.Cells[coDataCadastro, vLinha]           := _Biblioteca.DFormat(vNotas[i].dataCadastro);
    sgNotasFiscais.Cells[coTipoNotaAnalitico, vLinha]      := vNotas[i].tipoNotaAnalitico;
    sgNotasFiscais.Cells[coTipoMovimentoAnalitico, vLinha] := _NotasFiscais.getTipoMovimentoAnalitico(vNotas[i].tipoMovimento);
    sgNotasFiscais.Cells[coRazaoSocial, vLinha]            := vNotas[i].razoSocial;
    sgNotasFiscais.Cells[coNumeroNota, vLinha]             := _Biblioteca.NFormatN(vNotas[i].numeroNota);
    sgNotasFiscais.Cells[coValorTotal, vLinha]             := _Biblioteca.NFormat(vNotas[i].valorTotal);
    sgNotasFiscais.Cells[coValorTotProdutos, vLinha]       := _Biblioteca.NFormat(vNotas[i].ValorTotalProdutos);
    sgNotasFiscais.Cells[coValorOutDespesas, vLinha]       := _Biblioteca.NFormat(vNotas[i].ValorOutrasDespesas);
    sgNotasFiscais.Cells[coValorDesconto, vLinha]          := _Biblioteca.NFormat(vNotas[i].ValorDesconto);
    sgNotasFiscais.Cells[coBaseCalcICMS, vLinha]           := _Biblioteca.NFormat(vNotas[i].BaseCalculoIcms);
    sgNotasFiscais.Cells[coValorICMS, vLinha]              := _Biblioteca.NFormat(vNotas[i].ValorIcms);
    sgNotasFiscais.Cells[coBaseCalcICMSST, vLinha]         := _Biblioteca.NFormat(vNotas[i].BaseCalculoIcmsSt);
    sgNotasFiscais.Cells[coValorICMSST, vLinha]            := _Biblioteca.NFormat(vNotas[i].ValorIcmsSt);
    sgNotasFiscais.Cells[coValorIPI, vLinha]               := _Biblioteca.NFormat(vNotas[i].ValorIpi);
    sgNotasFiscais.Cells[coCFOP, vLinha]                   := vNotas[i].cfop;
    sgNotasFiscais.Cells[coStatus, vLinha]                 := vNotas[i].statusAnalitico;
    sgNotasFiscais.Cells[coEmpresa, vLinha]                := vNotas[i].empresa;
    sgNotasFiscais.Cells[coTipoNota, vLinha]               := vNotas[i].tipoNota;
    sgNotasFiscais.Cells[coTipoMovimento, vLinha]          := vNotas[i].tipoMovimento;
    sgNotasFiscais.Cells[coTipoLinha, vLinha]              := coLinhaDetalhe;
    sgNotasFiscais.Cells[coCadastroId, vLinha]           := _Biblioteca.NFormat(vNotas[i].cadastroId);
    sgNotasFiscais.Cells[coEmpresaId, vLinha]            := _Biblioteca.NFormat(vNotas[i].empresaId);

    vTotais.TotalValorTotal          := vTotais.TotalvalorTotal + vNotas[i].valorTotal;
    vTotais.TotalValorTotalProdutos  := vTotais.TotalValorTotalProdutos + vNotas[i].ValorTotalProdutos;
    vTotais.TotalValorOutrasDespesas := vTotais.TotalValorOutrasDespesas + vNotas[i].ValorOutrasDespesas;
    vTotais.TotalValorDesconto       := vTotais.TotalValorDesconto + vNotas[i].ValorDesconto;
    vTotais.TotalBaseCalculoIcms     := vTotais.TotalBaseCalculoIcms + vNotas[i].BaseCalculoIcms;
    vTotais.TotalValorIcms           := vTotais.TotalValorIcms + vNotas[i].ValorIcms;
    vTotais.TotalBaseCalculoIcmsSt   := vTotais.TotalBaseCalculoIcmsSt + vNotas[i].BaseCalculoIcmsSt;
    vTotais.TotalValorIcmsSt         := vTotais.TotalValorIcmsSt + vNotas[i].ValorIcmsSt;
    vTotais.TotalValorIpi            := vTotais.TotalValorIpi + vNotas[i].ValorIpi;

    _Biblioteca.AddNoVetorSemRepetir(vNotasIds, vNotas[i].notaFiscalId);
  end;

  Inc(vLinha);
  sgNotasFiscais.Cells[coNumeroNota, vLinha]       := 'TOTAIS:';
  sgNotasFiscais.Cells[coValorTotal, vLinha]       := _Biblioteca.NFormat(vTotais.TotalValorTotal);
  sgNotasFiscais.Cells[coValorTotProdutos, vLinha] := _Biblioteca.NFormat(vTotais.TotalValorTotalProdutos);
  sgNotasFiscais.Cells[coValorOutDespesas, vLinha] := _Biblioteca.NFormat(vTotais.TotalValorOutrasDespesas);
  sgNotasFiscais.Cells[coValorDesconto, vLinha]    := _Biblioteca.NFormat(vTotais.TotalValorDesconto);
  sgNotasFiscais.Cells[coBaseCalcICMS, vLinha]     := _Biblioteca.NFormat(vTotais.TotalBaseCalculoIcms);
  sgNotasFiscais.Cells[coValorICMS, vLinha]        := _Biblioteca.NFormat(vTotais.TotalValorIcms);
  sgNotasFiscais.Cells[coBaseCalcICMSST, vLinha]   := _Biblioteca.NFormat(vTotais.TotalBaseCalculoIcmsSt);
  sgNotasFiscais.Cells[coValorICMSST, vLinha]      := _Biblioteca.NFormat(vTotais.TotalValorIcmsSt);
  sgNotasFiscais.Cells[coValorIPI, vLinha]         := _Biblioteca.NFormat(vTotais.TotalValorIpi);
  sgNotasFiscais.Cells[coTipoLinha, vLinha]        := coLinhaTotal;

  FNotaItens := _NotasFiscaisItens.BuscarNotasFiscaisItensComando(Sessao.getConexaoBanco, 'where ' + _Biblioteca.FiltroInInt('ITE.NOTA_FISCAL_ID', vNotasIds));

  sgNotasFiscais.SetLinhasGridPorTamanhoVetor(vLinha);
  sgNotasFiscaisClick(Sender);

  SetarFoco(sgNotasFiscais);
end;

procedure TFormRelacaoNotasFiscais.FormCreate(Sender: TObject);
begin
  inherited;
  FrEmpresas.InserirDadoPorChave(Sessao.getEmpresaLogada.EmpresaId);
  FrIndiceDescontoVenda.Visible := (Sessao.getParametros.AplicarIndiceDeducao = 'S') and (Sessao.AutorizadoRotina('aplicar_indice_deducao'));
  sbAplicarIndiceDeducao.Visible := FrIndiceDescontoVenda.Visible;
  miEmitirNfsSelecionadas.Visible := Sessao.AutorizadoRotina('selecionar_varias_notas_emissao');
  sbAlterarTiposPagamento.Visible := Sessao.getUsuarioLogado.funcionario_id = 1;
end;

procedure TFormRelacaoNotasFiscais.Imprimir(Sender: TObject);
var
  vNotaFiscalId: Integer;
begin
  inherited;
  vNotaFiscalId := SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, sgNotasFiscais.Row]);
  if vNotaFiscalId = 0 then
    Exit;

  if sgNotasFiscais.Cells[coStatus, sgNotasFiscais.Row] <> 'Emitida' then begin
    Exclamar('� poss�vel somente fazer a impress�o do DANFE de uma nota fiscal eletr�nica j� emitida!');
    Exit;
  end;

  if sgNotasFiscais.Cells[coTipoNotaAnalitico, sgNotasFiscais.Row] = 'NF-e' then begin
    if _ImpressaoDANFE.Imprimir(vNotaFiscalId) then
      Carregar(Sender);
  end
  else begin
    if Impressao.ComprovanteGraficoNFCe.Imprimir(vNotaFiscalId) then
      Carregar(Sender);
  end;
end;

procedure TFormRelacaoNotasFiscais.miAlterarEmpresaEmitenteClick(
  Sender: TObject);
var
  vNotaFiscalId: Integer;
  vRetornoRec: TRetornoTelaFinalizar<RecEmpresas>;
  vRetorno: RecRetornoBD;
  vEmpresa: TArray<RecEmpresas>;
  vEmpresaAtualId: Integer;
  empresaAtualNota: TArray<RecParametrosEmpresa>;
  empresaNovaNota: TArray<RecParametrosEmpresa>;
begin
  inherited;
  vNotaFiscalId := SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, sgNotasFiscais.Row]);
  if vNotaFiscalId = 0 then
    Exit;

  vEmpresaAtualId := SFormatInt(sgNotasFiscais.Cells[coEmpresaId, sgNotasFiscais.Row]);

  if sgNotasFiscais.Cells[coStatus, sgNotasFiscais.Row] = 'Emitida' then begin
    Exclamar('A nota fiscal selecionada j� foi emitida!');
    Exit;
  end;

  if sgNotasFiscais.Cells[coStatus, sgNotasFiscais.Row] = 'Cancelada' then begin
    Exclamar('A nota fiscal selecionada est� cancelada!');
    Exit;
  end;

  vRetornoRec := BuscarDadosEmpresaEmissaoNota.BuscarDadosEmpresaEmitente;
  if vRetornoRec.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    Exit;
  end;

  try
    vEmpresa := _Empresas.BuscarEmpresas(Sessao.getConexaoBanco, 0, [vRetornoRec.Dados.EmpresaId]);

    //Necess�rio verificar a mudan�a de regimes porque as vezes a nota tem ICMS e no novo emitente n�o vai ter
    empresaAtualNota := _ParametrosEmpresa.BuscarParametrosEmpresas(Sessao.getConexaoBanco, 0, [vEmpresaAtualId]);
    empresaNovaNota := _ParametrosEmpresa.BuscarParametrosEmpresas(Sessao.getConexaoBanco, 0, [vEmpresa[0].EmpresaId]);

    if (empresaAtualNota[0].RegimeTributario = 'LP') and (empresaNovaNota[0].RegimeTributario <>  empresaAtualNota[0].RegimeTributario) then begin
      _Biblioteca.Informar(
        'ATEN��O! O regime tribut�rio da empresa da nota � diferente do regime da nova empresa.'+ #13#10 +
        'Antes de emitir a nota valide se a base e valor de icms est�o corretos.'
      );
    end;

    vRetorno := _RelacaoNotasFiscais.AlterarEmpresaEmissaoNota(Sessao.getConexaoBanco, vNotaFiscalId, vEmpresa[0]);
    if vRetorno.TeveErro then
      Exclamar(vRetorno.MensagemErro)
    else
      RotinaSucesso;
  finally
    Carregar(Sender);
  end;

end;

procedure TFormRelacaoNotasFiscais.miCancelarNFeClick(Sender: TObject);
var
  oNFe: TComunicacaoNFe;
  vJustificativa: string;
  vNotaFiscalId: Integer;

  vRetBanco: RecRetornoBD;
begin
  inherited;

  vNotaFiscalId := SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, sgNotasFiscais.Row]);
  if vNotaFiscalId = 0 then
    Exit;

  vRetBanco := _NotasFiscais.PodeCancelarNFe(Sessao.getConexaoBanco, vNotaFiscalId);
  if vRetBanco.TeveErro then begin
    _Biblioteca.Exclamar(vRetBanco.MensagemErro);
    Exit;
  end;

  vJustificativa := BuscaDados.BuscarDados('Justificativa', '');
  if vJustificativa = '' then begin
    Exclamar('� necess�rio informar a justificativa para cancelamento da NF-e!');
    Exit;
  end;

  if Length(Trim(vJustificativa)) < 15 then begin
    Exclamar('A justificativa para cancelamento da NF-e deve ter no m�nimo 15 caracteres!');
    Exit;
  end;

  oNFe := TComunicacaoNFe.Create(Self);
  if oNFe.CancelarNFe(vNotaFiscalId, vJustificativa) then
    Carregar(Sender);

  oNFe.Free;
end;

procedure TFormRelacaoNotasFiscais.miConsultaInutilizacoesClick(Sender: TObject);
begin
  inherited;
  ShellExecute(
    handle,
    'open',
    PChar('http://www.nfe.fazenda.gov.br/portal/consulta.aspx?tipoConsulta=inutilizacao&tipoConteudo=YG1QjUXR4PY='),
    '',
    '',
    SW_SHOWNORMAL
  );
end;

procedure TFormRelacaoNotasFiscais.miConsultaNFeCompletaClick(Sender: TObject);
begin
  inherited;
  ShellExecute(
    handle,
    'open',
    PChar('http://www.nfe.fazenda.gov.br/portal/consultaRecaptcha.aspx?tipoConsulta=completa&tipoConteudo=XbSeqxE8pl8='),
    '',
    '',
    SW_SHOWNORMAL
  );
end;

procedure TFormRelacaoNotasFiscais.miConsultaNFeCompletaSEFAZGoClick(Sender: TObject);
begin
  inherited;
  ShellExecute(
    handle,
    'open',
    PChar('http://www.nfe.go.gov.br/pagina/ver/10270/consulta-completa'),
    '',
    '',
    SW_SHOWNORMAL
  );
end;

procedure TFormRelacaoNotasFiscais.miEditarCapaNotaFiscalClick(Sender: TObject);
var
  vRetTela: TRetornoTelaFinalizar;
begin
  inherited;

  vRetTela := Edicao.CapaNotaFiscal.Editar(SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, sgNotasFiscais.Row]));
  if vRetTela.BuscaCancelada then begin
    RotinaCanceladaUsuario;
    SetarFoco(sgNotasFiscais);
    Exit;
  end;

  RotinaSucesso;
  Carregar(Sender);
end;

procedure TFormRelacaoNotasFiscais.miEditarItensNotaFiscalClick(Sender: TObject);
begin
  inherited;
  //Aleterado Ezequiel para poder eitar itens de notas emitidas
//  if sgNotasFiscais.Cells[coStatus, sgNotasFiscais.Row] <> 'N�o emitida' then begin
//    Exclamar('� poss�vel editar somente notas fiscais n�o emitidas!');
//    Exit;
//  end;

  case Edicao.NotaFiscalItens.Editar(SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, sgNotasFiscais.Row])).RetTela of
    trOk: begin
      RotinaSucesso;
      Carregar(Sender);
    end;

    trCancelado: begin
      RotinaCanceladaUsuario;
      SetarFoco(sgNotasFiscais);
    end;
  end;
end;

procedure TFormRelacaoNotasFiscais.miEmitirNFeNFCeClick(Sender: TObject);
var
  vNotaFiscalId: Integer;
  vRetBanco: RecRetornoBD;
begin
  inherited;

  vNotaFiscalId := SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, sgNotasFiscais.Row]);
  if vNotaFiscalId = 0 then
    Exit;

  vRetBanco := _NotasFiscais.GerarNFeDeNFCe(Sessao.getConexaoBanco, vNotaFiscalId);
  Sessao.AbortarSeHouveErro(vRetBanco);

  if _ComunicacaoNFE.EnviarNFe(vRetBanco.AsInt) then
    Carregar(Sender);
end;

procedure TFormRelacaoNotasFiscais.miEmitirNFePendenteClick(Sender: TObject);
var
  vNotaFiscalId: Integer;
begin
  inherited;
  vNotaFiscalId := SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, sgNotasFiscais.Row]);
  if vNotaFiscalId = 0 then
    Exit;

  try
    _ComunicacaoNFE.EnviarNFe(vNotaFiscalId);
  finally
    Carregar(Sender);
  end;
end;

procedure TFormRelacaoNotasFiscais.miEmitirNfsSelecionadasClick(
  Sender: TObject);
var
  vNotaFiscalId: Integer;
  i: Integer;
begin
  if not _Biblioteca.Perguntar('Deseja emitir todas as notas selecionadas?') then
    Exit;

  try
    for i := 0 to sgNotasFiscais.RowCount -1 do  begin
      if sgNotasFiscais.Cells[coSelecionado, i] <> '     ' then
        Continue;

//      if sgNotasFiscais.Cells[coTipoNota, i] <> 'N' then
//         Continue;

      if sgNotasFiscais.Cells[coTipoMovimento, i] = 'OUT' then
         Continue;

      if sgNotasFiscais.Cells[coStatus, i] <> 'N�o emitida' then
         Continue;

      vNotaFiscalId := SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, i]);
      if vNotaFiscalId = 0 then
        Continue;

      _ComunicacaoNFE.EnviarNFe(vNotaFiscalId, False);
    end;
  finally
    Carregar(Sender);
  end;
end;

procedure TFormRelacaoNotasFiscais.miGerarPDFDanfeClick(Sender: TObject);
var
  i: Integer;
  vNotasIds: TArray<Integer>;
  vRetorno: TRetornoTelaFinalizar<string>;
begin
  inherited;

  vNotasIds := _Biblioteca.getIdsSelecionados(sgNotasFiscais, coSelecionado, coNotaFiscalId);
  if vNotasIds = nil then begin
    _Biblioteca.NenhumRegistroSelecionado;
    Exit;
  end;

  if sgNotasFiscais.Cells[coStatus, sgNotasFiscais.Row] <> 'Emitida' then begin
    Exclamar('� poss�vel somente fazer a impress�o do DANFE de uma nota fiscal eletr�nica j� emitida!');
    Exit;
  end;

  vRetorno := Buscar.Diretorio.Buscar();
  if vRetorno.RetTela = trCancelado then
    Exit;

  for i := Low(vNotasIds) to High(vNotasIds) do
    _ImpressaoDANFE.Imprimir(vNotasIds[i], vRetorno.Dados)
end;

procedure TFormRelacaoNotasFiscais.miGerarXMLsClick(Sender: TObject);
var
  i: Integer;
  vXmls: TArray<RecXMLs>;
  vNotasIds: TArray<Integer>;
  vRetorno: TRetornoTelaFinalizar<string>;
begin
  inherited;

  vNotasIds := _Biblioteca.getIdsSelecionados(sgNotasFiscais, coSelecionado, coNotaFiscalId);
  if vNotasIds = nil then begin
    _Biblioteca.NenhumRegistroSelecionado;
    Exit;
  end;

  vXmls :=
    _NotasFiscais.BuscarXMLsNFe(
      Sessao.getConexaoBanco,
      'where ' + FiltroInInt('NFI.NOTA_FISCAL_ID', vNotasIds) + ' ' +
      'and NFI.STATUS in(''E'', ''A'') '
    );

  if vXmls = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vRetorno := Buscar.Diretorio.Buscar();
  if vRetorno.RetTela = trCancelado then
    Exit;

  for i := Low(vXmls) to High(vXmls) do begin
    vXmls[i].xml.SaveToFile(vRetorno.Dados + vXmls[i].chave + '.xml');
    vXmls[i].xml.Free;
  end;

  RotinaSucesso;
end;

procedure TFormRelacaoNotasFiscais.miImprimirDANFEClick(Sender: TObject);
var
  vNotaFiscalId: Integer;
begin
  inherited;
  vNotaFiscalId := SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, sgNotasFiscais.Row]);
  if vNotaFiscalId = 0 then
    Exit;

  if sgNotasFiscais.Cells[coStatus, sgNotasFiscais.Row] <> 'Emitida' then begin
    Exclamar('� poss�vel somente fazer a impress�o do DANFE de uma nota fiscal eletr�nica j� emitida!');
    Exit;
  end;

  if sgNotasFiscais.Cells[coTipoNotaAnalitico, sgNotasFiscais.Row] = 'NF-e' then begin
    if _ImpressaoDANFE.Imprimir(vNotaFiscalId) then
      Carregar(Sender);
  end
  else begin
    if Impressao.ComprovanteGraficoNFCe.Imprimir(vNotaFiscalId) then
      Carregar(Sender);
  end;
end;

procedure TFormRelacaoNotasFiscais.miInutilizarNumercaoNFeClick(Sender: TObject);
var
  oNFe: TComunicacaoNFe;
  vDadosInut: TRetornoTelaFinalizar<RecDadosInutilizacao>;
begin
  inherited;
  vDadosInut := BuscarDados.InutilizacaoNFe.BuscarDados();
  if vDadosInut.RetTela = trCancelado then
    Exit;

  oNFe := TComunicacaoNFe.Create(Self);

  if
    oNFe.InutilizarNFe(
      Sessao.getEmpresaLogada.cnpj,
      vDadosInut.Dados.ano,
      vDadosInut.Dados.serie,
      vDadosInut.Dados.numeracao_inicial,
      vDadosInut.Dados.numeracao_final,
      vDadosInut.Dados.justificativa,
      vDadosInut.Dados.modeloNota
    )
  then
    _Biblioteca.Informar('Numera��o inutilizada com sucesso.');

  oNFe.Free;
end;

procedure TFormRelacaoNotasFiscais.miMarcarDesmarcarFocadoClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.MarcarDesmarcarTodos(sgNotasFiscais, coSelecionado, VK_SPACE, []);
end;

procedure TFormRelacaoNotasFiscais.miMarcarDesmarcarTodosClick(Sender: TObject);
begin
  inherited;
  _Biblioteca.MarcarDesmarcarTodos(sgNotasFiscais, coSelecionado, VK_SPACE, [ssCtrl]);
end;

procedure TFormRelacaoNotasFiscais.miValidadorNFeClick(Sender: TObject);
begin
  inherited;
  ShellExecute(handle, 'open', PChar('https://www.sefaz.rs.gov.br/nfe/NFE-VAL.aspx'), '', '', SW_SHOWNORMAL);
end;

procedure TFormRelacaoNotasFiscais.sbAlterarTiposPagamentoClick(
  Sender: TObject);
var
  vRetornoRec: TRetornoTelaFinalizar<RecValoresNotaFiscal>;
  vRetorno: RecRetornoBD;
  vNotaFiscalId: Integer;
  vTotalPagamentos: Double;
begin
  inherited;
  vNotaFiscalId := SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, sgNotasFiscais.Row]);
  if vNotaFiscalId = 0 then
    Exit;

  vTotalPagamentos := _NotasFiscais.BuscarTiposPagamentosNota(
    Sessao.getConexaoBanco,
    vNotaFiscalId
  );

  vRetornoRec := BuscarDadosTipoPagamentoNota.BuscarDadosTipoPagamento(vTotalPagamentos);
  if vRetornoRec.RetTela = trCancelado then begin
    RotinaCanceladaUsuario;
    Exit;
  end;

  vRetorno := _NotasFiscais.AtualizarTiposPagamentoNota(
    Sessao.getConexaoBanco,
    vRetornoRec.Dados,
    vNotaFiscalId
  );

  if vRetorno.TeveErro then
    Exclamar(vRetorno.MensagemErro)
  else
    RotinaSucesso;
end;

procedure TFormRelacaoNotasFiscais.sbAplicarIndiceDeducaoClick(Sender: TObject);
var
  nota_fiscal_id: Integer;
  cadastro_id: Integer;
  indice_id: Integer;
  retorno: RecRetornoBD;
  i: Integer;
begin
  inherited;
  if FrIndiceDescontoVenda.EstaVazio then begin
     Exclamar('Selecione o indice de dedu��o que ser� aplicado!');
     Exit;
  end;

//  if sgNotasFiscais.Cells[coTipoNota, sgNotasFiscais.Row] <> 'N' then begin
//     Exclamar('O indice de dedu��o s� pode ser aplicado a uma NF-e!');
//     Exit;
//  end;
//
//  if sgNotasFiscais.Cells[coTipoMovimento, sgNotasFiscais.Row] = 'OUT' then begin
//     Exclamar('O indice de dedu��o n�o pode ser aplicado em notas geradas pelo movimento de "Outras Notas"!');
//     Exit;
//  end;
//
//  if sgNotasFiscais.Cells[coStatus, sgNotasFiscais.Row] <> 'N�o emitida' then begin
//     Exclamar('O indice de dedu��o s� pode ser aplicado em notas que ainda n�o foram emitidas!');
//     Exit;
//  end;

  if not _Biblioteca.Perguntar('Este processo � irrevers�vel. Deseja realmente continuar?') then
    Exit;

  indice_id := FrIndiceDescontoVenda.getDados().IndiceId;

  for i := 0 to sgNotasFiscais.RowCount -1 do  begin
    if sgNotasFiscais.Cells[coSelecionado, i] <> '     ' then
      Continue;
                                     
    if sgNotasFiscais.Cells[coTipoMovimento, i] = 'OUT' then
       Continue;

    if sgNotasFiscais.Cells[coStatus, i] <> 'N�o emitida' then
       Continue;

    cadastro_id := SFormatInt(sgNotasFiscais.Cells[coCadastroId, i]);
    if not _RelacaoNotasFiscais.PodeCalcularIndiceDeducao(Sessao.getConexaoBanco, cadastro_id) then
      Continue;

    nota_fiscal_id := SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, i]);

    retorno := _RelacaoNotasFiscais.AplicarIndiceDeducao(Sessao.getConexaoBanco, nota_fiscal_id, indice_id);
    if retorno.TeveErro then
      Exclamar('N�o foi poss�vel aplicar o indice de dedu��o na nota fiscal selecionada! ' + retorno.MensagemErro);
  end;

  Exclamar('Processo finalizado!');
  Carregar(nil);
end;

procedure TFormRelacaoNotasFiscais.sbImprimirClick(Sender: TObject);
begin
  GridToExcel(sgNotasFiscais);
end;

procedure TFormRelacaoNotasFiscais.sgNotasFiscaisClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
begin
  inherited;
  vLinha := 1;
  sgProdutos.ClearGrid;

  for i := Low(FNotaItens) to High(FNotaItens) do begin
    if SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, sgNotasFiscais.Row]) <> FNotaItens[i].nota_fiscal_id then
      Continue;

    sgProdutos.Cells[ciProdutoId, vLinha]                      := NFormat(FNotaItens[i].produto_id);
    sgProdutos.Cells[ciNomeProduto, vLinha]                    := FNotaItens[i].nome_produto;
    sgProdutos.Cells[ciCST, vLinha]                            := FNotaItens[i].cst;
    sgProdutos.Cells[ciPrecoUnitario, vLinha]                  := NFormat(FNotaItens[i].preco_unitario);
    sgProdutos.Cells[ciQuantidade, vLinha]                     := NFormat(FNotaItens[i].quantidade);
    sgProdutos.Cells[ciUnidade, vLinha]                        := FNotaItens[i].unidade;
    sgProdutos.Cells[ciValorDesconto, vLinha]                  := NFormatN(FNotaItens[i].valor_total_desconto);
    sgProdutos.Cells[ciValorOutrasDespesas, vLinha]            := NFormatN(FNotaItens[i].valor_total_outras_despesas);
    sgProdutos.Cells[ciValorTotal, vLinha]                     := NFormatN(FNotaItens[i].valor_total);
    sgProdutos.Cells[ciBaseCalculoICMS, vLinha]                := NFormatN(FNotaItens[i].base_calculo_icms);
    sgProdutos.Cells[ciIndiceReducaoBaseCalculoICMS, vLinha]   := NFormatN(FNotaItens[i].indice_reducao_base_icms, 6);
    sgProdutos.Cells[ciPercentualICMS, vLinha]                 := NFormatN(FNotaItens[i].percentual_icms);
    sgProdutos.Cells[ciValorICMS, vLinha]                      := NFormatN(FNotaItens[i].valor_icms);
    sgProdutos.Cells[ciBaseCalculoICMSST, vLinha]              := NFormatN(FNotaItens[i].base_calculo_icms_st);
    sgProdutos.Cells[ciIndiceReducaoBaseCalculoICMSST, vLinha] := NFormatN(FNotaItens[i].indice_reducao_base_icms_st, 6);
    sgProdutos.Cells[ciValorICMSST, vLinha]                    := NFormatN(FNotaItens[i].valor_icms_st);
    sgProdutos.Cells[ciBaseCalculoPIS, vLinha]                 := NFormatN(FNotaItens[i].base_calculo_pis);
    sgProdutos.Cells[ciPercentualPIS, vLinha]                  := NFormatN(FNotaItens[i].percentual_pis);
    sgProdutos.Cells[ciValorPIS, vLinha]                       := NFormatN(FNotaItens[i].valor_pis);
    sgProdutos.Cells[ciBaseCalculoCOFINS, vLinha]              := NFormatN(FNotaItens[i].base_calculo_cofins);
    sgProdutos.Cells[ciPercentualCOFINS, vLinha]               := NFormatN(FNotaItens[i].percentual_cofins);
    sgProdutos.Cells[ciValorCOFINS, vLinha]                    := NFormatN(FNotaItens[i].valor_cofins);
    sgProdutos.Cells[ciPercentualIVA, vLinha]                  := NFormatN(FNotaItens[i].iva);
    sgProdutos.Cells[ciPrecoPauta, vLinha]                     := NFormatN(FNotaItens[i].preco_pauta);
    sgProdutos.Cells[ciPercentualIPI, vLinha]                  := NFormatN(FNotaItens[i].percentual_ipi);
    sgProdutos.Cells[ciValorIPI, vLinha]                       := NFormatN(FNotaItens[i].valor_ipi);
    sgProdutos.Cells[ciInformacoesAdicionais, vLinha]          := FNotaItens[i].informacoes_adicionais;

    Inc(vLinha);
  end;
  sgProdutos.RowCount := IfThen(vLinha > 1, vLinha, 2);
end;

procedure TFormRelacaoNotasFiscais.sgNotasFiscaisDblClick(Sender: TObject);
begin
  inherited;
  Informacoes.NotaFiscal.Informar(SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, sgNotasFiscais.Row]));
end;

procedure TFormRelacaoNotasFiscais.sgNotasFiscaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coSelecionado, coTipoNotaAnalitico, coTipoMovimentoAnalitico, coStatus] then
    vAlinhamento := taCenter
  else if ACol in[coDataCadastro, coRazaoSocial, coCFOP, coEmpresa] then
    vAlinhamento := taLeftJustify
  else
    vAlinhamento := taRightJustify;

  sgNotasFiscais.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoNotasFiscais.sgNotasFiscaisGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgNotasFiscais.Cells[coTipoLinha, ARow] = coLinhaTotal then begin
    AFont.Color := clBlack;
    AFont.Style := [fsBold];
    ABrush.Color := $000096DB;
  end
  else if ACol = coTipoNotaAnalitico then begin
    AFont.Style := [fsBold];
    AFont.Color := IfThen(sgNotasFiscais.Cells[ACol, ARow] = 'NF-e', clBlue, $000096DB);
  end
  else if ACol = coTipoMovimentoAnalitico then begin
    AFont.Style := [fsBold];
    AFont.Color := _NotasFiscais.getCorTipoMovimento(sgNotasFiscais.Cells[coTipoMovimento, ARow]);
  end
  else if ACol = coStatus then begin
    AFont.Style := [fsBold];
    if sgNotasFiscais.Cells[ACol, ARow] = 'Emitida' then
      AFont.Color := $000096DB
    else if sgNotasFiscais.Cells[ACol, ARow] = 'Cancelada' then
      AFont.Color := clRed
    else if sgNotasFiscais.Cells[ACol, ARow] = 'Denegada' then
      AFont.Color := clRed
    else
      AFont.Color := $000080FF;
  end;
end;

procedure TFormRelacaoNotasFiscais.sgNotasFiscaisGetCellPicture(Sender: TObject; ARow, ACol: Integer; var APicture: TPicture);
begin
  inherited;

  if ARow = 0 then
    Exit;

  if (ACol <> coSelecionado) or (sgNotasFiscais.Cells[ACol, ARow] = '') then
   Exit;

  if sgNotasFiscais.Cells[ACol, ARow] = charNaoSelecionado then
    APicture := _Imagens.FormImagens.im1.Picture
  else if sgNotasFiscais.Cells[ACol, ARow] = charSelecionado then
    APicture := _Imagens.FormImagens.im2.Picture;
end;

procedure TFormRelacaoNotasFiscais.sgNotasFiscaisKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  _Biblioteca.MarcarDesmarcarTodos(sgNotasFiscais, coSelecionado, Key, Shift);
end;

procedure TFormRelacaoNotasFiscais.sgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[ciNomeProduto, ciUnidade] then
    vAlinhamento := taLeftJustify
  else if ACol = ciCST then
    vAlinhamento := taCenter
  else
    vAlinhamento := taRightJustify;

  sgProdutos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoNotasFiscais.SpeedButton2Click(Sender: TObject);
var
  i: Integer;
  vNotasIds: TArray<Integer>;
  vRetorno: TRetornoTelaFinalizar<string>;
  vRetornoDadosEmitente: TRetornoTelaFinalizar<RecEmpresas>;
  empresaAtualNota: TArray<RecParametrosEmpresa>;
  empresaNovaNota: TArray<RecParametrosEmpresa>;
  vTeveEmitita: Boolean;
  vTeveRegimeDiferente: Boolean;
  empresas: TArray<RecEmpresas>;
  vEmpresaId: Integer;
  vRetornoUpdate: RecRetornoBD;
begin
  inherited;
  vTeveEmitita := False;
  vTeveRegimeDiferente := False;

  vRetornoDadosEmitente := BuscarDadosEmpresaEmissaoNota.BuscarDadosEmpresaEmitente;
  if vRetornoDadosEmitente.BuscaCancelada then
    Exit;

  empresas := _Empresas.BuscarEmpresas(Sessao.getConexaoBanco, 0, [vRetornoDadosEmitente.Dados.EmpresaId]);

  for i := sgNotasFiscais.FixedRows to sgNotasFiscais.RowCount -2 do begin
    if sgNotasFiscais.Cells[coNumeroNota, i] <> '' then begin
      vTeveEmitita := true;
      Continue;
    end;

    if sgNotasFiscais.Cells[coSelecionado, i] <> charSelecionado then
      Continue;

    vEmpresaId := StrToInt(sgNotasFiscais.Cells[coEmpresaId, i]);

    //Necess�rio verificar a mudan�a de regimes porque as vezes a nota tem ICMS e no novo emitente n�o vai ter
    empresaAtualNota := _ParametrosEmpresa.BuscarParametrosEmpresas(Sessao.getConexaoBanco, 0, [vEmpresaId]);
    empresaNovaNota := _ParametrosEmpresa.BuscarParametrosEmpresas(Sessao.getConexaoBanco, 0, [empresas[0].EmpresaId]);

    if (empresaAtualNota[0].RegimeTributario = 'LP') and (empresaNovaNota[0].RegimeTributario <>  empresaAtualNota[0].RegimeTributario) then begin
      vTeveRegimeDiferente := True;
      Continue;
    end;

    SetLength(vNotasIds, Length(vNotasIds) + 1);
    vNotasIds[High(vNotasIds)] := StrToInt(_Biblioteca.RetornaNumeros(sgNotasFiscais.Cells[coNotaFiscalId, i]));
  end;

  if vNotasIds = nil then begin
    _Biblioteca.Informar('N�o foi selecionado nenhuma nota permitida a altera��o!');
    Abort;
  end;

  if vTeveEmitita then
    _Biblioteca.Informar('Notas fiscais com n�mero gerados n�o v�o ter os dados do emitente alterado!');

  if vTeveRegimeDiferente then begin
    _Biblioteca.Informar(
      'ATEN��O! O regime tribut�rio de algumas empresas seleiconadas s�o diferente do regime da nova empresa.'+ #13#10 +
      'Antes de emitir as notas valide se a base e valor de icms est�o corretos.'
    );
  end;

  vRetornoUpdate := _NotasFiscais.AtualizarVariasNotasFiscais(
    Sessao.getConexaoBanco,
    empresas[0].EmpresaId,
    empresas[0].RazaoSocial,
    empresas[0].NomeFantasia,
    empresas[0].Cnpj,
    empresas[0].InscricaoEstadual,
    empresas[0].Logradouro,
    empresas[0].Complemento,
    empresas[0].NomeBairro,
    empresas[0].NomeCidade,
    empresas[0].Numero,
    empresas[0].EstadoId,
    empresas[0].Cep,
    empresas[0].TelefonePrincipal,
    empresas[0].CodigoIBGECidade,
    empresas[0].CodigoIBGEEstado,
    vNotasIds
  );

  if vRetornoUpdate.TeveErro then begin
    _Biblioteca.Exclamar(vRetornoUpdate.MensagemErro);
    Abort;
  end;

  _Biblioteca.Informar('Altera��o realizada com sucesso.');
  Carregar(Self);
end;

procedure TFormRelacaoNotasFiscais.SpeedButton3Click(Sender: TObject);
var
  vRetorno: TRetornoTelaFinalizar<RecClientes>;
  vRetBanco: RecRetornoBD;
  i: Integer;
  vNotasIds: TArray<Integer>;
  vTeveEmitita: Boolean;
  vTeveRegimeDiferente: Boolean;
  empresas: TArray<RecEmpresas>;
  vEmpresaId: Integer;
  vRetornoUpdate: RecRetornoBD;
begin
  inherited;

  for i := sgNotasFiscais.FixedRows to sgNotasFiscais.RowCount -2 do begin
    if sgNotasFiscais.Cells[coNumeroNota, i] <> '' then begin
      vTeveEmitita := true;
      Continue;
    end;

    if sgNotasFiscais.Cells[coSelecionado, i] <> charSelecionado then
      Continue;

    SetLength(vNotasIds, Length(vNotasIds) + 1);
    vNotasIds[High(vNotasIds)] := StrToInt(_Biblioteca.RetornaNumeros(sgNotasFiscais.Cells[coNotaFiscalId, i]));
  end;

  if vNotasIds = nil then begin
    _Biblioteca.Informar('N�o foi selecionado nenhuma nota permitida a altera��o!');
    Abort;
  end;

  if vTeveEmitita then
    _Biblioteca.Informar('Notas fiscais com n�mero gerados n�o v�o ter os dados do emitente alterado!');

  vRetorno := BuscarDadosClienteEmissaoNota.BuscarDadosCliente;
  if vRetorno.BuscaCancelada then
    Exit;

  vRetornoUpdate := _NotasFiscais.AtualizarDestinatarioNotasFiscais(
    Sessao.getConexaoBanco,
    vNotasIds,
    vRetorno.Dados.RecCadastro.nome_fantasia,
    vRetorno.Dados.RecCadastro.razao_social,
    VRetorno.Dados.RecCadastro.tipo_pessoa,
    vRetorno.Dados.RecCadastro.cpf_cnpj,
    vRetorno.Dados.RecCadastro.inscricao_estadual,
    vRetorno.Dados.RecCadastro.logradouro,
    vRetorno.Dados.RecCadastro.complemento,
    vRetorno.Dados.RecCadastro.NomeBairro,
    vRetorno.Dados.RecCadastro.NomeCidade,
    vRetorno.Dados.RecCadastro.numero,
    vRetorno.Dados.RecCadastro.estado_id,
    vRetorno.Dados.RecCadastro.cep,
    vRetorno.Dados.RecCadastro.CodigoIbgeCidade,
    vRetorno.Dados.RecCadastro.inscricao_estadual,
    vRetorno.Dados.RecCadastro.logradouro,
    vRetorno.Dados.RecCadastro.complemento,
    vRetorno.Dados.RecCadastro.NomeBairro,
    vRetorno.Dados.RecCadastro.NomeCidade,
    vRetorno.Dados.RecCadastro.numero,
    vRetorno.Dados.RecCadastro.estado_id,
    vRetorno.Dados.RecCadastro.cep,
    vRetorno.Dados.RecCadastro.CodigoIbgeCidade
  );

  if vRetornoUpdate.TeveErro then begin
    _Biblioteca.Exclamar(vRetornoUpdate.MensagemErro);
    Abort;
  end;

  _Biblioteca.Informar('Altera��o realizada com sucesso.');
  Carregar(Self);
end;

procedure TFormRelacaoNotasFiscais.SpeedButton4Click(Sender: TObject);
var
  vNotaFiscalId: Integer;
begin
  inherited;
  vNotaFiscalId := SFormatInt(sgNotasFiscais.Cells[coNotaFiscalId, sgNotasFiscais.Row]);
  if vNotaFiscalId = 0 then
    Exit;

  if sgNotasFiscais.Cells[coStatus, sgNotasFiscais.Row] <> 'Emitida' then begin
    if
      (sgNotasFiscais.Cells[coTipoNotaAnalitico, sgNotasFiscais.Row] = 'NF-e') and
      (
        (sgNotasFiscais.Cells[coTipoMovimento, sgNotasFiscais.Row] = 'OUT') or
        (sgNotasFiscais.Cells[coTipoMovimento, sgNotasFiscais.Row] = 'DEF')
      )
    then begin
      if Perguntar('Deseja imprimir um espelho da nota sem valor fiscal?') then
        if _ImpressaoDANFE.Imprimir(vNotaFiscalId) then
          Carregar(Sender);
    end
    else
      Exclamar('N�o � permitido imprimir espelho desse tipo de nota!');

    Exit;
  end
  else
    Exclamar('� poss�vel somente fazer a impress�o do espelho de uma nota fiscal eletr�nica n�o emitida!');
end;

procedure TFormRelacaoNotasFiscais.VerificarRegistro(Sender: TObject);
begin
  inherited;
//  if gbFormasPagamento.NenhumMarcado then begin
//    _Biblioteca.Exclamar('� neces�rio informar ao menos 1 forma de pagamento!');
//    SetarFoco(gbFormasPagamento);
//    Abort;
//  end;

  if gbClasseFiscal.NenhumMarcado then begin
    Exclamar('� necess�rio informar ao menos uma classe fiscal!');
    SetarFoco(gbClasseFiscal);
    Abort;
  end;
end;

end.
