unit Pesquisa.GerenciadoresCartoesTEF;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Biblioteca, _Sessao,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _GerenciadorCartaoTEF, _RecordsCadastros,
  System.Math;

type
  TFormPesquisaGerenciadoresCartoesTEF = class(TFormHerancaPesquisas)
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
  protected
    procedure BuscarRegistros; override;
  end;

function Pesquisar(): TObject;

implementation

{$R *.dfm}

{ TFormPesquisaGerenciadoresCartoesTEF }

const
  cp_nome  = 2;
  cp_ativo = 3;

function Pesquisar(): TObject;
var
  obj: TObject;
begin
  obj := _HerancaPesquisas.Pesquisar(TFormPesquisaGerenciadoresCartoesTEF, _GerenciadorCartaoTEF.GetFiltros);
  if obj = nil then
    Result := nil
  else
    Result := RecGerenciadorCartaoTEF(obj);
end;

procedure TFormPesquisaGerenciadoresCartoesTEF.BuscarRegistros;
var
  i: Integer;
  vGerenciadores: TArray<RecGerenciadorCartaoTEF>;
begin
  inherited;

  vGerenciadores :=
    _GerenciadorCartaoTEF.BuscarGerenciadorCartaoTEF(
      Sessao.getConexaoBanco,
      cbOpcoesPesquisa.GetIndice,
      [eValorPesquisa.Text]
    );

  if vGerenciadores = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  FDados := TArray<TObject>(vGerenciadores);

  for i := Low(vGerenciadores) to High(vGerenciadores) do begin
    sgPesquisa.Cells[coSelecionado, i + 1] := 'N�o';
    sgPesquisa.Cells[coCodigo, i + 1]      := NFormat(vGerenciadores[i].gerenciador_cartao_id);
    sgPesquisa.Cells[cp_nome, i + 1]        := vGerenciadores[i].nome;
    sgPesquisa.Cells[cp_ativo, i + 1]       := SimNao(vGerenciadores[i].ativo);
  end;

  sgPesquisa.RowCount := IfThen(Length(vGerenciadores) = 1, 2, High(vGerenciadores) + 2);
  sgPesquisa.SetFocus;
end;

procedure TFormPesquisaGerenciadoresCartoesTEF.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol = cp_ativo then
    vAlinhamento := taCenter
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

end.
