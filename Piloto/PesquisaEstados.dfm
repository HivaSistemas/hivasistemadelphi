inherited FormPesquisaEstados: TFormPesquisaEstados
  Caption = 'Pesquisa de estados'
  ClientHeight = 245
  ClientWidth = 537
  ExplicitWidth = 545
  ExplicitHeight = 276
  PixelsPerInch = 96
  TextHeight = 14
  inherited sgPesquisa: TGridLuka
    Width = 537
    Height = 199
    ColCount = 4
    OnDrawCell = sgPesquisaDrawCell
    CorLinhaFoco = 38619
    HCol.Strings = (
      'Sel?'
      'Estado'
      'Nome'
      'Ativo')
    RealColCount = 4
    ExplicitWidth = 537
    ExplicitHeight = 199
    ColWidths = (
      29
      46
      266
      64)
  end
  inherited eQuantidadeDigitosPesquisa: TEdit
    Left = 8
    Top = 216
    Width = 37
    Text = '0'
    ExplicitLeft = 8
    ExplicitTop = 216
    ExplicitWidth = 37
  end
  inherited pnFiltro1: TPanel
    Width = 537
    ExplicitWidth = 537
    inherited eValorPesquisa: TEditLuka
      Width = 328
      ExplicitWidth = 328
    end
  end
end
