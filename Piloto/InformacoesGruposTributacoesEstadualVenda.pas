unit InformacoesGruposTributacoesEstadualVenda;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Menus, _FrameHerancaPrincipal, FrameICMS, Vcl.StdCtrls, CheckBoxLuka, EditLuka;

type
  TFormInformacoesGruposTributacoesEstadualVenda = class(TFormHerancaFinalizar)
    Label1: TLabel;
    eID: TEditLuka;
    ckAtivo: TCheckBoxLuka;
    lb1: TLabel;
    eDescricao: TEditLuka;
    FrICMS: TFrICMS;
  end;

procedure Informar(pGrupoTribEstadualVendaId: Integer);

implementation

{$R *.dfm}

uses _GruposTribEstadualVenda, _Biblioteca, _Sessao, _GruposTribEstadualVendaEst;

procedure Informar(pGrupoTribEstadualVendaId: Integer);
var
  vGrupo: TArray<RecGruposTribEstadualVenda>;
  vForm: TFormInformacoesGruposTributacoesEstadualVenda;
begin
  if pGrupoTribEstadualVendaId = 0 then
    Exit;

  vGrupo := _GruposTribEstadualVenda.BuscarGruposTribEstadualVenda(Sessao.getConexaoBanco, 0, [pGrupoTribEstadualVendaId]);
  if vGrupo = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vForm := TFormInformacoesGruposTributacoesEstadualVenda.Create(nil);

  vForm.eID.SetInformacao(vGrupo[0].GrupoTribEstadualVendaId);
  vForm.eDescricao.SetInformacao(vGrupo[0].Descricao);
  vForm.ckAtivo.CheckedStr := vGrupo[0].Ativo;
  vForm.FrICMS.ICMS        := _GruposTribEstadualVendaEst.BuscarGruposTribEstadualVendaEst(Sessao.getConexaoBanco, 0, [vGrupo[0].GrupoTribEstadualVendaId]);
  vForm.FrICMS.SomenteLeitura(True);  
  
  vForm.ShowModal;
  vForm.Free;  
end;

end.
