unit FrameProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Produtos, PesquisaProdutos, _RecordsEspeciais, System.Math,
  _Sessao, Vcl.Buttons, Vcl.Menus;

type
  TFrProdutos = class(TFrameHenrancaPesquisas)
    ckFiltroExtra: TCheckBox;
  public
    function getProduto(pLinha: Integer = -1): RecProdutos;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function AdicionarPesquisandoTodos: TArray<TObject>; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrProdutos }

function TFrProdutos.AdicionarDireto: TObject;
var
    produtos: TArray<RecProdutos>;
  begin
    produtos := _Produtos.BuscarProdutos(Sessao.getConexaoBanco, 0, [FChaveDigitada], ckSomenteAtivos.Checked, '');
    if produtos = nil then
      Result := nil
    else
      Result := produtos[0];
  end;

  function TFrProdutos.AdicionarPesquisando: TObject;
  begin
    Result := PesquisaProdutos.PesquisarProduto(ckFiltroExtra.Checked, ckSomenteAtivos.Checked);
  end;

  function TFrProdutos.AdicionarPesquisandoTodos: TArray<TObject>;
begin
  Result := PesquisaProdutos.PesquisarVarios(ckFiltroExtra.Checked, ckSomenteAtivos.Checked);
end;

function TFrProdutos.getProduto(pLinha: Integer): RecProdutos;
  begin
    if FDados.Count = 0 then
      Result := nil
    else
      Result := RecProdutos(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
  end;

  function TFrProdutos.IndiceChave(pSender: TObject): Integer;
  var
    i: Integer;
  begin
    Result := -1;
    for i := 0 to FDados.Count - 1 do begin
      if RecProdutos(FDados[i]).produto_id = RecProdutos(pSender).produto_id then begin
        Result := i;
        Break;
      end;
    end;
  end;

  procedure TFrProdutos.MontarGrid;
  var
    i: Integer;
    pSender: RecProdutos;
  begin
    with sgPesquisa do begin
      ClearGrid(1);
      for i := 0 to FDados.Count - 1 do begin
        pSender := RecProdutos(FDados[i]);
        AAdd([IntToStr(pSender.produto_id), pSender.nome]);
      end;
    end;
  end;

  end.
