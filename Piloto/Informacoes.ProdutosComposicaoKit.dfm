inherited FormInformacoesComposicaoKit: TFormInformacoesComposicaoKit
  Caption = 'Inf. dos produtos que comp'#245'e o kit'
  ClientHeight = 233
  ClientWidth = 395
  ExplicitWidth = 401
  ExplicitHeight = 262
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 196
    Width = 395
    ExplicitWidth = 395
  end
  object sgProdutos: TGridLuka
    Left = 3
    Top = 17
    Width = 390
    Height = 176
    ColCount = 3
    DefaultRowHeight = 19
    DrawingStyle = gdsClassic
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 1
    OnDrawCell = sgProdutosDrawCell
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Produto'
      'Nome'
      'Quantidade')
    Grid3D = False
    RealColCount = 3
    AtivarPopUpSelecao = False
    ColWidths = (
      58
      234
      79)
  end
  object st3: TStaticText
    Left = 3
    Top = 2
    Width = 70
    Height = 16
    Alignment = taRightJustify
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = 'Produto '
    Color = 15395562
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 2
    Transparent = False
  end
  object stProduto: TStaticText
    Left = 72
    Top = 2
    Width = 321
    Height = 16
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = ' 158 - CONTROLE KIT'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    Transparent = False
  end
end
