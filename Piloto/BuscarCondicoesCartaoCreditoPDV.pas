unit BuscarCondicoesCartaoCreditoPDV;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka, _Biblioteca,
  Vcl.Buttons, Vcl.ExtCtrls, _Sessao, _CondicoesCartaoCreditoPDV, _RecordsOrcamentosVendas;

type
  RecRespostaCondicoesCartaoPDV = record
    Acrescimo: Double;
    ValorTotal: Double;
    CondicaoId: Integer;
    QtdeParcelas: Integer;
  end;

  TFormBuscarCondicoesCartaoCreditoPDV = class(TFormHerancaFinalizar)
    sgCondicoes: TGridLuka;
    procedure sgCondicoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgCondicoesDblClick(Sender: TObject);
    procedure sgCondicoesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FProporcao: Double;
    FItens: TArray<RecOrcamentoItens>;

    procedure IniciarCondicoes(pCondicoes: TArray<RecCondicoesCartaoCreditoPDV>);
  end;

function BuscarCondicaoPDV(pValor: Double; pValorTotalReceber: Double; pItens: TArray<RecOrcamentoItens>): TRetornoTelaFinalizar<RecRespostaCondicoesCartaoPDV>;

implementation

{$R *.dfm}

const
  coDescricao  = 0;
  coParcela    = 1;
  coCondicaoId = 2;
  coValorTotal = 3;
  coQtdeParcelas = 4;

function BuscarCondicaoPDV(pValor: Double; pValorTotalReceber: Double; pItens: TArray<RecOrcamentoItens>): TRetornoTelaFinalizar<RecRespostaCondicoesCartaoPDV>;
var
  vForm: TFormBuscarCondicoesCartaoCreditoPDV;
  vCondicoes: TArray<RecCondicoesCartaoCreditoPDV>;
begin
  vCondicoes := _CondicoesCartaoCreditoPDV.BuscarCondicoesCartaoCreditoPDV(Sessao.getConexaoBanco, 0, [Sessao.getEmpresaLogada.EmpresaId]);
  if vCondicoes = nil then begin
    _Biblioteca.Exclamar('As condi��es de pagamento para cart�es de cr�dito no PDV n�o foram definidas, por favor fa�a a devida parametriza��o!');
    Exit;
  end;

  vForm := TFormBuscarCondicoesCartaoCreditoPDV.Create(nil);

  vForm.FProporcao := pValor / pValorTotalReceber;
  vForm.FItens := pItens;
  vForm.IniciarCondicoes(vCondicoes);

  if Result.Ok(vForm.ShowModal) then begin
    Result.Dados.CondicaoId   := SFormatInt( vForm.sgCondicoes.Cells[coCondicaoId, vForm.sgCondicoes.Row] );
    Result.Dados.ValorTotal   := SFormatDouble( vForm.sgCondicoes.Cells[coValorTotal, vForm.sgCondicoes.Row] );
    Result.Dados.Acrescimo    := Result.Dados.ValorTotal - pValor;
    Result.Dados.QtdeParcelas := SFormatInt( vForm.sgCondicoes.Cells[coQtdeParcelas, vForm.sgCondicoes.Row] );
  end;
end;

{ TFormBuscarCondicoesCartaoCreditoPDV }

procedure TFormBuscarCondicoesCartaoCreditoPDV.IniciarCondicoes(pCondicoes: TArray<RecCondicoesCartaoCreditoPDV>);
var
  i: Integer;
  j: Integer;

  vPrecoUnitario: Double;
  vValorParcelamento: Double;
begin
  for i := Low(pCondicoes) to High(pCondicoes) do begin
    vValorParcelamento := 0;
    for j := Low(FItens) to High(FItens) do begin
      if pCondicoes[i].PermitirPrecoPromocional = 'S' then
        vPrecoUnitario := _Biblioteca.Arredondar(FItens[j].PrecoPromocional * IIfDbl(pCondicoes[i].AplicarIndPrecoPromocional = 'S', pCondicoes[i].IndiceAcrescimo), 2)
      else
        vPrecoUnitario := _Biblioteca.Arredondar(FItens[j].preco_varejo * pCondicoes[i].IndiceAcrescimo, 2);

      vValorParcelamento := vValorParcelamento + Arredondar(vPrecoUnitario * FItens[j].quantidade, 2);
    end;

    vValorParcelamento := Arredondar(vValorParcelamento * FProporcao / pCondicoes[i].QuantidadeParcelas, 2);

    sgCondicoes.Cells[coDescricao, i]  := pCondicoes[i].NomeCondicao;
    sgCondicoes.Cells[coParcela, i]    := NFormat(pCondicoes[i].QuantidadeParcelas) + ' x R$ ' + NFormat( vValorParcelamento );
    sgCondicoes.Cells[coCondicaoId, i] := NFormat(pCondicoes[i].CondicaoId);
    sgCondicoes.Cells[coValorTotal, i] := NFormat( vValorParcelamento * pCondicoes[i].QuantidadeParcelas );
    sgCondicoes.Cells[coQtdeParcelas, i] := NFormat( pCondicoes[i].QuantidadeParcelas );
  end;
  sgCondicoes.SetLinhasGridPorTamanhoVetor( Length(pCondicoes) -1 );
end;

procedure TFormBuscarCondicoesCartaoCreditoPDV.sgCondicoesDblClick(Sender: TObject);
begin
  inherited;
  sbFinalizarClick(Sender);
end;

procedure TFormBuscarCondicoesCartaoCreditoPDV.sgCondicoesDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  inherited;
  sgCondicoes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], taLeftJustify, Rect);
end;

procedure TFormBuscarCondicoesCartaoCreditoPDV.sgCondicoesKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
    Finalizar(Sender);
end;

end.
