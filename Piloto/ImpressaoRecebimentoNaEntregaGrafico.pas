unit ImpressaoRecebimentoNaEntregaGrafico;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficos, QRCtrls, _Orcamentos, _OrcamentosItens,
  QRExport, QRPDFFilt, QRWebFilt, QuickRpt, Vcl.ExtCtrls, _Sessao, _Biblioteca, _RecordsOrcamentosVendas;

type
  TFormImpressaoRecebimentoNaEntregaGrafico = class(TFormHerancaRelatoriosGraficos)
    qrlClienteNF: TQRLabel;
    qrl4: TQRLabel;
    qrlCondicaoPagamentoNF: TQRLabel;
    qrl5: TQRLabel;
    qrl6: TQRLabel;
    qrl7: TQRLabel;
    qrl9: TQRLabel;
    qrlTotalSerPagoNF: TQRLabel;
    qrlTotalDescontoNF: TQRLabel;
    qrlTotalOutrasDespesasNF: TQRLabel;
    qrlTotalProdutosNF: TQRLabel;
    qrl8: TQRLabel;
    qrlNFValorFrete: TQRLabel;
    qrl1: TQRLabel;
    ColumnHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    qrl11: TQRLabel;
    qrlNFNomeProduto: TQRLabel;
    qrlNFQuantidade: TQRLabel;
    qrlCabProdutoId: TQRLabel;
    qrl12: TQRLabel;
    qrlNFMarca: TQRLabel;
    qrl15: TQRLabel;
    qrlNFUnidade: TQRLabel;
    qrl16: TQRLabel;
    qrlNFPrecoUnitario: TQRLabel;
    qrl17: TQRLabel;
    qrlNFValorTotal: TQRLabel;
    qrNFmeFormasPagto: TQRMemo;
    qrl18: TQRLabel;
    QRLabel2: TQRLabel;
    qrlOrcamentoIdNF: TQRLabel;
    QRLabel4: TQRLabel;
    qrlVendedorNF: TQRLabel;
    procedure qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    FPosicItens: Integer;
    FItens: TArray<RecOrcamentoItens>;
  end;

procedure Imprimir(pOrcamentoId: Integer);

implementation

{$R *.dfm}

procedure Imprimir(pOrcamentoId: Integer);
var
  vImpressora: RecImpressora;
  vOrcamento: TArray<RecOrcamentos>;
  vForm: TFormImpressaoRecebimentoNaEntregaGrafico;

  procedure AddFormaPagamento(pForma: string; pValor: Double);
  begin
    if pValor > 0 then
      vForm.qrNFmeFormasPagto.Lines.Add( _Biblioteca.RPad(pForma, 17, '.') + '.: R$ ' + NFormat(pValor) );
  end;

begin
  if pOrcamentoId = 0 then
    Exit;

  vOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 0, [pOrcamentoId]);
  if vOrcamento = nil then begin
    NenhumRegistro;
    Exit;
  end;

  vImpressora := Sessao.getImpressora( toComprovantePagamento );
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;

  vForm := TFormImpressaoRecebimentoNaEntregaGrafico.Create(Application, vImpressora);

  vForm.FItens := _OrcamentosItens.BuscarOrcamentosItens(Sessao.getConexaoBanco, 0, [pOrcamentoId]);
  vForm.PreencherCabecalho(Sessao.getEmpresaLogada.EmpresaId);

  vForm.qrlOrcamentoIdNF.Caption         := NFormat(vOrcamento[0].orcamento_id);
  vForm.qrlVendedorNF.Caption            := NFormat(vOrcamento[0].vendedor_id) + ' - ' + vOrcamento[0].nome_vendedor;
  vForm.qrlCondicaoPagamentoNF.Caption   := NFormat(vOrcamento[0].condicao_id) + ' - ' + vOrcamento[0].nome_condicao_pagto;
  vForm.qrlClienteNF.Caption             := NFormat(vOrcamento[0].cliente_id) + ' - ' + vOrcamento[0].nome_cliente;
  vForm.qrlTotalProdutosNF.Caption       := NFormat(vOrcamento[0].valor_total_produtos);
  vForm.qrlTotalOutrasDespesasNF.Caption := NFormat(vOrcamento[0].valor_outras_despesas);
  vForm.qrlNFValorFrete.Caption          := NFormat(vOrcamento[0].ValorFrete + vOrcamento[0].ValorFreteItens);
  vForm.qrlTotalDescontoNF.Caption       := NFormat(vOrcamento[0].valor_desconto);
  vForm.qrlTotalSerPagoNF.Caption        := NFormat(vOrcamento[0].valor_total);

  vForm.qrNFmeFormasPagto.Lines.Clear;
  AddFormaPagamento('Dinheiro', vOrcamento[0].valor_dinheiro);
  AddFormaPagamento('Cart�o cr�dito', vOrcamento[0].ValorCartaoCredito);
  AddFormaPagamento('Cart�o d�bito', vOrcamento[0].ValorCartaoDebito);
  AddFormaPagamento('Cobran�a', vOrcamento[0].Valor_Cobranca);
  AddFormaPagamento('Cheque', vOrcamento[0].valor_cheque);

  vForm.qrRelatorioNF.Height :=
    vForm.qrBandTitulo.Height + vForm.ColumnHeaderBand1.Height + (vForm.DetailBand1.Height * Length(vForm.FItens)) + vForm.PageFooterBand1.Height + 10;

  vForm.Imprimir;

  vForm.Free;
end;

procedure TFormImpressaoRecebimentoNaEntregaGrafico.DetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  qrlNFNomeProduto.Caption    := _Biblioteca.NFormat(FItens[FPosicItens].produto_id) + ' - ' + FItens[FPosicItens].nome;
  if Length(qrlNFNomeProduto.Caption) > 38 then
    qrlNFNomeProduto.Font.Size := 6
  else
    qrlNFNomeProduto.Font.Size := 7;

  qrlNFMarca.Caption          := FItens[FPosicItens].nome_marca;

  qrlNFQuantidade.Caption     := _Biblioteca.NFormatEstoque(FItens[FPosicItens].Quantidade);
  qrlNFUnidade.Caption        := FItens[FPosicItens].unidade_venda;

  qrlNFPrecoUnitario.Caption := NFormat(FItens[FPosicItens].preco_unitario);
  qrlNFValorTotal.Caption    := NFormat(FItens[FPosicItens].valor_total);
end;

procedure TFormImpressaoRecebimentoNaEntregaGrafico.qrRelatorioNFBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  inherited;
  FPosicItens := -1;
end;

procedure TFormImpressaoRecebimentoNaEntregaGrafico.qrRelatorioNFNeedData(Sender: TObject; var MoreData: Boolean);
begin
  inherited;
  Inc(FPosicItens);
  MoreData := FPosicItens < Length(FItens);
end;

end.
