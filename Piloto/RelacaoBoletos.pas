unit RelacaoBoletos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.ComCtrls, Vcl.StdCtrls, CheckBoxLuka, Vcl.Buttons, Vcl.ExtCtrls,Vcl.Printers,
  FrameTextos, Frame.Inteiros, FramePortadores, Frame.Cadastros, Vcl.FileCtrl,
  FrameDataInicialFinal, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameEmpresas, Vcl.Grids, GridLuka, FrameGruposClientes, ComboBoxLuka;

type
  TFormRelacaoBoletos = class(TFormHerancaRelatoriosPageControl)
    sgTitulos: TGridLuka;
    FrClientes: TFrameCadastros;
    FrPortadores: TFrPortadores;
    FrCodigoTitulo: TFrameInteiros;
    FrDocumento: TFrTextos;
    FrGruposClientes: TFrGruposClientes;
    FrDataCadastro: TFrDataInicialFinal;
    ckSalvarPDF: TCheckBox;
    edtCaminhoPDF: TEdit;
    sbPesquisa: TSpeedButton;
    cbxImpressoras: TComboBoxLuka;
    Label1: TLabel;
    ckNovoNossoNumero: TCheckBox;
    lb2: TLabel;
    cbSituacaoBoleto: TComboBoxLuka;
    ckEnviarEmail: TCheckBox;
    FrDataVencimento: TFrDataInicialFinal;
    procedure sgTitulosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sgTitulosGetCellPicture(Sender: TObject; ARow, ACol: Integer;
      var APicture: TPicture);
    procedure sgTitulosDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure sbPesquisaClick(Sender: TObject);
    procedure ckSalvarPDFClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure VerificarRegistro(Sender: TObject); override;
    procedure Imprimir(Sender: TObject); override;
    procedure Carregar(Sender: TObject); override;
  end;

var
  FormRelacaoBoletos: TFormRelacaoBoletos;

implementation

{$R *.dfm}

uses _Biblioteca, _RecordsRelatorios, _RelacaoContasReceber, _Sessao, _Imagens,
  _EmitirBoletos;

const
(* Grid t�tulos *)
  tiSelecionado     = 0;
  tiReceberId       = 1;
//  tiDocumento       = 2;
  tiCliente         = 2;
  tiParcela         = 3;
  tiValorDocumento  = 4;
  tiDataVencimento  = 5;
  tiTipoCobranca    = 6;
  tiPortador        = 7;


procedure TFormRelacaoBoletos.Carregar(Sender: TObject);
var
  vComando: string;
  FTitulos: TArray<_RecordsRelatorios.RecContasReceber>;
  vLinha, i: Integer;
begin
  inherited;
  vComando := '';

  WhereOuAnd(vComando,' CON.EMPRESA_ID = ' + IntToStr( Sessao.getParametrosEmpresa.EmpresaId ));

  if not FrClientes.EstaVazio then
    WhereOuAnd(vComando, FrClientes.getSqlFiltros('CON.CADASTRO_ID'));

  if not FrDataCadastro.EstaVazio then
    WhereOuAnd(vComando, FrDataCadastro.getSqlFiltros('CON.DATA_CADASTRO'));

  if not FrDataVencimento.EstaVazio then
    WhereOuAnd(vComando, FrDataVencimento.getSqlFiltros('CON.DATA_VENCIMENTO'));

  if not FrPortadores.EstaVazio then
    WhereOuAnd(vComando, FrPortadores.getSqlFiltros('CON.PORTADOR_ID'));

  if not FrCodigoTitulo.EstaVazio then
    WhereOuAnd(vComando, FrCodigoTitulo.getSqlFiltros('CON.RECEBER_ID'));

  if not FrDocumento.EstaVazio then
    WhereOuAnd(vComando, FrDocumento.TrazerFiltros('CON.DOCUMENTO'));

  WhereOuAnd(vComando, ' TCO.FORMA_PAGAMENTO = ''COB'' ');

  if cbSituacaoBoleto.GetValor <> 'NaoFiltrar' then
  begin
    if cbSituacaoBoleto.GetValor = 'NaoGerado' then
      WhereOuAnd(vComando,' CON.EMITIU_BOLETO_DUPLICATA = ''N'' ')
    else
      WhereOuAnd(vComando,' CON.EMITIU_BOLETO_DUPLICATA = ''S'' ');
  end;

  FrGruposClientes.getSqlFiltros('CLI.CLIENTE_GRUPO_ID', vComando, True);

  vComando := vComando + ' order by ';
  vComando := vComando + ' CON.RECEBER_ID, ';
  vComando := vComando + ' CON.CADASTRO_ID, ';
  vComando := vComando + ' CAD.RAZAO_SOCIAL ';

  FTitulos := _RelacaoContasReceber.BuscarContasReceberComando(
    Sessao.getConexaoBanco,
    vComando
  );
  if FTitulos = nil then begin
    _Biblioteca.NenhumRegistro;
    Exit;
  end;

  vLinha := 0;
  for i := Low(FTitulos) to High(FTitulos) do begin

    Inc(vLinha);
    sgTitulos.Cells[tiSelecionado, vLinha]     := charNaoSelecionado;
    sgTitulos.Cells[tiReceberId, vLinha]       := NFormat(FTitulos[i].receber_id);
//    sgTitulos.Cells[tiDocumento, vLinha]       := FTitulos[i].documento;
    sgTitulos.Cells[tiCliente, vLinha]         := getInformacao(FTitulos[i].cliente_id, FTitulos[i].nome_cliente);
    sgTitulos.Cells[tiParcela, vLinha]         := NFormat(FTitulos[i].parcela) + '/' + NFormat(FTitulos[i].numero_parcelas);
    sgTitulos.Cells[tiValorDocumento, vLinha]  := NFormatN(FTitulos[i].valor_documento);
    sgTitulos.Cells[tiDataVencimento, vLinha]  := DFormat(FTitulos[i].data_vencimento);
    sgTitulos.Cells[tiTipoCobranca, vLinha]    := NFormat(FTitulos[i].cobranca_id) + ' - ' + FTitulos[i].nome_tipo_cobranca;
    sgTitulos.Cells[tiPortador, vLinha]        := FTitulos[i].PortadorId + ' - ' + FTitulos[i].NomePortador;
  end;
  sgTitulos.SetLinhasGridPorTamanhoVetor(vLinha);
  SetarFoco(sgTitulos);
end;

procedure TFormRelacaoBoletos.ckSalvarPDFClick(Sender: TObject);
begin
  inherited;
  edtCaminhoPDF.Enabled := ckSalvarPDF.Checked;
  sbPesquisa.Enabled := ckSalvarPDF.Checked;
end;

procedure TFormRelacaoBoletos.FormCreate(Sender: TObject);
begin
  inherited;
  FrDataCadastro.eDataInicial.AsData := now;
  FrDataCadastro.eDataFinal.AsData := now;

  cbxImpressoras.Items := Printer.Printers;
  cbxImpressoras.Items.Insert(0,'<Impressoras>');
  cbxImpressoras.ItemIndex := 0;

end;

procedure TFormRelacaoBoletos.Imprimir(Sender: TObject);
var
  vBoletos: TBoletosACBr;
  vParametrosImpressao: TParametrosImpressao;
  vLinha: Integer;
begin
  inherited;
  if sgTitulos.Localizar([tiSelecionado],[charSelecionado]) = -1 then begin
    Exclamar('� necess�rio selecionar pelo menos um titulo!');
    Abort;
  end;

  if ckSalvarPDF.Checked then
  begin
    if not DirectoryExists(edtCaminhoPDF.Text) then
    begin
      Exclamar('Diret�rio informado para PDF n�o existe.');
      Abort;
    end;
  end;


  vBoletos := TBoletosACBr.Create;
  vParametrosImpressao:= TParametrosImpressao.Create;
  try
    for vLinha := 1 to sgTitulos.RowCount -1 do
    begin
      if sgTitulos.Cells[tiSelecionado,vLinha] = charSelecionado then
        vParametrosImpressao.AdicionarContasReceber(RetornaNumeros(sgTitulos.Cells[tiReceberId, vLinha]).ToInteger);
    end;

    vParametrosImpressao.ItensImpressao.CaminhoBoletoPDF := IIfStr(ckSalvarPDF.Checked, edtCaminhoPDF.Text,'');
    vParametrosImpressao.ItensImpressao.Impressora := IIfStr(cbxImpressoras.ItemIndex > 0,cbxImpressoras.Text,'');
    vParametrosImpressao.ItensImpressao.GerarNovoNossoNumero := ckNovoNossoNumero.Checked;
    vParametrosImpressao.ItensImpressao.EnviarBoletoEmail := ckEnviarEmail.Checked;
    vBoletos.emitirBoletas(vParametrosImpressao);
  finally
    vParametrosImpressao.Free;
    vBoletos.Free;
  end;

  pcDados.ActivePage := tsFiltros;
  sgTitulos.ClearGrid();

end;

procedure TFormRelacaoBoletos.sbPesquisaClick(Sender: TObject);
var
  vDir: string;
begin
  vDir := edtCaminhoPDF.Text;
  if  SelectDirectory('Selecione o diret�rio', '', vDir) then
    edtCaminhoPDF.Text := vDir;
end;

procedure TFormRelacaoBoletos.sgTitulosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  alinhamento: TAlignment;
begin
  inherited;
  if ACol in[tiReceberId, tiValorDocumento, tiParcela] then
    alinhamento := taRightJustify
  else
    alinhamento := taLeftJustify;

  sgTitulos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], alinhamento, Rect);
end;

procedure TFormRelacaoBoletos.sgTitulosGetCellPicture(Sender: TObject; ARow,
  ACol: Integer; var APicture: TPicture);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgTitulos.Cells[ACol, ARow] = '' then
   Exit;

  if ACol = tiSelecionado then begin
    if sgTitulos.Cells[ACol, ARow] = charNaoSelecionado then
      APicture := _Imagens.FormImagens.im1.Picture
    else if sgTitulos.Cells[ACol, ARow] = charSelecionado then
      APicture := _Imagens.FormImagens.im2.Picture;
  end;
end;

procedure TFormRelacaoBoletos.sgTitulosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  MarcarDesmarcarTodos(sgTitulos, tiSelecionado, Key, Shift);
end;

procedure TFormRelacaoBoletos.VerificarRegistro(Sender: TObject);
begin
  inherited;
  if (ckNovoNossoNumero.Checked) and (not _Sessao.Sessao.AutorizadoRotina('gerar_novo_nosso_numero')) then begin
    _Biblioteca.Exclamar('Voc� n�o possui autoriza��o para gerar novo nosso n�mero!');
    SetarFoco(ckNovoNossoNumero);
    Abort;
  end;
end;

end.
