inherited FormBuscarProdutosZerarEstoque: TFormBuscarProdutosZerarEstoque
  Caption = 'Buscar produtos que n'#227'o tiveram ajuste de estoque no per'#237'odo'
  ClientHeight = 463
  ClientWidth = 990
  ExplicitWidth = 996
  ExplicitHeight = 492
  PixelsPerInch = 96
  TextHeight = 14
  object sbZerarEstoque: TSpeedButton [0]
    Left = 608
    Top = 61
    Width = 200
    Height = 38
    Caption = 'Carregar estoque'
    Flat = True
    NumGlyphs = 2
    OnClick = sbZerarEstoqueClick
  end
  object Label1: TLabel [1]
    Left = 196
    Top = 120
    Width = 556
    Height = 14
    Caption = 
      'Produtos que controlam lote e com bloqueio de estoque n'#227'o ser'#227'o ' +
      'ajustados por esse procedimento!'
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
  end
  inherited pnOpcoes: TPanel
    Top = 426
    Width = 990
    TabOrder = 3
    ExplicitTop = 426
    ExplicitWidth = 990
  end
  object sgProdutos: TGridLuka
    Left = 0
    Top = 135
    Width = 990
    Height = 291
    Align = alBottom
    ColCount = 11
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    TabOrder = 4
    OnDrawCell = sgProdutosDrawCell
    OnKeyDown = sgProdutosKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 38619
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clActiveCaption
    HCol.Strings = (
      'Sel.'
      'Produto'
      'Nome'
      'Und.'
      'Marca'
      'Pre'#231'o unit.'
      'Local'
      'Quantidade'
      'Lote'
      'Dt. Fabrica'#231#227'o'
      'Dt. Vencimento')
    OnGetCellPicture = sgProdutosGetCellPicture
    Grid3D = False
    RealColCount = 11
    Indicador = True
    AtivarPopUpSelecao = False
    ExplicitTop = 134
    ColWidths = (
      32
      53
      213
      32
      132
      73
      126
      71
      87
      87
      87)
  end
  inline FrLocalProduto: TFrLocais
    Left = 342
    Top = 3
    Width = 252
    Height = 114
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 342
    ExplicitTop = 3
    ExplicitWidth = 252
    ExplicitHeight = 114
    inherited sgPesquisa: TGridLuka
      Width = 227
      Height = 97
      TabOrder = 5
      ExplicitWidth = 227
      ExplicitHeight = 97
    end
    inherited CkMultiSelecao: TCheckBox
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    inherited PnTitulos: TPanel
      Width = 252
      TabOrder = 4
      ExplicitWidth = 252
      inherited lbNomePesquisa: TLabel
        Width = 28
        Caption = 'Local'
        ExplicitWidth = 28
        ExplicitHeight = 14
      end
      inherited CkChaveUnica: TCheckBox
        Checked = False
        State = cbUnchecked
      end
      inherited pnSuprimir: TPanel
        Left = 147
        ExplicitLeft = 147
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 227
      Height = 98
      ExplicitLeft = 227
      ExplicitHeight = 98
    end
  end
  inline FrProduto: TFrProdutos
    Left = 2
    Top = 2
    Width = 331
    Height = 115
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 0
    TabStop = True
    ExplicitLeft = 2
    ExplicitTop = 2
    ExplicitWidth = 331
    ExplicitHeight = 115
    inherited sgPesquisa: TGridLuka
      Width = 306
      Height = 98
      TabOrder = 4
      ExplicitWidth = 306
      ExplicitHeight = 98
    end
    inherited CkAspas: TCheckBox
      TabOrder = 2
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 5
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 7
    end
    inherited CkMultiSelecao: TCheckBox
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    inherited PnTitulos: TPanel
      Width = 331
      TabOrder = 0
      ExplicitWidth = 331
      inherited lbNomePesquisa: TLabel
        Width = 42
        Caption = 'Produto'
        ExplicitWidth = 42
        ExplicitHeight = 14
      end
      inherited CkChaveUnica: TCheckBox
        Checked = False
        State = cbUnchecked
      end
      inherited pnSuprimir: TPanel
        Left = 226
        ExplicitLeft = 226
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 306
      Height = 99
      TabOrder = 1
      ExplicitLeft = 306
      ExplicitHeight = 99
      inherited sbPesquisa: TSpeedButton
        Visible = False
      end
    end
    inherited ckSomenteAtivos: TCheckBox
      TabOrder = 6
    end
  end
  inline FrDataAjuste: TFrDataInicialFinal
    Left = 607
    Top = 2
    Width = 201
    Height = 41
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 607
    ExplicitTop = 2
    ExplicitWidth = 201
    inherited Label1: TLabel
      Left = 1
      Width = 176
      Height = 14
      Caption = 'Produtos que tiveram corre'#231#245'es::'
      ExplicitLeft = 1
      ExplicitWidth = 176
      ExplicitHeight = 14
    end
    inherited lb1: TLabel
      Left = 92
      Width = 18
      Height = 14
      ExplicitLeft = 92
      ExplicitWidth = 18
      ExplicitHeight = 14
    end
    inherited eDataFinal: TEditLukaData
      Left = 115
      Height = 22
      ExplicitLeft = 115
      ExplicitHeight = 22
    end
    inherited eDataInicial: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
  end
end
