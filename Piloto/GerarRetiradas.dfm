inherited FormGerarRetiradas: TFormGerarRetiradas
  Caption = 'Gera'#231#227'o de retiradas de produtos'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Width = 145
    ExplicitWidth = 145
    inherited sbCarregar: TSpeedButton
      Left = 14
      Width = 112
      ExplicitLeft = 14
      ExplicitWidth = 112
    end
    inherited sbImprimir: TSpeedButton
      Left = 14
      Top = 480
      Width = 112
      Visible = False
      ExplicitLeft = 14
      ExplicitTop = 480
      ExplicitWidth = 112
    end
    object miGerarRetiradaProdutosSelecionados: TSpeedButton [2]
      Left = 1
      Top = 72
      Width = 140
      Height = 29
      Caption = '&Gerar retirada no ato'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miGerarRetiradaProdutosSelecionadosClick
    end
    object miGerarPendenciaEntregaProdutosSelecionados: TSpeedButton [3]
      Left = 0
      Top = 107
      Width = 141
      Height = 31
      Caption = '&Trans. em entrega pendente'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miGerarPendenciaEntregaProdutosSelecionadosClick
    end
    object miDefinirQuantidadeTodosItens: TSpeedButton [4]
      Left = 0
      Top = 165
      Width = 137
      Height = 29
      Caption = '(F6) Sel. todos itens '
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miDefinirQuantidadeTodosItensClick
    end
    object miRemoverQuantidadeTodosItensF8: TSpeedButton [5]
      Left = 0
      Top = 200
      Width = 137
      Height = 27
      Caption = '(F8) Remov. todos itens  '
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = miRemoverQuantidadeTodosItensF8Click
    end
    inherited ckOmitirFiltros: TCheckBoxLuka
      Left = 6
      Top = 512
      ExplicitLeft = 6
      ExplicitTop = 512
    end
  end
  inherited pcDados: TPageControl
    Left = 145
    Width = 869
    ActivePage = tsResultado
    ExplicitLeft = 145
    ExplicitWidth = 869
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 861
      ExplicitHeight = 518
      inline FrClientes: TFrClientes
        Left = 1
        Top = 3
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        TabStop = True
        ExplicitLeft = 1
        ExplicitTop = 3
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 45
            ExplicitWidth = 45
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 0
        Top = 88
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        TabStop = True
        ExplicitTop = 88
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrOrcamentos: TFrNumeros
        Left = 421
        Top = 49
        Width = 134
        Height = 72
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 4
        TabStop = True
        ExplicitLeft = 421
        ExplicitTop = 49
        inherited pnDescricao: TPanel
          Caption = 'Pedidos:'
        end
      end
      inline FrDataRecebimento: TFrDataInicialFinal
        Left = 418
        Top = 3
        Width = 199
        Height = 41
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 5
        TabStop = True
        ExplicitLeft = 418
        ExplicitTop = 3
        ExplicitWidth = 199
        inherited Label1: TLabel
          Width = 90
          Height = 14
          Caption = 'Data rec. pedido'
          ExplicitWidth = 90
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrLocaisPendencia: TFrLocais
        Left = 0
        Top = 258
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitTop = 258
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 112
            Caption = 'Locais da pend'#234'ncia'
            ExplicitWidth = 112
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrVendedores: TFrVendedores
        Left = 0
        Top = 173
        Width = 404
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        ExplicitTop = 173
        ExplicitWidth = 404
        inherited sgPesquisa: TGridLuka
          Width = 379
          ExplicitWidth = 379
        end
        inherited PnTitulos: TPanel
          Width = 404
          ExplicitWidth = 404
          inherited lbNomePesquisa: TLabel
            Width = 65
            ExplicitWidth = 65
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 299
            ExplicitLeft = 299
          end
        end
        inherited pnPesquisa: TPanel
          Left = 379
          ExplicitLeft = 379
        end
        inherited ckSomenteAtivos: TCheckBox
          Width = 113
          ExplicitWidth = 113
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 861
      ExplicitHeight = 518
      object spSeparador: TSplitter
        Left = 0
        Top = 176
        Width = 861
        Height = 6
        Cursor = crVSplit
        Align = alTop
        ResizeStyle = rsUpdate
        ExplicitTop = 167
        ExplicitWidth = 884
      end
      object sgOrcamentos: TGridLuka
        Left = 0
        Top = 17
        Width = 861
        Height = 159
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alTop
        ColCount = 6
        DefaultRowHeight = 19
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
        PopupMenu = pmPendencias
        TabOrder = 0
        OnClick = sgOrcamentosClick
        OnDblClick = sgOrcamentosDblClick
        OnDrawCell = sgOrcamentosDrawCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Empresa'
          'Local'
          'Pedido'
          'Cliente'
          'Data cadastro'
          'Vendedor')
        Grid3D = False
        RealColCount = 10
        AtivarPopUpSelecao = False
        ColWidths = (
          115
          105
          99
          220
          104
          205)
      end
      object sgItens: TGridLuka
        Left = 0
        Top = 199
        Width = 861
        Height = 319
        Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
        Align = alClient
        ColCount = 7
        DefaultRowHeight = 19
        FixedColor = 15395562
        FixedCols = 0
        RowCount = 2
        Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goEditing, goFixedRowClick]
        PopupMenu = pmItens
        TabOrder = 1
        OnDrawCell = sgItensDrawCell
        OnKeyDown = sgItensKeyDown
        OnKeyPress = NumerosVirgula
        OnSelectCell = sgItensSelectCell
        IncrementExpandCol = 0
        IncrementExpandRow = 0
        CorLinhaFoco = 38619
        CorFundoFoco = 16774625
        CorLinhaDesfoque = 14869218
        CorFundoDesfoque = 16382457
        CorSuperiorCabecalho = clWhite
        CorInferiorCabecalho = 13553358
        CorSeparadorLinhas = 12040119
        CorColunaFoco = clActiveCaption
        HCol.Strings = (
          'Produto'
          'Nome'
          'Marca'
          'Quantidade'
          'Und.'
          'Lote'
          'Qtde. retirar')
        OnGetCellColor = sgItensGetCellColor
        OnArrumarGrid = sgItensArrumarGrid
        Grid3D = False
        RealColCount = 15
        OrdenarOnClick = True
        AtivarPopUpSelecao = False
        ExplicitLeft = 2
        ExplicitTop = 205
        ColWidths = (
          56
          216
          171
          81
          42
          175
          115)
      end
      object StaticTextLuka1: TStaticTextLuka
        Left = 0
        Top = 182
        Width = 861
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Itens da pend'#234'ncia selecionada'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
      object StaticTextLuka2: TStaticTextLuka
        Left = 0
        Top = 0
        Width = 861
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        BevelInner = bvNone
        Caption = 'Pend'#234'ncias a retirar'
        Color = clSilver
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 3
        Transparent = False
        AsInt = 0
        TipoCampo = tcTexto
        CasasDecimais = 0
      end
    end
  end
  object pmPendencias: TPopupMenu
    Left = 472
    Top = 108
  end
  object pmItens: TPopupMenu
    Left = 584
    Top = 108
  end
end
