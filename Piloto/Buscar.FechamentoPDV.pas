unit Buscar.FechamentoPDV;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.StdCtrls, _Biblioteca, _RecordsOrcamentosVendas,
  EditLuka, _FrameHerancaPrincipal, _FrameHenrancaPesquisas, Vcl.Buttons, Vcl.ExtCtrls,
  _RecordsFinanceiros, BuscarDadosCartoes, _Sessao, BuscarCondicoesCartaoCreditoPDV,
  FrameTipoNotaGerar;

type
  RecFechamento = record
    ValorDinheiro: Double;
    ValorCartaoDebito: Double;
    ValorCartaoCredito: Double;
    ValorCredito: Double;
    valorTroco: Double;
    ValorAcrescimoCartao: Double;

    CondicaoCartaoId: Integer;
    QtdeMaximaParcelasCartao: Integer;
    IndiceCondicaoCartao: Double;
    Cartoes: TArray<RecTitulosFinanceiros>;
    TipoNotaGerar: string;
    EmitirReciboPagamento: Boolean;
  end;

  TFormBuscarFechamentoPDV = class(TFormHerancaFinalizar)
    gb1: TGroupBox;
    lb4: TLabel;
    eValorDinheiro: TEditLuka;
    lb1: TLabel;
    eValorCartaoDebito: TEditLuka;
    lb19: TLabel;
    eValorSerPago: TEditLuka;
    stPagamento: TStaticText;
    sbBuscarDadosCartoes: TSpeedButton;
    eValorTroco: TEditLuka;
    lb3: TLabel;
    lb5: TLabel;
    eValorCartaoCredito: TEditLuka;
    FrTipoNotaGerar: TFrTipoNotaGerar;
    sbBuscarCartaoCredito: TSpeedButton;
    procedure sbBuscarDadosCartoesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure eValorCartaoDebitoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorCartaoDebitoExit(Sender: TObject);
    procedure eValorDinheiroChange(Sender: TObject);
    procedure eValorCartaoCreditoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sbBuscarCartaoCreditoClick(Sender: TObject);
  private
    FClienteId: Integer;
    FNomeCondicao: string;
    FValorSerPago: Double;

    FDadosCartoes: TArray<RecTitulosFinanceiros>;
    FDadosCondicaoCartao: RecRespostaCondicoesCartaoPDV;

    FItens: TArray<RecOrcamentoItens>;
  protected
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function Buscar(const pValorSerPago: Double; const pClienteId: Integer; pItens: TArray<RecOrcamentoItens>): TRetornoTelaFinalizar<RecFechamento>;

implementation

{$R *.dfm}

function Buscar(const pValorSerPago: Double; const pClienteId: Integer; pItens: TArray<RecOrcamentoItens>): TRetornoTelaFinalizar<RecFechamento>;
var
  vForm: TFormBuscarFechamentoPDV;
begin
  vForm := TFormBuscarFechamentoPDV.Create(Application);

  vForm.FClienteId := pClienteId;
  vForm.FValorSerPago := pValorSerPago;
  vForm.eValorSerPago.AsDouble := pValorSerPago;
  vForm.FItens := pItens;
  vForm.eValorTroco.AsDouble := pValorSerPago * -1;

  if Result.Ok(vForm.ShowModal, True) then begin
    Result.Dados.ValorDinheiro        := vForm.eValorDinheiro.AsDouble;
    Result.Dados.ValorCartaoDebito    := vForm.eValorCartaoDebito.AsDouble;
    Result.Dados.ValorCartaoCredito   := vForm.eValorCartaoCredito.AsDouble;
    //Result.Dados.ValorCredito         := vForm.eValorCredito.AsDouble;
    Result.Dados.valorTroco           := vForm.eValorTroco.AsDouble;
    //Result.Dados.ValorAcrescimoCartao := vForm.eValorAcrescimoCartao.AsDouble;
    Result.Dados.CondicaoCartaoId     := vForm.FDadosCondicaoCartao.CondicaoId;
    Result.Dados.QtdeMaximaParcelasCartao := vForm.FDadosCondicaoCartao.QtdeParcelas;

    Result.Dados.Cartoes := vForm.FDadosCartoes;

    Result.Dados.TipoNotaGerar         := vForm.FrTipoNotaGerar.TipoNotaGerar;
    Result.Dados.EmitirReciboPagamento := vForm.FrTipoNotaGerar.ckRecibo.Checked;
  end;
end;

{ TFormBuscarFechamentoPDV }

procedure TFormBuscarFechamentoPDV.eValorCartaoCreditoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  vCondicaoPDV: TRetornoTelaFinalizar<RecRespostaCondicoesCartaoPDV>;
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eValorCartaoCredito.AsCurr = 0 then begin
    //eValorAcrescimoCartao.AsCurr := 0;
    eValorSerPago.AsDouble := FValorSerPago;
    FDadosCondicaoCartao.CondicaoId := -1;
  end
  else if eValorCartaoCredito.AsCurr > 0 then begin
    vCondicaoPDV := BuscarCondicoesCartaoCreditoPDV.BuscarCondicaoPDV( eValorCartaoCredito.AsCurr, eValorSerPago.AsDouble, FItens );
    if vCondicaoPDV.BuscaCancelada then
      Exit;

    FDadosCondicaoCartao := vCondicaoPDV.Dados;

    eValorCartaoCredito.AsDouble := vCondicaoPDV.Dados.ValorTotal;
    //eValorAcrescimoCartao.AsDouble := vCondicaoPDV.Dados.Acrescimo;
    eValorSerPago.AsDouble := FValorSerPago + vCondicaoPDV.Dados.Acrescimo;
  end;

  eValorDinheiroChange(Sender);

  if Sessao.getParametrosEstacao.UtilizarTef = 'N' then
    sbBuscarCartaoCreditoClick(nil);

//  ProximoCampo(Sender, Key, Shift);
  sbFinalizarClick(nil);
end;

procedure TFormBuscarFechamentoPDV.eValorCartaoDebitoExit(Sender: TObject);
begin
  inherited;
  if (eValorCartaoDebito.AsDouble = 0) and (FDadosCartoes <> nil) then
    _Biblioteca.Destruir(TArray<TObject>(FDadosCartoes));
end;

procedure TFormBuscarFechamentoPDV.eValorCartaoDebitoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Sessao.getParametrosEstacao.UtilizarTef = 'N' then begin
    if (Key = VK_RETURN) and (FDadosCartoes = nil) then
      sbBuscarDadosCartoesClick(nil);
  end;

  ProximoCampo(Sender, Key, Shift);
end;

procedure TFormBuscarFechamentoPDV.eValorDinheiroChange(Sender: TObject);
begin
  inherited;
  eValorTroco.AsCurr := (eValorDinheiro.AsCurr + eValorCartaoDebito.AsCurr + eValorCartaoCredito.AsDouble) - eValorSerPago.AsCurr;
end;

procedure TFormBuscarFechamentoPDV.FormCreate(Sender: TObject);
begin
  inherited;
  FDadosCondicaoCartao.CondicaoId := -1;
  FNomeCondicao := Sessao.getValorColunaStr('NOME','CONDICOES_PAGAMENTO', 'CONDICAO_ID = :P1', [Sessao.getParametrosEmpresa.CondicaoPagamentoPDV]);

  _Biblioteca.Visibilidade([sbBuscarDadosCartoes, sbBuscarCartaoCredito], Sessao.getParametrosEstacao.UtilizarTef = 'N');
end;

procedure TFormBuscarFechamentoPDV.sbBuscarCartaoCreditoClick(Sender: TObject);
begin
  inherited;
  if eValorCartaoCredito.AsCurr > 0 then begin
    FDadosCartoes :=
      BuscarDadosCartoes.BuscarCartoes(
        FDadosCondicaoCartao.CondicaoId,
        FNomeCondicao,
        eValorCartaoCredito.AsDouble,
        FDadosCartoes,
        False,
        FClienteId,
        'C',
        False
      ).Dados;
  end;
end;

procedure TFormBuscarFechamentoPDV.sbBuscarDadosCartoesClick(Sender: TObject);
begin
  inherited;
  if eValorCartaoDebito.AsCurr > 0 then begin
    FDadosCartoes :=
      BuscarDadosCartoes.BuscarCartoes(
        Sessao.getParametrosEmpresa.CondicaoPagamentoPDV,
        FNomeCondicao,
        eValorCartaoDebito.AsDouble,
        FDadosCartoes,
        False,
        FClienteId,
        'D',
        False
      ).Dados;
  end;
end;

procedure TFormBuscarFechamentoPDV.VerificarRegistro;
begin
  inherited;

  if eValorTroco.AsCurr < 0 then begin
    Exclamar('Existe diferen�a nas formas de pagamento informada!');
    SetarFoco(eValorDinheiro);
    Abort;
  end;

  if eValorCartaoDebito.AsCurr > eValorSerPago.AsCurr then begin
    Exclamar('O valor informado do cart�o n�o pode ser maior que o valor a ser pago!');
    SetarFoco(eValorCartaoDebito);
    Abort;
  end;

  if (eValorCartaoDebito.AsCurr > 0) and (FDadosCartoes = nil) and (Sessao.getParametrosEstacao.UtilizarTef = 'N') then begin
    _Biblioteca.Exclamar('Os cart�es n�o foram definidos corretamente!');
    SetarFoco(eValorCartaoDebito);
    Abort;
  end;
end;

end.
