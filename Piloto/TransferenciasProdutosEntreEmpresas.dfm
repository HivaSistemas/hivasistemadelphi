inherited FormTransferenciasProdutosEntreEmpresas: TFormTransferenciasProdutosEntreEmpresas
  Caption = 'Transfer'#234'ncia de produtos entre empresas'
  ClientHeight = 571
  ClientWidth = 994
  ExplicitWidth = 1000
  ExplicitHeight = 600
  PixelsPerInch = 96
  TextHeight = 14
  inherited Label1: TLabel
    Left = 125
    ExplicitLeft = 125
  end
  object lb7: TLabel [1]
    Left = 418
    Top = 90
    Width = 64
    Height = 14
    Caption = 'Quantidade'
  end
  inherited pnOpcoes: TPanel
    Height = 571
    TabOrder = 8
    ExplicitHeight = 571
    inherited sbExcluir: TSpeedButton
      Visible = False
    end
    inherited sbPesquisar: TSpeedButton
      Visible = False
    end
  end
  inherited eID: TEditLuka
    Left = 125
    Width = 54
    TabOrder = 0
    ExplicitLeft = 125
    ExplicitWidth = 54
  end
  inherited ckAtivo: TCheckBoxLuka
    Left = 907
    Top = -18
    TabOrder = 9
    Visible = False
    ExplicitLeft = 907
    ExplicitTop = -18
  end
  inline FrEmpresaOrigem: TFrEmpresas
    Left = 183
    Top = 1
    Width = 300
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 183
    ExplicitTop = 1
    ExplicitWidth = 300
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 275
      Height = 24
      ExplicitWidth = 275
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 300
      ExplicitWidth = 300
      inherited lbNomePesquisa: TLabel
        Width = 88
        Caption = 'Empresa origem'
        ExplicitWidth = 88
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 230
        ExplicitLeft = 230
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 275
      Height = 25
      ExplicitLeft = 275
      ExplicitHeight = 25
    end
  end
  inline FrEmpresaDestino: TFrEmpresas
    Left = 487
    Top = 1
    Width = 300
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 487
    ExplicitTop = 1
    ExplicitWidth = 300
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 275
      Height = 24
      ExplicitWidth = 275
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 300
      ExplicitWidth = 300
      inherited lbNomePesquisa: TLabel
        Width = 91
        Caption = 'Empresa destino'
        ExplicitWidth = 91
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 230
        ExplicitLeft = 230
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 275
      Height = 25
      ExplicitLeft = 275
      ExplicitHeight = 25
    end
  end
  inline FrLocalOrigem: TFrLocais
    Left = 478
    Top = 47
    Width = 310
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 5
    TabStop = True
    ExplicitLeft = 478
    ExplicitTop = 47
    ExplicitWidth = 310
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 285
      Height = 23
      ExplicitWidth = 285
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 310
      ExplicitWidth = 310
      inherited lbNomePesquisa: TLabel
        Width = 86
        Caption = 'Local de origem'
        ExplicitWidth = 86
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 240
        ExplicitLeft = 240
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 285
      Height = 24
      ExplicitLeft = 285
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  inline FrProduto: TFrProdutos
    Left = 125
    Top = 46
    Width = 346
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 4
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 46
    ExplicitWidth = 346
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 321
      Height = 23
      TabOrder = 1
      ExplicitWidth = 321
      ExplicitHeight = 23
    end
    inherited CkAspas: TCheckBox
      TabOrder = 3
    end
    inherited CkFiltroDuplo: TCheckBox
      TabOrder = 5
    end
    inherited CkPesquisaNumerica: TCheckBox
      TabOrder = 7
    end
    inherited PnTitulos: TPanel
      Width = 346
      TabOrder = 0
      ExplicitWidth = 346
      inherited lbNomePesquisa: TLabel
        Width = 42
        Caption = 'Produto'
        ExplicitWidth = 42
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 276
        ExplicitLeft = 276
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 321
      Height = 24
      TabOrder = 2
      ExplicitLeft = 321
      ExplicitHeight = 24
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
      TabOrder = 6
    end
  end
  object eQuantidade: TEditLuka
    Left = 418
    Top = 104
    Width = 121
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    Text = '0,000'
    OnKeyDown = eQuantidadeKeyDown
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 3
    PadraoEstoque = True
    NaoAceitarEspaco = False
  end
  inline FrLote: TFrameLotes
    Left = 125
    Top = 89
    Width = 287
    Height = 41
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 6
    TabStop = True
    ExplicitLeft = 125
    ExplicitTop = 89
    inherited lbLote: TLabel
      Width = 23
      Height = 14
      ExplicitWidth = 23
      ExplicitHeight = 14
    end
    inherited lbDataFabricacao: TLabel
      Width = 75
      Height = 14
      ExplicitWidth = 75
      ExplicitHeight = 14
    end
    inherited lbDataVencimento: TLabel
      Width = 56
      Height = 14
      ExplicitWidth = 56
      ExplicitHeight = 14
    end
    inherited lbBitola: TLabel
      Width = 33
      Height = 14
      ExplicitWidth = 33
      ExplicitHeight = 14
    end
    inherited lbTonalidade: TLabel
      Width = 62
      Height = 14
      ExplicitWidth = 62
      ExplicitHeight = 14
    end
    inherited eLote: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited eDataFabricacao: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
    inherited eDataVencimento: TEditLukaData
      Height = 22
      ExplicitHeight = 22
    end
    inherited eBitola: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
    inherited eTonalidade: TEditLuka
      Height = 22
      ExplicitHeight = 22
    end
  end
  object sgItens: TGridLuka
    Left = 123
    Top = 127
    Width = 869
    Height = 408
    Hint = 'Click com o bot'#227'o direito do mouse para mais op'#231#245'es.'
    Align = alCustom
    ColCount = 10
    DefaultRowHeight = 19
    DrawingStyle = gdsGradient
    FixedColor = 15395562
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing]
    PopupMenu = pmOpcoes
    TabOrder = 10
    OnDblClick = sgItensDblClick
    OnDrawCell = sgItensDrawCell
    OnKeyDown = sgItensKeyDown
    IncrementExpandCol = 0
    IncrementExpandRow = 0
    CorLinhaFoco = 16768407
    CorFundoFoco = 16774625
    CorLinhaDesfoque = 14869218
    CorFundoDesfoque = 16382457
    CorSuperiorCabecalho = clWhite
    CorInferiorCabecalho = 13553358
    CorSeparadorLinhas = 12040119
    CorColunaFoco = clMenuHighlight
    HCol.Strings = (
      'Produto'
      'Nome'
      'Marca'
      'Quantidade'
      'Und.'
      'Lote'
      'Pre'#231'o unit.'
      'Valor total'
      'Local'
      'Nome local')
    Grid3D = False
    RealColCount = 10
    Indicador = True
    AtivarPopUpSelecao = False
    ColWidths = (
      49
      209
      95
      72
      33
      78
      75
      78
      39
      197)
  end
  object st1: TStaticTextLuka
    Left = 888
    Top = 54
    Width = 103
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Est. fisico'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 11
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stEstoqueFisico: TStaticText
    Left = 888
    Top = 70
    Width = 103
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '5,0000'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    Transparent = False
  end
  object stEstoqueDisponivel: TStaticText
    Left = 792
    Top = 70
    Width = 97
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '10,0000'
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 13
    Transparent = False
  end
  object st2: TStaticTextLuka
    Left = 792
    Top = 54
    Width = 97
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Est. dispon'#237'vel'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 14
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object st3: TStaticTextLuka
    Left = 842
    Top = 536
    Width = 150
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Valor total'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 15
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object st6: TStaticTextLuka
    Left = 693
    Top = 536
    Width = 150
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BevelInner = bvNone
    Caption = 'Qtde. produtos'
    Color = 38619
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 16
    Transparent = False
    AsInt = 0
    TipoCampo = tcTexto
    CasasDecimais = 0
  end
  object stQtdeProdutos: TStaticTextLuka
    Left = 693
    Top = 553
    Width = 150
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 17
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  object stValorTotal: TStaticTextLuka
    Left = 842
    Top = 553
    Width = 150
    Height = 17
    Alignment = taCenter
    Anchors = [akLeft, akBottom]
    AutoSize = False
    BevelInner = bvNone
    BevelKind = bkFlat
    Caption = '0,00'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 18
    Transparent = False
    AsInt = 0
    TipoCampo = tcNumerico
    CasasDecimais = 2
  end
  inline FrMotorista: TFrMotoristas
    Left = 791
    Top = 1
    Width = 203
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 3
    TabStop = True
    ExplicitLeft = 791
    ExplicitTop = 1
    ExplicitWidth = 203
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 178
      Height = 24
      ExplicitWidth = 178
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 203
      ExplicitWidth = 203
      inherited lbNomePesquisa: TLabel
        Width = 53
        Caption = 'Motorista'
        ExplicitWidth = 53
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 133
        ExplicitLeft = 133
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 178
      Height = 25
      ExplicitLeft = 178
      ExplicitHeight = 25
    end
    inherited ckSomenteAtivos: TCheckBox
      Checked = True
      State = cbChecked
    end
  end
  object pmOpcoes: TPopupMenu
    Left = 896
    Top = 408
    object miBuscarTodosProdutosEstoqueDisponivel: TMenuItem
      Caption = 'Buscar todos os produtos com estoque dispon'#237'vel'
      OnClick = miBuscarTodosProdutosEstoqueDisponivelClick
    end
  end
end
