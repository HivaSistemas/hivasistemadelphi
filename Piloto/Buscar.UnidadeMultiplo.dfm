inherited FormBuscarUnidadeMultiplo: TFormBuscarUnidadeMultiplo
  ActiveControl = FrUnidade.sgPesquisa
  Caption = 'Buscar unidade e m'#250'ltiplo'
  ClientHeight = 85
  ClientWidth = 422
  ExplicitWidth = 428
  ExplicitHeight = 114
  PixelsPerInch = 96
  TextHeight = 14
  object lb1: TLabel [0]
    Left = 271
    Top = 4
    Width = 46
    Height = 14
    Caption = 'M'#250'ltiplo'
  end
  object lbl1: TLabel [1]
    Left = 343
    Top = 5
    Width = 59
    Height = 14
    Caption = 'Qtde. emb.'
  end
  inherited pnOpcoes: TPanel
    Top = 48
    Width = 422
    ExplicitTop = 48
    ExplicitWidth = 422
  end
  inline FrUnidade: TFrUnidades
    Left = 3
    Top = 2
    Width = 262
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 3
    ExplicitTop = 2
    ExplicitWidth = 262
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Width = 237
      Height = 23
      ExplicitWidth = 237
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      Width = 262
      ExplicitWidth = 262
      inherited lbNomePesquisa: TLabel
        Width = 47
        Caption = 'Unidade'
        ExplicitWidth = 47
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 192
        ExplicitLeft = 192
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 237
      Height = 24
      ExplicitLeft = 237
      ExplicitHeight = 24
    end
  end
  object eMultiplo: TEditLuka
    Left = 271
    Top = 19
    Width = 66
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 2
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
  object eQtdeEmbalagem: TEditLuka
    Left = 343
    Top = 20
    Width = 74
    Height = 22
    Alignment = taRightJustify
    CharCase = ecUpperCase
    TabOrder = 3
    Text = '0,0000'
    OnKeyDown = ProximoCampo
    TipoCampo = tcNumerico
    ApenasPositivo = False
    AsInt = 0
    CasasDecimais = 4
    NaoAceitarEspaco = False
  end
end
