unit FrameUnidades;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHenrancaPesquisas, Vcl.StdCtrls, _RecordsCadastros,
  Vcl.ExtCtrls, Vcl.Grids, GridLuka, _Sessao, System.Math, PesquisaUnidades, _Unidades,
  Vcl.Buttons, Vcl.Menus;

type
  TFrUnidades = class(TFrameHenrancaPesquisas)
  public
    function getUnidade(pLinha: Integer = -1): RecUnidades;
  protected
    procedure MontarGrid; override;
    function AdicionarDireto: TObject; override;
    function AdicionarPesquisando: TObject; override;
    function IndiceChave(pSender: TObject): Integer; override;
  end;

implementation

{$R *.dfm}

{ TFrUnidades }

function TFrUnidades.AdicionarDireto: TObject;
var
  unidades: TArray<RecUnidades>;
begin
  unidades := _Unidades.BuscarUnidades(Sessao.getConexaoBanco, 0, [FChaveDigitada] );
  if unidades = nil then
    Result := nil
  else
    Result := unidades[0];
end;

function TFrUnidades.AdicionarPesquisando: TObject;
begin
  Result := PesquisaUnidades.PesquisarUnidade;
end;

function TFrUnidades.getUnidade(pLinha: Integer): RecUnidades;
begin
  if FDados.Count = 0 then
    Result := nil
  else
    Result := RecUnidades(FDados[IfThen(pLinha = -1, sgPesquisa.Row, pLinha)]);
end;

function TFrUnidades.IndiceChave(pSender: TObject): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i := 0 to FDados.Count - 1 do begin
    if RecUnidades(FDados[i]).unidade_id = RecUnidades(pSender).unidade_id then begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TFrUnidades.MontarGrid;
var
  i: Integer;
  pSender: RecUnidades;
begin
  with sgPesquisa do begin
    ClearGrid(1);
    for i := 0 to FDados.Count - 1 do begin
      pSender := RecUnidades(FDados[i]);
      AAdd([pSender.unidade_id, pSender.descricao]);
    end;
  end;
end;

end.
