unit FrameDiretorioArquivo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.Buttons, _Biblioteca,
  Vcl.StdCtrls, EditLuka;

type
  TFrCaminhoArquivo = class(TFrameHerancaPrincipal)
    eCaminhoArquivo: TEditLuka;
    lb1: TLabel;
    sbPesquisarArquivo: TSpeedButton;
    procedure sbPesquisarArquivoClick(Sender: TObject);
    procedure eCaminhoArquivoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FFiltro: string;
    FOnAposPesquisarArquivo: TEventoObjeto;

    procedure setCaminhoArquivo(pCaminho: string);
  public
    function getCaminhoArquivo: string;
    function getNomeArquivo: string;
    procedure setFiltro(pFiltro: string);
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    procedure Clear; override;
    procedure SetFocus; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    function EstaVazio: Boolean; override;
  published
    property CaminhoArquivo: string read getCaminhoArquivo write setCaminhoArquivo;
    property OnAposPesquisarArquivo: TEventoObjeto read FOnAposPesquisarArquivo write FOnAposPesquisarArquivo;
  end;

implementation

{$R *.dfm}

procedure TFrCaminhoArquivo.Clear;
begin
  inherited;
  eCaminhoArquivo.Clear;
end;

procedure TFrCaminhoArquivo.eCaminhoArquivoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key <> VK_RETURN) then
    Exit;

  if eCaminhoArquivo.Text <> '' then begin // Caso j� tenha um caminho, passando para pr�ximo campo
    keybd_event(VK_TAB, 0, 0, 0);
    Exit;
  end;

  sbPesquisarArquivoClick(nil);
end;

function TFrCaminhoArquivo.EstaVazio: Boolean;
begin
  Result := eCaminhoArquivo.Text = '';
end;

function TFrCaminhoArquivo.getCaminhoArquivo: string;
begin
  Result := Trim(eCaminhoArquivo.Text);
end;

function TFrCaminhoArquivo.getNomeArquivo: string;
begin
  Result := ExtractFileName(eCaminhoArquivo.Text);
end;

procedure TFrCaminhoArquivo.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;
  _Biblioteca.Habilitar([eCaminhoArquivo, sbPesquisarArquivo], pEditando, pLimpar);
end;

procedure TFrCaminhoArquivo.sbPesquisarArquivoClick(Sender: TObject);
var
  vArquivo: TOpenDialog;
begin
  inherited;
  vArquivo := TOpenDialog.Create(Self);
  vArquivo.Filter := FFiltro;

  if vArquivo.Execute then begin
    eCaminhoArquivo.Text := vArquivo.FileName;

    if Assigned(FOnAposPesquisarArquivo) then
      FOnAposPesquisarArquivo(Self);
  end;
end;

procedure TFrCaminhoArquivo.setCaminhoArquivo(pCaminho: string);
begin
  eCaminhoArquivo.Text := pCaminho;

  if Assigned(FOnAposPesquisarArquivo) then
    FOnAposPesquisarArquivo(Self);
end;

procedure TFrCaminhoArquivo.SetFiltro(pFiltro: string);
begin
  FFiltro := pFiltro;
end;

procedure TFrCaminhoArquivo.SetFocus;
begin
  inherited;
  eCaminhoArquivo.SetFocus;
end;

procedure TFrCaminhoArquivo.SomenteLeitura(pValue: Boolean);
begin
  inherited;
  sbPesquisarArquivo.Enabled := not pValue;
end;

end.
