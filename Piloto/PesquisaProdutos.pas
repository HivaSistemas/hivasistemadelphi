unit PesquisaProdutos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaPesquisas, Vcl.StdCtrls, _Biblioteca, _Sessao,
  ComboBoxLuka, Vcl.Grids, GridLuka, EditLuka, _Produtos, System.Math, _RecordsCadastros,
  _RecordsEstoques, Vcl.ExtCtrls, StaticTextLuka, Vcl.Menus;

type
  TFormPesquisaProdutos = class(TFormHerancaPesquisas)
    eMarca: TEditLuka;
    lblMarca: TLabel;
    pnFiltroExtra: TPanel;
    ckAtivo: TCheckBox;
    ckInativos: TCheckBox;
    ckRevenda: TCheckBox;
    ckUsoConsumo: TCheckBox;
    ckAtivoImobilizado: TCheckBox;
    ckServico: TCheckBox;
    ckSobEncomenda: TCheckBox;
    ckPermitirDevolucao: TCheckBox;
    ckBloquearParaCompras: TCheckBox;
    ckBloquearParaVendas: TCheckBox;
    stctxtlk3: TStaticTextLuka;
    ckInativarZeraEstoque: TCheckBox;
    ckAceitaEstoqueNegativo: TCheckBox;
    ckOmitir: TCheckBox;
    pmOpcoesGrid: TPopupMenu;
    FiltrosExtra1: TMenuItem;
    MarcaDemarcaTodosCtrlespao1: TMenuItem;
    procedure sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure cbOpcoesPesquisaChange(Sender: TObject);
    procedure eMarcaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure eValorPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FiltrosExtra1Click(Sender: TObject);
    procedure MarcaDemarcaTodosCtrlespao1Click(Sender: TObject);
  protected
    procedure BuscarRegistros; override;
  end;

function PesquisarProduto(pFiltroExtra, pSomenteAtivos: Boolean): TObject;
function PesquisarVarios(pFiltroExtra, pSomenteAtivos: Boolean): TArray<TObject>;

implementation

{$R *.dfm}

{ TFormPesquisaProdutos }

const
  cbNomeMarcaAvancado = 4;
  cbNomeCodigoFabricante = 8;

var
  FSomenteAtivos: Boolean;
  FFiltroExtra: Boolean;

const
  cp_nome        = 2;
  cp_marca       = 3;
  cp_und_venda   = 4;
  cp_ativo       = 5;
  cp_codigo_orig = 6;
  cp_nome_compra = 7;
  cp_fabricante  = 8;

function PesquisarProduto(pFiltroExtra, pSomenteAtivos: Boolean): TObject;
var
  vProd: TObject;
begin
  FSomenteAtivos := pSomenteAtivos;
  FFiltroExtra := pFiltroExtra;
  vProd := _HerancaPesquisas.Pesquisar(TFormPesquisaProdutos, _Produtos.GetFiltros);
  if vProd = nil then
    Result := nil
  else
    Result := RecProdutos(vProd);
end;

function PesquisarVarios(pFiltroExtra, pSomenteAtivos: Boolean): TArray<TObject>;
var
  vProds: TArray<TObject>;
begin
  FSomenteAtivos := pSomenteAtivos;
  FFiltroExtra := pFiltroExtra;
  vProds := _HerancaPesquisas.PesquisarVarios(TFormPesquisaProdutos, _Produtos.GetFiltros);
  if vProds = nil then
    Result := nil
  else
    Result := TArray<TObject>(vProds);
end;

procedure TFormPesquisaProdutos.BuscarRegistros;
var
  i: Integer;
  produtos: TArray<RecProdutos>;
  filtroExtra: string;
begin
  inherited;

  filtroExtra := '';
  if pnFiltroExtra.Visible then begin
    FSomenteAtivos := False;
    if not ckAtivo.Checked and ckInativos.Checked then
      filtroExtra := 'and PRO.ATIVO = ''' + IIfStr(ckOmitir.Checked,'S','N') + ''''
    else
    if ckAtivo.Checked and ckInativos.Checked then
      if ckOmitir.Checked then
        filtroExtra := 'and PRO.ATIVO = ''V'' '
      else
        FSomenteAtivos := False
    else
      if ckOmitir.Checked then
        filtroExtra := 'and PRO.ATIVO = ''N'' '
      else
        FSomenteAtivos := True;

    if ckRevenda.Checked then
      filtroExtra := filtroExtra + 'and PRO.REVENDA = ''' + IIfStr(ckOmitir.Checked,'N','S') + '''';
    if ckUsoConsumo.Checked then
      filtroExtra := filtroExtra + 'and PRO.USO_CONSUMO = ''' + IIfStr(ckOmitir.Checked,'N','S') + '''';
    if ckAtivoImobilizado.Checked then
      filtroExtra := filtroExtra + 'and PRO.ATIVO_IMOBILIZADO = ''' + IIfStr(ckOmitir.Checked,'N','S') + '''';
    if ckServico.Checked then
      filtroExtra := filtroExtra + 'and PRO.SERVICO = ''' + IIfStr(ckOmitir.Checked,'N','S') + '''';
    if ckSobEncomenda.Checked then
      filtroExtra := filtroExtra + 'and PRO.SOB_ENCOMENDA = ''' + IIfStr(ckOmitir.Checked,'N','S') + '''';
    if ckPermitirDevolucao.Checked then
      filtroExtra := filtroExtra + 'and PRO.PERMITIR_DEVOLUCAO = ''' + IIfStr(ckOmitir.Checked,'N','S') + '''';
    if ckInativarZeraEstoque.Checked then
      filtroExtra := filtroExtra + 'and PRO.INATIVAR_ZERAR_ESTOQUE = ''' + IIfStr(ckOmitir.Checked,'N','S') + '''';
    if ckAceitaEstoqueNegativo.Checked then
      filtroExtra := filtroExtra + 'and PRO.ACEITAR_ESTOQUE_NEGATIVO = ''' + IIfStr(ckOmitir.Checked,'N','S') + '''';
    if ckBloquearParaCompras.Checked then
      filtroExtra := filtroExtra + 'and PRO.BLOQUEADO_COMPRAS = ''' + IIfStr(ckOmitir.Checked,'N','S') + '''';
    if ckBloquearParaVendas.Checked then
      filtroExtra := filtroExtra + 'and PRO.BLOQUEADO_VENDAS = ''' + IIfStr(ckOmitir.Checked,'N','S') + '''';
  end;

  if eMarca.Visible then begin
    produtos :=
      _Produtos.BuscarProdutos(
        Sessao.getConexaoBanco,
        cbOpcoesPesquisa.GetIndice,
        [eValorPesquisa.Text,eMarca.Text],
        FSomenteAtivos,
        filtroExtra
      );
  end
  else begin
    produtos :=
      _Produtos.BuscarProdutos(
        Sessao.getConexaoBanco,
        cbOpcoesPesquisa.GetIndice,
        [eValorPesquisa.Text],
        FSomenteAtivos,
        filtroExtra
      );
  end;

  if produtos = nil then begin
    _Biblioteca.NenhumRegistro;
    SetarFoco(eValorPesquisa);
    Exit;
  end;

  FDados := TArray<TObject>(produtos);

  for i := Low(produtos) to High(produtos) do begin
    sgPesquisa.Cells[coSelecionado, i + 1]  := charNaoSelecionado;
    sgPesquisa.Cells[coCodigo, i + 1]       := NFormat(produtos[i].produto_id);
    sgPesquisa.Cells[cp_nome, i + 1]        := produtos[i].nome;
    sgPesquisa.Cells[cp_marca, i + 1]       := produtos[i].nome_marca; //getInformacao(produtos[i].marca_id, produtos[i].nome_marca);
    sgPesquisa.Cells[cp_und_venda, i + 1]   := produtos[i].unidade_venda;
    sgPesquisa.Cells[cp_ativo, i + 1]       := _Biblioteca.SimNao(produtos[i].ativo);
    sgPesquisa.Cells[cp_codigo_orig, i + 1] := produtos[i].CodigoOriginalFabricante;
    sgPesquisa.Cells[cp_nome_compra, i + 1] := produtos[i].nome_compra;
    sgPesquisa.Cells[cp_fabricante, i + 1]  := getInformacao(produtos[i].fornecedor_id, produtos[i].nome_fabricante);
  end;

  sgPesquisa.SetLinhasGridPorTamanhoVetor( Length(produtos) );
  SetarFoco( sgPesquisa );
end;

procedure TFormPesquisaProdutos.cbOpcoesPesquisaChange(Sender: TObject);
begin
  inherited;
  _Biblioteca.Visibilidade([eMarca, lblMarca], cbOpcoesPesquisa.ItemIndex in [cbNomeMarcaAvancado, cbNomeCodigoFabricante]);
  _Biblioteca.Habilitar([eMarca, lblMarca], cbOpcoesPesquisa.ItemIndex in [cbNomeMarcaAvancado, cbNomeCodigoFabricante]);
  eValorPesquisa.Width := IfThen(eMarca.Visible, 344, 530);
end;

procedure TFormPesquisaProdutos.eMarcaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key <> VK_RETURN then
    Exit;

  BuscarRegistros;
end;

procedure TFormPesquisaProdutos.eValorPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
//  inherited;
  if Key <> VK_RETURN then
    Exit;

  if eMarca.Visible then
    SetarFoco(eMarca)
  else
    BuscarRegistros;
end;

procedure TFormPesquisaProdutos.FiltrosExtra1Click(Sender: TObject);
begin
  inherited;
  pnFiltroExtra.Visible := True;
end;

procedure TFormPesquisaProdutos.FormCreate(Sender: TObject);
begin
  inherited;
  ckAtivo.Checked := FSomenteAtivos;
end;

procedure TFormPesquisaProdutos.FormShow(Sender: TObject);
begin
  inherited;
//  FiltrosExtra1.Enabled := FFiltroExtra;
  if sgPesquisa.ColWidths[coSelecionado] < 1 then
    pnFiltroExtra.Visible := False
  else begin
//    pnFiltroExtra.Visible := FFiltroExtra;
    MarcaDemarcaTodosCtrlespao1.Enabled := True;
  end;
end;

procedure TFormPesquisaProdutos.MarcaDemarcaTodosCtrlespao1Click(
  Sender: TObject);
begin
  inherited;
  MarcarDesmarcarTodos(sgPesquisa, coSelecionado, VK_SPACE, [ssCtrl]);
end;

procedure TFormPesquisaProdutos.sgPesquisaDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if _Biblioteca.Em(ACol, [coSelecionado, cp_ativo]) then
    vAlinhamento := taCenter
  else if ACol in[
    coCodigo,
    cp_codigo_orig]  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgPesquisa.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormPesquisaProdutos.sgPesquisaGetCellColor(Sender: TObject; ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if sgPesquisa.Cells[cp_nome, ARow] = '' then
    Exit;

  if ACol = cp_ativo then begin
    AFont.Style := [fsBold];
    if sgPesquisa.Cells[cp_ativo, ARow] = 'Sim' then
      AFont.Color := coCorFonteEdicao1
    else
      AFont.Color := clRed;
  end;
end;

end.


