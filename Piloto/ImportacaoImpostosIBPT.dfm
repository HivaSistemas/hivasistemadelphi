inherited FormImportacaoImpostosIBPT: TFormImportacaoImpostosIBPT
  Caption = 'Importa'#231#227'o impostos IBPT'
  ClientHeight = 148
  ClientWidth = 448
  ExplicitWidth = 454
  ExplicitHeight = 177
  PixelsPerInch = 96
  TextHeight = 14
  object lbNCMs: TLabel [0]
    Left = 256
    Top = 59
    Width = 96
    Height = 26
    Caption = 'NCMs lidos'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbQtdeNCMs: TLabel [1]
    Left = 144
    Top = 59
    Width = 106
    Height = 26
    Alignment = taRightJustify
    AutoSize = False
    Caption = '0'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -21
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited pnOpcoes: TPanel
    Height = 148
    ExplicitHeight = 148
    inherited sbDesfazer: TSpeedButton
      Visible = False
    end
    inherited sbExcluir: TSpeedButton
      Visible = False
    end
    inherited sbPesquisar: TSpeedButton
      Visible = False
    end
  end
  inline FrArquivo: TFrCaminhoArquivo
    Left = 128
    Top = 12
    Width = 315
    Height = 43
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 128
    ExplicitTop = 12
    ExplicitWidth = 315
    ExplicitHeight = 43
    inherited lb1: TLabel
      Left = 0
      Top = 0
      Width = 118
      Height = 14
      Caption = 'Caminho arquivo IBPT'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 118
      ExplicitHeight = 14
    end
    inherited sbPesquisarArquivo: TSpeedButton
      Left = 283
      Top = 15
      ExplicitLeft = 283
      ExplicitTop = 15
    end
    inherited eCaminhoArquivo: TEditLuka
      Left = 0
      Width = 283
      Height = 22
      ExplicitLeft = 0
      ExplicitWidth = 283
      ExplicitHeight = 22
    end
  end
  inline FrEstado: TFrEstados
    Left = 128
    Top = 96
    Width = 315
    Height = 41
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    TabStop = True
    ExplicitLeft = 128
    ExplicitTop = 96
    ExplicitWidth = 315
    ExplicitHeight = 41
    inherited sgPesquisa: TGridLuka
      Width = 290
      Height = 24
      ExplicitWidth = 290
      ExplicitHeight = 24
    end
    inherited PnTitulos: TPanel
      Width = 315
      ExplicitWidth = 315
      inherited lbNomePesquisa: TLabel
        Width = 37
        Caption = 'Estado'
        ExplicitWidth = 37
        ExplicitHeight = 14
      end
      inherited pnSuprimir: TPanel
        Left = 245
        ExplicitLeft = 245
        inherited ckSuprimir: TCheckBox
          Visible = False
        end
      end
    end
    inherited pnPesquisa: TPanel
      Left = 290
      Height = 25
      ExplicitLeft = 290
      ExplicitHeight = 25
    end
  end
end
