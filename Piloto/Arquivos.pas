unit Arquivos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Grids, GridLuka,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.Menus, _Biblioteca, _Sessao, _Arquivos, _RecordsEspeciais;

type
  TFormArquivos = class(TFormHerancaFinalizar)
    sgArquivos: TGridLuka;
    pmArquivos: TPopupMenu;
    miN1: TMenuItem;
    Panel1: TPanel;
    miInserirNovoArquivo: TSpeedButton;
    miDeletarArquivoFocado: TSpeedButton;
    miSalvarArquivoFocado: TSpeedButton;
    Salvararquivo1: TMenuItem;
    SaveDialog1: TSaveDialog;
    procedure sgArquivosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure miInserirNovoArquivoClick(Sender: TObject);
    procedure miDeletarArquivoFocadoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Salvararquivo1Click(Sender: TObject);
  private
    FId: Integer;
    FOrigem: string;
    FArquivosIdsOriginais: TArray<Integer>;

    procedure inserirArquivos(pCaminhoArquivo: string); overload;

    procedure inserirArquivos(
      pArquivoId: Integer;
      pDataHoraCadastro: TDateTime;
      pNomeArquivo: string;
      pCaminhoArquivo: string
    ); overload;

    function getArquivos: TArray<RecArquivos>;
  protected
    procedure Finalizar(Sender: TObject); override;
    procedure VerificarRegistro(Sender: TObject); override;
  end;

function verArquivos(pId: Integer; pOrigem: string; pArquivos: TArray<RecArquivos>): TRetornoTelaFinalizar<TArray<RecArquivos>>;

implementation

{$R *.dfm}

const
  coArquivoId        = 0;
  coDataHoraCadastro = 1;
  coNomeArquivo      = 2;
  coCaminhoArquivo   = 3;

function verArquivos(pId: Integer; pOrigem: string; pArquivos: TArray<RecArquivos>): TRetornoTelaFinalizar<TArray<RecArquivos>>;
var
  i: Integer;
  vLinha: Integer;

  vForm: TFormArquivos;
begin
  vForm := TFormArquivos.Create(nil);

  vForm.FId     := pId;
  vForm.FOrigem := pOrigem;

  vLinha := 0;
  for i := Low(pArquivos) to High(pArquivos) do begin
    Inc(vLinha);

    vForm.sgArquivos.Cells[coArquivoid, vLinha]        := NFormatN(pArquivos[i].ArquivoId);
    vForm.sgArquivos.Cells[coDataHoraCadastro, vLinha] := DHFormatN(pArquivos[i].DataHoracadastro);
    vForm.sgArquivos.Cells[coNomeArquivo, vLinha]      := pArquivos[i].NomeArquivo;
    vForm.sgArquivos.Cells[coCaminhoArquivo, vLinha]   := pArquivos[i].CaminhoArquivo;

    if pArquivos[i].ArquivoId > 0 then
      _Biblioteca.AddNoVetorSemRepetir(vForm.FArquivosIdsOriginais, pArquivos[i].ArquivoId);
  end;
  vForm.sgArquivos.SetLinhasGridPorTamanhoVetor( vLinha );

  if Result.Ok(vForm.ShowModal) then begin
    // Se j� � um registro gravado, os arquivos j� foram inseridos pelo met�do Finalizar
    if pId > 0 then
      Exit;

    if vForm.sgArquivos.Cells[coNomeArquivo, 1] = '' then
      Exit;

    Result.Dados := vForm.getArquivos;
  end;
end;

procedure TFormArquivos.inserirArquivos(pCaminhoArquivo: string);
var
  vNomeArquivo: string;
begin
  vNomeArquivo := ExtractFileName(pCaminhoArquivo);

  inserirArquivos(
    0,
    0,
    vNomeArquivo,
    pCaminhoArquivo
  );
end;

procedure TFormArquivos.Finalizar(Sender: TObject);
var
  i: Integer;
  j: Integer;
  vAchou: Boolean;

  vRetBanco: RecRetornoBD;
  vItens: TArray<RecArquivos>;

  vArquivosExcluir: TArray<Integer>;
begin
  inherited;

  // Se n�o tem o registro pai gravado ainda n�o fazer nada.
  if FId = 0 then
    Exit;

  vAchou := False;
  vItens := getArquivos;

  // Se o usu�rio removeu todos os anexos vamos deletar todos eles do banco de dados
  if vItens = nil then
    vArquivosExcluir := FArquivosIdsOriginais
  else begin
     for i := Low(FArquivosIdsOriginais) to High(FArquivosIdsOriginais) do begin
       for j := Low(vItens) to High(vItens) do begin
         vAchou := FArquivosIdsOriginais[i] = vItens[j].ArquivoId;

         if vAchou then
           Break;
       end;

       if not vAchou then
         _Biblioteca.AddNoVetorSemRepetir(vArquivosExcluir, FArquivosIdsOriginais[i]);
     end;
  end;

  vRetBanco :=
    _Arquivos.AtualizarArquivos(
      Sessao.getConexaoBanco,
      FOrigem,
      FId,
      vItens,
      vArquivosExcluir,
      True,
      Sessao.getUsuarioLogado.cadastro_id
    );


  Sessao.AbortarSeHouveErro(vRetBanco);
end;

procedure TFormArquivos.FormCreate(Sender: TObject);
begin
  inherited;
  FArquivosIdsOriginais := nil;
end;

function TFormArquivos.getArquivos: TArray<RecArquivos>;
var
  i: Integer;
begin
  Result := nil;
  SetLength(Result, sgArquivos.RowCount -1);
  for i := 1 to sgArquivos.RowCount -1 do begin
    Result[i - 1].ArquivoId      := SFormatInt(sgArquivos.Cells[coArquivoId, i]);
    Result[i - 1].NomeArquivo    := sgArquivos.Cells[coNomeArquivo, i];
    Result[i - 1].CaminhoArquivo := sgArquivos.Cells[coCaminhoArquivo, i];
  end;
end;

procedure TFormArquivos.inserirArquivos(
  pArquivoId: Integer;
  pDataHoraCadastro: TDateTime;
  pNomeArquivo: string;
  pCaminhoArquivo: string
);
var
  vLinha: Integer;
begin
  if sgArquivos.Cells[coNomeArquivo, 1] = '' then
    vLinha := 1
  else begin
    vLinha := sgArquivos.RowCount;
    sgArquivos.RowCount := sgArquivos.RowCount + 1;
  end;

  sgArquivos.Cells[coArquivoId, vLinha]        := NFormatN(pArquivoId);
  sgArquivos.Cells[coDataHoraCadastro, vLinha] := DHFormatN(pDataHoraCadastro);
  sgArquivos.Cells[coNomeArquivo, vLinha]      := pNomeArquivo;
  sgArquivos.Cells[coCaminhoArquivo, vLinha]   := pCaminhoArquivo;
end;

procedure TFormArquivos.miDeletarArquivoFocadoClick(Sender: TObject);
begin
  inherited;
  if sgArquivos.Cells[coNomeArquivo, sgArquivos.Row] <> '' then
    sgArquivos.DeleteRow( sgArquivos.Row );
end;

procedure TFormArquivos.miInserirNovoArquivoClick(Sender: TObject);
var
  vDialog: TOpenDialog;
  vArquivo: file of Byte;
  vTamanhoArquivo: Integer;
begin
  inherited;

  vDialog := TOpenDialog.Create(Self);

  if vDialog.Execute then begin
    AssignFile(vArquivo, vDialog.FileName);
    Reset(vArquivo);
    vTamanhoArquivo := FileSize(vArquivo);
    CloseFile(vArquivo);

    if ( vTamanhoArquivo * 0.000001) > 3 then
      Exclamar('O arquivo n�o pode ser maior que 3 mb!')
    else
      inserirArquivos(vDialog.FileName);
  end;

  vDialog.Free;
end;

procedure TFormArquivos.Salvararquivo1Click(Sender: TObject);
var
  arquivo: TMemoryStream;
  nomeArquivo: string;
begin
  inherited;
  nomeArquivo := sgArquivos.Cells[coNomeArquivo, sgArquivos.Row];
  arquivo := _Arquivos.getArquivos(Sessao.getConexaoBanco, FId, FOrigem, nomeArquivo);

  if arquivo.Size > 0 then begin
    SaveDialog1.FileName := nomeArquivo;
    if SaveDialog1.Execute then begin
      arquivo.SaveToFile(SaveDialog1.FileName);
      Informar('Arquivo salvo com sucesso no caminho: ' + SaveDialog1.FileName);
    end;
  end;

end;

procedure TFormArquivos.sgArquivosDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if ACol in[coArquivoId] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgArquivos.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormArquivos.VerificarRegistro(Sender: TObject);
begin
  inherited;

end;

end.
