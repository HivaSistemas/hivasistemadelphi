unit ImpressaoMeiaPaginaDuplicataMercantil;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosGraficosMeiaPagina, _Biblioteca,
  QRExport, QRPDFFilt, QRWebFilt, QRCtrls, QuickRpt, Vcl.ExtCtrls, _Sessao, _ContasReceber,
  _RecordsOrcamentosVendas, _Orcamentos, _RecordsCadastros, _Clientes, _RecordsFinanceiros,
  System.StrUtils;

type
  TTipoMovimento = (tpOrcamento, tpAcumulado, tpFinanceiro);

  TFormImpressaoMeiaPaginaDuplicataMercantil = class(TFormHerancaRelatoriosGraficosMeiaPagina)
    qr2: TQRLabel;
    qrNomeCliente: TQRLabel;
    qr6: TQRLabel;
    qrCpfCnpjCliente: TQRLabel;
    qrCaptionVendedor: TQRLabel;
    qrVendedor: TQRLabel;
    qr14: TQRLabel;
    qrEnderecoCliente: TQRLabel;
    QRShape2: TQRShape;
    qrlCliente: TQRLabel;
    qrlCpfCnpjCliente: TQRLabel;
    qrInstrucao: TQRMemo;
    QRCompositeReport1: TQRCompositeReport;
    qr30: TQRLabel;
    qr29: TQRLabel;
    qr21: TQRLabel;
    qrCabecalhoMovimento: TQRLabel;
    qr19: TQRLabel;
    qr18: TQRLabel;
    qr17: TQRLabel;
    qr16: TQRLabel;
    qrDocumento: TQRLabel;
    qrValor: TQRLabel;
    qrDataEmissao: TQRLabel;
    qrDataVencimento: TQRLabel;
    qrPedido: TQRLabel;
    qrCodigoAltis: TQRLabel;
    qrParcela: TQRLabel;
    qrQtdeParcelas: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    NameclientTM: TQRLabel;
    CpfCnpjclienteTM: TQRLabel;
    QRLabel4: TQRLabel;
    qrEnderecoClienteTM: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    qrDocumentoTM: TQRLabel;
    qrValorTM: TQRLabel;
    qrDataEmissaoTM: TQRLabel;
    qrDataVencimentoTM: TQRLabel;
    qrParcelaTM: TQRLabel;
    qrQtdeParcelasTM: TQRLabel;
    qrInstrucaoTM: TQRMemo;
    QRShape1: TQRShape;
    qrlClienteTM: TQRLabel;
    qrlCpfCnpjClienteTM: TQRLabel;
    QRBand1: TQRBand;
    QRLabel9: TQRLabel;
    qrNomeEmpresaNaoFiscal: TQRLabel;
    qrendTM: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    qrbairroTM: TQRLabel;
    QRLabel25: TQRLabel;
    qrcidadeufTM: TQRLabel;
    QRLabel27: TQRLabel;
    qrCpfCnpjTM: TQRLabel;
    QRLabel29: TQRLabel;
    qrfoneempresaTM: TQRLabel;
    QRLabel33: TQRLabel;
    qremailTM: TQRLabel;
    QRLabel35: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
  end;

procedure Emitir(pMovimentoId: Integer; pTipoMovimento: TTipoMovimento);

implementation

{$R *.dfm}

uses _Empresas;

procedure Emitir(pMovimentoId: Integer; pTipoMovimento: TTipoMovimento);
var
  i: Integer;
  vImpressora: RecImpressora;
  vOrcamento: TArray<RecOrcamentos>;

  vCliente: TArray<RecClientes>;
  vTitulos: TArray<RecContasReceber>;
  vForm: TFormImpressaoMeiaPaginaDuplicataMercantil;
  vDadosEmpresa: TArray<RecEmpresas>;
begin
  vImpressora := Sessao.getImpressora(toComprovantePagtoFinanceiro);
  if vImpressora.CaminhoImpressora = '' then begin
    _Biblioteca.ImpressoraNaoConfigurada;
    Exit;
  end;

  //vImpressora.TipoImpressora := 'G';
  //vOrcamento := nil;

  if pTipoMovimento = tpOrcamento then begin
    vTitulos := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 10, [pMovimentoId]);
    if vTitulos = nil then begin
      _Biblioteca.Exclamar('Nenhum contas a receber foi encontrado para impress�o da duplicata!');
      Exit;
    end;

    vOrcamento := _Orcamentos.BuscarOrcamentos(Sessao.getConexaoBanco, 0, [pMovimentoId]);
    vCliente   := _Clientes.BuscarClientes(Sessao.getConexaoBanco, 0, [vOrcamento[0].cliente_id], False);
  end
  else if pTipoMovimento = tpAcumulado then begin
    vTitulos := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 11, [pMovimentoId]);
    if vTitulos = nil then begin
      _Biblioteca.Exclamar('Nenhum contas a receber foi encontrado para impress�o da duplicata!');
      Exit;
    end;

    vCliente   := _Clientes.BuscarClientes(Sessao.getConexaoBanco, 0, [vTitulos[0].CadastroId], False);
  end
  else begin
    vTitulos := _ContasReceber.BuscarContasReceber(Sessao.getConexaoBanco, 12, [pMovimentoId]);
    if vTitulos = nil then begin
      _Biblioteca.Exclamar('Nenhum contas a receber foi encontrado para impress�o da duplicata!');
      Exit;
    end;

    vCliente   := _Clientes.BuscarClientes(Sessao.getConexaoBanco, 0, [vTitulos[0].CadastroId], False);
  end;

  for i := Low(vTitulos) to High(vTitulos) do begin
   //vForm := TFormImpressaoMeiaPaginaDuplicataMercantil.Create(Application, vImpressora);
    vForm := TFormImpressaoMeiaPaginaDuplicataMercantil.Create(nil, vImpressora);

    if vForm.ImpressaoGrafica then
      vForm.PreencherCabecalho(Sessao.getEmpresaLogada.EmpresaId)
    else begin
      vDadosEmpresa := _Empresas.BuscarEmpresas(Sessao.getConexaoBanco, 0, [Sessao.getEmpresaLogada.EmpresaId]);

      vForm.qrNomeEmpresaNaoFiscal.Caption := IIfStr(vDadosEmpresa[0].NomeResumidoImpressoesNF <> '', vDadosEmpresa[0].NomeResumidoImpressoesNF, vDadosEmpresa[0].RazaoSocial);
      vForm.qrCpfCnpjTM.Caption            := vDadosEmpresa[0].Cnpj;
      vForm.qrendTM.Caption                := vDadosEmpresa[0].Logradouro + ' ' + vDadosEmpresa[0].Complemento + ' n� ' + vDadosEmpresa[0].Numero;
      vForm.qrbairroTM.caption             := vDadosEmpresa[0].NomeBairro;
      vForm.qrcidadeufTM.Caption           :=  vDadosEmpresa[0].NomeCidade + '/' + vDadosEmpresa[0].EstadoId;
      vForm.qrfoneempresaTM.Caption        := '' + vDadosEmpresa[0].TelefonePrincipal;
      vForm.qremailTM.Caption              := vDadosEmpresa[0].EMail;

    end;

    vForm.qrNomeCliente.Caption    := getInformacao(vCliente[0].cadastro_id, vCliente[0].RecCadastro.razao_social);
    vForm.NameclientTM.Caption    := getInformacao(vCliente[0].cadastro_id, vCliente[0].RecCadastro.razao_social);
    vForm.qrCpfCnpjCliente.Caption := vCliente[0].RecCadastro.cpf_cnpj;
    vForm.CpfCnpjclienteTM.Caption := vCliente[0].RecCadastro.cpf_cnpj;

    if pTipoMovimento = tpOrcamento then begin
      vForm.qrVendedor.Caption  := getInformacao(vOrcamento[0].vendedor_id, vOrcamento[0].nome_vendedor);
      vForm.qrVendedor.Enabled  := True;
      vForm.qrCaptionVendedor.Enabled := True;

      vForm.qrCabecalhoMovimento.Caption := 'Pedido';
    end
    else if pTipoMovimento = tpAcumulado then
      vForm.qrCabecalhoMovimento.Caption := 'Acumulado';

    vForm.qrEnderecoCliente.Caption :=
      vCliente[0].RecCadastro.logradouro + ' ' +
      vCliente[0].RecCadastro.complemento + ' nr ' +
      vCliente[0].RecCadastro.numero + ', ' +
      vCliente[0].RecCadastro.NomeBairro + ' ,' +
      vCliente[0].RecCadastro.NomeCidade + ' - ' +
      vCliente[0].RecCadastro.estado_id + '. CEP: ' +
      vCliente[0].RecCadastro.cep;

       vForm.qrEnderecoClienteTM.Caption :=
      vCliente[0].RecCadastro.logradouro + ' ' +
      vCliente[0].RecCadastro.complemento + ' nr ' +
      vCliente[0].RecCadastro.numero + ', ' +
      vCliente[0].RecCadastro.NomeBairro + ' ,' +
      vCliente[0].RecCadastro.NomeCidade + ' - ' +
      vCliente[0].RecCadastro.estado_id + '. CEP: ' +
      vCliente[0].RecCadastro.cep;

    vForm.qrInstrucao.Lines.Text := AnsiReplaceStr(vForm.qrInstrucao.Lines.Text, ':RAZAO_SOCIAL_EMPRESA', Sessao.getEmpresaLogada.RazaoSocial);
    vForm.qrInstrucaoTM.Lines.Text := AnsiReplaceStr(vForm.qrInstrucaoTM.Lines.Text, ':RAZAO_SOCIAL_EMPRESA', Sessao.getEmpresaLogada.RazaoSocial);

    vForm.qrlCliente.Caption          := 'Cliente.: ' + vCliente[0].RecCadastro.razao_social;
    vForm.qrlClienteTM.Caption        := 'Cliente.: ' + vCliente[0].RecCadastro.razao_social;
    vForm.qrlCpfCnpjCliente.Caption   := 'CPF/CNPJ: ' + vCliente[0].RecCadastro.cpf_cnpj;
    vForm.qrlCpfCnpjClienteTM.Caption := 'CPF/CNPJ: ' + vCliente[0].RecCadastro.cpf_cnpj;

    vForm.qrDocumento.Caption        := vTitulos[i].documento;
    vForm.qrDocumentoTM.Caption      := vTitulos[i].documento;
    vForm.qrValor.Caption            := NFormat(vTitulos[i].ValorDocumento);
    vForm.qrValorTM.Caption          := NFormat(vTitulos[i].ValorDocumento);
    vForm.qrDataEmissao.Caption      := DFormat(vTitulos[i].data_emissao);
    vForm.qrDataEmissaoTM.Caption    := DFormat(vTitulos[i].data_emissao);
    vForm.qrDataVencimento.Caption   := DFormat(vTitulos[i].data_vencimento);
    vForm.qrDataVencimentoTM.Caption := DFormat(vTitulos[i].data_vencimento);

    if pTipoMovimento = tpOrcamento then
      vForm.qrPedido.Caption         := NFormatN(vTitulos[i].orcamento_id)

    else
      vForm.qrPedido.Caption         := NFormatN(vTitulos[i].AcumuladoId);


    vForm.qrCodigoAltis.Caption      := NFormatN(vTitulos[i].ReceberId);
    vForm.qrParcela.Caption          := NFormatN(vTitulos[i].Parcela);
    vForm.qrParcelaTM.Caption        := NFormatN(vTitulos[i].Parcela);
    vForm.qrQtdeParcelas.Caption     := NFormatN(vTitulos[i].NumeroParcelas);
    vForm.qrQtdeParcelasTM.Caption   := NFormatN(vTitulos[i].NumeroParcelas);

    vForm.qrRelatorioA4.Preview;
//    vForm.qrRelatorioNF.Preview;
    vForm.Free;
  end;
end;

end.
