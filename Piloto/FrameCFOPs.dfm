inherited FrCFOPs: TFrCFOPs
  Width = 328
  Height = 82
  ExplicitWidth = 328
  ExplicitHeight = 82
  inherited sgPesquisa: TGridLuka
    Width = 303
    Height = 65
    ExplicitWidth = 303
    ExplicitHeight = 65
    ColWidths = (
      46
      309)
  end
  inherited CkAspas: TCheckBox
    Checked = True
    State = cbChecked
  end
  inherited CkPesquisaNumerica: TCheckBox
    Checked = False
    State = cbUnchecked
  end
  inherited CkMultiSelecao: TCheckBox
    Left = 120
    ExplicitLeft = 120
  end
  inherited PnTitulos: TPanel
    Width = 328
    ExplicitWidth = 328
    inherited lbNomePesquisa: TLabel
      Width = 32
      Caption = 'CFOPs'
      ExplicitWidth = 32
    end
    inherited pnSuprimir: TPanel
      Left = 223
      ExplicitLeft = 223
    end
  end
  object cbTipoCFOPs: TComboBoxLuka [6]
    Left = 119
    Top = 58
    Width = 98
    Height = 21
    BevelKind = bkFlat
    Style = csDropDownList
    Color = 16645629
    TabOrder = 6
    TabStop = False
    Visible = False
    Items.Strings = (
      'Entrada'
      'Sa'#237'da'
      'Todos')
    AsInt = 0
  end
  inherited pnPesquisa: TPanel
    Left = 303
    Height = 66
    TabOrder = 7
    ExplicitLeft = 303
    ExplicitHeight = 66
    DesignSize = (
      25
      66)
  end
  inherited ckSomenteAtivos: TCheckBox
    Left = 120
    TabOrder = 8
    ExplicitLeft = 120
  end
  object ckEntradaMercadorias: TCheckBox [9]
    Left = 223
    Top = 24
    Width = 97
    Height = 17
    Caption = 'Entr. Mercadorias'
    TabOrder = 9
    Visible = False
  end
  object ckEntradaConhecimentoFrete: TCheckBox [10]
    Left = 223
    Top = 40
    Width = 97
    Height = 17
    Caption = 'Entr. Conh.Frete'
    TabOrder = 10
    Visible = False
  end
  object ckEntradaServicos: TCheckBox [11]
    Left = 223
    Top = 56
    Width = 105
    Height = 17
    Caption = 'Entr. Servi'#231'os'
    TabOrder = 11
    Visible = False
  end
end
