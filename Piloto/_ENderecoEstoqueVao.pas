unit _EnderecoEstoqueVao;

Interface

uses
  _Conexao, _OperacoesBancoDados, _RecordsEspeciais, _Biblioteca, System.SysUtils, _Sessao, _RecordsCadastros;

{$M+}
type
  TEnderecoEstoqueVao = class(TOperacoes)
  public
    constructor Create(pConexao: TConexao);
  protected
    function getRecordEnderecoEstoqueVao: RecEnderecoEstoqueVao;
  end;

function AtualizarEnderecoEstoqueVao(
  pConexao: TConexao;
  pVaoId: Integer;
  pDescricao: string
): RecRetornoBD;

function BuscarEnderecoEstoqueVao(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEnderecoEstoqueVao>;

function ExcluirEnderecoEstoqueVao(
  pConexao: TConexao;
  pVaoId: Integer
): RecRetornoBD;

function getFiltros: TArray<RecFiltros>;

implementation

{ TEnderecoEstoqueVao }

function getFiltros: TArray<RecFiltros>;
begin
  SetLength(Result, 2);

  Result[0] :=
    _OperacoesBancoDados.NovoRecFiltro(
      '',
      False,
      0,
      'where VAO_ID = :P1'
    );

  Result[1] :=
    _OperacoesBancoDados.NovoRecFiltro(
      'Descri��o',
      True,
      0,
      'where DESCRICAO like :P1 || ''%'' ' +
      'order by ' +
      '  DESCRICAO '
    );
end;

constructor TEnderecoEstoqueVao.Create(pConexao: TConexao);
begin
  inherited Create(pConexao, 'ENDERECO_EST_VAO');

  FSql :=
    'select ' +
    '  VAO_ID, ' +
    '  DESCRICAO ' +
    'from ' +
    '  ENDERECO_EST_VAO ';

  setFiltros(getFiltros);

  AddColuna('VAO_ID', True);
  AddColuna('DESCRICAO');
end;

function TEnderecoEstoqueVao.getRecordEnderecoEstoqueVao: RecEnderecoEstoqueVao;
begin
  Result := RecEnderecoEstoqueVao.Create;
  Result.vao_id     := getInt('VAO_ID', True);
  Result.descricao  := getString('DESCRICAO');
end;

function AtualizarEnderecoEstoqueVao(
  pConexao: TConexao;
  pVaoId: Integer;
  pDescricao: string
): RecRetornoBD;
var
  t: TEnderecoEstoqueVao;
  vNovo: Boolean;
  seq: TSequencia;
begin
  Result.Iniciar;
  t := TEnderecoEstoqueVao.Create(pConexao);

  vNovo := pVaoId = 0;
  if vNovo then begin
    seq := TSequencia.Create(pConexao, 'SEQ_END_EST_VAO_ID');
    pVaoId := seq.getProximaSequencia;
    result.AsInt := pVaoId;
    seq.Free;
  end;

  try
    pConexao.IniciarTransacao;

    t.setInt('VAO_ID', pVaoId, True);
    t.setString('DESCRICAO', pDescricao);

    if vNovo then
      t.Inserir
    else
      t.Atualizar;

    pConexao.FinalizarTransacao;
  except
    on e: Exception do begin
      pConexao.VoltarTransacao;
      Result.TratarErro(e);
    end;
  end;

  t.Free;
end;

function BuscarEnderecoEstoqueVao(
  pConexao: TConexao;
  pIndice: ShortInt;
  pFiltros: array of Variant
): TArray<RecEnderecoEstoqueVao>;
var
  i: Integer;
  t: TEnderecoEstoqueVao;
begin
  Result := nil;
  t := TEnderecoEstoqueVao.Create(pConexao);

  if t.Pesquisar(pIndice, pFiltros) then begin
    SetLength(Result, t.getQuantidadeRegistros);
    for i := 0 to t.getQuantidadeRegistros - 1 do begin
      Result[i] := t.getRecordEnderecoEstoqueVao;
      t.ProximoRegistro;
    end;
  end;

  t.Free;
end;

function ExcluirEnderecoEstoqueVao(
  pConexao: TConexao;
  pVaoId: Integer
): RecRetornoBD;
var
  t: TEnderecoEstoqueVao;
begin
  Result.TeveErro := False;
  t := TEnderecoEstoqueVao.Create(pConexao);

  try
    t.setInt('VAO_ID', pVaoId, True);

    t.Excluir;
  except
    on e: Exception do begin
      Result.TeveErro := True;
      Result.MensagemErro := e.Message;
    end;
  end;

  t.Free;
end;

end.
