unit BuscarPercentual;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaFinalizar, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.StdCtrls, EditLuka, _Biblioteca;

type
  TFormBuscarPercentual = class(TFormHerancaFinalizar)
    eVersao: TEditLuka;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BuscarDadosPercentual(caption: string = ''): TRetornoTelaFinalizar<Double>;

implementation

{$R *.dfm}

function BuscarDadosPercentual(caption: string = ''): TRetornoTelaFinalizar<Double>;
var
  vForm: TFormBuscarPercentual;
begin
  vForm := TFormBuscarPercentual.Create(Application);
  if (caption <> '') then
    vForm.Caption := caption;

  if Result.Ok(vForm.ShowModal) then
    Result.Dados := vForm.eVersao.AsDouble;

  FreeAndNil(vForm);
end;

end.
