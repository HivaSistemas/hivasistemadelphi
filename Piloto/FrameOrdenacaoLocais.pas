unit FrameOrdenacaoLocais;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _FrameHerancaPrincipal, Vcl.StdCtrls, _Biblioteca,
  StaticTextLuka, Vcl.Grids, GridLuka, Vcl.ExtCtrls, Vcl.Buttons, PesquisaLocaisProdutos,
  _RecordsCadastros, BuscaDados, _LocaisProdutos, _Sessao;

type
  TFrOrdenacaoLocais = class(TFrameHerancaPrincipal)
    sbSubir: TSpeedButton;
    sbDescer: TSpeedButton;
    Panel1: TPanel;
    sgLocais: TGridLuka;
    StaticTextLuka1: TStaticTextLuka;
    sbAdicionarLocais: TSpeedButton;
    procedure sbDescerClick(Sender: TObject);
    procedure sbSubirClick(Sender: TObject);
    procedure sgLocaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sbAdicionarLocaisClick(Sender: TObject);
    procedure sgLocaisKeyPress(Sender: TObject; var Key: Char);
    procedure sgLocaisKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;

    procedure SetFocus; override;
    procedure Clear; override;
    procedure SomenteLeitura(pValue: Boolean); override;
    procedure Modo(pEditando: Boolean; pLimpar: Boolean = True); override;
    function EstaVazio: Boolean; override;
    function TrazerArrayLocais: TArray<Integer>;
    procedure AddLocal(pLocalId: Integer; pNome: string);
  end;

implementation

{$R *.dfm}

const
  coLocalId   = 0;
  coNomeLocal = 1;

procedure TFrOrdenacaoLocais.AddLocal(pLocalId: Integer; pNome: string);
var
  vLinha: Integer;
begin
  vLinha := sgLocais.Localizar([coLocalId], [NFormat(pLocalId)]);
  if vLinha > -1 then begin
    sgLocais.Row := vLinha;
    SetarFoco(sgLocais);
    Exit;
  end;

  if sgLocais.Cells[coLocalId, 0] = '' then
    vLinha := 0
  else
    vLinha := sgLocais.RowCount;

  sgLocais.Cells[coLocalId, vLinha]   := NFormat(pLocalId);
  sgLocais.Cells[coNomeLocal, vLinha] := pNome;
  sgLocais.RowCount := vLinha + 1;

  sgLocais.Row := vLinha;
  SetarFoco(sgLocais);
end;

procedure TFrOrdenacaoLocais.Clear;
begin
  inherited;
  sgLocais.ClearGrid();
end;

constructor TFrOrdenacaoLocais.Create(AOwner: TComponent);
begin
  inherited;
  sgLocais.OnKeyPress := sgLocaisKeyPress;
end;

function TFrOrdenacaoLocais.EstaVazio: Boolean;
begin
  Result := sgLocais.Cells[coLocalId, 0] = '';
end;

procedure TFrOrdenacaoLocais.Modo(pEditando, pLimpar: Boolean);
begin
  inherited;

  _Biblioteca.Habilitar([
    sgLocais,
    sbAdicionarLocais,
    sbSubir,
    sbDescer],
    pEditando,
    pLimpar
  );

  if pLimpar then
  	Clear;
end;

procedure TFrOrdenacaoLocais.sbAdicionarLocaisClick(Sender: TObject);
var
  vLocal: RecLocaisProdutos;
begin
  inherited;
  vLocal := RecLocaisProdutos(PesquisaLocaisProdutos.PesquisarLocalProduto);
  if vLocal = nil then
    Exit;

  AddLocal(vLocal.local_id, vLocal.nome);
end;

procedure TFrOrdenacaoLocais.sbDescerClick(Sender: TObject);
begin
  inherited;
  if sgLocais.Row + 1 < sgLocais.RowCount then begin
    sgLocais.TrocaRow(sgLocais.Row + 1, sgLocais.Row);
    sgLocais.Row := sgLocais.Row + 1;
  end;
end;

procedure TFrOrdenacaoLocais.sbSubirClick(Sender: TObject);
begin
  inherited;
  if sgLocais.Row - 1 > -1 then begin
    sgLocais.TrocaRow(sgLocais.Row -1, sgLocais.Row);
    sgLocais.Row := sgLocais.Row -1;
  end;
end;

procedure TFrOrdenacaoLocais.SetFocus;
begin
  inherited;
  SetarFoco(sgLocais);
end;

procedure TFrOrdenacaoLocais.sgLocaisDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;
  if Acol = coLocalId then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgLocais.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFrOrdenacaoLocais.sgLocaisKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
    sgLocais.DeleteRow(sgLocais.Row);
end;

procedure TFrOrdenacaoLocais.sgLocaisKeyPress(Sender: TObject; var Key: Char);
var
  vChaveDigitada: string;
  vLocal: TArray<RecLocaisProdutos>;
begin
  inherited;
  if CharInSet(Key, ['0'..'9']) then begin
    vChaveDigitada := BuscaDados.BuscarDados(tiNumerico, 'Busca digitada', Key);
    if SFormatInt(vChaveDigitada) > 0 then begin
      vLocal := _LocaisProdutos.BuscarLocaisProdutos(Sessao.getConexaoBanco, 0, [SFormatInt(vChaveDigitada)]);
      if vLocal <> nil then
        AddLocal(vLocal[0].local_id, vLocal[0].nome);
    end
    else
      SetFocus;
  end;
end;

procedure TFrOrdenacaoLocais.SomenteLeitura(pValue: Boolean);
begin
  inherited;

end;

function TFrOrdenacaoLocais.TrazerArrayLocais: TArray<Integer>;
var
  i: Integer;
begin
  SetLength(Result, sgLocais.RowCount);
  for i := 0 to sgLocais.RowCount -1 do
    Result[i] := SFormatInt(sgLocais.Cells[coLocalId, i]);
end;

end.
