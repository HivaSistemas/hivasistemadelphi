unit RelacaoProducoesEstoque;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, _HerancaRelatoriosPageControl,
  Vcl.Grids, GridLuka, Vcl.StdCtrls, StaticTextLuka, Vcl.ComCtrls, CheckBoxLuka,
  Vcl.Buttons, Vcl.ExtCtrls, FrameProdutos, FrameNumeros, FrameFuncionarios,
  FrameDataInicialFinal, _FrameHerancaPrincipal, _FrameHenrancaPesquisas,
  FrameEmpresas, _ProducaoEstoque, _ProducaoEstoqueItens, _RecordsEstoques, _Biblioteca,
  _Sessao;

type
  TFormRelacaoProducoesEstoque = class(TFormHerancaRelatoriosPageControl)
    StaticTextLuka1: TStaticTextLuka;
    sgAjustes: TGridLuka;
    StaticTextLuka2: TStaticTextLuka;
    sgItens: TGridLuka;
    FrEmpresas: TFrEmpresas;
    FrDataCadastro: TFrDataInicialFinal;
    FrUsuarioInsercao: TFrFuncionarios;
    FrCodigoAjuste: TFrNumeros;
    FrProdutos: TFrProdutos;
    Panel1: TPanel;
    st4: TStaticText;
    st1: TStaticText;
    stTotalSaidas: TStaticText;
    stTotalEntradas: TStaticText;
    procedure sgAjustesClick(Sender: TObject);
    procedure sgAjustesDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgAjustesGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgItensDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure sgItensGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    FItens: TArray<RecProducaoEstoqueItens>;
  protected
    procedure Carregar(Sender: TObject); override;
  public
    { Public declarations }
  end;

var
  FormRelacaoProducoesEstoque: TFormRelacaoProducoesEstoque;

implementation

const
  coProducaoId      = 0;
  coEmpresa         = 1;
  coDataHoraAjuste  = 2;
  coUsuarioAjuste   = 3;

  (* Itens *)
  ciProdutoId         = 0;
  ciNome              = 1;
  ciMarca             = 2;
  ciNaturezaAnalitico = 3;
  ciQuantidade        = 4;
  ciPrecoUnitario     = 5;
  ciValorTotal        = 6;
  ciNatureza          = 7;

{$R *.dfm}

{ TFormHerancaRelatoriosPageControl1 }

procedure TFormRelacaoProducoesEstoque.Carregar(Sender: TObject);
var
  i: Integer;
  vSql: string;
  vProducoes: TArray<RecProducaoEstoque>;
  vProducoesIds: TArray<Integer>;
begin
  inherited;
  sgAjustes.ClearGrid();
  sgItens.ClearGrid();

  FItens := nil;
  vProducoesIds := nil;

  if not FrCodigoAjuste.EstaVazio then
    _Biblioteca.WhereOuAnd(vSql, FrCodigoAjuste.TrazerFiltros('PRO.PRODUCAO_ESTOQUE_ID'))
  else begin
    if not FrEmpresas.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrEmpresas.getSqlFiltros('PRO.EMPRESA_ID'));

    if not FrProdutos.EstaVazio then
      _Biblioteca.WhereOuAnd(
        vSql,
        ' PRO.PRODUCAO_ESTOQUE_ID in(select distinct PRODUCAO_ESTOQUE_ID from PRODUCAO_ESTOQUE_ITENS where ' + FrProdutos.getSqlFiltros('PRODUTO_ID') + ') '
      );

    if not FrUsuarioInsercao.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrUsuarioInsercao.getSqlFiltros('PRO.USUARIO_PRODUCAO_ID') );

    if not FrDataCadastro.EstaVazio then
      _Biblioteca.WhereOuAnd(vSql, FrDataCadastro.getSqlFiltros('trunc(PRO.DATA_HORA_PRODUCAO)') );
  end;

  vSql := vSql + 'order by PRO.PRODUCAO_ESTOQUE_ID ';

  vProducoes := _ProducaoEstoque.BuscarProducaoEstoqueComando(Sessao.getConexaoBanco, vSql);
  if vProducoes = nil then begin
    NenhumRegistro;
    Exit;
  end;

  for i := Low(vProducoes) to High(vProducoes) do begin
    sgAjustes.Cells[coProducaoId, i + 1] := NFormatN( vProducoes[i].producao_estoque_id );
    sgAjustes.Cells[coEmpresa, i + 1]         := NFormatN( vProducoes[i].EmpresaId ) + ' - ' + vProducoes[i].NomeFantasia;
    sgAjustes.Cells[coDataHoraAjuste, i + 1]  := DHFormat( vProducoes[i].data_hora_producao );
    sgAjustes.Cells[coUsuarioAjuste, i + 1]   := NFormatN( vProducoes[i].usuario_producao_id ) + ' - ' + vProducoes[i].NomeUsuarioProducao;

    _Biblioteca.AddNoVetorSemRepetir(vProducoesIds, vProducoes[i].producao_estoque_id);
  end;
  sgAjustes.RowCount := _Biblioteca.SetLinhasGridPorTamanhoVetor( Length(vProducoes) );

  FItens := _ProducaoEstoqueItens.BuscarProducaoEstoqueItensComando(
    Sessao.getConexaoBanco,
    'where ' + FiltroInInt('ITE.PRODUCAO_ESTOQUE_ID', vProducoesIds)
  );

  sgAjustesClick(nil);
  SetarFoco(sgAjustes);
end;

procedure TFormRelacaoProducoesEstoque.sgAjustesClick(Sender: TObject);
var
  i: Integer;
  vLinha: Integer;
  vProducaoId: Integer;
  vTotalEntradas: Double;
  vTotalSaidas: Double;
begin
  inherited;
  vLinha := 0;
  vTotalEntradas := 0;
  vTotalSaidas := 0;
  sgItens.ClearGrid();
  vProducaoId := SFormatInt(sgAjustes.Cells[coProducaoId, sgAjustes.Row]);
  for i := Low(FItens) to High(FItens) do begin
    if vProducaoId <> FItens[i].Producao_estoque_id then
      Continue;

    Inc(vLinha);

    sgItens.Cells[ciProdutoId, vLinha]     := NFormat(FItens[i].produto_id);
    sgItens.Cells[ciNome, vLinha]          := FItens[i].NomeProduto;
    sgItens.Cells[ciMarca, vLinha]         := NFormat(FItens[i].MarcaId) + ' - ' + FItens[i].NomeMarca;
    sgItens.Cells[ciNaturezaAnalitico, vLinha] := FItens[i].NaturezaAnalitico;
    sgItens.Cells[ciNatureza, vLinha]      := FItens[i].Natureza;
    sgItens.Cells[ciQuantidade, vLinha]    := NFormatEstoque(FItens[i].Quantidade);
    sgItens.Cells[ciPrecoUnitario, vLinha] := NFormat(FItens[i].PrecoUnitario);
    sgItens.Cells[ciValorTotal, vLinha]    := NFormat(FItens[i].PrecoUnitario * FItens[i].Quantidade);

    if FItens[i].Natureza = 'E' then
      vTotalEntradas := vTotalEntradas + (FItens[i].PrecoUnitario * FItens[i].Quantidade)
    else
      vTotalSaidas := vTotalSaidas + (FItens[i].PrecoUnitario * FItens[i].Quantidade);

  end;
  sgItens.SetLinhasGridPorTamanhoVetor( vLinha );
  stTotalSaidas.Caption := NFormat(vTotalSaidas);
  stTotalEntradas.Caption := NFormat(vTotalEntradas);
end;

procedure TFormRelacaoProducoesEstoque.sgAjustesDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in [coProducaoId] then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgAjustes.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoProducoesEstoque.sgAjustesGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;
end;

procedure TFormRelacaoProducoesEstoque.sgItensDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  vAlinhamento: TAlignment;
begin
  inherited;

  if ACol in[ciNaturezaAnalitico] then
    vAlinhamento := taCenter
  else if ACol in[
    ciProdutoId,
    ciQuantidade,
    ciPrecoUnitario,
    ciValorTotal]
  then
    vAlinhamento := taRightJustify
  else
    vAlinhamento := taLeftJustify;

  sgItens.MergeCells([ARow, ACol], [ARow, ACol], [ARow, ACol], vAlinhamento, Rect);
end;

procedure TFormRelacaoProducoesEstoque.sgItensGetCellColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  inherited;
  if ARow = 0 then
    Exit;

  if ACol = ciNaturezaAnalitico then begin
    AFont.Style := [fsBold];
    if sgItens.Cells[ciNatureza, ARow] = 'E' then
      AFont.Color := clBlue
    else if sgItens.Cells[ciNatureza, ARow] = 'S' then
      AFont.Color := clGreen
    else
      AFont.Color := $000080FF;
  end;
end;

end.
