inherited FormRelacaoCurvaABC: TFormRelacaoCurvaABC
  Caption = 'Curva ABC'
  ExplicitTop = -44
  PixelsPerInch = 96
  TextHeight = 14
  inherited pcDados: TPageControl
    ActivePage = tsResultado
    inherited tsFiltros: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object Label1: TLabel
        Left = 3
        Top = 6
        Width = 58
        Height = 14
        Caption = '% Classe A'
      end
      object Label2: TLabel
        Left = 86
        Top = 6
        Width = 58
        Height = 14
        Caption = '% Classe B'
      end
      object Label3: TLabel
        Left = 169
        Top = 6
        Width = 57
        Height = 14
        Caption = '% Classe C'
      end
      object Label4: TLabel
        Left = 252
        Top = 6
        Width = 59
        Height = 14
        Caption = '% Classe D'
      end
      inline FrClientes: TFrClientes
        Left = 1
        Top = 364
        Width = 403
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        Visible = False
        ExplicitLeft = 1
        ExplicitTop = 364
        ExplicitWidth = 403
        inherited sgPesquisa: TGridLuka
          Width = 378
          ExplicitWidth = 378
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 403
          ExplicitWidth = 403
          inherited lbNomePesquisa: TLabel
            Width = 45
            ExplicitWidth = 45
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 298
            ExplicitLeft = 298
          end
        end
        inherited pnPesquisa: TPanel
          Left = 378
          ExplicitLeft = 378
        end
      end
      inline FrEmpresas: TFrEmpresas
        Left = 0
        Top = 62
        Width = 404
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        TabStop = True
        ExplicitTop = 62
        ExplicitWidth = 404
        inherited sgPesquisa: TGridLuka
          Width = 379
          Align = alNone
          ExplicitWidth = 379
        end
        inherited PnTitulos: TPanel
          Width = 404
          ExplicitWidth = 404
          inherited lbNomePesquisa: TLabel
            Width = 53
            ExplicitWidth = 53
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 299
            ExplicitLeft = 299
            inherited ckSuprimir: TCheckBox
              Left = 5
              Top = -1
              Width = 63
              ExplicitLeft = 5
              ExplicitTop = -1
              ExplicitWidth = 63
            end
          end
        end
        inherited pnPesquisa: TPanel
          Left = 384
          Width = 34
          Align = alNone
          ExplicitLeft = 384
          ExplicitWidth = 34
          inherited sbPesquisa: TSpeedButton
            Left = -2
            Top = 6
            ExplicitLeft = -2
            ExplicitTop = 6
          end
        end
      end
      inline FrDataCadastro: TFrDataInicialFinal
        Left = 416
        Top = 96
        Width = 217
        Height = 68
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 2
        TabStop = True
        Visible = False
        ExplicitLeft = 416
        ExplicitTop = 96
        ExplicitWidth = 217
        ExplicitHeight = 68
        inherited Label1: TLabel
          Width = 93
          Height = 14
          ExplicitWidth = 93
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      inline FrDataRecebimento: TFrDataInicialFinal
        Left = 416
        Top = 16
        Width = 217
        Height = 68
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 3
        TabStop = True
        ExplicitLeft = 416
        ExplicitTop = 16
        ExplicitWidth = 217
        ExplicitHeight = 68
        inherited Label1: TLabel
          Width = 115
          Height = 14
          Caption = 'Data de recebimento'
          ExplicitWidth = 115
          ExplicitHeight = 14
        end
        inherited lb1: TLabel
          Width = 18
          Height = 14
          ExplicitWidth = 18
          ExplicitHeight = 14
        end
        inherited eDataFinal: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
        inherited eDataInicial: TEditLukaData
          Height = 22
          ExplicitHeight = 22
        end
      end
      object percClasseA: TEditLuka
        Left = 3
        Top = 26
        Width = 77
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 4
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object percClasseB: TEditLuka
        Left = 86
        Top = 26
        Width = 77
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 5
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object percClasseC: TEditLuka
        Left = 169
        Top = 26
        Width = 77
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 6
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      object percClasseD: TEditLuka
        Left = 252
        Top = 26
        Width = 77
        Height = 22
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 7
        TipoCampo = tcNumerico
        ApenasPositivo = True
        AsInt = 0
        CasasDecimais = 0
        NaoAceitarEspaco = False
      end
      inline FrMarca: TFrMarcas
        Left = 0
        Top = 149
        Width = 404
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Calibri'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 8
        TabStop = True
        ExplicitTop = 149
        ExplicitWidth = 404
        inherited sgPesquisa: TGridLuka
          Width = 379
          TabOrder = 1
          ExplicitWidth = 379
        end
        inherited CkAspas: TCheckBox
          TabOrder = 2
        end
        inherited CkFiltroDuplo: TCheckBox
          TabOrder = 4
        end
        inherited CkPesquisaNumerica: TCheckBox
          TabOrder = 5
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        inherited PnTitulos: TPanel
          Width = 404
          TabOrder = 0
          ExplicitWidth = 404
          inherited lbNomePesquisa: TLabel
            Width = 33
            Caption = 'Marca'
            ExplicitWidth = 33
            ExplicitHeight = 14
          end
          inherited pnSuprimir: TPanel
            Left = 299
            ExplicitLeft = 299
          end
        end
        inherited pnPesquisa: TPanel
          Left = 379
          ExplicitLeft = 379
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
      end
      inline FrProdutos: TFrProdutos
        Left = 0
        Top = 240
        Width = 404
        Height = 81
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Color = clWhite
        ParentBackground = False
        ParentColor = False
        TabOrder = 9
        TabStop = True
        ExplicitTop = 240
        ExplicitWidth = 404
        inherited sgPesquisa: TGridLuka
          Width = 379
          ExplicitWidth = 379
        end
        inherited CkMultiSelecao: TCheckBox
          Checked = True
          State = cbChecked
        end
        inherited PnTitulos: TPanel
          Width = 404
          ExplicitWidth = 404
          inherited lbNomePesquisa: TLabel
            Width = 48
            ExplicitWidth = 48
            ExplicitHeight = 14
          end
          inherited CkChaveUnica: TCheckBox
            Checked = False
            State = cbUnchecked
          end
          inherited pnSuprimir: TPanel
            Left = 299
            ExplicitLeft = 299
          end
        end
        inherited pnPesquisa: TPanel
          Left = 379
          ExplicitLeft = 379
          inherited sbPesquisa: TSpeedButton
            Visible = False
          end
        end
        inherited ckFiltroExtra: TCheckBox
          Checked = True
          State = cbChecked
        end
      end
    end
    inherited tsResultado: TTabSheet
      ExplicitLeft = 4
      ExplicitTop = 25
      ExplicitWidth = 884
      ExplicitHeight = 518
      object pcResultado: TPageControl
        Left = 0
        Top = 0
        Width = 884
        Height = 518
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        ActivePage = tsPorProdutosQuantidade
        Align = alClient
        TabOrder = 0
        object tsPorProdutosValorTotal: TTabSheet
          Margins.Left = 0
          Margins.Top = 0
          Margins.Right = 0
          Margins.Bottom = 0
          Caption = 'Por valor de produtos'
          object sgProdutosValor: TGridLuka
            Left = 0
            Top = 0
            Width = 876
            Height = 489
            Align = alClient
            ColCount = 10
            DefaultRowHeight = 19
            DrawingStyle = gdsClassic
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect, goFixedRowClick]
            TabOrder = 0
            OnDrawCell = sgProdutosValorDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clMenuHighlight
            HCol.Strings = (
              'Ranking'
              'Produto'
              'Nome'
              'Pre'#231'o unit'#225'rio'
              'Quantidade'
              'Und.'
              'Custo total'
              'Valor Total'
              '%'
              'Classifica'#231#227'o')
            OnGetCellColor = sgProdutosValorGetCellColor
            Grid3D = False
            RealColCount = 15
            OrdenarOnClick = True
            AtivarPopUpSelecao = False
            ColWidths = (
              54
              62
              260
              84
              83
              39
              116
              116
              64
              92)
          end
        end
        object tsPorProdutosQuantidade: TTabSheet
          Caption = 'Por qdt de produtos'
          ImageIndex = 2
          object sgProdutosQuantidade: TGridLuka
            Left = 0
            Top = 0
            Width = 876
            Height = 489
            Align = alClient
            ColCount = 7
            DefaultRowHeight = 19
            DrawingStyle = gdsClassic
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect, goFixedRowClick]
            TabOrder = 0
            OnDrawCell = sgProdutosQuantidadeDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clMenuHighlight
            HCol.Strings = (
              'Ranking'
              'Produto'
              'Nome'
              'Und.'
              'Quantidade'
              '%'
              'Classifica'#231#227'o')
            OnGetCellColor = sgProdutosQuantidadeGetCellColor
            Grid3D = False
            RealColCount = 15
            OrdenarOnClick = True
            AtivarPopUpSelecao = False
            ColWidths = (
              62
              62
              308
              37
              82
              67
              75)
          end
        end
        object tsSinteticoTipoCobranca: TTabSheet
          Caption = 'Por valor de clientes'
          Enabled = False
          ImageIndex = 1
          object sgClientesValor: TGridLuka
            Left = 0
            Top = 0
            Width = 876
            Height = 489
            Align = alClient
            ColCount = 7
            DefaultRowHeight = 19
            DrawingStyle = gdsClassic
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect, goFixedRowClick]
            TabOrder = 0
            OnDrawCell = sgClientesValorDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clMenuHighlight
            HCol.Strings = (
              'Ranking'
              'Cliente'
              'Nome'
              'Qtde. vendas'
              'Valor Total'
              '%'
              'Classifica'#231#227'o')
            OnGetCellColor = sgClientesValorGetCellColor
            Grid3D = False
            RealColCount = 15
            OrdenarOnClick = True
            AtivarPopUpSelecao = False
            ColWidths = (
              60
              62
              233
              105
              110
              64
              78)
          end
        end
        object ts1: TTabSheet
          Caption = 'Por qdt de clientes'
          Enabled = False
          ImageIndex = 3
          object sgClientesQuantidade: TGridLuka
            Left = 0
            Top = 0
            Width = 876
            Height = 489
            Align = alClient
            ColCount = 6
            DefaultRowHeight = 19
            DrawingStyle = gdsClassic
            FixedColor = 15395562
            FixedCols = 0
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect, goFixedRowClick]
            TabOrder = 0
            OnDrawCell = sgClientesQuantidadeDrawCell
            IncrementExpandCol = 0
            IncrementExpandRow = 0
            CorLinhaFoco = 38619
            CorFundoFoco = 16774625
            CorLinhaDesfoque = 14869218
            CorFundoDesfoque = 16382457
            CorSuperiorCabecalho = clWhite
            CorInferiorCabecalho = 13553358
            CorSeparadorLinhas = 12040119
            CorColunaFoco = clMenuHighlight
            HCol.Strings = (
              'Ranking'
              'Cliente'
              'Nome'
              'Qtde. vendas'
              '%'
              'Classifica'#231#227'o')
            OnGetCellColor = sgClientesQuantidadeGetCellColor
            Grid3D = False
            RealColCount = 15
            OrdenarOnClick = True
            AtivarPopUpSelecao = False
            ColWidths = (
              62
              62
              233
              105
              69
              87)
          end
        end
      end
    end
  end
  object frxReport: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 45410.818617372680000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 360
    Top = 184
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = dstValorProdutos
        DataSetName = 'frxdstValorProdutos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Top = 70.811070000000000000
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000001000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000010000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133889999999990000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000001000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000010000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133889999999990000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000001000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000001000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000010000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000010000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133889999999990000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133889999999990000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000001000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 2.779530000000000000
          Top = 70.811070000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Ranking')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 49.354360000000000000
          Top = 70.811070000000000000
          Width = 226.771800000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 278.464750000000000000
          Top = 70.811070000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o unit'#225'rio')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 522.575140000000100000
          Top = 70.811070000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Valor total')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 648.638220000000000000
          Top = 70.811070000000000000
          Width = 68.031540000000010000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Classifica'#231#227'o')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo32: TfrxMemoView
          Left = 454.323130000000000000
          Top = 70.811070000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Custo total')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 354.275820000000000000
          Top = 70.811070000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 419.307360000000000000
          Top = 70.811070000000000000
          Width = 30.236240000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Und.')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 591.386210000000000000
          Top = 70.811070000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '%')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 166.299320000000000000
        Width = 718.110700000000000000
        DataSet = dstValorProdutos
        DataSetName = 'frxdstValorProdutos'
        RowCount = 0
        object frxdstTranscodigoProd: TfrxMemoView
          Left = 2.779530000000000000
          Top = 1.000000000000000000
          Width = 45.354330708661410000
          Height = 11.338590000000000000
          DataField = 'Ranking'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstValorProdutos."Ranking"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo25: TfrxMemoView
          Left = 49.511811020000010000
          Top = 1.000000000000000000
          Width = 226.771653540000000000
          Height = 11.338590000000000000
          DataField = 'Produto'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstValorProdutos."Produto"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 278.551181100000000000
          Top = 1.000000000000000000
          Width = 71.811023620000000000
          Height = 11.338590000000000000
          DataField = 'PrecoUnitario'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstValorProdutos."PrecoUnitario"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 522.574803150000000000
          Top = 1.000000000000000000
          Width = 64.251968500000000000
          Height = 11.338590000000000000
          DataField = 'ValorTotal'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstValorProdutos."ValorTotal"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 648.566929130000000000
          Top = 1.000000000000000000
          Width = 68.031496060000000000
          Height = 11.338590000000000000
          DataField = 'Classificacao'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstValorProdutos."Classificacao"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo33: TfrxMemoView
          Left = 454.299212600000000000
          Top = 1.000000000000000000
          Width = 64.251968500000000000
          Height = 11.338590000000000000
          DataField = 'CustoTotal'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstValorProdutos."CustoTotal"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo15: TfrxMemoView
          Left = 354.141732280000000000
          Top = 1.000000000000000000
          Width = 60.472440940000000000
          Height = 11.338590000000000000
          DataField = 'Quantidade'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstValorProdutos."Quantidade"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 419.149606300000000000
          Top = 1.000000000000000000
          Width = 30.236220470000000000
          Height = 11.338590000000000000
          DataField = 'Unidade'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstValorProdutos."Unidade"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 591.228346460000000000
          Top = 1.000000000000000000
          Width = 52.913385830000000000
          Height = 11.338590000000000000
          DataField = 'Percentual'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstValorProdutos."Percentual"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 49.133890000000000000
          Top = 1.000000000000000000
          Width = 226.771653540000000000
          Height = 11.338590000000000000
          DataField = 'ProdutoNegrito'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxdstValorProdutos."ProdutoNegrito"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 355.275820000000000000
          Top = 1.000000000000000000
          Width = 60.472440940000000000
          Height = 11.338590000000000000
          DataField = 'QuantidadeNegrito'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstValorProdutos."QuantidadeNegrito"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 453.543600000000000000
          Top = 1.000000000000000000
          Width = 64.251968500000000000
          Height = 11.338590000000000000
          DataField = 'CustoTotalNegrito'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstValorProdutos."CustoTotalNegrito"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo31: TfrxMemoView
          Left = 522.575140000000100000
          Top = 1.000000000000000000
          Width = 64.251968500000000000
          Height = 11.338590000000000000
          DataField = 'ValorTotalNegrito'
          DataSet = dstValorProdutos
          DataSetName = 'frxdstValorProdutos'
          DisplayFormat.FormatStr = '#,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstValorProdutos."ValorTotalNegrito"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 41.952782760000000000
        Top = 241.889920000000000000
        Width = 718.110700000000000000
        object Line1: TfrxLineView
          Top = 3.779529999999994000
          Width = 718.110700000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
    end
  end
  object dstValorProdutos: TfrxDBDataset
    UserName = 'frxdstValorProdutos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Ranking=Ranking'
      'Produto=Produto'
      'PrecoUnitario=PrecoUnitario'
      'Quantidade=Quantidade'
      'Unidade=Unidade'
      'CustoTotal=CustoTotal'
      'ValorTotal=ValorTotal'
      'Percentual=Percentual'
      'Classificacao=Classificacao'
      'QuantidadeNegrito=QuantidadeNegrito'
      'CustoTotalNegrito=CustoTotalNegrito'
      'ValorTotalNegrito=ValorTotalNegrito'
      'ProdutoNegrito=ProdutoNegrito')
    DataSet = cdsValorProdutos
    BCDToCurrency = False
    Left = 360
    Top = 248
  end
  object cdsValorProdutos: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Ranking'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Produto'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'PrecoUnitario'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Quantidade'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Unidade'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CustoTotal'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ValorTotal'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Percentual'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Classificacao'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ProdutoNegrito'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'QuantidadeNegrito'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CustoTotalNegrito'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ValorTotalNegrito'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 360
    Top = 313
    object cdsValorRanking: TStringField
      FieldName = 'Ranking'
    end
    object CdsValorProduto: TStringField
      FieldName = 'Produto'
      Size = 60
    end
    object cdsValorPrecoUnitario: TStringField
      FieldName = 'PrecoUnitario'
    end
    object cdsValorQuantidade: TStringField
      FieldName = 'Quantidade'
    end
    object cdsValorUnidade: TStringField
      FieldName = 'Unidade'
    end
    object cdsValorCustoTotal: TStringField
      FieldName = 'CustoTotal'
    end
    object cdsValorTotal: TStringField
      FieldName = 'ValorTotal'
    end
    object cdsValorPercentual: TStringField
      FieldName = 'Percentual'
    end
    object cdsValorClassificacao: TStringField
      FieldName = 'Classificacao'
    end
    object cdsValorQuantidadeNegrito: TStringField
      FieldName = 'QuantidadeNegrito'
    end
    object cdsValorCustoTotalNegrito: TStringField
      FieldName = 'CustoTotalNegrito'
    end
    object cdsValorTotalNegrito: TStringField
      FieldName = 'ValorTotalNegrito'
    end
    object cdsValorProdutoNegrito: TStringField
      FieldName = 'ProdutoNegrito'
      Size = 60
    end
  end
  object frxReport1: TfrxReport
    Version = '5.1.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44043.704685613400000000
    ReportOptions.LastChange = 45410.818617372680000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 562
    Top = 185
    Datasets = <
      item
        DataSet = dmRelatorio.dstEmpresa
        DataSetName = 'frxdstEmpresa'
      end
      item
        DataSet = frxDBDataset1
        DataSetName = 'frxdstValorProdutos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier'
      Font.Style = []
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 86.929190000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          Top = 70.811070000000000000
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
        object Memo1: TfrxMemoView
          Left = 11.338590000000000000
          Top = 3.779530000000001000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Empresa:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456710000000000000
          Top = 34.015770000000010000
          Width = 52.913420000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Bairro:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 15.118120000000000000
          Top = 49.133889999999990000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000001000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 321.260050000000000000
          Top = 34.015770000000010000
          Width = 64.252010000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Cidade/UF:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 340.157700000000000000
          Top = 49.133889999999990000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'E-mail:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 82.708720000000000000
          Top = 3.779530000000001000
          Width = 188.976500000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 383.512060000000000000
          Top = 3.779530000000001000
          Width = 200.315090000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."CNPJ"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 84.929190000000000000
          Top = 18.897650000000000000
          Width = 551.811380000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            
              '[frxdstEmpresa."LOGRADOURO"] [frxdstEmpresa."COMPLEMENTO"] n'#176'[fr' +
              'xdstEmpresa."NUMERO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 82.708720000000000000
          Top = 34.015770000000010000
          Width = 207.874150000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_BAIRRO"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 383.512060000000000000
          Top = 34.015770000000010000
          Width = 185.196970000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."NOME_CIDADE"] / [frxdstEmpresa."ESTADO_ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 82.708720000000000000
          Top = 49.133889999999990000
          Width = 75.590600000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."TELEFONE_PRINCIPAL"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 383.512060000000000000
          Top = 49.133889999999990000
          Width = 260.787570000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstEmpresa."E_MAIL"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 545.252320000000000000
          Top = 3.779530000000001000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Emitido em [Date] '#224's [Time]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 2.779530000000000000
          Top = 70.811070000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Ranking')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 49.354360000000000000
          Top = 70.811070000000000000
          Width = 298.582870000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Produto')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 512.575139999999200000
          Top = 70.811070000000000000
          Width = 68.031540000000010000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            'Classifica'#231#227'o')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 545.252320000000000000
          Top = 18.897650000000000000
          Width = 170.078850000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Pag. [Page] de [TotalPages#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo5: TfrxMemoView
          Left = 354.275820000000000000
          Top = 70.811070000000000000
          Width = 60.472480000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 419.307360000000000000
          Top = 70.811070000000000000
          Width = 30.236240000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Und.')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 455.323129999999200000
          Top = 70.811070000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '%')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 166.299320000000000000
        Width = 718.110700000000000000
        DataSet = frxDBDataset1
        DataSetName = 'frxdstValorProdutos'
        RowCount = 0
        object frxdstTranscodigoProd: TfrxMemoView
          Left = 2.779530000000000000
          Top = 1.000000000000000000
          Width = 45.354330710000000000
          Height = 11.338590000000000000
          DataField = 'Ranking'
          DataSet = frxDBDataset1
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstValorProdutos."Ranking"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo25: TfrxMemoView
          Left = 49.511811020000010000
          Top = 1.000000000000000000
          Width = 298.582723540000000000
          Height = 11.338590000000000000
          DataField = 'Cliente'
          DataSet = frxDBDataset1
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxdstValorProdutos."Cliente"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 512.503849129999200000
          Top = 1.000000000000000000
          Width = 68.031496060000000000
          Height = 11.338590000000000000
          DataField = 'Classificacao'
          DataSet = frxDBDataset1
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstValorProdutos."Classificacao"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object Memo15: TfrxMemoView
          Left = 354.141732280000000000
          Top = 1.000000000000000000
          Width = 60.472440940000000000
          Height = 11.338590000000000000
          DataField = 'Quantidade'
          DataSet = frxDBDataset1
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstValorProdutos."Quantidade"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 419.149606300000000000
          Top = 1.000000000000000000
          Width = 30.236220470000000000
          Height = 11.338590000000000000
          DataField = 'Unidade'
          DataSet = frxDBDataset1
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxdstValorProdutos."Unidade"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 455.165266459999200000
          Top = 1.000000000000000000
          Width = 52.913385830000000000
          Height = 11.338590000000000000
          DataField = 'Percentual'
          DataSet = frxDBDataset1
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstValorProdutos."Percentual"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 49.133890000000000000
          Top = 1.000000000000000000
          Width = 298.582723540000000000
          Height = 11.338590000000000000
          DataField = 'ClienteNegrito'
          DataSet = frxDBDataset1
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxdstValorProdutos."ClienteNegrito"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 354.275820000000000000
          Top = 1.000000000000000000
          Width = 60.472440940000000000
          Height = 11.338590000000000000
          DataField = 'QuantidadeNegrito'
          DataSet = frxDBDataset1
          DataSetName = 'frxdstValorProdutos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxdstValorProdutos."QuantidadeNegrito"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 41.952782760000000000
        Top = 241.889920000000000000
        Width = 718.110700000000000000
        object Line1: TfrxLineView
          Top = 3.779529999999994000
          Width = 718.110700000000000000
          StretchMode = smMaxHeight
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        object Shape2: TfrxShapeView
          Width = 718.110700000000000000
          Height = 15.118120000000000000
          Fill.BackColor = cl3DLight
        end
      end
    end
  end
  object frxDBDataset1: TfrxDBDataset
    UserName = 'frxdstValorProdutos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Ranking=Ranking'
      'Cliente=Cliente'
      'Unidade=Unidade'
      'Quantidade=Quantidade'
      'Percentual=Percentual'
      'Classificacao=Classificacao'
      'ClienteNegrito=ClienteNegrito'
      'QuantidadeNegrito=QuantidadeNegrito')
    DataSet = ClientDataSet1
    BCDToCurrency = False
    Left = 562
    Top = 249
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Ranking'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Cliente'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'Quantidade'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Unidade'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Percentual'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Classificacao'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'ClienteNegrito'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'QuantidadeNegrito'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 562
    Top = 314
    object cdsQuantidadeRanking: TStringField
      FieldName = 'Ranking'
    end
    object cdsQuantidadeCliente: TStringField
      FieldName = 'Cliente'
      Size = 60
    end
    object cdsQuantidadeUnidade: TStringField
      FieldName = 'Unidade'
    end
    object cdsQuantidadeQuantidade: TStringField
      FieldName = 'Quantidade'
    end
    object cdsQuantidadePercentual: TStringField
      FieldName = 'Percentual'
    end
    object cdsQuantidadeClassificacao: TStringField
      FieldName = 'Classificacao'
    end
    object cdsQuantidadeClienteNegrito: TStringField
      FieldName = 'ClienteNegrito'
      Size = 60
    end
    object cdsQuantidadeQuantidadeNegrito: TStringField
      FieldName = 'QuantidadeNegrito'
    end
  end
end
