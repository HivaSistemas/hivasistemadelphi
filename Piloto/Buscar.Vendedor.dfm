inherited FormBuscarVendedor: TFormBuscarVendedor
  ActiveControl = FrVendedor.sgPesquisa
  Caption = 'Busca de vendedor'
  ClientHeight = 85
  ClientWidth = 411
  ExplicitWidth = 417
  ExplicitHeight = 114
  PixelsPerInch = 96
  TextHeight = 14
  inherited pnOpcoes: TPanel
    Top = 48
    Width = 411
    ExplicitTop = 48
    ExplicitWidth = 411
  end
  inline FrVendedor: TFrVendedores
    Left = 4
    Top = 1
    Width = 403
    Height = 40
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 1
    TabStop = True
    ExplicitLeft = 4
    ExplicitTop = 1
    ExplicitHeight = 40
    inherited sgPesquisa: TGridLuka
      Height = 23
      ExplicitHeight = 23
    end
    inherited PnTitulos: TPanel
      inherited lbNomePesquisa: TLabel
        Width = 54
        Height = 15
        Caption = 'Vendedor'
        ExplicitWidth = 54
      end
    end
    inherited pnPesquisa: TPanel
      Height = 24
      ExplicitHeight = 24
    end
  end
end
