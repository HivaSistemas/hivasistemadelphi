unit _Scripts1;

interface

function CriarTabelaADMINISTRADORAS_CARTOES: string;
function CriarTriggerADMINISTRADORAS_CARTOES_IU_BR: string;
function CriarSequenceSEQ_ADMINISTRADORAS_CARTOES: string;
function CriarColunaADMINISTRADORA_CARTAO_ID: string;
function CriarFK_TP_COB_ADMIN_CARTAO_ID: string;
function AjustarViewVW_TIPOS_ALTER_LOGS_TP_COBRANC: string;
function AjustarTriggerTIPOS_COBRANCA_U_AR: string;
function CriarColunaCODIGO_AUTORIZACAO: string;
function AdicionarVariasColunasGERENCIADORES_CARTOES_TEF: string;
function AdicionarColunaNUMERO_CARTAOTabelaORCAMENTOS_PAGAMENTOS: string;
function AjustarProcedureGERAR_CARTOES_RECEBER: string;
function AjustarProcedureRECEBER_PEDIDO: string;
function AddColunasCONTAS_REC_BAIXAS_PAGAMENTOS: string;
function AjustarProcGERAR_CARTOES_RECEBER_BAIXA: string;
function AjustarProcCONSOLIDAR_BAIXA_CONTAS_REC: string;
function AddColunaTabelaACUMULADOS_PAGAMENTOS: string;
function AjustarProcGERAR_CARTOES_REC_ACUMULADO: string;
function AjustarProcRECEBER_ACUMULADO: string;
function AdicionarColunaTIPO_RECEB_CARTAOTabelaCONTAS_REC_BAIXAS_PAGAMENTOS: string;
function AdicionarColunaTIPO_RECEB_CARTAOTabelaORCAMENTOS_PAGAMENTOS: string;
function AdicionarColunaTIPO_RECEB_CARTAOTabelaACUMULADOS_PAGAMENTOS: string;
function AlimentarTIPO_RECEB_CARTAOTabelaCONTAS_REC_BAIXAS_PAGAMENTOS: string;
function AlimentarTIPO_RECEB_CARTAOTabelaORCAMENTOS_PAGAMENTOS: string;
function AlimentarTIPO_RECEB_CARTAOTabelaACUMULADOS_PAGAMENTOS: string;
function AdicionarColunaBANDEIRA_CARTAO_NFE: string;
function AlimentarColunaBANDEIRA_CARTAO_NFE: string;
function AjustarConstraintCK_TP_COBRANCA_FORMA_PAGTO_CRT: string;
function AddColunaQTDE_SEG_TRAVAR_ALTIS_OCIOSO: string;
function AddConstraintCK_PAR_QTDE_SEG_TR_ALTIS_OCI: string;
function AjustarProcADD_NO_VETOR_SEM_REPETIR: string;
function AjustarProcGERAR_NOTA_DEV_RETIRADA: string;
function AjustarProcGERAR_NOTA_DEVOLUCAO_ACUMULADO: string;
function AjustarProcGERAR_NOTA_DEVOLUCAO_ENTREGA: string;
function AjustarProcGERAR_NOTA_RETORNO_ENTREGA: string;
function AjustarConstraintCK_CFOPS_CST_OPER_TIPO_MOV: string;
function AjustarConstraintCK_NOTAS_FISCAIS_TIPO_MOVIMENT: string;
function AjustarFunctionBUSCAR_CFOP_ID_OPERACAO: string;
function AjustarProcedureBAIXAR_ENTREGA: string;
function AjustarTriggerORCAMENTOS_U_AR: string;
function AjustarViewVW_TIPOS_ALTER_LOGS_ORCAMENTOS: string;
function DroparColunaECF_ID: string;
function AjustarPackageRecordsNotasFiscais: string;
function DroparProcedureINSERIR_NOTA_FISCAL: string;
function SubstituirTriggerProdutos: string;
function AJustarLogsProdutos: string;
function AjusteMotivoInatividade: string;
function AjustarTriggerAJUSTES_ESTOQUE_ITENS_IU_BR: string;
function AdicionarColunaNUMERO_ESTABELECIMENTO_CIELO: string;
function AjustarViewVW_TIPOS_ALTER_LOGS_CT_RECEBER: string;
function AjustarTriggerCONTAS_RECEBER_U_AR: string;
function AjustarProcedureATUALIZAR_RETENCOES_CONTAS_REC: string;
function DroparConstraintFK_CONTAS_REC_ITEM_ID_CRT_ORC: string;
function AlterarCampoCADASTRO_IDTabelaCONTAS_RECEBER_BAIXAS: string;
function AjustarConstraintCK_CONTAS_RECEBER_BAIXAS_TIPO: string;
function AjustarConstraintCK_CONTAS_REC_BX_TIPO_CAD_ID: string;
function AjustarTriggerCONTAS_REC_BX_PAGTOS_DIN_IU_BR: string;
function AjustarTriggerCONTAS_PAGAR_BX_PAG_DIN_IU_BR: string;
function AjustarProcedureATUALIZAR_CUSTOS_ENTRADA: string;
function AjustarProcedureVOLTAR_CUSTOS_ENTRADA: string;
function Atualizando_VW_ENTREGAS: string;
function Atualizando_VW_RELACAO_VENDAS_PRODUTOS: string;
function Criando_Coluna_QTDE_MAXIMA_DIAS_DEVOLUCAO_Tabela_PARAMETROS: string;
function Criando_CK_PAR_QTDE_MAX_DIAS_DEVOLUCAO: string;
function DroparColunaESTACAO_EMISSAO_NOTA: string;
function AjustarTriggerNOTAS_FISCAIS_IU_BR: string;
function AjustarTriggerNOTAS_FISCAIS_U_AR: string;
function LimparLogsNOTAS_FISCAIS_TIPO9: string;
function AdicionarNovasColunasEnderecoEntregaNOTAS_FISCAIS: string;
function AjustarFunctionBUSCAR_ENDERECO_EMISSAO_NF: string;
function AtualizarPackageRecordsNotasFiscais: string;
function AjustarProcedureGERAR_NOTA_ENTREGA: string;
function AjustandoColunasTabelaCONHECIMENTOS_FRETES: string;
function AjustarTriggerCONHECIMENTOS_FRETES_IU_BR: string;
function CriarNovasColunasMOVIMENTOS_CONTAS: string;
function AjustarConstraintCK_MOV_CONTAS_CONCILIADO: string;
function CriarColunaSALDO_CONCILIADO_ATUAL: string;
function AjustarProcedureMOVIMENTAR_SALDO_CONTA: string;
function AjustarTriggerTURNOS_CAIXA_IU_BR: string;
function AjustarTriggerMOVIMENTOS_TURNOS_IU_BR: string;
function AjustarTriggerMOVIMENTOS_CONTAS_IU_BR: string;
function AjustarColunaCONTA_IDVariasTabelas: string;
function AjustarViewVW_MOVIMENTOS_CONTAS: string;
function AjustarDataMovimentosMOVIMENTOS_CONTAS: string;
function AjustarSaldoContas: string;
function CriarTabelaLISTA_NEGRA: string;
function CriarTriggerLISTA_NEGRA_IU_BR: string;
function CriarSequenceSEQ_LISTA_NEGRA: string;
function CriarNovasColunasCLIENTES: string;
function AdicionarColunaVALOR_ADIANTADOTabelaCONTAS_RECEBER: string;
function AdicionarColunaTIPO_COB_ADIANTAMENTO_FIN_ID: string;
function AjustarTriggerTIPOS_COBRANCA_IU_BR: string;
function AdicionarColunaRECEBER_ADIANTADO_ID: string;
function AjustarConstraintCK_CONTAS_RECEBER_BAIXAS_TIPO2: string;
function AjustarConstraintCK_CONTAS_REC_BX_TIPO_CAD_ID2: string;
function AdicionarColunaVALOR_ADIANTADOTabelaCONTAS_RECEBER_BAIXAS: string;
function AdicionarCK_CONTAS_REC_BX_VALOR_ADIANT: string;
function AjustarCK_CONTAS_REC_BX_VALOR_LIQ: string;
function AjustarTriggerCONTAS_RECEBER_BAIXAS_IU_BR: string;
function AjusteVwVidaProdutoFisico: string;
function AjusteVwVidaProdutoDisponivel: string;
function AjusteVwVidaProdutoFiscal: string;
function CriarTabelaCONTAS_CUSTODIA: string;
function CriarTriggerCONTAS_CUSTODIA_IU_BR: string;
function CriarSequenceSEQ_CONTAS_CUSTODIA: string;
function CriarNovasColunasCONTAS_RECEBER: string;
function AddFK_CONTAS_REC_CON_CUSTODIA_ID: string;
function AjustarTriggerCONTAS_RECEBER_U_AR2: string;
function AjustarViewVW_TIPOS_ALTER_LOGS_CT_RECEBER2: string;
function AjustarViewVW_ESTOQUES_DIVISAO: string;
function AddColunaEMITIR_DUPLIC_MERCANTIL_VENDA: string;
function AjustarTriggerTIPOS_COBRANCA_U_AR2: string;
function AjustarViewVW_TIPOS_ALTER_LOGS_TP_COBRANC2: string;
function AjustarProcedureSETAR_NFE_EMITIDA: string;
function AjustarProcGERAR_ENTR_TRANS_PRODUTOS_EMP: string;
function CriarTabelaCONTAS_REC_HISTORICO_COBRANCAS: string;
function AjustarTriggerCONTAS_REC_HISTORICO_COB_IU_BR: string;
function AjustarViewVW_VIDA_PRODUTO_DISPONIVEL: string;
function AjustarViewVW_VIDA_PRODUTO_FISCAL: string;
function AjustarViewVW_VIDA_PRODUTO_FISICO: string;
function AjustarProcGERAR_NOTA_TRANSF_PROD_EMPR: string;
function AjustarProcGERAR_NOTA_TRANSF_MOVIMENTOS: string;
function AjustarConstraintCK_CFOPS_CST_OPER_TIPO_MOV2: string;
function AjustarFunctionBUSCAR_CFOP_ID_OPERACAO2: string;
function AjustarViewVW_MOV_PEND_EMISSAO_NF_TRANSF: string;
function AjustarViewVW_MOV_ITE_PEND_EMISSAO_NF_TR: string;
function AjustarConstraintCK_PAR_EMP_TIPO_REDIREC_NOTA: string;
function AjustarConstraintCK_NOTAS_FISCAIS_TIPO_MOVIMENT2: string;
function DroparColunasTabelaNOTAS_FISCAIS: string;
function CriarTabelaCONTROLES_ENTREGAS_AJUDANTES: string;
function AjustarTriggerCONTROLES_ENTREGAS_AJUD_IU_BR: string;
function AjustarTriggerCONTROLES_ENTREGAS_D_BR: string;
function CriarColunaAJUDANTETabelaFUNCIONARIOS: string;
function CriarColunaQTDE_DIAS_BLOQ_VENDA_TIT_VENC: string;
function CriarColunaTIPO_COB_FECHAMENTO_TURNO_ID: string;
function AjustarConstraintCK_CONTAS_RECEBER_ORIGEM: string;
function AjustarProcedureLANCAR_DIFERENCA_FECH_CAIXA: string;
function AjustarTriggerCLIENTES_IU_BR: string;
function AjustarTriggerCADASTROS_IU_BR: string;
function AdicionarColunaVALOR_LIQUIDOTabelaCompras: string;
function AdicionarColunaIGNORAR_BLOQUEIOS_VENDATabelaClientes: string;
function AjustarConstraintCK_CLIENTES_IGNORAR_BLOQ_VENDA: string;
function AdicionarColunaATIVOTabelaCLIENTES_GRUPOS: string;
function AdicionarConstraintCK_CLIENTES_GRUPOS_ATIVO: string;
function AdicionarSequenceSEQ_BLOQUEIOS_ESTOQUES: string;
function AjustarViewVW_RELACAO_ORCAMENTOS_VENDAS: string;
function AjustarViewVW_ESTOQUES: string;
function AjustarViewVW_ESTOQUES_DIVISAO2: string;
function AjustarProcedureGERAR_CREDITO_DEVOLUCAO: string;
function AjustarViewVW_BOLETOS_RECEBER: string;
function AjustarViewVERIFICAR_BLOQUEIOS_ORCAMENTOS: string;
function AjustarTabelaBLOQUEIOS_ESTOQUES_ITENS: string;
function CriarNovasConstraintsBLOQUEIOS_ESTOQUES_ITENS: string;
function CriarTabelaBLOQUEIOS_ESTOQUES_BAIXAS: string;
function CriarTriggerBLOQ_ESTOQUES_BAIXAS_IU_BR: string;
function CriarTabelaBLOQUEIOS_EST_BAIXAS_ITENS: string;
function AjustarTriggerBLOQ_ESTOQUES_BX_ITENS_IU_BR: string;
function AjustarSequenceSEQ_BLOQUEIOS_ESTOQUES_BAIXAS: string;
function AjustarProcedureCONSOLIDAR_BX_BLOQUEIO_ESTOQUE: string;
function AjustarProcedureBLOQUEIOS_ESTOQUES_ITENS_IU_BR: string;
function AjustarUniqueUN_BLOQUEIOS_EST_ITENS_PRO_LOT: string;
function AjustarTriggerDEVOLUCOES_ITENS_D_BR: string;
function AjustarViewVW_ORCAMENTOS_IMPRESSAO: string;
function AjustarViewVW_ENTREGAS_PENDENTES: string;
function AjustarProcedureAGENDAR_ITENS_SEM_PREVISAO: string;
function AdicionarColunasAltisW: string;
function AdicionarColunarValorPixOrcamento: string;

implementation

function CriarTabelaADMINISTRADORAS_CARTOES: string;
begin
  Result :=
    'create table ADMINISTRADORAS_CARTOES('+ #13 +
    '  ADMINISTRADORA_ID       number(3) not null,'+ #13 +
    '  DESCRICAO               varchar2(60) not null,'+ #13 +
    '  CNPJ                    varchar2(18) not null,'+ #13 +
    '  ATIVO                   char(1) default ''S'' not null,'+ #13 +
    #13 +
    '  /* Colunas de logs do sistema */'+ #13 +
    '  USUARIO_SESSAO_ID       number(4) not null,'+ #13 +
    '  DATA_HORA_ALTERACAO     date not null,'+ #13 +
    '  ROTINA_ALTERACAO        varchar2(30) not null,'+ #13 +
    '  ESTACAO_ALTERACAO       varchar2(30) not null'+ #13 +
    ');'+ #13 +
    #13 +
    '/* Chave prim�ria */'+ #13 +
    'alter table ADMINISTRADORAS_CARTOES'+ #13 +
    'add constraint PK_ADMINISTRADORAS_CARTOES'+ #13 +
    'primary key(ADMINISTRADORA_ID)'+ #13 +
    'using index tablespace INDICES;'+ #13 +
    #13 +
    '/* Checagens */'+ #13 +
    'alter table ADMINISTRADORAS_CARTOES'+ #13 +
    'add constraint CK_ADMIN_CARTOES_CNPJ'+ #13 +
    'check( length(CNPJ) = 18 );'+ #13 +
    #13 +
    'alter table ADMINISTRADORAS_CARTOES'+ #13 +
    'add constraint CK_ADMIN_CARTOES_ATIVO'+ #13 +
    'check( ATIVO in(''S'', ''N'') );'+ #13
end;

function CriarTriggerADMINISTRADORAS_CARTOES_IU_BR: string;
begin
  Result :=
    'create or replace trigger ADMINISTRADORAS_CARTOES_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on ADMINISTRADORAS_CARTOES'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    'end ADMINISTRADORAS_CARTOES_IU_BR;'+ #13
end;

function CriarSequenceSEQ_ADMINISTRADORAS_CARTOES: string;
begin
  Result :=
    'create sequence SEQ_ADMINISTRADORAS_CARTOES'+ #13 +
    'start with 1 increment by 1'+ #13 +
    'minvalue 1'+ #13 +
    'maxvalue 999'+ #13 +
    'nocycle'+ #13 +
    'nocache'+ #13 +
    'noorder;'+ #13
end;

function CriarColunaADMINISTRADORA_CARTAO_ID: string;
begin
  Result :=
    'alter table TIPOS_COBRANCA'+ #13 +
    'add ADMINISTRADORA_CARTAO_ID     number(3);'+ #13
end;

function CriarFK_TP_COB_ADMIN_CARTAO_ID: string;
begin
  Result :=
    'alter table TIPOS_COBRANCA'+ #13 +
    'add constraint FK_TP_COB_ADMIN_CARTAO_ID'+ #13 +
    'foreign key(ADMINISTRADORA_CARTAO_ID)'+ #13 +
    'references ADMINISTRADORAS_CARTOES(ADMINISTRADORA_ID);'+ #13
end;

function AjustarViewVW_TIPOS_ALTER_LOGS_TP_COBRANC: string;
begin
  Result :=
    'create or replace view VW_TIPOS_ALTER_LOGS_TP_COBRANC'+ #13 +
    'as'+ #13 +
    'select 1 as TIPO_ALTERACAO_LOG_ID, ''Portador'' as CAMPO from dual union'+ #13 +
    'select 2 as TIPO_ALTERACAO_LOG_ID, ''Nome'' as CAMPO from dual union'+ #13 +
    'select 3 as TIPO_ALTERACAO_LOG_ID, ''Taxa de reten��o'' as CAMPO from dual union'+ #13 +
    'select 4 as TIPO_ALTERACAO_LOG_ID, ''Forma de pagamento'' as CAMPO from dual union'+ #13 +
    'select 5 as TIPO_ALTERACAO_LOG_ID, ''Rede do cart�o'' as CAMPO from dual union'+ #13 +
    'select 6 as TIPO_ALTERACAO_LOG_ID, ''Tipo de cart�o'' as CAMPO from dual union'+ #13 +
    'select 7 as TIPO_ALTERACAO_LOG_ID, ''Tipo de receb. cart�o'' as CAMPO from dual union'+ #13 +
    'select 8 as TIPO_ALTERACAO_LOG_ID, ''Tipo de pagto. comiss�o'' as CAMPO from dual union'+ #13 +
    'select 9 as TIPO_ALTERACAO_LOG_ID, ''% Comiss�o'' as CAMPO from dual union'+ #13 +
    'select 10 as TIPO_ALTERACAO_LOG_ID, ''Utilizar lim. cr�dito do cliente'' as CAMPO from dual union'+ #13 +
    'select 11 as TIPO_ALTERACAO_LOG_ID, ''Boleto banc�rio?'' as CAMPO from dual union'+ #13 +
    'select 12 as TIPO_ALTERACAO_LOG_ID, ''Incid�ncia financeira'' as CAMPO from dual union'+ #13 +
    'select 13 as TIPO_ALTERACAO_LOG_ID, ''Ativo?'' as CAMPO from dual union'+ #13 +
    'select 14 as TIPO_ALTERACAO_LOG_ID, ''Permitir prazo m�dio?'' as CAMPO from dual union'+ #13 +
    'select 15 as TIPO_ALTERACAO_LOG_ID, ''Prazo m�dio'' as CAMPO from dual union'+ #13 +
    'select 16 as TIPO_ALTERACAO_LOG_ID, ''Administradora de cart�o'' as CAMPO from dual;'+ #13
end;

function AjustarTriggerTIPOS_COBRANCA_U_AR: string;
begin
  Result :=
    'create or replace trigger TIPOS_COBRANCA_U_AR'+ #13 +
    'after update '+ #13 +
    'on TIPOS_COBRANCA'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    #13 +
    '  procedure INSERIR_LOG(pTIPO_ALTERACAO_LOG_ID in number, pVALOR_ANTERIOR in string, pNOVO_VALOR in string)'+ #13 +
    '  is begin'+ #13 +
    '    if nvl(pVALOR_ANTERIOR, ''X'') <> nvl(pNOVO_VALOR, ''X'') then'+ #13 +
    '      insert into LOGS_TIPOS_COBRANCA(TIPO_ALTERACAO_LOG_ID, COBRANCA_ID, VALOR_ANTERIOR, NOVO_VALOR)'+ #13 +
    '      values(pTIPO_ALTERACAO_LOG_ID, :new.COBRANCA_ID, pVALOR_ANTERIOR, pNOVO_VALOR);'+ #13 +
    '    end if;'+ #13 +
    '  end;'+ #13 +
    #13 +
    'begin'+ #13 +
    '  /* View VW_TIPOS_ALTER_LOGS_TP_COBRANC */'+ #13 +
    #13 +
    '  INSERIR_LOG(1, :old.PORTADOR_ID, :new.PORTADOR_ID);'+ #13 +
    '  INSERIR_LOG(2, :old.NOME, :new.NOME);'+ #13 +
    '  INSERIR_LOG(3, :old.TAXA_RETENCAO_MES, :new.TAXA_RETENCAO_MES);'+ #13 +
    '  INSERIR_LOG(4, :old.FORMA_PAGAMENTO, :new.FORMA_PAGAMENTO);'+ #13 +
    '  INSERIR_LOG(5, :old.REDE_CARTAO, :new.REDE_CARTAO);'+ #13 +
    '  INSERIR_LOG(6, :old.TIPO_CARTAO, :new.TIPO_CARTAO);'+ #13 +
    '  INSERIR_LOG(7, :old.TIPO_RECEBIMENTO_CARTAO, :new.TIPO_RECEBIMENTO_CARTAO);'+ #13 +
    '  INSERIR_LOG(8, :old.TIPO_PAGAMENTO_COMISSAO, :new.TIPO_PAGAMENTO_COMISSAO);'+ #13 +
    '  INSERIR_LOG(9, :old.PERCENTUAL_COMISSAO, :new.PERCENTUAL_COMISSAO);'+ #13 +
    '  INSERIR_LOG(10, :old.UTILIZAR_LIMITE_CRED_CLIENTE, :new.UTILIZAR_LIMITE_CRED_CLIENTE);'+ #13 +
    '  INSERIR_LOG(11, :old.BOLETO_BANCARIO, :new.BOLETO_BANCARIO);'+ #13 +
    '  INSERIR_LOG(12, :old.INCIDENCIA_FINANCEIRA, :new.INCIDENCIA_FINANCEIRA);'+ #13 +
    '  INSERIR_LOG(13, :old.ATIVO, :new.ATIVO);'+ #13 +
    '  INSERIR_LOG(14, :old.PERMITIR_PRAZO_MEDIO, :new.PERMITIR_PRAZO_MEDIO);'+ #13 +
    '  INSERIR_LOG(15, :old.PRAZO_MEDIO, :new.PRAZO_MEDIO);'+ #13 +
    '  INSERIR_LOG(16, :old.ADMINISTRADORA_CARTAO_ID, :new.ADMINISTRADORA_CARTAO_ID);'+ #13 +
    #13 +
    'end TIPOS_COBRANCA_U_AR;'+ #13
end;

function CriarColunaCODIGO_AUTORIZACAO: string;
begin
  Result :=
    'alter table ORCAMENTOS_PAGAMENTOS'+ #13 +
    'add CODIGO_AUTORIZACAO   varchar2(50);'+ #13
end;

function AdicionarVariasColunasGERENCIADORES_CARTOES_TEF: string;
begin
  Result :=
    'alter table GERENCIADORES_CARTOES_TEF'+ #13 +
    'add ('+ #13 +
    '  TEXTO_PROCURAR_BANDEIRA_1      varchar2(20),'+ #13 +
    '  POSICAO_INICIAL_PROC_BAND_1    number(3),'+ #13 +
    '  QTDE_CARACTERES_COPIAR_BAND_1  number(3),  '+ #13 +
    #13 +
    '  TEXTO_PROCURAR_BANDEIRA_2      varchar2(20),'+ #13 +
    '  POSICAO_INICIAL_PROC_BAND_2    number(3),'+ #13 +
    '  QTDE_CARACTERES_COPIAR_BAND_2  number(3),'+ #13 +
    #13 +
    '  TEXTO_PROCURAR_REDE_1          varchar2(20),'+ #13 +
    '  POSICAO_INICIAL_PROC_REDE_1    number(3),'+ #13 +
    '  QTDE_CARACTERES_COPIAR_REDE_1  number(3),'+ #13 +
    #13 +
    '  TEXTO_PROCURAR_REDE_2          varchar2(20),'+ #13 +
    '  POSICAO_INICIAL_PROC_REDE_2    number(3),'+ #13 +
    '  QTDE_CARACTERES_COPIAR_REDE_2  number(3),'+ #13 +
    #13 +
    '  TEXTO_PROCURAR_NUMERO_CARTAO_1 varchar2(20),'+ #13 +
    '  POSIC_INI_PROC_NUMERO_CARTAO_1 number(3),'+ #13 +
    '  QTDE_CARAC_COPIAR_NR_CARTAO_1  number(3),'+ #13 +
    #13 +
    '  TEXTO_PROCURAR_NUMERO_CARTAO_2 varchar2(20),'+ #13 +
    '  POSIC_INI_PROC_NUMERO_CARTAO_2 number(3),'+ #13 +
    '  QTDE_CARAC_COPIAR_NR_CARTAO_2  number(3),'+ #13 +
    #13 +
    '  TEXTO_PROCURAR_NSU_1           varchar2(20),'+ #13 +
    '  POSICAO_INI_PROCURAR_NSU_1     number(3),'+ #13 +
    '  QTDE_CARAC_COPIAR_NSU_1        number(3),'+ #13 +
    #13 +
    '  TEXTO_PROCURAR_NSU_2           varchar2(20),'+ #13 +
    '  POSICAO_INI_PROCURAR_NSU_2     number(3),'+ #13 +
    '  QTDE_CARAC_COPIAR_NSU_2        number(3),'+ #13 +
    #13 +
    '  TEXTO_PROCURAR_COD_AUTORIZ_1   varchar2(20),'+ #13 +
    '  POSICAO_INI_PRO_COD_AUTORIZ_1  number(3),'+ #13 +
    '  QTDE_CARAC_COP_COD_AUTORIZ_1   number(3),'+ #13 +
    #13 +
    '  TEXTO_PROCURAR_COD_AUTORIZ_2   varchar2(20),'+ #13 +
    '  POSICAO_INI_PRO_COD_AUTORIZ_2  number(3),'+ #13 +
    '  QTDE_CARAC_COP_COD_AUTORIZ_2   number(3)'+ #13 +
    ');'+ #13
end;

function AdicionarColunaNUMERO_CARTAOTabelaORCAMENTOS_PAGAMENTOS: string;
begin
  Result :=
    'alter table ORCAMENTOS_PAGAMENTOS'+ #13 +
    'add NUMERO_CARTAO        varchar2(19);'+ #13
end;

function AjustarProcedureGERAR_CARTOES_RECEBER: string;
begin
  Result :=
    'create or replace procedure GERAR_CARTOES_RECEBER('+ #13 +
    '  iORCAMENTO_ID     in number, -- 0'+ #13 +
    '  iITEM_ID          in number, -- 1'+ #13 +
    '  iNSU              in string, -- 2'+ #13 +
    '  iCODIGO_AUTOR_TEF in string, -- 3'+ #13 +
    '  iNUMERO_CARTAO    in string  -- 4'+ #13 +
    ')'+ #13 +
    'is '+ #13 +
    '  i                     number default 0;'+ #13 +
    '  vValorCartao          number not null default 0;'+ #13 +
    '  vDadosPagamentoPedido RecordsVendas.RecPedidosPagamentos;'+ #13 +
    '  vCartoesGerar         RecordsFinanceiros.ArrayOfRecParcelasReceber;'+ #13 +
    '  vReceberId            CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
    #13 +
    '  cursor cCartoes is'+ #13 +
    '  select'+ #13 +
    '    PAG.COBRANCA_ID,'+ #13 +
    '    PAG.VALOR,'+ #13 +
    '    TIP.TIPO_CARTAO,'+ #13 +
    '    TIP.PORTADOR_ID'+ #13 +
    '  from'+ #13 +
    '    ORCAMENTOS_PAGAMENTOS PAG'+ #13 +
    #13 +
    '  inner join TIPOS_COBRANCA TIP'+ #13 +
    '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID'+ #13 +
    #13 +
    '  and TIP.FORMA_PAGAMENTO = ''CRT'''+ #13 +
    '  where PAG.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
    '  and PAG.ITEM_ID = iITEM_ID;'+ #13 +
    #13 +
    '  cursor c_dias_prazo(pCOBRANCA_ID positive) is'+ #13 +
    '  select'+ #13 +
    '    DIAS,'+ #13 +
    '    rownum as PARCELA,'+ #13 +
    '    count(*) over (partition by count(*)) as NUMERO_PARCELAS'+ #13 +
    '  from'+ #13 +
    '    TIPO_COBRANCA_DIAS_PRAZO      '+ #13 +
    '  where COBRANCA_ID = pCOBRANCA_ID  '+ #13 +
    '  group by'+ #13 +
    '    DIAS,'+ #13 +
    '    rownum'+ #13 +
    '  order by DIAS;'+ #13 +
    #13 +
    'begin'+ #13 +
    '  delete from CONTAS_RECEBER'+ #13 +
    '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
    '  and ITEM_ID_CRT_ORCAMENTO = iITEM_ID;'+ #13 +
    #13 +
    '  update ORCAMENTOS_PAGAMENTOS set'+ #13 +
    '    NSU_TEF = iNSU'+ #13 +
    '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
    '  and ITEM_ID = iITEM_ID;'+ #13 +
    #13 +
    '  begin'+ #13 +
    '    select'+ #13 +
    '      0 as VALOR_DINHEIRO,'+ #13 +
    '      ORC.VALOR_CARTAO_DEBITO,'+ #13 +
    '      ORC.VALOR_CARTAO_CREDITO,'+ #13 +
    '      0 as VALOR_CREDITO,'+ #13 +
    '      0 as VALOR_COBRANCA,'+ #13 +
    '      0 as VALOR_CHEQUE,'+ #13 +
    '      0 as VALOR_FINANCEIRA,'+ #13 +
    '      0 as VALOR_ACUMULATIVO,'+ #13 +
    '      ORC.VENDEDOR_ID,'+ #13 +
    '      ORC.EMPRESA_ID,'+ #13 +
    '      ORC.CLIENTE_ID,'+ #13 +
    '      ORC.STATUS,'+ #13 +
    '      ORC.TURNO_ID'+ #13 +
    '    into'+ #13 +
    '      vDadosPagamentoPedido.VALOR_DINHEIRO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CREDITO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_COBRANCA,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CHEQUE,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_FINANCEIRA,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_ACUMULATIVO,'+ #13 +
    '      vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
    '      vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
    '      vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
    '      vDadosPagamentoPedido.STATUS,'+ #13 +
    '      vDadosPagamentoPedido.TURNO_ID'+ #13 +
    '    from'+ #13 +
    '      ORCAMENTOS ORC'+ #13 +
    '    where ORC.ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
    '  exception'+ #13 +
    '    when others then'+ #13 +
    '     ERRO(''N�o foi encontrado os valores de pagamento para o or�amento '' || iORCAMENTO_ID || ''!'' || sqlerrm);'+ #13 +
    '  end;'+ #13 +
    #13 +
    '   /* Inserindo os cart�es no financeiro */'+ #13 +
    '  if vDadosPagamentoPedido.VALOR_CARTAO_DEBITO + vDadosPagamentoPedido.VALOR_CARTAO_CREDITO > 0 then'+ #13 +
    '    for vCartoes in cCartoes loop'+ #13 +
    '      vValorCartao := vCartoes.VALOR;'+ #13 +
    '      for v_dias_prazo in c_dias_prazo(vCartoes.COBRANCA_ID) loop'+ #13 +
    '        vValorCartao := round(vValorCartao - vCartoes.VALOR / v_dias_prazo.NUMERO_PARCELAS, 2);'+ #13 +
    #13 +
    '        vCartoesGerar(i).COBRANCA_ID         := vCartoes.COBRANCA_ID;'+ #13 +
    '        vCartoesGerar(i).ITEM_ID             := iITEM_ID;'+ #13 +
    '        vCartoesGerar(i).TIPO_CARTAO         := vCartoes.TIPO_CARTAO;'+ #13 +
    '        vCartoesGerar(i).PARCELA             := v_dias_prazo.PARCELA;'+ #13 +
    '        vCartoesGerar(i).NUMERO_PARCELAS     := v_dias_prazo.NUMERO_PARCELAS;'+ #13 +
    '        vCartoesGerar(i).VALOR_PARCELA       := round(vCartoes.VALOR / v_dias_prazo.NUMERO_PARCELAS, 2);'+ #13 +
    '        vCartoesGerar(i).DATA_VENCIMENTO     := trunc(sysdate) + v_dias_prazo.DIAS;'+ #13 +
    '        vCartoesGerar(i).PORTADOR_ID         := vCartoes.PORTADOR_ID;'+ #13 +
    '        vCartoesGerar(i).PLANO_FINANCEIRO_ID := ''1.001.004'';'+ #13 +
    #13 +
    '        if v_dias_prazo.PARCELA = v_dias_prazo.NUMERO_PARCELAS and vValorCartao <> 0 then'+ #13 +
    '          vCartoesGerar(i).VALOR_PARCELA := vCartoesGerar(i).VALOR_PARCELA + vValorCartao;'+ #13 +
    '        end if;'+ #13 +
    #13 +
    '        i := i + 1;'+ #13 +
    '      end loop;      '+ #13 +
    '    end loop;'+ #13 +
    #13 +
    '    if vDadosPagamentoPedido.TURNO_ID is null then'+ #13 +
    '      ERRO(''Turno de recebimento do cart�o n�o encontrado!'');'+ #13 +
    '    end if;'+ #13 +
    #13 +
    '    if vCartoesGerar.first is null then'+ #13 +
    '      ERRO(''O Altis n�o conseguiu gerar as parcelas do cart�o corretamente, por favor verifique se o tipo de cobran�a est� parametrizado corretamente!'');'+ #13 +
    '    end if;'+ #13 +
    #13 +
    '    for i in vCartoesGerar.first..vCartoesGerar.last loop'+ #13 +
    '      vReceberId := SEQ_RECEBER_ID.nextval;'+ #13 +
    #13 +
    '      insert into CONTAS_RECEBER('+ #13 +
    '        RECEBER_ID,'+ #13 +
    '        CADASTRO_ID,'+ #13 +
    '        EMPRESA_ID,'+ #13 +
    '        ORIGEM,'+ #13 +
    '        ORCAMENTO_ID,'+ #13 +
    '        COBRANCA_ID,'+ #13 +
    '        PORTADOR_ID,'+ #13 +
    '        PLANO_FINANCEIRO_ID,'+ #13 +
    '        TURNO_ID,'+ #13 +
    '        VENDEDOR_ID,'+ #13 +
    '        DOCUMENTO,'+ #13 +
    '        DATA_VENCIMENTO,'+ #13 +
    '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
    '        NSU,'+ #13 +
    '        CODIGO_AUTORIZACAO_TEF,'+ #13 +
    '        ITEM_ID_CRT_ORCAMENTO,'+ #13 +
    '        VALOR_DOCUMENTO,'+ #13 +
    '        STATUS,'+ #13 +
    '        PARCELA,'+ #13 +
    '        NUMERO_PARCELAS,'+ #13 +
    '        NUMERO_CARTAO_TRUNCADO'+ #13 +
    '      )values('+ #13 +
    '        vReceberId,'+ #13 +
    '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
    '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
    '        ''ORC'','+ #13 +
    '        iORCAMENTO_ID,'+ #13 +
    '        vCartoesGerar(i).COBRANCA_ID,'+ #13 +
    '        vCartoesGerar(i).PORTADOR_ID,'+ #13 +
    '        vCartoesGerar(i).PLANO_FINANCEIRO_ID,'+ #13 +
    '        vDadosPagamentoPedido.TURNO_ID,'+ #13 +
    '        vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
    '        ''PED-'' || iORCAMENTO_ID || ''-'' || vCartoesGerar(i).PARCELA || ''/'' || vCartoesGerar(i).NUMERO_PARCELAS || ''/CR'' || vCartoesGerar(i).TIPO_CARTAO,'+ #13 +
    '        vCartoesGerar(i).DATA_VENCIMENTO,'+ #13 +
    '        vCartoesGerar(i).DATA_VENCIMENTO,'+ #13 +
    '        iNSU,'+ #13 +
    '        iCODIGO_AUTOR_TEF,'+ #13 +
    '        iITEM_ID,'+ #13 +
    '        vCartoesGerar(i).VALOR_PARCELA,'+ #13 +
    '        ''A'','+ #13 +
    '        vCartoesGerar(i).PARCELA,'+ #13 +
    '        vCartoesGerar(i).NUMERO_PARCELAS,'+ #13 +
    '        iNUMERO_CARTAO'+ #13 +
    '      );'+ #13 +
    '    end loop;     '+ #13 +
    '  end if;   '+ #13 +
    #13 +
    'end GERAR_CARTOES_RECEBER;'+ #13
end;

function AjustarProcedureRECEBER_PEDIDO: string;
begin
  Result :=
    'create or replace procedure RECEBER_PEDIDO('+ #13 +
    '  iORCAMENTO_ID        in positiven,'+ #13 +
    '  iTURNO_ID            in positiven,'+ #13 +
    '  iVALOR_TROCO         in number,'+ #13 +
    '  iGERAR_CREDITO_TROCO in string,'+ #13 +
    '  iTIPO_NOTA_GERAR     in string'+ #13 +
    ')'+ #13 +
    'is'+ #13 +
    '  i                       naturaln default 0;'+ #13 +
    '  vRetiradaId             RETIRADAS.RETIRADA_ID%type;'+ #13 +
    '  vReceberId              CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
    '  vDadosPagamentoPedido   RecordsVendas.RecPedidosPagamentos;'+ #13 +
    #13 +
    '  vBaixaCreditoPagarId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;'+ #13 +
    '  vValorTotal           ORCAMENTOS.VALOR_TOTAL%type;'+ #13 +
    '  vValorBaixarCredito   number default 0;'+ #13 +
    '  vValorRestante        number default 0;'+ #13 +
    '  vReceberNaEntrega     ORCAMENTOS.RECEBER_NA_ENTREGA%type;'+ #13 +
    #13 +
    '  cursor cCheques is'+ #13 +
    '  select'+ #13 +
    '    COBRANCA_ID,'+ #13 +
    '    ITEM_ID,'+ #13 +
    '    DATA_VENCIMENTO,'+ #13 +
    '    BANCO,'+ #13 +
    '    AGENCIA,'+ #13 +
    '    CONTA_CORRENTE,'+ #13 +
    '    NUMERO_CHEQUE,'+ #13 +
    '    VALOR_CHEQUE,'+ #13 +
    '    NOME_EMITENTE,'+ #13 +
    '    TELEFONE_EMITENTE,'+ #13 +
    '    PARCELA,'+ #13 +
    '    NUMERO_PARCELAS'+ #13 +
    '  from'+ #13 +
    '    ORCAMENTOS_PAGAMENTOS_CHEQUES'+ #13 +
    '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
    '  order by '+ #13 +
    '		COBRANCA_ID, '+ #13 +
    '		PARCELA,'+ #13 +
    '		DATA_VENCIMENTO, '+ #13 +
    '		ITEM_ID;'+ #13 +
    #13 +
    '  cursor cCobrancas(pTIPO in string) is'+ #13 +
    '  select'+ #13 +
    '    PAG.COBRANCA_ID,'+ #13 +
    '    PAG.ITEM_ID,'+ #13 +
    '    PAG.VALOR,'+ #13 +
    '    PAG.DATA_VENCIMENTO,'+ #13 +
    '    PAG.PARCELA,'+ #13 +
    '    PAG.NUMERO_PARCELAS,'+ #13 +
    '    TIP.BOLETO_BANCARIO,'+ #13 +
    '    PAG.NSU_TEF,'+ #13 +
    '    TIP.PORTADOR_ID,'+ #13 +
    '    PAG.NUMERO_CARTAO,'+ #13 +
    '    PAG.CODIGO_AUTORIZACAO'+ #13 +
    '  from'+ #13 +
    '    ORCAMENTOS_PAGAMENTOS PAG'+ #13 +
    #13 +
    '  join TIPOS_COBRANCA TIP'+ #13 +
    '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    '+ #13 +
    '  and TIP.FORMA_PAGAMENTO = pTIPO'+ #13 +
    #13 +
    '  where PAG.ORCAMENTO_ID = iORCAMENTO_ID  '+ #13 +
    '  order by '+ #13 +
    '		COBRANCA_ID,'+ #13 +
    '		PARCELA,'+ #13 +
    '		DATA_VENCIMENTO, '+ #13 +
    '		ITEM_ID;'+ #13 +
    #13 +
    '  cursor cCreditos is'+ #13 +
    '  select'+ #13 +
    '    ORC.PAGAR_ID,'+ #13 +
    '    PAG.VALOR_DOCUMENTO'+ #13 +
    '  from'+ #13 +
    '    ORCAMENTOS_CREDITOS ORC'+ #13 +
    #13 +
    '  inner join CONTAS_PAGAR PAG'+ #13 +
    '  on ORC.PAGAR_ID = PAG.PAGAR_ID'+ #13 +
    #13 +
    '  where ORC.ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
    '  and ORC.MOMENTO_USO = ''R'';'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  begin'+ #13 +
    '    select'+ #13 +
    '      VALOR_DINHEIRO,'+ #13 +
    '      VALOR_CARTAO_DEBITO,'+ #13 +
    '      VALOR_CARTAO_CREDITO,'+ #13 +
    '      VALOR_CREDITO,'+ #13 +
    '      VALOR_COBRANCA,'+ #13 +
    '      VALOR_CHEQUE,'+ #13 +
    '      VALOR_FINANCEIRA,'+ #13 +
    '      VALOR_ACUMULATIVO,'+ #13 +
    '      STATUS,'+ #13 +
    '      VENDEDOR_ID,'+ #13 +
    '      EMPRESA_ID,'+ #13 +
    '      CLIENTE_ID,'+ #13 +
    '      RECEBER_NA_ENTREGA,'+ #13 +
    '      VALOR_TOTAL'+ #13 +
    '    into'+ #13 +
    '      vDadosPagamentoPedido.VALOR_DINHEIRO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CREDITO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_COBRANCA,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CHEQUE,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_FINANCEIRA,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_ACUMULATIVO,'+ #13 +
    '      vDadosPagamentoPedido.STATUS,'+ #13 +
    '      vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
    '      vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
    '      vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
    '      vReceberNaEntrega,'+ #13 +
    '      vValorTotal'+ #13 +
    '    from'+ #13 +
    '      ORCAMENTOS'+ #13 +
    '    where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
    '  exception'+ #13 +
    '    when others then'+ #13 +
    '      ERRO(''N�o foi encontrado os valores de pagamento para o or�amento '' || iORCAMENTO_ID || ''!'' || chr(13) || chr(10) || sqlerrm);'+ #13 +
    '  end;'+ #13 +
    #13 +
    '  if vDadosPagamentoPedido.STATUS = ''RE'' then'+ #13 +
    '    ERRO(''Este or�amento j� foi recebido!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if vDadosPagamentoPedido.STATUS not in(''VR'', ''VE'') then'+ #13 +
    '    ERRO(''Este pedido n�o est� aguardando recebimento, verifique!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  /* Apagando as previs�es dos recebimentos na entrega */'+ #13 +
    '  if vDadosPagamentoPedido.STATUS = ''VE'' then'+ #13 +
    '    delete from CONTAS_RECEBER'+ #13 +
    '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
    '    and COBRANCA_ID = SESSAO.PARAMETROS_GERAIS.TIPO_COB_RECEB_ENTREGA_ID;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  /* Inserindo cheques no financeiro */'+ #13 +
    '  if vDadosPagamentoPedido.VALOR_CHEQUE > 0 then'+ #13 +
    '    for vCheques in cCheques loop'+ #13 +
    '      select SEQ_RECEBER_ID.nextval'+ #13 +
    '      into vReceberId'+ #13 +
    '      from dual;'+ #13 +
    #13 +
    '      insert into CONTAS_RECEBER('+ #13 +
    '        RECEBER_ID,'+ #13 +
    '        CADASTRO_ID,'+ #13 +
    '        EMPRESA_ID,'+ #13 +
    '        ORCAMENTO_ID,'+ #13 +
    '        COBRANCA_ID,'+ #13 +
    '        PORTADOR_ID,'+ #13 +
    '        PLANO_FINANCEIRO_ID,'+ #13 +
    '        TURNO_ID,'+ #13 +
    '        VENDEDOR_ID,'+ #13 +
    '        DOCUMENTO,'+ #13 +
    '        BANCO,'+ #13 +
    '        AGENCIA,'+ #13 +
    '        CONTA_CORRENTE,'+ #13 +
    '        NUMERO_CHEQUE,'+ #13 +
    '        NOME_EMITENTE,'+ #13 +
    '        TELEFONE_EMITENTE,'+ #13 +
    '        DATA_EMISSAO,'+ #13 +
    '        DATA_VENCIMENTO,'+ #13 +
    '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
    '        VALOR_DOCUMENTO,'+ #13 +
    '        STATUS,'+ #13 +
    '        PARCELA,'+ #13 +
    '        NUMERO_PARCELAS,'+ #13 +
    '        ORIGEM'+ #13 +
    '      )values('+ #13 +
    '        vReceberId,'+ #13 +
    '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
    '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
    '        iORCAMENTO_ID,'+ #13 +
    '        vCheques.COBRANCA_ID,'+ #13 +
    '        ''9998'','+ #13 +
    '        ''1.001.002'','+ #13 +
    '        iTURNO_ID,'+ #13 +
    '        vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
    '        ''PED-'' || iORCAMENTO_ID || vCheques.ITEM_ID || ''/CHQ'','+ #13 +
    '        vCheques.BANCO,'+ #13 +
    '        vCheques.AGENCIA,'+ #13 +
    '        vCheques.CONTA_CORRENTE,'+ #13 +
    '        vCheques.NUMERO_CHEQUE,'+ #13 +
    '        vCheques.NOME_EMITENTE,'+ #13 +
    '        vCheques.TELEFONE_EMITENTE,'+ #13 +
    '        trunc(sysdate),'+ #13 +
    '        vCheques.DATA_VENCIMENTO,'+ #13 +
    '        vCheques.DATA_VENCIMENTO,'+ #13 +
    '        vCheques.VALOR_CHEQUE,'+ #13 +
    '        ''A'','+ #13 +
    '        vCheques.PARCELA,'+ #13 +
    '        vCheques.NUMERO_PARCELAS,'+ #13 +
    '        ''ORC'''+ #13 +
    '      );'+ #13 +
    '    end loop;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if vDadosPagamentoPedido.VALOR_COBRANCA > 0 then'+ #13 +
    '    for vCobrancas in cCobrancas(''COB'') loop'+ #13 +
    '      select SEQ_RECEBER_ID.nextval'+ #13 +
    '      into vReceberId'+ #13 +
    '      from dual;'+ #13 +
    #13 +
    '      insert into CONTAS_RECEBER('+ #13 +
    '        RECEBER_ID,'+ #13 +
    '        CADASTRO_ID,'+ #13 +
    '        EMPRESA_ID,'+ #13 +
    '        ORCAMENTO_ID,'+ #13 +
    '        COBRANCA_ID,'+ #13 +
    '        PORTADOR_ID,'+ #13 +
    '        PLANO_FINANCEIRO_ID,'+ #13 +
    '        TURNO_ID,'+ #13 +
    '        VENDEDOR_ID,'+ #13 +
    '        DOCUMENTO,'+ #13 +
    '        DATA_EMISSAO,'+ #13 +
    '        DATA_VENCIMENTO,'+ #13 +
    '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
    '        VALOR_DOCUMENTO,'+ #13 +
    '        STATUS,'+ #13 +
    '        PARCELA,'+ #13 +
    '        NUMERO_PARCELAS,'+ #13 +
    '        ORIGEM'+ #13 +
    '      )values('+ #13 +
    '        vReceberId,'+ #13 +
    '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
    '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
    '        iORCAMENTO_ID,'+ #13 +
    '        vCobrancas.COBRANCA_ID,'+ #13 +
    '        vCobrancas.PORTADOR_ID,'+ #13 +
    '        case when vCobrancas.BOLETO_BANCARIO = ''N'' then ''1.001.005'' else ''1.001.006'' end,'+ #13 +
    '        iTURNO_ID,'+ #13 +
    '        vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
    '        ''PED-'' || iORCAMENTO_ID || vCobrancas.ITEM_ID || case when vCobrancas.BOLETO_BANCARIO = ''S'' then ''/BOL'' else ''/COB'' end,'+ #13 +
    '        trunc(sysdate),'+ #13 +
    '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
    '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
    '        vCobrancas.VALOR,'+ #13 +
    '        ''A'','+ #13 +
    '        vCobrancas.PARCELA,'+ #13 +
    '        vCobrancas.NUMERO_PARCELAS,'+ #13 +
    '        ''ORC'''+ #13 +
    '      );'+ #13 +
    '    end loop;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if vDadosPagamentoPedido.VALOR_ACUMULATIVO > 0 then'+ #13 +
    '    AJUSTAR_CONTAS_REC_ACUMULATIVO(iORCAMENTO_ID);'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if iGERAR_CREDITO_TROCO = ''S'' and iVALOR_TROCO > 0 then'+ #13 +
    '    GERAR_CREDITO_TROCO(iORCAMENTO_ID, ''ORC'', vDadosPagamentoPedido.CLIENTE_ID, iVALOR_TROCO);'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if vReceberNaEntrega = ''S'' and vDadosPagamentoPedido.VALOR_CREDITO > 0 then'+ #13 +
    '    vValorRestante :='+ #13 +
    '      vValorTotal - ('+ #13 +
    '        vDadosPagamentoPedido.VALOR_DINHEIRO +'+ #13 +
    '        vDadosPagamentoPedido.VALOR_CHEQUE +'+ #13 +
    '        vDadosPagamentoPedido.VALOR_CARTAO_DEBITO +'+ #13 +
    '        vDadosPagamentoPedido.VALOR_CARTAO_CREDITO +'+ #13 +
    '        vDadosPagamentoPedido.VALOR_COBRANCA +'+ #13 +
    '        vDadosPagamentoPedido.VALOR_FINANCEIRA'+ #13 +
    '      );'+ #13 +
    #13 +
    '    for xCreditos in cCreditos loop'+ #13 +
    '      if xCreditos.VALOR_DOCUMENTO >= vValorRestante then'+ #13 +
    '        vValorBaixarCredito := vValorRestante;'+ #13 +
    '        vValorRestante := 0;'+ #13 +
    '      else'+ #13 +
    '        vValorBaixarCredito := xCreditos.VALOR_DOCUMENTO;'+ #13 +
    '        vValorRestante := vValorRestante - vValorBaixarCredito;'+ #13 +
    '      end if;'+ #13 +
    #13 +
    '      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iORCAMENTO_ID, ''ORC'', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);'+ #13 +
    #13 +
    '      if vValorRestante = 0 then'+ #13 +
    '        exit;'+ #13 +
    '      end if;'+ #13 +
    '    end loop;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  update ORCAMENTOS set'+ #13 +
    '    VALOR_TROCO = case when iGERAR_CREDITO_TROCO = ''N'' then iVALOR_TROCO else 0 end,'+ #13 +
    '    TURNO_ID = iTURNO_ID,'+ #13 +
    '    STATUS = ''RE'''+ #13 +
    '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
    #13 +
    '  /* N�o alterar este processo!!! Ele deve ser depois do recebimento */'+ #13 +
    '  if vDadosPagamentoPedido.VALOR_CARTAO_DEBITO + vDadosPagamentoPedido.VALOR_CARTAO_CREDITO > 0 then'+ #13 +
    '    for vCobrancas in cCobrancas(''CRT'') loop'+ #13 +
    '      GERAR_CARTOES_RECEBER(iORCAMENTO_ID, vCobrancas.ITEM_ID, vCobrancas.NSU_TEF, vCobrancas.CODIGO_AUTORIZACAO, vCobrancas.NUMERO_CARTAO);'+ #13 +
    '    end loop;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  select'+ #13 +
    '    max(RETIRADA_ID)'+ #13 +
    '  into'+ #13 +
    '    vRetiradaId'+ #13 +
    '  from'+ #13 +
    '    RETIRADAS'+ #13 +
    '  where TIPO_MOVIMENTO = ''A'''+ #13 +
    '  and ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
    #13 +
    '  if vRetiradaId is not null then'+ #13 +
    '    update RETIRADAS set'+ #13 +
    '      TIPO_NOTA_GERAR = nvl(iTIPO_NOTA_GERAR, ''NI'')'+ #13 +
    '    where RETIRADA_ID = vRetiradaId;'+ #13 +
    #13 +
    '    /* Caso o cliente trabalhe com confirma��o de sa�das de mercadorias */'+ #13 +
    '    if SESSAO.PARAMETROS_EMPRESA_LOGADA.CONFIRMAR_SAIDA_PRODUTOS = ''N'' then'+ #13 +
    '      CONFIRMAR_RETIRADA(vRetiradaId, ''AUTOMATICO'');'+ #13 +
    '    end if;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  ATUALIZAR_RETENCOES_CONTAS_REC(iORCAMENTO_ID, ''ORC'');'+ #13 +
    #13 +
    'end RECEBER_PEDIDO;'+ #13
end;

function AddColunasCONTAS_REC_BAIXAS_PAGAMENTOS: string;
begin
  Result :=
    'alter table CONTAS_REC_BAIXAS_PAGAMENTOS'+ #13 +
    'add( '+ #13 +
    '  NUMERO_CARTAO        varchar2(19),'+ #13 +
    '  CODIGO_AUTORIZACAO   varchar2(50)'+ #13 +
    ');'+ #13
end;

function AjustarProcGERAR_CARTOES_RECEBER_BAIXA: string;
begin
  Result :=
    'create or replace procedure GERAR_CARTOES_RECEBER_BAIXA('+ #13 +
    '  iBAIXA_ID         in number, -- 0'+ #13 +
    '  iITEM_ID          in number, -- 1'+ #13 +
    '  iNSU              in string, -- 2'+ #13 +
    '  iCODIGO_AUTOR_TEF in string, -- 3'+ #13 +
    '  iNUMERO_CARTAO    in string -- 4'+ #13 +
    ')'+ #13 +
    'is '+ #13 +
    '  i                     number default 0;'+ #13 +
    '  vValorCartao          number not null default 0;  '+ #13 +
    '  vCartoesGerar         RecordsFinanceiros.ArrayOfRecParcelasReceber;'+ #13 +
    #13 +
    '  vReceberId            CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
    '  vEmpresaId            CONTAS_RECEBER_BAIXAS.EMPRESA_ID%type;'+ #13 +
    '  vCadastroId           CONTAS_RECEBER_BAIXAS.CADASTRO_ID%type;'+ #13 +
    '  vTurnoId              CONTAS_RECEBER_BAIXAS.TURNO_ID%type;'+ #13 +
    #13 +
    '  cursor cCartoes is'+ #13 +
    '  select'+ #13 +
    '    PAG.COBRANCA_ID,'+ #13 +
    '    PAG.VALOR,'+ #13 +
    '    TIP.TIPO_CARTAO,'+ #13 +
    '    TIP.PORTADOR_ID'+ #13 +
    '  from'+ #13 +
    '    CONTAS_REC_BAIXAS_PAGAMENTOS PAG'+ #13 +
    #13 +
    '  inner join TIPOS_COBRANCA TIP'+ #13 +
    '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID'+ #13 +
    #13 +
    '  and TIP.FORMA_PAGAMENTO = ''CRT'''+ #13 +
    '  where PAG.BAIXA_ID = iBAIXA_ID'+ #13 +
    '  and PAG.ITEM_ID = iITEM_ID;'+ #13 +
    #13 +
    #13 +
    '  cursor c_dias_prazo(pCOBRANCA_ID positive) is'+ #13 +
    '  select'+ #13 +
    '    DIAS,'+ #13 +
    '    rownum as PARCELA,'+ #13 +
    '    count(*) over (partition by count(*)) as NUMERO_PARCELAS'+ #13 +
    '  from'+ #13 +
    '    TIPO_COBRANCA_DIAS_PRAZO      '+ #13 +
    '  where COBRANCA_ID = pCOBRANCA_ID  '+ #13 +
    '  group by'+ #13 +
    '    DIAS,'+ #13 +
    '    rownum'+ #13 +
    '  order by DIAS;'+ #13 +
    #13 +
    'begin'+ #13 +
    '  delete from CONTAS_RECEBER'+ #13 +
    '  where BAIXA_ORIGEM_ID = iBAIXA_ID'+ #13 +
    '  and ITEM_ID_CRT_ORCAMENTO = iITEM_ID;'+ #13 +
    #13 +
    '  update CONTAS_REC_BAIXAS_PAGAMENTOS set'+ #13 +
    '    NSU_TEF = iNSU'+ #13 +
    '  where BAIXA_ID = iBAIXA_ID'+ #13 +
    '  and ITEM_ID = iITEM_ID;'+ #13 +
    #13 +
    '  begin'+ #13 +
    '    select            '+ #13 +
    '      BAI.EMPRESA_ID,'+ #13 +
    '      BAI.CADASTRO_ID,      '+ #13 +
    '      BAI.TURNO_ID'+ #13 +
    '    into      '+ #13 +
    '      vEmpresaId,'+ #13 +
    '      vCadastroId,'+ #13 +
    '      vTurnoId'+ #13 +
    '    from'+ #13 +
    '      CONTAS_RECEBER_BAIXAS BAI'+ #13 +
    '    where BAI.BAIXA_ID = iBAIXA_ID;'+ #13 +
    '  exception'+ #13 +
    '    when others then'+ #13 +
    '     ERRO(''N�o foi encontrado os valores de pagamento para a baixa '' || iBAIXA_ID || ''! '' || sqlerrm);'+ #13 +
    '  end;'+ #13 +
    #13 +
    '   /* Inserindo os cart�es no financeiro */'+ #13 +
    '  for vCartoes in cCartoes loop'+ #13 +
    '    vValorCartao := vCartoes.VALOR;'+ #13 +
    '    for v_dias_prazo in c_dias_prazo(vCartoes.COBRANCA_ID) loop'+ #13 +
    '      vValorCartao := round(vValorCartao - vCartoes.VALOR / v_dias_prazo.NUMERO_PARCELAS, 2);'+ #13 +
    #13 +
    '      vCartoesGerar(i).COBRANCA_ID         := vCartoes.COBRANCA_ID;'+ #13 +
    '      vCartoesGerar(i).ITEM_ID             := iITEM_ID;'+ #13 +
    '      vCartoesGerar(i).TIPO_CARTAO         := vCartoes.TIPO_CARTAO;'+ #13 +
    '      vCartoesGerar(i).PARCELA             := v_dias_prazo.PARCELA;'+ #13 +
    '      vCartoesGerar(i).NUMERO_PARCELAS     := v_dias_prazo.NUMERO_PARCELAS;'+ #13 +
    '      vCartoesGerar(i).VALOR_PARCELA       := round(vCartoes.VALOR / v_dias_prazo.NUMERO_PARCELAS, 2);'+ #13 +
    '      vCartoesGerar(i).DATA_VENCIMENTO     := trunc(sysdate) + v_dias_prazo.DIAS;'+ #13 +
    '      vCartoesGerar(i).PORTADOR_ID         := vCartoes.PORTADOR_ID;'+ #13 +
    '      vCartoesGerar(i).PLANO_FINANCEIRO_ID := ''1.001.004'';'+ #13 +
    #13 +
    '      if v_dias_prazo.PARCELA = v_dias_prazo.NUMERO_PARCELAS and vValorCartao <> 0 then'+ #13 +
    '        vCartoesGerar(i).VALOR_PARCELA := vCartoesGerar(i).VALOR_PARCELA + vValorCartao;'+ #13 +
    '      end if;'+ #13 +
    #13 +
    '      i := i + 1;'+ #13 +
    '    end loop;      '+ #13 +
    '  end loop;'+ #13 +
    #13 +
    '  if vTurnoId is null then'+ #13 +
    '    ERRO(''Turno de recebimento do cart�o n�o encontrado!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if vCartoesGerar.last is null then'+ #13 +
    '    ERRO(''N�o foi poss�vel gerar as parcelas dos cart�es no contas a receber, por favor verifique o tipo de cobran�a selecionado!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  for i in vCartoesGerar.first..vCartoesGerar.last loop'+ #13 +
    '    vReceberId := SEQ_RECEBER_ID.nextval;'+ #13 +
    #13 +
    '    insert into CONTAS_RECEBER('+ #13 +
    '      RECEBER_ID,'+ #13 +
    '      CADASTRO_ID,'+ #13 +
    '      EMPRESA_ID,      '+ #13 +
    '      COBRANCA_ID,'+ #13 +
    '      PORTADOR_ID,'+ #13 +
    '      PLANO_FINANCEIRO_ID,'+ #13 +
    '      TURNO_ID,      '+ #13 +
    '      DOCUMENTO,'+ #13 +
    '      DATA_VENCIMENTO,'+ #13 +
    '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
    '      NSU,'+ #13 +
    '      CODIGO_AUTORIZACAO_TEF,'+ #13 +
    '      ITEM_ID_CRT_ORCAMENTO,'+ #13 +
    '      VALOR_DOCUMENTO,'+ #13 +
    '      STATUS,'+ #13 +
    '      PARCELA,'+ #13 +
    '      NUMERO_PARCELAS,'+ #13 +
    '      ORIGEM,'+ #13 +
    '      BAIXA_ORIGEM_ID,'+ #13 +
    '      NUMERO_CARTAO_TRUNCADO'+ #13 +
    '    )values('+ #13 +
    '      vReceberId,'+ #13 +
    '      vCadastroId,'+ #13 +
    '      vEmpresaId,      '+ #13 +
    '      vCartoesGerar(i).COBRANCA_ID,'+ #13 +
    '      vCartoesGerar(i).PORTADOR_ID,'+ #13 +
    '      vCartoesGerar(i).PLANO_FINANCEIRO_ID,'+ #13 +
    '      vTurnoId,      '+ #13 +
    '      ''BXR-'' || iBAIXA_ID || ''-'' || vCartoesGerar(i).PARCELA || ''/'' || vCartoesGerar(i).NUMERO_PARCELAS || ''/CR'' || vCartoesGerar(i).TIPO_CARTAO,'+ #13 +
    '      vCartoesGerar(i).DATA_VENCIMENTO,'+ #13 +
    '      vCartoesGerar(i).DATA_VENCIMENTO,'+ #13 +
    '      iNSU,'+ #13 +
    '      iCODIGO_AUTOR_TEF,'+ #13 +
    '      iITEM_ID,'+ #13 +
    '      vCartoesGerar(i).VALOR_PARCELA,'+ #13 +
    '      ''A'','+ #13 +
    '      vCartoesGerar(i).PARCELA,'+ #13 +
    '      vCartoesGerar(i).NUMERO_PARCELAS,'+ #13 +
    '      ''BXR'','+ #13 +
    '      iBAIXA_ID,'+ #13 +
    '      iNUMERO_CARTAO'+ #13 +
    '    );'+ #13 +
    '  end loop;'+ #13 +
    #13 +
    'end GERAR_CARTOES_RECEBER_BAIXA;'+ #13
end;

function AjustarProcCONSOLIDAR_BAIXA_CONTAS_REC: string;
begin
  Result :=
    'create or replace procedure CONSOLIDAR_BAIXA_CONTAS_REC(iBAIXA_RECEBER_ID in number)'+ #13 +
    'is'+ #13 +
    #13 +
    '  vReceberId     CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
    '  vCadastroId    CONTAS_RECEBER_BAIXAS.BAIXA_ID%type;'+ #13 +
    '  vEmpresaId     CONTAS_RECEBER_BAIXAS.EMPRESA_ID%type;'+ #13 +
    '  vValorRetencao CONTAS_RECEBER_BAIXAS.VALOR_RETENCAO%type;'+ #13 +
    #13 +
    '  vValorBaixarCredito   number default 0;'+ #13 +
    '  vValorRestante        number default 0;'+ #13 +
    #13 +
    '  vBaixaCreditoPagarId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;'+ #13 +
    '  vValorDinheiro        CONTAS_RECEBER_BAIXAS.VALOR_DINHEIRO%type;'+ #13 +
    '  vValorCheque          CONTAS_RECEBER_BAIXAS.VALOR_CHEQUE%type;'+ #13 +
    '  vValorCartaoDeb       CONTAS_RECEBER_BAIXAS.VALOR_CARTAO_DEBITO%type;'+ #13 +
    '  vValorCartaoCred      CONTAS_RECEBER_BAIXAS.VALOR_CARTAO_CREDITO%type;'+ #13 +
    '  vValorCobranca        CONTAS_RECEBER_BAIXAS.VALOR_COBRANCA%type;'+ #13 +
    '  vValorCredito         CONTAS_RECEBER_BAIXAS.VALOR_CREDITO%type;'+ #13 +
    '  vValorTotal           CONTAS_RECEBER_BAIXAS.VALOR_LIQUIDO%type;'+ #13 +
    #13 +
    '  cursor cCheques is'+ #13 +
    '  select'+ #13 +
    '    CHQ.COBRANCA_ID,'+ #13 +
    '    CHQ.DATA_VENCIMENTO,'+ #13 +
    '    CHQ.PARCELA,'+ #13 +
    '    CHQ.NUMERO_PARCELAS,'+ #13 +
    '    CHQ.BANCO,'+ #13 +
    '    CHQ.AGENCIA,     '+ #13 +
    '    CHQ.CONTA_CORRENTE,'+ #13 +
    '    CHQ.NOME_EMITENTE,'+ #13 +
    '    CHQ.TELEFONE_EMITENTE,'+ #13 +
    '    CHQ.VALOR_CHEQUE,'+ #13 +
    '    CHQ.NUMERO_CHEQUE,'+ #13 +
    '    CHQ.ITEM_ID'+ #13 +
    '  from'+ #13 +
    '    CONTAS_REC_BAIXAS_PAGTOS_CHQ CHQ'+ #13 +
    '  where CHQ.BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
    #13 +
    #13 +
    '  cursor cCobrancas(pTIPO in string) is'+ #13 +
    '  select'+ #13 +
    '    PAG.COBRANCA_ID,'+ #13 +
    '    PAG.ITEM_ID,'+ #13 +
    '    PAG.VALOR,'+ #13 +
    '    PAG.DATA_VENCIMENTO,'+ #13 +
    '    PAG.PARCELA,'+ #13 +
    '    PAG.NUMERO_PARCELAS,'+ #13 +
    '    TIP.BOLETO_BANCARIO,'+ #13 +
    '    PAG.NSU_TEF,'+ #13 +
    '    TIP.PORTADOR_ID'+ #13 +
    '  from'+ #13 +
    '    CONTAS_REC_BAIXAS_PAGAMENTOS PAG'+ #13 +
    #13 +
    '  join TIPOS_COBRANCA TIP'+ #13 +
    '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    '+ #13 +
    '  and TIP.FORMA_PAGAMENTO = pTIPO'+ #13 +
    #13 +
    '  where PAG.BAIXA_ID = iBAIXA_RECEBER_ID  '+ #13 +
    '  order by '+ #13 +
    '		COBRANCA_ID,'+ #13 +
    '		PARCELA,'+ #13 +
    '		DATA_VENCIMENTO, '+ #13 +
    '		ITEM_ID;'+ #13 +
    #13 +
    #13 +
    '  cursor cCartoes is'+ #13 +
    '  select'+ #13 +
    '    ITEM_ID,'+ #13 +
    '    NSU_TEF,'+ #13 +
    '    NUMERO_CARTAO,'+ #13 +
    '    CODIGO_AUTORIZACAO'+ #13 +
    '  from'+ #13 +
    '    CONTAS_REC_BAIXAS_PAGAMENTOS'+ #13 +
    '  where BAIXA_ID = iBAIXA_RECEBER_ID'+ #13 +
    '  and TIPO = ''CR'';'+ #13 +
    #13 +
    '  cursor cCreditos is'+ #13 +
    '  select'+ #13 +
    '    BAI.PAGAR_ID,'+ #13 +
    '    PAG.VALOR_DOCUMENTO'+ #13 +
    '  from'+ #13 +
    '    CONTAS_REC_BAIXAS_CREDITOS BAI'+ #13 +
    #13 +
    '  inner join CONTAS_PAGAR PAG'+ #13 +
    '  on BAI.PAGAR_ID = PAG.PAGAR_ID'+ #13 +
    #13 +
    '  where BAI.BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  select'+ #13 +
    '    EMPRESA_ID,'+ #13 +
    '    CADASTRO_ID,'+ #13 +
    '    VALOR_DINHEIRO,'+ #13 +
    '    VALOR_CHEQUE,'+ #13 +
    '    VALOR_CARTAO_DEBITO,'+ #13 +
    '    VALOR_CARTAO_CREDITO,'+ #13 +
    '    VALOR_COBRANCA,'+ #13 +
    '    VALOR_CREDITO,'+ #13 +
    '    VALOR_LIQUIDO,'+ #13 +
    '    VALOR_RETENCAO'+ #13 +
    '  into'+ #13 +
    '    vEmpresaId,'+ #13 +
    '    vCadastroId,'+ #13 +
    '    vValorDinheiro,'+ #13 +
    '    vValorCheque,'+ #13 +
    '    vValorCartaoDeb,'+ #13 +
    '    vValorCartaoCred,'+ #13 +
    '    vValorCobranca,'+ #13 +
    '    vValorCredito,'+ #13 +
    '    vValorTotal,'+ #13 +
    '    vValorRetencao'+ #13 +
    '  from'+ #13 +
    '    CONTAS_RECEBER_BAIXAS'+ #13 +
    '  where BAIXA_ID = iBAIXA_RECEBER_ID;'+ #13 +
    #13 +
    '  for xCheques in cCheques loop      '+ #13 +
    '    select SEQ_RECEBER_ID.nextval'+ #13 +
    '    into vReceberId'+ #13 +
    '    from dual;'+ #13 +
    #13 +
    '    insert into CONTAS_RECEBER('+ #13 +
    '      RECEBER_ID,'+ #13 +
    '      CADASTRO_ID,'+ #13 +
    '      EMPRESA_ID,      '+ #13 +
    '      COBRANCA_ID,'+ #13 +
    '      PORTADOR_ID,'+ #13 +
    '      PLANO_FINANCEIRO_ID,      '+ #13 +
    '      DOCUMENTO,'+ #13 +
    '      BANCO,'+ #13 +
    '      AGENCIA,'+ #13 +
    '      CONTA_CORRENTE,'+ #13 +
    '      NUMERO_CHEQUE,'+ #13 +
    '      NOME_EMITENTE,'+ #13 +
    '      TELEFONE_EMITENTE,'+ #13 +
    '      DATA_EMISSAO,'+ #13 +
    '      DATA_VENCIMENTO,'+ #13 +
    '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
    '      VALOR_DOCUMENTO,'+ #13 +
    '      STATUS,'+ #13 +
    '      PARCELA,'+ #13 +
    '      NUMERO_PARCELAS,'+ #13 +
    '      ORIGEM,'+ #13 +
    '      BAIXA_ORIGEM_ID'+ #13 +
    '    )values('+ #13 +
    '      vReceberId,'+ #13 +
    '      vCadastroId,'+ #13 +
    '      vEmpresaId,'+ #13 +
    '      xCheques.COBRANCA_ID,'+ #13 +
    '      ''9998'','+ #13 +
    '      ''1.001.002'',      '+ #13 +
    '      ''BXR-'' || iBAIXA_RECEBER_ID || xCheques.ITEM_ID || ''/CHQ'','+ #13 +
    '      xCheques.BANCO,'+ #13 +
    '      xCheques.AGENCIA,'+ #13 +
    '      xCheques.CONTA_CORRENTE,'+ #13 +
    '      xCheques.NUMERO_CHEQUE,'+ #13 +
    '      xCheques.NOME_EMITENTE,'+ #13 +
    '      xCheques.TELEFONE_EMITENTE,'+ #13 +
    '      trunc(sysdate),'+ #13 +
    '      xCheques.DATA_VENCIMENTO,'+ #13 +
    '      xCheques.DATA_VENCIMENTO,'+ #13 +
    '      xCheques.VALOR_CHEQUE,'+ #13 +
    '      ''A'','+ #13 +
    '      xCheques.PARCELA,'+ #13 +
    '      xCheques.NUMERO_PARCELAS,'+ #13 +
    '      ''BXR'','+ #13 +
    '      iBAIXA_RECEBER_ID'+ #13 +
    '    );    '+ #13 +
    '  end loop;'+ #13 +
    #13 +
    '  for xCobrancas in cCobrancas(''COB'') loop'+ #13 +
    '    select SEQ_RECEBER_ID.nextval'+ #13 +
    '    into vReceberId'+ #13 +
    '    from dual;'+ #13 +
    #13 +
    '    insert into CONTAS_RECEBER('+ #13 +
    '      RECEBER_ID,'+ #13 +
    '      CADASTRO_ID,'+ #13 +
    '      EMPRESA_ID,      '+ #13 +
    '      COBRANCA_ID,'+ #13 +
    '      PORTADOR_ID,'+ #13 +
    '      PLANO_FINANCEIRO_ID,        '+ #13 +
    '      DOCUMENTO,'+ #13 +
    '      DATA_EMISSAO,'+ #13 +
    '      DATA_VENCIMENTO,'+ #13 +
    '      DATA_VENCIMENTO_ORIGINAL,'+ #13 +
    '      VALOR_DOCUMENTO,'+ #13 +
    '      STATUS,'+ #13 +
    '      PARCELA,'+ #13 +
    '      NUMERO_PARCELAS,'+ #13 +
    '      ORIGEM,'+ #13 +
    '      BAIXA_ORIGEM_ID'+ #13 +
    '    )values('+ #13 +
    '      vReceberId,'+ #13 +
    '      vCadastroId,'+ #13 +
    '      vEmpresaId,'+ #13 +
    '      xCobrancas.COBRANCA_ID,'+ #13 +
    '      xCobrancas.PORTADOR_ID,'+ #13 +
    '      case when xCobrancas.BOLETO_BANCARIO = ''N'' then ''1.001.005'' else ''1.001.006'' end,            '+ #13 +
    '      ''BXR-'' || iBAIXA_RECEBER_ID || xCobrancas.ITEM_ID || case when xCobrancas.BOLETO_BANCARIO = ''S'' then ''/BOL'' else ''/COB'' end,'+ #13 +
    '      trunc(sysdate),'+ #13 +
    '      xCobrancas.DATA_VENCIMENTO,'+ #13 +
    '      xCobrancas.DATA_VENCIMENTO,'+ #13 +
    '      xCobrancas.VALOR,'+ #13 +
    '      ''A'','+ #13 +
    '      xCobrancas.PARCELA,'+ #13 +
    '      xCobrancas.NUMERO_PARCELAS,'+ #13 +
    '      ''BXR'','+ #13 +
    '      iBAIXA_RECEBER_ID'+ #13 +
    '    );'+ #13 +
    '  end loop;  '+ #13 +
    #13 +
    '  for xCartoes in cCartoes loop'+ #13 +
    '    GERAR_CARTOES_RECEBER_BAIXA(iBAIXA_RECEBER_ID, xCartoes.ITEM_ID, xCartoes.NSU_TEF, xCartoes.CODIGO_AUTORIZACAO, xCartoes.NUMERO_CARTAO);'+ #13 +
    '  end loop;'+ #13 +
    #13 +
    '  ATUALIZAR_RETENCOES_CONTAS_REC(iBAIXA_RECEBER_ID, ''BXR'');'+ #13 +
    #13 +
    '  if vValorCredito > 0 then'+ #13 +
    '    vValorRestante := vValorTotal - (vValorDinheiro + vValorCheque + vValorCartaoDeb + vValorCartaoCred + vValorCobranca);'+ #13 +
    '    for xCreditos in cCreditos loop'+ #13 +
    '      if xCreditos.VALOR_DOCUMENTO >= vValorRestante then'+ #13 +
    '        vValorBaixarCredito := vValorRestante;'+ #13 +
    '        vValorRestante := 0;'+ #13 +
    '      else'+ #13 +
    '        vValorBaixarCredito := xCreditos.VALOR_DOCUMENTO;'+ #13 +
    '        vValorRestante := vValorRestante - vValorBaixarCredito;'+ #13 +
    '      end if;'+ #13 +
    #13 +
    '      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iBAIXA_RECEBER_ID, ''BCR'', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);'+ #13 +
    #13 +
    '      if vValorRestante = 0 then'+ #13 +
    '        exit;'+ #13 +
    '      end if;'+ #13 +
    '    end loop;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end CONSOLIDAR_BAIXA_CONTAS_REC;'+ #13
end;

function AddColunaTabelaACUMULADOS_PAGAMENTOS: string;
begin
  Result :=
    'alter table ACUMULADOS_PAGAMENTOS'+ #13 +
    'add('+ #13 +
    '  CODIGO_AUTORIZACAO   varchar2(50),'+ #13 +
    '  NUMERO_CARTAO        varchar2(19)'+ #13 +
    ');'+ #13
end;

function AjustarProcGERAR_CARTOES_REC_ACUMULADO: string;
begin
  Result :=
    'create or replace procedure GERAR_CARTOES_REC_ACUMULADO('+ #13 +
    '  iACUMULADO_ID     in number, -- 0'+ #13 +
    '  iITEM_ID          in number, -- 1'+ #13 +
    '  iNSU              in string, -- 2'+ #13 +
    '  iCODIGO_AUTOR_TEF in string, -- 3'+ #13 +
    '  iNUMERO_CARTAO    in string  -- 4'+ #13 +
    ')'+ #13 +
    'is '+ #13 +
    '  i                     number default 0;'+ #13 +
    '  vValorCartao          number not null default 0;'+ #13 +
    '  vDadosPagamentoPedido RecordsVendas.RecPedidosPagamentos;'+ #13 +
    '  vCartoesGerar         RecordsFinanceiros.ArrayOfRecParcelasReceber;'+ #13 +
    '  vReceberId            CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
    #13 +
    '  cursor cCartoes is'+ #13 +
    '  select'+ #13 +
    '    PAG.COBRANCA_ID,'+ #13 +
    '    PAG.VALOR,'+ #13 +
    '    TIP.TIPO_CARTAO,'+ #13 +
    '    TIP.PORTADOR_ID'+ #13 +
    '  from'+ #13 +
    '    ACUMULADOS_PAGAMENTOS PAG'+ #13 +
    #13 +
    '  inner join TIPOS_COBRANCA TIP'+ #13 +
    '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID'+ #13 +
    #13 +
    '  and TIP.FORMA_PAGAMENTO = ''CRT'''+ #13 +
    '  where PAG.ACUMULADO_ID = iACUMULADO_ID'+ #13 +
    '  and PAG.ITEM_ID = iITEM_ID;'+ #13 +
    #13 +
    '  cursor c_dias_prazo(pCOBRANCA_ID positive) is'+ #13 +
    '  select'+ #13 +
    '    DIAS,'+ #13 +
    '    rownum as PARCELA,'+ #13 +
    '    count(*) over (partition by count(*)) as NUMERO_PARCELAS'+ #13 +
    '  from'+ #13 +
    '    TIPO_COBRANCA_DIAS_PRAZO      '+ #13 +
    '  where COBRANCA_ID = pCOBRANCA_ID  '+ #13 +
    '  group by'+ #13 +
    '    DIAS,'+ #13 +
    '    rownum'+ #13 +
    '  order by DIAS;'+ #13 +
    #13 +
    'begin'+ #13 +
    '  delete from CONTAS_RECEBER'+ #13 +
    '  where ACUMULADO_ID = iACUMULADO_ID'+ #13 +
    '  and ITEM_ID_CRT_ORCAMENTO = iITEM_ID;'+ #13 +
    #13 +
    '  update ACUMULADOS_PAGAMENTOS set'+ #13 +
    '    NSU_TEF = iNSU'+ #13 +
    '  where ACUMULADO_ID = iACUMULADO_ID'+ #13 +
    '  and ITEM_ID = iITEM_ID;'+ #13 +
    #13 +
    '  begin'+ #13 +
    '    select'+ #13 +
    '      0 as VALOR_DINHEIRO,'+ #13 +
    '      ACU.VALOR_CARTAO_DEBITO,'+ #13 +
    '      ACU.VALOR_CARTAO_CREDITO,'+ #13 +
    '      0 as VALOR_CREDITO,'+ #13 +
    '      0 as VALOR_COBRANCA,'+ #13 +
    '      0 as VALOR_CHEQUE,'+ #13 +
    '      0 as VALOR_FINANCEIRA,'+ #13 +
    '      0 as VALOR_ACUMULATIVO,      '+ #13 +
    '      ACU.EMPRESA_ID,'+ #13 +
    '      ACU.CLIENTE_ID,'+ #13 +
    '      ACU.STATUS,'+ #13 +
    '      ACU.TURNO_ID'+ #13 +
    '    into'+ #13 +
    '      vDadosPagamentoPedido.VALOR_DINHEIRO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CREDITO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_COBRANCA,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CHEQUE,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_FINANCEIRA,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_ACUMULATIVO,      '+ #13 +
    '      vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
    '      vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
    '      vDadosPagamentoPedido.STATUS,'+ #13 +
    '      vDadosPagamentoPedido.TURNO_ID'+ #13 +
    '    from'+ #13 +
    '      ACUMULADOS ACU'+ #13 +
    '    where ACU.ACUMULADO_ID = iACUMULADO_ID;'+ #13 +
    '  exception'+ #13 +
    '    when others then'+ #13 +
    '     ERRO(''N�o foi encontrado os valores de pagamento para o acumulado '' || iACUMULADO_ID || ''!'' || sqlerrm);'+ #13 +
    '  end;'+ #13 +
    #13 +
    '   /* Inserindo os cart�es no financeiro */'+ #13 +
    '  if vDadosPagamentoPedido.VALOR_CARTAO_DEBITO + vDadosPagamentoPedido.VALOR_CARTAO_CREDITO > 0 then'+ #13 +
    '    for vCartoes in cCartoes loop'+ #13 +
    '      vValorCartao := vCartoes.VALOR;'+ #13 +
    '      for v_dias_prazo in c_dias_prazo(vCartoes.COBRANCA_ID) loop'+ #13 +
    '        vValorCartao := round(vValorCartao - vCartoes.VALOR / v_dias_prazo.NUMERO_PARCELAS, 2);'+ #13 +
    #13 +
    '        vCartoesGerar(i).COBRANCA_ID         := vCartoes.COBRANCA_ID;'+ #13 +
    '        vCartoesGerar(i).ITEM_ID             := iITEM_ID;'+ #13 +
    '        vCartoesGerar(i).TIPO_CARTAO         := vCartoes.TIPO_CARTAO;'+ #13 +
    '        vCartoesGerar(i).PARCELA             := v_dias_prazo.PARCELA;'+ #13 +
    '        vCartoesGerar(i).NUMERO_PARCELAS     := v_dias_prazo.NUMERO_PARCELAS;'+ #13 +
    '        vCartoesGerar(i).VALOR_PARCELA       := round(vCartoes.VALOR / v_dias_prazo.NUMERO_PARCELAS, 2);'+ #13 +
    '        vCartoesGerar(i).DATA_VENCIMENTO     := trunc(sysdate) + v_dias_prazo.DIAS;'+ #13 +
    '        vCartoesGerar(i).PORTADOR_ID         := vCartoes.PORTADOR_ID;'+ #13 +
    '        vCartoesGerar(i).PLANO_FINANCEIRO_ID := ''1.001.004'';'+ #13 +
    #13 +
    '        if v_dias_prazo.PARCELA = v_dias_prazo.NUMERO_PARCELAS and vValorCartao <> 0 then'+ #13 +
    '          vCartoesGerar(i).VALOR_PARCELA := vCartoesGerar(i).VALOR_PARCELA + vValorCartao;'+ #13 +
    '        end if;'+ #13 +
    #13 +
    '        i := i + 1;'+ #13 +
    '      end loop;      '+ #13 +
    '    end loop;'+ #13 +
    #13 +
    '    if vDadosPagamentoPedido.TURNO_ID is null then'+ #13 +
    '      ERRO(''Turno de recebimento do cart�o n�o encontrado!'');'+ #13 +
    '    end if;'+ #13 +
    #13 +
    '    if vCartoesGerar.first is null then'+ #13 +
    '      ERRO(''O Altis n�o conseguiu gerar as parcelas do cart�o corretamente, por favor verifique se o tipo de cobran�a est� parametrizado corretamente!'');'+ #13 +
    '    end if;'+ #13 +
    #13 +
    '    for i in vCartoesGerar.first..vCartoesGerar.last loop'+ #13 +
    '      vReceberId := SEQ_RECEBER_ID.nextval;'+ #13 +
    #13 +
    '      insert into CONTAS_RECEBER('+ #13 +
    '        RECEBER_ID,'+ #13 +
    '        CADASTRO_ID,'+ #13 +
    '        EMPRESA_ID,'+ #13 +
    '        ORIGEM,'+ #13 +
    '        ACUMULADO_ID,'+ #13 +
    '        COBRANCA_ID,'+ #13 +
    '        PORTADOR_ID,'+ #13 +
    '        PLANO_FINANCEIRO_ID,'+ #13 +
    '        TURNO_ID,'+ #13 +
    '        VENDEDOR_ID,'+ #13 +
    '        DOCUMENTO,'+ #13 +
    '        DATA_VENCIMENTO,'+ #13 +
    '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
    '        NSU,'+ #13 +
    '        CODIGO_AUTORIZACAO_TEF,'+ #13 +
    '        ITEM_ID_CRT_ORCAMENTO,'+ #13 +
    '        VALOR_DOCUMENTO,'+ #13 +
    '        STATUS,'+ #13 +
    '        PARCELA,'+ #13 +
    '        NUMERO_PARCELAS,'+ #13 +
    '        NUMERO_CARTAO_TRUNCADO'+ #13 +
    '      )values('+ #13 +
    '        vReceberId,'+ #13 +
    '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
    '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
    '        ''ACU'','+ #13 +
    '        iACUMULADO_ID,'+ #13 +
    '        vCartoesGerar(i).COBRANCA_ID,'+ #13 +
    '        vCartoesGerar(i).PORTADOR_ID,'+ #13 +
    '        vCartoesGerar(i).PLANO_FINANCEIRO_ID,'+ #13 +
    '        vDadosPagamentoPedido.TURNO_ID,'+ #13 +
    '        vDadosPagamentoPedido.VENDEDOR_ID,'+ #13 +
    '        ''ACU-'' || iACUMULADO_ID || ''-'' || vCartoesGerar(i).PARCELA || ''/'' || vCartoesGerar(i).NUMERO_PARCELAS || ''/CR'' || vCartoesGerar(i).TIPO_CARTAO,'+ #13 +
    '        vCartoesGerar(i).DATA_VENCIMENTO,'+ #13 +
    '        vCartoesGerar(i).DATA_VENCIMENTO,'+ #13 +
    '        iNSU,'+ #13 +
    '        iCODIGO_AUTOR_TEF,'+ #13 +
    '        iITEM_ID,'+ #13 +
    '        vCartoesGerar(i).VALOR_PARCELA,'+ #13 +
    '        ''A'','+ #13 +
    '        vCartoesGerar(i).PARCELA,'+ #13 +
    '        vCartoesGerar(i).NUMERO_PARCELAS,'+ #13 +
    '        iNUMERO_CARTAO'+ #13 +
    '      );'+ #13 +
    '    end loop;     '+ #13 +
    '  end if;   '+ #13 +
    #13 +
    'end GERAR_CARTOES_REC_ACUMULADO;'+ #13
end;

function AjustarProcRECEBER_ACUMULADO: string;
begin
  Result :=
    'create or replace procedure RECEBER_ACUMULADO('+ #13 +
    '  iACUMULADO_ID        in number,'+ #13 +
    '  iTURNO_ID            in number,'+ #13 +
    '  iVALOR_TROCO         in number,'+ #13 +
    '  iGERAR_CREDITO_TROCO in string,'+ #13 +
    '  iTIPO_NOTA_GERAR     in string'+ #13 +
    ')'+ #13 +
    'is'+ #13 +
    '  i                       naturaln default 0;  '+ #13 +
    '  vReceberId              CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
    '  vBaixaId                CONTAS_RECEBER_BAIXAS.BAIXA_ID%type;'+ #13 +
    '  vDadosPagamentoPedido   RecordsVendas.RecPedidosPagamentos;'+ #13 +
    #13 +
    '  cursor cCheques is'+ #13 +
    '  select'+ #13 +
    '    COBRANCA_ID,'+ #13 +
    '    ITEM_ID,'+ #13 +
    '    DATA_VENCIMENTO,'+ #13 +
    '    BANCO,'+ #13 +
    '    AGENCIA,'+ #13 +
    '    CONTA_CORRENTE,'+ #13 +
    '    NUMERO_CHEQUE,'+ #13 +
    '    VALOR_CHEQUE,'+ #13 +
    '    NOME_EMITENTE,'+ #13 +
    '    TELEFONE_EMITENTE,'+ #13 +
    '    PARCELA,'+ #13 +
    '    NUMERO_PARCELAS'+ #13 +
    '  from'+ #13 +
    '    ACUMULADOS_PAGAMENTOS_CHEQUES'+ #13 +
    '  where ACUMULADO_ID = iACUMULADO_ID'+ #13 +
    '  order by '+ #13 +
    '		COBRANCA_ID, '+ #13 +
    '		PARCELA,'+ #13 +
    '		DATA_VENCIMENTO, '+ #13 +
    '		ITEM_ID;'+ #13 +
    #13 +
    '  cursor cCobrancas(pTIPO in string) is'+ #13 +
    '  select'+ #13 +
    '    PAG.COBRANCA_ID,'+ #13 +
    '    PAG.ITEM_ID,'+ #13 +
    '    PAG.VALOR,'+ #13 +
    '    PAG.DATA_VENCIMENTO,'+ #13 +
    '    PAG.PARCELA,'+ #13 +
    '    PAG.NUMERO_PARCELAS,'+ #13 +
    '    TIP.BOLETO_BANCARIO,'+ #13 +
    '    PAG.NSU_TEF,'+ #13 +
    '    PAG.CODIGO_AUTORIZACAO,'+ #13 +
    '    PAG.NUMERO_CARTAO,'+ #13 +
    '    TIP.PORTADOR_ID'+ #13 +
    '  from'+ #13 +
    '    ACUMULADOS_PAGAMENTOS PAG'+ #13 +
    #13 +
    '  join TIPOS_COBRANCA TIP'+ #13 +
    '  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    '+ #13 +
    '  and TIP.FORMA_PAGAMENTO = pTIPO'+ #13 +
    #13 +
    '  where PAG.ACUMULADO_ID = iACUMULADO_ID  '+ #13 +
    '  order by '+ #13 +
    '		COBRANCA_ID,'+ #13 +
    '		PARCELA,'+ #13 +
    '		DATA_VENCIMENTO, '+ #13 +
    '		ITEM_ID;'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  begin'+ #13 +
    '    select'+ #13 +
    '      VALOR_DINHEIRO,'+ #13 +
    '      VALOR_CARTAO_DEBITO,'+ #13 +
    '      VALOR_CARTAO_CREDITO,'+ #13 +
    '      VALOR_CREDITO,'+ #13 +
    '      VALOR_COBRANCA,'+ #13 +
    '      VALOR_CHEQUE,'+ #13 +
    '      VALOR_FINANCEIRA,      '+ #13 +
    '      STATUS,      '+ #13 +
    '      EMPRESA_ID,'+ #13 +
    '      CLIENTE_ID'+ #13 +
    '    into'+ #13 +
    '      vDadosPagamentoPedido.VALOR_DINHEIRO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CREDITO,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_COBRANCA,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_CHEQUE,'+ #13 +
    '      vDadosPagamentoPedido.VALOR_FINANCEIRA,      '+ #13 +
    '      vDadosPagamentoPedido.STATUS,      '+ #13 +
    '      vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
    '      vDadosPagamentoPedido.CLIENTE_ID'+ #13 +
    '    from'+ #13 +
    '      ACUMULADOS'+ #13 +
    '    where ACUMULADO_ID = iACUMULADO_ID;'+ #13 +
    '  exception'+ #13 +
    '    when others then'+ #13 +
    '      ERRO(''N�o foi encontrado os valores de pagamento para o acumulado '' || iACUMULADO_ID || ''!'' || chr(13) || chr(10) || sqlerrm);'+ #13 +
    '  end;'+ #13 +
    #13 +
    '  if vDadosPagamentoPedido.STATUS = ''RE'' then'+ #13 +
    '    ERRO(''Este acumulado j� foi recebido!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if vDadosPagamentoPedido.STATUS not in(''AR'') then'+ #13 +
    '    ERRO(''Este acumulado n�o est� aguardando recebimento, verifique!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  /* Inserindo cheques no financeiro */'+ #13 +
    '  if vDadosPagamentoPedido.VALOR_CHEQUE > 0 then'+ #13 +
    '    for vCheques in cCheques loop'+ #13 +
    '      select SEQ_RECEBER_ID.nextval'+ #13 +
    '      into vReceberId'+ #13 +
    '      from dual;'+ #13 +
    #13 +
    '      insert into CONTAS_RECEBER('+ #13 +
    '        RECEBER_ID,'+ #13 +
    '        CADASTRO_ID,'+ #13 +
    '        EMPRESA_ID,'+ #13 +
    '        ACUMULADO_ID,'+ #13 +
    '        COBRANCA_ID,'+ #13 +
    '        PORTADOR_ID,'+ #13 +
    '        PLANO_FINANCEIRO_ID,'+ #13 +
    '        TURNO_ID,        '+ #13 +
    '        DOCUMENTO,'+ #13 +
    '        BANCO,'+ #13 +
    '        AGENCIA,'+ #13 +
    '        CONTA_CORRENTE,'+ #13 +
    '        NUMERO_CHEQUE,'+ #13 +
    '        NOME_EMITENTE,'+ #13 +
    '        TELEFONE_EMITENTE,'+ #13 +
    '        DATA_EMISSAO,'+ #13 +
    '        DATA_VENCIMENTO,'+ #13 +
    '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
    '        VALOR_DOCUMENTO,'+ #13 +
    '        STATUS,'+ #13 +
    '        PARCELA,'+ #13 +
    '        NUMERO_PARCELAS,'+ #13 +
    '        ORIGEM'+ #13 +
    '      )values('+ #13 +
    '        vReceberId,'+ #13 +
    '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
    '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
    '        iACUMULADO_ID,'+ #13 +
    '        vCheques.COBRANCA_ID,'+ #13 +
    '        ''9998'','+ #13 +
    '        ''1.001.002'','+ #13 +
    '        iTURNO_ID,        '+ #13 +
    '        ''ACU-'' || iACUMULADO_ID || '' - '' || vCheques.ITEM_ID || ''/CHQ'','+ #13 +
    '        vCheques.BANCO,'+ #13 +
    '        vCheques.AGENCIA,'+ #13 +
    '        vCheques.CONTA_CORRENTE,'+ #13 +
    '        vCheques.NUMERO_CHEQUE,'+ #13 +
    '        vCheques.NOME_EMITENTE,'+ #13 +
    '        vCheques.TELEFONE_EMITENTE,'+ #13 +
    '        trunc(sysdate),'+ #13 +
    '        vCheques.DATA_VENCIMENTO,'+ #13 +
    '        vCheques.DATA_VENCIMENTO,'+ #13 +
    '        vCheques.VALOR_CHEQUE,'+ #13 +
    '        ''A'','+ #13 +
    '        vCheques.PARCELA,'+ #13 +
    '        vCheques.NUMERO_PARCELAS,'+ #13 +
    '        ''ACU'''+ #13 +
    '      );'+ #13 +
    '    end loop;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if vDadosPagamentoPedido.VALOR_COBRANCA > 0 then'+ #13 +
    '    for vCobrancas in cCobrancas(''COB'') loop'+ #13 +
    '      select SEQ_RECEBER_ID.nextval'+ #13 +
    '      into vReceberId'+ #13 +
    '      from dual;'+ #13 +
    #13 +
    '      insert into CONTAS_RECEBER('+ #13 +
    '        RECEBER_ID,'+ #13 +
    '        CADASTRO_ID,'+ #13 +
    '        EMPRESA_ID,'+ #13 +
    '        ACUMULADO_ID,'+ #13 +
    '        COBRANCA_ID,'+ #13 +
    '        PORTADOR_ID,'+ #13 +
    '        PLANO_FINANCEIRO_ID,'+ #13 +
    '        TURNO_ID,        '+ #13 +
    '        DOCUMENTO,'+ #13 +
    '        DATA_EMISSAO,'+ #13 +
    '        DATA_VENCIMENTO,'+ #13 +
    '        DATA_VENCIMENTO_ORIGINAL,'+ #13 +
    '        VALOR_DOCUMENTO,'+ #13 +
    '        STATUS,'+ #13 +
    '        PARCELA,'+ #13 +
    '        NUMERO_PARCELAS,'+ #13 +
    '        ORIGEM'+ #13 +
    '      )values('+ #13 +
    '        vReceberId,'+ #13 +
    '        vDadosPagamentoPedido.CLIENTE_ID,'+ #13 +
    '        vDadosPagamentoPedido.EMPRESA_ID,'+ #13 +
    '        iACUMULADO_ID,'+ #13 +
    '        vCobrancas.COBRANCA_ID,'+ #13 +
    '        vCobrancas.PORTADOR_ID,'+ #13 +
    '        case when vCobrancas.BOLETO_BANCARIO = ''N'' then ''1.001.005'' else ''1.001.006'' end,'+ #13 +
    '        iTURNO_ID,        '+ #13 +
    '        ''ACU-'' || iACUMULADO_ID  || '' - '' || vCobrancas.ITEM_ID || case when vCobrancas.BOLETO_BANCARIO = ''S'' then ''/BOL'' else ''/COB'' end,'+ #13 +
    '        trunc(sysdate),'+ #13 +
    '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
    '        vCobrancas.DATA_VENCIMENTO,'+ #13 +
    '        vCobrancas.VALOR,'+ #13 +
    '        ''A'','+ #13 +
    '        vCobrancas.PARCELA,'+ #13 +
    '        vCobrancas.NUMERO_PARCELAS,'+ #13 +
    '        ''ACU'''+ #13 +
    '      );'+ #13 +
    '    end loop;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if iGERAR_CREDITO_TROCO = ''S'' and iVALOR_TROCO > 0 then'+ #13 +
    '    GERAR_CREDITO_TROCO(iACUMULADO_ID, ''ACU'', vDadosPagamentoPedido.CLIENTE_ID, iVALOR_TROCO);'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  update ACUMULADOS set'+ #13 +
    '    VALOR_TROCO = case when iGERAR_CREDITO_TROCO = ''N'' then iVALOR_TROCO else 0 end,'+ #13 +
    '    TURNO_ID = iTURNO_ID,'+ #13 +
    '    STATUS = ''RE'''+ #13 +
    '  where ACUMULADO_ID = iACUMULADO_ID;'+ #13 +
    #13 +
    '  /* N�o alterar este processo!!! Ele deve ser depois do recebimento */'+ #13 +
    '  if vDadosPagamentoPedido.VALOR_CARTAO_DEBITO + vDadosPagamentoPedido.VALOR_CARTAO_CREDITO > 0 then'+ #13 +
    '    for vCobrancas in cCobrancas(''CRT'') loop'+ #13 +
    '      GERAR_CARTOES_REC_ACUMULADO(iACUMULADO_ID, vCobrancas.ITEM_ID, vCobrancas.NSU_TEF, vCobrancas.CODIGO_AUTORIZACAO, vCobrancas.NUMERO_CARTAO);'+ #13 +
    '    end loop;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  ATUALIZAR_RETENCOES_CONTAS_REC(iACUMULADO_ID, ''ACU'');'+ #13 +
    #13 +
    '  GERAR_NOTA_ACUMULADO(iACUMULADO_ID);'+ #13 +
    #13 +
    'end RECEBER_ACUMULADO;'+ #13
end;

function AdicionarColunaTIPO_RECEB_CARTAOTabelaCONTAS_REC_BAIXAS_PAGAMENTOS: string;
begin
  Result :=
    'alter table CONTAS_REC_BAIXAS_PAGAMENTOS'+ #13 +
    'add TIPO_RECEB_CARTAO char(3);'+ #13
end;

function AdicionarColunaTIPO_RECEB_CARTAOTabelaORCAMENTOS_PAGAMENTOS: string;
begin
  Result :=
    'alter table ORCAMENTOS_PAGAMENTOS'+ #13 +
    'add TIPO_RECEB_CARTAO    char(3);'+ #13
end;

function AdicionarColunaTIPO_RECEB_CARTAOTabelaACUMULADOS_PAGAMENTOS: string;
begin
  Result :=
    'alter table ACUMULADOS_PAGAMENTOS'+ #13 +
    'add  TIPO_RECEB_CARTAO    char(3);'+ #13
end;

function AlimentarTIPO_RECEB_CARTAOTabelaCONTAS_REC_BAIXAS_PAGAMENTOS: string;
begin
  Result :=
    'begin'+ #13 +
    #13 +
    '  INICIAR_SESSAO(1, 1, ''ALTIS'', ''ALTIS'');'+ #13 +
    #13 +
    '  update CONTAS_REC_BAIXAS_PAGAMENTOS set'+ #13 +
    '    TIPO_RECEB_CARTAO = ''POS'''+ #13 +
    '  where TIPO = ''CR'';'+ #13 +
    #13 +
    'end;'+ #13 +
    '/' +
    #13 +
    'alter table CONTAS_REC_BAIXAS_PAGAMENTOS'+ #13 +
    'drop constraint CK_CON_REC_BX_PAGTOS_DATA_VENC;'+ #13 +
    #13 +
    'alter table CONTAS_REC_BAIXAS_PAGAMENTOS'+ #13 +
    'add constraint CK_CON_REC_BX_PAGTOS_DATA_VENC'+ #13 +
    'check('+ #13 +
    '  (TIPO = ''CR'' and DATA_VENCIMENTO is null and PARCELA is null and NUMERO_PARCELAS is null and TIPO_RECEB_CARTAO is not null)'+ #13 +
    '  or'+ #13 +
    '  (TIPO = ''CO'' and DATA_VENCIMENTO is not null and PARCELA is not null and NUMERO_PARCELAS is not null and TIPO_RECEB_CARTAO is null)'+ #13 +
    ');'+ #13 +
    #13 +
    'alter table CONTAS_REC_BAIXAS_PAGAMENTOS'+ #13 +
    'add constraint CK_CON_REC_BX_PAG_TP_REC_CRT'+ #13 +
    'check(TIPO_RECEB_CARTAO in(null, ''TEF'', ''POS''));'+ #13
end;

function AlimentarTIPO_RECEB_CARTAOTabelaORCAMENTOS_PAGAMENTOS: string;
begin
  Result :=
    'begin'+ #13 +
    #13 +
    '  INICIAR_SESSAO(1, 1, ''ALTIS'', ''ALTIS'');'+ #13 +
    #13 +
    '  update ORCAMENTOS_PAGAMENTOS set'+ #13 +
    '    TIPO_RECEB_CARTAO = ''POS'''+ #13 +
    '  where TIPO = ''CR'';'+ #13 +
    #13 +
    'end;'+ #13 +
    '/' +
    #13 +
    'alter table ORCAMENTOS_PAGAMENTOS'+ #13 +
    'drop constraint CK_ORC_PAGTOS_DATA_VENCIMENTO;'+ #13 +
    #13 +
    'alter table ORCAMENTOS_PAGAMENTOS'+ #13 +
    'add constraint CK_ORC_PAGTOS_DATA_VENCIMENTO'+ #13 +
    'check('+ #13 +
    '  (TIPO = ''CR'' and DATA_VENCIMENTO is null and TIPO_RECEB_CARTAO is not null)'+ #13 +
    '  or'+ #13 +
    '  (TIPO in(''CO'', ''FI'') and DATA_VENCIMENTO is not null and TIPO_RECEB_CARTAO is null)'+ #13 +
    ');'+ #13 +
    #13 +
    'alter table ORCAMENTOS_PAGAMENTOS'+ #13 +
    'add constraint CK_ORC_PAGTOS_TIPO_REC_CRT'+ #13 +
    'check(TIPO_RECEB_CARTAO in(null, ''TEF'', ''POS''));'+ #13
end;

function AlimentarTIPO_RECEB_CARTAOTabelaACUMULADOS_PAGAMENTOS: string;
begin
  Result :=
    'begin'+ #13 +
    #13 +
    '  INICIAR_SESSAO(1, 1, ''ALTIS'', ''ALTIS'');'+ #13 +
    #13 +
    '  update ACUMULADOS_PAGAMENTOS set'+ #13 +
    '    TIPO_RECEB_CARTAO = ''POS'''+ #13 +
    '  where TIPO = ''CR'';'+ #13 +
    #13 +
    'end;'+ #13 +
    '/' +
    #13 +
    'alter table ACUMULADOS_PAGAMENTOS'+ #13 +
    'drop constraint CK_ACU_PAGTOS_DATA_VENCIMENTO;'+ #13 +
    #13 +
    'alter table ACUMULADOS_PAGAMENTOS'+ #13 +
    'add constraint CK_ACU_PAGTOS_DATA_VENCIMENTO'+ #13 +
    'check('+ #13 +
    '  (TIPO = ''CR'' and DATA_VENCIMENTO is null and TIPO_RECEB_CARTAO is not null)'+ #13 +
    '  or'+ #13 +
    '  (TIPO in(''CO'', ''FI'') and DATA_VENCIMENTO is not null and TIPO_RECEB_CARTAO is null)'+ #13 +
    ');'+ #13
end;

function AdicionarColunaBANDEIRA_CARTAO_NFE: string;
begin
  Result :=
    'alter table TIPOS_COBRANCA'+ #13 +
    'add BANDEIRA_CARTAO_NFE          char(2);'+ #13
end;

function AlimentarColunaBANDEIRA_CARTAO_NFE: string;
begin
  Result :=
    'begin'+ #13 +
    '  INICIAR_SESSAO(1, 1, ''ALTIS'', ''ALTIS'');'+ #13 +
    #13 +
    '  update TIPOS_COBRANCA set'+ #13 +
    '    BANDEIRA_CARTAO_NFE = ''01'''+ #13 +
    '  where NOME like ''%VISA%'''+ #13 +
    '  and FORMA_PAGAMENTO = ''CRT'';'+ #13 +
    #13 +
    '  update TIPOS_COBRANCA set'+ #13 +
    '    BANDEIRA_CARTAO_NFE = ''02'''+ #13 +
    '  where NOME like ''%MASTER%'''+ #13 +
    '  and FORMA_PAGAMENTO = ''CRT'';'+ #13 +
    #13 +
    '  update TIPOS_COBRANCA set'+ #13 +
    '    BANDEIRA_CARTAO_NFE = ''03'''+ #13 +
    '  where NOME like ''%AMEX%'' or NOME like ''%EXPRESS%'' or NOME like ''%AMERICAN%'''+ #13 +
    '  and FORMA_PAGAMENTO = ''CRT'';'+ #13 +
    #13 +
    '  update TIPOS_COBRANCA set'+ #13 +
    '    BANDEIRA_CARTAO_NFE = ''04'''+ #13 +
    '  where NOME like ''%SORO%'''+ #13 +
    '  and FORMA_PAGAMENTO = ''CRT'';'+ #13 +
    #13 +
    '  update TIPOS_COBRANCA set'+ #13 +
    '    BANDEIRA_CARTAO_NFE = ''05'''+ #13 +
    '  where NOME like ''%DINER%'''+ #13 +
    '  and FORMA_PAGAMENTO = ''CRT'';'+ #13 +
    #13 +
    '  update TIPOS_COBRANCA set'+ #13 +
    '    BANDEIRA_CARTAO_NFE = ''06'''+ #13 +
    '  where NOME like ''%ELO%'''+ #13 +
    '  and FORMA_PAGAMENTO = ''CRT'';'+ #13 +
    #13 +
    '  update TIPOS_COBRANCA set'+ #13 +
    '    BANDEIRA_CARTAO_NFE = ''07'''+ #13 +
    '  where NOME like ''%HIPER%'''+ #13 +
    '  and FORMA_PAGAMENTO = ''CRT'';'+ #13 +
    #13 +
    '  update TIPOS_COBRANCA set'+ #13 +
    '    BANDEIRA_CARTAO_NFE = ''08'''+ #13 +
    '  where NOME like ''%AURA%'''+ #13 +
    '  and FORMA_PAGAMENTO = ''CRT'';'+ #13 +
    #13 +
    '  update TIPOS_COBRANCA set'+ #13 +
    '    BANDEIRA_CARTAO_NFE = ''09'''+ #13 +
    '  where NOME like ''%CABAL%'''+ #13 +
    '  and FORMA_PAGAMENTO = ''CRT'';'+ #13 +
    #13 +
    '  update TIPOS_COBRANCA set'+ #13 +
    '    BANDEIRA_CARTAO_NFE = ''99'''+ #13 +
    '  where FORMA_PAGAMENTO = ''CRT'''+ #13 +
    '  and BANDEIRA_CARTAO_NFE is null;'+ #13 +
    'end;';
end;

function AjustarConstraintCK_TP_COBRANCA_FORMA_PAGTO_CRT: string;
begin
  Result :=
    'alter table TIPOS_COBRANCA'+ #13 +
    'drop constraint CK_TP_COBRANCA_FORMA_PAGTO_CRT;'+ #13 +
    #13 +
    'alter table TIPOS_COBRANCA'+ #13 +
    'add constraint CK_TP_COBRANCA_FORMA_PAGTO_CRT'+ #13 +
    'check('+ #13 +
    '  FORMA_PAGAMENTO <> ''CRT'' and TIPO_CARTAO is null and REDE_CARTAO is null and TIPO_RECEBIMENTO_CARTAO is null and BANDEIRA_CARTAO_NFE is null'+ #13 +
    '  or'+ #13 +
    '  FORMA_PAGAMENTO = ''CRT'' and TIPO_CARTAO is not null and REDE_CARTAO is not null and TIPO_RECEBIMENTO_CARTAO is not null and BANDEIRA_CARTAO_NFE is not null'+ #13 +
    ');'+ #13
end;

function AddColunaQTDE_SEG_TRAVAR_ALTIS_OCIOSO: string;
begin
  Result :=
    'alter table PARAMETROS'+ #13 +
    'add QTDE_SEG_TRAVAR_ALTIS_OCIOSO number(4) default 3600 not null;'+ #13
end;

function AddConstraintCK_PAR_QTDE_SEG_TR_ALTIS_OCI: string;
begin
  Result :=
    'alter table PARAMETROS'+ #13 +
    'add constraint CK_PAR_QTDE_SEG_TR_ALTIS_OCI'+ #13 +
    'check(QTDE_SEG_TRAVAR_ALTIS_OCIOSO > 0);'+ #13
end;

function AjustarProcADD_NO_VETOR_SEM_REPETIR: string;
begin
  Result :=
    'create or replace procedure ADD_NO_VETOR_SEM_REPETIR('+ #13 +
    '  ioVETOR in out TIPOS.ArrayOfNumeros,'+ #13 +
    '  iVALOR in number'+ #13 +
    ')'+ #13 +
    'is'+ #13 +
    '  vAchou boolean;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  vAchou := false;'+ #13 +
    #13 +
    '  if ioVetor.count > 0 then'+ #13 +
    '    for i in ioVETOR.first..ioVETOR.last loop'+ #13 +
    '      if iVALOR = ioVetor(i) then'+ #13 +
    '        vAchou := true;'+ #13 +
    '        exit;'+ #13 +
    '      end if;'+ #13 +
    '    end loop;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if not vAchou then'+ #13 +
    '    ioVETOR(ioVETOR.count + 1) := iVALOR;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end ADD_NO_VETOR_SEM_REPETIR;'+ #13
end;

function AjustarProcGERAR_NOTA_DEV_RETIRADA: string;
  function GetComando1: string;
  begin
    Result :=
      'create or replace procedure GERAR_NOTA_DEV_RETIRADA(iDEVOLUCAO_ID in number, iRETIRADA_ID in number)'+ #13 +
      'is'+ #13 +
      '  i                     number default 0;'+ #13 +
      '  vNotaFiscalId         positive;'+ #13 +
      '  vNotasReferenciasIds  TIPOS.ArrayOfNumeros;'+ #13 +
      '  vDadosNota            RecordsNotasFiscais.RecNotaFiscal;'+ #13 +
      '  vDadosItens           RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;'+ #13 +
      #13 +
      '  vValorMaisSignificativo number default 0;'+ #13 +
      '  vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;'+ #13 +
      '  vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;'+ #13 +
      #13 +
      '  cursor cItens is'+ #13 +
      '  select'+ #13 +
      '    NIT.NOTA_FISCAL_ID as NOTA_FISCAL_ORIGEM_ID,'+ #13 +
      '    ITE.PRODUTO_ID,'+ #13 +
      '    ITE.ITEM_ID,'+ #13 +
      '    DIR.QUANTIDADE_DEVOLVIDOS as QUANTIDADE,'+ #13 +
      '    NIT.NOME_PRODUTO,'+ #13 +
      #13 +
      '    round(NIT.VALOR_TOTAL / NIT.QUANTIDADE * DIR.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_TOTAL,'+ #13 +
      '    NIT.PRECO_UNITARIO,'+ #13 +
      '    round(NIT.VALOR_TOTAL_DESCONTO / NIT.QUANTIDADE * DIR.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_TOTAL_DESCONTO,'+ #13 +
      '    round(NIT.VALOR_TOTAL_OUTRAS_DESPESAS / NIT.QUANTIDADE * DIR.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_ICMS / NIT.QUANTIDADE * DIR.QUANTIDADE_DEVOLVIDOS, 2) as BASE_CALCULO_ICMS,'+ #13 +
      '    NIT.PERCENTUAL_ICMS,'+ #13 +
      '    round(NIT.VALOR_ICMS / NIT.QUANTIDADE * DIR.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_ICMS,'+ #13 +
      '    NIT.INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_ICMS_ST / NIT.QUANTIDADE * DIR.QUANTIDADE_DEVOLVIDOS, 2) as BASE_CALCULO_ICMS_ST,'+ #13 +
      '    NIT.PERCENTUAL_ICMS_ST,'+ #13 +
      '    round(NIT.VALOR_ICMS_ST / NIT.QUANTIDADE * DIR.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_ICMS_ST,'+ #13 +
      '    NIT.INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      #13 +
      '    NIT.PRECO_PAUTA,'+ #13 +
      '    NIT.IVA,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_COFINS / NIT.QUANTIDADE * DIR.QUANTIDADE_DEVOLVIDOS, 2) as BASE_CALCULO_COFINS,'+ #13 +
      '    NIT.PERCENTUAL_COFINS,'+ #13 +
      '    round(NIT.VALOR_COFINS / NIT.QUANTIDADE * DIR.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_COFINS,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_PIS / NIT.QUANTIDADE * DIR.QUANTIDADE_DEVOLVIDOS, 2) as BASE_CALCULO_PIS,'+ #13 +
      '    NIT.PERCENTUAL_PIS,'+ #13 +
      '    round(NIT.VALOR_PIS / NIT.QUANTIDADE * DIR.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_PIS,'+ #13 +
      #13 +
      '    round(NIT.VALOR_IPI / NIT.QUANTIDADE * DIR.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_IPI,'+ #13 +
      '    NIT.PERCENTUAL_IPI,'+ #13 +
      #13 +
      '    NIT.CST,'+ #13 +
      '    NIT.CODIGO_NCM,'+ #13 +
      '    NIT.UNIDADE,'+ #13 +
      '    NIT.CODIGO_BARRAS,'+ #13 +
      '    NIT.CEST,'+ #13 +
      '    NIT.CST_PIS,'+ #13 +
      '    NIT.CST_COFINS'+ #13 +
      '  from'+ #13 +
      '    ('+ #13 +
      '      /* Este select � feito desta forma por causa dos LOTES! */'+ #13 +
      '      select'+ #13 +
      '        DEVOLUCAO_ID,'+ #13 +
      '        RETIRADA_ID,'+ #13 +
      '        ITEM_ID,'+ #13 +
      '        sum(QUANTIDADE) as QUANTIDADE_DEVOLVIDOS'+ #13 +
      '      from'+ #13 +
      '        DEVOLUCOES_ITENS_RETIRADAS'+ #13 +
      '      where RETIRADA_ID = iRETIRADA_ID'+ #13 +
      '      group by'+ #13 +
      '        DEVOLUCAO_ID,'+ #13 +
      '        RETIRADA_ID,'+ #13 +
      '        ITEM_ID'+ #13 +
      '    ) DIR'+ #13 +
      #13 +
      '  inner join DEVOLUCOES_ITENS ITE'+ #13 +
      '  on DIR.DEVOLUCAO_ID = ITE.DEVOLUCAO_ID'+ #13 +
      '  and DIR.ITEM_ID = ITE.ITEM_ID'+ #13 +
      #13 +
      '  inner join DEVOLUCOES DEV'+ #13 +
      '  on ITE.DEVOLUCAO_ID = DEV.DEVOLUCAO_ID'+ #13 +
      #13 +
      '  inner join NOTAS_FISCAIS NOF'+ #13 +
      '  on DIR.RETIRADA_ID = NOF.RETIRADA_ID'+ #13 +
      '  and NOF.STATUS = ''E'''+ #13 +
      #13 +
      '  inner join NOTAS_FISCAIS_ITENS NIT'+ #13 +
      '  on NOF.NOTA_FISCAL_ID = NIT.NOTA_FISCAL_ID'+ #13 +
      '  and ITE.ITEM_ID = NIT.ITEM_ID'+ #13 +
      #13 +
      '  where ITE.DEVOLUCAO_ID = iDEVOLUCAO_ID;'+ #13 +
      #13 +
      'begin '+ #13 +
      #13 +
      ' begin'+ #13 +
      '    select'+ #13 +
      '      DEV.DEVOLUCAO_ID,'+ #13 +
      '      DEV.EMPRESA_ID,'+ #13 +
      '      nvl(DEV.CLIENTE_CREDITO_ID, ORC.CLIENTE_ID),'+ #13 +
      '      ORC.ORCAMENTO_ID,'+ #13 +
      '      0 as VALOR_DINHEIRO,'+ #13 +
      '      0 as VALOR_CARTAO_CREDITO,'+ #13 +
      '      0 as VALOR_CARTAO_DEBITO,'+ #13 +
      '      0 as VALOR_CREDITO,'+ #13 +
      '      0 as VALOR_COBRANCA,'+ #13 +
      '      0 as VALOR_CHEQUE,'+ #13 +
      '      0 as VALOR_FINANCEIRA,'+ #13 +
      '      0 as VALOR_FRETE,'+ #13 +
      '      0 as VALOR_SEGURO,'+ #13 +
      #13 +
      '      '''' as NOME_CONSUMIDOR_FINAL,'+ #13 +
      #13 +
      '      /* Emitente */      '+ #13 +
      '      EMP.RAZAO_SOCIAL,'+ #13 +
      '      EMP.NOME_FANTASIA,      '+ #13 +
      '      EMP.CNPJ,'+ #13 +
      '      EMP.INSCRICAO_ESTADUAL,'+ #13 +
      '      EMP.LOGRADOURO,'+ #13 +
      '      EMP.COMPLEMENTO,'+ #13 +
      '      BAE.NOME as NOME_BAIRRO_EMITENTE,'+ #13 +
      '      CIE.NOME as NOME_CIDADE_EMITENTE,'+ #13 +
      '      EMP.NUMERO,'+ #13 +
      '      ESE.ESTADO_ID,'+ #13 +
      '      EMP.CEP,'+ #13 +
      '      EMP.TELEFONE_PRINCIPAL,'+ #13 +
      '      CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      #13 +
      '      /* Destinatario */'+ #13 +
      '      CAD.NOME_FANTASIA,'+ #13 +
      '      CAD.RAZAO_SOCIAL,'+ #13 +
      '      CAD.TIPO_PESSOA,'+ #13 +
      '      CAD.CPF_CNPJ,'+ #13 +
      '      CAD.INSCRICAO_ESTADUAL,'+ #13 +
      '      CAD.LOGRADOURO,'+ #13 +
      '      CAD.COMPLEMENTO,'+ #13 +
      '      CAD.NUMERO,'+ #13 +
      '      CAD.CEP,      '+ #13 +
      '      BAI.NOME as NOME_BAIRRO,'+ #13 +
      '      CID.NOME as NOME_CIDADE,'+ #13 +
      '      CID.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      CID.ESTADO_ID as ESTADO_ID_DESTINATARIO,'+ #13 +
      '      CLI.TIPO_CLIENTE'+ #13 +
      '    into'+ #13 +
      '      vDadosNota.DEVOLUCAO_ID,'+ #13 +
      '      vDadosNota.EMPRESA_ID,'+ #13 +
      '      vDadosNota.CADASTRO_ID,'+ #13 +
      '      vDadosNota.ORCAMENTO_ID,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CREDITO,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '      vDadosNota.VALOR_FRETE,'+ #13 +
      '      vDadosNota.VALOR_SEGURO,'+ #13 +
      #13 +
      '      vDadosNota.NOME_CONSUMIDOR_FINAL,'+ #13 +
      '      /* Emitente */'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_FANTASIA_EMITENTE,      '+ #13 +
      '      vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '      vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '      vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '      vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '      vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '      vDadosNota.CEP_EMITENTE,'+ #13 +
      '      vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '      /* Destinatario */'+ #13 +
      '      vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '      vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '      vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '      vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '      vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '      vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      '      vDadosNota.TIPO_CLIENTE'+ #13 +
      '    from'+ #13 +
      '      DEVOLUCOES DEV'+ #13 +
      #13 +
      '    inner join ORCAMENTOS ORC'+ #13 +
      '    on DEV.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
      #13 +
      '    inner join CADASTROS CAD'+ #13 +
      '    on nvl(DEV.CLIENTE_CREDITO_ID, ORC.CLIENTE_ID) = CAD.CADASTRO_ID'+ #13 +
      #13 +
      '    inner join CLIENTES CLI'+ #13 +
      '    on CAD.CADASTRO_ID = CLI.CADASTRO_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAI'+ #13 +
      '    on CAD.BAIRRO_ID = BAI.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CID'+ #13 +
      '		on BAI.CIDADE_ID = CID.CIDADE_ID '+ #13 +
      #13 +
      '    inner join EMPRESAS EMP'+ #13 +
      '    on ORC.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAE'+ #13 +
      '    on EMP.BAIRRO_ID = BAE.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CIE'+ #13 +
      '    on BAE.CIDADE_ID = CIE.CIDADE_ID'+ #13 +
      #13 +
      '    inner join ESTADOS ESE'+ #13 +
      '    on CIE.ESTADO_ID = ESE.ESTADO_ID'+ #13 +
      #13 +
      '    cross join PARAMETROS PAR'+ #13 +
      #13 +
      '    where DEV.DEVOLUCAO_ID = iDEVOLUCAO_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Houve um problema ao buscar os dados da devolu��o para gera��o da NFe!'');'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  if vDadosNota.CADASTRO_ID = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then'+ #13 +
      '    ERRO(''N�o � poss�vel gerar uma nota de devolu��o de uma venda para consumidor final!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  vDadosNota.BASE_CALCULO_ICMS    := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS           := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_ICMS_ST := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS_ST        := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_PIS     := 0;'+ #13 +
      '  vDadosNota.VALOR_PIS            := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_COFINS  := 0;'+ #13 +
      '  vDadosNota.VALOR_COFINS         := 0;'+ #13 +
      '  vDadosNota.VALOR_IPI            := 0;'+ #13 +
      '  vDadosNota.VALOR_FRETE          := 0;'+ #13 +
      '  vDadosNota.VALOR_SEGURO         := 0;'+ #13 +
      '  vDadosNota.PESO_LIQUIDO         := 0;'+ #13 +
      '  vDadosNota.PESO_BRUTO           := 0;  '+ #13 +
      #13 +
      #13 +
      '  for vItens in cItens loop'+ #13 +
      '    if length(vItens.CODIGO_NCM) < 8 then'+ #13 +
      '      Erro(''O c�digo NCM do produto '' || vItens.PRODUTO_ID || '' - '' || vItens.NOME_PRODUTO || '' n�o est� incorreto, o NCM � composto de 8 dig�tos, verifique no cadastro de produtos!'');'+ #13 +
      '    end if;  '+ #13 +
      #13 +
      '    vDadosItens(i).PRODUTO_ID                  := vItens.PRODUTO_ID;'+ #13 +
      '    vDadosItens(i).ITEM_ID                     := vItens.ITEM_ID;'+ #13 +
      '    vDadosItens(i).NOME_PRODUTO                := vItens.NOME_PRODUTO;'+ #13 +
      '    vDadosItens(i).UNIDADE                     := vItens.UNIDADE;'+ #13 +
      '    vDadosItens(i).CST                         := vItens.CST;'+ #13 +
      '    vDadosItens(i).CODIGO_NCM                  := vItens.CODIGO_NCM;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL                 := vItens.VALOR_TOTAL;'+ #13 +
      '    vDadosItens(i).PRECO_UNITARIO              := vItens.PRECO_UNITARIO;'+ #13 +
      '    vDadosItens(i).QUANTIDADE                  := vItens.QUANTIDADE;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_DESCONTO        := vItens.VALOR_TOTAL_DESCONTO;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := vItens.VALOR_TOTAL_OUTRAS_DESPESAS;'+ #13 +
      '    vDadosItens(i).CODIGO_BARRAS               := vItens.CODIGO_BARRAS;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_ICMS           := vItens.BASE_CALCULO_ICMS;'+ #13 +
      '    vDadosItens(i).INDICE_REDUCAO_BASE_ICMS    := vItens.INDICE_REDUCAO_BASE_ICMS;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_ICMS             := vItens.PERCENTUAL_ICMS;'+ #13 +
      '    vDadosItens(i).VALOR_ICMS                  := vItens.VALOR_ICMS;'+ #13 +
      '    vDadosItens(i).IVA                         := vItens.IVA;'+ #13 +
      '    vDadosItens(i).PRECO_PAUTA                 := vItens.PRECO_PAUTA;'+ #13 +
      #13 +
      '    vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST := vItens.INDICE_REDUCAO_BASE_ICMS_ST;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_ICMS_ST        := vItens.BASE_CALCULO_ICMS_ST;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_ICMS_ST          := vItens.PERCENTUAL_ICMS_ST;'+ #13 +
      '    vDadosItens(i).VALOR_ICMS_ST               := vItens.VALOR_ICMS_ST;'+ #13 +
      #13 +
      '    vDadosItens(i).BASE_CALCULO_PIS            := vItens.BASE_CALCULO_PIS;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_PIS              := vItens.PERCENTUAL_PIS;'+ #13 +
      '    vDadosItens(i).VALOR_PIS                   := vItens.VALOR_PIS;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_COFINS         := vItens.BASE_CALCULO_COFINS;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_COFINS           := vItens.PERCENTUAL_COFINS;'+ #13 +
      '    vDadosItens(i).VALOR_COFINS                := vItens.VALOR_COFINS;'+ #13 +
      '    vDadosItens(i).VALOR_IPI                   := vItens.VALOR_IPI;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_IPI              := vItens.PERCENTUAL_IPI;'+ #13 +
      '    vDadosItens(i).CST_PIS                     := vItens.CST_PIS;'+ #13 +
      '    vDadosItens(i).CST_COFINS                  := vItens.CST_COFINS;'+ #13 +
      '    vDadosItens(i).CEST                        := vItens.CEST;'+ #13 +
      #13 +
      '    vDadosItens(i).CFOP_ID                     := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vItens.CST, ''DEV'', ''I'');'+ #13 +
      '		vDadosItens(i).NATUREZA_OPERACAO					 := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);'+ #13 +
      #13 +
      '    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS    := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;'+ #13 +
      '    vDadosNota.VALOR_ICMS           := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST        := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;'+ #13 +
      '    vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;'+ #13 +
      '    vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;'+ #13 +
      '    vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL :='+ #13 +
      '      vDadosNota.VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;'+ #13 +
      '    vDadosNota.VALOR_DESCONTO := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;'+ #13 +
      '    /* ------------------------------------------------------------------------------------------------------ */'+ #13 +
      #13 +
      '		if'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO >'+ #13 +
      '      vValorMaisSignificativo'+ #13 +
      '    then'+ #13 +
      '			vValorMaisSignificativo :='+ #13 +
      '        vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '        vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '        vDadosItens(i).VALOR_IPI -'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '			vCfopIdCapa 	  	      := vDadosItens(i).CFOP_ID;'+ #13 +
      '			vNaturezaOperacao       := vDadosItens(i).NATUREZA_OPERACAO;'+ #13 +
      '		end if;'+ #13 +
      #13 +
      '    ADD_NO_VETOR_SEM_REPETIR(vNotasReferenciasIds, vItens.NOTA_FISCAL_ORIGEM_ID);'+ #13 +
      #13 +
      '    i := i + 1;'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  if vDadosItens.count < 1 then'+ #13 +
      '    ERRO(''Os produtos da nota fiscal n�o foram encontrados!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '	if vCfopIdCapa is null then'+ #13 +
      '		vCfopIdCapa 			:= vDadosItens(0).CFOP_ID;'+ #13 +
      '		vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;'+ #13 +
      '	end if;'+ #13 +
      #13 +
      '	vDadosNota.CFOP_ID := vCfopIdCapa;'+ #13 +
      '  vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;'+ #13 +
      #13 +
      '  vDadosNota.INFORMACOES_COMPLEMENTARES := ''Devolu��o '' || iDEVOLUCAO_ID || '' referente a retirada '' || iRETIRADA_ID;'+ #13 +
      #13 +
      '  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;'+ #13 +
      #13 +
      '	insert into NOTAS_FISCAIS('+ #13 +
      '    NOTA_FISCAL_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    CFOP_ID,'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    ORCAMENTO_BASE_ID,'+ #13 +
      '    RETIRADA_ID,'+ #13 +
      '    DEVOLUCAO_ID,'+ #13 +
      '		MODELO_NOTA,'+ #13 +
      '		SERIE_NOTA,'+ #13 +
      '    NATUREZA_OPERACAO,'+ #13 +
      '		STATUS,'+ #13 +
      '    TIPO_MOVIMENTO,'+ #13 +
      '    RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    NOME_FANTASIA_EMITENTE,'+ #13 +
      '    REGIME_TRIBUTARIO,'+ #13 +
      '    CNPJ_EMITENTE,'+ #13 +
      '    INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    LOGRADOURO_EMITENTE,'+ #13 +
      '    COMPLEMENTO_EMITENTE,'+ #13 +
      '    NOME_BAIRRO_EMITENTE,'+ #13 +
      '    NOME_CIDADE_EMITENTE,'+ #13 +
      '    NUMERO_EMITENTE,'+ #13 +
      '    ESTADO_ID_EMITENTE,'+ #13 +
      '    CEP_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    TIPO_NOTA,'+ #13 +
      '    LOGRADOURO_DESTINATARIO,'+ #13 +
      '    COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    ESTADO_ID_DESTINATARIO,'+ #13 +
      '    CEP_DESTINATARIO,'+ #13 +
      '    NUMERO_DESTINATARIO,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    VALOR_TOTAL,'+ #13 +
      '    VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    VALOR_DESCONTO,'+ #13 +
      '    VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    BASE_CALCULO_ICMS,'+ #13 +
      '    VALOR_ICMS,'+ #13 +
      '    BASE_CALCULO_ICMS_ST,'+ #13 +
      '    VALOR_ICMS_ST,'+ #13 +
      '    BASE_CALCULO_PIS,'+ #13 +
      '    VALOR_PIS,'+ #13 +
      '    BASE_CALCULO_COFINS,'+ #13 +
      '    VALOR_COFINS,'+ #13 +
      '    VALOR_IPI,'+ #13 +
      '    VALOR_FRETE,'+ #13 +
      '    VALOR_SEGURO,'+ #13 +
      '    VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '    VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '    VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '    VALOR_RECEBIDO_CREDITO,'+ #13 +
      '    VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '    VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '    PESO_LIQUIDO,'+ #13 +
      '    PESO_BRUTO,'+ #13 +
      '		INFORMACOES_COMPLEMENTARES'+ #13 +
      '  )values('+ #13 +
      '		vNotaFiscalId,'+ #13 +
      '    vDadosNota.CADASTRO_ID,'+ #13 +
      '    vDadosNota.CFOP_ID,'+ #13 +
      '    vDadosNota.EMPRESA_ID,'+ #13 +
      '    vDadosNota.ORCAMENTO_ID,'+ #13 +
      '    iRETIRADA_ID,'+ #13 +
      '    iDEVOLUCAO_ID,'+ #13 +
      '		''55'','+ #13 +
      '		SESSAO.PARAMETROS_EMPRESA_LOGADA.SERIE_NFE,'+ #13 +
      '    vDadosNota.NATUREZA_OPERACAO,'+ #13 +
      '		''N'','+ #13 +
      '    ''DRE'','+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_EMITENTE,'+ #13 +
      '    SESSAO.PARAMETROS_EMPRESA_LOGADA.REGIME_TRIBUTARIO,'+ #13 +
      '    vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '    vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '    vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '    vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '    vDadosNota.CEP_EMITENTE,'+ #13 +
      '    vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    vDadosNota.NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    vDadosNota.TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    ''N'','+ #13 +
      '    vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '    vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      '    vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '    vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    vDadosNota.VALOR_TOTAL,'+ #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    vDadosNota.VALOR_DESCONTO,'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS,'+ #13 +
      '    vDadosNota.VALOR_ICMS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST,'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST,'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS,'+ #13 +
      '    vDadosNota.VALOR_PIS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_COFINS,'+ #13 +
      '    vDadosNota.VALOR_COFINS,'+ #13 +
      '    vDadosNota.VALOR_IPI,'+ #13 +
      '    vDadosNota.VALOR_FRETE,'+ #13 +
      '    vDadosNota.VALOR_SEGURO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CREDITO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '    vDadosNota.PESO_LIQUIDO,'+ #13 +
      '    vDadosNota.PESO_BRUTO,'+ #13 +
      '		vDadosNota.INFORMACOES_COMPLEMENTARES'+ #13 +
      '  );'+ #13 +
      #13 +
      '  /* inserindo a refer�ncia */'+ #13 +
      '  for i in vNotasReferenciasIds.first..vNotasReferenciasIds.last loop'+ #13 +
      '    insert into NOTAS_FISCAIS_REFERENCIAS('+ #13 +
      '      NOTA_FISCAL_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      TIPO_REFERENCIA,'+ #13 +
      '      NOTA_FISCAL_ID_REFERENCIADA'+ #13 +
      '    )values('+ #13 +
      '      vNotaFiscalId,'+ #13;
  end;

  function GetComando2: string;
  begin
    Result :=
      '      i,'+ #13 +
      '      ''A'','+ #13 +
      '      vNotasReferenciasIds(i)'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  for i in 0..vDadosItens.count - 1 loop'+ #13 +
      '    insert into NOTAS_FISCAIS_ITENS('+ #13 +
      '      NOTA_FISCAL_ID,'+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      NOME_PRODUTO,'+ #13 +
      '      UNIDADE,'+ #13 +
      '      CFOP_ID,'+ #13 +
      '      CST,'+ #13 +
      '      CODIGO_NCM,'+ #13 +
      '      VALOR_TOTAL,'+ #13 +
      '      PRECO_UNITARIO,'+ #13 +
      '      QUANTIDADE,'+ #13 +
      '      VALOR_TOTAL_DESCONTO,'+ #13 +
      '      VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      BASE_CALCULO_ICMS,'+ #13 +
      '      PERCENTUAL_ICMS,'+ #13 +
      '      VALOR_ICMS,'+ #13 +
      '      BASE_CALCULO_ICMS_ST,'+ #13 +
      '      VALOR_ICMS_ST,'+ #13 +
      '      CST_PIS,'+ #13 +
      '      BASE_CALCULO_PIS,'+ #13 +
      '      PERCENTUAL_PIS,'+ #13 +
      '      VALOR_PIS,'+ #13 +
      '      CST_COFINS,'+ #13 +
      '      BASE_CALCULO_COFINS,'+ #13 +
      '      PERCENTUAL_COFINS,'+ #13 +
      '      VALOR_COFINS,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      IVA,'+ #13 +
      '      PRECO_PAUTA,'+ #13 +
      '      CODIGO_BARRAS,'+ #13 +
      '      VALOR_IPI,'+ #13 +
      '      PERCENTUAL_IPI,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      PERCENTUAL_ICMS_ST,'+ #13 +
      '      CEST'+ #13 +
      '    )values('+ #13 +
      '      vNotaFiscalId,'+ #13 +
      '      vDadosItens(i).PRODUTO_ID,'+ #13 +
      '      vDadosItens(i).ITEM_ID,'+ #13 +
      '      vDadosItens(i).NOME_PRODUTO,'+ #13 +
      '      vDadosItens(i).UNIDADE,'+ #13 +
      '      vDadosItens(i).CFOP_ID,'+ #13 +
      '      vDadosItens(i).CST,'+ #13 +
      '      vDadosItens(i).CODIGO_NCM,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL,'+ #13 +
      '      vDadosItens(i).PRECO_UNITARIO,'+ #13 +
      '      vDadosItens(i).QUANTIDADE,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CST_PIS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS,'+ #13 +
      '      vDadosItens(i).VALOR_PIS,'+ #13 +
      '      vDadosItens(i).CST_COFINS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS,'+ #13 +
      '      vDadosItens(i).VALOR_COFINS,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      vDadosItens(i).IVA,'+ #13 +
      '      vDadosItens(i).PRECO_PAUTA,'+ #13 +
      '      vDadosItens(i).CODIGO_BARRAS,'+ #13 +
      '      vDadosItens(i).VALOR_IPI,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_IPI,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CEST'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      'end GERAR_NOTA_DEV_RETIRADA;'+ #13;
  end;

begin
  Result :=
    GetComando1 + GetComando2;
end;

function AjustarProcGERAR_NOTA_DEVOLUCAO_ACUMULADO: string;
  function GetComando1: string;
  begin
    Result :=
      'create or replace procedure GERAR_NOTA_DEVOLUCAO_ACUMULADO(iDEVOLUCAO_ID in number)'+ #13 +
      'is'+ #13 +
      '  i                 number default 0;'+ #13 +
      '  vNotaFiscalId     positive;'+ #13 +
      '  vDadosNota        RecordsNotasFiscais.RecNotaFiscal;'+ #13 +
      '  vDadosItens       RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;'+ #13 +
      '  vNotasReferenciasIds  TIPOS.ArrayOfNumeros;'+ #13 +
      #13 +
      '  vValorMaisSignificativo number default 0;'+ #13 +
      '  vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;'+ #13 +
      '  vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;'+ #13 +
      #13 +
      '  cursor cItens is'+ #13 +
      '  select'+ #13 +
      '    NIT.NOTA_FISCAL_ID as NOTA_FISCAL_ORIGEM_ID,'+ #13 +
      '    ITE.PRODUTO_ID,    '+ #13 +
      '    ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO as QUANTIDADE,'+ #13 +
      '    NIT.NOME_PRODUTO,'+ #13 +
      #13 +
      '    round(NIT.VALOR_TOTAL / NIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO), 2) as VALOR_TOTAL,'+ #13 +
      '    NIT.PRECO_UNITARIO,'+ #13 +
      '    round(NIT.VALOR_TOTAL_DESCONTO / NIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO), 2) as VALOR_TOTAL_DESCONTO,'+ #13 +
      '    round(NIT.VALOR_TOTAL_OUTRAS_DESPESAS / NIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO), 2) as VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_ICMS / NIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO), 2) as BASE_CALCULO_ICMS,'+ #13 +
      '    NIT.PERCENTUAL_ICMS,'+ #13 +
      '    round(NIT.VALOR_ICMS / NIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO), 2) as VALOR_ICMS,'+ #13 +
      '    NIT.INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_ICMS_ST / NIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO), 2) as BASE_CALCULO_ICMS_ST,'+ #13 +
      '    NIT.PERCENTUAL_ICMS_ST,'+ #13 +
      '    round(NIT.VALOR_ICMS_ST / NIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO), 2) as VALOR_ICMS_ST,'+ #13 +
      '    NIT.INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      #13 +
      '    NIT.PRECO_PAUTA,'+ #13 +
      '    NIT.IVA,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_COFINS / NIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO), 2) as BASE_CALCULO_COFINS,'+ #13 +
      '    NIT.PERCENTUAL_COFINS,'+ #13 +
      '    round(NIT.VALOR_COFINS / NIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO), 2) as VALOR_COFINS,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_PIS / NIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO), 2) as BASE_CALCULO_PIS,'+ #13 +
      '    NIT.PERCENTUAL_PIS,'+ #13 +
      '    round(NIT.VALOR_PIS / NIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO), 2) as VALOR_PIS,'+ #13 +
      #13 +
      '    round(NIT.VALOR_IPI / NIT.QUANTIDADE * (ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO), 2) as VALOR_IPI,'+ #13 +
      '    NIT.PERCENTUAL_IPI,'+ #13 +
      #13 +
      '    NIT.CST,'+ #13 +
      '    NIT.CODIGO_NCM,'+ #13 +
      '    NIT.UNIDADE,'+ #13 +
      '    NIT.CODIGO_BARRAS,'+ #13 +
      '    NIT.CEST,'+ #13 +
      '    NIT.CST_PIS,'+ #13 +
      '    NIT.CST_COFINS'+ #13 +
      '  from'+ #13 +
      '    DEVOLUCOES_ITENS ITE'+ #13 +
      #13 +
      '  inner join DEVOLUCOES DEV'+ #13 +
      '  on ITE.DEVOLUCAO_ID = DEV.DEVOLUCAO_ID'+ #13 +
      #13 +
      '  inner join ORCAMENTOS ORC'+ #13 +
      '  on DEV.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
      #13 +
      '  inner join NOTAS_FISCAIS NOF'+ #13 +
      '  on ORC.ACUMULADO_ID = NOF.ACUMULADO_ID'+ #13 +
      '  and NOF.STATUS = ''E'''+ #13 +
      #13 +
      '  inner join NOTAS_FISCAIS_ITENS NIT'+ #13 +
      '  on NOF.NOTA_FISCAL_ID = NIT.NOTA_FISCAL_ID'+ #13 +
      '  and ITE.PRODUTO_ID = NIT.PRODUTO_ID'+ #13 +
      #13 +
      '  where ITE.DEVOLUCAO_ID = iDEVOLUCAO_ID;'+ #13 +
      #13 +
      'begin '+ #13 +
      #13 +
      ' begin'+ #13 +
      '    select'+ #13 +
      '      DEV.DEVOLUCAO_ID,'+ #13 +
      '      DEV.EMPRESA_ID,'+ #13 +
      '      ORC.CLIENTE_ID,'+ #13 +
      '      ORC.ACUMULADO_ID,'+ #13 +
      '      ORC.ORCAMENTO_ID,'+ #13 +
      #13 +
      '      /* Emitente */      '+ #13 +
      '      EMP.RAZAO_SOCIAL,'+ #13 +
      '      EMP.NOME_FANTASIA,      '+ #13 +
      '      EMP.CNPJ,'+ #13 +
      '      EMP.INSCRICAO_ESTADUAL,'+ #13 +
      '      EMP.LOGRADOURO,'+ #13 +
      '      EMP.COMPLEMENTO,'+ #13 +
      '      BAE.NOME as NOME_BAIRRO_EMITENTE,'+ #13 +
      '      CIE.NOME as NOME_CIDADE_EMITENTE,'+ #13 +
      '      EMP.NUMERO,'+ #13 +
      '      ESE.ESTADO_ID,'+ #13 +
      '      EMP.CEP,'+ #13 +
      '      EMP.TELEFONE_PRINCIPAL,'+ #13 +
      '      CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      #13 +
      '      /* Destinatario */'+ #13 +
      '      CAD.NOME_FANTASIA,'+ #13 +
      '      CAD.RAZAO_SOCIAL,'+ #13 +
      '      CAD.TIPO_PESSOA,'+ #13 +
      '      CAD.CPF_CNPJ,'+ #13 +
      '      CAD.INSCRICAO_ESTADUAL,'+ #13 +
      '      CAD.LOGRADOURO,'+ #13 +
      '      CAD.COMPLEMENTO,'+ #13 +
      '      CAD.NUMERO,'+ #13 +
      '      CAD.CEP,      '+ #13 +
      '      BAI.NOME as NOME_BAIRRO,'+ #13 +
      '      CID.NOME as NOME_CIDADE,'+ #13 +
      '      CID.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      CID.ESTADO_ID as ESTADO_ID_DESTINATARIO,'+ #13 +
      '      CLI.TIPO_CLIENTE'+ #13 +
      '    into'+ #13 +
      '      vDadosNota.DEVOLUCAO_ID,'+ #13 +
      '      vDadosNota.EMPRESA_ID,'+ #13 +
      '      vDadosNota.CADASTRO_ID,'+ #13 +
      '      vDadosNota.ACUMULADO_ID,'+ #13 +
      '      vDadosNota.ORCAMENTO_ID,'+ #13 +
      #13 +
      '      /* Emitente */'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_FANTASIA_EMITENTE,      '+ #13 +
      '      vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '      vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '      vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '      vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '      vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '      vDadosNota.CEP_EMITENTE,'+ #13 +
      '      vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '      /* Destinatario */'+ #13 +
      '      vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '      vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '      vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '      vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '      vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '      vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      '      vDadosNota.TIPO_CLIENTE'+ #13 +
      '    from'+ #13 +
      '      DEVOLUCOES DEV'+ #13 +
      #13 +
      '    inner join ORCAMENTOS ORC'+ #13 +
      '    on DEV.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
      #13 +
      '    inner join CADASTROS CAD'+ #13 +
      '    on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
      #13 +
      '    inner join CLIENTES CLI'+ #13 +
      '    on ORC.CLIENTE_ID = CLI.CADASTRO_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAI'+ #13 +
      '    on CAD.BAIRRO_ID = BAI.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CID'+ #13 +
      '		on BAI.CIDADE_ID = CID.CIDADE_ID '+ #13 +
      #13 +
      '    inner join EMPRESAS EMP'+ #13 +
      '    on ORC.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAE'+ #13 +
      '    on EMP.BAIRRO_ID = BAE.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CIE'+ #13 +
      '    on BAE.CIDADE_ID = CIE.CIDADE_ID'+ #13 +
      #13 +
      '    inner join ESTADOS ESE'+ #13 +
      '    on CIE.ESTADO_ID = ESE.ESTADO_ID'+ #13 +
      #13 +
      '    cross join PARAMETROS PAR'+ #13 +
      #13 +
      '    where DEV.DEVOLUCAO_ID = iDEVOLUCAO_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Houve um erro ao buscar os dados da devolu��o para gera��o da NFe!'');'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  if vDadosNota.CADASTRO_ID = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then'+ #13 +
      '    ERRO(''N�o � poss�vel gerar uma nota de devolu��o de uma venda para consumidor final!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  vDadosNota.BASE_CALCULO_ICMS    := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS           := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_ICMS_ST := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS_ST        := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_PIS     := 0;'+ #13 +
      '  vDadosNota.VALOR_PIS            := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_COFINS  := 0;'+ #13 +
      '  vDadosNota.VALOR_COFINS         := 0;'+ #13 +
      '  vDadosNota.VALOR_IPI            := 0;'+ #13 +
      '  vDadosNota.VALOR_FRETE          := 0;'+ #13 +
      '  vDadosNota.VALOR_SEGURO         := 0;'+ #13 +
      '  vDadosNota.PESO_LIQUIDO         := 0;'+ #13 +
      '  vDadosNota.PESO_BRUTO           := 0;  '+ #13 +
      #13 +
      #13 +
      '  for vItens in cItens loop'+ #13 +
      '    if length(vItens.CODIGO_NCM) < 8 then'+ #13 +
      '      Erro(''O c�digo NCM do produto '' || vItens.PRODUTO_ID || '' - '' || vItens.NOME_PRODUTO || '' n�o est� incorreto, o NCM � composto de 8 dig�tos, verifique no cadastro de produtos!'');'+ #13 +
      '    end if;  '+ #13 +
      #13 +
      '    vDadosItens(i).PRODUTO_ID                  := vItens.PRODUTO_ID;'+ #13 +
      '    vDadosItens(i).ITEM_ID                     := i + 1;'+ #13 +
      '    vDadosItens(i).NOME_PRODUTO                := vItens.NOME_PRODUTO;'+ #13 +
      '    vDadosItens(i).UNIDADE                     := vItens.UNIDADE;'+ #13 +
      '    vDadosItens(i).CST                         := vItens.CST;'+ #13 +
      '    vDadosItens(i).CODIGO_NCM                  := vItens.CODIGO_NCM;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL                 := vItens.VALOR_TOTAL;'+ #13 +
      '    vDadosItens(i).PRECO_UNITARIO              := vItens.PRECO_UNITARIO;'+ #13 +
      '    vDadosItens(i).QUANTIDADE                  := vItens.QUANTIDADE;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_DESCONTO        := vItens.VALOR_TOTAL_DESCONTO;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := vItens.VALOR_TOTAL_OUTRAS_DESPESAS;'+ #13 +
      '    vDadosItens(i).CODIGO_BARRAS               := vItens.CODIGO_BARRAS;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_ICMS           := vItens.BASE_CALCULO_ICMS;'+ #13 +
      '    vDadosItens(i).INDICE_REDUCAO_BASE_ICMS    := vItens.INDICE_REDUCAO_BASE_ICMS;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_ICMS             := vItens.PERCENTUAL_ICMS;'+ #13 +
      '    vDadosItens(i).VALOR_ICMS                  := vItens.VALOR_ICMS;'+ #13 +
      '    vDadosItens(i).IVA                         := vItens.IVA;'+ #13 +
      '    vDadosItens(i).PRECO_PAUTA                 := vItens.PRECO_PAUTA;'+ #13 +
      '    vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST := vItens.INDICE_REDUCAO_BASE_ICMS_ST;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_ICMS_ST        := vItens.BASE_CALCULO_ICMS_ST;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_ICMS_ST          := vItens.PERCENTUAL_ICMS_ST;'+ #13 +
      '    vDadosItens(i).VALOR_ICMS_ST               := vItens.VALOR_ICMS_ST;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_PIS            := vItens.BASE_CALCULO_PIS;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_PIS              := vItens.PERCENTUAL_PIS;'+ #13 +
      '    vDadosItens(i).VALOR_PIS                   := vItens.VALOR_PIS;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_COFINS         := vItens.BASE_CALCULO_COFINS;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_COFINS           := vItens.PERCENTUAL_COFINS;'+ #13 +
      '    vDadosItens(i).VALOR_COFINS                := vItens.VALOR_COFINS;'+ #13 +
      '    vDadosItens(i).VALOR_IPI                   := vItens.VALOR_IPI;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_IPI              := vItens.PERCENTUAL_IPI;'+ #13 +
      '    vDadosItens(i).CST_PIS                     := vItens.CST_PIS;'+ #13 +
      '    vDadosItens(i).CST_COFINS                  := vItens.CST_COFINS;'+ #13 +
      '    vDadosItens(i).CEST                        := vItens.CEST;'+ #13 +
      #13 +
      '    vDadosItens(i).CFOP_ID                     := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vItens.CST, ''DEV'', ''I'');'+ #13 +
      '		vDadosItens(i).NATUREZA_OPERACAO					 := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);'+ #13 +
      #13 +
      '    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS    := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;'+ #13 +
      '    vDadosNota.VALOR_ICMS           := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST        := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;'+ #13 +
      '    vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;'+ #13 +
      '    vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;'+ #13 +
      '    vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL :='+ #13 +
      '      vDadosNota.VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;'+ #13 +
      '    vDadosNota.VALOR_DESCONTO := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;'+ #13 +
      '    /* ------------------------------------------------------------------------------------------------------ */'+ #13 +
      #13 +
      '		if'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO >'+ #13 +
      '      vValorMaisSignificativo'+ #13 +
      '    then'+ #13 +
      '			vValorMaisSignificativo :='+ #13 +
      '        vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '        vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '        vDadosItens(i).VALOR_IPI -'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '			vCfopIdCapa 	  	      := vDadosItens(i).CFOP_ID;'+ #13 +
      '			vNaturezaOperacao       := vDadosItens(i).NATUREZA_OPERACAO;'+ #13 +
      '		end if;'+ #13 +
      #13 +
      '    ADD_NO_VETOR_SEM_REPETIR(vNotasReferenciasIds, vItens.NOTA_FISCAL_ORIGEM_ID);'+ #13 +
      #13 +
      '    i := i + 1;'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  if vDadosItens.count < 1 then'+ #13 +
      '    ERRO(''Os produtos da nota fiscal n�o foram encontrados!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '	if vCfopIdCapa is null then'+ #13 +
      '		vCfopIdCapa 			:= vDadosItens(0).CFOP_ID;'+ #13 +
      '		vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;'+ #13 +
      '	end if;'+ #13 +
      #13 +
      '	vDadosNota.CFOP_ID := vCfopIdCapa;'+ #13 +
      '  vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;'+ #13 +
      #13 +
      '  vDadosNota.INFORMACOES_COMPLEMENTARES := ''Devolu��o '' || iDEVOLUCAO_ID || '' referente ao acumulado '' || vDadosNota.ACUMULADO_ID;'+ #13 +
      #13 +
      '  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;'+ #13 +
      #13 +
      '	insert into NOTAS_FISCAIS('+ #13 +
      '    NOTA_FISCAL_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    CFOP_ID,'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    ACUMULADO_ID,'+ #13 +
      '    DEVOLUCAO_ID,'+ #13 +
      '    ORCAMENTO_BASE_ID,'+ #13 +
      '		MODELO_NOTA,'+ #13 +
      '		SERIE_NOTA,'+ #13 +
      '    NATUREZA_OPERACAO,'+ #13 +
      '		STATUS,'+ #13 +
      '    TIPO_MOVIMENTO,'+ #13 +
      '    RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    NOME_FANTASIA_EMITENTE,'+ #13 +
      '    REGIME_TRIBUTARIO,'+ #13 +
      '    CNPJ_EMITENTE,'+ #13 +
      '    INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    LOGRADOURO_EMITENTE,'+ #13 +
      '    COMPLEMENTO_EMITENTE,'+ #13 +
      '    NOME_BAIRRO_EMITENTE,'+ #13 +
      '    NOME_CIDADE_EMITENTE,'+ #13 +
      '    NUMERO_EMITENTE,'+ #13 +
      '    ESTADO_ID_EMITENTE,'+ #13 +
      '    CEP_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    TIPO_NOTA,'+ #13 +
      '    LOGRADOURO_DESTINATARIO,'+ #13 +
      '    COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    ESTADO_ID_DESTINATARIO,'+ #13 +
      '    CEP_DESTINATARIO,'+ #13 +
      '    NUMERO_DESTINATARIO,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    VALOR_TOTAL,'+ #13 +
      '    VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    VALOR_DESCONTO,'+ #13 +
      '    VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    BASE_CALCULO_ICMS,'+ #13 +
      '    VALOR_ICMS,'+ #13 +
      '    BASE_CALCULO_ICMS_ST,'+ #13 +
      '    VALOR_ICMS_ST,'+ #13 +
      '    BASE_CALCULO_PIS,'+ #13 +
      '    VALOR_PIS,'+ #13 +
      '    BASE_CALCULO_COFINS,'+ #13 +
      '    VALOR_COFINS,'+ #13 +
      '    VALOR_IPI,'+ #13 +
      '    VALOR_FRETE,'+ #13 +
      '    VALOR_SEGURO,    '+ #13 +
      '    PESO_LIQUIDO,'+ #13 +
      '    PESO_BRUTO,'+ #13 +
      '		INFORMACOES_COMPLEMENTARES'+ #13 +
      '  )values('+ #13 +
      '		vNotaFiscalId,'+ #13 +
      '    vDadosNota.CADASTRO_ID,'+ #13 +
      '    vDadosNota.CFOP_ID,'+ #13 +
      '    vDadosNota.EMPRESA_ID,'+ #13 +
      '    vDadosNota.ACUMULADO_ID,    '+ #13 +
      '    iDEVOLUCAO_ID,'+ #13 +
      '    vDadosNota.ORCAMENTO_ID,'+ #13 +
      '		''55'','+ #13 +
      '		SESSAO.PARAMETROS_EMPRESA_LOGADA.SERIE_NFE,'+ #13 +
      '    vDadosNota.NATUREZA_OPERACAO,'+ #13 +
      '		''N'','+ #13 +
      '    ''DAC'','+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_EMITENTE,'+ #13 +
      '    SESSAO.PARAMETROS_EMPRESA_LOGADA.REGIME_TRIBUTARIO,'+ #13 +
      '    vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '    vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '    vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '    vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '    vDadosNota.CEP_EMITENTE,'+ #13 +
      '    vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    vDadosNota.NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    vDadosNota.TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    ''N'','+ #13 +
      '    vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '    vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      '    vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '    vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    vDadosNota.VALOR_TOTAL,'+ #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    vDadosNota.VALOR_DESCONTO,'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS,'+ #13 +
      '    vDadosNota.VALOR_ICMS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST,'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST,'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS,'+ #13 +
      '    vDadosNota.VALOR_PIS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_COFINS,'+ #13 +
      '    vDadosNota.VALOR_COFINS,'+ #13 +
      '    vDadosNota.VALOR_IPI,'+ #13 +
      '    vDadosNota.VALOR_FRETE,'+ #13 +
      '    vDadosNota.VALOR_SEGURO,    '+ #13 +
      '    vDadosNota.PESO_LIQUIDO,'+ #13 +
      '    vDadosNota.PESO_BRUTO,'+ #13 +
      '		vDadosNota.INFORMACOES_COMPLEMENTARES'+ #13 +
      '  );'+ #13 +
      #13 +
      '  /* inserindo a refer�ncia */'+ #13 +
      '  for i in vNotasReferenciasIds.first..vNotasReferenciasIds.last loop'+ #13 +
      '    insert into NOTAS_FISCAIS_REFERENCIAS('+ #13 +
      '      NOTA_FISCAL_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      TIPO_REFERENCIA,'+ #13 +
      '      NOTA_FISCAL_ID_REFERENCIADA'+ #13 +
      '    )values('+ #13 +
      '      vNotaFiscalId,'+ #13 +
      '      i,'+ #13 +
      '      ''A'','+ #13 +
      '      vNotasReferenciasIds(i)'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  for i in 0..vDadosItens.count - 1 loop'+ #13 +
      '    insert into NOTAS_FISCAIS_ITENS('+ #13 +
      '      NOTA_FISCAL_ID,'+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      NOME_PRODUTO,'+ #13 +
      '      UNIDADE,'+ #13 +
      '      CFOP_ID,'+ #13 +
      '      CST,'+ #13 +
      '      CODIGO_NCM,'+ #13 +
      '      VALOR_TOTAL,'+ #13 +
      '      PRECO_UNITARIO,'+ #13 +
      '      QUANTIDADE,'+ #13 +
      '      VALOR_TOTAL_DESCONTO,'+ #13 +
      '      VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      BASE_CALCULO_ICMS,'+ #13 +
      '      PERCENTUAL_ICMS,'+ #13 +
      '      VALOR_ICMS,'+ #13 +
      '      BASE_CALCULO_ICMS_ST,'+ #13 +
      '      VALOR_ICMS_ST,'+ #13 +
      '      CST_PIS,'+ #13 +
      '      BASE_CALCULO_PIS,'+ #13 +
      '      PERCENTUAL_PIS,'+ #13 +
      '      VALOR_PIS,'+ #13 +
      '      CST_COFINS,'+ #13 +
      '      BASE_CALCULO_COFINS,'+ #13 +
      '      PERCENTUAL_COFINS,'+ #13 +
      '      VALOR_COFINS,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      IVA,'+ #13 +
      '      PRECO_PAUTA,'+ #13 +
      '      CODIGO_BARRAS,'+ #13 +
      '      VALOR_IPI,'+ #13 +
      '      PERCENTUAL_IPI,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      PERCENTUAL_ICMS_ST,'+ #13 +
      '      CEST'+ #13 +
      '    )values('+ #13 +
      '      vNotaFiscalId,'+ #13 +
      '      vDadosItens(i).PRODUTO_ID,'+ #13 +
      '      vDadosItens(i).ITEM_ID,'+ #13 +
      '      vDadosItens(i).NOME_PRODUTO,'+ #13 +
      '      vDadosItens(i).UNIDADE,'+ #13 +
      '      vDadosItens(i).CFOP_ID,'+ #13 +
      '      vDadosItens(i).CST,'+ #13;
  end;

  function GetComando2: string;
  begin
    Result :=
      '      vDadosItens(i).CODIGO_NCM,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL,'+ #13 +
      '      vDadosItens(i).PRECO_UNITARIO,'+ #13 +
      '      vDadosItens(i).QUANTIDADE,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CST_PIS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS,'+ #13 +
      '      vDadosItens(i).VALOR_PIS,'+ #13 +
      '      vDadosItens(i).CST_COFINS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS,'+ #13 +
      '      vDadosItens(i).VALOR_COFINS,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      vDadosItens(i).IVA,'+ #13 +
      '      vDadosItens(i).PRECO_PAUTA,'+ #13 +
      '      vDadosItens(i).CODIGO_BARRAS,'+ #13 +
      '      vDadosItens(i).VALOR_IPI,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_IPI,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CEST'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      'end GERAR_NOTA_DEVOLUCAO_ACUMULADO;'+ #13;
  end;

begin
  Result :=
    GetComando1 + GetComando2;
end;

function AjustarProcGERAR_NOTA_DEVOLUCAO_ENTREGA: string;
  function GetComando1: string;
  begin
    Result :=
      'create or replace procedure GERAR_NOTA_DEVOLUCAO_ENTREGA(iDEVOLUCAO_ID in number, iENTREGA_ID in number)'+ #13 +
      'is'+ #13 +
      '  i                 number default 0;'+ #13 +
      '  vNotaFiscalId     positive;'+ #13 +
      '  vDadosNota        RecordsNotasFiscais.RecNotaFiscal;'+ #13 +
      '  vDadosItens       RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;'+ #13 +
      #13 +
      '  vValorMaisSignificativo number default 0;'+ #13 +
      '  vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;'+ #13 +
      '  vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;'+ #13 +
      '  vNotasReferenciasIds    TIPOS.ArrayOfNumeros;'+ #13 +
      #13 +
      '  cursor cItens is'+ #13 +
      '  select'+ #13 +
      '    NIT.NOTA_FISCAL_ID as NOTA_FISCAL_ORIGEM_ID,'+ #13 +
      '    ITE.PRODUTO_ID,'+ #13 +
      '    ITE.ITEM_ID,'+ #13 +
      '    DIE.QUANTIDADE_DEVOLVIDOS as QUANTIDADE,'+ #13 +
      '    NIT.NOME_PRODUTO,'+ #13 +
      #13 +
      '    round(NIT.VALOR_TOTAL / NIT.QUANTIDADE * DIE.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_TOTAL,'+ #13 +
      '    NIT.PRECO_UNITARIO,'+ #13 +
      '    round(NIT.VALOR_TOTAL_DESCONTO / NIT.QUANTIDADE * DIE.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_TOTAL_DESCONTO,'+ #13 +
      '    round(NIT.VALOR_TOTAL_OUTRAS_DESPESAS / NIT.QUANTIDADE * DIE.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_ICMS / NIT.QUANTIDADE * DIE.QUANTIDADE_DEVOLVIDOS, 2) as BASE_CALCULO_ICMS,'+ #13 +
      '    NIT.PERCENTUAL_ICMS,'+ #13 +
      '    round(NIT.VALOR_ICMS / NIT.QUANTIDADE * DIE.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_ICMS,'+ #13 +
      '    NIT.INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_ICMS_ST / NIT.QUANTIDADE * DIE.QUANTIDADE_DEVOLVIDOS, 2) as BASE_CALCULO_ICMS_ST,'+ #13 +
      '    NIT.PERCENTUAL_ICMS_ST,'+ #13 +
      '    round(NIT.VALOR_ICMS_ST / NIT.QUANTIDADE * DIE.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_ICMS_ST,'+ #13 +
      '    NIT.INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      #13 +
      '    NIT.PRECO_PAUTA,'+ #13 +
      '    NIT.IVA,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_COFINS / NIT.QUANTIDADE * DIE.QUANTIDADE_DEVOLVIDOS, 2) as BASE_CALCULO_COFINS,'+ #13 +
      '    NIT.PERCENTUAL_COFINS,'+ #13 +
      '    round(NIT.VALOR_COFINS / NIT.QUANTIDADE * DIE.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_COFINS,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_PIS / NIT.QUANTIDADE * DIE.QUANTIDADE_DEVOLVIDOS, 2) as BASE_CALCULO_PIS,'+ #13 +
      '    NIT.PERCENTUAL_PIS,'+ #13 +
      '    round(NIT.VALOR_PIS / NIT.QUANTIDADE * DIE.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_PIS,'+ #13 +
      #13 +
      '    round(NIT.VALOR_IPI / NIT.QUANTIDADE * DIE.QUANTIDADE_DEVOLVIDOS, 2) as VALOR_IPI,'+ #13 +
      '    NIT.PERCENTUAL_IPI,'+ #13 +
      #13 +
      '    NIT.CST,'+ #13 +
      '    NIT.CODIGO_NCM,'+ #13 +
      '    NIT.UNIDADE,'+ #13 +
      '    NIT.CODIGO_BARRAS,'+ #13 +
      '    NIT.CEST,'+ #13 +
      '    NIT.CST_PIS,'+ #13 +
      '    NIT.CST_COFINS'+ #13 +
      '  from'+ #13 +
      '    ('+ #13 +
      '      /* Este select � feito desta forma por causa dos LOTES! */'+ #13 +
      '      select'+ #13 +
      '        DEVOLUCAO_ID,'+ #13 +
      '        ENTREGA_ID,'+ #13 +
      '        ITEM_ID,'+ #13 +
      '        sum(QUANTIDADE) as QUANTIDADE_DEVOLVIDOS'+ #13 +
      '      from'+ #13 +
      '        DEVOLUCOES_ITENS_ENTREGAS'+ #13 +
      '      where ENTREGA_ID = iENTREGA_ID'+ #13 +
      '      group by'+ #13 +
      '        DEVOLUCAO_ID,'+ #13 +
      '        ENTREGA_ID,'+ #13 +
      '        ITEM_ID'+ #13 +
      '    ) DIE'+ #13 +
      #13 +
      '  inner join DEVOLUCOES_ITENS ITE'+ #13 +
      '  on DIE.DEVOLUCAO_ID = ITE.DEVOLUCAO_ID'+ #13 +
      '  and DIE.ITEM_ID = ITE.ITEM_ID'+ #13 +
      #13 +
      '  inner join DEVOLUCOES DEV'+ #13 +
      '  on ITE.DEVOLUCAO_ID = DEV.DEVOLUCAO_ID'+ #13 +
      #13 +
      '  inner join NOTAS_FISCAIS NOF'+ #13 +
      '  on DIE.ENTREGA_ID = NOF.ENTREGA_ID'+ #13 +
      #13 +
      '  inner join NOTAS_FISCAIS_ITENS NIT'+ #13 +
      '  on NOF.NOTA_FISCAL_ID = NIT.NOTA_FISCAL_ID'+ #13 +
      '  and ITE.ITEM_ID = NIT.ITEM_ID'+ #13 +
      #13 +
      '  where ITE.DEVOLUCAO_ID = iDEVOLUCAO_ID;'+ #13 +
      #13 +
      'begin '+ #13 +
      #13 +
      ' begin'+ #13 +
      '    select'+ #13 +
      '      DEV.DEVOLUCAO_ID,'+ #13 +
      '      DEV.EMPRESA_ID,'+ #13 +
      '      ORC.CLIENTE_ID,'+ #13 +
      '      ORC.ORCAMENTO_ID,'+ #13 +
      '      0 as VALOR_DINHEIRO,'+ #13 +
      '      0 as VALOR_CARTAO_CREDITO,'+ #13 +
      '      0 as VALOR_CARTAO_DEBITO,'+ #13 +
      '      0 as VALOR_CREDITO,'+ #13 +
      '      0 as VALOR_COBRANCA,'+ #13 +
      '      0 as VALOR_CHEQUE,'+ #13 +
      '      0 as VALOR_FINANCEIRA,'+ #13 +
      '      0 as VALOR_FRETE,'+ #13 +
      '      0 as VALOR_SEGURO,'+ #13 +
      #13 +
      '      '''' as NOME_CONSUMIDOR_FINAL,'+ #13 +
      #13 +
      '      /* Emitente */      '+ #13 +
      '      EMP.RAZAO_SOCIAL,'+ #13 +
      '      EMP.NOME_FANTASIA,      '+ #13 +
      '      EMP.CNPJ,'+ #13 +
      '      EMP.INSCRICAO_ESTADUAL,'+ #13 +
      '      EMP.LOGRADOURO,'+ #13 +
      '      EMP.COMPLEMENTO,'+ #13 +
      '      BAE.NOME as NOME_BAIRRO_EMITENTE,'+ #13 +
      '      CIE.NOME as NOME_CIDADE_EMITENTE,'+ #13 +
      '      EMP.NUMERO,'+ #13 +
      '      ESE.ESTADO_ID,'+ #13 +
      '      EMP.CEP,'+ #13 +
      '      EMP.TELEFONE_PRINCIPAL,'+ #13 +
      '      CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      #13 +
      '      /* Destinatario */'+ #13 +
      '      CAD.NOME_FANTASIA,'+ #13 +
      '      CAD.RAZAO_SOCIAL,'+ #13 +
      '      CAD.TIPO_PESSOA,'+ #13 +
      '      CAD.CPF_CNPJ,'+ #13 +
      '      CAD.INSCRICAO_ESTADUAL,'+ #13 +
      '      CAD.LOGRADOURO,'+ #13 +
      '      CAD.COMPLEMENTO,'+ #13 +
      '      CAD.NUMERO,'+ #13 +
      '      CAD.CEP,      '+ #13 +
      '      BAI.NOME as NOME_BAIRRO,'+ #13 +
      '      CID.NOME as NOME_CIDADE,'+ #13 +
      '      CID.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      CID.ESTADO_ID as ESTADO_ID_DESTINATARIO,'+ #13 +
      '      CLI.TIPO_CLIENTE'+ #13 +
      '    into'+ #13 +
      '      vDadosNota.DEVOLUCAO_ID,'+ #13 +
      '      vDadosNota.EMPRESA_ID,'+ #13 +
      '      vDadosNota.CADASTRO_ID,'+ #13 +
      '      vDadosNota.ORCAMENTO_ID,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CREDITO,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '      vDadosNota.VALOR_FRETE,'+ #13 +
      '      vDadosNota.VALOR_SEGURO,'+ #13 +
      #13 +
      '      vDadosNota.NOME_CONSUMIDOR_FINAL,'+ #13 +
      '      /* Emitente */'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_FANTASIA_EMITENTE,      '+ #13 +
      '      vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '      vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '      vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '      vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '      vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '      vDadosNota.CEP_EMITENTE,'+ #13 +
      '      vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '      /* Destinatario */'+ #13 +
      '      vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '      vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '      vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '      vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '      vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '      vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      '      vDadosNota.TIPO_CLIENTE'+ #13 +
      '    from'+ #13 +
      '      DEVOLUCOES DEV'+ #13 +
      #13 +
      '    inner join ORCAMENTOS ORC'+ #13 +
      '    on DEV.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
      #13 +
      '    inner join CADASTROS CAD'+ #13 +
      '    on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
      #13 +
      '    inner join CLIENTES CLI'+ #13 +
      '    on ORC.CLIENTE_ID = CLI.CADASTRO_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAI'+ #13 +
      '    on CAD.BAIRRO_ID = BAI.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CID'+ #13 +
      '		on BAI.CIDADE_ID = CID.CIDADE_ID '+ #13 +
      #13 +
      '    inner join EMPRESAS EMP'+ #13 +
      '    on ORC.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAE'+ #13 +
      '    on EMP.BAIRRO_ID = BAE.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CIE'+ #13 +
      '    on BAE.CIDADE_ID = CIE.CIDADE_ID'+ #13 +
      #13 +
      '    inner join ESTADOS ESE'+ #13 +
      '    on CIE.ESTADO_ID = ESE.ESTADO_ID'+ #13 +
      #13 +
      '    cross join PARAMETROS PAR'+ #13 +
      #13 +
      '    where DEV.DEVOLUCAO_ID = iDEVOLUCAO_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Houve um erro ao buscar os dados da devolu��o para gera��o da NFe!'');'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  if vDadosNota.CADASTRO_ID = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then'+ #13 +
      '    ERRO(''N�o � poss�vel gerar uma nota de devolu��o de uma venda para consumidor final!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  vDadosNota.BASE_CALCULO_ICMS    := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS           := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_ICMS_ST := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS_ST        := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_PIS     := 0;'+ #13 +
      '  vDadosNota.VALOR_PIS            := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_COFINS  := 0;'+ #13 +
      '  vDadosNota.VALOR_COFINS         := 0;'+ #13 +
      '  vDadosNota.VALOR_IPI            := 0;'+ #13 +
      '  vDadosNota.VALOR_FRETE          := 0;'+ #13 +
      '  vDadosNota.VALOR_SEGURO         := 0;'+ #13 +
      '  vDadosNota.PESO_LIQUIDO         := 0;'+ #13 +
      '  vDadosNota.PESO_BRUTO           := 0;  '+ #13 +
      #13 +
      #13 +
      '  for vItens in cItens loop'+ #13 +
      '    if length(vItens.CODIGO_NCM) < 8 then'+ #13 +
      '      Erro(''O c�digo NCM do produto '' || vItens.PRODUTO_ID || '' - '' || vItens.NOME_PRODUTO || '' n�o est� incorreto, o NCM � composto de 8 dig�tos, verifique no cadastro de produtos!'');'+ #13 +
      '    end if;  '+ #13 +
      #13 +
      '    vDadosItens(i).PRODUTO_ID                  := vItens.PRODUTO_ID;'+ #13 +
      '    vDadosItens(i).ITEM_ID                     := vItens.ITEM_ID;'+ #13 +
      '    vDadosItens(i).NOME_PRODUTO                := vItens.NOME_PRODUTO;'+ #13 +
      '    vDadosItens(i).UNIDADE                     := vItens.UNIDADE;'+ #13 +
      '    vDadosItens(i).CST                         := vItens.CST;'+ #13 +
      '    vDadosItens(i).CODIGO_NCM                  := vItens.CODIGO_NCM;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL                 := vItens.VALOR_TOTAL;'+ #13 +
      '    vDadosItens(i).PRECO_UNITARIO              := vItens.PRECO_UNITARIO;'+ #13 +
      '    vDadosItens(i).QUANTIDADE                  := vItens.QUANTIDADE;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_DESCONTO        := vItens.VALOR_TOTAL_DESCONTO;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := vItens.VALOR_TOTAL_OUTRAS_DESPESAS;'+ #13 +
      '    vDadosItens(i).CODIGO_BARRAS               := vItens.CODIGO_BARRAS;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_ICMS           := vItens.BASE_CALCULO_ICMS;'+ #13 +
      '    vDadosItens(i).INDICE_REDUCAO_BASE_ICMS    := vItens.INDICE_REDUCAO_BASE_ICMS;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_ICMS             := vItens.PERCENTUAL_ICMS;'+ #13 +
      '    vDadosItens(i).VALOR_ICMS                  := vItens.VALOR_ICMS;'+ #13 +
      '    vDadosItens(i).IVA                         := vItens.IVA;'+ #13 +
      '    vDadosItens(i).PRECO_PAUTA                 := vItens.PRECO_PAUTA;'+ #13 +
      #13 +
      '    vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST := vItens.INDICE_REDUCAO_BASE_ICMS_ST;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_ICMS_ST        := vItens.BASE_CALCULO_ICMS_ST;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_ICMS_ST          := vItens.PERCENTUAL_ICMS_ST;'+ #13 +
      '    vDadosItens(i).VALOR_ICMS_ST               := vItens.VALOR_ICMS_ST;'+ #13 +
      #13 +
      '    vDadosItens(i).BASE_CALCULO_PIS            := vItens.BASE_CALCULO_PIS;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_PIS              := vItens.PERCENTUAL_PIS;'+ #13 +
      '    vDadosItens(i).VALOR_PIS                   := vItens.VALOR_PIS;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_COFINS         := vItens.BASE_CALCULO_COFINS;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_COFINS           := vItens.PERCENTUAL_COFINS;'+ #13 +
      '    vDadosItens(i).VALOR_COFINS                := vItens.VALOR_COFINS;'+ #13 +
      '    vDadosItens(i).VALOR_IPI                   := vItens.VALOR_IPI;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_IPI              := vItens.PERCENTUAL_IPI;'+ #13 +
      '    vDadosItens(i).CST_PIS                     := vItens.CST_PIS;'+ #13 +
      '    vDadosItens(i).CST_COFINS                  := vItens.CST_COFINS;'+ #13 +
      '    vDadosItens(i).CEST                        := vItens.CEST;'+ #13 +
      #13 +
      '    vDadosItens(i).CFOP_ID                     := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vItens.CST, ''DEV'', ''I'');'+ #13 +
      '		vDadosItens(i).NATUREZA_OPERACAO					 := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);'+ #13 +
      #13 +
      '    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS    := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;'+ #13 +
      '    vDadosNota.VALOR_ICMS           := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST        := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;'+ #13 +
      '    vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;'+ #13 +
      '    vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;'+ #13 +
      '    vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL :='+ #13 +
      '      vDadosNota.VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;'+ #13 +
      '    vDadosNota.VALOR_DESCONTO := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;'+ #13 +
      '    /* ------------------------------------------------------------------------------------------------------ */'+ #13 +
      #13 +
      '		if'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO >'+ #13 +
      '      vValorMaisSignificativo'+ #13 +
      '    then'+ #13 +
      '			vValorMaisSignificativo :='+ #13 +
      '        vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '        vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '        vDadosItens(i).VALOR_IPI -'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '			vCfopIdCapa 	  	      := vDadosItens(i).CFOP_ID;'+ #13 +
      '			vNaturezaOperacao       := vDadosItens(i).NATUREZA_OPERACAO;'+ #13 +
      '		end if;'+ #13 +
      #13 +
      '    ADD_NO_VETOR_SEM_REPETIR(vNotasReferenciasIds, vItens.NOTA_FISCAL_ORIGEM_ID);'+ #13 +
      #13 +
      '    i := i + 1;'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  if vDadosItens.count < 1 then'+ #13 +
      '    ERRO(''Os produtos da nota fiscal n�o foram encontrados!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '	if vCfopIdCapa is null then'+ #13 +
      '		vCfopIdCapa 			:= vDadosItens(0).CFOP_ID;'+ #13 +
      '		vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;'+ #13 +
      '	end if;'+ #13 +
      #13 +
      '	vDadosNota.CFOP_ID := vCfopIdCapa;'+ #13 +
      '  vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;'+ #13 +
      #13 +
      '  vDadosNota.INFORMACOES_COMPLEMENTARES := ''Devolu��o '' || iDEVOLUCAO_ID || '' referente a entrega '' || iENTREGA_ID;'+ #13 +
      #13 +
      '  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;'+ #13 +
      #13 +
      '	insert into NOTAS_FISCAIS('+ #13 +
      '    NOTA_FISCAL_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    CFOP_ID,'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    ORCAMENTO_BASE_ID,'+ #13 +
      '    ENTREGA_ID,'+ #13 +
      '    DEVOLUCAO_ID,'+ #13 +
      '		MODELO_NOTA,'+ #13 +
      '		SERIE_NOTA,'+ #13 +
      '    NATUREZA_OPERACAO,'+ #13 +
      '		STATUS,'+ #13 +
      '    TIPO_MOVIMENTO,'+ #13 +
      '    RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    NOME_FANTASIA_EMITENTE,'+ #13 +
      '    REGIME_TRIBUTARIO,'+ #13 +
      '    CNPJ_EMITENTE,'+ #13 +
      '    INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    LOGRADOURO_EMITENTE,'+ #13 +
      '    COMPLEMENTO_EMITENTE,'+ #13 +
      '    NOME_BAIRRO_EMITENTE,'+ #13 +
      '    NOME_CIDADE_EMITENTE,'+ #13 +
      '    NUMERO_EMITENTE,'+ #13 +
      '    ESTADO_ID_EMITENTE,'+ #13 +
      '    CEP_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    TIPO_NOTA,'+ #13 +
      '    LOGRADOURO_DESTINATARIO,'+ #13 +
      '    COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    ESTADO_ID_DESTINATARIO,'+ #13 +
      '    CEP_DESTINATARIO,'+ #13 +
      '    NUMERO_DESTINATARIO,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    VALOR_TOTAL,'+ #13 +
      '    VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    VALOR_DESCONTO,'+ #13 +
      '    VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    BASE_CALCULO_ICMS,'+ #13 +
      '    VALOR_ICMS,'+ #13 +
      '    BASE_CALCULO_ICMS_ST,'+ #13 +
      '    VALOR_ICMS_ST,'+ #13 +
      '    BASE_CALCULO_PIS,'+ #13 +
      '    VALOR_PIS,'+ #13 +
      '    BASE_CALCULO_COFINS,'+ #13 +
      '    VALOR_COFINS,'+ #13 +
      '    VALOR_IPI,'+ #13 +
      '    VALOR_FRETE,'+ #13 +
      '    VALOR_SEGURO,'+ #13 +
      '    VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '    VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '    VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '    VALOR_RECEBIDO_CREDITO,'+ #13 +
      '    VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '    VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '    PESO_LIQUIDO,'+ #13 +
      '    PESO_BRUTO,'+ #13 +
      '		INFORMACOES_COMPLEMENTARES'+ #13 +
      '  )values('+ #13 +
      '		vNotaFiscalId,'+ #13 +
      '    vDadosNota.CADASTRO_ID,'+ #13 +
      '    vDadosNota.CFOP_ID,'+ #13 +
      '    vDadosNota.EMPRESA_ID,'+ #13 +
      '    vDadosNota.ORCAMENTO_ID,'+ #13 +
      '    iENTREGA_ID,'+ #13 +
      '    iDEVOLUCAO_ID,'+ #13 +
      '		''55'','+ #13 +
      '		SESSAO.PARAMETROS_EMPRESA_LOGADA.SERIE_NFE,'+ #13 +
      '    vDadosNota.NATUREZA_OPERACAO,'+ #13 +
      '		''N'','+ #13 +
      '    ''DEN'','+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_EMITENTE,'+ #13 +
      '    SESSAO.PARAMETROS_EMPRESA_LOGADA.REGIME_TRIBUTARIO,'+ #13 +
      '    vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '    vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '    vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '    vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '    vDadosNota.CEP_EMITENTE,'+ #13 +
      '    vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    vDadosNota.NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    vDadosNota.TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    ''N'','+ #13 +
      '    vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '    vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      '    vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '    vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    vDadosNota.VALOR_TOTAL,'+ #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    vDadosNota.VALOR_DESCONTO,'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS,'+ #13 +
      '    vDadosNota.VALOR_ICMS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST,'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST,'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS,'+ #13 +
      '    vDadosNota.VALOR_PIS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_COFINS,'+ #13 +
      '    vDadosNota.VALOR_COFINS,'+ #13 +
      '    vDadosNota.VALOR_IPI,'+ #13 +
      '    vDadosNota.VALOR_FRETE,'+ #13 +
      '    vDadosNota.VALOR_SEGURO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CREDITO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '    vDadosNota.PESO_LIQUIDO,'+ #13 +
      '    vDadosNota.PESO_BRUTO,'+ #13 +
      '		vDadosNota.INFORMACOES_COMPLEMENTARES'+ #13 +
      '  );'+ #13 +
      #13 +
      '  /* inserindo a refer�ncia */'+ #13 +
      '  for i in vNotasReferenciasIds.first..vNotasReferenciasIds.last loop'+ #13 +
      '    insert into NOTAS_FISCAIS_REFERENCIAS('+ #13 +
      '      NOTA_FISCAL_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      TIPO_REFERENCIA,'+ #13 +
      '      NOTA_FISCAL_ID_REFERENCIADA'+ #13 +
      '    )values('+ #13 +
      '      vNotaFiscalId,'+ #13 +
      '      i,'+ #13;
  end;

  function GetComando2: string;
  begin
    Result :=
      '      ''A'','+ #13 +
      '      vNotasReferenciasIds(i)'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  for i in 0..vDadosItens.count - 1 loop'+ #13 +
      '    insert into NOTAS_FISCAIS_ITENS('+ #13 +
      '      NOTA_FISCAL_ID,'+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      NOME_PRODUTO,'+ #13 +
      '      UNIDADE,'+ #13 +
      '      CFOP_ID,'+ #13 +
      '      CST,'+ #13 +
      '      CODIGO_NCM,'+ #13 +
      '      VALOR_TOTAL,'+ #13 +
      '      PRECO_UNITARIO,'+ #13 +
      '      QUANTIDADE,'+ #13 +
      '      VALOR_TOTAL_DESCONTO,'+ #13 +
      '      VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      BASE_CALCULO_ICMS,'+ #13 +
      '      PERCENTUAL_ICMS,'+ #13 +
      '      VALOR_ICMS,'+ #13 +
      '      BASE_CALCULO_ICMS_ST,'+ #13 +
      '      VALOR_ICMS_ST,'+ #13 +
      '      CST_PIS,'+ #13 +
      '      BASE_CALCULO_PIS,'+ #13 +
      '      PERCENTUAL_PIS,'+ #13 +
      '      VALOR_PIS,'+ #13 +
      '      CST_COFINS,'+ #13 +
      '      BASE_CALCULO_COFINS,'+ #13 +
      '      PERCENTUAL_COFINS,'+ #13 +
      '      VALOR_COFINS,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      IVA,'+ #13 +
      '      PRECO_PAUTA,'+ #13 +
      '      CODIGO_BARRAS,'+ #13 +
      '      VALOR_IPI,'+ #13 +
      '      PERCENTUAL_IPI,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      PERCENTUAL_ICMS_ST,'+ #13 +
      '      CEST'+ #13 +
      '    )values('+ #13 +
      '      vNotaFiscalId,'+ #13 +
      '      vDadosItens(i).PRODUTO_ID,'+ #13 +
      '      vDadosItens(i).ITEM_ID,'+ #13 +
      '      vDadosItens(i).NOME_PRODUTO,'+ #13 +
      '      vDadosItens(i).UNIDADE,'+ #13 +
      '      vDadosItens(i).CFOP_ID,'+ #13 +
      '      vDadosItens(i).CST,'+ #13 +
      '      vDadosItens(i).CODIGO_NCM,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL,'+ #13 +
      '      vDadosItens(i).PRECO_UNITARIO,'+ #13 +
      '      vDadosItens(i).QUANTIDADE,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CST_PIS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS,'+ #13 +
      '      vDadosItens(i).VALOR_PIS,'+ #13 +
      '      vDadosItens(i).CST_COFINS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS,'+ #13 +
      '      vDadosItens(i).VALOR_COFINS,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      vDadosItens(i).IVA,'+ #13 +
      '      vDadosItens(i).PRECO_PAUTA,'+ #13 +
      '      vDadosItens(i).CODIGO_BARRAS,'+ #13 +
      '      vDadosItens(i).VALOR_IPI,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_IPI,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CEST'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      'end GERAR_NOTA_DEVOLUCAO_ENTREGA;'+ #13;
  end;

begin
  Result :=
    GetComando1 + GetComando2;
end;

function AjustarProcGERAR_NOTA_RETORNO_ENTREGA: string;
  function GetComando1: string;
  begin
    Result :=
      'create or replace procedure GERAR_NOTA_RETORNO_ENTREGA(iENTREGA_ID in number)'+ #13 +
      'is'+ #13 +
      '  i                       number default 0;'+ #13 +
      '  vQtde                   number default 0;'+ #13 +
      '  vNotaFiscalId           positive;'+ #13 +
      '  vDadosNota              RecordsNotasFiscais.RecNotaFiscal;'+ #13 +
      '  vDadosItens             RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;'+ #13 +
      #13 +
      '  vValorMaisSignificativo number default 0;'+ #13 +
      '  vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;'+ #13 +
      '  vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;'+ #13 +
      '  vNotasReferenciasIds    TIPOS.ArrayOfNumeros;'+ #13 +
      #13 +
      '  vTipoOperacao           char(1);'+ #13 +
      '  vSerieNFe               PARAMETROS_EMPRESA.SERIE_NFE%type;'+ #13 +
      #13 +
      '  cursor cItens is'+ #13 +
      '  select'+ #13 +
      '    NIT.NOTA_FISCAL_ID as NOTA_FISCAL_ORIGEM_ID,'+ #13 +
      '    ITE.PRODUTO_ID,'+ #13 +
      '    ITE.ITEM_ID,'+ #13 +
      '    EIT.QUANTIDADE,'+ #13 +
      '    NIT.NOME_PRODUTO,'+ #13 +
      #13 +
      '    round(NIT.VALOR_TOTAL / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_TOTAL,'+ #13 +
      '    NIT.PRECO_UNITARIO,'+ #13 +
      '    round(NIT.VALOR_TOTAL_DESCONTO / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_TOTAL_DESCONTO,'+ #13 +
      '    round(NIT.VALOR_TOTAL_OUTRAS_DESPESAS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_ICMS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as BASE_CALCULO_ICMS,'+ #13 +
      '    NIT.PERCENTUAL_ICMS,'+ #13 +
      '    round(NIT.VALOR_ICMS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_ICMS,'+ #13 +
      '    NIT.INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_ICMS_ST / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as BASE_CALCULO_ICMS_ST,'+ #13 +
      '    NIT.PERCENTUAL_ICMS_ST,'+ #13 +
      '    round(NIT.VALOR_ICMS_ST / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_ICMS_ST,'+ #13 +
      '    NIT.INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      #13 +
      '    NIT.PRECO_PAUTA,'+ #13 +
      '    NIT.IVA,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_COFINS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as BASE_CALCULO_COFINS,'+ #13 +
      '    NIT.PERCENTUAL_COFINS,'+ #13 +
      '    round(NIT.VALOR_COFINS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_COFINS,'+ #13 +
      #13 +
      '    round(NIT.BASE_CALCULO_PIS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as BASE_CALCULO_PIS,'+ #13 +
      '    NIT.PERCENTUAL_PIS,'+ #13 +
      '    round(NIT.VALOR_PIS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_PIS,'+ #13 +
      #13 +
      '    round(NIT.VALOR_IPI / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_IPI,'+ #13 +
      '    NIT.PERCENTUAL_IPI,'+ #13 +
      #13 +
      '    NIT.CST,'+ #13 +
      '    NIT.CODIGO_NCM,'+ #13 +
      '    NIT.UNIDADE,'+ #13 +
      '    NIT.CODIGO_BARRAS,'+ #13 +
      '    NIT.CEST,'+ #13 +
      '    NIT.CST_PIS,'+ #13 +
      '    NIT.CST_COFINS'+ #13 +
      '  from'+ #13 +
      '    ('+ #13 +
      '      /* Este select � feito desta forma por causa dos LOTES! */'+ #13 +
      '      select'+ #13 +
      '        ENTREGA_ID,'+ #13 +
      '        PRODUTO_ID,'+ #13 +
      '        ITEM_ID,'+ #13 +
      '        sum(RETORNADOS) as QUANTIDADE'+ #13 +
      '      from'+ #13 +
      '        ENTREGAS_ITENS'+ #13 +
      '      where ENTREGA_ID = iENTREGA_ID'+ #13 +
      '      group by'+ #13 +
      '        ENTREGA_ID,'+ #13 +
      '        PRODUTO_ID,'+ #13 +
      '        ITEM_ID'+ #13 +
      '      having sum(RETORNADOS) > 0'+ #13 +
      '    ) EIT'+ #13 +
      #13 +
      '  inner join ENTREGAS ENT'+ #13 +
      '  on EIT.ENTREGA_ID = ENT.ENTREGA_ID'+ #13 +
      #13 +
      '  inner join ORCAMENTOS_ITENS ITE'+ #13 +
      '  on ENT.ORCAMENTO_ID = ITE.ORCAMENTO_ID'+ #13 +
      '  and EIT.PRODUTO_ID = ITE.PRODUTO_ID'+ #13 +
      '  and EIT.ITEM_ID = ITE.ITEM_ID'+ #13 +
      #13 +
      '  inner join NOTAS_FISCAIS NOF'+ #13 +
      '  on EIT.ENTREGA_ID = NOF.ENTREGA_ID'+ #13 +
      #13 +
      '  inner join NOTAS_FISCAIS_ITENS NIT'+ #13 +
      '  on NOF.NOTA_FISCAL_ID = NIT.NOTA_FISCAL_ID'+ #13 +
      '  and ITE.ITEM_ID = NIT.ITEM_ID'+ #13 +
      #13 +
      '  where EIT.ENTREGA_ID = iENTREGA_ID'+ #13 +
      '  /* N�o trazer os produtos KITS */'+ #13 +
      '  and ITE.TIPO_CONTROLE_ESTOQUE not in(''K'', ''A'')'+ #13 +
      '  order by'+ #13 +
      '    ITE.ITEM_ID;'+ #13 +
      #13 +
      'begin '+ #13 +
      #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    NOTAS_FISCAIS'+ #13 +
      '  where ENTREGA_ID = iENTREGA_ID'+ #13 +
      '  and TIPO_MOVIMENTO = ''VEN'''+ #13 +
      '  and STATUS = ''E'';'+ #13 +
      #13 +
      '  if vQtde = 0 then'+ #13 +
      '    return;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    ENTREGAS_ITENS'+ #13 +
      '  where ENTREGA_ID = iENTREGA_ID'+ #13 +
      '  and RETORNADOS > 0;'+ #13 +
      #13 +
      '  if vQtde = 0 then'+ #13 +
      '    return;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  begin'+ #13 +
      '    select'+ #13 +
      '      NFI.EMPRESA_ID,'+ #13 +
      '      NFI.CADASTRO_ID,'+ #13 +
      '      NFI.ORCAMENTO_ID,'+ #13 +
      '      0 as VALOR_DINHEIRO,'+ #13 +
      '      0 as VALOR_CARTAO_CREDITO,'+ #13 +
      '      0 as VALOR_CARTAO_DEBITO,'+ #13 +
      '      0 as VALOR_CREDITO,'+ #13 +
      '      0 as VALOR_COBRANCA,'+ #13 +
      '      0 as VALOR_CHEQUE,'+ #13 +
      '      0 as VALOR_FINANCEIRA,'+ #13 +
      '      0 as VALOR_FRETE,'+ #13 +
      '      0 as VALOR_SEGURO,'+ #13 +
      #13 +
      '      '''' as NOME_CONSUMIDOR_FINAL,'+ #13 +
      #13 +
      '      /* Emitente */      '+ #13 +
      '      EMP.RAZAO_SOCIAL,'+ #13 +
      '      EMP.NOME_FANTASIA,      '+ #13 +
      '      EMP.CNPJ,'+ #13 +
      '      EMP.INSCRICAO_ESTADUAL,'+ #13 +
      '      EMP.LOGRADOURO,'+ #13 +
      '      EMP.COMPLEMENTO,'+ #13 +
      '      BAE.NOME as NOME_BAIRRO_EMITENTE,'+ #13 +
      '      CIE.NOME as NOME_CIDADE_EMITENTE,'+ #13 +
      '      EMP.NUMERO,'+ #13 +
      '      ESE.ESTADO_ID,'+ #13 +
      '      EMP.CEP,'+ #13 +
      '      EMP.TELEFONE_PRINCIPAL,'+ #13 +
      '      CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      #13 +
      '      /* Destinatario */'+ #13 +
      '      NFI.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '      NFI.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '      NFI.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '      NFI.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '      NFI.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '      NFI.LOGRADOURO_DESTINATARIO,'+ #13 +
      '      NFI.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '      NFI.NUMERO_DESTINATARIO,'+ #13 +
      '      NFI.CEP_DESTINATARIO,      '+ #13 +
      '      NFI.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '      NFI.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '      NFI.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      NFI.ESTADO_ID_DESTINATARIO,'+ #13 +
      #13 +
      '      case when NFI.ESTADO_ID_DESTINATARIO = ESE.ESTADO_ID then ''I'' else ''E'' end,'+ #13 +
      '      PAE.SERIE_NFE'+ #13 +
      '    into'+ #13 +
      '      vDadosNota.EMPRESA_ID,'+ #13 +
      '      vDadosNota.CADASTRO_ID,'+ #13 +
      '      vDadosNota.ORCAMENTO_ID,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CREDITO,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '      vDadosNota.VALOR_FRETE,'+ #13 +
      '      vDadosNota.VALOR_SEGURO,'+ #13 +
      #13 +
      '      vDadosNota.NOME_CONSUMIDOR_FINAL,'+ #13 +
      '      /* Emitente */'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_FANTASIA_EMITENTE,      '+ #13 +
      '      vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '      vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '      vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '      vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '      vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '      vDadosNota.CEP_EMITENTE,'+ #13 +
      '      vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '      /* Destinatario */'+ #13 +
      '      vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '      vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '      vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '      vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '      vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '      vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      #13 +
      '      vTipoOperacao,'+ #13 +
      '      vSerieNFe'+ #13 +
      '    from'+ #13 +
      '      NOTAS_FISCAIS NFI'+ #13 +
      #13 +
      '    inner join PARAMETROS_EMPRESA PAE'+ #13 +
      '    on NFI.EMPRESA_ID = PAE.EMPRESA_ID'+ #13 +
      #13 +
      '    inner join EMPRESAS EMP'+ #13 +
      '    on NFI.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAE'+ #13 +
      '    on EMP.BAIRRO_ID = BAE.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CIE'+ #13 +
      '    on BAE.CIDADE_ID = CIE.CIDADE_ID'+ #13 +
      #13 +
      '    inner join ESTADOS ESE'+ #13 +
      '    on CIE.ESTADO_ID = ESE.ESTADO_ID    '+ #13 +
      #13 +
      '    where NFI.ENTREGA_ID = iENTREGA_ID'+ #13 +
      '    and NFI.TIPO_MOVIMENTO = ''VEN'''+ #13 +
      '    and NFI.STATUS = ''E'';'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Houve um erro ao buscar os dados da devolu��o para gera��o da NFe!'');'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  if vDadosNota.CADASTRO_ID = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then'+ #13 +
      '    ERRO(''N�o � poss�vel gerar uma nota de devolu��o de uma venda para consumidor final!'');'+ #13 +
      '  end if;   '+ #13 +
      #13 +
      '  vDadosNota.BASE_CALCULO_ICMS    := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS           := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_ICMS_ST := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS_ST        := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_PIS     := 0;'+ #13 +
      '  vDadosNota.VALOR_PIS            := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_COFINS  := 0;'+ #13 +
      '  vDadosNota.VALOR_COFINS         := 0;'+ #13 +
      '  vDadosNota.VALOR_IPI            := 0;'+ #13 +
      '  vDadosNota.VALOR_FRETE          := 0;'+ #13 +
      '  vDadosNota.VALOR_SEGURO         := 0;'+ #13 +
      '  vDadosNota.PESO_LIQUIDO         := 0;'+ #13 +
      '  vDadosNota.PESO_BRUTO           := 0;  '+ #13 +
      #13 +
      #13 +
      '  for vItens in cItens loop'+ #13 +
      '    if length(vItens.CODIGO_NCM) < 8 then'+ #13 +
      '      Erro(''O c�digo NCM do produto '' || vItens.PRODUTO_ID || '' - '' || vItens.NOME_PRODUTO || '' n�o est� incorreto, o NCM � composto de 8 dig�tos, verifique no cadastro de produtos!'');'+ #13 +
      '    end if;  '+ #13 +
      #13 +
      '    vDadosItens(i).PRODUTO_ID                  := vItens.PRODUTO_ID;'+ #13 +
      '    vDadosItens(i).ITEM_ID                     := vItens.ITEM_ID;'+ #13 +
      '    vDadosItens(i).NOME_PRODUTO                := vItens.NOME_PRODUTO;'+ #13 +
      '    vDadosItens(i).UNIDADE                     := vItens.UNIDADE;'+ #13 +
      '    vDadosItens(i).CST                         := vItens.CST;'+ #13 +
      '    vDadosItens(i).CODIGO_NCM                  := vItens.CODIGO_NCM;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL                 := vItens.VALOR_TOTAL;'+ #13 +
      '    vDadosItens(i).PRECO_UNITARIO              := vItens.PRECO_UNITARIO;'+ #13 +
      '    vDadosItens(i).QUANTIDADE                  := vItens.QUANTIDADE;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_DESCONTO        := vItens.VALOR_TOTAL_DESCONTO;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := vItens.VALOR_TOTAL_OUTRAS_DESPESAS;'+ #13 +
      '    vDadosItens(i).CODIGO_BARRAS               := vItens.CODIGO_BARRAS;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_ICMS           := vItens.BASE_CALCULO_ICMS;'+ #13 +
      '    vDadosItens(i).INDICE_REDUCAO_BASE_ICMS    := vItens.INDICE_REDUCAO_BASE_ICMS;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_ICMS             := vItens.PERCENTUAL_ICMS;'+ #13 +
      '    vDadosItens(i).VALOR_ICMS                  := vItens.VALOR_ICMS;'+ #13 +
      '    vDadosItens(i).IVA                         := vItens.IVA;'+ #13 +
      '    vDadosItens(i).PRECO_PAUTA                 := vItens.PRECO_PAUTA;'+ #13 +
      #13 +
      '    vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST := vItens.INDICE_REDUCAO_BASE_ICMS_ST;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_ICMS_ST        := vItens.BASE_CALCULO_ICMS_ST;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_ICMS_ST          := vItens.PERCENTUAL_ICMS_ST;'+ #13 +
      '    vDadosItens(i).VALOR_ICMS_ST               := vItens.VALOR_ICMS_ST;'+ #13 +
      #13 +
      '    vDadosItens(i).BASE_CALCULO_PIS            := vItens.BASE_CALCULO_PIS;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_PIS              := vItens.PERCENTUAL_PIS;'+ #13 +
      '    vDadosItens(i).VALOR_PIS                   := vItens.VALOR_PIS;'+ #13 +
      '    vDadosItens(i).BASE_CALCULO_COFINS         := vItens.BASE_CALCULO_COFINS;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_COFINS           := vItens.PERCENTUAL_COFINS;'+ #13 +
      '    vDadosItens(i).VALOR_COFINS                := vItens.VALOR_COFINS;'+ #13 +
      '    vDadosItens(i).VALOR_IPI                   := vItens.VALOR_IPI;'+ #13 +
      '    vDadosItens(i).PERCENTUAL_IPI              := vItens.PERCENTUAL_IPI;'+ #13 +
      '    vDadosItens(i).CST_PIS                     := vItens.CST_PIS;'+ #13 +
      '    vDadosItens(i).CST_COFINS                  := vItens.CST_COFINS;'+ #13 +
      '    vDadosItens(i).CEST                        := vItens.CEST;'+ #13 +
      #13 +
      '    vDadosItens(i).CFOP_ID                     := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vItens.CST, ''REN'', vTipoOperacao);'+ #13 +
      '		vDadosItens(i).NATUREZA_OPERACAO					 := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);'+ #13 +
      #13 +
      '    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS    := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;'+ #13 +
      '    vDadosNota.VALOR_ICMS           := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST        := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;'+ #13 +
      '    vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;'+ #13 +
      '    vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;'+ #13 +
      '    vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL :='+ #13 +
      '      vDadosNota.VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;'+ #13 +
      '    vDadosNota.VALOR_DESCONTO := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;'+ #13 +
      '    /* ------------------------------------------------------------------------------------------------------ */'+ #13 +
      #13 +
      '		if'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO >'+ #13 +
      '      vValorMaisSignificativo'+ #13 +
      '    then'+ #13 +
      '			vValorMaisSignificativo :='+ #13 +
      '        vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '        vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '        vDadosItens(i).VALOR_IPI -'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '			vCfopIdCapa 	  	      := vDadosItens(i).CFOP_ID;'+ #13 +
      '			vNaturezaOperacao       := vDadosItens(i).NATUREZA_OPERACAO;'+ #13 +
      '		end if;'+ #13 +
      #13 +
      '    ADD_NO_VETOR_SEM_REPETIR(vNotasReferenciasIds, vItens.NOTA_FISCAL_ORIGEM_ID);'+ #13 +
      #13 +
      '    i := i + 1;'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  if vDadosItens.count < 1 then'+ #13 +
      '    ERRO(''Os produtos da nota fiscal n�o foram encontrados!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '	if vCfopIdCapa is null then'+ #13 +
      '		vCfopIdCapa 			:= vDadosItens(0).CFOP_ID;'+ #13 +
      '		vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;'+ #13 +
      '	end if;'+ #13 +
      #13 +
      '	vDadosNota.CFOP_ID := vCfopIdCapa;'+ #13 +
      '  vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;'+ #13 +
      #13 +
      '  vDadosNota.INFORMACOES_COMPLEMENTARES := ''Retorno referente a entrega '' || iENTREGA_ID;'+ #13 +
      #13 +
      '  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;'+ #13 +
      #13 +
      '	insert into NOTAS_FISCAIS('+ #13 +
      '    NOTA_FISCAL_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    CFOP_ID,'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    ORCAMENTO_BASE_ID,'+ #13 +
      '    ENTREGA_ID,    '+ #13 +
      '		MODELO_NOTA,'+ #13 +
      '		SERIE_NOTA,'+ #13 +
      '    NATUREZA_OPERACAO,'+ #13 +
      '		STATUS,'+ #13 +
      '    TIPO_MOVIMENTO,'+ #13 +
      '    RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    NOME_FANTASIA_EMITENTE,'+ #13 +
      '    REGIME_TRIBUTARIO,'+ #13 +
      '    CNPJ_EMITENTE,'+ #13 +
      '    INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    LOGRADOURO_EMITENTE,'+ #13 +
      '    COMPLEMENTO_EMITENTE,'+ #13 +
      '    NOME_BAIRRO_EMITENTE,'+ #13 +
      '    NOME_CIDADE_EMITENTE,'+ #13 +
      '    NUMERO_EMITENTE,'+ #13 +
      '    ESTADO_ID_EMITENTE,'+ #13 +
      '    CEP_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    TIPO_NOTA,'+ #13 +
      '    LOGRADOURO_DESTINATARIO,'+ #13 +
      '    COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    ESTADO_ID_DESTINATARIO,'+ #13 +
      '    CEP_DESTINATARIO,'+ #13 +
      '    NUMERO_DESTINATARIO,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    VALOR_TOTAL,'+ #13 +
      '    VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    VALOR_DESCONTO,'+ #13 +
      '    VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    BASE_CALCULO_ICMS,'+ #13 +
      '    VALOR_ICMS,'+ #13 +
      '    BASE_CALCULO_ICMS_ST,'+ #13 +
      '    VALOR_ICMS_ST,'+ #13 +
      '    BASE_CALCULO_PIS,'+ #13 +
      '    VALOR_PIS,'+ #13 +
      '    BASE_CALCULO_COFINS,'+ #13 +
      '    VALOR_COFINS,'+ #13 +
      '    VALOR_IPI,'+ #13 +
      '    VALOR_FRETE,'+ #13 +
      '    VALOR_SEGURO,'+ #13 +
      '    VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '    VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '    VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '    VALOR_RECEBIDO_CREDITO,'+ #13 +
      '    VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '    VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '    PESO_LIQUIDO,'+ #13 +
      '    PESO_BRUTO,'+ #13 +
      '		INFORMACOES_COMPLEMENTARES'+ #13 +
      '  )values('+ #13 +
      '		vNotaFiscalId,'+ #13 +
      '    vDadosNota.CADASTRO_ID,'+ #13 +
      '    vDadosNota.CFOP_ID,'+ #13 +
      '    vDadosNota.EMPRESA_ID,'+ #13 +
      '    vDadosNota.ORCAMENTO_ID,'+ #13 +
      '    iENTREGA_ID,    '+ #13 +
      '		''55'','+ #13 +
      '		vSerieNFe,'+ #13 +
      '    vDadosNota.NATUREZA_OPERACAO,'+ #13 +
      '		''N'','+ #13 +
      '    ''REN'','+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_EMITENTE,'+ #13 +
      '    SESSAO.PARAMETROS_EMPRESA_LOGADA.REGIME_TRIBUTARIO,'+ #13 +
      '    vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '    vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '    vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '    vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '    vDadosNota.CEP_EMITENTE,'+ #13 +
      '    vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    vDadosNota.NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    vDadosNota.TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    ''N'','+ #13 +
      '    vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '    vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      '    vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '    vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    vDadosNota.VALOR_TOTAL,'+ #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    vDadosNota.VALOR_DESCONTO,'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS,'+ #13 +
      '    vDadosNota.VALOR_ICMS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST,'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST,'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS,'+ #13 +
      '    vDadosNota.VALOR_PIS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_COFINS,'+ #13 +
      '    vDadosNota.VALOR_COFINS,'+ #13;
  end;

  function GetComando2: string;
  begin
    Result :=
      '    vDadosNota.VALOR_IPI,'+ #13 +
      '    vDadosNota.VALOR_FRETE,'+ #13 +
      '    vDadosNota.VALOR_SEGURO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CREDITO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '    vDadosNota.PESO_LIQUIDO,'+ #13 +
      '    vDadosNota.PESO_BRUTO,'+ #13 +
      '		vDadosNota.INFORMACOES_COMPLEMENTARES'+ #13 +
      '  );'+ #13 +
      #13 +
      '  /* inserindo a refer�ncia */'+ #13 +
      '  for i in vNotasReferenciasIds.first..vNotasReferenciasIds.last loop'+ #13 +
      '    insert into NOTAS_FISCAIS_REFERENCIAS('+ #13 +
      '      NOTA_FISCAL_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      TIPO_REFERENCIA,'+ #13 +
      '      NOTA_FISCAL_ID_REFERENCIADA'+ #13 +
      '    )values('+ #13 +
      '      vNotaFiscalId,'+ #13 +
      '      i,'+ #13 +
      '      ''A'','+ #13 +
      '      vNotasReferenciasIds(i)'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  for i in 0..vDadosItens.count - 1 loop'+ #13 +
      '    insert into NOTAS_FISCAIS_ITENS('+ #13 +
      '      NOTA_FISCAL_ID,'+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      NOME_PRODUTO,'+ #13 +
      '      UNIDADE,'+ #13 +
      '      CFOP_ID,'+ #13 +
      '      CST,'+ #13 +
      '      CODIGO_NCM,'+ #13 +
      '      VALOR_TOTAL,'+ #13 +
      '      PRECO_UNITARIO,'+ #13 +
      '      QUANTIDADE,'+ #13 +
      '      VALOR_TOTAL_DESCONTO,'+ #13 +
      '      VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      BASE_CALCULO_ICMS,'+ #13 +
      '      PERCENTUAL_ICMS,'+ #13 +
      '      VALOR_ICMS,'+ #13 +
      '      BASE_CALCULO_ICMS_ST,'+ #13 +
      '      VALOR_ICMS_ST,'+ #13 +
      '      CST_PIS,'+ #13 +
      '      BASE_CALCULO_PIS,'+ #13 +
      '      PERCENTUAL_PIS,'+ #13 +
      '      VALOR_PIS,'+ #13 +
      '      CST_COFINS,'+ #13 +
      '      BASE_CALCULO_COFINS,'+ #13 +
      '      PERCENTUAL_COFINS,'+ #13 +
      '      VALOR_COFINS,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      IVA,'+ #13 +
      '      PRECO_PAUTA,'+ #13 +
      '      CODIGO_BARRAS,'+ #13 +
      '      VALOR_IPI,'+ #13 +
      '      PERCENTUAL_IPI,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      PERCENTUAL_ICMS_ST,'+ #13 +
      '      CEST'+ #13 +
      '    )values('+ #13 +
      '      vNotaFiscalId,'+ #13 +
      '      vDadosItens(i).PRODUTO_ID,'+ #13 +
      '      vDadosItens(i).ITEM_ID,'+ #13 +
      '      vDadosItens(i).NOME_PRODUTO,'+ #13 +
      '      vDadosItens(i).UNIDADE,'+ #13 +
      '      vDadosItens(i).CFOP_ID,'+ #13 +
      '      vDadosItens(i).CST,'+ #13 +
      '      vDadosItens(i).CODIGO_NCM,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL,'+ #13 +
      '      vDadosItens(i).PRECO_UNITARIO,'+ #13 +
      '      vDadosItens(i).QUANTIDADE,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CST_PIS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS,'+ #13 +
      '      vDadosItens(i).VALOR_PIS,'+ #13 +
      '      vDadosItens(i).CST_COFINS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS,'+ #13 +
      '      vDadosItens(i).VALOR_COFINS,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      vDadosItens(i).IVA,'+ #13 +
      '      vDadosItens(i).PRECO_PAUTA,'+ #13 +
      '      vDadosItens(i).CODIGO_BARRAS,'+ #13 +
      '      vDadosItens(i).VALOR_IPI,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_IPI,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CEST'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      'end GERAR_NOTA_RETORNO_ENTREGA;'+ #13;
  end;

begin
  Result :=
    GetComando1 + GetComando2;
end;

function AjustarConstraintCK_CFOPS_CST_OPER_TIPO_MOV: string;
begin
  Result :=
    'alter table CFOPS_CST_OPERACOES'+ #13 +
    'drop constraint CK_CFOPS_CST_OPER_TIPO_MOV;'+ #13 +
    #13 +
    'alter table CFOPS_CST_OPERACOES'+ #13 +
    'add constraint CK_CFOPS_CST_OPER_TIPO_MOV'+ #13 +
    'check(TIPO_MOVIMENTO in(''VIT'', ''DEV'', ''TPE'', ''REN''));'+ #13
end;

function AjustarConstraintCK_NOTAS_FISCAIS_TIPO_MOVIMENT: string;
begin
  Result :=
    'alter table NOTAS_FISCAIS'+ #13 +
    'drop constraint CK_NOTAS_FISCAIS_TIPO_MOVIMENT;'+ #13 +
    #13 +
    'alter table NOTAS_FISCAIS'+ #13 +
    'add constraint CK_NOTAS_FISCAIS_TIPO_MOVIMENT'+ #13 +
    'check('+ #13 +
    '  TIPO_MOVIMENTO = ''VRA'' and ORCAMENTO_BASE_ID is not null and RETIRADA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''VRE'' and ORCAMENTO_BASE_ID is not null and RETIRADA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''VEN'' and ORCAMENTO_BASE_ID is not null and ENTREGA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''DRE'' and ORCAMENTO_BASE_ID is not null and DEVOLUCAO_ID is not null and RETIRADA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''DEN'' and ORCAMENTO_BASE_ID is not null and DEVOLUCAO_ID is not null and ENTREGA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''OUT'' and OUTRA_NOTA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''ACU'' and ACUMULADO_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''DAC'' and ORCAMENTO_BASE_ID is not null and DEVOLUCAO_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''TPE'' and TRANSFERENCIA_PRODUTOS_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''TFR'' and TRANSF_FISCAL_RETIRADA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''TFE'' and TRANSF_FISCAL_ENTREGA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''NNE'' and NOTA_FISCAL_ORIGEM_CUPOM_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''REN'' and ORCAMENTO_BASE_ID is not null and ENTREGA_ID is not null'+ #13 +
    ');'+ #13
end;

function AjustarFunctionBUSCAR_CFOP_ID_OPERACAO: string;
begin
  Result :=
    'create or replace function BUSCAR_CFOP_ID_OPERACAO('+ #13 +
    '  iEMPRESA_ID     in number,'+ #13 +
    '  iCST            in string,'+ #13 +
    '  iTIPO_MOVIMENTO in string,'+ #13 +
    '  iTIPO_OPERACAO  in string'+ #13 +
    ') '+ #13 +
    'return string'+ #13 +
    'is'+ #13 +
    '  vCfopId  CFOP.CFOP_PESQUISA_ID%type;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  if iCST not in(''00'',''10'',''20'',''30'',''40'',''41'',''50'',''51'',''60'',''70'',''90'') then'+ #13 +
    '    ERRO(''CST passado via par�metro incorreto!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '/* iTIPO_OPERACAO'+ #13 +
    '  I - Interna'+ #13 +
    '  E - Interestadual'+ #13 +
    '  X - Exterior'+ #13 +
    '*/'+ #13 +
    '  if not(nvl(iTIPO_OPERACAO, ''A'') in(''I'', ''E'', ''X'')) then'+ #13 +
    '    ERRO(''TIPO_OPERACAO passado via par�metro incorreto!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  /*'+ #13 +
    '    iTIPO_MOVIMENTO'+ #13 +
    '    VIT - Venda interna'+ #13 +
    '    DEV - Devolu��o de venda'+ #13 +
    '    TPE - Transfer�ncia de produtos entre empresas'+ #13 +
    '    REN - Retorno de entrega'+ #13 +
    '  */'+ #13 +
    #13 +
    '  if not(nvl(iTIPO_MOVIMENTO, ''XXX'') in(''VIT'', ''DEV'', ''TPE'', ''REN'')) then'+ #13 +
    '    ERRO(''iTIPO_MOVIMENTO passado via par�metro incorreto!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  begin'+ #13 +
    '    select'+ #13 +
    '      CFOP_ID'+ #13 +
    '    into'+ #13 +
    '      vCfopId'+ #13 +
    '    from'+ #13 +
    '      CFOPS_CST_OPERACOES'+ #13 +
    '    where CST = iCST'+ #13 +
    '    and EMPRESA_ID = iEMPRESA_ID'+ #13 +
    '    and TIPO_OPERACAO = iTIPO_OPERACAO'+ #13 +
    '    and TIPO_MOVIMENTO = iTIPO_MOVIMENTO;'+ #13 +
    '  exception'+ #13 +
    '    when others then'+ #13 +
    '      ERRO('+ #13 +
    '        ''Falha ao buscar os par�metros de CFOP! '' || sqlerrm || chr(13) ||'+ #13 +
    '        ''CST '' || iCST || chr(13) ||'+ #13 +
    '        ''TIPO OPER. '' || iTIPO_OPERACAO || chr(13) ||'+ #13 +
    '        ''EMPRESA '' || iEMPRESA_ID || chr(13) ||'+ #13 +
    '        ''TIPO MOV '' || iTIPO_MOVIMENTO'+ #13 +
    '      );'+ #13 +
    '  end;'+ #13 +
    #13 +
    '  return vCfopId;'+ #13 +
    #13 +
    'end BUSCAR_CFOP_ID_OPERACAO;'+ #13
end;

function AjustarProcedureBAIXAR_ENTREGA: string;
begin
  Result :=
    'create or replace procedure BAIXAR_ENTREGA('+ #13 +
    '  iENTREGA_ID      in number,'+ #13 +
    '  iSTATUS          in string,'+ #13 +
    #13 +
    '  iPRODUTOS_IDS    in TIPOS.ArrayOfNumeros,'+ #13 +
    '  iITENS_IDS       in TIPOS.ArrayOfNumeros,  '+ #13 +
    '  iLOTES           in TIPOS.ArrayOfString,'+ #13 +
    '  iRETORNADOS      in TIPOS.ArrayOfNumeros  '+ #13 +
    ')'+ #13 +
    'is'+ #13 +
    '  i            number;'+ #13 +
    '  vQtde        number default 0;  '+ #13 +
    #13 +
    '  vPrevisaoEntrega     ENTREGAS.PREVISAO_ENTREGA%type;'+ #13 +
    '  vLocalId             ENTREGAS.LOCAL_ID%type;'+ #13 +
    '  vEmpresaId           ENTREGAS.EMPRESA_ENTREGA_ID%type;'+ #13 +
    '  vOrcamentoId         ENTREGAS.ORCAMENTO_ID%type;'+ #13 +
    '  vPendenteRecebimento char(1);'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  select'+ #13 +
    '    ENT.EMPRESA_ENTREGA_ID,'+ #13 +
    '    ENT.LOCAL_ID,'+ #13 +
    '    ENT.ORCAMENTO_ID,'+ #13 +
    '    ENT.PREVISAO_ENTREGA,'+ #13 +
    '    case when PAE.PERM_BAIXAR_ENT_REC_PENDENTE = ''N'' and ORC.RECEBER_NA_ENTREGA = ''S'' and ORC.STATUS <> ''RE'' then ''S'' else ''N'' end as PENDENTE_RECEBIMENTO'+ #13 +
    '  into'+ #13 +
    '    vEmpresaId,'+ #13 +
    '    vLocalId,'+ #13 +
    '    vOrcamentoId,'+ #13 +
    '    vPrevisaoEntrega,'+ #13 +
    '    vPendenteRecebimento'+ #13 +
    '  from'+ #13 +
    '    ENTREGAS ENT'+ #13 +
    #13 +
    '  inner join ORCAMENTOS ORC'+ #13 +
    '  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    '  inner join PARAMETROS_EMPRESA PAE'+ #13 +
    '  on ORC.EMPRESA_ID = PAE.EMPRESA_ID'+ #13 +
    #13 +
    '  where ENT.ENTREGA_ID = iENTREGA_ID;'+ #13 +
    #13 +
    '  if vPendenteRecebimento = ''S'' and iSTATUS <> ''RTO'' then'+ #13 +
    '    ERRO(''N�o � permitido realizar baixa de uma entrega onde o pedido � para recebimento na entrega e o mesmo ainda n�o foi recebido no caixa!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if iSTATUS in(''RPA'', ''RTO'') then'+ #13 +
    #13 +
    '    for i in iITENS_IDS.first..iITENS_IDS.last loop '+ #13 +
    '      if iRETORNADOS(i) = 0 then'+ #13 +
    '        continue;'+ #13 +
    '      end if;'+ #13 +
    #13 +
    '      update ENTREGAS_ITENS set'+ #13 +
    '        RETORNADOS = iRETORNADOS(i)'+ #13 +
    '      where ENTREGA_ID = iENTREGA_ID'+ #13 +
    '      and LOTE = iLOTES(i)'+ #13 +
    '      and ITEM_ID = iITENS_IDS(i);'+ #13 +
    #13 +
    '      VERIFICAR_PENDENCIA_ENTREGA(vEmpresaId, vLocalId, vOrcamentoId, vPrevisaoEntrega, iSTATUS);'+ #13 +
    #13 +
    '      INSERIR_ENTREGAS_ITENS_PEND('+ #13 +
    '        vEmpresaId,'+ #13 +
    '        vLocalId,'+ #13 +
    '        vOrcamentoId,'+ #13 +
    '        vPrevisaoEntrega,'+ #13 +
    '        iPRODUTOS_IDS(i),'+ #13 +
    '        iITENS_IDS(i),'+ #13 +
    '        iLOTES(i),'+ #13 +
    '        iRETORNADOS(i)'+ #13 +
    '      );      '+ #13 +
    '    end loop;'+ #13 +
    #13 +
    '  end if;'+ #13 +
    #13 +
    '  GERAR_NOTA_RETORNO_ENTREGA(iENTREGA_ID);'+ #13 +
    #13 +
    'end BAIXAR_ENTREGA;'+ #13
end;

function AjustarTriggerORCAMENTOS_U_AR: string;
begin
  Result :=
    'create or replace trigger ORCAMENTOS_U_AR'+ #13 +
    'after update '+ #13 +
    'on ORCAMENTOS'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    #13 +
    '  procedure INSERIR_LOG(pTIPO_ALTERACAO_LOG_ID in number, pVALOR_ANTERIOR in string, pNOVO_VALOR in string)'+ #13 +
    '  is begin'+ #13 +
    '    if nvl(pVALOR_ANTERIOR, ''X'') <> nvl(pNOVO_VALOR, ''X'') then'+ #13 +
    '      insert into LOGS_ORCAMENTOS(TIPO_ALTERACAO_LOG_ID, ORCAMENTO_ID, VALOR_ANTERIOR, NOVO_VALOR)'+ #13 +
    '      values(pTIPO_ALTERACAO_LOG_ID, :new.ORCAMENTO_ID, pVALOR_ANTERIOR, pNOVO_VALOR);'+ #13 +
    '    end if;'+ #13 +
    '  end;'+ #13 +
    #13 +
    'begin'+ #13 +
    '  INSERIR_LOG(1, :old.EMPRESA_ID, :new.EMPRESA_ID);'+ #13 +
    '  INSERIR_LOG(2, :old.CLIENTE_ID, :new.CLIENTE_ID);'+ #13 +
    '  INSERIR_LOG(3, :old.VENDEDOR_ID, :new.VENDEDOR_ID);'+ #13 +
    '  INSERIR_LOG(4, :old.CONDICAO_ID, :new.CONDICAO_ID);'+ #13 +
    '  INSERIR_LOG(5, :old.STATUS, :new.STATUS);'+ #13 +
    '  INSERIR_LOG(6, :old.INDICE_OVER_PRICE, :new.INDICE_OVER_PRICE);'+ #13 +
    '  INSERIR_LOG(7, :old.VALOR_TOTAL_PRODUTOS, :new.VALOR_TOTAL_PRODUTOS);'+ #13 +
    '  INSERIR_LOG(8, :old.VALOR_TOTAL_PRODUTOS_PROMOCAO, :new.VALOR_TOTAL_PRODUTOS_PROMOCAO);'+ #13 +
    '  INSERIR_LOG(9, :old.VALOR_TOTAL, :new.VALOR_TOTAL);'+ #13 +
    '  INSERIR_LOG(10, :old.VALOR_OUTRAS_DESPESAS, :new.VALOR_OUTRAS_DESPESAS);'+ #13 +
    '  INSERIR_LOG(11, :old.VALOR_DESCONTO, :new.VALOR_DESCONTO);'+ #13 +
    '  INSERIR_LOG(12, :old.TURNO_ID, :new.TURNO_ID);'+ #13 +
    '  INSERIR_LOG(13, :old.ORIGEM_VENDA, :new.ORIGEM_VENDA);'+ #13 +
    '  INSERIR_LOG(14, :old.TIPO_ENTREGA, :new.TIPO_ENTREGA);'+ #13 +
    '  INSERIR_LOG(15, :old.INDICE_CONDICAO_PAGAMENTO, :new.INDICE_CONDICAO_PAGAMENTO);'+ #13 +
    '  INSERIR_LOG(16, :old.TELEFONE_CONSUMIDOR_FINAL, :new.TELEFONE_CONSUMIDOR_FINAL);'+ #13 +
    '  INSERIR_LOG(17, :old.DATA_HORA_RECEBIMENTO, :new.DATA_HORA_RECEBIMENTO);'+ #13 +
    '  INSERIR_LOG(18, :old.VALOR_DINHEIRO, :new.VALOR_DINHEIRO);'+ #13 +
    '  INSERIR_LOG(19, :old.VALOR_CHEQUE, :new.VALOR_CHEQUE);'+ #13 +
    '  INSERIR_LOG(20, :old.VALOR_CARTAO_DEBITO, :new.VALOR_CARTAO_DEBITO);'+ #13 +
    '  INSERIR_LOG(21, :old.VALOR_CARTAO_CREDITO, :new.VALOR_CARTAO_CREDITO);'+ #13 +
    '  INSERIR_LOG(22, :old.VALOR_COBRANCA, :new.VALOR_COBRANCA);'+ #13 +
    '  INSERIR_LOG(23, :old.VALOR_ACUMULATIVO, :new.VALOR_ACUMULATIVO);'+ #13 +
    '  INSERIR_LOG(24, :old.VALOR_FINANCEIRA, :new.VALOR_FINANCEIRA);'+ #13 +
    '  INSERIR_LOG(25, :old.VALOR_CREDITO, :new.VALOR_CREDITO);'+ #13 +
    '  INSERIR_LOG(26, :old.VALOR_TROCO, :new.VALOR_TROCO);'+ #13 +
    '  INSERIR_LOG(27, :old.VALOR_FRETE, :new.VALOR_FRETE);'+ #13 +
    '  INSERIR_LOG(28, :old.DATA_ENTREGA, :new.DATA_ENTREGA);'+ #13 +
    '  INSERIR_LOG(29, :old.HORA_ENTREGA, :new.HORA_ENTREGA);'+ #13 +
    '  INSERIR_LOG(30, :old.LOGRADOURO, :new.LOGRADOURO);'+ #13 +
    '  INSERIR_LOG(31, :old.COMPLEMENTO, :new.COMPLEMENTO);'+ #13 +
    '  INSERIR_LOG(32, :old.NUMERO, :new.NUMERO);'+ #13 +
    '  INSERIR_LOG(33, :old.PONTO_REFERENCIA, :new.PONTO_REFERENCIA);'+ #13 +
    '  INSERIR_LOG(34, :old.BAIRRO_ID, :new.BAIRRO_ID);'+ #13 +
    '  INSERIR_LOG(35, :old.CEP, :new.CEP);'+ #13 +
    '  INSERIR_LOG(36, :old.INSCRICAO_ESTADUAL, :new.INSCRICAO_ESTADUAL);'+ #13 +
    #13 +
    'end ORCAMENTOS_U_AR;'+ #13
end;

function AjustarViewVW_TIPOS_ALTER_LOGS_ORCAMENTOS: string;
begin
  Result :=
    'create or replace view VW_TIPOS_ALTER_LOGS_ORCAMENTOS'+ #13 +
    'as'+ #13 +
    'select 1 as TIPO_ALTERACAO_LOG_ID, ''Empresa'' as CAMPO from dual union'+ #13 +
    'select 2 as TIPO_ALTERACAO_LOG_ID, ''Cliente'' as CAMPO from dual union'+ #13 +
    'select 3 as TIPO_ALTERACAO_LOG_ID, ''Vendedor'' as CAMPO from dual union'+ #13 +
    'select 4 as TIPO_ALTERACAO_LOG_ID, ''Condi��o de pagto'' as CAMPO from dual union'+ #13 +
    'select 5 as TIPO_ALTERACAO_LOG_ID, ''Status'' as CAMPO from dual union'+ #13 +
    'select 6 as TIPO_ALTERACAO_LOG_ID, ''Indice overprice'' as CAMPO from dual union'+ #13 +
    'select 7 as TIPO_ALTERACAO_LOG_ID, ''Valor total prod.'' as CAMPO from dual union'+ #13 +
    'select 8 as TIPO_ALTERACAO_LOG_ID, ''Valor total prod.prom.'' as CAMPO from dual union'+ #13 +
    'select 9 as TIPO_ALTERACAO_LOG_ID, ''Valor total'' as CAMPO from dual union'+ #13 +
    'select 10 as TIPO_ALTERACAO_LOG_ID, ''Valor out.despesas'' as CAMPO from dual union'+ #13 +
    'select 11 as TIPO_ALTERACAO_LOG_ID, ''Valor desconto'' as CAMPO from dual union'+ #13 +
    'select 12 as TIPO_ALTERACAO_LOG_ID, ''Turno'' as CAMPO from dual union'+ #13 +
    'select 13 as TIPO_ALTERACAO_LOG_ID, ''Origem'' as CAMPO from dual union'+ #13 +
    'select 14 as TIPO_ALTERACAO_LOG_ID, ''Tipo de entrega'' as CAMPO from dual union'+ #13 +
    'select 15 as TIPO_ALTERACAO_LOG_ID, ''Indice cond.pagto'' as CAMPO from dual union'+ #13 +
    'select 16 as TIPO_ALTERACAO_LOG_ID, ''Tel.cons.final'' as CAMPO from dual union'+ #13 +
    'select 17 as TIPO_ALTERACAO_LOG_ID, ''Data\hora receb.'' as CAMPO from dual union'+ #13 +
    'select 18 as TIPO_ALTERACAO_LOG_ID, ''Valor dinheiro'' as CAMPO from dual union'+ #13 +
    'select 19 as TIPO_ALTERACAO_LOG_ID, ''Valor cheque'' as CAMPO from dual union'+ #13 +
    'select 20 as TIPO_ALTERACAO_LOG_ID, ''Valor cart.d�bito'' as CAMPO from dual union'+ #13 +
    'select 21 as TIPO_ALTERACAO_LOG_ID, ''Valor cart.cr�dito'' as CAMPO from dual union'+ #13 +
    'select 22 as TIPO_ALTERACAO_LOG_ID, ''Valor cobran�a'' as CAMPO from dual union'+ #13 +
    'select 23 as TIPO_ALTERACAO_LOG_ID, ''Valor acumulado'' as CAMPO from dual union'+ #13 +
    'select 24 as TIPO_ALTERACAO_LOG_ID, ''Valor financeira'' as CAMPO from dual union'+ #13 +
    'select 25 as TIPO_ALTERACAO_LOG_ID, ''Valor cr�dito'' as CAMPO from dual union'+ #13 +
    'select 26 as TIPO_ALTERACAO_LOG_ID, ''Valor troco'' as CAMPO from dual union'+ #13 +
    'select 27 as TIPO_ALTERACAO_LOG_ID, ''Valor frete'' as CAMPO from dual union'+ #13 +
    'select 28 as TIPO_ALTERACAO_LOG_ID, ''Data entrega'' as CAMPO from dual union'+ #13 +
    'select 29 as TIPO_ALTERACAO_LOG_ID, ''Hora entrega'' as CAMPO from dual union'+ #13 +
    'select 30 as TIPO_ALTERACAO_LOG_ID, ''Logradouro'' as CAMPO from dual union'+ #13 +
    'select 31 as TIPO_ALTERACAO_LOG_ID, ''Complemento'' as CAMPO from dual union'+ #13 +
    'select 32 as TIPO_ALTERACAO_LOG_ID, ''Numero'' as CAMPO from dual union'+ #13 +
    'select 33 as TIPO_ALTERACAO_LOG_ID, ''Ponto refer�ncia'' as CAMPO from dual union'+ #13 +
    'select 34 as TIPO_ALTERACAO_LOG_ID, ''Bairro'' as CAMPO from dual union'+ #13 +
    'select 35 as TIPO_ALTERACAO_LOG_ID, ''CEP'' as CAMPO from dual union'+ #13 +
    'select 36 as TIPO_ALTERACAO_LOG_ID, ''Inscri��o estadual'' as CAMPO from dual;'+ #13
end;

function DroparColunaECF_ID: string;
begin
  Result :=
    'alter table NOTAS_FISCAIS '+ #13 +
    'drop column ECF_ID;'+ #13
end;

function AjustarPackageRecordsNotasFiscais: string;
begin
  Result :=
    'create or replace package RecordsNotasFiscais'+ #13 +
    'as'+ #13 +
    #13 +
    '  type RecNotaFiscal is record('+ #13 +
    '    -- Dados do or�amento ---------------------------------------------------'+ #13 +
    '    CADASTRO_ID                 NOTAS_FISCAIS.CADASTRO_ID%type,'+ #13 +
    '    CFOP_ID                     NOTAS_FISCAIS.CFOP_ID%type,'+ #13 +
    '    EMPRESA_ID                  NOTAS_FISCAIS.EMPRESA_ID%type,'+ #13 +
    '    ORCAMENTO_ID                NOTAS_FISCAIS.ORCAMENTO_ID%type,'+ #13 +
    '    ACUMULADO_ID                NOTAS_FISCAIS.ACUMULADO_ID%type,'+ #13 +
    '    DEVOLUCAO_ID                NOTAS_FISCAIS.DEVOLUCAO_ID%type,'+ #13 +
    '    NUMERO_NOTA                 NOTAS_FISCAIS.NUMERO_NOTA%type,'+ #13 +
    '    /* Dados da empresa emitente da NF */'+ #13 +
    '    RAZAO_SOCIAL_EMITENTE       NOTAS_FISCAIS.RAZAO_SOCIAL_EMITENTE%type,'+ #13 +
    '    NOME_FANTASIA_EMITENTE      NOTAS_FISCAIS.NOME_FANTASIA_EMITENTE%type,    '+ #13 +
    '    CNPJ_EMITENTE               NOTAS_FISCAIS.CNPJ_EMITENTE%type,'+ #13 +
    '    INSCRICAO_ESTADUAL_EMITENTE NOTAS_FISCAIS.INSCRICAO_ESTADUAL_EMITENTE%type,'+ #13 +
    '    LOGRADOURO_EMITENTE         NOTAS_FISCAIS.LOGRADOURO_EMITENTE%type,'+ #13 +
    '    COMPLEMENTO_EMITENTE        NOTAS_FISCAIS.COMPLEMENTO_EMITENTE%type,'+ #13 +
    '    NOME_BAIRRO_EMITENTE        NOTAS_FISCAIS.NOME_BAIRRO_EMITENTE%type,'+ #13 +
    '    NOME_CIDADE_EMITENTE        NOTAS_FISCAIS.NOME_CIDADE_EMITENTE%type,'+ #13 +
    '    NUMERO_EMITENTE             NOTAS_FISCAIS.NUMERO_EMITENTE%type,'+ #13 +
    '    ESTADO_ID_EMITENTE          NOTAS_FISCAIS.ESTADO_ID_EMITENTE%type,'+ #13 +
    '    CEP_EMITENTE                NOTAS_FISCAIS.CEP_EMITENTE%type,'+ #13 +
    '    TELEFONE_EMITENTE           NOTAS_FISCAIS.TELEFONE_EMITENTE%type,'+ #13 +
    '    CODIGO_IBGE_MUNICIPIO_EMIT  NOTAS_FISCAIS.CODIGO_IBGE_MUNICIPIO_EMIT%type,'+ #13 +
    '    CODIGO_IBGE_ESTADO_EMITENT  NOTAS_FISCAIS.CODIGO_IBGE_ESTADO_EMITENT%type,    '+ #13 +
    '    /* Dados do cliente */'+ #13 +
    '    NOME_FANTASIA_DESTINATARIO  NOTAS_FISCAIS.NOME_FANTASIA_DESTINATARIO%type,'+ #13 +
    '    RAZAO_SOCIAL_DESTINATARIO   NOTAS_FISCAIS.RAZAO_SOCIAL_DESTINATARIO%type,'+ #13 +
    '    TIPO_PESSOA_DESTINATARIO    NOTAS_FISCAIS.TIPO_PESSOA_DESTINATARIO%type,'+ #13 +
    '    CPF_CNPJ_DESTINATARIO       NOTAS_FISCAIS.CPF_CNPJ_DESTINATARIO%type,'+ #13 +
    '    INSCRICAO_ESTADUAL_DESTINAT NOTAS_FISCAIS.INSCRICAO_ESTADUAL_DESTINAT%type,'+ #13 +
    '    LOGRADOURO_DESTINATARIO     NOTAS_FISCAIS.LOGRADOURO_DESTINATARIO%type,'+ #13 +
    '    COMPLEMENTO_DESTINATARIO    NOTAS_FISCAIS.COMPLEMENTO_DESTINATARIO%type,'+ #13 +
    '    NOME_BAIRRO_DESTINATARIO    NOTAS_FISCAIS.NOME_BAIRRO_DESTINATARIO%type,'+ #13 +
    '    NOME_CIDADE_DESTINATARIO    NOTAS_FISCAIS.NOME_CIDADE_DESTINATARIO%type,'+ #13 +
    '    NUMERO_DESTINATARIO         NOTAS_FISCAIS.NUMERO_DESTINATARIO%type,'+ #13 +
    '    ESTADO_ID_DESTINATARIO      NOTAS_FISCAIS.ESTADO_ID_DESTINATARIO%type,'+ #13 +
    '    CEP_DESTINATARIO            NOTAS_FISCAIS.CEP_DESTINATARIO%type,'+ #13 +
    '    CODIGO_IBGE_MUNICIPIO_DEST  NOTAS_FISCAIS.CODIGO_IBGE_MUNICIPIO_DEST%type,'+ #13 +
    '    TIPO_CLIENTE                CLIENTES.TIPO_CLIENTE%type,'+ #13 +
    #13 +
    '    NOME_CONSUMIDOR_FINAL       NOTAS_FISCAIS.NOME_CONSUMIDOR_FINAL%type,'+ #13 +
    '    TELEFONE_CONSUMIDOR_FINAL   NOTAS_FISCAIS.TELEFONE_CONSUMIDOR_FINAL%type,'+ #13 +
    '    NATUREZA_OPERACAO           NOTAS_FISCAIS.NATUREZA_OPERACAO%type,'+ #13 +
    #13 +
    '    /* Valores da Nota fiscal */'+ #13 +
    '    TOTAL_IMPOSTOS_ESTADUAL     number(8,2) default 0,'+ #13 +
    '    TOTAL_IMPOSTOS_FEDERAL      number(8,2) default 0,'+ #13 +
    '    VALOR_TOTAL                 NOTAS_FISCAIS.VALOR_TOTAL%type default 0,'+ #13 +
    '    VALOR_TOTAL_PRODUTOS        NOTAS_FISCAIS.VALOR_TOTAL_PRODUTOS%type default 0,'+ #13 +
    '    VALOR_DESCONTO              NOTAS_FISCAIS.VALOR_DESCONTO%type default 0,'+ #13 +
    '    VALOR_OUTRAS_DESPESAS       NOTAS_FISCAIS.VALOR_OUTRAS_DESPESAS%type default 0,'+ #13 +
    '    VALOR_FRETE                 NOTAS_FISCAIS.VALOR_FRETE%type default 0,'+ #13 +
    '    VALOR_SEGURO                NOTAS_FISCAIS.VALOR_SEGURO%type default 0,'+ #13 +
    '    BASE_CALCULO_ICMS           NOTAS_FISCAIS.BASE_CALCULO_ICMS%type default 0,'+ #13 +
    '    VALOR_ICMS                  NOTAS_FISCAIS.VALOR_ICMS%type default 0,'+ #13 +
    '    BASE_CALCULO_ICMS_ST        NOTAS_FISCAIS.BASE_CALCULO_ICMS_ST%type default 0,'+ #13 +
    '    VALOR_ICMS_ST               NOTAS_FISCAIS.VALOR_ICMS_ST%type default 0,'+ #13 +
    '    BASE_CALCULO_PIS            NOTAS_FISCAIS.BASE_CALCULO_PIS%type default 0,'+ #13 +
    '    VALOR_PIS                   NOTAS_FISCAIS.VALOR_PIS%type default 0,'+ #13 +
    '    BASE_CALCULO_COFINS         NOTAS_FISCAIS.BASE_CALCULO_COFINS%type default 0,'+ #13 +
    '    VALOR_COFINS                NOTAS_FISCAIS.VALOR_COFINS%type default 0,'+ #13 +
    '    VALOR_IPI                   NOTAS_FISCAIS.VALOR_IPI%type default 0,'+ #13 +
    #13 +
    '    PESO_LIQUIDO                NOTAS_FISCAIS.PESO_LIQUIDO%type default 0,'+ #13 +
    '    PESO_BRUTO                  NOTAS_FISCAIS.PESO_BRUTO%type default 0,'+ #13 +
    #13 +
    '    VALOR_RECEBIDO_DINHEIRO     NOTAS_FISCAIS.VALOR_RECEBIDO_DINHEIRO%type default 0,'+ #13 +
    '    VALOR_RECEBIDO_CARTAO_CRED  NOTAS_FISCAIS.VALOR_RECEBIDO_CARTAO_CRED%type default 0,'+ #13 +
    '    VALOR_RECEBIDO_CARTAO_DEB   NOTAS_FISCAIS.VALOR_RECEBIDO_CARTAO_DEB%type default 0,'+ #13 +
    '    VALOR_RECEBIDO_CREDITO      NOTAS_FISCAIS.VALOR_RECEBIDO_CREDITO%type default 0,'+ #13 +
    '    VALOR_RECEBIDO_COBRANCA     NOTAS_FISCAIS.VALOR_RECEBIDO_COBRANCA%type default 0,'+ #13 +
    '    VALOR_RECEBIDO_CHEQUE       NOTAS_FISCAIS.VALOR_RECEBIDO_CHEQUE%type default 0,'+ #13 +
    '    VALOR_RECEBIDO_FINANCEIRA   NOTAS_FISCAIS.VALOR_RECEBIDO_FINANCEIRA%type default 0,'+ #13 +
    #13 +
    '    INFORMACOES_COMPLEMENTARES  NOTAS_FISCAIS.INFORMACOES_COMPLEMENTARES%type'+ #13 +
    '  );  '+ #13 +
    #13 +
    '  type RecNotasFiscaisItens is record('+ #13 +
    '    PRODUTO_ID                  PRODUTOS.PRODUTO_ID%type,'+ #13 +
    '    ITEM_ID                     NOTAS_FISCAIS_ITENS.ITEM_ID%type,'+ #13 +
    '    NOME_PRODUTO                PRODUTOS.NOME%type,'+ #13 +
    '    CST                         NOTAS_FISCAIS_ITENS.CST%type,'+ #13 +
    '    UNIDADE                     NOTAS_FISCAIS_ITENS.UNIDADE%type,'+ #13 +
    '    CFOP_ID                     NOTAS_FISCAIS_ITENS.CFOP_ID%type,'+ #13 +
    '    CODIGO_NCM                  NOTAS_FISCAIS_ITENS.CODIGO_NCM%type,'+ #13 +
    '    VALOR_TOTAL                 NOTAS_FISCAIS_ITENS.VALOR_TOTAL%type,'+ #13 +
    '    PRECO_UNITARIO              NOTAS_FISCAIS_ITENS.PRECO_UNITARIO%type,'+ #13 +
    '    QUANTIDADE                  NOTAS_FISCAIS_ITENS.QUANTIDADE%type,'+ #13 +
    '    VALOR_TOTAL_DESCONTO        NOTAS_FISCAIS_ITENS.VALOR_TOTAL_DESCONTO%type,'+ #13 +
    '    VALOR_TOTAL_OUTRAS_DESPESAS NOTAS_FISCAIS_ITENS.VALOR_TOTAL_OUTRAS_DESPESAS%type,'+ #13 +
    '    VALOR_TOTAL_FRETE           ORCAMENTOS.VALOR_FRETE%type,'+ #13 +
    '    BASE_CALCULO_ICMS           NOTAS_FISCAIS_ITENS.BASE_CALCULO_ICMS%type,'+ #13 +
    '    INDICE_REDUCAO_BASE_ICMS    NOTAS_FISCAIS_ITENS.INDICE_REDUCAO_BASE_ICMS%type,'+ #13 +
    '    PERCENTUAL_ICMS             NOTAS_FISCAIS_ITENS.PERCENTUAL_ICMS%type,'+ #13 +
    '    VALOR_ICMS                  NOTAS_FISCAIS_ITENS.VALOR_ICMS%type,'+ #13 +
    '    INDICE_REDUCAO_BASE_ICMS_ST NOTAS_FISCAIS_ITENS.INDICE_REDUCAO_BASE_ICMS_ST%type,'+ #13 +
    '    BASE_CALCULO_ICMS_ST        NOTAS_FISCAIS_ITENS.BASE_CALCULO_ICMS_ST%type,'+ #13 +
    '    PERCENTUAL_ICMS_ST          NOTAS_FISCAIS_ITENS.PERCENTUAL_ICMS_ST%type,'+ #13 +
    '    VALOR_ICMS_ST               NOTAS_FISCAIS_ITENS.VALOR_ICMS_ST%type,'+ #13 +
    '    CST_PIS                     NOTAS_FISCAIS_ITENS.CST_PIS%type,'+ #13 +
    '    CST_COFINS                  NOTAS_FISCAIS_ITENS.CST_COFINS%type,'+ #13 +
    '    BASE_CALCULO_PIS            NOTAS_FISCAIS_ITENS.BASE_CALCULO_PIS%type,'+ #13 +
    '    PERCENTUAL_PIS              NOTAS_FISCAIS_ITENS.PERCENTUAL_PIS%type,'+ #13 +
    '    VALOR_PIS                   NOTAS_FISCAIS_ITENS.VALOR_PIS%type,'+ #13 +
    '    IVA                         NOTAS_FISCAIS_ITENS.IVA%type,'+ #13 +
    '    BASE_CALCULO_COFINS         NOTAS_FISCAIS_ITENS.BASE_CALCULO_COFINS%type,'+ #13 +
    '    PERCENTUAL_COFINS           NOTAS_FISCAIS_ITENS.PERCENTUAL_COFINS%type,'+ #13 +
    '    VALOR_COFINS                NOTAS_FISCAIS_ITENS.VALOR_COFINS%type,'+ #13 +
    '    VALOR_IPI                   NOTAS_FISCAIS_ITENS.VALOR_IPI%type,'+ #13 +
    '    PERCENTUAL_IPI              NOTAS_FISCAIS_ITENS.PERCENTUAL_IPI%type,'+ #13 +
    '    PRECO_PAUTA                 NOTAS_FISCAIS_ITENS.PRECO_PAUTA%type,'+ #13 +
    '    CODIGO_BARRAS               NOTAS_FISCAIS_ITENS.CODIGO_BARRAS%type,'+ #13 +
    '		NATUREZA_OPERACAO           NOTAS_FISCAIS.NATUREZA_OPERACAO%type,'+ #13 +
    '    CEST                        NOTAS_FISCAIS_ITENS.CEST%type'+ #13 +
    '  );  '+ #13 +
    #13 +
    '  /* --------- Declara��o dos dos tipos acima na forma de Arrays ------------- */  '+ #13 +
    #13 +
    '  type ArrayOfRecNotasFiscaisItens is table of RecNotasFiscaisItens index by naturaln;'+ #13 +
    #13 +
    '  type RecImpostosCalculados is record('+ #13 +
    '    BASE_CALCULO_ICMS           NOTAS_FISCAIS_ITENS.BASE_CALCULO_ICMS%type,'+ #13 +
    '    INDICE_REDUCAO_BASE_ICMS    NOTAS_FISCAIS_ITENS.INDICE_REDUCAO_BASE_ICMS%type,'+ #13 +
    '    PERCENTUAL_ICMS             NOTAS_FISCAIS_ITENS.PERCENTUAL_ICMS%type,'+ #13 +
    '    VALOR_ICMS                  NOTAS_FISCAIS_ITENS.VALOR_ICMS%type,'+ #13 +
    '    INDICE_REDUCAO_BASE_ICMS_ST NOTAS_FISCAIS_ITENS.INDICE_REDUCAO_BASE_ICMS_ST%type,'+ #13 +
    '    BASE_CALCULO_ICMS_ST        NOTAS_FISCAIS_ITENS.BASE_CALCULO_ICMS_ST%type,'+ #13 +
    '    PERCENTUAL_ICMS_ST          NOTAS_FISCAIS_ITENS.PERCENTUAL_ICMS_ST%type,'+ #13 +
    '    VALOR_ICMS_ST               NOTAS_FISCAIS_ITENS.VALOR_ICMS_ST%type,'+ #13 +
    '    BASE_CALCULO_PIS            NOTAS_FISCAIS_ITENS.BASE_CALCULO_PIS%type,'+ #13 +
    '    PERCENTUAL_PIS              NOTAS_FISCAIS_ITENS.PERCENTUAL_PIS%type,'+ #13 +
    '    VALOR_PIS                   NOTAS_FISCAIS_ITENS.VALOR_PIS%type,'+ #13 +
    '    IVA                         NOTAS_FISCAIS_ITENS.IVA%type,'+ #13 +
    '    BASE_CALCULO_COFINS         NOTAS_FISCAIS_ITENS.BASE_CALCULO_COFINS%type,'+ #13 +
    '    PERCENTUAL_COFINS           NOTAS_FISCAIS_ITENS.PERCENTUAL_COFINS%type,'+ #13 +
    '    VALOR_COFINS                NOTAS_FISCAIS_ITENS.VALOR_COFINS%type,'+ #13 +
    '    VALOR_IPI                   NOTAS_FISCAIS_ITENS.VALOR_IPI%type,'+ #13 +
    '    PERCENTUAL_IPI              NOTAS_FISCAIS_ITENS.PERCENTUAL_IPI%type,'+ #13 +
    '    PRECO_PAUTA                 NOTAS_FISCAIS_ITENS.PRECO_PAUTA%type'+ #13 +
    '  );'+ #13 +
    #13 +
    '  type RecDescontoConcedido is record('+ #13 +
    '    IndiceDescontoVendaId   INDICES_DESCONTOS_VENDA.INDICE_ID%type,'+ #13 +
    '    PercentualDesconto      INDICES_DESCONTOS_VENDA.PERCENTUAL_DESCONTO%type,'+ #13 +
    '    PrecoCusto              INDICES_DESCONTOS_VENDA.PRECO_CUSTO%type,'+ #13 +
    '    TipoCusto               INDICES_DESCONTOS_VENDA.TIPO_CUSTO%type,'+ #13 +
    '    TipoDescontoPrecoCusto  INDICES_DESCONTOS_VENDA.TIPO_DESCONTO_PRECO_CUSTO%type,'+ #13 +
    '    ValorTotalCusto         number(8,2) default 0'+ #13 +
    '  );'+ #13 +
    #13 +
    'end RecordsNotasFiscais;'+ #13
end;

function DroparProcedureINSERIR_NOTA_FISCAL: string;
begin
  Result :=
    'drop procedure INSERIR_NOTA_FISCAL;'+ #13
end;

function SubstituirTriggerProdutos: string;
begin
  Result :=
    'CREATE or replace TRIGGER PRODUTOS_U_AR'+ #13 +
    ' AFTER UPDATE'+ #13 +
    ' on PRODUTOS'+ #13 +
    ' for each row'+ #13 +
    ' declare'+ #13 +
    #13 +
    '  procedure INSERIR_LOG(pTIPO_ALTERACAO_LOG_ID in number, pVALOR_ANTERIOR in string, pNOVO_VALOR in string)'+ #13 +
    '  is begin'+ #13 +
    '    if nvl(pVALOR_ANTERIOR, ''X'') <> nvl(pNOVO_VALOR, ''X'') then'+ #13 +
    '      insert into LOGS_PRODUTOS(TIPO_ALTERACAO_LOG_ID, PRODUTO_ID, VALOR_ANTERIOR, NOVO_VALOR)'+ #13 +
    '      values(pTIPO_ALTERACAO_LOG_ID, :new.PRODUTO_ID, pVALOR_ANTERIOR, pNOVO_VALOR);'+ #13 +
    '    end if;'+ #13 +
    '  end;'+ #13 +
    #13 +
    'begin'+ #13 +
    '  /* View VW_TIPOS_ALTER_LOGS_PRODUTOS */'+ #13 +
    #13 +
    '  INSERIR_LOG(1, :old.DATA_CADASTRO, :new.DATA_CADASTRO);'+ #13 +
    '  INSERIR_LOG(2, :old.NOME, :new.NOME);'+ #13 +
    '  INSERIR_LOG(3, :old.NOME_COMPRA, :new.NOME_COMPRA);'+ #13 +
    '  INSERIR_LOG(4, :old.FORNECEDOR_ID, :new.FORNECEDOR_ID);'+ #13 +
    '  INSERIR_LOG(5, :old.UNIDADE_VENDA, :new.UNIDADE_VENDA);'+ #13 +
    '  INSERIR_LOG(6, :old.MARCA_ID, :new.MARCA_ID);'+ #13 +
    '  INSERIR_LOG(7, :old.MULTIPLO_VENDA, :new.MULTIPLO_VENDA);'+ #13 +
    '  INSERIR_LOG(8, :old.PESO, :new.PESO);'+ #13 +
    '  INSERIR_LOG(9, :old.VOLUME, :new.VOLUME);'+ #13 +
    '  INSERIR_LOG(10, :old.LINHA_PRODUTO_ID, :new.LINHA_PRODUTO_ID);'+ #13 +
    '  INSERIR_LOG(11, :old.CODIGO_BARRAS, :new.CODIGO_BARRAS);'+ #13 +
    '  INSERIR_LOG(12, :old.ACEITAR_ESTOQUE_NEGATIVO, :new.ACEITAR_ESTOQUE_NEGATIVO);'+ #13 +
    '  INSERIR_LOG(13, :old.INATIVAR_ZERAR_ESTOQUE, :new.INATIVAR_ZERAR_ESTOQUE);'+ #13 +
    '  INSERIR_LOG(14, :old.BLOQUEADO_COMPRAS, :new.BLOQUEADO_COMPRAS);'+ #13 +
    '  INSERIR_LOG(15, :old.BLOQUEADO_VENDAS, :new.BLOQUEADO_VENDAS);'+ #13 +
    '  INSERIR_LOG(16, :old.PERMITIR_DEVOLUCAO, :new.PERMITIR_DEVOLUCAO);'+ #13 +
    '  INSERIR_LOG(17, :old.CARACTERISTICAS, :new.CARACTERISTICAS);'+ #13 +
    '  INSERIR_LOG(18, :old.ATIVO, :new.ATIVO);'+ #13 +
    '  INSERIR_LOG(19, :old.MOTIVO_INATIVIDADE, :new.MOTIVO_INATIVIDADE);'+ #13 +
    '  INSERIR_LOG(20, :old.CEST, :new.CEST);'+ #13 +
    '  INSERIR_LOG(21, :old.CODIGO_NCM, :new.CODIGO_NCM);'+ #13 +
    '  INSERIR_LOG(22, :old.PRODUTO_DIVERSOS_PDV, :new.PRODUTO_DIVERSOS_PDV);'+ #13 +
    '  INSERIR_LOG(23, :old.CODIGO_BALANCA, :new.CODIGO_BALANCA);'+ #13 +
    '  INSERIR_LOG(24, :old.PRODUTO_PAI_ID, :new.PRODUTO_PAI_ID);'+ #13 +
    '  INSERIR_LOG(25, :old.QUANTIDADE_VEZES_PAI, :new.QUANTIDADE_VEZES_PAI);'+ #13 +
    '  INSERIR_LOG(26, :old.TIPO_CONTROLE_ESTOQUE, :new.TIPO_CONTROLE_ESTOQUE);'+ #13 +
    '  INSERIR_LOG(27, :old.EXIGIR_DATA_FABRICACAO_LOTE, :new.EXIGIR_DATA_FABRICACAO_LOTE);'+ #13 +
    '  INSERIR_LOG(28, :old.EXIGIR_DATA_VENCIMENTO_LOTE, :new.EXIGIR_DATA_VENCIMENTO_LOTE);'+ #13 +
    '  INSERIR_LOG(29, :old.SOB_ENCOMENDA, :new.SOB_ENCOMENDA);'+ #13 +
    '  INSERIR_LOG(30, :old.DIAS_AVISO_VENCIMENTO, :new.DIAS_AVISO_VENCIMENTO);'+ #13 +
    '  INSERIR_LOG(31, :old.REVENDA, :new.REVENDA);'+ #13 +
    '  INSERIR_LOG(32, :old.USO_CONSUMO, :new.USO_CONSUMO);'+ #13 +
    '  INSERIR_LOG(33, :old.SUJEITO_COMISSAO, :new.SUJEITO_COMISSAO);'+ #13 +
    '  INSERIR_LOG(34, :old.BLOQUEAR_ENTRAD_SEM_PED_COMPRA, :new.BLOQUEAR_ENTRAD_SEM_PED_COMPRA);'+ #13 +
    '  INSERIR_LOG(35, :old.EXIGIR_MODELO_NOTA_FISCAL, :new.EXIGIR_MODELO_NOTA_FISCAL);'+ #13 +
    '  INSERIR_LOG(36, :old.EXIGIR_SEPARACAO, :new.EXIGIR_SEPARACAO);'+ #13 +
    '  INSERIR_LOG(37, :old.VALOR_ADICIONAL_FRETE, :new.VALOR_ADICIONAL_FRETE);'+ #13 +
    '  INSERIR_LOG(38, :old.PERCENTUAL_COMISSAO_VISTA, :new.PERCENTUAL_COMISSAO_VISTA);'+ #13 +
    '  INSERIR_LOG(39, :old.PERCENTUAL_COMISSAO_PRAZO, :new.PERCENTUAL_COMISSAO_PRAZO);'+ #13 +
    '  INSERIR_LOG(40, :old.CODIGO_ORIGINAL_FABRICANTE, :new.CODIGO_ORIGINAL_FABRICANTE);'+ #13 +
    '  INSERIR_LOG(41, :old.TIPO_DEF_AUTOMATICA_LOTE, :new.TIPO_DEF_AUTOMATICA_LOTE);'+ #13 +
    '  INSERIR_LOG(42, :old.UNIDADE_ENTREGA_ID, :new.UNIDADE_ENTREGA_ID);'+ #13 +
    '  INSERIR_LOG(43, :old.CONF_SOMENTE_CODIGO_BARRAS, :new.CONF_SOMENTE_CODIGO_BARRAS);'+ #13 +
    '  INSERIR_LOG(44, :old.PAGAR_COMISSAO_PROFISSIONAL, :new.PAGAR_COMISSAO_PROFISSIONAL);'+ #13 +
    '  INSERIR_LOG(45, :old.SERVICO, :new.SERVICO);'+ #13 +
    '  INSERIR_LOG(46, :old.PERC_FRETE_ADICIONAL_VENDA, :new.PERC_FRETE_ADICIONAL_VENDA);'+ #13 +
    '  INSERIR_LOG(47, :old.PERC_IPI, :new.PERC_IPI);'+ #13 +
    '  INSERIR_LOG(48, :old.MENSAGEM_NOTA_FISCAL, :new.MENSAGEM_NOTA_FISCAL);'+ #13 +
    '  INSERIR_LOG(49, :old.KIT_ACEITA_DEV_DESMEMBRADA, :new.KIT_ACEITA_DEV_DESMEMBRADA);'+ #13 +
    '  INSERIR_LOG(50, :old.KIT_ATUALIZ_PCO_ENTRADA_COMP, :new.KIT_ATUALIZ_PCO_ENTRADA_COMP);'+ #13 +
    '  INSERIR_LOG(51, :old.KIT_ATUALIZ_PCO_VENDA_COMP, :new.KIT_ATUALIZ_PCO_VENDA_COMP);'+ #13 +
    '  INSERIR_LOG(52, :old.QTD_DIAS_GARANTIA_FABRICANTE, :new.QTD_DIAS_GARANTIA_FABRICANTE);'+ #13 +
    '  INSERIR_LOG(53, :old.CODIGO_ORIGEM_FISCAL, :new.CODIGO_ORIGEM_FISCAL);'+ #13 +
    '  INSERIR_LOG(54, :old.ATIVO_IMOBILIZADO, :new.ATIVO_IMOBILIZADO);'+ #13 +
    '  INSERIR_LOG(55, :old.PRODUTO_RETORNAVEL, :new.PRODUTO_RETORNAVEL);'+ #13 +
    '  INSERIR_LOG(56, :old.PERMITE_VENDA_AUTO_SERVICO, :new.PERMITE_VENDA_AUTO_SERVICO);'+ #13 +
    '  INSERIR_LOG(57, :old.PERTENCE_CESTA_BASICA, :new.PERTENCE_CESTA_BASICA);'+ #13 +
    '  INSERIR_LOG(58, :old.CONTROLAR_NUMERO_SERIE, :new.CONTROLAR_NUMERO_SERIE);'+ #13 +
    '  INSERIR_LOG(59, :old.DATA_PRIMEIRA_ENTRADA, :new.DATA_PRIMEIRA_ENTRADA);'+ #13 +
    '  INSERIR_LOG(60, :old.DIAS_ENTREGA_SOB_ENCOMENDA, :new.DIAS_ENTREGA_SOB_ENCOMENDA);'+ #13 +
    '  INSERIR_LOG(61, :old.QTD_EMBALAGEM_COMPRA, :new.QTD_EMBALAGEM_COMPRA);'+ #13 +
    '  INSERIR_LOG(62, :old.MAPA_RECLASSIFICACAO_ID, :new.MAPA_RECLASSIFICACAO_ID);'+ #13 +
    '  INSERIR_LOG(63, :old.PERMITE_ENTREGAR, :new.PERMITE_ENTREGAR);'+ #13 +
    '  INSERIR_LOG(64, :old.PERMITE_ENTREGA_ATO, :new.PERMITE_ENTREGA_ATO);'+ #13 +
    '  INSERIR_LOG(65, :old.PERMITIR_VENDER_ACUMULADO, :new.PERMITIR_VENDER_ACUMULADO);'+ #13 +
    '  INSERIR_LOG(66, :old.PRECO_TABELA_COMPRA, :new.PRECO_TABELA_COMPRA);'+ #13 +
    '  INSERIR_LOG(67, :old.SIMILAR_ID, :new.SIMILAR_ID);'+ #13 +
    '  INSERIR_LOG(68, :old.TIPO_VENDA_SEM_PREVISAO, :new.TIPO_VENDA_SEM_PREVISAO);'+ #13 +
    '  INSERIR_LOG(69, :old.VOLUME_LITROS, :new.VOLUME_LITROS);'+ #13 +
    #13 +
    #13 +
    #13 +
    'end PRODUTOS_U_AR;'+ #13
end;

function AJustarLogsProdutos: string;
begin
  Result :=
    'CREATE OR REPLACE VIEW VW_TIPOS_ALTER_LOGS_PRODUTOS('+ #13 +
    '  TIPO_ALTERACAO_LOG_ID,'+ #13 +
    '  CAMPO'+ #13 +
    ')'+ #13 +
    'AS'+ #13 +
    '  select 1 as TIPO_ALTERACAO_LOG_ID, ''Data de cadastro'' as CAMPO from dual union'+ #13 +
    '  select 2 as TIPO_ALTERACAO_LOG_ID, ''Nome'' as CAMPO from dual union'+ #13 +
    '  select 3 as TIPO_ALTERACAO_LOG_ID, ''Nome de compra'' as CAMPO from dual union'+ #13 +
    '  select 4 as TIPO_ALTERACAO_LOG_ID, ''Fornecedor'' as CAMPO from dual union'+ #13 +
    '  select 5 as TIPO_ALTERACAO_LOG_ID, ''Unidade de venda'' as CAMPO from dual union'+ #13 +
    '  select 6 as TIPO_ALTERACAO_LOG_ID, ''Marca'' as CAMPO from dual union'+ #13 +
    '  select 7 as TIPO_ALTERACAO_LOG_ID, ''Multiplo de venda'' as CAMPO from dual union'+ #13 +
    '  select 8 as TIPO_ALTERACAO_LOG_ID, ''PESO'' as CAMPO from dual union'+ #13 +
    '  select 9 as TIPO_ALTERACAO_LOG_ID, ''Volume'' as CAMPO from dual union'+ #13 +
    #13 +
    '  select 10 as TIPO_ALTERACAO_LOG_ID, ''Linha'' as CAMPO from dual union'+ #13 +
    '  select 11 as TIPO_ALTERACAO_LOG_ID, ''Codigo de Barras'' as CAMPO from dual union'+ #13 +
    '  select 12 as TIPO_ALTERACAO_LOG_ID, ''Aceita estoque negativo'' as CAMPO from dual union'+ #13 +
    '  select 13 as TIPO_ALTERACAO_LOG_ID, ''Inativar ao zerar estoque'' as CAMPO from dual union'+ #13 +
    '  select 14 as TIPO_ALTERACAO_LOG_ID, ''Bloqueado para compras'' as CAMPO from dual union'+ #13 +
    '  select 15 as TIPO_ALTERACAO_LOG_ID, ''Bloqueado para vendas'' as CAMPO from dual union'+ #13 +
    '  select 16 as TIPO_ALTERACAO_LOG_ID, ''Permite devolucao'' as CAMPO from dual union'+ #13 +
    '  select 17 as TIPO_ALTERACAO_LOG_ID, ''Caracteristicas'' as CAMPO from dual union'+ #13 +
    '  select 18 as TIPO_ALTERACAO_LOG_ID, ''Ativo'' as CAMPO from dual union'+ #13 +
    '  select 19 as TIPO_ALTERACAO_LOG_ID, ''Motivo de inativar'' as CAMPO from dual union'+ #13 +
    '  select 20 as TIPO_ALTERACAO_LOG_ID, ''Codigo Cest'' as CAMPO from dual union'+ #13 +
    #13 +
    '  select 21 as TIPO_ALTERACAO_LOG_ID, ''Codigo NCM'' as CAMPO from dual union'+ #13 +
    '  select 22 as TIPO_ALTERACAO_LOG_ID, ''Produto Diversos PDV'' as CAMPO from dual union'+ #13 +
    '  select 23 as TIPO_ALTERACAO_LOG_ID, ''Codigo Balanca'' as CAMPO from dual union'+ #13 +
    '  select 24 as TIPO_ALTERACAO_LOG_ID, ''Produto Pai'' as CAMPO from dual union'+ #13 +
    '  select 25 as TIPO_ALTERACAO_LOG_ID, ''Quantidade de Vezes o pai'' as CAMPO from dual union'+ #13 +
    '  select 26 as TIPO_ALTERACAO_LOG_ID, ''Tipo de controle de estoque'' as CAMPO from dual union'+ #13 +
    '  select 27 as TIPO_ALTERACAO_LOG_ID, ''Exigir data de fabricacao'' as CAMPO from dual union'+ #13 +
    '  select 28 as TIPO_ALTERACAO_LOG_ID, ''Exigir data de vencimento'' as CAMPO from dual union'+ #13 +
    '  select 29 as TIPO_ALTERACAO_LOG_ID, ''Sob Encomenda'' as CAMPO from dual union'+ #13 +
    '  select 30 as TIPO_ALTERACAO_LOG_ID, ''Dias de aviso vencimento'' as CAMPO from dual union'+ #13 +
    #13 +
    '  select 31 as TIPO_ALTERACAO_LOG_ID, ''Revenda'' as CAMPO from dual union'+ #13 +
    '  select 32 as TIPO_ALTERACAO_LOG_ID, ''Uso e consumo'' as CAMPO from dual union'+ #13 +
    '  select 33 as TIPO_ALTERACAO_LOG_ID, ''Sujeito a comissao'' as CAMPO from dual union'+ #13 +
    '  select 34 as TIPO_ALTERACAO_LOG_ID, ''Bloquear entrada sem pedido de compra'' as CAMPO from dual union'+ #13 +
    '  select 35 as TIPO_ALTERACAO_LOG_ID, ''Exigir modelo nota fiscal'' as CAMPO from dual union'+ #13 +
    '  select 36 as TIPO_ALTERACAO_LOG_ID, ''Exigir separacao'' as CAMPO from dual union'+ #13 +
    '  select 37 as TIPO_ALTERACAO_LOG_ID, ''Valor adicional do frete'' as CAMPO from dual union'+ #13 +
    '  select 38 as TIPO_ALTERACAO_LOG_ID, ''% Comissao a vista'' as CAMPO from dual union'+ #13 +
    '  select 39 as TIPO_ALTERACAO_LOG_ID, ''% Comissao a prazo'' as CAMPO from dual union'+ #13 +
    '  select 40 as TIPO_ALTERACAO_LOG_ID, ''Codigo original do fabricante'' as CAMPO from dual union'+ #13 +
    #13 +
    '  select 41 as TIPO_ALTERACAO_LOG_ID, ''Tipo definicao aut lote'' as CAMPO from dual union'+ #13 +
    '  select 42 as TIPO_ALTERACAO_LOG_ID, ''Unidade de entrega'' as CAMPO from dual union'+ #13 +
    '  select 43 as TIPO_ALTERACAO_LOG_ID, ''Conferencia por codigo de barras'' as CAMPO from dual union'+ #13 +
    '  select 44 as TIPO_ALTERACAO_LOG_ID, ''Pagar comissao profissional'' as CAMPO from dual union'+ #13 +
    '  select 45 as TIPO_ALTERACAO_LOG_ID, ''Servico'' as CAMPO from dual union'+ #13 +
    '  select 46 as TIPO_ALTERACAO_LOG_ID, ''% Frete adicional na venda'' as CAMPO from dual union'+ #13 +
    '  select 47 as TIPO_ALTERACAO_LOG_ID, ''% IPI'' as CAMPO from dual union'+ #13 +
    '  select 48 as TIPO_ALTERACAO_LOG_ID, ''Mensagem nota fiscal'' as CAMPO from dual union'+ #13 +
    '  select 49 as TIPO_ALTERACAO_LOG_ID, ''Kit devolucao desmenbrada'' as CAMPO from dual union'+ #13 +
    '  select 50 as TIPO_ALTERACAO_LOG_ID, ''Kit atualiza custo na entrada'' as CAMPO from dual union'+ #13 +
    #13 +
    '  select 51 as TIPO_ALTERACAO_LOG_ID, ''Kit atualiza preco de venda'' as CAMPO from dual union'+ #13 +
    '  select 52 as TIPO_ALTERACAO_LOG_ID, ''Quantidade dias garantia fabricante'' as CAMPO from dual union'+ #13 +
    '  select 53 as TIPO_ALTERACAO_LOG_ID, ''Codigo origem fiscal'' as CAMPO from dual union'+ #13 +
    '  select 54 as TIPO_ALTERACAO_LOG_ID, ''Ativo imobilizado'' as CAMPO from dual union'+ #13 +
    '  select 55 as TIPO_ALTERACAO_LOG_ID, ''Produto Retornavel'' as CAMPO from dual union'+ #13 +
    '  select 56 as TIPO_ALTERACAO_LOG_ID, ''Permite venda PDV'' as CAMPO from dual union'+ #13 +
    '  select 57 as TIPO_ALTERACAO_LOG_ID, ''Pertence cesta basica'' as CAMPO from dual union'+ #13 +
    '  select 58 as TIPO_ALTERACAO_LOG_ID, ''Controlar numero serie'' as CAMPO from dual union'+ #13 +
    '  select 59 as TIPO_ALTERACAO_LOG_ID, ''Data primeira entrada'' as CAMPO from dual union'+ #13 +
    '  select 60 as TIPO_ALTERACAO_LOG_ID, ''Dias entraga sob encomenda'' as CAMPO from dual union'+ #13 +
    #13 +
    '  select 61 as TIPO_ALTERACAO_LOG_ID, ''Quantidade embalem compra'' as CAMPO from dual union'+ #13 +
    '  select 62 as TIPO_ALTERACAO_LOG_ID, ''Mapa de reclassificaco'' as CAMPO from dual union'+ #13 +
    '  select 63 as TIPO_ALTERACAO_LOG_ID, ''Permite entregar'' as CAMPO from dual union'+ #13 +
    '  select 64 as TIPO_ALTERACAO_LOG_ID, ''Permite entrega no ato'' as CAMPO from dual union'+ #13 +
    '  select 65 as TIPO_ALTERACAO_LOG_ID, ''Permite vender no acumulado'' as CAMPO from dual union'+ #13 +
    '  select 66 as TIPO_ALTERACAO_LOG_ID, ''Preco tabela de compra'' as CAMPO from dual union'+ #13 +
    '  select 67 as TIPO_ALTERACAO_LOG_ID, ''Similar'' as CAMPO from dual union'+ #13 +
    '  select 68 as TIPO_ALTERACAO_LOG_ID, ''Tipo de venda sem previsao'' as CAMPO from dual union'+ #13 +
    '  select 69 as TIPO_ALTERACAO_LOG_ID, ''Volume em litros'' as CAMPO from dual;'+ #13
end;

function AjusteMotivoInatividade: string;
begin
  Result :=
    'ALTER TABLE PRODUTOS'+ #13 +
    'DROP CONSTRAINT CK_PRODUTOS_ATIVO;'+ #13 +
    #13 +
    'alter trigger PRODUTOS_IU_BR disable;'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  INICIAR_SESSAO(1, 1, ''ALTIS'', ''ALTIS'');'+ #13 +
    #13 +
    'update PRODUTOS set MOTIVO_INATIVIDADE = ''INATIVO'''+ #13 +
    'where ATIVO = ''N'' or INATIVAR_ZERAR_ESTOQUE = ''S'''+ #13 +
    'and MOTIVO_INATIVIDADE is null;'+ #13 +
    #13 +
    'update PRODUTOS set MOTIVO_INATIVIDADE = null'+ #13 +
    'where ATIVO = ''S'' and INATIVAR_ZERAR_ESTOQUE = ''N'';'+ #13 +
    #13 +
    'end;'+ #13 +
    '/' +
    #13 +
    'alter table PRODUTOS'+ #13 +
    'add constraint CK_PRODUTOS_MOT_INAT'+ #13 +
    'CHECK'+ #13 +
    '('+ #13 +
    '((ATIVO = ''S'' and INATIVAR_ZERAR_ESTOQUE = ''N'' and MOTIVO_INATIVIDADE is null)'+ #13 +
    'or'+ #13 +
    '(ATIVO = ''S'' and INATIVAR_ZERAR_ESTOQUE = ''S'' and MOTIVO_INATIVIDADE is not null)'+ #13 +
    'or'+ #13 +
    '(ATIVO = ''N'' and INATIVAR_ZERAR_ESTOQUE = ''N'' and MOTIVO_INATIVIDADE is not null)'+ #13 +
    'or'+ #13 +
    '(ATIVO = ''N'' and INATIVAR_ZERAR_ESTOQUE = ''S'' and MOTIVO_INATIVIDADE is not null))'+ #13 +
    ');'+ #13
end;

function AjustarTriggerAJUSTES_ESTOQUE_ITENS_IU_BR: string;
begin
  Result :=
    'create or replace trigger AJUSTES_ESTOQUE_ITENS_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on AJUSTES_ESTOQUE_ITENS'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    '  vEmpresaId           AJUSTES_ESTOQUE.EMPRESA_ID%type;'+ #13 +
    '  vCustoAjusteEstoque  AJUSTES_ESTOQUE_ITENS.PRECO_UNITARIO%type;'+ #13 +
    'begin'+ #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '  if SESSAO.ROTINA = ''DESVIAR'' then'+ #13 +
    '    return;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if updating then'+ #13 +
    '    ERRO(''N�o s�o permitidas altera��es na tabela "AJUSTES_ESTOQUE_ITENS"!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if inserting then'+ #13 +
    '    /*Buscando a capa do ajuste de estoque*/'+ #13 +
    '    select'+ #13 +
    '      EMPRESA_ID'+ #13 +
    '    into'+ #13 +
    '      vEmpresaId'+ #13 +
    '    from'+ #13 +
    '      AJUSTES_ESTOQUE'+ #13 +
    '    where AJUSTE_ESTOQUE_ID = :new.AJUSTE_ESTOQUE_ID;'+ #13 +
    #13 +
    '    MOVIMENTAR_ESTOQUE('+ #13 +
    '      vEmpresaId,'+ #13 +
    '      :new.PRODUTO_ID,'+ #13 +
    '      :new.LOCAL_ID,'+ #13 +
    '      :new.LOTE,'+ #13 +
    '      :new.QUANTIDADE,'+ #13 +
    '      case when :new.NATUREZA = ''E'' then ''AJE'' else ''AJS'' end,'+ #13 +
    '      :new.DATA_FABRICACAO,'+ #13 +
    '      :new.DATA_VENCIMENTO'+ #13 +
    '    );    '+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end AJUSTES_ESTOQUE_ITENS_IU_BR;'+ #13
end;

function AdicionarColunaNUMERO_ESTABELECIMENTO_CIELO: string;
begin
  Result :=
    'alter table PARAMETROS_EMPRESA'+ #13 +
    'add NUMERO_ESTABELECIMENTO_CIELO    varchar2(20);'+ #13
end;

function AjustarViewVW_TIPOS_ALTER_LOGS_CT_RECEBER: string;
begin
  Result :=
    'create or replace view VW_TIPOS_ALTER_LOGS_CT_RECEBER'+ #13 +
    'as'+ #13 +
    'select 1 as TIPO_ALTERACAO_LOG_ID, ''C�digo da baixa'' as CAMPO from dual union'+ #13 +
    'select 2 as TIPO_ALTERACAO_LOG_ID, ''Status'' as CAMPO from dual union'+ #13 +
    'select 3 as TIPO_ALTERACAO_LOG_ID, ''Data vencimento'' as CAMPO from dual union'+ #13 +
    'select 4 as TIPO_ALTERACAO_LOG_ID, ''Valor documento'' as CAMPO from dual union'+ #13 +
    'select 5 as TIPO_ALTERACAO_LOG_ID, ''Parcela'' as CAMPO from dual union'+ #13 +
    'select 6 as TIPO_ALTERACAO_LOG_ID, ''Quantidade de parcelas'' as CAMPO from dual union'+ #13 +
    'select 7 as TIPO_ALTERACAO_LOG_ID, ''Tipo de cobran�a'' as CAMPO from dual;'+ #13
end;

function AjustarTriggerCONTAS_RECEBER_U_AR: string;
begin
  Result :=
    'create or replace trigger CONTAS_RECEBER_U_AR'+ #13 +
    'after update '+ #13 +
    'on CONTAS_RECEBER'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    #13 +
    '  procedure INSERIR_LOG(pTIPO_ALTERACAO_LOG_ID in number, pVALOR_ANTERIOR in string, pNOVO_VALOR in string)'+ #13 +
    '  is begin'+ #13 +
    '    if nvl(pVALOR_ANTERIOR, ''X'') <> nvl(pNOVO_VALOR, ''X'') then'+ #13 +
    '      insert into LOGS_CONTAS_RECEBER(TIPO_ALTERACAO_LOG_ID, RECEBER_ID, VALOR_ANTERIOR, NOVO_VALOR)'+ #13 +
    '      values(pTIPO_ALTERACAO_LOG_ID, :new.RECEBER_ID, pVALOR_ANTERIOR, pNOVO_VALOR);'+ #13 +
    '    end if;'+ #13 +
    '  end;'+ #13 +
    #13 +
    'begin'+ #13 +
    '  /* View VW_TIPOS_ALTER_LOGS_CT_RECEBER */'+ #13 +
    #13 +
    '  INSERIR_LOG(1, :old.BAIXA_ID, :new.BAIXA_ID);'+ #13 +
    '  INSERIR_LOG(2, :old.STATUS, :new.STATUS);'+ #13 +
    '  INSERIR_LOG(3, :old.DATA_VENCIMENTO, :new.DATA_VENCIMENTO);'+ #13 +
    '  INSERIR_LOG(4, :old.VALOR_DOCUMENTO, :new.VALOR_DOCUMENTO);'+ #13 +
    '  INSERIR_LOG(5, :old.PARCELA, :new.PARCELA);'+ #13 +
    '  INSERIR_LOG(6, :old.NUMERO_PARCELAS, :new.NUMERO_PARCELAS);'+ #13 +
    '  INSERIR_LOG(7, :old.COBRANCA_ID, :new.COBRANCA_ID);'+ #13 +
    #13 +
    'end CONTAS_RECEBER_U_AR;'+ #13
end;

function AjustarProcedureATUALIZAR_RETENCOES_CONTAS_REC: string;
begin
  Result :=
    'create or replace procedure ATUALIZAR_RETENCOES_CONTAS_REC(iMOVIMENTO_ID in number, iTIPO_MOVIMENTO in string)'+ #13 +
    'is'+ #13 +
    #13 +
    '  cursor cCartoesPedido is'+ #13 +
    '  select'+ #13 +
    '    COR.RECEBER_ID,'+ #13 +
    '    round(COR.VALOR_DOCUMENTO * TPC.TAXA_RETENCAO_MES * 0.01, 2) as VALOR_RETENCAO'+ #13 +
    '  from'+ #13 +
    '    CONTAS_RECEBER COR'+ #13 +
    #13 +
    '  inner join TIPOS_COBRANCA TPC'+ #13 +
    '  on COR.COBRANCA_ID = TPC.COBRANCA_ID'+ #13 +
    #13 +
    '  where COR.ORCAMENTO_ID = iMOVIMENTO_ID'+ #13 +
    '  and TPC.FORMA_PAGAMENTO = ''CRT'''+ #13 +
    '  and COR.STATUS = ''A'';'+ #13 +
    #13 +
    #13 +
    '  cursor cCartoesAcumulado is'+ #13 +
    '  select'+ #13 +
    '    COR.RECEBER_ID,'+ #13 +
    '    round(COR.VALOR_DOCUMENTO * TPC.TAXA_RETENCAO_MES * 0.01, 2) as VALOR_RETENCAO'+ #13 +
    '  from'+ #13 +
    '    CONTAS_RECEBER COR'+ #13 +
    #13 +
    '  inner join TIPOS_COBRANCA TPC'+ #13 +
    '  on COR.COBRANCA_ID = TPC.COBRANCA_ID'+ #13 +
    #13 +
    '  where COR.ACUMULADO_ID = iMOVIMENTO_ID'+ #13 +
    '  and TPC.FORMA_PAGAMENTO = ''CRT'''+ #13 +
    '  and COR.STATUS = ''A'';'+ #13 +
    #13 +
    #13 +
    '  cursor cCartoesBaixa is'+ #13 +
    '  select'+ #13 +
    '    COR.RECEBER_ID,'+ #13 +
    '    round(COR.VALOR_DOCUMENTO * TPC.TAXA_RETENCAO_MES * 0.01, 2) as VALOR_RETENCAO'+ #13 +
    '  from'+ #13 +
    '    CONTAS_RECEBER COR'+ #13 +
    #13 +
    '  inner join TIPOS_COBRANCA TPC'+ #13 +
    '  on COR.COBRANCA_ID = TPC.COBRANCA_ID'+ #13 +
    #13 +
    '  where COR.BAIXA_ORIGEM_ID = iMOVIMENTO_ID'+ #13 +
    '  and TPC.FORMA_PAGAMENTO = ''CRT'''+ #13 +
    '  and COR.STATUS = ''A'';'+ #13 +
    #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  if iTIPO_MOVIMENTO not in(''ORC'', ''BXR'', ''ACU'') then'+ #13 +
    '    ERRO(''O tipo de movimento s� pode ser "ORC", "ACU" ou "BXR", entre em contato com a Altis!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if iTIPO_MOVIMENTO = ''ORC'' then'+ #13 +
    '    for xCartoes in cCartoesPedido loop'+ #13 +
    '      update CONTAS_RECEBER set'+ #13 +
    '        VALOR_RETENCAO = xCartoes.VALOR_RETENCAO'+ #13 +
    '      where RECEBER_ID = xCartoes.RECEBER_ID;'+ #13 +
    '    end loop;'+ #13 +
    '  elsif iTIPO_MOVIMENTO = ''ACU'' then'+ #13 +
    '    for xCartoes in cCartoesAcumulado loop'+ #13 +
    '      update CONTAS_RECEBER set'+ #13 +
    '        VALOR_RETENCAO = xCartoes.VALOR_RETENCAO'+ #13 +
    '      where RECEBER_ID = xCartoes.RECEBER_ID;'+ #13 +
    '    end loop;'+ #13 +
    '  else'+ #13 +
    '     for xBaixas in cCartoesBaixa loop     '+ #13 +
    '       update CONTAS_RECEBER set'+ #13 +
    '         VALOR_RETENCAO = xBaixas.VALOR_RETENCAO'+ #13 +
    '       where RECEBER_ID = xBaixas.RECEBER_ID;     '+ #13 +
    '     end loop;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end ATUALIZAR_RETENCOES_CONTAS_REC;'+ #13
end;

function DroparConstraintFK_CONTAS_REC_ITEM_ID_CRT_ORC: string;
begin
  Result :=
    'alter table CONTAS_RECEBER'+ #13 +
    'drop constraint FK_CONTAS_REC_ITEM_ID_CRT_ORC;'+ #13
end;

function AlterarCampoCADASTRO_IDTabelaCONTAS_RECEBER_BAIXAS: string;
begin
  Result :=
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'modify CADASTRO_ID               number(10) null;'+ #13
end;

function AjustarConstraintCK_CONTAS_RECEBER_BAIXAS_TIPO: string;
begin
  Result :=
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'drop constraint CK_CONTAS_RECEBER_BAIXAS_TIPO;'+ #13 +
    #13 +
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'add constraint CK_CONTAS_RECEBER_BAIXAS_TIPO'+ #13 +
    'check('+ #13 +
    '  TIPO in(''CRE'', ''BCR'', ''BCA'')'+ #13 +
    '  or'+ #13 +
    '  (TIPO = ''ECR'' and BAIXA_PAGAR_ORIGEM_ID is not null)'+ #13 +
    ');'+ #13
end;

function AjustarConstraintCK_CONTAS_REC_BX_TIPO_CAD_ID: string;
begin
  Result :=
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'drop constraint CK_CONTAS_REC_BX_TIPO_CAD_ID;'+ #13 +
    #13 +
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'add constraint CK_CONTAS_REC_BX_TIPO_CAD_ID'+ #13 +
    'check('+ #13 +
    '  (TIPO in(''CRE'', ''BCR'', ''ECR'') and CADASTRO_ID is not null)'+ #13 +
    '  or'+ #13 +
    '  (TIPO = ''BCA'') and CADASTRO_ID is null'+ #13 +
    ');'+ #13
end;

function AjustarTriggerCONTAS_REC_BX_PAGTOS_DIN_IU_BR: string;
begin
  Result :=
    'create or replace trigger CONTAS_REC_BX_PAGTOS_DIN_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on CONTAS_REC_BAIXAS_PAGTOS_DIN'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    '  vDataPagamento date;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '  if inserting then'+ #13 +
    '    select'+ #13 +
    '      DATA_PAGAMENTO'+ #13 +
    '    into'+ #13 +
    '      vDataPagamento'+ #13 +
    '    from'+ #13 +
    '      CONTAS_RECEBER_BAIXAS'+ #13 +
    '    where BAIXA_ID = :new.BAIXA_ID;'+ #13 +
    #13 +
    '    insert into MOVIMENTOS_CONTAS('+ #13 +
    '      MOVIMENTO_ID,'+ #13 +
    '      CONTA_ID,'+ #13 +
    '      VALOR_MOVIMENTO,'+ #13 +
    '      DATA_HORA_MOVIMENTO,'+ #13 +
    '      USUARIO_MOVIMENTO_ID,'+ #13 +
    '      TIPO_MOVIMENTO,            '+ #13 +
    '      CENTRO_CUSTO_ID,                  '+ #13 +
    '      BAIXA_RECEBER_ID'+ #13 +
    '    )values('+ #13 +
    '      SEQ_MOVIMENTOS_CONTAS.nextval,'+ #13 +
    '      :new.CONTA_ID,'+ #13 +
    '      :new.VALOR,'+ #13 +
    '      vDataPagamento,'+ #13 +
    '      SESSAO.USUARIO_SESSAO_ID,'+ #13 +
    '      ''BXR'','+ #13 +
    '      1,'+ #13 +
    '      :new.BAIXA_ID'+ #13 +
    '    );'+ #13 +
    '  end if;  '+ #13 +
    #13 +
    'end CONTAS_REC_BX_PAGTOS_DIN_IU_BR;'+ #13
end;

function AjustarTriggerCONTAS_PAGAR_BX_PAG_DIN_IU_BR: string;
begin
  Result :=
    'create or replace trigger CONTAS_PAGAR_BX_PAG_DIN_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on CONTAS_PAGAR_BAIXAS_PAGTOS_DIN'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    '  vDataPagamento date;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '  if inserting then'+ #13 +
    '    select'+ #13 +
    '      DATA_PAGAMENTO'+ #13 +
    '    into'+ #13 +
    '      vDataPagamento'+ #13 +
    '    from'+ #13 +
    '      CONTAS_PAGAR_BAIXAS'+ #13 +
    '    where BAIXA_ID = :new.BAIXA_ID;'+ #13 +
    #13 +
    '    insert into MOVIMENTOS_CONTAS('+ #13 +
    '      MOVIMENTO_ID,'+ #13 +
    '      CONTA_ID,'+ #13 +
    '      VALOR_MOVIMENTO,'+ #13 +
    '      DATA_HORA_MOVIMENTO,'+ #13 +
    '      USUARIO_MOVIMENTO_ID,'+ #13 +
    '      TIPO_MOVIMENTO,            '+ #13 +
    '      CENTRO_CUSTO_ID,                  '+ #13 +
    '      BAIXA_PAGAR_ID'+ #13 +
    '    )values('+ #13 +
    '      SEQ_MOVIMENTOS_CONTAS.nextval,'+ #13 +
    '      :new.CONTA_ID,'+ #13 +
    '      :new.VALOR,'+ #13 +
    '      vDataPagamento,'+ #13 +
    '      SESSAO.USUARIO_SESSAO_ID,'+ #13 +
    '      ''BXP'','+ #13 +
    '      1,'+ #13 +
    '      :new.BAIXA_ID'+ #13 +
    '    );'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end CONTAS_PAGAR_BX_PAG_DIN_IU_BR;'+ #13
end;

function AjustarProcedureATUALIZAR_CUSTOS_ENTRADA: string;
begin
  Result :=
    'create or replace procedure ATUALIZAR_CUSTOS_ENTRADA(iENTRADA_ID in number)'+ #13 +
    'is'+ #13 +
    #13 +
    '  vEmpresaId             ENTRADAS_NOTAS_FISCAIS.ENTRADA_ID%type;'+ #13 +
    '  vAtualizarCustoEntrada ENTRADAS_NOTAS_FISCAIS.ATUALIZAR_CUSTO_ENTRADA%type;  '+ #13 +
    #13 +
    '  type RecFormacaoCusto is record('+ #13 +
    '    PRECO_TABELA        CUSTOS_PRODUTOS.PRECO_TABELA%type default 0,'+ #13 +
    '    PERC_ICMS           CUSTOS_PRODUTOS.PERCENTUAL_ICMS_ENTRADA%type default 0,'+ #13 +
    '    PERC_ST             CUSTOS_PRODUTOS.PERCENTUAL_ST_ENTRADA%type default 0,'+ #13 +
    '    PERC_FRETE          CUSTOS_PRODUTOS.PERCENTUAL_FRETE_ENTRADA%type default 0,'+ #13 +
    '    PERC_ICMS_FRETE     CUSTOS_PRODUTOS.PERCENTUAL_ICMS_FRETE_ENTRADA%type default 0,'+ #13 +
    '    PERC_PIS            CUSTOS_PRODUTOS.PERCENTUAL_PIS_ENTRADA%type default 0,'+ #13 +
    '    PERC_COFINS         CUSTOS_PRODUTOS.PERCENTUAL_COFINS_ENTRADA%type default 0,'+ #13 +
    '    PERC_DESCONTO       CUSTOS_PRODUTOS.PERCENTUAL_DESCONTO_ENTRADA%type default 0,'+ #13 +
    '    PERC_OUTRAS_DESP    CUSTOS_PRODUTOS.PERCENTUAL_OUTRAS_DESPESAS_ENT%type default 0,'+ #13 +
    '    PERC_OUTROS_CUSTOS  CUSTOS_PRODUTOS.PERCENTUAL_OUTROS_CUSTOS_ENTR%type default 0,'+ #13 +
    '    PERC_IPI            CUSTOS_PRODUTOS.PERCENTUAL_IPI_ENTRADA%type default 0,'+ #13 +
    '    PERC_DIFAL          CUSTOS_PRODUTOS.PERCENTUAL_DIFAL_ENTRADA%type default 0,'+ #13 +
    #13 +
    '    PRECO_FINAL_ANTERIOR            CUSTOS_PRODUTOS.PRECO_FINAL%type default 0,'+ #13 +
    '    PRECO_FINAL_MEDIO_ANTERIOR      CUSTOS_PRODUTOS.PRECO_FINAL_MEDIO%type default 0,'+ #13 +
    #13 +
    '    PRECO_LIQUIDO_ANTERIOR          CUSTOS_PRODUTOS.PRECO_LIQUIDO%type default 0,'+ #13 +
    '    PRECO_LIQUIDO_MEDIO_ANTERIOR    CUSTOS_PRODUTOS.PRECO_LIQUIDO_MEDIO%type default 0,'+ #13 +
    #13 +
    '    CMV_ANTERIOR                    CUSTOS_PRODUTOS.CMV%type default 0'+ #13 +
    '  );'+ #13 +
    #13 +
    '  vFormacaoCusto         RecFormacaoCusto;'+ #13 +
    #13 +
    '  cursor cItens is'+ #13 +
    '  select'+ #13 +
    '    ITE.PRODUTO_ID,'+ #13 +
    '    CFO.ENTRADA_MERCADORIAS_BONIFIC,'+ #13 +
    '    ITE.PRECO_FINAL,'+ #13 +
    '    ITE.PRECO_LIQUIDO,'+ #13 +
    '    ITE.QUANTIDADE_ENTRADA_ALTIS as QUANTIDADE,'+ #13 +
    '    round((ITE.VALOR_TOTAL - ITE.VALOR_TOTAL_DESCONTO + ITE.VALOR_TOTAL_OUTRAS_DESPESAS + ITE.VALOR_IPI + ITE.VALOR_ICMS_ST) / ITE.QUANTIDADE_ENTRADA_ALTIS, 5) as PRECO_TRIBUTAVEL,'+ #13 +
    '    round((ITE.VALOR_TOTAL - ITE.VALOR_TOTAL_DESCONTO + ITE.VALOR_TOTAL_OUTRAS_DESPESAS + ITE.VALOR_ICMS_ST) / ITE.QUANTIDADE_ENTRADA_ALTIS, 5) as PRECO_TRIB_SEM_IPI,'+ #13 +
    '    round(ITE.VALOR_TOTAL / ITE.QUANTIDADE_ENTRADA_ALTIS, 5) as PRECO_UNITARIO,'+ #13 +
    '    ITE.VALOR_TOTAL_DESCONTO / ITE.QUANTIDADE_ENTRADA_ALTIS as VALOR_DESCONTO_UNITARIO,'+ #13 +
    '    ITE.VALOR_TOTAL_OUTRAS_DESPESAS / ITE.QUANTIDADE_ENTRADA_ALTIS as VALOR_OUTRAS_DESPESAS_UNITARIO,'+ #13 +
    #13 +
    '    ITE.VALOR_ICMS_CUSTO as VALOR_ICMS_UNITARIO,'+ #13 +
    '    ITE.VALOR_ICMS_ST_CUSTO as VALOR_ICMS_ST_UNITARIO,'+ #13 +
    '    ITE.VALOR_ICMS_FRETE_CUSTO as VALOR_ICMS_FRETE_UNITARIO,'+ #13 +
    '    ITE.VALOR_PIS_CUSTO as VALOR_PIS_UNITARIO,'+ #13 +
    '    ITE.VALOR_COFINS_CUSTO as VALOR_COFINS_UNITARIO,'+ #13 +
    '    ITE.VALOR_IPI_CUSTO as VALOR_IPI_UNITARIO,'+ #13 +
    '    ITE.VALOR_FRETE_CUSTO as VALOR_FRETE_UNITARIO,'+ #13 +
    '    ITE.VALOR_DIFAL_CUSTO as VALOR_DIFAL_UNITARIO,'+ #13 +
    '    ITE.VALOR_OUTROS_CUSTO as VALOR_OUTROS_CUSTOS_UNITARIO'+ #13 +
    '  from'+ #13 +
    '    ENTRADAS_NOTAS_FISCAIS_ITENS ITE'+ #13 +
    #13 +
    '  inner join PRODUTOS PRO'+ #13 +
    '  on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    '  inner join CFOP CFO'+ #13 +
    '  on ITE.CFOP_ID = CFO.CFOP_PESQUISA_ID'+ #13 +
    #13 +
    '  where ITE.ENTRADA_ID = iENTRADA_ID;'+ #13 +
    #13 +
    '  cursor cComprasBaixar is'+ #13 +
    '  select distinct'+ #13 +
    '    COMPRA_ID'+ #13 +
    '  from'+ #13 +
    '    ENTRADAS_NF_ITENS_COMPRAS'+ #13 +
    '  where ENTRADA_ID = iENTRADA_ID;'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  select'+ #13 +
    '    EMPRESA_ID,'+ #13 +
    '    ATUALIZAR_CUSTO_ENTRADA'+ #13 +
    '  into'+ #13 +
    '    vEmpresaId,'+ #13 +
    '    vAtualizarCustoEntrada'+ #13 +
    '  from'+ #13 +
    '    ENTRADAS_NOTAS_FISCAIS'+ #13 +
    '  where ENTRADA_ID = iENTRADA_ID;'+ #13 +
    #13 +
    '  for vItens in cItens loop'+ #13 +
    #13 +
    '    if vItens.ENTRADA_MERCADORIAS_BONIFIC = ''S'' then'+ #13 +
    '      ATUALIZAR_CMV(vEmpresaId, vItens.PRODUTO_ID, 0, vItens.QUANTIDADE);'+ #13 +
    '      continue;'+ #13 +
    '    else'+ #13 +
    '      ATUALIZAR_CMV(vEmpresaId, vItens.PRODUTO_ID, vItens.PRECO_FINAL, vItens.QUANTIDADE);'+ #13 +
    '    end if;'+ #13 +
    #13 +
    '    /* Se n�o � para atualizar os custos, passando para frente ap�s atualizar o CMV */'+ #13 +
    '    if vAtualizarCustoEntrada = ''N'' then'+ #13 +
    '      continue;'+ #13 +
    '    end if;'+ #13 +
    #13 +
    '    select'+ #13 +
    '      PRECO_TABELA,'+ #13 +
    '      PERCENTUAL_ICMS_ENTRADA,'+ #13 +
    '      PERCENTUAL_ST_ENTRADA,'+ #13 +
    '      PERCENTUAL_FRETE_ENTRADA,'+ #13 +
    '      PERCENTUAL_ICMS_FRETE_ENTRADA,'+ #13 +
    '      PERCENTUAL_PIS_ENTRADA,'+ #13 +
    '      PERCENTUAL_COFINS_ENTRADA,'+ #13 +
    '      PERCENTUAL_DESCONTO_ENTRADA,'+ #13 +
    '      PERCENTUAL_OUTRAS_DESPESAS_ENT,'+ #13 +
    '      PERCENTUAL_OUTROS_CUSTOS_ENTR,'+ #13 +
    '      PERCENTUAL_IPI_ENTRADA,'+ #13 +
    '      PERCENTUAL_DIFAL_ENTRADA,      '+ #13 +
    '      PRECO_FINAL,'+ #13 +
    '      PRECO_FINAL_MEDIO,'+ #13 +
    '      PRECO_LIQUIDO,'+ #13 +
    '      PRECO_LIQUIDO_MEDIO,'+ #13 +
    '      CMV      '+ #13 +
    '    into'+ #13 +
    '      vFormacaoCusto.PRECO_TABELA,'+ #13 +
    '      vFormacaoCusto.PERC_ICMS,'+ #13 +
    '      vFormacaoCusto.PERC_ST,'+ #13 +
    '      vFormacaoCusto.PERC_FRETE,'+ #13 +
    '      vFormacaoCusto.PERC_ICMS_FRETE,'+ #13 +
    '      vFormacaoCusto.PERC_PIS,'+ #13 +
    '      vFormacaoCusto.PERC_COFINS,'+ #13 +
    '      vFormacaoCusto.PERC_DESCONTO,'+ #13 +
    '      vFormacaoCusto.PERC_OUTRAS_DESP,'+ #13 +
    '      vFormacaoCusto.PERC_OUTROS_CUSTOS,'+ #13 +
    '      vFormacaoCusto.PERC_IPI,'+ #13 +
    '      vFormacaoCusto.PERC_DIFAL,'+ #13 +
    '      vFormacaoCusto.PRECO_FINAL_ANTERIOR,'+ #13 +
    '      vFormacaoCusto.PRECO_FINAL_MEDIO_ANTERIOR,'+ #13 +
    '      vFormacaoCusto.PRECO_LIQUIDO_ANTERIOR,'+ #13 +
    '      vFormacaoCusto.PRECO_LIQUIDO_MEDIO_ANTERIOR,'+ #13 +
    '      vFormacaoCusto.CMV_ANTERIOR      '+ #13 +
    '    from'+ #13 +
    '      VW_CUSTOS_PRODUTOS CUS'+ #13 +
    #13 +
    '    inner join ESTOQUES EST'+ #13 +
    '    on CUS.EMPRESA_ID = EST.EMPRESA_ID'+ #13 +
    '    and CUS.PRODUTO_ID = EST.PRODUTO_ID'+ #13 +
    #13 +
    '    where EST.EMPRESA_ID = vEmpresaId'+ #13 +
    '    and EST.PRODUTO_ID = vItens.PRODUTO_ID;'+ #13 +
    #13 +
    '    update ENTRADAS_NOTAS_FISCAIS_ITENS set      '+ #13 +
    '      PRECO_TABELA_ANTERIOR           = vFormacaoCusto.PRECO_TABELA,'+ #13 +
    '      PERCENTUAL_DESCONTO_ENTR_ANT    = vFormacaoCusto.PERC_DESCONTO,'+ #13 +
    '      PERCENTUAL_IPI_ENTRADA_ANT      = vFormacaoCusto.PERC_IPI,'+ #13 +
    '      PERCENTUAL_OUTRAS_DESP_ENT_ANT  = vFormacaoCusto.PERC_OUTRAS_DESP,'+ #13 +
    '      PERCENTUAL_FRETE_ENTR_ANTERIOR  = vFormacaoCusto.PERC_FRETE,'+ #13 +
    '      PERCENTUAL_ST_ENTRADA_ANTERIOR  = vFormacaoCusto.PERC_ST,'+ #13 +
    '      PERCENTUAL_ICMS_ENTR_ANTERIOR   = vFormacaoCusto.PERC_ICMS,'+ #13 +
    '      PERCENTUAL_ICMS_FRETE_ENTR_ANT  = vFormacaoCusto.PERC_ICMS_FRETE,'+ #13 +
    '      PERCENTUAL_PIS_ENTRADA_ANTER    = vFormacaoCusto.PERC_PIS,'+ #13 +
    '      PERCENTUAL_COFINS_ENT_ANTERIOR  = vFormacaoCusto.PERC_COFINS,'+ #13 +
    '      /*PERCENTUAL_PIS_FRETE_ENT_ANT    = vFormacaoCusto.PRECO_TABELA,'+ #13 +
    '      PERCENTUAL_COF_FRETE_ENT_ANT    = vFormacaoCusto.PRECO_TABELA,*/'+ #13 +
    '      PERCENTUAL_OUT_CUS_ENTR_ANTER   = vFormacaoCusto.PERC_OUTROS_CUSTOS,'+ #13 +
    '      --PERCENTUAL_CUSTO_FIXO_ENT_ANT   = vFormacaoCusto.PERC_IPI,'+ #13 +
    '      PERCENTUAL_DIFAL_ENTRADA_ANTER  = vFormacaoCusto.PERC_DIFAL,'+ #13 +
    '      PRECO_FINAL_ANTERIOR            = vFormacaoCusto.PRECO_FINAL_ANTERIOR,'+ #13 +
    '      PRECO_FINAL_MEDIO_ANTERIOR      = vFormacaoCusto.PRECO_FINAL_MEDIO_ANTERIOR,'+ #13 +
    '      PRECO_LIQUIDO_ANTERIOR          = vFormacaoCusto.PRECO_LIQUIDO_ANTERIOR,'+ #13 +
    '      PRECO_LIQUIDO_MEDIO_ANTERIOR    = vFormacaoCusto.PRECO_LIQUIDO_MEDIO_ANTERIOR,'+ #13 +
    '      CMV_ANTERIOR                    = vFormacaoCusto.CMV_ANTERIOR'+ #13 +
    '    where ENTRADA_ID = iENTRADA_ID'+ #13 +
    '    and PRODUTO_ID = vItens.PRODUTO_ID;   '+ #13 +
    #13 +
    '    vFormacaoCusto.PRECO_TABELA       := vItens.PRECO_UNITARIO;'+ #13 +
    #13 +
    '    vFormacaoCusto.PERC_ICMS :='+ #13 +
    '      case when vItens.VALOR_ICMS_ST_UNITARIO > 0 then 0 else round(vItens.VALOR_ICMS_UNITARIO / vItens.PRECO_TRIB_SEM_IPI, 5) * 100 end;'+ #13 +
    #13 +
    '    vFormacaoCusto.PERC_ST            := round(vItens.VALOR_ICMS_ST_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;'+ #13 +
    '    vFormacaoCusto.PERC_FRETE         := round(vItens.VALOR_FRETE_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;'+ #13 +
    '    vFormacaoCusto.PERC_PIS           := round(vItens.VALOR_PIS_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;'+ #13 +
    '    vFormacaoCusto.PERC_COFINS        := round(vItens.VALOR_COFINS_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;'+ #13 +
    '    vFormacaoCusto.PERC_DESCONTO      := round(vItens.VALOR_DESCONTO_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;'+ #13 +
    '    vFormacaoCusto.PERC_OUTRAS_DESP   := round(vItens.VALOR_OUTRAS_DESPESAS_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;'+ #13 +
    '    vFormacaoCusto.PERC_IPI           := round(vItens.VALOR_IPI_UNITARIO / vItens.PRECO_TRIB_SEM_IPI, 5) * 100;'+ #13 +
    '    vFormacaoCusto.PERC_ICMS_FRETE    := round(vItens.VALOR_ICMS_FRETE_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;'+ #13 +
    #13 +
    '    vFormacaoCusto.PERC_OUTROS_CUSTOS := round(vItens.VALOR_OUTROS_CUSTOS_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;'+ #13 +
    '    vFormacaoCusto.PERC_DIFAL         := round(vItens.VALOR_DIFAL_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;'+ #13 +
    #13 +
    '    ATUALIZAR_CUSTOS_PRODUTOS('+ #13 +
    '      vEmpresaId,'+ #13 +
    '      vItens.PRODUTO_ID,'+ #13 +
    '      vItens.PRECO_FINAL,'+ #13 +
    '      vItens.PRECO_LIQUIDO,'+ #13 +
    '      vItens.QUANTIDADE,'+ #13 +
    '      ''ENT'','+ #13 +
    '      iENTRADA_ID,'+ #13 +
    '      vFormacaoCusto.PRECO_TABELA,'+ #13 +
    '      vFormacaoCusto.PERC_ICMS,'+ #13 +
    '      vFormacaoCusto.PERC_ST,'+ #13 +
    '      vFormacaoCusto.PERC_FRETE,'+ #13 +
    '      vFormacaoCusto.PERC_ICMS_FRETE,'+ #13 +
    '      vFormacaoCusto.PERC_PIS,'+ #13 +
    '      vFormacaoCusto.PERC_COFINS,'+ #13 +
    '      vFormacaoCusto.PERC_DESCONTO,'+ #13 +
    '      vFormacaoCusto.PERC_OUTRAS_DESP,'+ #13 +
    '      vFormacaoCusto.PERC_OUTROS_CUSTOS,'+ #13 +
    '      vFormacaoCusto.PERC_IPI,'+ #13 +
    '      vFormacaoCusto.PERC_DIFAL'+ #13 +
    '    );'+ #13 +
    #13 +
    '  end loop; '+ #13 +
    #13 +
    'end ATUALIZAR_CUSTOS_ENTRADA;'+ #13
end;

function AjustarProcedureVOLTAR_CUSTOS_ENTRADA: string;
begin
  Result :=
    'create or replace procedure VOLTAR_CUSTOS_ENTRADA(iENTRADA_ID in number)'+ #13 +
    'is'+ #13 +
    #13 +
    '  vUltimaEntradaId       ENTRADAS_NOTAS_FISCAIS.ENTRADA_ID%type;'+ #13 +
    '  vEmpresaId             ENTRADAS_NOTAS_FISCAIS.EMPRESA_ID%type;'+ #13 +
    '  vAtualizarCustoEntrada ENTRADAS_NOTAS_FISCAIS.ATUALIZAR_CUSTO_ENTRADA%type;'+ #13 +
    #13 +
    '  cursor cItens is'+ #13 +
    '  select'+ #13 +
    '    ITE.PRODUTO_ID,'+ #13 +
    '    CFO.ENTRADA_MERCADORIAS_BONIFIC,'+ #13 +
    '    ITE.QUANTIDADE_ENTRADA_ALTIS as QUANTIDADE,'+ #13 +
    '    ITE.ENTRADA_ANTERIOR_ID,'+ #13 +
    '    ITE.PRECO_TABELA_ANTERIOR,'+ #13 +
    '    ITE.PERCENTUAL_DESCONTO_ENTR_ANT,'+ #13 +
    '    ITE.PERCENTUAL_IPI_ENTRADA_ANT,'+ #13 +
    '    ITE.PERCENTUAL_OUTRAS_DESP_ENT_ANT,'+ #13 +
    '    ITE.PERCENTUAL_FRETE_ENTR_ANTERIOR,'+ #13 +
    '    ITE.PERCENTUAL_ST_ENTRADA_ANTERIOR,'+ #13 +
    '    ITE.PERCENTUAL_ICMS_ENTR_ANTERIOR,'+ #13 +
    '    ITE.PERCENTUAL_ICMS_FRETE_ENTR_ANT,'+ #13 +
    '    ITE.PERCENTUAL_PIS_ENTRADA_ANTER,'+ #13 +
    '    ITE.PERCENTUAL_COFINS_ENT_ANTERIOR,'+ #13 +
    '    /*PERCENTUAL_PIS_FRETE_ENT_ANT    = vFormacaoCusto.PRECO_TABELA,'+ #13 +
    '    PERCENTUAL_COF_FRETE_ENT_ANT    = vFormacaoCusto.PRECO_TABELA,*/'+ #13 +
    '    ITE.PERCENTUAL_OUT_CUS_ENTR_ANTER,'+ #13 +
    '    --PERCENTUAL_CUSTO_FIXO_ENT_ANT   = vFormacaoCusto.PERC_IPI,'+ #13 +
    '    ITE.PERCENTUAL_DIFAL_ENTRADA_ANTER,'+ #13 +
    '    ITE.PRECO_FINAL,'+ #13 +
    '    ITE.PRECO_FINAL_ANTERIOR,'+ #13 +
    '    ITE.PRECO_FINAL_MEDIO_ANTERIOR,'+ #13 +
    '    ITE.PRECO_LIQUIDO_ANTERIOR,'+ #13 +
    '    ITE.PRECO_LIQUIDO_MEDIO_ANTERIOR,'+ #13 +
    '    ITE.CMV_ANTERIOR'+ #13 +
    '  from'+ #13 +
    '    ENTRADAS_NOTAS_FISCAIS_ITENS ITE'+ #13 +
    #13 +
    '  inner join CFOP CFO'+ #13 +
    '  on ITE.CFOP_ID = CFO.CFOP_PESQUISA_ID'+ #13 +
    #13 +
    '  where ITE.ENTRADA_ID = iENTRADA_ID;'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  select'+ #13 +
    '    EMPRESA_ID,'+ #13 +
    '    ATUALIZAR_CUSTO_ENTRADA'+ #13 +
    '  into'+ #13 +
    '    vEmpresaId,'+ #13 +
    '    vAtualizarCustoEntrada'+ #13 +
    '  from'+ #13 +
    '    ENTRADAS_NOTAS_FISCAIS'+ #13 +
    '  where ENTRADA_ID = iENTRADA_ID;'+ #13 +
    #13 +
    '  /* Se n�o atualizou custos, saindo do procedimento */'+ #13 +
    '  if vAtualizarCustoEntrada = ''N'' then'+ #13 +
    '    return;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  for xItens in cItens loop'+ #13 +
    '    select'+ #13 +
    '      ULTIMA_ENTRADA_ID'+ #13 +
    '    into'+ #13 +
    '      vUltimaEntradaId'+ #13 +
    '    from'+ #13 +
    '      ESTOQUES'+ #13 +
    '    where EMPRESA_ID = vEmpresaId'+ #13 +
    '    and PRODUTO_ID = xItens.PRODUTO_ID;'+ #13 +
    #13 +
    '    if xItens.ENTRADA_MERCADORIAS_BONIFIC = ''S'' then'+ #13 +
    '      ATUALIZAR_CMV(vEmpresaId, xItens.PRODUTO_ID, 0, xItens.QUANTIDADE * -1);'+ #13 +
    '      continue;'+ #13 +
    '    else'+ #13 +
    '      ATUALIZAR_CMV(vEmpresaId, xItens.PRODUTO_ID, xItens.PRECO_FINAL, xItens.QUANTIDADE * -1);'+ #13 +
    '    end if;'+ #13 +
    #13 +
    '    /* Se n�o for a ultima entrada apenas voltando o CMV */'+ #13 +
    '    if vUltimaEntradaId <> iENTRADA_ID then      '+ #13 +
    '      continue;'+ #13 +
    '    end if;'+ #13 +
    #13 +
    '    update CUSTOS_PRODUTOS set'+ #13 +
    '      PRECO_TABELA                   = xItens.PRECO_TABELA_ANTERIOR,'+ #13 +
    '      PERCENTUAL_ICMS_ENTRADA        = xItens.PERCENTUAL_ICMS_ENTR_ANTERIOR,'+ #13 +
    '      PERCENTUAL_ST_ENTRADA          = xItens.PERCENTUAL_ST_ENTRADA_ANTERIOR,'+ #13 +
    '      PERCENTUAL_FRETE_ENTRADA       = xItens.PERCENTUAL_FRETE_ENTR_ANTERIOR,'+ #13 +
    '      PERCENTUAL_ICMS_FRETE_ENTRADA  = xItens.PERCENTUAL_ICMS_FRETE_ENTR_ANT,'+ #13 +
    '      PERCENTUAL_PIS_ENTRADA         = xItens.PERCENTUAL_PIS_ENTRADA_ANTER,'+ #13 +
    '      PERCENTUAL_COFINS_ENTRADA      = xItens.PERCENTUAL_COFINS_ENT_ANTERIOR,'+ #13 +
    '      PERCENTUAL_DESCONTO_ENTRADA    = xItens.PERCENTUAL_DESCONTO_ENTR_ANT,'+ #13 +
    '      PERCENTUAL_OUTRAS_DESPESAS_ENT = xItens.PERCENTUAL_OUTRAS_DESP_ENT_ANT,'+ #13 +
    '      PERCENTUAL_OUTROS_CUSTOS_ENTR  = xItens.PERCENTUAL_OUT_CUS_ENTR_ANTER,'+ #13 +
    '      PERCENTUAL_IPI_ENTRADA         = xItens.PERCENTUAL_IPI_ENTRADA_ANT,'+ #13 +
    '      PERCENTUAL_DIFAL_ENTRADA       = xItens.PERCENTUAL_DIFAL_ENTRADA_ANTER,'+ #13 +
    '    /*PERCENTUAL_PIS_FRETE_ENT_ANT    = PRECO_TABELA,'+ #13 +
    '      PERCENTUAL_COF_FRETE_ENT_ANT    = PRECO_TABELA,*/'+ #13 +
    #13 +
    '      PRECO_FINAL                    = xItens.PRECO_FINAL_ANTERIOR,'+ #13 +
    '      PRECO_FINAL_MEDIO              = xItens.PRECO_FINAL_MEDIO_ANTERIOR,      '+ #13 +
    #13 +
    '      PRECO_LIQUIDO                  = xItens.PRECO_LIQUIDO_ANTERIOR,'+ #13 +
    '      PRECO_LIQUIDO_MEDIO            = xItens.PRECO_LIQUIDO_MEDIO_ANTERIOR,'+ #13 +
    #13 +
    '      ORIGEM                         = case when xItens.ENTRADA_ANTERIOR_ID is null then ''NEN'' else ''ENT'' end,'+ #13 +
    '      ORIGEM_ID                      = xItens.ENTRADA_ANTERIOR_ID'+ #13 +
    '    where ORIGEM = ''ENT'''+ #13 +
    '    and ORIGEM_ID = iENTRADA_ID'+ #13 +
    '    and EMPRESA_ID = vEmpresaId'+ #13 +
    '    and PRODUTO_ID = xItens.PRODUTO_ID;'+ #13 +
    #13 +
    '  end loop;'+ #13 +
    #13 +
    'end VOLTAR_CUSTOS_ENTRADA;'+ #13
end;

function Atualizando_VW_ENTREGAS: string;
begin
  Result :=
    'create or replace view VW_ENTREGAS'+ #13 +
    'as'+ #13 +
    'select'+ #13 +
    '  RET.TIPO_MOVIMENTO,'+ #13 +
    '  RET.RETIRADA_ID as MOVIMENTO_ID,'+ #13 +
    '  RET.DATA_HORA_CADASTRO,'+ #13 +
    '  RET.LOCAL_ID,'+ #13 +
    '  LOC.NOME as NOME_LOCAL,'+ #13 +
    '  RET.EMPRESA_ID,'+ #13 +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA,'+ #13 +
    '  RET.ORCAMENTO_ID,'+ #13 +
    '  ORC.VENDEDOR_ID,'+ #13 +
    '  FVE.NOME as NOME_VENDEDOR,'+ #13 +
    '  PES.QTDE_PRODUTOS,'+ #13 +
    '  PES.PESO_TOTAL,'+ #13 +
    '  ORC.CLIENTE_ID,'+ #13 +
    '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
    '  RET.USUARIO_CADASTRO_ID,'+ #13 +
    '  FCA.NOME as NOME_USUARIO_CADASTRO,'+ #13 +
    '  RET.USUARIO_CONFIRMACAO_ID,'+ #13 +
    '  FCO.NOME as NOME_USUARIO_CONFIRMACAO,'+ #13 +
    '  RET.DATA_HORA_RETIRADA as DATA_HORA_ENTREGA,'+ #13 +
    '  RET.NOME_PESSOA_RETIRADA as NOME_PESSOA_RECEBEU'+ #13 +
    'from'+ #13 +
    '  RETIRADAS RET'+ #13 +
    #13 +
    'inner join LOCAIS_PRODUTOS LOC'+ #13 +
    'on RET.LOCAL_ID = LOC.LOCAL_ID'+ #13 +
    #13 +
    'inner join EMPRESAS EMP'+ #13 +
    'on RET.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join FUNCIONARIOS FCA'+ #13 +
    'on RET.USUARIO_CADASTRO_ID = FCA.FUNCIONARIO_ID'+ #13 +
    #13 +
    'left join FUNCIONARIOS FCO'+ #13 +
    'on RET.USUARIO_CONFIRMACAO_ID = FCO.FUNCIONARIO_ID'+ #13 +
    #13 +
    'inner join ('+ #13 +
    '  select'+ #13 +
    '    ITE.RETIRADA_ID,'+ #13 +
    '    count(*) as QTDE_PRODUTOS,'+ #13 +
    '    sum(ITE.QUANTIDADE * PRO.PESO) as PESO_TOTAL'+ #13 +
    '  from'+ #13 +
    '    RETIRADAS_ITENS ITE'+ #13 +
    '  inner join PRODUTOS PRO'+ #13 +
    '  on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    '  group by'+ #13 +
    '    ITE.RETIRADA_ID'+ #13 +
    ') PES'+ #13 +
    'on RET.RETIRADA_ID = PES.RETIRADA_ID'+ #13 +
    #13 +
    'left join FUNCIONARIOS FVE'+ #13 +
    'on ORC.VENDEDOR_ID = FVE.FUNCIONARIO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    'select'+ #13 +
    '  ''E'' as TIPO_MOVIMENTO,'+ #13 +
    '  ENT.ENTREGA_ID as MOVIMENTO_ID,'+ #13 +
    '  ENT.DATA_HORA_CADASTRO,'+ #13 +
    '  ENT.LOCAL_ID,'+ #13 +
    '  LOC.NOME as NOME_LOCAL,'+ #13 +
    '  ENT.EMPRESA_ENTREGA_ID as EMPRESA_ID,'+ #13 +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA,'+ #13 +
    '  ENT.ORCAMENTO_ID,'+ #13 +
    '  ORC.VENDEDOR_ID,'+ #13 +
    '  FVE.NOME as NOME_VENDEDOR,'+ #13 +
    '  PES.QTDE_PRODUTOS,'+ #13 +
    '  PES.PESO_TOTAL,'+ #13 +
    '  ORC.CLIENTE_ID,'+ #13 +
    '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
    '  ENT.USUARIO_CADASTRO_ID,'+ #13 +
    '  FCA.NOME as NOME_USUARIO_CADASTRO,'+ #13 +
    '  ENT.USUARIO_CONFIRMACAO_ID,'+ #13 +
    '  FCO.NOME as NOME_USUARIO_CONFIRMACAO,'+ #13 +
    '  ENT.DATA_HORA_REALIZOU_ENTREGA as DATA_HORA_ENTREGA,'+ #13 +
    '  ENT.NOME_PESSOA_RECEBEU_ENTREGA as NOME_PESSOA_RECEBEU'+ #13 +
    'from'+ #13 +
    '  ENTREGAS ENT'+ #13 +
    #13 +
    'inner join LOCAIS_PRODUTOS LOC'+ #13 +
    'on ENT.LOCAL_ID = LOC.LOCAL_ID'+ #13 +
    #13 +
    'inner join EMPRESAS EMP'+ #13 +
    'on ENT.EMPRESA_ENTREGA_ID = EMP.EMPRESA_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join FUNCIONARIOS FCA'+ #13 +
    'on ENT.USUARIO_CADASTRO_ID = FCA.FUNCIONARIO_ID'+ #13 +
    #13 +
    'left join FUNCIONARIOS FCO'+ #13 +
    'on ENT.USUARIO_CONFIRMACAO_ID = FCO.FUNCIONARIO_ID'+ #13 +
    #13 +
    'inner join ('+ #13 +
    '  select'+ #13 +
    '    ITE.ENTREGA_ID,'+ #13 +
    '    count(*) as QTDE_PRODUTOS,'+ #13 +
    '    sum(ITE.QUANTIDADE * PRO.PESO) as PESO_TOTAL'+ #13 +
    '  from'+ #13 +
    '    ENTREGAS_ITENS ITE'+ #13 +
    '  inner join PRODUTOS PRO'+ #13 +
    '  on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    '  group by'+ #13 +
    '    ITE.ENTREGA_ID'+ #13 +
    ') PES'+ #13 +
    'on ENT.ENTREGA_ID = PES.ENTREGA_ID'+ #13 +
    #13 +
    'left join FUNCIONARIOS FVE'+ #13 +
    'on ORC.VENDEDOR_ID = FVE.FUNCIONARIO_ID;'+ #13
end;

function Atualizando_VW_RELACAO_VENDAS_PRODUTOS: string;
begin
  Result :=
    'create or replace view VW_RELACAO_VENDAS_PRODUTOS'+ #13 +
    'as'+ #13 +
    'select'+ #13 +
    '  MOV.TIPO_MOVIMENTO,'+ #13 +
    '  MOV.CLIENTE_ID,'+ #13 +
    '  MOV.MOVIMENTO_ID,'+ #13 +
    '  MOV.DATA_HORA_MOVIMENTO,'+ #13 +
    '  MOV.EMPRESA_ID,'+ #13 +
    '  MOV.PRODUTO_ID,'+ #13 +
    '  PRO.NOME as NOME_PRODUTO,'+ #13 +
    '  PRO.MARCA_ID,'+ #13 +
    '  MAR.NOME as NOME_MARCA,'+ #13 +
    '  PRO.LINHA_PRODUTO_ID,'+ #13 +
    '  PRO.FORNECEDOR_ID,'+ #13 +
    '  FRN.GRUPO_FORNECEDOR_ID,'+ #13 +
    '  PRO.UNIDADE_VENDA,'+ #13 +
    '  MOV.QUANTIDADE,'+ #13 +
    '  MOV.VALOR_LIQUIDO,'+ #13 +
    '  MOV.VENDEDOR_ID,'+ #13 +
    '  MOV.NOME_VENDEDOR,'+ #13 +
    '  MOV.VALOR_LUCRO_BRUTO'+ #13 +
    'from ('+ #13 +
    '  select'+ #13 +
    '    ''VEN'' as TIPO_MOVIMENTO,'+ #13 +
    '	ORC.CLIENTE_ID,'+ #13 +
    '    ORC.ORCAMENTO_ID as MOVIMENTO_ID,'+ #13 +
    '    ORC.DATA_HORA_RECEBIMENTO as DATA_HORA_MOVIMENTO,'+ #13 +
    '    ORC.EMPRESA_ID,'+ #13 +
    '    ITE.PRODUTO_ID,'+ #13 +
    '    ITE.QUANTIDADE,'+ #13 +
    '    ITE.VALOR_TOTAL - VALOR_TOTAL_DESCONTO + ITE.VALOR_TOTAL_OUTRAS_DESPESAS + ITE.VALOR_TOTAL_FRETE AS VALOR_LIQUIDO,'+ #13 +
    '    ORC.VENDEDOR_ID,'+ #13 +
    '    FVE.NOME as NOME_VENDEDOR,'+ #13 +
    '	('+ #13 +
    '	  ITE.VALOR_TOTAL - '+ #13 +
    '	  case PAE.TIPO_CUSTO_VIS_LUCRO_VENDA '+ #13 +
    '	    when ''FIN'' then zvl(ITE.PRECO_LIQUIDO, ITE.CUSTO_ULTIMO_PEDIDO) '+ #13 +
    '	    when ''COM'' then zvl(ITE.CUSTO_ULTIMO_PEDIDO, ITE.PRECO_LIQUIDO) '+ #13 +
    '	    else ITE.CMV end '+ #13 +
    '	) as VALOR_LUCRO_BRUTO'+ #13 +
    '  from'+ #13 +
    '    ORCAMENTOS_ITENS ITE'+ #13 +
    #13 +
    '  inner join ORCAMENTOS ORC'+ #13 +
    '  on ITE.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    '  inner join PARAMETROS_EMPRESA PAE'+ #13 +
    '  on ORC.EMPRESA_ID = PAE.EMPRESA_ID'+ #13 +
    #13 +
    '  left join FUNCIONARIOS FVE'+ #13 +
    '  on ORC.VENDEDOR_ID = FVE.FUNCIONARIO_ID'+ #13 +
    #13 +
    '  where ORC.STATUS = ''RE'''+ #13 +
    '  and (ITE.TIPO_CONTROLE_ESTOQUE in(''K'', ''A'') or ITE.ITEM_KIT_ID is null)'+ #13 +
    #13 +
    '  union all'+ #13 +
    #13 +
    '  select'+ #13 +
    '    ''DEV'' as TIPO_MOVIMENTO,'+ #13 +
    '	ORC.CLIENTE_ID,'+ #13 +
    '    DEV.DEVOLUCAO_ID as MOVIMENTO_ID,'+ #13 +
    '    DEV.DATA_HORA_DEVOLUCAO as DATA_HORA_MOVIMENTO,'+ #13 +
    '    DEV.EMPRESA_ID,'+ #13 +
    '    ITE.PRODUTO_ID,'+ #13 +
    '    ITE.DEVOLVIDOS_ENTREGUES + ITE.DEVOLVIDOS_PENDENCIAS + ITE.DEVOLVIDOS_SEM_PREVISAO as QUANTIDADE,'+ #13 +
    '    ITE.VALOR_LIQUIDO,'+ #13 +
    '    ORC.VENDEDOR_ID,'+ #13 +
    '    FVE.NOME as NOME_VENDEDOR,'+ #13 +
    '    0 as VALOR_LUCRO_BRUTO	'+ #13 +
    '  from'+ #13 +
    '    DEVOLUCOES_ITENS ITE'+ #13 +
    #13 +
    '  inner join DEVOLUCOES DEV'+ #13 +
    '  on ITE.DEVOLUCAO_ID = DEV.DEVOLUCAO_ID'+ #13 +
    #13 +
    '  inner join ORCAMENTOS ORC'+ #13 +
    '  on DEV.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    '  inner join ORCAMENTOS_ITENS OIT'+ #13 +
    '  on DEV.ORCAMENTO_ID = OIT.ORCAMENTO_ID'+ #13 +
    '  and ITE.ITEM_ID = OIT.ITEM_ID'+ #13 +
    #13 +
    '  left join FUNCIONARIOS FVE'+ #13 +
    '  on ORC.VENDEDOR_ID = FVE.FUNCIONARIO_ID'+ #13 +
    #13 +
    '  where DEV.STATUS = ''B'''+ #13 +
    '  and (OIT.TIPO_CONTROLE_ESTOQUE in(''K'', ''A'') or OIT.ITEM_KIT_ID is null)'+ #13 +
    #13 +
    ') MOV'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on MOV.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'inner join MARCAS MAR'+ #13 +
    'on PRO.MARCA_ID = MAR.MARCA_ID'+ #13 +
    #13 +
    'inner join FORNECEDORES FRN'+ #13 +
    'on PRO.FORNECEDOR_ID = FRN.CADASTRO_ID;'+ #13
end;

function Criando_Coluna_QTDE_MAXIMA_DIAS_DEVOLUCAO_Tabela_PARAMETROS: string;
begin
  Result :=
    'alter table PARAMETROS add(QTDE_MAXIMA_DIAS_DEVOLUCAO number(3) default 30 not null);'+ #13
end;

function Criando_CK_PAR_QTDE_MAX_DIAS_DEVOLUCAO: string;
begin
  Result :=
    'alter table PARAMETROS'+ #13 +
    'add constraint CK_PAR_QTDE_MAX_DIAS_DEVOLUCAO'+ #13 +
    'check(QTDE_MAXIMA_DIAS_DEVOLUCAO > 0);'+ #13
end;

function DroparColunaESTACAO_EMISSAO_NOTA: string;
begin
  Result :=
    'alter table NOTAS_FISCAIS'+ #13 +
    'drop column ESTACAO_EMISSAO_NOTA;'+ #13
end;

function AjustarTriggerNOTAS_FISCAIS_IU_BR: string;
begin
  Result :=
    'create or replace trigger NOTAS_FISCAIS_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on NOTAS_FISCAIS'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '  if inserting then'+ #13 +
    '    :new.CHAVE_NFE := RETORNA_NUMEROS(:new.CHAVE_NFE);'+ #13 +
    '  else'+ #13 +
    '    if :new.STATUS = ''C'' then'+ #13 +
    '      :new.DATA_HORA_CANCELAMENTO_NOTA := sysdate;'+ #13 +
    '      return;'+ #13 +
    '    end if;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end NOTAS_FISCAIS_IU_BR;'+ #13
end;

function AjustarTriggerNOTAS_FISCAIS_U_AR: string;
begin
  Result :=
    'create or replace trigger NOTAS_FISCAIS_U_AR'+ #13 +
    'after update'+ #13 +
    'on NOTAS_FISCAIS'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    #13 +
    '  procedure INSERIR_LOG(pTIPO_ALTERACAO_LOG_ID in number, pVALOR_ANTERIOR in string, pNOVO_VALOR in string)'+ #13 +
    '  is begin'+ #13 +
    '    if nvl(pVALOR_ANTERIOR, ''X'') <> nvl(pNOVO_VALOR, ''X'') then'+ #13 +
    '      insert into LOGS_NOTAS_FISCAIS(TIPO_ALTERACAO_LOG_ID, NOTA_FISCAL_ID, VALOR_ANTERIOR, NOVO_VALOR)'+ #13 +
    '      values(pTIPO_ALTERACAO_LOG_ID, :new.NOTA_FISCAL_ID, pVALOR_ANTERIOR, pNOVO_VALOR);'+ #13 +
    '    end if;'+ #13 +
    '  end;'+ #13 +
    #13 +
    'begin'+ #13 +
    '  /* View VW_TIPOS_ALTER_LOGS_NTF */'+ #13 +
    #13 +
    '  INSERIR_LOG(1, :old.DANFE_IMPRESSO, :new.DANFE_IMPRESSO);'+ #13 +
    '  INSERIR_LOG(2, :old.ENVIOU_EMAIL_NFE_CLIENTE, :new.ENVIOU_EMAIL_NFE_CLIENTE);'+ #13 +
    '  INSERIR_LOG(3, :old.STATUS, :new.STATUS);'+ #13 +
    '  INSERIR_LOG(4, :old.NUMERO_NOTA, :new.NUMERO_NOTA);'+ #13 +
    '  INSERIR_LOG(5, :old.MODELO_NOTA, :new.MODELO_NOTA);'+ #13 +
    '  INSERIR_LOG(6, :old.SERIE_NOTA, :new.SERIE_NOTA);'+ #13 +
    '  INSERIR_LOG(7, :old.CFOP_ID, :new.CFOP_ID);'+ #13 +
    '  INSERIR_LOG(8, :old.INSCRICAO_ESTADUAL_DESTINAT, :new.INSCRICAO_ESTADUAL_DESTINAT);'+ #13 +
    '/*  9 est� disponivel. */'+ #13 +
    '  INSERIR_LOG(10, :old.LOGRADOURO_DESTINATARIO, :new.LOGRADOURO_DESTINATARIO);'+ #13 +
    '  INSERIR_LOG(11, :old.COMPLEMENTO_DESTINATARIO, :new.COMPLEMENTO_DESTINATARIO);'+ #13 +
    '  INSERIR_LOG(12, :old.NUMERO_DESTINATARIO, :new.NUMERO_DESTINATARIO);'+ #13 +
    '  INSERIR_LOG(13, :old.NOME_BAIRRO_DESTINATARIO, :new.NOME_BAIRRO_DESTINATARIO);'+ #13 +
    '  INSERIR_LOG(14, :old.CEP_DESTINATARIO, :new.CEP_DESTINATARIO);'+ #13 +
    '  INSERIR_LOG(15, :old.CHAVE_NFE, :new.CHAVE_NFE);'+ #13 +
    '  INSERIR_LOG(16, :old.BASE_CALCULO_ICMS, :new.BASE_CALCULO_ICMS);'+ #13 +
    '  INSERIR_LOG(17, :old.VALOR_ICMS, :new.VALOR_ICMS);'+ #13 +
    '  INSERIR_LOG(18, :old.BASE_CALCULO_ICMS_ST, :new.BASE_CALCULO_ICMS_ST);'+ #13 +
    '  INSERIR_LOG(19, :old.VALOR_ICMS_ST, :new.VALOR_ICMS_ST);'+ #13 +
    '  INSERIR_LOG(20, :old.VALOR_DESCONTO, :new.VALOR_DESCONTO);'+ #13 +
    '  INSERIR_LOG(21, :old.VALOR_TOTAL_PRODUTOS, :new.VALOR_TOTAL_PRODUTOS);'+ #13 +
    '  INSERIR_LOG(22, :old.VALOR_FRETE, :new.VALOR_FRETE);'+ #13 +
    '  INSERIR_LOG(23, :old.VALOR_SEGURO, :new.VALOR_SEGURO);'+ #13 +
    '  INSERIR_LOG(24, :old.VALOR_OUTRAS_DESPESAS, :new.VALOR_OUTRAS_DESPESAS);'+ #13 +
    '  INSERIR_LOG(25, :old.VALOR_TOTAL, :new.VALOR_TOTAL);'+ #13 +
    #13 +
    'end NOTAS_FISCAIS_U_AR;'+ #13
end;

function LimparLogsNOTAS_FISCAIS_TIPO9: string;
begin
  Result :=
    'delete from LOGS_NOTAS_FISCAIS'+ #13 +
    'where TIPO_ALTERACAO_LOG_ID = 9;'+ #13
end;

function AdicionarNovasColunasEnderecoEntregaNOTAS_FISCAIS: string;
begin
  Result :=
    'alter table NOTAS_FISCAIS'+ #13 +
    'add ('+ #13 +
    '  INSCRICAO_EST_ENTR_DESTINAT   varchar2(20),'+ #13 +
    '  LOGRADOURO_ENTR_DESTINATARIO  varchar2(100),'+ #13 +
    '  COMPLEMENTO_ENTR_DESTINATARIO varchar2(100),'+ #13 +
    '  NOME_BAIRRO_ENTR_DESTINATARIO varchar2(60),'+ #13 +
    '  NOME_CIDADE_ENTR_DESTINATARIO varchar2(60),'+ #13 +
    '  NUMERO_ENTR_DESTINATARIO      varchar2(10),'+ #13 +
    '  ESTADO_ID_ENTR_DESTINATARIO   char(2),'+ #13 +
    '  CEP_ENTR_DESTINATARIO         varchar2(9),'+ #13 +
    '  CODIGO_IBGE_MUNIC_ENTR_DEST   number(7)'+ #13 +
    ');'+ #13
end;

function AjustarFunctionBUSCAR_ENDERECO_EMISSAO_NF: string;
begin
  Result :=
    'create or replace function BUSCAR_ENDERECO_EMISSAO_NF(iMOVIMENTO_ID in number, iTIPO_MOVIMENTO in string)'+ #13 +
    'return RecordsNotasFiscais.RecEnderecoEmissaoNotaFiscal'+ #13 +
    'is '+ #13 +
    '  vUtilizarEnderecoMovCapa  char(1) default ''S'';'+ #13 +
    '  vRetorno              RecordsNotasFiscais.RecEnderecoEmissaoNotaFiscal;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  if iTIPO_MOVIMENTO not in(''ORC'', ''ACU'') then'+ #13 +
    '    ERRO(''O tipo de movimento s� pode ser "ORC" e "ACU"!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if iTIPO_MOVIMENTO = ''ORC'' then'+ #13 +
    #13 +
    '    select'+ #13 +
    '      case'+ #13 +
    '        when ORC.CLIENTE_ID = PAR.CADASTRO_CONSUMIDOR_FINAL_ID then ''N'''+ #13 +
    '        when ORC.INSCRICAO_ESTADUAL is not null then ''S''        '+ #13 +
    '        when CAD.TIPO_PESSOA = ''F'' and ORC.BAIRRO_ID is not null then ''S'''+ #13 +
    '        else ''N'''+ #13 +
    '      end'+ #13 +
    '    into'+ #13 +
    '      vUtilizarEnderecoMovCapa'+ #13 +
    '    from'+ #13 +
    '      ORCAMENTOS ORC'+ #13 +
    #13 +
    '    inner join CADASTROS CAD'+ #13 +
    '    on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    '    cross join PARAMETROS PAR'+ #13 +
    #13 +
    '    where ORC.ORCAMENTO_ID = iMOVIMENTO_ID;'+ #13 +
    #13 +
    '    select'+ #13 +
    '      case when vUtilizarEnderecoMovCapa = ''S'' then ORC.INSCRICAO_ESTADUAL else CAD.INSCRICAO_ESTADUAL end,'+ #13 +
    '      case when vUtilizarEnderecoMovCapa = ''S'' then ORC.LOGRADOURO else CAD.LOGRADOURO end,'+ #13 +
    '      case when vUtilizarEnderecoMovCapa = ''S'' then ORC.COMPLEMENTO else CAD.COMPLEMENTO end,'+ #13 +
    '      case when vUtilizarEnderecoMovCapa = ''S'' then ORC.NUMERO else CAD.NUMERO end,'+ #13 +
    '      case when vUtilizarEnderecoMovCapa = ''S'' then ORC.CEP else CAD.CEP end,'+ #13 +
    '      BAC.NOME_BAIRRO,'+ #13 +
    '      BAC.NOME_CIDADE,'+ #13 +
    '      BAC.CODIGO_IBGE_CIDADE,'+ #13 +
    '      BAC.ESTADO_ID,'+ #13 +
    #13 +
    '      ORC.INSCRICAO_ESTADUAL,'+ #13 +
    '      ORC.LOGRADOURO,'+ #13 +
    '      ORC.COMPLEMENTO,'+ #13 +
    '      ORC.NUMERO,'+ #13 +
    '      ORC.CEP,'+ #13 +
    '      BEN.NOME_BAIRRO,'+ #13 +
    '      BEN.NOME_CIDADE,'+ #13 +
    '      BEN.CODIGO_IBGE_CIDADE,'+ #13 +
    '      BEN.ESTADO_ID'+ #13 +
    '    into'+ #13 +
    '      vRetorno.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
    '      vRetorno.LOGRADOURO_DESTINATARIO,'+ #13 +
    '      vRetorno.COMPLEMENTO_DESTINATARIO,'+ #13 +
    '      vRetorno.NUMERO_DESTINATARIO,'+ #13 +
    '      vRetorno.CEP_DESTINATARIO,'+ #13 +
    '      vRetorno.NOME_BAIRRO_DESTINATARIO,'+ #13 +
    '      vRetorno.NOME_CIDADE_DESTINATARIO,'+ #13 +
    '      vRetorno.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
    '      vRetorno.ESTADO_ID_DESTINATARIO,'+ #13 +
    #13 +
    '      vRetorno.INSCRICAO_EST_ENTR_DESTINAT,'+ #13 +
    '      vRetorno.LOGRADOURO_ENTR_DESTINATARIO,'+ #13 +
    '      vRetorno.COMPLEMENTO_ENTR_DESTINATARIO,'+ #13 +
    '      vRetorno.NUMERO_ENTR_DESTINATARIO,'+ #13 +
    '      vRetorno.CEP_ENTR_DESTINATARIO,'+ #13 +
    '      vRetorno.NOME_BAIRRO_ENTR_DESTINATARIO,'+ #13 +
    '      vRetorno.NOME_CIDADE_ENTR_DESTINATARIO,      '+ #13 +
    '      vRetorno.CODIGO_IBGE_MUNIC_ENTR_DEST,'+ #13 +
    '      vRetorno.ESTADO_ID_ENTR_DESTINATARIO'+ #13 +
    '    from'+ #13 +
    '      ORCAMENTOS ORC    '+ #13 +
    #13 +
    '    inner join CADASTROS CAD'+ #13 +
    '    on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    '    inner join VW_BAIRROS BAC'+ #13 +
    '    on case when vUtilizarEnderecoMovCapa = ''S'' then ORC.BAIRRO_ID else CAD.BAIRRO_ID end = BAC.BAIRRO_ID'+ #13 +
    #13 +
    '    left join VW_BAIRROS BEN'+ #13 +
    '    on ORC.BAIRRO_ID = BEN.BAIRRO_ID'+ #13 +
    #13 +
    '    where ORC.ORCAMENTO_ID = iMOVIMENTO_ID;'+ #13 +
    #13 +
    '    if vUtilizarEnderecoMovCapa = ''S'' then'+ #13 +
    '      vRetorno.ENDERECO_INFO_COMPLEMENTARES :='+ #13 +
    '        ''Local de entrega: '' || vRetorno.LOGRADOURO_DESTINATARIO || '' '' || vRetorno.COMPLEMENTO_DESTINATARIO || '' '' || '' Nr '' || vRetorno.NUMERO_DESTINATARIO || '','' ||'+ #13 +
    '         vRetorno.NOME_BAIRRO_DESTINATARIO || '' - '' || vRetorno.NOME_CIDADE_DESTINATARIO || ''/'' || vRetorno.ESTADO_ID_DESTINATARIO || '' CEP: '' || vRetorno.CEP_DESTINATARIO;'+ #13 +
    '    end if;'+ #13 +
    #13 +
    '  else'+ #13 +
    #13 +
    '    select'+ #13 +
    '      case'+ #13 +
    '        when ACU.INSCRICAO_ESTADUAL is not null then ''S'''+ #13 +
    '        when CAD.TIPO_PESSOA = ''F'' and ACU.BAIRRO_ID is not null then ''S'''+ #13 +
    '        else ''N'''+ #13 +
    '      end'+ #13 +
    '    into'+ #13 +
    '      vUtilizarEnderecoMovCapa'+ #13 +
    '    from'+ #13 +
    '      ACUMULADOS ACU'+ #13 +
    #13 +
    '    inner join CADASTROS CAD'+ #13 +
    '    on ACU.CLIENTE_ID = CAD.CADASTRO_ID;'+ #13 +
    #13 +
    '    select'+ #13 +
    '      case when vUtilizarEnderecoMovCapa = ''S'' then ACU.INSCRICAO_ESTADUAL else CAD.INSCRICAO_ESTADUAL end,'+ #13 +
    '      case when vUtilizarEnderecoMovCapa = ''S'' then ACU.LOGRADOURO else CAD.LOGRADOURO end,'+ #13 +
    '      case when vUtilizarEnderecoMovCapa = ''S'' then ACU.COMPLEMENTO else CAD.COMPLEMENTO end,'+ #13 +
    '      case when vUtilizarEnderecoMovCapa = ''S'' then ACU.NUMERO else CAD.NUMERO end,'+ #13 +
    '      case when vUtilizarEnderecoMovCapa = ''S'' then ACU.CEP else CAD.CEP end,'+ #13 +
    '      BAC.NOME_BAIRRO,'+ #13 +
    '      BAC.NOME_CIDADE,'+ #13 +
    '      BAC.CODIGO_IBGE_CIDADE,'+ #13 +
    '      BAC.ESTADO_ID'+ #13 +
    '    into     '+ #13 +
    '      vRetorno.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
    '      vRetorno.LOGRADOURO_DESTINATARIO,'+ #13 +
    '      vRetorno.COMPLEMENTO_DESTINATARIO,'+ #13 +
    '      vRetorno.NUMERO_DESTINATARIO,'+ #13 +
    '      vRetorno.CEP_DESTINATARIO,'+ #13 +
    '      vRetorno.NOME_BAIRRO_DESTINATARIO,'+ #13 +
    '      vRetorno.NOME_CIDADE_DESTINATARIO,'+ #13 +
    '      vRetorno.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
    '      vRetorno.ESTADO_ID_DESTINATARIO      '+ #13 +
    '    from'+ #13 +
    '      ACUMULADOS ACU'+ #13 +
    #13 +
    '    inner join CADASTROS CAD'+ #13 +
    '    on ACU.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    '    inner join VW_BAIRROS BAC'+ #13 +
    '    on case when vUtilizarEnderecoMovCapa = ''S'' then ACU.BAIRRO_ID else CAD.BAIRRO_ID end = BAC.BAIRRO_ID'+ #13 +
    #13 +
    '    where ACU.ACUMULADO_ID = iMOVIMENTO_ID;'+ #13 +
    #13 +
    '  end if;'+ #13 +
    #13 +
    '  return vRetorno;'+ #13 +
    #13 +
    'end BUSCAR_ENDERECO_EMISSAO_NF;'+ #13
end;

function AtualizarPackageRecordsNotasFiscais: string;
begin
  Result :=
    'create or replace package RecordsNotasFiscais'+ #13 +
    'as'+ #13 +
    #13 +
    '  type RecNotaFiscal is record('+ #13 +
    '    -- Dados do or�amento ---------------------------------------------------'+ #13 +
    '    CADASTRO_ID                 NOTAS_FISCAIS.CADASTRO_ID%type,'+ #13 +
    '    CFOP_ID                     NOTAS_FISCAIS.CFOP_ID%type,'+ #13 +
    '    EMPRESA_ID                  NOTAS_FISCAIS.EMPRESA_ID%type,'+ #13 +
    '    ORCAMENTO_ID                NOTAS_FISCAIS.ORCAMENTO_ID%type,'+ #13 +
    '    ACUMULADO_ID                NOTAS_FISCAIS.ACUMULADO_ID%type,'+ #13 +
    '    DEVOLUCAO_ID                NOTAS_FISCAIS.DEVOLUCAO_ID%type,'+ #13 +
    '    NUMERO_NOTA                 NOTAS_FISCAIS.NUMERO_NOTA%type,'+ #13 +
    '    /* Dados da empresa emitente da NF */'+ #13 +
    '    RAZAO_SOCIAL_EMITENTE       NOTAS_FISCAIS.RAZAO_SOCIAL_EMITENTE%type,'+ #13 +
    '    NOME_FANTASIA_EMITENTE      NOTAS_FISCAIS.NOME_FANTASIA_EMITENTE%type,    '+ #13 +
    '    CNPJ_EMITENTE               NOTAS_FISCAIS.CNPJ_EMITENTE%type,'+ #13 +
    '    INSCRICAO_ESTADUAL_EMITENTE NOTAS_FISCAIS.INSCRICAO_ESTADUAL_EMITENTE%type,'+ #13 +
    '    LOGRADOURO_EMITENTE         NOTAS_FISCAIS.LOGRADOURO_EMITENTE%type,'+ #13 +
    '    COMPLEMENTO_EMITENTE        NOTAS_FISCAIS.COMPLEMENTO_EMITENTE%type,'+ #13 +
    '    NOME_BAIRRO_EMITENTE        NOTAS_FISCAIS.NOME_BAIRRO_EMITENTE%type,'+ #13 +
    '    NOME_CIDADE_EMITENTE        NOTAS_FISCAIS.NOME_CIDADE_EMITENTE%type,'+ #13 +
    '    NUMERO_EMITENTE             NOTAS_FISCAIS.NUMERO_EMITENTE%type,'+ #13 +
    '    ESTADO_ID_EMITENTE          NOTAS_FISCAIS.ESTADO_ID_EMITENTE%type,'+ #13 +
    '    CEP_EMITENTE                NOTAS_FISCAIS.CEP_EMITENTE%type,'+ #13 +
    '    TELEFONE_EMITENTE           NOTAS_FISCAIS.TELEFONE_EMITENTE%type,'+ #13 +
    '    CODIGO_IBGE_MUNICIPIO_EMIT  NOTAS_FISCAIS.CODIGO_IBGE_MUNICIPIO_EMIT%type,'+ #13 +
    '    CODIGO_IBGE_ESTADO_EMITENT  NOTAS_FISCAIS.CODIGO_IBGE_ESTADO_EMITENT%type,    '+ #13 +
    '    /* Dados do cliente */'+ #13 +
    '    NOME_FANTASIA_DESTINATARIO  NOTAS_FISCAIS.NOME_FANTASIA_DESTINATARIO%type,'+ #13 +
    '    RAZAO_SOCIAL_DESTINATARIO   NOTAS_FISCAIS.RAZAO_SOCIAL_DESTINATARIO%type,'+ #13 +
    '    TIPO_PESSOA_DESTINATARIO    NOTAS_FISCAIS.TIPO_PESSOA_DESTINATARIO%type,'+ #13 +
    '    CPF_CNPJ_DESTINATARIO       NOTAS_FISCAIS.CPF_CNPJ_DESTINATARIO%type,'+ #13 +
    '    INSCRICAO_ESTADUAL_DESTINAT NOTAS_FISCAIS.INSCRICAO_ESTADUAL_DESTINAT%type,'+ #13 +
    '    LOGRADOURO_DESTINATARIO     NOTAS_FISCAIS.LOGRADOURO_DESTINATARIO%type,'+ #13 +
    '    COMPLEMENTO_DESTINATARIO    NOTAS_FISCAIS.COMPLEMENTO_DESTINATARIO%type,'+ #13 +
    '    NOME_BAIRRO_DESTINATARIO    NOTAS_FISCAIS.NOME_BAIRRO_DESTINATARIO%type,'+ #13 +
    '    NOME_CIDADE_DESTINATARIO    NOTAS_FISCAIS.NOME_CIDADE_DESTINATARIO%type,'+ #13 +
    '    NUMERO_DESTINATARIO         NOTAS_FISCAIS.NUMERO_DESTINATARIO%type,'+ #13 +
    '    ESTADO_ID_DESTINATARIO      NOTAS_FISCAIS.ESTADO_ID_DESTINATARIO%type,'+ #13 +
    '    CEP_DESTINATARIO            NOTAS_FISCAIS.CEP_DESTINATARIO%type,'+ #13 +
    '    CODIGO_IBGE_MUNICIPIO_DEST  NOTAS_FISCAIS.CODIGO_IBGE_MUNICIPIO_DEST%type,'+ #13 +
    '    TIPO_CLIENTE                CLIENTES.TIPO_CLIENTE%type,'+ #13 +
    #13 +
    '    NOME_CONSUMIDOR_FINAL       NOTAS_FISCAIS.NOME_CONSUMIDOR_FINAL%type,'+ #13 +
    '    TELEFONE_CONSUMIDOR_FINAL   NOTAS_FISCAIS.TELEFONE_CONSUMIDOR_FINAL%type,'+ #13 +
    '    NATUREZA_OPERACAO           NOTAS_FISCAIS.NATUREZA_OPERACAO%type,'+ #13 +
    #13 +
    '    /* Valores da Nota fiscal */'+ #13 +
    '    TOTAL_IMPOSTOS_ESTADUAL     number(8,2) default 0,'+ #13 +
    '    TOTAL_IMPOSTOS_FEDERAL      number(8,2) default 0,'+ #13 +
    '    VALOR_TOTAL                 NOTAS_FISCAIS.VALOR_TOTAL%type default 0,'+ #13 +
    '    VALOR_TOTAL_PRODUTOS        NOTAS_FISCAIS.VALOR_TOTAL_PRODUTOS%type default 0,'+ #13 +
    '    VALOR_DESCONTO              NOTAS_FISCAIS.VALOR_DESCONTO%type default 0,'+ #13 +
    '    VALOR_OUTRAS_DESPESAS       NOTAS_FISCAIS.VALOR_OUTRAS_DESPESAS%type default 0,'+ #13 +
    '    VALOR_FRETE                 NOTAS_FISCAIS.VALOR_FRETE%type default 0,'+ #13 +
    '    VALOR_SEGURO                NOTAS_FISCAIS.VALOR_SEGURO%type default 0,'+ #13 +
    '    BASE_CALCULO_ICMS           NOTAS_FISCAIS.BASE_CALCULO_ICMS%type default 0,'+ #13 +
    '    VALOR_ICMS                  NOTAS_FISCAIS.VALOR_ICMS%type default 0,'+ #13 +
    '    BASE_CALCULO_ICMS_ST        NOTAS_FISCAIS.BASE_CALCULO_ICMS_ST%type default 0,'+ #13 +
    '    VALOR_ICMS_ST               NOTAS_FISCAIS.VALOR_ICMS_ST%type default 0,'+ #13 +
    '    BASE_CALCULO_PIS            NOTAS_FISCAIS.BASE_CALCULO_PIS%type default 0,'+ #13 +
    '    VALOR_PIS                   NOTAS_FISCAIS.VALOR_PIS%type default 0,'+ #13 +
    '    BASE_CALCULO_COFINS         NOTAS_FISCAIS.BASE_CALCULO_COFINS%type default 0,'+ #13 +
    '    VALOR_COFINS                NOTAS_FISCAIS.VALOR_COFINS%type default 0,'+ #13 +
    '    VALOR_IPI                   NOTAS_FISCAIS.VALOR_IPI%type default 0,'+ #13 +
    #13 +
    '    PESO_LIQUIDO                NOTAS_FISCAIS.PESO_LIQUIDO%type default 0,'+ #13 +
    '    PESO_BRUTO                  NOTAS_FISCAIS.PESO_BRUTO%type default 0,'+ #13 +
    #13 +
    '    VALOR_RECEBIDO_DINHEIRO     NOTAS_FISCAIS.VALOR_RECEBIDO_DINHEIRO%type default 0,'+ #13 +
    '    VALOR_RECEBIDO_CARTAO_CRED  NOTAS_FISCAIS.VALOR_RECEBIDO_CARTAO_CRED%type default 0,'+ #13 +
    '    VALOR_RECEBIDO_CARTAO_DEB   NOTAS_FISCAIS.VALOR_RECEBIDO_CARTAO_DEB%type default 0,'+ #13 +
    '    VALOR_RECEBIDO_CREDITO      NOTAS_FISCAIS.VALOR_RECEBIDO_CREDITO%type default 0,'+ #13 +
    '    VALOR_RECEBIDO_COBRANCA     NOTAS_FISCAIS.VALOR_RECEBIDO_COBRANCA%type default 0,'+ #13 +
    '    VALOR_RECEBIDO_CHEQUE       NOTAS_FISCAIS.VALOR_RECEBIDO_CHEQUE%type default 0,'+ #13 +
    '    VALOR_RECEBIDO_FINANCEIRA   NOTAS_FISCAIS.VALOR_RECEBIDO_FINANCEIRA%type default 0,'+ #13 +
    #13 +
    '    INFORMACOES_COMPLEMENTARES  NOTAS_FISCAIS.INFORMACOES_COMPLEMENTARES%type'+ #13 +
    '  );  '+ #13 +
    #13 +
    '  type RecNotasFiscaisItens is record('+ #13 +
    '    PRODUTO_ID                  PRODUTOS.PRODUTO_ID%type,'+ #13 +
    '    ITEM_ID                     NOTAS_FISCAIS_ITENS.ITEM_ID%type,'+ #13 +
    '    NOME_PRODUTO                PRODUTOS.NOME%type,'+ #13 +
    '    CST                         NOTAS_FISCAIS_ITENS.CST%type,'+ #13 +
    '    UNIDADE                     NOTAS_FISCAIS_ITENS.UNIDADE%type,'+ #13 +
    '    CFOP_ID                     NOTAS_FISCAIS_ITENS.CFOP_ID%type,'+ #13 +
    '    CODIGO_NCM                  NOTAS_FISCAIS_ITENS.CODIGO_NCM%type,'+ #13 +
    '    VALOR_TOTAL                 NOTAS_FISCAIS_ITENS.VALOR_TOTAL%type,'+ #13 +
    '    PRECO_UNITARIO              NOTAS_FISCAIS_ITENS.PRECO_UNITARIO%type,'+ #13 +
    '    QUANTIDADE                  NOTAS_FISCAIS_ITENS.QUANTIDADE%type,'+ #13 +
    '    VALOR_TOTAL_DESCONTO        NOTAS_FISCAIS_ITENS.VALOR_TOTAL_DESCONTO%type,'+ #13 +
    '    VALOR_TOTAL_OUTRAS_DESPESAS NOTAS_FISCAIS_ITENS.VALOR_TOTAL_OUTRAS_DESPESAS%type,'+ #13 +
    '    VALOR_TOTAL_FRETE           ORCAMENTOS.VALOR_FRETE%type,'+ #13 +
    '    BASE_CALCULO_ICMS           NOTAS_FISCAIS_ITENS.BASE_CALCULO_ICMS%type,'+ #13 +
    '    INDICE_REDUCAO_BASE_ICMS    NOTAS_FISCAIS_ITENS.INDICE_REDUCAO_BASE_ICMS%type,'+ #13 +
    '    PERCENTUAL_ICMS             NOTAS_FISCAIS_ITENS.PERCENTUAL_ICMS%type,'+ #13 +
    '    VALOR_ICMS                  NOTAS_FISCAIS_ITENS.VALOR_ICMS%type,'+ #13 +
    '    INDICE_REDUCAO_BASE_ICMS_ST NOTAS_FISCAIS_ITENS.INDICE_REDUCAO_BASE_ICMS_ST%type,'+ #13 +
    '    BASE_CALCULO_ICMS_ST        NOTAS_FISCAIS_ITENS.BASE_CALCULO_ICMS_ST%type,'+ #13 +
    '    PERCENTUAL_ICMS_ST          NOTAS_FISCAIS_ITENS.PERCENTUAL_ICMS_ST%type,'+ #13 +
    '    VALOR_ICMS_ST               NOTAS_FISCAIS_ITENS.VALOR_ICMS_ST%type,'+ #13 +
    '    CST_PIS                     NOTAS_FISCAIS_ITENS.CST_PIS%type,'+ #13 +
    '    CST_COFINS                  NOTAS_FISCAIS_ITENS.CST_COFINS%type,'+ #13 +
    '    BASE_CALCULO_PIS            NOTAS_FISCAIS_ITENS.BASE_CALCULO_PIS%type,'+ #13 +
    '    PERCENTUAL_PIS              NOTAS_FISCAIS_ITENS.PERCENTUAL_PIS%type,'+ #13 +
    '    VALOR_PIS                   NOTAS_FISCAIS_ITENS.VALOR_PIS%type,'+ #13 +
    '    IVA                         NOTAS_FISCAIS_ITENS.IVA%type,'+ #13 +
    '    BASE_CALCULO_COFINS         NOTAS_FISCAIS_ITENS.BASE_CALCULO_COFINS%type,'+ #13 +
    '    PERCENTUAL_COFINS           NOTAS_FISCAIS_ITENS.PERCENTUAL_COFINS%type,'+ #13 +
    '    VALOR_COFINS                NOTAS_FISCAIS_ITENS.VALOR_COFINS%type,'+ #13 +
    '    VALOR_IPI                   NOTAS_FISCAIS_ITENS.VALOR_IPI%type,'+ #13 +
    '    PERCENTUAL_IPI              NOTAS_FISCAIS_ITENS.PERCENTUAL_IPI%type,'+ #13 +
    '    PRECO_PAUTA                 NOTAS_FISCAIS_ITENS.PRECO_PAUTA%type,'+ #13 +
    '    CODIGO_BARRAS               NOTAS_FISCAIS_ITENS.CODIGO_BARRAS%type,'+ #13 +
    '		NATUREZA_OPERACAO           NOTAS_FISCAIS.NATUREZA_OPERACAO%type,'+ #13 +
    '    CEST                        NOTAS_FISCAIS_ITENS.CEST%type'+ #13 +
    '  );  '+ #13 +
    #13 +
    '  /* --------- Declara��o dos dos tipos acima na forma de Arrays ------------- */  '+ #13 +
    #13 +
    '  type ArrayOfRecNotasFiscaisItens is table of RecNotasFiscaisItens index by naturaln;'+ #13 +
    #13 +
    '  type RecImpostosCalculados is record('+ #13 +
    '    BASE_CALCULO_ICMS           NOTAS_FISCAIS_ITENS.BASE_CALCULO_ICMS%type,'+ #13 +
    '    INDICE_REDUCAO_BASE_ICMS    NOTAS_FISCAIS_ITENS.INDICE_REDUCAO_BASE_ICMS%type,'+ #13 +
    '    PERCENTUAL_ICMS             NOTAS_FISCAIS_ITENS.PERCENTUAL_ICMS%type,'+ #13 +
    '    VALOR_ICMS                  NOTAS_FISCAIS_ITENS.VALOR_ICMS%type,'+ #13 +
    '    INDICE_REDUCAO_BASE_ICMS_ST NOTAS_FISCAIS_ITENS.INDICE_REDUCAO_BASE_ICMS_ST%type,'+ #13 +
    '    BASE_CALCULO_ICMS_ST        NOTAS_FISCAIS_ITENS.BASE_CALCULO_ICMS_ST%type,'+ #13 +
    '    PERCENTUAL_ICMS_ST          NOTAS_FISCAIS_ITENS.PERCENTUAL_ICMS_ST%type,'+ #13 +
    '    VALOR_ICMS_ST               NOTAS_FISCAIS_ITENS.VALOR_ICMS_ST%type,'+ #13 +
    '    BASE_CALCULO_PIS            NOTAS_FISCAIS_ITENS.BASE_CALCULO_PIS%type,'+ #13 +
    '    PERCENTUAL_PIS              NOTAS_FISCAIS_ITENS.PERCENTUAL_PIS%type,'+ #13 +
    '    VALOR_PIS                   NOTAS_FISCAIS_ITENS.VALOR_PIS%type,'+ #13 +
    '    IVA                         NOTAS_FISCAIS_ITENS.IVA%type,'+ #13 +
    '    BASE_CALCULO_COFINS         NOTAS_FISCAIS_ITENS.BASE_CALCULO_COFINS%type,'+ #13 +
    '    PERCENTUAL_COFINS           NOTAS_FISCAIS_ITENS.PERCENTUAL_COFINS%type,'+ #13 +
    '    VALOR_COFINS                NOTAS_FISCAIS_ITENS.VALOR_COFINS%type,'+ #13 +
    '    VALOR_IPI                   NOTAS_FISCAIS_ITENS.VALOR_IPI%type,'+ #13 +
    '    PERCENTUAL_IPI              NOTAS_FISCAIS_ITENS.PERCENTUAL_IPI%type,'+ #13 +
    '    PRECO_PAUTA                 NOTAS_FISCAIS_ITENS.PRECO_PAUTA%type'+ #13 +
    '  );'+ #13 +
    #13 +
    '  type RecDescontoConcedido is record('+ #13 +
    '    IndiceDescontoVendaId   INDICES_DESCONTOS_VENDA.INDICE_ID%type,'+ #13 +
    '    PercentualDesconto      INDICES_DESCONTOS_VENDA.PERCENTUAL_DESCONTO%type,'+ #13 +
    '    PrecoCusto              INDICES_DESCONTOS_VENDA.PRECO_CUSTO%type,'+ #13 +
    '    TipoCusto               INDICES_DESCONTOS_VENDA.TIPO_CUSTO%type,'+ #13 +
    '    TipoDescontoPrecoCusto  INDICES_DESCONTOS_VENDA.TIPO_DESCONTO_PRECO_CUSTO%type,'+ #13 +
    '    ValorTotalCusto         number(8,2) default 0'+ #13 +
    '  );'+ #13 +
    #13 +
    '  type RecEnderecoEmissaoNotaFiscal is record(    '+ #13 +
    '    INSCRICAO_ESTADUAL_DESTINAT   NOTAS_FISCAIS.INSCRICAO_ESTADUAL_DESTINAT%type,'+ #13 +
    '    LOGRADOURO_DESTINATARIO       NOTAS_FISCAIS.LOGRADOURO_DESTINATARIO%type,'+ #13 +
    '    COMPLEMENTO_DESTINATARIO      NOTAS_FISCAIS.COMPLEMENTO_DESTINATARIO%type,'+ #13 +
    '    NUMERO_DESTINATARIO           NOTAS_FISCAIS.NUMERO_DESTINATARIO%type,'+ #13 +
    '    CEP_DESTINATARIO              NOTAS_FISCAIS.CEP_DESTINATARIO%type,'+ #13 +
    '    NOME_BAIRRO_DESTINATARIO      NOTAS_FISCAIS.NOME_BAIRRO_DESTINATARIO%type,'+ #13 +
    '    NOME_CIDADE_DESTINATARIO      NOTAS_FISCAIS.NOME_CIDADE_DESTINATARIO%type,'+ #13 +
    '    CODIGO_IBGE_MUNICIPIO_DEST    NOTAS_FISCAIS.CODIGO_IBGE_MUNICIPIO_DEST%type,'+ #13 +
    '    ESTADO_ID_DESTINATARIO        NOTAS_FISCAIS.ESTADO_ID_DESTINATARIO%type,'+ #13 +
    #13 +
    '    INSCRICAO_EST_ENTR_DESTINAT   NOTAS_FISCAIS.INSCRICAO_EST_ENTR_DESTINAT%type,'+ #13 +
    '    LOGRADOURO_ENTR_DESTINATARIO  NOTAS_FISCAIS.LOGRADOURO_ENTR_DESTINATARIO%type,'+ #13 +
    '    COMPLEMENTO_ENTR_DESTINATARIO NOTAS_FISCAIS.COMPLEMENTO_ENTR_DESTINATARIO%type,'+ #13 +
    '    NOME_BAIRRO_ENTR_DESTINATARIO NOTAS_FISCAIS.NOME_BAIRRO_ENTR_DESTINATARIO%type,'+ #13 +
    '    NOME_CIDADE_ENTR_DESTINATARIO NOTAS_FISCAIS.NOME_CIDADE_ENTR_DESTINATARIO%type,'+ #13 +
    '    NUMERO_ENTR_DESTINATARIO      NOTAS_FISCAIS.NUMERO_ENTR_DESTINATARIO%type,'+ #13 +
    '    ESTADO_ID_ENTR_DESTINATARIO   NOTAS_FISCAIS.ESTADO_ID_ENTR_DESTINATARIO%type,'+ #13 +
    '    CEP_ENTR_DESTINATARIO         NOTAS_FISCAIS.CEP_ENTR_DESTINATARIO%type,'+ #13 +
    '    CODIGO_IBGE_MUNIC_ENTR_DEST   NOTAS_FISCAIS.CODIGO_IBGE_MUNIC_ENTR_DEST%type,'+ #13 +
    #13 +
    '    ENDERECO_INFO_COMPLEMENTARES  NOTAS_FISCAIS.INFORMACOES_COMPLEMENTARES%type'+ #13 +
    '  );'+ #13 +
    #13 +
    'end RecordsNotasFiscais;'+ #13
end;

function AjustarProcedureGERAR_NOTA_ENTREGA: string;
  function GetComando1: string;
  begin
    Result :=
      'create or replace procedure GERAR_NOTA_ENTREGA(iENTREGA_ID in number)'+ #13 +
      'is'+ #13 +
      '  vIndice                 number default 0;'+ #13 +
      '  vIndiceDescVenda        number default 1;'+ #13 +
      '  vValorTotalPedido       ORCAMENTOS.VALOR_TOTAL%type;'+ #13 +
      #13 +
      '  i                       number default 0;'+ #13 +
      '  vQtde                   number default 0;'+ #13 +
      '  vValorMaisSignificativo number default 0;'+ #13 +
      '  vNotaFiscalId           NOTAS_FISCAIS.NOTA_FISCAL_ID%type;'+ #13 +
      '  vTipoNotaGerar          varchar2(2) default ''NE'';'+ #13 +
      #13 +
      '  vModeloNota             NOTAS_FISCAIS.MODELO_NOTA%type;'+ #13 +
      '  vSerieNota              NOTAS_FISCAIS.SERIE_NOTA%type;'+ #13 +
      '  vAcumulado              CONDICOES_PAGAMENTO.ACUMULATIVO%type;'+ #13 +
      #13 +
      '  vOrcamentoId            ENTREGAS.ORCAMENTO_ID%type;'+ #13 +
      '  vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;'+ #13 +
      '  vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;'+ #13 +
      '  vDadosDescontoVenda     RecordsNotasFiscais.RecDescontoConcedido;'+ #13 +
      '  vDadosNota              RecordsNotasFiscais.RecNotaFiscal;'+ #13 +
      '  vDadosImpItens          RecordsNotasFiscais.RecImpostosCalculados;'+ #13 +
      '  vDadosItens             RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;'+ #13 +
      #13 +
      '  vRegimeTributario       PARAMETROS_EMPRESA.REGIME_TRIBUTARIO%type;'+ #13 +
      '  vSerieNFCe              PARAMETROS_EMPRESA.SERIE_NFCE%type;'+ #13 +
      '  vSerieNFe               PARAMETROS_EMPRESA.SERIE_NFE%type;'+ #13 +
      #13 +
      '  vEnderecoCapa           RecordsNotasFiscais.RecEnderecoEmissaoNotaFiscal;'+ #13 +
      #13 +
      '  cursor cItens(pEstadoId in string, pTipoCliente in string, pEmpresaNotaId in number) is'+ #13 +
      '  select'+ #13 +
      '    ITE.PRODUTO_ID,'+ #13 +
      '    ITE.ITEM_ID,'+ #13 +
      '    ITE.PRECO_UNITARIO,'+ #13 +
      '    EIT.QUANTIDADE,'+ #13 +
      '    ITE.VALOR_TOTAL / ITE.QUANTIDADE * EIT.QUANTIDADE as VALOR_TOTAL,'+ #13 +
      '    ITE.VALOR_TOTAL_DESCONTO / ITE.QUANTIDADE * EIT.QUANTIDADE as VALOR_TOTAL_DESCONTO,'+ #13 +
      '    ITE.VALOR_TOTAL_OUTRAS_DESPESAS / ITE.QUANTIDADE * EIT.QUANTIDADE as VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '    ITE.VALOR_TOTAL_FRETE / ITE.QUANTIDADE * EIT.QUANTIDADE as VALOR_TOTAL_FRETE,'+ #13 +
      '    CUS.CUSTO_ULTIMO_PEDIDO,'+ #13 +
      '    CUS.PRECO_LIQUIDO,'+ #13 +
      '    CUS.CMV,'+ #13 +
      '    PRO.NOME as NOME_PRODUTO,'+ #13 +
      '    PRO.CODIGO_NCM,'+ #13 +
      '    PRO.UNIDADE_VENDA,'+ #13 +
      '    PRO.CODIGO_BARRAS,'+ #13 +
      '    PRO.CEST,'+ #13 +
      '    nvl(IMP.PERC_ESTADUAL, 0) as PERC_ESTADUAL,'+ #13 +
      '    nvl(IMP.PERC_FEDERAL_NACIONAL, 0) as PERC_FEDERAL,'+ #13 +
      '    nvl(IMP.PERC_MUNICIPAL, 0) as PERC_MUNICIPAL'+ #13 +
      '  from'+ #13 +
      '    ('+ #13 +
      '      /* Este select � feito desta forma por causa dos LOTES! */'+ #13 +
      '      select'+ #13 +
      '        ENTREGA_ID,'+ #13 +
      '        PRODUTO_ID,'+ #13 +
      '        ITEM_ID,'+ #13 +
      '        sum(QUANTIDADE - NAO_SEPARADOS) as QUANTIDADE'+ #13 +
      '      from'+ #13 +
      '        ENTREGAS_ITENS'+ #13 +
      '      where ENTREGA_ID = iENTREGA_ID'+ #13 +
      '      group by'+ #13 +
      '        ENTREGA_ID,'+ #13 +
      '        PRODUTO_ID,'+ #13 +
      '        ITEM_ID'+ #13 +
      '      having sum(QUANTIDADE - NAO_SEPARADOS) > 0'+ #13 +
      '    ) EIT'+ #13 +
      #13 +
      '  inner join ENTREGAS ENT'+ #13 +
      '  on EIT.ENTREGA_ID = ENT.ENTREGA_ID'+ #13 +
      #13 +
      '  inner join ORCAMENTOS_ITENS ITE'+ #13 +
      '  on ENT.ORCAMENTO_ID = ITE.ORCAMENTO_ID'+ #13 +
      '  and EIT.PRODUTO_ID = ITE.PRODUTO_ID'+ #13 +
      '  and EIT.ITEM_ID = ITE.ITEM_ID'+ #13 +
      #13 +
      '  inner join PRODUTOS PRO'+ #13 +
      '  on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      #13 +
      '	inner join ORCAMENTOS ORC'+ #13 +
      '	on ITE.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
      #13 +
      '  inner join VW_CUSTOS_PRODUTOS CUS'+ #13 +
      '  on EIT.PRODUTO_ID = CUS.PRODUTO_ID'+ #13 +
      '  and CUS.EMPRESA_ID = pEmpresaNotaId  '+ #13 +
      #13 +
      '  left join IMPOSTOS_IBPT IMP'+ #13 +
      '  on PRO.CODIGO_NCM = IMP.CODIGO_NCM'+ #13 +
      '  and IMP.ESTADO_ID = pEstadoId'+ #13 +
      #13 +
      '  where EIT.ENTREGA_ID = iENTREGA_ID'+ #13 +
      '  /* N�o trazer os produtos KITS */'+ #13 +
      '  and ITE.TIPO_CONTROLE_ESTOQUE not in(''K'', ''A'')'+ #13 +
      '  order by'+ #13 +
      '    ITE.ITEM_ID;'+ #13 +
      #13 +
      'begin'+ #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    NOTAS_FISCAIS'+ #13 +
      '  where ENTREGA_ID = iENTREGA_ID'+ #13 +
      '  and NUMERO_NOTA is not null;'+ #13 +
      #13 +
      '  if vQtde > 0 then'+ #13 +
      '    return;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    NOTAS_FISCAIS'+ #13 +
      '  where ENTREGA_ID = iENTREGA_ID'+ #13 +
      '  and NUMERO_NOTA is null;'+ #13 +
      #13 +
      '  if vQtde > 0 then'+ #13 +
      '    ERRO(''A entrega j� gerou nota!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    CON.ACUMULATIVO,'+ #13 +
      '    IDV.INDICE_ID,'+ #13 +
      '    IDV.PERCENTUAL_DESCONTO,'+ #13 +
      '    IDV.PRECO_CUSTO,'+ #13 +
      '    IDV.TIPO_CUSTO,'+ #13 +
      '    IDV.TIPO_DESCONTO_PRECO_CUSTO'+ #13 +
      '  into'+ #13 +
      '    vAcumulado,'+ #13 +
      '    vDadosDescontoVenda.IndiceDescontoVendaId,'+ #13 +
      '    vDadosDescontoVenda.PercentualDesconto,'+ #13 +
      '    vDadosDescontoVenda.PrecoCusto,'+ #13 +
      '    vDadosDescontoVenda.TipoCusto,'+ #13 +
      '    vDadosDescontoVenda.TipoDescontoPrecoCusto'+ #13 +
      '  from'+ #13 +
      '    ORCAMENTOS ORC'+ #13 +
      #13 +
      '  inner join ENTREGAS ENT'+ #13 +
      '  on ORC.ORCAMENTO_ID = ENT.ORCAMENTO_ID'+ #13 +
      #13 +
      '  inner join CONDICOES_PAGAMENTO CON'+ #13 +
      '  on ORC.CONDICAO_ID = CON.CONDICAO_ID'+ #13 +
      #13 +
      '  left join INDICES_DESCONTOS_VENDA IDV'+ #13 +
      '  on ORC.INDICE_DESCONTO_VENDA_ID = IDV.INDICE_ID'+ #13 +
      #13 +
      '  where ENT.ENTREGA_ID = iENTREGA_ID;'+ #13 +
      #13 +
      '  if vAcumulado = ''S'' then'+ #13 +
      '    return;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  if vDadosDescontoVenda.IndiceDescontoVendaId is not null then'+ #13 +
      '    if vDadosDescontoVenda.PrecoCusto = ''N'' then'+ #13 +
      '      vIndiceDescVenda := 1 - vDadosDescontoVenda.PercentualDesconto * 0.01;'+ #13 +
      '    else'+ #13 +
      '      if vDadosDescontoVenda.TipoDescontoPrecoCusto = ''A'' then'+ #13 +
      '        vIndiceDescVenda := 1 + vDadosDescontoVenda.PercentualDesconto * 0.01;'+ #13 +
      '      elsif vDadosDescontoVenda.TipoDescontoPrecoCusto = ''D'' then'+ #13 +
      '        vIndiceDescVenda := 1 - vDadosDescontoVenda.PercentualDesconto * 0.01;'+ #13 +
      '      else'+ #13 +
      '        vIndiceDescVenda := 1;'+ #13 +
      '      end if;'+ #13 +
      '    end if;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    sum((OIT.VALOR_TOTAL - OIT.VALOR_TOTAL_DESCONTO + OIT.VALOR_TOTAL_OUTRAS_DESPESAS) / OIT.QUANTIDADE * EIT.QUANTIDADE) / ORC.VALOR_TOTAL * vIndiceDescVenda as INDICE,'+ #13 +
      '    ORC.VALOR_TOTAL'+ #13 +
      '  into'+ #13 +
      '    vIndice,'+ #13 +
      '    vValorTotalPedido'+ #13 +
      '  from'+ #13 +
      '    ENTREGAS_ITENS EIT'+ #13 +
      #13 +
      '  inner join ENTREGAS ENT'+ #13 +
      '  on EIT.ENTREGA_ID = ENT.ENTREGA_ID'+ #13 +
      #13 +
      '  inner join ORCAMENTOS_ITENS OIT'+ #13 +
      '  on ENT.ORCAMENTO_ID = OIT.ORCAMENTO_ID'+ #13 +
      '  and EIT.ITEM_ID = OIT.ITEM_ID'+ #13 +
      '  and OIT.TIPO_CONTROLE_ESTOQUE <> ''K'' -- Removendo o produto principal para n�o duplicar os valores'+ #13 +
      #13 +
      '  inner join ORCAMENTOS ORC'+ #13 +
      '  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
      #13 +
      '  where EIT.ENTREGA_ID = iENTREGA_ID'+ #13 +
      '  group by'+ #13 +
      '    ORC.VALOR_TOTAL;'+ #13 +
      #13 +
      '  select'+ #13 +
      '    ENT.ORCAMENTO_ID,'+ #13 +
      '    ENT.TIPO_NOTA_GERAR,'+ #13 +
      '    ENT.EMPRESA_ENTREGA_ID,'+ #13 +
      '    ORC.CLIENTE_ID'+ #13 +
      '  into'+ #13 +
      '    vOrcamentoId,'+ #13 +
      '    vTipoNotaGerar,'+ #13 +
      '    vDadosNota.EMPRESA_ID,'+ #13 +
      '    vDadosNota.CADASTRO_ID'+ #13 +
      '  from'+ #13 +
      '    ENTREGAS ENT'+ #13 +
      #13 +
      '  inner join ORCAMENTOS ORC'+ #13 +
      '  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
      #13 +
      '  where ENT.ENTREGA_ID = iENTREGA_ID;'+ #13 +
      #13 +
      '  /* Buscando a empresa do redirecionamento de nota fiscal */'+ #13 +
      '  vDadosNota.EMPRESA_ID := BUSCAR_EMPRESA_EMISSAO_NOTA(vDadosNota.EMPRESA_ID, vDadosNota.CADASTRO_ID, ''E'');'+ #13 +
      #13 +
      '  begin'+ #13 +
      '    select'+ #13 +
      '      ORC.ORCAMENTO_ID,'+ #13 +
      '      ORC.CLIENTE_ID,'+ #13 +
      '      ORC.VALOR_DINHEIRO,'+ #13 +
      '      nvl(CAR.VALOR_CARTAO_CREDITO, 0) as VALOR_CARTAO_CREDITO,'+ #13 +
      '      nvl(CAR.VALOR_CARTAO_DEBITO, 0) as VALOR_CARTAO_DEBITO,'+ #13 +
      '      ORC.VALOR_CREDITO,'+ #13 +
      '      ORC.VALOR_COBRANCA,'+ #13 +
      '      ORC.VALOR_CHEQUE,'+ #13 +
      '      ORC.VALOR_FINANCEIRA,'+ #13 +
      '      ORC.NOME_CONSUMIDOR_FINAL,'+ #13 +
      #13 +
      '      /* Emitente */'+ #13 +
      '      EMP.RAZAO_SOCIAL,'+ #13 +
      '      EMP.NOME_FANTASIA,'+ #13 +
      '      EMP.CNPJ,'+ #13 +
      '      EMP.INSCRICAO_ESTADUAL,'+ #13 +
      '      EMP.LOGRADOURO,'+ #13 +
      '      EMP.COMPLEMENTO,'+ #13 +
      '      BAE.NOME as NOME_BAIRRO_EMITENTE,'+ #13 +
      '      CIE.NOME as NOME_CIDADE_EMITENTE,'+ #13 +
      '      EMP.NUMERO,'+ #13 +
      '      ESE.ESTADO_ID,'+ #13 +
      '      EMP.CEP,'+ #13 +
      '      EMP.TELEFONE_PRINCIPAL,'+ #13 +
      '      CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      #13 +
      '      /* Destinatario */'+ #13 +
      '      CAD.NOME_FANTASIA,'+ #13 +
      '      CAD.RAZAO_SOCIAL,'+ #13 +
      '      CAD.TIPO_PESSOA,'+ #13 +
      '      CAD.CPF_CNPJ,'+ #13 +
      #13 +
      '      CLI.TIPO_CLIENTE,'+ #13 +
      '      PAE.INF_COMPL_DOCS_ELETRONICOS || '' '' || ORC.OBSERVACOES_NFE'+ #13 +
      '    into'+ #13 +
      '      vDadosNota.ORCAMENTO_ID,'+ #13 +
      '      vDadosNota.CADASTRO_ID,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CREDITO,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '      vDadosNota.VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '      vDadosNota.NOME_CONSUMIDOR_FINAL,'+ #13 +
      #13 +
      '      /* Emitente */'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_FANTASIA_EMITENTE,'+ #13 +
      '      vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '      vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '      vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '      vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '      vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '      vDadosNota.CEP_EMITENTE,'+ #13 +
      '      vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      #13 +
      '      /* Destinatario */'+ #13 +
      '      vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '      vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '      vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      #13 +
      '      vDadosNota.TIPO_CLIENTE,'+ #13 +
      '      vDadosNota.INFORMACOES_COMPLEMENTARES'+ #13 +
      '    from'+ #13 +
      '      ENTREGAS ENT'+ #13 +
      #13 +
      '    inner join ORCAMENTOS ORC'+ #13 +
      '    on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
      #13 +
      '    inner join CADASTROS CAD'+ #13 +
      '    on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
      #13 +
      '    inner join CLIENTES CLI'+ #13 +
      '    on ORC.CLIENTE_ID = CLI.CADASTRO_ID'+ #13 +
      #13 +
      '    inner join EMPRESAS EMP'+ #13 +
      '    on EMP.EMPRESA_ID = vDadosNota.EMPRESA_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAE'+ #13 +
      '    on EMP.BAIRRO_ID = BAE.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CIE'+ #13 +
      '    on BAE.CIDADE_ID = CIE.CIDADE_ID'+ #13 +
      #13 +
      '    inner join ESTADOS ESE'+ #13 +
      '    on CIE.ESTADO_ID = ESE.ESTADO_ID'+ #13 +
      #13 +
      '    inner join PARAMETROS_EMPRESA PAE'+ #13 +
      '    on PAE.EMPRESA_ID = vDadosNota.EMPRESA_ID'+ #13 +
      #13 +
      '    left join ('+ #13 +
      '      select'+ #13 +
      '        OPG.ORCAMENTO_ID,'+ #13 +
      '        sum(case when TPG.TIPO_CARTAO = ''C'' then OPG.VALOR else 0 end) as VALOR_CARTAO_CREDITO,'+ #13 +
      '        sum(case when TPG.TIPO_CARTAO = ''D'' then OPG.VALOR else 0 end) as VALOR_CARTAO_DEBITO'+ #13 +
      '      from'+ #13 +
      '        ORCAMENTOS_PAGAMENTOS OPG'+ #13 +
      #13 +
      '      inner join TIPOS_COBRANCA TPG'+ #13 +
      '      on OPG.COBRANCA_ID = TPG.COBRANCA_ID'+ #13 +
      #13 +
      '      where OPG.TIPO = ''CR'''+ #13 +
      '      and OPG.ORCAMENTO_ID = vOrcamentoId'+ #13 +
      #13 +
      '      group by'+ #13 +
      '        OPG.ORCAMENTO_ID'+ #13 +
      '    ) CAR'+ #13 +
      '    on ORC.ORCAMENTO_ID = CAR.ORCAMENTO_ID'+ #13 +
      #13 +
      '    cross join PARAMETROS PAR'+ #13 +
      #13 +
      '    where ENT.ENTREGA_ID = iENTREGA_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Houve um problema ao buscar os dados do or�amento para gera��o dos dados da NF! '' || sqlerrm);'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  vEnderecoCapa := BUSCAR_ENDERECO_EMISSAO_NF(vOrcamentoId, ''ORC'');'+ #13 +
      #13 +
      '  vDadosNota.INSCRICAO_ESTADUAL_DESTINAT := vEnderecoCapa.INSCRICAO_ESTADUAL_DESTINAT;'+ #13 +
      '  vDadosNota.LOGRADOURO_DESTINATARIO     := vEnderecoCapa.LOGRADOURO_DESTINATARIO;'+ #13 +
      '  vDadosNota.COMPLEMENTO_DESTINATARIO    := vEnderecoCapa.COMPLEMENTO_DESTINATARIO;'+ #13 +
      '  vDadosNota.NUMERO_DESTINATARIO         := vEnderecoCapa.NUMERO_DESTINATARIO;'+ #13 +
      '  vDadosNota.CEP_DESTINATARIO            := vEnderecoCapa.CEP_DESTINATARIO;'+ #13 +
      '  vDadosNota.NOME_BAIRRO_DESTINATARIO    := vEnderecoCapa.NOME_BAIRRO_DESTINATARIO;'+ #13 +
      '  vDadosNota.NOME_CIDADE_DESTINATARIO    := vEnderecoCapa.NOME_CIDADE_DESTINATARIO;'+ #13 +
      '  vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST  := vEnderecoCapa.CODIGO_IBGE_MUNICIPIO_DEST;'+ #13 +
      '  vDadosNota.ESTADO_ID_DESTINATARIO      := vEnderecoCapa.ESTADO_ID_DESTINATARIO;'+ #13 +
      #13 +
      '  if vEnderecoCapa.ENDERECO_INFO_COMPLEMENTARES is not null then'+ #13 +
      '    vDadosNota.INFORMACOES_COMPLEMENTARES := vDadosNota.INFORMACOES_COMPLEMENTARES || '';'' || vEnderecoCapa.ENDERECO_INFO_COMPLEMENTARES;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  vDadosNota.BASE_CALCULO_ICMS    := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS           := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_ICMS_ST := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS_ST        := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_PIS     := 0;'+ #13 +
      '  vDadosNota.VALOR_PIS            := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_COFINS  := 0;'+ #13 +
      '  vDadosNota.VALOR_COFINS         := 0;'+ #13 +
      '  vDadosNota.VALOR_IPI            := 0;'+ #13 +
      '  vDadosNota.VALOR_FRETE          := 0;'+ #13 +
      '  vDadosNota.VALOR_SEGURO         := 0;'+ #13 +
      '  vDadosNota.PESO_LIQUIDO         := 0;'+ #13 +
      '  vDadosNota.PESO_BRUTO           := 0;'+ #13 +
      #13 +
      '  for vItens in cItens(vDadosNota.ESTADO_ID_DESTINATARIO, vDadosNota.TIPO_CLIENTE, vDadosNota.EMPRESA_ID) loop'+ #13 +
      #13 +
      '    if length(vItens.CODIGO_NCM) < 8 then'+ #13 +
      '      Erro(''O c�digo NCM do produto '' || vItens.PRODUTO_ID || '' - '' || vItens.NOME_PRODUTO || '' est� incorreto, o NCM � composto de 8 dig�tos, verifique no cadastro de produtos!'');'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '    vDadosItens(i).PRODUTO_ID    := vItens.PRODUTO_ID;'+ #13 +
      '    vDadosItens(i).ITEM_ID       := vItens.ITEM_ID;'+ #13 +
      '    vDadosItens(i).NOME_PRODUTO  := vItens.NOME_PRODUTO;'+ #13 +
      '    vDadosItens(i).UNIDADE       := vItens.UNIDADE_VENDA;'+ #13 +
      '    vDadosItens(i).CODIGO_NCM    := vItens.CODIGO_NCM;'+ #13 +
      '    vDadosItens(i).QUANTIDADE    := vItens.QUANTIDADE;'+ #13 +
      '    vDadosItens(i).CODIGO_BARRAS := vItens.CODIGO_BARRAS;'+ #13 +
      '    vDadosItens(i).CEST          := vItens.CEST;'+ #13 +
      #13 +
      '    if vDadosDescontoVenda.IndiceDescontoVendaId is not null and vDadosDescontoVenda.PrecoCusto = ''S'' then'+ #13 +
      '      if vDadosDescontoVenda.TipoCusto = ''C'' and vItens.CUSTO_ULTIMO_PEDIDO > 0 then'+ #13 +
      '        vDadosItens(i).PRECO_UNITARIO := vItens.CUSTO_ULTIMO_PEDIDO;'+ #13 +
      '      elsif vDadosDescontoVenda.TipoCusto = ''F'' and vItens.PRECO_LIQUIDO > 0 then'+ #13 +
      '        vDadosItens(i).PRECO_UNITARIO := vItens.PRECO_LIQUIDO;'+ #13 +
      '      elsif vDadosDescontoVenda.TipoCusto = ''M'' and vItens.CMV > 0 then'+ #13 +
      '        vDadosItens(i).PRECO_UNITARIO := vItens.CMV;'+ #13 +
      '      else'+ #13 +
      '        vDadosItens(i).PRECO_UNITARIO := vItens.PRECO_UNITARIO;'+ #13 +
      '      end if;'+ #13 +
      #13 +
      '      vDadosItens(i).PRECO_UNITARIO              := trunc(vDadosItens(i).PRECO_UNITARIO * vIndiceDescVenda, 2);'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL                 := trunc(vDadosItens(i).PRECO_UNITARIO * vItens.QUANTIDADE, 2);'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO        := 0;'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := 0;'+ #13 +
      #13 +
      '      vDadosDescontoVenda.ValorTotalCusto := vDadosDescontoVenda.ValorTotalCusto + vDadosItens(i).VALOR_TOTAL;'+ #13 +
      '    elsif vDadosDescontoVenda.IndiceDescontoVendaId is null then'+ #13 +
      '      vDadosItens(i).PRECO_UNITARIO              := vItens.PRECO_UNITARIO;'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL                 := round(vDadosItens(i).PRECO_UNITARIO * vItens.QUANTIDADE, 2);'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO        := vItens.VALOR_TOTAL_DESCONTO;'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := vItens.VALOR_TOTAL_OUTRAS_DESPESAS + vItens.VALOR_TOTAL_FRETE;'+ #13 +
      '    else'+ #13 +
      '      vDadosItens(i).PRECO_UNITARIO              := trunc(vItens.PRECO_UNITARIO * vIndice, 2);'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL                 := trunc(vDadosItens(i).PRECO_UNITARIO * vItens.QUANTIDADE, 2);'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO        := trunc(vItens.VALOR_TOTAL_DESCONTO * vIndice, 2);'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := trunc((vItens.VALOR_TOTAL_OUTRAS_DESPESAS + vItens.VALOR_TOTAL_FRETE) * vIndice, 2);'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '    /* Buscando os dados de impostos item a item da venda */'+ #13 +
      '    BUSCAR_CALC_IMPOSTOS_PRODUTO('+ #13 +
      '      vItens.PRODUTO_ID,'+ #13 +
      '      vDadosNota.EMPRESA_ID,'+ #13 +
      '      vDadosNota.CADASTRO_ID,'+ #13 +
      #13 +
      '      vDadosItens(i).VALOR_TOTAL,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO,'+ #13 +
      #13 +
      '      vDadosItens(i).CST,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS,'+ #13 +
      #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS_ST,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PRECO_PAUTA,'+ #13 +
      '      vDadosItens(i).IVA,'+ #13 +
      #13 +
      '      vDadosItens(i).VALOR_IPI,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_IPI,'+ #13 +
      #13 +
      '      vDadosItens(i).CST_PIS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS,'+ #13 +
      '      vDadosItens(i).VALOR_PIS,'+ #13 +
      #13 +
      '      vDadosItens(i).CST_COFINS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS,'+ #13 +
      '      vDadosItens(i).VALOR_COFINS'+ #13 +
      '		);'+ #13 +
      #13 +
      '    vDadosItens(i).CFOP_ID           := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vDadosItens(i).CST, ''VIT'', ''I'');'+ #13 +
      '		vDadosItens(i).NATUREZA_OPERACAO := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);'+ #13 +
      '    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS     := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;'+ #13 +
      '    vDadosNota.VALOR_ICMS            := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST  := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST         := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS      := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;'+ #13 +
      '    vDadosNota.VALOR_PIS             := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;'+ #13 +
      '    vDadosNota.VALOR_COFINS          := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;'+ #13 +
      '    vDadosNota.VALOR_IPI             := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL :='+ #13 +
      '      vDadosNota.VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS  := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;'+ #13 +
      '    vDadosNota.VALOR_DESCONTO        := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;'+ #13 +
      '    /* ------------------------------------------------------------------------------------------------------ */'+ #13 +
      #13 +
      '		if'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO >'+ #13 +
      '      vValorMaisSignificativo'+ #13 +
      '    then'+ #13 +
      '			vValorMaisSignificativo :='+ #13 +
      '        vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '        vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '        vDadosItens(i).VALOR_IPI -'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '			vCfopIdCapa 	  	:= vDadosItens(i).CFOP_ID;'+ #13 +
      '			vNaturezaOperacao := vDadosItens(i).NATUREZA_OPERACAO;'+ #13 +
      '		end if;'+ #13 +
      #13 +
      '    /* Calculando os tributos aproximados IBPT, Lei 12.741 de 2012 */'+ #13 +
      '    vDadosNota.TOTAL_IMPOSTOS_ESTADUAL := vDadosNota.TOTAL_IMPOSTOS_ESTADUAL + round(vDadosItens(i).VALOR_TOTAL * vItens.PERC_ESTADUAL * 0.01, 2);'+ #13 +
      '    vDadosNota.TOTAL_IMPOSTOS_FEDERAL := vDadosNota.TOTAL_IMPOSTOS_FEDERAL + round(vDadosItens(i).VALOR_TOTAL * vItens.PERC_FEDERAL * 0.01, 2);'+ #13;
  end;

  function GetComando2: string;
  begin
    Result :=
      #13 +
      '    i := i + 1;'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  if vDadosItens.count < 1 then'+ #13 +
      '    ERRO(''Os produtos da nota fiscal n�o foram encontrados!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '	if vCfopIdCapa is null then'+ #13 +
      '		vCfopIdCapa 			:= vDadosItens(0).CFOP_ID;'+ #13 +
      '		vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;'+ #13 +
      '	end if;'+ #13 +
      #13 +
      '  -- vTipoNotaGerar := BUSCAR_TIPO_NOTA_GERAR(vOrcamentoId);'+ #13 +
      #13 +
      '  /* Ajustando as formas de pagamento da nota */'+ #13 +
      '  if vDadosDescontoVenda.IndiceDescontoVendaId is not null and vDadosDescontoVenda.PrecoCusto = ''S'' then'+ #13 +
      '    vIndice := vDadosDescontoVenda.ValorTotalCusto / vValorTotalPedido * vIndiceDescVenda;'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  vDadosNota.VALOR_RECEBIDO_DINHEIRO    := vDadosNota.VALOR_RECEBIDO_DINHEIRO * vIndice;'+ #13 +
      '  vDadosNota.VALOR_RECEBIDO_CARTAO_CRED := vDadosNota.VALOR_RECEBIDO_CARTAO_CRED * vIndice;'+ #13 +
      '  vDadosNota.VALOR_RECEBIDO_CARTAO_DEB  := vDadosNota.VALOR_RECEBIDO_CARTAO_DEB * vIndice;'+ #13 +
      '  vDadosNota.VALOR_RECEBIDO_CREDITO     := vDadosNota.VALOR_RECEBIDO_CREDITO * vIndice;'+ #13 +
      '  vDadosNota.VALOR_RECEBIDO_COBRANCA    := vDadosNota.VALOR_RECEBIDO_COBRANCA * vIndice;'+ #13 +
      '  vDadosNota.VALOR_RECEBIDO_CHEQUE      := vDadosNota.VALOR_RECEBIDO_CHEQUE * vIndice;'+ #13 +
      '  vDadosNota.VALOR_RECEBIDO_FINANCEIRA  := vDadosNota.VALOR_RECEBIDO_FINANCEIRA * vIndice;'+ #13 +
      #13 +
      '  RATEAR_VALORES_FORMAS_PAGTO_NF('+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CREDITO,'+ #13 +
      '    vDadosNota.VALOR_TOTAL'+ #13 +
      '  );'+ #13 +
      #13 +
      '  select'+ #13 +
      '    REGIME_TRIBUTARIO,'+ #13 +
      '    SERIE_NFE,'+ #13 +
      '    SERIE_NFCE'+ #13 +
      '  into'+ #13 +
      '    vRegimeTributario,'+ #13 +
      '    vSerieNFCe,'+ #13 +
      '    vSerieNFe'+ #13 +
      '  from'+ #13 +
      '    PARAMETROS_EMPRESA'+ #13 +
      '  where EMPRESA_ID = vDadosNota.EMPRESA_ID;'+ #13 +
      #13 +
      '  if vTipoNotaGerar = ''NI'' then'+ #13 +
      '    if vDadosNota.TIPO_PESSOA_DESTINATARIO = ''F'' then'+ #13 +
      '      vModeloNota := ''65'';'+ #13 +
      '      vSerieNota := vSerieNFCe;'+ #13 +
      '      vTipoNotaGerar := ''C'';'+ #13 +
      '    else'+ #13 +
      '      vModeloNota := ''55'';'+ #13 +
      '      vSerieNota  := to_char(nvl(vSerieNFe, 1));'+ #13 +
      '      vTipoNotaGerar := ''N'';'+ #13 +
      '    end if;'+ #13 +
      '  elsif (vTipoNotaGerar = ''NF'') or (vDadosNota.TIPO_PESSOA_DESTINATARIO = ''J'') then'+ #13 +
      '    vModeloNota := ''55'';'+ #13 +
      '    vSerieNota  := to_char(nvl(vSerieNFe, 1));'+ #13 +
      '    vTipoNotaGerar := ''N'';'+ #13 +
      '  else'+ #13 +
      '    vModeloNota := ''65'';'+ #13 +
      '    vTipoNotaGerar := ''C'';'+ #13 +
      '    vSerieNota  := to_char(vSerieNFCe);'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '	vDadosNota.CFOP_ID           := vCfopIdCapa;'+ #13 +
      '  vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;'+ #13 +
      #13 +
      '  vDadosNota.INFORMACOES_COMPLEMENTARES :='+ #13 +
      '    vDadosNota.INFORMACOES_COMPLEMENTARES || ''; Trib. aprox. R$ '' || NFORMAT(vDadosNota.TOTAL_IMPOSTOS_FEDERAL, 2) || '' federal, R$ '' || NFORMAT(vDadosNota.TOTAL_IMPOSTOS_ESTADUAL, 2) || '' estadual. '';'+ #13 +
      #13 +
      '  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;'+ #13 +
      #13 +
      '	insert into NOTAS_FISCAIS('+ #13 +
      '    NOTA_FISCAL_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    CFOP_ID,'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '    ORCAMENTO_ID,'+ #13 +
      '    ORCAMENTO_BASE_ID,'+ #13 +
      '    ENTREGA_ID,'+ #13 +
      '		MODELO_NOTA,'+ #13 +
      '		SERIE_NOTA,'+ #13 +
      '    NATUREZA_OPERACAO,'+ #13 +
      '		STATUS,'+ #13 +
      '    TIPO_MOVIMENTO,'+ #13 +
      '    RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    NOME_FANTASIA_EMITENTE,'+ #13 +
      '    REGIME_TRIBUTARIO,'+ #13 +
      '    CNPJ_EMITENTE,'+ #13 +
      '    INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    LOGRADOURO_EMITENTE,'+ #13 +
      '    COMPLEMENTO_EMITENTE,'+ #13 +
      '    NOME_BAIRRO_EMITENTE,'+ #13 +
      '    NOME_CIDADE_EMITENTE,'+ #13 +
      '    NUMERO_EMITENTE,'+ #13 +
      '    ESTADO_ID_EMITENTE,'+ #13 +
      '    CEP_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    CPF_CNPJ_DESTINATARIO,'+ #13 +
      #13 +
      '    INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    TIPO_NOTA,'+ #13 +
      '    LOGRADOURO_DESTINATARIO,'+ #13 +
      '    COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    ESTADO_ID_DESTINATARIO,'+ #13 +
      '    CEP_DESTINATARIO,'+ #13 +
      '    NUMERO_DESTINATARIO,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      #13 +
      '    INSCRICAO_EST_ENTR_DESTINAT,'+ #13 +
      '    LOGRADOURO_ENTR_DESTINATARIO,'+ #13 +
      '    COMPLEMENTO_ENTR_DESTINATARIO,'+ #13 +
      '    NOME_BAIRRO_ENTR_DESTINATARIO,'+ #13 +
      '    NOME_CIDADE_ENTR_DESTINATARIO,'+ #13 +
      '    NUMERO_ENTR_DESTINATARIO,'+ #13 +
      '    ESTADO_ID_ENTR_DESTINATARIO,'+ #13 +
      '    CEP_ENTR_DESTINATARIO,'+ #13 +
      '    CODIGO_IBGE_MUNIC_ENTR_DEST,'+ #13 +
      #13 +
      '    VALOR_TOTAL,'+ #13 +
      '    VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    VALOR_DESCONTO,'+ #13 +
      '    VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    BASE_CALCULO_ICMS,'+ #13 +
      '    VALOR_ICMS,'+ #13 +
      '    BASE_CALCULO_ICMS_ST,'+ #13 +
      '    VALOR_ICMS_ST,'+ #13 +
      '    BASE_CALCULO_PIS,'+ #13 +
      '    VALOR_PIS,'+ #13 +
      '    BASE_CALCULO_COFINS,'+ #13 +
      '    VALOR_COFINS,'+ #13 +
      '    VALOR_IPI,'+ #13 +
      '    VALOR_FRETE,'+ #13 +
      '    VALOR_SEGURO,'+ #13 +
      '    VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '    VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '    VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '    VALOR_RECEBIDO_CREDITO,'+ #13 +
      '    VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '    VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '    PESO_LIQUIDO,'+ #13 +
      '    PESO_BRUTO,'+ #13 +
      '		INFORMACOES_COMPLEMENTARES'+ #13 +
      '  )values('+ #13 +
      '    vNotaFiscalId,'+ #13 +
      '    vDadosNota.CADASTRO_ID,'+ #13 +
      '    vDadosNota.CFOP_ID,'+ #13 +
      '    vDadosNota.EMPRESA_ID,'+ #13 +
      '    vDadosNota.ORCAMENTO_ID,'+ #13 +
      '    vDadosNota.ORCAMENTO_ID,'+ #13 +
      '    iENTREGA_ID, -- ENTREGA_ID'+ #13 +
      '		vModeloNota,'+ #13 +
      '		vSerieNota,'+ #13 +
      '    vDadosNota.NATUREZA_OPERACAO,'+ #13 +
      '		''N'','+ #13 +
      '    ''VEN'','+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_EMITENTE,'+ #13 +
      '    vRegimeTributario,'+ #13 +
      '    vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '    vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '    vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '    vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '    vDadosNota.CEP_EMITENTE,'+ #13 +
      '    vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    vDadosNota.NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    vDadosNota.TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    vTipoNotaGerar,'+ #13 +
      '    vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '    vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      '    vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '    vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      #13 +
      '    vEnderecoCapa.INSCRICAO_EST_ENTR_DESTINAT,'+ #13 +
      '    vEnderecoCapa.LOGRADOURO_ENTR_DESTINATARIO,'+ #13 +
      '    vEnderecoCapa.COMPLEMENTO_ENTR_DESTINATARIO,'+ #13 +
      '    vEnderecoCapa.NOME_BAIRRO_ENTR_DESTINATARIO,'+ #13 +
      '    vEnderecoCapa.NOME_CIDADE_ENTR_DESTINATARIO,'+ #13 +
      '    vEnderecoCapa.NUMERO_ENTR_DESTINATARIO,'+ #13 +
      '    vEnderecoCapa.ESTADO_ID_ENTR_DESTINATARIO,'+ #13 +
      '    vEnderecoCapa.CEP_ENTR_DESTINATARIO,'+ #13 +
      '    vEnderecoCapa.CODIGO_IBGE_MUNIC_ENTR_DEST,'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL,'+ #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    vDadosNota.VALOR_DESCONTO,'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS,'+ #13 +
      '    vDadosNota.VALOR_ICMS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST,'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST,'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS,'+ #13 +
      '    vDadosNota.VALOR_PIS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_COFINS,'+ #13 +
      '    vDadosNota.VALOR_COFINS,'+ #13 +
      '    vDadosNota.VALOR_IPI,'+ #13 +
      '    vDadosNota.VALOR_FRETE,'+ #13 +
      '    vDadosNota.VALOR_SEGURO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_DINHEIRO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CREDITO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_CHEQUE,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_FINANCEIRA,'+ #13 +
      '    vDadosNota.PESO_LIQUIDO,'+ #13 +
      '    vDadosNota.PESO_BRUTO,'+ #13 +
      '		vDadosNota.INFORMACOES_COMPLEMENTARES'+ #13 +
      '  );'+ #13 +
      #13 +
      '  for i in 0..vDadosItens.count - 1 loop'+ #13 +
      '    insert into NOTAS_FISCAIS_ITENS('+ #13 +
      '      NOTA_FISCAL_ID,'+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      NOME_PRODUTO,'+ #13 +
      '      UNIDADE,'+ #13 +
      '      CFOP_ID,'+ #13 +
      '      CST,'+ #13 +
      '      CODIGO_NCM,'+ #13 +
      '      VALOR_TOTAL,'+ #13 +
      '      PRECO_UNITARIO,'+ #13 +
      '      QUANTIDADE,'+ #13 +
      '      VALOR_TOTAL_DESCONTO,'+ #13 +
      '      VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      BASE_CALCULO_ICMS,'+ #13 +
      '      PERCENTUAL_ICMS,'+ #13 +
      '      VALOR_ICMS,'+ #13 +
      '      BASE_CALCULO_ICMS_ST,'+ #13 +
      '      VALOR_ICMS_ST,'+ #13 +
      '      CST_PIS,'+ #13 +
      '      BASE_CALCULO_PIS,'+ #13 +
      '      PERCENTUAL_PIS,'+ #13 +
      '      VALOR_PIS,'+ #13 +
      '      CST_COFINS,'+ #13 +
      '      BASE_CALCULO_COFINS,'+ #13 +
      '      PERCENTUAL_COFINS,'+ #13 +
      '      VALOR_COFINS,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      IVA,'+ #13 +
      '      PRECO_PAUTA,'+ #13 +
      '      CODIGO_BARRAS,'+ #13 +
      '      VALOR_IPI,'+ #13 +
      '      PERCENTUAL_IPI,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      PERCENTUAL_ICMS_ST,'+ #13 +
      '      CEST'+ #13 +
      '    )values('+ #13 +
      '      vNotaFiscalId,'+ #13 +
      '      vDadosItens(i).PRODUTO_ID,'+ #13 +
      '      vDadosItens(i).ITEM_ID,'+ #13 +
      '      vDadosItens(i).NOME_PRODUTO,'+ #13 +
      '      vDadosItens(i).UNIDADE,'+ #13 +
      '      vDadosItens(i).CFOP_ID,'+ #13 +
      '      vDadosItens(i).CST,'+ #13 +
      '      vDadosItens(i).CODIGO_NCM,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL,'+ #13 +
      '      vDadosItens(i).PRECO_UNITARIO,'+ #13 +
      '      vDadosItens(i).QUANTIDADE,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CST_PIS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS,'+ #13 +
      '      vDadosItens(i).VALOR_PIS,'+ #13 +
      '      vDadosItens(i).CST_COFINS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS,'+ #13 +
      '      vDadosItens(i).VALOR_COFINS,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      vDadosItens(i).IVA,'+ #13 +
      '      vDadosItens(i).PRECO_PAUTA,'+ #13 +
      '      vDadosItens(i).CODIGO_BARRAS,'+ #13 +
      '      vDadosItens(i).VALOR_IPI,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_IPI,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CEST'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      'end GERAR_NOTA_ENTREGA;'+ #13;
  end;

begin
  Result :=
    GetComando1 + GetComando2;
end;

function AjustandoColunasTabelaCONHECIMENTOS_FRETES: string;
begin
  Result :=
    'alter table CONHECIMENTOS_FRETES disable all triggers;'+ #13 +
    #13 +
    'alter table CONHECIMENTOS_FRETES'+ #13 +
    'add USUARIO_CADASTRO_ID     number(4);'+ #13 +
    #13 +
    'update CONHECIMENTOS_FRETES set'+ #13 +
    '  USUARIO_CADASTRO_ID = USUARIO_SESSAO_ID;'+ #13 +
    #13 +
    'alter table CONHECIMENTOS_FRETES'+ #13 +
    'modify USUARIO_CADASTRO_ID not null;'+ #13 +
    #13 +
    'alter table CONHECIMENTOS_FRETES enable all triggers;'+ #13
end;

function AjustarTriggerCONHECIMENTOS_FRETES_IU_BR: string;
begin
  Result :=
    'create or replace trigger CONHECIMENTOS_FRETES_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on CONHECIMENTOS_FRETES'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '  if inserting then'+ #13 +
    '    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '    :new.DATA_HORA_CADASTRO  := sysdate;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end CONHECIMENTOS_FRETES_IU_BR;'+ #13
end;

function CriarNovasColunasMOVIMENTOS_CONTAS: string;
begin
  Result :=
    'alter table MOVIMENTOS_CONTAS'+ #13 +
    'add ('+ #13 +
    '  USUARIO_CONCILIACAO_ID  number(4),'+ #13 +
    '  DATA_HORA_CONCILIACAO   date,'+ #13 +
    '  MOVIMENTO_ID_BANCO      varchar2(20)'+ #13 +
    ');'+ #13
end;

function AjustarConstraintCK_MOV_CONTAS_CONCILIADO: string;
begin
  Result :=
    'alter table MOVIMENTOS_CONTAS'+ #13 +
    'drop constraint CK_MOV_CONTAS_CONCILIADO;'+ #13 +
    #13 +
    'alter table MOVIMENTOS_CONTAS'+ #13 +
    'add constraint CK_MOV_CONTAS_CONCILIADO'+ #13 +
    'check('+ #13 +
    '  (CONCILIADO = ''N'' and USUARIO_CONCILIACAO_ID is null and DATA_HORA_CONCILIACAO is null)'+ #13 +
    '  or'+ #13 +
    '  (CONCILIADO = ''S'' and USUARIO_CONCILIACAO_ID is not null and DATA_HORA_CONCILIACAO is not null)'+ #13 +
    ');'+ #13
end;

function CriarColunaSALDO_CONCILIADO_ATUAL: string;
begin
  Result :=
    'alter table CONTAS disable all triggers;'+ #13 +
    #13 +
    'alter table CONTAS'+ #13 +
    'add SALDO_CONCILIADO_ATUAL       number(10,2) default 0 not null;'+ #13 +
    #13 +
    'alter table CONTAS enable all triggers;'+ #13
end;

function AjustarProcedureMOVIMENTAR_SALDO_CONTA: string;
begin
  Result :=
    'create or replace procedure MOVIMENTAR_SALDO_CONTA( iCONTA_ID in string, iVALOR in number, iTIPO_MOVIMENTO in string )'+ #13 +
    'is'+ #13 +
    'begin'+ #13 +
    #13 +
    '  if iTIPO_MOVIMENTO not in(''A'', ''C'', ''T'') then'+ #13 +
    '    ERRO(''O tipo de movimento de saldo s� pode ser "A", "C" ou "T"!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if iTIPO_MOVIMENTO = ''A'' then'+ #13 +
    '    update CONTAS set'+ #13 +
    '      SALDO_ATUAL = SALDO_ATUAL + iVALOR'+ #13 +
    '    where CONTA = iCONTA_ID;'+ #13 +
    '  elsif iTIPO_MOVIMENTO = ''C'' then'+ #13 +
    '    update CONTAS set'+ #13 +
    '      SALDO_CONCILIADO_ATUAL = SALDO_CONCILIADO_ATUAL + iVALOR'+ #13 +
    '    where CONTA = iCONTA_ID;'+ #13 +
    '  else'+ #13 +
    '    update CONTAS set'+ #13 +
    '      SALDO_ATUAL = SALDO_ATUAL + iVALOR,'+ #13 +
    '      SALDO_CONCILIADO_ATUAL = SALDO_CONCILIADO_ATUAL + iVALOR'+ #13 +
    '    where CONTA = iCONTA_ID;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end MOVIMENTAR_SALDO_CONTA;'+ #13
end;

function AjustarTriggerTURNOS_CAIXA_IU_BR: string;
begin
  Result :=
    'create or replace trigger TURNOS_CAIXA_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on TURNOS_CAIXA'+ #13 +
    'for each row'+ #13 +
    'declare  '+ #13 +
    '  vTurnoAbertoId   naturaln default 0; -- NaturalN pois pode ser zero mais n�o pode ser null'+ #13 +
    'begin'+ #13 +
    #13 +
    '  if :old.STATUS = ''B'' then'+ #13 +
    '    ERRO(''N�o � permitido alterar um turno j� baixado!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '  if inserting then'+ #13 +
    '    begin'+ #13 +
    '      select TURNO_ID'+ #13 +
    '      into vTurnoAbertoId'+ #13 +
    '      from TURNOS_CAIXA'+ #13 +
    '      where FUNCIONARIO_ID = :new.FUNCIONARIO_ID'+ #13 +
    '      and DATA_HORA_FECHAMENTO is null'+ #13 +
    '      and rownum = 1;'+ #13 +
    '    exception'+ #13 +
    '      when others then'+ #13 +
    '        vTurnoAbertoId := 0;'+ #13 +
    '    end;'+ #13 +
    #13 +
    '    if vTurnoAbertoId > 0 then'+ #13 +
    '      ERRO( ''J� existe um turno em aberto para este funcion�rio. Turno: '' || vTurnoAbertoId );'+ #13 +
    '    end if;'+ #13 +
    #13 +
    '    :new.STATUS := ''A'';'+ #13 +
    '    :new.DATA_HORA_ABERTURA := sysdate;'+ #13 +
    '    :new.USUARIO_ABERTURA_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    #13 +
    '    if :new.VALOR_INICIAL > 0 then'+ #13 +
    '      MOVIMENTAR_SALDO_CONTA( :new.CONTA_ORIGEM_DIN_INI_ID, :new.VALOR_INICIAL * -1, ''T'' );'+ #13 +
    '    end if;'+ #13 +
    '  else'+ #13 +
    '    /* Se estiver fechando o turno */'+ #13 +
    '    if :old.STATUS = ''A'' and :new.STATUS = ''B'' then'+ #13 +
    '      :new.USUARIO_FECHAMENTO_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '      :new.DATA_HORA_FECHAMENTO := sysdate;'+ #13 +
    '    end if;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end TURNOS_CAIXA_IU_BR;'+ #13
end;

function AjustarTriggerMOVIMENTOS_TURNOS_IU_BR: string;
begin
  Result :=
    'create or replace trigger MOVIMENTOS_TURNOS_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on MOVIMENTOS_TURNOS'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    #13 +
    '  cursor cCartoes is'+ #13 +
    '  select'+ #13 +
    '    COR.ORCAMENTO_ID,'+ #13 +
    '    COR.BAIXA_ORIGEM_ID,'+ #13 +
    '    COR.ITEM_ID_CRT_ORCAMENTO'+ #13 +
    '  from'+ #13 +
    '    CONTAS_RECEBER COR'+ #13 +
    #13 +
    '  inner join TIPOS_COBRANCA TCO'+ #13 +
    '  on COR.COBRANCA_ID = TCO.COBRANCA_ID'+ #13 +
    #13 +
    '  inner join CADASTROS CAD'+ #13 +
    '  on COR.CADASTRO_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    '  where COR.TURNO_ID = :new.TURNO_ID'+ #13 +
    '  and TCO.FORMA_PAGAMENTO = ''CRT'''+ #13 +
    #13 +
    '  group by'+ #13 +
    '    COR.ORCAMENTO_ID,'+ #13 +
    '    COR.BAIXA_ORIGEM_ID,'+ #13 +
    '    COR.ITEM_ID_CRT_ORCAMENTO;'+ #13 +
    #13 +
    '  cursor cCobrancas is'+ #13 +
    '  select'+ #13 +
    '    COR.RECEBER_ID,'+ #13 +
    '    TPC.FORMA_PAGAMENTO'+ #13 +
    '  from'+ #13 +
    '    CONTAS_RECEBER COR'+ #13 +
    #13 +
    '  inner join TIPOS_COBRANCA TPC'+ #13 +
    '  on COR.COBRANCA_ID = TPC.COBRANCA_ID'+ #13 +
    '  and TPC.FORMA_PAGAMENTO in(''CHQ'', ''COB'')'+ #13 +
    #13 +
    '  where COR.TURNO_ID = :new.TURNO_ID;'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  if updating then'+ #13 +
    '    ERRO(''N�o s�o permitidas altera��es nos movimentos de turnos!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '  :new.DATA_HORA_MOVIMENTO := sysdate;'+ #13 +
    '  :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    #13 +
    '  if :new.TIPO_MOVIMENTO in(''SUP'', ''SAN'', ''FEC'') and :new.VALOR_DINHEIRO > 0 then'+ #13 +
    '    MOVIMENTAR_SALDO_CONTA( :new.CONTA_ID, :new.VALOR_DINHEIRO * case when :new.TIPO_MOVIMENTO = ''SUP'' then -1 else 1 end, ''T'' );'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if inserting then'+ #13 +
    '    if :new.TIPO_MOVIMENTO = ''FEC'' then'+ #13 +
    '      update TURNOS_CAIXA set'+ #13 +
    '        VALOR_DINHEIRO_INF_FECHAMENTO = :new.VALOR_DINHEIRO,'+ #13 +
    '        VALOR_CHEQUE_INF_FECHAMENTO = :new.VALOR_CHEQUE,'+ #13 +
    '        VALOR_CARTAO_INF_FECHAMENTO = :new.VALOR_CARTAO,'+ #13 +
    '        VALOR_COBRANCA_INF_FECHAMENTO = :new.VALOR_COBRANCA,'+ #13 +
    '        STATUS = ''B'''+ #13 +
    '      where TURNO_ID = :new.TURNO_ID;'+ #13 +
    #13 +
    '      update CONTAS_RECEBER set'+ #13 +
    '        TURNO_ID = null'+ #13 +
    '      where TURNO_ID = :new.TURNO_ID;'+ #13 +
    #13 +
    '      for xCartoes in cCartoes loop'+ #13 +
    '        GRAVAR_TITULOS_MOV_TURNOS_ITE(xCartoes.ORCAMENTO_ID, xCartoes.ITEM_ID_CRT_ORCAMENTO, :new.MOVIMENTO_TURNO_ID, ''CRT'', null);'+ #13 +
    '      end loop;'+ #13 +
    #13 +
    '      for xCob in cCobrancas loop'+ #13 +
    '        GRAVAR_TITULOS_MOV_TURNOS_ITE(null, null, :new.MOVIMENTO_TURNO_ID, ''CRT'', xCob.RECEBER_ID);'+ #13 +
    '      end loop;'+ #13 +
    '    end if;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end MOVIMENTOS_TURNOS_IU_BR;'+ #13
end;

function AjustarTriggerMOVIMENTOS_CONTAS_IU_BR: string;
begin
  Result :=
    'create or replace trigger MOVIMENTOS_CONTAS_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on MOVIMENTOS_CONTAS'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    '  vSinal number;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '  if inserting then'+ #13 +
    '    :new.USUARIO_MOVIMENTO_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '    :new.CONCILIADO           := ''N'';'+ #13 +
    #13 +
    '    if :new.TIPO_MOVIMENTO = ''ENM'' then'+ #13 +
    '      MOVIMENTAR_SALDO_CONTA(:new.CONTA_ID, :new.VALOR_MOVIMENTO, ''A'');'+ #13 +
    '    elsif :new.TIPO_MOVIMENTO = ''TRA'' then'+ #13 +
    '      MOVIMENTAR_SALDO_CONTA(:new.CONTA_ID, :new.VALOR_MOVIMENTO * -1, ''A'');'+ #13 +
    '      MOVIMENTAR_SALDO_CONTA(:new.CONTA_DESTINO_ID, :new.VALOR_MOVIMENTO, ''A'');'+ #13 +
    '    end if;'+ #13 +
    #13 +
    '  else'+ #13 +
    '    vSinal := case when :new.TIPO_MOVIMENTO in(''BXR'', ''ENM'') then 1 else -1 end;'+ #13 +
    #13 +
    '    if :old.CONCILIADO = ''N'' and :new.CONCILIADO = ''S'' then'+ #13 +
    '      :new.USUARIO_CONCILIACAO_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '      :new.DATA_HORA_CONCILIACAO  := sysdate;'+ #13 +
    #13 +
    '      if :new.TIPO_MOVIMENTO = ''TRA'' then'+ #13 +
    '        MOVIMENTAR_SALDO_CONTA(:new.CONTA_ID, :new.VALOR_MOVIMENTO * -1, ''C'');'+ #13 +
    '        MOVIMENTAR_SALDO_CONTA(:new.CONTA_DESTINO_ID, :new.VALOR_MOVIMENTO, ''C'');'+ #13 +
    '      else'+ #13 +
    '        MOVIMENTAR_SALDO_CONTA(:new.CONTA_ID, :new.VALOR_MOVIMENTO * vSinal, ''C'');'+ #13 +
    '      end if;'+ #13 +
    #13 +
    '    elsif :old.CONCILIADO = ''S'' and :new.CONCILIADO = ''N'' then'+ #13 +
    '      :new.USUARIO_CONCILIACAO_ID := null;'+ #13 +
    '      :new.DATA_HORA_CONCILIACAO  := null;'+ #13 +
    #13 +
    '      if :new.TIPO_MOVIMENTO = ''TRA'' then'+ #13 +
    '        MOVIMENTAR_SALDO_CONTA(:new.CONTA_ID, :new.VALOR_MOVIMENTO, ''C'');'+ #13 +
    '        MOVIMENTAR_SALDO_CONTA(:new.CONTA_DESTINO_ID, :new.VALOR_MOVIMENTO * -1, ''C'');'+ #13 +
    '      else'+ #13 +
    '        MOVIMENTAR_SALDO_CONTA(:new.CONTA_ID, :new.VALOR_MOVIMENTO * vSinal, ''C'');'+ #13 +
    '      end if;'+ #13 +
    '    end if;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end MOVIMENTOS_CONTAS_IU_BR;'+ #13
end;

function AjustarColunaCONTA_IDVariasTabelas: string;
begin
  Result :=
    'alter table CONTAS_FUNC_AUTORIZADOS'+ #13 +
    'modify CONTA_ID varchar2(20);'+ #13 +
    #13 +
    'alter table CONTAS_PAGAR_BAIXAS_PAGTOS_DIN'+ #13 +
    'modify CONTA_ID varchar2(20);'+ #13 +
    #13 +
    'alter table CONTAS_REC_BAIXAS_PAGTOS_DIN'+ #13 +
    'modify CONTA_ID varchar2(20);'+ #13 +
    #13 +
    'alter table MOVIMENTOS_CONTAS'+ #13 +
    'modify CONTA_ID varchar2(20);'+ #13 +
    #13 +
    'alter table MOVIMENTOS_TURNOS'+ #13 +
    'modify CONTA_ID varchar2(20);'+ #13 +
    #13 +
    'alter table PORTADORES_CONTAS'+ #13 +
    'modify CONTA_ID varchar2(20);'+ #13
end;

function AjustarViewVW_MOVIMENTOS_CONTAS: string;
begin
  Result :=
    'create or replace view VW_MOVIMENTOS_CONTAS'+ #13 +
    'as'+ #13 +
    'select'+ #13 +
    '  ''TRA'' as TIPO_MOVIMENTO,'+ #13 +
    '  MOV.MOVIMENTO_ID,'+ #13 +
    '  MOV.DATA_HORA_MOVIMENTO,'+ #13 +
    '  MOV.CONTA_DESTINO_ID as CONTA_ID,'+ #13 +
    '  MOV.VALOR_MOVIMENTO,  '+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  MOV.PLANO_FINANCEIRO_ID,'+ #13 +
    '  MOV.CENTRO_CUSTO_ID,'+ #13 +
    '  MOV.BAIXA_RECEBER_ID,'+ #13 +
    '  MOV.BAIXA_PAGAR_ID,'+ #13 +
    '  MOV.USUARIO_MOVIMENTO_ID,'+ #13 +
    '  MOV.CONCILIADO,'+ #13 +
    '  MOV.USUARIO_CONCILIACAO_ID,'+ #13 +
    '  MOV.DATA_HORA_CONCILIACAO,'+ #13 +
    '  MOV.MOVIMENTO_ID_BANCO'+ #13 +
    'from'+ #13 +
    '  MOVIMENTOS_CONTAS MOV'+ #13 +
    #13 +
    'where MOV.CONTA_DESTINO_ID is not null -- Trazendo apenas os movimentos de origem, ou seja, se for uma transfer�ncia a conta que fez a transferencia para outra.'+ #13 +
    'and MOV.TIPO_MOVIMENTO = ''TRA'''+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    'select'+ #13 +
    '  ''TRA'' as TIPO_MOVIMENTO,'+ #13 +
    '  MOV.MOVIMENTO_ID,'+ #13 +
    '  MOV.DATA_HORA_MOVIMENTO,'+ #13 +
    '  MOV.CONTA_ID,'+ #13 +
    '  MOV.VALOR_MOVIMENTO,  '+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  MOV.PLANO_FINANCEIRO_ID,'+ #13 +
    '  MOV.CENTRO_CUSTO_ID,'+ #13 +
    '  MOV.BAIXA_RECEBER_ID,'+ #13 +
    '  MOV.BAIXA_PAGAR_ID,'+ #13 +
    '  MOV.USUARIO_MOVIMENTO_ID,'+ #13 +
    '  MOV.CONCILIADO,'+ #13 +
    '  MOV.USUARIO_CONCILIACAO_ID,'+ #13 +
    '  MOV.DATA_HORA_CONCILIACAO,'+ #13 +
    '  MOV.MOVIMENTO_ID_BANCO'+ #13 +
    'from'+ #13 +
    '  MOVIMENTOS_CONTAS MOV'+ #13 +
    #13 +
    'where MOV.CONTA_DESTINO_ID is null -- Trazendo apenas os movimentos de destino, ou seja, se for uma transfer�ncia a conta que recebeu a transferencia.'+ #13 +
    'and MOV.TIPO_MOVIMENTO = ''TRA'''+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    'select'+ #13 +
    '  MOV.TIPO_MOVIMENTO,'+ #13 +
    '  MOV.MOVIMENTO_ID,'+ #13 +
    '  MOV.DATA_HORA_MOVIMENTO,'+ #13 +
    '  MOV.CONTA_ID,'+ #13 +
    '  MOV.VALOR_MOVIMENTO,  '+ #13 +
    '  case when TIPO_MOVIMENTO in(''BXR'', ''ENM'') then ''E'' else ''S'' end as NATUREZA,'+ #13 +
    '  MOV.PLANO_FINANCEIRO_ID,'+ #13 +
    '  MOV.CENTRO_CUSTO_ID,'+ #13 +
    '  MOV.BAIXA_RECEBER_ID,'+ #13 +
    '  MOV.BAIXA_PAGAR_ID,'+ #13 +
    '  MOV.USUARIO_MOVIMENTO_ID,'+ #13 +
    '  MOV.CONCILIADO,'+ #13 +
    '  MOV.USUARIO_CONCILIACAO_ID,'+ #13 +
    '  MOV.DATA_HORA_CONCILIACAO,'+ #13 +
    '  MOV.MOVIMENTO_ID_BANCO'+ #13 +
    'from'+ #13 +
    '  MOVIMENTOS_CONTAS MOV'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    'select'+ #13 +
    '  ''ATC'' as TIPO_MOVIMENTO,'+ #13 +
    '  TUR.TURNO_ID as MOVIMENTO_ID,'+ #13 +
    '  TUR.DATA_HORA_ABERTURA,'+ #13 +
    '  TUR.CONTA_ORIGEM_DIN_INI_ID as CONTA_ID,'+ #13 +
    '  TUR.VALOR_INICIAL,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  null as PLANO_FINANCEIRO_ID,'+ #13 +
    '  null as CENTRO_CUSTO_ID,'+ #13 +
    '  null as BAIXA_RECEBER_ID,'+ #13 +
    '  null as BAIXA_PAGAR_ID,'+ #13 +
    '  TUR.USUARIO_ABERTURA_ID,'+ #13 +
    '  ''S'' as CONCILIADO,'+ #13 +
    '  TUR.USUARIO_ABERTURA_ID,'+ #13 +
    '  TUR.DATA_HORA_ABERTURA,'+ #13 +
    '  null'+ #13 +
    'from'+ #13 +
    '  TURNOS_CAIXA TUR'+ #13 +
    'where TUR.VALOR_INICIAL > 0'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    'select'+ #13 +
    '  TUR.TIPO_MOVIMENTO as TIPO_MOVIMENTO,'+ #13 +
    '  TUR.MOVIMENTO_TURNO_ID as MOVIMENTO_ID,'+ #13 +
    '  TUR.DATA_HORA_MOVIMENTO,'+ #13 +
    '  TUR.CONTA_ID as CONTA_ID,'+ #13 +
    '  TUR.VALOR_DINHEIRO,'+ #13 +
    '  case when TUR.TIPO_MOVIMENTO in(''SAN'', ''FEC'') then ''E'' else ''S'' end as NATUREZA,'+ #13 +
    '  null as PLANO_FINANCEIRO_ID,'+ #13 +
    '  null as CENTRO_CUSTO_ID,'+ #13 +
    '  null as BAIXA_RECEBER_ID,'+ #13 +
    '  null as BAIXA_PAGAR_ID,'+ #13 +
    '  TUR.USUARIO_CADASTRO_ID,'+ #13 +
    '  ''S'' as CONCILIADO,'+ #13 +
    '  TUR.USUARIO_CADASTRO_ID,'+ #13 +
    '  TUR.DATA_HORA_MOVIMENTO,'+ #13 +
    '  null'+ #13 +
    'from'+ #13 +
    '  MOVIMENTOS_TURNOS TUR'+ #13 +
    'where TUR.VALOR_DINHEIRO > 0;'+ #13
end;

function AjustarDataMovimentosMOVIMENTOS_CONTAS: string;
begin
  Result :=
    'declare'+ #13 +
    '  vDataPagamento date;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  iniciar_sessao(1, 1, ''AJUSTE'', ''AJUSTE'');'+ #13 +
    #13 +
    '  for xReceber in ( select MOVIMENTO_ID, BAIXA_RECEBER_ID, CONTA_ID from MOVIMENTOS_CONTAS where TIPO_MOVIMENTO = ''BXR'' ) loop'+ #13 +
    #13 +
    '    select'+ #13 +
    '      DATA_PAGAMENTO'+ #13 +
    '    into'+ #13 +
    '      vDataPagamento'+ #13 +
    '    from'+ #13 +
    '      CONTAS_RECEBER_BAIXAS'+ #13 +
    '    where BAIXA_ID = xReceber.BAIXA_RECEBER_ID;'+ #13 +
    #13 +
    '    if to_char(vDataPagamento, ''yyyy'') <= ''2009'' then'+ #13 +
    '       vDataPagamento := (add_months(vDataPagamento, (2018 - to_number(to_char(vDataPagamento, ''yyyy''))) * 12));'+ #13 +
    '    end if;'+ #13 +
    #13 +
    '    update MOVIMENTOS_CONTAS set'+ #13 +
    '      DATA_HORA_MOVIMENTO = vDataPagamento'+ #13 +
    '    where MOVIMENTO_ID = xReceber.MOVIMENTO_ID;'+ #13 +
    #13 +
    '  end loop;'+ #13 +
    #13 +
    '  for xPagar in ( select MOVIMENTO_ID, BAIXA_PAGAR_ID, CONTA_ID from MOVIMENTOS_CONTAS where TIPO_MOVIMENTO = ''BXP'' ) loop'+ #13 +
    #13 +
    '    select'+ #13 +
    '      DATA_PAGAMENTO'+ #13 +
    '    into'+ #13 +
    '      vDataPagamento'+ #13 +
    '    from'+ #13 +
    '      CONTAS_PAGAR_BAIXAS'+ #13 +
    '    where BAIXA_ID = xPagar.BAIXA_PAGAR_ID;'+ #13 +
    #13 +
    '    update MOVIMENTOS_CONTAS set'+ #13 +
    '      DATA_HORA_MOVIMENTO = vDataPagamento'+ #13 +
    '    where MOVIMENTO_ID = xPagar.MOVIMENTO_ID;'+ #13 +
    #13 +
    '  end loop;  '+ #13 +
    #13 +
    'end;'+ #13
end;

function AjustarSaldoContas: string;
begin
  Result :=
    'begin'+ #13 +
    #13 +
    '  INICIAR_SESSAO(1, 1, ''AJUSTE'', ''AJUSTE'');'+ #13 +
    #13 +
    '  update CONTAS set'+ #13 +
    '    SALDO_ATUAL = 0;'+ #13 +
    #13 +
    '  for xMov in ('+ #13 +
    '    select'+ #13 +
    '      CONTA_ID,'+ #13 +
    '      sum(VALOR_MOVIMENTO * case when NATUREZA = ''E'' then 1 else -1 end) SALDO_ATUAL'+ #13 +
    '    from'+ #13 +
    '      VW_MOVIMENTOS_CONTAS'+ #13 +
    '    group by'+ #13 +
    '      CONTA_ID'+ #13 +
    '  ) loop'+ #13 +
    #13 +
    '    update CONTAS set'+ #13 +
    '      SALDO_ATUAL = xMov.SALDO_ATUAL'+ #13 +
    '    where CONTA = xMov.CONTA_ID;'+ #13 +
    #13 +
    '  end loop;'+ #13 +
    #13 +
    'end;'+ #13
end;

function CriarTabelaLISTA_NEGRA: string;
begin
  Result :=
    'create table LISTA_NEGRA('+ #13 +
    '  LISTA_ID    number(3) not null,'+ #13 +
    '  NOME        varchar2(60) not null,'+ #13 +
    '  ATIVO       char(1) not null,'+ #13 +
    #13 +
    '  /* Colunas de logs do sistema */'+ #13 +
    '  USUARIO_SESSAO_ID       number(4) not null,'+ #13 +
    '  DATA_HORA_ALTERACAO     date not null,'+ #13 +
    '  ROTINA_ALTERACAO        varchar2(30) not null,'+ #13 +
    '  ESTACAO_ALTERACAO       varchar2(30) not null'+ #13 +
    ');'+ #13 +
    #13 +
    '/* Chaves primarias */'+ #13 +
    'alter table LISTA_NEGRA'+ #13 +
    'add constraint PK_LISTA_NEGRA'+ #13 +
    'primary key(LISTA_ID)'+ #13 +
    'using index tablespace INDICES;'+ #13 +
    #13 +
    '/* Checagens */'+ #13 +
    'alter table LISTA_NEGRA'+ #13 +
    'add constraint CK_LISTA_NEGRA_ATIVO'+ #13 +
    'check(ATIVO in(''S'', ''N''));'+ #13
end;

function CriarTriggerLISTA_NEGRA_IU_BR: string;
begin
  Result :=
    'create or replace trigger LISTA_NEGRA_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on LISTA_NEGRA'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    'end LISTA_NEGRA_IU_BR;'+ #13
end;

function CriarSequenceSEQ_LISTA_NEGRA: string;
begin
  Result :=
    'create sequence SEQ_LISTA_NEGRA'+ #13 +
    'start with 1 increment by 1'+ #13 +
    'minvalue 1'+ #13 +
    'maxvalue 999'+ #13 +
    'nocycle'+ #13 +
    'nocache'+ #13 +
    'noorder;'+ #13
end;

function CriarNovasColunasCLIENTES: string;
begin
  Result :=
    'alter table CLIENTES'+ #13 +
    'add('+ #13 +
    '  LISTA_NEGRA_ID          number(3),'+ #13 +
    '  OBSERVACOES_LISTA_NEGRA varchar2(200)'+ #13 +
    ');'+ #13 +
    #13 +
    'alter table CLIENTES'+ #13 +
    'add constraint FK_CLIENTES_LISTA_NEGRA_ID'+ #13 +
    'foreign key(LISTA_NEGRA_ID)'+ #13 +
    'references LISTA_NEGRA(LISTA_ID);'+ #13 +
    #13 +
    'alter table CLIENTES'+ #13 +
    'add constraint CK_CLIENTES_LISTA_NEGRA'+ #13 +
    'check('+ #13 +
    '  LISTA_NEGRA_ID is null and OBSERVACOES_LISTA_NEGRA is null'+ #13 +
    '  or'+ #13 +
    '  LISTA_NEGRA_ID is not null and OBSERVACOES_LISTA_NEGRA is not null'+ #13 +
    ');'+ #13
end;

function AdicionarColunaVALOR_ADIANTADOTabelaCONTAS_RECEBER: string;
begin
  Result :=
    'alter table CONTAS_RECEBER'+ #13 +
    'add VALOR_ADIANTADO           number(8,2) default 0 not null;'+ #13 +
    #13 +
    'alter table CONTAS_RECEBER'+ #13 +
    'add constraint CK_CONTAS_REC_VALOR_ADIANTADO'+ #13 +
    'check('+ #13 +
    '  VALOR_ADIANTADO >= 0'+ #13 +
    '  and'+ #13 +
    '  VALOR_ADIANTADO < VALOR_DOCUMENTO - VALOR_RETENCAO'+ #13 +
    ');'+ #13
end;

function AdicionarColunaTIPO_COB_ADIANTAMENTO_FIN_ID: string;
begin
  Result :=
    'alter table PARAMETROS'+ #13 +
    'add   TIPO_COB_ADIANTAMENTO_FIN_ID    number(3);'+ #13 +
    #13 +
    'alter table PARAMETROS'+ #13 +
    'add constraint FK_PAR_TIPO_COBR_ADIAN_FINA_ID'+ #13 +
    'foreign key(TIPO_COB_ADIANTAMENTO_FIN_ID)'+ #13 +
    'references TIPOS_COBRANCA(COBRANCA_ID);'+ #13
end;

function AjustarTriggerTIPOS_COBRANCA_IU_BR: string;
begin
  Result :=
    'create or replace trigger TIPOS_COBRANCA_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on TIPOS_COBRANCA'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    '  vQtde number;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  if :old.FORMA_PAGAMENTO <> :new.FORMA_PAGAMENTO then'+ #13 +
    '    select'+ #13 +
    '      count(*)'+ #13 +
    '    into'+ #13 +
    '      vQtde'+ #13 +
    '    from'+ #13 +
    '      PARAMETROS'+ #13 +
    '    where :new.COBRANCA_ID in('+ #13 +
    '      TIPO_COBRANCA_GERACAO_CRED_ID,'+ #13 +
    '      TIPO_COBRANCA_GER_CRED_IMP_ID,'+ #13 +
    '      TIPO_COB_RECEB_ENTREGA_ID,'+ #13 +
    '      TIPO_COB_ACUMULATIVO_ID,'+ #13 +
    '      TIPO_COB_ADIANTAMENTO_ACU_ID,'+ #13 +
    '      TIPO_COB_ADIANTAMENTO_FIN_ID'+ #13 +
    '    );'+ #13 +
    #13 +
    '    if vQtde > 0 then'+ #13 +
    '      ERRO(''N�o � permitido alterar a forma de pagamento deste tipo de cobran�a pois ele est� ligado a um par�metro geral!'');'+ #13 +
    '    end if;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    #13 +
    'end TIPOS_COBRANCA_IU_BR;'+ #13
end;

function AdicionarColunaRECEBER_ADIANTADO_ID: string;
begin
  Result :=
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'add RECEBER_ADIANTADO_ID      number(12);'+ #13 +
    #13 +
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'add constraint FK_CONTAS_REC_BX_RECEB_ADI_ID'+ #13 +
    'foreign key(RECEBER_ADIANTADO_ID)'+ #13 +
    'references CONTAS_RECEBER(RECEBER_ID);';
end;

function AjustarConstraintCK_CONTAS_RECEBER_BAIXAS_TIPO2: string;
begin
  Result :=
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'drop constraint CK_CONTAS_RECEBER_BAIXAS_TIPO;'+ #13 +
    #13 +
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'add constraint CK_CONTAS_RECEBER_BAIXAS_TIPO'+ #13 +
    'check('+ #13 +
    '  TIPO in(''CRE'', ''BCR'', ''BCA'')'+ #13 +
    '  or'+ #13 +
    '  (TIPO = ''ECR'' and BAIXA_PAGAR_ORIGEM_ID is not null)'+ #13 +
    '  or'+ #13 +
    '  (TIPO = ''ADI'' and RECEBER_ADIANTADO_ID is not null)'+ #13 +
    ');'+ #13
end;

function AjustarConstraintCK_CONTAS_REC_BX_TIPO_CAD_ID2: string;
begin
  Result :=
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'drop constraint CK_CONTAS_REC_BX_TIPO_CAD_ID;'+ #13 +
    #13 +
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'add constraint CK_CONTAS_REC_BX_TIPO_CAD_ID'+ #13 +
    'check('+ #13 +
    '  (TIPO in(''CRE'', ''BCR'', ''ECR'', ''ADI'') and CADASTRO_ID is not null)'+ #13 +
    '  or'+ #13 +
    '  (TIPO = ''BCA'') and CADASTRO_ID is null'+ #13 +
    ');'+ #13
end;

function AdicionarColunaVALOR_ADIANTADOTabelaCONTAS_RECEBER_BAIXAS: string;
begin
  Result :=
    'alter table CONTAS_RECEBER_BAIXAS disable all triggers;'+ #13 +
    #13 +
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'add VALOR_ADIANTADO  number(8,2) default 0 not null;'+ #13 +
    #13 +
    'alter table CONTAS_RECEBER_BAIXAS enable all triggers;'+ #13
end;

function AdicionarCK_CONTAS_REC_BX_VALOR_ADIANT: string;
begin
  Result :=
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'add constraint CK_CONTAS_REC_BX_VALOR_ADIANT'+ #13 +
    'check(VALOR_ADIANTADO >= 0);'+ #13
end;

function AjustarCK_CONTAS_REC_BX_VALOR_LIQ: string;
begin
  Result :=
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'drop constraint CK_CONTAS_REC_BX_VALOR_LIQ;'+ #13 +
    #13 +
    'alter table CONTAS_RECEBER_BAIXAS'+ #13 +
    'add constraint CK_CONTAS_REC_BX_VALOR_LIQ'+ #13 +
    'check('+ #13 +
    '  VALOR_LIQUIDO = VALOR_TITULOS + VALOR_JUROS + VALOR_MULTA - VALOR_DESCONTO - VALOR_ADIANTADO - VALOR_RETENCAO'+ #13 +
    '  and'+ #13 +
    '  VALOR_LIQUIDO <= ('+ #13 +
    '    VALOR_DINHEIRO +'+ #13 +
    '    VALOR_CHEQUE +'+ #13 +
    '    VALOR_CARTAO_DEBITO +'+ #13 +
    '    VALOR_CARTAO_CREDITO +'+ #13 +
    '    VALOR_COBRANCA +'+ #13 +
    '    VALOR_CREDITO'+ #13 +
    '  )'+ #13 +
    ');'+ #13
end;

function AjustarTriggerCONTAS_RECEBER_BAIXAS_IU_BR: string;
begin
  Result :=
    'create or replace trigger CONTAS_RECEBER_BAIXAS_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on CONTAS_RECEBER_BAIXAS'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '	if inserting then'+ #13 +
    '		:new.DATA_HORA_BAIXA  := sysdate;'+ #13 +
    '		if :new.RECEBER_CAIXA = ''S'' then		'+ #13 +
    '			:new.USUARIO_ENVIO_CAIXA_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '		elsif :new.RECEBIDO = ''S'' then			'+ #13 +
    '			:new.USUARIO_BAIXA_ID := SESSAO.USUARIO_SESSAO_ID;  '+ #13 +
    '		end if;		'+ #13 +
    '	else'+ #13 +
    '		if :new.RECEBER_CAIXA = ''S'' and :old.RECEBIDO = ''N'' and :new.RECEBIDO = ''S'' then'+ #13 +
    '		  :new.USUARIO_BAIXA_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '		end if;		'+ #13 +
    '	end if;'+ #13 +
    #13 +
    '	:new.VALOR_LIQUIDO :='+ #13 +
    '		:new.VALOR_TITULOS +'+ #13 +
    '    :new.VALOR_MULTA +'+ #13 +
    '    :new.VALOR_JUROS -'+ #13 +
    '    :new.VALOR_ADIANTADO -'+ #13 +
    '		:new.VALOR_DESCONTO -'+ #13 +
    '    :new.VALOR_RETENCAO;'+ #13 +
    #13 +
    'end CONTAS_RECEBER_BAIXAS_IU_BR;'+ #13
end;

function AjusteVwVidaProdutoFisico: string;
begin
  Result :=
    'create or replace view VW_VIDA_PRODUTO_FISICO'+ #13 +
    'as'+ #13 +
    '/* Retiradas */'+ #13 +
    #13 +
    'select'+ #13 +
    '  case when RET.TIPO_MOVIMENTO = ''A'' then ''RAT'' else ''RET'' end as TIPO_MOVIMENTO,'+ #13 +
    '  RET.RETIRADA_ID as MOVIMENTO_ID,'+ #13 +
    '  RET.DATA_HORA_RETIRADA as DATA_HORA_MOVIMENTO,'+ #13 +
    '  RET.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  RIT.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  ORC.ORCAMENTO_ID AS NUMERODOCUMENTO,'+ #13 +
    '  ORC.CLIENTE_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  RETIRADAS_ITENS RIT'+ #13 +
    #13 +
    'inner join RETIRADAS RET'+ #13 +
    'on RIT.RETIRADA_ID = RET.RETIRADA_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on RIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where RET.CONFIRMADA = ''S'''+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Entregas */'+ #13 +
    'select'+ #13 +
    '  ''VEN'' TIPO_MOVIMENTO,'+ #13 +
    '  ENT.ENTREGA_ID as MOVIMENTO_ID,'+ #13 +
    '  ENT.DATA_HORA_CADASTRO as DATA_HORA_MOVIMENTO,'+ #13 +
    '  ENT.EMPRESA_ENTREGA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  EIT.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  ORC.ORCAMENTO_ID AS NUMERODOCUMENTO,'+ #13 +
    '  ORC.CLIENTE_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  ENTREGAS_ITENS EIT'+ #13 +
    #13 +
    'inner join ENTREGAS ENT'+ #13 +
    'on EIT.ENTREGA_ID = ENT.ENTREGA_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Retorno de entregas */'+ #13 +
    'select'+ #13 +
    '  ''REN'' TIPO_MOVIMENTO,'+ #13 +
    '  ENT.ENTREGA_ID as MOVIMENTO_ID,'+ #13 +
    '  ENT.DATA_HORA_CADASTRO as DATA_HORA_MOVIMENTO,'+ #13 +
    '  ENT.EMPRESA_ENTREGA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  EIT.RETORNADOS * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  ORC.ORCAMENTO_ID AS NUMERODOCUMENTO,'+ #13 +
    '  ORC.CLIENTE_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  ENTREGAS_ITENS EIT'+ #13 +
    #13 +
    'inner join ENTREGAS ENT'+ #13 +
    'on EIT.ENTREGA_ID = ENT.ENTREGA_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where EIT.RETORNADOS > 0'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Ajuste de estoque */'+ #13 +
    'select'+ #13 +
    '  ''AJU'' as TIPO_MOVIMENTO,'+ #13 +
    '  AJU.AJUSTE_ESTOQUE_ID,'+ #13 +
    '  AJU.DATA_HORA_AJUSTE,'+ #13 +
    '  AJU.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  AJI.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  AJI.NATUREZA,'+ #13 +
    '  AJU.AJUSTE_ESTOQUE_ID AS NUMERODOCUMENTO,'+ #13 +
    '  AJU.USUARIO_AJUSTE_ID AS CADASTRO_ID,'+ #13 +
    '  FUN.NOME AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  AJUSTES_ESTOQUE_ITENS AJI'+ #13 +
    #13 +
    'inner join AJUSTES_ESTOQUE AJU'+ #13 +
    'on AJI.AJUSTE_ESTOQUE_ID = AJU.AJUSTE_ESTOQUE_ID'+ #13 +
    #13 +
    'inner join FUNCIONARIOS FUN'+ #13 +
    'on FUN.FUNCIONARIO_ID = AJU.USUARIO_AJUSTE_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on AJI.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Devolu��es */'+ #13 +
    'select'+ #13 +
    '  ''DEV'' as TIPO_MOVIMENTO,'+ #13 +
    '  DEV.DEVOLUCAO_ID,'+ #13 +
    '  DEV.DATA_HORA_CONFIRMACAO,'+ #13 +
    '  DEV.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  DIT.DEVOLVIDOS_ENTREGUES * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  DEV.DEVOLUCAO_ID AS NUMERODOCUMENTO,'+ #13 +
    '  DEV.CLIENTE_CREDITO_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  DEVOLUCOES_ITENS DIT'+ #13 +
    #13 +
    'inner join DEVOLUCOES DEV'+ #13 +
    'on DIT.DEVOLUCAO_ID = DEV.DEVOLUCAO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on DEV.CLIENTE_CREDITO_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on DIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where DEV.STATUS = ''B'''+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Entradas de mercadorias */'+ #13 +
    'select'+ #13 +
    '  ''ENT'' as TIPO_MOVIMENTO,'+ #13 +
    '  ENT.ENTRADA_ID,'+ #13 +
    '  ENT.DATA_HORA_CADASTRO,'+ #13 +
    '  ENT.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  EIT.QUANTIDADE_ENTRADA_ALTIS * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  ENT.NUMERO_NOTA AS NUMERODOCUMENTO,'+ #13 +
    '  ENT.FORNECEDOR_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  ENTRADAS_NOTAS_FISCAIS_ITENS EIT'+ #13 +
    #13 +
    'inner join ENTRADAS_NOTAS_FISCAIS ENT'+ #13 +
    'on EIT.ENTRADA_ID = ENT.ENTRADA_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ENT.FORNECEDOR_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where ENT.STATUS in(''FCO'', ''ECO'', ''BAI'')'+ #13 +
    'and ENT.ORIGEM_ENTRADA <> ''TFP'''+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Transfer�ncia entre empresas - Sa�da */'+ #13 +
    'select'+ #13 +
    '  ''TPS'' as TIPO_MOVIMENTO,'+ #13 +
    '  TRA.TRANSFERENCIA_ID,'+ #13 +
    '  TRA.DATA_HORA_CADASTRO,'+ #13 +
    '  TRA.EMPRESA_ORIGEM_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  ITE.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  TRA.TRANSFERENCIA_ID AS NUMERODOCUMENTO,'+ #13 +
    '  TRA.USUARIO_CADASTRO_ID AS CADASTRO_ID,'+ #13 +
    '  FUN.NOME AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  TRANSF_PRODUTOS_EMPRESAS_ITENS ITE'+ #13 +
    #13 +
    'inner join TRANSF_PRODUTOS_EMPRESAS TRA'+ #13 +
    'on ITE.TRANSFERENCIA_ID = TRA.TRANSFERENCIA_ID'+ #13 +
    #13 +
    'inner join FUNCIONARIOS FUN'+ #13 +
    'on FUN.FUNCIONARIO_ID = TRA.USUARIO_CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Transfer�ncia entre empresas - Entrada */'+ #13 +
    'select'+ #13 +
    '  ''TPE'' as TIPO_MOVIMENTO,'+ #13 +
    '  TRA.TRANSFERENCIA_ID,'+ #13 +
    '  TRA.DATA_HORA_CADASTRO,'+ #13 +
    '  TRA.EMPRESA_DESTINO_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  ITE.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  TRA.TRANSFERENCIA_ID AS NUMERODOCUMENTO,'+ #13 +
    '  TRA.USUARIO_CADASTRO_ID AS CADASTRO_ID,'+ #13 +
    '  FUN.NOME AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  TRANSF_PRODUTOS_EMPRESAS_ITENS ITE'+ #13 +
    #13 +
    'inner join TRANSF_PRODUTOS_EMPRESAS TRA'+ #13 +
    'on ITE.TRANSFERENCIA_ID = TRA.TRANSFERENCIA_ID'+ #13 +
    #13 +
    'inner join FUNCIONARIOS FUN'+ #13 +
    'on FUN.FUNCIONARIO_ID = TRA.USUARIO_CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID;'+ #13
end;

function AjusteVwVidaProdutoDisponivel: string;
begin
  Result :=
    'create or replace view VW_VIDA_PRODUTO_DISPONIVEL'+ #13 +
    'as'+ #13 +
    '/* Retiradas */'+ #13 +
    'select'+ #13 +
    '  ''RAT'' as TIPO_MOVIMENTO,'+ #13 +
    '  RET.RETIRADA_ID as MOVIMENTO_ID,'+ #13 +
    '  RET.DATA_HORA_CADASTRO as DATA_HORA_MOVIMENTO,'+ #13 +
    '  ORC.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  RIT.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  ORC.ORCAMENTO_ID AS NUMERODOCUMENTO,'+ #13 +
    '  ORC.CLIENTE_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  RETIRADAS_ITENS RIT'+ #13 +
    #13 +
    'inner join RETIRADAS RET'+ #13 +
    'on RIT.RETIRADA_ID = RET.RETIRADA_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on RIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'where RET.TIPO_MOVIMENTO = ''A'''+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* A retirar */'+ #13 +
    'select'+ #13 +
    '  ''RET'' as TIPO_MOVIMENTO,'+ #13 +
    '  EIP.ORCAMENTO_ID as MOVIMENTO_ID,'+ #13 +
    '  PEN.DATA_HORA_CADASTRO as DATA_HORA_MOVIMENTO,'+ #13 +
    '  PEN.EMPRESA_ID as EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  EIP.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  EIP.ORCAMENTO_ID AS NUMERODOCUMENTO,'+ #13 +
    '  ORC.CLIENTE_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  RETIRADAS_ITENS_PENDENTES EIP'+ #13 +
    #13 +
    'inner join RETIRADAS_PENDENTES PEN'+ #13 +
    'on EIP.ORCAMENTO_ID = PEN.ORCAMENTO_ID'+ #13 +
    'and EIP.LOCAL_ID = PEN.LOCAL_ID'+ #13 +
    'and EIP.EMPRESA_ID = PEN.EMPRESA_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIP.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on EIP.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Entregas pendentes */'+ #13 +
    'select'+ #13 +
    '  ''ENG'' as TIPO_MOVIMENTO,'+ #13 +
    '  EIP.ORCAMENTO_ID as MOVIMENTO_ID,'+ #13 +
    '  PEN.DATA_HORA_CADASTRO as DATA_HORA_MOVIMENTO,'+ #13 +
    '  PEN.EMPRESA_ID as EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  EIP.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  EIP.ORCAMENTO_ID AS NUMERODOCUMENTO,'+ #13 +
    '  ORC.CLIENTE_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  ENTREGAS_ITENS_PENDENTES EIP'+ #13 +
    #13 +
    'inner join ENTREGAS_PENDENTES PEN'+ #13 +
    'on EIP.ORCAMENTO_ID = PEN.ORCAMENTO_ID'+ #13 +
    'and EIP.LOCAL_ID = PEN.LOCAL_ID'+ #13 +
    'and EIP.EMPRESA_ID = PEN.EMPRESA_ID'+ #13 +
    'and EIP.PREVISAO_ENTREGA = PEN.PREVISAO_ENTREGA'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIP.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on EIP.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Ajustes de estoque */'+ #13 +
    'select'+ #13 +
    '  ''AJU'' as TIPO_MOVIMENTO,'+ #13 +
    '  AJU.AJUSTE_ESTOQUE_ID,'+ #13 +
    '  AJU.DATA_HORA_AJUSTE,'+ #13 +
    '  AJU.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  AJI.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  AJI.NATUREZA,'+ #13 +
    '  AJU.AJUSTE_ESTOQUE_ID AS NUMERODOCUMENTO,'+ #13 +
    '  AJU.USUARIO_AJUSTE_ID AS CADASTRO_ID,'+ #13 +
    '  FUN.NOME AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  AJUSTES_ESTOQUE_ITENS AJI'+ #13 +
    #13 +
    'inner join AJUSTES_ESTOQUE AJU'+ #13 +
    'on AJI.AJUSTE_ESTOQUE_ID = AJU.AJUSTE_ESTOQUE_ID'+ #13 +
    #13 +
    'inner join FUNCIONARIOS FUN'+ #13 +
    'on FUN.FUNCIONARIO_ID = AJU.USUARIO_AJUSTE_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on AJI.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Devolu��es */'+ #13 +
    'select'+ #13 +
    '  ''DEV'' as TIPO_MOVIMENTO,'+ #13 +
    '  DEV.DEVOLUCAO_ID,'+ #13 +
    '  DEV.DATA_HORA_DEVOLUCAO,'+ #13 +
    '  DEV.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  (DIT.DEVOLVIDOS_ENTREGUES + DIT.DEVOLVIDOS_PENDENCIAS) * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  DEV.DEVOLUCAO_ID AS NUMERODOCUMENTO,'+ #13 +
    '  DEV.CLIENTE_CREDITO_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  DEVOLUCOES_ITENS DIT'+ #13 +
    #13 +
    'inner join DEVOLUCOES DEV'+ #13 +
    'on DIT.DEVOLUCAO_ID = DEV.DEVOLUCAO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on DEV.CLIENTE_CREDITO_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on DIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where DIT.DEVOLVIDOS_ENTREGUES + DIT.DEVOLVIDOS_PENDENCIAS > 0'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Entradas de mercadorias */'+ #13 +
    'select'+ #13 +
    '  ''ENT'' as TIPO_MOVIMENTO,'+ #13 +
    '  ENT.ENTRADA_ID,'+ #13 +
    '  ENT.DATA_HORA_CADASTRO,'+ #13 +
    '  ENT.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  (EIT.QUANTIDADE * EIT.QUANTIDADE_EMBALAGEM) * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  ENT.NUMERO_NOTA AS NUMERODOCUMENTO,'+ #13 +
    '  ENT.FORNECEDOR_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  ENTRADAS_NOTAS_FISCAIS_ITENS EIT'+ #13 +
    #13 +
    'inner join ENTRADAS_NOTAS_FISCAIS ENT'+ #13 +
    'on EIT.ENTRADA_ID = ENT.ENTRADA_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ENT.FORNECEDOR_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where ENT.STATUS = ''BAI'''+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Transfer�ncia entre empresas - Sa�da */'+ #13 +
    'select'+ #13 +
    '  ''TPS'' as TIPO_MOVIMENTO,'+ #13 +
    '  TRA.TRANSFERENCIA_ID,'+ #13 +
    '  TRA.DATA_HORA_CADASTRO,'+ #13 +
    '  TRA.EMPRESA_ORIGEM_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  ITE.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  TRA.TRANSFERENCIA_ID AS NUMERODOCUMENTO,'+ #13 +
    '  TRA.USUARIO_CADASTRO_ID AS CADASTRO_ID,'+ #13 +
    '  FUN.NOME AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  TRANSF_PRODUTOS_EMPRESAS_ITENS ITE'+ #13 +
    #13 +
    'inner join TRANSF_PRODUTOS_EMPRESAS TRA'+ #13 +
    'on ITE.TRANSFERENCIA_ID = TRA.TRANSFERENCIA_ID'+ #13 +
    #13 +
    'inner join FUNCIONARIOS FUN'+ #13 +
    'on FUN.FUNCIONARIO_ID = TRA.USUARIO_CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Transfer�ncia entre empresas - Entrada */'+ #13 +
    'select'+ #13 +
    '  ''TPE'' as TIPO_MOVIMENTO,'+ #13 +
    '  TRA.TRANSFERENCIA_ID,'+ #13 +
    '  TRA.DATA_HORA_CADASTRO,'+ #13 +
    '  TRA.EMPRESA_DESTINO_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  ITE.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  TRA.TRANSFERENCIA_ID AS NUMERODOCUMENTO,'+ #13 +
    '  TRA.USUARIO_CADASTRO_ID AS CADASTRO_ID,'+ #13 +
    '  FUN.NOME AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  TRANSF_PRODUTOS_EMPRESAS_ITENS ITE'+ #13 +
    #13 +
    'inner join TRANSF_PRODUTOS_EMPRESAS TRA'+ #13 +
    'on ITE.TRANSFERENCIA_ID = TRA.TRANSFERENCIA_ID'+ #13 +
    #13 +
    'inner join FUNCIONARIOS FUN'+ #13 +
    'on FUN.FUNCIONARIO_ID = TRA.USUARIO_CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID;'+ #13
end;

function AjusteVwVidaProdutoFiscal: string;
begin
  Result :=
    'create or replace view VW_VIDA_PRODUTO_FISCAL'+ #13 +
    'as'+ #13 +
    '/* Notas fiscais emitidas */'+ #13 +
    'select'+ #13 +
    '  NFI.TIPO_MOVIMENTO,'+ #13 +
    '  NFI.NOTA_FISCAL_ID as MOVIMENTO_ID,'+ #13 +
    '  NFI.DATA_HORA_EMISSAO as DATA_HORA_MOVIMENTO,'+ #13 +
    '  NFI.EMPRESA_ID,'+ #13 +
    '  NIT.PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  NIT.QUANTIDADE,'+ #13 +
    '  case when substr(NIT.CFOP_ID, 1, 1) in(1,2,3) then ''E'' else ''S'' end as NATUREZA,'+ #13 +
    '  NFI.NUMERO_NOTA AS NUMERODOCUMENTO,'+ #13 +
    '  NFI.CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  NOTAS_FISCAIS_ITENS NIT'+ #13 +
    #13 +
    'inner join NOTAS_FISCAIS NFI'+ #13 +
    'on NIT.NOTA_FISCAL_ID = NFI.NOTA_FISCAL_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on NFI.CADASTRO_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on NIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where NFI.STATUS = ''E'''+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    'select'+ #13 +
    '  ''ENT'' as TIPO_MOVIMENTO,'+ #13 +
    '  ENT.ENTRADA_ID,'+ #13 +
    '  ENT.DATA_HORA_CADASTRO,'+ #13 +
    '  ENT.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  EIT.QUANTIDADE * EIT.QUANTIDADE_EMBALAGEM * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  ENT.NUMERO_NOTA AS NUMERODOCUMENTO,'+ #13 +
    '  ENT.FORNECEDOR_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOMECADASTRO'+ #13 +
    'from'+ #13 +
    '  ENTRADAS_NOTAS_FISCAIS_ITENS EIT'+ #13 +
    #13 +
    'inner join ENTRADAS_NOTAS_FISCAIS ENT'+ #13 +
    'on EIT.ENTRADA_ID = ENT.ENTRADA_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ENT.FORNECEDOR_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIT.PRODUTO_ID = PRO.PRODUTO_ID;'+ #13
end;

function CriarTabelaCONTAS_CUSTODIA: string;
begin
  Result :=
    'create table CONTAS_CUSTODIA('+ #13 +
    '  CONTA_CUSTODIA_ID           number(3) not null,'+ #13 +
    '  NOME                        varchar2(60) not null,  '+ #13 +
    '  ATIVO                       char(1) default ''S'' not null,'+ #13 +
    #13 +
    '  /* Colunas de logs do sistema */'+ #13 +
    '  USUARIO_SESSAO_ID       number(4) not null,'+ #13 +
    '  DATA_HORA_ALTERACAO     date not null,'+ #13 +
    '  ROTINA_ALTERACAO        varchar2(30) not null,'+ #13 +
    '  ESTACAO_ALTERACAO       varchar2(30) not null  '+ #13 +
    ');'+ #13 +
    #13 +
    '/* Chave prim�ria */'+ #13 +
    'alter table CONTAS_CUSTODIA'+ #13 +
    'add constraint PK_CONTAS_CUSTODIA'+ #13 +
    'primary key(CONTA_CUSTODIA_ID)'+ #13 +
    'using index tablespace INDICES;'+ #13 +
    #13 +
    '/* Checagens */'+ #13 +
    'alter table CONTAS_CUSTODIA'+ #13 +
    'add constraint CK_CONTAS_CUSTODIA_ATIVO'+ #13 +
    'check(ATIVO in(''S'', ''N''));'+ #13
end;

function CriarTriggerCONTAS_CUSTODIA_IU_BR: string;
begin
  Result :=
    'create or replace trigger CONTAS_CUSTODIA_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on CONTAS_CUSTODIA'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    'end CONTAS_CUSTODIA_IU_BR;'+ #13
end;

function CriarSequenceSEQ_CONTAS_CUSTODIA: string;
begin
  Result :=
    'create sequence SEQ_CONTAS_CUSTODIA'+ #13 +
    'start with 1 increment by 1'+ #13 +
    'minvalue 1'+ #13 +
    'maxvalue 999'+ #13 +
    'nocycle'+ #13 +
    'nocache'+ #13 +
    'noorder;'+ #13
end;

function CriarNovasColunasCONTAS_RECEBER: string;
begin
  Result :=
    'alter table CONTAS_RECEBER'+ #13 +
    'add ('+ #13 +
    '  CONTA_CUSTODIA_ID           number(3),'+ #13 +
    '  OBSERVACOES_CONTA_CUSTODIA  varchar2(200)'+ #13 +
    ');'+ #13 +
    #13 +
    'alter table CONTAS_RECEBER'+ #13 +
    'add constraint CK_CONTAS_REC_OBSEV_CON_CUS'+ #13 +
    'check('+ #13 +
    '  CONTA_CUSTODIA_ID is null and OBSERVACOES_CONTA_CUSTODIA is null'+ #13 +
    '  or'+ #13 +
    '  CONTA_CUSTODIA_ID is not null and OBSERVACOES_CONTA_CUSTODIA is not null'+ #13 +
    ');'+ #13
end;

function AddFK_CONTAS_REC_CON_CUSTODIA_ID: string;
begin
  Result :=
    'alter table CONTAS_RECEBER'+ #13 +
    'add constraint FK_CONTAS_REC_CON_CUSTODIA_ID'+ #13 +
    'foreign key(CONTA_CUSTODIA_ID)'+ #13 +
    'references CONTAS_CUSTODIA(CONTA_CUSTODIA_ID);'+ #13
end;

function AjustarTriggerCONTAS_RECEBER_U_AR2: string;
begin
  Result :=
    'create or replace trigger CONTAS_RECEBER_U_AR'+ #13 +
    'after update '+ #13 +
    'on CONTAS_RECEBER'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    #13 +
    '  procedure INSERIR_LOG(pTIPO_ALTERACAO_LOG_ID in number, pVALOR_ANTERIOR in string, pNOVO_VALOR in string)'+ #13 +
    '  is begin'+ #13 +
    '    if nvl(pVALOR_ANTERIOR, ''X'') <> nvl(pNOVO_VALOR, ''X'') then'+ #13 +
    '      insert into LOGS_CONTAS_RECEBER(TIPO_ALTERACAO_LOG_ID, RECEBER_ID, VALOR_ANTERIOR, NOVO_VALOR)'+ #13 +
    '      values(pTIPO_ALTERACAO_LOG_ID, :new.RECEBER_ID, pVALOR_ANTERIOR, pNOVO_VALOR);'+ #13 +
    '    end if;'+ #13 +
    '  end;'+ #13 +
    #13 +
    'begin'+ #13 +
    '  /* View VW_TIPOS_ALTER_LOGS_CT_RECEBER */'+ #13 +
    #13 +
    '  INSERIR_LOG(1, :old.BAIXA_ID, :new.BAIXA_ID);'+ #13 +
    '  INSERIR_LOG(2, :old.STATUS, :new.STATUS);'+ #13 +
    '  INSERIR_LOG(3, :old.DATA_VENCIMENTO, :new.DATA_VENCIMENTO);'+ #13 +
    '  INSERIR_LOG(4, :old.VALOR_DOCUMENTO, :new.VALOR_DOCUMENTO);'+ #13 +
    '  INSERIR_LOG(5, :old.PARCELA, :new.PARCELA);'+ #13 +
    '  INSERIR_LOG(6, :old.NUMERO_PARCELAS, :new.NUMERO_PARCELAS);'+ #13 +
    '  INSERIR_LOG(7, :old.COBRANCA_ID, :new.COBRANCA_ID);'+ #13 +
    '  INSERIR_LOG(8, :old.CONTA_CUSTODIA_ID, :new.CONTA_CUSTODIA_ID);'+ #13 +
    '  INSERIR_LOG(9, :old.OBSERVACOES_CONTA_CUSTODIA, :new.OBSERVACOES_CONTA_CUSTODIA);'+ #13 +
    #13 +
    'end CONTAS_RECEBER_U_AR;'+ #13
end;

function AjustarViewVW_TIPOS_ALTER_LOGS_CT_RECEBER2: string;
begin
  Result :=
    'create or replace view VW_TIPOS_ALTER_LOGS_CT_RECEBER'+ #13 +
    'as'+ #13 +
    'select 1 as TIPO_ALTERACAO_LOG_ID, ''C�digo da baixa'' as CAMPO from dual union'+ #13 +
    'select 2 as TIPO_ALTERACAO_LOG_ID, ''Status'' as CAMPO from dual union'+ #13 +
    'select 3 as TIPO_ALTERACAO_LOG_ID, ''Data vencimento'' as CAMPO from dual union'+ #13 +
    'select 4 as TIPO_ALTERACAO_LOG_ID, ''Valor documento'' as CAMPO from dual union'+ #13 +
    'select 5 as TIPO_ALTERACAO_LOG_ID, ''Parcela'' as CAMPO from dual union'+ #13 +
    'select 6 as TIPO_ALTERACAO_LOG_ID, ''Quantidade de parcelas'' as CAMPO from dual union'+ #13 +
    'select 7 as TIPO_ALTERACAO_LOG_ID, ''Tipo de cobran�a'' as CAMPO from dual union'+ #13 +
    'select 8 as TIPO_ALTERACAO_LOG_ID, ''Conta cust�dia'' as CAMPO from dual union'+ #13 +
    'select 9 as TIPO_ALTERACAO_LOG_ID, ''Observa��es da conta cust.'' as CAMPO from dual;'+ #13
end;

function AjustarViewVW_ESTOQUES_DIVISAO: string;
begin
  Result :=
    'create or replace view VW_ESTOQUES_DIVISAO'+ #13 +
    'as'+ #13 +
    'select'+ #13 +
    '  EDI.EMPRESA_ID,'+ #13 +
    '  EDI.LOCAL_ID,'+ #13 +
    '  LOC.NOME as NOME_LOCAL,'+ #13 +
    '  PRO.PRODUTO_ID,'+ #13 +
    '  PRO.NOME as NOME_PRODUTO,'+ #13 +
    '  MAR.NOME as NOME_MARCA,'+ #13 +
    '  PRO.UNIDADE_VENDA,'+ #13 +
    '  PRO.TIPO_CONTROLE_ESTOQUE,  '+ #13 +
    '  EDI.LOTE,'+ #13 +
    '  EDI.FISICO / PRO.QUANTIDADE_VEZES_PAI as FISICO,'+ #13 +
    '  EDI.DISPONIVEL / PRO.QUANTIDADE_VEZES_PAI as DISPONIVEL,'+ #13 +
    '  EDI.RESERVADO / PRO.QUANTIDADE_VEZES_PAI as RESERVADO,'+ #13 +
    '  EDI.MULTIPLO_PONTA_ESTOQUE,'+ #13 +
    '  EDI.PONTA_ESTOQUE,'+ #13 +
    '  PRO.ACEITAR_ESTOQUE_NEGATIVO,'+ #13 +
    '  LOT.DATA_FABRICACAO,'+ #13 +
    '  LOT.DATA_VENCIMENTO,'+ #13 +
    '  PRO.PRODUTO_PAI_ID,'+ #13 +
    '  round(zvl(CUS.PRECO_FINAL, CUS.PRECO_LIQUIDO) * PRO.QUANTIDADE_VEZES_PAI, 2) as CUSTO_TRANSFERENCIA'+ #13 +
    'from'+ #13 +
    '  PRODUTOS PRO'+ #13 +
    #13 +
    'inner join ESTOQUES_DIVISAO EDI'+ #13 +
    'on PRO.PRODUTO_PAI_ID = EDI.PRODUTO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS_LOTES LOT'+ #13 +
    'on EDI.PRODUTO_ID = LOT.PRODUTO_ID'+ #13 +
    'and EDI.LOTE = LOT.LOTE'+ #13 +
    #13 +
    'inner join LOCAIS_PRODUTOS LOC'+ #13 +
    'on EDI.LOCAL_ID = LOC.LOCAL_ID'+ #13 +
    #13 +
    'inner join CUSTOS_PRODUTOS CUS'+ #13 +
    'on PRO.PRODUTO_PAI_ID = CUS.PRODUTO_ID'+ #13 +
    'and EDI.EMPRESA_ID = CUS.EMPRESA_ID'+ #13 +
    #13 +
    'inner join MARCAS MAR'+ #13 +
    'on PRO.MARCA_ID = MAR.MARCA_ID;'+ #13
end;

function AddColunaEMITIR_DUPLIC_MERCANTIL_VENDA: string;
begin
  Result :=
    'alter table TIPOS_COBRANCA'+ #13 +
    'add   EMITIR_DUPLIC_MERCANTIL_VENDA char(1) default ''N'' not null;'+ #13 +
    #13 +
    'alter table TIPOS_COBRANCA'+ #13 +
    'add constraint CK_TP_COB_EMITIR_DUP_MERC_VEN'+ #13 +
    'check(EMITIR_DUPLIC_MERCANTIL_VENDA in(''S'', ''N''));'+ #13
end;

function AjustarTriggerTIPOS_COBRANCA_U_AR2: string;
begin
  Result :=
    'create or replace trigger TIPOS_COBRANCA_U_AR'+ #13 +
    'after update '+ #13 +
    'on TIPOS_COBRANCA'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    #13 +
    '  procedure INSERIR_LOG(pTIPO_ALTERACAO_LOG_ID in number, pVALOR_ANTERIOR in string, pNOVO_VALOR in string)'+ #13 +
    '  is begin'+ #13 +
    '    if nvl(pVALOR_ANTERIOR, ''X'') <> nvl(pNOVO_VALOR, ''X'') then'+ #13 +
    '      insert into LOGS_TIPOS_COBRANCA(TIPO_ALTERACAO_LOG_ID, COBRANCA_ID, VALOR_ANTERIOR, NOVO_VALOR)'+ #13 +
    '      values(pTIPO_ALTERACAO_LOG_ID, :new.COBRANCA_ID, pVALOR_ANTERIOR, pNOVO_VALOR);'+ #13 +
    '    end if;'+ #13 +
    '  end;'+ #13 +
    #13 +
    'begin'+ #13 +
    '  /* View VW_TIPOS_ALTER_LOGS_TP_COBRANC */'+ #13 +
    #13 +
    '  INSERIR_LOG(1, :old.PORTADOR_ID, :new.PORTADOR_ID);'+ #13 +
    '  INSERIR_LOG(2, :old.NOME, :new.NOME);'+ #13 +
    '  INSERIR_LOG(3, :old.TAXA_RETENCAO_MES, :new.TAXA_RETENCAO_MES);'+ #13 +
    '  INSERIR_LOG(4, :old.FORMA_PAGAMENTO, :new.FORMA_PAGAMENTO);'+ #13 +
    '  INSERIR_LOG(5, :old.REDE_CARTAO, :new.REDE_CARTAO);'+ #13 +
    '  INSERIR_LOG(6, :old.TIPO_CARTAO, :new.TIPO_CARTAO);'+ #13 +
    '  INSERIR_LOG(7, :old.TIPO_RECEBIMENTO_CARTAO, :new.TIPO_RECEBIMENTO_CARTAO);'+ #13 +
    '  INSERIR_LOG(8, :old.TIPO_PAGAMENTO_COMISSAO, :new.TIPO_PAGAMENTO_COMISSAO);'+ #13 +
    '  INSERIR_LOG(9, :old.PERCENTUAL_COMISSAO, :new.PERCENTUAL_COMISSAO);'+ #13 +
    '  INSERIR_LOG(10, :old.UTILIZAR_LIMITE_CRED_CLIENTE, :new.UTILIZAR_LIMITE_CRED_CLIENTE);'+ #13 +
    '  INSERIR_LOG(11, :old.BOLETO_BANCARIO, :new.BOLETO_BANCARIO);'+ #13 +
    '  INSERIR_LOG(12, :old.INCIDENCIA_FINANCEIRA, :new.INCIDENCIA_FINANCEIRA);'+ #13 +
    '  INSERIR_LOG(13, :old.ATIVO, :new.ATIVO);'+ #13 +
    '  INSERIR_LOG(14, :old.PERMITIR_PRAZO_MEDIO, :new.PERMITIR_PRAZO_MEDIO);'+ #13 +
    '  INSERIR_LOG(15, :old.PRAZO_MEDIO, :new.PRAZO_MEDIO);'+ #13 +
    '  INSERIR_LOG(16, :old.ADMINISTRADORA_CARTAO_ID, :new.ADMINISTRADORA_CARTAO_ID);'+ #13 +
    '  INSERIR_LOG(17, :old.EMITIR_DUPLIC_MERCANTIL_VENDA, :new.EMITIR_DUPLIC_MERCANTIL_VENDA);'+ #13 +
    #13 +
    'end TIPOS_COBRANCA_U_AR;'+ #13
end;

function AjustarViewVW_TIPOS_ALTER_LOGS_TP_COBRANC2: string;
begin
  Result :=
    'create or replace view VW_TIPOS_ALTER_LOGS_TP_COBRANC'+ #13 +
    'as'+ #13 +
    'select 1 as TIPO_ALTERACAO_LOG_ID, ''Portador'' as CAMPO from dual union'+ #13 +
    'select 2 as TIPO_ALTERACAO_LOG_ID, ''Nome'' as CAMPO from dual union'+ #13 +
    'select 3 as TIPO_ALTERACAO_LOG_ID, ''Taxa de reten��o'' as CAMPO from dual union'+ #13 +
    'select 4 as TIPO_ALTERACAO_LOG_ID, ''Forma de pagamento'' as CAMPO from dual union'+ #13 +
    'select 5 as TIPO_ALTERACAO_LOG_ID, ''Rede do cart�o'' as CAMPO from dual union'+ #13 +
    'select 6 as TIPO_ALTERACAO_LOG_ID, ''Tipo de cart�o'' as CAMPO from dual union'+ #13 +
    'select 7 as TIPO_ALTERACAO_LOG_ID, ''Tipo de receb. cart�o'' as CAMPO from dual union'+ #13 +
    'select 8 as TIPO_ALTERACAO_LOG_ID, ''Tipo de pagto. comiss�o'' as CAMPO from dual union'+ #13 +
    'select 9 as TIPO_ALTERACAO_LOG_ID, ''% Comiss�o'' as CAMPO from dual union'+ #13 +
    'select 10 as TIPO_ALTERACAO_LOG_ID, ''Utilizar lim. cr�dito do cliente'' as CAMPO from dual union'+ #13 +
    'select 11 as TIPO_ALTERACAO_LOG_ID, ''Boleto banc�rio?'' as CAMPO from dual union'+ #13 +
    'select 12 as TIPO_ALTERACAO_LOG_ID, ''Incid�ncia financeira'' as CAMPO from dual union'+ #13 +
    'select 13 as TIPO_ALTERACAO_LOG_ID, ''Ativo?'' as CAMPO from dual union'+ #13 +
    'select 14 as TIPO_ALTERACAO_LOG_ID, ''Permitir prazo m�dio?'' as CAMPO from dual union'+ #13 +
    'select 15 as TIPO_ALTERACAO_LOG_ID, ''Prazo m�dio'' as CAMPO from dual union'+ #13 +
    'select 16 as TIPO_ALTERACAO_LOG_ID, ''Administradora de cart�o'' as CAMPO from dual union'+ #13 +
    'select 17 as TIPO_ALTERACAO_LOG_ID, ''Emitir dupl.mercantil de venda'' as CAMPO from dual;'+ #13
end;

function AjustarProcedureSETAR_NFE_EMITIDA: string;
begin
  Result :=
    'create or replace procedure SETAR_NFE_EMITIDA('+ #13 +
    '  iNOTA_FISCAL_ID          in positiven,'+ #13 +
    '  iCHAVE_NFE               in string,'+ #13 +
    '  iPROTOCOLO_NFE           in string,'+ #13 +
    '  iDATA_HORA_EMISSAO       in date,'+ #13 +
    '  iDATA_HORA_PROTOCOLO_NFE in date'+ #13 +
    ')'+ #13 +
    'is'+ #13 +
    '  vEmpresaId            EMPRESAS.EMPRESA_ID%type;'+ #13 +
    '  vTipoMovimento        NOTAS_FISCAIS.TIPO_MOVIMENTO%type;'+ #13 +
    '  vMovimentarEstOutras  NOTAS_FISCAIS.MOVIMENTAR_EST_OUTRAS_NOTAS%type;'+ #13 +
    '  vNaturezaEntrada      char(1);'+ #13 +
    '  vEntradaId            ENTRADAS_NOTAS_FISCAIS.ENTRADA_ID%type;'+ #13 +
    #13 +
    '  cursor cItens is'+ #13 +
    '  select'+ #13 +
    '    PRODUTO_ID,'+ #13 +
    '    QUANTIDADE'+ #13 +
    '  from'+ #13 +
    '    NOTAS_FISCAIS_ITENS'+ #13 +
    '  where NOTA_FISCAL_ID = iNOTA_FISCAL_ID;'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  update NOTAS_FISCAIS set'+ #13 +
    '    DATA_HORA_EMISSAO = iDATA_HORA_EMISSAO,'+ #13 +
    '    CHAVE_NFE =  iCHAVE_NFE,'+ #13 +
    '    PROTOCOLO_NFE = iPROTOCOLO_NFE,'+ #13 +
    '    DATA_HORA_PROTOCOLO_NFE = iDATA_HORA_PROTOCOLO_NFE,'+ #13 +
    '    STATUS_NFE = ''100'','+ #13 +
    '    STATUS = ''E'','+ #13 +
    '    MOTIVO_STATUS_NFE = ''AUTORIZADO O USO DA NF-E'''+ #13 +
    '  where NOTA_FISCAL_ID = iNOTA_FISCAL_ID;'+ #13 +
    #13 +
    '  select'+ #13 +
    '    EMPRESA_ID,'+ #13 +
    '    TIPO_MOVIMENTO,'+ #13 +
    '    MOVIMENTAR_EST_OUTRAS_NOTAS,'+ #13 +
    '    case when substr(CFOP_ID, 1, 1) in(''1'', ''2'', ''3'') then ''S'' else ''N'' end'+ #13 +
    '  into'+ #13 +
    '    vEmpresaId,'+ #13 +
    '    vTipoMovimento,'+ #13 +
    '    vMovimentarEstOutras,'+ #13 +
    '    vNaturezaEntrada'+ #13 +
    '  from NOTAS_FISCAIS'+ #13 +
    '  where NOTA_FISCAL_ID = iNOTA_FISCAL_ID;'+ #13 +
    #13 +
    '  for vItens in cItens loop'+ #13 +
    '    if (vTipoMovimento <> ''OUT'') or (vMovimentarEstOutras = ''S'') then'+ #13 +
    '      MOVIMENTAR_ESTOQUE_FISCAL(vEmpresaId, vItens.PRODUTO_ID, vItens.QUANTIDADE * case when vNaturezaEntrada = ''S'' then 1 else -1 end);'+ #13 +
    '    end if;'+ #13 +
    '  end loop;'+ #13 +
    #13 +
    '  if vTipoMovimento = ''TPE'' then'+ #13 +
    '    GERAR_ENTR_TRANS_PRODUTOS_EMP(iNOTA_FISCAL_ID, ''TPE'');'+ #13 +
    '  elsif vTipoMovimento in(''TFR'', ''TFE'') then'+ #13 +
    '    GERAR_ENTR_TRANS_PRODUTOS_EMP(iNOTA_FISCAL_ID, ''TFP'');'+ #13 +
    #13 +
    '    update ENTRADAS_NOTAS_FISCAIS set'+ #13 +
    '      STATUS = ''ECO'''+ #13 +
    '    where NOTA_TRANSF_PROD_ORIGEM_ID = iNOTA_FISCAL_ID;'+ #13 +
    #13 +
    '    select'+ #13 +
    '       ENTRADA_ID'+ #13 +
    '    into'+ #13 +
    '      vEntradaId'+ #13 +
    '    from'+ #13 +
    '      ENTRADAS_NOTAS_FISCAIS'+ #13 +
    '    where NOTA_TRANSF_PROD_ORIGEM_ID = iNOTA_FISCAL_ID;'+ #13 +
    #13 +
    '    CONS_ESTOQUE_ENTR_NOTA_FISCAL(vEntradaId);'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end SETAR_NFE_EMITIDA;'+ #13
end;

function AjustarProcGERAR_ENTR_TRANS_PRODUTOS_EMP: string;
begin
  Result :=
    'create or replace procedure GERAR_ENTR_TRANS_PRODUTOS_EMP(iMOVIMENTO_ID in number, iTIPO_MOVIMENTO in string)'+ #13 +
    'is'+ #13 +
    '  vNotaFiscalSaidaId NOTAS_FISCAIS.NOTA_FISCAL_ID%type;'+ #13 +
    '  vEntradaId         ENTRADAS_NOTAS_FISCAIS.ENTRADA_ID%type;'+ #13 +
    '  vStatus            NOTAS_FISCAIS.STATUS%type;'+ #13 +
    '  vChaveNFe          ENTRADAS_NOTAS_FISCAIS.CHAVE_NFE%type;'+ #13 +
    '  vFornecedorId      FORNECEDORES.CADASTRO_ID%type;'+ #13 +
    '  vQtde              number;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  if iTIPO_MOVIMENTO not in(''TPE'', ''TFP'') then'+ #13 +
    '    ERRO(''O tipo de movimento s� pode ser "TPE" ou "TFP"!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  select'+ #13 +
    '    NOTA_FISCAL_ID,'+ #13 +
    '    STATUS,'+ #13 +
    '    CHAVE_NFE,'+ #13 +
    '    CADASTRO_ID'+ #13 +
    '  into'+ #13 +
    '    vNotaFiscalSaidaId,'+ #13 +
    '    vStatus,'+ #13 +
    '    vChaveNFe,'+ #13 +
    '    vFornecedorId'+ #13 +
    '  from'+ #13 +
    '    NOTAS_FISCAIS'+ #13 +
    '  where NOTA_FISCAL_ID = iMOVIMENTO_ID;'+ #13 +
    #13 +
    '  if vStatus <> ''E'' then'+ #13 +
    '    ERRO(''Apenas notas fiscais j� emitidas podem gerar entrada de transfer�ncia de produtos entre empresas!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  select'+ #13 +
    '    count(*)'+ #13 +
    '  into'+ #13 +
    '    vQtde'+ #13 +
    '  from'+ #13 +
    '    FORNECEDORES'+ #13 +
    '  where CADASTRO_ID = vFornecedorId;'+ #13 +
    #13 +
    '  if vQtde = 0 then'+ #13 +
    '    ERRO(''O destinat�rio da NFe n�o est� cadastrado como fornecedor, por favor fa�a o cadastro para que seja realizada a entrada da NFe na empresa de destino!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  vChaveNFe :='+ #13 +
    '    substr(vChaveNFe, 1, 4) || ''.'' ||'+ #13 +
    '    substr(vChaveNFe, 5, 4) || ''.'' ||'+ #13 +
    '    substr(vChaveNFe, 9, 4) || ''.'' ||'+ #13 +
    '    substr(vChaveNFe, 13, 4) || ''.'' ||'+ #13 +
    '    substr(vChaveNFe, 17, 4) || ''.'' ||'+ #13 +
    '    substr(vChaveNFe, 21, 4) || ''.'' ||'+ #13 +
    '    substr(vChaveNFe, 25, 4) || ''.'' ||'+ #13 +
    '    substr(vChaveNFe, 29, 4) || ''.'' ||'+ #13 +
    '    substr(vChaveNFe, 33, 4) || ''.'' ||'+ #13 +
    '    substr(vChaveNFe, 37, 4) || ''.'' ||'+ #13 +
    '    substr(vChaveNFe, 41, 4);'+ #13 +
    #13 +
    '  vEntradaId := SEQ_ENTRADA_NOTAS_FISC_ENT_ID.nextval;'+ #13 +
    #13 +
    '  insert into ENTRADAS_NOTAS_FISCAIS('+ #13 +
    '    ENTRADA_ID,'+ #13 +
    '    FORNECEDOR_ID,'+ #13 +
    '    CFOP_ID,'+ #13 +
    '    EMPRESA_ID,'+ #13 +
    '    ORIGEM_ENTRADA,'+ #13 +
    '    NOTA_TRANSF_PROD_ORIGEM_ID,'+ #13 +
    '    NUMERO_NOTA,'+ #13 +
    '    MODELO_NOTA,'+ #13 +
    '    SERIE_NOTA,    '+ #13 +
    '    TIPO_RATEIO_FRETE,    '+ #13 +
    '    VALOR_TOTAL,'+ #13 +
    '    VALOR_TOTAL_PRODUTOS,'+ #13 +
    '    BASE_CALCULO_ICMS,'+ #13 +
    '    VALOR_ICMS,'+ #13 +
    '    BASE_CALCULO_ICMS_ST,'+ #13 +
    '    VALOR_ICMS_ST,'+ #13 +
    '    BASE_CALCULO_PIS,'+ #13 +
    '    VALOR_PIS,'+ #13 +
    '    BASE_CALCULO_COFINS,'+ #13 +
    '    VALOR_COFINS,    '+ #13 +
    '    PESO_LIQUIDO,'+ #13 +
    '    PESO_BRUTO,'+ #13 +
    '    DATA_HORA_EMISSAO,'+ #13 +
    '    DATA_ENTRADA,'+ #13 +
    '    DATA_PRIMEIRA_PARCELA,'+ #13 +
    '    STATUS,'+ #13 +
    '    PLANO_FINANCEIRO_ID,    '+ #13 +
    '    CHAVE_NFE'+ #13 +
    '  )'+ #13 +
    '  select'+ #13 +
    '    vEntradaId,'+ #13 +
    '    vFornecedorId,'+ #13 +
    '    NFI.CFOP_ID,'+ #13 +
    '    EEN.EMPRESA_ID,'+ #13 +
    '    iTIPO_MOVIMENTO,'+ #13 +
    '    vNotaFiscalSaidaId,'+ #13 +
    '    NFI.NUMERO_NOTA,'+ #13 +
    '    ''55'','+ #13 +
    '    NFI.SERIE_NOTA,'+ #13 +
    '    ''V'','+ #13 +
    '    NFI.VALOR_TOTAL,'+ #13 +
    '    NFI.VALOR_TOTAL_PRODUTOS,'+ #13 +
    '    NFI.BASE_CALCULO_ICMS,'+ #13 +
    '    NFI.VALOR_ICMS,'+ #13 +
    '    NFI.BASE_CALCULO_ICMS_ST,'+ #13 +
    '    NFI.VALOR_ICMS_ST,'+ #13 +
    '    NFI.BASE_CALCULO_PIS,'+ #13 +
    '    NFI.VALOR_PIS,'+ #13 +
    '    NFI.BASE_CALCULO_COFINS,'+ #13 +
    '    NFI.VALOR_COFINS,'+ #13 +
    '    NFI.PESO_LIQUIDO,'+ #13 +
    '    NFI.PESO_BRUTO,'+ #13 +
    '    NFI.DATA_HORA_EMISSAO,'+ #13 +
    '    trunc(sysdate), -- DATA_ENTRADA'+ #13 +
    '    trunc(sysdate),'+ #13 +
    '    ''ACM'','+ #13 +
    '    ''2.001.001'','+ #13 +
    '    vChaveNFe'+ #13 +
    '  from'+ #13 +
    '    NOTAS_FISCAIS NFI'+ #13 +
    #13 +
    '  /* Trazendo a empresa da sa�da */'+ #13 +
    '  inner join EMPRESAS EMP'+ #13 +
    '  on NFI.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
    #13 +
    '  /* Trazendo a empresa que ser� realizada a entrada pelo c�digo do cadastro */'+ #13 +
    '  inner join EMPRESAS EEN'+ #13 +
    '  on NFI.CADASTRO_ID = EEN.CADASTRO_ID'+ #13 +
    #13 +
    '  where NFI.NOTA_FISCAL_ID = vNotaFiscalSaidaId;'+ #13 +
    #13 +
    '  insert into ENTRADAS_NOTAS_FISCAIS_ITENS('+ #13 +
    '    ENTRADA_ID,'+ #13 +
    '    PRODUTO_ID,'+ #13 +
    '    ITEM_ID,'+ #13 +
    '    CFOP_ID,'+ #13 +
    '    LOCAL_ENTRADA_ID,'+ #13 +
    '    CST,'+ #13 +
    '    CODIGO_NCM,'+ #13 +
    '    VALOR_TOTAL,'+ #13 +
    '    PRECO_UNITARIO,'+ #13 +
    '    QUANTIDADE,'+ #13 +
    '    UNIDADE_ENTRADA_ID,'+ #13 +
    '    VALOR_TOTAL_DESCONTO,'+ #13 +
    '    VALOR_TOTAL_OUTRAS_DESPESAS,    '+ #13 +
    '    ORIGEM_PRODUTO,'+ #13 +
    #13 +
    '    QUANTIDADE_EMBALAGEM,'+ #13 +
    '    MULTIPLO_COMPRA,'+ #13 +
    '    UNIDADE_COMPRA_ID,'+ #13 +
    #13 +
    '    QUANTIDADE_ENTRADA_ALTIS,'+ #13 +
    #13 +
    '    /* ICMS NORMAL */'+ #13 +
    '    INDICE_REDUCAO_BASE_ICMS,'+ #13 +
    '    BASE_CALCULO_ICMS,'+ #13 +
    '    PERCENTUAL_ICMS,'+ #13 +
    '    VALOR_ICMS,'+ #13 +
    '    /* ------------------------------------------------ */'+ #13 +
    #13 +
    '    /* ST */'+ #13 +
    '    INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
    '    BASE_CALCULO_ICMS_ST,'+ #13 +
    '    PERCENTUAL_ICMS_ST,'+ #13 +
    '    VALOR_ICMS_ST,'+ #13 +
    '    IVA,'+ #13 +
    '    /* ------------------------------------------------ */'+ #13 +
    #13 +
    '    /* PIS */'+ #13 +
    '    CST_PIS,'+ #13 +
    '    BASE_CALCULO_PIS,'+ #13 +
    '    PERCENTUAL_PIS,'+ #13 +
    '    VALOR_PIS,'+ #13 +
    '    /* ------------------------------------------------ */'+ #13 +
    #13 +
    '    /* COFINS */'+ #13 +
    '    CST_COFINS,'+ #13 +
    '    BASE_CALCULO_COFINS,'+ #13 +
    '    PERCENTUAL_COFINS,'+ #13 +
    '    VALOR_COFINS,'+ #13 +
    '    /* ----------------------------------------------- */'+ #13 +
    #13 +
    '    /* IPI */'+ #13 +
    '    VALOR_IPI,'+ #13 +
    '    PERCENTUAL_IPI,'+ #13 +
    '    /* ----------------------------------------------- */'+ #13 +
    #13 +
    '    PRECO_FINAL,'+ #13 +
    '    PRECO_LIQUIDO,'+ #13 +
    '    PRECO_PAUTA,'+ #13 +
    '    INFORMACOES_ADICIONAIS'+ #13 +
    '  )'+ #13 +
    '  select'+ #13 +
    '    vEntradaId,'+ #13 +
    '    ITE.PRODUTO_ID,'+ #13 +
    '    ITE.ITEM_ID,'+ #13 +
    '    ITE.CFOP_ID,'+ #13 +
    '    1,'+ #13 +
    '    ITE.CST,'+ #13 +
    '    ITE.CODIGO_NCM,'+ #13 +
    '    ITE.VALOR_TOTAL,'+ #13 +
    '    ITE.PRECO_UNITARIO,'+ #13 +
    '    ITE.QUANTIDADE,'+ #13 +
    '    PRO.UNIDADE_VENDA,'+ #13 +
    '    0,'+ #13 +
    '    0,'+ #13 +
    '    ITE.ORIGEM_PRODUTO,'+ #13 +
    #13 +
    '    1,'+ #13 +
    '    1,'+ #13 +
    '    PRO.UNIDADE_VENDA,'+ #13 +
    #13 +
    '    ITE.QUANTIDADE,'+ #13 +
    #13 +
    '    /* ICMS NORMAL */'+ #13 +
    '    ITE.INDICE_REDUCAO_BASE_ICMS,'+ #13 +
    '    ITE.BASE_CALCULO_ICMS,'+ #13 +
    '    ITE.PERCENTUAL_ICMS,'+ #13 +
    '    ITE.VALOR_ICMS,'+ #13 +
    '    /* ------------------------------------------------ */'+ #13 +
    #13 +
    '    /* ST */'+ #13 +
    '    ITE.INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
    '    ITE.BASE_CALCULO_ICMS_ST,'+ #13 +
    '    ITE.PERCENTUAL_ICMS_ST,'+ #13 +
    '    ITE.VALOR_ICMS_ST,'+ #13 +
    '    ITE.IVA,'+ #13 +
    '    /* ------------------------------------------------ */'+ #13 +
    #13 +
    '    /* PIS */'+ #13 +
    '    ITE.CST_PIS,'+ #13 +
    '    ITE.BASE_CALCULO_PIS,'+ #13 +
    '    ITE.PERCENTUAL_PIS,'+ #13 +
    '    ITE.VALOR_PIS,'+ #13 +
    '    /* ------------------------------------------------ */'+ #13 +
    #13 +
    '    /* COFINS */'+ #13 +
    '    ITE.CST_COFINS,'+ #13 +
    '    ITE.BASE_CALCULO_COFINS,'+ #13 +
    '    ITE.PERCENTUAL_COFINS,'+ #13 +
    '    ITE.VALOR_COFINS,'+ #13 +
    '    /* ----------------------------------------------- */'+ #13 +
    #13 +
    '    /* IPI */'+ #13 +
    '    ITE.VALOR_IPI,'+ #13 +
    '    ITE.PERCENTUAL_IPI,'+ #13 +
    '    /* ----------------------------------------------- */'+ #13 +
    #13 +
    '    0, -- PRECO_FINAL'+ #13 +
    '    0, -- PRECO_LIQUIDO'+ #13 +
    '    ITE.PRECO_PAUTA,'+ #13 +
    '    ITE.INFORMACOES_ADICIONAIS'+ #13 +
    '  from'+ #13 +
    '    NOTAS_FISCAIS_ITENS ITE'+ #13 +
    #13 +
    '  inner join PRODUTOS PRO'+ #13 +
    '  on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    '  where ITE.NOTA_FISCAL_ID = vNotaFiscalSaidaId;'+ #13 +
    #13 +
    'end GERAR_ENTR_TRANS_PRODUTOS_EMP;'+ #13
end;

function CriarTabelaCONTAS_REC_HISTORICO_COBRANCAS: string;
begin
  Result :=
    'create table CONTAS_REC_HISTORICO_COBRANCAS('+ #13 +
    '  RECEBER_ID            number(12,0) not null,'+ #13 +
    '  ITEM_ID               number(3) not null,'+ #13 +
    #13 +
    '  HISTORICO             varchar2(800) not null,'+ #13 +
    #13 +
    '  USUARIO_COBRANCA_ID   number(4) not null,'+ #13 +
    '  DATA_HORA_COBRANCA    date not null,'+ #13 +
    #13 +
    '  /* Colunas de logs do sistema */'+ #13 +
    '  USUARIO_SESSAO_ID       number(4) not null,'+ #13 +
    '  DATA_HORA_ALTERACAO     date not null,'+ #13 +
    '  ROTINA_ALTERACAO        varchar2(30) not null,'+ #13 +
    '  ESTACAO_ALTERACAO       varchar2(30) not null  '+ #13 +
    ');'+ #13 +
    #13 +
    '/* Chave prim�ria */'+ #13 +
    'alter table CONTAS_REC_HISTORICO_COBRANCAS'+ #13 +
    'add constraint CK_CON_REC_HISTORICO_COBRANCAS'+ #13 +
    'primary key(RECEBER_ID, ITEM_ID)'+ #13 +
    'using index tablespace INDICES;'+ #13 +
    #13 +
    '/* Chaves estrangeiras */'+ #13 +
    'alter table CONTAS_REC_HISTORICO_COBRANCAS'+ #13 +
    'add constraint FK_CON_REC_HIST_COBR_RECEB_ID'+ #13 +
    'foreign key(RECEBER_ID)'+ #13 +
    'references CONTAS_RECEBER(RECEBER_ID);'+ #13 +
    #13 +
    'alter table CONTAS_REC_HISTORICO_COBRANCAS'+ #13 +
    'add constraint FK_CON_REC_HIS_COBR_USU_COB_ID'+ #13 +
    'foreign key(USUARIO_COBRANCA_ID)'+ #13 +
    'references FUNCIONARIOS(FUNCIONARIO_ID);'+ #13
end;

function AjustarTriggerCONTAS_REC_HISTORICO_COB_IU_BR: string;
begin
  Result :=
    'create or replace trigger CONTAS_REC_HISTORICO_COB_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on CONTAS_REC_HISTORICO_COBRANCAS'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '  if inserting then'+ #13 +
    '    :new.USUARIO_COBRANCA_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '    :new.DATA_HORA_COBRANCA  := sysdate;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end CONTAS_REC_HISTORICO_COB_IU_BR;'+ #13
end;

function AjustarViewVW_VIDA_PRODUTO_DISPONIVEL: string;
begin
  Result :=
    'create or replace view VW_VIDA_PRODUTO_DISPONIVEL'+ #13 +
    'as'+ #13 +
    '/* Retiradas */'+ #13 +
    'select'+ #13 +
    '  ''RAT'' as TIPO_MOVIMENTO,'+ #13 +
    '  RET.RETIRADA_ID as MOVIMENTO_ID,'+ #13 +
    '  RET.DATA_HORA_CADASTRO as DATA_HORA_MOVIMENTO,'+ #13 +
    '  ORC.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  RIT.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  ORC.ORCAMENTO_ID as NUMERO_DOCUMENTO,'+ #13 +
    '  ORC.CLIENTE_ID as CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL as NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  RETIRADAS_ITENS RIT'+ #13 +
    #13 +
    'inner join RETIRADAS RET'+ #13 +
    'on RIT.RETIRADA_ID = RET.RETIRADA_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on RIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'where RET.TIPO_MOVIMENTO = ''A'''+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* A retirar */'+ #13 +
    'select'+ #13 +
    '  ''RET'' as TIPO_MOVIMENTO,'+ #13 +
    '  EIP.ORCAMENTO_ID as MOVIMENTO_ID,'+ #13 +
    '  PEN.DATA_HORA_CADASTRO as DATA_HORA_MOVIMENTO,'+ #13 +
    '  PEN.EMPRESA_ID as EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  EIP.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  EIP.ORCAMENTO_ID as NUMERO_DOCUMENTO,'+ #13 +
    '  ORC.CLIENTE_ID as CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL as NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  RETIRADAS_ITENS_PENDENTES EIP'+ #13 +
    #13 +
    'inner join RETIRADAS_PENDENTES PEN'+ #13 +
    'on EIP.ORCAMENTO_ID = PEN.ORCAMENTO_ID'+ #13 +
    'and EIP.LOCAL_ID = PEN.LOCAL_ID'+ #13 +
    'and EIP.EMPRESA_ID = PEN.EMPRESA_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIP.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on EIP.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Entregas pendentes */'+ #13 +
    'select'+ #13 +
    '  ''ENG'' as TIPO_MOVIMENTO,'+ #13 +
    '  EIP.ORCAMENTO_ID as MOVIMENTO_ID,'+ #13 +
    '  PEN.DATA_HORA_CADASTRO as DATA_HORA_MOVIMENTO,'+ #13 +
    '  PEN.EMPRESA_ID as EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  EIP.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  EIP.ORCAMENTO_ID as NUMERO_DOCUMENTO,'+ #13 +
    '  ORC.CLIENTE_ID as CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL as NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  ENTREGAS_ITENS_PENDENTES EIP'+ #13 +
    #13 +
    'inner join ENTREGAS_PENDENTES PEN'+ #13 +
    'on EIP.ORCAMENTO_ID = PEN.ORCAMENTO_ID'+ #13 +
    'and EIP.LOCAL_ID = PEN.LOCAL_ID'+ #13 +
    'and EIP.EMPRESA_ID = PEN.EMPRESA_ID'+ #13 +
    'and EIP.PREVISAO_ENTREGA = PEN.PREVISAO_ENTREGA'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIP.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on EIP.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Ajustes de estoque */'+ #13 +
    'select'+ #13 +
    '  ''AJU'' as TIPO_MOVIMENTO,'+ #13 +
    '  AJU.AJUSTE_ESTOQUE_ID,'+ #13 +
    '  AJU.DATA_HORA_AJUSTE,'+ #13 +
    '  AJU.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  AJI.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  AJI.NATUREZA,'+ #13 +
    '  AJU.AJUSTE_ESTOQUE_ID as NUMERO_DOCUMENTO,'+ #13 +
    '  AJU.USUARIO_AJUSTE_ID as CADASTRO_ID,'+ #13 +
    '  FUN.NOME as NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  AJUSTES_ESTOQUE_ITENS AJI'+ #13 +
    #13 +
    'inner join AJUSTES_ESTOQUE AJU'+ #13 +
    'on AJI.AJUSTE_ESTOQUE_ID = AJU.AJUSTE_ESTOQUE_ID'+ #13 +
    #13 +
    'inner join FUNCIONARIOS FUN'+ #13 +
    'on FUN.FUNCIONARIO_ID = AJU.USUARIO_AJUSTE_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on AJI.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Devolu��es */'+ #13 +
    'select'+ #13 +
    '  ''DEV'' as TIPO_MOVIMENTO,'+ #13 +
    '  DEV.DEVOLUCAO_ID,'+ #13 +
    '  DEV.DATA_HORA_DEVOLUCAO,'+ #13 +
    '  DEV.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  (DIT.DEVOLVIDOS_ENTREGUES + DIT.DEVOLVIDOS_PENDENCIAS) * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  DEV.DEVOLUCAO_ID AS NUMERO_DOCUMENTO,'+ #13 +
    '  DEV.CLIENTE_CREDITO_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  DEVOLUCOES_ITENS DIT'+ #13 +
    #13 +
    'inner join DEVOLUCOES DEV'+ #13 +
    'on DIT.DEVOLUCAO_ID = DEV.DEVOLUCAO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on DEV.CLIENTE_CREDITO_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on DIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where DIT.DEVOLVIDOS_ENTREGUES + DIT.DEVOLVIDOS_PENDENCIAS > 0'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Entradas de mercadorias */'+ #13 +
    'select'+ #13 +
    '  ''ENT'' as TIPO_MOVIMENTO,'+ #13 +
    '  ENT.ENTRADA_ID,'+ #13 +
    '  ENT.DATA_HORA_CADASTRO,'+ #13 +
    '  ENT.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  (EIT.QUANTIDADE * EIT.QUANTIDADE_EMBALAGEM) * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  ENT.NUMERO_NOTA as NUMERO_DOCUMENTO,'+ #13 +
    '  ENT.FORNECEDOR_ID as CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL as NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  ENTRADAS_NOTAS_FISCAIS_ITENS EIT'+ #13 +
    #13 +
    'inner join ENTRADAS_NOTAS_FISCAIS ENT'+ #13 +
    'on EIT.ENTRADA_ID = ENT.ENTRADA_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ENT.FORNECEDOR_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where ENT.STATUS in(''FCO'', ''BAI'', ''ECO'')'+ #13 +
    'and ENT.ORIGEM_ENTRADA not in(''TFP''); -- Removendo as transferencias fiscais'+ #13
end;

function AjustarViewVW_VIDA_PRODUTO_FISCAL: string;
begin
  Result :=
    'create or replace view VW_VIDA_PRODUTO_FISCAL'+ #13 +
    'as'+ #13 +
    '/* Notas fiscais emitidas */'+ #13 +
    'select'+ #13 +
    '  NFI.TIPO_MOVIMENTO,'+ #13 +
    '  NFI.NOTA_FISCAL_ID as MOVIMENTO_ID,'+ #13 +
    '  NFI.DATA_HORA_EMISSAO as DATA_HORA_MOVIMENTO,'+ #13 +
    '  NFI.EMPRESA_ID,'+ #13 +
    '  NIT.PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  NIT.QUANTIDADE,'+ #13 +
    '  case when substr(NIT.CFOP_ID, 1, 1) in(1,2,3) then ''E'' else ''S'' end as NATUREZA,'+ #13 +
    '  NFI.NUMERO_NOTA AS NUMERO_DOCUMENTO,'+ #13 +
    '  NFI.CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  NOTAS_FISCAIS_ITENS NIT'+ #13 +
    #13 +
    'inner join NOTAS_FISCAIS NFI'+ #13 +
    'on NIT.NOTA_FISCAL_ID = NFI.NOTA_FISCAL_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on NFI.CADASTRO_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on NIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where NFI.STATUS = ''E'''+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    'select'+ #13 +
    '  ''ENT'' as TIPO_MOVIMENTO,'+ #13 +
    '  ENT.ENTRADA_ID,'+ #13 +
    '  ENT.DATA_HORA_CADASTRO,'+ #13 +
    '  ENT.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  EIT.QUANTIDADE * EIT.QUANTIDADE_EMBALAGEM * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  ENT.NUMERO_NOTA AS NUMERO_DOCUMENTO,'+ #13 +
    '  ENT.FORNECEDOR_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  ENTRADAS_NOTAS_FISCAIS_ITENS EIT'+ #13 +
    #13 +
    'inner join ENTRADAS_NOTAS_FISCAIS ENT'+ #13 +
    'on EIT.ENTRADA_ID = ENT.ENTRADA_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ENT.FORNECEDOR_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where STATUS in(''ECO'', ''FCO'', ''BAI'');'+ #13
end;

function AjustarViewVW_VIDA_PRODUTO_FISICO: string;
begin
  Result :=
    'create or replace view VW_VIDA_PRODUTO_FISICO'+ #13 +
    'as'+ #13 +
    '/* Retiradas */'+ #13 +
    #13 +
    'select'+ #13 +
    '  case when RET.TIPO_MOVIMENTO = ''A'' then ''RAT'' else ''RET'' end as TIPO_MOVIMENTO,'+ #13 +
    '  RET.RETIRADA_ID as MOVIMENTO_ID,'+ #13 +
    '  RET.DATA_HORA_RETIRADA as DATA_HORA_MOVIMENTO,'+ #13 +
    '  RET.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  RIT.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  ORC.ORCAMENTO_ID AS NUMERO_DOCUMENTO,'+ #13 +
    '  ORC.CLIENTE_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  RETIRADAS_ITENS RIT'+ #13 +
    #13 +
    'inner join RETIRADAS RET'+ #13 +
    'on RIT.RETIRADA_ID = RET.RETIRADA_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on RIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where RET.CONFIRMADA = ''S'''+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Entregas */'+ #13 +
    'select'+ #13 +
    '  ''VEN'' TIPO_MOVIMENTO,'+ #13 +
    '  ENT.ENTREGA_ID as MOVIMENTO_ID,'+ #13 +
    '  ENT.DATA_HORA_CADASTRO as DATA_HORA_MOVIMENTO,'+ #13 +
    '  ENT.EMPRESA_ENTREGA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  EIT.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''S'' as NATUREZA,'+ #13 +
    '  ORC.ORCAMENTO_ID AS NUMERO_DOCUMENTO,'+ #13 +
    '  ORC.CLIENTE_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  ENTREGAS_ITENS EIT'+ #13 +
    #13 +
    'inner join ENTREGAS ENT'+ #13 +
    'on EIT.ENTREGA_ID = ENT.ENTREGA_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Retorno de entregas */'+ #13 +
    'select'+ #13 +
    '  ''REN'' TIPO_MOVIMENTO,'+ #13 +
    '  ENT.ENTREGA_ID as MOVIMENTO_ID,'+ #13 +
    '  ENT.DATA_HORA_CADASTRO as DATA_HORA_MOVIMENTO,'+ #13 +
    '  ENT.EMPRESA_ENTREGA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  EIT.RETORNADOS * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  ORC.ORCAMENTO_ID AS NUMERO_DOCUMENTO,'+ #13 +
    '  ORC.CLIENTE_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  ENTREGAS_ITENS EIT'+ #13 +
    #13 +
    'inner join ENTREGAS ENT'+ #13 +
    'on EIT.ENTREGA_ID = ENT.ENTREGA_ID'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where EIT.RETORNADOS > 0'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Ajuste de estoque */'+ #13 +
    'select'+ #13 +
    '  ''AJU'' as TIPO_MOVIMENTO,'+ #13 +
    '  AJU.AJUSTE_ESTOQUE_ID,'+ #13 +
    '  AJU.DATA_HORA_AJUSTE,'+ #13 +
    '  AJU.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  AJI.QUANTIDADE * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  AJI.NATUREZA,'+ #13 +
    '  AJU.AJUSTE_ESTOQUE_ID AS NUMERO_DOCUMENTO,'+ #13 +
    '  AJU.USUARIO_AJUSTE_ID AS CADASTRO_ID,'+ #13 +
    '  FUN.NOME AS NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  AJUSTES_ESTOQUE_ITENS AJI'+ #13 +
    #13 +
    'inner join AJUSTES_ESTOQUE AJU'+ #13 +
    'on AJI.AJUSTE_ESTOQUE_ID = AJU.AJUSTE_ESTOQUE_ID'+ #13 +
    #13 +
    'inner join FUNCIONARIOS FUN'+ #13 +
    'on FUN.FUNCIONARIO_ID = AJU.USUARIO_AJUSTE_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on AJI.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Devolu��es */'+ #13 +
    'select'+ #13 +
    '  ''DEV'' as TIPO_MOVIMENTO,'+ #13 +
    '  DEV.DEVOLUCAO_ID,'+ #13 +
    '  DEV.DATA_HORA_CONFIRMACAO,'+ #13 +
    '  DEV.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  DIT.DEVOLVIDOS_ENTREGUES * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  DEV.DEVOLUCAO_ID AS NUMERO_DOCUMENTO,'+ #13 +
    '  DEV.CLIENTE_CREDITO_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  DEVOLUCOES_ITENS DIT'+ #13 +
    #13 +
    'inner join DEVOLUCOES DEV'+ #13 +
    'on DIT.DEVOLUCAO_ID = DEV.DEVOLUCAO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on DEV.CLIENTE_CREDITO_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on DIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where DEV.STATUS = ''B'''+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    '/* Entradas de mercadorias */'+ #13 +
    'select'+ #13 +
    '  ''ENT'' as TIPO_MOVIMENTO,'+ #13 +
    '  ENT.ENTRADA_ID,'+ #13 +
    '  ENT.DATA_HORA_CADASTRO,'+ #13 +
    '  ENT.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_PAI_ID as PRODUTO_ID,'+ #13 +
    '  PRO.NOME,'+ #13 +
    '  EIT.QUANTIDADE_ENTRADA_ALTIS * PRO.QUANTIDADE_VEZES_PAI as QUANTIDADE,'+ #13 +
    '  ''E'' as NATUREZA,'+ #13 +
    '  ENT.NUMERO_NOTA AS NUMERO_DOCUMENTO,'+ #13 +
    '  ENT.FORNECEDOR_ID AS CADASTRO_ID,'+ #13 +
    '  CAD.RAZAO_SOCIAL AS NOME_CADASTRO'+ #13 +
    'from'+ #13 +
    '  ENTRADAS_NOTAS_FISCAIS_ITENS EIT'+ #13 +
    #13 +
    'inner join ENTRADAS_NOTAS_FISCAIS ENT'+ #13 +
    'on EIT.ENTRADA_ID = ENT.ENTRADA_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ENT.FORNECEDOR_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on EIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'where ENT.STATUS in(''FCO'', ''ECO'', ''BAI'')'+ #13 +
    'and ENT.ORIGEM_ENTRADA not in(''TFP''); -- Removendo as transferencias fiscais'+ #13
end;

function AjustarProcGERAR_NOTA_TRANSF_PROD_EMPR: string;
  function GetComando1: string;
  begin
    Result :=
      'create or replace procedure GERAR_NOTA_TRANSF_PROD_EMPR(iTRANSFERENCIA_ID in number, oNOTA_FISCAL_ID out number)'+ #13 +
      'is  '+ #13 +
      #13 +
      '  i                       number default 0;'+ #13 +
      '  vQtde                   number default 0;'+ #13 +
      '  vValorMaisSignificativo number default 0;'+ #13 +
      #13 +
      '  vNotaFiscalId           NOTAS_FISCAIS.NOTA_FISCAL_ID%type;      '+ #13 +
      '  vSerieNota              NOTAS_FISCAIS.SERIE_NOTA%type;   '+ #13 +
      #13 +
      '  vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;'+ #13 +
      '  vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;'+ #13 +
      '  vDadosNota              RecordsNotasFiscais.RecNotaFiscal;'+ #13 +
      '  vDadosItens             RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;  '+ #13 +
      #13 +
      '  vRegimeTributario       PARAMETROS_EMPRESA.REGIME_TRIBUTARIO%type;'+ #13 +
      '  vSerieNFe               PARAMETROS_EMPRESA.SERIE_NFE%type;'+ #13 +
      '  vTipoEmpresaDestino     EMPRESAS.TIPO_EMPRESA%type;'+ #13 +
      #13 +
      '  cursor cItens is'+ #13 +
      '  select'+ #13 +
      '    TIT.PRODUTO_ID,'+ #13 +
      '    TIT.ITEM_ID,'+ #13 +
      '    TIT.PRECO_UNITARIO,'+ #13 +
      '    TIT.QUANTIDADE,'+ #13 +
      '    TIT.VALOR_TOTAL,'+ #13 +
      '    PRO.NOME as NOME_PRODUTO,'+ #13 +
      '    PRO.CODIGO_NCM,'+ #13 +
      '    PRO.UNIDADE_VENDA,'+ #13 +
      '    PRO.CODIGO_BARRAS,'+ #13 +
      '    PRO.CEST'+ #13 +
      '  from'+ #13 +
      '    /* Este select � feito desta forma por causa dos LOTES! */'+ #13 +
      '    ('+ #13 +
      '      select'+ #13 +
      '        TRANSFERENCIA_ID,'+ #13 +
      '        PRODUTO_ID,'+ #13 +
      '        ITEM_ID,'+ #13 +
      '        PRECO_UNITARIO,'+ #13 +
      '        TIPO_CONTROLE_ESTOQUE,'+ #13 +
      '        sum(VALOR_TOTAL)as VALOR_TOTAL,'+ #13 +
      '        sum(QUANTIDADE) as QUANTIDADE'+ #13 +
      '      from'+ #13 +
      '        TRANSF_PRODUTOS_EMPRESAS_ITENS'+ #13 +
      '      where TRANSFERENCIA_ID = iTRANSFERENCIA_ID'+ #13 +
      '      group by'+ #13 +
      '        TRANSFERENCIA_ID,'+ #13 +
      '        PRODUTO_ID,'+ #13 +
      '        ITEM_ID,'+ #13 +
      '        PRECO_UNITARIO,'+ #13 +
      '        TIPO_CONTROLE_ESTOQUE'+ #13 +
      '    ) TIT'+ #13 +
      #13 +
      '  inner join TRANSF_PRODUTOS_EMPRESAS TPE'+ #13 +
      '  on TIT.TRANSFERENCIA_ID = TPE.TRANSFERENCIA_ID'+ #13 +
      #13 +
      '  inner join PRODUTOS PRO'+ #13 +
      '  on TIT.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
      #13 +
      '  where TIT.TRANSFERENCIA_ID = iTRANSFERENCIA_ID'+ #13 +
      '  /* N�o trazer os produtos KITS */'+ #13 +
      '  and TIT.TIPO_CONTROLE_ESTOQUE not in(''K'', ''A'')'+ #13 +
      '  order by'+ #13 +
      '    TIT.ITEM_ID;'+ #13 +
      #13 +
      'begin'+ #13 +
      '  select'+ #13 +
      '    count(*)'+ #13 +
      '  into'+ #13 +
      '    vQtde'+ #13 +
      '  from'+ #13 +
      '    NOTAS_FISCAIS'+ #13 +
      '  where TRANSFERENCIA_PRODUTOS_ID = iTRANSFERENCIA_ID;'+ #13 +
      #13 +
      '  if vQtde > 0 then'+ #13 +
      '    ERRO(''A transfer�ncia de produtos entre empresas j� gerou nota!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  begin'+ #13 +
      '    select'+ #13 +
      '      TPE.EMPRESA_ORIGEM_ID,'+ #13 +
      '      CAD.CADASTRO_ID,'+ #13 +
      #13 +
      '      /* Emitente */'+ #13 +
      '      EMP.RAZAO_SOCIAL,'+ #13 +
      '      EMP.NOME_FANTASIA,      '+ #13 +
      '      EMP.CNPJ,'+ #13 +
      '      EMP.INSCRICAO_ESTADUAL,'+ #13 +
      '      EMP.LOGRADOURO,'+ #13 +
      '      EMP.COMPLEMENTO,'+ #13 +
      '      BAE.NOME as NOME_BAIRRO_EMITENTE,'+ #13 +
      '      CIE.NOME as NOME_CIDADE_EMITENTE,'+ #13 +
      '      EMP.NUMERO,'+ #13 +
      '      ESE.ESTADO_ID,'+ #13 +
      '      EMP.CEP,'+ #13 +
      '      EMP.TELEFONE_PRINCIPAL,'+ #13 +
      '      CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      #13 +
      '      /* Destinatario */'+ #13 +
      '      CAD.NOME_FANTASIA,'+ #13 +
      '      CAD.RAZAO_SOCIAL,'+ #13 +
      '      CAD.TIPO_PESSOA,'+ #13 +
      '      CAD.CPF_CNPJ,'+ #13 +
      '      CAD.INSCRICAO_ESTADUAL,'+ #13 +
      '      CAD.LOGRADOURO,'+ #13 +
      '      CAD.COMPLEMENTO,'+ #13 +
      '      CAD.NUMERO,'+ #13 +
      '      CAD.CEP,      '+ #13 +
      '      BAI.NOME as NOME_BAIRRO,'+ #13 +
      '      CID.NOME as NOME_CIDADE,'+ #13 +
      '      CID.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      CID.ESTADO_ID as ESTADO_ID_DESTINATARIO,'+ #13 +
      #13 +
      '      PAE.REGIME_TRIBUTARIO,'+ #13 +
      '      PAE.SERIE_NFE,'+ #13 +
      '      EMD.TIPO_EMPRESA'+ #13 +
      '    into      '+ #13 +
      '      vDadosNota.EMPRESA_ID,'+ #13 +
      '      vDadosNota.CADASTRO_ID,      '+ #13 +
      #13 +
      '      /* Emitente */'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_FANTASIA_EMITENTE,      '+ #13 +
      '      vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '      vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '      vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '      vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '      vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '      vDadosNota.CEP_EMITENTE,'+ #13 +
      '      vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      #13 +
      '      /* Destinatario */'+ #13 +
      '      vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '      vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '      vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '      vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '      vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '      vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      #13 +
      '      /* Par�metros da empresa emitente */'+ #13 +
      '      vRegimeTributario,'+ #13 +
      '      vSerieNFe,'+ #13 +
      '      vTipoEmpresaDestino'+ #13 +
      '    from'+ #13 +
      '      TRANSF_PRODUTOS_EMPRESAS TPE'+ #13 +
      #13 +
      '    inner join EMPRESAS EMP'+ #13 +
      '    on TPE.EMPRESA_ORIGEM_ID = EMP.EMPRESA_ID'+ #13 +
      #13 +
      '    inner join EMPRESAS EMD'+ #13 +
      '    on TPE.EMPRESA_DESTINO_ID = EMD.EMPRESA_ID'+ #13 +
      #13 +
      '    inner join CADASTROS CAD'+ #13 +
      '    on EMD.CADASTRO_ID = CAD.CADASTRO_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAI'+ #13 +
      '    on CAD.BAIRRO_ID = BAI.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CID'+ #13 +
      '    on BAI.CIDADE_ID = CID.CIDADE_ID '+ #13 +
      #13 +
      '    inner join BAIRROS BAE'+ #13 +
      '    on EMP.BAIRRO_ID = BAE.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CIE'+ #13 +
      '    on BAE.CIDADE_ID = CIE.CIDADE_ID'+ #13 +
      #13 +
      '    inner join ESTADOS ESE'+ #13 +
      '    on CIE.ESTADO_ID = ESE.ESTADO_ID'+ #13 +
      #13 +
      '    inner join PARAMETROS_EMPRESA PAE'+ #13 +
      '    on PAE.EMPRESA_ID = TPE.EMPRESA_ORIGEM_ID'+ #13 +
      #13 +
      '    cross join PARAMETROS PAR'+ #13 +
      #13 +
      '    where TPE.TRANSFERENCIA_ID = iTRANSFERENCIA_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Houve um erro ao buscar os dados do or�amento para gera��o dos dados de NF! '' + sqlerrm);'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  vDadosNota.BASE_CALCULO_ICMS    := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS           := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_ICMS_ST := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS_ST        := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_PIS     := 0;'+ #13 +
      '  vDadosNota.VALOR_PIS            := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_COFINS  := 0;'+ #13 +
      '  vDadosNota.VALOR_COFINS         := 0;'+ #13 +
      '  vDadosNota.VALOR_IPI            := 0;'+ #13 +
      '  vDadosNota.VALOR_FRETE          := 0;'+ #13 +
      '  vDadosNota.VALOR_SEGURO         := 0;'+ #13 +
      '  vDadosNota.PESO_LIQUIDO         := 0;'+ #13 +
      '  vDadosNota.PESO_BRUTO           := 0;    '+ #13 +
      #13 +
      '  for vItens in cItens loop'+ #13 +
      '    if length(vItens.CODIGO_NCM) < 8 then'+ #13 +
      '      Erro(''O c�digo NCM do produto '' || vItens.PRODUTO_ID || '' - '' || vItens.NOME_PRODUTO || '' est� incorreto, o NCM � composto de 8 dig�tos, verifique no cadastro de produtos!'');'+ #13 +
      '    end if;  '+ #13 +
      #13 +
      '    vDadosItens(i).PRODUTO_ID    := vItens.PRODUTO_ID;'+ #13 +
      '    vDadosItens(i).ITEM_ID       := vItens.ITEM_ID;'+ #13 +
      '    vDadosItens(i).NOME_PRODUTO  := vItens.NOME_PRODUTO;'+ #13 +
      '    vDadosItens(i).UNIDADE       := vItens.UNIDADE_VENDA;'+ #13 +
      '    vDadosItens(i).CODIGO_NCM    := vItens.CODIGO_NCM;'+ #13 +
      '    vDadosItens(i).QUANTIDADE    := vItens.QUANTIDADE;'+ #13 +
      '    vDadosItens(i).CODIGO_BARRAS := vItens.CODIGO_BARRAS;'+ #13 +
      '    vDadosItens(i).CEST          := vItens.CEST;'+ #13 +
      #13 +
      '    vDadosItens(i).PRECO_UNITARIO              := vItens.PRECO_UNITARIO;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL                 := round(vDadosItens(i).PRECO_UNITARIO * vItens.QUANTIDADE, 2);'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_DESCONTO        := 0;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := 0;'+ #13 +
      #13 +
      '    /* Buscando os dados de impostos item a item da venda */'+ #13 +
      '    BUSCAR_CALC_IMPOSTOS_PRODUTO('+ #13 +
      '      vItens.PRODUTO_ID,'+ #13 +
      '      vDadosNota.EMPRESA_ID,'+ #13 +
      '      vDadosNota.CADASTRO_ID,'+ #13 +
      #13 +
      '      vDadosItens(i).VALOR_TOTAL,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO,'+ #13 +
      #13 +
      '      vDadosItens(i).CST,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS,'+ #13 +
      #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS_ST,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PRECO_PAUTA,'+ #13 +
      '      vDadosItens(i).IVA,'+ #13 +
      #13 +
      '      vDadosItens(i).VALOR_IPI,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_IPI,'+ #13 +
      #13 +
      '      vDadosItens(i).CST_PIS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS,'+ #13 +
      '      vDadosItens(i).VALOR_PIS,'+ #13 +
      #13 +
      '      vDadosItens(i).CST_COFINS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS,'+ #13 +
      '      vDadosItens(i).VALOR_COFINS'+ #13 +
      '		);'+ #13 +
      #13 +
      '    /* Se for para deposito fechado, zerando os impostos */'+ #13 +
      '    if vTipoEmpresaDestino = ''D'' then'+ #13 +
      '      vDadosItens(i).CST                  := ''41'';'+ #13 +
      '      vDadosItens(i).CST_PIS              := ''08'';'+ #13 +
      '      vDadosItens(i).CST_COFINS           := ''08'';'+ #13 +
      #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS    := 0;'+ #13 +
      '      vDadosItens(i).VALOR_ICMS           := 0;'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST := 0;'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST        := 0;'+ #13 +
      #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS     := 0;'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS       := 0;'+ #13 +
      '      vDadosItens(i).VALOR_PIS            := 0;'+ #13 +
      #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS  := 0;'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS    := 0;'+ #13 +
      '      vDadosItens(i).VALOR_COFINS         := 0;'+ #13 +
      #13 +
      '      vDadosItens(i).CFOP_ID := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vDadosItens(i).CST, ''RDF'', ''I'');'+ #13 +
      '    else'+ #13 +
      '      vDadosItens(i).CFOP_ID := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vDadosItens(i).CST, ''TPE'', ''I'');'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '		vDadosItens(i).NATUREZA_OPERACAO := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);'+ #13 +
      #13 +
      '    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS    := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;'+ #13 +
      '    vDadosNota.VALOR_ICMS           := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST        := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;'+ #13 +
      '    vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;'+ #13 +
      '    vDadosNota.BASE_CALCULO_COFINS  := vDadosNota.BASE_CALCULO_COFINS + vDadosItens(i).BASE_CALCULO_COFINS;'+ #13 +
      '    vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;'+ #13 +
      '    vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL :='+ #13 +
      '      vDadosNota.VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS  := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;'+ #13 +
      '    vDadosNota.VALOR_DESCONTO        := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;'+ #13 +
      '    /* ------------------------------------------------------------------------------------------------------ */'+ #13 +
      #13 +
      '		if'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO >'+ #13 +
      '      vValorMaisSignificativo'+ #13 +
      '    then'+ #13 +
      '			vValorMaisSignificativo :='+ #13 +
      '        vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '        vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '        vDadosItens(i).VALOR_IPI -'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '			vCfopIdCapa 	  	:= vDadosItens(i).CFOP_ID;'+ #13 +
      '			vNaturezaOperacao := vDadosItens(i).NATUREZA_OPERACAO;'+ #13 +
      '		end if;'+ #13 +
      #13 +
      '    i := i + 1;'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  if vDadosItens.count < 1 then'+ #13 +
      '    ERRO(''Os produtos da nota fiscal n�o foram encontrados!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '	if vCfopIdCapa is null then '+ #13 +
      '		vCfopIdCapa 			:= vDadosItens(0).CFOP_ID;'+ #13 +
      '		vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;'+ #13 +
      '	end if;'+ #13 +
      #13 +
      '	vDadosNota.CFOP_ID := vCfopIdCapa;'+ #13 +
      '  vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;'+ #13 +
      #13 +
      '  vDadosNota.VALOR_RECEBIDO_COBRANCA := vDadosNota.VALOR_TOTAL;'+ #13 +
      #13 +
      '  vSerieNota  := to_char(nvl(vSerieNFe, 1));  '+ #13 +
      #13 +
      '  if vTipoEmpresaDestino = ''D'' then'+ #13 +
      '    vDadosNota.INFORMACOES_COMPLEMENTARES := ''N�o incidencia do ICMS, nos termos do Art. 79, inciso I, "j" e Art. 19 do Anexo XII do RCTE-GO'';'+ #13 +
      '  else'+ #13 +
      '    vDadosNota.INFORMACOES_COMPLEMENTARES := ''Transf. produtos entre empresas.'';'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;'+ #13 +
      #13 +
      '	insert into NOTAS_FISCAIS('+ #13 +
      '    NOTA_FISCAL_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    CFOP_ID,'+ #13 +
      '    EMPRESA_ID,    '+ #13 +
      '    TRANSFERENCIA_PRODUTOS_ID,'+ #13 +
      '		MODELO_NOTA,'+ #13 +
      '		SERIE_NOTA,'+ #13 +
      '    NATUREZA_OPERACAO,'+ #13 +
      '		STATUS,'+ #13 +
      '    TIPO_MOVIMENTO,'+ #13 +
      '    RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    NOME_FANTASIA_EMITENTE,'+ #13 +
      '    REGIME_TRIBUTARIO,'+ #13 +
      '    CNPJ_EMITENTE,'+ #13 +
      '    INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    LOGRADOURO_EMITENTE,'+ #13 +
      '    COMPLEMENTO_EMITENTE,'+ #13 +
      '    NOME_BAIRRO_EMITENTE,'+ #13 +
      '    NOME_CIDADE_EMITENTE,'+ #13 +
      '    NUMERO_EMITENTE,'+ #13 +
      '    ESTADO_ID_EMITENTE,'+ #13 +
      '    CEP_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    TIPO_NOTA,'+ #13 +
      '    LOGRADOURO_DESTINATARIO,'+ #13 +
      '    COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    ESTADO_ID_DESTINATARIO,'+ #13 +
      '    CEP_DESTINATARIO,'+ #13 +
      '    NUMERO_DESTINATARIO,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    VALOR_TOTAL,'+ #13 +
      '    VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    VALOR_DESCONTO,'+ #13 +
      '    VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    BASE_CALCULO_ICMS,'+ #13 +
      '    VALOR_ICMS,'+ #13 +
      '    BASE_CALCULO_ICMS_ST,'+ #13 +
      '    VALOR_ICMS_ST,'+ #13 +
      '    BASE_CALCULO_PIS,'+ #13 +
      '    VALOR_PIS,'+ #13 +
      '    BASE_CALCULO_COFINS,'+ #13 +
      '    VALOR_COFINS,'+ #13 +
      '    VALOR_IPI,'+ #13 +
      '    VALOR_FRETE,'+ #13 +
      '    VALOR_SEGURO,    '+ #13 +
      '    VALOR_RECEBIDO_COBRANCA,    '+ #13 +
      '    PESO_LIQUIDO,'+ #13 +
      '    PESO_BRUTO,'+ #13 +
      '		INFORMACOES_COMPLEMENTARES'+ #13 +
      '  )values('+ #13 +
      '		vNotaFiscalId,'+ #13 +
      '    vDadosNota.CADASTRO_ID,'+ #13 +
      '    vDadosNota.CFOP_ID,'+ #13 +
      '    vDadosNota.EMPRESA_ID,    '+ #13 +
      '    iTRANSFERENCIA_ID,'+ #13 +
      '		''55'','+ #13 +
      '		vSerieNota,'+ #13 +
      '    vDadosNota.NATUREZA_OPERACAO,'+ #13 +
      '		''N'','+ #13 +
      '    ''TPE'','+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_EMITENTE,'+ #13 +
      '    vRegimeTributario,'+ #13 +
      '    vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '    vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '    vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '    vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '    vDadosNota.CEP_EMITENTE,'+ #13 +
      '    vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    vDadosNota.NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    vDadosNota.TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    ''N'','+ #13 +
      '    vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '    vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      '    vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '    vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    vDadosNota.VALOR_TOTAL,'+ #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    vDadosNota.VALOR_DESCONTO,'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS,'+ #13 +
      '    vDadosNota.VALOR_ICMS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST,'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST,'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS,'+ #13 +
      '    vDadosNota.VALOR_PIS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_COFINS,'+ #13 +
      '    vDadosNota.VALOR_COFINS,'+ #13 +
      '    vDadosNota.VALOR_IPI,'+ #13 +
      '    vDadosNota.VALOR_FRETE,'+ #13 +
      '    vDadosNota.VALOR_SEGURO,    '+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_COBRANCA,    '+ #13 +
      '    vDadosNota.PESO_LIQUIDO,'+ #13 +
      '    vDadosNota.PESO_BRUTO,'+ #13 +
      '		vDadosNota.INFORMACOES_COMPLEMENTARES'+ #13 +
      '  );'+ #13 +
      #13 +
      '  for i in 0..vDadosItens.count - 1 loop'+ #13 +
      '    insert into NOTAS_FISCAIS_ITENS('+ #13 +
      '      NOTA_FISCAL_ID,'+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      NOME_PRODUTO,'+ #13 +
      '      UNIDADE,'+ #13 +
      '      CFOP_ID,'+ #13 +
      '      CST,'+ #13 +
      '      CODIGO_NCM,'+ #13 +
      '      VALOR_TOTAL,'+ #13 +
      '      PRECO_UNITARIO,'+ #13 +
      '      QUANTIDADE,'+ #13 +
      '      VALOR_TOTAL_DESCONTO,'+ #13 +
      '      VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      BASE_CALCULO_ICMS,'+ #13;
  end;

  function GetComando2: string;
  begin
    Result :=
      '      PERCENTUAL_ICMS,'+ #13 +
      '      VALOR_ICMS,'+ #13 +
      '      BASE_CALCULO_ICMS_ST,'+ #13 +
      '      VALOR_ICMS_ST,'+ #13 +
      '      CST_PIS,'+ #13 +
      '      BASE_CALCULO_PIS,'+ #13 +
      '      PERCENTUAL_PIS,'+ #13 +
      '      VALOR_PIS,'+ #13 +
      '      CST_COFINS,'+ #13 +
      '      BASE_CALCULO_COFINS,'+ #13 +
      '      PERCENTUAL_COFINS,'+ #13 +
      '      VALOR_COFINS,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      IVA,'+ #13 +
      '      PRECO_PAUTA,'+ #13 +
      '      CODIGO_BARRAS,'+ #13 +
      '      VALOR_IPI,'+ #13 +
      '      PERCENTUAL_IPI,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      PERCENTUAL_ICMS_ST,'+ #13 +
      '      CEST'+ #13 +
      '    )values('+ #13 +
      '      vNotaFiscalId,'+ #13 +
      '      vDadosItens(i).PRODUTO_ID,'+ #13 +
      '      vDadosItens(i).ITEM_ID,'+ #13 +
      '      vDadosItens(i).NOME_PRODUTO,'+ #13 +
      '      vDadosItens(i).UNIDADE,'+ #13 +
      '      vDadosItens(i).CFOP_ID,'+ #13 +
      '      vDadosItens(i).CST,'+ #13 +
      '      vDadosItens(i).CODIGO_NCM,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL,'+ #13 +
      '      vDadosItens(i).PRECO_UNITARIO,'+ #13 +
      '      vDadosItens(i).QUANTIDADE,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CST_PIS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS,'+ #13 +
      '      vDadosItens(i).VALOR_PIS,'+ #13 +
      '      vDadosItens(i).CST_COFINS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS,'+ #13 +
      '      vDadosItens(i).VALOR_COFINS,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      vDadosItens(i).IVA,'+ #13 +
      '      vDadosItens(i).PRECO_PAUTA,'+ #13 +
      '      vDadosItens(i).CODIGO_BARRAS,'+ #13 +
      '      vDadosItens(i).VALOR_IPI,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_IPI,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CEST'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  oNOTA_FISCAL_ID := vNotaFiscalId;'+ #13 +
      #13 +
      'end GERAR_NOTA_TRANSF_PROD_EMPR;'+ #13;
  end;

begin
  Result :=
    GetComando1 + GetComando2;
end;

function AjustarProcGERAR_NOTA_TRANSF_MOVIMENTOS: string;
  function GetComando1: string;
  begin
    Result :=
      'create or replace procedure GERAR_NOTA_TRANSF_MOVIMENTOS('+ #13 +
      '  iEMPRESA_ORIGEM_ID in number,'+ #13 +
      '  iEMPRESA_DESTINO_ID in number,'+ #13 +
      '  iDATA_INICIAL in date,'+ #13 +
      '  iDATA_FINAL in date,'+ #13 +
      '  oNOTA_FISCAL_ID out number'+ #13 +
      ')'+ #13 +
      'is'+ #13 +
      '  i                       number default 0;'+ #13 +
      '  vQtde                   number default 0;'+ #13 +
      '  vValorMaisSignificativo number default 0;'+ #13 +
      #13 +
      '  vNotaFiscalId           NOTAS_FISCAIS.NOTA_FISCAL_ID%type;'+ #13 +
      '  vSerieNota              NOTAS_FISCAIS.SERIE_NOTA%type;'+ #13 +
      #13 +
      '  vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;'+ #13 +
      '  vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;'+ #13 +
      '  vDadosNota              RecordsNotasFiscais.RecNotaFiscal;'+ #13 +
      '  vDadosItens             RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;'+ #13 +
      #13 +
      '  vRegimeTributario       PARAMETROS_EMPRESA.REGIME_TRIBUTARIO%type;'+ #13 +
      '  vSerieNFe               PARAMETROS_EMPRESA.SERIE_NFE%type;'+ #13 +
      '  vTipoEmpresaOrigem      EMPRESAS.TIPO_EMPRESA%type;'+ #13 +
      #13 +
      '  cursor cItens is'+ #13 +
      '  select'+ #13 +
      '    PRODUTO_ID,'+ #13 +
      '    ITEM_ID,'+ #13 +
      '    QUANTIDADE,'+ #13 +
      '    NOME_PRODUTO,'+ #13 +
      '    PRECO_UNITARIO,'+ #13 +
      '    CODIGO_NCM,'+ #13 +
      '    UNIDADE_VENDA,'+ #13 +
      '    CODIGO_BARRAS,'+ #13 +
      '    CEST'+ #13 +
      '  from'+ #13 +
      '    VW_MOV_ITE_PEND_EMISSAO_NF_TR'+ #13 +
      '  where EMPRESA_ORIGEM_ID = iEMPRESA_ORIGEM_ID'+ #13 +
      '  and EMPRESA_DESTINO_ID = iEMPRESA_DESTINO_ID'+ #13 +
      '  and DATA_MOVIMENTO between iDATA_INICIAL and iDATA_FINAL;'+ #13 +
      #13 +
      'begin'+ #13 +
      '  vDadosNota.EMPRESA_ID := iEMPRESA_ORIGEM_ID;'+ #13 +
      #13 +
      '  begin'+ #13 +
      '    select'+ #13 +
      '      CAD.CADASTRO_ID,'+ #13 +
      #13 +
      '      /* Emitente */'+ #13 +
      '      EMP.RAZAO_SOCIAL,'+ #13 +
      '      EMP.NOME_FANTASIA,'+ #13 +
      '      EMP.CNPJ,'+ #13 +
      '      EMP.INSCRICAO_ESTADUAL,'+ #13 +
      '      EMP.LOGRADOURO,'+ #13 +
      '      EMP.COMPLEMENTO,'+ #13 +
      '      BAE.NOME as NOME_BAIRRO_EMITENTE,'+ #13 +
      '      CIE.NOME as NOME_CIDADE_EMITENTE,'+ #13 +
      '      EMP.NUMERO,'+ #13 +
      '      ESE.ESTADO_ID,'+ #13 +
      '      EMP.CEP,'+ #13 +
      '      EMP.TELEFONE_PRINCIPAL,'+ #13 +
      '      CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      #13 +
      '      /* Destinatario */'+ #13 +
      '      CAD.NOME_FANTASIA,'+ #13 +
      '      CAD.RAZAO_SOCIAL,'+ #13 +
      '      CAD.TIPO_PESSOA,'+ #13 +
      '      CAD.CPF_CNPJ,'+ #13 +
      '      CAD.INSCRICAO_ESTADUAL,'+ #13 +
      '      CAD.LOGRADOURO,'+ #13 +
      '      CAD.COMPLEMENTO,'+ #13 +
      '      CAD.NUMERO,'+ #13 +
      '      CAD.CEP,'+ #13 +
      '      BAI.NOME as NOME_BAIRRO,'+ #13 +
      '      CID.NOME as NOME_CIDADE,'+ #13 +
      '      CID.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      CID.ESTADO_ID as ESTADO_ID_DESTINATARIO,'+ #13 +
      #13 +
      '      PAE.REGIME_TRIBUTARIO,'+ #13 +
      '      PAE.SERIE_NFE,'+ #13 +
      '      EMP.TIPO_EMPRESA'+ #13 +
      '    into'+ #13 +
      '      vDadosNota.CADASTRO_ID,'+ #13 +
      #13 +
      '      /* Emitente */'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_FANTASIA_EMITENTE,'+ #13 +
      '      vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '      vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '      vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '      vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '      vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '      vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '      vDadosNota.CEP_EMITENTE,'+ #13 +
      '      vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      #13 +
      '      /* Destinatario */'+ #13 +
      '      vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '      vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '      vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '      vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '      vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '      vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '      vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '      vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '      vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '      vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '      vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      #13 +
      '      /* Par�metros da empresa emitente */'+ #13 +
      '      vRegimeTributario,'+ #13 +
      '      vSerieNFe,'+ #13 +
      '      vTipoEmpresaOrigem'+ #13 +
      '    from'+ #13 +
      '      EMPRESAS EMP'+ #13 +
      #13 +
      '    inner join EMPRESAS EMD'+ #13 +
      '    on EMD.EMPRESA_ID = iEMPRESA_DESTINO_ID'+ #13 +
      #13 +
      '    inner join CADASTROS CAD'+ #13 +
      '    on EMD.CADASTRO_ID = CAD.CADASTRO_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAI'+ #13 +
      '    on CAD.BAIRRO_ID = BAI.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CID'+ #13 +
      '    on BAI.CIDADE_ID = CID.CIDADE_ID'+ #13 +
      #13 +
      '    inner join BAIRROS BAE'+ #13 +
      '    on EMP.BAIRRO_ID = BAE.BAIRRO_ID'+ #13 +
      #13 +
      '    inner join CIDADES CIE'+ #13 +
      '    on BAE.CIDADE_ID = CIE.CIDADE_ID'+ #13 +
      #13 +
      '    inner join ESTADOS ESE'+ #13 +
      '    on CIE.ESTADO_ID = ESE.ESTADO_ID'+ #13 +
      #13 +
      '    inner join PARAMETROS_EMPRESA PAE'+ #13 +
      '    on PAE.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
      #13 +
      '    cross join PARAMETROS PAR'+ #13 +
      #13 +
      '    where EMP.EMPRESA_ID = iEMPRESA_ORIGEM_ID;'+ #13 +
      '  exception'+ #13 +
      '    when others then'+ #13 +
      '      ERRO(''Houve um problema ao buscar os dados das empresas para gera��o da NFe! '' + sqlerrm);'+ #13 +
      '  end;'+ #13 +
      #13 +
      '  vDadosNota.BASE_CALCULO_ICMS    := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS           := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_ICMS_ST := 0;'+ #13 +
      '  vDadosNota.VALOR_ICMS_ST        := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_PIS     := 0;'+ #13 +
      '  vDadosNota.VALOR_PIS            := 0;'+ #13 +
      '  vDadosNota.BASE_CALCULO_COFINS  := 0;'+ #13 +
      '  vDadosNota.VALOR_COFINS         := 0;'+ #13 +
      '  vDadosNota.VALOR_IPI            := 0;'+ #13 +
      '  vDadosNota.VALOR_FRETE          := 0;'+ #13 +
      '  vDadosNota.VALOR_SEGURO         := 0;'+ #13 +
      '  vDadosNota.PESO_LIQUIDO         := 0;'+ #13 +
      '  vDadosNota.PESO_BRUTO           := 0;'+ #13 +
      #13 +
      '  for vItens in cItens loop'+ #13 +
      '    if length(vItens.CODIGO_NCM) < 8 then'+ #13 +
      '      Erro(''O c�digo NCM do produto '' || vItens.PRODUTO_ID || '' - '' || vItens.NOME_PRODUTO || '' est� incorreto, o NCM � composto de 8 dig�tos, verifique no cadastro de produtos!'');'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '    vDadosItens(i).PRODUTO_ID    := vItens.PRODUTO_ID;'+ #13 +
      '    vDadosItens(i).ITEM_ID       := vItens.ITEM_ID;'+ #13 +
      '    vDadosItens(i).NOME_PRODUTO  := vItens.NOME_PRODUTO;'+ #13 +
      '    vDadosItens(i).UNIDADE       := vItens.UNIDADE_VENDA;'+ #13 +
      '    vDadosItens(i).CODIGO_NCM    := vItens.CODIGO_NCM;'+ #13 +
      '    vDadosItens(i).QUANTIDADE    := vItens.QUANTIDADE;'+ #13 +
      '    vDadosItens(i).CODIGO_BARRAS := vItens.CODIGO_BARRAS;'+ #13 +
      '    vDadosItens(i).CEST          := vItens.CEST;'+ #13 +
      #13 +
      '    vDadosItens(i).PRECO_UNITARIO              := vItens.PRECO_UNITARIO;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL                 := round(vDadosItens(i).PRECO_UNITARIO * vItens.QUANTIDADE, 2);'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_DESCONTO        := 0;'+ #13 +
      '    vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := 0;'+ #13 +
      #13 +
      '    /* Buscando os dados de impostos item a item da venda */'+ #13 +
      '    BUSCAR_CALC_IMPOSTOS_PRODUTO('+ #13 +
      '      vItens.PRODUTO_ID,'+ #13 +
      '      vDadosNota.EMPRESA_ID,'+ #13 +
      '      vDadosNota.CADASTRO_ID,'+ #13 +
      #13 +
      '      vDadosItens(i).VALOR_TOTAL,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO,'+ #13 +
      #13 +
      '      vDadosItens(i).CST,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS,'+ #13 +
      #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS_ST,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PRECO_PAUTA,'+ #13 +
      '      vDadosItens(i).IVA,'+ #13 +
      #13 +
      '      vDadosItens(i).VALOR_IPI,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_IPI,'+ #13 +
      #13 +
      '      vDadosItens(i).CST_PIS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS,'+ #13 +
      '      vDadosItens(i).VALOR_PIS,'+ #13 +
      #13 +
      '      vDadosItens(i).CST_COFINS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS,'+ #13 +
      '      vDadosItens(i).VALOR_COFINS'+ #13 +
      '		);'+ #13 +
      #13 +
      '    /* Se for para deposito fechado, zerando os impostos */'+ #13 +
      '    if vTipoEmpresaOrigem = ''D'' then'+ #13 +
      '      vDadosItens(i).CST                  := ''41'';'+ #13 +
      '      vDadosItens(i).CST_PIS              := ''08'';'+ #13 +
      '      vDadosItens(i).CST_COFINS           := ''08'';'+ #13 +
      #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS    := 0;'+ #13 +
      '      vDadosItens(i).VALOR_ICMS           := 0;'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST := 0;'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST        := 0;'+ #13 +
      #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS     := 0;'+ #13 +
      '      vDadosItens(i).PERCENTUAL_PIS       := 0;'+ #13 +
      '      vDadosItens(i).VALOR_PIS            := 0;'+ #13 +
      #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS  := 0;'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS    := 0;'+ #13 +
      '      vDadosItens(i).VALOR_COFINS         := 0;'+ #13 +
      #13 +
      '      vDadosItens(i).CFOP_ID := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vDadosItens(i).CST, ''RRD'', ''I'');'+ #13 +
      '    else'+ #13 +
      '      vDadosItens(i).CFOP_ID := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vDadosItens(i).CST, ''TPE'', ''I'');'+ #13 +
      '    end if;'+ #13 +
      #13 +
      '		vDadosItens(i).NATUREZA_OPERACAO := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);'+ #13 +
      #13 +
      '    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS    := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;'+ #13 +
      '    vDadosNota.VALOR_ICMS           := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST        := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;'+ #13 +
      '    vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;'+ #13 +
      '    vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;'+ #13 +
      '    vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL :='+ #13 +
      '      vDadosNota.VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS  := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;'+ #13 +
      '    vDadosNota.VALOR_DESCONTO        := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;'+ #13 +
      '    /* ------------------------------------------------------------------------------------------------------ */'+ #13 +
      #13 +
      '		if'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '      vDadosItens(i).VALOR_IPI -'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO >'+ #13 +
      '      vValorMaisSignificativo'+ #13 +
      '    then'+ #13 +
      '			vValorMaisSignificativo :='+ #13 +
      '        vDadosItens(i).VALOR_TOTAL +'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +'+ #13 +
      '        vDadosItens(i).VALOR_ICMS_ST +'+ #13 +
      '        vDadosItens(i).VALOR_IPI -'+ #13 +
      '        vDadosItens(i).VALOR_TOTAL_DESCONTO;'+ #13 +
      #13 +
      '			vCfopIdCapa 	  	:= vDadosItens(i).CFOP_ID;'+ #13 +
      '			vNaturezaOperacao := vDadosItens(i).NATUREZA_OPERACAO;'+ #13 +
      '		end if;'+ #13 +
      #13 +
      '    i := i + 1;'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  if vDadosItens.count < 1 then'+ #13 +
      '    ERRO(''Os produtos da nota fiscal n�o foram encontrados!'');'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '	if vCfopIdCapa is null then'+ #13 +
      '		vCfopIdCapa 			:= vDadosItens(0).CFOP_ID;'+ #13 +
      '		vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;'+ #13 +
      '	end if;'+ #13 +
      #13 +
      '	vDadosNota.CFOP_ID           := vCfopIdCapa;'+ #13 +
      '  vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;'+ #13 +
      #13 +
      '  vDadosNota.VALOR_RECEBIDO_COBRANCA := vDadosNota.VALOR_TOTAL;'+ #13 +
      #13 +
      '  vSerieNota  := to_char(nvl(vSerieNFe, 1));'+ #13 +
      #13 +
      '  if vTipoEmpresaOrigem = ''D'' then'+ #13 +
      '    vDadosNota.INFORMACOES_COMPLEMENTARES := ''N�o incidencia do ICMS, nos termos do Art. 79, inciso I, "j" e Art. 19 do Anexo XII do RCTE-GO'';'+ #13 +
      '  else'+ #13 +
      '    vDadosNota.INFORMACOES_COMPLEMENTARES := ''Transfer�ncia de produtos para suprir ESTOQUE.'';'+ #13 +
      '  end if;'+ #13 +
      #13 +
      '  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;'+ #13 +
      #13 +
      '	insert into NOTAS_FISCAIS('+ #13 +
      '    NOTA_FISCAL_ID,'+ #13 +
      '    CADASTRO_ID,'+ #13 +
      '    CFOP_ID,'+ #13 +
      '    EMPRESA_ID,'+ #13 +
      '		MODELO_NOTA,'+ #13 +
      '		SERIE_NOTA,'+ #13 +
      '    NATUREZA_OPERACAO,'+ #13 +
      '		STATUS,'+ #13 +
      '    TIPO_MOVIMENTO,'+ #13 +
      '    RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    NOME_FANTASIA_EMITENTE,'+ #13 +
      '    REGIME_TRIBUTARIO,'+ #13 +
      '    CNPJ_EMITENTE,'+ #13 +
      '    INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    LOGRADOURO_EMITENTE,'+ #13 +
      '    COMPLEMENTO_EMITENTE,'+ #13 +
      '    NOME_BAIRRO_EMITENTE,'+ #13 +
      '    NOME_CIDADE_EMITENTE,'+ #13 +
      '    NUMERO_EMITENTE,'+ #13 +
      '    ESTADO_ID_EMITENTE,'+ #13 +
      '    CEP_EMITENTE,'+ #13 +
      '    TELEFONE_EMITENTE,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    TIPO_NOTA,'+ #13 +
      '    LOGRADOURO_DESTINATARIO,'+ #13 +
      '    COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    ESTADO_ID_DESTINATARIO,'+ #13 +
      '    CEP_DESTINATARIO,'+ #13 +
      '    NUMERO_DESTINATARIO,'+ #13 +
      '    CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    VALOR_TOTAL,'+ #13 +
      '    VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    VALOR_DESCONTO,'+ #13 +
      '    VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    BASE_CALCULO_ICMS,'+ #13 +
      '    VALOR_ICMS,'+ #13 +
      '    BASE_CALCULO_ICMS_ST,'+ #13 +
      '    VALOR_ICMS_ST,'+ #13 +
      '    BASE_CALCULO_PIS,'+ #13 +
      '    VALOR_PIS,'+ #13 +
      '    BASE_CALCULO_COFINS,'+ #13 +
      '    VALOR_COFINS,'+ #13 +
      '    VALOR_IPI,'+ #13 +
      '    VALOR_FRETE,'+ #13 +
      '    VALOR_SEGURO,'+ #13 +
      '    VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    PESO_LIQUIDO,'+ #13 +
      '    PESO_BRUTO,'+ #13 +
      '		INFORMACOES_COMPLEMENTARES'+ #13 +
      '  )values('+ #13 +
      '		vNotaFiscalId,'+ #13 +
      '    vDadosNota.CADASTRO_ID,'+ #13 +
      '    vDadosNota.CFOP_ID,'+ #13 +
      '    vDadosNota.EMPRESA_ID,'+ #13 +
      '		''55'','+ #13 +
      '		vSerieNota,'+ #13 +
      '    vDadosNota.NATUREZA_OPERACAO,'+ #13 +
      '		''N'','+ #13 +
      '    ''TFI'','+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_EMITENTE,'+ #13 +
      '    vRegimeTributario,'+ #13 +
      '    vDadosNota.CNPJ_EMITENTE,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,'+ #13 +
      '    vDadosNota.LOGRADOURO_EMITENTE,'+ #13 +
      '    vDadosNota.COMPLEMENTO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_EMITENTE,'+ #13 +
      '    vDadosNota.NOME_CIDADE_EMITENTE,'+ #13 +
      '    vDadosNota.NUMERO_EMITENTE,'+ #13 +
      '    vDadosNota.ESTADO_ID_EMITENTE,'+ #13 +
      '    vDadosNota.CEP_EMITENTE,'+ #13 +
      '    vDadosNota.TELEFONE_EMITENTE,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,'+ #13 +
      '    vDadosNota.NOME_FANTASIA_DESTINATARIO,'+ #13 +
      '    vDadosNota.RAZAO_SOCIAL_DESTINATARIO,'+ #13 +
      '    vDadosNota.TIPO_PESSOA_DESTINATARIO,'+ #13 +
      '    vDadosNota.CPF_CNPJ_DESTINATARIO,'+ #13 +
      '    vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,'+ #13 +
      '    vDadosNota.NOME_CONSUMIDOR_FINAL,'+ #13 +
      '    vDadosNota.TELEFONE_CONSUMIDOR_FINAL,'+ #13 +
      '    ''N'','+ #13 +
      '    vDadosNota.LOGRADOURO_DESTINATARIO,'+ #13 +
      '    vDadosNota.COMPLEMENTO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_BAIRRO_DESTINATARIO,'+ #13 +
      '    vDadosNota.NOME_CIDADE_DESTINATARIO,'+ #13 +
      '    vDadosNota.ESTADO_ID_DESTINATARIO,'+ #13 +
      '    vDadosNota.CEP_DESTINATARIO,'+ #13 +
      '    vDadosNota.NUMERO_DESTINATARIO,'+ #13 +
      '    vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,'+ #13 +
      '    vDadosNota.VALOR_TOTAL,'+ #13 +
      '    vDadosNota.VALOR_TOTAL_PRODUTOS,'+ #13 +
      '    vDadosNota.VALOR_DESCONTO,'+ #13 +
      '    vDadosNota.VALOR_OUTRAS_DESPESAS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS,'+ #13 +
      '    vDadosNota.VALOR_ICMS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_ICMS_ST,'+ #13 +
      '    vDadosNota.VALOR_ICMS_ST,'+ #13 +
      '    vDadosNota.BASE_CALCULO_PIS,'+ #13 +
      '    vDadosNota.VALOR_PIS,'+ #13 +
      '    vDadosNota.BASE_CALCULO_COFINS,'+ #13 +
      '    vDadosNota.VALOR_COFINS,'+ #13 +
      '    vDadosNota.VALOR_IPI,'+ #13 +
      '    vDadosNota.VALOR_FRETE,'+ #13 +
      '    vDadosNota.VALOR_SEGURO,'+ #13 +
      '    vDadosNota.VALOR_RECEBIDO_COBRANCA,'+ #13 +
      '    vDadosNota.PESO_LIQUIDO,'+ #13 +
      '    vDadosNota.PESO_BRUTO,'+ #13 +
      '		vDadosNota.INFORMACOES_COMPLEMENTARES'+ #13 +
      '  );'+ #13 +
      #13 +
      '  for i in 0..vDadosItens.count - 1 loop'+ #13 +
      '    insert into NOTAS_FISCAIS_ITENS('+ #13 +
      '      NOTA_FISCAL_ID,'+ #13 +
      '      PRODUTO_ID,'+ #13 +
      '      ITEM_ID,'+ #13 +
      '      NOME_PRODUTO,'+ #13 +
      '      UNIDADE,'+ #13 +
      '      CFOP_ID,'+ #13 +
      '      CST,'+ #13 +
      '      CODIGO_NCM,'+ #13 +
      '      VALOR_TOTAL,'+ #13 +
      '      PRECO_UNITARIO,'+ #13 +
      '      QUANTIDADE,'+ #13 +
      '      VALOR_TOTAL_DESCONTO,'+ #13 +
      '      VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      BASE_CALCULO_ICMS,'+ #13 +
      '      PERCENTUAL_ICMS,'+ #13 +
      '      VALOR_ICMS,'+ #13 +
      '      BASE_CALCULO_ICMS_ST,'+ #13 +
      '      VALOR_ICMS_ST,'+ #13 +
      '      CST_PIS,'+ #13 +
      '      BASE_CALCULO_PIS,'+ #13 +
      '      PERCENTUAL_PIS,'+ #13 +
      '      VALOR_PIS,'+ #13 +
      '      CST_COFINS,'+ #13 +
      '      BASE_CALCULO_COFINS,'+ #13 +
      '      PERCENTUAL_COFINS,'+ #13 +
      '      VALOR_COFINS,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      IVA,'+ #13 +
      '      PRECO_PAUTA,'+ #13 +
      '      CODIGO_BARRAS,'+ #13 +
      '      VALOR_IPI,'+ #13 +
      '      PERCENTUAL_IPI,'+ #13 +
      '      INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      PERCENTUAL_ICMS_ST,'+ #13 +
      '      CEST'+ #13 +
      '    )values('+ #13 +
      '      vNotaFiscalId,'+ #13 +
      '      vDadosItens(i).PRODUTO_ID,'+ #13 +
      '      vDadosItens(i).ITEM_ID,'+ #13 +
      '      vDadosItens(i).NOME_PRODUTO,'+ #13 +
      '      vDadosItens(i).UNIDADE,'+ #13 +
      '      vDadosItens(i).CFOP_ID,'+ #13 +
      '      vDadosItens(i).CST,'+ #13 +
      '      vDadosItens(i).CODIGO_NCM,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL,'+ #13 +
      '      vDadosItens(i).PRECO_UNITARIO,'+ #13 +
      '      vDadosItens(i).QUANTIDADE,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_DESCONTO,'+ #13 +
      '      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_ICMS_ST,'+ #13 +
      '      vDadosItens(i).VALOR_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CST_PIS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_PIS,'+ #13;
  end;

  function GetComando2: string;
  begin
    Result :=
      '      vDadosItens(i).PERCENTUAL_PIS,'+ #13 +
      '      vDadosItens(i).VALOR_PIS,'+ #13 +
      '      vDadosItens(i).CST_COFINS,'+ #13 +
      '      vDadosItens(i).BASE_CALCULO_COFINS,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_COFINS,'+ #13 +
      '      vDadosItens(i).VALOR_COFINS,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,'+ #13 +
      '      vDadosItens(i).IVA,'+ #13 +
      '      vDadosItens(i).PRECO_PAUTA,'+ #13 +
      '      vDadosItens(i).CODIGO_BARRAS,'+ #13 +
      '      vDadosItens(i).VALOR_IPI,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_IPI,'+ #13 +
      '      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,'+ #13 +
      '      vDadosItens(i).PERCENTUAL_ICMS_ST,'+ #13 +
      '      vDadosItens(i).CEST'+ #13 +
      '    );'+ #13 +
      '  end loop;'+ #13 +
      #13 +
      '  update RETIRADAS set'+ #13 +
      '    NOTA_TRANSF_ESTOQUE_FISCAL_ID = vNotaFiscalId'+ #13 +
      '  where RETIRADA_ID in('+ #13 +
      '    select'+ #13 +
      '      MOVIMENTO_ID'+ #13 +
      '    from'+ #13 +
      '      VW_MOV_PEND_EMISSAO_NF_TRANSF'+ #13 +
      '    where EMPRESA_ORIGEM_ID = iEMPRESA_ORIGEM_ID'+ #13 +
      '    and trunc(DATA_HORA_EMISSAO) between iDATA_INICIAL and iDATA_FINAL'+ #13 +
      '    and TIPO_MOVIMENTO in(''VRE'', ''VRA'')'+ #13 +
      '  );'+ #13 +
      #13 +
      '  update ENTREGAS set'+ #13 +
      '    NOTA_TRANSF_ESTOQUE_FISCAL_ID = vNotaFiscalId'+ #13 +
      '  where ENTREGA_ID in('+ #13 +
      '    select'+ #13 +
      '      MOVIMENTO_ID'+ #13 +
      '    from'+ #13 +
      '      VW_MOV_PEND_EMISSAO_NF_TRANSF'+ #13 +
      '    where EMPRESA_ORIGEM_ID = iEMPRESA_ORIGEM_ID'+ #13 +
      '    and trunc(DATA_HORA_EMISSAO) between iDATA_INICIAL and iDATA_FINAL'+ #13 +
      '    and TIPO_MOVIMENTO = ''VEN'''+ #13 +
      '  );'+ #13 +
      #13 +
      '  oNOTA_FISCAL_ID := vNotaFiscalId;'+ #13 +
      #13 +
      'end GERAR_NOTA_TRANSF_MOVIMENTOS;'+ #13;
  end;

begin
  Result :=
    GetComando1 + GetComando2;
end;

function AjustarConstraintCK_CFOPS_CST_OPER_TIPO_MOV2: string;
begin
  Result :=
    'alter table CFOPS_CST_OPERACOES'+ #13 +
    'drop constraint CK_CFOPS_CST_OPER_TIPO_MOV;'+ #13 +
    #13 +
    'alter table CFOPS_CST_OPERACOES'+ #13 +
    'add constraint CK_CFOPS_CST_OPER_TIPO_MOV'+ #13 +
    'check(TIPO_MOVIMENTO in(''VIT'', ''DEV'', ''TPE'', ''REN'', ''RDF'', ''RRD''));'+ #13
end;

function AjustarFunctionBUSCAR_CFOP_ID_OPERACAO2: string;
begin
  Result :=
    'create or replace function BUSCAR_CFOP_ID_OPERACAO('+ #13 +
    '  iEMPRESA_ID     in number,'+ #13 +
    '  iCST            in string,'+ #13 +
    '  iTIPO_MOVIMENTO in string,'+ #13 +
    '  iTIPO_OPERACAO  in string'+ #13 +
    ') '+ #13 +
    'return string'+ #13 +
    'is'+ #13 +
    '  vCfopId  CFOP.CFOP_PESQUISA_ID%type;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  if iCST not in(''00'',''10'',''20'',''30'',''40'',''41'',''50'',''51'',''60'',''70'',''90'') then'+ #13 +
    '    ERRO(''CST passado via par�metro incorreto!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '/* iTIPO_OPERACAO'+ #13 +
    '  I - Interna'+ #13 +
    '  E - Interestadual'+ #13 +
    '  X - Exterior'+ #13 +
    '*/'+ #13 +
    '  if not(nvl(iTIPO_OPERACAO, ''A'') in(''I'', ''E'', ''X'')) then'+ #13 +
    '    ERRO(''TIPO_OPERACAO passado via par�metro incorreto!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  /*'+ #13 +
    '    iTIPO_MOVIMENTO'+ #13 +
    '    VIT - Venda interna'+ #13 +
    '    DEV - Devolu��o de venda'+ #13 +
    '    TPE - Transfer�ncia de produtos entre empresas'+ #13 +
    '    REN - Retorno de entrega'+ #13 +
    '    RDF - Remessa para dep�sito fechado'+ #13 +
    '    RRD - Retorno de remessa para dep�sito'+ #13 +
    '  */'+ #13 +
    #13 +
    '  if not(nvl(iTIPO_MOVIMENTO, ''XXX'') in(''VIT'', ''DEV'', ''TPE'', ''REN'', ''RDF'', ''RRD'')) then'+ #13 +
    '    ERRO(''iTIPO_MOVIMENTO passado via par�metro incorreto!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  begin'+ #13 +
    '    select'+ #13 +
    '      CFOP_ID'+ #13 +
    '    into'+ #13 +
    '      vCfopId'+ #13 +
    '    from'+ #13 +
    '      CFOPS_CST_OPERACOES'+ #13 +
    '    where CST = iCST'+ #13 +
    '    and EMPRESA_ID = iEMPRESA_ID'+ #13 +
    '    and TIPO_OPERACAO = iTIPO_OPERACAO'+ #13 +
    '    and TIPO_MOVIMENTO = iTIPO_MOVIMENTO;'+ #13 +
    '  exception'+ #13 +
    '    when others then'+ #13 +
    '      ERRO('+ #13 +
    '        ''Falha ao buscar os par�metros de CFOP! '' || sqlerrm || chr(13) ||'+ #13 +
    '        ''CST '' || iCST || chr(13) ||'+ #13 +
    '        ''TIPO OPER. '' || iTIPO_OPERACAO || chr(13) ||'+ #13 +
    '        ''EMPRESA '' || iEMPRESA_ID || chr(13) ||'+ #13 +
    '        ''TIPO MOV '' || iTIPO_MOVIMENTO'+ #13 +
    '      );'+ #13 +
    '  end;'+ #13 +
    #13 +
    '  return vCfopId;'+ #13 +
    #13 +
    'end BUSCAR_CFOP_ID_OPERACAO;'+ #13
end;

function AjustarViewVW_MOV_PEND_EMISSAO_NF_TRANSF: string;
begin
  Result :=
    'create or replace view VW_MOV_PEND_EMISSAO_NF_TRANSF'+ #13 +
    'as'+ #13 +
    'select'+ #13 +
    '  RET.RETIRADA_ID as MOVIMENTO_ID,'+ #13 +
    '  case when RET.TIPO_MOVIMENTO = ''A'' then ''VRA'' else ''VRE'' end as TIPO_MOVIMENTO,'+ #13 +
    '  NOF.DATA_HORA_EMISSAO,'+ #13 +
    '  ORC.CLIENTE_ID,'+ #13 +
    '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
    '  RET.EMPRESA_ID as EMPRESA_ORIGEM_ID,'+ #13 +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA_ORIGEM,'+ #13 +
    '  NOF.EMPRESA_ID as EMPRESA_DESTINO_ID,'+ #13 +
    '  EMD.NOME_FANTASIA as NOME_EMPRESA_DESTINO'+ #13 +
    'from'+ #13 +
    '  RETIRADAS RET'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join NOTAS_FISCAIS NOF'+ #13 +
    'on RET.RETIRADA_ID = NOF.RETIRADA_ID'+ #13 +
    #13 +
    'inner join EMPRESAS EMP'+ #13 +
    'on RET.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
    #13 +
    'inner join EMPRESAS EMD'+ #13 +
    'on NOF.EMPRESA_ID = EMD.EMPRESA_ID'+ #13 +
    #13 +
    'where RET.EMPRESA_ID <> NOF.EMPRESA_ID'+ #13 +
    'and NOF.STATUS = ''E'''+ #13 +
    'and RET.NOTA_TRANSF_ESTOQUE_FISCAL_ID is null'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    'select'+ #13 +
    '  ENT.ENTREGA_ID,'+ #13 +
    '  ''VEN'' as TIPO_MOVIMENTO,'+ #13 +
    '  NOF.DATA_HORA_EMISSAO,'+ #13 +
    '  ORC.CLIENTE_ID,'+ #13 +
    '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
    '  ENT.EMPRESA_ENTREGA_ID as EMPRESA_ORIGEM_ID,'+ #13 +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA_ORIGEM,'+ #13 +
    '  NOF.EMPRESA_ID as EMPRESA_DESTINO_ID,'+ #13 +
    '  EMD.NOME_FANTASIA as NOME_EMPRESA_DESTINO'+ #13 +
    'from'+ #13 +
    '  ENTREGAS ENT'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join NOTAS_FISCAIS NOF'+ #13 +
    'on ENT.ENTREGA_ID = NOF.ENTREGA_ID'+ #13 +
    #13 +
    'inner join EMPRESAS EMP'+ #13 +
    'on ENT.EMPRESA_ENTREGA_ID = EMP.EMPRESA_ID'+ #13 +
    #13 +
    'inner join EMPRESAS EMD'+ #13 +
    'on NOF.EMPRESA_ID = EMD.EMPRESA_ID'+ #13 +
    #13 +
    'where ENT.EMPRESA_ENTREGA_ID <> NOF.EMPRESA_ID'+ #13 +
    'and NOF.STATUS = ''E'''+ #13 +
    'and ENT.NOTA_TRANSF_ESTOQUE_FISCAL_ID is null;'+ #13
end;

function AjustarViewVW_MOV_ITE_PEND_EMISSAO_NF_TR: string;
begin
  Result :=
    'create or replace view VW_MOV_ITE_PEND_EMISSAO_NF_TR'+ #13 +
    'as'+ #13 +
    'select'+ #13 +
    '  RET.EMPRESA_ID as EMPRESA_ORIGEM_ID,'+ #13 +
    '  NOF.EMPRESA_ID as EMPRESA_DESTINO_ID,'+ #13 +
    '  trunc(NOF.DATA_HORA_EMISSAO) as DATA_MOVIMENTO,'+ #13 +
    #13 +
    '  PRO.PRODUTO_ID,'+ #13 +
    '  ITE.ITEM_ID,'+ #13 +
    '  ITE.QUANTIDADE,'+ #13 +
    '  PRO.NOME as NOME_PRODUTO,'+ #13 +
    #13 +
    '  zvl(zvl(zvl(CUS.PRECO_FINAL, CUS.PRECO_FINAL_MEDIO), CUS.PRECO_LIQUIDO), CUS.PRECO_LIQUIDO_MEDIO) as PRECO_UNITARIO,    '+ #13 +
    #13 +
    '  PRO.CODIGO_NCM,'+ #13 +
    '  PRO.UNIDADE_VENDA,'+ #13 +
    '  PRO.CODIGO_BARRAS,'+ #13 +
    '  PRO.CEST'+ #13 +
    'from'+ #13 +
    '  RETIRADAS_ITENS ITE '+ #13 +
    #13 +
    'inner join RETIRADAS RET'+ #13 +
    'on ITE.RETIRADA_ID = RET.RETIRADA_ID'+ #13 +
    #13 +
    'inner join NOTAS_FISCAIS NOF'+ #13 +
    'on RET.RETIRADA_ID = NOF.RETIRADA_ID'+ #13 +
    'and NOF.STATUS = ''E'''+ #13 +
    'and RET.EMPRESA_ID <> NOF.EMPRESA_ID'+ #13 +
    'and RET.NOTA_TRANSF_ESTOQUE_FISCAL_ID is null'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'inner join VW_CUSTOS_PRODUTOS CUS'+ #13 +
    'on PRO.PRODUTO_ID = CUS.PRODUTO_ID'+ #13 +
    'and CUS.EMPRESA_ID = RET.EMPRESA_ID'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    'select'+ #13 +
    '  ENT.EMPRESA_ENTREGA_ID as EMPRESA_ORIGEM_ID,'+ #13 +
    '  NOF.EMPRESA_ID as EMPRESA_DESTINO_ID,'+ #13 +
    '  trunc(NOF.DATA_HORA_EMISSAO) as DATA_MOVIMENTO,'+ #13 +
    #13 +
    '  PRO.PRODUTO_ID,'+ #13 +
    '  ITE.ITEM_ID,'+ #13 +
    '  ITE.QUANTIDADE,'+ #13 +
    '  PRO.NOME as NOME_PRODUTO,'+ #13 +
    #13 +
    '  zvl(zvl(zvl(CUS.PRECO_FINAL, CUS.PRECO_FINAL_MEDIO), CUS.PRECO_LIQUIDO), CUS.PRECO_LIQUIDO_MEDIO) as PRECO_UNITARIO,    '+ #13 +
    #13 +
    '  PRO.CODIGO_NCM,'+ #13 +
    '  PRO.UNIDADE_VENDA,'+ #13 +
    '  PRO.CODIGO_BARRAS,'+ #13 +
    '  PRO.CEST'+ #13 +
    'from'+ #13 +
    '  ENTREGAS_ITENS ITE '+ #13 +
    #13 +
    'inner join ENTREGAS ENT'+ #13 +
    'on ITE.ENTREGA_ID = ENT.ENTREGA_ID'+ #13 +
    #13 +
    'inner join NOTAS_FISCAIS NOF'+ #13 +
    'on ENT.ENTREGA_ID = NOF.ENTREGA_ID'+ #13 +
    'and NOF.STATUS = ''E'''+ #13 +
    'and ENT.EMPRESA_ENTREGA_ID <> NOF.EMPRESA_ID'+ #13 +
    'and ENT.NOTA_TRANSF_ESTOQUE_FISCAL_ID is null'+ #13 +
    #13 +
    'inner join PRODUTOS PRO'+ #13 +
    'on ITE.PRODUTO_ID = PRO.PRODUTO_ID'+ #13 +
    #13 +
    'inner join VW_CUSTOS_PRODUTOS CUS'+ #13 +
    'on PRO.PRODUTO_ID = CUS.PRODUTO_ID'+ #13 +
    'and CUS.EMPRESA_ID = ENT.EMPRESA_ENTREGA_ID;'+ #13
end;

function AjustarConstraintCK_PAR_EMP_TIPO_REDIREC_NOTA: string;
begin
  Result :=
    'alter table PARAMETROS_EMPRESA'+ #13 +
    'drop constraint CK_PAR_EMP_TIPO_REDIREC_NOTA;'+ #13 +
    #13 +
    'alter table PARAMETROS_EMPRESA'+ #13 +
    'add constraint CK_PAR_EMP_TIPO_REDIREC_NOTA'+ #13 +
    'check(TIPO_REDIRECIONAMENTO_NOTA in(''NRE'', ''RPJ'', ''TOD''));'+ #13
end;

function AjustarConstraintCK_NOTAS_FISCAIS_TIPO_MOVIMENT2: string;
begin
  Result :=
    'alter table NOTAS_FISCAIS'+ #13 +
    'drop constraint CK_NOTAS_FISCAIS_TIPO_MOVIMENT;'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  INICIAR_SESSAO(1, 1, ''ALTIS'', ''ALTIS'');'+ #13 +
    #13 +
    '  update NOTAS_FISCAIS set'+ #13 +
    '    TIPO_MOVIMENTO = ''TFI'','+ #13 +
    '    RETIRADA_ID = null,'+ #13 +
    '    ENTREGA_ID = null'+ #13 +
    '  where TIPO_MOVIMENTO in(''TFE'', ''TFR'');'+ #13 +
    #13 +
    'end;'+ #13 +
    '/' +
    #13 +
    'alter table NOTAS_FISCAIS'+ #13 +
    'add constraint CK_NOTAS_FISCAIS_TIPO_MOVIMENT'+ #13 +
    'check('+ #13 +
    '  TIPO_MOVIMENTO = ''VRA'' and ORCAMENTO_BASE_ID is not null and RETIRADA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''VRE'' and ORCAMENTO_BASE_ID is not null and RETIRADA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''VEN'' and ORCAMENTO_BASE_ID is not null and ENTREGA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''DRE'' and ORCAMENTO_BASE_ID is not null and DEVOLUCAO_ID is not null and RETIRADA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''DEN'' and ORCAMENTO_BASE_ID is not null and DEVOLUCAO_ID is not null and ENTREGA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''OUT'' and OUTRA_NOTA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''ACU'' and ACUMULADO_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''DAC'' and ORCAMENTO_BASE_ID is not null and DEVOLUCAO_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''TPE'' and TRANSFERENCIA_PRODUTOS_ID is not null  '+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''NNE'' and NOTA_FISCAL_ORIGEM_CUPOM_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''REN'' and ORCAMENTO_BASE_ID is not null and ENTREGA_ID is not null'+ #13 +
    '  or'+ #13 +
    '  TIPO_MOVIMENTO = ''TFI'''+ #13 +
    ');'+ #13
end;

function DroparColunasTabelaNOTAS_FISCAIS: string;
begin
  Result :=
    'alter table NOTAS_FISCAIS'+ #13 +
    'drop (TRANSF_FISCAL_ENTREGA_ID, TRANSF_FISCAL_RETIRADA_ID) cascade constraints;'+ #13
end;

function CriarTabelaCONTROLES_ENTREGAS_AJUDANTES: string;
begin
  Result :=
    'create table CONTROLES_ENTREGAS_AJUDANTES('+ #13 +
    '  CONTROLE_ENTREGA_ID               number(10) not null,'+ #13 +
    '  AJUDANTE_ID                       number(4) not null,'+ #13 +
    #13 +
    '  /* Colunas de logs do sistema */'+ #13 +
    '  USUARIO_SESSAO_ID                 number(4) not null,'+ #13 +
    '  DATA_HORA_ALTERACAO               date not null,'+ #13 +
    '  ROTINA_ALTERACAO                  varchar2(30) not null,'+ #13 +
    '  ESTACAO_ALTERACAO                 varchar2(30) not null'+ #13 +
    ');'+ #13 +
    #13 +
    '/* Chaves primaria */'+ #13 +
    'alter table CONTROLES_ENTREGAS_AJUDANTES'+ #13 +
    'add constraint PK_CONTROLES_ENTRE_AJUDANTES'+ #13 +
    'primary key(CONTROLE_ENTREGA_ID, AJUDANTE_ID)'+ #13 +
    'using index tablespace INDICES;'+ #13 +
    #13 +
    '/* Chaves estrangeiras */'+ #13 +
    'alter table CONTROLES_ENTREGAS_AJUDANTES'+ #13 +
    'add constraint FK_CONT_ENTRE_AJUD_CONT_ENT_ID'+ #13 +
    'foreign key(CONTROLE_ENTREGA_ID)'+ #13 +
    'references CONTROLES_ENTREGAS(CONTROLE_ENTREGA_ID);'+ #13 +
    #13 +
    'alter table CONTROLES_ENTREGAS_AJUDANTES'+ #13 +
    'add constraint FK_CONT_ENTRE_AJUD_AJUDANTE_ID'+ #13 +
    'foreign key(AJUDANTE_ID)'+ #13 +
    'references FUNCIONARIOS(FUNCIONARIO_ID);'+ #13
end;

function AjustarTriggerCONTROLES_ENTREGAS_AJUD_IU_BR: string;
begin
  Result :=
    'create or replace trigger CONTROLES_ENTREGAS_AJUD_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on CONTROLES_ENTREGAS_AJUDANTES'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    'end CONTROLES_ENTREGAS_AJUD_IU_BR;'+ #13
end;

function AjustarTriggerCONTROLES_ENTREGAS_D_BR: string;
begin
  Result :=
    'create or replace trigger CONTROLES_ENTREGAS_D_BR'+ #13 +
    'before delete'+ #13 +
    'on CONTROLES_ENTREGAS'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    '  if :old.STATUS = ''B'' then'+ #13 +
    '    ERRO(''N�o � permitido deletar um controle de entrega j� baixado|!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  delete from CONTROLES_ENTREGAS_AJUDANTES'+ #13 +
    '  where CONTROLE_ENTREGA_ID = :old.CONTROLE_ENTREGA_ID;'+ #13 +
    #13 +
    '  delete from CONTROLES_ENTREGAS_ITENS'+ #13 +
    '  where CONTROLE_ENTREGA_ID = :old.CONTROLE_ENTREGA_ID;'+ #13 +
    #13 +
    'end CONTROLES_ENTREGAS_D_BR;'+ #13
end;

function CriarColunaAJUDANTETabelaFUNCIONARIOS: string;
begin
  Result :=
    'alter table FUNCIONARIOS disable all triggers;'+ #13 +
    #13 +
    'alter table FUNCIONARIOS'+ #13 +
    'add AJUDANTE char(1) default ''N'' not null;'+ #13 +
    #13 +
    'alter table FUNCIONARIOS enable all triggers;'+ #13 +
    #13 +
    'alter table FUNCIONARIOS'+ #13 +
    'add constraint CK_FUNCIONARIOS_AJUDANTE'+ #13 +
    'check(AJUDANTE in(''S'', ''N''));'+ #13
end;

function CriarColunaQTDE_DIAS_BLOQ_VENDA_TIT_VENC: string;
begin
  Result :=
    'alter table PARAMETROS_EMPRESA disable all triggers;'+ #13 +
    #13 +
    'alter table PARAMETROS_EMPRESA'+ #13 +
    'add QTDE_DIAS_BLOQ_VENDA_TIT_VENC   number(3) default 15 not null;'+ #13 +
    #13 +
    'alter table PARAMETROS_EMPRESA enable all triggers;'+ #13 +
    #13 +
    'alter table PARAMETROS_EMPRESA'+ #13 +
    'add constraint CK_PAR_EM_QTD_DIAS_BLOQ_V_T_VE'+ #13 +
    'check(QTDE_DIAS_BLOQ_VENDA_TIT_VENC >= 0);'+ #13
end;

function CriarColunaTIPO_COB_FECHAMENTO_TURNO_ID: string;
begin
  Result :=
    'alter table PARAMETROS'+ #13 +
    'add TIPO_COB_FECHAMENTO_TURNO_ID    number(3);'+ #13 +
    #13 +
    'alter table PARAMETROS'+ #13 +
    'add constraint FK_PAR_TIPO_COBR_FECH_TURN_ID'+ #13 +
    'foreign key(TIPO_COB_FECHAMENTO_TURNO_ID)'+ #13 +
    'references TIPOS_COBRANCA(COBRANCA_ID);'+ #13
end;

function AjustarConstraintCK_CONTAS_RECEBER_ORIGEM: string;
begin
  Result :=
    'alter table CONTAS_RECEBER'+ #13 +
    'drop constraint CK_CONTAS_RECEBER_ORIGEM;'+ #13 +
    #13 +
    'alter table CONTAS_RECEBER'+ #13 +
    'add constraint CK_CONTAS_RECEBER_ORIGEM'+ #13 +
    'check('+ #13 +
    '  (ORIGEM = ''MAN'')'+ #13 +
    '  or'+ #13 +
    '  (ORIGEM = ''ORC'' and ORCAMENTO_ID is not null)'+ #13 +
    '  or'+ #13 +
    '  (ORIGEM = ''BXR'' and BAIXA_ORIGEM_ID is not null)'+ #13 +
    '  or'+ #13 +
    '  (ORIGEM = ''BAP'' and BAIXA_PAGAR_ORIGEM_ID is not null)'+ #13 +
    '  or'+ #13 +
    '  (ORIGEM = ''DTU'')'+ #13 +
    '  or'+ #13 +
    '  (ORIGEM = ''ACU'' and ACUMULADO_ID is not null)'+ #13 +
    '  or'+ #13 +
    '  (ORIGEM = ''ADA'' and ORCAMENTO_ID is not null)'+ #13 +
    ');'+ #13
end;

function AjustarProcedureLANCAR_DIFERENCA_FECH_CAIXA: string;
begin
  Result :=
    'create or replace procedure LANCAR_DIFERENCA_FECH_CAIXA(iTURNO_ID in number, iVALOR_DIFERENCA in number)'+ #13 +
    'is'+ #13 +
    '  vReceberId            CONTAS_RECEBER.RECEBER_ID%type;'+ #13 +
    '  vCadastroId           FUNCIONARIOS.CADASTRO_ID%type;'+ #13 +
    '  vEmpresaId            TURNOS_CAIXA.EMPRESA_ID%type;'+ #13 +
    '  vFechamentoId         MOVIMENTOS_TURNOS.MOVIMENTO_TURNO_ID%type;'+ #13 +
    '  vValorMinLancamento   PARAMETROS_EMPRESA.VALOR_MIN_DIF_TURNO_CONTAS_REC%type;'+ #13 +
    #13 +
    '  vTipoCobDiferencaId   PARAMETROS.TIPO_COB_FECHAMENTO_TURNO_ID%type;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  /* Se a difern�a for posivita n�o tem o porque lan�ar nada */'+ #13 +
    '  if iVALOR_DIFERENCA > 0 then'+ #13 +
    '    return;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  select'+ #13 +
    '    FUN.CADASTRO_ID,'+ #13 +
    '    TUR.EMPRESA_ID'+ #13 +
    '  into'+ #13 +
    '    vCadastroId,'+ #13 +
    '    vEmpresaId'+ #13 +
    '  from'+ #13 +
    '    TURNOS_CAIXA TUR'+ #13 +
    '  inner join FUNCIONARIOS FUN'+ #13 +
    '  on TUR.FUNCIONARIO_ID = FUN.FUNCIONARIO_ID'+ #13 +
    '  where TUR.TURNO_ID = iTURNO_ID;'+ #13 +
    #13 +
    '  select'+ #13 +
    '    VALOR_MIN_DIF_TURNO_CONTAS_REC'+ #13 +
    '  into'+ #13 +
    '    vValorMinLancamento'+ #13 +
    '  from'+ #13 +
    '    PARAMETROS_EMPRESA'+ #13 +
    '  where EMPRESA_ID = vEmpresaId;'+ #13 +
    #13 +
    '  if (vValorMinLancamento = 0) or (abs(iVALOR_DIFERENCA) < vValorMinLancamento) then'+ #13 +
    '    return;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  select'+ #13 +
    '    MOVIMENTO_TURNO_ID'+ #13 +
    '  into'+ #13 +
    '    vFechamentoId'+ #13 +
    '  from'+ #13 +
    '    MOVIMENTOS_TURNOS'+ #13 +
    '  where TURNO_ID = iTURNO_ID'+ #13 +
    '  and TIPO_MOVIMENTO = ''FEC'';'+ #13 +
    #13 +
    '  select'+ #13 +
    '    TIPO_COB_FECHAMENTO_TURNO_ID'+ #13 +
    '  into'+ #13 +
    '    vTipoCobDiferencaId'+ #13 +
    '  from'+ #13 +
    '    PARAMETROS;'+ #13 +
    #13 +
    '  vReceberId := SEQ_RECEBER_ID.nextval;'+ #13 +
    #13 +
    '  insert into CONTAS_RECEBER('+ #13 +
    '    RECEBER_ID,'+ #13 +
    '    CADASTRO_ID,'+ #13 +
    '    EMPRESA_ID,'+ #13 +
    '    COBRANCA_ID,'+ #13 +
    '    PLANO_FINANCEIRO_ID,'+ #13 +
    '    DOCUMENTO,'+ #13 +
    '    PORTADOR_ID,'+ #13 +
    '    DATA_VENCIMENTO,'+ #13 +
    '    DATA_VENCIMENTO_ORIGINAL,'+ #13 +
    '    VALOR_DOCUMENTO,'+ #13 +
    '    STATUS,'+ #13 +
    '    PARCELA,'+ #13 +
    '    NUMERO_PARCELAS,'+ #13 +
    '    ORIGEM'+ #13 +
    '  )values('+ #13 +
    '    vReceberId,'+ #13 +
    '    vCadastroId,'+ #13 +
    '    vEmpresaId,'+ #13 +
    '    vTipoCobDiferencaId,      -- Duplicata'+ #13 +
    '    ''1.001.005'', -- Plano financeiro'+ #13 +
    '    ''DIF-TUR'' || iTURNO_ID,'+ #13 +
    '    ''9999'', -- Portador'+ #13 +
    '    last_day(sysdate),'+ #13 +
    '    last_day(sysdate),'+ #13 +
    '    abs(iVALOR_DIFERENCA),'+ #13 +
    '    ''A'','+ #13 +
    '    1,'+ #13 +
    '    1,'+ #13 +
    '    ''DTU'''+ #13 +
    '  );'+ #13 +
    #13 +
    '  GRAVAR_TITULOS_MOV_TURNOS_ITE(null, null, vFechamentoId, ''COB'', vReceberId);'+ #13 +
    #13 +
    'end LANCAR_DIFERENCA_FECH_CAIXA;'+ #13
end;

function AjustarTriggerCLIENTES_IU_BR: string;
begin
  Result :=
    'create or replace trigger CLIENTES_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on CLIENTES'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '  if SESSAO.ROTINA = ''SUPORTE'' then'+ #13 +
    '    return;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  if inserting then'+ #13 +
    '    :new.DATA_CADASTRO := sysdate;'+ #13 +
    #13 +
    '    update CADASTROS set'+ #13 +
    '      E_CLIENTE = ''S'''+ #13 +
    '    where CADASTRO_ID = :new.CADASTRO_ID;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '	if :new.CADASTRO_ID = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID and SESSAO.ROTINA not in(''SEM DEFINICAO'', ''ATUALIZAR_INFORMACOES_CLIENTES'') then'+ #13 +
    '	  ERRO(''Altera��es no cliente consumidor final n�o s�o permitidas!'' || SESSAO.ROTINA);'+ #13 +
    '	end if;'+ #13 +
    #13 +
    '  if nvl(:old.LIMITE_CREDITO, 0) <> :new.LIMITE_CREDITO then'+ #13 +
    '    :new.USUARIO_APROV_LIMITE_CRED_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end CLIENTES_IU_BR;'+ #13
end;

function AjustarTriggerCADASTROS_IU_BR: string;
begin
  Result :=
    'create or replace trigger CADASTROS_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on CADASTROS'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    '  SESSAO.ROTINA := ''SUPORTE'';'+ #13 +
    #13 +
    '  if SESSAO.ROTINA = ''SUPORTE'' then'+ #13 +
    '    return;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '	if :new.CADASTRO_ID = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then'+ #13 +
    '	  ERRO(''Altera��es no cadastro de consumidor final n�o s�o permitidas!'');'+ #13 +
    '	end if;'+ #13 +
    #13 +
    '  :new.NUMERO := nvl(:new.NUMERO, ''SN'');'+ #13 +
    '  :new.NOME_FANTASIA := nvl(:new.NOME_FANTASIA, :new.RAZAO_SOCIAL);'+ #13 +
    #13 +
    'end CADASTROS_IU_BR;'+ #13
end;

function AdicionarColunaVALOR_LIQUIDOTabelaCompras: string;
begin
  Result :=
    'alter table COMPRAS'+ #13 +
    'add VALOR_LIQUIDO         number(8,2);'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  INICIAR_SESSAO(1, 1, ''ALITS'', ''ALTIS'');'+ #13 +
    #13 +
    '  for xCompras in ('+ #13 +
    '    select'+ #13 +
    '      COMPRA_ID,'+ #13 +
    '      sum(VALOR_LIQUIDO) as VALOR_LIQUIDO'+ #13 +
    '    from'+ #13 +
    '      COMPRAS_ITENS'+ #13 +
    '    group by'+ #13 +
    '      COMPRA_ID'+ #13 +
    '  ) loop'+ #13 +
    #13 +
    '    update COMPRAS set'+ #13 +
    '      VALOR_LIQUIDO = xCompras.VALOR_LIQUIDO'+ #13 +
    '    where COMPRA_ID = xCompras.COMPRA_ID;'+ #13 +
    #13 +
    '  end loop;'+ #13 +
    #13 +
    'end;'+ #13 +
    '/'+ #13 +
    #13 +
    'alter table COMPRAS'+ #13 +
    'add constraint CK_COMPRAS_VALOR_LIQUIDO'+ #13 +
    'check(VALOR_LIQUIDO > 0);'+ #13 +
    #13 +
    'alter table COMPRAS'+ #13 +
    'modify VALOR_LIQUIDO         number(8,2) not null;'+ #13
end;

function AdicionarColunaIGNORAR_BLOQUEIOS_VENDATabelaClientes: string;
begin
  Result :=
    'alter table CLIENTES disable all triggers;'+ #13 +
    #13 +
    'alter table CLIENTES'+ #13 +
    'add IGNORAR_BLOQUEIOS_VENDA char(1) default ''N'' not null;'+ #13 +
    #13 +
    'alter table CLIENTES enable all triggers;'+ #13
end;

function AjustarConstraintCK_CLIENTES_IGNORAR_BLOQ_VENDA: string;
begin
  Result :=
    'alter table CLIENTES'+ #13 +
    'add constraint CK_CLIENTES_IGNORAR_BLOQ_VENDA'+ #13 +
    'check(IGNORAR_BLOQUEIOS_VENDA in(''S'', ''N''));'+ #13
end;

function AdicionarColunaATIVOTabelaCLIENTES_GRUPOS: string;
begin
  Result :=
    'alter table CLIENTES_GRUPOS disable all triggers;'+ #13 +
    #13 +
    'alter table CLIENTES_GRUPOS'+ #13 +
    'add  ATIVO               char(1) default ''S'' not null;'+ #13 +
    #13 +
    'alter table CLIENTES_GRUPOS enable all triggers;'+ #13
end;

function AdicionarConstraintCK_CLIENTES_GRUPOS_ATIVO: string;
begin
  Result :=
    'alter table CLIENTES_GRUPOS'+ #13 +
    'add constraint CK_CLIENTES_GRUPOS_ATIVO'+ #13 +
    'check(ATIVO in(''S'', ''N''));'+ #13
end;

function AdicionarSequenceSEQ_BLOQUEIOS_ESTOQUES: string;
begin
  Result :=
    'create sequence SEQ_BLOQUEIOS_ESTOQUES'+ #13 +
    'start with 1 increment by 1'+ #13 +
    'minvalue 1 '+ #13 +
    'maxvalue 999999999999'+ #13 +
    'nocycle'+ #13 +
    'nocache'+ #13 +
    'noorder;'+ #13
end;

function AjustarViewVW_RELACAO_ORCAMENTOS_VENDAS: string;
begin
  Result :=
    'create or replace view VW_RELACAO_ORCAMENTOS_VENDAS'+ #13 +
    'as'+ #13 +
    'select'+ #13 +
    '  ORC.ORCAMENTO_ID,'+ #13 +
    '  ORC.CLIENTE_ID,'+ #13 +
    '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
    '  ORC.DATA_CADASTRO,'+ #13 +
    '  ORC.VENDEDOR_ID,'+ #13 +
    '  VEN.NOME as NOME_VENDEDOR,'+ #13 +
    '  ORC.CONDICAO_ID,'+ #13 +
    '  CON.NOME as NOME_CONDICAO_PAGAMENTO,'+ #13 +
    '  ORC.VALOR_TOTAL_PRODUTOS,'+ #13 +
    '  ORC.VALOR_OUTRAS_DESPESAS,'+ #13 +
    '  ORC.VALOR_DESCONTO,'+ #13 +
    '  ORC.VALOR_TOTAL,'+ #13 +
    '  ORC.EMPRESA_ID,'+ #13 +
    '  ORC.VALOR_DINHEIRO,'+ #13 +
    '  ORC.VALOR_CHEQUE,'+ #13 +
    '  ORC.VALOR_CARTAO_DEBITO,'+ #13 +
    '  ORC.VALOR_CARTAO_CREDITO,'+ #13 +
    '  ORC.VALOR_COBRANCA,'+ #13 +
    '  ORC.VALOR_FINANCEIRA,'+ #13 +
    '  ORC.VALOR_ACUMULATIVO,'+ #13 +
    '  ORC.VALOR_CREDITO,'+ #13 +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA,'+ #13 +
    '  ORC.TIPO_ANALISE_CUSTO,'+ #13 +
    '  ORC.STATUS,'+ #13 +
    '  ORC.TURNO_ID,'+ #13 +
    '  ORC.DATA_ENTREGA,'+ #13 +
    '  ORC.DATA_HORA_RECEBIMENTO,'+ #13 +
    '  ORC.INDICE_DESCONTO_VENDA_ID,'+ #13 +
    '  CUS.VALOR_CUSTO_ENTRADA,'+ #13 +
    '  CUS.VALOR_CUSTO_FINAL,'+ #13 +
    '  CLI.CLIENTE_GRUPO_ID,'+ #13 +
    '  GCL.NOME as NOME_GRUPO_CLIENTE'+ #13 +
    'from'+ #13 +
    '  ORCAMENTOS ORC'+ #13 +
    #13 +
    'inner join CLIENTES CLI'+ #13 +
    'on ORC.CLIENTE_ID = CLI.CADASTRO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on CLI.CADASTRO_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join CONDICOES_PAGAMENTO CON'+ #13 +
    'on ORC.CONDICAO_ID = CON.CONDICAO_ID'+ #13 +
    #13 +
    'left join FUNCIONARIOS VEN'+ #13 +
    'on ORC.VENDEDOR_ID = VEN.FUNCIONARIO_ID'+ #13 +
    #13 +
    'inner join EMPRESAS EMP'+ #13 +
    'on ORC.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
    #13 +
    'inner join VW_CUSTOS_ORCAMENTOS CUS'+ #13 +
    'on ORC.ORCAMENTO_ID = CUS.ORCAMENTO_ID'+ #13 +
    #13 +
    'left join CLIENTES_GRUPOS GCL'+ #13 +
    'on CLI.CLIENTE_GRUPO_ID = GCL.CLIENTE_GRUPO_ID'+ #13 +
    #13 +
    'with read only;'+ #13
end;

function AjustarViewVW_ESTOQUES: string;
begin
  Result :=
    'create or replace view VW_ESTOQUES'+ #13 +
    'as'+ #13 +
    'select'+ #13 +
    '  EST.EMPRESA_ID,'+ #13 +
    '  PRO.PRODUTO_ID,  '+ #13 +
    '  EST.FISICO / PRO.QUANTIDADE_VEZES_PAI as FISICO,'+ #13 +
    '  EST.DISPONIVEL / PRO.QUANTIDADE_VEZES_PAI as DISPONIVEL,'+ #13 +
    '  EST.RESERVADO / PRO.QUANTIDADE_VEZES_PAI as RESERVADO,'+ #13 +
    '  EST.BLOQUEADO / PRO.QUANTIDADE_VEZES_PAI as BLOQUEADO,'+ #13 +
    '  EST.ESTOQUE / PRO.QUANTIDADE_VEZES_PAI as ESTOQUE,'+ #13 +
    '  EST.COMPRAS_PENDENTES / PRO.QUANTIDADE_VEZES_PAI as COMPRAS_PENDENTES,'+ #13 +
    '  PRO.ACEITAR_ESTOQUE_NEGATIVO,'+ #13 +
    '  EST.ULTIMA_ENTRADA_ID,'+ #13 +
    '  EST.PENULTIMA_ENTRADA_ID'+ #13 +
    'from'+ #13 +
    '  PRODUTOS PRO'+ #13 +
    #13 +
    'inner join ESTOQUES EST'+ #13 +
    'on PRO.PRODUTO_PAI_ID = EST.PRODUTO_ID;'+ #13
end;

function AjustarViewVW_ESTOQUES_DIVISAO2: string;
begin
  Result :=
    'create or replace view VW_ESTOQUES_DIVISAO'+ #13 +
    'as'+ #13 +
    'select'+ #13 +
    '  EDI.EMPRESA_ID,'+ #13 +
    '  EDI.LOCAL_ID,'+ #13 +
    '  LOC.NOME as NOME_LOCAL,'+ #13 +
    '  PRO.PRODUTO_ID,'+ #13 +
    '  PRO.NOME as NOME_PRODUTO,'+ #13 +
    '  MAR.NOME as NOME_MARCA,'+ #13 +
    '  PRO.UNIDADE_VENDA,'+ #13 +
    '  PRO.TIPO_CONTROLE_ESTOQUE,  '+ #13 +
    '  EDI.LOTE,'+ #13 +
    '  EDI.FISICO / PRO.QUANTIDADE_VEZES_PAI as FISICO,'+ #13 +
    '  EDI.DISPONIVEL / PRO.QUANTIDADE_VEZES_PAI as DISPONIVEL,'+ #13 +
    '  EDI.RESERVADO / PRO.QUANTIDADE_VEZES_PAI as RESERVADO,'+ #13 +
    '  EDI.BLOQUEADO / PRO.QUANTIDADE_VEZES_PAI as BLOQUEADO,'+ #13 +
    '  EDI.MULTIPLO_PONTA_ESTOQUE,'+ #13 +
    '  EDI.PONTA_ESTOQUE,'+ #13 +
    '  PRO.ACEITAR_ESTOQUE_NEGATIVO,'+ #13 +
    '  LOT.DATA_FABRICACAO,'+ #13 +
    '  LOT.DATA_VENCIMENTO,'+ #13 +
    '  PRO.PRODUTO_PAI_ID,'+ #13 +
    '  round(zvl(CUS.PRECO_FINAL, CUS.PRECO_LIQUIDO) * PRO.QUANTIDADE_VEZES_PAI, 2) as CUSTO_TRANSFERENCIA'+ #13 +
    'from'+ #13 +
    '  PRODUTOS PRO'+ #13 +
    #13 +
    'inner join ESTOQUES_DIVISAO EDI'+ #13 +
    'on PRO.PRODUTO_PAI_ID = EDI.PRODUTO_ID'+ #13 +
    #13 +
    'inner join PRODUTOS_LOTES LOT'+ #13 +
    'on EDI.PRODUTO_ID = LOT.PRODUTO_ID'+ #13 +
    'and EDI.LOTE = LOT.LOTE'+ #13 +
    #13 +
    'inner join LOCAIS_PRODUTOS LOC'+ #13 +
    'on EDI.LOCAL_ID = LOC.LOCAL_ID'+ #13 +
    #13 +
    'inner join CUSTOS_PRODUTOS CUS'+ #13 +
    'on PRO.PRODUTO_PAI_ID = CUS.PRODUTO_ID'+ #13 +
    'and EDI.EMPRESA_ID = CUS.EMPRESA_ID'+ #13 +
    #13 +
    'inner join MARCAS MAR'+ #13 +
    'on PRO.MARCA_ID = MAR.MARCA_ID;'+ #13
end;

function AjustarProcedureGERAR_CREDITO_DEVOLUCAO: string;
begin
  Result :=
    'create or replace procedure GERAR_CREDITO_DEVOLUCAO(iDEVOLUCAO_ID in number)'+ #13 +
    'is'+ #13 +
    '  vClienteId   CONTAS_PAGAR.CADASTRO_ID%type;'+ #13 +
    '  vEmpresaId   CONTAS_PAGAR.EMPRESA_ID%type;'+ #13 +
    '  vPagarId     CONTAS_PAGAR.PAGAR_ID%type;'+ #13 +
    '  vValorGerar  CONTAS_PAGAR.VALOR_DOCUMENTO%type;'+ #13 +
    #13 +
    '  vCobrancaId  PARAMETROS.TIPO_COBRANCA_GERACAO_CRED_ID%type;'+ #13 +
    '  vIndice      CONTAS_PAGAR.INDICE_CONDICAO_PAGAMENTO%type;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  select'+ #13 +
    '    DEV.EMPRESA_ID,'+ #13 +
    '    nvl(DEV.CLIENTE_CREDITO_ID, ORC.CLIENTE_ID) as CLIENTE_ID,'+ #13 +
    '    DEV.VALOR_LIQUIDO,'+ #13 +
    '    ORC.INDICE_CONDICAO_PAGAMENTO'+ #13 +
    '  into'+ #13 +
    '    vEmpresaId,'+ #13 +
    '    vClienteId,'+ #13 +
    '    vValorGerar,'+ #13 +
    '    vIndice'+ #13 +
    '  from'+ #13 +
    '    DEVOLUCOES DEV'+ #13 +
    #13 +
    '  inner join ORCAMENTOS ORC'+ #13 +
    '  on DEV.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    '  where DEV.DEVOLUCAO_ID = iDEVOLUCAO_ID;'+ #13 +
    #13 +
    '  begin'+ #13 +
    '    select'+ #13 +
    '      TIPO_COBRANCA_GERACAO_CRED_ID'+ #13 +
    '    into'+ #13 +
    '      vCobrancaId'+ #13 +
    '    from'+ #13 +
    '      PARAMETROS;'+ #13 +
    '  exception'+ #13 +
    '    when others then'+ #13 +
    '      ERRO(''Tipo de cobran�a para gera��o do cr�dito n�o parametrizado, fa�a a parametriza��o nos "Par�metros"!'');'+ #13 +
    '  end;'+ #13 +
    #13 +
    '  if vClienteId = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then'+ #13 +
    '    ERRO(''N�o � permitido gerar cr�dito para consumidor final!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  select SEQ_PAGAR_ID.nextval'+ #13 +
    '  into vPagarId'+ #13 +
    '  from dual;'+ #13 +
    #13 +
    '  insert into CONTAS_PAGAR('+ #13 +
    '    PAGAR_ID,'+ #13 +
    '    CADASTRO_ID,'+ #13 +
    '    EMPRESA_ID,'+ #13 +
    '    DOCUMENTO,'+ #13 +
    '    ORIGEM,'+ #13 +
    '    DEVOLUCAO_ID,'+ #13 +
    '    COBRANCA_ID,'+ #13 +
    '    PORTADOR_ID,'+ #13 +
    '    PLANO_FINANCEIRO_ID,'+ #13 +
    '    DATA_VENCIMENTO,'+ #13 +
    '    DATA_VENCIMENTO_ORIGINAL,'+ #13 +
    '    VALOR_DOCUMENTO,'+ #13 +
    '    STATUS,'+ #13 +
    '    PARCELA,'+ #13 +
    '    NUMERO_PARCELAS,'+ #13 +
    '    INDICE_CONDICAO_PAGAMENTO,'+ #13 +
    '	  BLOQUEADO,'+ #13 +
    '    MOTIVO_BLOQUEIO'+ #13 +
    '  )values('+ #13 +
    '    vPagarId,'+ #13 +
    '    vClienteId,'+ #13 +
    '    vEmpresaId,'+ #13 +
    '    ''DEV-'' || NFORMAT(iDEVOLUCAO_ID, 0), -- Documento'+ #13 +
    '    ''DEV'','+ #13 +
    '    iDEVOLUCAO_ID,'+ #13 +
    '    vCobrancaId,'+ #13 +
    '    ''9999'','+ #13 +
    '    ''1.002.001'','+ #13 +
    '    trunc(sysdate),'+ #13 +
    '    trunc(sysdate),'+ #13 +
    '    vValorGerar,'+ #13 +
    '    ''A'','+ #13 +
    '    1,'+ #13 +
    '    1,'+ #13 +
    '    vIndice,'+ #13 +
    '	  SESSAO.PARAMETROS_GERAIS.BLOQUEAR_CREDITO_DEVOLUCAO,'+ #13 +
    '    case when SESSAO.PARAMETROS_GERAIS.BLOQUEAR_CREDITO_DEVOLUCAO = ''S'' then ''Bloqueio autom�tico de novos cr�ditos'' else null end'+ #13 +
    '  );'+ #13 +
    #13 +
    'end GERAR_CREDITO_DEVOLUCAO;'+ #13
end;

function AjustarViewVW_BOLETOS_RECEBER: string;
begin
  Result :=
    'create or replace view VW_BOLETOS_RECEBER'+ #13 +
    'as'+ #13 +
    'select '+ #13 +
    '  COR.RECEBER_ID, '+ #13 +
    '  COR.ORCAMENTO_ID,'+ #13 +
    '  COR.ACUMULADO_ID,'+ #13 +
    '  COR.PARCELA,'+ #13 +
    '  COR.NUMERO_PARCELAS,'+ #13 +
    '  COR.PORTADOR_ID,'+ #13 +
    '  POR.DESCRICAO as NOME_PORTADOR,'+ #13 +
    '  COR.COBRANCA_ID,'+ #13 +
    '  TCO.NOME as NOME_TIPO_COBRANCA,  '+ #13 +
    '  COR.EMPRESA_ID,'+ #13 +
    '  EMP.RAZAO_SOCIAL as NOME_EMPRESA,'+ #13 +
    '  COR.CADASTRO_ID,'+ #13 +
    '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
    '  COR.VALOR_DOCUMENTO,'+ #13 +
    '  COR.NOSSO_NUMERO,'+ #13 +
    '  COR.DOCUMENTO,'+ #13 +
    '  COR.STATUS,'+ #13 +
    '  COR.DATA_VENCIMENTO,'+ #13 +
    '  COR.DATA_EMISSAO,'+ #13 +
    '  COR.OBSERVACOES,'+ #13 +
    '  CON.CONTA,'+ #13 +
    '  CON.DIGITO_CONTA,'+ #13 +
    '  CON.CARTEIRA,'+ #13 +
    '  POR.INSTRUCAO_BOLETO,'+ #13 +
    '  CON.AGENCIA,'+ #13 +
    '  CON.DIGITO_AGENCIA,'+ #13 +
    '  CAD.LOGRADOURO,'+ #13 +
    '  CAD.COMPLEMENTO,'+ #13 +
    '  CAD.CEP,'+ #13 +
    '  BAI.NOME_BAIRRO,'+ #13 +
    '  BAI.NOME_CIDADE,'+ #13 +
    '  BAI.ESTADO_ID,'+ #13 +
    '  EMP.CNPJ as CNPJ_EMPRESA,'+ #13 +
    '  EMP.LOGRADOURO as LOGRADOURO_EMPRESA,'+ #13 +
    '  EMP.COMPLEMENTO as COMPLEMENTO_EMPRESA,'+ #13 +
    '  BEP.NOME_BAIRRO as NOME_BAIRRO_EMPRESA,'+ #13 +
    '  BEP.NOME_CIDADE as NOME_CIDADE_EMPRESA,'+ #13 +
    '  BEP.ESTADO_ID as ESTADO_ID_EMPRESA,'+ #13 +
    '  EMP.CEP as CEP_EMPRESA,'+ #13 +
    '  CAD.CPF_CNPJ'+ #13 +
    'from '+ #13 +
    '  CONTAS_RECEBER COR '+ #13 +
    #13 +
    'inner join CADASTROS CAD '+ #13 +
    'on COR.CADASTRO_ID = CAD.CADASTRO_ID '+ #13 +
    #13 +
    'inner join TIPOS_COBRANCA TCO '+ #13 +
    'on COR.COBRANCA_ID = TCO.COBRANCA_ID '+ #13 +
    #13 +
    'inner join PORTADORES POR '+ #13 +
    'on COR.PORTADOR_ID = POR.PORTADOR_ID '+ #13 +
    #13 +
    'inner join EMPRESAS EMP '+ #13 +
    'on COR.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
    #13 +
    'inner join PORTADORES_CONTAS PCO'+ #13 +
    'on POR.PORTADOR_ID = PCO.PORTADOR_ID'+ #13 +
    'and COR.EMPRESA_ID = PCO.EMPRESA_ID'+ #13 +
    #13 +
    'inner join CONTAS CON'+ #13 +
    'on PCO.CONTA_ID = CON.CONTA'+ #13 +
    #13 +
    'inner join VW_BAIRROS BAI'+ #13 +
    'on CAD.BAIRRO_ID = BAI.BAIRRO_ID'+ #13 +
    #13 +
    'inner join VW_BAIRROS BEP'+ #13 +
    'on EMP.BAIRRO_ID = BEP.BAIRRO_ID'+ #13 +
    #13 +
    'where TCO.BOLETO_BANCARIO = ''S'';'+ #13
end;

function AjustarViewVERIFICAR_BLOQUEIOS_ORCAMENTOS: string;
begin
  Result :=
    'create or replace procedure VERIFICAR_BLOQUEIOS_ORCAMENTOS(iORCAMENTO_ID in number)'+ #13 +
    'is'+ #13 +
    '  vQtde number;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  select '+ #13 +
    '    count(*)'+ #13 +
    '  into'+ #13 +
    '    vQtde'+ #13 +
    '  from ORCAMENTOS_BLOQUEIOS'+ #13 +
    '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
    '  and USUARIO_LIBERACAO_ID is null;'+ #13 +
    #13 +
    '  if vQtde = 0 then'+ #13 +
    '    update ORCAMENTOS set '+ #13 +
    '      STATUS = case when STATUS = ''OB'' then ''OE'' else ''VR'' end'+ #13 +
    '    where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end VERIFICAR_BLOQUEIOS_ORCAMENTOS;'+ #13
end;

function AjustarTabelaBLOQUEIOS_ESTOQUES_ITENS: string;
begin
  Result :=
    'alter table BLOQUEIOS_ESTOQUES_ITENS disable all triggers;'+ #13 +
    #13 +
    'alter table BLOQUEIOS_ESTOQUES_ITENS'+ #13 +
    'add ('+ #13 +
    '  BAIXADOS            number(20,4) default 0 not null,'+ #13 +
    '  CANCELADOS          number(20,4) default 0 not null,'+ #13 +
    '  SALDO               number(20,4) default 0 not null'+ #13 +
    ');'+ #13 +
    #13 +
    'update BLOQUEIOS_ESTOQUES_ITENS set'+ #13 +
    '  SALDO = QUANTIDADE - BAIXADOS - CANCELADOS;'+ #13 +
    #13 +
    'alter table BLOQUEIOS_ESTOQUES_ITENS enable all triggers;'+ #13
end;

function CriarNovasConstraintsBLOQUEIOS_ESTOQUES_ITENS: string;
begin
  Result :=
    'alter table BLOQUEIOS_ESTOQUES_ITENS'+ #13 +
    'add constraint CK_BLOQ_EST_ITENS_BAIXADOS'+ #13 +
    'check(BAIXADOS >= 0);'+ #13 +
    #13 +
    'alter table BLOQUEIOS_ESTOQUES_ITENS'+ #13 +
    'add constraint CK_BLOQ_EST_ITENS_CANCELADOS'+ #13 +
    'check(CANCELADOS >= 0);'+ #13 +
    #13 +
    'alter table BLOQUEIOS_ESTOQUES_ITENS'+ #13 +
    'add constraint CK_BLOQ_EST_ITENS_SALDO'+ #13 +
    'check(SALDO = QUANTIDADE - CANCELADOS - BAIXADOS);'+ #13
end;

function CriarTabelaBLOQUEIOS_ESTOQUES_BAIXAS: string;
begin
  Result :=
    'create table BLOQUEIOS_ESTOQUES_BAIXAS('+ #13 +
    '  BAIXA_ID            number(12) not null,'+ #13 +
    '  BLOQUEIO_ID         number(12) not null,'+ #13 +
    #13 +
    '  TIPO                char(1) not null,'+ #13 +
    #13 +
    '  USUARIO_BAIXA_ID    number(4) not null,'+ #13 +
    '  DATA_HORA_BAIXA     date not null,'+ #13 +
    #13 +
    '  OBSERVACOES         varchar2(800),'+ #13 +
    #13 +
    '  /* Colunas de logs do sistema */'+ #13 +
    '  USUARIO_SESSAO_ID       number(4) not null,'+ #13 +
    '  DATA_HORA_ALTERACAO     date not null,'+ #13 +
    '  ROTINA_ALTERACAO        varchar2(30) not null,'+ #13 +
    '  ESTACAO_ALTERACAO       varchar2(30) not null'+ #13 +
    ');'+ #13 +
    #13 +
    '/* Chave prim�ria */'+ #13 +
    'alter table BLOQUEIOS_ESTOQUES_BAIXAS'+ #13 +
    'add constraint PK_BLOQUEIOS_ESTOQUES_BAIXAS'+ #13 +
    'primary key(BAIXA_ID)'+ #13 +
    'using index tablespace INDICES;'+ #13 +
    #13 +
    '/* Chaves estrangeiras */'+ #13 +
    'alter table BLOQUEIOS_ESTOQUES_BAIXAS'+ #13 +
    'add constraint FK_BLOQ_EST_BAIXAS_BLOQUEIO_ID'+ #13 +
    'foreign key(BLOQUEIO_ID)'+ #13 +
    'references BLOQUEIOS_ESTOQUES(BLOQUEIO_ID);'+ #13 +
    #13 +
    '/* Checagens */'+ #13 +
    '/* '+ #13 +
    '  TIPO '+ #13 +
    '  B - Baixa'+ #13 +
    '  C - Cancelamento'+ #13 +
    '*/'+ #13 +
    'alter table BLOQUEIOS_ESTOQUES_BAIXAS'+ #13 +
    'add constraint CK_BLOQ_EST_BAIXAS_TIPO'+ #13 +
    'check(TIPO in(''B'', ''C''));'+ #13
end;

function CriarTriggerBLOQ_ESTOQUES_BAIXAS_IU_BR: string;
begin
  Result :=
    'create or replace trigger BLOQ_ESTOQUES_BAIXAS_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on BLOQUEIOS_ESTOQUES_BAIXAS'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '  if inserting then'+ #13 +
    '    :new.USUARIO_BAIXA_ID := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '    :new.DATA_HORA_BAIXA  := sysdate;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end BLOQ_ESTOQUES_BAIXAS_IU_BR;'+ #13
end;

function CriarTabelaBLOQUEIOS_EST_BAIXAS_ITENS: string;
begin
  Result :=
    'create table BLOQUEIOS_EST_BAIXAS_ITENS('+ #13 +
    '  BAIXA_ID            number(12) not null,  '+ #13 +
    '  ITEM_ID             number(3) not null,'+ #13 +
    '  QUANTIDADE          number(20,4) not null,'+ #13 +
    #13 +
    '  /* Colunas de logs do sistema */'+ #13 +
    '  USUARIO_SESSAO_ID       number(4) not null,'+ #13 +
    '  DATA_HORA_ALTERACAO     date not null,'+ #13 +
    '  ROTINA_ALTERACAO        varchar2(30) not null,'+ #13 +
    '  ESTACAO_ALTERACAO       varchar2(30) not null'+ #13 +
    ');'+ #13 +
    #13 +
    '/* Chave prim�ria */'+ #13 +
    'alter table BLOQUEIOS_EST_BAIXAS_ITENS'+ #13 +
    'add constraint PK_BLOQUEIOS_EST_BAIXAS_ITENS'+ #13 +
    'primary key(BAIXA_ID, ITEM_ID)'+ #13 +
    'using index tablespace INDICES;'+ #13 +
    #13 +
    '/* Chaves estrangeiras */'+ #13 +
    'alter table BLOQUEIOS_EST_BAIXAS_ITENS'+ #13 +
    'add constraint FK_BLOQ_EST_BAIXAS_BAIXA_ID'+ #13 +
    'foreign key(BAIXA_ID)'+ #13 +
    'references BLOQUEIOS_ESTOQUES_BAIXAS(BAIXA_ID);'+ #13 +
    #13 +
    '/* Checagens */'+ #13 +
    'alter table BLOQUEIOS_EST_BAIXAS_ITENS'+ #13 +
    'add constraint CK_BLOQ_EST_BAIXAS_QUANTIDADE'+ #13 +
    'check(QUANTIDADE > 0);'+ #13
end;

function AjustarTriggerBLOQ_ESTOQUES_BX_ITENS_IU_BR: string;
begin
  Result :=
    'create or replace trigger BLOQ_ESTOQUES_BX_ITENS_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on BLOQUEIOS_EST_BAIXAS_ITENS'+ #13 +
    'for each row'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    'end BLOQ_ESTOQUES_BX_ITENS_IU_BR;'+ #13
end;

function AjustarSequenceSEQ_BLOQUEIOS_ESTOQUES_BAIXAS: string;
begin
  Result :=
    'create sequence SEQ_BLOQUEIOS_ESTOQUES_BAIXAS'+ #13 +
    'start with 1 increment by 1'+ #13 +
    'minvalue 1'+ #13 +
    'maxvalue 999999999999'+ #13 +
    'nocycle'+ #13 +
    'nocache'+ #13 +
    'noorder;'+ #13
end;

function AjustarProcedureCONSOLIDAR_BX_BLOQUEIO_ESTOQUE: string;
begin
  Result :=
    'create or replace procedure CONSOLIDAR_BX_BLOQUEIO_ESTOQUE(iBAIXA_ID in number)'+ #13 +
    'is'+ #13 +
    '  vQtde       number;'+ #13 +
    '  vBloqueioId BLOQUEIOS_ESTOQUES_BAIXAS.BLOQUEIO_ID%type;'+ #13 +
    '  vTipoBaixa  BLOQUEIOS_ESTOQUES_BAIXAS.TIPO%type;'+ #13 +
    #13 +
    '  cursor cItens is'+ #13 +
    '  select'+ #13 +
    '    ITEM_ID,'+ #13 +
    '    QUANTIDADE'+ #13 +
    '  from'+ #13 +
    '    BLOQUEIOS_EST_BAIXAS_ITENS'+ #13 +
    '  where BAIXA_ID = iBAIXA_ID;'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  select'+ #13 +
    '    BLOQUEIO_ID,'+ #13 +
    '    TIPO'+ #13 +
    '  into'+ #13 +
    '    vBloqueioId,'+ #13 +
    '    vTipoBaixa'+ #13 +
    '  from'+ #13 +
    '    BLOQUEIOS_ESTOQUES_BAIXAS'+ #13 +
    '  where BAIXA_ID = iBAIXA_ID;'+ #13 +
    #13 +
    '  for xItens in cItens loop'+ #13 +
    #13 +
    '    update BLOQUEIOS_ESTOQUES_ITENS set'+ #13 +
    '      CANCELADOS = CANCELADOS + case when vTipoBaixa = ''C'' then xItens.QUANTIDADE else 0 end,'+ #13 +
    '      BAIXADOS = BAIXADOS + case when vTipoBaixa = ''B'' then xItens.QUANTIDADE else 0 end'+ #13 +
    '    where BLOQUEIO_ID = vBloqueioId'+ #13 +
    '    and ITEM_ID = xItens.ITEM_ID;'+ #13 +
    #13 +
    '  end loop;'+ #13 +
    #13 +
    '  select'+ #13 +
    '    count(*)'+ #13 +
    '  into'+ #13 +
    '    vQtde'+ #13 +
    '  from'+ #13 +
    '    BLOQUEIOS_ESTOQUES_ITENS'+ #13 +
    '  where BLOQUEIO_ID = vBloqueioId'+ #13 +
    '  and SALDO > 0;'+ #13 +
    #13 +
    '  if vQtde = 0 then'+ #13 +
    '    update BLOQUEIOS_ESTOQUES set'+ #13 +
    '      STATUS = ''B'''+ #13 +
    '    where BLOQUEIO_ID = vBloqueioId;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end CONSOLIDAR_BX_BLOQUEIO_ESTOQUE;'+ #13
end;

function AjustarProcedureBLOQUEIOS_ESTOQUES_ITENS_IU_BR: string;
begin
  Result :=
    'create or replace trigger BLOQUEIOS_ESTOQUES_ITENS_IU_BR'+ #13 +
    'before insert or update'+ #13 +
    'on BLOQUEIOS_ESTOQUES_ITENS'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    '  vEmpresaId  BLOQUEIOS_ESTOQUES.EMPRESA_ID%type;'+ #13 +
    'begin'+ #13 +
    #13 +
    '  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;'+ #13 +
    '  :new.DATA_HORA_ALTERACAO := sysdate;'+ #13 +
    '  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;'+ #13 +
    '  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;'+ #13 +
    #13 +
    '  :new.SALDO := :new.QUANTIDADE - :new.BAIXADOS - :new.CANCELADOS;'+ #13 +
    #13 +
    '  select'+ #13 +
    '    EMPRESA_ID'+ #13 +
    '  into'+ #13 +
    '    vEmpresaId'+ #13 +
    '  from'+ #13 +
    '    BLOQUEIOS_ESTOQUES'+ #13 +
    '  where BLOQUEIO_ID = :new.BLOQUEIO_ID;'+ #13 +
    #13 +
    '  if inserting then'+ #13 +
    '    MOVIMENTAR_ESTOQUE_BLOQUEADO('+ #13 +
    '      vEmpresaId,'+ #13 +
    '      :new.PRODUTO_ID,'+ #13 +
    '      :new.LOCAL_ID,'+ #13 +
    '      :new.LOTE,'+ #13 +
    '      :new.QUANTIDADE'+ #13 +
    '    );'+ #13 +
    '  else'+ #13 +
    '    MOVIMENTAR_ESTOQUE_BLOQUEADO('+ #13 +
    '      vEmpresaId,'+ #13 +
    '      :new.PRODUTO_ID,'+ #13 +
    '      :new.LOCAL_ID,'+ #13 +
    '      :new.LOTE,'+ #13 +
    '      :new.SALDO - :old.SALDO'+ #13 +
    '    );'+ #13 +
    #13 +
    '  end if;'+ #13 +
    #13 +
    'end BLOQUEIOS_ESTOQUES_ITENS_IU_BR;'+ #13
end;

function AjustarUniqueUN_BLOQUEIOS_EST_ITENS_PRO_LOT: string;
begin
  Result :=
    'alter table BLOQUEIOS_ESTOQUES_ITENS'+ #13 +
    'drop constraint UN_BLOQUEIOS_EST_ITENS_PRO_LOT;'+ #13 +
    #13 +
    'drop index UN_BLOQUEIOS_EST_ITENS_PRO_LOT;'+ #13 +
    #13 +
    'alter table BLOQUEIOS_ESTOQUES_ITENS'+ #13 +
    'add constraint UN_BLOQUEIOS_EST_ITENS_PRO_LOT'+ #13 +
    'unique(BLOQUEIO_ID, PRODUTO_ID, LOTE)'+ #13 +
    'using index tablespace INDICES;'+ #13
end;

function AjustarTriggerDEVOLUCOES_ITENS_D_BR: string;
begin
  Result :=
    'create or replace trigger DEVOLUCOES_ITENS_D_BR'+ #13 +
    'before delete'+ #13 +
    'on DEVOLUCOES_ITENS'+ #13 +
    'for each row'+ #13 +
    'declare'+ #13 +
    #13 +
    '  vOrcamentoId     ORCAMENTOS.ORCAMENTO_ID%type;'+ #13 +
    '  vQtdeUtilizar    number default 0;'+ #13 +
    '  vQtdeRestante    number;'+ #13 +
    #13 +
    '  cursor cPendencias(pORCAMENTO_ID in number) is'+ #13 +
    '  select'+ #13 +
    '    ''R'' as TIPO,'+ #13 +
    '    EMPRESA_ID,'+ #13 +
    '    LOCAL_ID,'+ #13 +
    '    ITEM_ID,'+ #13 +
    '    LOTE,'+ #13 +
    '    DEVOLVIDOS as SALDO,'+ #13 +
    '    null as PREVISAO_ENTREGA'+ #13 +
    '  from'+ #13 +
    '    RETIRADAS_ITENS_PENDENTES'+ #13 +
    '  where ORCAMENTO_ID = pORCAMENTO_ID  '+ #13 +
    '  and ITEM_ID = :old.ITEM_ID'+ #13 +
    #13 +
    '  union all'+ #13 +
    #13 +
    '  select'+ #13 +
    '    ''E'' as TIPO,'+ #13 +
    '    EMPRESA_ID,'+ #13 +
    '    LOCAL_ID,'+ #13 +
    '    ITEM_ID,'+ #13 +
    '    LOTE,'+ #13 +
    '    DEVOLVIDOS as SALDO,'+ #13 +
    '    PREVISAO_ENTREGA'+ #13 +
    '  from'+ #13 +
    '    ENTREGAS_ITENS_PENDENTES'+ #13 +
    '  where ORCAMENTO_ID = pORCAMENTO_ID'+ #13 +
    '  and ITEM_ID = :old.ITEM_ID;'+ #13 +
    #13 +
    'begin'+ #13 +
    #13 +
    '  select'+ #13 +
    '    ORCAMENTO_ID'+ #13 +
    '  into'+ #13 +
    '    vOrcamentoId'+ #13 +
    '  from'+ #13 +
    '    DEVOLUCOES'+ #13 +
    '  where DEVOLUCAO_ID = :old.DEVOLUCAO_ID;'+ #13 +
    #13 +
    '  /* Voltando as pend�ncias (Retirar e entregar) */'+ #13 +
    '  if :old.DEVOLVIDOS_PENDENCIAS > 0 then'+ #13 +
    '    vQtdeRestante := :old.DEVOLVIDOS_PENDENCIAS;'+ #13 +
    '    for xPend in cPendencias(vOrcamentoId) loop'+ #13 +
    '      if xPend.SALDO >= vQtdeRestante then'+ #13 +
    '        vQtdeUtilizar := vQtdeRestante;'+ #13 +
    '        vQtdeRestante := 0;'+ #13 +
    '      else'+ #13 +
    '        vQtdeUtilizar := xPend.SALDO;'+ #13 +
    '        vQtdeRestante := vQtdeRestante - vQtdeUtilizar;'+ #13 +
    '      end if;'+ #13 +
    #13 +
    '      if xPend.TIPO = ''R'' then'+ #13 +
    '        update RETIRADAS_ITENS_PENDENTES set'+ #13 +
    '          DEVOLVIDOS = DEVOLVIDOS - vQtdeUtilizar'+ #13 +
    '        where ORCAMENTO_ID = vOrcamentoId'+ #13 +
    '        and EMPRESA_ID = xPend.EMPRESA_ID'+ #13 +
    '        and LOCAL_ID = xPend.LOCAL_ID'+ #13 +
    '        and ITEM_ID = xPend.ITEM_ID'+ #13 +
    '        and LOTE = xPend.LOTE;'+ #13 +
    '      else'+ #13 +
    '        update ENTREGAS_ITENS_PENDENTES set'+ #13 +
    '          DEVOLVIDOS = DEVOLVIDOS - vQtdeUtilizar'+ #13 +
    '        where ORCAMENTO_ID = vOrcamentoId'+ #13 +
    '        and EMPRESA_ID = xPend.EMPRESA_ID'+ #13 +
    '        and LOCAL_ID = xPend.LOCAL_ID'+ #13 +
    '        and ITEM_ID = xPend.ITEM_ID'+ #13 +
    '        and LOTE = xPend.LOTE'+ #13 +
    '        and PREVISAO_ENTREGA = xPend.PREVISAO_ENTREGA;'+ #13 +
    '      end if;'+ #13 +
    #13 +
    '      /* Se n�o tem mais nada a devolver, saindo do loop */'+ #13 +
    '      if vQtdeRestante = 0 then'+ #13 +
    '        exit;'+ #13 +
    '      end if;'+ #13 +
    #13 +
    '    end loop;'+ #13 +
    #13 +
    '    if vQtdeRestante > 0 then'+ #13 +
    '      ERRO(''N�o existe saldo suficiente para realizar a devolu��o, verifique as pend�ncias!'');'+ #13 +
    '    end if;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  /* Devolvendo os itens sem previs�o */'+ #13 +
    '  if :old.DEVOLVIDOS_SEM_PREVISAO > 0 then'+ #13 +
    '    update ENTREGAS_ITENS_SEM_PREVISAO set'+ #13 +
    '      DEVOLVIDOS = DEVOLVIDOS - :old.DEVOLVIDOS_SEM_PREVISAO'+ #13 +
    '    where ORCAMENTO_ID = vOrcamentoId'+ #13 +
    '    and ITEM_ID = :old.ITEM_ID;'+ #13 +
    '  end if;'+ #13 +
    #13 +
    'end DEVOLUCOES_ITENS_D_BR;'+ #13
end;

function AjustarViewVW_ORCAMENTOS_IMPRESSAO: string;
begin
  Result :=
    'create or replace view VW_ORCAMENTOS_IMPRESSAO'+ #13 +
    'as'+ #13 +
    'select'+ #13 +
    '  ORC.EMPRESA_ID,'+ #13 +
    '  ORC.ORCAMENTO_ID,'+ #13 +
    '  ORC.CLIENTE_ID,'+ #13 +
    '  nvl(ORC.NOME_CONSUMIDOR_FINAL, CAD.NOME_FANTASIA) as NOME_CLIENTE,'+ #13 +
    '  CAD.LOGRADOURO,'+ #13 +
    '  CAD.COMPLEMENTO,'+ #13 +
    '  CAD.NUMERO,'+ #13 +
    '  CAD.BAIRRO_ID,'+ #13 +
    '  BAI.NOME as NOME_BAIRRO,'+ #13 +
    '  CAD.CEP,'+ #13 +
    '  CID.CIDADE_ID,'+ #13 +
    '  CID.NOME as NOME_CIDADE,'+ #13 +
    '  EST.ESTADO_ID,'+ #13 +
    '  EST.NOME as NOME_ESTADO,'+ #13 +
    '  CAD.TELEFONE_PRINCIPAL,'+ #13 +
    '  CAD.TELEFONE_CELULAR,  '+ #13 +
    '  ORC.CONDICAO_ID,'+ #13 +
    '  CON.NOME as NOME_CONDICAO_PAGAMENTO,'+ #13 +
    '  ORC.VENDEDOR_ID,'+ #13 +
    '  VEN.NOME as NOME_VENDEDOR,'+ #13 +
    '  ORC.VALOR_TOTAL,'+ #13 +
    '  ORC.VALOR_TOTAL_PRODUTOS,'+ #13 +
    '  ORC.VALOR_OUTRAS_DESPESAS,'+ #13 +
    '  ORC.VALOR_FRETE,'+ #13 +
    '  ORC.VALOR_FRETE_ITENS,'+ #13 +
    '  ORC.VALOR_DESCONTO,'+ #13 +
    '  ORC.VALOR_DINHEIRO,'+ #13 +
    '  ORC.VALOR_CHEQUE,'+ #13 +
    '  ORC.VALOR_CARTAO_DEBITO,'+ #13 +
    '  ORC.VALOR_CARTAO_CREDITO,'+ #13 +
    '  ORC.VALOR_COBRANCA,'+ #13 +
    '  ORC.VALOR_CREDITO,'+ #13 +
    '  ORC.VALOR_TOTAL_PRODUTOS_PROMOCAO,'+ #13 +
    '  ORC.STATUS'+ #13 +
    'from '+ #13 +
    '  ORCAMENTOS ORC'+ #13 +
    #13 +
    'inner join CLIENTES CLI'+ #13 +
    'on ORC.CLIENTE_ID = CLI.CADASTRO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on CLI.CADASTRO_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join BAIRROS BAI'+ #13 +
    'on CAD.BAIRRO_ID = BAI.BAIRRO_ID'+ #13 +
    #13 +
    'inner join CIDADES CID'+ #13 +
    'on BAI.CIDADE_ID =  CID.CIDADE_ID'+ #13 +
    #13 +
    'inner join ESTADOS EST'+ #13 +
    'on CID.ESTADO_ID = EST.ESTADO_ID'+ #13 +
    #13 +
    'inner join CONDICOES_PAGAMENTO CON'+ #13 +
    'on ORC.CONDICAO_ID = CON.CONDICAO_ID'+ #13 +
    #13 +
    'inner join FUNCIONARIOS VEN'+ #13 +
    'on ORC.VENDEDOR_ID = VEN.FUNCIONARIO_ID'+ #13 +
    #13 +
    'with read only;'+ #13
end;

function AjustarViewVW_ENTREGAS_PENDENTES: string;
begin
  Result :=
    'create or replace view VW_ENTREGAS_PENDENTES'+ #13 +
    'as'+ #13 +
    'select'+ #13 +
    '  ''R'' as TIPO_MOVIMENTO,'+ #13 +
    '  RET.ORCAMENTO_ID,'+ #13 +
    '  ORC.CLIENTE_ID,'+ #13 +
    '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
    '  RET.EMPRESA_ID,'+ #13 +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA,'+ #13 +
    '  RET.LOCAL_ID,'+ #13 +
    '  LOC.NOME as NOME_LOCAL,'+ #13 +
    '  ORC.VENDEDOR_ID,'+ #13 +
    '  FVE.NOME as NOME_VENDEDOR,'+ #13 +
    '  RET.DATA_HORA_CADASTRO,'+ #13 +
    '  trunc(sysdate) as PREVISAO_ENTREGA,'+ #13 +
    '  ORC.DATA_HORA_RECEBIMENTO'+ #13 +
    'from'+ #13 +
    '  RETIRADAS_PENDENTES RET'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join EMPRESAS EMP'+ #13 +
    'on RET.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
    #13 +
    'inner join LOCAIS_PRODUTOS LOC'+ #13 +
    'on RET.LOCAL_ID = LOC.LOCAL_ID'+ #13 +
    #13 +
    'left join FUNCIONARIOS FVE'+ #13 +
    'on ORC.VENDEDOR_ID = FVE.FUNCIONARIO_ID'+ #13 +
    #13 +
    'where (RET.EMPRESA_ID, RET.ORCAMENTO_ID, RET.LOCAL_ID) in('+ #13 +
    '  select'+ #13 +
    '    EMPRESA_ID,'+ #13 +
    '    ORCAMENTO_ID,'+ #13 +
    '    LOCAL_ID'+ #13 +
    '  from'+ #13 +
    '    RETIRADAS_ITENS_PENDENTES'+ #13 +
    '  where SALDO > 0'+ #13 +
    ')'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    'select'+ #13 +
    '  ''E'' as TIPO_MOVIMENTO,'+ #13 +
    '  ENT.ORCAMENTO_ID,'+ #13 +
    '  ORC.CLIENTE_ID,'+ #13 +
    '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
    '  ENT.EMPRESA_ID,'+ #13 +
    '  EMP.NOME_FANTASIA as NOME_EMPRESA,'+ #13 +
    '  ENT.LOCAL_ID,'+ #13 +
    '  LOC.NOME as NOME_LOCAL,'+ #13 +
    '  ORC.VENDEDOR_ID,'+ #13 +
    '  FVE.NOME as NOME_VENDEDOR,'+ #13 +
    '  ENT.DATA_HORA_CADASTRO,'+ #13 +
    '  ENT.PREVISAO_ENTREGA,'+ #13 +
    '  ORC.DATA_HORA_RECEBIMENTO'+ #13 +
    'from'+ #13 +
    '  ENTREGAS_PENDENTES ENT'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'inner join EMPRESAS EMP'+ #13 +
    'on ENT.EMPRESA_ID = EMP.EMPRESA_ID'+ #13 +
    #13 +
    'inner join LOCAIS_PRODUTOS LOC'+ #13 +
    'on ENT.LOCAL_ID = LOC.LOCAL_ID  '+ #13 +
    #13 +
    'left join FUNCIONARIOS FVE'+ #13 +
    'on ORC.VENDEDOR_ID = FVE.FUNCIONARIO_ID'+ #13 +
    #13 +
    'where (ENT.EMPRESA_ID, ENT.ORCAMENTO_ID, ENT.PREVISAO_ENTREGA, ENT.LOCAL_ID) in('+ #13 +
    '  select'+ #13 +
    '    EMPRESA_ID,'+ #13 +
    '    ORCAMENTO_ID,'+ #13 +
    '    PREVISAO_ENTREGA,'+ #13 +
    '    LOCAL_ID'+ #13 +
    '  from'+ #13 +
    '    ENTREGAS_ITENS_PENDENTES'+ #13 +
    '  where SALDO > 0'+ #13 +
    ')'+ #13 +
    #13 +
    'union all'+ #13 +
    #13 +
    'select distinct'+ #13 +
    '  ''S'' as TIPO_MOVIMENTO,'+ #13 +
    '  SPE.ORCAMENTO_ID,'+ #13 +
    '  ORC.CLIENTE_ID,'+ #13 +
    '  CAD.NOME_FANTASIA as NOME_CLIENTE,'+ #13 +
    '  null as EMPRESA_ID,'+ #13 +
    '  null as NOME_EMPRESA,'+ #13 +
    '  null as LOCAL_ID,'+ #13 +
    '  null as NOME_LOCAL,'+ #13 +
    '  ORC.VENDEDOR_ID,'+ #13 +
    '  FVE.NOME as NOME_VENDEDOR,'+ #13 +
    '  null as DATA_HORA_CADASTRO,'+ #13 +
    '  trunc(sysdate) as PREVISAO_ENTREGA,'+ #13 +
    '  ORC.DATA_HORA_RECEBIMENTO'+ #13 +
    'from'+ #13 +
    '  ENTREGAS_ITENS_SEM_PREVISAO SPE'+ #13 +
    #13 +
    'inner join ORCAMENTOS ORC'+ #13 +
    'on SPE.ORCAMENTO_ID = ORC.ORCAMENTO_ID'+ #13 +
    #13 +
    'inner join CADASTROS CAD'+ #13 +
    'on ORC.CLIENTE_ID = CAD.CADASTRO_ID'+ #13 +
    #13 +
    'left join FUNCIONARIOS FVE'+ #13 +
    'on ORC.VENDEDOR_ID = FVE.FUNCIONARIO_ID'+ #13 +
    #13 +
    'where SPE.SALDO > 0;'+ #13
end;

function AjustarProcedureAGENDAR_ITENS_SEM_PREVISAO: string;
begin
  Result :=
    'create or replace procedure AGENDAR_ITENS_SEM_PREVISAO('+ #13 +
    '  iORCAMENTO_ID       in number,'+ #13 +
    '  iEMPRESA_GERACAO_ID in number,'+ #13 +
    '  iPREVISAO_ENTREGA   in date,'+ #13 +
    '  iVetPRODUTOS_IDS    in TIPOS.ArrayOfNumeros,'+ #13 +
    '  iVetITENS_IDS       in TIPOS.ArrayOfNumeros,'+ #13 +
    '  iVetQUANTIDADES     in TIPOS.ArrayOfNumeros'+ #13 +
    ')'+ #13 +
    'is'+ #13 +
    '  i       number;'+ #13 +
    '  vStatus ORCAMENTOS.STATUS%type;'+ #13 +
    'begin'+ #13 +
    '  select'+ #13 +
    '    STATUS'+ #13 +
    '  into'+ #13 +
    '    vStatus'+ #13 +
    '  from'+ #13 +
    '    ORCAMENTOS'+ #13 +
    '  where ORCAMENTO_ID = iORCAMENTO_ID;'+ #13 +
    #13 +
    '  if vStatus not in(''VE'', ''RE'') then'+ #13 +
    '    ERRO(''O or�amento ainda n�o foi recebido, verifique!'');'+ #13 +
    '  end if;'+ #13 +
    #13 +
    '  for i in iVetPRODUTOS_IDS.first..iVetPRODUTOS_IDS.last loop'+ #13 +
    '    update ENTREGAS_ITENS_SEM_PREVISAO set'+ #13 +
    '      ENTREGUES = ENTREGUES + iVetQUANTIDADES(i)'+ #13 +
    '    where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
    '    and PRODUTO_ID = iVetPRODUTOS_IDS(i)'+ #13 +
    '    and ITEM_ID = iVetITENS_IDS(i);'+ #13 +
    #13 +
    '    insert into ENTREGAS_ITENS_A_GERAR_TEMP('+ #13 +
    '      ORCAMENTO_ID,'+ #13 +
    '      PREVISAO_ENTREGA,'+ #13 +
    '      PRODUTO_ID,'+ #13 +
    '      ITEM_ID,'+ #13 +
    '      QUANTIDADE,'+ #13 +
    '      TIPO_ENTREGA'+ #13 +
    '    )values('+ #13 +
    '      iORCAMENTO_ID,'+ #13 +
    '      iPREVISAO_ENTREGA,'+ #13 +
    '      iVetPRODUTOS_IDS(i),'+ #13 +
    '      iVetITENS_IDS(i),'+ #13 +
    '      iVetQUANTIDADES(i),'+ #13 +
    '      ''EN'''+ #13 +
    '    );'+ #13 +
    '  end loop;'+ #13 +
    #13 +
    '  GERAR_PENDENCIA_ENTREGAS(iORCAMENTO_ID, iEMPRESA_GERACAO_ID);'+ #13 +
    #13 +
    '  /* Removendo as pend�ncia j� que n�o existem mais */'+ #13 +
    '  delete from ENTREGAS_ITENS_SEM_PREVISAO'+ #13 +
    '  where ORCAMENTO_ID = iORCAMENTO_ID'+ #13 +
    '  and SALDO = 0;'+ #13 +
    #13 +
    'end AGENDAR_ITENS_SEM_PREVISAO;'+ #13
end;

function AdicionarColunasAltisW: string;
begin
  Result :=
    'alter table FUNCIONARIOS disable all triggers;'+ #13 +
    #13 +
    'alter table FUNCIONARIOS'+ #13 +
    'add ('+ #13 +
    '  SENHA_ALTIS_W                  varchar2(50) default ''123456'' not null,'+ #13 +
    '  ACESSA_ALTIS_W                 char(1) default ''N'' not null'+ #13 +
    ');'+ #13 +
    #13 +
    'alter table FUNCIONARIOS enable all triggers;'+ #13 +
    #13 +
    'alter table FUNCIONARIOS'+ #13 +
    'add constraint CK_FUNCIONARIOS_ACESSA_ALTIS_W'+ #13 +
    'check(ACESSA_ALTIS_W in(''S'', ''N''));'+ #13
end;

function AdicionarColunarValorPixOrcamento: string;
begin
  Result := 'ALTER TABLE ORCAMENTOS ADD VALOR_PIX number(8,2) default 0 not null';
end;

end.
