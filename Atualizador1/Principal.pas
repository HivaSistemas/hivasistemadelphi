unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids,
  Vcl.StdCtrls, MemDS, Xml.XMLDoc, Xml.XMLIntf, Vcl.Buttons, DAScript, OraScript, OraCall, Data.DB, DBAccess, Ora, _Scripts1, OraErrHand,
  System.UITypes, _Scripts2, Vcl.ComCtrls;

type
  TFormComandos = class(TForm)
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    sbConectar: TSpeedButton;
    sgComandos: TStringGrid;
    eLogs: TMemo;
    edtUsuario: TEdit;
    edtSenha: TEdit;
    edtServidor: TEdit;
    edtPorta: TEdit;
    edtServico: TEdit;
    Conexao: TOraSession;
    Script: TOraScript;
    sbExecutarScriptSelecionado: TSpeedButton;
    sbExecutarScriptApartirLinha: TSpeedButton;
    OraErrorHandler1: TOraErrorHandler;
    Query: TOraQuery;
    sbAlterarVersaoBD: TSpeedButton;
    pbProgresso: TProgressBar;
    procedure sbConectarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sbExecutarScriptSelecionadoClick(Sender: TObject);
    procedure OraErrorHandler1Error(Sender: TObject; E: Exception; ErrorCode: Integer; const ConstraintName: string; var Msg: string);
    procedure sbExecutarScriptApartirLinhaClick(Sender: TObject);
    procedure sbAlterarVersaoBDClick(Sender: TObject);

  private
    FScripts: TArray<string>;
    FExecutandoValidacoesObjetos: Boolean;

    procedure addScript(pScript: string);
    procedure addLog(pLog: string);
    function ValidarObjetos: Boolean;
  public
    { Public declarations }
  end;

var
  FormComandos: TFormComandos;

implementation

{$R *.dfm}

const
  coLinha  = 0;
  coScript = 1;

procedure TFormComandos.addLog(pLog: string);
begin
  eLogs.Lines.Add('');
  eLogs.Lines.Add(pLog);
end;

procedure TFormComandos.addScript(pScript: string);
begin
  SetLength(FScripts, Length(FScripts) + 1);
  FScripts[High(FScripts)] := pScript;
end;

procedure TFormComandos.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  FScripts := nil;

  addScript(_Scripts1.CriarTabelaADMINISTRADORAS_CARTOES);
  addScript(_Scripts1.CriarTriggerADMINISTRADORAS_CARTOES_IU_BR);
  addScript(_Scripts1.CriarSequenceSEQ_ADMINISTRADORAS_CARTOES);
  addScript(_Scripts1.CriarColunaADMINISTRADORA_CARTAO_ID);
  addScript(_Scripts1.CriarFK_TP_COB_ADMIN_CARTAO_ID);
  addScript(_Scripts1.AjustarTriggerTIPOS_COBRANCA_U_AR);
  addScript(_Scripts1.CriarColunaCODIGO_AUTORIZACAO);
  addScript(_Scripts1.AdicionarVariasColunasGERENCIADORES_CARTOES_TEF);
  addScript(_Scripts1.AdicionarColunaNUMERO_CARTAOTabelaORCAMENTOS_PAGAMENTOS);
  addScript(_Scripts1.AjustarProcedureGERAR_CARTOES_RECEBER);
  addScript(_Scripts1.AjustarProcedureRECEBER_PEDIDO);
  addScript(_Scripts1.AddColunasCONTAS_REC_BAIXAS_PAGAMENTOS);
  addScript(_Scripts1.AjustarProcGERAR_CARTOES_RECEBER_BAIXA);
  addScript(_Scripts1.AjustarProcCONSOLIDAR_BAIXA_CONTAS_REC);
  addScript(_Scripts1.AddColunaTabelaACUMULADOS_PAGAMENTOS);
  addScript(_Scripts1.AjustarProcGERAR_CARTOES_REC_ACUMULADO);
  addScript(_Scripts1.AjustarProcRECEBER_ACUMULADO);
  addScript(_Scripts1.AdicionarColunaTIPO_RECEB_CARTAOTabelaCONTAS_REC_BAIXAS_PAGAMENTOS);
  addScript(_Scripts1.AdicionarColunaTIPO_RECEB_CARTAOTabelaORCAMENTOS_PAGAMENTOS);
  addScript(_Scripts1.AdicionarColunaTIPO_RECEB_CARTAOTabelaACUMULADOS_PAGAMENTOS);
  addScript(_Scripts1.AlimentarTIPO_RECEB_CARTAOTabelaCONTAS_REC_BAIXAS_PAGAMENTOS);
  addScript(_Scripts1.AlimentarTIPO_RECEB_CARTAOTabelaORCAMENTOS_PAGAMENTOS);
  addScript(_Scripts1.AlimentarTIPO_RECEB_CARTAOTabelaACUMULADOS_PAGAMENTOS);
  addScript(_Scripts1.AdicionarColunaBANDEIRA_CARTAO_NFE);
  addScript(_Scripts1.AlimentarColunaBANDEIRA_CARTAO_NFE);
  addScript(_Scripts1.AjustarConstraintCK_TP_COBRANCA_FORMA_PAGTO_CRT);
  addScript(_Scripts1.AddColunaQTDE_SEG_TRAVAR_ALTIS_OCIOSO);
  addScript(_Scripts1.AddConstraintCK_PAR_QTDE_SEG_TR_ALTIS_OCI);
  addScript(_Scripts1.AjustarProcADD_NO_VETOR_SEM_REPETIR);
  addScript(_Scripts1.AjustarProcGERAR_NOTA_DEV_RETIRADA);
  addScript(_Scripts1.AjustarProcGERAR_NOTA_DEVOLUCAO_ACUMULADO);
  addScript(_Scripts1.AjustarProcGERAR_NOTA_DEVOLUCAO_ENTREGA);
  addScript(_Scripts1.AjustarProcGERAR_NOTA_RETORNO_ENTREGA);
  addScript(_Scripts1.AjustarConstraintCK_CFOPS_CST_OPER_TIPO_MOV);
  addScript(_Scripts1.AjustarConstraintCK_NOTAS_FISCAIS_TIPO_MOVIMENT);
  addScript(_Scripts1.AjustarFunctionBUSCAR_CFOP_ID_OPERACAO);
  addScript(_Scripts1.AjustarProcedureBAIXAR_ENTREGA);
  addScript(_Scripts1.AjustarTriggerORCAMENTOS_U_AR);
  addScript(_Scripts1.AjustarViewVW_TIPOS_ALTER_LOGS_ORCAMENTOS);
  addScript(_Scripts1.DroparColunaECF_ID);
  addScript(_Scripts1.AjustarPackageRecordsNotasFiscais);
  addScript(_Scripts1.DroparProcedureINSERIR_NOTA_FISCAL);
  addScript(_Scripts1.SubstituirTriggerProdutos);
  addScript(_Scripts1.AJustarLogsProdutos);
  addScript(_Scripts1.AjusteMotivoInatividade);
  addScript(_Scripts1.AjustarTriggerAJUSTES_ESTOQUE_ITENS_IU_BR);
  addScript(_Scripts1.AdicionarColunaNUMERO_ESTABELECIMENTO_CIELO);
  addScript(_Scripts1.AjustarViewVW_TIPOS_ALTER_LOGS_CT_RECEBER);
  addScript(_Scripts1.AjustarTriggerCONTAS_RECEBER_U_AR);
  addScript(_Scripts1.AjustarProcedureATUALIZAR_RETENCOES_CONTAS_REC);
  addScript(_Scripts1.DroparConstraintFK_CONTAS_REC_ITEM_ID_CRT_ORC);
  addScript(_Scripts1.AlterarCampoCADASTRO_IDTabelaCONTAS_RECEBER_BAIXAS);
  addScript(_Scripts1.AjustarConstraintCK_CONTAS_RECEBER_BAIXAS_TIPO);
  addScript(_Scripts1.AjustarConstraintCK_CONTAS_REC_BX_TIPO_CAD_ID);
  addScript(_Scripts1.AjustarTriggerCONTAS_REC_BX_PAGTOS_DIN_IU_BR);
  addScript(_Scripts1.AjustarTriggerCONTAS_PAGAR_BX_PAG_DIN_IU_BR);
  addScript(_Scripts1.AjustarProcedureATUALIZAR_CUSTOS_ENTRADA);
  addScript(_Scripts1.AjustarProcedureVOLTAR_CUSTOS_ENTRADA);
  addScript(_Scripts1.Atualizando_VW_ENTREGAS);
  addScript(_Scripts1.Atualizando_VW_RELACAO_VENDAS_PRODUTOS);
  addScript(_Scripts1.Criando_Coluna_QTDE_MAXIMA_DIAS_DEVOLUCAO_Tabela_PARAMETROS);
  addScript(_Scripts1.Criando_CK_PAR_QTDE_MAX_DIAS_DEVOLUCAO);
  addScript(_Scripts1.DroparColunaESTACAO_EMISSAO_NOTA);
  addScript(_Scripts1.AjustarTriggerNOTAS_FISCAIS_IU_BR);
  addScript(_Scripts1.AjustarTriggerNOTAS_FISCAIS_U_AR);
  addScript(_Scripts1.LimparLogsNOTAS_FISCAIS_TIPO9);
  addScript(_Scripts1.AdicionarNovasColunasEnderecoEntregaNOTAS_FISCAIS);
  addScript(_Scripts1.AtualizarPackageRecordsNotasFiscais);
  addScript(_Scripts1.AjustarFunctionBUSCAR_ENDERECO_EMISSAO_NF);
  addScript(_Scripts1.AjustarProcedureGERAR_NOTA_ENTREGA);
  addScript(_Scripts1.AjustandoColunasTabelaCONHECIMENTOS_FRETES);
  addScript(_Scripts1.AjustarTriggerCONHECIMENTOS_FRETES_IU_BR);
  addScript(_Scripts1.CriarNovasColunasMOVIMENTOS_CONTAS);
  addScript(_Scripts1.AjustarConstraintCK_MOV_CONTAS_CONCILIADO);
  addScript(_Scripts1.CriarColunaSALDO_CONCILIADO_ATUAL);
  addScript(_Scripts1.AjustarProcedureMOVIMENTAR_SALDO_CONTA);
  addScript(_Scripts1.AjustarTriggerTURNOS_CAIXA_IU_BR);
  addScript(_Scripts1.AjustarTriggerMOVIMENTOS_TURNOS_IU_BR);
  addScript(_Scripts1.AjustarTriggerMOVIMENTOS_CONTAS_IU_BR);
  addScript(_Scripts1.AjustarColunaCONTA_IDVariasTabelas);
  addScript(_Scripts1.AjustarViewVW_MOVIMENTOS_CONTAS);
  addScript(_Scripts1.AjustarDataMovimentosMOVIMENTOS_CONTAS);
  addScript(_Scripts1.AjustarSaldoContas);
  addScript(_Scripts1.CriarTabelaLISTA_NEGRA);
  addScript(_Scripts1.CriarTriggerLISTA_NEGRA_IU_BR);
  addScript(_Scripts1.CriarSequenceSEQ_LISTA_NEGRA);
  addScript(_Scripts1.CriarNovasColunasCLIENTES);
  addScript(_Scripts1.AdicionarColunaVALOR_ADIANTADOTabelaCONTAS_RECEBER);
  addScript(_Scripts1.AdicionarColunaTIPO_COB_ADIANTAMENTO_FIN_ID);
  addScript(_Scripts1.AjustarTriggerTIPOS_COBRANCA_IU_BR);
  addScript(_Scripts1.AdicionarColunaRECEBER_ADIANTADO_ID);
  addScript(_Scripts1.AjustarConstraintCK_CONTAS_RECEBER_BAIXAS_TIPO2);
  addScript(_Scripts1.AjustarConstraintCK_CONTAS_REC_BX_TIPO_CAD_ID2);
  addScript(_Scripts1.AdicionarColunaVALOR_ADIANTADOTabelaCONTAS_RECEBER_BAIXAS);
  addScript(_Scripts1.AdicionarCK_CONTAS_REC_BX_VALOR_ADIANT);
  addScript(_Scripts1.AjustarCK_CONTAS_REC_BX_VALOR_LIQ);
  addScript(_Scripts1.AjustarTriggerCONTAS_RECEBER_BAIXAS_IU_BR);
  addScript(_Scripts1.AjusteVwVidaProdutoFisico);
  addScript(_Scripts1.AjusteVwVidaProdutoDisponivel);
  addScript(_Scripts1.AjusteVwVidaProdutoFiscal);
  addScript(_Scripts1.CriarTabelaCONTAS_CUSTODIA);
  addScript(_Scripts1.CriarTriggerCONTAS_CUSTODIA_IU_BR);
  addScript(_Scripts1.CriarSequenceSEQ_CONTAS_CUSTODIA);
  addScript(_Scripts1.CriarNovasColunasCONTAS_RECEBER);
  addScript(_Scripts1.AddFK_CONTAS_REC_CON_CUSTODIA_ID);
  addScript(_Scripts1.AjustarTriggerCONTAS_RECEBER_U_AR2);
  addScript(_Scripts1.AjustarViewVW_TIPOS_ALTER_LOGS_CT_RECEBER2);
  addScript(_Scripts1.AjustarViewVW_ESTOQUES_DIVISAO);
  addScript(_Scripts1.AddColunaEMITIR_DUPLIC_MERCANTIL_VENDA);
  addScript(_Scripts1.AjustarTriggerTIPOS_COBRANCA_U_AR2);
  addScript(_Scripts1.AjustarViewVW_TIPOS_ALTER_LOGS_TP_COBRANC2);
  addScript(_Scripts1.AjustarProcedureSETAR_NFE_EMITIDA);
  addScript(_Scripts1.AjustarProcGERAR_ENTR_TRANS_PRODUTOS_EMP);
  addScript(_Scripts1.CriarTabelaCONTAS_REC_HISTORICO_COBRANCAS);
  addScript(_Scripts1.AjustarTriggerCONTAS_REC_HISTORICO_COB_IU_BR);
  addScript(_Scripts1.AjustarViewVW_VIDA_PRODUTO_DISPONIVEL);
  addScript(_Scripts1.AjustarViewVW_VIDA_PRODUTO_FISCAL);
  addScript(_Scripts1.AjustarViewVW_VIDA_PRODUTO_FISICO);
  addScript(_Scripts1.AjustarViewVW_MOV_PEND_EMISSAO_NF_TRANSF);
  addScript(_Scripts1.AjustarViewVW_MOV_ITE_PEND_EMISSAO_NF_TR);
  addScript(_Scripts1.AjustarProcGERAR_NOTA_TRANSF_PROD_EMPR);
  addScript(_Scripts1.AjustarProcGERAR_NOTA_TRANSF_MOVIMENTOS);
  addScript(_Scripts1.AjustarConstraintCK_CFOPS_CST_OPER_TIPO_MOV2);
  addScript(_Scripts1.AjustarFunctionBUSCAR_CFOP_ID_OPERACAO2);
  addScript(_Scripts1.AjustarConstraintCK_PAR_EMP_TIPO_REDIREC_NOTA);
  addScript(_Scripts1.AjustarConstraintCK_NOTAS_FISCAIS_TIPO_MOVIMENT2);
  addScript(_Scripts1.DroparColunasTabelaNOTAS_FISCAIS);
  addScript(_Scripts1.CriarTabelaCONTROLES_ENTREGAS_AJUDANTES);
  addScript(_Scripts1.AjustarTriggerCONTROLES_ENTREGAS_AJUD_IU_BR);
  addScript(_Scripts1.AjustarTriggerCONTROLES_ENTREGAS_D_BR);
  addScript(_Scripts1.CriarColunaAJUDANTETabelaFUNCIONARIOS);
  addScript(_Scripts1.CriarColunaQTDE_DIAS_BLOQ_VENDA_TIT_VENC);
  addScript(_Scripts1.CriarColunaTIPO_COB_FECHAMENTO_TURNO_ID);
  addScript(_Scripts1.AjustarConstraintCK_CONTAS_RECEBER_ORIGEM);
  addScript(_Scripts1.AjustarProcedureLANCAR_DIFERENCA_FECH_CAIXA);
  addScript(_Scripts1.AjustarTriggerCLIENTES_IU_BR);
  addScript(_Scripts1.AjustarTriggerCADASTROS_IU_BR);
  addScript(_Scripts1.AdicionarColunaVALOR_LIQUIDOTabelaCompras);
  addScript(_Scripts1.AdicionarColunaIGNORAR_BLOQUEIOS_VENDATabelaClientes);
  addScript(_Scripts1.AjustarConstraintCK_CLIENTES_IGNORAR_BLOQ_VENDA);
  addScript(_Scripts1.AdicionarColunaATIVOTabelaCLIENTES_GRUPOS);
  addScript(_Scripts1.AdicionarConstraintCK_CLIENTES_GRUPOS_ATIVO);
  addScript(_Scripts1.AdicionarSequenceSEQ_BLOQUEIOS_ESTOQUES);
  addScript(_Scripts1.AjustarViewVW_RELACAO_ORCAMENTOS_VENDAS);
  addScript(_Scripts1.AjustarViewVW_ESTOQUES);
  addScript(_Scripts1.AjustarViewVW_ESTOQUES_DIVISAO2);
  addScript(_Scripts1.AjustarProcedureGERAR_CREDITO_DEVOLUCAO);
  addScript(_Scripts1.AjustarViewVW_BOLETOS_RECEBER);
  addScript(_Scripts1.AjustarViewVERIFICAR_BLOQUEIOS_ORCAMENTOS);
  addScript(_Scripts1.AjustarTabelaBLOQUEIOS_ESTOQUES_ITENS);
  addScript(_Scripts1.CriarNovasConstraintsBLOQUEIOS_ESTOQUES_ITENS);
  addScript(_Scripts1.CriarTabelaBLOQUEIOS_ESTOQUES_BAIXAS);
  addScript(_Scripts1.CriarTriggerBLOQ_ESTOQUES_BAIXAS_IU_BR);
  addScript(_Scripts1.CriarTabelaBLOQUEIOS_EST_BAIXAS_ITENS);
  addScript(_Scripts1.AjustarTriggerBLOQ_ESTOQUES_BX_ITENS_IU_BR);
  addScript(_Scripts1.AjustarSequenceSEQ_BLOQUEIOS_ESTOQUES_BAIXAS);
  addScript(_Scripts1.AjustarProcedureCONSOLIDAR_BX_BLOQUEIO_ESTOQUE);
  addScript(_Scripts1.AjustarProcedureBLOQUEIOS_ESTOQUES_ITENS_IU_BR);
  addScript(_Scripts1.AjustarUniqueUN_BLOQUEIOS_EST_ITENS_PRO_LOT);
  addScript(_Scripts1.AjustarTriggerDEVOLUCOES_ITENS_D_BR);
  addScript(_Scripts1.AjustarViewVW_ORCAMENTOS_IMPRESSAO);
  addScript(_Scripts1.AjustarViewVW_ENTREGAS_PENDENTES);
  addScript(_Scripts1.AjustarProcedureAGENDAR_ITENS_SEM_PREVISAO);
  addScript(_Scripts1.AdicionarColunasAltisW);
  addScript(_Scripts1.AdicionarColunarValorPixOrcamento);

  for i := Low(FScripts) to High(FScripts) do begin
    sgComandos.Cells[coLinha, i + 1]  := IntToStr(i + 1);
    sgComandos.Cells[coScript, i + 1] := FScripts[i];
  end;

  sgComandos.RowCount := Length(FScripts) + 1;
end;

procedure TFormComandos.OraErrorHandler1Error(Sender: TObject; E: Exception; ErrorCode: Integer; const ConstraintName: string; var Msg: string);
begin
  if FExecutandoValidacoesObjetos then
    Exit;

  Conexao.Rollback;
  ShowMessage('Erro ao executar script');

  eLogs.Lines.Add('');
  eLogs.Lines.Add('Falha ao executar script: ');
  eLogs.Lines.Add(Script.SQL.Text);
  eLogs.Lines.Add('Motivo: ');
  eLogs.Lines.Add(e.Message);

  Abort;
end;

procedure TFormComandos.sbAlterarVersaoBDClick(Sender: TObject);
begin
  Conexao.StartTransaction;

  Script.SQL.Clear;
  Script.SQL.Add('update PARAMETROS set');
  Script.SQL.Add('  VERSAO_ATUAL = ''5.0.0.0'', ');
  Script.SQL.Add('  COMANDO_ATUAL = 51000000 ');
  Script.Execute;

  Script.SQL.Clear;
  Script.SQL.Add('purge recyclebin;');
  Script.Execute;

  Conexao.Commit;

  ShowMessage('Vers�o atualizada com sucesso!');
end;

procedure TFormComandos.sbConectarClick(Sender: TObject);
begin
  Conexao.Server := edtServidor.Text + ':' + edtPorta.Text + '/' + edtServico.Text;
  Conexao.Password := edtSenha.Text;
  Conexao.ConnectString := edtUsuario.Text + '/' + edtSenha.Text + '@' + edtServidor.Text + ':' + edtPorta.Text + '/' + edtServico.Text;

  try
    Conexao.Connect;

    Script.SQL.Clear;
    Script.SQL.Add('begin INICIAR_SESSAO(1, 1, ''ATUALIZADOR'', ''ATUALIZADOR''); end;');
    Script.Execute;
  except
    on e: Exception do begin
      ShowMessage('Falha ao conectar-se no servidor: ' + e.Message);
      Abort;
    end;
  end;

  ShowMessage('Conectado');
end;

procedure TFormComandos.sbExecutarScriptApartirLinhaClick(Sender: TObject);
var
  i: Integer;
begin
  pbProgresso.Min := 1;
  pbProgresso.Position := sgComandos.Row;
  pbProgresso.Max := Length(FScripts);

  FExecutandoValidacoesObjetos := False;

  if not Conexao.Connected then begin
    ShowMessage('Conecte ao banco de dados!');
    Exit;
  end;

  for i := sgComandos.Row -1 to High(FScripts) do begin
    addLog('Executando com sucesso! ' + sgComandos.Cells[coLinha, i + 1]);

    sgComandos.Row := i + 1;
    sgComandos.Refresh;

    Conexao.StartTransaction;

    try
      Script.SQL.Clear;
      Script.SQL.Text := FScripts[i];
      Script.Execute;

      if not ValidarObjetos then
        addLog('N�o foi poss�vel validar todos objetos!');
    except
      Conexao.Rollback;
      if MessageDlg('Deseja Continuar executando a atualizacao? ',mtinformation,[mbyes, mbno], 0) = mrOk then
        Continue
      else
        Abort;
    end;

    Conexao.Commit;
    pbProgresso.Position := i;

    addLog('Executado com sucesso! ' + sgComandos.Cells[coLinha, i + 1]);
  end;

  Script.SQL.Clear;
  Script.SQL.Add('purge recyclebin;');
  Script.Execute;

  addLog('COMANDOS EXECUTADOS COM SUCESSO.');
  ShowMessage('COMANDO EXECUTADOS COM SUCESSO.');

  if not ValidarObjetos then
    ShowMessage('N�o foi poss�vel validar todos objetos!');
end;

procedure TFormComandos.sbExecutarScriptSelecionadoClick(Sender: TObject);
begin
  FExecutandoValidacoesObjetos := False;

  if not Conexao.Connected then begin
    ShowMessage('Conecte ao banco de dados!');
    Exit;
  end;
  Conexao.StartTransaction;

  Script.SQL.Clear;
  Script.SQL.Text := FScripts[sgComandos.Row -1];
  Script.Execute;

  Script.SQL.Clear;
  Script.SQL.Add('purge recyclebin;');
  Script.Execute;

  Conexao.Commit;
  addLog('Executado com sucesso!');
end;

function TFormComandos.ValidarObjetos: Boolean;
var
  i: Integer;
  vExec: TOraQuery;
begin
  FExecutandoValidacoesObjetos := True;
  vExec := TOraQuery.Create(Conexao);
  vExec.Session := Conexao;

  Query.SQL.Clear;
  Query.SQL.Add('select');
  Query.SQL.Add('  OBJECT_TYPE,');
  Query.SQL.Add('  OBJECT_NAME');
  Query.SQL.Add('from');
  Query.SQL.Add('  USER_OBJECTS');

  Query.SQL.Add('where STATUS = ''INVALID''');

  Query.SQL.Add('order by');
  Query.SQL.Add('  OBJECT_TYPE,');
  Query.SQL.Add('  OBJECT_NAME');

  Query.Active := True;
  Result := Query.RecordCount = 0;
  if not Result then begin
    for i := 0 to 9 do begin
      Query.First;
      while not Query.eof do begin
        try
          vExec.SQL.Clear;
          vExec.SQL.Add('alter ' + Query.Fields[0].AsString + ' ' +  Query.Fields[1].AsString + ' compile');
          vExec.Execute;
          Query.Next;
        except
          Query.Next;
          Continue;
        end;
      end;
    end;
  end;
  vExec.Free;
  FExecutandoValidacoesObjetos := False;
end;

end.
