object FormComandos: TFormComandos
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Altis update vers'#227'o 5'
  ClientHeight = 383
  ClientWidth = 583
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lb1: TLabel
    Left = 1
    Top = 1
    Width = 36
    Height = 13
    Caption = 'Usu'#225'rio'
  end
  object lb2: TLabel
    Left = 106
    Top = 1
    Width = 30
    Height = 13
    Caption = 'Senha'
  end
  object lb3: TLabel
    Left = 192
    Top = 1
    Width = 40
    Height = 13
    Caption = 'Servidor'
  end
  object lb4: TLabel
    Left = 422
    Top = 1
    Width = 26
    Height = 13
    Caption = 'Porta'
  end
  object lb5: TLabel
    Left = 468
    Top = 1
    Width = 35
    Height = 13
    Caption = 'Servi'#231'o'
  end
  object sbConectar: TSpeedButton
    Left = 512
    Top = 14
    Width = 70
    Height = 22
    Caption = 'Conectar'
    OnClick = sbConectarClick
  end
  object sbExecutarScriptSelecionado: TSpeedButton
    Left = 1
    Top = 175
    Width = 176
    Height = 29
    Caption = 'Executar script selecionado'
    OnClick = sbExecutarScriptSelecionadoClick
  end
  object sbExecutarScriptApartirLinha: TSpeedButton
    Left = 179
    Top = 175
    Width = 176
    Height = 29
    Caption = 'Executar scripts apartir da linha sel.'
    OnClick = sbExecutarScriptApartirLinhaClick
  end
  object sbAlterarVersaoBD: TSpeedButton
    Left = 406
    Top = 175
    Width = 176
    Height = 29
    Caption = 'Alterar vers'#227'o do banco de dados!'
    OnClick = sbAlterarVersaoBDClick
  end
  object sgComandos: TStringGrid
    Left = 1
    Top = 38
    Width = 581
    Height = 137
    ColCount = 2
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
    TabOrder = 0
    ColWidths = (
      55
      503)
  end
  object eLogs: TMemo
    Left = 1
    Top = 222
    Width = 581
    Height = 160
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object edtUsuario: TEdit
    Left = 1
    Top = 14
    Width = 101
    Height = 21
    TabOrder = 2
    Text = 'ENCASA'
  end
  object edtSenha: TEdit
    Left = 106
    Top = 14
    Width = 82
    Height = 21
    PasswordChar = '*'
    TabOrder = 3
    Text = 'DADOS'
  end
  object edtServidor: TEdit
    Left = 192
    Top = 14
    Width = 224
    Height = 21
    TabOrder = 4
    Text = 'LOCALHOST'
  end
  object edtPorta: TEdit
    Left = 422
    Top = 14
    Width = 40
    Height = 21
    NumbersOnly = True
    TabOrder = 5
    Text = '1521'
  end
  object edtServico: TEdit
    Left = 468
    Top = 14
    Width = 42
    Height = 21
    TabOrder = 6
    Text = 'ORCL'
  end
  object pbProgresso: TProgressBar
    Left = 1
    Top = 204
    Width = 581
    Height = 17
    TabOrder = 7
  end
  object Conexao: TOraSession
    Options.Direct = True
    Username = 'ENCASA'
    Server = 'LOCALHOST:1521'
    Connected = True
    Left = 400
    Top = 297
    EncryptedPassword = 'BBFFBEFFBBFFB0FFACFF'
  end
  object Script: TOraScript
    Session = Conexao
    Left = 448
    Top = 297
  end
  object OraErrorHandler1: TOraErrorHandler
    Session = Conexao
    OnError = OraErrorHandler1Error
    Left = 512
    Top = 298
  end
  object Query: TOraQuery
    Session = Conexao
    Left = 512
    Top = 230
  end
end
