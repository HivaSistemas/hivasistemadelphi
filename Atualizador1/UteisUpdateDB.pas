unit UteisUpdateDB;

interface

uses
  Classes, SysUtils, _Conexao, DB, Vcl.Dialogs, System.UITypes, OraScript, _OperacoesBancoDados, _Sessao;

type
  RecScript = record
    script: string;
    passarSeErro: Boolean;
  end;

procedure ExecutarScript(pCodigoScript: Integer; pScript: RecScript);
function ValidarObjetos: Boolean;
function MontarScript(pPassarSeErro: Boolean; pScript: string): RecScript;

implementation

procedure ExecutarScript(pCodigoScript: Integer; pScript: RecScript);
var
  vVersao: Integer;
  vSql: TExecucao;
  vSqlUltimoComando: TExecucao;

  vPesq: TConsulta;
  vCodigoComandoParametros: Integer;
begin
  vVersao := Sessao.getVersaoSistemaInt;
  pCodigoScript := vVersao + pCodigoScript;

  vSql := TExecucao.Create(Sessao.getConexaoBanco);
  vSqlUltimoComando := TExecucao.Create(Sessao.getConexaoBanco);

  vPesq := TConsulta.Create(Sessao.getConexaoBanco);

  // Ir� atualizar o �ltimo comando manual
  vSqlUltimoComando.SQL.Add('update PARAMETROS set COMANDO_ATUAL = :P1');

  // Buscando o �ltimo comando dos PARAMETROS
  vPesq.Active := False;
  vPesq.SQL.Clear;
  vPesq.SQL.Add('select');
  vPesq.SQL.Add('  nvl(COMANDO_ATUAL, 0) as COMANDO_ATUAL');
  vPesq.SQL.Add('from');
  vPesq.SQL.Add('  PARAMETROS');

  try
    // Buscando o �ltimo comando dos PARAMETROS
    // Tem que ser executado a cada script mesmo, porque pode ser que v�rias m�quinas tenham os scripts em mem�ria,
    // sendo assim, somente a primeira ir� e dever� executar esse script
    vPesq.Pesquisar;
    vCodigoComandoParametros := vPesq.GetInt('COMANDO_ATUAL');

    // Se o �ltimo comando manual dos PARAMETROS for maior ou igual ao do script, n�o executar o script, pois o comando j� foi rodado pelo exe
    if vCodigoComandoParametros >= pCodigoScript then
      Exit;

    try
      // Ser� comitado script por script
      Sessao.getConexaoBanco.IniciarTransacao;

      vSql.SQL.Clear;
      vSql.SQL.Add(pScript.script);
      vSql.Executar;

      // Atualizando a coluna COMANDO_ATUAL da tabela PARAMETROS
      vSqlUltimoComando.Executar([pCodigoScript]);

      // Fechando a transa��o do script atual
      Sessao.getConexaoBanco.FinalizarTransacao;
    except on E: Exception do
      begin
        Sessao.getConexaoBanco.VoltarTransacao;

        // Se aceitar falha, atualizar o �ltimo comando manual e continuar a execu��o dos demais scipts
        if pScript.passarSeErro then begin
          try
            Sessao.getConexaoBanco.IniciarTransacao;

            vSqlUltimoComando.Executar([pCodigoScript]);

            Sessao.getConexaoBanco.FinalizarTransacao;
          except
            on e: Exception do begin
              Sessao.getConexaoBanco.VoltarTransacao;
              ShowMessage(
                'Erro ao atualizar o �ltimo comando.' + #13 +
                'C�digo: ' + IntToStr(pCodigoScript) + #13 +
                'Mensagem de erro: ' + e.Message
              );
              Halt;
            end;
          end;
        end
        else begin
          ShowMessage(
            'Ocorreu um erro ao executar o script de atualiza��o.' + #13 +
            'C�digo: ' + IntToStr(pCodigoScript) + #13 +
            'Mensagem de erro: ' + e.Message
          );
          Halt;
        end;
      end;
    end;
  finally
    vSql.Sql.Clear;
    vSql.Sql.Add('purge recyclebin'); // Limpando a lixeira do Oracle
    vSql.Executar;

    FreeAndNil(vSql);
    FreeAndNil(vSqlUltimoComando);

    vPesq.Active := False;
    vPesq.Free;
  end;
end;

function ValidarObjetos: Boolean;
var
  vPesq: TConsulta;
  vExec: TExecucao;
begin
  // Validando os objetos no bando de dados.
  vPesq := TConsulta.Create(Sessao.getConexaoBanco);
  vExec := TExecucao.Create(Sessao.getConexaoBanco);

  vPesq.SQL.Clear;
  vPesq.SQL.Add('select');
  vPesq.SQL.Add('  OBJECT_TYPE,');
  vPesq.SQL.Add('  OBJECT_NAME');
  vPesq.SQL.Add('from');
  vPesq.SQL.Add('  USER_OBJECTS');

  vPesq.SQL.Add('where STATUS = ''INVALID''');

  vPesq.SQL.Add('order by');
  vPesq.SQL.Add('  OBJECT_TYPE,');
  vPesq.SQL.Add('  OBJECT_NAME');

  Result := not vPesq.Pesquisar;
  if not Result then begin
    while not vPesq.eof do begin
      try
        vExec.SQL.Clear;
        vExec.SQL.Add('alter ' + vPesq.GetString(0) + ' ' +  vPesq.GetString(1) + ' compile');
        vExec.Executar;
        vPesq.Next;
      except
        vPesq.Next;
        Continue;
      end;
    end;
  end;

  vExec.Free;
  vPesq.Free;
end;

function MontarScript(pPassarSeErro: Boolean; pScript: string): RecScript;
begin
  Result.passarSeErro := pPassarSeErro;
  Result.script := pScript;
end;

end.
