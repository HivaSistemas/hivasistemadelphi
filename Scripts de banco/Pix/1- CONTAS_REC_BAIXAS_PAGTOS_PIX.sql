create table CONTAS_REC_BAIXAS_PAGTOS_PIX(
  BAIXA_ID     		 number(10),
	CONTA_ID    		 varchar(8) not null,
  VALOR            number(8,2) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table CONTAS_REC_BAIXAS_PAGTOS_PIX
add constraint PK_CONTAS_REC_BX_PAGTOS_PIX
primary key(BAIXA_ID, CONTA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_REC_BAIXAS_PAGTOS_PIX
add constraint FK_CONTA_R_BX_PAG_PIX_BAIXA_ID
foreign key(BAIXA_ID)
references CONTAS_RECEBER_BAIXAS(BAIXA_ID);

alter table CONTAS_REC_BAIXAS_PAGTOS_PIX
add constraint FK_CONTA_R_BX_PAG_PIX_CONTA_ID
foreign key(CONTA_ID)
references CONTAS(CONTA);

/* Checagens */
alter table CONTAS_REC_BAIXAS_PAGTOS_PIX
add constraint CK_CONTAS_REC_BX_PAG_PIX_VALOR
check(VALOR > 0);