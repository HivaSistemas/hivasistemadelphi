create or replace procedure RATEAR_VALORES_FORMAS_PAGTO_NF(
  iVALOR_DINHEIRO       in out number,
  iVALOR_PIX            in out number,
  iVALOR_CARTAO_DEBITO  in out number,
  iVALOR_CARTAO_CREDITO in out number,
  iVALOR_COBRANCA       in out number,
  iVALOR_CHEQUE         in out number,
  iVALOR_FINANCEIRA     in out number,
  iVALOR_CREDITO        in out number,
  iVALOR_TOTAL          in number
)
is
  vDiferencaFormasPagto   number default 0;
begin

  vDiferencaFormasPagto :=
    (
      iVALOR_DINHEIRO +
      iVALOR_PIX +
      iVALOR_CARTAO_DEBITO +
      iVALOR_CARTAO_CREDITO +
      iVALOR_COBRANCA +
      iVALOR_CHEQUE +
      iVALOR_FINANCEIRA +
      iVALOR_CREDITO
    ) -
    iVALOR_TOTAL;

  if vDiferencaFormasPagto = 0 then
    return;
  end if;

  while abs(vDiferencaFormasPagto) > 0 loop
    if vDiferencaFormasPagto < 0 then
      if iVALOR_CREDITO > 0.01 then
        iVALOR_CREDITO := iVALOR_CREDITO + 0.01;
      elsif iVALOR_FINANCEIRA > 0.01 then
        iVALOR_FINANCEIRA := iVALOR_FINANCEIRA + 0.01;
      elsif iVALOR_CHEQUE > 0.01 then
        iVALOR_CHEQUE := iVALOR_CHEQUE + 0.01;
      elsif iVALOR_COBRANCA > 0.01 then
        iVALOR_COBRANCA := iVALOR_COBRANCA + 0.01;
      elsif iVALOR_CARTAO_CREDITO > 0.01 then
        iVALOR_CARTAO_CREDITO := iVALOR_CARTAO_CREDITO + 0.01;
      elsif iVALOR_CARTAO_DEBITO > 0.01 then
        iVALOR_CARTAO_DEBITO := iVALOR_CARTAO_DEBITO + 0.01;
      elsif iVALOR_DINHEIRO > 0.01 then
        iVALOR_DINHEIRO := iVALOR_DINHEIRO + 0.01;
      elsif iVALOR_PIX > 0.01 then
        iVALOR_PIX := iVALOR_PIX + 0.01;
      end if;
      vDiferencaFormasPagto := vDiferencaFormasPagto + 0.01;
    else
      if iVALOR_CREDITO > 0.01 then
        iVALOR_CREDITO := iVALOR_CREDITO - 0.01;
      elsif iVALOR_FINANCEIRA > 0.01 then
        iVALOR_FINANCEIRA := iVALOR_FINANCEIRA - 0.01;
      elsif iVALOR_CHEQUE > 0.01 then
        iVALOR_CHEQUE := iVALOR_CHEQUE - 0.01;
      elsif iVALOR_COBRANCA > 0.01 then
        iVALOR_COBRANCA := iVALOR_COBRANCA - 0.01;
      elsif iVALOR_CARTAO_CREDITO > 0.01 then
        iVALOR_CARTAO_CREDITO := iVALOR_CARTAO_CREDITO - 0.01;
      elsif iVALOR_CARTAO_DEBITO > 0.01 then
        iVALOR_CARTAO_DEBITO := iVALOR_CARTAO_DEBITO - 0.01;
      elsif iVALOR_DINHEIRO > 0.01 then
        iVALOR_DINHEIRO := iVALOR_DINHEIRO - 0.01;
      elsif iVALOR_PIX > 0.01 then
        iVALOR_PIX := iVALOR_PIX - 0.01;
      end if;
      vDiferencaFormasPagto := vDiferencaFormasPagto - 0.01;
    end if;
  end loop;

end RATEAR_VALORES_FORMAS_PAGTO_NF;
