﻿create or replace view VW_MOVIMENTOS_CONTAS
as
select
  'TRA' as TIPO_MOVIMENTO,
  MOV.MOVIMENTO_ID,
  MOV.DATA_HORA_MOVIMENTO,
  MOV.CONTA_DESTINO_ID as CONTA_ID,
  MOV.VALOR_MOVIMENTO,  
  'E' as NATUREZA,
  MOV.PLANO_FINANCEIRO_ID,
  MOV.CENTRO_CUSTO_ID,
  MOV.BAIXA_RECEBER_ID,
  MOV.BAIXA_PAGAR_ID,
  MOV.USUARIO_MOVIMENTO_ID,
  MOV.CONCILIADO,
  MOV.USUARIO_CONCILIACAO_ID,
  MOV.DATA_HORA_CONCILIACAO,
  MOV.MOVIMENTO_ID_BANCO
from
  MOVIMENTOS_CONTAS MOV
  
where MOV.CONTA_DESTINO_ID is not null -- Trazendo apenas os movimentos de origem, ou seja, se for uma transferência a conta que fez a transferencia para outra.
and MOV.TIPO_MOVIMENTO = 'TRA'

union all

select
  'TRA' as TIPO_MOVIMENTO,
  MOV.MOVIMENTO_ID,
  MOV.DATA_HORA_MOVIMENTO,
  MOV.CONTA_ID,
  MOV.VALOR_MOVIMENTO,  
  'S' as NATUREZA,
  MOV.PLANO_FINANCEIRO_ID,
  MOV.CENTRO_CUSTO_ID,
  MOV.BAIXA_RECEBER_ID,
  MOV.BAIXA_PAGAR_ID,
  MOV.USUARIO_MOVIMENTO_ID,
  MOV.CONCILIADO,
  MOV.USUARIO_CONCILIACAO_ID,
  MOV.DATA_HORA_CONCILIACAO,
  MOV.MOVIMENTO_ID_BANCO
from
  MOVIMENTOS_CONTAS MOV
  
where MOV.CONTA_DESTINO_ID is null -- Trazendo apenas os movimentos de destino, ou seja, se for uma transferência a conta que recebeu a transferencia.
and MOV.TIPO_MOVIMENTO = 'TRA'

union all

select
  MOV.TIPO_MOVIMENTO,
  MOV.MOVIMENTO_ID,
  MOV.DATA_HORA_MOVIMENTO,
  MOV.CONTA_ID,
  MOV.VALOR_MOVIMENTO,  
  case when TIPO_MOVIMENTO in('BXR', 'ENM', 'PIX') then 'E' else 'S' end as NATUREZA,
  MOV.PLANO_FINANCEIRO_ID,
  MOV.CENTRO_CUSTO_ID,
  MOV.BAIXA_RECEBER_ID,
  MOV.BAIXA_PAGAR_ID,
  MOV.USUARIO_MOVIMENTO_ID,
  MOV.CONCILIADO,
  MOV.USUARIO_CONCILIACAO_ID,
  MOV.DATA_HORA_CONCILIACAO,
  MOV.MOVIMENTO_ID_BANCO
from
  MOVIMENTOS_CONTAS MOV

union all

select
  'ATC' as TIPO_MOVIMENTO,
  TUR.TURNO_ID as MOVIMENTO_ID,
  TUR.DATA_HORA_ABERTURA,
  TUR.CONTA_ORIGEM_DIN_INI_ID as CONTA_ID,
  TUR.VALOR_INICIAL,
  'S' as NATUREZA,
  null as PLANO_FINANCEIRO_ID,
  null as CENTRO_CUSTO_ID,
  null as BAIXA_RECEBER_ID,
  null as BAIXA_PAGAR_ID,
  TUR.USUARIO_ABERTURA_ID,
  'S' as CONCILIADO,
  TUR.USUARIO_ABERTURA_ID,
  TUR.DATA_HORA_ABERTURA,
  null
from
  TURNOS_CAIXA TUR
where TUR.VALOR_INICIAL > 0

union all

select
  TUR.TIPO_MOVIMENTO as TIPO_MOVIMENTO,
  TUR.MOVIMENTO_TURNO_ID as MOVIMENTO_ID,
  TUR.DATA_HORA_MOVIMENTO,
  TUR.CONTA_ID as CONTA_ID,
  TUR.VALOR_DINHEIRO,
  case when TUR.TIPO_MOVIMENTO in('SAN', 'FEC') then 'E' else 'S' end as NATUREZA,
  null as PLANO_FINANCEIRO_ID,
  null as CENTRO_CUSTO_ID,
  null as BAIXA_RECEBER_ID,
  null as BAIXA_PAGAR_ID,
  TUR.USUARIO_CADASTRO_ID,
  'S' as CONCILIADO,
  TUR.USUARIO_CADASTRO_ID,
  TUR.DATA_HORA_MOVIMENTO,
  null
from
  MOVIMENTOS_TURNOS TUR
where TUR.VALOR_DINHEIRO > 0;
