create or replace function RETIRAR_ACENTOS(pTexto in string)
return string
is
  vTexto varchar2(4000);
begin
  vTexto := pTexto;

  vTexto := replace(replace(replace(replace(replace(vTexto, 'Ä', 'A'), 'Á', 'A'),'Â', 'A'), 'Ã', 'A'), 'À', 'A');
  vTexto := replace(replace(replace(replace(replace(vTexto, 'ä', 'A'), 'á', 'A'),'â', 'A'), 'ã', 'A'), 'à', 'A');

  vTexto := replace(replace(replace(replace(vTexto, 'Ë', 'E'), 'È', 'E'), 'Ê', 'E'), 'É', 'E');
  vTexto := replace(replace(replace(replace(vTexto, 'ë', 'E'), 'è', 'E'), 'ê', 'E'), 'é', 'E');

  vTexto := replace(replace(replace(replace(vTexto, 'Ï', 'I'), 'Í', 'I'), 'Ì', 'I'), 'Î', 'I');
  vTexto := replace(replace(replace(replace(vTexto, 'ï', 'I'), 'í', 'I'), 'ì', 'I'), 'î', 'I');

   vTexto := replace(replace(replace(replace(replace(vTexto, 'Ö', 'O'), 'Ó', 'O'),'Ò', 'O'), 'Ô', 'O'), 'Õ', 'O');
   vTexto := replace(replace(replace(replace(replace(vTexto, 'ö', 'O'), 'ó', 'O'),'ò', 'O'), 'ô', 'O'), 'õ', 'O');

   vTexto := replace(replace(replace(replace(vTexto, 'Ü', 'U'), 'Ú', 'U'),'Ù', 'U'), 'Û', 'U');
   vTexto := replace(replace(replace(replace(vTexto, 'ü', 'U'), 'ú', 'U'),'ù', 'U'), 'û', 'U');

  vTexto := replace(vTexto, 'Ç', 'C');
  vTexto := replace(vTexto, 'ç', 'C');

  vTexto := replace(vTexto, 'º', 'O');
  vTexto := replace(vTexto, 'ª', 'A');

  return vTexto;

end RETIRAR_ACENTOS;
/
