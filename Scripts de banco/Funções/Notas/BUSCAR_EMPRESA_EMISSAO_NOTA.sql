create or replace function BUSCAR_EMPRESA_EMISSAO_NOTA(iEMPRESA_ID in number, iCLIENTE_ID in number, iTIPO_ENTREGA in string)
return number
is
  vTipoPessoa                    CADASTROS.TIPO_PESSOA%type;
  
  vTipoRedirecionamentoNota      PARAMETROS_EMPRESA.TIPO_REDIRECIONAMENTO_NOTA%type;
  vEmpresaDirecionarRetiraAtoId  PARAMETROS_EMPRESA.EMPRESA_RED_NOTA_RETIRA_ATO_ID%type;
  vEmpresaDirecionarRetirarId    PARAMETROS_EMPRESA.EMPRESA_RED_NOTA_RETIRAR_ID%type;
  vEmpresaDirecionarEntregar     PARAMETROS_EMPRESA.EMPRESA_RED_NOTA_ENTREGAR_ID%type;

  vIgnorarRedirRegraNotaFisc     CLIENTES.IGNORAR_REDIR_REGRA_NOTA_FISC%type;
begin
  
  select
    TIPO_REDIRECIONAMENTO_NOTA,
    EMPRESA_RED_NOTA_RETIRA_ATO_ID,
    EMPRESA_RED_NOTA_RETIRAR_ID,
    EMPRESA_RED_NOTA_ENTREGAR_ID
  into
    vTipoRedirecionamentoNota,
    vEmpresaDirecionarRetiraAtoId,
    vEmpresaDirecionarRetirarId,
    vEmpresaDirecionarEntregar
  from
    PARAMETROS_EMPRESA
  where EMPRESA_ID = iEMPRESA_ID;
  
  if vTipoRedirecionamentoNota = 'NRE' then
    return iEMPRESA_ID;
  end if;

  select
    CAD.TIPO_PESSOA,
    CLI.IGNORAR_REDIR_REGRA_NOTA_FISC
  into
    vTipoPessoa,
    vIgnorarRedirRegraNotaFisc
  from
    CADASTROS CAD

  inner join CLIENTES CLI
  on CAD.CADASTRO_ID = CLI.CADASTRO_ID

  where CAD.CADASTRO_ID = iCLIENTE_ID;

  if vIgnorarRedirRegraNotaFisc = 'S' then
    return iEMPRESA_ID;
  end if;

  if vTipoRedirecionamentoNota = 'RPJ' and vTipoPessoa = 'F' then
    return iEMPRESA_ID;
  end if;

  if iTIPO_ENTREGA = 'A' and vEmpresaDirecionarRetiraAtoId is not null then
    return vEmpresaDirecionarRetiraAtoId;
  elsif iTIPO_ENTREGA = 'R' and vEmpresaDirecionarRetirarId is not null then
    return vEmpresaDirecionarRetirarId;
  elsif iTIPO_ENTREGA = 'E' and vEmpresaDirecionarEntregar is not null then
    return vEmpresaDirecionarEntregar;
  else
    return iEMPRESA_ID;
  end if;

end BUSCAR_EMPRESA_EMISSAO_NOTA;
/