﻿create or replace function BUSCAR_TIPO_NOTA_GERAR_VENDA(iCLIENTE_ID in number, iTIPO_NOTA_GERAR in string)
return string
is
  vConsumidorFinal    char(1) default 'N';
	vTipoCliente        char(2) default 'NC';
  vEmitirSomenteNFe   CLIENTES.EMITIR_SOMENTE_NFE%type;
  vOrigemVenda        ORCAMENTOS.ORIGEM_VENDA%type;
begin

  /* Vou trabalhar nestas regras mais para frente */
  return 'C';

	if
    SESSAO.PARAMETROS_EMPRESA_LOGADA.UTILIZA_NFE = 'N' and
    SESSAO.PARAMETROS_EMPRESA_LOGADA.UTILIZA_NFCE = 'N'
  then
	  ERRO('Não será possível continuar esta rotina, pois falta parametrizar nos "Parâmetros de empresa" o uso de NFe ou NFCe!');
	end if;

  select



  if iTIPO_NOTA_GERAR = 'NI' then
    if vDadosNota.TIPO_PESSOA_DESTINATARIO = 'F' then
      vModeloNota := '65';
      vSerieNota := SESSAO.PARAMETROS_EMPRESA_LOGADA.SERIE_NFCE;
      iTIPO_NOTA_GERAR := 'C';
    else
      vModeloNota := '55';
      vSerieNota  := to_char(nvl(SESSAO.PARAMETROS_EMPRESA_LOGADA.SERIE_NFE, 1));
      iTIPO_NOTA_GERAR := 'N';
    end if;
  elsif (iTIPO_NOTA_GERAR = 'NF') or (vDadosNota.TIPO_PESSOA_DESTINATARIO = 'J') then
    vModeloNota := '55';
    vSerieNota  := to_char(nvl(SESSAO.PARAMETROS_EMPRESA_LOGADA.SERIE_NFE, 1));
    iTIPO_NOTA_GERAR := 'N';
  else
    vModeloNota := '65';
    iTIPO_NOTA_GERAR := 'C';
    vSerieNota  := to_char(SESSAO.PARAMETROS_EMPRESA_LOGADA.SERIE_NFCE);
  end if;

end BUSCAR_TIPO_NOTA_GERAR_VENDA;
/
