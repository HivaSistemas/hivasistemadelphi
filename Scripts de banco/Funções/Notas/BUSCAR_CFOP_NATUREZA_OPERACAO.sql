create or replace function BUSCAR_CFOP_NATUREZA_OPERACAO(iCFOP_ID in string)
return string
is
  vNatureza  CFOP.DESCRICAO%type;
begin

  select
    NOME
  into
    vNatureza
  from
    CFOP
  where CFOP_PESQUISA_ID = iCFOP_ID;
  
  return vNatureza;
    
end BUSCAR_CFOP_NATUREZA_OPERACAO;
/