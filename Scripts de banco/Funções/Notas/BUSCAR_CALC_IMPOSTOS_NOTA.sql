﻿create or replace function BUSCAR_CALC_IMPOSTOS_NOTA(
  iEMPRESA_ID            in number,
  iVALOR_TOTAL_PRODUTOS  in number,
  iVALOR_OUTRAS_DESPESAS in number,
  iVALOR_DESCONTO        in number,
  iCST                   in string,
  iPERCENTUAL_ICMS       in number,
  iINDICE_REDUCAO_ICMS   in number,
  iIVA                   in number,
  iPERCENTUAL_PIS        in number,
  iCST_PIS               in number,
  iPERCENTUAL_COFINS     in number,
  iCST_COFINS            in number,
  iPERCENTUAL_ICMS_INTER in number
)
return RecordsNotasFiscais.RecImpostosCalculados
is
  vRet                RecordsNotasFiscais.RecImpostosCalculados;
  vRegimeTributario   PARAMETROS_EMPRESA.REGIME_TRIBUTARIO%type;
  vIcmsDestino        number;
begin
  vRet.BASE_CALCULO_ICMS           := 0;
  vRet.INDICE_REDUCAO_BASE_ICMS    := 1;
  vRet.PERCENTUAL_ICMS             := 0;
  vRet.VALOR_ICMS                  := 0;

  vRet.BASE_CALCULO_ICMS_INTER     := 0;
  vRet.PERCENTUAL_ICMS_INTER       := 0;
  vRet.VALOR_ICMS_INTER            := 0;

  vRet.INDICE_REDUCAO_BASE_ICMS_ST := 1;
  vRet.BASE_CALCULO_ICMS_ST        := 0;
  vRet.PERCENTUAL_ICMS_ST          := 0;
  vRet.VALOR_ICMS_ST               := 0;
  vRet.IVA                         := 0;
  vRet.PRECO_PAUTA                 := 0;

  vRet.VALOR_IPI                   := 0;
  vRet.PERCENTUAL_IPI              := 0;

  vRet.BASE_CALCULO_PIS            := 0;
  vRet.PERCENTUAL_PIS              := 0;
  vRet.VALOR_PIS                   := 0;

  vRet.BASE_CALCULO_COFINS         := 0;
  vRet.PERCENTUAL_COFINS           := 0;
  vRet.VALOR_COFINS                := 0;

  select
    REGIME_TRIBUTARIO
  into
    vRegimeTributario
  from
    PARAMETROS_EMPRESA
  where EMPRESA_ID = iEMPRESA_ID;

  /* Se a empresa for do simples nacional não calcula nada */
  if vRegimeTributario = 'SN' then
    return vRet;
  end if;

  vRet.BASE_CALCULO_PIS         := iVALOR_TOTAL_PRODUTOS + iVALOR_OUTRAS_DESPESAS - iVALOR_DESCONTO;
  vRet.PERCENTUAL_PIS           := iPERCENTUAL_PIS;
  vRet.VALOR_PIS                := trunc(vRet.BASE_CALCULO_PIS * (vRet.PERCENTUAL_PIS * 0.01), 2);

  vRet.BASE_CALCULO_COFINS      := iVALOR_TOTAL_PRODUTOS + iVALOR_OUTRAS_DESPESAS - iVALOR_DESCONTO;
  vRet.PERCENTUAL_COFINS        := iPERCENTUAL_COFINS;
  vRet.VALOR_COFINS             := trunc(vRet.BASE_CALCULO_COFINS * (vRet.PERCENTUAL_COFINS * 0.01), 2);
  
  if iCST = '00' then
    vRet.BASE_CALCULO_ICMS        := iVALOR_TOTAL_PRODUTOS + iVALOR_OUTRAS_DESPESAS - iVALOR_DESCONTO;
    vRet.PERCENTUAL_ICMS          := iPERCENTUAL_ICMS;
    vRet.VALOR_ICMS               := round(vRet.BASE_CALCULO_ICMS * (vRet.PERCENTUAL_ICMS * 0.01), 2);

    if iPERCENTUAL_ICMS_INTER > 0 then
      vRet.BASE_CALCULO_ICMS_INTER := vRet.BASE_CALCULO_ICMS;
      vRet.PERCENTUAL_ICMS_INTER := iPERCENTUAL_ICMS_INTER;
      vIcmsDestino := (vRet.BASE_CALCULO_ICMS_INTER * (vRet.PERCENTUAL_ICMS_INTER * 0.01));
      vRet.VALOR_ICMS_INTER := round(vIcmsDestino - vRet.VALOR_ICMS, 2);
    end if;
  elsif iCST = '20' then
    vRet.BASE_CALCULO_ICMS        := round((iVALOR_TOTAL_PRODUTOS + iVALOR_OUTRAS_DESPESAS - iVALOR_DESCONTO) * iINDICE_REDUCAO_ICMS, 2);
    vRet.PERCENTUAL_ICMS          := iPERCENTUAL_ICMS;
    vRet.VALOR_ICMS               := round(vRet.BASE_CALCULO_ICMS * (vRet.PERCENTUAL_ICMS * 0.01), 2);
    vRet.INDICE_REDUCAO_BASE_ICMS := iINDICE_REDUCAO_ICMS;

    if iPERCENTUAL_ICMS_INTER > 0 then
      vRet.BASE_CALCULO_ICMS_INTER := vRet.BASE_CALCULO_ICMS;
      vRet.PERCENTUAL_ICMS_INTER := iPERCENTUAL_ICMS_INTER;
      vIcmsDestino := (vRet.BASE_CALCULO_ICMS_INTER * (vRet.PERCENTUAL_ICMS_INTER * 0.01));
      vRet.VALOR_ICMS_INTER := round(vIcmsDestino - vRet.VALOR_ICMS, 2);
    end if;
  elsif iCST not in ('60', '40') then
    ERRO('CST ' || iCST || ' de saída não implementado!');
  end if;
  
  return vRet;
end BUSCAR_CALC_IMPOSTOS_NOTA;
/