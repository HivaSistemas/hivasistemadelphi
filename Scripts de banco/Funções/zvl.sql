create or replace function zvl(
  iVALOR1 in number, 
  iVALOR2 in number
)
return number
is
begin
  if nvl(iVALOR1, 0) = 0 then
    return iVALOR2;
  else
    return iVALOR1;  
  end if;
end zvl;
/