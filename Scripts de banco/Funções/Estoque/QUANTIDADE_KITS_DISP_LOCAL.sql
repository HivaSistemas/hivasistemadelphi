create or replace function QUANTIDADE_KITS_DISP_LOCAL(iEMPRESA_ID in number, iLOCAL_KIT_ID in number, iPRODUTO_KIT_ID in number)
return number
is  
  vRetorno            number;    
begin   
  
  select
    trunc(min(DISPONIVEL), 0) as DISPONIVEL
  into
    vRetorno
  from (
    select
      PRK.PRODUTO_ID,
      sum(ZERO_NEGATIVO(DIV.DISPONIVEL) / PRK.QUANTIDADE) as DISPONIVEL
    from
      PRODUTOS_KIT PRK

    inner join VW_ESTOQUES_DIVISAO DIV
    on DIV.EMPRESA_ID = iEMPRESA_ID
    and PRK.PRODUTO_ID = DIV.PRODUTO_ID
    and DIV.LOCAL_ID = iLOCAL_KIT_ID

    where PRK.PRODUTO_KIT_ID = iPRODUTO_KIT_ID

    group by
      PRK.PRODUTO_ID,
      DIV.ACEITAR_ESTOQUE_NEGATIVO
  );
  
  return nvl(vRetorno, 0);

end QUANTIDADE_KITS_DISP_LOCAL;
/