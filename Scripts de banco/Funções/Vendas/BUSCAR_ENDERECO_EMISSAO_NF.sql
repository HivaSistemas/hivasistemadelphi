create or replace function BUSCAR_ENDERECO_EMISSAO_NF(iMOVIMENTO_ID in number, iTIPO_MOVIMENTO in string)
return RecordsNotasFiscais.RecEnderecoEmissaoNotaFiscal
is 
  vUtilizarEnderecoMovCapa  char(1) default 'S';
  vRetorno              RecordsNotasFiscais.RecEnderecoEmissaoNotaFiscal;
begin

  if iTIPO_MOVIMENTO not in('ORC', 'ACU') then
    ERRO('O tipo de movimento só pode ser "ORC" e "ACU"!');
  end if;
  
  if iTIPO_MOVIMENTO = 'ORC' then
  
    select
      case
        when ORC.CLIENTE_ID = PAR.CADASTRO_CONSUMIDOR_FINAL_ID then 'N'
        when ORC.INSCRICAO_ESTADUAL is not null then 'S'        
        when CAD.TIPO_PESSOA = 'F' and ORC.BAIRRO_ID is not null then 'S'
        else 'N'
      end
    into
      vUtilizarEnderecoMovCapa
    from
      ORCAMENTOS ORC
     
    inner join CADASTROS CAD
    on ORC.CLIENTE_ID = CAD.CADASTRO_ID
    
    cross join PARAMETROS PAR
    
    where ORC.ORCAMENTO_ID = iMOVIMENTO_ID;
    
    select
      case when vUtilizarEnderecoMovCapa = 'S' then ORC.INSCRICAO_ESTADUAL else CAD.INSCRICAO_ESTADUAL end,
      case when vUtilizarEnderecoMovCapa = 'S' then ORC.LOGRADOURO else CAD.LOGRADOURO end,
      case when vUtilizarEnderecoMovCapa = 'S' then ORC.COMPLEMENTO else CAD.COMPLEMENTO end,
      case when vUtilizarEnderecoMovCapa = 'S' then ORC.NUMERO else CAD.NUMERO end,
      case when vUtilizarEnderecoMovCapa = 'S' then ORC.CEP else CAD.CEP end,
      BAC.NOME_BAIRRO,
      BAC.NOME_CIDADE,
      BAC.CODIGO_IBGE_CIDADE,
      BAC.ESTADO_ID,
      
      ORC.INSCRICAO_ESTADUAL,
      ORC.LOGRADOURO,
      ORC.COMPLEMENTO,
      ORC.NUMERO,
      ORC.CEP,
      BEN.NOME_BAIRRO,
      BEN.NOME_CIDADE,
      BEN.CODIGO_IBGE_CIDADE,
      BEN.ESTADO_ID
    into
      vRetorno.INSCRICAO_ESTADUAL_DESTINAT,
      vRetorno.LOGRADOURO_DESTINATARIO,
      vRetorno.COMPLEMENTO_DESTINATARIO,
      vRetorno.NUMERO_DESTINATARIO,
      vRetorno.CEP_DESTINATARIO,
      vRetorno.NOME_BAIRRO_DESTINATARIO,
      vRetorno.NOME_CIDADE_DESTINATARIO,
      vRetorno.CODIGO_IBGE_MUNICIPIO_DEST,
      vRetorno.ESTADO_ID_DESTINATARIO,
      
      vRetorno.INSCRICAO_EST_ENTR_DESTINAT,
      vRetorno.LOGRADOURO_ENTR_DESTINATARIO,
      vRetorno.COMPLEMENTO_ENTR_DESTINATARIO,
      vRetorno.NUMERO_ENTR_DESTINATARIO,
      vRetorno.CEP_ENTR_DESTINATARIO,
      vRetorno.NOME_BAIRRO_ENTR_DESTINATARIO,
      vRetorno.NOME_CIDADE_ENTR_DESTINATARIO,      
      vRetorno.CODIGO_IBGE_MUNIC_ENTR_DEST,
      vRetorno.ESTADO_ID_ENTR_DESTINATARIO
    from
      ORCAMENTOS ORC    

    inner join CADASTROS CAD
    on ORC.CLIENTE_ID = CAD.CADASTRO_ID

    inner join VW_BAIRROS BAC
    on case when vUtilizarEnderecoMovCapa = 'S' then ORC.BAIRRO_ID else CAD.BAIRRO_ID end = BAC.BAIRRO_ID
    
    left join VW_BAIRROS BEN
    on ORC.BAIRRO_ID = BEN.BAIRRO_ID
   
    where ORC.ORCAMENTO_ID = iMOVIMENTO_ID;
    
    if vUtilizarEnderecoMovCapa = 'S' then
      vRetorno.ENDERECO_INFO_COMPLEMENTARES :=
        'Local de entrega: ' || vRetorno.LOGRADOURO_DESTINATARIO || ' ' || vRetorno.COMPLEMENTO_DESTINATARIO || ' ' || ' Nr ' || vRetorno.NUMERO_DESTINATARIO || ',' ||
         vRetorno.NOME_BAIRRO_DESTINATARIO || ' - ' || vRetorno.NOME_CIDADE_DESTINATARIO || '/' || vRetorno.ESTADO_ID_DESTINATARIO || ' CEP: ' || vRetorno.CEP_DESTINATARIO;
    end if;
    
  else
  
    select
      case
        when ACU.INSCRICAO_ESTADUAL is not null then 'S'
        when CAD.TIPO_PESSOA = 'F' and ACU.BAIRRO_ID is not null then 'S'
        else 'N'
      end
    into
      vUtilizarEnderecoMovCapa
    from
      ACUMULADOS ACU
     
    inner join CADASTROS CAD
    on ACU.CLIENTE_ID = CAD.CADASTRO_ID;
    
    select
      case when vUtilizarEnderecoMovCapa = 'S' then ACU.INSCRICAO_ESTADUAL else CAD.INSCRICAO_ESTADUAL end,
      case when vUtilizarEnderecoMovCapa = 'S' then ACU.LOGRADOURO else CAD.LOGRADOURO end,
      case when vUtilizarEnderecoMovCapa = 'S' then ACU.COMPLEMENTO else CAD.COMPLEMENTO end,
      case when vUtilizarEnderecoMovCapa = 'S' then ACU.NUMERO else CAD.NUMERO end,
      case when vUtilizarEnderecoMovCapa = 'S' then ACU.CEP else CAD.CEP end,
      BAC.NOME_BAIRRO,
      BAC.NOME_CIDADE,
      BAC.CODIGO_IBGE_CIDADE,
      BAC.ESTADO_ID
    into     
      vRetorno.INSCRICAO_ESTADUAL_DESTINAT,
      vRetorno.LOGRADOURO_DESTINATARIO,
      vRetorno.COMPLEMENTO_DESTINATARIO,
      vRetorno.NUMERO_DESTINATARIO,
      vRetorno.CEP_DESTINATARIO,
      vRetorno.NOME_BAIRRO_DESTINATARIO,
      vRetorno.NOME_CIDADE_DESTINATARIO,
      vRetorno.CODIGO_IBGE_MUNICIPIO_DEST,
      vRetorno.ESTADO_ID_DESTINATARIO      
    from
      ACUMULADOS ACU

    inner join CADASTROS CAD
    on ACU.CLIENTE_ID = CAD.CADASTRO_ID

    inner join VW_BAIRROS BAC
    on case when vUtilizarEnderecoMovCapa = 'S' then ACU.BAIRRO_ID else CAD.BAIRRO_ID end = BAC.BAIRRO_ID
   
    where ACU.ACUMULADO_ID = iMOVIMENTO_ID;
    
  end if;
  
  return vRetorno;
   
end BUSCAR_ENDERECO_EMISSAO_NF;
/