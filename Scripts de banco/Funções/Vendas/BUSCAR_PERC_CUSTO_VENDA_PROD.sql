create or replace function BUSCAR_PERC_CUSTO_VENDA_PROD(
  iEMPRESA_ID in number,
  iPRODUTO_ID in number
)
return number
is
  vQtde                number;
  vPercCustoVenda      number;
  vTipoControleEstoque PRODUTOS.TIPO_CONTROLE_ESTOQUE%type;
  vRegimeTributario    PARAMETROS_EMPRESA.REGIME_TRIBUTARIO%type;

  cursor cProdutosKit is
  select
    PRODUTO_ID
  from
    PRODUTOS_KIT
  where PRODUTO_KIT_ID = iPRODUTO_ID;

begin
  vPercCustoVenda := 0;

  select
    TIPO_CONTROLE_ESTOQUE
  into
    vTipoControleEstoque
  from
    PRODUTOS
  where PRODUTO_ID = iPRODUTO_ID;

  if vTipoControleEstoque in('K', 'A', 'I') then

    vQtde := 0;
    for xProdutosKit in cProdutosKit loop
      vPercCustoVenda := vPercCustoVenda + BUSCAR_PERC_CUSTO_VENDA_PROD(iEMPRESA_ID, xProdutosKit.PRODUTO_ID);
      vQtde := vQtde + 1;
    end loop;

    vPercCustoVenda := vPercCustoVenda / vQtde;

  else
    begin
      select
        REGIME_TRIBUTARIO
      into
        vRegimeTributario
      from
        PARAMETROS_EMPRESA
      where EMPRESA_ID = iEMPRESA_ID;

      select
        case when PCF.ZERAR_CUSTO = 'S' then
          0
        else
          PCE.PERCENTUAL_COMISSAO +
          PCE.PERCENTUAL_FRETE_VENDA +
          case when NVL(PCF.CUSTO_FIXO, 0) > 0 then PCF.CUSTO_FIXO else PCE.PERCENTUAL_CUSTO_FIXO end +
          PCE.PERCENTUAL_CSLL +
          PCE.PERCENTUAL_IRPJ +
          case when
            ICM.CST_NAO_CONTRIBUINTE_VENDA in('00','20') and vRegimeTributario <> 'SN' then ICM.PERC_ICMS_VENDA * ICM.INDICE_REDUCAO_BASE_ICMS_VENDA
          else
            0
          end +
          PCE.PERCENTUAL_PIS + PCE.PERCENTUAL_COFINS
        end as PERCENTUAL_CUSTO_VENDA
      into
        vPercCustoVenda
      from
        PRODUTOS PRO

      inner join PERCENTUAIS_CUSTOS_EMPRESA PCE
      on PCE.EMPRESA_ID = iEMPRESA_ID

      inner join PRODUTOS_CUSTO_FIXO PCF
      on iEMPRESA_ID = PCF.EMPRESA_ID
      and PRO.PRODUTO_ID = PCF.PRODUTO_ID

      inner join EMPRESAS EMP
      on PCE.EMPRESA_ID = EMP.EMPRESA_ID

      inner join VW_BAIRROS BAI
      on EMP.BAIRRO_ID = BAI.BAIRRO_ID

      inner join VW_GRUPOS_TRIBUTACOES_ESTADUAL ICM
      on PRO.PRODUTO_ID = ICM.PRODUTO_ID
      and PCE.EMPRESA_ID = ICM.EMPRESA_ID
      and ICM.ESTADO_ID_VENDA = BAI.ESTADO_ID
      and ICM.ESTADO_ID_COMPRA = BAI.ESTADO_ID

      where PRO.ATIVO = 'S'
      and PRO.TIPO_CONTROLE_ESTOQUE not in('K', 'A', 'I')
      and PRO.PRODUTO_ID = iPRODUTO_ID;
    exception
      when others then
        ERRO( 'Problema ao buscar custos do produto ' || iPRODUTO_ID || '. Motivo: ' || sqlerrm );
    end;
  end if;

  return vPercCustoVenda;

end BUSCAR_PERC_CUSTO_VENDA_PROD;
/
