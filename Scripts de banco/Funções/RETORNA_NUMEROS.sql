create or replace function RETORNA_NUMEROS(
	iTEXTO in string
)
return string
is
	v_retorno	varchar2(200)	default null;
	i			number;
begin
	if iTEXTO is not null then
		for i in 1..length(iTEXTO) loop
			if substr(iTEXTO, i, 1) between '0' and '9' then
				v_retorno := v_retorno || substr(iTEXTO, i, 1);
			end if;
		end loop;
	end if;

	return v_retorno;
end RETORNA_NUMEROS;