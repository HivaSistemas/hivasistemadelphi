create or replace function ZERO_NEGATIVO(iVALOR in number)
return number
is
begin

  if iVALOR < 0 then
    return 0;
  else
    return iVALOR;
  end if;

end ZERO_NEGATIVO;