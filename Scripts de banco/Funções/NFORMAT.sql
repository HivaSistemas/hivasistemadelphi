create or replace function NFORMAT(iVALOR in number, iDECIMAIS in number)
return string
is
  vFinalvMascara varchar2(10) default null;
  vMascara       varchar2(30) default '9G999G999G999G990';
  vDecimais      number;
begin

  if iDECIMAIS > 0 then
    for vDecimais in 1..iDECIMAIS loop
      if vFinalvMascara is null then
        vFinalvMascara := 'D';
      end if;

      vFinalvMascara := vFinalvMascara || '0';
    end loop;
  end if;
  
  return ltrim(to_char(iVALOR, vMascara || vFinalvMascara,'nls_numeric_characters=,.'));
  
end NFORMAT;
/