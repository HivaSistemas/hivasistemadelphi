create or replace function DIAS_UTEIS_PERIODO(iEMPRESA_ID in number, iDATA_INICIAL in date, iDATA_FINAL in date)
return number
is
  vQtdeDias      number;
  vQtdeFeriados  number;  
  vIndiceSabado  PARAMETROS_EMPRESA.INDICE_SABADO%type;
  vIndiceDomingo PARAMETROS_EMPRESA.INDICE_DOMINGO%type;
begin

  /* Selecionando os feriados do período */
  select
    sum( 1 * INDICE_DIA ) as QUANTIDADE_FERIADOS
  into
    vQtdeFeriados
  from
    FERIADOS
  where EMPRESA_ID = iEMPRESA_ID
  and DATA between iDATA_INICIAL and iDATA_FINAL;
  
  vQtdeFeriados := nvl(vQtdeFeriados, 0);
  
  /* Buscando os sábados e domingos */
  select
    INDICE_SABADO,
    INDICE_DOMINGO
  into
    vIndiceSabado,
    vIndiceDomingo
  from
    PARAMETROS_EMPRESA
  where EMPRESA_ID = iEMPRESA_ID;
  
  select
    sum(case to_char(iDATA_INICIAL + level, 'D') when '7' then vIndiceSabado when '1' then vIndiceDomingo else 1 end) as QUANTIDADE_DIAS_UTEIS
  into
    vQtdeDias
  from
    dual  
  connect by level <= iDATA_FINAL - iDATA_INICIAL;
  
  return vQtdeDias + 1 - vQtdeFeriados;
  
end DIAS_UTEIS_PERIODO;
