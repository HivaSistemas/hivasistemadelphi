create or replace function BUSCAR_CUSTO_COMPRA_PRODUT(
  iEMPRESA_ID in number,
  iPRODUTO_ID in number
)
return number
is
  vCustoCompra         number;
  vTipoControleEstoque PRODUTOS.TIPO_CONTROLE_ESTOQUE%type;
  vTipoCustoUtilizar   PARAMETROS_EMPRESA.TIPO_CUSTO_VIS_LUCRO_VENDA%type;
  vProdutoPaiId        PRODUTOS.PRODUTO_PAI_ID%type;
  vQuantidadeVezesPai  PRODUTOS.QUANTIDADE_VEZES_PAI%type;
  vTipoControleEstPai  PRODUTOS.TIPO_CONTROLE_ESTOQUE%type;
begin
  vCustoCompra := 0;
  vTipoControleEstPai := 'N';
  select
    TIPO_CONTROLE_ESTOQUE,
    PRODUTO_PAI_ID,
    QUANTIDADE_VEZES_PAI
  into
    vTipoControleEstoque,
    vProdutoPaiId,
    vQuantidadeVezesPai
  from
    PRODUTOS
  where PRODUTO_ID = iPRODUTO_ID;

  if vProdutoPaiId <> iPRODUTO_ID then
    select
      TIPO_CONTROLE_ESTOQUE
    into
      vTipoControleEstPai
    from
      PRODUTOS
    where PRODUTO_ID = vProdutoPaiId;
  end if;

  select
    TIPO_CUSTO_VIS_LUCRO_VENDA
  into
    vTipoCustoUtilizar
  from
    PARAMETROS_EMPRESA
  where EMPRESA_ID = iEMPRESA_ID;

  if (vProdutoPaiId <> iPRODUTO_ID) and ((vTipoControleEstoque in('K', 'A', 'I')) or (vTipoControleEstPai in('K', 'A', 'I'))) then
    select
      sum(
        round(
          case vTipoCustoUtilizar
            when 'FIN' then zvl(PCU.PRECO_LIQUIDO, PCU.CUSTO_ULTIMO_PEDIDO)
            when 'COM' then zvl(PCU.CUSTO_ULTIMO_PEDIDO, PCU.PRECO_LIQUIDO)
            else  PCU.CMV
          end, 4)
      ) as CUSTO_COMPRA_COMERCIAL
    into
      vCustoCompra
    from
      PRODUTOS PRO

    inner join PRODUTOS_KIT PRK
    on PRO.PRODUTO_ID = PRK.PRODUTO_KIT_ID

    inner join VW_CUSTOS_PRODUTOS PCU
    on PRK.PRODUTO_ID = PCU.PRODUTO_ID
    and PCU.EMPRESA_ID = iEMPRESA_ID

    where PRO.ATIVO = 'S'
    and PRO.PRODUTO_ID = vProdutoPaiId;

  elsif vTipoControleEstoque in('K', 'A', 'I') then

    select
      sum(
        round(
          case vTipoCustoUtilizar
            when 'FIN' then zvl(PCU.PRECO_LIQUIDO, PCU.CUSTO_ULTIMO_PEDIDO)
            when 'COM' then zvl(PCU.CUSTO_ULTIMO_PEDIDO, PCU.PRECO_LIQUIDO)
            else PCU.CMV
          end, 4)
      ) as CUSTO_COMPRA_COMERCIAL
    into
      vCustoCompra
    from
      PRODUTOS PRO

    inner join PRODUTOS_KIT PRK
    on PRO.PRODUTO_ID = PRK.PRODUTO_KIT_ID

    inner join VW_CUSTOS_PRODUTOS PCU
    on PRK.PRODUTO_ID = PCU.PRODUTO_ID
    and PCU.EMPRESA_ID = iEMPRESA_ID

    where PRO.ATIVO = 'S'
    and PRO.TIPO_CONTROLE_ESTOQUE in('K', 'A', 'I')
    and PRO.PRODUTO_ID = iPRODUTO_ID;
  else
    select
      case vTipoCustoUtilizar
        when 'FIN' then zvl(PCU.PRECO_LIQUIDO, PCU.CUSTO_ULTIMO_PEDIDO)
        when 'COM' then zvl(PCU.CUSTO_ULTIMO_PEDIDO, PCU.PRECO_LIQUIDO)
      else PCU.CMV
      end
    into
      vCustoCompra
    from
      PRODUTOS PRO

    inner join VW_CUSTOS_PRODUTOS PCU
    on PRO.PRODUTO_ID = PCU.PRODUTO_ID
    and PCU.EMPRESA_ID = iEMPRESA_ID

    where PRO.ATIVO = 'S'
    and PRO.TIPO_CONTROLE_ESTOQUE not in('K', 'A', 'I')
    and PRO.PRODUTO_ID = iPRODUTO_ID;

  end if;

  return vCustoCompra;

end BUSCAR_CUSTO_COMPRA_PRODUT;
