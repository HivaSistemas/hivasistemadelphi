create or replace view VW_GRUPOS_TRIBUTACOES_ESTADUAL
as
select
  PRO.PRODUTO_ID,
  PGT.EMPRESA_ID,
  GEC.ESTADO_ID as ESTADO_ID_COMPRA,
  GEV.ESTADO_ID as ESTADO_ID_VENDA,

  PGT.GRUPO_TRIB_ESTADUAL_COMPRA_ID,
  GEC.CST_REVENDA as CST_REVENDA_COMPRA,
  GEC.CST_INDUSTRIA as CST_INDUSTRIA_COMPRA,
  GEC.CST_DISTRIBUIDORA as CST_DISTRIBUIDORA_COMPRA,  

  PGT.GRUPO_TRIB_ESTADUAL_VENDA_ID,
  GEV.CST_CLINICA_HOSPITAL as CST_CLINICA_HOSPITAL_VENDA,
  GEV.CST_CONSTRUTORA as CST_CONSTRUTORA_VENDA,
  GEV.CST_CONTRIBUINTE as CST_CONTRIBUINTE_VENDA,
  GEV.CST_NAO_CONTRIBUINTE as CST_NAO_CONTRIBUINTE_VENDA,
  GEV.CST_ORGAO_PUBLICO as CST_ORGAO_PUBLICO_VENDA,
  GEV.CST_PRODUTOR_RURAL as CST_PRODUTOR_RURAL_VENDA,
  GEV.CST_REVENDA as CST_REVENDA_VENDA,
  zvl(GEV.PERCENTUAL_ICMS, EST.PERC_ICMS_INTERNO) as PERC_ICMS_VENDA,
  GEV.INDICE_REDUCAO_BASE_ICMS as INDICE_REDUCAO_BASE_ICMS_VENDA,
  GEV.IVA as IVA_VENDA,
  GEV.PERCENTUAL_ICMS_INTER
from
  PRODUTOS PRO
  
inner join PRODUTOS_GRUPOS_TRIB_EMPRESAS PGT
on PRO.PRODUTO_ID = PGT.PRODUTO_ID

inner join GRUPOS_TRIB_ESTADUAL_COMP_EST GEC
on PGT.GRUPO_TRIB_ESTADUAL_COMPRA_ID = GEC.GRUPO_TRIB_ESTADUAL_COMPRA_ID

inner join GRUPOS_TRIB_ESTADUAL_VENDA_EST GEV
on PGT.GRUPO_TRIB_ESTADUAL_VENDA_ID = GEV.GRUPO_TRIB_ESTADUAL_VENDA_ID

inner join ESTADOS EST
on GEV.ESTADO_ID = EST.ESTADO_ID;
/