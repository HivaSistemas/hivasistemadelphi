create or replace view VW_CST
as
select
  '00' as CST,
  'TRIBUTADA INTEGRALMENTE' as DESCRICAO
from
  DUAL
  
union all

select
  '10' as CST,
  'TRIBUTADA E COM COBRANÇA DO ICMS POR SUBSTITUIÇÃO TRIBUTÁRIA' as DESCRICAO
from
  DUAL
  
union all

select
  '20' as CST,
  'COM REDUÇÃO DE BASE DE CÁLCULO' as DESCRICAO
from
  DUAL
  
union all

select
  '30' as CST,
  'ISENTA OU NÃO TRIBUTADA E COM COBRANÇA DO ICMS POR SUBSTITUIÇÃO TRIBUTÁRIA' as DESCRICAO
from
  DUAL
  
union all

select
  '40' as CST,
  'ISENTA' as DESCRICAO
from
  DUAL
  
union all

select
  '41' as CST,
  'NÃO TRIBUTADA' as DESCRICAO
from
  DUAL
  
union all

select
  '50' as CST,
  'SUSPENSÃO' as DESCRICAO
from
  DUAL
  
union all

select
  '51' as CST,
  'DIFERIMENTO' as DESCRICAO
from
  DUAL
  
union all

select
  '60' as CST,
  'ICMS COBRADO ANTERIORMENTE POR SUBSTITUIÇÃO TRIBUTÁRIA' as DESCRICAO
from
  DUAL
  
union all

select
  '70' as CST,
  'COM REDUÇÃO DA BASE DE CÁLCULO E COBRANÇA DO ICMS POR SUBSTITUIÇÃO TRIBUTÁRIA' as DESCRICAO
from
  DUAL
  
union all

select
  '90' as CST,
  'OUTRAS' as DESCRICAO
from
  DUAL;