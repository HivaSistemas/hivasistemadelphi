create or replace view VW_CAMPOS_CADASTRO_CLIENTE as
select
  1 as id,
  'CNAE' as descricao,
  'J' as tipo_cliente,
  'eCNAE' as nome
from dual

union all

select
  2,
  'RG',
  'F',
  'eRG'
from dual

union all

select
  3,
  'Org�o exp.',
  'F',
  'eOrgaoExpedidor'
from dual

union all

select
  4,
  'Data nascimento',
  'F',
  'eDataNascimento'
from dual

union all

select
  5,
  'Estado civil',
  'F',
  'cbEstadoCivil'
from dual

union all

select
  6,
  'Sexo',
  'F',
  'rgSexo'
from dual

union all

select
  7,
  'Telefone',
  'A',
  'eTelefone'
from dual

union all

select
  8,
  'Celular',
  'A',
  'eCelular'
from dual

union all

select
  9,
  'E-mail',
  'A',
  'eEmail'
from dual

union all

select
  10,
  'Grupo de cliente',
  'A',
  'FrGrupoCliente'
from dual

union all

select
  11,
  'Vendedor padr�o',
  'A',
  'FrVendedorPadrao'
from dual;
