create or replace view VW_TIPO_COB_PEDIDO_COM_BAI_ACU
as
select
  ACUMULADO_ID,
  sum(VALOR) as VALOR_SOMENTE_NA_BAIXA
from
  (
  select
    ACUMULADO_ID,
    COBRANCA_ID,
    VALOR
  from
    ACUMULADOS_PAGAMENTOS

  union all

  select
    ACUMULADO_ID,
    COBRANCA_ID,
    VALOR_CHEQUE
  from
    ACUMULADOS_PAGAMENTOS_CHEQUES
) MOV

inner join TIPOS_COBRANCA TCO
on MOV.COBRANCA_ID = TCO.COBRANCA_ID

where TCO.TIPO_PAGAMENTO_COMISSAO = 'B'

group by
  ACUMULADO_ID