create or replace view VW_FAIXAS_SIMPLES_NACIONAL as select
  1 as FAIXA,
  'Ate 180.000,00  -  4,00 %' as DESCRICAO,
  4 as PERCENTUAL
from dual

union all

select
  2 as FAIXA,
  'De 180.000,01 a 360.000,00  -  7,30 %' as DESCRICAO,
  7.30 as PERCENTUAL
from dual
  
union all

select  
  3 as FAIXA,
  'De 360.000,01 a 720.000,00  -  9,50 %' as DESCRICAO,
  9.50 as PERCENTUAL
from dual

union all

select  
  4 as FAIXA,
  'De 720.000,01 a 1.800.000,00  - 10,70 %' as DESCRICAO,
  10.70 as PERCENTUAL
from dual

union all

select  
  5 as FAIXA,
  'De 1.800.000,01 a 3.600.000,00  -  14,30 %' as DESCRICAO,
  14.30 as PERCENTUAL
from dual

union all

select  
  6 as FAIXA,
  'De 3.600.000,01 a 4.800.000,00  -  19,00 %' as DESCRICAO,
  19.00 as PERCENTUAL
from dual