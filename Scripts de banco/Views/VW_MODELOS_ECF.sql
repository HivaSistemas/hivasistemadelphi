create or replace view VW_MODELOS_ECF as
select
  1 as MODELO_ID,
  'Bematech MP-2100 TH FI' as MODELO_ECF
from 
  dual

union all

select
  2 as MODELO_ID,
  'Bematech MP-3000 TH FI' as MODELO_ECF  
from 
  dual

union all  

select
  3 as MODELO_ID,
  'Bematech MP-4000 TH FI'
from 
  dual

union all

select
  4 as MODELO_ID,
  'Bematech MP-4200 TH FI'
from 
  dual

union all

select
  4 as MODELO_ID,
  'Bematech MP-4200 TH FI II'
from
  dual
  
with read only;