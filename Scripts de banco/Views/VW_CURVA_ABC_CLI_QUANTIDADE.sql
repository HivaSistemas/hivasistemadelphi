create or replace view VW_CURVA_ABC_CLI_QUANTIDADE
as
select
  RES.CLIENTE_ID,
  RES.NOME_CLIENTE,  	
  RES.QUANTIDADE_VENDAS,
  RES.PERCENTUAL,  
  sum(PERCENTUAL) over (order by PERCENTUAL desc) as ACUMULATIVO_PERCENTUAL
from(
	select
		ORC.CLIENTE_ID,
		CAD.NOME_FANTASIA as NOME_CLIENTE,		
		count(ORC.ORCAMENTO_ID) as QUANTIDADE_VENDAS,		    
		round(count(ORC.ORCAMENTO_ID) / sum(count(ORC.ORCAMENTO_ID)) over () * 100, 2) as PERCENTUAL
	from
		ORCAMENTOS ORC
		
	inner join CLIENTES CLI
	on ORC.CLIENTE_ID = CLI.CADASTRO_ID

	inner join CADASTROS CAD
	on CLI.CADASTRO_ID = CAD.CADASTRO_ID
	
	where ORC.STATUS in('RE','GN','EM')

	group by
		ORC.CLIENTE_ID,
		CAD.NOME_FANTASIA		
		
	order by
	  round(count(ORC.ORCAMENTO_ID) / sum(count(ORC.ORCAMENTO_ID)) over () * 100, 2) desc
) RES
  
with read only;