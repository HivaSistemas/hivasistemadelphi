create or replace view VW_ENTREGAS
as
select
  RET.TIPO_MOVIMENTO,
  RET.RETIRADA_ID as MOVIMENTO_ID,
  RET.DATA_HORA_CADASTRO,
  RET.LOCAL_ID,
  LOC.NOME as NOME_LOCAL,
  RET.EMPRESA_ID,
  EMP.NOME_FANTASIA as NOME_EMPRESA,
  RET.ORCAMENTO_ID,
  ORC.VENDEDOR_ID,
  FVE.NOME as NOME_VENDEDOR,
  PES.QTDE_PRODUTOS,
  PES.PESO_TOTAL,
  ORC.CLIENTE_ID,
  CAD.NOME_FANTASIA as NOME_CLIENTE,
  RET.USUARIO_CADASTRO_ID,
  FCA.NOME as NOME_USUARIO_CADASTRO,
  RET.USUARIO_CONFIRMACAO_ID,
  FCO.NOME as NOME_USUARIO_CONFIRMACAO,
  RET.DATA_HORA_RETIRADA as DATA_HORA_ENTREGA,
  RET.NOME_PESSOA_RETIRADA as NOME_PESSOA_RECEBEU
from
  RETIRADAS RET
  
inner join LOCAIS_PRODUTOS LOC
on RET.LOCAL_ID = LOC.LOCAL_ID

inner join EMPRESAS EMP
on RET.EMPRESA_ID = EMP.EMPRESA_ID

inner join ORCAMENTOS ORC
on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID

inner join CADASTROS CAD
on ORC.CLIENTE_ID = CAD.CADASTRO_ID

inner join FUNCIONARIOS FCA
on RET.USUARIO_CADASTRO_ID = FCA.FUNCIONARIO_ID

left join FUNCIONARIOS FCO
on RET.USUARIO_CONFIRMACAO_ID = FCO.FUNCIONARIO_ID

inner join (
  select
    ITE.RETIRADA_ID,
    count(*) as QTDE_PRODUTOS,
    sum(ITE.QUANTIDADE * PRO.PESO) as PESO_TOTAL
  from
    RETIRADAS_ITENS ITE
  inner join PRODUTOS PRO
  on ITE.PRODUTO_ID = PRO.PRODUTO_ID
  group by
    ITE.RETIRADA_ID
) PES
on RET.RETIRADA_ID = PES.RETIRADA_ID

left join FUNCIONARIOS FVE
on ORC.VENDEDOR_ID = FVE.FUNCIONARIO_ID

union all

select
  'E' as TIPO_MOVIMENTO,
  ENT.ENTREGA_ID as MOVIMENTO_ID,
  ENT.DATA_HORA_CADASTRO,
  ENT.LOCAL_ID,
  LOC.NOME as NOME_LOCAL,
  ENT.EMPRESA_ENTREGA_ID as EMPRESA_ID,
  EMP.NOME_FANTASIA as NOME_EMPRESA,
  ENT.ORCAMENTO_ID,
  ORC.VENDEDOR_ID,
  FVE.NOME as NOME_VENDEDOR,
  PES.QTDE_PRODUTOS,
  PES.PESO_TOTAL,
  ORC.CLIENTE_ID,
  CAD.NOME_FANTASIA as NOME_CLIENTE,
  ENT.USUARIO_CADASTRO_ID,
  FCA.NOME as NOME_USUARIO_CADASTRO,
  ENT.USUARIO_CONFIRMACAO_ID,
  FCO.NOME as NOME_USUARIO_CONFIRMACAO,
  ENT.DATA_HORA_REALIZOU_ENTREGA as DATA_HORA_ENTREGA,
  ENT.NOME_PESSOA_RECEBEU_ENTREGA as NOME_PESSOA_RECEBEU
from
  ENTREGAS ENT
  
inner join LOCAIS_PRODUTOS LOC
on ENT.LOCAL_ID = LOC.LOCAL_ID

inner join EMPRESAS EMP
on ENT.EMPRESA_ENTREGA_ID = EMP.EMPRESA_ID

inner join ORCAMENTOS ORC
on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID

inner join CADASTROS CAD
on ORC.CLIENTE_ID = CAD.CADASTRO_ID

inner join FUNCIONARIOS FCA
on ENT.USUARIO_CADASTRO_ID = FCA.FUNCIONARIO_ID

left join FUNCIONARIOS FCO
on ENT.USUARIO_CONFIRMACAO_ID = FCO.FUNCIONARIO_ID

inner join (
  select
    ITE.ENTREGA_ID,
    count(*) as QTDE_PRODUTOS,
    sum(ITE.QUANTIDADE * PRO.PESO) as PESO_TOTAL
  from
    ENTREGAS_ITENS ITE
  inner join PRODUTOS PRO
  on ITE.PRODUTO_ID = PRO.PRODUTO_ID
  group by
    ITE.ENTREGA_ID
) PES
on ENT.ENTREGA_ID = PES.ENTREGA_ID

left join FUNCIONARIOS FVE
on ORC.VENDEDOR_ID = FVE.FUNCIONARIO_ID;