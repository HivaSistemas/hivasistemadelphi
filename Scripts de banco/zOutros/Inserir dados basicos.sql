declare

  procedure VER_FUNCAO(pCodigo in number, pNome in string) 
  is
  declare
    vQtde number;
  begin
  
    select count(*)
    into vQtde
    from FUNCOES_FUNCIONARIOS
    where FUNCAO_ID = pCODIGO;
    
    if vQtde = 0 then    
      insert into FUNCOES_FUNCIONARIOS(
        FUNCAO_ID,
        NOME,
        ATIVO
      ) values (
        pCodigo,
        pNome,
        'S'
      );  
    end if;
  
  end;

begin
  
  VER_FUNCAO(1, 'GERENTE');
  VER_FUNCAO(2, 'VENDEDOR');
  VER_FUNCAO(3, 'CAIXA');
  VER_FUNCAO(4, 'ADMINISTRACAO');
  

end;