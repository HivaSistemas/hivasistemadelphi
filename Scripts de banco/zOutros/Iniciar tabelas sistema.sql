begin

  insert into FUNCOES_FUNCIONARIOS(
    FUNCAO_ID,
    NOME,
    ATIVO
  )values(
    SEQ_FUNCAO_ID.nextval,
    'GERAL',
    'S'
  );
  
  insert into ESTADOS(
    ESTADO_ID,
    NOME,
    ATIVO
  )values(
    'GO',
    'GOIÁS',
    'S'
  );
  
  insert into CIDADES(
    CIDADE_ID,
    NOME,
    ESTADO_ID,
    ATIVO
  )values(
    SEQ_CIDADE_ID.nextval,
    'GOIÂNIA',
    'GO',  
    'S'
  );
  
  insert into EMPRESAS(
    EMPRESA_ID,
    RAZAO_SOCIAL,
    NOME_FANTASIA,
    CNPJ,
    LOGRADOURO,
    COMPLEMENTO,  
    BAIRRO_ID,  
    CEP,  
    INSCRICAO_ESTADUAL
  )values(
    SEQ_EMPRESA_ID.nextval,
    'LOJA 1 LTDA',
    'LOJA 1',
    '11.111.111/0001-11',
    'GERAL',
    'GERAL',
    3,
    '74000-000',
    'ISENTO'
  );
  
  insert into PARAMETROS_EMPRESA(EMPRESA_ID, REGIME_TRIBUTARIO, ALIQUOTA_SIMPLES_NACIONAL)values(1, 'SN', 4);
  
  INSERT INTO CADASTROS(
    CADASTRO_ID,
    NOME_FANTASIA,  
    TIPO_PESSOA,
    CPF_CNPJ,  
    BAIRRO_ID
  )VALUES(
    1,
    'CONSUMIDOR FINAL',
    'F',
    '111.111.111-11',
    3
  );
    
  INSERT INTO CLIENTES(
    CADASTRO_ID,    	
    RESTRICAO_SERASA,
    RESTRICAO_SPC
  )VALUES(
    1,
    'N',
    'N'
  ); 
  
  INSERT INTO PARAMETROS(
    CADASTRO_CONSUMIDOR_FINAL_ID,  
    USUARIO_GERENTE_SISTEMA_ID
  )VALUES(
    1,
    1
  );
  
end;