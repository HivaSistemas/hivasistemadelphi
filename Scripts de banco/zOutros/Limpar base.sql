declare
  
  cursor cTriggers is
  select
    TRIGGER_NAME
  from
    USER_TRIGGERS;

begin

  for xTrig in cTriggers loop
    execute immediate 'alter trigger ' || xTrig.TRIGGER_NAME || ' disable';
  end loop;

  delete from MENSAGENS;
  
  delete from ORCAMENTOS_CREDITOS;
  delete from MOVIMENTOS_TURNOS_ITENS;
  delete from MOVIMENTOS_TURNOS;
  delete from MOVIMENTOS_CONTAS;
  
  delete from CONTAS_PAGAR_BAIXAS_PAGTOS_DIN;
  
  delete from CONTAS_RECEBER
  where BAIXA_PAGAR_ORIGEM_ID is not null;
  
  delete from CONTAS_PAGAR
  where BAIXA_ORIGEM_ID is not null;  
  
  delete from CONTAS_PAGAR_BAIXAS_ITENS;
  delete from CONTAS_PAGAR_BAIXAS;
  delete from CONTAS_PAGAR;
  
  delete from CONTAS_PAGAR
  where BAIXA_RECEBER_ORIGEM_ID is not null;

  delete from CONTAS_REC_BAIXAS_PAGTOS_DIN;  
  
  update CONTAS_RECEBER set
    BAIXA_ID = null,
    STATUS = 'A';  
  
  delete from CONTAS_RECEBER_BAIXAS_ITENS;
  delete from CONTAS_RECEBER_BAIXAS;
  delete from CONTAS_RECEBER;
  
  update ENTREGAS set 
    CONTROLE_ENTREGA_ID = null;  
  
  delete from CONTROLES_ENTREGAS_ITENS;
  delete from CONTROLES_ENTREGAS;  
  
  delete from NOTAS_FISCAIS_ITENS;
  delete from NOTAS_FISCAIS;

  delete from MOVIMENTOS_CREDITOS_CLIENTES;
  
  delete from COMPRAS_ITENS_BAIXAS;
  delete from COMPRAS_BAIXAS;
  delete from COMPRAS_ITENS;
  delete from COMPRAS_PREVISOES;
  delete from COMPRAS;
  
  delete from DEVOLUCOES_ITENS_ENTREGAS;
  delete from DEVOLUCOES_ITENS;
  delete from DEVOLUCOES;
  
  delete from ENTREGAS_ITENS;
  delete from ENTREGAS;

  delete from RETIRADAS_ITENS;
  delete from RETIRADAS;
  
  delete from ENTREGAS_ITENS_PENDENTES;
  delete from ENTREGAS_PENDENTES;
  
  delete from RETIRADAS_ITENS_PENDENTES;
  delete from RETIRADAS_PENDENTES;
  
  delete from ENTREGAS_ITENS_SEM_PREVISAO;
  
  delete from ORCAMENTOS_ITENS;
  delete from ORCAMENTOS_PAGAMENTOS;
  delete from ORCAMENTOS_PAGAMENTOS_CHEQUES;
  delete from ORCAMENTOS_BLOQUEIOS;  
  delete from ORCAMENTOS_TRAMITES;
  delete from ORCAMENTOS;
  
  delete from ESTOQUES_DIVISAO
  where EMPRESA_ID <> 1;
  
  delete from ESTOQUES
  where EMPRESA_ID <> 1;
  
  update ESTOQUES_DIVISAO set    
    FISICO = 0,
    DISPONIVEL = 0,
    RESERVADO = 0;
    
  update ESTOQUES set
    ESTOQUE = 0,
    FISICO = 0,
    DISPONIVEL = 0,
    RESERVADO = 0;   

  delete from TURNOS_CAIXA;
  
  delete from PRECOS_PRODUTOS;
  delete from CUSTOS_PRODUTOS;
  delete from PRODUTOS_ICMS;
  delete from PRODUTOS_ICMS_COMPRA;

  delete from AJUSTES_ESTOQUE_ITENS;  
  delete from AJUSTES_ESTOQUE;
  
  delete from TRANSFERENCIAS_LOCAIS_ITENS;
  delete from TRANSFERENCIAS_LOCAIS;
  
  delete from ESTOQUES_DIVISAO;
  delete from ESTOQUES;
  
  delete from ENTRADAS_NOTAS_FISC_FINANCEIRO;
  delete from ENTRADAS_NOTAS_FISCAIS_ITENS;
  delete from ENTRADAS_NOTAS_FISCAIS;
  
  delete from PRODUTOS_FORN_COD_ORIGINAIS;
  delete from PRODUTOS_RELACIONADOS;
  delete from PRODUTOS_MULTIPLOS_COMPRA;
  delete from PRODUTOS_PROMOCOES;
  delete from PRODUTOS_KIT;
  delete from PRODUTOS_LOTES;
  delete from LOGS_PRODUTOS;
  delete from LOGS_PRECOS_PRODUTOS;
  delete from PRODUTOS_ORDENS_LOC_ENTREGAS;
  delete from PRODUTOS;
     
  delete from CONHEC_FRETES_FINANCEIROS;
  delete from CONHECIMENTOS_FRETES_ITENS;
  delete from CONHECIMENTOS_FRETES;
  
  delete from CLIENTES_CONDIC_PAGTO_RESTRIT;
  
  update EMPRESAS set
    CADASTRO_ID = null;
    
  delete from AUTORIZACOES
  where FUNCIONARIO_ID <> 1;
  
  delete from FUNCIONARIOS
  where FUNCIONARIO_ID <> 1;    
    
  update FUNCIONARIOS set 
    CADASTRO_ID = 1;
  
  delete from LOGS_CADASTROS;
  delete from LOGS_CLIENTES;
  delete from LOGS_FORNECEDORES;
  delete from LOGS_MOTORISTAS;
  delete from LOGS_TELAS;
  
  delete from MOTORISTAS
  where CADASTRO_ID <> 1;  
  
  delete from TRANSPORTADORAS
  where CADASTRO_ID <> 1;
  
  delete from CLIENTES
  where CADASTRO_ID <> 1;
  
  delete from FORNECEDORES
  where CADASTRO_ID <> 1;
  
  delete from CADASTROS
  where CADASTRO_ID <> 1;
  
  delete from BAIRROS
  where BAIRRO_ID <> 1;
  
  delete from CIDADES
  where CIDADE_ID <> 1;  
  
  delete from LOGS_TIPOS_COBRANCA;
  delete from TIPOS_COBR_COND_PAGAMENTO;
  delete from TIPOS_COBRANCA;
  
  update PARAMETROS_EMPRESA set
    COND_PAGTO_PADRAO_PESQ_VENDA = null,
    CONDICAO_PAGTO_PAD_VENDA_ASSIS = null,
    CONDICAO_PAGAMENTO_PDV = null,
    CONDICAO_PAGAMENTO_CONS_PROD_2 = null,
    CONDICAO_PAGAMENTO_CONS_PROD_3 = null,
    CONDICAO_PAGAMENTO_CONS_PROD_4 = null;
  
  delete from LOGS_CONDICOES_PAGAMENTO;
  delete from CONDICOES_PAGAMENTO_PRAZOS;
  delete from CONDICOES_PAGAMENTO; 
  
  delete from GRUPOS_ESTOQUES
  where EMPRESA_GRUPO_ID <> 1;  
  
  delete from GRUPOS_ESTOQUES
  where EMPRESA_ID <> 1;  

  delete from PARAMETROS_EMPRESA 
  where EMPRESA_ID <> 1;  
  
  delete from EMPRESAS 
  where EMPRESA_ID <> 1;

  for xSeq in (
    select
      SEQUENCE_NAME,
      MIN_VALUE,
      MAX_VALUE
    from
      USER_SEQUENCES
    where LAST_NUMBER > 1
  ) loop
    execute immediate 'drop sequence ' || xSeq.SEQUENCE_NAME;
    
    execute immediate 
      'create sequence ' || xSeq.SEQUENCE_NAME || ' ' ||
      'start with 1 increment by 1 ' ||
      'minvalue 1 ' || 
      'maxvalue ' || xSeq.MAX_VALUE || ' ' ||
      'nocycle ' ||
      'nocache ' ||
      'noorder';
      
  end loop;
  
  for xTrig in cTriggers loop
    execute immediate 'alter trigger ' || xTrig.TRIGGER_NAME || ' enable';
  end loop;
  
  execute immediate 'purge recyclebin';
    
end;
