begin

  for x in( select TABLE_NAME from user_tables ) loop
    execute immediate 'ANALYZE TABLE ' || x.TABLE_NAME || ' COMPUTE STATISTICS';
  end loop;

end;