declare

  procedure VER_ESTADO(pEstadoId in ESTADOS.ESTADO_ID%type, pNomeEstado in ESTADOS.NOME%type) is
  begin
    insert into ESTADOS(
      ESTADO_ID,
      NOME,
      ATIVO
    )values(
      pEstadoId,
      pNomeEstado,
      'S'
    );    
  end;

begin

  --VER_ESTADO('GO', 'GOIÁS');
  --VER_ESTADO('DF', 'DISTRITO FEDERAL');
  VER_ESTADO('AC', 'ACRE');
  VER_ESTADO('AL', 'ALAGOAS');
  VER_ESTADO('AP', 'AMAPÁ');
  VER_ESTADO('AM', 'AMAZONAS');  
  VER_ESTADO('BA', 'BAHIA');
  VER_ESTADO('CE', 'CEARÁ');
  VER_ESTADO('ES', 'ESPÍRITO SANTO');
  VER_ESTADO('MA', 'MARANHÃO');  
  VER_ESTADO('MT', 'MATO GROSSO');
  VER_ESTADO('MS', 'MATO GROSSO DO SUL');
  VER_ESTADO('MG', 'MINAS GERAIS');
  VER_ESTADO('PA', 'PARÁ');
  VER_ESTADO('PB', 'PARAÍBA');
  VER_ESTADO('PR', 'PARANÁ');
  VER_ESTADO('PE', 'PERNAMBUCO');
  VER_ESTADO('PI', 'PIAUÍ');
  VER_ESTADO('RJ', 'RIO DE JANEIRO');
  VER_ESTADO('RN', 'RIO GRANDE DO NORTE');
  VER_ESTADO('RS', 'RIO GRANDE DO SUL');
  VER_ESTADO('RO', 'RONDÔNIA');
  VER_ESTADO('RR', 'RORAÍMA');
  VER_ESTADO('SC', 'SANTA CATARINA');
  VER_ESTADO('SP', 'SÃO PAULO');
  VER_ESTADO('SE', 'SERGIPE');
  VER_ESTADO('TO', 'TOCANTINS'); 

end;