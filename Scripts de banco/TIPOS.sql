create or replace package TIPOS 
as 

  type ArrayOfNumeros is table of number index by positiven;
  type ArrayOfString is table of varchar2(100) index by positiven;
  type ArrayOfData is table of date index by positiven;

end TIPOS;
/