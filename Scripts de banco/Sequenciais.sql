create sequence SEQ_DESENVOLVIMENTO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 99999
nocycle
nocache
noorder;

create sequence SEQ_CIDADE_ID
start with 1 increment by 1
minvalue 1 
maxvalue 99999
nocycle
nocache
noorder;

create sequence SEQ_BAIRRO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 99999
nocycle
nocache
noorder;

create sequence SEQ_COBRANCA_ID
start with 1 increment by 1
minvalue 1 
maxvalue 99999
nocycle
nocache
noorder;

create sequence SEQ_CONDICAO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 1000
nocycle
nocache
noorder;

create sequence SEQ_CARGO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 1000
nocycle
nocache
noorder;

create sequence SEQ_PROFISSIONAIS_CARGO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 1000
nocycle
nocache
noorder;

create sequence SEQ_FUNCIONARIO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999
nocycle
nocache
noorder;

create sequence SEQ_LOCAL_ID
start with 1 increment by 1
minvalue 1 
maxvalue 99999
nocycle
nocache
noorder;

create sequence SEQ_CADASTRO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999
nocycle
nocache
noorder;

create sequence SEQ_MARCA_ID
start with 1 increment by 1
minvalue 1 
maxvalue 99999
nocycle
nocache
noorder;

create sequence SEQ_PRODUTO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 99999999
nocycle
nocache
noorder;

create sequence SEQ_EMPRESA_ID
start with 1 increment by 1
minvalue 1 
maxvalue 1000
nocycle
nocache
noorder;

create sequence SEQ_ORCAMENTO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_MOVIMENTO_TURNO_ID
start with 1 increment by 1
minvalue 1
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_TURNO_ID
start with 1 increment by 1
minvalue 1
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_NOTA_FISCAL_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_RECEBER_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_LOTE_EVENTOS_NFE_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_MOVIMENTOS_PRODUTOS_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_GERENCIADOR_CARTAO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_MOTIVO_AJUSTE_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_AJUSTE_ESTOQUE_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_PRODUCAO_ESTOQUE_ID
start with 1 increment by 1
minvalue 1
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_ENTRADA_NOTAS_FISC_ENT_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_DEVOLUCAO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_MOV_CREDITOS_CLIENTES
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_CONTAS_RECEBER_BAIXAS
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_PAGAR_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_CONTAS_PAGAR_BAIXAS
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_RETIRADAS_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_MENSAGENS_ID
start with 1 increment by 1
minvalue 1
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_AGRUPADOR_MENSAGENS
start with 1 increment by 1
minvalue 1
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_PRODUTOS_DEPARTAMENTOS_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_PRODUTOS_SECOES_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_PRODUTOS_LINHAS_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_ENTREGAS_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_AGRUP_ENTREGAS_ID
start with 1 increment by 1
minvalue 1
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_AGRUP_RETIRADAS_ID
start with 1 increment by 1
minvalue 1
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_ROTA_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_CENTRO_CUSTO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_TRANSFERENCIA_LOCAL_ID
start with 1 increment by 1
minvalue 1 
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_VEICULO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 99999
nocycle
nocache
noorder;

create sequence SEQ_CONTROLE_ENTREGA_ID
start with 1 increment by 1
minvalue 1 
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_COMPRA_ID
start with 1 increment by 1
minvalue 1 
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_CONHECIMENTO_ID
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_COMPRAS_BAIXA_ID
start with 1 increment by 1
minvalue 1 
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_MOVIMENTOS_CONTAS
start with 1 increment by 1
minvalue 1 
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_GRUPOS_FORNECEDORES
start with 1 increment by 1
minvalue 1 
maxvalue 9999
nocycle
nocache
noorder;

create sequence SEQ_OUTRAS_NOTAS
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_REMESSAS_COBRANCAS
start with 1 increment by 1
minvalue 1 
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_ACUMULADOS
start with 1 increment by 1
minvalue 1 
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_GRUPOS_TRIB_EST_COMPRA
start with 1 increment by 1
minvalue 1 
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_GRUPOS_TRIB_EST_VENDA
start with 1 increment by 1
minvalue 1 
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_GRUPOS_TRIB_FEDERAL_COMPRA
start with 1 increment by 1
minvalue 1 
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_GRUPOS_TRIB_FEDERAL_VENDA
start with 1 increment by 1
minvalue 1 
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_INDICES_DESCONTOS_VENDA
start with 1 increment by 1
minvalue 1 
maxvalue 9
nocycle
nocache
noorder;

create sequence SEQ_CLIENTES_ORIGENS_ID
start with 1 increment by 1
minvalue 1 
maxvalue 99999
nocycle
nocache
noorder;

create sequence SEQ_CLIENTES_GRUPOS_ID
start with 1 increment by 1
minvalue 1 
maxvalue 99999
nocycle
nocache
noorder;

create sequence SEQ_GRUPOS_FINANCEIROS_ID
start with 1 increment by 1
minvalue 1 
maxvalue 99999
nocycle
nocache
noorder;

create sequence SEQ_TRANSF_PROD_EMPRESAS
start with 1 increment by 1
minvalue 1 
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_BLOQUEIOS_ESTOQUES
start with 1 increment by 1
minvalue 1 
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_ARQUIVOS
start with 1 increment by 1
minvalue 1 
maxvalue 99999999999999999999
nocycle
nocache
noorder;

create sequence SEQ_ENTRADAS_NF_SERVICOS
start with 1 increment by 1
minvalue 1
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_PRE_ENTRADAS
start with 1 increment by 1
minvalue 1
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_PRE_ENTRADAS_CTE
start with 1 increment by 1
minvalue 1
maxvalue 9999999999
nocycle
nocache
noorder;

create sequence SEQ_ADMINISTRADORAS_CARTOES
start with 1 increment by 1
minvalue 1
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_LISTA_NEGRA
start with 1 increment by 1
minvalue 1
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_CONTAS_CUSTODIA
start with 1 increment by 1
minvalue 1
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_BLOQUEIOS_ESTOQUES_BAIXAS
start with 1 increment by 1
minvalue 1
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_DEVOLUCOES_ENT_NOTAS_FISC
start with 1 increment by 1
minvalue 1
maxvalue 999999999999
nocycle
nocache
noorder;

create sequence SEQ_END_EST_RUA_ID
start with 1 increment by 1
minvalue 1
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_END_EST_MOD_ID
start with 1 increment by 1
minvalue 1
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_END_EST_VAO_ID
start with 1 increment by 1
minvalue 1
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_END_EST_NIV_ID
start with 1 increment by 1
minvalue 1
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_END_ESTOQUE_ID
start with 1 increment by 1
minvalue 1
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_CRM_ID
start with 1 increment by 1
minvalue 1
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_CRM_OCORRENCIAS_ID
start with 1 increment by 1
minvalue 1
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_TIP_ACO_ORC_ID
start with 1 increment by 1
minvalue 2
maxvalue 999
nocycle
nocache
noorder;

create sequence SEQ_TIP_BLO_EST_ID
start with 2 increment by 1
minvalue 2
maxvalue 999
nocycle
nocache
noorder;
