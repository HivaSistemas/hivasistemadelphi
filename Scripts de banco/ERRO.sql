create or replace procedure ERRO(
	iMENSAGEM_ERRO in string
)
is 
begin	  
  /* Procedimento para gerar exceção no banco de dados e parar execução */
  Raise_Application_Error(-20000, iMENSAGEM_ERRO);  
end ERRO;