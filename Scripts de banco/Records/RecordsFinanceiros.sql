﻿create or replace package RecordsFinanceiros
as
  
  type RecParcelasReceber is record(
    DATA_VENCIMENTO     CONTAS_RECEBER.DATA_VENCIMENTO%type,
    VALOR_PARCELA       CONTAS_RECEBER.VALOR_DOCUMENTO%type,
    PARCELA             CONTAS_RECEBER.PARCELA%type,
    NUMERO_PARCELAS     CONTAS_RECEBER.NUMERO_PARCELAS%type,
    COBRANCA_ID         CONTAS_RECEBER.COBRANCA_ID%type,
    ITEM_ID             ORCAMENTOS_PAGAMENTOS.ITEM_ID%type,
    TIPO_CARTAO         TIPOS_COBRANCA.TIPO_CARTAO%type,
    PORTADOR_ID         PORTADORES.PORTADOR_ID%type,
    PLANO_FINANCEIRO_ID PLANOS_FINANCEIROS.PLANO_FINANCEIRO_ID%type
  );

  /* --------- Declaração dos dos tipos acima na forma de Arrays ------------- */
  type ArrayOfRecParcelasReceber is table of RecParcelasReceber index by naturaln;
    
end RecordsFinanceiros;