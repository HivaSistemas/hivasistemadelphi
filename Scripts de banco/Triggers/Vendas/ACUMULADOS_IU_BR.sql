create or replace trigger ACUMULADOS_IU_BR
before insert or update
on ACUMULADOS
for each row
begin    

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
    :new.USUARIO_FECHAMENTO_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.DATA_HORA_FECHAMENTO  := sysdate;
  else
    if :old.STATUS = 'AR' and :new.STATUS = 'RE' then
      :new.USUARIO_RECEBIMENTO_ID := SESSAO.USUARIO_SESSAO_ID;
      :new.DATA_HORA_RECEBIMENTO  := sysdate;
    elsif :old.STATUS <> :new.STATUS and :new.STATUS = 'CA' then
      :new.USUARIO_CANCELAMENTO_ID := SESSAO.USUARIO_SESSAO_ID;
      :new.DATA_HORA_CANCELAMENTO  := sysdate;
    end if;
  end if;
  
end ACUMULADOS_IU_BR;
/