create or replace trigger ORCAMENTOS_IU_AR
after insert or update
on ORCAMENTOS
for each row
begin    
  
  /* Gerando a tramitação do orçamento */
  if nvl(:old.STATUS, 'X') <> nvl(:new.STATUS, 'X') then
    insert into ORCAMENTOS_TRAMITES(ORCAMENTO_ID, STATUS, VALOR_TOTAL)
    values(:new.ORCAMENTO_ID, :new.STATUS, :new.VALOR_TOTAL);
  end if;
  
end ORCAMENTOS_IU_AR;
/