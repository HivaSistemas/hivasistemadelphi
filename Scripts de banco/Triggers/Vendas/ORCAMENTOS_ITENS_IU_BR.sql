create or replace trigger ORCAMENTOS_ITENS_IU_BR
before insert or update
on ORCAMENTOS_ITENS
for each row
declare
  vEmpresaId ORCAMENTOS.EMPRESA_ID%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then
    if :new.TIPO_CONTROLE_ESTOQUE in('K', 'A') then   --KIT E KIT AGRUPADO
      :new.PRECO_FINAL         := 0;
      :new.PRECO_FINAL_MEDIO   := 0;
      :new.PRECO_LIQUIDO       := 0;
      :new.PRECO_LIQUIDO_MEDIO := 0;
      :new.CMV                 := 0;
      :new.CUSTO_PEDIDO_MEDIO  := 0;
      :new.CUSTO_ULTIMO_PEDIDO := 0;
    elsif :new.TIPO_CONTROLE_ESTOQUE in('I') then  --PRODU��O
      select
        EMPRESA_ID
      into
        vEmpresaId
      from
        ORCAMENTOS
      where ORCAMENTO_ID = :new.ORCAMENTO_ID;

      select
        round(CUS.PRECO_FINAL * :new.QUANTIDADE, 2),
        round(CUS.PRECO_FINAL_MEDIO * :new.QUANTIDADE, 2),
        round(CUS.PRECO_LIQUIDO * :new.QUANTIDADE, 2),
        round(CUS.PRECO_LIQUIDO_MEDIO * :new.QUANTIDADE, 2),
        round(CUS.CMV * :new.QUANTIDADE, 2),
        round(CUS.CUSTO_PEDIDO_MEDIO * :new.QUANTIDADE, 2),
        round(
          case PAE.TIPO_CUSTO_VIS_LUCRO_VENDA
            when 'FIN' then zvl(zvl(CUS.PRECO_LIQUIDO, CUS.CUSTO_ULTIMO_PEDIDO), BUSCAR_CUSTO_COMPRA_PRODUTO(PAE.EMPRESA_ID, CUS.PRODUTO_ID))
            when 'COM' then zvl(zvl(CUS.CUSTO_ULTIMO_PEDIDO, CUS.PRECO_LIQUIDO), BUSCAR_CUSTO_COMPRA_PRODUTO(PAE.EMPRESA_ID, CUS.PRODUTO_ID))
            else CUS.CMV
          end * :new.QUANTIDADE,
        2)
      into
        :new.PRECO_FINAL,
        :new.PRECO_FINAL_MEDIO,
        :new.PRECO_LIQUIDO,
        :new.PRECO_LIQUIDO_MEDIO,
        :new.CMV,
        :new.CUSTO_PEDIDO_MEDIO,
        :new.CUSTO_ULTIMO_PEDIDO
      from
        VW_CUSTOS_PRODUTOS CUS

      inner join PARAMETROS_EMPRESA PAE
      on CUS.EMPRESA_ID = PAE.EMPRESA_ID

      where CUS.PRODUTO_ID = :new.PRODUTO_ID
      and CUS.EMPRESA_ID = vEmpresaId;
    else
      select
        EMPRESA_ID
      into
        vEmpresaId
      from
        ORCAMENTOS
      where ORCAMENTO_ID = :new.ORCAMENTO_ID;

      select
        round(CUS.PRECO_FINAL * :new.QUANTIDADE, 2),
        round(CUS.PRECO_FINAL_MEDIO * :new.QUANTIDADE, 2),
        round(CUS.PRECO_LIQUIDO * :new.QUANTIDADE, 2),
        round(CUS.PRECO_LIQUIDO_MEDIO * :new.QUANTIDADE, 2),
        round(CUS.CMV * :new.QUANTIDADE, 2),
        round(CUS.CUSTO_PEDIDO_MEDIO * :new.QUANTIDADE, 2),
        round(CUS.CUSTO_ULTIMO_PEDIDO * :new.QUANTIDADE, 2)
      into
        :new.PRECO_FINAL,
        :new.PRECO_FINAL_MEDIO,
        :new.PRECO_LIQUIDO,
        :new.PRECO_LIQUIDO_MEDIO,
        :new.CMV,
        :new.CUSTO_PEDIDO_MEDIO,
        :new.CUSTO_ULTIMO_PEDIDO
      from
        VW_CUSTOS_PRODUTOS CUS

      where CUS.PRODUTO_ID = :new.PRODUTO_ID
      and CUS.EMPRESA_ID = vEmpresaId;
    end if;
  end if;

end ORCAMENTOS_ITENS_IU_BR;
/
