﻿create or replace trigger DEVOLUCOES_ITENS_D_BR
before delete
on DEVOLUCOES_ITENS
for each row
declare

  vOrcamentoId     ORCAMENTOS.ORCAMENTO_ID%type;
  vQtdeUtilizar    number default 0;
  vQtdeRestante    number;

  cursor cPendencias(pORCAMENTO_ID in number) is
  select
    'R' as TIPO,
    EMPRESA_ID,
    LOCAL_ID,
    ITEM_ID,
    LOTE,
    DEVOLVIDOS as SALDO,
    null as PREVISAO_ENTREGA
  from
    RETIRADAS_ITENS_PENDENTES
  where ORCAMENTO_ID = pORCAMENTO_ID  
  and ITEM_ID = :old.ITEM_ID

  union all

  select
    'E' as TIPO,
    EMPRESA_ID,
    LOCAL_ID,
    ITEM_ID,
    LOTE,
    DEVOLVIDOS as SALDO,
    PREVISAO_ENTREGA
  from
    ENTREGAS_ITENS_PENDENTES
  where ORCAMENTO_ID = pORCAMENTO_ID
  and ITEM_ID = :old.ITEM_ID;

begin

  select
    ORCAMENTO_ID
  into
    vOrcamentoId
  from
    DEVOLUCOES
  where DEVOLUCAO_ID = :old.DEVOLUCAO_ID;

  /* Voltando as pendências (Retirar e entregar) */
  if :old.DEVOLVIDOS_PENDENCIAS > 0 then
    vQtdeRestante := :old.DEVOLVIDOS_PENDENCIAS;
    for xPend in cPendencias(vOrcamentoId) loop
      if xPend.SALDO >= vQtdeRestante then
        vQtdeUtilizar := vQtdeRestante;
        vQtdeRestante := 0;
      else
        vQtdeUtilizar := xPend.SALDO;
        vQtdeRestante := vQtdeRestante - vQtdeUtilizar;
      end if;

      if xPend.TIPO = 'R' then
        update RETIRADAS_ITENS_PENDENTES set
          DEVOLVIDOS = DEVOLVIDOS - vQtdeUtilizar
        where ORCAMENTO_ID = vOrcamentoId
        and EMPRESA_ID = xPend.EMPRESA_ID
        and LOCAL_ID = xPend.LOCAL_ID
        and ITEM_ID = xPend.ITEM_ID
        and LOTE = xPend.LOTE;
      else
        update ENTREGAS_ITENS_PENDENTES set
          DEVOLVIDOS = DEVOLVIDOS - vQtdeUtilizar
        where ORCAMENTO_ID = vOrcamentoId
        and EMPRESA_ID = xPend.EMPRESA_ID
        and LOCAL_ID = xPend.LOCAL_ID
        and ITEM_ID = xPend.ITEM_ID
        and LOTE = xPend.LOTE
        and PREVISAO_ENTREGA = xPend.PREVISAO_ENTREGA;
      end if;

      /* Se não tem mais nada a devolver, saindo do loop */
      if vQtdeRestante = 0 then
        exit;
      end if;

    end loop;

    if vQtdeRestante > 0 then
      ERRO('Não existe saldo suficiente para realizar a devolução, verifique as pendências!');
    end if;
  end if;

  /* Devolvendo os itens sem previsão */
  if :old.DEVOLVIDOS_SEM_PREVISAO > 0 then
    update ENTREGAS_ITENS_SEM_PREVISAO set
      DEVOLVIDOS = DEVOLVIDOS - :old.DEVOLVIDOS_SEM_PREVISAO
    where ORCAMENTO_ID = vOrcamentoId
    and ITEM_ID = :old.ITEM_ID;
  end if;

end DEVOLUCOES_ITENS_D_BR;
/