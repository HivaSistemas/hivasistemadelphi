create or replace trigger ACUMULADOS_ORCAMENTOS_IU_BR
before insert or update
on ACUMULADOS_ORCAMENTOS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
    update ORCAMENTOS set
      ACUMULADO_ID = :new.ACUMULADO_ID
    where ORCAMENTO_ID = :new.ORCAMENTO_ID;
  end if;

end ACUMULADOS_ORCAMENTOS_IU_BR;
/