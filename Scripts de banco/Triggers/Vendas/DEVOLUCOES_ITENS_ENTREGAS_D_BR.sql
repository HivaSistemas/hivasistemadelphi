create or replace trigger DEVOLUCOES_ITENS_ENTREGAS_D_BR
before delete
on DEVOLUCOES_ITENS_ENTREGAS
for each row
declare
  vEmpresaId   number;
  vProdutoId   ORCAMENTOS_ITENS.PRODUTO_ID%type;
  vOrcamentoId DEVOLUCOES.ORCAMENTO_ID%type;
begin
  
  select
    EMPRESA_ID,
    ORCAMENTO_ID
  into
    vEmpresaId,
    vOrcamentoId
  from
    DEVOLUCOES
  where DEVOLUCAO_ID = :old.DEVOLUCAO_ID;
  
  select
    PRODUTO_ID
  into
    vProdutoId
  from
    ORCAMENTOS_ITENS
  where ORCAMENTO_ID = vOrcamentoId
  and ITEM_ID = :old.ITEM_ID;

  update ENTREGAS_ITENS set
    DEVOLVIDOS = DEVOLVIDOS - :old.QUANTIDADE
  where ENTREGA_ID = :old.ENTREGA_ID
  and ITEM_ID = :old.ITEM_ID
  and LOTE = :old.LOTE;

  MOVIMENTAR_ESTOQUE(
    vEmpresaId,
    vProdutoId,
    :old.LOCAL_DEVOLUCAO_ID,
    :old.LOTE,
    :old.QUANTIDADE,
    'CDE',
    null,
    null
  );

end DEVOLUCOES_ITENS_ENTREGAS_D_BR;
/