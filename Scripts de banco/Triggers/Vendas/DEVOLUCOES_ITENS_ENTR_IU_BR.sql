create or replace trigger DEVOLUCOES_ITENS_ENTR_IU_BR
before insert or update
on DEVOLUCOES_ITENS_ENTREGAS
for each row
declare
  vEmpresaId   number;
  vProdutoId   ORCAMENTOS_ITENS.PRODUTO_ID%type;
  vOrcamentoId DEVOLUCOES.ORCAMENTO_ID%type;
  vDevolucaoSomenteFiscal char default 'N';
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then

    select
      NVL(DEVOLUCAO_SOMENTE_FISCAL, 'N')
    into
      vDevolucaoSomenteFiscal
    from
      FUNCIONARIOS
    where FUNCIONARIO_ID = SESSAO.USUARIO_SESSAO_ID;

    if vDevolucaoSomenteFiscal = 'S' then
      return;
    end if;

    select
      EMPRESA_ID,
      ORCAMENTO_ID
    into
      vEmpresaId,
      vOrcamentoId
    from
      DEVOLUCOES
    where DEVOLUCAO_ID = :new.DEVOLUCAO_ID;
    
    select
      PRODUTO_ID
    into
      vProdutoId
    from
      ORCAMENTOS_ITENS
    where ORCAMENTO_ID = vOrcamentoId
    and ITEM_ID = :new.ITEM_ID;

    update ENTREGAS_ITENS set
      DEVOLVIDOS = DEVOLVIDOS + :new.QUANTIDADE
    where ENTREGA_ID = :new.ENTREGA_ID
    and ITEM_ID = :new.ITEM_ID
    and LOTE = :new.LOTE;

    MOVIMENTAR_ESTOQUE(
      vEmpresaId,
      vProdutoId,
      :new.LOCAL_DEVOLUCAO_ID,
      :new.LOTE,
      :new.QUANTIDADE,
      'DEV',
      null,
      null
    );
  end if;

end DEVOLUCOES_ITENS_ENTR_IU_BR;
/