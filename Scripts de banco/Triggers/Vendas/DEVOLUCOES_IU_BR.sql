create or replace trigger DEVOLUCOES_IU_BR
before insert or update
on DEVOLUCOES
for each row
declare
  vAcumuladoNaoRecebido char(1) default 'N';
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if SESSAO.ROTINA = 'DESVIAR' then
    return;
  end if;

  if :old.STATUS = 'B' then
    ERRO('N�o � permitido alterar devolu��es j� baixadas!');
  end if;

  select
    case when nvl(ACU.STATUS, 'X') = 'AR' then 'S' else 'N' end
  into
    vAcumuladoNaoRecebido
  from
    ORCAMENTOS ORC

  left join ACUMULADOS ACU
  on ORC.ACUMULADO_ID = ACU.ACUMULADO_ID

  where ORC.ORCAMENTO_ID = :new.ORCAMENTO_ID;

  if vAcumuladoNaoRecebido = 'S' then
    ERRO('Este pedido que est� sendo devolvido faz parte de um acumulado ainda n�o recebido, por favor realize o recebimento do acumulado!');
  end if;

  if inserting then
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.DATA_HORA_DEVOLUCAO := sysdate;
  else
    if :old.STATUS = 'A' and :new.STATUS = 'B' then
      :new.DATA_HORA_CONFIRMACAO  := sysdate;
      :new.USUARIO_CONFIRMACAO_ID := SESSAO.USUARIO_SESSAO_ID;
    end if;
  end if;

end DEVOLUCOES_IU_BR;
/