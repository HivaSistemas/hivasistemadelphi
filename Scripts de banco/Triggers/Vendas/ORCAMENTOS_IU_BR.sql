﻿create or replace trigger ORCAMENTOS_IU_BR
before insert or update
on ORCAMENTOS
for each row
declare
  vQtdeBloqueios   number;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.TIPO_ACOMPANHAMENTO_ID := 1;
  elsif updating then
    if :old.STATUS <> :new.STATUS then
      if :new.STATUS = 'RE' then
        :new.DATA_HORA_RECEBIMENTO := sysdate;
      elsif :new.STATUS = 'VR' then
        select count(*)
        into vQtdeBloqueios
        from ORCAMENTOS_BLOQUEIOS
        where ORCAMENTO_ID = :new.ORCAMENTO_ID
        and USUARIO_LIBERACAO_ID is null;

        if vQtdeBloqueios > 0 then
          ERRO('Existe bloqueios para este orçamento, não é permitido alterar o status para "Aguardando recebimento"!');
        end if;
      end if;
    end if;
  end if;
  
  if :new.CLIENTE_ID = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then
    :new.NOME_CONSUMIDOR_FINAL := nvl(:new.NOME_CONSUMIDOR_FINAL, 'CONSUMIDOR FINAL');    
  end if;
  
end ORCAMENTOS_IU_BR;
/