create or replace TRIGGER ORC_ITENS_DEF_LOCAIS_IU_BR
before insert or update
on ORCAMENTOS_ITENS_DEF_LOCAIS
for each row
begin
  if updating then
    if :new.QUANTIDADE_ATO = 0 and :new.QUANTIDADE_ENTREGAR = 0 and :new.QUANTIDADE_RETIRAR = 0 then
      :new.STATUS := 'GER';
    end if;
  end if;
end ORC_ITENS_DEF_LOCAIS_IU_BR;