create or replace trigger DEVOLUCOES_ITENS_IU_BR
before insert or update
on DEVOLUCOES_ITENS
for each row
declare

  vOrcamentoId     ORCAMENTOS.ORCAMENTO_ID%type;
  vQtdeUtilizar    number default 0;
  vQtdeRestante    number;
  vDevolucaoSomenteFiscal char default 'N';

  cursor cPendencias(pORCAMENTO_ID in number) is
  select
    'R' as TIPO,
    EMPRESA_ID,
    LOCAL_ID,
    ITEM_ID,
    LOTE,
    SALDO,
    null as PREVISAO_ENTREGA
  from
    RETIRADAS_ITENS_PENDENTES
  where ORCAMENTO_ID = pORCAMENTO_ID
  and SALDO > 0
  and ITEM_ID = :new.ITEM_ID

  union all

  select
    'E' as TIPO,
    EMPRESA_ID,
    LOCAL_ID,
    ITEM_ID,
    LOTE,
    SALDO,
    PREVISAO_ENTREGA
  from
    ENTREGAS_ITENS_PENDENTES
  where ORCAMENTO_ID = pORCAMENTO_ID
  and SALDO > 0
  and ITEM_ID = :new.ITEM_ID;

begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then

    select
      NVL(DEVOLUCAO_SOMENTE_FISCAL, 'N')
    into
      vDevolucaoSomenteFiscal
    from
      FUNCIONARIOS
    where FUNCIONARIO_ID = SESSAO.USUARIO_SESSAO_ID;

    select
      ORCAMENTO_ID
    into
      vOrcamentoId
    from
      DEVOLUCOES
    where DEVOLUCAO_ID = :new.DEVOLUCAO_ID;

    /* N�o ser� mexido na parte de entregues agora, ela ser� alterada na parte de confirma��o de devolu��es */


    if vDevolucaoSomenteFiscal = 'N' then
      /* Devolvendo as pend�ncias (Retirar e entregar) */
      if :new.DEVOLVIDOS_PENDENCIAS > 0 then
        vQtdeRestante := :new.DEVOLVIDOS_PENDENCIAS;
        for xPend in cPendencias(vOrcamentoId) loop
          if xPend.SALDO >= vQtdeRestante then
            vQtdeUtilizar := vQtdeRestante;
            vQtdeRestante := 0;
          else
            vQtdeUtilizar := xPend.SALDO;
            vQtdeRestante := vQtdeRestante - vQtdeUtilizar;
          end if;

          if xPend.TIPO = 'R' then
            update RETIRADAS_ITENS_PENDENTES set
              DEVOLVIDOS = DEVOLVIDOS + vQtdeUtilizar
            where ORCAMENTO_ID = vOrcamentoId
            and EMPRESA_ID = xPend.EMPRESA_ID
            and LOCAL_ID = xPend.LOCAL_ID
            and ITEM_ID = xPend.ITEM_ID
            and LOTE = xPend.LOTE;
          else
            update ENTREGAS_ITENS_PENDENTES set
              DEVOLVIDOS = DEVOLVIDOS + vQtdeUtilizar
            where ORCAMENTO_ID = vOrcamentoId
            and EMPRESA_ID = xPend.EMPRESA_ID
            and LOCAL_ID = xPend.LOCAL_ID
            and ITEM_ID = xPend.ITEM_ID
            and LOTE = xPend.LOTE
            and PREVISAO_ENTREGA = xPend.PREVISAO_ENTREGA;
          end if;

          /* Se n�o tem mais nada a devolver, saindo do loop */
          if vQtdeRestante = 0 then
            exit;
          end if;

        end loop;

        if vQtdeRestante > 0 then
          ERRO('N�o existe saldo suficiente para realizar a devolu��o, verifique as pend�ncias!');
        end if;
      end if;

      /* Devolvendo os itens sem previs�o */
      if :new.DEVOLVIDOS_SEM_PREVISAO > 0 then
        update ENTREGAS_ITENS_SEM_PREVISAO set
          DEVOLVIDOS = DEVOLVIDOS + :new.DEVOLVIDOS_SEM_PREVISAO
        where ORCAMENTO_ID = vOrcamentoId
        and ITEM_ID = :new.ITEM_ID;
      end if;
    end if;
  end if;

end DEVOLUCOES_ITENS_IU_BR;
/
