﻿create or replace trigger CONTAS_PAGAR_IU_BR
before insert
on CONTAS_PAGAR
for each row
declare
  v_forma_pagamento  TIPOS_COBRANCA.FORMA_PAGAMENTO%type; 
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if :new.DATA_VENCIMENTO < to_date('01/01/2010', 'dd/mm/yyyy') then
    ERRO('A data de vencimento está incorreta, verifique a data de vencimento lançada! Vencimento: ' || :new.DATA_VENCIMENTO);
  end if;

  if inserting then
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;    
    :new.DATA_CADASTRO       := trunc(sysdate);
    :new.DATA_CONTABIL       := nvl(:new.DATA_CONTABIL, trunc(sysdate));
    :new.DATA_EMISSAO        := nvl(:new.DATA_EMISSAO, trunc(sysdate));
  end if;

end CONTAS_PAGAR_IU_BR;
/