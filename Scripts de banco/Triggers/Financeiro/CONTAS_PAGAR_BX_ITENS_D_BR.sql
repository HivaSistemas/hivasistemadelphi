create or replace trigger CONTAS_PAGAR_BX_ITENS_D_BR
before delete
on CONTAS_PAGAR_BAIXAS_ITENS
for each row
begin      
  
  update CONTAS_PAGAR set
    STATUS = 'A',
    BAIXA_ID = null
  where PAGAR_ID = :old.PAGAR_ID;
  
end CONTAS_PAGAR_BX_ITENS_D_BR;