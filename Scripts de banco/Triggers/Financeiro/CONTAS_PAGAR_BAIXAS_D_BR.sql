create or replace trigger CONTAS_PAGAR_BAIXAS_D_BR
before delete
on CONTAS_PAGAR_BAIXAS
for each row
begin

  if :old.TIPO = 'ECP' and SESSAO.ROTINA not in('CANCELAR_BAIXA_CONTAS_RECEBER') then
    ERRO('N�o � permitido cancelar diretamente o encontro de contas a pagar, fa�a o cancelamento da baixa que o originou! Baixa a receber origem: ' || :old.BAIXA_RECEBER_ORIGEM_ID);
  end if;

  delete from CONTAS_PAGAR_BAIXAS_ITENS
  where BAIXA_ID = :old.BAIXA_ID;

end CONTAS_PAGAR_BAIXAS_D_BR;
/