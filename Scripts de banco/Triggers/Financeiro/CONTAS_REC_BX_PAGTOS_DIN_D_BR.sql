create or replace trigger CONTAS_REC_BX_PAGTOS_DIN_D_BR
before delete
on CONTAS_REC_BAIXAS_PAGTOS_DIN
for each row
begin
  
  delete from MOVIMENTOS_CONTAS
  where CONTA_ID = :old.CONTA_ID
  and BAIXA_RECEBER_ID = :old.BAIXA_ID;  
  
end CONTAS_REC_BX_PAGTOS_DIN_D_BR;
/