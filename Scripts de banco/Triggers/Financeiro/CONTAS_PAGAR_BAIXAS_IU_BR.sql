create or replace trigger CONTAS_PAGAR_BAIXAS_IU_BR
before insert or update
on CONTAS_PAGAR_BAIXAS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then
    :new.DATA_HORA_BAIXA  := sysdate;
    :new.USUARIO_BAIXA_ID := SESSAO.USUARIO_SESSAO_ID;
  end if;

	:new.VALOR_LIQUIDO :=
		:new.VALOR_TITULOS +
    :new.VALOR_JUROS -
		:new.VALOR_DESCONTO;

end CONTAS_PAGAR_BAIXAS_IU_BR;
/
