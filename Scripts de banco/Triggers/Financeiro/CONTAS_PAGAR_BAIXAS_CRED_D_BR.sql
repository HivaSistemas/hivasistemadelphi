create or replace trigger CONTAS_PAGAR_BAIXAS_CRED_D_BR
before delete
on CONTAS_PAGAR_BAIXAS_CREDITOS
for each row
declare
  vBaixaReceberId   CONTAS_RECEBER.BAIXA_ID%type;
begin

  select 
    BAIXA_ID 
  into
    vBaixaReceberId
  from 
    CONTAS_RECEBER 
  where RECEBER_ID = :old.RECEBER_ID;
  
  if vBaixaReceberId is not null then
    CANCELAR_BAIXA_CONTAS_RECEBER(vBaixaReceberId, 'N');
  end if;

end CONTAS_PAGAR_BAIXAS_CRED_D_BR;
/