create or replace trigger CONTAS_PAGAR_BX_ITENS_IU_BR
before insert or update
on CONTAS_PAGAR_BAIXAS_ITENS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  update CONTAS_PAGAR set
    STATUS = 'B',
    BAIXA_ID = :new.BAIXA_ID
  where PAGAR_ID = :new.PAGAR_ID;
  
end CONTAS_PAGAR_BX_ITENS_IU_BR;
