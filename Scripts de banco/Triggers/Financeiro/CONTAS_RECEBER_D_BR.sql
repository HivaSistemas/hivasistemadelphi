create or replace trigger CONTAS_RECEBER_D_BR
before delete
on CONTAS_RECEBER
for each row
begin

  delete from LOGS_CONTAS_RECEBER
  where RECEBER_ID = :old.RECEBER_ID;

end CONTAS_RECEBER_D_BR;
/