create or replace trigger CONTAS_RECEBER_BAIXAS_IU_BR
before insert or update
on CONTAS_RECEBER_BAIXAS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

	if inserting then
		:new.DATA_HORA_BAIXA  := sysdate;
		if :new.RECEBER_CAIXA = 'S' then		
			:new.USUARIO_ENVIO_CAIXA_ID := SESSAO.USUARIO_SESSAO_ID;
		elsif :new.RECEBIDO = 'S' then			
			:new.USUARIO_BAIXA_ID := SESSAO.USUARIO_SESSAO_ID;  
		end if;		
	else
		if :new.RECEBER_CAIXA = 'S' and :old.RECEBIDO = 'N' and :new.RECEBIDO = 'S' then
		  :new.USUARIO_BAIXA_ID := SESSAO.USUARIO_SESSAO_ID;
		end if;		
	end if;
	
	:new.VALOR_LIQUIDO :=
		:new.VALOR_TITULOS +
    :new.VALOR_MULTA +
    :new.VALOR_JUROS -
    :new.VALOR_ADIANTADO -
		:new.VALOR_DESCONTO -
    :new.VALOR_RETENCAO;

end CONTAS_RECEBER_BAIXAS_IU_BR;
/