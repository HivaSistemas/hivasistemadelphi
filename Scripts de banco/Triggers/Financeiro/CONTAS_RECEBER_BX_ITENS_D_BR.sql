create or replace trigger CONTAS_RECEBER_BX_ITENS_D_BR
before delete
on CONTAS_RECEBER_BAIXAS_ITENS
for each row
begin      
  
  update CONTAS_RECEBER set
    STATUS = 'A',
    BAIXA_ID = null
  where RECEBER_ID = :old.RECEBER_ID;
  
end CONTAS_RECEBER_BX_ITENS_D_BR;