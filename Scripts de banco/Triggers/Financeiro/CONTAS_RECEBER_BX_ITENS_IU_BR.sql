create or replace TRIGGER CONTAS_RECEBER_BX_ITENS_IU_BR
before insert or update
on CONTAS_RECEBER_BAIXAS_ITENS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then
    update CONTAS_RECEBER set
      STATUS = 'B',
	  COMISSIONAR = 'S',
      BAIXA_ID = :new.BAIXA_ID
    where RECEBER_ID = :new.RECEBER_ID;
  end if;

end CONTAS_RECEBER_BX_ITENS_IU_BR;