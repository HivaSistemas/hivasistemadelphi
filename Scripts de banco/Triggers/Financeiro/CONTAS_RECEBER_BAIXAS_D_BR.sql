create or replace trigger CONTAS_RECEBER_BAIXAS_D_BR
before delete
on CONTAS_RECEBER_BAIXAS
for each row
begin

  if :old.TIPO = 'ECR' and SESSAO.ROTINA not in('CANCELAR_BAIXA_CONTAS_PAGAR') then
    ERRO('N�o � permitido cancelar diretamente o encontro de contas a receber, fa�a o cancelamento da baixa que o originou! Baixa a pagar origem: ' || :old.BAIXA_PAGAR_ORIGEM_ID);
  end if;
  
end CONTAS_RECEBER_BAIXAS_D_BR;
/