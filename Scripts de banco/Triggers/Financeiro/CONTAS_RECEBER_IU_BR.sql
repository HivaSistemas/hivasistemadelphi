create or replace trigger CONTAS_RECEBER_IU_BR
before insert or update
on CONTAS_RECEBER
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if :new.DATA_VENCIMENTO < to_date('01/01/2000', 'dd/mm/yyyy') then
    ERRO('A data de vencimento est� incorreta, verifique a data de vencimento lan�ada! Vencimento: ' || :new.DATA_VENCIMENTO);
  end if;
  
  if inserting then
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.DATA_CADASTRO       := trunc(sysdate);
    :new.DATA_CONTABIL       := nvl(:new.DATA_CONTABIL, trunc(sysdate));
    :new.DATA_EMISSAO        := nvl(:new.DATA_EMISSAO, trunc(sysdate));
  else
    if :new.COBRANCA_ID = SESSAO.PARAMETROS_GERAIS.TIPO_COB_RECEB_ENTREGA_ID and :old.STATUS = 'A' and :new.STATUS = 'B' then
      ERRO('N�o � permitido baixar t�tulos recebimento na entrega, fa�a o recebimento da venda no caixa!');
    end if;
  end if;
  
end CONTAS_RECEBER_IU_BR;
/