create or replace trigger CONTAS_PAGAR_BX_PAG_DIN_D_BR
before delete
on CONTAS_PAGAR_BAIXAS_PAGTOS_DIN
for each row
begin
  
  delete from MOVIMENTOS_CONTAS
  where CONTA_ID = :old.CONTA_ID
  and BAIXA_PAGAR_ID = :old.BAIXA_ID;  
  
end CONTAS_PAGAR_BX_PAG_DIN_D_BR;
/