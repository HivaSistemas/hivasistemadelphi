create or replace trigger MOVIMENTOS_TURNOS_IU_BR
before insert or update
on MOVIMENTOS_TURNOS
for each row
declare

  cursor cCartoes is
  select
    COR.ORCAMENTO_ID,
    COR.BAIXA_ORIGEM_ID,
    COR.ITEM_ID_CRT_ORCAMENTO
  from
    CONTAS_RECEBER COR

  inner join TIPOS_COBRANCA TCO
  on COR.COBRANCA_ID = TCO.COBRANCA_ID

  inner join CADASTROS CAD
  on COR.CADASTRO_ID = CAD.CADASTRO_ID

  where COR.TURNO_ID = :new.TURNO_ID
  and TCO.FORMA_PAGAMENTO = 'CRT'

  group by
    COR.ORCAMENTO_ID,
    COR.BAIXA_ORIGEM_ID,
    COR.ITEM_ID_CRT_ORCAMENTO;

  cursor cCobrancas is
  select
    COR.RECEBER_ID,
    TPC.FORMA_PAGAMENTO
  from
    CONTAS_RECEBER COR

  inner join TIPOS_COBRANCA TPC
  on COR.COBRANCA_ID = TPC.COBRANCA_ID
  and TPC.FORMA_PAGAMENTO in('CHQ', 'COB')

  where COR.TURNO_ID = :new.TURNO_ID;

begin

  if updating then
    ERRO('N�o s�o permitidas altera��es nos movimentos de turnos!');
  end if;

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  :new.DATA_HORA_MOVIMENTO := sysdate;
  :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;

  if :new.TIPO_MOVIMENTO in('SUP', 'SAN', 'FEC') and :new.VALOR_DINHEIRO > 0 then
    MOVIMENTAR_SALDO_CONTA( :new.CONTA_ID, :new.VALOR_DINHEIRO * case when :new.TIPO_MOVIMENTO = 'SUP' then -1 else 1 end, 'T' );
  end if;

  if inserting then
    if :new.TIPO_MOVIMENTO = 'FEC' then
      update TURNOS_CAIXA set
        VALOR_DINHEIRO_INF_FECHAMENTO = :new.VALOR_DINHEIRO,
        VALOR_CHEQUE_INF_FECHAMENTO = :new.VALOR_CHEQUE,
        VALOR_CARTAO_INF_FECHAMENTO = :new.VALOR_CARTAO,
        VALOR_COBRANCA_INF_FECHAMENTO = :new.VALOR_COBRANCA,
        STATUS = 'B'
      where TURNO_ID = :new.TURNO_ID;

      update CONTAS_RECEBER set
        TURNO_ID = null
      where TURNO_ID = :new.TURNO_ID;

      for xCartoes in cCartoes loop
        GRAVAR_TITULOS_MOV_TURNOS_ITE(xCartoes.ORCAMENTO_ID, xCartoes.ITEM_ID_CRT_ORCAMENTO, :new.MOVIMENTO_TURNO_ID, 'CRT', null);
      end loop;

      for xCob in cCobrancas loop
        GRAVAR_TITULOS_MOV_TURNOS_ITE(null, null, :new.MOVIMENTO_TURNO_ID, 'CRT', xCob.RECEBER_ID);
      end loop;
    end if;
  end if;

end MOVIMENTOS_TURNOS_IU_BR;
/