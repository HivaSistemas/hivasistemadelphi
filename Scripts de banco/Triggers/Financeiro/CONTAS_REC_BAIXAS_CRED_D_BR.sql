create or replace trigger CONTAS_REC_BAIXAS_CRED_D_BR
before delete
on CONTAS_REC_BAIXAS_CREDITOS
for each row
declare
  vBaixaPagarId   CONTAS_PAGAR.BAIXA_ID%type;
begin

  select 
    BAIXA_ID 
  into
    vBaixaPagarId
  from 
    CONTAS_PAGAR 
  where PAGAR_ID = :old.PAGAR_ID;
  
  if vBaixaPagarId is not null then
    CANCELAR_BAIXA_CONTAS_PAGAR(vBaixaPagarId);
  end if;

end CONTAS_REC_BAIXAS_CRED_D_BR;
/