create or replace trigger CONTAS_REC_BX_PAGTOS_DIN_IU_BR
before insert or update
on CONTAS_REC_BAIXAS_PAGTOS_DIN
for each row
declare
  vDataPagamento date;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then
    select
      NVL(DATA_PAGAMENTO, TRUNC(SYSDATE))
    into
      vDataPagamento
    from
      CONTAS_RECEBER_BAIXAS
    where BAIXA_ID = :new.BAIXA_ID;

    insert into MOVIMENTOS_CONTAS(
      MOVIMENTO_ID,
      CONTA_ID,
      VALOR_MOVIMENTO,
      DATA_HORA_MOVIMENTO,
      USUARIO_MOVIMENTO_ID,
      TIPO_MOVIMENTO,            
      CENTRO_CUSTO_ID,                  
      BAIXA_RECEBER_ID
    )values(
      SEQ_MOVIMENTOS_CONTAS.nextval,
      :new.CONTA_ID,
      :new.VALOR,
      vDataPagamento,
      SESSAO.USUARIO_SESSAO_ID,
      'BXR',
      1,
      :new.BAIXA_ID
    );
  end if;  

end CONTAS_REC_BX_PAGTOS_DIN_IU_BR;
/