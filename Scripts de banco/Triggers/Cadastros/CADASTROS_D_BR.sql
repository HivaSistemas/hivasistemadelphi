create or replace trigger CADASTROS_D_BR
before delete
on CADASTROS
for each row
begin

	if :new.CADASTRO_ID = 1 then
	  ERRO('Não é possível deletar o cadastro de consumidor final!');
	end if;
  
end CADASTROS_D_BR;