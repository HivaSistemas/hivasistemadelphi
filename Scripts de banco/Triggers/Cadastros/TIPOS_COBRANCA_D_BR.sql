create or replace trigger TIPOS_COBRANCA_D_BR
before delete
on TIPOS_COBRANCA
for each row
begin

  if :old.COBRANCA_ID between 1 and 100 and SESSAO.USUARIO_SESSAO_ID <> 1 then
    ERRO('Não é permitido alterar os tipos de cobrança padrão do sistema!');
  end if;  

end TIPOS_COBRANCA_D_BR;