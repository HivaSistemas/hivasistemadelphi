﻿create or replace trigger CLIENTES_D_BR
before delete
on CLIENTES
for each row
begin

	if :new.CADASTRO_ID = 1 then
	  ERRO('Não é possível deletar o cliente consumidor final!');
	end if;

  update CADASTROS set
    E_CLIENTE = 'N'
  where CADASTRO_ID = :old.CADASTRO_ID;

  EXCLUIR_CADASTRO(:old.CADASTRO_ID);

end CLIENTES_D_BR;