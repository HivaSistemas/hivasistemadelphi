create or replace trigger LOGS_TELAS_IU_BR
before insert or update
on LOGS_TELAS
for each row
begin
  
  :new.DATA_HORA := sysdate;
  
end LOGS_TELAS_IU_BR;