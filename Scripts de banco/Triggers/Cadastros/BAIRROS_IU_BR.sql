create or replace trigger BAIRROS_IU_BR
before insert or update
on BAIRROS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if :new.BAIRRO_ID = 1 then
    ERRO('Não é permitido alterar o bairro 1!');
  end if;

end BAIRROS_IU_BR;
/