create or replace trigger CUSTOS_PRODUTOS_IU_BR
before insert or update
on CUSTOS_PRODUTOS
for each row
declare
  vPrecoTabela     number;
  vPrecoLiquido    number;

  vTipoControleEstoque PRODUTOS.TIPO_CONTROLE_ESTOQUE%type;
  vProdutoPaiId        PRODUTOS.PRODUTO_PAI_ID%type;
begin

  select
    TIPO_CONTROLE_ESTOQUE,
    PRODUTO_PAI_ID
  into
    vTipoControleEstoque,
    vProdutoPaiId
  from
    PRODUTOS
  where PRODUTO_ID = :new.PRODUTO_ID;

  if vTipoControleEstoque in('K', 'A') then
    ERRO('Produtos kit n�o podem ter custos definidos!');
  end if;

  if :new.PRODUTO_ID <> vProdutoPaiId then
    ERRO('Produtos filho n�o pode ter custos definidos!');
  end if;

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

end CUSTOS_PRODUTOS_IU_BR;
