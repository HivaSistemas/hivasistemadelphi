create or replace trigger FUNCIONARIOS_D_BR
before delete
on FUNCIONARIOS
for each row
begin

  update CADASTROS set
    E_FUNCIONARIO = 'N'
  where CADASTRO_ID = :old.CADASTRO_ID;

  EXCLUIR_CADASTRO(:old.CADASTRO_ID);

end FUNCIONARIOS_D_BR;
