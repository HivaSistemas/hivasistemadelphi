﻿create or replace trigger TIPOS_COBRANCA_IU_BR
before insert or update
on TIPOS_COBRANCA
for each row
declare
  vQtde number;
begin

  if :old.FORMA_PAGAMENTO <> :new.FORMA_PAGAMENTO then
    select
      count(*)
    into
      vQtde
    from
      PARAMETROS
    where :new.COBRANCA_ID in(
      TIPO_COBRANCA_GERACAO_CRED_ID,
      TIPO_COBRANCA_GER_CRED_IMP_ID,
      TIPO_COB_RECEB_ENTREGA_ID,
      TIPO_COB_ACUMULATIVO_ID,
      TIPO_COB_ADIANTAMENTO_ACU_ID,
      TIPO_COB_ADIANTAMENTO_FIN_ID
    );

    if vQtde > 0 then
      ERRO('Não é permitido alterar a forma de pagamento deste tipo de cobrança pois ele está ligado a um parâmetro geral!');
    end if;
  end if;


end TIPOS_COBRANCA_IU_BR;
/