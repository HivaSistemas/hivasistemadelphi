create or replace trigger CFOP_IU_BR
before insert or update
on CFOP
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then
    :new.CFOP_PESQUISA_ID := :new.CFOP_ID || '-' || to_char(:new.FLUTUACAO);
  end if;

end CFOP_IU_BR;
/