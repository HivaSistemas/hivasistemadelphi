create or replace trigger PROFISSIONAIS_IU_BR  
before insert or update
on PROFISSIONAIS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then
    update CADASTROS set
      E_PROFISSIONAL = 'S'
    where CADASTRO_ID = :new.CADASTRO_ID;
  end if;

end PROFISSIONAIS_IU_BR;
