create or replace trigger TRANSPORTADORAS_IU_BR
before insert or update
on TRANSPORTADORAS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
    :new.DATA_HORA_CADASTRO := sysdate;
    
    update CADASTROS set
      E_TRANSPORTADORA = 'S'
    where CADASTRO_ID = :new.CADASTRO_ID;
  end if;

end TRANSPORTADORAS_IU_BR;