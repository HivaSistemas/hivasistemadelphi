create or replace trigger PLANOS_FINANCEIROS_IU_BR
before insert or update
on PLANOS_FINANCEIROS
for each row
begin
  if
    :new.PERMITE_ALTERACAO = 'N' or
    (
      substr(:new.PLANO_FINANCEIRO_ID, 1, 5) = '1.001' and
      to_number(substr(:new.PLANO_FINANCEIRO_ID, 7, 3)) < 21
    ) or
    (
      substr(:new.PLANO_FINANCEIRO_ID, 1, 4) = '1.00' and
      substr(:new.PLANO_FINANCEIRO_ID, 5, 1) in ('2','3','5') and
      to_number(substr(:new.PLANO_FINANCEIRO_ID, 7, 3)) < 6
    )
  then
    ERRO('N�o � permitido alterar planos financeiros padr�es Hiva!');
  end if;

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

end PLANOS_FINANCEIROS_IU_BR;