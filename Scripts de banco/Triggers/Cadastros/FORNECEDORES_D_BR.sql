create or replace trigger FORNECEDORES_D_BR
before delete
on FORNECEDORES
for each row
begin

  update CADASTROS set
    E_FORNECEDOR = 'N'
  where CADASTRO_ID = :old.CADASTRO_ID;

  EXCLUIR_CADASTRO(:old.CADASTRO_ID);

end FORNECEDORES_D_BR;
