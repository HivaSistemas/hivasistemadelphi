﻿create or replace trigger PROFISSIONAIS_D_BR
before delete
on PROFISSIONAIS
for each row
begin

  update CADASTROS set
    E_PROFISSIONAL = 'N'
  where CADASTRO_ID = :old.CADASTRO_ID;

  EXCLUIR_CADASTRO(:old.CADASTRO_ID);
  
end PROFISSIONAIS_D_BR;
