create or replace trigger CONDICOES_PAGAMENTO_IU_BR
before insert or update
on CONDICOES_PAGAMENTO
for each row
declare
  vQtde number;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if updating then

    if :old.ACUMULATIVO <> :new.ACUMULATIVO then
      select
        count(*)
      into
        vQtde
      from
        ORCAMENTOS
      where CONDICAO_ID = :new.CONDICAO_ID;

      if vQtde > 0 then
        ERRO('N�o � permitido alterar o par�metro "10 - Acumulado" pois j� houve pedidos com esta condi��o de pagamento!');
      end if;
    end if;

  end if;

end CONDICOES_PAGAMENTO_IU_BR;
/