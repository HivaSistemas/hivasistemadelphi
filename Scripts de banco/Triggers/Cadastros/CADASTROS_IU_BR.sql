﻿create or replace trigger CADASTROS_IU_BR
before insert or update
on CADASTROS
for each row
begin

  SESSAO.ROTINA := 'SUPORTE';

  if SESSAO.ROTINA = 'SUPORTE' then
    return;
  end if;

	if :new.CADASTRO_ID = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then
	  ERRO('Alterações no cadastro de consumidor final não são permitidas!');
	end if;
  
  :new.NUMERO := nvl(:new.NUMERO, 'SN');
  :new.NOME_FANTASIA := nvl(:new.NOME_FANTASIA, :new.RAZAO_SOCIAL);

end CADASTROS_IU_BR;
/