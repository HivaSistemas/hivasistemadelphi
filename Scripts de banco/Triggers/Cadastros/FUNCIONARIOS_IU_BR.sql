create or replace trigger FUNCIONARIOS_IU_BR
before insert or update
on FUNCIONARIOS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if :old.SENHA_SISTEMA <> :new.SENHA_SISTEMA then
    :new.DATA_ULTIMA_SENHA := trunc(sysdate);
  end if;

end FUNCIONARIOS_IU_BR;
/