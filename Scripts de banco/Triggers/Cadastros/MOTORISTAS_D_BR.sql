create or replace trigger MOTORISTAS_D_BR
before delete
on MOTORISTAS
for each row
begin

  update CADASTROS set
    E_MOTORISTA = 'N'
  where CADASTRO_ID = :old.CADASTRO_ID;

  EXCLUIR_CADASTRO(:old.CADASTRO_ID);

end MOTORISTAS_D_BR;
