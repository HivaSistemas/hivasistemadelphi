create or replace trigger MENSAGENS_IU_BR
before insert or update
on MENSAGENS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

	if inserting then
    :new.USUARIO_ENVIO_MENSAGEM_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.DATA_HORA_ENVIO := sysdate;  
  end if;

  :new.ASSUNTO := nvl(:new.ASSUNTO, 'SEM ASSUNTO');
  
end MENSAGENS_IU_BR;