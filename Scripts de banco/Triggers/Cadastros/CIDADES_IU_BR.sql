create or replace trigger CIDADES_IU_BR
before insert or update
on CIDADES
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if SESSAO.ROTINA <> 'ATUALIZAR_CIDADE_BD' then
    if :new.CIDADE_ID = 1 then
      ERRO('N�o � permitido alterar a cidade 1!');
    end if;
  end if;

end CIDADES_IU_BR;
/