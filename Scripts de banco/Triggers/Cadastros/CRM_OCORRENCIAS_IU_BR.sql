create or replace TRIGGER CRM_OCORRENCIAS_IU_BR
before insert or update
on CRM_OCORRENCIAS
for each row
begin

  if inserting then
    :new.USUARIO := SESSAO.USUARIO_SESSAO_ID;
    :new.DATA_CADASTRO := sysdate;
    :new.STATUS := 'AB';
  else
    if (:old.STATUS = 'AB' or :old.STATUS = 'PR') and (:new.STATUS = 'CO' or :new.STATUS = 'RE') then
      :new.DATA_CONCLUSAO  := sysdate;
    elsif (:new.STATUS <> 'CO' or :new.STATUS <> 'RE') then
      :new.DATA_CONCLUSAO := null;
    end if;
  end if;

end CRM_OCORRENCIAS_IU_BR;
