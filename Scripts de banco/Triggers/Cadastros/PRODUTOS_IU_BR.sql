create or replace trigger PRODUTOS_IU_BR
before insert or update
on PRODUTOS
for each row
declare
  vQtde number;
begin

  if inserting then
    :new.PRODUTO_PAI_ID := :new.PRODUTO_ID;
  end if;

  /* Se for produto filho */
  if :new.CODIGO_ORIGINAL_FABRICANTE is not null and :new.PRODUTO_ID <> :new.PRODUTO_PAI_ID then
    ERRO('N�o � permitido adicionar o c�digo original do fabricante em produto filho, adicione o c�digo original no produto pai! Produto pai: ' || :new.PRODUTO_ID);
  end if;

  if updating then
    if :old.ATIVO = 'S' and :new.ATIVO = 'N' then
      select
        count(*)
      into
        vQtde
      from
        ESTOQUES_DIVISAO
      where PRODUTO_ID = :new.PRODUTO_PAI_ID
      and DISPONIVEL <> 0;

      if vQtde > 0 then
        ERRO('O produto n�o pode ser inativado pois o estoque dispon�vel do mesmo n�o se encontra zerado!');
      end if;

      :new.MOTIVO_INATIVIDADE := 'INATIVADO';
    elsif :old.ATIVO = 'N' and :new.ATIVO = 'S' then
      :new.MOTIVO_INATIVIDADE := null;
    end if;
  end if;

end PRODUTOS_IU_BR;
/