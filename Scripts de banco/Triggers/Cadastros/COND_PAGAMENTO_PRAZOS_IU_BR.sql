create or replace trigger COND_PAGAMENTO_PRAZOS_IU_BR
before insert or update
on CONDICOES_PAGAMENTO_PRAZOS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

end COND_PAGAMENTO_PRAZOS_IU_BR;
/