﻿create or replace trigger CLIENTES_IU_BR
before insert or update
on CLIENTES
for each row
begin


  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if SESSAO.ROTINA = 'SUPORTE' then
    return;
  end if;

  if inserting then
    :new.DATA_CADASTRO := sysdate;

    update CADASTROS set
      E_CLIENTE = 'S'
    where CADASTRO_ID = :new.CADASTRO_ID;
  end if;

	if :new.CADASTRO_ID = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID and SESSAO.ROTINA not in('SEM DEFINICAO', 'ATUALIZAR_INFORMACOES_CLIENTES') then
	  ERRO('Alterações no cliente consumidor final não são permitidas!' || SESSAO.ROTINA);
	end if;

  if nvl(:old.LIMITE_CREDITO, 0) <> :new.LIMITE_CREDITO then
    :new.USUARIO_APROV_LIMITE_CRED_ID := SESSAO.USUARIO_SESSAO_ID;
  end if;

end CLIENTES_IU_BR;
/