create or replace trigger TRANSPORTADORAS_D_BR
before delete
on TRANSPORTADORAS
for each row
begin

  update CADASTROS set
    E_TRANSPORTADORA = 'N'
  where CADASTRO_ID = :old.CADASTRO_ID;

  EXCLUIR_CADASTRO(:old.CADASTRO_ID);

end TRANSPORTADORAS_D_BR;
/
