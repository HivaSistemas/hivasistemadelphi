create or replace trigger PRECOS_PRODUTOS_IU_BR
before insert or update
on PRECOS_PRODUTOS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if updating then
    if :old.PRECO_VAREJO <> :new.PRECO_VAREJO then
      :new.DATA_PRECO_VAREJO := trunc(sysdate);
    end if;

    if :old.PRECO_ATACADO_1 <> :new.PRECO_ATACADO_1 then
      :new.DATA_PRECO_ATACADO_1 := trunc(sysdate);
    end if;

    if :old.PRECO_ATACADO_2 <> :new.PRECO_ATACADO_2 then
      :new.DATA_PRECO_ATACADO_2 := trunc(sysdate);
    end if;

    if :old.PRECO_ATACADO_3 <> :new.PRECO_ATACADO_3 then
      :new.DATA_PRECO_ATACADO_3 := trunc(sysdate);
    end if;

    if :old.PRECO_PDV <> :new.PRECO_PDV then
      :new.DATA_PRECO_PDV := trunc(sysdate);
    end if;
  end if;

end PRECOS_PRODUTOS_IU_BR;
