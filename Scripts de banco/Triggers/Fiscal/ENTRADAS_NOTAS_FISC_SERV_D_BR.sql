create or replace trigger ENTRADAS_NOTAS_FISC_SERV_D_BR
before delete
on ENTRADAS_NOTAS_FISC_SERVICOS
for each row
declare
  vQtde number;
begin

  select
    count(*)
  into
    vQtde
  from
    CONTAS_PAGAR
  where ENTRADA_SERVICO_ID = :old.ENTRADA_ID
  and STATUS = 'B';
  
  if vQtde > 0 then
    ERRO('Existem títulos a pagar baixados vinculados a esta entrada de nota de servico!');
  end if;
  
  delete from CONTAS_PAGAR
  where ENTRADA_SERVICO_ID = :old.ENTRADA_ID;

  delete from ENTRADAS_NF_SERVICOS_FINANC
  where ENTRADA_ID = :old.ENTRADA_ID;
  
end ENTRADAS_NOTAS_FISC_SERV_D_BR;
/