create or replace trigger ENTRADAS_NOTAS_FISC_SERV_IU_BR
before insert or update
on ENTRADAS_NOTAS_FISC_SERVICOS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.DATA_HORA_CADASTRO  := sysdate;
  end if;

end ENTRADAS_NOTAS_FISC_SERV_IU_BR;
/