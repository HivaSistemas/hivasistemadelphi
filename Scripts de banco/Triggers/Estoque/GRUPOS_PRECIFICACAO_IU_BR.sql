create or replace trigger GRUPOS_PRECIFICACAO_IU_BR
before insert or update
on GRUPOS_PRECIFICACAO
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

end GRUPOS_PRECIFICACAO_IU_BR;
