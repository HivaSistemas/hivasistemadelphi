﻿create or replace trigger ENTRADAS_NF_ITENS_COMP_IU_BR
before insert or update
on ENTRADAS_NF_ITENS_COMPRAS
for each row
begin

  if updating then
    ERRO('Não são permitidas alterações na tabela ENTRADAS_NF_ITENS_COMPRAS!');
  end if;

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
  
    update COMPRAS_ITENS set
      ENTREGUES = ENTREGUES + :new.QUANTIDADE
    where COMPRA_ID = :new.COMPRA_ID
    and ITEM_ID = :new.ITEM_COMPRA_ID;

  end if;

end ENTRADAS_NF_ITENS_COMP_IU_BR;
/