create or replace trigger ENTRADAS_NF_FINANCEIRO_IU_BR
before insert or update
on ENTRADAS_NOTAS_FISC_FINANCEIRO
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

end ENTRADAS_NF_FINANCEIRO_IU_BR;