create or replace trigger ENTRADAS_NOTAS_FISCAIS_IU_BR
before insert or update
on ENTRADAS_NOTAS_FISCAIS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.DATA_HORA_CADASTRO  := sysdate;
  end if;

  :new.SERIE_NOTA := trim(:new.SERIE_NOTA);

end ENTRADAS_NOTAS_FISCAIS_IU_BR;
/