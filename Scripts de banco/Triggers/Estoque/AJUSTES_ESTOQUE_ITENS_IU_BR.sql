﻿create or replace trigger AJUSTES_ESTOQUE_ITENS_IU_BR
before insert or update
on AJUSTES_ESTOQUE_ITENS
for each row
declare
  vEmpresaId           AJUSTES_ESTOQUE.EMPRESA_ID%type;
  vCustoAjusteEstoque  AJUSTES_ESTOQUE_ITENS.PRECO_UNITARIO%type;
  vTipoEstoqueAjustado AJUSTES_ESTOQUE.TIPO_ESTOQUE_AJUSTADO%type;
begin
  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if SESSAO.ROTINA = 'DESVIAR' then
    return;
  end if;

  if updating then
    ERRO('Não são permitidas alterações na tabela "AJUSTES_ESTOQUE_ITENS"!');
  end if;

  if inserting then
    /*Buscando a capa do ajuste de estoque*/
    select
      EMPRESA_ID,
      TIPO_ESTOQUE_AJUSTADO
    into
      vEmpresaId,
      vTipoEstoqueAjustado
    from
      AJUSTES_ESTOQUE
    where AJUSTE_ESTOQUE_ID = :new.AJUSTE_ESTOQUE_ID;

    if vTipoEstoqueAjustado = 'FISCAL' then
      MOVIMENTAR_ESTOQUE_FISCAL(
        vEmpresaId,
        :new.PRODUTO_ID,
        case when :new.NATUREZA = 'E' then :new.QUANTIDADE else :new.QUANTIDADE * -1 end
      );
    else
      MOVIMENTAR_ESTOQUE(
        vEmpresaId,
        :new.PRODUTO_ID,
        :new.LOCAL_ID,
        :new.LOTE,
        :new.QUANTIDADE,
        case when :new.NATUREZA = 'E' then 'AJE' else 'AJS' end,
        :new.DATA_FABRICACAO,
        :new.DATA_VENCIMENTO
      );
    end if;
  end if;

end AJUSTES_ESTOQUE_ITENS_IU_BR;
/