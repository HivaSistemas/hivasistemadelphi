create or replace trigger COMPRAS_ITENS_IU_BR
before insert or update
on COMPRAS_ITENS
for each row
declare
  vEmpresaId  COMPRAS.EMPRESA_ID%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  :new.SALDO := :new.QUANTIDADE - :new.ENTREGUES - :new.BAIXADOS - :new.CANCELADOS;

  if nvl(:old.SALDO, 0) <> :new.SALDO then
    select
      EMPRESA_ID
    into
      vEmpresaId
    from
      COMPRAS
    where COMPRA_ID = :new.COMPRA_ID;

    MOVIMENTAR_COMPRAS_PENDENTES(vEmpresaId, :new.PRODUTO_ID, ((:new.SALDO - nvl(:old.SALDO, 0)) * :new.QUANTIDADE_EMBALAGEM));
  end if;

  :new.PRECO_FINAL := :new.PRECO_UNITARIO - (:new.VALOR_DESCONTO / :new.QUANTIDADE) + (:new.VALOR_OUTRAS_DESPESAS / :new.QUANTIDADE);

end COMPRAS_ITENS_IU_BR;
/