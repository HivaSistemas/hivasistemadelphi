create or replace trigger TRANSFERENCIAS_LOCAIS_IU_BR
before insert or update
on TRANSFERENCIAS_LOCAIS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
    :new.DATA_HORA_CADASTRO  := sysdate;
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
  end if;

end TRANSFERENCIAS_LOCAIS_IU_BR;