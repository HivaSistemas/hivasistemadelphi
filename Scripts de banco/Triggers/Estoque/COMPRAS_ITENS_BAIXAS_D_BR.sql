create or replace trigger COMPRAS_ITENS_BAIXAS_D_BR
before delete
on COMPRAS_ITENS_BAIXAS
for each row
declare
  vCompraId number;
begin

  select
    COMPRA_ID
  into
    vCompraId
  from
    COMPRAS_BAIXAS
  where BAIXA_ID = :old.BAIXA_ID;

  update COMPRAS_ITENS set
    BAIXADOS = BAIXADOS - :old.QUANTIDADE
  where COMPRA_ID = vCompraId
  and ITEM_ID = :old.ITEM_ID;

end COMPRAS_ITENS_BAIXAS_D_BR;
/