create or replace trigger ENTRADAS_NOTAS_FISCAIS_IU_AR
after insert or update
on ENTRADAS_NOTAS_FISCAIS
for each row
begin

  update PRE_ENTRADAS_NOTAS_FISCAIS set
    STATUS = 'ENT',
    ENTRADA_ID = :new.ENTRADA_ID
  where CHAVE_ACESSO_NFE = RETORNA_NUMEROS(:new.CHAVE_NFE)
  and ENTRADA_ID is null;

end ENTRADAS_NOTAS_FISCAIS_IU_AR;
/