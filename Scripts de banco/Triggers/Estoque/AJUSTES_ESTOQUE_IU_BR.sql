create or replace trigger AJUSTES_ESTOQUE_IU_BR
before insert or update
on AJUSTES_ESTOQUE
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then
    :new.DATA_HORA_AJUSTE  := sysdate;
    :new.USUARIO_AJUSTE_ID := SESSAO.USUARIO_SESSAO_ID;    
    :new.EMPRESA_ID        := SESSAO.PARAMETROS_EMPRESA_LOGADA.EMPRESA_ID;
  end if;
end AJUSTES_ESTOQUE_IU_BR;
/