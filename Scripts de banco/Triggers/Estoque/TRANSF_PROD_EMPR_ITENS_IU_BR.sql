create or replace trigger TRANSF_PROD_EMPR_ITENS_IU_BR
before insert or update
on TRANSF_PRODUTOS_EMPRESAS_ITENS
for each row
declare
  vEmpresaOrigemId  TRANSF_PRODUTOS_EMPRESAS.EMPRESA_ORIGEM_ID%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
  
    select
      EMPRESA_ORIGEM_ID
    into
      vEmpresaOrigemId
    from
      TRANSF_PRODUTOS_EMPRESAS
    where TRANSFERENCIA_ID = :new.TRANSFERENCIA_ID;
  
    MOVIMENTAR_ESTOQUE(
      vEmpresaOrigemId,
      :new.PRODUTO_ID,
      :new.LOCAL_ORIGEM_ID,
      :new.LOTE,
      :new.QUANTIDADE,
      'TPS',
      null,
      null
    );
  
  end if;
  
end TRANSF_PROD_EMPR_ITENS_IU_BR;
/