create or replace trigger BLOQUEIOS_ESTOQUES_D_BR
before delete
on BLOQUEIOS_ESTOQUES
for each row
begin

  if :old.STATUS = 'B' then
    ERRO('Não é permitida a exclusão de um bloqueio de estoque já baixado!');
  end if;

end BLOQUEIOS_ESTOQUES_D_BR;
/