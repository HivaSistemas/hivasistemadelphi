create or replace TRIGGER PRODUCAO_ESTOQUE_ITENS_IU_BR
before insert or update
on PRODUCAO_ESTOQUE_ITENS
for each row
declare
  vEmpresaId           PRODUCAO_ESTOQUE.EMPRESA_ID%type;
begin
  if SESSAO.ROTINA = 'DESVIAR' then
    return;
  end if;

  if updating then
    ERRO('N�o s�o permitidas alterac?es na tabela "PRODUCAO_ESTOQUE_ITENS"!');
  end if;

  if inserting then
    /*Buscando a capa do ajuste de estoque*/
    select
      EMPRESA_ID
    into
      vEmpresaId
    from
      PRODUCAO_ESTOQUE
    where PRODUCAO_ESTOQUE_ID = :new.PRODUCAO_ESTOQUE_ID;

    MOVIMENTAR_ESTOQUE(
      vEmpresaId,
      :new.PRODUTO_ID,
      :new.LOCAL_ID,
      '???',
      :new.QUANTIDADE,
      case when :new.NATUREZA = 'E' then 'PRE' else 'PRS' end,
      '',
      ''
    );
  end if;

end PRODUCAO_ESTOQUE_ITENS_IU_BR;
