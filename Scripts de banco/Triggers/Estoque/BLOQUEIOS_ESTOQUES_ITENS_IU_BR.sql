create or replace trigger BLOQUEIOS_ESTOQUES_ITENS_IU_BR
before insert or update
on BLOQUEIOS_ESTOQUES_ITENS
for each row
declare
  vEmpresaId  BLOQUEIOS_ESTOQUES.EMPRESA_ID%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  :new.SALDO := :new.QUANTIDADE - :new.BAIXADOS - :new.CANCELADOS;

  select
    EMPRESA_ID
  into
    vEmpresaId
  from
    BLOQUEIOS_ESTOQUES
  where BLOQUEIO_ID = :new.BLOQUEIO_ID;

  if inserting then
    MOVIMENTAR_ESTOQUE_BLOQUEADO(
      vEmpresaId,
      :new.PRODUTO_ID,
      :new.LOCAL_ID,
      :new.LOTE,
      :new.QUANTIDADE
    );
  else
    MOVIMENTAR_ESTOQUE_BLOQUEADO(
      vEmpresaId,
      :new.PRODUTO_ID,
      :new.LOCAL_ID,
      :new.LOTE,
      :new.SALDO - :old.SALDO
    );
    
  end if;

end BLOQUEIOS_ESTOQUES_ITENS_IU_BR;
/