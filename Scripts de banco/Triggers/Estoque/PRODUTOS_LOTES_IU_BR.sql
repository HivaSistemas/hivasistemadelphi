create or replace trigger PRODUTOS_LOTES_IU_BR
before insert or update
on PRODUTOS_LOTES
for each row
declare
  xProdutoFilho         char(1);
  xExigirFabricacaoLote PRODUTOS.EXIGIR_DATA_FABRICACAO_LOTE%type;
  xExigirVencimentoLote PRODUTOS.EXIGIR_DATA_VENCIMENTO_LOTE%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then
    select
      case when PRODUTO_ID <> PRODUTO_PAI_ID then 'S' else 'N' end,
      EXIGIR_DATA_FABRICACAO_LOTE,
      EXIGIR_DATA_VENCIMENTO_LOTE
    into
      xProdutoFilho,
      xExigirFabricacaoLote,
      xExigirVencimentoLote
    from
      PRODUTOS
    where PRODUTO_ID = :new.PRODUTO_ID;

    if xProdutoFilho = 'S' then
      ERRO('Produtos filhos n�o pode ter lotes cadastrados!');
    end if;

    if xExigirFabricacaoLote = 'S' and :new.DATA_FABRICACAO is null then
      ERRO('A data de fabrica��o para o produto ' || :new.PRODUTO_ID || ' lote ' || :new.LOTE || ' n�o foi informado corretamente!');
    end if;

    if xExigirVencimentoLote = 'S' and :new.DATA_VENCIMENTO is null then
      ERRO('A data de vencimento para o produto ' || :new.PRODUTO_ID || ' lote ' || :new.LOTE || ' n�o foi informado corretamente!');
    end if;
  end if;

end PRODUTOS_LOTES_IU_BR;
/
