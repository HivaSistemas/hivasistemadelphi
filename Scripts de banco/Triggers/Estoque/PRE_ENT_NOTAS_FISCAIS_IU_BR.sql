create or replace trigger PRE_ENT_NOTAS_FISCAIS_IU_BR
before insert or update
on PRE_ENTRADAS_NOTAS_FISCAIS
for each row
declare
  vQtde       number;
  vEntradaId  ENTRADAS_NOTAS_FISCAIS.ENTRADA_ID%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then  
    :new.STATUS := 'AMA';

    select
      count(*)
    into
      vQtde
    from
      ENTRADAS_NOTAS_FISCAIS
    where RETORNA_NUMEROS(CHAVE_NFE) = RETORNA_NUMEROS(:new.CHAVE_ACESSO_NFE);

    /* Se chave existe a entrada da nota fiscal */
    /* então já salvando como entrada realizada */
    if vQtde > 0 then
      select
        ENTRADA_ID
      into
        vEntradaId
      from
        ENTRADAS_NOTAS_FISCAIS
      where RETORNA_NUMEROS(CHAVE_NFE) = RETORNA_NUMEROS(:new.CHAVE_ACESSO_NFE);

      :new.ENTRADA_ID := vEntradaId;
      :new.STATUS     := 'ENT';
    end if;
  else  
    if :old.STATUS <> :new.STATUS then
      if :new.STATUS = 'MAN' then
        :new.USUARIO_MANIFESTACAO_ID := SESSAO.USUARIO_SESSAO_ID;
        :new.DATA_HORA_MANIFESTACAO  := sysdate;
      elsif :new.STATUS = 'ONR' then
        :new.USUARIO_OPER_NAO_REALIZADA_ID := SESSAO.USUARIO_SESSAO_ID;
        :new.DATA_HORA_OPER_NAO_REALIZADA  := sysdate;
      elsif :new.STATUS = 'DES' then
        :new.USUARIO_DESCONHECIMENTO_ID := SESSAO.USUARIO_SESSAO_ID;
        :new.DATA_HORA_DESCONHECIMENTO  := sysdate;
      elsif :new.STATUS = 'CON' then
        :new.USUARIO_CONFIRMACAO_ID := SESSAO.USUARIO_SESSAO_ID;
        :new.DATA_HORA_CONFIRMACAO  := sysdate;         
      end if;
    end if;
  end if;

end PRE_ENT_NOTAS_FISCAIS_IU_BR;
/