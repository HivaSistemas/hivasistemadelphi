﻿create or replace trigger ESTOQUES_IU_BR
before insert or update
on ESTOQUES
for each row
declare
  vTipoControleEstoque   PRODUTOS.TIPO_CONTROLE_ESTOQUE%type;
  vProdutoPaiId          PRODUTOS.PRODUTO_PAI_ID%type;
  vAceitaEstoqueNegativo PRODUTOS.ACEITAR_ESTOQUE_NEGATIVO%type;
begin

  select
    TIPO_CONTROLE_ESTOQUE,
    PRODUTO_PAI_ID,
    ACEITAR_ESTOQUE_NEGATIVO
  into
    vTipoControleEstoque,
    vProdutoPaiId,
    vAceitaEstoqueNegativo
  from
    PRODUTOS
  where PRODUTO_ID = :new.PRODUTO_ID;

  if inserting then
    if vTipoControleEstoque in('K', 'A') then
      ERRO('Produtos kit ou industrialização não podem ter custos definidos!');
    end if;
  end if;
  
  if updating then
    :new.DISPONIVEL := :new.FISICO - :new.RESERVADO - :new.BLOQUEADO;

    if
      (:new.DISPONIVEL < 0) and
      (vAceitaEstoqueNegativo = 'N') and
      (
        (SESSAO.ROTINA not in('ATUALIZAR_ENTRADA_NF'))
        and
        (SESSAO.ROTINA = 'REALIZAR_AJUSTE_ESTOQUE') and (:new.DISPONIVEL < :old.DISPONIVEL)
      )
    then
      ERRO(
        'Estoque insuficiente para a operação! ' || chr(13) ||
        ' Produto: ' || :new.PRODUTO_ID || chr(13) ||
        ' Empresa: ' || :new.EMPRESA_ID
      );
    end if;
  end if;

end ESTOQUES_IU_BR;
/