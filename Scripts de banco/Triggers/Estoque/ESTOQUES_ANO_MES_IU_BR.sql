create or replace trigger ESTOQUES_ANO_MES_IU_BR
before insert or update
on ESTOQUES_ANO_MES
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

end ESTOQUES_ANO_MES_IU_BR;
/
