create or replace trigger COMPRAS_IU_BR
before insert or update
on COMPRAS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.DATA_HORA_COMPRA    := sysdate;
    
  else
    if :old.STATUS <> :new.STATUS then
      if :old.STATUS = 'A' and :new.STATUS = 'B' then
        :new.USUARIO_BAIXA_ID := SESSAO.USUARIO_SESSAO_ID;
        :new.DATA_HORA_BAIXA  := sysdate;
      elsif :old.STATUS = 'A' and :new.STATUS = 'C' then
        :new.USUARIO_CANCELAMENTO_ID := SESSAO.USUARIO_SESSAO_ID;
        :new.DATA_HORA_CANCELAMENTO  := sysdate;
      elsif :old.STATUS = 'B' and :new.STATUS = 'A' and SESSAO.ROTINA in('DESCONSOLIDAR_EST_ENTRADA_NF', 'CANCELAR_COMPRA_BAIXA') then
        :new.USUARIO_BAIXA_ID := null;
        :new.DATA_HORA_BAIXA  := null;
      else
        ERRO('Altera��o de status de compra n�o permitida!');
      end if;
    end if;
  end if;

end COMPRAS_IU_BR;
/