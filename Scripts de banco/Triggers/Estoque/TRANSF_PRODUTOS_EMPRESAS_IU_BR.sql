create or replace trigger TRANSF_PRODUTOS_EMPRESAS_IU_BR
before insert or update
on TRANSF_PRODUTOS_EMPRESAS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
    :new.DATA_HORA_CADASTRO  := sysdate;
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
  else
    if :old.STATUS <> :new.STATUS then
      if :new.STATUS = 'B' then
        :new.DATA_HORA_BAIXA  := sysdate;
        :new.USUARIO_BAIXA_ID := SESSAO.USUARIO_SESSAO_ID;
      elsif :new.STATUS = 'C' then
        :new.DATA_HORA_CANCELAMENTO  := sysdate;
        :new.USUARIO_CANCELAMENTO_ID := SESSAO.USUARIO_SESSAO_ID;
      end if;    
    end if;
  end if;
  
end TRANSF_PRODUTOS_EMPRESAS_IU_BR;
/