create or replace trigger LOGS_ESTOQUES_IU_BR
before insert or update
on LOGS_ESTOQUES
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

end LOGS_ESTOQUES_IU_BR;
/