﻿create or replace trigger TRANSFERENCIAS_LOC_ITENS_IU_BR
before insert or update
on TRANSFERENCIAS_LOCAIS_ITENS
for each row
declare
  vEmpresaId      TRANSFERENCIAS_LOCAIS.EMPRESA_ID%type;
  vLocalOrigemId  TRANSFERENCIAS_LOCAIS.LOCAL_ORIGEM_ID%type;
  vConfirmarTransfLocais PARAMETROS.CONFIRMAR_TRANSF_LOCAIS%type;
  vExisteRegistro  number;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
  
    select
      EMPRESA_ID,
      LOCAL_ORIGEM_ID
    into
      vEmpresaId,
      vLocalOrigemId
    from
      TRANSFERENCIAS_LOCAIS
    where TRANSFERENCIA_LOCAL_ID = :new.TRANSFERENCIA_LOCAL_ID;

    /* Verificando se utiliza confirmação de transferencia de locais, caso utilize
    a movimentação do estoque no local de destino só é feito após a confirmação */
    select
      CONFIRMAR_TRANSF_LOCAIS
    into
      vConfirmarTransfLocais
    from
      PARAMETROS;

    /*Verificando se o registro inserido é proveniente de uma confirmação
    de transferencia de estoque, caso seja deve movimentar o estoque do destino */
    select
      count(*)
    into
      vExisteRegistro
    from
      TRANSFERENCIAS_LOC_ITENS_TMP
    where TRANSFERENCIA_LOCAL_ID = :new.TRANSFERENCIA_LOCAL_ID;

    /* Realizando a saída do estoque físico no local de origem */
    if vExisteRegistro = 0 then
      MOVIMENTAR_ESTOQUE(vEmpresaId, :new.PRODUTO_ID, vLocalOrigemId, :new.LOTE, :new.QUANTIDADE, 'TLS', null, null);
    end if;

    /* Realizando a entrada do estoque físico no local de destino */
    if (vConfirmarTransfLocais = 'N') or (vExisteRegistro > 0) then
      MOVIMENTAR_ESTOQUE(vEmpresaId, :new.PRODUTO_ID, :new.LOCAL_DESTINO_ID, :new.LOTE, :new.QUANTIDADE, 'TLE', null, null);
    end if;
  
  end if;

end TRANSFERENCIAS_LOC_ITENS_IU_BR;
/