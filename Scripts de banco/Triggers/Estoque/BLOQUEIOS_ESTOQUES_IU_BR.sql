create or replace trigger BLOQUEIOS_ESTOQUES_IU_BR
before insert or update
on BLOQUEIOS_ESTOQUES
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then  
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.DATA_HORA_CADASTRO  := sysdate;    
  else  
    if :old.STATUS = 'A' and :new.STATUS = 'B' then
      :new.USUARIO_BAIXA_ID := SESSAO.USUARIO_SESSAO_ID;
      :new.DATA_HORA_BAIXA  := sysdate;
    end if;  
  end if;

end BLOQUEIOS_ESTOQUES_IU_BR;
/