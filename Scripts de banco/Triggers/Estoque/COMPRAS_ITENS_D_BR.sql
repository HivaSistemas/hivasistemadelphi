create or replace trigger COMPRAS_ITENS_D_BR
before delete
on COMPRAS_ITENS
for each row
declare
  vEmpresaId  COMPRAS.EMPRESA_ID%type;
begin

  select
    EMPRESA_ID
  into
    vEmpresaId
  from
    COMPRAS
  where COMPRA_ID = :old.COMPRA_ID;

  MOVIMENTAR_COMPRAS_PENDENTES(vEmpresaId, :old.PRODUTO_ID, :old.SALDO * :old.QUANTIDADE_EMBALAGEM * -1);

end COMPRAS_ITENS_D_BR;