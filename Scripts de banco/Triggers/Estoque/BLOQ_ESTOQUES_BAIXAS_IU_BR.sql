create or replace trigger BLOQ_ESTOQUES_BAIXAS_IU_BR
before insert or update
on BLOQUEIOS_ESTOQUES_BAIXAS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
    :new.USUARIO_BAIXA_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.DATA_HORA_BAIXA  := sysdate;
  end if;

end BLOQ_ESTOQUES_BAIXAS_IU_BR;
/