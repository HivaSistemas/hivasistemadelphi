create or replace trigger ENTRADAS_NOTAS_FISCAIS_D_BR
before delete
on ENTRADAS_NOTAS_FISCAIS
for each row
begin
  
  update PRE_ENTRADAS_NOTAS_FISCAIS set
    STATUS = 'MAN',
    ENTRADA_ID = null
  where CHAVE_ACESSO_NFE = RETORNA_NUMEROS(:old.CHAVE_NFE);

end ENTRADAS_NOTAS_FISCAIS_D_BR;
/