create or replace trigger BLOQUEIOS_ESTOQUES_ITENS_D_BR
before delete
on BLOQUEIOS_ESTOQUES_ITENS
for each row
declare
  vEmpresaId  BLOQUEIOS_ESTOQUES.EMPRESA_ID%type;
begin

  select
    EMPRESA_ID
  into
    vEmpresaId
  from
    BLOQUEIOS_ESTOQUES
  where BLOQUEIO_ID = :old.BLOQUEIO_ID;
  
  MOVIMENTAR_ESTOQUE_BLOQUEADO(
    vEmpresaId,
    :old.PRODUTO_ID,
    :old.LOCAL_ID,
    :old.LOTE,
    :old.QUANTIDADE * -1
  );

end BLOQUEIOS_ESTOQUES_ITENS_D_BR;
/