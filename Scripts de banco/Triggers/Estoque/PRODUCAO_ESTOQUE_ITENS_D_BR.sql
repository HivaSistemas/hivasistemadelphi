create or replace TRIGGER PRODUCAO_ESTOQUE_ITENS_D_BR
before delete
on PRODUCAO_ESTOQUE_ITENS
for each row
declare
  vEmpresaId  EMPRESAS.EMPRESA_ID%type;
begin

  select
    EMPRESA_ID
  into
    vEmpresaId
  from
    PRODUCAO_ESTOQUE
  where PRODUCAO_ESTOQUE_ID = :old.PRODUCAO_ESTOQUE_ID;

  MOVIMENTAR_ESTOQUE(
    vEmpresaId,
    :old.PRODUTO_ID,
    :old.LOCAL_ID,
    '???',
    :old.QUANTIDADE,
    case when :old.NATUREZA = 'E' then 'DAE' else 'DAS' end,
    null,
    null
  );

end PRODUCAO_ESTOQUE_ITENS_D_BR;
