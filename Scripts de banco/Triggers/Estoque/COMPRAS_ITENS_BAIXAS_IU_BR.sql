﻿create or replace trigger COMPRAS_ITENS_BAIXAS_IU_BR
before insert or update
on COMPRAS_ITENS_BAIXAS
for each row
declare
  vCompraId number;
begin

  if updating then
    ERRO('Não são permitidas alterações na tabela COMPRAS_ITENS_BAIXAS!');
  end if;

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
  
    select
      COMPRA_ID
    into
      vCompraId
    from
      COMPRAS_BAIXAS
    where BAIXA_ID = :new.BAIXA_ID;
  
    update COMPRAS_ITENS set
      BAIXADOS = BAIXADOS + :new.QUANTIDADE
    where COMPRA_ID = vCompraId
    and ITEM_ID = :new.ITEM_ID;
  
  end if;

end COMPRAS_ITENS_BAIXAS_IU_BR;
/