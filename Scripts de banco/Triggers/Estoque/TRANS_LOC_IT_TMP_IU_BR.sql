create or replace TRIGGER TRANS_LOC_IT_TMP_IU_BR
before insert or update
on TRANSFERENCIAS_LOC_ITENS_TMP
for each row
declare
  vEmpresaId      TRANSFERENCIAS_LOCAIS_TEMP.EMPRESA_ID%type;
  vLocalOrigemId  TRANSFERENCIAS_LOCAIS_TEMP.LOCAL_ORIGEM_ID%type;
begin
  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then

    select
      EMPRESA_ID,
      LOCAL_ORIGEM_ID
    into
      vEmpresaId,
      vLocalOrigemId
    from
      TRANSFERENCIAS_LOCAIS_TEMP
    where TRANSFERENCIA_LOCAL_ID = :new.TRANSFERENCIA_LOCAL_ID;

    /* Realizando a saida do estoque fisico no local de origem */
    MOVIMENTAR_ESTOQUE(vEmpresaId, :new.PRODUTO_ID, vLocalOrigemId, :new.LOTE, :new.QUANTIDADE, 'TLS', null, null);

    /* Realizando a entrada do estoque fisico no local de destino */
/*    MOVIMENTAR_ESTOQUE(vEmpresaId, :new.PRODUTO_ID, :new.LOCAL_DESTINO_ID, :new.LOTE, :new.QUANTIDADE, 'TLE', null, null); */

  end if;

end TRANS_LOC_IT_TMP_IU_BR;
