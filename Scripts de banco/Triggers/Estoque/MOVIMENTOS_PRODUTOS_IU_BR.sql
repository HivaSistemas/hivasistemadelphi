create or replace trigger MOVIMENTOS_PRODUTOS_IU_BR
before insert or update
on MOVIMENTOS_PRODUTOS
for each row
begin
  :new.DATA_HORA_MOVIMENTO      := sysdate;
  :new.FUNCIONARIO_MOVIMENTO_ID := SESSAO.USUARIO_SESSAO_ID;
end MOVIMENTOS_PRODUTOS_IU_BR;