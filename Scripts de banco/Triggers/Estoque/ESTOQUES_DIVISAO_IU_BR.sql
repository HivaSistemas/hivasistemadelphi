create or replace trigger ESTOQUES_DIVISAO_IU_BR
before insert or update
on ESTOQUES_DIVISAO
for each row
declare
  vAceitaEstoqueNegativo    PRODUTOS.ACEITAR_ESTOQUE_NEGATIVO%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if updating then
    :new.DISPONIVEL := :new.FISICO - :new.RESERVADO - :new.BLOQUEADO;
    if (not SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS) and SESSAO.ROTINA not in('REALIZAR_AJUSTE_ESTOQUE', 'ATUALIZAR_ENTRADA_NF', 'REALIZAR_TRANSF_LOCAIS', 'CONFIRMAR_RETIRADA') then
      if :new.DISPONIVEL < 0 then
        select
          ACEITAR_ESTOQUE_NEGATIVO
        into
          vAceitaEstoqueNegativo
        from
          PRODUTOS
        where PRODUTO_ID = :new.PRODUTO_ID;

        if vAceitaEstoqueNegativo = 'N' then
          ERRO(
            'Estoque insuficiente para a opera��o! ' || chr(13) ||
            ' Produto: ' || :new.PRODUTO_ID || chr(13) ||
            ' Empresa: ' || :new.EMPRESA_ID
          );
        end if;
      end if;
    end if;
  end if;

end ESTOQUES_DIVISAO_IU_BR;
/
