create or replace trigger TRANSF_PROD_EMPR_ITENS_D_BR
before delete
on TRANSF_PRODUTOS_EMPRESAS_ITENS
for each row
declare
  vEmpresaOrigemId  TRANSF_PRODUTOS_EMPRESAS.EMPRESA_ORIGEM_ID%type;
begin
  
  select
    EMPRESA_ORIGEM_ID
  into
    vEmpresaOrigemId
  from
    TRANSF_PRODUTOS_EMPRESAS
  where TRANSFERENCIA_ID = :new.TRANSFERENCIA_ID;

  MOVIMENTAR_ESTOQUE(
    vEmpresaOrigemId,
    :new.PRODUTO_ID,
    :new.LOCAL_ORIGEM_ID,
    :new.LOTE,
    :new.QUANTIDADE * -1,
    'TPS',
    null,
    null
  );
  
end TRANSF_PROD_EMPR_ITENS_D_BR;
/