create or replace trigger ENTRADAS_NF_ITENS_COMP_D_BR
before delete
on ENTRADAS_NF_ITENS_COMPRAS
for each row
begin  

  update COMPRAS_ITENS set
    ENTREGUES = ENTREGUES - :old.QUANTIDADE
  where COMPRA_ID = :old.COMPRA_ID
  and ITEM_ID = :old.ITEM_COMPRA_ID;

end ENTRADAS_NF_ITENS_COMP_D_BR;
/