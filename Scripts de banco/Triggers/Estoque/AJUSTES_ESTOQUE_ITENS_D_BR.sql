create or replace trigger AJUSTES_ESTOQUE_ITENS_D_BR
before delete
on AJUSTES_ESTOQUE_ITENS
for each row
declare
  vEmpresaId  EMPRESAS.EMPRESA_ID%type;
  vTipoEstoqueAjustado AJUSTES_ESTOQUE.TIPO_ESTOQUE_AJUSTADO%type;
begin
  
  select
    EMPRESA_ID,
    TIPO_ESTOQUE_AJUSTADO
  into
    vEmpresaId,
    vTipoEstoqueAjustado
  from
    AJUSTES_ESTOQUE
  where AJUSTE_ESTOQUE_ID = :old.AJUSTE_ESTOQUE_ID; 

  if vTipoEstoqueAjustado = 'FISCAL' then
    MOVIMENTAR_ESTOQUE_FISCAL(
      vEmpresaId,
      :old.PRODUTO_ID,
      case when :old.NATUREZA = 'S' then :old.QUANTIDADE else :old.QUANTIDADE * -1 end
    );
  else
    MOVIMENTAR_ESTOQUE(
      vEmpresaId,
      :old.PRODUTO_ID,
      :old.LOCAL_ID,
      :old.LOTE,
      :old.QUANTIDADE,
      case when :old.NATUREZA = 'E' then 'DAE' else 'DAS' end,
      null,
      null
    );
  end if;

end AJUSTES_ESTOQUE_ITENS_D_BR;
/