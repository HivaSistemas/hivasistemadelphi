create or replace trigger PRE_ENTRADAS_CTE_IU_BR
before insert or update
on PRE_ENTRADAS_CTE
for each row
declare
  vQtde             number;
  vConhecimentoId   CONHECIMENTOS_FRETES.CONHECIMENTO_ID%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then  
    :new.STATUS := 'AEN';

    select
      count(*)
    into
      vQtde
    from
      CONHECIMENTOS_FRETES
    where RETORNA_NUMEROS(CHAVE_CONHECIMENTO) = RETORNA_NUMEROS(:new.CHAVE_ACESSO_CTE);

    /* Se chave existe a entrada da nota fiscal */
    /* então já salvando como entrada realizada */
    if vQtde > 0 then
      select
        CONHECIMENTO_ID
      into
        vConhecimentoId
      from
        CONHECIMENTOS_FRETES
      where RETORNA_NUMEROS(CHAVE_CONHECIMENTO) = RETORNA_NUMEROS(:new.CHAVE_ACESSO_CTE);

      :new.CONHECIMENTO_ID := vConhecimentoId;
      :new.STATUS          := 'ENT';
    end if;  
  end if;

end PRE_ENTRADAS_CTE_IU_BR;
/