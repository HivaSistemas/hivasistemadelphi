﻿create or replace trigger TURNOS_CAIXA_IU_BR
before insert or update
on TURNOS_CAIXA
for each row
declare  
  vTurnoAbertoId   naturaln default 0; -- NaturalN pois pode ser zero mais não pode ser null
begin

  if :old.STATUS = 'B' then
    ERRO('Não é permitido alterar um turno já baixado!');
  end if;

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then
    begin
      select TURNO_ID
      into vTurnoAbertoId
      from TURNOS_CAIXA
      where FUNCIONARIO_ID = :new.FUNCIONARIO_ID
      and DATA_HORA_FECHAMENTO is null
      and rownum = 1;
    exception
      when others then
        vTurnoAbertoId := 0;
    end;
  
    if vTurnoAbertoId > 0 then
      ERRO( 'Já existe um turno em aberto para este funcionário. Turno: ' || vTurnoAbertoId );
    end if;

    :new.STATUS := 'A';
    :new.DATA_HORA_ABERTURA := sysdate;
    :new.USUARIO_ABERTURA_ID := SESSAO.USUARIO_SESSAO_ID;

    if :new.VALOR_INICIAL > 0 then
      MOVIMENTAR_SALDO_CONTA( :new.CONTA_ORIGEM_DIN_INI_ID, :new.VALOR_INICIAL * -1, 'T' );
    end if;
  else
    /* Se estiver fechando o turno */
    if :old.STATUS = 'A' and :new.STATUS = 'B' then
      :new.USUARIO_FECHAMENTO_ID := SESSAO.USUARIO_SESSAO_ID;
      :new.DATA_HORA_FECHAMENTO := sysdate;
    end if;
  end if;

end TURNOS_CAIXA_IU_BR;
/
