create or replace trigger MOVIMENTOS_CONTAS_IU_BR
before insert or update
on MOVIMENTOS_CONTAS
for each row
declare
  vSinal number;
begin
  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then
    :new.USUARIO_MOVIMENTO_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.CONCILIADO           := 'N';

    if :new.TIPO_MOVIMENTO = 'ENM' then
      MOVIMENTAR_SALDO_CONTA(:new.CONTA_ID, :new.VALOR_MOVIMENTO, 'A');
    elsif :new.TIPO_MOVIMENTO = 'TRA' then
      MOVIMENTAR_SALDO_CONTA(:new.CONTA_ID, :new.VALOR_MOVIMENTO * -1, 'A');
      MOVIMENTAR_SALDO_CONTA(:new.CONTA_DESTINO_ID, :new.VALOR_MOVIMENTO, 'A');
    end if;

  else
    vSinal := case when :new.TIPO_MOVIMENTO in('BXR', 'ENM') then 1 else -1 end;

    if :old.CONCILIADO = 'N' and :new.CONCILIADO = 'S' then
      :new.USUARIO_CONCILIACAO_ID := SESSAO.USUARIO_SESSAO_ID;
      :new.DATA_HORA_CONCILIACAO  := sysdate;

      if :new.TIPO_MOVIMENTO = 'TRA' then
        MOVIMENTAR_SALDO_CONTA(:new.CONTA_ID, :new.VALOR_MOVIMENTO * -1, 'C');
        MOVIMENTAR_SALDO_CONTA(:new.CONTA_DESTINO_ID, :new.VALOR_MOVIMENTO, 'C');
      else
        MOVIMENTAR_SALDO_CONTA(:new.CONTA_ID, :new.VALOR_MOVIMENTO * vSinal, 'C');
      end if;

    elsif :old.CONCILIADO = 'S' and :new.CONCILIADO = 'N' then
      :new.USUARIO_CONCILIACAO_ID := null;
      :new.DATA_HORA_CONCILIACAO  := null;

      if :new.TIPO_MOVIMENTO = 'TRA' then
        MOVIMENTAR_SALDO_CONTA(:new.CONTA_ID, :new.VALOR_MOVIMENTO, 'C');
        MOVIMENTAR_SALDO_CONTA(:new.CONTA_DESTINO_ID, :new.VALOR_MOVIMENTO * -1, 'C');
      else
        MOVIMENTAR_SALDO_CONTA(:new.CONTA_ID, :new.VALOR_MOVIMENTO * vSinal, 'C');
      end if;
    end if;
  end if;

end MOVIMENTOS_CONTAS_IU_BR;
/