﻿create or replace trigger MOVIMENTOS_TURNOS_ITENS_IU_BR
before insert or update
on MOVIMENTOS_TURNOS_ITENS
for each row
declare
  vQtde number;
begin

  vQtde := 0;
  if :new.TIPO = 'R' then
    select count(*)
    into vQtde
    from CONTAS_RECEBER
    where RECEBER_ID = :new.ID;
  else
    select count(*)
    into vQtde
    from CONTAS_PAGAR
    where PAGAR_ID = :new.ID;
  end if;
  
  if vQtde = 0 then
    ERRO('Não foi encontrado nenhum título financeiro!');
  end if;
  
end MOVIMENTOS_TURNOS_ITENS_IU_BR;