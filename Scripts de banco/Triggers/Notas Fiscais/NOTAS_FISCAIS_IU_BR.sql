﻿create or replace trigger NOTAS_FISCAIS_IU_BR
before insert or update
on NOTAS_FISCAIS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if inserting then
    :new.CHAVE_NFE := RETORNA_NUMEROS(:new.CHAVE_NFE);
  else
    if :new.STATUS = 'C' then
      :new.DATA_HORA_CANCELAMENTO_NOTA := sysdate;
      return;
    end if;
  end if;
  
end NOTAS_FISCAIS_IU_BR;
/