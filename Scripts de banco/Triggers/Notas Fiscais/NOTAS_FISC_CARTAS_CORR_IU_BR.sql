create or replace trigger NOTAS_FISC_CARTAS_CORR_IU_BR
before insert or update
on NOTAS_FISCAIS_CARTAS_CORRECOES
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.DATA_HORA_CADASTRO  := sysdate;
  else
    if :old.STATUS = 'N' and :new.STATUS = 'E' then
      :new.DATA_HORA_EMISSAO := sysdate;
    end if;
  end if;

end NOTAS_FISC_CARTAS_CORR_IU_BR;
/