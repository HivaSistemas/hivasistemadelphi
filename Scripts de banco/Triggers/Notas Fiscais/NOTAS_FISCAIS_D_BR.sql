﻿create or replace trigger NOTAS_FISCAIS_D_BR
before delete
on NOTAS_FISCAIS
for each row
begin

  if :old.STATUS in('E', 'C') then
    ERRO('Não é permitido deletar uma nota fiscal já emitida ou cancelada!');
  end if;

  if :old.NUMERO_NOTA is not null then
    ERRO('Não é permitido deletar uma nota fiscal que gerou numero!');
  end if;

  update RETIRADAS set
    NOTA_TRANSF_ESTOQUE_FISCAL_ID = null
  where NOTA_TRANSF_ESTOQUE_FISCAL_ID = :old.NOTA_FISCAL_ID;

  update ENTREGAS set
    NOTA_TRANSF_ESTOQUE_FISCAL_ID = null
  where NOTA_TRANSF_ESTOQUE_FISCAL_ID = :old.NOTA_FISCAL_ID;

  delete from LOGS_NOTAS_FISCAIS
  where NOTA_FISCAL_ID = :old.NOTA_FISCAL_ID;

  delete from LOGS_NOTAS_FISCAIS_ITENS
  where NOTA_FISCAL_ID = :old.NOTA_FISCAL_ID;

  delete from NOTAS_FISCAIS_REFERENCIAS
  where NOTA_FISCAL_ID = :old.NOTA_FISCAL_ID;

  delete from NOTAS_FISCAIS_ITENS
  where NOTA_FISCAL_ID = :old.NOTA_FISCAL_ID;
  
end NOTAS_FISCAIS_D_BR;
/