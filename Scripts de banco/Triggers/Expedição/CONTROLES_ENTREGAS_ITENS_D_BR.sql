create or replace trigger CONTROLES_ENTREGAS_ITENS_D_BR
before delete
on CONTROLES_ENTREGAS_ITENS
for each row
begin

  /* Se ainda não houve a circulação da mercadoria, */
  /* apagando para que seja possível refazer o controle de entrega */
  delete from NOTAS_FISCAIS
  where ENTREGA_ID = :old.ENTREGA_ID
  and TIPO_MOVIMENTO = 'VEN'
  and NUMERO_NOTA is null;
  
  /* Voltando a entrega para aguardando carregamento */
  update ENTREGAS set
    CONTROLE_ENTREGA_ID = null,
    STATUS = 'AGC'
  where ENTREGA_ID = :old.ENTREGA_ID;    

end CONTROLES_ENTREGAS_ITENS_D_BR;
/