create or replace trigger RETIRADAS_ITENS_D_BR
before delete
on RETIRADAS_ITENS
for each row
declare
  vEmpresaId      RETIRADAS.EMPRESA_ID%type;
  vLocalId        RETIRADAS.LOCAL_ID%type;
  vTipoMovimento  RETIRADAS.TIPO_MOVIMENTO%type;
  vOrcamentoId    RETIRADAS.ORCAMENTO_ID%type;
  vConfirmada     RETIRADAS.CONFIRMADA%type;
begin

  select
    RET.EMPRESA_ID,
    RET.LOCAL_ID,
    RET.TIPO_MOVIMENTO,
    RET.ORCAMENTO_ID,
    RET.CONFIRMADA
  into
    vEmpresaId,
    vLocalId,
    vTipoMovimento,
    vOrcamentoId,
    vConfirmada
  from
    RETIRADAS RET
  where RETIRADA_ID = :old.RETIRADA_ID;

  SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS := true;
  begin
    /* Se movimentou o estoque fisico */
    if vConfirmada = 'S' then
      MOVIMENTAR_ESTOQUE(
        vEmpresaId,
        :old.PRODUTO_ID,
        vLocalId,
        :old.LOTE,
        :old.QUANTIDADE,
        'CRP',
        null,
        null
      );
    else
      MOVIMENTAR_ESTOQUE_RESERVADO(
        vEmpresaId,
        vLocalId,
        :old.PRODUTO_ID,
        :old.LOTE,
        :old.QUANTIDADE * -1
      );
    end if;
  exception
    when others then
      SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS := false;
      raise;
  end;
  SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS := false;

  update ORCAMENTOS_ITENS set
    QUANTIDADE_RETIRADOS = QUANTIDADE_RETIRADOS - :old.QUANTIDADE
  where ORCAMENTO_ID = vOrcamentoId
  and ITEM_ID = :old.ITEM_ID;

end RETIRADAS_ITENS_D_BR;
/