create or replace trigger ENTREGAS_IU_BR
before insert or update 
on ENTREGAS
for each row
declare
  vStatusPedido   ORCAMENTOS.STATUS%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  if SESSAO.ROTINA = 'UPDATE' then
    return;
  end if;

  select
    STATUS
  into
    vStatusPedido
  from
    ORCAMENTOS
  where ORCAMENTO_ID = :new.ORCAMENTO_ID;

  if vStatusPedido not in('RE', 'VE') then
    ERRO('Pedido ainda n�o recebido, por favor verifique!');
  end if;

  if inserting then
    :new.DATA_HORA_CADASTRO  := sysdate;
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
  else
    if :old.STATUS in('ENT', 'RPA') and SESSAO.ROTINA not in('GERAR_NF_TRANSF_MOVIMENTOS', 'GERAR_ENTREGA_PENDENCIA_S_M', 'ATU_QTDE_VIAS_IMP_COMP_ENT_S_M') then
      ERRO('N�o � permitido alterar uma entrega j� finalizada!');
    end if;

    if :old.STATUS = 'AGS' and :new.STATUS = 'ESE' then
      :new.USUARIO_INICIO_SEPARACAO_ID := SESSAO.USUARIO_SESSAO_ID;
      :new.DATA_HORA_INICIO_SEPARACAO  := sysdate;
    elsif :old.STATUS = 'ESE' and :new.STATUS = 'AGC' then
      :new.USUARIO_FINALIZOU_SEP_ID := SESSAO.USUARIO_SESSAO_ID;
      :new.DATA_HORA_FINALIZOU_SEP  := sysdate;
    elsif :old.STATUS not in('ENT', 'RPA') and :new.STATUS in('ENT', 'RPA') then
      :new.USUARIO_CONFIRMACAO_ID := SESSAO.USUARIO_SESSAO_ID;
      if :new.DATA_HORA_REALIZOU_ENTREGA is null then
        :new.DATA_HORA_REALIZOU_ENTREGA := sysdate;
      end if;
    elsif :old.STATUS = 'ESE' and :new.STATUS = 'AGS' then
      :new.USUARIO_INICIO_SEPARACAO_ID := null;
      :new.DATA_HORA_INICIO_SEPARACAO  := null;
    end if;
  end if;

end ENTREGAS_IU_BR;
/