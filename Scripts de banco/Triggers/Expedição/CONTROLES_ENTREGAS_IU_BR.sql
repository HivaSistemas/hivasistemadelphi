create or replace trigger CONTROLES_ENTREGAS_IU_BR
before insert or update
on CONTROLES_ENTREGAS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
    :new.STATUS := 'T';
    :new.DATA_HORA_CADASTRO  := sysdate;
  else
    if :old.STATUS = 'B' and :new.STATUS = 'B' then
      ERRO('O controle de entrega j� se encontra baixado!');
    end if;

    if :old.STATUS = 'B' and :new.STATUS = 'T' then
      ERRO('N�o � permitido alterar o status de um controle de entrega baixado!');
    end if;

    if :old.STATUS = 'T' and :new.STATUS = 'B' then
      :new.USUARIO_BAIXA_ID := SESSAO.USUARIO_SESSAO_ID;
      :new.DATA_HORA_BAIXA  := sysdate;
    end if;  
  end if;

end CONTROLES_ENTREGAS_IU_BR;
