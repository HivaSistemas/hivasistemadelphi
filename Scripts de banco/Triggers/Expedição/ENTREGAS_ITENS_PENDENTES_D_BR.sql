create or replace trigger ENTREGAS_ITENS_PENDENTES_D_BR
before delete
on ENTREGAS_ITENS_PENDENTES
for each row
begin

  SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS := true;  
  begin
    -- Reservando a nova quantidade
    MOVIMENTAR_ESTOQUE_RESERVADO(
      :old.EMPRESA_ID,
      :old.LOCAL_ID,
      :old.PRODUTO_ID,
      :old.LOTE,
      :old.SALDO * -1
    );

    update ORCAMENTOS_ITENS set
      QUANTIDADE_ENTREGAR = QUANTIDADE_ENTREGAR - :old.SALDO
    where ORCAMENTO_ID = :old.ORCAMENTO_ID
    and ITEM_ID = :old.ITEM_ID;
  exception
    when others then
      SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS := false;
      raise;
  end;  
  SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS := false;
  
end ENTREGAS_ITENS_PENDENTES_D_BR;