create or replace trigger CONTROLES_ENTREGAS_ITENS_IU_BR
before insert or update
on CONTROLES_ENTREGAS_ITENS
for each row
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;
  
  if inserting then
    update ENTREGAS set
      CONTROLE_ENTREGA_ID = :new.CONTROLE_ENTREGA_ID,
      STATUS = 'ERO'
    where ENTREGA_ID = :new.ENTREGA_ID;

    GERAR_NOTA_ENTREGA(:new.ENTREGA_ID);
  end if;

end CONTROLES_ENTREGAS_ITENS_IU_BR;
/
