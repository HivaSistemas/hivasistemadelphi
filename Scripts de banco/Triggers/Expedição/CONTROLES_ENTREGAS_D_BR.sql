﻿create or replace trigger CONTROLES_ENTREGAS_D_BR
before delete
on CONTROLES_ENTREGAS
for each row
begin

  if :old.STATUS = 'B' then
    ERRO('Não é permitido deletar um controle de entrega já baixado|!');
  end if;

  delete from CONTROLES_ENTREGAS_AJUDANTES
  where CONTROLE_ENTREGA_ID = :old.CONTROLE_ENTREGA_ID;

  delete from CONTROLES_ENTREGAS_ITENS
  where CONTROLE_ENTREGA_ID = :old.CONTROLE_ENTREGA_ID;
  
end CONTROLES_ENTREGAS_D_BR;
/
