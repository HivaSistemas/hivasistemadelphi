create or replace trigger RETIRADAS_IU_BR
before insert or update 
on RETIRADAS
for each row
declare
  vStatusPedido     ORCAMENTOS.STATUS%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  select
    STATUS
  into
    vStatusPedido
  from
    ORCAMENTOS
  where ORCAMENTO_ID = :new.ORCAMENTO_ID;

  if (vStatusPedido not in( 'VE', 'RE')) and (SESSAO.ROTINA not in('ATUA_ORCAMENTO_PDV', 'ATUALIZAR_ORCAMENTO')) then
    ERRO('Pedido ainda n�o recebido, por favor verifique!');
  end if;

  if inserting then
    :new.DATA_HORA_CADASTRO  := sysdate;
    :new.USUARIO_CADASTRO_ID := SESSAO.USUARIO_SESSAO_ID;
  else
    if :old.CONFIRMADA = 'N' and :new.CONFIRMADA = 'S' then
      :new.USUARIO_CONFIRMACAO_ID := SESSAO.USUARIO_SESSAO_ID;
      if :new.DATA_HORA_RETIRADA is null then
        :new.DATA_HORA_RETIRADA := sysdate;
      end if;
    end if;
  end if;

end RETIRADAS_IU_BR;
/