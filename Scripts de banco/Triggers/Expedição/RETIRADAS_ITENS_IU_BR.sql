create or replace trigger RETIRADAS_ITENS_IU_BR
before insert or update
on RETIRADAS_ITENS
for each row
declare
  vEmpresaId      RETIRADAS.EMPRESA_ID%type;
  vLocalId        RETIRADAS.LOCAL_ID%type;
  vTipoMovimento  RETIRADAS.TIPO_MOVIMENTO%type;
  vOrcamentoId    RETIRADAS.ORCAMENTO_ID%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  select
    RET.EMPRESA_ID,
    RET.LOCAL_ID,
    RET.TIPO_MOVIMENTO,
    RET.ORCAMENTO_ID
  into
    vEmpresaId,
    vLocalId,
    vTipoMovimento,
    vOrcamentoId
  from
    RETIRADAS RET
  where RETIRADA_ID = :new.RETIRADA_ID;

  SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS := true;
  begin
    if inserting then
      MOVIMENTAR_ESTOQUE_RESERVADO(
        vEmpresaId,
        vLocalId,
        :new.PRODUTO_ID,
        :new.LOTE,
        :new.QUANTIDADE
      );

      update ORCAMENTOS_ITENS set
        QUANTIDADE_RETIRADOS = QUANTIDADE_RETIRADOS + :new.QUANTIDADE
      where ORCAMENTO_ID = vOrcamentoId
      and PRODUTO_ID = :new.PRODUTO_ID;
    end if;
  exception
    when others then
      SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS := false;
      raise;
  end;
  SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS := false;

  if updating then
    if :old.DEVOLVIDOS <> :new.DEVOLVIDOS then
      update ORCAMENTOS_ITENS set
        QUANTIDADE_RETIRADOS = QUANTIDADE_RETIRADOS - (:new.DEVOLVIDOS - :old.DEVOLVIDOS),
        QUANTIDADE_DEVOLV_ENTREGUES = QUANTIDADE_DEVOLV_ENTREGUES + (:new.DEVOLVIDOS - :old.DEVOLVIDOS)
      where ORCAMENTO_ID = vOrcamentoId
      and ITEM_ID = :new.ITEM_ID;
    end if;
  end if;

end RETIRADAS_ITENS_IU_BR;
