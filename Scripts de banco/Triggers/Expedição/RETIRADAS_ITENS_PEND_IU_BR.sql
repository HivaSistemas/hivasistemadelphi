create or replace trigger RETIRADAS_ITENS_PEND_IU_BR
before insert or update
on RETIRADAS_ITENS_PENDENTES
for each row
begin

  if SESSAO.ROTINA = 'DESVIAR' then
    return;
  end if;

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  :new.SALDO := :new.QUANTIDADE - :new.ENTREGUES - :new.DEVOLVIDOS;

  SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS := true;  
  begin  
    if updating then    
      -- Voltando o estoque
      MOVIMENTAR_ESTOQUE_RESERVADO(
        :old.EMPRESA_ID,
        :old.LOCAL_ID,
        :old.PRODUTO_ID,
        :old.LOTE,
        :old.SALDO * -1
      );
    end if;

    -- Reservando a nova quantidade
    MOVIMENTAR_ESTOQUE_RESERVADO(
      :new.EMPRESA_ID,
      :new.LOCAL_ID,
      :new.PRODUTO_ID,
      :new.LOTE,
      :new.SALDO
    );
  exception
    when others then
      SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS := false;
      raise;
  end;  
  SESSAO.EXECUTANDO_TRIGGERS_ENTREGAS := false;

  update ORCAMENTOS_ITENS set
    QUANTIDADE_RETIRAR = QUANTIDADE_RETIRAR + (:new.SALDO - nvl(:old.SALDO, 0)),
    QUANTIDADE_DEVOLV_PENDENTES = QUANTIDADE_DEVOLV_PENDENTES + :new.DEVOLVIDOS - nvl(:old.DEVOLVIDOS, 0)
  where ORCAMENTO_ID = :new.ORCAMENTO_ID
  and ITEM_ID = :new.ITEM_ID;
  
end RETIRADAS_ITENS_PEND_IU_BR;
/
