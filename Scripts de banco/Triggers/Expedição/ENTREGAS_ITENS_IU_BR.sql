create or replace trigger ENTREGAS_ITENS_IU_BR
before insert or update
on ENTREGAS_ITENS
for each row
declare
  vOrcamentoId    ENTREGAS.ORCAMENTO_ID%type;
  vEmpresaId      ENTREGAS.EMPRESA_ENTREGA_ID%type;
  vLocalId        ENTREGAS.LOCAL_ID%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;

  select
    ENT.ORCAMENTO_ID,
    ENT.EMPRESA_ENTREGA_ID,
    ENT.LOCAL_ID
  into
    vOrcamentoId,
    vEmpresaId,
    vLocalId
  from
    ENTREGAS ENT
  where ENT.ENTREGA_ID = :new.ENTREGA_ID;

  if inserting then
    MOVIMENTAR_ESTOQUE(
      vEmpresaId,
      :new.PRODUTO_ID,
      vLocalId,
      :new.LOTE,
      :new.QUANTIDADE,
      'ENT',
      null,
      null
    );

    update ORCAMENTOS_ITENS set
      QUANTIDADE_ENTREGUES = QUANTIDADE_ENTREGUES + :new.QUANTIDADE
    where ORCAMENTO_ID = vOrcamentoId
    and PRODUTO_ID = :new.PRODUTO_ID
    and ITEM_ID = :new.ITEM_ID;
  else
    /* Se est� havendo retorno de entrega ou se na separa��o n�o foi encontrado os produtos */
    if :old.RETORNADOS <> :new.RETORNADOS or :old.NAO_SEPARADOS <> :new.NAO_SEPARADOS then
      /* Voltando o estoque f�sico */
      MOVIMENTAR_ESTOQUE(
        vEmpresaId,
        :new.PRODUTO_ID,
        vLocalId,
        :new.LOTE,
        (:new.RETORNADOS - :old.RETORNADOS) + (:new.NAO_SEPARADOS - :old.NAO_SEPARADOS),
        'CEN',
        null,
        null
      );

      update ORCAMENTOS_ITENS set
        QUANTIDADE_ENTREGUES = QUANTIDADE_ENTREGUES - ((:new.RETORNADOS - :old.RETORNADOS) + (:new.NAO_SEPARADOS - :old.NAO_SEPARADOS))
      where ORCAMENTO_ID = vOrcamentoId
      and ITEM_ID = :new.ITEM_ID;
    /* Se for devolu��o */
    elsif :old.DEVOLVIDOS <> :new.DEVOLVIDOS then
      update ORCAMENTOS_ITENS set
        QUANTIDADE_ENTREGUES = QUANTIDADE_ENTREGUES - (:new.DEVOLVIDOS - :old.DEVOLVIDOS),
        QUANTIDADE_DEVOLV_ENTREGUES = QUANTIDADE_DEVOLV_ENTREGUES + (:new.DEVOLVIDOS - :old.DEVOLVIDOS)
      where ORCAMENTO_ID = vOrcamentoId
      and ITEM_ID = :new.ITEM_ID;
    end if;
  end if;

end ENTREGAS_ITENS_IU_BR;
/
