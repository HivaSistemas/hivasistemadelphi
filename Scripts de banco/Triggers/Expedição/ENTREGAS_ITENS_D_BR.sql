create or replace trigger ENTREGAS_ITENS_D_BR
before delete
on ENTREGAS_ITENS
for each row
declare
  vOrcamentoId    ORCAMENTOS.ORCAMENTO_ID%type;
  vEmpresaId      ORCAMENTOS.EMPRESA_ID%type;
  vLocalId        ENTREGAS.LOCAL_ID%type;
begin  
  select
    ENT.ORCAMENTO_ID,
    ENT.EMPRESA_ENTREGA_ID,
    ENT.LOCAL_ID
  into
    vOrcamentoId,
    vEmpresaId,
    vLocalId
  from
    ENTREGAS ENT
  where ENT.ENTREGA_ID = :old.ENTREGA_ID;
  
  update ORCAMENTOS_ITENS set
    QUANTIDADE_ENTREGUES = QUANTIDADE_ENTREGUES - :old.QUANTIDADE
  where ORCAMENTO_ID = vOrcamentoId
  and PRODUTO_ID = :old.PRODUTO_ID
  and ITEM_ID = :old.ITEM_ID;

  MOVIMENTAR_ESTOQUE(
    vEmpresaId,
    :old.PRODUTO_ID,
    vLocalId,
    :old.LOTE,
    :old.QUANTIDADE,
    'CEN',
    null,
    null
  );
  
end ENTREGAS_ITENS_D_BR;
/
