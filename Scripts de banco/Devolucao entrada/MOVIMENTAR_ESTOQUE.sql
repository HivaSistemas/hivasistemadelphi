﻿create or replace procedure MOVIMENTAR_ESTOQUE(
  iEMPRESA_ID in number,
  iPRODUTO_ID in number,
  iLOCAL_ID   in number,
  iLOTE       in string,
  iQUANTIDADE in number,
  iTIPO_MOVIMENTO in string,
  iDATA_FABRICACAO in date,
  iDATA_VENCIMENTO in date
)
is
  vQtdeVezesPai  PRODUTOS.QUANTIDADE_VEZES_PAI%type;
  vProdutoPaiId  PRODUTOS.PRODUTO_PAI_ID%type;
  vQuantidade    number;
begin
  select
    QUANTIDADE_VEZES_PAI,
    PRODUTO_PAI_ID
  into
    vQtdeVezesPai,
    vProdutoPaiId
  from
    PRODUTOS
  where PRODUTO_ID = iPRODUTO_ID;

  vQuantidade := iQUANTIDADE * vQtdeVezesPai;

  /* A data de fabricacao e data de vencimento deve sempre ser informado quando for uma movimentacao de entrada */
  /* Quando for saida nao precisa ser informado, isso e apenas para que o lote seja cadastrado corretamente */
  VERIFICAR_LOTE_CADASTRADO(iEMPRESA_ID, vProdutoPaiId, iLOCAL_ID, iLOTE, iDATA_FABRICACAO, iDATA_VENCIMENTO);

 /* iTIPO_MOVIMENTO
   ENM - Entradas de mercadorias
   DEN - Devolução de entradas

   DEV - Devolução de mercadorias entregues ( Retirados e Entregues )
   CDE - Cancelamento de devolução

   CRP - Cancelamento de recebimento de pedido

   RTA - Retirada no ato
   RET - Retiradas
   ENT - Entregas
   CEN - Cancelamento de entrega / Retorno de entrega

   AJE - Ajuste de estoque ENTRADA
   AJS - Ajuste de estoque SAÍDA
   DAE - Deletando ajuste de estoque ENTRADA
   DAS - Deletando ajuste de estoque SAIDA

   TPS - Transf. produtos SAÍDA
   TPE - Transf. produtos ENTRADA

   TLS - Transferência de locais SAÍDA
   TLE - Transferência de locais ENTRADA
 */

  if iTIPO_MOVIMENTO in('RTA', 'RET', 'ENT') then
    update ESTOQUES_DIVISAO set
      RESERVADO = RESERVADO - vQuantidade,
      FISICO = FISICO - vQuantidade
    where PRODUTO_ID = vProdutoPaiId
    and EMPRESA_ID = iEMPRESA_ID
    and LOCAL_ID = iLOCAL_ID
    and LOTE = iLOTE;

    update ESTOQUES set
      RESERVADO = RESERVADO - vQuantidade,
      FISICO = FISICO - vQuantidade
    where PRODUTO_ID = vProdutoPaiId
    and EMPRESA_ID = iEMPRESA_ID;
  elsif iTIPO_MOVIMENTO in('AJE', 'DAS', 'CRP', 'DEV', 'ENM', 'CEN', 'TPE') then
    update ESTOQUES_DIVISAO set
      FISICO = FISICO + vQuantidade
    where PRODUTO_ID = vProdutoPaiId
    and EMPRESA_ID = iEMPRESA_ID
    and LOCAL_ID = iLOCAL_ID
    and LOTE = iLOTE;

    update ESTOQUES set
      FISICO = FISICO + vQuantidade
    where PRODUTO_ID = vProdutoPaiId
    and EMPRESA_ID = iEMPRESA_ID;
  elsif iTIPO_MOVIMENTO in('AJS', 'DAE', 'TPS', 'DEN', 'CDE') then
    update ESTOQUES_DIVISAO set
      FISICO = FISICO - vQuantidade
    where PRODUTO_ID = vProdutoPaiId
    and EMPRESA_ID = iEMPRESA_ID
    and LOCAL_ID = iLOCAL_ID
    and LOTE = iLOTE;

    update ESTOQUES set
      FISICO = FISICO - vQuantidade
    where PRODUTO_ID = vProdutoPaiId
    and EMPRESA_ID = iEMPRESA_ID;
  elsif iTIPO_MOVIMENTO in('TLS', 'TLE') then
    update ESTOQUES_DIVISAO set
      FISICO = FISICO + vQuantidade * case when iTIPO_MOVIMENTO = 'TLS' then -1 else 1 end
    where PRODUTO_ID = vProdutoPaiId
    and EMPRESA_ID = iEMPRESA_ID
    and LOCAL_ID = iLOCAL_ID
    and LOTE = iLOTE;
  else
    ERRO('Tipo de movimentação de estoque não implementada, entre em contato com o suporte!');
  end if;

end MOVIMENTAR_ESTOQUE;
/