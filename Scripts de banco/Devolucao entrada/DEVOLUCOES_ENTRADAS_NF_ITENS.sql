create table DEVOLUCOES_ENTRADAS_NF_ITENS(
  DEVOLUCAO_ID                number(12) not null,
  PRODUTO_ID                  number(10) not null,
  ITEM_ID                     number(3) not null,  
  VALOR_TOTAL                 number(8,2) not null,
  PRECO_UNITARIO              number(8,2) not null,
  QUANTIDADE                  number(20,4) not null,  
  VALOR_TOTAL_DESCONTO        number(8,2) not null,
  VALOR_TOTAL_OUTRAS_DESPESAS number(8,2) not null,

  LOCAL_ID                    number(5,0) not null,
  
  /* ICMS NORMAL */  
  BASE_CALCULO_ICMS           number(8,2) not null,
  VALOR_ICMS                  number(8,2) not null,
  /* ------------------------------------------------ */
  
  /* ST */    
  BASE_CALCULO_ICMS_ST        number(8,2) not null,
  VALOR_ICMS_ST               number(8,2) not null,
  /* ------------------------------------------------ */
  
  /* IPI */
  VALOR_IPI                   number(8,2) not null,
  /* ----------------------------------------------- */

  PESO_UNITARIO               number(8,3) default 0 not null,  

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint PK_DEV_ENTRADAS_NF_ITENS
primary key(DEVOLUCAO_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */ 
alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint FK_DEV_ENT_NOT_FISC_ITE_DEV_ID
foreign key(DEVOLUCAO_ID)
references DEVOLUCOES_ENTRADAS_NOTAS_FISC(DEVOLUCAO_ID);

alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint FK_DEV_ENT_NOT_FISC_ITE_PRO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint FK_DEV_ENT_NOT_FISC_ITE_LOC_ID
foreign key(LOCAL_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);


/* Constraints */
alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint CK_DEV_ENT_NF_ITE_PRECO_UNIT
check(PRECO_UNITARIO > 0);

alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint CK_DEV_ENT_NF_ITE_QUANTIDADE
check(QUANTIDADE > 0);

alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint CK_DEV_ENT_NF_ITE_VALOR_TOT
check(VALOR_TOTAL > 0);

alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint CK_DEV_ENT_NF_ITE_VLR_TOT_DESC
check(VALOR_TOTAL_DESCONTO >= 0);

alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint CK_DEV_ENT_NF_IT_VLR_T_OUT_DES
check(VALOR_TOTAL_OUTRAS_DESPESAS >= 0);

alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint CK_DEV_ENT_NF_ITE_BASE_C_ICMS
check(BASE_CALCULO_ICMS >= 0);

alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint CK_DEV_ENT_NF_ITE_VALOR_ICMS
check(VALOR_ICMS >= 0);

alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint CK_DEV_ENT_NF_ITE_CALC_ICMS_ST
check(BASE_CALCULO_ICMS_ST >= 0);

alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint CK_DEV_ENT_NF_ITE_VLR_ICMS_ST
check(VALOR_ICMS_ST >= 0);

alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint CK_DEV_ENT_NF_ITE_VALOR_IPI
check(VALOR_IPI >= 0);

alter table DEVOLUCOES_ENTRADAS_NF_ITENS
add constraint CK_DEV_ENT_NF_ITE_PESO_UNIT
check(PESO_UNITARIO >= 0);
