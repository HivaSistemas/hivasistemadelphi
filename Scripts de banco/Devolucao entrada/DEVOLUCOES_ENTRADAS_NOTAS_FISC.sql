﻿create table DEVOLUCOES_ENTRADAS_NOTAS_FISC(
  DEVOLUCAO_ID                number(12) not null,
  ENTRADA_ID                  number(12) not null,
  EMPRESA_ID                  number(3) not null,
  
  DATA_HORA_CADASTRO          date not null,
  USUARIO_CADASTRO_ID         number(4) not null,

  DATA_HORA_BAIXA             date,
  USUARIO_BAIXA_ID            number(4),
  
  STATUS                      char(3) default 'AGC' not null,
  
  VALOR_TOTAL                 number(8,2) not null,
  VALOR_TOTAL_PRODUTOS        number(8,2) not null,
  VALOR_DESCONTO              number(8,2) default 0 not null,
  VALOR_OUTRAS_DESPESAS       number(8,2) default 0 not null,
  
  BASE_CALCULO_ICMS           number(8,2) not null,
  VALOR_ICMS                  number(8,2) not null,

  BASE_CALCULO_ICMS_ST        number(8,2) not null,
  VALOR_ICMS_ST               number(8,2) not null,
 
  VALOR_IPI                   number(8,2) default 0 not null,

  MOTIVO_DEVOLUCAO            varchar2(400),
  OBSERVACOES_NFE             varchar2(400),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaría */
alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint PK_DEVOLUCOES_ENTR_NOTAS_FISC
primary key(DEVOLUCAO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint FK_DEV_ENT_NF_ENTRADA_ID
foreign key(ENTRADA_ID)
references ENTRADAS_NOTAS_FISCAIS(ENTRADA_ID);

alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint FK_DEV_ENT_NOTAS_FISC_EMP_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint FK_DEV_ENT_NF_USU_CADASTRO_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint FK_DEV_ENT_NF_USU_BAIXA_ID
foreign key(USUARIO_BAIXA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* Checagens */
alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint CK_DEV_ENT_NF_VALOR_TOTAL
check(VALOR_TOTAL > 0);

alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint CK_DEV_ENT_NF_VALOR_TOTAL_PROD
check(VALOR_TOTAL_PRODUTOS > 0);

alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint CK_DEV_ENT_NF_VALOR_DESCONTO
check(VALOR_DESCONTO >= 0);

alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint CK_DEV_ENT_NF_VLR_OUT_DESPES
check(VALOR_OUTRAS_DESPESAS >= 0);

alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint CK_DEV_ENT_NF_BASE_CALC_ICMS
check(BASE_CALCULO_ICMS >= 0);

alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint CK_DEV_ENT_NF_VALOR_ICMS
check(VALOR_ICMS >= 0);

alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint CK_DEV_ENT_NF_BASE_CAL_ICMS_ST
check(BASE_CALCULO_ICMS_ST >= 0);

alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint CK_DEV_ENT_NF_VALOR_ICMS_ST
check(VALOR_ICMS_ST >= 0);

alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint CK_DEV_ENT_NF_VALOR_IPI
check(VALOR_IPI >= 0);

/* 
  STATUS
  AGC - Aguardando conferencia
  BAI - Baixada
*/
alter table DEVOLUCOES_ENTRADAS_NOTAS_FISC
add constraint CK_DEV_ENT_NOTAS_FISC_STATUS
check(
  STATUS = 'AGC' and USUARIO_BAIXA_ID is null and DATA_HORA_BAIXA is null
  or
  STATUS = 'BAI' and USUARIO_BAIXA_ID is not null and DATA_HORA_BAIXA is not null
);
