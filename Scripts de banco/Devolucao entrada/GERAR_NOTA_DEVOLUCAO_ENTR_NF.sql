﻿create or replace procedure GERAR_NOTA_DEVOLUCAO_ENTR_NF(iDEVOLUCAO_ID in number)
is
  type RecDadosEntrada is record(    
    NUMERO_NOTA           ENTRADAS_NOTAS_FISCAIS.NUMERO_NOTA%type,
    SERIE_NOTA            ENTRADAS_NOTAS_FISCAIS.SERIE_NOTA%type,
    DATA_EMISSAO          ENTRADAS_NOTAS_FISCAIS.DATA_HORA_EMISSAO%type,
    CHAVE_NFE             ENTRADAS_NOTAS_FISCAIS.CHAVE_NFE%type,
    
    BASE_CALC_ICMS        number default 0,
    VALOR_ICMS            number default 0,
    
    BASE_CALC_ICMS_ST     number default 0,
    VALOR_ICMS_ST         number default 0,
    
    VALOR_IPI             number default 0
  );
  
  i                       number default 0;
  vQtde                   number default 0;
  vValorMaisSignificativo number default 0;

  vNotaFiscalId           NOTAS_FISCAIS.NOTA_FISCAL_ID%type;
  vTipoMovimento          RETIRADAS.TIPO_MOVIMENTO%type;  
  vDadosEntrada           RecDadosEntrada;
  vStatus                 DEVOLUCOES_ENTRADAS_NOTAS_FISC.STATUS%type;

  vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;
  vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;
  vDadosNota              RecordsNotasFiscais.RecNotaFiscal;
  vDadosItens             RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;

  vRegimeTributario       PARAMETROS_EMPRESA.REGIME_TRIBUTARIO%type;  
  vSerieNFe               PARAMETROS_EMPRESA.SERIE_NFE%type;
  
  vTipoOperacao           varchar(2) default 'I';  

  cursor cItens(pEstadoId in string, pTipoCliente in string, pEmpresaNotaId in number) is
  select
    EIT.PRODUTO_ID,    
    DIT.ITEM_ID,    
    DIT.PRECO_UNITARIO,
    DIT.QUANTIDADE,
    DIT.VALOR_TOTAL,
    DIT.VALOR_TOTAL_DESCONTO,
    DIT.VALOR_TOTAL_OUTRAS_DESPESAS,
    PRO.NOME as NOME_PRODUTO,
    PRO.CODIGO_NCM,
    PRO.UNIDADE_VENDA,
    PRO.CODIGO_BARRAS,
    PRO.CEST,
    EIT.CST,
    
    DIT.BASE_CALCULO_ICMS,
    EIT.INDICE_REDUCAO_BASE_ICMS,
    EIT.PERCENTUAL_ICMS,
    DIT.VALOR_ICMS,
    
    DIT.BASE_CALCULO_ICMS_ST,
    EIT.INDICE_REDUCAO_BASE_ICMS_ST,
    EIT.PERCENTUAL_ICMS_ST,
    DIT.VALOR_ICMS_ST,
    EIT.IVA,
    
    EIT.PERCENTUAL_IPI,
    DIT.VALOR_IPI
  from
    DEVOLUCOES_ENTRADAS_NF_ITENS DIT
    
  inner join DEVOLUCOES_ENTRADAS_NOTAS_FISC DEV
  on DIT.DEVOLUCAO_ID = DEV.DEVOLUCAO_ID

  inner join ENTRADAS_NOTAS_FISCAIS_ITENS EIT
  on DEV.ENTRADA_ID = EIT.ENTRADA_ID
  and DIT.ITEM_ID = EIT.ITEM_ID

  inner join PRODUTOS PRO
  on EIT.PRODUTO_ID = PRO.PRODUTO_ID
	
  where DIT.DEVOLUCAO_ID = iDEVOLUCAO_ID
  order by
    DIT.ITEM_ID;

begin

  select
    count(*)
  into
    vQtde
  from
    NOTAS_FISCAIS
  where DEVOLUCAO_ENTRADA_ID = iDEVOLUCAO_ID;

  if vQtde > 0 then
    ERRO('A retirada já gerou nota!');
  end if;
  
  begin
    select
      DEV.EMPRESA_ID,
      DEV.STATUS,      
      ENT.FORNECEDOR_ID,
      PAE.REGIME_TRIBUTARIO,
      PAE.SERIE_NFE,
      
      ENT.NUMERO_NOTA,
      ENT.SERIE_NOTA,
      trunc(ENT.DATA_HORA_EMISSAO),
      ENT.CHAVE_NFE,
      
      /* Emitente */
      EMP.RAZAO_SOCIAL,
      EMP.NOME_FANTASIA,      
      EMP.CNPJ,
      EMP.INSCRICAO_ESTADUAL,
      EMP.LOGRADOURO,
      EMP.COMPLEMENTO,
      BAE.NOME as NOME_BAIRRO_EMITENTE,
      CIE.NOME as NOME_CIDADE_EMITENTE,
      EMP.NUMERO,
      ESE.ESTADO_ID,
      EMP.CEP,
      EMP.TELEFONE_PRINCIPAL,
      CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,
      ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,
      
      /* Destinatario */
      CAD.NOME_FANTASIA,
      CAD.RAZAO_SOCIAL,
      CAD.TIPO_PESSOA,
      CAD.CPF_CNPJ,
      CAD.INSCRICAO_ESTADUAL,
      CAD.LOGRADOURO,
      CAD.COMPLEMENTO,
      CAD.NUMERO,
      CAD.CEP,
      BAI.NOME as NOME_BAIRRO,
      CID.NOME as NOME_CIDADE,
      CID.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_DEST,
      CID.ESTADO_ID as ESTADO_ID_DESTINATARIO,
      DEV.OBSERVACOES_NFE
    into
      vDadosNota.EMPRESA_ID,
      vStatus,
      vDadosNota.CADASTRO_ID,
      vRegimeTributario,
      vSerieNFe,
      
      vDadosEntrada.NUMERO_NOTA,
      vDadosEntrada.SERIE_NOTA,
      vDadosEntrada.DATA_EMISSAO,
      vDadosEntrada.CHAVE_NFE,

      /* Emitente */
      vDadosNota.RAZAO_SOCIAL_EMITENTE,
      vDadosNota.NOME_FANTASIA_EMITENTE,      
      vDadosNota.CNPJ_EMITENTE,
      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,
      vDadosNota.LOGRADOURO_EMITENTE,
      vDadosNota.COMPLEMENTO_EMITENTE,
      vDadosNota.NOME_BAIRRO_EMITENTE,
      vDadosNota.NOME_CIDADE_EMITENTE,
      vDadosNota.NUMERO_EMITENTE,
      vDadosNota.ESTADO_ID_EMITENTE,
      vDadosNota.CEP_EMITENTE,
      vDadosNota.TELEFONE_EMITENTE,
      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,
      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,

      /* Destinatario */
      vDadosNota.NOME_FANTASIA_DESTINATARIO,
      vDadosNota.RAZAO_SOCIAL_DESTINATARIO,
      vDadosNota.TIPO_PESSOA_DESTINATARIO,
      vDadosNota.CPF_CNPJ_DESTINATARIO,
      vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,
      vDadosNota.LOGRADOURO_DESTINATARIO,
      vDadosNota.COMPLEMENTO_DESTINATARIO,
      vDadosNota.NUMERO_DESTINATARIO,
      vDadosNota.CEP_DESTINATARIO,
      vDadosNota.NOME_BAIRRO_DESTINATARIO,
      vDadosNota.NOME_CIDADE_DESTINATARIO,
      vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,
      vDadosNota.ESTADO_ID_DESTINATARIO,      
      vDadosNota.INFORMACOES_COMPLEMENTARES
    from
      DEVOLUCOES_ENTRADAS_NOTAS_FISC DEV
    
    inner join ENTRADAS_NOTAS_FISCAIS ENT
    on DEV.ENTRADA_ID = ENT.ENTRADA_ID
    
    inner join CADASTROS CAD
    on ENT.FORNECEDOR_ID = CAD.CADASTRO_ID
        
    inner join BAIRROS BAI
    on CAD.BAIRRO_ID = BAI.BAIRRO_ID
    
    inner join CIDADES CID
		on BAI.CIDADE_ID = CID.CIDADE_ID 

    inner join EMPRESAS EMP
    on DEV.EMPRESA_ID = EMP.EMPRESA_ID

    inner join BAIRROS BAE
    on EMP.BAIRRO_ID = BAE.BAIRRO_ID
    
    inner join CIDADES CIE
    on BAE.CIDADE_ID = CIE.CIDADE_ID
    
    inner join ESTADOS ESE
    on CIE.ESTADO_ID = ESE.ESTADO_ID

    inner join PARAMETROS_EMPRESA PAE
    on DEV.EMPRESA_ID = PAE.EMPRESA_ID
    
    cross join PARAMETROS PAR
    
    where DEV.DEVOLUCAO_ID = iDEVOLUCAO_ID;
  exception
    when others then
      ERRO('Houve um erro ao buscar os dados da entrada para geração da NF! ' + sqlerrm);
  end;
  
  if vStatus <> 'BAI' then
    ERRO('Não é permitido gerar crédito de uma devolução de entrada que não esteja baixada!');
  end if; 

  vDadosNota.BASE_CALCULO_ICMS    := 0;
  vDadosNota.VALOR_ICMS           := 0;
  vDadosNota.BASE_CALCULO_ICMS_ST := 0;
  vDadosNota.VALOR_ICMS_ST        := 0;
  vDadosNota.BASE_CALCULO_PIS     := 0;
  vDadosNota.VALOR_PIS            := 0;
  vDadosNota.BASE_CALCULO_COFINS  := 0;
  vDadosNota.VALOR_COFINS         := 0;
  vDadosNota.VALOR_IPI            := 0;
  vDadosNota.VALOR_FRETE          := 0;
  vDadosNota.VALOR_SEGURO         := 0;
  vDadosNota.PESO_LIQUIDO         := 0;
  vDadosNota.PESO_BRUTO           := 0;    
	
  for vItens in cItens(vDadosNota.ESTADO_ID_DESTINATARIO, vDadosNota.TIPO_CLIENTE, vDadosNota.EMPRESA_ID) loop
    if length(vItens.CODIGO_NCM) < 8 then
      Erro('O código NCM do produto ' || vItens.PRODUTO_ID || ' - ' || vItens.NOME_PRODUTO || ' está incorreto, o NCM é composto de 8 digítos, verifique no cadastro de produtos!');
    end if;  

    vDadosItens(i).PRODUTO_ID    := vItens.PRODUTO_ID;
    vDadosItens(i).ITEM_ID       := vItens.ITEM_ID;
    vDadosItens(i).NOME_PRODUTO  := vItens.NOME_PRODUTO;
    vDadosItens(i).UNIDADE       := vItens.UNIDADE_VENDA;
    vDadosItens(i).CODIGO_NCM    := vItens.CODIGO_NCM;
    vDadosItens(i).QUANTIDADE    := vItens.QUANTIDADE;
    vDadosItens(i).CODIGO_BARRAS := vItens.CODIGO_BARRAS;
    vDadosItens(i).CEST          := vItens.CEST;
    
    vDadosItens(i).PRECO_UNITARIO              := vItens.PRECO_UNITARIO;
    vDadosItens(i).VALOR_TOTAL                 := vItens.VALOR_TOTAL;
    vDadosItens(i).VALOR_TOTAL_DESCONTO        := vItens.VALOR_TOTAL_DESCONTO;
    vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := vItens.VALOR_TOTAL_OUTRAS_DESPESAS;

    vDadosItens(i).CST                        := vItens.CST;
    vDadosItens(i).BASE_CALCULO_ICMS          := vItens.BASE_CALCULO_ICMS;
    vDadosItens(i).INDICE_REDUCAO_BASE_ICMS   := vItens.INDICE_REDUCAO_BASE_ICMS;
    vDadosItens(i).PERCENTUAL_ICMS            := vItens.PERCENTUAL_ICMS;
    vDadosItens(i).VALOR_ICMS                 := vItens.VALOR_ICMS;

    vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST := vItens.INDICE_REDUCAO_BASE_ICMS_ST;
    vDadosItens(i).BASE_CALCULO_ICMS_ST        := vItens.BASE_CALCULO_ICMS_ST;
    vDadosItens(i).PERCENTUAL_ICMS_ST          := vItens.PERCENTUAL_ICMS_ST;
    vDadosItens(i).VALOR_ICMS_ST               := vItens.VALOR_ICMS_ST;
    vDadosItens(i).PRECO_PAUTA                 := 0;
    vDadosItens(i).IVA                         := vItens.IVA;

    vDadosItens(i).PERCENTUAL_IPI              := vItens.PERCENTUAL_IPI;
    vDadosItens(i).VALOR_IPI                   := vItens.VALOR_IPI;

    /* VERIFICAR COM O DANIEL DA CONTABILIDADE QUAIS SERÃO OS CALCULOS
    vDadosItens(i).CST_PIS                     := vItens. ;
    vDadosItens(i).BASE_CALCULO_PIS            := vItens. ;
    vDadosItens(i).PERCENTUAL_PIS              := vItens. ;
    vDadosItens(i).VALOR_PIS                   := vItens. ;

    vDadosItens(i).CST_COFINS                  := vItens. ;
    vDadosItens(i).BASE_CALCULO_COFINS         := vItens. ;
    vDadosItens(i).PERCENTUAL_COFINS           := vItens. ;
    vDadosItens(i).VALOR_COFINS                := vItens. ; */
    
    vDadosItens(i).CST_PIS                     := '49';
    vDadosItens(i).BASE_CALCULO_PIS            := 0;
    vDadosItens(i).PERCENTUAL_PIS              := 0;
    vDadosItens(i).VALOR_PIS                   := 0;

    vDadosItens(i).CST_COFINS                  := '49';
    vDadosItens(i).BASE_CALCULO_COFINS         := 0;
    vDadosItens(i).PERCENTUAL_COFINS           := 0;
    vDadosItens(i).VALOR_COFINS                := 0;    
	
    if vDadosNota.ESTADO_ID_EMITENTE = vDadosNota.ESTADO_ID_DESTINATARIO then
      vTipoOperacao := 'I';
    else
      vTipoOperacao := 'E';
    end if;	

    vDadosItens(i).CFOP_ID :=
      BUSCAR_CFOP_ID_OPERACAO(
        vDadosNota.EMPRESA_ID,
        vDadosItens(i).CST,
        'DEN',
        vTipoOperacao
      );

		vDadosItens(i).NATUREZA_OPERACAO := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);
    
    /* Se a empresa for do simples nacional, ajustando os impostos que não se destacam neste regime tributario */
    if vRegimeTributario = 'SN' then
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := vItens.VALOR_TOTAL_OUTRAS_DESPESAS + vDadosItens(i).VALOR_ICMS_ST + vDadosItens(i).VALOR_IPI;
      
      vDadosEntrada.BASE_CALC_ICMS := vDadosEntrada.BASE_CALC_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;
      vDadosEntrada.VALOR_ICMS     := vDadosEntrada.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;
      
      vDadosEntrada.BASE_CALC_ICMS_ST := vDadosEntrada.BASE_CALC_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;
      vDadosEntrada.VALOR_ICMS_ST     := vDadosEntrada.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;
      
      vDadosEntrada.VALOR_IPI := vDadosEntrada.VALOR_IPI + vDadosItens(i).VALOR_IPI;
      
/*      vDadosItens(i).BASE_CALCULO_ICMS := 0; */
/*      vDadosItens(i).PERCENTUAL_ICMS   := 0; */
/*      vDadosItens(i).VALOR_ICMS        := 0; */
      
/*      vDadosItens(i).BASE_CALCULO_ICMS_ST := 0; */
/*      vDadosItens(i).PERCENTUAL_ICMS_ST   := 0; */
/*      vDadosItens(i).VALOR_ICMS_ST        := 0; */

/*      vDadosItens(i).VALOR_IPI := 0; */
    end if;

    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */
    vDadosNota.BASE_CALCULO_ICMS    := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;
    vDadosNota.VALOR_ICMS           := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;
    vDadosNota.BASE_CALCULO_ICMS_ST := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;
    vDadosNota.VALOR_ICMS_ST        := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;
    vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;
    vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;
    vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;
    vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;

    vDadosNota.VALOR_TOTAL :=
      vDadosNota.VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
      vDadosItens(i).VALOR_ICMS_ST +
      vDadosItens(i).VALOR_IPI -
      vDadosItens(i).VALOR_TOTAL_DESCONTO;

    vDadosNota.VALOR_TOTAL_PRODUTOS := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;
    vDadosNota.VALOR_DESCONTO := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;
    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;
    /* ------------------------------------------------------------------------------------------------------ */
    
		if
      vDadosItens(i).VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
      vDadosItens(i).VALOR_ICMS_ST +
      vDadosItens(i).VALOR_IPI -
      vDadosItens(i).VALOR_TOTAL_DESCONTO >
      vValorMaisSignificativo
    then
			vValorMaisSignificativo :=
        vDadosItens(i).VALOR_TOTAL +
        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
        vDadosItens(i).VALOR_ICMS_ST +
        vDadosItens(i).VALOR_IPI -
        vDadosItens(i).VALOR_TOTAL_DESCONTO;

			vCfopIdCapa 	  	:= vDadosItens(i).CFOP_ID;
			vNaturezaOperacao := vDadosItens(i).NATUREZA_OPERACAO;
		end if;
   
    i := i + 1;
  end loop;

  if vDadosItens.count < 1 then
    ERRO('Os produtos da nota fiscal não foram encontrados!');
  end if;

	if vCfopIdCapa is null then 
		vCfopIdCapa 			:= vDadosItens(0).CFOP_ID;
		vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;
	end if;

	vDadosNota.CFOP_ID           := vCfopIdCapa;
  vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;  

  vDadosNota.INFORMACOES_COMPLEMENTARES := vDadosNota.INFORMACOES_COMPLEMENTARES || 
    '. Devolucao codigo ' || iDEVOLUCAO_ID || ' referente a nota numero ' || vDadosEntrada.NUMERO_NOTA || 
    ' serie ' || vDadosEntrada.SERIE_NOTA || ' emitida no dia ' || vDadosEntrada.DATA_EMISSAO;
    
  if vDadosEntrada.BASE_CALC_ICMS > 0 then
    vDadosNota.INFORMACOES_COMPLEMENTARES := vDadosNota.INFORMACOES_COMPLEMENTARES || '; Base ICMS R$ ' || vDadosEntrada.BASE_CALC_ICMS || '; Valor ICMS R$ ' || vDadosEntrada.VALOR_ICMS;
  end if;
  
  if vDadosEntrada.BASE_CALC_ICMS_ST > 0 then
    vDadosNota.INFORMACOES_COMPLEMENTARES := vDadosNota.INFORMACOES_COMPLEMENTARES || '; Base ICMS ST R$ ' || vDadosEntrada.BASE_CALC_ICMS_ST || '; Valor ICMS ST R$ ' || vDadosEntrada.VALOR_ICMS_ST;
  end if;  
  
  if vDadosEntrada.VALOR_IPI > 0 then
    vDadosNota.INFORMACOES_COMPLEMENTARES := vDadosNota.INFORMACOES_COMPLEMENTARES || '; Valor IPI R$ ' || vDadosEntrada.VALOR_IPI;
  end if;    

  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;

	insert into NOTAS_FISCAIS(
    NOTA_FISCAL_ID,
    CADASTRO_ID,
    CFOP_ID,
    EMPRESA_ID,
    DEVOLUCAO_ENTRADA_ID,    
		MODELO_NOTA,
		SERIE_NOTA,
    NATUREZA_OPERACAO,
		STATUS,
    TIPO_MOVIMENTO,
    RAZAO_SOCIAL_EMITENTE,
    NOME_FANTASIA_EMITENTE,
    REGIME_TRIBUTARIO,
    CNPJ_EMITENTE,
    INSCRICAO_ESTADUAL_EMITENTE,
    LOGRADOURO_EMITENTE,
    COMPLEMENTO_EMITENTE,
    NOME_BAIRRO_EMITENTE,
    NOME_CIDADE_EMITENTE,
    NUMERO_EMITENTE,
    ESTADO_ID_EMITENTE,
    CEP_EMITENTE,
    TELEFONE_EMITENTE,
    CODIGO_IBGE_MUNICIPIO_EMIT,
    CODIGO_IBGE_ESTADO_EMITENT,
    NOME_FANTASIA_DESTINATARIO,
    RAZAO_SOCIAL_DESTINATARIO,
    TIPO_PESSOA_DESTINATARIO,
    CPF_CNPJ_DESTINATARIO,
    INSCRICAO_ESTADUAL_DESTINAT,
    NOME_CONSUMIDOR_FINAL,
    TELEFONE_CONSUMIDOR_FINAL,
    TIPO_NOTA,
    LOGRADOURO_DESTINATARIO,
    COMPLEMENTO_DESTINATARIO,
    NOME_BAIRRO_DESTINATARIO,
    NOME_CIDADE_DESTINATARIO,
    ESTADO_ID_DESTINATARIO,
    CEP_DESTINATARIO,
    NUMERO_DESTINATARIO,
    CODIGO_IBGE_MUNICIPIO_DEST,
    VALOR_TOTAL,
    VALOR_TOTAL_PRODUTOS,
    VALOR_DESCONTO,
    VALOR_OUTRAS_DESPESAS,
    BASE_CALCULO_ICMS,
    VALOR_ICMS,
    BASE_CALCULO_ICMS_ST,
    VALOR_ICMS_ST,
    BASE_CALCULO_PIS,
    VALOR_PIS,
    BASE_CALCULO_COFINS,
    VALOR_COFINS,
    VALOR_IPI,
    VALOR_FRETE,
    VALOR_SEGURO,
    VALOR_RECEBIDO_DINHEIRO,
    VALOR_RECEBIDO_CARTAO_CRED,
    VALOR_RECEBIDO_CARTAO_DEB,
    VALOR_RECEBIDO_CREDITO,
    VALOR_RECEBIDO_COBRANCA,
    VALOR_RECEBIDO_CHEQUE,
    VALOR_RECEBIDO_FINANCEIRA,
    PESO_LIQUIDO,
    PESO_BRUTO,
		INFORMACOES_COMPLEMENTARES
  )values(
		vNotaFiscalId,
    vDadosNota.CADASTRO_ID,
    vDadosNota.CFOP_ID,
    vDadosNota.EMPRESA_ID,    
    iDEVOLUCAO_ID,
		'55',
		vSerieNFe,
    vDadosNota.NATUREZA_OPERACAO,
		'N',
    'DEF',
    vDadosNota.RAZAO_SOCIAL_EMITENTE,
    vDadosNota.NOME_FANTASIA_EMITENTE,
    vRegimeTributario,
    vDadosNota.CNPJ_EMITENTE,
    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,
    vDadosNota.LOGRADOURO_EMITENTE,
    vDadosNota.COMPLEMENTO_EMITENTE,
    vDadosNota.NOME_BAIRRO_EMITENTE,
    vDadosNota.NOME_CIDADE_EMITENTE,
    vDadosNota.NUMERO_EMITENTE,
    vDadosNota.ESTADO_ID_EMITENTE,
    vDadosNota.CEP_EMITENTE,
    vDadosNota.TELEFONE_EMITENTE,
    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,
    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,
    vDadosNota.NOME_FANTASIA_DESTINATARIO,
    vDadosNota.RAZAO_SOCIAL_DESTINATARIO,
    vDadosNota.TIPO_PESSOA_DESTINATARIO,
    vDadosNota.CPF_CNPJ_DESTINATARIO,
    vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,
    vDadosNota.NOME_CONSUMIDOR_FINAL,
    vDadosNota.TELEFONE_CONSUMIDOR_FINAL,
    'N',
    vDadosNota.LOGRADOURO_DESTINATARIO,
    vDadosNota.COMPLEMENTO_DESTINATARIO,
    vDadosNota.NOME_BAIRRO_DESTINATARIO,
    vDadosNota.NOME_CIDADE_DESTINATARIO,
    vDadosNota.ESTADO_ID_DESTINATARIO,
    vDadosNota.CEP_DESTINATARIO,
    vDadosNota.NUMERO_DESTINATARIO,
    vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,
    vDadosNota.VALOR_TOTAL,
    vDadosNota.VALOR_TOTAL_PRODUTOS,
    vDadosNota.VALOR_DESCONTO,
    vDadosNota.VALOR_OUTRAS_DESPESAS,
    vDadosNota.BASE_CALCULO_ICMS,
    vDadosNota.VALOR_ICMS,
    vDadosNota.BASE_CALCULO_ICMS_ST,
    vDadosNota.VALOR_ICMS_ST,
    vDadosNota.BASE_CALCULO_PIS,
    vDadosNota.VALOR_PIS,
    vDadosNota.BASE_CALCULO_COFINS,
    vDadosNota.VALOR_COFINS,
    vDadosNota.VALOR_IPI,
    vDadosNota.VALOR_FRETE,
    vDadosNota.VALOR_SEGURO,
    vDadosNota.VALOR_RECEBIDO_DINHEIRO,
    vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,
    vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,
    vDadosNota.VALOR_RECEBIDO_CREDITO,
    vDadosNota.VALOR_RECEBIDO_COBRANCA,
    vDadosNota.VALOR_RECEBIDO_CHEQUE,
    vDadosNota.VALOR_RECEBIDO_FINANCEIRA,
    vDadosNota.PESO_LIQUIDO,
    vDadosNota.PESO_BRUTO,
		vDadosNota.INFORMACOES_COMPLEMENTARES
  );

  insert into NOTAS_FISCAIS_REFERENCIAS(
    NOTA_FISCAL_ID,
    ITEM_ID,
    TIPO_NOTA,
    TIPO_REFERENCIA,
    CHAVE_NFE_REFERENCIADA
  )values(
    vNotaFiscalId,
    1,
    'N',
    'M',
    RETORNA_NUMEROS(vDadosEntrada.CHAVE_NFE)
  );  

  for i in 0..vDadosItens.count - 1 loop
    insert into NOTAS_FISCAIS_ITENS(
      NOTA_FISCAL_ID,
      PRODUTO_ID,
      ITEM_ID,
      NOME_PRODUTO,
      UNIDADE,
      CFOP_ID,
      CST,
      CODIGO_NCM,
      VALOR_TOTAL,
      PRECO_UNITARIO,
      QUANTIDADE,
      VALOR_TOTAL_DESCONTO,
      VALOR_TOTAL_OUTRAS_DESPESAS,
      BASE_CALCULO_ICMS,
      PERCENTUAL_ICMS,
      VALOR_ICMS,
      BASE_CALCULO_ICMS_ST,
      VALOR_ICMS_ST,
      CST_PIS,
      BASE_CALCULO_PIS,
      PERCENTUAL_PIS,
      VALOR_PIS,
      CST_COFINS,
      BASE_CALCULO_COFINS,
      PERCENTUAL_COFINS,
      VALOR_COFINS,
      INDICE_REDUCAO_BASE_ICMS,
      IVA,
      PRECO_PAUTA,
      CODIGO_BARRAS,
      VALOR_IPI,
      PERCENTUAL_IPI,
      INDICE_REDUCAO_BASE_ICMS_ST,
      PERCENTUAL_ICMS_ST,
      CEST
    )values(
      vNotaFiscalId,
      vDadosItens(i).PRODUTO_ID,
      vDadosItens(i).ITEM_ID,
      vDadosItens(i).NOME_PRODUTO,
      vDadosItens(i).UNIDADE,
      vDadosItens(i).CFOP_ID,
      vDadosItens(i).CST,
      vDadosItens(i).CODIGO_NCM,
      vDadosItens(i).VALOR_TOTAL,
      vDadosItens(i).PRECO_UNITARIO,
      vDadosItens(i).QUANTIDADE,
      vDadosItens(i).VALOR_TOTAL_DESCONTO,
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,
      vDadosItens(i).BASE_CALCULO_ICMS,
      vDadosItens(i).PERCENTUAL_ICMS,
      vDadosItens(i).VALOR_ICMS,
      vDadosItens(i).BASE_CALCULO_ICMS_ST,
      vDadosItens(i).VALOR_ICMS_ST,
      vDadosItens(i).CST_PIS,
      vDadosItens(i).BASE_CALCULO_PIS,
      vDadosItens(i).PERCENTUAL_PIS,
      vDadosItens(i).VALOR_PIS,
      vDadosItens(i).CST_COFINS,
      vDadosItens(i).BASE_CALCULO_COFINS,
      vDadosItens(i).PERCENTUAL_COFINS,
      vDadosItens(i).VALOR_COFINS,
      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,
      vDadosItens(i).IVA,
      vDadosItens(i).PRECO_PAUTA,
      vDadosItens(i).CODIGO_BARRAS,
      vDadosItens(i).VALOR_IPI,
      vDadosItens(i).PERCENTUAL_IPI,
      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,
      vDadosItens(i).PERCENTUAL_ICMS_ST,
      vDadosItens(i).CEST
    );
  end loop;

end GERAR_NOTA_DEVOLUCAO_ENTR_NF;
/