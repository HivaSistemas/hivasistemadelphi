create table DEVOLUCOES_ENTR_NF_ITENS_LOTES(
  DEVOLUCAO_ID                number(12) not null,  
  ITEM_ID                     number(3) not null,
  LOTE                        varchar2(80) not null,
  QUANTIDADE                  number(20,4) not null, /* Esta quantidade é sem conversao, é oque de fato vem na nota fiscal */
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null   
);

/* Chave primaria */
alter table DEVOLUCOES_ENTR_NF_ITENS_LOTES
add constraint PK_DEV_ENTR_NF_ITENS_LOTES
primary key(DEVOLUCAO_ID, ITEM_ID, LOTE)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table DEVOLUCOES_ENTR_NF_ITENS_LOTES
add constraint FK_DEV_ENTR_NF_ITE_LOT_DEV_ID
foreign key(DEVOLUCAO_ID)
references DEVOLUCOES_ENTRADAS_NOTAS_FISC(DEVOLUCAO_ID);

alter table DEVOLUCOES_ENTR_NF_ITENS_LOTES
add constraint FK_DEV_EN_NF_ITE_LOT_DE_ITE_ID
foreign key(DEVOLUCAO_ID, ITEM_ID)
references DEVOLUCOES_ENTRADAS_NF_ITENS(DEVOLUCAO_ID, ITEM_ID);

/* Checagens */
alter table DEVOLUCOES_ENTR_NF_ITENS_LOTES
add constraint CK_DEV_EN_NF_ITE_LOT_QTDE
check(QUANTIDADE > 0);