create or replace procedure CONSOLIDAR_DEVOLUC_ENTRADA_NF(iDEVOLUCAO_ID in number)
is
  vEmpresaId   EMPRESAS.EMPRESA_ID%type;
  vStatus      DEVOLUCOES_ENTRADAS_NOTAS_FISC.STATUS%type;
  
  cursor cItens is
  select
    EIT.PRODUTO_ID,
    DIT.ITEM_ID,
    EIT.QUANTIDADE_EMBALAGEM,
    (DIT.QUANTIDADE * EIT.QUANTIDADE_EMBALAGEM) as QUANTIDADE,
    PRO.TIPO_CONTROLE_ESTOQUE,
    DIT.LOCAL_ID
  from
    DEVOLUCOES_ENTRADAS_NF_ITENS DIT

  inner join DEVOLUCOES_ENTRADAS_NOTAS_FISC DEN
  on DIT.DEVOLUCAO_ID = DEN.DEVOLUCAO_ID

  inner join ENTRADAS_NOTAS_FISCAIS_ITENS EIT
  on DEN.ENTRADA_ID = EIT.ENTRADA_ID
  and DIT.ITEM_ID = EIT.ITEM_ID

  inner join PRODUTOS PRO
  on EIT.PRODUTO_ID = PRO.PRODUTO_ID
  
  where DIT.DEVOLUCAO_ID = iDEVOLUCAO_ID;
  
  cursor cLotes(pITEM_ID in number) is
  select
    LOTE,
    QUANTIDADE
  from
    DEVOLUCOES_ENTR_NF_ITENS_LOTES
  where DEVOLUCAO_ID = iDEVOLUCAO_ID
  and ITEM_ID = pITEM_ID;

begin

  select
    EMPRESA_ID,
    STATUS
  into
    vEmpresaId,
    vStatus
  from
    DEVOLUCOES_ENTRADAS_NOTAS_FISC
  where DEVOLUCAO_ID = iDEVOLUCAO_ID;
  
  if vStatus = 'BAI' then
    ERRO('Esta devolução de entrada já está baixada!');
  end if;

  for xItens in cItens loop
  
    if xItens.TIPO_CONTROLE_ESTOQUE in('L', 'G', 'P') then
      for xLotes in cLotes(xItens.ITEM_ID) loop
        MOVIMENTAR_ESTOQUE(vEmpresaId, xItens.PRODUTO_ID, xItens.LOCAL_ID, xLotes.LOTE, xLotes.QUANTIDADE * xItens.QUANTIDADE_EMBALAGEM, 'DEN', null, null);
      end loop;
    else
      MOVIMENTAR_ESTOQUE(vEmpresaId, xItens.PRODUTO_ID, xItens.LOCAL_ID, '???', xItens.QUANTIDADE, 'DEN', null, null);
    end if;
    
  end loop;  
    
  update DEVOLUCOES_ENTRADAS_NOTAS_FISC set
    STATUS = 'BAI'
  where DEVOLUCAO_ID = iDEVOLUCAO_ID;
  
  GERAR_CREDITO_DEV_ENTRADA_NF(iDEVOLUCAO_ID);
  GERAR_NOTA_DEVOLUCAO_ENTR_NF(iDEVOLUCAO_ID);

end CONSOLIDAR_DEVOLUC_ENTRADA_NF;
/