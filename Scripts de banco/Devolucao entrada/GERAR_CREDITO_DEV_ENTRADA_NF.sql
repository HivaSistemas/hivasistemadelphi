create or replace procedure GERAR_CREDITO_DEV_ENTRADA_NF(iDEVOLUCAO_ID in number)
is
  vFornecedorId  CONTAS_RECEBER.CADASTRO_ID%type;
  vEmpresaId     CONTAS_RECEBER.EMPRESA_ID%type;
  vReceberId     CONTAS_RECEBER.RECEBER_ID%type;
  vValorGerar    CONTAS_RECEBER.VALOR_DOCUMENTO%type;
  vStatus        DEVOLUCOES_ENTRADAS_NOTAS_FISC.STATUS%type;

  vCobrancaId  PARAMETROS.TIPO_COBRANCA_GERACAO_CRED_ID%type;  
begin

  select
    DEV.EMPRESA_ID,
    ENT.FORNECEDOR_ID,
    DEV.VALOR_TOTAL,
    DEV.STATUS
  into
    vEmpresaId,
    vFornecedorId,
    vValorGerar,
    vStatus    
  from
    DEVOLUCOES_ENTRADAS_NOTAS_FISC DEV

  inner join ENTRADAS_NOTAS_FISCAIS ENT
  on DEV.ENTRADA_ID = ENT.ENTRADA_ID

  where DEV.DEVOLUCAO_ID = iDEVOLUCAO_ID;
  
  if vStatus <> 'BAI' then
    ERRO('Não é permitido gerar crédito de uma devolução de entrada que não esteja baixada!');
  end if;

  begin
    select
      TIPO_COBRANCA_GERACAO_CRED_ID
    into
      vCobrancaId
    from
      PARAMETROS;
  exception
    when others then
      ERRO('Tipo de cobrança para geração do crédito não parametrizado, faça a parametrização nos "Parâmetros"!');
  end;

  if vFornecedorId = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then
    ERRO('Não é permitido gerar crédito para consumidor final!');
  end if;

  select SEQ_RECEBER_ID.nextval
  into vReceberId
  from dual;

  insert into CONTAS_RECEBER(
    RECEBER_ID,
    CADASTRO_ID,
    EMPRESA_ID,
    DOCUMENTO,
    ORIGEM,
    DEVOLUCAO_ENTRADA_ID,
    COBRANCA_ID,
    PORTADOR_ID,
    PLANO_FINANCEIRO_ID,
    DATA_VENCIMENTO,
    DATA_VENCIMENTO_ORIGINAL,
    VALOR_DOCUMENTO,
    STATUS,
    PARCELA,
    NUMERO_PARCELAS	  
  )values(
    vReceberId,
    vFornecedorId,
    vEmpresaId,
    'DEN-' || NFORMAT(iDEVOLUCAO_ID, 0), -- Documento
    'DEN',
    iDEVOLUCAO_ID,
    vCobrancaId,
    '9999',
    '1.002.001',
    trunc(sysdate),
    trunc(sysdate),
    vValorGerar,
    'A',
    1,
    1    
  );

end GERAR_CREDITO_DEV_ENTRADA_NF;
/