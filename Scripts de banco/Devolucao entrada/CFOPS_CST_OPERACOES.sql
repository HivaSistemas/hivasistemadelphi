﻿create table CFOPS_CST_OPERACOES(
  EMPRESA_ID     number(3) not null,
  CFOP_ID        varchar2(7) not null,
  TIPO_MOVIMENTO char(3) not null,
  CST            char(2) not null,
  TIPO_OPERACAO  char(1) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table CFOPS_CST_OPERACOES
add constraint PK_CFOPS_CST_OPERACOES
primary key(EMPRESA_ID, TIPO_MOVIMENTO, CST, TIPO_OPERACAO)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CFOPS_CST_OPERACOES
add constraint FK_CFOPS_CST_OPERACOES_EMPR_ID
foreign key(EMPRESA_ID)
references PARAMETROS_EMPRESA(EMPRESA_ID);

alter table CFOPS_CST_OPERACOES
add constraint FK_CFOPS_CST_OPERACOES_CFOP_ID
foreign key(CFOP_ID)
references CFOP(CFOP_PESQUISA_ID);

/* Checagens */

/* TIPO_MOVIMENTO
  VIT - Venda interna
  DEV - Devolução
  TPE - Transferência de produtos entre empresas
  REN - Retorno de entrega
  RDF - Remessa para depósito fechado
  RRD - Retorno de remessa para depósito
  SFA - Simples faturamento
  DEN - Devolução de entrada de NF
  REF - Remessa de entrega futura
  PEN - Produção entrada
  PSA - Produção saída
*/
alter table CFOPS_CST_OPERACOES
add constraint CK_CFOPS_CST_OPER_TIPO_MOV
check(TIPO_MOVIMENTO in('VIT', 'DEV', 'TPE', 'REN', 'RDF', 'RRD', 'SFA', 'DEN', 'REF', 'PEN', 'PSA', 'AEE', 'AES'));

alter table CFOPS_CST_OPERACOES
add constraint CK_CFOPS_CST_OPER_CST
check(CST in('00','10','20','30','40','41','50','51','60','70','90'));

/* TIPO_OPERACAO
  I - Interna
  E - Interestadual
  X - Exterior
*/
alter table CFOPS_CST_OPERACOES
add constraint CK_CFOPS_CST_OPER_TIPO_OPERAC
check(TIPO_OPERACAO in('I','E','X'));