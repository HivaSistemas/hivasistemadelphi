create or replace trigger DEVOLUC_ENTR_NF_ITENS_IU_BR
before insert or update
on DEVOLUCOES_ENTRADAS_NF_ITENS
for each row
declare
  vEntradaId      DEVOLUCOES_ENTRADAS_NOTAS_FISC.ENTRADA_ID%type;
begin

  :new.USUARIO_SESSAO_ID   := SESSAO.USUARIO_SESSAO_ID;
  :new.DATA_HORA_ALTERACAO := sysdate;
  :new.ROTINA_ALTERACAO    := SESSAO.ROTINA;
  :new.ESTACAO_ALTERACAO   := SESSAO.NOME_COMPUTADOR_SESSAO;  
  
  if inserting then
    select
      ENTRADA_ID
    into
      vEntradaId
    from
      DEVOLUCOES_ENTRADAS_NOTAS_FISC
    where DEVOLUCAO_ID = :new.DEVOLUCAO_ID;
  
    update ENTRADAS_NOTAS_FISCAIS_ITENS set
      QUANTIDADE_DEVOLVIDOS = QUANTIDADE_DEVOLVIDOS + :new.QUANTIDADE
    where ENTRADA_ID = vEntradaId
    and ITEM_ID = :new.ITEM_ID;
  end if;
  
end DEVOLUC_ENTR_NF_ITENS_IU_BR;
/