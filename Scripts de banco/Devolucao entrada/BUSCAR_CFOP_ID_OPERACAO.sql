﻿create or replace function BUSCAR_CFOP_ID_OPERACAO(
  iEMPRESA_ID     in number,
  iCST            in string,
  iTIPO_MOVIMENTO in string,
  iTIPO_OPERACAO  in string
) 
return string
is
  vCfopId  CFOP.CFOP_PESQUISA_ID%type;
begin

  if iCST not in('00','10','20','30','40','41','50','51','60','70','90') then
    ERRO('CST passado via parâmetro incorreto!');
  end if;
  
/* iTIPO_OPERACAO
  I - Interna
  E - Interestadual
  X - Exterior
*/
  if not(nvl(iTIPO_OPERACAO, 'A') in('I', 'E', 'X')) then
    ERRO('TIPO_OPERACAO passado via parâmetro incorreto!');
  end if;

  /*
    iTIPO_MOVIMENTO
    VIT - Venda
    DEV - Devolução de venda
    TPE - Transferência de produtos entre empresas
    REN - Retorno de entrega
    RDF - Remessa para depósito fechado
    RRD - Retorno de remessa para depósito
    SFA - Simples faturamento
    REF - Remessa de entrega futura( Quando há a nota de simples faturamento )
    DEN - Devolução de entrada de NF
    PEN - Produção entrada
    PSA - Produção saída
    AEE - Ajuste Estoque Entrada
    AES - Ajuste Estoque Saída
  */

  if not(nvl(iTIPO_MOVIMENTO, 'XXX') in('VIT', 'DEV', 'TPE', 'REN', 'RDF', 'RRD', 'SFA', 'REF', 'DEN', 'PEN', 'PSA', 'AEE', 'AES')) then
    ERRO('iTIPO_MOVIMENTO passado via parâmetro incorreto!');
  end if;

  begin
    select
      CFOP_ID
    into
      vCfopId
    from
      CFOPS_CST_OPERACOES
    where CST = iCST
    and EMPRESA_ID = iEMPRESA_ID
    and TIPO_OPERACAO = iTIPO_OPERACAO
    and TIPO_MOVIMENTO = iTIPO_MOVIMENTO;
  exception
    when others then
      ERRO(
        'Falha ao buscar os parâmetros de CFOP! ' || sqlerrm || chr(13) ||
        'CST ' || iCST || chr(13) ||
        'TIPO OPER. ' || iTIPO_OPERACAO || chr(13) ||
        'EMPRESA ' || iEMPRESA_ID || chr(13) ||
        'TIPO MOV ' || iTIPO_MOVIMENTO
      );
  end;

  return vCfopId;
    
end BUSCAR_CFOP_ID_OPERACAO;
/