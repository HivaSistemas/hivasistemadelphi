﻿create or replace procedure INICIAR_SESSAO(
	iEMPRESA_ID      in number,
  iFUNCIONARIO_ID  in number,
  iSISTEMA         in string,
	iNOME_COMPUTADOR in string
)
is

  vDataValidadeModPreEntr date;

  procedure SETAR_CONFIGURACOES_ORACLE is
  begin
    begin
      /* Força o separador decimal */
      execute immediate
        'alter session set NLS_NUMERIC_CHARACTERS = '',.'' ';

      /* Força o formato da data */
      execute immediate
        'alter session set NLS_DATE_FORMAT = ''DD/MM/YYYY'' ';

    exception
      when others then
        null;
    end;
  end;

begin

  SESSAO.USUARIO_SESSAO_ID      := iFUNCIONARIO_ID;
  SESSAO.SISTEMA                := iSISTEMA;
	SESSAO.NOME_COMPUTADOR_SESSAO := iNOME_COMPUTADOR;
  
  SETAR_CONFIGURACOES_ORACLE;

  select
    DATA_VALIDADE_MOD_PRE_ENTR
  into
    vDataValidadeModPreEntr
  from
    PARAMETROS;

  if vDataValidadeModPreEntr is not null then
    if trunc(sysdate) > vDataValidadeModPreEntr then
      update PARAMETROS set
        UTIL_MODULO_PRE_ENTRADAS = 'N';
    end if;
  end if;

  begin
    select *
    into SESSAO.PARAMETROS_GERAIS
    from PARAMETROS;

    select *
    into SESSAO.PARAMETROS_EMPRESA_LOGADA
    from PARAMETROS_EMPRESA
    where EMPRESA_ID = iEMPRESA_ID;
  exception
    when others then
      if iSISTEMA <> 'IMPORTADOR' then
        raise;
      end if;
  end;

  delete from FILTROS_PADROES_TELAS_USUARIO
  where TELA = 'tformpesquisavendas'
  and CAMPO in ('eproduto', 'emarca')
  and USUARIO_ID = iFUNCIONARIO_ID;

end INICIAR_SESSAO;
/
