create or replace procedure BUSCAR_NUMERO_NOTA_EMISSAO(
  pNOTA_FISCAL_ID in string,
  oNUMERO_NOTA out number,
  oDATA_HORA_GERACAO out date
)
is
  vEmpresaId  NOTAS_FISCAIS.EMPRESA_ID%type;
  vTipoNota   NOTAS_FISCAIS.TIPO_NOTA%type;
  vNumeroNota NOTAS_FISCAIS.NUMERO_NOTA%type;
  vAtualizarRegistroNota boolean default false;
begin

  select
    EMPRESA_ID,
    TIPO_NOTA,
    NUMERO_NOTA
  into
    vEmpresaId,
    vTipoNota,
    vNumeroNota
  from
    NOTAS_FISCAIS
  where NOTA_FISCAL_ID = pNOTA_FISCAL_ID;
  
  if nvl(vNumeroNota, 0) = 0 then
    vAtualizarRegistroNota := true;  
    if vTipoNota = 'C' then
      select
        NUMERO_ULT_NFCE_EMITIDA + 1
      into
        vNumeroNota      
      from
        PARAMETROS_EMPRESA 
      where EMPRESA_ID = vEmpresaId
      for update;
    else
      select
        NUMERO_ULT_NFE_EMITIDA + 1
      into
        vNumeroNota
      from
        PARAMETROS_EMPRESA
      where EMPRESA_ID = vEmpresaId
      for update;
    end if;
  end if;

  if vAtualizarRegistroNota then
    update NOTAS_FISCAIS set
      NUMERO_NOTA = vNumeroNota
    where NOTA_FISCAL_ID = pNOTA_FISCAL_ID;

    if vTipoNota = 'C' then
      update PARAMETROS_EMPRESA set
        NUMERO_ULT_NFCE_EMITIDA = vNumeroNota
      where EMPRESA_ID = vEmpresaId;
    else
      update PARAMETROS_EMPRESA set
        NUMERO_ULT_NFE_EMITIDA = vNumeroNota
      where EMPRESA_ID = vEmpresaId;
    end if;
  end if;
  
  oNUMERO_NOTA := vNumeroNota;
  oDATA_HORA_GERACAO := sysdate;
end BUSCAR_NUMERO_NOTA_EMISSAO;
/