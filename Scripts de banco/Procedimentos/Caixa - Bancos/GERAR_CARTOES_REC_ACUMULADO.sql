﻿create or replace procedure GERAR_CARTOES_REC_ACUMULADO(
  iACUMULADO_ID     in number, -- 0
  iITEM_ID          in number, -- 1
  iNSU              in string, -- 2
  iCODIGO_AUTOR_TEF in string, -- 3
  iNUMERO_CARTAO    in string  -- 4
)
is 
  i                     number default 0;
  vValorCartao          number not null default 0;
  vDadosPagamentoPedido RecordsVendas.RecPedidosPagamentos;
  vCartoesGerar         RecordsFinanceiros.ArrayOfRecParcelasReceber;
  vReceberId            CONTAS_RECEBER.RECEBER_ID%type;
  
  cursor cCartoes is
  select
    PAG.COBRANCA_ID,
    PAG.VALOR,
    TIP.TIPO_CARTAO,
    TIP.PORTADOR_ID
  from
    ACUMULADOS_PAGAMENTOS PAG
  
  inner join TIPOS_COBRANCA TIP
  on PAG.COBRANCA_ID = TIP.COBRANCA_ID
  
  and TIP.FORMA_PAGAMENTO = 'CRT'
  where PAG.ACUMULADO_ID = iACUMULADO_ID
  and PAG.ITEM_ID = iITEM_ID;
  
  cursor c_dias_prazo(pCOBRANCA_ID positive) is
  select
    DIAS,
    rownum as PARCELA,
    count(*) over (partition by count(*)) as NUMERO_PARCELAS
  from
    TIPO_COBRANCA_DIAS_PRAZO      
  where COBRANCA_ID = pCOBRANCA_ID  
  group by
    DIAS,
    rownum
  order by DIAS;
  
begin
  delete from CONTAS_RECEBER
  where ACUMULADO_ID = iACUMULADO_ID
  and ITEM_ID_CRT_ORCAMENTO = iITEM_ID;

  update ACUMULADOS_PAGAMENTOS set
    NSU_TEF = iNSU
  where ACUMULADO_ID = iACUMULADO_ID
  and ITEM_ID = iITEM_ID;

  begin
    select
      0 as VALOR_DINHEIRO,
      ACU.VALOR_CARTAO_DEBITO,
      ACU.VALOR_CARTAO_CREDITO,
      0 as VALOR_CREDITO,
      0 as VALOR_COBRANCA,
      0 as VALOR_CHEQUE,
      0 as VALOR_FINANCEIRA,
      0 as VALOR_ACUMULATIVO,      
      ACU.EMPRESA_ID,
      ACU.CLIENTE_ID,
      ACU.STATUS,
      ACU.TURNO_ID
    into
      vDadosPagamentoPedido.VALOR_DINHEIRO,
      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,
      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,
      vDadosPagamentoPedido.VALOR_CREDITO,
      vDadosPagamentoPedido.VALOR_COBRANCA,
      vDadosPagamentoPedido.VALOR_CHEQUE,
      vDadosPagamentoPedido.VALOR_FINANCEIRA,
      vDadosPagamentoPedido.VALOR_ACUMULATIVO,      
      vDadosPagamentoPedido.EMPRESA_ID,
      vDadosPagamentoPedido.CLIENTE_ID,
      vDadosPagamentoPedido.STATUS,
      vDadosPagamentoPedido.TURNO_ID
    from
      ACUMULADOS ACU
    where ACU.ACUMULADO_ID = iACUMULADO_ID;
  exception
    when others then
     ERRO('Não foi encontrado os valores de pagamento para o acumulado ' || iACUMULADO_ID || '!' || sqlerrm);
  end;

   /* Inserindo os cartões no financeiro */
  if vDadosPagamentoPedido.VALOR_CARTAO_DEBITO + vDadosPagamentoPedido.VALOR_CARTAO_CREDITO > 0 then
    for vCartoes in cCartoes loop
      vValorCartao := vCartoes.VALOR;
      for v_dias_prazo in c_dias_prazo(vCartoes.COBRANCA_ID) loop
        vValorCartao := round(vValorCartao - vCartoes.VALOR / v_dias_prazo.NUMERO_PARCELAS, 2);
        
        vCartoesGerar(i).COBRANCA_ID         := vCartoes.COBRANCA_ID;
        vCartoesGerar(i).ITEM_ID             := iITEM_ID;
        vCartoesGerar(i).TIPO_CARTAO         := vCartoes.TIPO_CARTAO;
        vCartoesGerar(i).PARCELA             := v_dias_prazo.PARCELA;
        vCartoesGerar(i).NUMERO_PARCELAS     := v_dias_prazo.NUMERO_PARCELAS;
        vCartoesGerar(i).VALOR_PARCELA       := round(vCartoes.VALOR / v_dias_prazo.NUMERO_PARCELAS, 2);
        vCartoesGerar(i).DATA_VENCIMENTO     := trunc(sysdate) + v_dias_prazo.DIAS;
        vCartoesGerar(i).PORTADOR_ID         := vCartoes.PORTADOR_ID;
        vCartoesGerar(i).PLANO_FINANCEIRO_ID := '1.001.004';
        
        if v_dias_prazo.PARCELA = v_dias_prazo.NUMERO_PARCELAS and vValorCartao <> 0 then
          vCartoesGerar(i).VALOR_PARCELA := vCartoesGerar(i).VALOR_PARCELA + vValorCartao;
        end if;
        
        i := i + 1;
      end loop;      
    end loop;

    if vDadosPagamentoPedido.TURNO_ID is null then
      ERRO('Turno de recebimento do cartão não encontrado!');
    end if;

    if vCartoesGerar.first is null then
      ERRO('O Hiva não conseguiu gerar as parcelas do cartão corretamente, por favor verifique se o tipo de cobrança está parametrizado corretamente!');
    end if;

    for i in vCartoesGerar.first..vCartoesGerar.last loop
      vReceberId := SEQ_RECEBER_ID.nextval;

      insert into CONTAS_RECEBER(
        RECEBER_ID,
        CADASTRO_ID,
        EMPRESA_ID,
        ORIGEM,
        ACUMULADO_ID,
        COBRANCA_ID,
        PORTADOR_ID,
        PLANO_FINANCEIRO_ID,
        TURNO_ID,
        VENDEDOR_ID,
        DOCUMENTO,
        DATA_VENCIMENTO,
        DATA_VENCIMENTO_ORIGINAL,
        NSU,
        CODIGO_AUTORIZACAO_TEF,
        ITEM_ID_CRT_ORCAMENTO,
        VALOR_DOCUMENTO,
        STATUS,
        PARCELA,
        NUMERO_PARCELAS,
        NUMERO_CARTAO_TRUNCADO
      )values(
        vReceberId,
        vDadosPagamentoPedido.CLIENTE_ID,
        vDadosPagamentoPedido.EMPRESA_ID,
        'ACU',
        iACUMULADO_ID,
        vCartoesGerar(i).COBRANCA_ID,
        vCartoesGerar(i).PORTADOR_ID,
        vCartoesGerar(i).PLANO_FINANCEIRO_ID,
        vDadosPagamentoPedido.TURNO_ID,
        vDadosPagamentoPedido.VENDEDOR_ID,
        'ACU-' || iACUMULADO_ID || '-' || vCartoesGerar(i).PARCELA || '/' || vCartoesGerar(i).NUMERO_PARCELAS || '/CR' || vCartoesGerar(i).TIPO_CARTAO,
        vCartoesGerar(i).DATA_VENCIMENTO,
        vCartoesGerar(i).DATA_VENCIMENTO,
        iNSU,
        iCODIGO_AUTOR_TEF,
        iITEM_ID,
        vCartoesGerar(i).VALOR_PARCELA,
        'A',
        vCartoesGerar(i).PARCELA,
        vCartoesGerar(i).NUMERO_PARCELAS,
        iNUMERO_CARTAO
      );
    end loop;     
  end if;   
  
end GERAR_CARTOES_REC_ACUMULADO;
/