﻿create or replace procedure RECEBER_ACUMULADO(
  iACUMULADO_ID        in number,
  iTURNO_ID            in number,
  iVALOR_TROCO         in number,
  iGERAR_CREDITO_TROCO in string,
  iTIPO_NOTA_GERAR     in string
)
is
  i                       naturaln default 0;  
  vReceberId              CONTAS_RECEBER.RECEBER_ID%type;
  vBaixaId                CONTAS_RECEBER_BAIXAS.BAIXA_ID%type;
  vEmitirNotaFechamentoPedido PARAMETROS.EMITIR_NOTA_ACUMULADO_FECH_PED%type;
  vDadosPagamentoPedido   RecordsVendas.RecPedidosPagamentos;

  cursor cCheques is
  select
    COBRANCA_ID,
    ITEM_ID,
    DATA_VENCIMENTO,
    BANCO,
    AGENCIA,
    CONTA_CORRENTE,
    NUMERO_CHEQUE,
    VALOR_CHEQUE,
    NOME_EMITENTE,
    CPF_CNPJ_EMITENTE,
    TELEFONE_EMITENTE,
    PARCELA,
    NUMERO_PARCELAS
  from
    ACUMULADOS_PAGAMENTOS_CHEQUES
  where ACUMULADO_ID = iACUMULADO_ID
  order by 
		COBRANCA_ID, 
		PARCELA,
		DATA_VENCIMENTO, 
		ITEM_ID;
  
  cursor cCobrancas(pTIPO in string) is
  select
    PAG.COBRANCA_ID,
    PAG.ITEM_ID,
    PAG.VALOR,
    PAG.DATA_VENCIMENTO,
    PAG.PARCELA,
    PAG.NUMERO_PARCELAS,
    TIP.BOLETO_BANCARIO,
    PAG.NSU_TEF,
    PAG.CODIGO_AUTORIZACAO,
    PAG.NUMERO_CARTAO,
    TIP.PORTADOR_ID
  from
    ACUMULADOS_PAGAMENTOS PAG
  
  join TIPOS_COBRANCA TIP
  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    
  and TIP.FORMA_PAGAMENTO = pTIPO

  where PAG.ACUMULADO_ID = iACUMULADO_ID  
  order by 
		COBRANCA_ID,
		PARCELA,
		DATA_VENCIMENTO, 
		ITEM_ID;

begin

  begin
    select
      VALOR_DINHEIRO,
      VALOR_CARTAO_DEBITO,
      VALOR_CARTAO_CREDITO,
      VALOR_CREDITO,
      VALOR_COBRANCA,
      VALOR_CHEQUE,
      VALOR_FINANCEIRA,      
      STATUS,      
      EMPRESA_ID,
      CLIENTE_ID
    into
      vDadosPagamentoPedido.VALOR_DINHEIRO,
      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,
      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,
      vDadosPagamentoPedido.VALOR_CREDITO,
      vDadosPagamentoPedido.VALOR_COBRANCA,
      vDadosPagamentoPedido.VALOR_CHEQUE,
      vDadosPagamentoPedido.VALOR_FINANCEIRA,      
      vDadosPagamentoPedido.STATUS,      
      vDadosPagamentoPedido.EMPRESA_ID,
      vDadosPagamentoPedido.CLIENTE_ID
    from
      ACUMULADOS
    where ACUMULADO_ID = iACUMULADO_ID;
  exception
    when others then
      ERRO('Não foi encontrado os valores de pagamento para o acumulado ' || iACUMULADO_ID || '!' || chr(13) || chr(10) || sqlerrm);
  end;

  begin
    select
      EMITIR_NOTA_ACUMULADO_FECH_PED
    into
      vEmitirNotaFechamentoPedido
    from
      PARAMETROS;
  exception
    when others then
      vEmitirNotaFechamentoPedido := 'N';
  end;

  if vDadosPagamentoPedido.STATUS = 'RE' then
    ERRO('Este acumulado já foi recebido!');
  end if;

  if vDadosPagamentoPedido.STATUS not in('AR') then
    ERRO('Este acumulado não está aguardando recebimento, verifique!');
  end if;

  /* Inserindo cheques no financeiro */
  if vDadosPagamentoPedido.VALOR_CHEQUE > 0 then
    for vCheques in cCheques loop
      select SEQ_RECEBER_ID.nextval
      into vReceberId
      from dual;

      insert into CONTAS_RECEBER(
        RECEBER_ID,
        CADASTRO_ID,
        EMPRESA_ID,
        ACUMULADO_ID,
        COBRANCA_ID,
        PORTADOR_ID,
        PLANO_FINANCEIRO_ID,
        TURNO_ID,        
        DOCUMENTO,
        BANCO,
        AGENCIA,
        CONTA_CORRENTE,
        NUMERO_CHEQUE,
        NOME_EMITENTE,
        TELEFONE_EMITENTE,
        CPF_CNPJ_EMITENTE,
        DATA_EMISSAO,
        DATA_VENCIMENTO,
        DATA_VENCIMENTO_ORIGINAL,
        VALOR_DOCUMENTO,
        STATUS,
        PARCELA,
        NUMERO_PARCELAS,
        ORIGEM
      )values(
        vReceberId,
        vDadosPagamentoPedido.CLIENTE_ID,
        vDadosPagamentoPedido.EMPRESA_ID,
        iACUMULADO_ID,
        vCheques.COBRANCA_ID,
        '9998',
        '1.001.002',
        iTURNO_ID,        
        'ACU-' || iACUMULADO_ID || ' - ' || vCheques.ITEM_ID || '/CHQ',
        vCheques.BANCO,
        vCheques.AGENCIA,
        vCheques.CONTA_CORRENTE,
        vCheques.NUMERO_CHEQUE,
        vCheques.NOME_EMITENTE,
        vCheques.TELEFONE_EMITENTE,
        vCheques.CPF_CNPJ_EMITENTE,
        trunc(sysdate),
        vCheques.DATA_VENCIMENTO,
        vCheques.DATA_VENCIMENTO,
        vCheques.VALOR_CHEQUE,
        'A',
        vCheques.PARCELA,
        vCheques.NUMERO_PARCELAS,
        'ACU'
      );
    end loop;
  end if;

  if vDadosPagamentoPedido.VALOR_COBRANCA > 0 then
    for vCobrancas in cCobrancas('COB') loop
      select SEQ_RECEBER_ID.nextval
      into vReceberId
      from dual;

      insert into CONTAS_RECEBER(
        RECEBER_ID,
        CADASTRO_ID,
        EMPRESA_ID,
        ACUMULADO_ID,
        COBRANCA_ID,
        PORTADOR_ID,
        PLANO_FINANCEIRO_ID,
        TURNO_ID,        
        DOCUMENTO,
        DATA_EMISSAO,
        DATA_VENCIMENTO,
        DATA_VENCIMENTO_ORIGINAL,
        VALOR_DOCUMENTO,
        STATUS,
        PARCELA,
        NUMERO_PARCELAS,
        ORIGEM
      )values(
        vReceberId,
        vDadosPagamentoPedido.CLIENTE_ID,
        vDadosPagamentoPedido.EMPRESA_ID,
        iACUMULADO_ID,
        vCobrancas.COBRANCA_ID,
        vCobrancas.PORTADOR_ID,
        case when vCobrancas.BOLETO_BANCARIO = 'N' then '1.001.005' else '1.001.006' end,
        iTURNO_ID,        
        'ACU-' || iACUMULADO_ID  || ' - ' || vCobrancas.ITEM_ID || case when vCobrancas.BOLETO_BANCARIO = 'S' then '/BOL' else '/COB' end,
        trunc(sysdate),
        vCobrancas.DATA_VENCIMENTO,
        vCobrancas.DATA_VENCIMENTO,
        vCobrancas.VALOR,
        'A',
        vCobrancas.PARCELA,
        vCobrancas.NUMERO_PARCELAS,
        'ACU'
      );
    end loop;
  end if;
  
  if iGERAR_CREDITO_TROCO = 'S' and iVALOR_TROCO > 0 then
    GERAR_CREDITO_TROCO(iACUMULADO_ID, 'ACU', vDadosPagamentoPedido.CLIENTE_ID, iVALOR_TROCO);
  end if;

  update ACUMULADOS set
    VALOR_TROCO = case when iGERAR_CREDITO_TROCO = 'N' then iVALOR_TROCO else 0 end,
    TURNO_ID = iTURNO_ID,
    STATUS = 'RE',
    TIPO_NOTA_GERAR = iTIPO_NOTA_GERAR
  where ACUMULADO_ID = iACUMULADO_ID;

  /* Não alterar este processo!!! Ele deve ser depois do recebimento */
  if vDadosPagamentoPedido.VALOR_CARTAO_DEBITO + vDadosPagamentoPedido.VALOR_CARTAO_CREDITO > 0 then
    for vCobrancas in cCobrancas('CRT') loop
      GERAR_CARTOES_REC_ACUMULADO(iACUMULADO_ID, vCobrancas.ITEM_ID, vCobrancas.NSU_TEF, vCobrancas.CODIGO_AUTORIZACAO, vCobrancas.NUMERO_CARTAO);
    end loop;
  end if;

  ATUALIZAR_RETENCOES_CONTAS_REC(iACUMULADO_ID, 'ACU');

  if vEmitirNotaFechamentoPedido = 'N' then
    GERAR_NOTA_ACUMULADO(iACUMULADO_ID);
  end if;

end RECEBER_ACUMULADO;
/