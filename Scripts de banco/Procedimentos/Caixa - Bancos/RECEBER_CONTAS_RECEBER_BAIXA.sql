﻿create or replace procedure RECEBER_CONTAS_RECEBER_BAIXA(
iBAIXA_ID in number,  
iTURNO_ID in number) is
begin

  update CONTAS_RECEBER_BAIXAS set
    DATA_PAGAMENTO = trunc(sysdate),
    RECEBIDO = 'S',
    TURNO_ID = iTURNO_ID
  where BAIXA_ID = iBAIXA_ID;

  CONSOLIDAR_BAIXA_CONTAS_REC(iBAIXA_ID, NULL);

end RECEBER_CONTAS_RECEBER_BAIXA;

/
