create or replace procedure RECEBER_PEDIDO(
  iORCAMENTO_ID        in positiven,
  iTURNO_ID            in positiven,
  iVALOR_TROCO         in number,
  iGERAR_CREDITO_TROCO in string,
  iTIPO_NOTA_GERAR     in string
)
is
  i                       naturaln default 0;
  vRetiradaId             RETIRADAS.RETIRADA_ID%type;
  vReceberId              CONTAS_RECEBER.RECEBER_ID%type;
  vDadosPagamentoPedido   RecordsVendas.RecPedidosPagamentos;

  vBaixaCreditoPagarId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;
  vValorTotal           ORCAMENTOS.VALOR_TOTAL%type;
  vValorBaixarCredito   number default 0;
  vValorRestante        number default 0;
  vReceberNaEntrega     ORCAMENTOS.RECEBER_NA_ENTREGA%type;

  cursor cRetiradas is
  select
    RETIRADA_ID
  from
    RETIRADAS
  where TIPO_MOVIMENTO = 'A'
  and ORCAMENTO_ID = iORCAMENTO_ID;

  cursor cCheques is
  select
    COBRANCA_ID,
    ITEM_ID,
    DATA_VENCIMENTO,
    BANCO,
    AGENCIA,
    CONTA_CORRENTE,
    NUMERO_CHEQUE,
    VALOR_CHEQUE,
    NOME_EMITENTE,
    TELEFONE_EMITENTE,
    PARCELA,
    NUMERO_PARCELAS
  from
    ORCAMENTOS_PAGAMENTOS_CHEQUES
  where ORCAMENTO_ID = iORCAMENTO_ID
  order by 
    COBRANCA_ID, 
    PARCELA,
    DATA_VENCIMENTO, 
    ITEM_ID;

  cursor cCobrancas(pTIPO in string) is
  select
    PAG.COBRANCA_ID,
    PAG.ITEM_ID,
    PAG.VALOR,
    PAG.DATA_VENCIMENTO,
    PAG.PARCELA,
    PAG.NUMERO_PARCELAS,
    TIP.BOLETO_BANCARIO,
    PAG.NSU_TEF,
    TIP.PORTADOR_ID,
    PAG.NUMERO_CARTAO,
    PAG.CODIGO_AUTORIZACAO
  from
    ORCAMENTOS_PAGAMENTOS PAG

  join TIPOS_COBRANCA TIP
  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    
  and TIP.FORMA_PAGAMENTO = pTIPO

  where PAG.ORCAMENTO_ID = iORCAMENTO_ID  
  order by 
    COBRANCA_ID,
    PARCELA,
    DATA_VENCIMENTO, 
    ITEM_ID;

  cursor cCreditos is
  select
    ORC.PAGAR_ID,
    PAG.VALOR_DOCUMENTO
  from
    ORCAMENTOS_CREDITOS ORC

  inner join CONTAS_PAGAR PAG
  on ORC.PAGAR_ID = PAG.PAGAR_ID

  where ORC.ORCAMENTO_ID = iORCAMENTO_ID
  and ORC.MOMENTO_USO = 'R';

begin

  begin
    select
      VALOR_DINHEIRO,
      VALOR_CARTAO_DEBITO,
      VALOR_CARTAO_CREDITO,
      VALOR_CREDITO,
      VALOR_COBRANCA,
      VALOR_CHEQUE,
      VALOR_FINANCEIRA,
      VALOR_ACUMULATIVO,
      STATUS,
      VENDEDOR_ID,
      EMPRESA_ID,
      CLIENTE_ID,
      RECEBER_NA_ENTREGA,
      VALOR_TOTAL,
      OBSERVACOES_TITULOS
    into
      vDadosPagamentoPedido.VALOR_DINHEIRO,
      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,
      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,
      vDadosPagamentoPedido.VALOR_CREDITO,
      vDadosPagamentoPedido.VALOR_COBRANCA,
      vDadosPagamentoPedido.VALOR_CHEQUE,
      vDadosPagamentoPedido.VALOR_FINANCEIRA,
      vDadosPagamentoPedido.VALOR_ACUMULATIVO,
      vDadosPagamentoPedido.STATUS,
      vDadosPagamentoPedido.VENDEDOR_ID,
      vDadosPagamentoPedido.EMPRESA_ID,
      vDadosPagamentoPedido.CLIENTE_ID,
      vReceberNaEntrega,
      vValorTotal,
      vDadosPagamentoPedido.OBSERVACOES_TITULOS
    from
      ORCAMENTOS
    where ORCAMENTO_ID = iORCAMENTO_ID;
  exception
    when others then
      ERRO('N?o foi encontrado os valores de pagamento para o orcamento ' || iORCAMENTO_ID || '!' || chr(13) || chr(10) || sqlerrm);
  end;

  if vDadosPagamentoPedido.STATUS = 'RE' then
    ERRO('Este orcamento ja foi recebido!');
  end if;

  if vDadosPagamentoPedido.STATUS not in('VR', 'VE') then
    ERRO('Este pedido n?o esta aguardando recebimento, verifique!');
  end if;

  /* Apagando as previs?es dos recebimentos na entrega */
  if vDadosPagamentoPedido.STATUS = 'VE' then
    delete from CONTAS_RECEBER
    where ORCAMENTO_ID = iORCAMENTO_ID
    and COBRANCA_ID = SESSAO.PARAMETROS_GERAIS.TIPO_COB_RECEB_ENTREGA_ID;
  end if;

  /* Inserindo cheques no financeiro */
  if vDadosPagamentoPedido.VALOR_CHEQUE > 0 then
    for vCheques in cCheques loop
      select SEQ_RECEBER_ID.nextval
      into vReceberId
      from dual;

      insert into CONTAS_RECEBER(
        RECEBER_ID,
        CADASTRO_ID,
        EMPRESA_ID,
        ORCAMENTO_ID,
        COBRANCA_ID,
        PORTADOR_ID,
        PLANO_FINANCEIRO_ID,
        TURNO_ID,
        VENDEDOR_ID,
        DOCUMENTO,
        BANCO,
        AGENCIA,
        CONTA_CORRENTE,
        NUMERO_CHEQUE,
        NOME_EMITENTE,
        TELEFONE_EMITENTE,
        DATA_EMISSAO,
        DATA_VENCIMENTO,
        DATA_VENCIMENTO_ORIGINAL,
        VALOR_DOCUMENTO,
        STATUS,
        PARCELA,
        NUMERO_PARCELAS,
        ORIGEM,
        OBSERVACOES
      )values(
        vReceberId,
        vDadosPagamentoPedido.CLIENTE_ID,
        vDadosPagamentoPedido.EMPRESA_ID,
        iORCAMENTO_ID,
        vCheques.COBRANCA_ID,
        '9998',
        '1.001.002',
        iTURNO_ID,
        vDadosPagamentoPedido.VENDEDOR_ID,
        'PED-' || iORCAMENTO_ID || vCheques.ITEM_ID || '/CHQ',
        vCheques.BANCO,
        vCheques.AGENCIA,
        vCheques.CONTA_CORRENTE,
        vCheques.NUMERO_CHEQUE,
        vCheques.NOME_EMITENTE,
        vCheques.TELEFONE_EMITENTE,
        trunc(sysdate),
        vCheques.DATA_VENCIMENTO,
        vCheques.DATA_VENCIMENTO,
        vCheques.VALOR_CHEQUE,
        'A',
        vCheques.PARCELA,
        vCheques.NUMERO_PARCELAS,
        'ORC',
        vDadosPagamentoPedido.OBSERVACOES_TITULOS
      );
    end loop;
  end if;

  if vDadosPagamentoPedido.VALOR_COBRANCA > 0 then
    for vCobrancas in cCobrancas('COB') loop
      select SEQ_RECEBER_ID.nextval
      into vReceberId
      from dual;

      insert into CONTAS_RECEBER(
        RECEBER_ID,
        CADASTRO_ID,
        EMPRESA_ID,
        ORCAMENTO_ID,
        COBRANCA_ID,
        PORTADOR_ID,
        PLANO_FINANCEIRO_ID,
        TURNO_ID,
        VENDEDOR_ID,
        DOCUMENTO,
        DATA_EMISSAO,
        DATA_VENCIMENTO,
        DATA_VENCIMENTO_ORIGINAL,
        VALOR_DOCUMENTO,
        STATUS,
        PARCELA,
        NUMERO_PARCELAS,
        ORIGEM,
        OBSERVACOES
      )values(
        vReceberId,
        vDadosPagamentoPedido.CLIENTE_ID,
        vDadosPagamentoPedido.EMPRESA_ID,
        iORCAMENTO_ID,
        vCobrancas.COBRANCA_ID,
        vCobrancas.PORTADOR_ID,
        case when vCobrancas.BOLETO_BANCARIO = 'N' then '1.001.005' else '1.001.006' end,
        iTURNO_ID,
        vDadosPagamentoPedido.VENDEDOR_ID,
        'PED-' || iORCAMENTO_ID || vCobrancas.ITEM_ID || case when vCobrancas.BOLETO_BANCARIO = 'S' then '/BOL' else '/COB' end,
        trunc(sysdate),
        vCobrancas.DATA_VENCIMENTO,
        vCobrancas.DATA_VENCIMENTO,
        vCobrancas.VALOR,
        'A',
        vCobrancas.PARCELA,
        vCobrancas.NUMERO_PARCELAS,
        'ORC',
        vDadosPagamentoPedido.OBSERVACOES_TITULOS
      );
    end loop;
  end if;

  if vDadosPagamentoPedido.VALOR_ACUMULATIVO > 0 then
    AJUSTAR_CONTAS_REC_ACUMULATIVO(iORCAMENTO_ID);
  end if;

  if iGERAR_CREDITO_TROCO = 'S' and iVALOR_TROCO > 0 then
    GERAR_CREDITO_TROCO(iORCAMENTO_ID, 'ORC', vDadosPagamentoPedido.CLIENTE_ID, iVALOR_TROCO);
  end if;

  if vReceberNaEntrega = 'S' and vDadosPagamentoPedido.VALOR_CREDITO > 0 then
    vValorRestante :=
      vValorTotal - (
        vDadosPagamentoPedido.VALOR_DINHEIRO +
        vDadosPagamentoPedido.VALOR_CHEQUE +
        vDadosPagamentoPedido.VALOR_CARTAO_DEBITO +
        vDadosPagamentoPedido.VALOR_CARTAO_CREDITO +
        vDadosPagamentoPedido.VALOR_COBRANCA +
        vDadosPagamentoPedido.VALOR_FINANCEIRA
      );

    for xCreditos in cCreditos loop
      if xCreditos.VALOR_DOCUMENTO >= vValorRestante then
        vValorBaixarCredito := vValorRestante;
        vValorRestante := 0;
      else
        vValorBaixarCredito := xCreditos.VALOR_DOCUMENTO;
        vValorRestante := vValorRestante - vValorBaixarCredito;
      end if;

      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iORCAMENTO_ID, 'ORC', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);

      if vValorRestante = 0 then
        exit;
      end if;
    end loop;
  end if;

  update ORCAMENTOS set
    VALOR_TROCO = case when iGERAR_CREDITO_TROCO = 'N' then iVALOR_TROCO else 0 end,
    TURNO_ID = iTURNO_ID,
    STATUS = 'RE'
  where ORCAMENTO_ID = iORCAMENTO_ID;

  /* N?o alterar este processo!!! Ele deve ser depois do recebimento */
  if vDadosPagamentoPedido.VALOR_CARTAO_DEBITO + vDadosPagamentoPedido.VALOR_CARTAO_CREDITO > 0 then
    for vCobrancas in cCobrancas('CRT') loop
      GERAR_CARTOES_RECEBER(iORCAMENTO_ID, vCobrancas.ITEM_ID, vCobrancas.NSU_TEF, vCobrancas.CODIGO_AUTORIZACAO, vCobrancas.NUMERO_CARTAO);
    end loop;
  end if;

  select
    max(RETIRADA_ID)
  into
    vRetiradaId
  from
    RETIRADAS
  where TIPO_MOVIMENTO = 'A'
  and ORCAMENTO_ID = iORCAMENTO_ID;

  if vRetiradaId is not null then
    update RETIRADAS set
      TIPO_NOTA_GERAR = nvl(iTIPO_NOTA_GERAR, 'NI')
    where ORCAMENTO_ID = iORCAMENTO_ID
  and TIPO_MOVIMENTO = 'A';

    /* Caso o cliente trabalhe com confirmac?o de saidas de mercadorias */
    if SESSAO.PARAMETROS_EMPRESA_LOGADA.CONFIRMAR_SAIDA_PRODUTOS = 'N' then
      for vRetiradas in cRetiradas loop
        CONFIRMAR_RETIRADA(vRetiradas.RETIRADA_ID, 'AUTOMATICO');
      end loop;
    end if;
  end if;

  ATUALIZAR_RETENCOES_CONTAS_REC(iORCAMENTO_ID, 'ORC');

end RECEBER_PEDIDO;
