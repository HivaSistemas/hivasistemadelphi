create or replace procedure LANCAR_DIFERENCA_FECH_CAIXA(iTURNO_ID in number, iVALOR_DIFERENCA in number)
is
  vReceberId            CONTAS_RECEBER.RECEBER_ID%type;
  vCadastroId           FUNCIONARIOS.CADASTRO_ID%type;
  vEmpresaId            TURNOS_CAIXA.EMPRESA_ID%type;
  vFechamentoId         MOVIMENTOS_TURNOS.MOVIMENTO_TURNO_ID%type;
  vValorMinLancamento   PARAMETROS_EMPRESA.VALOR_MIN_DIF_TURNO_CONTAS_REC%type;

  vTipoCobDiferencaId   PARAMETROS.TIPO_COB_FECHAMENTO_TURNO_ID%type;
begin

  /* Se a difern�a for posivita n�o tem o porque lan�ar nada */
  if iVALOR_DIFERENCA > 0 then
    return;
  end if;

  select
    FUN.CADASTRO_ID,
    TUR.EMPRESA_ID
  into
    vCadastroId,
    vEmpresaId
  from
    TURNOS_CAIXA TUR
  inner join FUNCIONARIOS FUN
  on TUR.FUNCIONARIO_ID = FUN.FUNCIONARIO_ID
  where TUR.TURNO_ID = iTURNO_ID;

  select
    VALOR_MIN_DIF_TURNO_CONTAS_REC
  into
    vValorMinLancamento
  from
    PARAMETROS_EMPRESA
  where EMPRESA_ID = vEmpresaId;

  if (vValorMinLancamento = 0) or (abs(iVALOR_DIFERENCA) < vValorMinLancamento) then
    return;
  end if;

  select
    MOVIMENTO_TURNO_ID
  into
    vFechamentoId
  from
    MOVIMENTOS_TURNOS
  where TURNO_ID = iTURNO_ID
  and TIPO_MOVIMENTO = 'FEC';

  select
    TIPO_COB_FECHAMENTO_TURNO_ID
  into
    vTipoCobDiferencaId
  from
    PARAMETROS;

  vReceberId := SEQ_RECEBER_ID.nextval;

  insert into CONTAS_RECEBER(
    RECEBER_ID,
    CADASTRO_ID,
    EMPRESA_ID,
    COBRANCA_ID,
    PLANO_FINANCEIRO_ID,
    DOCUMENTO,
    PORTADOR_ID,
    DATA_VENCIMENTO,
    DATA_VENCIMENTO_ORIGINAL,
    VALOR_DOCUMENTO,
    STATUS,
    PARCELA,
    NUMERO_PARCELAS,
    ORIGEM
  )values(
    vReceberId,
    vCadastroId,
    vEmpresaId,
    vTipoCobDiferencaId,      -- Duplicata
    '1.001.005', -- Plano financeiro
    'DIF-TUR' || iTURNO_ID,
    '9999', -- Portador
    last_day(sysdate),
    last_day(sysdate),
    abs(iVALOR_DIFERENCA),
    'A',
    1,
    1,
    'DTU'
  );

  GRAVAR_TITULOS_MOV_TURNOS_ITE(null, null, vFechamentoId, 'COB', vReceberId);

end LANCAR_DIFERENCA_FECH_CAIXA;
/
