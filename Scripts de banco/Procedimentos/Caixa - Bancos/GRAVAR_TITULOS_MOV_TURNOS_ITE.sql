﻿create or replace procedure GRAVAR_TITULOS_MOV_TURNOS_ITE(
  iORCAMENTO_ID          in number,
  iITEM_ID_CRT_ORCAMENTO in number,
  iMOVIMENTO_ID          in number,
  iTIPO                  in string,
  iRECEBER_ID            in number
)
is  
begin

  if not iTIPO in('CHQ', 'CRT', 'COB') then
    ERRO('Os tipos de movimento só podem ser "CHQ", "CRT" e "COB"!');
  end if;
  
  if iTIPO = 'CRT' then
    insert into MOVIMENTOS_TURNOS_ITENS(
      MOVIMENTO_TURNO_ID,
      ID,
	    TIPO
    )
    select
      iMOVIMENTO_ID,
      RECEBER_ID,
      'R'
    from
      CONTAS_RECEBER
    where ORCAMENTO_ID = iORCAMENTO_ID
    and ITEM_ID_CRT_ORCAMENTO = iITEM_ID_CRT_ORCAMENTO;
  else
    insert into MOVIMENTOS_TURNOS_ITENS(
      MOVIMENTO_TURNO_ID,
      ID,
	    TIPO
    )values(
      iMOVIMENTO_ID,
      iRECEBER_ID,
	    'R'
    );   
  end if;
  
end GRAVAR_TITULOS_MOV_TURNOS_ITE;
/