﻿create or replace procedure FINALIZAR_ANO_MES
is
  vDataPrimeiraVenda  date;
  vUltimoAnoMes       PARAMETROS.ULTIMO_ANO_MES_FINALIZADO%type;
begin
  for xEmpresas in ( select EMPRESA_ID from EMPRESAS where ATIVO = 'S' ORDER BY 1 ) loop

    /* Descobrindo quando se inicou o uso do sistema */
    select
      min(DATA_HORA_RECEBIMENTO)
    into
      vDataPrimeiraVenda
    from
      ORCAMENTOS ORC

    inner join TURNOS_CAIXA TUR
    on ORC.TURNO_ID = TUR.TURNO_ID

    where ORC.STATUS = 'RE'
    and TUR.FUNCIONARIO_ID <> 1
    and ORC.EMPRESA_ID = xEmpresas.EMPRESA_ID;

    if vDataPrimeiraVenda is null then
      continue;
    end if;

    select
      nvl(ULTIMO_ANO_MES_FINALIZADO, to_number(to_char(add_months(vDataPrimeiraVenda, -1), 'yyyymm')))
    into
      vUltimoAnoMes
    from
      PARAMETROS_EMPRESA
    where
      EMPRESA_ID = xEmpresas.EMPRESA_ID;

    /* Se já finalizou, não fazer nada */
    if vUltimoAnoMes >= to_number(to_char(add_months(sysdate, -1), 'yyyymm')) then
      continue;
    end if;

    insert into CUSTOS_PRODUTOS_ANO_MES(
      EMPRESA_ID,
      PRODUTO_ID,
      ANO_MES,
      PRECO_FINAL,
      PRECO_FINAL_MEDIO,
      PRECO_LIQUIDO,
      PRECO_LIQUIDO_MEDIO,
      CMV,
      CUSTO_PEDIDO_MEDIO,
      CUSTO_ULTIMO_PEDIDO
    )
    select
      EMPRESA_ID,
      PRODUTO_ID,
      to_number(to_char(add_months(sysdate, -1), 'yyyymm')),
      PRECO_FINAL,
      PRECO_FINAL_MEDIO,
      PRECO_LIQUIDO,
      PRECO_LIQUIDO_MEDIO,
      CMV,
      CUSTO_PEDIDO_MEDIO,
      CUSTO_ULTIMO_PEDIDO
    from
      CUSTOS_PRODUTOS
    where EMPRESA_ID = xEmpresas.EMPRESA_ID;

    insert into ESTOQUES_ANO_MES(
      EMPRESA_ID,
      PRODUTO_ID,
      ANO_MES,
      ESTOQUE,
      FISICO,
      DISPONIVEL,
      RESERVADO
    )
    select
      EMPRESA_ID,
      PRODUTO_ID,
      to_number(to_char(add_months(sysdate, -1), 'yyyymm')),
      ESTOQUE,
      FISICO,
      DISPONIVEL,
      RESERVADO
    from
      ESTOQUES
    where EMPRESA_ID = xEmpresas.EMPRESA_ID;

    update PARAMETROS_EMPRESA set
      ULTIMO_ANO_MES_FINALIZADO = to_number(to_char(add_months(sysdate, -1), 'yyyymm'))
    where EMPRESA_ID = xEmpresas.EMPRESA_ID;

  end loop;

end FINALIZAR_ANO_MES;
/
