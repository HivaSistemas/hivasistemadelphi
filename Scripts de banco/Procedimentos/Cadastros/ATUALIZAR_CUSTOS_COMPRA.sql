create or replace procedure ATUALIZAR_CUSTOS_COMPRA(
  iEMPRESA_ID in number, 
  iPRODUTO_ID in number,
  iNOVO_CUSTO_PEDIDO in number,
  iNOVA_QUANTIDADE in number
)
is
  vProdutoPaiId    PRODUTOS.PRODUTO_PAI_ID%type;
  vQtdeVezesPai    PRODUTOS.QUANTIDADE_VEZES_PAI%type;
  
  vCustoMedioAtual CUSTOS_PRODUTOS.CUSTO_PEDIDO_MEDIO%type;
  vEstoqueAtual    ESTOQUES.FISICO%type;
begin

  select
    PRODUTO_PAI_ID,
    QUANTIDADE_VEZES_PAI
  into
    vProdutoPaiId,
    vQtdeVezesPai
  from
    PRODUTOS
  where PRODUTO_ID = iPRODUTO_ID;
  
  
  select
    case when EST.FISICO - EST.ESTOQUE <= 0 then 0 else (EST.FISICO - EST.ESTOQUE) * vQtdeVezesPai * CUS.CUSTO_PEDIDO_MEDIO end,
    case when EST.FISICO - EST.ESTOQUE <= 0 then 0 else EST.FISICO - EST.ESTOQUE end
  into
    vCustoMedioAtual,
    vEstoqueAtual
  from
    ESTOQUES EST
    
  inner join CUSTOS_PRODUTOS CUS
  on EST.EMPRESA_ID = CUS.EMPRESA_ID
  and EST.PRODUTO_ID = CUS.PRODUTO_ID
    
  where EST.PRODUTO_ID = vProdutoPaiId
  and EST.EMPRESA_ID = iEMPRESA_ID;
  
  vCustoMedioAtual := (vCustoMedioAtual + (iNOVO_CUSTO_PEDIDO * iNOVA_QUANTIDADE)) / ( vEstoqueAtual + iNOVA_QUANTIDADE );
  
  update CUSTOS_PRODUTOS set
    CUSTO_PEDIDO_MEDIO_ANTERIOR = CUSTO_PEDIDO_MEDIO,
    CUSTO_PEDIDO_MEDIO = vCustoMedioAtual,
    CUSTO_ULTIMO_PEDIDO = iNOVO_CUSTO_PEDIDO
  where EMPRESA_ID = iEMPRESA_ID
  and PRODUTO_ID = vProdutoPaiId;

  ATUALIZAR_CMV(iEMPRESA_ID, iPRODUTO_ID, iNOVO_CUSTO_PEDIDO, iNOVA_QUANTIDADE);

end ATUALIZAR_CUSTOS_COMPRA;
/