﻿create or replace procedure INICIAR_INFORMAC_NOVO_PRODUTO(iPRODUTO_ID in number, iEMPRESA_ID in number default 0)
is

  vTipoControleEstoque PRODUTOS.TIPO_CONTROLE_ESTOQUE%type;
  vProdutoPaiId        PRODUTOS.PRODUTO_PAI_ID%type;
  vQtde                number;

  cursor cEmpresas is
  select
    EMPRESA_ID
  from
    EMPRESAS
  where ATIVO = 'S'
  and EMPRESA_ID = zvl(iEMPRESA_ID, EMPRESA_ID);

  cursor cLocais is
  select
    LOCAL_ID
  from
    LOCAIS_PRODUTOS
  where ATIVO = 'S';
    
begin
  select
    TIPO_CONTROLE_ESTOQUE,
    PRODUTO_PAI_ID
  into
    vTipoControleEstoque,
    vProdutoPaiId
  from
    PRODUTOS
  where PRODUTO_ID = iPRODUTO_ID;

  select
    count(*)
  into
    vQtde
  from
    PRODUTOS_LOTES
  where PRODUTO_ID = iPRODUTO_ID;

  if vQtde = 0 then
    insert into PRODUTOS_LOTES(
      PRODUTO_ID,
      LOTE
    )values(
      iPRODUTO_ID,
      '???'
    );
  end if;

  for vEmpresas in cEmpresas loop
    /* Produtos kit não tem estoque, o estoque é formado pelos produtos que o compõe */
    /* Produtos pai e filho não tem estoque, o estoque é do produto pai */
    /* Produtos industrializados não tem estoque de inicio, somente depois de produzidos */
    if vTipoControleEstoque not in('K', 'A') and vProdutoPaiId = iPRODUTO_ID then


      insert into ESTOQUES(
        EMPRESA_ID,
        PRODUTO_ID,
        ESTOQUE
      )values(
        vEmpresas.EMPRESA_ID,
        iPRODUTO_ID,
        0
      );

      for xLocais in cLocais loop
        insert into ESTOQUES_DIVISAO(
          EMPRESA_ID,
          LOCAL_ID,
          PRODUTO_ID,
          LOTE
        )values(
          vEmpresas.EMPRESA_ID,
          xLocais.LOCAL_ID,
          iPRODUTO_ID,
          '???'
        );
      end loop;

      insert into CUSTOS_PRODUTOS(
        EMPRESA_ID,
        PRODUTO_ID
      )values(
        vEmpresas.EMPRESA_ID,
        iPRODUTO_ID
      );
    end if;

    insert into PRECOS_PRODUTOS(
      EMPRESA_ID,
      PRODUTO_ID
    )values(
      vEmpresas.EMPRESA_ID,
      iPRODUTO_ID
    );

    insert into PRODUTOS_CUSTO_FIXO(
      EMPRESA_ID,
      PRODUTO_ID,
      CUSTO_FIXO
    )values(
      vEmpresas.EMPRESA_ID,
      iPRODUTO_ID,
      0
    );
  end loop;

end INICIAR_INFORMAC_NOVO_PRODUTO;
/