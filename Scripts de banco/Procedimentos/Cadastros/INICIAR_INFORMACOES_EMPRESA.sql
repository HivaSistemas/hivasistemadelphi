create or replace procedure INICIAR_INFORMACOES_EMPRESA(iEMPRESA_ID in number)
is
begin

  insert into PARAMETROS_EMPRESA(
    EMPRESA_ID,
    REGIME_TRIBUTARIO
  )values(
    iEMPRESA_ID,
    'LP'
  );

  insert into PARAMETROS_PLANOS_FINANC_EMPRE(
    EMPRESA_ID,
    PLANO_FIN_AJUSTE_EST_NORMAL_ID
  )values(
    iEMPRESA_ID,
    '2.001.004'
  );

  insert into PERCENTUAIS_CUSTOS_EMPRESA(
    EMPRESA_ID
  )values(
    iEMPRESA_ID
  );

  for xProd in ( select PRODUTO_ID from PRODUTOS ) loop
    INICIAR_INFORMAC_NOVO_PRODUTO(xProd.PRODUTO_ID, iEMPRESA_ID);
  end loop;

end INICIAR_INFORMACOES_EMPRESA;
/