create or replace procedure INICIAR_INFORMAC_NOVO_PROD_IND
is
  cursor cEmpresas is
  select
    EMPRESA_ID
  from
    EMPRESAS
  where ATIVO = 'S';

  cursor cLocais is
  select
    LOCAL_ID
  from
    LOCAIS_PRODUTOS
  where ATIVO = 'S';

  cursor cProdutos is
  select
    PRODUTO_ID
  from
    PRODUTOS
  where TIPO_CONTROLE_ESTOQUE = 'I'
  and PRODUTO_ID not in(
    select
      PRODUTO_ID
    from
      ESTOQUES_DIVISAO
  );

begin
  for vProdutos in cProdutos loop
   
   for vEmpresas in cEmpresas loop
      insert into ESTOQUES(
        EMPRESA_ID,
        PRODUTO_ID,
        ESTOQUE
      )values(
        vEmpresas.EMPRESA_ID,
        vProdutos.PRODUTO_ID,
        0
      );
	  
      insert into CUSTOS_PRODUTOS(
        EMPRESA_ID,
        PRODUTO_ID
      )values(
        vEmpresas.EMPRESA_ID,
        vProdutos.PRODUTO_ID
      );	  
	  
	  for xLocais in cLocais loop
        insert into ESTOQUES_DIVISAO(
          EMPRESA_ID,
          LOCAL_ID,
          PRODUTO_ID,
          LOTE
        )values(
          vEmpresas.EMPRESA_ID,
          xLocais.LOCAL_ID,
          vProdutos.PRODUTO_ID,
          '???'
        );	  
	  end loop;
	  
	end loop;
	
  end loop;

end INICIAR_INFORMAC_NOVO_PROD_IND;
/