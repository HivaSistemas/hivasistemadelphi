create or replace procedure GERAR_CREDITO_DEVOLUCAO(iDEVOLUCAO_ID in number)
is
  vClienteId   CONTAS_PAGAR.CADASTRO_ID%type;
  vEmpresaId   CONTAS_PAGAR.EMPRESA_ID%type;
  vPagarId     CONTAS_PAGAR.PAGAR_ID%type;
  vValorGerar  CONTAS_PAGAR.VALOR_DOCUMENTO%type;

  vCobrancaId  PARAMETROS.TIPO_COBRANCA_GERACAO_CRED_ID%type;
  vIndice      CONTAS_PAGAR.INDICE_CONDICAO_PAGAMENTO%type;
  vQtdeDiasDevolucao number(6);
  vQtdeMaximaDiasDevolucao PARAMETROS.QTDE_MAXIMA_DIAS_DEVOLUCAO%type;
  vPercentualDeducao PARAMETROS_DIAS_DEVOLUCAO.PERCENTUAL%type;
begin

  select
    DEV.EMPRESA_ID,
    nvl(DEV.CLIENTE_CREDITO_ID, ORC.CLIENTE_ID) as CLIENTE_ID,
    DEV.VALOR_LIQUIDO,
    ORC.INDICE_CONDICAO_PAGAMENTO,
    QTDE_DIAS_DEVOLUCAO
  into
    vEmpresaId,
    vClienteId,
    vValorGerar,
    vIndice,
    vQtdeDiasDevolucao
  from
    DEVOLUCOES DEV

  inner join ORCAMENTOS ORC
  on DEV.ORCAMENTO_ID = ORC.ORCAMENTO_ID

  where DEV.DEVOLUCAO_ID = iDEVOLUCAO_ID;

  begin
    select
      TIPO_COBRANCA_GERACAO_CRED_ID,
      QTDE_MAXIMA_DIAS_DEVOLUCAO
    into
      vCobrancaId,
      vQtdeMaximaDiasDevolucao
    from
      PARAMETROS;
  exception
    when others then
      ERRO('Tipo de cobran�a para gera��o do cr�dito n�o parametrizado, fa�a a parametriza��o nos "Par�metros"!');
  end;

  vPercentualDeducao := 0;

  if vQtdeMaximaDiasDevolucao < vQtdeDiasDevolucao then
    begin
      select
        PERCENTUAL
      into
        vPercentualDeducao
      from
        PARAMETROS_DIAS_DEVOLUCAO
      where EMPRESA_ID = vEmpresaId
      and VALOR_INICIAL <= vQtdeDiasDevolucao
      and VALOR_FINAL >= vQtdeDiasDevolucao;
    exception
      when others then
        ERRO('Quantidade de dias de devolu��o n�o configurado, fa�a a parametriza��o nos "Par�metros de devolu��o"!');
    end;

    vValorGerar := vValorGerar - (vValorGerar * (vPercentualDeducao / 100));
  end if;

  if vClienteId = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then
    ERRO('N�o � permitido gerar cr�dito para consumidor final!');
  end if;

  select SEQ_PAGAR_ID.nextval
  into vPagarId
  from dual;

  insert into CONTAS_PAGAR(
    PAGAR_ID,
    CADASTRO_ID,
    EMPRESA_ID,
    DOCUMENTO,
    ORIGEM,
    DEVOLUCAO_ID,
    COBRANCA_ID,
    PORTADOR_ID,
    PLANO_FINANCEIRO_ID,
    DATA_VENCIMENTO,
    DATA_VENCIMENTO_ORIGINAL,
    VALOR_DOCUMENTO,
    STATUS,
    PARCELA,
    NUMERO_PARCELAS,
    INDICE_CONDICAO_PAGAMENTO,
	  BLOQUEADO,
    MOTIVO_BLOQUEIO
  )values(
    vPagarId,
    vClienteId,
    vEmpresaId,
    'DEV-' || NFORMAT(iDEVOLUCAO_ID, 0), -- Documento
    'DEV',
    iDEVOLUCAO_ID,
    vCobrancaId,
    '9999',
    '1.002.001',
    trunc(sysdate),
    trunc(sysdate),
    vValorGerar,
    'A',
    1,
    1,
    vIndice,
	  SESSAO.PARAMETROS_GERAIS.BLOQUEAR_CREDITO_DEVOLUCAO,
    case when SESSAO.PARAMETROS_GERAIS.BLOQUEAR_CREDITO_DEVOLUCAO = 'S' then 'Bloqueio autom�tico de novos cr�ditos' else null end
  );

end GERAR_CREDITO_DEVOLUCAO;
/