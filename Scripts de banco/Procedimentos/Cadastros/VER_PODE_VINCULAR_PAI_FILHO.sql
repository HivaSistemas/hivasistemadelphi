﻿create or replace procedure VER_PODE_VINCULAR_PAI_FILHO(pPRODUTO_ID in number)
is
  vQtde number;
begin
  
  select
    count(*)
  into
    vQtde
  from
    RETIRADAS_ITENS
  where PRODUTO_ID = pPRODUTO_ID;
  
  if vQtde < 0 then
    ERRO('Já houve vendas com este produto selecionado!');
  end if;

end VER_PODE_VINCULAR_PAI_FILHO;