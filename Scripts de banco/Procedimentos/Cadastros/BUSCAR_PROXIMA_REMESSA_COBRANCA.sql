﻿create or replace procedure BUSCAR_PROXIMA_REMESSA_COBR(
  iCONTA_ID in string,
  oNUMERO_REMESSA_BOLETO out number
)
is

  vProximaRemessa   CONTAS.NUMERO_REMESSA_BOLETO%type;

begin

  select
    NUMERO_REMESSA_BOLETO
  into
    vProximaRemessa
  from
    CONTAS
  where CONTA = iCONTA_ID;
  
  if vProximaRemessa is null then
    ERRO('O numero de remessa para a conta ' || iCONTA_ID || ' não foi configurada!' );
  end if;
    
  vProximaRemessa := vProximaRemessa + 1;
  
  update CONTAS set
    NUMERO_REMESSA_BOLETO = vProximaRemessa
  where CONTA = iCONTA_ID;

  oNUMERO_REMESSA_BOLETO := vProximaRemessa;

end BUSCAR_PROXIMA_REMESSA_COBR;
/