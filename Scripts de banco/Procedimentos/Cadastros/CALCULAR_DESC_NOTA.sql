create or replace procedure CALCULAR_DESC_NOTA(
  oNOTA_FISCAL_ID in number,
  oINDICE_DESC_NOTA in number
)
is
  vDadosDescontoVenda     	RecordsNotasFiscais.RecDescontoConcedido;
  vDadosNota                RecordsNotasFiscais.RecNotaFiscal;
  vDadosItens               RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;  
  vIndice                	  number default 1;
  vIndiceDescVenda        	number default 1;  
  i							number default 0;
  
  cursor cItens(pNotaFiscalId in number) is
	select
	  PRODUTO_ID,
	  ITEM_ID,
	  PRECO_UNITARIO,
	  VALOR_TOTAL,
	  VALOR_TOTAL_DESCONTO,
	  VALOR_TOTAL_OUTRAS_DESPESAS,
	  VALOR_IPI,
	  QUANTIDADE,
    BASE_CALCULO_ICMS,
    VALOR_ICMS,
    BASE_CALCULO_PIS,
    VALOR_PIS,
    BASE_CALCULO_COFINS,
    VALOR_COFINS
	from
	  NOTAS_FISCAIS_ITENS
	where NOTA_FISCAL_ID = pNotaFiscalId;
	
begin

  select
    IDV.INDICE_ID,
    IDV.PERCENTUAL_DESCONTO,
    IDV.PRECO_CUSTO,
    IDV.TIPO_CUSTO,
    IDV.TIPO_DESCONTO_PRECO_CUSTO
  into
    vDadosDescontoVenda.IndiceDescontoVendaId,
    vDadosDescontoVenda.PercentualDesconto,
    vDadosDescontoVenda.PrecoCusto,
    vDadosDescontoVenda.TipoCusto,
    vDadosDescontoVenda.TipoDescontoPrecoCusto
  from
    INDICES_DESCONTOS_VENDA IDV

  where IDV.INDICE_ID = oINDICE_DESC_NOTA;
  
	select
	  VALOR_TOTAL_PRODUTOS,
	  VALOR_DESCONTO,
	  VALOR_OUTRAS_DESPESAS,
	  VALOR_IPI,
	  VALOR_TOTAL,
	  VALOR_RECEBIDO_CARTAO_CRED,
	  VALOR_RECEBIDO_CARTAO_DEB,
	  VALOR_RECEBIDO_CHEQUE,
	  VALOR_RECEBIDO_COBRANCA,
	  VALOR_RECEBIDO_CREDITO,
	  VALOR_RECEBIDO_DINHEIRO,
    VALOR_RECEBIDO_PIX,
	  VALOR_RECEBIDO_FINANCEIRA,
    VALOR_RECEBIDO_ACUMULADO,
    BASE_CALCULO_ICMS,
    VALOR_ICMS,
    BASE_CALCULO_PIS,
    VALOR_PIS,
    BASE_CALCULO_COFINS,
    VALOR_COFINS
	into
	  vDadosNota.VALOR_TOTAL_PRODUTOS,
	  vDadosNota.VALOR_DESCONTO,
	  vDadosNota.VALOR_OUTRAS_DESPESAS,
	  vDadosNota.VALOR_IPI,
	  vDadosNota.VALOR_TOTAL,
	  vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,
	  vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,
	  vDadosNota.VALOR_RECEBIDO_CHEQUE,
	  vDadosNota.VALOR_RECEBIDO_COBRANCA,
	  vDadosNota.VALOR_RECEBIDO_CREDITO,
	  vDadosNota.VALOR_RECEBIDO_DINHEIRO,
    vDadosNota.VALOR_RECEBIDO_PIX,
	  vDadosNota.VALOR_RECEBIDO_FINANCEIRA,
    vDadosNota.VALOR_RECEBIDO_ACUMULADO,
    vDadosNota.BASE_CALCULO_ICMS,
    vDadosNota.VALOR_ICMS,
    vDadosNota.BASE_CALCULO_PIS,
    vDadosNota.VALOR_PIS,
    vDadosNota.BASE_CALCULO_COFINS,
    vDadosNota.VALOR_COFINS
	from
	  NOTAS_FISCAIS
	where NOTA_FISCAL_ID = oNOTA_FISCAL_ID;  

  if vDadosDescontoVenda.IndiceDescontoVendaId is not null then
    if vDadosDescontoVenda.PrecoCusto = 'N' then
      vIndiceDescVenda := 1 - vDadosDescontoVenda.PercentualDesconto * 0.01;
    else
      if vDadosDescontoVenda.TipoDescontoPrecoCusto = 'A' then
        vIndiceDescVenda := 1 + vDadosDescontoVenda.PercentualDesconto * 0.01;
      elsif vDadosDescontoVenda.TipoDescontoPrecoCusto = 'D' then
        vIndiceDescVenda := 1 - vDadosDescontoVenda.PercentualDesconto * 0.01;
      else
        vIndiceDescVenda := 1;
      end if;
    end if;
  end if;
  
  vDadosNota.VALOR_IPI := 0;
  vDadosNota.VALOR_TOTAL := 0;
  vDadosNota.VALOR_TOTAL_PRODUTOS := 0;
  vDadosNota.VALOR_DESCONTO := 0;
  vDadosNota.VALOR_OUTRAS_DESPESAS := 0;
  vDadosNota.BASE_CALCULO_ICMS := 0;
  vDadosNota.VALOR_ICMS := 0;
  vDadosNota.BASE_CALCULO_PIS := 0;
  vDadosNota.VALOR_PIS := 0;
  vDadosNota.BASE_CALCULO_COFINS := 0;
  vDadosNota.VALOR_COFINS := 0;
  for vItens in cItens(oNOTA_FISCAL_ID) loop
    vDadosItens(i).PRODUTO_ID	 			         := vItens.PRODUTO_ID;
	vDadosItens(i).ITEM_ID  	 			           := vItens.ITEM_ID;
	vDadosItens(i).PRECO_UNITARIO              := trunc(vItens.PRECO_UNITARIO * vIndiceDescVenda, 2);
	vDadosItens(i).VALOR_TOTAL                 := trunc(vDadosItens(i).PRECO_UNITARIO * vItens.QUANTIDADE, 2);
	vDadosItens(i).VALOR_TOTAL_DESCONTO        := trunc(vItens.VALOR_TOTAL_DESCONTO * vIndiceDescVenda, 2);
	vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := trunc(vItens.VALOR_TOTAL_OUTRAS_DESPESAS * vIndiceDescVenda, 2);
	vDadosItens(i).VALOR_IPI				           := trunc(vItens.VALOR_IPI * vIndiceDescVenda, 2);

	vDadosItens(i).BASE_CALCULO_ICMS				   := trunc(vItens.BASE_CALCULO_ICMS * vIndiceDescVenda, 2);
	vDadosItens(i).VALOR_ICMS				           := trunc(vItens.VALOR_ICMS * vIndiceDescVenda, 2);
	vDadosItens(i).BASE_CALCULO_PIS				     := trunc(vItens.BASE_CALCULO_PIS * vIndiceDescVenda, 2);
	vDadosItens(i).VALOR_PIS				           := trunc(vItens.VALOR_PIS * vIndiceDescVenda, 2);
	vDadosItens(i).BASE_CALCULO_COFINS				 := trunc(vItens.BASE_CALCULO_COFINS * vIndiceDescVenda, 2);
	vDadosItens(i).VALOR_COFINS				         := trunc(vItens.VALOR_COFINS * vIndiceDescVenda, 2);

    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */
  vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;
  vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;
  vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;
  vDadosNota.BASE_CALCULO_COFINS  := vDadosNota.BASE_CALCULO_COFINS + vDadosItens(i).BASE_CALCULO_COFINS;
  vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;

    vDadosNota.VALOR_TOTAL :=
      vDadosNota.VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
      vDadosItens(i).VALOR_IPI -
      vDadosItens(i).VALOR_TOTAL_DESCONTO;

    vDadosNota.VALOR_TOTAL_PRODUTOS := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;
    vDadosNota.VALOR_DESCONTO := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;
    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;
    vDadosNota.BASE_CALCULO_ICMS := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;
    vDadosNota.VALOR_ICMS := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;

    /* ------------------------------------------------------------------------------------------------------ */	

    i := i + 1;	
  end loop;  
  
  vDadosNota.VALOR_RECEBIDO_DINHEIRO    := vDadosNota.VALOR_RECEBIDO_DINHEIRO * vIndiceDescVenda;
  vDadosNota.VALOR_RECEBIDO_PIX         := vDadosNota.VALOR_RECEBIDO_PIX * vIndiceDescVenda;
  vDadosNota.VALOR_RECEBIDO_CARTAO_CRED := vDadosNota.VALOR_RECEBIDO_CARTAO_CRED * vIndiceDescVenda;
  vDadosNota.VALOR_RECEBIDO_CARTAO_DEB  := vDadosNota.VALOR_RECEBIDO_CARTAO_DEB * vIndiceDescVenda;
  vDadosNota.VALOR_RECEBIDO_CREDITO     := vDadosNota.VALOR_RECEBIDO_CREDITO * vIndiceDescVenda;
  vDadosNota.VALOR_RECEBIDO_COBRANCA    := vDadosNota.VALOR_RECEBIDO_COBRANCA * vIndiceDescVenda;
  vDadosNota.VALOR_RECEBIDO_CHEQUE      := vDadosNota.VALOR_RECEBIDO_CHEQUE * vIndiceDescVenda;
  vDadosNota.VALOR_RECEBIDO_FINANCEIRA  := vDadosNota.VALOR_RECEBIDO_FINANCEIRA * vIndiceDescVenda;
  vDadosNota.VALOR_RECEBIDO_ACUMULADO   := vDadosNota.VALOR_RECEBIDO_ACUMULADO * vIndiceDescVenda;

  RATEAR_VALORES_FORMAS_PAGTO_NF(
    vDadosNota.VALOR_RECEBIDO_DINHEIRO,
    vDadosNota.VALOR_RECEBIDO_PIX,
    vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,
    vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,
    vDadosNota.VALOR_RECEBIDO_COBRANCA,
    vDadosNota.VALOR_RECEBIDO_CHEQUE,
    vDadosNota.VALOR_RECEBIDO_FINANCEIRA,
    vDadosNota.VALOR_RECEBIDO_CREDITO,
    vDadosNota.VALOR_RECEBIDO_ACUMULADO,
    vDadosNota.VALOR_TOTAL
  );  
  
  update NOTAS_FISCAIS set 
    VALOR_TOTAL = vDadosNota.VALOR_TOTAL,
    VALOR_TOTAL_PRODUTOS = vDadosNota.VALOR_TOTAL_PRODUTOS,
    VALOR_DESCONTO = vDadosNota.VALOR_DESCONTO,
    VALOR_OUTRAS_DESPESAS = vDadosNota.VALOR_OUTRAS_DESPESAS,
    VALOR_IPI = vDadosNota.VALOR_IPI,
    VALOR_RECEBIDO_DINHEIRO = vDadosNota.VALOR_RECEBIDO_DINHEIRO,
    VALOR_RECEBIDO_PIX = vDadosNota.VALOR_RECEBIDO_PIX,
    VALOR_RECEBIDO_CARTAO_CRED = vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,
    VALOR_RECEBIDO_CARTAO_DEB = vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,
    VALOR_RECEBIDO_CREDITO = vDadosNota.VALOR_RECEBIDO_CREDITO,
    VALOR_RECEBIDO_COBRANCA = vDadosNota.VALOR_RECEBIDO_COBRANCA,
    VALOR_RECEBIDO_CHEQUE = vDadosNota.VALOR_RECEBIDO_CHEQUE,
    VALOR_RECEBIDO_FINANCEIRA = vDadosNota.VALOR_RECEBIDO_FINANCEIRA,
    VALOR_RECEBIDO_ACUMULADO = vDadosNota.VALOR_RECEBIDO_ACUMULADO,
    BASE_CALCULO_ICMS = vDadosNota.BASE_CALCULO_ICMS,
    VALOR_ICMS = vDadosNota.VALOR_ICMS,
    BASE_CALCULO_PIS = vDadosNota.BASE_CALCULO_PIS,
    VALOR_PIS = vDadosNota.VALOR_PIS,
    BASE_CALCULO_COFINS = vDadosNota.BASE_CALCULO_COFINS,
    VALOR_COFINS = vDadosNota.VALOR_COFINS
  where
    NOTA_FISCAL_ID = oNOTA_FISCAL_ID;
	
  for i in 0..vDadosItens.count - 1 loop
    
    update NOTAS_FISCAIS_ITENS set
      VALOR_TOTAL = vDadosItens(i).VALOR_TOTAL,
      PRECO_UNITARIO = vDadosItens(i).PRECO_UNITARIO,
      VALOR_TOTAL_DESCONTO = vDadosItens(i).VALOR_TOTAL_DESCONTO,
      VALOR_TOTAL_OUTRAS_DESPESAS = vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,
      VALOR_IPI  = vDadosItens(i).VALOR_IPI,
      BASE_CALCULO_ICMS  = vDadosItens(i).BASE_CALCULO_ICMS,
      VALOR_ICMS  = vDadosItens(i).VALOR_ICMS,
      BASE_CALCULO_PIS  = vDadosItens(i).BASE_CALCULO_PIS,
      VALOR_PIS  = vDadosItens(i).VALOR_PIS,
      BASE_CALCULO_COFINS  = vDadosItens(i).BASE_CALCULO_COFINS,
      VALOR_COFINS  = vDadosItens(i).VALOR_COFINS
	where NOTA_FISCAL_ID = oNOTA_FISCAL_ID
    and PRODUTO_ID = vDadosItens(i).PRODUTO_ID
    and ITEM_ID = vDadosItens(i).ITEM_ID;
  end loop;

end CALCULAR_DESC_NOTA;