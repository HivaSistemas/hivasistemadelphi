create or replace procedure EXCLUIR_CADASTRO(pCADASTRO_ID in number)
is
  vQtde number;
begin

  select
    count(*)
  into
    vQtde
  from
    CADASTROS
  where CADASTRO_ID = pCADASTRO_ID
  and (
    E_CLIENTE        = 'S' or
    E_CONCORRENTE    = 'S' or
    E_FORNECEDOR     = 'S' or
    E_FUNCIONARIO    = 'S' or
    E_MOTORISTA      = 'S' or
    E_PROFISSIONAL   = 'S' or
    E_TRANSPORTADORA = 'S'
  );

  if vQtde > 0 then
    return;
  end if;

  begin
    delete from LOGS_CADASTROS where CADASTRO_ID = pCADASTRO_ID;
    delete from CADASTROS where CADASTRO_ID = pCADASTRO_ID;
  exception
    when others then
      ERRO('Existem opera��es j� realizadas com esse cadastro!');
  end;

end EXCLUIR_CADASTRO;
