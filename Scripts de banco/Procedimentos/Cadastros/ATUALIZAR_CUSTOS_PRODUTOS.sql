create or replace procedure ATUALIZAR_CUSTOS_PRODUTOS(
  iEMPRESA_ID         in number,
  iPRODUTO_ID         in number,
  iNOVO_PRECO_FINAL   in number,
  iNOVO_PRECO_LIQUIDO in number,
  iNOVA_QUANTIDADE    in number,
  iORIGEM             in string,
  iORIGEM_ID          in number,
  iPRECO_TABELA       in number,
  iPERC_ICMS          in number,
  iPERC_ST            in number,
  iPERC_FRETE         in number,
  iPERC_ICMS_FRETE    in number,
  iPERC_PIS           in number,
  iPERC_COFINS        in number,
  iPERC_DESCONTO      in number,
  iPERC_OUTRAS_DESP   in number,
  iPERC_OUTROS_CUSTOS in number,
  iPERC_IPI           in number,
  iPERC_DIFAL         in number
)
is
  vProdutoPaiId  PRODUTOS.PRODUTO_PAI_ID%type;
  vQtdeVezesPai  PRODUTOS.QUANTIDADE_VEZES_PAI%type;
  
  vPrecoFinalMedioAtual   number(20,5);
  vPrecoLiquidoMedioAtual number(20,5);

  vEstoqueAtual           ESTOQUES.ESTOQUE%type;
begin

  select
    QUANTIDADE_VEZES_PAI,
    PRODUTO_PAI_ID
  into
    vQtdeVezesPai,
    vProdutoPaiId
  from
    PRODUTOS
  where PRODUTO_ID = iPRODUTO_ID;

  begin
    select
      case when EST.ESTOQUE <= 0 then 0 else EST.ESTOQUE * vQtdeVezesPai * CUS.PRECO_FINAL_MEDIO end,
      case when EST.ESTOQUE <= 0 then 0 else EST.ESTOQUE * vQtdeVezesPai * CUS.PRECO_LIQUIDO_MEDIO end,
      case when EST.ESTOQUE <= 0 then 0 else EST.ESTOQUE end
    into
      vPrecoFinalMedioAtual,
      vPrecoLiquidoMedioAtual,
      vEstoqueAtual
    from
      ESTOQUES EST

    inner join CUSTOS_PRODUTOS CUS
    on EST.EMPRESA_ID = CUS.EMPRESA_ID
    and EST.PRODUTO_ID = CUS.PRODUTO_ID

    where EST.PRODUTO_ID = vProdutoPaiId
    and EST.EMPRESA_ID = iEMPRESA_ID;
  exception
    when others then
      erro(
        'Problemas na busca dos custos m�dio. Produto ' || vProdutoPaiId || chr(13) ||
        sqlerrm
      );
  end;

  begin
    vPrecoFinalMedioAtual   := (vPrecoFinalMedioAtual + ( iNOVO_PRECO_FINAL * iNOVA_QUANTIDADE )) / ( vEstoqueAtual + iNOVA_QUANTIDADE );
    vPrecoLiquidoMedioAtual := (vPrecoLiquidoMedioAtual + ( iNOVO_PRECO_LIQUIDO * iNOVA_QUANTIDADE )) / ( vEstoqueAtual + iNOVA_QUANTIDADE );
  exception
    when others then
      erro(
        'Problemas no calculo do custo m�dio. Produto ' || vProdutoPaiId || chr(13) ||
        sqlerrm
      );
  end;

  begin
    update CUSTOS_PRODUTOS set
      PRECO_TABELA                   = round(iPRECO_TABELA / vQtdeVezesPai, 4),
      PERCENTUAL_ICMS_ENTRADA        = iPERC_ICMS,
      PERCENTUAL_ST_ENTRADA          = iPERC_ST,
      PERCENTUAL_FRETE_ENTRADA       = iPERC_FRETE,
      PERCENTUAL_ICMS_FRETE_ENTRADA  = iPERC_ICMS_FRETE,
      PERCENTUAL_PIS_ENTRADA         = iPERC_PIS,
      PERCENTUAL_COFINS_ENTRADA      = iPERC_COFINS,
      PERCENTUAL_DESCONTO_ENTRADA    = iPERC_DESCONTO,
      PERCENTUAL_OUTRAS_DESPESAS_ENT = iPERC_OUTRAS_DESP,
      PERCENTUAL_OUTROS_CUSTOS_ENTR  = iPERC_OUTROS_CUSTOS,
      PERCENTUAL_IPI_ENTRADA         = iPERC_IPI,
      PERCENTUAL_DIFAL_ENTRADA       = iPERC_DIFAL,

      PRECO_FINAL                    = round(iNOVO_PRECO_FINAL / vQtdeVezesPai, 4),
      PRECO_FINAL_MEDIO              = vPrecoFinalMedioAtual,
      PRECO_FINAL_ANTERIOR           = PRECO_FINAL_MEDIO,

      PRECO_LIQUIDO                  = round(iNOVO_PRECO_LIQUIDO / vQtdeVezesPai, 4),
      PRECO_LIQUIDO_MEDIO            = vPrecoLiquidoMedioAtual,
      PRECO_LIQUIDO_ANTERIOR         = PRECO_LIQUIDO_MEDIO,

      ORIGEM                         = iORIGEM,
      ORIGEM_ID                      = iORIGEM_ID
    where EMPRESA_ID = iEMPRESA_ID
    and PRODUTO_ID = vProdutoPaiId;
  exception
    when others then
      ERRO(
        'Problemas ao consolidar o novo custo. Produto ' || vProdutoPaiId || chr(13) ||
        '% ICMS ' || iPERC_ICMS || chr(13) ||
        '% ST ' || iPERC_ST || chr(13) ||
        '% Frete ' || iPERC_FRETE || chr(13) ||
        '% ICMS Frete ' || iPERC_ICMS_FRETE || chr(13) ||
        '% PIS ' || iPERC_PIS || chr(13) ||
        '% COFINS ' || iPERC_COFINS || chr(13) ||
        '% Outros custos ' || iPERC_OUTROS_CUSTOS || chr(13) ||
        sqlerrm
      );
  end;

end ATUALIZAR_CUSTOS_PRODUTOS;
/