create or replace procedure ATUALIZAR_NOTAS_SCRIPT
is
  cadastroId  CADASTROS.CADASTRO_ID%type;
  cursor cNotas is
  select
    NOTA_FISCAL_ID,
    CPF_CNPJ_DESTINATARIO
  from NOTAS_FISCAIS
  WHERE TIPO_NOTA = 'N'
  AND MODELO_NOTA = 55
  AND STATUS = 'N'
  AND trunc(data_hora_alteracao) = to_date('11/01/2021', 'dd/mm/yyyy')
  AND EMPRESA_ID = 1
  AND CADASTRO_ID = 1
  and CPF_CNPJ_DESTINATARIO <> '00000000000'
  and CPF_CNPJ_DESTINATARIO <> '   .   .   -  ';  
begin
 
  for vNotas in cNotas loop
   begin
    select
    CADASTRO_ID
  into
    cadastroId
  from
    CADASTROS
  where CPF_CNPJ = vNotas.CPF_CNPJ_DESTINATARIO;
    exception
    when others then
        erro(vNotas.CPF_CNPJ_DESTINATARIO);  
    end;
  
  update
    NOTAS_FISCAIS
  set
    CADASTRO_ID = cadastroId
  where NOTA_FISCAL_ID = vNotas.NOTA_FISCAL_ID; 
  end loop;  
    
end;
