create or replace procedure ATUALIZAR_CODIGO_ORIGINAL
is
  codigoOriginal PRODUTOS_FORN_COD_ORIGINAIS.CODIGO_ORIGINAL%type;
  cursor cProdutos is
  select
    PRODUTO_ID
  from PRODUTOS
  where CODIGO_ORIGINAL_FABRICANTE is null
  and PRODUTO_ID in (
    select
      PRODUTO_ID
    from PRODUTOS_FORN_COD_ORIGINAIS
  );
begin
  for vProdutos in cProdutos loop
    begin
		select CODIGO_ORIGINAL into codigoOriginal from (
		  select
			CODIGO_ORIGINAL
		  from PRODUTOS_FORN_COD_ORIGINAIS
		  where PRODUTO_ID = vProdutos.PRODUTO_ID
		  order by DATA_HORA_ALTERACAO desc
		) where rownum = 1;
    exception
      when others then
        codigoOriginal := '';
    end;
	
	update PRODUTOS set
	  CODIGO_ORIGINAL_FABRICANTE = codigoOriginal
	where PRODUTO_ID = vProdutos.PRODUTO_ID;
  end loop;
end;
