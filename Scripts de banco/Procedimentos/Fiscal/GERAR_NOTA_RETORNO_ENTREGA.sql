﻿create or replace procedure GERAR_NOTA_RETORNO_ENTREGA(iENTREGA_ID in number)
is
  i                       number default 0;
  vQtde                   number default 0;
  vNotaFiscalId           positive;
  vDadosNota              RecordsNotasFiscais.RecNotaFiscal;
  vDadosItens             RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;

  vValorMaisSignificativo number default 0;
  vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;
  vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;
  vNotasReferenciasIds    TIPOS.ArrayOfNumeros;
  
  vTipoOperacao           char(1);
  vSerieNFe               PARAMETROS_EMPRESA.SERIE_NFE%type;

  cursor cItens is
  select
    NIT.NOTA_FISCAL_ID as NOTA_FISCAL_ORIGEM_ID,
    ITE.PRODUTO_ID,
    ITE.ITEM_ID,
    EIT.QUANTIDADE,
    NIT.NOME_PRODUTO,

    round(NIT.VALOR_TOTAL / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_TOTAL,
    NIT.PRECO_UNITARIO,
    round(NIT.VALOR_TOTAL_DESCONTO / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_TOTAL_DESCONTO,
    round(NIT.VALOR_TOTAL_OUTRAS_DESPESAS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_TOTAL_OUTRAS_DESPESAS,

    round(NIT.BASE_CALCULO_ICMS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as BASE_CALCULO_ICMS,
    NIT.PERCENTUAL_ICMS,
    round(NIT.VALOR_ICMS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_ICMS,
    NIT.INDICE_REDUCAO_BASE_ICMS,

    round(NIT.BASE_CALCULO_ICMS_ST / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as BASE_CALCULO_ICMS_ST,
    NIT.PERCENTUAL_ICMS_ST,
    round(NIT.VALOR_ICMS_ST / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_ICMS_ST,
    NIT.INDICE_REDUCAO_BASE_ICMS_ST,

    NIT.PRECO_PAUTA,
    NIT.IVA,

    round(NIT.BASE_CALCULO_COFINS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as BASE_CALCULO_COFINS,
    NIT.PERCENTUAL_COFINS,
    round(NIT.VALOR_COFINS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_COFINS,

    round(NIT.BASE_CALCULO_PIS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as BASE_CALCULO_PIS,
    NIT.PERCENTUAL_PIS,
    round(NIT.VALOR_PIS / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_PIS,

    round(NIT.VALOR_IPI / NIT.QUANTIDADE * EIT.QUANTIDADE, 2) as VALOR_IPI,
    NIT.PERCENTUAL_IPI,

    NIT.CST,
    NIT.CODIGO_NCM,
    NIT.UNIDADE,
    NIT.CODIGO_BARRAS,
    NIT.CEST,
    NIT.CST_PIS,
    NIT.CST_COFINS
  from
    (
      /* Este select é feito desta forma por causa dos LOTES! */
      select
        ENTREGA_ID,
        PRODUTO_ID,
        ITEM_ID,
        sum(RETORNADOS) as QUANTIDADE
      from
        ENTREGAS_ITENS
      where ENTREGA_ID = iENTREGA_ID
      group by
        ENTREGA_ID,
        PRODUTO_ID,
        ITEM_ID
      having sum(RETORNADOS) > 0
    ) EIT
    
  inner join ENTREGAS ENT
  on EIT.ENTREGA_ID = ENT.ENTREGA_ID

  inner join ORCAMENTOS_ITENS ITE
  on ENT.ORCAMENTO_ID = ITE.ORCAMENTO_ID
  and EIT.PRODUTO_ID = ITE.PRODUTO_ID
  and EIT.ITEM_ID = ITE.ITEM_ID

  inner join NOTAS_FISCAIS NOF
  on EIT.ENTREGA_ID = NOF.ENTREGA_ID

  inner join NOTAS_FISCAIS_ITENS NIT
  on NOF.NOTA_FISCAL_ID = NIT.NOTA_FISCAL_ID
  and ITE.ITEM_ID = NIT.ITEM_ID

  where EIT.ENTREGA_ID = iENTREGA_ID
  /* Nâo trazer os produtos KITS */
  and ITE.TIPO_CONTROLE_ESTOQUE not in('K', 'A')
  order by
    ITE.ITEM_ID;

begin 

  select
    count(*)
  into
    vQtde
  from
    NOTAS_FISCAIS
  where ENTREGA_ID = iENTREGA_ID
  and TIPO_MOVIMENTO = 'VEN'
  and STATUS = 'E';
  
  if vQtde = 0 then
    return;
  end if;
  
  select
    count(*)
  into
    vQtde
  from
    ENTREGAS_ITENS
  where ENTREGA_ID = iENTREGA_ID
  and RETORNADOS > 0;
  
  if vQtde = 0 then
    return;
  end if;
  
  begin
    select
      NFI.EMPRESA_ID,
      NFI.CADASTRO_ID,
      NFI.ORCAMENTO_ID,
      0 as VALOR_DINHEIRO,
      0 as VALOR_CARTAO_CREDITO,
      0 as VALOR_CARTAO_DEBITO,
      0 as VALOR_CREDITO,
      0 as VALOR_COBRANCA,
      0 as VALOR_CHEQUE,
      0 as VALOR_FINANCEIRA,
      0 as VALOR_FRETE,
      0 as VALOR_SEGURO,
      
      '' as NOME_CONSUMIDOR_FINAL,

      /* Emitente */      
      EMP.RAZAO_SOCIAL,
      EMP.NOME_FANTASIA,      
      EMP.CNPJ,
      EMP.INSCRICAO_ESTADUAL,
      EMP.LOGRADOURO,
      EMP.COMPLEMENTO,
      BAE.NOME as NOME_BAIRRO_EMITENTE,
      CIE.NOME as NOME_CIDADE_EMITENTE,
      EMP.NUMERO,
      ESE.ESTADO_ID,
      EMP.CEP,
      EMP.TELEFONE_PRINCIPAL,
      CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,
      ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,
      
      /* Destinatario */
      NFI.NOME_FANTASIA_DESTINATARIO,
      NFI.RAZAO_SOCIAL_DESTINATARIO,
      NFI.TIPO_PESSOA_DESTINATARIO,
      NFI.CPF_CNPJ_DESTINATARIO,
      NFI.INSCRICAO_ESTADUAL_DESTINAT,
      NFI.LOGRADOURO_DESTINATARIO,
      NFI.COMPLEMENTO_DESTINATARIO,
      NFI.NUMERO_DESTINATARIO,
      NFI.CEP_DESTINATARIO,      
      NFI.NOME_BAIRRO_DESTINATARIO,
      NFI.NOME_CIDADE_DESTINATARIO,
      NFI.CODIGO_IBGE_MUNICIPIO_DEST,
      NFI.ESTADO_ID_DESTINATARIO,
      
      case when NFI.ESTADO_ID_DESTINATARIO = ESE.ESTADO_ID then 'I' else 'E' end,
      PAE.SERIE_NFE
    into
      vDadosNota.EMPRESA_ID,
      vDadosNota.CADASTRO_ID,
      vDadosNota.ORCAMENTO_ID,
      vDadosNota.VALOR_RECEBIDO_DINHEIRO,
      vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,
      vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,
      vDadosNota.VALOR_RECEBIDO_CREDITO,
      vDadosNota.VALOR_RECEBIDO_COBRANCA,
      vDadosNota.VALOR_RECEBIDO_CHEQUE,
      vDadosNota.VALOR_RECEBIDO_FINANCEIRA,
      vDadosNota.VALOR_FRETE,
      vDadosNota.VALOR_SEGURO,
      
      vDadosNota.NOME_CONSUMIDOR_FINAL,
      /* Emitente */
      vDadosNota.RAZAO_SOCIAL_EMITENTE,
      vDadosNota.NOME_FANTASIA_EMITENTE,      
      vDadosNota.CNPJ_EMITENTE,
      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,
      vDadosNota.LOGRADOURO_EMITENTE,
      vDadosNota.COMPLEMENTO_EMITENTE,
      vDadosNota.NOME_BAIRRO_EMITENTE,
      vDadosNota.NOME_CIDADE_EMITENTE,
      vDadosNota.NUMERO_EMITENTE,
      vDadosNota.ESTADO_ID_EMITENTE,
      vDadosNota.CEP_EMITENTE,
      vDadosNota.TELEFONE_EMITENTE,
      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,
      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,
      /* Destinatario */
      vDadosNota.NOME_FANTASIA_DESTINATARIO,
      vDadosNota.RAZAO_SOCIAL_DESTINATARIO,
      vDadosNota.TIPO_PESSOA_DESTINATARIO,
      vDadosNota.CPF_CNPJ_DESTINATARIO,
      vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,
      vDadosNota.LOGRADOURO_DESTINATARIO,
      vDadosNota.COMPLEMENTO_DESTINATARIO,
      vDadosNota.NUMERO_DESTINATARIO,
      vDadosNota.CEP_DESTINATARIO,
      vDadosNota.NOME_BAIRRO_DESTINATARIO,
      vDadosNota.NOME_CIDADE_DESTINATARIO,
      vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,
      vDadosNota.ESTADO_ID_DESTINATARIO,
      
      vTipoOperacao,
      vSerieNFe
    from
      NOTAS_FISCAIS NFI
      
    inner join PARAMETROS_EMPRESA PAE
    on NFI.EMPRESA_ID = PAE.EMPRESA_ID
    
    inner join EMPRESAS EMP
    on NFI.EMPRESA_ID = EMP.EMPRESA_ID
    
    inner join BAIRROS BAE
    on EMP.BAIRRO_ID = BAE.BAIRRO_ID
    
    inner join CIDADES CIE
    on BAE.CIDADE_ID = CIE.CIDADE_ID
    
    inner join ESTADOS ESE
    on CIE.ESTADO_ID = ESE.ESTADO_ID    
    
    where NFI.ENTREGA_ID = iENTREGA_ID
    and NFI.TIPO_MOVIMENTO = 'VEN'
    and NFI.STATUS = 'E';
  exception
    when others then
      ERRO('Houve um erro ao buscar os dados da devolução para geração da NFe!');
  end;

  if vDadosNota.CADASTRO_ID = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then
    ERRO('Não é possível gerar uma nota de devolução de uma venda para consumidor final!');
  end if;   

  vDadosNota.BASE_CALCULO_ICMS    := 0;
  vDadosNota.VALOR_ICMS           := 0;
  vDadosNota.BASE_CALCULO_ICMS_ST := 0;
  vDadosNota.VALOR_ICMS_ST        := 0;
  vDadosNota.BASE_CALCULO_PIS     := 0;
  vDadosNota.VALOR_PIS            := 0;
  vDadosNota.BASE_CALCULO_COFINS  := 0;
  vDadosNota.VALOR_COFINS         := 0;
  vDadosNota.VALOR_IPI            := 0;
  vDadosNota.VALOR_FRETE          := 0;
  vDadosNota.VALOR_SEGURO         := 0;
  vDadosNota.PESO_LIQUIDO         := 0;
  vDadosNota.PESO_BRUTO           := 0;  

  
  for vItens in cItens loop
    if length(vItens.CODIGO_NCM) < 8 then
      Erro('O código NCM do produto ' || vItens.PRODUTO_ID || ' - ' || vItens.NOME_PRODUTO || ' não está incorreto, o NCM é composto de 8 digítos, verifique no cadastro de produtos!');
    end if;  
  
    vDadosItens(i).PRODUTO_ID                  := vItens.PRODUTO_ID;
    vDadosItens(i).ITEM_ID                     := vItens.ITEM_ID;
    vDadosItens(i).NOME_PRODUTO                := vItens.NOME_PRODUTO;
    vDadosItens(i).UNIDADE                     := vItens.UNIDADE;
    vDadosItens(i).CST                         := vItens.CST;
    vDadosItens(i).CODIGO_NCM                  := vItens.CODIGO_NCM;
    vDadosItens(i).VALOR_TOTAL                 := vItens.VALOR_TOTAL;
    vDadosItens(i).PRECO_UNITARIO              := vItens.PRECO_UNITARIO;
    vDadosItens(i).QUANTIDADE                  := vItens.QUANTIDADE;
    vDadosItens(i).VALOR_TOTAL_DESCONTO        := vItens.VALOR_TOTAL_DESCONTO;
    vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := vItens.VALOR_TOTAL_OUTRAS_DESPESAS;
    vDadosItens(i).CODIGO_BARRAS               := vItens.CODIGO_BARRAS;
    vDadosItens(i).BASE_CALCULO_ICMS           := vItens.BASE_CALCULO_ICMS;
    vDadosItens(i).INDICE_REDUCAO_BASE_ICMS    := vItens.INDICE_REDUCAO_BASE_ICMS;
    vDadosItens(i).PERCENTUAL_ICMS             := vItens.PERCENTUAL_ICMS;
    vDadosItens(i).VALOR_ICMS                  := vItens.VALOR_ICMS;
    vDadosItens(i).IVA                         := vItens.IVA;
    vDadosItens(i).PRECO_PAUTA                 := vItens.PRECO_PAUTA;

    vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST := vItens.INDICE_REDUCAO_BASE_ICMS_ST;
    vDadosItens(i).BASE_CALCULO_ICMS_ST        := vItens.BASE_CALCULO_ICMS_ST;
    vDadosItens(i).PERCENTUAL_ICMS_ST          := vItens.PERCENTUAL_ICMS_ST;
    vDadosItens(i).VALOR_ICMS_ST               := vItens.VALOR_ICMS_ST;

    vDadosItens(i).BASE_CALCULO_PIS            := vItens.BASE_CALCULO_PIS;
    vDadosItens(i).PERCENTUAL_PIS              := vItens.PERCENTUAL_PIS;
    vDadosItens(i).VALOR_PIS                   := vItens.VALOR_PIS;
    vDadosItens(i).BASE_CALCULO_COFINS         := vItens.BASE_CALCULO_COFINS;
    vDadosItens(i).PERCENTUAL_COFINS           := vItens.PERCENTUAL_COFINS;
    vDadosItens(i).VALOR_COFINS                := vItens.VALOR_COFINS;
    vDadosItens(i).VALOR_IPI                   := vItens.VALOR_IPI;
    vDadosItens(i).PERCENTUAL_IPI              := vItens.PERCENTUAL_IPI;
    vDadosItens(i).CST_PIS                     := vItens.CST_PIS;
    vDadosItens(i).CST_COFINS                  := vItens.CST_COFINS;
    vDadosItens(i).CEST                        := vItens.CEST;

    vDadosItens(i).CFOP_ID                     := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vItens.CST, 'REN', vTipoOperacao);
		vDadosItens(i).NATUREZA_OPERACAO					 := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);

    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */
    vDadosNota.BASE_CALCULO_ICMS    := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;
    vDadosNota.VALOR_ICMS           := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;
    vDadosNota.BASE_CALCULO_ICMS_ST := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;
    vDadosNota.VALOR_ICMS_ST        := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;
    vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;
    vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;
    vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;
    vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;

    vDadosNota.VALOR_TOTAL :=
      vDadosNota.VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
      vDadosItens(i).VALOR_ICMS_ST +
      vDadosItens(i).VALOR_IPI -
      vDadosItens(i).VALOR_TOTAL_DESCONTO;

    vDadosNota.VALOR_TOTAL_PRODUTOS := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;
    vDadosNota.VALOR_DESCONTO := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;
    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;
    /* ------------------------------------------------------------------------------------------------------ */

		if
      vDadosItens(i).VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
      vDadosItens(i).VALOR_ICMS_ST +
      vDadosItens(i).VALOR_IPI -
      vDadosItens(i).VALOR_TOTAL_DESCONTO >
      vValorMaisSignificativo
    then
			vValorMaisSignificativo :=
        vDadosItens(i).VALOR_TOTAL +
        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
        vDadosItens(i).VALOR_ICMS_ST +
        vDadosItens(i).VALOR_IPI -
        vDadosItens(i).VALOR_TOTAL_DESCONTO;

			vCfopIdCapa 	  	      := vDadosItens(i).CFOP_ID;
			vNaturezaOperacao       := vDadosItens(i).NATUREZA_OPERACAO;
		end if;

    ADD_NO_VETOR_SEM_REPETIR(vNotasReferenciasIds, vItens.NOTA_FISCAL_ORIGEM_ID);

    i := i + 1;
  end loop;

  if vDadosItens.count < 1 then
    ERRO('Os produtos da nota fiscal não foram encontrados!');
  end if;

	if vCfopIdCapa is null then
		vCfopIdCapa 			:= vDadosItens(0).CFOP_ID;
		vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;
	end if;

	vDadosNota.CFOP_ID := vCfopIdCapa;
  vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;

  vDadosNota.INFORMACOES_COMPLEMENTARES := 'Retorno referente a entrega ' || iENTREGA_ID;

  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;

	insert into NOTAS_FISCAIS(
    NOTA_FISCAL_ID,
    CADASTRO_ID,
    CFOP_ID,
    EMPRESA_ID,
    ORCAMENTO_BASE_ID,
    ENTREGA_ID,    
		MODELO_NOTA,
		SERIE_NOTA,
    NATUREZA_OPERACAO,
		STATUS,
    TIPO_MOVIMENTO,
    RAZAO_SOCIAL_EMITENTE,
    NOME_FANTASIA_EMITENTE,
    REGIME_TRIBUTARIO,
    CNPJ_EMITENTE,
    INSCRICAO_ESTADUAL_EMITENTE,
    LOGRADOURO_EMITENTE,
    COMPLEMENTO_EMITENTE,
    NOME_BAIRRO_EMITENTE,
    NOME_CIDADE_EMITENTE,
    NUMERO_EMITENTE,
    ESTADO_ID_EMITENTE,
    CEP_EMITENTE,
    TELEFONE_EMITENTE,
    CODIGO_IBGE_MUNICIPIO_EMIT,
    CODIGO_IBGE_ESTADO_EMITENT,
    NOME_FANTASIA_DESTINATARIO,
    RAZAO_SOCIAL_DESTINATARIO,
    TIPO_PESSOA_DESTINATARIO,
    CPF_CNPJ_DESTINATARIO,
    INSCRICAO_ESTADUAL_DESTINAT,
    NOME_CONSUMIDOR_FINAL,
    TELEFONE_CONSUMIDOR_FINAL,
    TIPO_NOTA,
    LOGRADOURO_DESTINATARIO,
    COMPLEMENTO_DESTINATARIO,
    NOME_BAIRRO_DESTINATARIO,
    NOME_CIDADE_DESTINATARIO,
    ESTADO_ID_DESTINATARIO,
    CEP_DESTINATARIO,
    NUMERO_DESTINATARIO,
    CODIGO_IBGE_MUNICIPIO_DEST,
    VALOR_TOTAL,
    VALOR_TOTAL_PRODUTOS,
    VALOR_DESCONTO,
    VALOR_OUTRAS_DESPESAS,
    BASE_CALCULO_ICMS,
    VALOR_ICMS,
    BASE_CALCULO_ICMS_ST,
    VALOR_ICMS_ST,
    BASE_CALCULO_PIS,
    VALOR_PIS,
    BASE_CALCULO_COFINS,
    VALOR_COFINS,
    VALOR_IPI,
    VALOR_FRETE,
    VALOR_SEGURO,
    VALOR_RECEBIDO_DINHEIRO,
    VALOR_RECEBIDO_CARTAO_CRED,
    VALOR_RECEBIDO_CARTAO_DEB,
    VALOR_RECEBIDO_CREDITO,
    VALOR_RECEBIDO_COBRANCA,
    VALOR_RECEBIDO_CHEQUE,
    VALOR_RECEBIDO_FINANCEIRA,
    PESO_LIQUIDO,
    PESO_BRUTO,
		INFORMACOES_COMPLEMENTARES
  )values(
		vNotaFiscalId,
    vDadosNota.CADASTRO_ID,
    vDadosNota.CFOP_ID,
    vDadosNota.EMPRESA_ID,
    vDadosNota.ORCAMENTO_ID,
    iENTREGA_ID,    
		'55',
		vSerieNFe,
    vDadosNota.NATUREZA_OPERACAO,
		'N',
    'REN',
    vDadosNota.RAZAO_SOCIAL_EMITENTE,
    vDadosNota.NOME_FANTASIA_EMITENTE,
    SESSAO.PARAMETROS_EMPRESA_LOGADA.REGIME_TRIBUTARIO,
    vDadosNota.CNPJ_EMITENTE,
    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,
    vDadosNota.LOGRADOURO_EMITENTE,
    vDadosNota.COMPLEMENTO_EMITENTE,
    vDadosNota.NOME_BAIRRO_EMITENTE,
    vDadosNota.NOME_CIDADE_EMITENTE,
    vDadosNota.NUMERO_EMITENTE,
    vDadosNota.ESTADO_ID_EMITENTE,
    vDadosNota.CEP_EMITENTE,
    vDadosNota.TELEFONE_EMITENTE,
    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,
    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,
    vDadosNota.NOME_FANTASIA_DESTINATARIO,
    vDadosNota.RAZAO_SOCIAL_DESTINATARIO,
    vDadosNota.TIPO_PESSOA_DESTINATARIO,
    vDadosNota.CPF_CNPJ_DESTINATARIO,
    vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,
    vDadosNota.NOME_CONSUMIDOR_FINAL,
    vDadosNota.TELEFONE_CONSUMIDOR_FINAL,
    'N',
    vDadosNota.LOGRADOURO_DESTINATARIO,
    vDadosNota.COMPLEMENTO_DESTINATARIO,
    vDadosNota.NOME_BAIRRO_DESTINATARIO,
    vDadosNota.NOME_CIDADE_DESTINATARIO,
    vDadosNota.ESTADO_ID_DESTINATARIO,
    vDadosNota.CEP_DESTINATARIO,
    vDadosNota.NUMERO_DESTINATARIO,
    vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,
    vDadosNota.VALOR_TOTAL,
    vDadosNota.VALOR_TOTAL_PRODUTOS,
    vDadosNota.VALOR_DESCONTO,
    vDadosNota.VALOR_OUTRAS_DESPESAS,
    vDadosNota.BASE_CALCULO_ICMS,
    vDadosNota.VALOR_ICMS,
    vDadosNota.BASE_CALCULO_ICMS_ST,
    vDadosNota.VALOR_ICMS_ST,
    vDadosNota.BASE_CALCULO_PIS,
    vDadosNota.VALOR_PIS,
    vDadosNota.BASE_CALCULO_COFINS,
    vDadosNota.VALOR_COFINS,
    vDadosNota.VALOR_IPI,
    vDadosNota.VALOR_FRETE,
    vDadosNota.VALOR_SEGURO,
    vDadosNota.VALOR_RECEBIDO_DINHEIRO,
    vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,
    vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,
    vDadosNota.VALOR_RECEBIDO_CREDITO,
    vDadosNota.VALOR_RECEBIDO_COBRANCA,
    vDadosNota.VALOR_RECEBIDO_CHEQUE,
    vDadosNota.VALOR_RECEBIDO_FINANCEIRA,
    vDadosNota.PESO_LIQUIDO,
    vDadosNota.PESO_BRUTO,
		vDadosNota.INFORMACOES_COMPLEMENTARES
  );

  /* inserindo a referência */
  for i in vNotasReferenciasIds.first..vNotasReferenciasIds.last loop
    insert into NOTAS_FISCAIS_REFERENCIAS(
      NOTA_FISCAL_ID,
      ITEM_ID,
      TIPO_REFERENCIA,
      NOTA_FISCAL_ID_REFERENCIADA
    )values(
      vNotaFiscalId,
      i,
      'A',
      vNotasReferenciasIds(i)
    );
  end loop;

  for i in 0..vDadosItens.count - 1 loop
    insert into NOTAS_FISCAIS_ITENS(
      NOTA_FISCAL_ID,
      PRODUTO_ID,
      ITEM_ID,
      NOME_PRODUTO,
      UNIDADE,
      CFOP_ID,
      CST,
      CODIGO_NCM,
      VALOR_TOTAL,
      PRECO_UNITARIO,
      QUANTIDADE,
      VALOR_TOTAL_DESCONTO,
      VALOR_TOTAL_OUTRAS_DESPESAS,
      BASE_CALCULO_ICMS,
      PERCENTUAL_ICMS,
      VALOR_ICMS,
      BASE_CALCULO_ICMS_ST,
      VALOR_ICMS_ST,
      CST_PIS,
      BASE_CALCULO_PIS,
      PERCENTUAL_PIS,
      VALOR_PIS,
      CST_COFINS,
      BASE_CALCULO_COFINS,
      PERCENTUAL_COFINS,
      VALOR_COFINS,
      INDICE_REDUCAO_BASE_ICMS,
      IVA,
      PRECO_PAUTA,
      CODIGO_BARRAS,
      VALOR_IPI,
      PERCENTUAL_IPI,
      INDICE_REDUCAO_BASE_ICMS_ST,
      PERCENTUAL_ICMS_ST,
      CEST
    )values(
      vNotaFiscalId,
      vDadosItens(i).PRODUTO_ID,
      vDadosItens(i).ITEM_ID,
      vDadosItens(i).NOME_PRODUTO,
      vDadosItens(i).UNIDADE,
      vDadosItens(i).CFOP_ID,
      vDadosItens(i).CST,
      vDadosItens(i).CODIGO_NCM,
      vDadosItens(i).VALOR_TOTAL,
      vDadosItens(i).PRECO_UNITARIO,
      vDadosItens(i).QUANTIDADE,
      vDadosItens(i).VALOR_TOTAL_DESCONTO,
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,
      vDadosItens(i).BASE_CALCULO_ICMS,
      vDadosItens(i).PERCENTUAL_ICMS,
      vDadosItens(i).VALOR_ICMS,
      vDadosItens(i).BASE_CALCULO_ICMS_ST,
      vDadosItens(i).VALOR_ICMS_ST,
      vDadosItens(i).CST_PIS,
      vDadosItens(i).BASE_CALCULO_PIS,
      vDadosItens(i).PERCENTUAL_PIS,
      vDadosItens(i).VALOR_PIS,
      vDadosItens(i).CST_COFINS,
      vDadosItens(i).BASE_CALCULO_COFINS,
      vDadosItens(i).PERCENTUAL_COFINS,
      vDadosItens(i).VALOR_COFINS,
      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,
      vDadosItens(i).IVA,
      vDadosItens(i).PRECO_PAUTA,
      vDadosItens(i).CODIGO_BARRAS,
      vDadosItens(i).VALOR_IPI,
      vDadosItens(i).PERCENTUAL_IPI,
      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,
      vDadosItens(i).PERCENTUAL_ICMS_ST,
      vDadosItens(i).CEST
    );
  end loop;

end GERAR_NOTA_RETORNO_ENTREGA;
/