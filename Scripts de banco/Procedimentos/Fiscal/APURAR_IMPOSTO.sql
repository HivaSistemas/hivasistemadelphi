create or replace procedure APURAR_IMPOSTO(iNOTA_FISCAL_ID in number)
is
begin

  delete from APURACOES_IMPOSTOS
  where MOVIMENTO_ID = iNOTA_FISCAL_ID
  and TIPO_MOVIMENTO = 'NFI';

  insert into APURACOES_IMPOSTOS(
    MOVIMENTO_ID,
    TIPO_MOVIMENTO,
    EMPRESA_ID,
    CFOP_ID,
    
    DATA_CONTABIL,
    
    VALOR_CONTABIL,
    
    BASE_CALCULO_ICMS,
    VALOR_ICMS,
    
    BASE_CACULO_ICMS_ST,
    VALOR_ICMS_ST,
    
    VALOR_IPI,
    
    BASE_CACULO_PIS,
    VALOR_PIS,
    
    BASE_CACULO_COFINS,
    VALOR_COFINS,
    
    NATUREZA
  )
  select
    iNOTA_FISCAL_ID,
    'NFI',
    NFI.EMPRESA_ID,
    NIT.CFOP_ID,
    
    trunc(NFI.DATA_HORA_EMISSAO),    
    
    sum(NFI.VALOR_TOTAL) as VALOR_CONTABIL,
    sum(NIT.BASE_CALCULO_ICMS) as BASE_CALCULO_ICMS,
    sum(NIT.VALOR_ICMS) as VALOR_ICMS,
    sum(NIT.BASE_CALCULO_ICMS_ST) as BASE_CALCULO_ICMS_ST,
    sum(NIT.VALOR_ICMS_ST) as VALOR_ICMS_ST,
    sum(NIT.VALOR_IPI) as BASE_CALCULO_IPI,
    sum(NIT.BASE_CALCULO_PIS) as BASE_CALCULO_PIS,
    sum(NIT.VALOR_PIS) as VALOR_PIS,
    sum(NIT.BASE_CALCULO_COFINS) as BASE_CALCULO_COFINS,
    sum(NIT.VALOR_COFINS) as VALOR_COFINS,
    
    case when substr(NFI.CFOP_ID, 1, 1) in('1', '2', '3') then 'E' else 'S' end
  from
    NOTAS_FISCAIS_ITENS NIT

  inner join NOTAS_FISCAIS NFI
  on NIT.NOTA_FISCAL_ID = NFI.NOTA_FISCAL_ID

  where NFI.STATUS in('C', 'E')
  and NFI.NOTA_FISCAL_ID = iNOTA_FISCAL_ID

  group by
    NIT.CFOP_ID,
    NFI.EMPRESA_ID,
    trunc(NFI.DATA_HORA_EMISSAO),
    case when substr(NFI.CFOP_ID, 1, 1) in('1', '2', '3') then 'E' else 'S' end    

  order by
    NIT.CFOP_ID;

end APURAR_IMPOSTO;
/