﻿create or replace procedure GERAR_NFE_DE_NFCE(
  iNOTA_FISCAL_ID in number,
  oNOTA_FISCAL_ID out number
)
is
	i                 number default 0;
  vSerieNFe         NOTAS_FISCAIS.SERIE_NOTA%type;
	vDadosNota        NOTAS_FISCAIS%rowtype;
	vNotaFiscalId     NOTAS_FISCAIS.NOTA_FISCAL_ID%type;
  vRegimeTributario NOTAS_FISCAIS.REGIME_TRIBUTARIO%type;
	
	vQtde 					  number;
	
	type ArrayOfDadosNotaItens is table of NOTAS_FISCAIS_ITENS%rowtype index by PLS_INTEGER;
	vDadosItens 		 ArrayOfDadosNotaItens;
	
begin

	select count(*)
	into vQtde
	from NOTAS_FISCAIS
	where NOTA_FISCAL_ORIGEM_CUPOM_ID = iNOTA_FISCAL_ID;
	
	if vQtde > 0 then
		ERRO('Já foi emitida uma nota fiscal eletrônica para este cupom fiscal!');
	end if;

	select *
	into vDadosNota
	from NOTAS_FISCAIS
	where NOTA_FISCAL_ID = iNOTA_FISCAL_ID;
		
	if vDadosNota.TIPO_NOTA <> 'C' then
		ERRO('Está nota não é um NFCe!');
	end if;
	
	if vDadosNota.CADASTRO_ID = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then
		ERRO('Não é possível gerar uma nota fiscal eletrônica de um consumidor final!');
	end if;
	
	if vDadosNota.STATUS = 'N' then
		ERRO('Está nota ainda não emitida!');
	elsif vDadosNota.STATUS = 'C' then
		ERRO('Está nota está cancelada!');
	end if;
  
	for vItens in (select * from NOTAS_FISCAIS_ITENS where NOTA_FISCAL_ID = iNOTA_FISCAL_ID) loop
		vDadosItens(i) := vItens;
    i := i + 1;
	end loop;
	
	if vDadosItens.count = 0 then
		ERRO('Os produtos da NFCe não foram encontrados!');
	end if;  
  
  select
    SERIE_NFE,
    REGIME_TRIBUTARIO
  into
    vSerieNFe,
    vRegimeTributario
  from
    PARAMETROS_EMPRESA
  where EMPRESA_ID = vDadosNota.EMPRESA_ID;
  
  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;

	insert into NOTAS_FISCAIS(
    NOTA_FISCAL_ID,
    CADASTRO_ID,
    CFOP_ID,
    EMPRESA_ID,    
		MODELO_NOTA,
		SERIE_NOTA,
    NATUREZA_OPERACAO,
		STATUS,
    TIPO_MOVIMENTO,
    RAZAO_SOCIAL_EMITENTE,
    NOME_FANTASIA_EMITENTE,
    REGIME_TRIBUTARIO,
    CNPJ_EMITENTE,
    INSCRICAO_ESTADUAL_EMITENTE,
    LOGRADOURO_EMITENTE,
    COMPLEMENTO_EMITENTE,
    NOME_BAIRRO_EMITENTE,
    NOME_CIDADE_EMITENTE,
    NUMERO_EMITENTE,
    ESTADO_ID_EMITENTE,
    CEP_EMITENTE,
    TELEFONE_EMITENTE,
    CODIGO_IBGE_MUNICIPIO_EMIT,
    CODIGO_IBGE_ESTADO_EMITENT,
    NOME_FANTASIA_DESTINATARIO,
    RAZAO_SOCIAL_DESTINATARIO,
    TIPO_PESSOA_DESTINATARIO,
    CPF_CNPJ_DESTINATARIO,
    INSCRICAO_ESTADUAL_DESTINAT,
    NOME_CONSUMIDOR_FINAL,
    TELEFONE_CONSUMIDOR_FINAL,
    TIPO_NOTA,
    LOGRADOURO_DESTINATARIO,
    COMPLEMENTO_DESTINATARIO,
    NOME_BAIRRO_DESTINATARIO,
    NOME_CIDADE_DESTINATARIO,
    ESTADO_ID_DESTINATARIO,
    CEP_DESTINATARIO,
    NUMERO_DESTINATARIO,
    CODIGO_IBGE_MUNICIPIO_DEST,
    VALOR_TOTAL,
    VALOR_TOTAL_PRODUTOS,
    VALOR_DESCONTO,
    VALOR_OUTRAS_DESPESAS,
    BASE_CALCULO_ICMS,
    VALOR_ICMS,
    BASE_CALCULO_ICMS_ST,
    VALOR_ICMS_ST,
    BASE_CALCULO_PIS,
    VALOR_PIS,
    BASE_CALCULO_COFINS,
    VALOR_COFINS,
    VALOR_IPI,
    VALOR_FRETE,
    VALOR_SEGURO,
    VALOR_RECEBIDO_DINHEIRO,
    VALOR_RECEBIDO_PIX,
    VALOR_RECEBIDO_CARTAO_CRED,
    VALOR_RECEBIDO_CARTAO_DEB,
    VALOR_RECEBIDO_CREDITO,
    VALOR_RECEBIDO_COBRANCA,
    VALOR_RECEBIDO_CHEQUE,
    VALOR_RECEBIDO_FINANCEIRA,
    PESO_LIQUIDO,
    PESO_BRUTO,
		INFORMACOES_COMPLEMENTARES,
    NOTA_FISCAL_ORIGEM_CUPOM_ID
  )values(
    vNotaFiscalId,
    vDadosNota.CADASTRO_ID,
    case when substr(to_char(vDadosNota.CFOP_ID), 1, 1) = '5' then '5929-0' else '6929-0' end,
    vDadosNota.EMPRESA_ID,    
		'55',
		vSerieNFe,
    'LANCAMENTO DEC. VENDA COM EMISSAO DE DOC. FISCAL NFCE',
		'N',
    'NNE',
    vDadosNota.RAZAO_SOCIAL_EMITENTE,
    vDadosNota.NOME_FANTASIA_EMITENTE,
    vRegimeTributario,
    vDadosNota.CNPJ_EMITENTE,
    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,
    vDadosNota.LOGRADOURO_EMITENTE,
    vDadosNota.COMPLEMENTO_EMITENTE,
    vDadosNota.NOME_BAIRRO_EMITENTE,
    vDadosNota.NOME_CIDADE_EMITENTE,
    vDadosNota.NUMERO_EMITENTE,
    vDadosNota.ESTADO_ID_EMITENTE,
    vDadosNota.CEP_EMITENTE,
    vDadosNota.TELEFONE_EMITENTE,
    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,
    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,
    vDadosNota.NOME_FANTASIA_DESTINATARIO,
    vDadosNota.RAZAO_SOCIAL_DESTINATARIO,
    vDadosNota.TIPO_PESSOA_DESTINATARIO,
    vDadosNota.CPF_CNPJ_DESTINATARIO,
    vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,
    vDadosNota.NOME_CONSUMIDOR_FINAL,
    vDadosNota.TELEFONE_CONSUMIDOR_FINAL,
    'N',
    vDadosNota.LOGRADOURO_DESTINATARIO,
    vDadosNota.COMPLEMENTO_DESTINATARIO,
    vDadosNota.NOME_BAIRRO_DESTINATARIO,
    vDadosNota.NOME_CIDADE_DESTINATARIO,
    vDadosNota.ESTADO_ID_DESTINATARIO,
    vDadosNota.CEP_DESTINATARIO,
    vDadosNota.NUMERO_DESTINATARIO,
    vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,
    vDadosNota.VALOR_TOTAL,
    vDadosNota.VALOR_TOTAL_PRODUTOS,
    vDadosNota.VALOR_DESCONTO,
    vDadosNota.VALOR_OUTRAS_DESPESAS,
    0, --BASE_CALCULO_ICMS,
    0, --VALOR_ICMS,
    0, --BASE_CALCULO_ICMS_ST,
    0, --VALOR_ICMS_ST,
    0, --BASE_CALCULO_PIS,
    0, --VALOR_PIS,
    0, --BASE_CALCULO_COFINS,
    0, --VALOR_COFINS,
    0, --VALOR_IPI,
    vDadosNota.VALOR_FRETE,
    vDadosNota.VALOR_SEGURO,
    vDadosNota.VALOR_RECEBIDO_DINHEIRO,
    vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,
    vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,
    vDadosNota.VALOR_RECEBIDO_CREDITO,
    vDadosNota.VALOR_RECEBIDO_COBRANCA,
    vDadosNota.VALOR_RECEBIDO_CHEQUE,
    vDadosNota.VALOR_RECEBIDO_FINANCEIRA,
    vDadosNota.PESO_LIQUIDO,
    vDadosNota.PESO_BRUTO,
		'REF. NFCE ' || vDadosNota.NUMERO_NOTA || ' EMITIDA EM ' || vDadosNota.DATA_HORA_EMISSAO,
    vDadosNota.NOTA_FISCAL_ID
  );  
  
  for i in 0..vDadosItens.count - 1 loop
    insert into NOTAS_FISCAIS_ITENS(
      NOTA_FISCAL_ID,
      PRODUTO_ID,
      ITEM_ID,
      NOME_PRODUTO,
      UNIDADE,
      CFOP_ID,
      CST,
      CODIGO_NCM,
      VALOR_TOTAL,
      PRECO_UNITARIO,
      QUANTIDADE,
      VALOR_TOTAL_DESCONTO,
      VALOR_TOTAL_OUTRAS_DESPESAS,
      BASE_CALCULO_ICMS,
      PERCENTUAL_ICMS,
      VALOR_ICMS,
      BASE_CALCULO_ICMS_ST,
      VALOR_ICMS_ST,
      CST_PIS,
      BASE_CALCULO_PIS,
      PERCENTUAL_PIS,
      VALOR_PIS,
      CST_COFINS,
      BASE_CALCULO_COFINS,
      PERCENTUAL_COFINS,
      VALOR_COFINS,
      INDICE_REDUCAO_BASE_ICMS,
      IVA,
      PRECO_PAUTA,
      CODIGO_BARRAS,
      VALOR_IPI,
      PERCENTUAL_IPI,
      INDICE_REDUCAO_BASE_ICMS_ST,
      PERCENTUAL_ICMS_ST,
      CEST
    )values(
      vNotaFiscalId,
      vDadosItens(i).PRODUTO_ID,
      vDadosItens(i).ITEM_ID,
      vDadosItens(i).NOME_PRODUTO,
      vDadosItens(i).UNIDADE,
      vDadosItens(i).CFOP_ID,
      vDadosItens(i).CST,
      vDadosItens(i).CODIGO_NCM,
      vDadosItens(i).VALOR_TOTAL,
      vDadosItens(i).PRECO_UNITARIO,
      vDadosItens(i).QUANTIDADE,
      vDadosItens(i).VALOR_TOTAL_DESCONTO,
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,
      0, --BASE_CALCULO_ICMS,
      0, --PERCENTUAL_ICMS,
      0, --VALOR_ICMS,
      0, -- BASE_CALCULO_ICMS_ST,
      0, -- VALOR_ICMS_ST,
      vDadosItens(i).CST_PIS,
      0, --BASE_CALCULO_PIS,
      0, --PERCENTUAL_PIS,
      0, --VALOR_PIS,
      vDadosItens(i).CST_COFINS,
      0, --BASE_CALCULO_COFINS,
      0, --PERCENTUAL_COFINS,
      0, --VALOR_COFINS,
      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,
      vDadosItens(i).IVA,
      0, --PRECO_PAUTA,
      vDadosItens(i).CODIGO_BARRAS,
      0, --VALOR_IPI,
      0, --PERCENTUAL_IPI,
      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,
      0, --PERCENTUAL_ICMS_ST,
      vDadosItens(i).CEST
    );
  end loop;

  oNOTA_FISCAL_ID := vNotaFiscalId;

end GERAR_NFE_DE_NFCE;
/