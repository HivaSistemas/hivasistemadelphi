create or replace procedure BUSCAR_CALC_IMPOSTOS_PROD_CST(
  iPRODUTO_ID          in number, -- 0
  iEMPRESA_ID          in number, -- 1
  iCST                 in string, -- 2
  iESTADO_DESTINO_ID   in string, -- 3
  iVALOR_TOTAL_PRODUTO in number, -- 4
  iVALOR_OUTRAS_DESP   in number, -- 5
  iVALOR_DESCONTO      in number, -- 6 

  oCST                       out string, -- 7
  oBASE_CALCULO_ICMS         out number, -- 8
  oINDICE_REDUCAO_BASE_ICMS  out number, -- 9
  oPERCENTUAL_ICMS           out number, -- 10
  oVALOR_ICMS                out number, -- 11

  oINDICE_REDUCAO_BC_ICMS_ST out number, -- 12
  oBASE_CALCULO_ICMS_ST      out number, -- 13
  oPERCENTUAL_ICMS_ST        out number, -- 14
  oVALOR_ICMS_ST             out number, -- 15

  oPRECO_PAUTA               out number, -- 16
  oIVA                       out number, -- 17

  oVALOR_IPI                 out number, -- 18
  oPERCENTUAL_IPI            out number, -- 19

  oCST_PIS                   out string, -- 20
  oBASE_CALCULO_PIS          out number, -- 21
  oPERCENTUAL_PIS            out number, -- 22
  oVALOR_PIS                 out number, -- 23

  oCST_COFINS                out string, -- 24
  oBASE_CALCULO_COFINS       out number, -- 25
  oPERCENTUAL_COFINS         out number, -- 26
  oVALOR_COFINS              out number  -- 27
)
is

  type RecDadosProduto is record(
    CST                      GRUPOS_TRIB_ESTADUAL_VENDA_EST.CST_NAO_CONTRIBUINTE%type,
    PERCENTUAL_ICMS          GRUPOS_TRIB_ESTADUAL_VENDA_EST.PERCENTUAL_ICMS%type,
    PERCENTUAL_ICMS_INTER    GRUPOS_TRIB_ESTADUAL_VENDA_EST.PERCENTUAL_ICMS_INTER%type,
    INDICE_REDUCAO_BASE_ICMS GRUPOS_TRIB_ESTADUAL_VENDA_EST.INDICE_REDUCAO_BASE_ICMS%type,
    IVA                      GRUPOS_TRIB_ESTADUAL_VENDA_EST.IVA%type,
    PERCENTUAL_PIS           PERCENTUAIS_CUSTOS_EMPRESA.PERCENTUAL_PIS%type,
    CST_PIS                  GRUPOS_TRIB_FEDERAL_VENDA.CST_PIS%type,
    PERCENTUAL_COFINS        PERCENTUAIS_CUSTOS_EMPRESA.PERCENTUAL_COFINS%type,
    CST_COFINS               GRUPOS_TRIB_FEDERAL_VENDA.CST_COFINS%type
  );

  vDadosProduto    RecDadosProduto;
  vImpostos        RecordsNotasFiscais.RecImpostosCalculados;
begin
 
  select
    iCST,
    PRI.PERC_ICMS_VENDA,
    PRI.INDICE_REDUCAO_BASE_ICMS_VENDA,
    PRI.IVA_VENDA,
    PAE.ALIQUOTA_PIS,
    GTF.CST_PIS_VENDA as CST_PIS,
    PAE.ALIQUOTA_COFINS,
    GTF.CST_COFINS_VENDA as CST_COFINS
  into
    vDadosProduto.CST,
    vDadosProduto.PERCENTUAL_ICMS,
    vDadosProduto.INDICE_REDUCAO_BASE_ICMS,
    vDadosProduto.IVA,
    vDadosProduto.PERCENTUAL_PIS,
    vDadosProduto.CST_PIS,
    vDadosProduto.PERCENTUAL_COFINS,
    vDadosProduto.CST_COFINS
  from
    VW_GRUPOS_TRIBUTACOES_ESTADUAL PRI

  inner join PRODUTOS PRO
  on PRI.PRODUTO_ID = PRO.PRODUTO_ID

  inner join PARAMETROS_EMPRESA PAE
  on PAE.EMPRESA_ID = iEMPRESA_ID

  inner join VW_GRUPOS_TRIBUTACOES_FEDERAL GTF
  on PRO.PRODUTO_ID = GTF.PRODUTO_ID
  and GTF.EMPRESA_ID = iEMPRESA_ID

  where PRI.PRODUTO_ID = iPRODUTO_ID
  and PRI.EMPRESA_ID = iEMPRESA_ID
  and PRI.ESTADO_ID_COMPRA = iESTADO_DESTINO_ID
  and PRI.ESTADO_ID_VENDA = iESTADO_DESTINO_ID;
 
  vImpostos :=
    BUSCAR_CALC_IMPOSTOS_NOTA(
      iEMPRESA_ID,
      iVALOR_TOTAL_PRODUTO,
      iVALOR_OUTRAS_DESP,
      iVALOR_DESCONTO,
      vDadosProduto.CST,
      vDadosProduto.PERCENTUAL_ICMS,
      vDadosProduto.INDICE_REDUCAO_BASE_ICMS,
      vDadosProduto.IVA,
      vDadosProduto.PERCENTUAL_PIS,
      vDadosProduto.CST_PIS,
      vDadosProduto.PERCENTUAL_COFINS,
      vDadosProduto.CST_COFINS,
      vDadosProduto.PERCENTUAL_ICMS_INTER
    );

  oCST                       := vDadosProduto.CST;

  oBASE_CALCULO_ICMS         := vImpostos.BASE_CALCULO_ICMS;
  oINDICE_REDUCAO_BASE_ICMS  := vImpostos.INDICE_REDUCAO_BASE_ICMS;
  oPERCENTUAL_ICMS           := vImpostos.PERCENTUAL_ICMS;
  oVALOR_ICMS                := vImpostos.VALOR_ICMS;

  oINDICE_REDUCAO_BC_ICMS_ST := vImpostos.INDICE_REDUCAO_BASE_ICMS_ST;
  oBASE_CALCULO_ICMS_ST      := vImpostos.BASE_CALCULO_ICMS_ST;
  oPERCENTUAL_ICMS_ST        := vImpostos.PERCENTUAL_ICMS_ST;
  oVALOR_ICMS_ST             := vImpostos.VALOR_ICMS_ST;
  oIVA                       := vImpostos.IVA;

  oVALOR_IPI                 := vImpostos.VALOR_IPI;
  oPERCENTUAL_IPI            := vImpostos.PERCENTUAL_IPI;
  oPRECO_PAUTA               := vImpostos.PRECO_PAUTA;

  oCST_PIS                   := vDadosProduto.CST_PIS;
  oBASE_CALCULO_PIS          := vImpostos.BASE_CALCULO_PIS;
  oPERCENTUAL_PIS            := vImpostos.PERCENTUAL_PIS;
  oVALOR_PIS                 := vImpostos.VALOR_PIS;

  oCST_COFINS                := vDadosProduto.CST_COFINS;
  oBASE_CALCULO_COFINS       := vImpostos.BASE_CALCULO_COFINS;
  oPERCENTUAL_COFINS         := vImpostos.PERCENTUAL_COFINS;
  oVALOR_COFINS              := vImpostos.VALOR_COFINS;

end BUSCAR_CALC_IMPOSTOS_PROD_CST;
/