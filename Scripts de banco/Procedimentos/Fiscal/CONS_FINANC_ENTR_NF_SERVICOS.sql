create or replace procedure CONS_FINANC_ENTR_NF_SERVICOS(iENTRADA_ID in number)
is

  vPagarId               CONTAS_PAGAR.PAGAR_ID%type;
  vFornecedorId          CONTAS_PAGAR.CADASTRO_ID%type;
  vEmpresaId             CONTAS_PAGAR.EMPRESA_ID%type;
  vPlanoFinanceiroId     CONTAS_PAGAR.PLANO_FINANCEIRO_ID%type;
  vCentroCustoId         CONTAS_PAGAR.CENTRO_CUSTO_ID%type;  
  vQtde                  number;

  cursor cTitulos is
  select
    TIT.COBRANCA_ID,
    TIT.CODIGO_BARRAS,
    TIT.NOSSO_NUMERO,
    TIT.DOCUMENTO,
    TIT.PARCELA,
    TIT.NUMERO_PARCELAS,
    TIT.VALOR_DOCUMENTO,
    TCO.PORTADOR_ID,    
    TIT.DATA_VENCIMENTO    
  from
    ENTRADAS_NF_SERVICOS_FINANC TIT
    
  inner join TIPOS_COBRANCA TCO
  on TIT.COBRANCA_ID = TCO.COBRANCA_ID
  
  where TIT.ENTRADA_ID = iENTRADA_ID;
  
begin

  select
    FORNECEDOR_ID,
    EMPRESA_ID,
    PLANO_FINANCEIRO_ID,    
    nvl(CENTRO_CUSTO_ID, 1)
  into
    vFornecedorId,
    vEmpresaId,
    vPlanoFinanceiroId,    
    vCentroCustoId
  from
    ENTRADAS_NOTAS_FISC_SERVICOS
  where ENTRADA_ID = iENTRADA_ID;

  for xTitulos in cTitulos loop
  
    select SEQ_PAGAR_ID.nextval 
    into vPagarId 
    from dual;

    insert into CONTAS_PAGAR(
      PAGAR_ID,
      CADASTRO_ID,
      EMPRESA_ID,
      DOCUMENTO,
      ORIGEM,      
      ENTRADA_SERVICO_ID,
      COBRANCA_ID,
      PORTADOR_ID,
      PLANO_FINANCEIRO_ID,
      DATA_VENCIMENTO,
      DATA_VENCIMENTO_ORIGINAL,
      VALOR_DOCUMENTO,
      STATUS,      
      PARCELA,
      NUMERO_PARCELAS,
      CENTRO_CUSTO_ID
    )values(
      vPagarId,
      vFornecedorId,
      vEmpresaId,      
      xTitulos.DOCUMENTO,
      'ENS', -- Tipo de movimento
      iENTRADA_ID,
      xTitulos.COBRANCA_ID, -- Tipo de cobrança previsão
      xTitulos.PORTADOR_ID,
      vPlanoFinanceiroId,
      xTitulos.DATA_VENCIMENTO,
      xTitulos.DATA_VENCIMENTO,
      xTitulos.VALOR_DOCUMENTO, -- Valor do financeiro
      'A',      
      xTitulos.PARCELA,
      xTitulos.NUMERO_PARCELAS,
      vCentroCustoId
    );  
  
  end loop;
  
end CONS_FINANC_ENTR_NF_SERVICOS;
/