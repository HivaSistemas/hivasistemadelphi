create or replace procedure DESCONS_FINANC_ENT_NOTA_FISCAL(iENTRADA_ID in number)
is  
  vStatus               ENTRADAS_NOTAS_FISCAIS.STATUS%type;  
  vConhecimentoFreteId  ENTRADAS_NOTAS_FISCAIS.CONHECIMENTO_FRETE_ID%type;
  vQtde                 number;
begin

  select
    STATUS,
    CONHECIMENTO_FRETE_ID
  into
    vStatus,
    vConhecimentoFreteId
  from
    ENTRADAS_NOTAS_FISCAIS
  where ENTRADA_ID = iENTRADA_ID;

  if vStatus <> 'BAI' then
    ERRO('Somente entrada com o financeiro consolidado pode ter o financeiro desconsolidado!');
  end if;

  select
    count(*)
  into
    vQtde
  from
    CONTAS_PAGAR_BAIXAS_ITENS
  where PAGAR_ID in(
    select
      PAGAR_ID
    from
      CONTAS_PAGAR
    where ENTRADA_ID = iENTRADA_ID
  );

  if vQtde > 0 then
    ERRO('Existem contas a pagar gerados por esta entrada que est�o baixados, por favor cancele as baixas antes de realizar esta opera��o!');
  end if;

  delete from LOGS_CONTAS_PAGAR
  where PAGAR_ID in(
    select
      PAGAR_ID
    from
      CONTAS_PAGAR
    where CONHECIMENTO_FRETE_ID = vConhecimentoFreteId
  );

  delete from LOGS_CONTAS_PAGAR
  where PAGAR_ID in(
    select
      PAGAR_ID
    from
      CONTAS_PAGAR
    where ENTRADA_ID = iENTRADA_ID
  );

  delete from CONTAS_PAGAR
  where CONHECIMENTO_FRETE_ID = vConhecimentoFreteId;

  delete from CONTAS_PAGAR
  where ENTRADA_ID = iENTRADA_ID;
  
  update ENTRADAS_NOTAS_FISCAIS set
    STATUS = 'ECO'
  where ENTRADA_ID = iENTRADA_ID;
  
end DESCONS_FINANC_ENT_NOTA_FISCAL;
/