create or replace procedure VOLTAR_CUSTOS_ENTRADA(iENTRADA_ID in number)
is

  vUltimaEntradaId       ENTRADAS_NOTAS_FISCAIS.ENTRADA_ID%type;
  vEmpresaId             ENTRADAS_NOTAS_FISCAIS.EMPRESA_ID%type;
  vAtualizarCustoEntrada ENTRADAS_NOTAS_FISCAIS.ATUALIZAR_CUSTO_ENTRADA%type;

  cursor cItens is
  select
    ITE.PRODUTO_ID,
    CFO.ENTRADA_MERCADORIAS_BONIFIC,
    ITE.QUANTIDADE_ENTRADA_ALTIS as QUANTIDADE,
    ITE.ENTRADA_ANTERIOR_ID,
    ITE.PRECO_TABELA_ANTERIOR,
    ITE.PERCENTUAL_DESCONTO_ENTR_ANT,
    ITE.PERCENTUAL_IPI_ENTRADA_ANT,
    ITE.PERCENTUAL_OUTRAS_DESP_ENT_ANT,
    ITE.PERCENTUAL_FRETE_ENTR_ANTERIOR,
    ITE.PERCENTUAL_ST_ENTRADA_ANTERIOR,
    ITE.PERCENTUAL_ICMS_ENTR_ANTERIOR,
    ITE.PERCENTUAL_ICMS_FRETE_ENTR_ANT,
    ITE.PERCENTUAL_PIS_ENTRADA_ANTER,
    ITE.PERCENTUAL_COFINS_ENT_ANTERIOR,
    /*PERCENTUAL_PIS_FRETE_ENT_ANT    = vFormacaoCusto.PRECO_TABELA,
    PERCENTUAL_COF_FRETE_ENT_ANT    = vFormacaoCusto.PRECO_TABELA,*/
    ITE.PERCENTUAL_OUT_CUS_ENTR_ANTER,
    --PERCENTUAL_CUSTO_FIXO_ENT_ANT   = vFormacaoCusto.PERC_IPI,
    ITE.PERCENTUAL_DIFAL_ENTRADA_ANTER,
    ITE.PRECO_FINAL,
    ITE.PRECO_FINAL_ANTERIOR,
    ITE.PRECO_FINAL_MEDIO_ANTERIOR,
    ITE.PRECO_LIQUIDO_ANTERIOR,
    ITE.PRECO_LIQUIDO_MEDIO_ANTERIOR,
    ITE.CMV_ANTERIOR
  from
    ENTRADAS_NOTAS_FISCAIS_ITENS ITE

  inner join CFOP CFO
  on ITE.CFOP_ID = CFO.CFOP_PESQUISA_ID

  where ITE.ENTRADA_ID = iENTRADA_ID;

begin

  select
    EMPRESA_ID,
    ATUALIZAR_CUSTO_ENTRADA
  into
    vEmpresaId,
    vAtualizarCustoEntrada
  from
    ENTRADAS_NOTAS_FISCAIS
  where ENTRADA_ID = iENTRADA_ID;

  /* Se n�o atualizou custos, saindo do procedimento */
  if vAtualizarCustoEntrada = 'N' then
    return;
  end if;

  for xItens in cItens loop
    select
      ULTIMA_ENTRADA_ID
    into
      vUltimaEntradaId
    from
      ESTOQUES
    where EMPRESA_ID = vEmpresaId
    and PRODUTO_ID = xItens.PRODUTO_ID;

    if xItens.ENTRADA_MERCADORIAS_BONIFIC = 'S' then
      ATUALIZAR_CMV(vEmpresaId, xItens.PRODUTO_ID, 0, xItens.QUANTIDADE * -1);
      continue;
    else
      ATUALIZAR_CMV(vEmpresaId, xItens.PRODUTO_ID, xItens.PRECO_FINAL, xItens.QUANTIDADE * -1);
    end if;
           
    /* Se n�o for a ultima entrada apenas voltando o CMV */
    if vUltimaEntradaId <> iENTRADA_ID then      
      continue;
    end if;
    
    update CUSTOS_PRODUTOS set
      PRECO_TABELA                   = xItens.PRECO_TABELA_ANTERIOR,
      PERCENTUAL_ICMS_ENTRADA        = xItens.PERCENTUAL_ICMS_ENTR_ANTERIOR,
      PERCENTUAL_ST_ENTRADA          = xItens.PERCENTUAL_ST_ENTRADA_ANTERIOR,
      PERCENTUAL_FRETE_ENTRADA       = xItens.PERCENTUAL_FRETE_ENTR_ANTERIOR,
      PERCENTUAL_ICMS_FRETE_ENTRADA  = xItens.PERCENTUAL_ICMS_FRETE_ENTR_ANT,
      PERCENTUAL_PIS_ENTRADA         = xItens.PERCENTUAL_PIS_ENTRADA_ANTER,
      PERCENTUAL_COFINS_ENTRADA      = xItens.PERCENTUAL_COFINS_ENT_ANTERIOR,
      PERCENTUAL_DESCONTO_ENTRADA    = xItens.PERCENTUAL_DESCONTO_ENTR_ANT,
      PERCENTUAL_OUTRAS_DESPESAS_ENT = xItens.PERCENTUAL_OUTRAS_DESP_ENT_ANT,
      PERCENTUAL_OUTROS_CUSTOS_ENTR  = xItens.PERCENTUAL_OUT_CUS_ENTR_ANTER,
      PERCENTUAL_IPI_ENTRADA         = xItens.PERCENTUAL_IPI_ENTRADA_ANT,
      PERCENTUAL_DIFAL_ENTRADA       = xItens.PERCENTUAL_DIFAL_ENTRADA_ANTER,
    /*PERCENTUAL_PIS_FRETE_ENT_ANT    = PRECO_TABELA,
      PERCENTUAL_COF_FRETE_ENT_ANT    = PRECO_TABELA,*/
      
      PRECO_FINAL                    = xItens.PRECO_FINAL_ANTERIOR,
      PRECO_FINAL_MEDIO              = xItens.PRECO_FINAL_MEDIO_ANTERIOR,      

      PRECO_LIQUIDO                  = xItens.PRECO_LIQUIDO_ANTERIOR,
      PRECO_LIQUIDO_MEDIO            = xItens.PRECO_LIQUIDO_MEDIO_ANTERIOR,

      ORIGEM                         = case when xItens.ENTRADA_ANTERIOR_ID is null then 'NEN' else 'ENT' end,
      ORIGEM_ID                      = xItens.ENTRADA_ANTERIOR_ID
    where ORIGEM = 'ENT'
    and ORIGEM_ID = iENTRADA_ID
    and EMPRESA_ID = vEmpresaId
    and PRODUTO_ID = xItens.PRODUTO_ID;

  end loop;


  for xGrupoPrecificacao in (select EMPRESA_GRUPO_ID from GRUPOS_PRECIFICACAO where EMPRESA_ID = vEmpresaId) loop

    if xGrupoPrecificacao.EMPRESA_GRUPO_ID <> vEmpresaId then

      for xItens in cItens loop
        select
          ULTIMA_ENTRADA_ID
        into
          vUltimaEntradaId
        from
          ESTOQUES
        where EMPRESA_ID = xGrupoPrecificacao.EMPRESA_GRUPO_ID
        and PRODUTO_ID = xItens.PRODUTO_ID;

        if xItens.ENTRADA_MERCADORIAS_BONIFIC = 'S' then
          ATUALIZAR_CMV(xGrupoPrecificacao.EMPRESA_GRUPO_ID, xItens.PRODUTO_ID, 0, xItens.QUANTIDADE * -1);
          continue;
        else
          ATUALIZAR_CMV(xGrupoPrecificacao.EMPRESA_GRUPO_ID, xItens.PRODUTO_ID, xItens.PRECO_FINAL, xItens.QUANTIDADE * -1);
        end if;

        /* Se n�o for a ultima entrada apenas voltando o CMV */
        if vUltimaEntradaId <> iENTRADA_ID then
          continue;
        end if;

        update CUSTOS_PRODUTOS set
          PRECO_TABELA                   = xItens.PRECO_TABELA_ANTERIOR,
          PERCENTUAL_ICMS_ENTRADA        = xItens.PERCENTUAL_ICMS_ENTR_ANTERIOR,
          PERCENTUAL_ST_ENTRADA          = xItens.PERCENTUAL_ST_ENTRADA_ANTERIOR,
          PERCENTUAL_FRETE_ENTRADA       = xItens.PERCENTUAL_FRETE_ENTR_ANTERIOR,
          PERCENTUAL_ICMS_FRETE_ENTRADA  = xItens.PERCENTUAL_ICMS_FRETE_ENTR_ANT,
          PERCENTUAL_PIS_ENTRADA         = xItens.PERCENTUAL_PIS_ENTRADA_ANTER,
          PERCENTUAL_COFINS_ENTRADA      = xItens.PERCENTUAL_COFINS_ENT_ANTERIOR,
          PERCENTUAL_DESCONTO_ENTRADA    = xItens.PERCENTUAL_DESCONTO_ENTR_ANT,
          PERCENTUAL_OUTRAS_DESPESAS_ENT = xItens.PERCENTUAL_OUTRAS_DESP_ENT_ANT,
          PERCENTUAL_OUTROS_CUSTOS_ENTR  = xItens.PERCENTUAL_OUT_CUS_ENTR_ANTER,
          PERCENTUAL_IPI_ENTRADA         = xItens.PERCENTUAL_IPI_ENTRADA_ANT,
          PERCENTUAL_DIFAL_ENTRADA       = xItens.PERCENTUAL_DIFAL_ENTRADA_ANTER,
        /*PERCENTUAL_PIS_FRETE_ENT_ANT    = PRECO_TABELA,
          PERCENTUAL_COF_FRETE_ENT_ANT    = PRECO_TABELA,*/

          PRECO_FINAL                    = xItens.PRECO_FINAL_ANTERIOR,
          PRECO_FINAL_MEDIO              = xItens.PRECO_FINAL_MEDIO_ANTERIOR,

          PRECO_LIQUIDO                  = xItens.PRECO_LIQUIDO_ANTERIOR,
          PRECO_LIQUIDO_MEDIO            = xItens.PRECO_LIQUIDO_MEDIO_ANTERIOR,

          ORIGEM                         = case when xItens.ENTRADA_ANTERIOR_ID is null then 'NEN' else 'ENT' end,
          ORIGEM_ID                      = xItens.ENTRADA_ANTERIOR_ID
        where ORIGEM = 'ENT'
        and ORIGEM_ID = iENTRADA_ID
        and EMPRESA_ID = xGrupoPrecificacao.EMPRESA_GRUPO_ID
        and PRODUTO_ID = xItens.PRODUTO_ID;

      end loop;

    end if;

  end loop;
  
end VOLTAR_CUSTOS_ENTRADA;
/