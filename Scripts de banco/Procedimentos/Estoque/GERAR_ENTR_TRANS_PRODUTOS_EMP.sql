﻿create or replace procedure GERAR_ENTR_TRANS_PRODUTOS_EMP(iMOVIMENTO_ID in number, iTIPO_MOVIMENTO in string)
is
  vNotaFiscalSaidaId NOTAS_FISCAIS.NOTA_FISCAL_ID%type;
  vEntradaId         ENTRADAS_NOTAS_FISCAIS.ENTRADA_ID%type;
  vStatus            NOTAS_FISCAIS.STATUS%type;
  vChaveNFe          ENTRADAS_NOTAS_FISCAIS.CHAVE_NFE%type;
  vFornecedorId      FORNECEDORES.CADASTRO_ID%type;
  vQtde              number;
begin

  if iTIPO_MOVIMENTO not in('TPE', 'TFP') then
    ERRO('O tipo de movimento só pode ser "TPE" ou "TFP"!');
  end if;

  select
    NFI.NOTA_FISCAL_ID,
    NFI.STATUS,
    NFI.CHAVE_NFE,
    EMP.CADASTRO_ID
  into
    vNotaFiscalSaidaId,
    vStatus,
    vChaveNFe,
    vFornecedorId
  from
    NOTAS_FISCAIS NFI

  inner join EMPRESAS EMP
  on NFI.EMPRESA_ID = EMP.EMPRESA_ID

  where NFI.NOTA_FISCAL_ID = iMOVIMENTO_ID;
  
  if vStatus <> 'E' then
    ERRO('Apenas notas fiscais já emitidas podem gerar entrada de transferência de produtos entre empresas!');
  end if;

  select
    count(*)
  into
    vQtde
  from
    FORNECEDORES
  where CADASTRO_ID = vFornecedorId;

  if vQtde = 0 then
    ERRO('O destinatário da NFe não está cadastrado como fornecedor, por favor faça o cadastro para que seja realizada a entrada da NFe na empresa de destino!');
  end if;
  
  vChaveNFe :=
    substr(vChaveNFe, 1, 4) || '.' ||
    substr(vChaveNFe, 5, 4) || '.' ||
    substr(vChaveNFe, 9, 4) || '.' ||
    substr(vChaveNFe, 13, 4) || '.' ||
    substr(vChaveNFe, 17, 4) || '.' ||
    substr(vChaveNFe, 21, 4) || '.' ||
    substr(vChaveNFe, 25, 4) || '.' ||
    substr(vChaveNFe, 29, 4) || '.' ||
    substr(vChaveNFe, 33, 4) || '.' ||
    substr(vChaveNFe, 37, 4) || '.' ||
    substr(vChaveNFe, 41, 4);

  select
    count(*)
  into
    vQtde
  from
    ENTRADAS_NOTAS_FISCAIS
  where CHAVE_NFE = vChaveNFe;

  if vQtde > 0 then
    ERRO('Já existe uma entrada cadastrada para esta NF-e, por favor verifique no "Relação de entradas de notas fiscais"!');
  end if;

  vEntradaId := SEQ_ENTRADA_NOTAS_FISC_ENT_ID.nextval;

  insert into ENTRADAS_NOTAS_FISCAIS(
    ENTRADA_ID,
    FORNECEDOR_ID,
    CFOP_ID,
    EMPRESA_ID,
    ORIGEM_ENTRADA,
    NOTA_TRANSF_PROD_ORIGEM_ID,
    NUMERO_NOTA,
    MODELO_NOTA,
    SERIE_NOTA,    
    TIPO_RATEIO_FRETE,    
    VALOR_TOTAL,
    VALOR_TOTAL_PRODUTOS,
    BASE_CALCULO_ICMS,
    VALOR_ICMS,
    BASE_CALCULO_ICMS_ST,
    VALOR_ICMS_ST,
    BASE_CALCULO_PIS,
    VALOR_PIS,
    BASE_CALCULO_COFINS,
    VALOR_COFINS,    
    PESO_LIQUIDO,
    PESO_BRUTO,
    DATA_HORA_EMISSAO,
    DATA_ENTRADA,
    DATA_PRIMEIRA_PARCELA,
    STATUS,
    PLANO_FINANCEIRO_ID,    
    CHAVE_NFE
  )
  select
    vEntradaId,
    vFornecedorId,
    NFI.CFOP_ID,
    EEN.EMPRESA_ID,
    iTIPO_MOVIMENTO,
    vNotaFiscalSaidaId,
    NFI.NUMERO_NOTA,
    '55',
    NFI.SERIE_NOTA,
    'V',
    NFI.VALOR_TOTAL,
    NFI.VALOR_TOTAL_PRODUTOS,
    NFI.BASE_CALCULO_ICMS,
    NFI.VALOR_ICMS,
    NFI.BASE_CALCULO_ICMS_ST,
    NFI.VALOR_ICMS_ST,
    NFI.BASE_CALCULO_PIS,
    NFI.VALOR_PIS,
    NFI.BASE_CALCULO_COFINS,
    NFI.VALOR_COFINS,
    NFI.PESO_LIQUIDO,
    NFI.PESO_BRUTO,
    NFI.DATA_HORA_EMISSAO,
    trunc(sysdate), -- DATA_ENTRADA
    trunc(sysdate),
    'ACM',
    '2.001.001',
    vChaveNFe
  from
    NOTAS_FISCAIS NFI
    
  /* Trazendo a empresa da saída */
  inner join EMPRESAS EMP
  on NFI.EMPRESA_ID = EMP.EMPRESA_ID
  
  /* Trazendo a empresa que será realizada a entrada pelo código do cadastro */
  inner join EMPRESAS EEN
  on NFI.CADASTRO_ID = EEN.CADASTRO_ID
  
  where NFI.NOTA_FISCAL_ID = vNotaFiscalSaidaId;
    
  insert into ENTRADAS_NOTAS_FISCAIS_ITENS(
    ENTRADA_ID,
    PRODUTO_ID,
    ITEM_ID,
    CFOP_ID,
    LOCAL_ENTRADA_ID,
    CST,
    CODIGO_NCM,
    VALOR_TOTAL,
    PRECO_UNITARIO,
    QUANTIDADE,
    UNIDADE_ENTRADA_ID,
    VALOR_TOTAL_DESCONTO,
    VALOR_TOTAL_OUTRAS_DESPESAS,    
    ORIGEM_PRODUTO,

    QUANTIDADE_EMBALAGEM,
    MULTIPLO_COMPRA,
    UNIDADE_COMPRA_ID,

    QUANTIDADE_ENTRADA_ALTIS,
    
    /* ICMS NORMAL */
    INDICE_REDUCAO_BASE_ICMS,
    BASE_CALCULO_ICMS,
    PERCENTUAL_ICMS,
    VALOR_ICMS,
    /* ------------------------------------------------ */
    
    /* ST */
    INDICE_REDUCAO_BASE_ICMS_ST,
    BASE_CALCULO_ICMS_ST,
    PERCENTUAL_ICMS_ST,
    VALOR_ICMS_ST,
    IVA,
    /* ------------------------------------------------ */
    
    /* PIS */
    CST_PIS,
    BASE_CALCULO_PIS,
    PERCENTUAL_PIS,
    VALOR_PIS,
    /* ------------------------------------------------ */
    
    /* COFINS */
    CST_COFINS,
    BASE_CALCULO_COFINS,
    PERCENTUAL_COFINS,
    VALOR_COFINS,
    /* ----------------------------------------------- */
    
    /* IPI */
    VALOR_IPI,
    PERCENTUAL_IPI,
    /* ----------------------------------------------- */
    
    PRECO_FINAL,
    PRECO_LIQUIDO,
    PRECO_PAUTA,
    INFORMACOES_ADICIONAIS
  )
  select
    vEntradaId,
    ITE.PRODUTO_ID,
    ITE.ITEM_ID,
    ITE.CFOP_ID,
    1,
    ITE.CST,
    ITE.CODIGO_NCM,
    ITE.VALOR_TOTAL,
    ITE.PRECO_UNITARIO,
    ITE.QUANTIDADE,
    PRO.UNIDADE_VENDA,
    0,
    0,
    ITE.ORIGEM_PRODUTO,

    1,
    1,
    PRO.UNIDADE_VENDA,

    ITE.QUANTIDADE,
    
    /* ICMS NORMAL */
    ITE.INDICE_REDUCAO_BASE_ICMS,
    ITE.BASE_CALCULO_ICMS,
    ITE.PERCENTUAL_ICMS,
    ITE.VALOR_ICMS,
    /* ------------------------------------------------ */
    
    /* ST */
    ITE.INDICE_REDUCAO_BASE_ICMS_ST,
    ITE.BASE_CALCULO_ICMS_ST,
    ITE.PERCENTUAL_ICMS_ST,
    ITE.VALOR_ICMS_ST,
    ITE.IVA,
    /* ------------------------------------------------ */
    
    /* PIS */
    ITE.CST_PIS,
    ITE.BASE_CALCULO_PIS,
    ITE.PERCENTUAL_PIS,
    ITE.VALOR_PIS,
    /* ------------------------------------------------ */
    
    /* COFINS */
    ITE.CST_COFINS,
    ITE.BASE_CALCULO_COFINS,
    ITE.PERCENTUAL_COFINS,
    ITE.VALOR_COFINS,
    /* ----------------------------------------------- */
    
    /* IPI */
    ITE.VALOR_IPI,
    ITE.PERCENTUAL_IPI,
    /* ----------------------------------------------- */
    
    0, -- PRECO_FINAL
    0, -- PRECO_LIQUIDO
    ITE.PRECO_PAUTA,
    ITE.INFORMACOES_ADICIONAIS
  from
    NOTAS_FISCAIS_ITENS ITE

  inner join PRODUTOS PRO
  on ITE.PRODUTO_ID = PRO.PRODUTO_ID

  where ITE.NOTA_FISCAL_ID = vNotaFiscalSaidaId;

end GERAR_ENTR_TRANS_PRODUTOS_EMP;
/