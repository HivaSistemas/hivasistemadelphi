﻿create or replace procedure ATUALIZAR_CUSTOS_ENTRADA(iENTRADA_ID in number)
is

  vEmpresaId             ENTRADAS_NOTAS_FISCAIS.ENTRADA_ID%type;
  vAtualizarCustoEntrada ENTRADAS_NOTAS_FISCAIS.ATUALIZAR_CUSTO_ENTRADA%type;  
  
  type RecFormacaoCusto is record(
    PRECO_TABELA        CUSTOS_PRODUTOS.PRECO_TABELA%type default 0,
    PERC_ICMS           CUSTOS_PRODUTOS.PERCENTUAL_ICMS_ENTRADA%type default 0,
    PERC_ST             CUSTOS_PRODUTOS.PERCENTUAL_ST_ENTRADA%type default 0,
    PERC_FRETE          CUSTOS_PRODUTOS.PERCENTUAL_FRETE_ENTRADA%type default 0,
    PERC_ICMS_FRETE     CUSTOS_PRODUTOS.PERCENTUAL_ICMS_FRETE_ENTRADA%type default 0,
    PERC_PIS            CUSTOS_PRODUTOS.PERCENTUAL_PIS_ENTRADA%type default 0,
    PERC_COFINS         CUSTOS_PRODUTOS.PERCENTUAL_COFINS_ENTRADA%type default 0,
    PERC_DESCONTO       CUSTOS_PRODUTOS.PERCENTUAL_DESCONTO_ENTRADA%type default 0,
    PERC_OUTRAS_DESP    CUSTOS_PRODUTOS.PERCENTUAL_OUTRAS_DESPESAS_ENT%type default 0,
    PERC_OUTROS_CUSTOS  CUSTOS_PRODUTOS.PERCENTUAL_OUTROS_CUSTOS_ENTR%type default 0,
    PERC_IPI            CUSTOS_PRODUTOS.PERCENTUAL_IPI_ENTRADA%type default 0,
    PERC_DIFAL          CUSTOS_PRODUTOS.PERCENTUAL_DIFAL_ENTRADA%type default 0,
    
    PRECO_FINAL_ANTERIOR            CUSTOS_PRODUTOS.PRECO_FINAL%type default 0,
    PRECO_FINAL_MEDIO_ANTERIOR      CUSTOS_PRODUTOS.PRECO_FINAL_MEDIO%type default 0,

    PRECO_LIQUIDO_ANTERIOR          CUSTOS_PRODUTOS.PRECO_LIQUIDO%type default 0,
    PRECO_LIQUIDO_MEDIO_ANTERIOR    CUSTOS_PRODUTOS.PRECO_LIQUIDO_MEDIO%type default 0,

    CMV_ANTERIOR                    CUSTOS_PRODUTOS.CMV%type default 0
  );
  
  vFormacaoCusto         RecFormacaoCusto;
  
  cursor cItens is
  select
    ITE.PRODUTO_ID,
    CFO.ENTRADA_MERCADORIAS_BONIFIC,
    ITE.PRECO_FINAL,
    ITE.PRECO_LIQUIDO,
    ITE.QUANTIDADE_ENTRADA_ALTIS as QUANTIDADE,
    round((ITE.VALOR_TOTAL - ITE.VALOR_TOTAL_DESCONTO + ITE.VALOR_TOTAL_OUTRAS_DESPESAS + ITE.VALOR_IPI + ITE.VALOR_ICMS_ST) / ITE.QUANTIDADE_ENTRADA_ALTIS, 5) as PRECO_TRIBUTAVEL,
    round((ITE.VALOR_TOTAL - ITE.VALOR_TOTAL_DESCONTO + ITE.VALOR_TOTAL_OUTRAS_DESPESAS) / ITE.QUANTIDADE_ENTRADA_ALTIS, 5) as PRECO_TRIB_SEM_ST,
    round((ITE.VALOR_TOTAL - ITE.VALOR_TOTAL_DESCONTO + ITE.VALOR_TOTAL_OUTRAS_DESPESAS) / ITE.QUANTIDADE_ENTRADA_ALTIS, 5) as PRECO_TRIB_SEM_IPI,
    round(ITE.VALOR_TOTAL / ITE.QUANTIDADE_ENTRADA_ALTIS, 5) as PRECO_UNITARIO,
    ITE.VALOR_TOTAL_DESCONTO / ITE.QUANTIDADE_ENTRADA_ALTIS as VALOR_DESCONTO_UNITARIO,
    ITE.VALOR_TOTAL_OUTRAS_DESPESAS / ITE.QUANTIDADE_ENTRADA_ALTIS as VALOR_OUTRAS_DESPESAS_UNITARIO,

    ITE.VALOR_ICMS_CUSTO as VALOR_ICMS_UNITARIO,
    ITE.VALOR_ICMS_ST_CUSTO as VALOR_ICMS_ST_UNITARIO,
    ITE.VALOR_ICMS_FRETE_CUSTO as VALOR_ICMS_FRETE_UNITARIO,
    ITE.VALOR_PIS_CUSTO as VALOR_PIS_UNITARIO,
    ITE.VALOR_COFINS_CUSTO as VALOR_COFINS_UNITARIO,
    ITE.VALOR_IPI_CUSTO as VALOR_IPI_UNITARIO,
    ITE.VALOR_FRETE_CUSTO as VALOR_FRETE_UNITARIO,
    ITE.VALOR_DIFAL_CUSTO as VALOR_DIFAL_UNITARIO,
    ITE.VALOR_OUTROS_CUSTO as VALOR_OUTROS_CUSTOS_UNITARIO
  from
    ENTRADAS_NOTAS_FISCAIS_ITENS ITE

  inner join PRODUTOS PRO
  on ITE.PRODUTO_ID = PRO.PRODUTO_ID

  inner join CFOP CFO
  on ITE.CFOP_ID = CFO.CFOP_PESQUISA_ID

  where ITE.ENTRADA_ID = iENTRADA_ID;

  cursor cComprasBaixar is
  select distinct
    COMPRA_ID
  from
    ENTRADAS_NF_ITENS_COMPRAS
  where ENTRADA_ID = iENTRADA_ID;

begin

  select
    EMPRESA_ID,
    ATUALIZAR_CUSTO_ENTRADA
  into
    vEmpresaId,
    vAtualizarCustoEntrada
  from
    ENTRADAS_NOTAS_FISCAIS
  where ENTRADA_ID = iENTRADA_ID;

  for vItens in cItens loop

    ATUALIZAR_CMV(vEmpresaId, vItens.PRODUTO_ID, vItens.PRECO_FINAL, vItens.QUANTIDADE);

    /* Se não é para atualizar os custos, passando para frente após atualizar o CMV */
    if vAtualizarCustoEntrada = 'N' then
      continue;
    end if;
    
    select
      PRECO_TABELA,
      PERCENTUAL_ICMS_ENTRADA,
      PERCENTUAL_ST_ENTRADA,
      PERCENTUAL_FRETE_ENTRADA,
      PERCENTUAL_ICMS_FRETE_ENTRADA,
      PERCENTUAL_PIS_ENTRADA,
      PERCENTUAL_COFINS_ENTRADA,
      PERCENTUAL_DESCONTO_ENTRADA,
      PERCENTUAL_OUTRAS_DESPESAS_ENT,
      PERCENTUAL_OUTROS_CUSTOS_ENTR,
      PERCENTUAL_IPI_ENTRADA,
      PERCENTUAL_DIFAL_ENTRADA,      
      PRECO_FINAL,
      PRECO_FINAL_MEDIO,
      PRECO_LIQUIDO,
      PRECO_LIQUIDO_MEDIO,
      CMV      
    into
      vFormacaoCusto.PRECO_TABELA,
      vFormacaoCusto.PERC_ICMS,
      vFormacaoCusto.PERC_ST,
      vFormacaoCusto.PERC_FRETE,
      vFormacaoCusto.PERC_ICMS_FRETE,
      vFormacaoCusto.PERC_PIS,
      vFormacaoCusto.PERC_COFINS,
      vFormacaoCusto.PERC_DESCONTO,
      vFormacaoCusto.PERC_OUTRAS_DESP,
      vFormacaoCusto.PERC_OUTROS_CUSTOS,
      vFormacaoCusto.PERC_IPI,
      vFormacaoCusto.PERC_DIFAL,
      vFormacaoCusto.PRECO_FINAL_ANTERIOR,
      vFormacaoCusto.PRECO_FINAL_MEDIO_ANTERIOR,
      vFormacaoCusto.PRECO_LIQUIDO_ANTERIOR,
      vFormacaoCusto.PRECO_LIQUIDO_MEDIO_ANTERIOR,
      vFormacaoCusto.CMV_ANTERIOR      
    from
      VW_CUSTOS_PRODUTOS CUS

    inner join ESTOQUES EST
    on CUS.EMPRESA_ID = EST.EMPRESA_ID
    and CUS.PRODUTO_ID = EST.PRODUTO_ID
    
    where EST.EMPRESA_ID = vEmpresaId
    and EST.PRODUTO_ID = vItens.PRODUTO_ID;
    
    update ENTRADAS_NOTAS_FISCAIS_ITENS set      
      PRECO_TABELA_ANTERIOR           = vFormacaoCusto.PRECO_TABELA,
      PERCENTUAL_DESCONTO_ENTR_ANT    = vFormacaoCusto.PERC_DESCONTO,
      PERCENTUAL_IPI_ENTRADA_ANT      = vFormacaoCusto.PERC_IPI,
      PERCENTUAL_OUTRAS_DESP_ENT_ANT  = vFormacaoCusto.PERC_OUTRAS_DESP,
      PERCENTUAL_FRETE_ENTR_ANTERIOR  = vFormacaoCusto.PERC_FRETE,
      PERCENTUAL_ST_ENTRADA_ANTERIOR  = vFormacaoCusto.PERC_ST,
      PERCENTUAL_ICMS_ENTR_ANTERIOR   = vFormacaoCusto.PERC_ICMS,
      PERCENTUAL_ICMS_FRETE_ENTR_ANT  = vFormacaoCusto.PERC_ICMS_FRETE,
      PERCENTUAL_PIS_ENTRADA_ANTER    = vFormacaoCusto.PERC_PIS,
      PERCENTUAL_COFINS_ENT_ANTERIOR  = vFormacaoCusto.PERC_COFINS,
      /*PERCENTUAL_PIS_FRETE_ENT_ANT    = vFormacaoCusto.PRECO_TABELA,
      PERCENTUAL_COF_FRETE_ENT_ANT    = vFormacaoCusto.PRECO_TABELA,*/
      PERCENTUAL_OUT_CUS_ENTR_ANTER   = vFormacaoCusto.PERC_OUTROS_CUSTOS,
      --PERCENTUAL_CUSTO_FIXO_ENT_ANT   = vFormacaoCusto.PERC_IPI,
      PERCENTUAL_DIFAL_ENTRADA_ANTER  = vFormacaoCusto.PERC_DIFAL,
      PRECO_FINAL_ANTERIOR            = vFormacaoCusto.PRECO_FINAL_ANTERIOR,
      PRECO_FINAL_MEDIO_ANTERIOR      = vFormacaoCusto.PRECO_FINAL_MEDIO_ANTERIOR,
      PRECO_LIQUIDO_ANTERIOR          = vFormacaoCusto.PRECO_LIQUIDO_ANTERIOR,
      PRECO_LIQUIDO_MEDIO_ANTERIOR    = vFormacaoCusto.PRECO_LIQUIDO_MEDIO_ANTERIOR,
      CMV_ANTERIOR                    = vFormacaoCusto.CMV_ANTERIOR
    where ENTRADA_ID = iENTRADA_ID
    and PRODUTO_ID = vItens.PRODUTO_ID;   
  
    vFormacaoCusto.PRECO_TABELA       := vItens.PRECO_UNITARIO;

    vFormacaoCusto.PERC_ICMS :=
      case when vItens.VALOR_ICMS_ST_UNITARIO > 0 then 0 else round(vItens.VALOR_ICMS_UNITARIO / vItens.PRECO_TRIB_SEM_IPI, 5) * 100 end;

    vFormacaoCusto.PERC_ST            := round(vItens.VALOR_ICMS_ST_UNITARIO / vItens.PRECO_TRIB_SEM_ST, 5) * 100;
    vFormacaoCusto.PERC_FRETE         := round(vItens.VALOR_FRETE_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;
    vFormacaoCusto.PERC_PIS           := round(vItens.VALOR_PIS_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;
    vFormacaoCusto.PERC_COFINS        := round(vItens.VALOR_COFINS_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;
    vFormacaoCusto.PERC_DESCONTO      := round(vItens.VALOR_DESCONTO_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;
    vFormacaoCusto.PERC_OUTRAS_DESP   := round(vItens.VALOR_OUTRAS_DESPESAS_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;
    vFormacaoCusto.PERC_IPI           := round(vItens.VALOR_IPI_UNITARIO / vItens.PRECO_TRIB_SEM_IPI, 5) * 100;
    vFormacaoCusto.PERC_ICMS_FRETE    := round(vItens.VALOR_ICMS_FRETE_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;

    vFormacaoCusto.PERC_OUTROS_CUSTOS := round(vItens.VALOR_OUTROS_CUSTOS_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;
    vFormacaoCusto.PERC_DIFAL         := round(vItens.VALOR_DIFAL_UNITARIO / vItens.PRECO_TRIBUTAVEL, 5) * 100;

    ATUALIZAR_CUSTOS_PRODUTOS(
      vEmpresaId,
      vItens.PRODUTO_ID,
      vItens.PRECO_FINAL,
      vItens.PRECO_LIQUIDO,
      vItens.QUANTIDADE,
      'ENT',
      iENTRADA_ID,
      vFormacaoCusto.PRECO_TABELA,
      vFormacaoCusto.PERC_ICMS,
      vFormacaoCusto.PERC_ST,
      vFormacaoCusto.PERC_FRETE,
      vFormacaoCusto.PERC_ICMS_FRETE,
      vFormacaoCusto.PERC_PIS,
      vFormacaoCusto.PERC_COFINS,
      vFormacaoCusto.PERC_DESCONTO,
      vFormacaoCusto.PERC_OUTRAS_DESP,
      vFormacaoCusto.PERC_OUTROS_CUSTOS,
      vFormacaoCusto.PERC_IPI,
      vFormacaoCusto.PERC_DIFAL
    );

    for xGrupoPrecificacao in (select EMPRESA_GRUPO_ID from GRUPOS_PRECIFICACAO where EMPRESA_ID = vEmpresaId) loop

      if xGrupoPrecificacao.EMPRESA_GRUPO_ID <> vEmpresaId then

        ATUALIZAR_CUSTOS_PRODUTOS(
          xGrupoPrecificacao.EMPRESA_GRUPO_ID,
          vItens.PRODUTO_ID,
          vItens.PRECO_FINAL,
          vItens.PRECO_LIQUIDO,
          vItens.QUANTIDADE,
          'ENT',
          iENTRADA_ID,
          vFormacaoCusto.PRECO_TABELA,
          vFormacaoCusto.PERC_ICMS,
          vFormacaoCusto.PERC_ST,
          vFormacaoCusto.PERC_FRETE,
          vFormacaoCusto.PERC_ICMS_FRETE,
          vFormacaoCusto.PERC_PIS,
          vFormacaoCusto.PERC_COFINS,
          vFormacaoCusto.PERC_DESCONTO,
          vFormacaoCusto.PERC_OUTRAS_DESP,
          vFormacaoCusto.PERC_OUTROS_CUSTOS,
          vFormacaoCusto.PERC_IPI,
          vFormacaoCusto.PERC_DIFAL
        );
      end if;
    end loop;

  end loop; 

end ATUALIZAR_CUSTOS_ENTRADA;
/