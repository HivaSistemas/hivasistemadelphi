create or replace procedure VERIFICAR_LOTE_CADASTRADO(
  iEMPRESA_ID       in number,
  iPRODUTO_ID       in number,
  iLOCAL_ID         in number,
  iLOTE             in string,
  iDATA_FABRICACAO  in date,
  iDATA_VENCIMENTO  in date
)
is
  vQtde  number;
begin

  select
    count(*)
  into
    vQtde
  from
    PRODUTOS
  where TIPO_CONTROLE_ESTOQUE in('K', 'A')
  and PRODUTO_ID = iPRODUTO_ID;

  if vQtde > 0 then
    return;
  end if;

  select
    count(*)
  into
    vQtde
  from
    PRODUTOS_LOTES
  where PRODUTO_ID = iPRODUTO_ID
  and LOTE = iLOTE;

  if vQtde = 0 then
    insert into PRODUTOS_LOTES(
      PRODUTO_ID,
      LOTE,
      DATA_FABRICACAO,
      DATA_VENCIMENTO
    )values(
      iPRODUTO_ID,
      iLOTE,
      iDATA_FABRICACAO,
      iDATA_VENCIMENTO
    );
  end if;

  select
    count(*)
  into
    vQtde
  from
    ESTOQUES_DIVISAO
  where EMPRESA_ID = iEMPRESA_ID
  and LOCAL_ID = iLOCAL_ID
  and PRODUTO_ID = iPRODUTO_ID
  and LOTE = iLOTE;

  if vQtde = 0 then
    insert into ESTOQUES_DIVISAO(
      EMPRESA_ID,
      PRODUTO_ID,
      LOCAL_ID,
      LOTE
    )values(
      iEMPRESA_ID,
      iPRODUTO_ID,
      iLOCAL_ID,
      iLOTE
    );
  end if;

end VERIFICAR_LOTE_CADASTRADO;
/