create or replace procedure MOVIMENTAR_COMPRAS_PENDENTES(
  iEMPRESA_ID in number,
  iPRODUTO_ID in number,
  iQUANTIDADE in number
)
is
  vProdutoPaiId PRODUTOS.PRODUTO_PAI_ID%type;
  vQtdeVezesPai PRODUTOS.QUANTIDADE_VEZES_PAI%type;
begin

  select
    PRODUTO_PAI_ID,
    QUANTIDADE_VEZES_PAI
  into
    vProdutoPaiId,
    vQtdeVezesPai
  from
    PRODUTOS
  where PRODUTO_ID = iPRODUTO_ID;
  
  update ESTOQUES set
    COMPRAS_PENDENTES = COMPRAS_PENDENTES + iQUANTIDADE * vQtdeVezesPai
  where EMPRESA_ID = iEMPRESA_ID
  and PRODUTO_ID = vProdutoPaiId;

end MOVIMENTAR_COMPRAS_PENDENTES;