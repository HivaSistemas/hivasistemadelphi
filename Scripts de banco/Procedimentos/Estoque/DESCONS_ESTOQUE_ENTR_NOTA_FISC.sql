create or replace procedure DESCONS_ESTOQUE_ENTR_NOTA_FISC(iENTRADA_ID in number)
is

  vEmpresaId             ENTRADAS_NOTAS_FISCAIS.EMPRESA_ID%type;
  vStatus                ENTRADAS_NOTAS_FISCAIS.STATUS%type;
  vPenultimaEntradaId    ESTOQUES.PENULTIMA_ENTRADA_ID%type;
  vOrigemEntrada         ENTRADAS_NOTAS_FISCAIS.ORIGEM_ENTRADA%type;

  cursor cItens is
  select
    ITE.PRODUTO_ID,
    ITE.ITEM_ID,
    ITE.LOCAL_ENTRADA_ID,
    PRO.TIPO_CONTROLE_ESTOQUE,
    ITE.QUANTIDADE_ENTRADA_ALTIS as QUANTIDADE,
    ENTRADA_ANTERIOR_ID
  from
    ENTRADAS_NOTAS_FISCAIS_ITENS ITE

  inner join PRODUTOS PRO
  on ITE.PRODUTO_ID = PRO.PRODUTO_ID

  where ITE.ENTRADA_ID = iENTRADA_ID;


  cursor cLotes(pITEM_ID in number) is
  select
    LOTE,
    QUANTIDADE
  from
    ENTRADAS_NF_ITENS_LOTES
  where ENTRADA_ID = iENTRADA_ID
  and ITEM_ID = pITEM_ID;


  cursor cComprasBaixadas is
  select distinct
    COMPRA_ID
  from
    ENTRADAS_NF_ITENS_COMPRAS
  where ENTRADA_ID = iENTRADA_ID;

begin
  select
    EMPRESA_ID,
    STATUS,
    ORIGEM_ENTRADA
  into
    vEmpresaId,
    vStatus,
    vOrigemEntrada
  from
    ENTRADAS_NOTAS_FISCAIS
  where ENTRADA_ID = iENTRADA_ID;
  
  if vStatus <> 'ECO' then
    ERRO('Somente entrada com o estoque consolidado pode ser desconsolidada!');
  end if;

  if vOrigemEntrada = 'TFP' then
    ERRO('N�o � permitido desconsolidar entradas provenientes de transfer�ncia fiscal de prodtuos!');
  end if;

  VOLTAR_CUSTOS_ENTRADA(iENTRADA_ID);

  for vItens in cItens loop
    if vItens.TIPO_CONTROLE_ESTOQUE in('L','G','P') then
      for vLotes in cLotes(vItens.ITEM_ID) loop
        MOVIMENTAR_ESTOQUE(vEmpresaId, vItens.PRODUTO_ID, vItens.LOCAL_ENTRADA_ID, vLotes.LOTE, vLotes.QUANTIDADE *-1, 'ENM', null, null);
      end loop;
    else
      MOVIMENTAR_ESTOQUE(vEmpresaId, vItens.PRODUTO_ID, vItens.LOCAL_ENTRADA_ID, '???', vItens.QUANTIDADE * -1, 'ENM', null, null);
    end if;

    MOVIMENTAR_ESTOQUE_FISCAL(vEmpresaId, vItens.PRODUTO_ID, vItens.QUANTIDADE * -1);
    
    begin
      select
        ITE.ENTRADA_ANTERIOR_ID
      into
        vPenultimaEntradaId
      from
        ENTRADAS_NOTAS_FISCAIS_ITENS ITE
        
      inner join VW_ESTOQUES EST
      on EST.EMPRESA_ID = vEmpresaId
      and ITE.PRODUTO_ID = EST.PRODUTO_ID
        
      where ITE.ENTRADA_ID = EST.PENULTIMA_ENTRADA_ID
      and ITE.PRODUTO_ID = vItens.PRODUTO_ID;
    exception
      when others then
        vPenultimaEntradaId := null;
    end;
    
    update ESTOQUES set
      PENULTIMA_ENTRADA_ID = vPenultimaEntradaId,
      ULTIMA_ENTRADA_ID = PENULTIMA_ENTRADA_ID
    where EMPRESA_ID = vEmpresaId
    and PRODUTO_ID = vItens.PRODUTO_ID;    
  end loop;
  
  for xComprasBaixadas in cComprasBaixadas loop
    update COMPRAS set
      STATUS = 'A'
    where COMPRA_ID = xComprasBaixadas.COMPRA_ID
    and STATUS = 'B';
  end loop;
  
  update ENTRADAS_NOTAS_FISCAIS set
    STATUS = 'ACM'
  where ENTRADA_ID = iENTRADA_ID;

end DESCONS_ESTOQUE_ENTR_NOTA_FISC;
/