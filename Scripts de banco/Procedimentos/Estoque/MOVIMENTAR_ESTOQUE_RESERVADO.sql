﻿create or replace procedure MOVIMENTAR_ESTOQUE_RESERVADO(
  iEMPRESA_ID in number,
  iLOCAL_ID   in number,
  iPRODUTO_ID in number,
  iLOTE       in string,
  iQUANTIDADE in number
)
as
  vQtde         number;
  vQtdeVezesPai  PRODUTOS.QUANTIDADE_VEZES_PAI%type;
  vProdutoPaiId  PRODUTOS.PRODUTO_PAI_ID%type;
  vQuantidade    number;
begin

  select
    QUANTIDADE_VEZES_PAI,
    PRODUTO_PAI_ID
  into
    vQtdeVezesPai,
    vProdutoPaiId
  from
    PRODUTOS
  where PRODUTO_ID = iPRODUTO_ID;

  vQuantidade := iQUANTIDADE * vQtdeVezesPai;

  update ESTOQUES_DIVISAO set
     RESERVADO = RESERVADO + vQuantidade
   where EMPRESA_ID = iEMPRESA_ID
   and PRODUTO_ID = vProdutoPaiId
   and LOCAL_ID = iLOCAL_ID
   and LOTE = iLOTE;

  update ESTOQUES set
    RESERVADO = RESERVADO + vQuantidade
  where EMPRESA_ID = iEMPRESA_ID
  and PRODUTO_ID = vProdutoPaiId;


  select sum(DISPONIVEL)
  into vQtde
  from ESTOQUES
  where PRODUTO_ID = vProdutoPaiId;
	
  if vQtde <= 0 then
    update PRODUTOS set
      ATIVO = 'N',
      MOTIVO_INATIVIDADE = 'INATIVIDADE AUTOMÁTICA(PARÂMETRO NO CADASTRO DE PRODUTOS)'
    where PRODUTO_ID = vProdutoPaiId
    and ATIVO = 'N';
  end if;
  
end MOVIMENTAR_ESTOQUE_RESERVADO;