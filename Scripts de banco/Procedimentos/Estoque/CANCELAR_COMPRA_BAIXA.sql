﻿create or replace procedure CANCELAR_COMPRA_BAIXA(iBAIXA_ID in number)
is
  vCompraId number;
  vQtde     number;
begin

  /* Validando se existem contas a pagar já baixados para a compra */
  select
    count(*)
  into
    vQtde
  from
    CONTAS_PAGAR
  where COMPRA_BAIXA_ID = iBAIXA_ID
  and STATUS = 'B';

  if vQtde > 0 then
    ERRO('Existem contas a pagar baixados para esta compra, cancele primeiro suas baixas!');
  end if;
  
  select
    COMPRA_ID
  into
    vCompraId
  from
    COMPRAS_BAIXAS
  where BAIXA_ID = iBAIXA_ID;  
  
  delete from CONTAS_PAGAR
  where COMPRA_BAIXA_ID = iBAIXA_ID;
    
  delete from COMPRAS_ITENS_BAIXAS
  where BAIXA_ID = iBAIXA_ID;
  
  delete from AJUSTES_ESTOQUE_ITENS
  where AJUSTE_ESTOQUE_ID in(
    select
      AJUSTE_ESTOQUE_ID
    from
      AJUSTES_ESTOQUE
    where BAIXA_COMPRA_ID = iBAIXA_ID
  );
  
  delete from AJUSTES_ESTOQUE
  where BAIXA_COMPRA_ID = iBAIXA_ID;
      
  delete from COMPRAS_BAIXAS
  where BAIXA_ID = iBAIXA_ID;
  
  VER_BAIXAR_CANCELAR_COMPRA(vCompraId);

end CANCELAR_COMPRA_BAIXA;
/