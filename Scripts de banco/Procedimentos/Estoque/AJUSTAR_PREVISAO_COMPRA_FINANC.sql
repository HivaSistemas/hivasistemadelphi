﻿create or replace procedure AJUSTAR_PREVISAO_COMPRA_FINANC(
  iCOMPRA_ID in number
)
is
  vPagarId           CONTAS_PAGAR.PAGAR_ID%type;
  vEmpresaId         COMPRAS.EMPRESA_ID%type;
  vFornecedorId      COMPRAS.FORNECEDOR_ID%type;
  vPlanoFinanceiroId FORNECEDORES.PLANO_FINANCEIRO_REVENDA%type;
  vValorProdutosPend number;  
begin

  /* Deletando a previsão já existente */
  delete from CONTAS_PAGAR
  where COMPRA_ID = iCOMPRA_ID
  and COBRANCA_ID = 5;
  
  select
    round(sum(VALOR_LIQUIDO / QUANTIDADE * SALDO), 2)
  into
    vValorProdutosPend    
  from
    COMPRAS_ITENS
  where COMPRA_ID = iCOMPRA_ID;
  
  if vValorProdutosPend = 0 then
    return;
  end if;
  
  select
    COM.EMPRESA_ID,
    COM.FORNECEDOR_ID,
    nvl(COM.PLANO_FINANCEIRO_ID, FRN.PLANO_FINANCEIRO_REVENDA)
  into  
    vEmpresaId,
    vFornecedorId,
    vPlanoFinanceiroId
  from
    COMPRAS COM
    
  inner join FORNECEDORES FRN
  on COM.FORNECEDOR_ID = FRN.CADASTRO_ID
  
  where COM.COMPRA_ID = iCOMPRA_ID;  
  
  select SEQ_PAGAR_ID.nextval 
  into vPagarId 
  from dual;

  insert into CONTAS_PAGAR(
    PAGAR_ID,
    CADASTRO_ID,
    EMPRESA_ID,
    DOCUMENTO,
    ORIGEM,      
    COMPRA_ID,      
    COBRANCA_ID,
    PORTADOR_ID,
    PLANO_FINANCEIRO_ID,
    DATA_VENCIMENTO,
    DATA_VENCIMENTO_ORIGINAL,
    VALOR_DOCUMENTO,
    STATUS,      
    PARCELA,
    NUMERO_PARCELAS
  )values(
    vPagarId,      
    vFornecedorId,
    vEmpresaId,
    'COM-' || NFORMAT(iCOMPRA_ID, 0) || '/PREV', -- Documento
    'COM',
    iCOMPRA_ID,      
    5, -- Tipo de cobrança previsão
    '9999',
    vPlanoFinanceiroId,
    trunc(sysdate) + 30,
    trunc(sysdate) + 30,
    round(vValorProdutosPend, 2), -- Valor do financeiro
    'A',      
    1,
    1    
  );  

end AJUSTAR_PREVISAO_COMPRA_FINANC;
/