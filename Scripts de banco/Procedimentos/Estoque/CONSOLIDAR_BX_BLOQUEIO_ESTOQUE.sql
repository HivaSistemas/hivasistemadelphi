create or replace procedure CONSOLIDAR_BX_BLOQUEIO_ESTOQUE(iBAIXA_ID in number)
is
  vQtde       number;
  vBloqueioId BLOQUEIOS_ESTOQUES_BAIXAS.BLOQUEIO_ID%type;
  vTipoBaixa  BLOQUEIOS_ESTOQUES_BAIXAS.TIPO%type;

  cursor cItens is
  select
    ITEM_ID,
    QUANTIDADE
  from
    BLOQUEIOS_EST_BAIXAS_ITENS
  where BAIXA_ID = iBAIXA_ID;

begin
  
  select
    BLOQUEIO_ID,
    TIPO
  into
    vBloqueioId,
    vTipoBaixa
  from
    BLOQUEIOS_ESTOQUES_BAIXAS
  where BAIXA_ID = iBAIXA_ID;
  
  for xItens in cItens loop
    
    update BLOQUEIOS_ESTOQUES_ITENS set
      CANCELADOS = CANCELADOS + case when vTipoBaixa = 'C' then xItens.QUANTIDADE else 0 end,
      BAIXADOS = BAIXADOS + case when vTipoBaixa = 'B' then xItens.QUANTIDADE else 0 end
    where BLOQUEIO_ID = vBloqueioId
    and ITEM_ID = xItens.ITEM_ID;
  
  end loop;
  
  select
    count(*)
  into
    vQtde
  from
    BLOQUEIOS_ESTOQUES_ITENS
  where BLOQUEIO_ID = vBloqueioId
  and SALDO > 0;
  
  if vQtde = 0 then
    update BLOQUEIOS_ESTOQUES set
      STATUS = 'B'
    where BLOQUEIO_ID = vBloqueioId;
  end if;
  
end CONSOLIDAR_BX_BLOQUEIO_ESTOQUE;
/