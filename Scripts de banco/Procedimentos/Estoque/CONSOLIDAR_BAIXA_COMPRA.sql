﻿create or replace procedure CONSOLIDAR_BAIXA_COMPRA(iCOMPRA_BAIXA_ID in number)
is

  vEmpresaId          COMPRAS.EMPRESA_ID%type;
  vFornecedorId       COMPRAS.FORNECEDOR_ID%type;
  vCompraId           COMPRAS.COMPRA_ID%type;
  vPlanoFinanceiroId  COMPRAS.PLANO_FINANCEIRO_ID%type;

  vValorProdutos      number;
  vValorTotalProdutos number;
  vIndice             number;
  vPagarId            number;
  vQtdeTitulos        number;


  cursor cTitulos(pCOMPRA_ID in number) is
  select
    CPR.COBRANCA_ID,
    CPR.PARCELA,
    CPR.NUMERO_PARCELAS,
    CPR.VALOR,
    CPR.ITEM_ID,
    CPR.DATA_VENCIMENTO,
    TCO.PORTADOR_ID
  from
    COMPRAS_PREVISOES CPR

  inner join TIPOS_COBRANCA TCO
  on CPR.COBRANCA_ID = TCO.COBRANCA_ID

  where CPR.COMPRA_ID = pCOMPRA_ID;


  cursor cItens is
  select
    ITE.PRODUTO_ID,
    ITE.PRECO_FINAL,
    ITB.QUANTIDADE * ITE.QUANTIDADE_EMBALAGEM as QUANTIDADE
  from
    COMPRAS_ITENS_BAIXAS ITB
  inner join COMPRAS_BAIXAS BAI
  on ITB.BAIXA_ID = BAI.BAIXA_ID
  inner join COMPRAS_ITENS ITE
  on ITB.ITEM_ID = ITE.ITEM_ID
  and BAI.COMPRA_ID = ITE.COMPRA_ID
  where BAI.BAIXA_ID = iCOMPRA_BAIXA_ID;


begin

  select
    COM.EMPRESA_ID,
    COM.COMPRA_ID,
    COM.PLANO_FINANCEIRO_ID,
    COM.FORNECEDOR_ID
  into
    vEmpresaId,
    vCompraId,
    vPlanoFinanceiroId,
    vFornecedorId
  from
    COMPRAS COM
  inner join COMPRAS_BAIXAS BAI
  on COM.COMPRA_ID = BAI.COMPRA_ID
  and BAI.BAIXA_ID = iCOMPRA_BAIXA_ID;

  select
    count(*)
  into
    vQtdeTitulos
  from
    COMPRAS_PREVISOES CPR

  inner join TIPOS_COBRANCA TCO
  on CPR.COBRANCA_ID = TCO.COBRANCA_ID

  where CPR.COMPRA_ID = vCompraId;


  select
    SUM(ITB.quantidade * ITE.preco_unitario)
  into
    vValorProdutos
  from
    COMPRAS_ITENS_BAIXAS ITB
  inner join COMPRAS_BAIXAS BAI
  on ITB.BAIXA_ID = BAI.BAIXA_ID
  inner join COMPRAS_ITENS ITE
  on ITB.ITEM_ID = ITE.ITEM_ID
  and BAI.COMPRA_ID = ITE.COMPRA_ID
  where BAI.BAIXA_ID = iCOMPRA_BAIXA_ID;

  select
    sum(VALOR_LIQUIDO)
  into
    vValorTotalProdutos
  from
    COMPRAS_ITENS
  where COMPRA_ID = vCompraId;

  vIndice := vValorProdutos / vValorTotalProdutos;

  if vQtdeTitulos = 0 then
  /*Ezequiel Validar*
    --vQtdeTitulos := 1;*/
    ERRO('Por favor lance os titulos do pedido na tela de "Pedido de compras"');
  end if;

  vValorProdutos := vValorProdutos / vQtdeTitulos;
  for xTitulos in cTitulos(vCompraId) loop
    select SEQ_PAGAR_ID.nextval
    into vPagarId
    from dual;

    insert into CONTAS_PAGAR(
      PAGAR_ID,
      CADASTRO_ID,
      EMPRESA_ID,
      DOCUMENTO,
      ORIGEM,
      COMPRA_BAIXA_ID,
      COBRANCA_ID,
      PORTADOR_ID,
      PLANO_FINANCEIRO_ID,
      DATA_VENCIMENTO,
      DATA_VENCIMENTO_ORIGINAL,
      VALOR_DOCUMENTO,
      STATUS,
      PARCELA,
      NUMERO_PARCELAS
    )values(
      vPagarId,
      vFornecedorId,
      vEmpresaId,
      'CBX-' || NFORMAT(iCOMPRA_BAIXA_ID, 0) || '/' || NFORMAT(xTitulos.ITEM_ID, 0) || '/COM', -- Documento
      'CBX',
      iCOMPRA_BAIXA_ID,
      xTitulos.COBRANCA_ID,
      xTitulos.PORTADOR_ID,
      vPlanoFinanceiroId,
      xTitulos.DATA_VENCIMENTO,
      xTitulos.DATA_VENCIMENTO,
      round(vValorProdutos, 2),
      'A',
      xTitulos.PARCELA,
      xTitulos.NUMERO_PARCELAS
    );
  end loop;

  for xItens in cItens loop

    ATUALIZAR_CUSTOS_COMPRA(vEmpresaId, xItens.PRODUTO_ID, xItens.PRECO_FINAL, xItens.QUANTIDADE);

  end loop;

  VER_BAIXAR_CANCELAR_COMPRA(vCompraId);

end CONSOLIDAR_BAIXA_COMPRA;
