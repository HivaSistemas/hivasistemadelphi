﻿create or replace procedure INSERIR_MOVIMENTO_PRODUTO(
  iEMPRESA_ID         in number,
  iPRODUTO_ID         in number,
  iQUANTIDADE         in number,
  iOPERACAO           in string, -- ENT - Entrada de notas, VEN - Vendas, CAN - Cancelado de venda, AJU - Ajuste de estoque, DEV - Devolução, RES - Reserva de estoque, CRE - Cancelamento Reserva
  pTIPO_ANALISE_CUSTO in string,
  iID                 in number
)
is

  procedure VER_ESTOQUE_FISCAL is
    vMovimentoId   number;
    vQtdeAnterior  number;
    vQtdeNova      number;
    vQtde          number;
  begin
    select SEQ_MOVIMENTOS_PRODUTOS_ID.nextval
    into vMovimentoId
    from dual;

    select ESTOQUE
    into vQtdeAnterior
    from ESTOQUES
    where PRODUTO_ID = iPRODUTO_ID
    and EMPRESA_ID = iEMPRESA_ID;

    if iOPERACAO = 'AJU' then
      vQtde     := iQUANTIDADE + vQtdeAnterior * -1;
      vQtdeNova := iQUANTIDADE;
    else
      vQtde     := iQUANTIDADE;
      vQtdeNova := vQtdeAnterior + (iQUANTIDADE * case when iOPERACAO in('VEN', 'RES') then -1 else 1 end);
    end if;

    insert into MOVIMENTOS_PRODUTOS(
      MOVIMENTO_ID,
      EMPRESA_ID,
      PRODUTO_ID,
      TIPO_MOVIMENTO,
      QUANTIDADE_ANTERIOR,
      QUANTIDADE,
      QUANTIDADE_NOVA,
      TIPO_ANALISE_CUSTO,
      ORCAMENTO_ID,
      ENTRADA_ID,
      AJUSTE_ESTOQUE_ID,
      DEVOLUCAO_ID
    )values(
      vMovimentoId,
      iEMPRESA_ID,
      iPRODUTO_ID,
      iOPERACAO,
      vQtdeAnterior,
      vQtde,
      vQtdeNova,
      pTIPO_ANALISE_CUSTO,
      case when iOPERACAO in('VEN','CAN','RES','CRE') then iID else null end,
      case when iOPERACAO = 'ENT' then iID else null end,
      case when iOPERACAO = 'AJU' then iID else null end,
      case when iOPERACAO = 'DEV' then iID else null end
    );
  end;

  procedure VER_ESTOQUE_REAL is
    vMovimentoId   number;
    vQtdeAnterior  number;
    vQtdeNova      number;
    vQtde          number;
  begin
    select SEQ_MOVIMENTOS_PRODUTOS_ID.nextval
    into vMovimentoId
    from dual;

    select FISICO
    into vQtdeAnterior
    from ESTOQUES
    where PRODUTO_ID = iPRODUTO_ID
    and EMPRESA_ID = iEMPRESA_ID;

    if iOPERACAO = 'AJU' then
      vQtde     := iQUANTIDADE + vQtdeAnterior * -1;
      vQtdeNova := iQUANTIDADE;
    else
      vQtde     := iQUANTIDADE;
      vQtdeNova := vQtdeAnterior + (iQUANTIDADE * case when iOPERACAO in('VEN', 'RES') then -1 else 1 end);
    end if;

    insert into MOVIMENTOS_PRODUTOS(
      MOVIMENTO_ID,
      EMPRESA_ID,
      PRODUTO_ID,
      TIPO_MOVIMENTO,
      QUANTIDADE_ANTERIOR,
      QUANTIDADE,
      QUANTIDADE_NOVA,
      TIPO_ANALISE_CUSTO,
      ORCAMENTO_ID,
      ENTRADA_ID,
      AJUSTE_ESTOQUE_ID,
      DEVOLUCAO_ID
    )values(
      vMovimentoId,
      iEMPRESA_ID,
      iPRODUTO_ID,
      iOPERACAO,
      vQtdeAnterior,
      vQtde,
      vQtdeNova,
      pTIPO_ANALISE_CUSTO,
      case when iOPERACAO in('VEN','CAN','RES','CRE') then iID else null end,
      case when iOPERACAO = 'ENT' then iID else null end,
      case when iOPERACAO = 'AJU' then iID else null end,
      case when iOPERACAO = 'DEV' then iID else null end
    );
  end;

begin

  if not nvl(iOPERACAO, 'X') in('ENT', 'VEN', 'CAN', 'AJU','DEV','RES','CRE') then
    ERRO('A operação só pode ser do tipo "ENT", "CANC", "AJU", "DEV","VEN","RES" ou "CRE"!');
  end if;

  if not nvl(pTIPO_ANALISE_CUSTO, 'X') in('A', 'B', 'C') then
    ERRO('O tipo de análise só pode ser "A", "B" ou "C"!');
  end if;

  if pTIPO_ANALISE_CUSTO in('A', 'C') then
    VER_ESTOQUE_FISCAL;
  end if;

  if pTIPO_ANALISE_CUSTO in('B', 'C') then
    VER_ESTOQUE_REAL;
  end if;

end INSERIR_MOVIMENTO_PRODUTO;