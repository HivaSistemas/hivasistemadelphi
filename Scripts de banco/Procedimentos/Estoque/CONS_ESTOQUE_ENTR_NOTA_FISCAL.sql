﻿create or replace procedure CONS_ESTOQUE_ENTR_NOTA_FISCAL(iENTRADA_ID in number)
is

  vEmpresaId             ENTRADAS_NOTAS_FISCAIS.EMPRESA_ID%type;
  vEntradaAnteriorId     ENTRADAS_NOTAS_FISCAIS.ENTRADA_ID%type;
  vOrigemEntrada         ENTRADAS_NOTAS_FISCAIS.ORIGEM_ENTRADA%type;

  cursor cItens is
  select
    ITE.PRODUTO_ID,
    ITE.ITEM_ID,
    ITE.LOCAL_ENTRADA_ID,
    PRO.TIPO_CONTROLE_ESTOQUE,
    ITE.QUANTIDADE_ENTRADA_ALTIS as QUANTIDADE
  from
    ENTRADAS_NOTAS_FISCAIS_ITENS ITE

  inner join PRODUTOS PRO
  on ITE.PRODUTO_ID = PRO.PRODUTO_ID

  where ITE.ENTRADA_ID = iENTRADA_ID;


  cursor cComprasBaixar is
  select distinct
    COMPRA_ID
  from
    ENTRADAS_NF_ITENS_COMPRAS
  where ENTRADA_ID = iENTRADA_ID;


  cursor cLotes(pITEM_ID in number) is
  select
    LOTE,
    QUANTIDADE,
    DATA_FABRICACAO,
    DATA_VENCIMENTO
  from
    ENTRADAS_NF_ITENS_LOTES
  where ENTRADA_ID = iENTRADA_ID
  and ITEM_ID = pITEM_ID;
  
begin
  select
    EMPRESA_ID,
    ORIGEM_ENTRADA
  into
    vEmpresaId,
    vOrigemEntrada
  from
    ENTRADAS_NOTAS_FISCAIS
  where ENTRADA_ID = iENTRADA_ID;

  ATUALIZAR_CUSTOS_ENTRADA(iENTRADA_ID);
  
  for vItens in cItens loop

    /* Se não for para movimentar apenas o ESTOQUE */
    if vOrigemEntrada not in('TFR', 'TFE') then
      if vItens.TIPO_CONTROLE_ESTOQUE in('L','G','P') then
        for vLotes in cLotes(vItens.ITEM_ID) loop
          MOVIMENTAR_ESTOQUE(vEmpresaId, vItens.PRODUTO_ID, vItens.LOCAL_ENTRADA_ID, vLotes.LOTE, vLotes.QUANTIDADE, 'ENM', vLotes.DATA_FABRICACAO, vLotes.DATA_VENCIMENTO);
        end loop;
      else
        MOVIMENTAR_ESTOQUE(vEmpresaId, vItens.PRODUTO_ID, vItens.LOCAL_ENTRADA_ID, '???', vItens.QUANTIDADE, 'ENM', null, null);
      end if;

      select
        ULTIMA_ENTRADA_ID
      into
        vEntradaAnteriorId
      from
        VW_ESTOQUES
      where EMPRESA_ID = vEmpresaId
      and PRODUTO_ID = vItens.PRODUTO_ID;

      update ENTRADAS_NOTAS_FISCAIS_ITENS set
        ENTRADA_ANTERIOR_ID = vEntradaAnteriorId
      where ENTRADA_ID = iENTRADA_ID
      and PRODUTO_ID = vItens.PRODUTO_ID;

      update ESTOQUES set
        PENULTIMA_ENTRADA_ID = ULTIMA_ENTRADA_ID,
        ULTIMA_ENTRADA_ID = iENTRADA_ID
      where EMPRESA_ID = vEmpresaId
      and PRODUTO_ID = vItens.PRODUTO_ID;

    end if;

    MOVIMENTAR_ESTOQUE_FISCAL(vEmpresaId, vItens.PRODUTO_ID, vItens.QUANTIDADE);

  end loop;

  for xCompras in cComprasBaixar loop
    VER_BAIXAR_CANCELAR_COMPRA(xCompras.COMPRA_ID);
  end loop;

end CONS_ESTOQUE_ENTR_NOTA_FISCAL;
/