create or replace procedure BAIXAR_BLOQUEIO_ESTOQUE(iBLOQUEIO_ID in number)
is

  cursor cItens is
  select
    BLO.EMPRESA_ID,
    ITE.PRODUTO_ID,
    ITE.LOCAL_ID,
    ITE.LOTE,
    ITE.QUANTIDADE
  from
    BLOQUEIOS_ESTOQUES_ITENS ITE
    
  inner join BLOQUEIOS_ESTOQUES BLO
  on ITE.BLOQUEIO_ID = BLO.BLOQUEIO_ID  
    
  where ITE.BLOQUEIO_ID = iBLOQUEIO_ID;

begin

  for vItens in cItens loop
  
    MOVIMENTAR_ESTOQUE_BLOQUEADO(
      vItens.EMPRESA_ID,
      vItens.PRODUTO_ID,
      vItens.LOCAL_ID,
      vItens.LOTE,
      vItens.QUANTIDADE * -1
    );  
  
  end loop;
  
  update BLOQUEIOS_ESTOQUES set
    STATUS = 'B'
  where BLOQUEIO_ID = iBLOQUEIO_ID;

end BAIXAR_BLOQUEIO_ESTOQUE;
/