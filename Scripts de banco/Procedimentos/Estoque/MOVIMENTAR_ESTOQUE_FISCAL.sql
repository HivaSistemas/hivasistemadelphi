create or replace procedure MOVIMENTAR_ESTOQUE_FISCAL(
  iEMPRESA_ID in number,
  iPRODUTO_ID in number,
  iQUANTIDADE in number
)
is

  vProdutoPaiId PRODUTOS.PRODUTO_ID%type;
  vQtdeVezesPai PRODUTOS.QUANTIDADE_VEZES_PAI%type;

begin

  select
    PRODUTO_PAI_ID,
    QUANTIDADE_VEZES_PAI
  into
    vProdutoPaiId,
    vQtdeVezesPai
  from
    PRODUTOS
  where PRODUTO_ID = iPRODUTO_ID;
  
  update ESTOQUES set 
    ESTOQUE = ESTOQUE + (iQUANTIDADE * vQtdeVezesPai)
  where PRODUTO_ID = vProdutoPaiId
  and EMPRESA_ID = iEMPRESA_ID;
  
end MOVIMENTAR_ESTOQUE_FISCAL;