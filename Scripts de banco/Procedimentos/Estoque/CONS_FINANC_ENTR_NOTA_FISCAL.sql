﻿create or replace procedure CONS_FINANC_ENTR_NOTA_FISCAL(iENTRADA_ID in number)
is

  vPagarId               CONTAS_PAGAR.PAGAR_ID%type;
  vFornecedorId          CONTAS_PAGAR.CADASTRO_ID%type;
  vEmpresaId             CONTAS_PAGAR.EMPRESA_ID%type;
  vPlanoFinanceiroId     CONTAS_PAGAR.PLANO_FINANCEIRO_ID%type;
  vCentroCustoId         CONTAS_PAGAR.CENTRO_CUSTO_ID%type;
  vDataEmissao           CONTAS_PAGAR.DATA_EMISSAO%type;
  vConhecimentoFreteId   ENTRADAS_NOTAS_FISCAIS.CONHECIMENTO_FRETE_ID%type;
  vStatus                ENTRADAS_NOTAS_FISCAIS.STATUS%type;
  vEmpresaConhecimentoId CONHECIMENTOS_FRETES.EMPRESA_ID%type;
  vTransportadoraId      CONHECIMENTOS_FRETES.TRANSPORTADORA_ID%type;
  vQtde                  number;

  cursor cTitulos is
  select
    TIT.COBRANCA_ID,
    TIT.CODIGO_BARRAS,
    TIT.NOSSO_NUMERO,
    TIT.DOCUMENTO,
    TIT.PARCELA,
    TIT.NUMERO_PARCELAS,
    TIT.VALOR_DOCUMENTO,
    TCO.PORTADOR_ID,    
    TIT.DATA_VENCIMENTO,
    TIT.TIPO,
    TIT.FORNECEDOR_ID,
    TIT.PLANO_FINANCEIRO_ID,
    TIT.CENTRO_CUSTO_ID
  from
    ENTRADAS_NOTAS_FISC_FINANCEIRO TIT
    
  inner join TIPOS_COBRANCA TCO
  on TIT.COBRANCA_ID = TCO.COBRANCA_ID
  
  where TIT.ENTRADA_ID = iENTRADA_ID;
  
  
  cursor cTitulosConhecimento(pCONHECIMENTO_ID in number) is
  select
    TIT.COBRANCA_ID,
    TIT.CODIGO_BARRAS,
    TIT.NOSSO_NUMERO,
    TIT.DOCUMENTO,
    TIT.PARCELA,
    TIT.NUMERO_PARCELAS,
    TIT.VALOR,
    TCO.PORTADOR_ID,    
    TIT.DATA_VENCIMENTO
  from
    CONHEC_FRETES_FINANCEIROS TIT
    
  inner join TIPOS_COBRANCA TCO
  on TIT.COBRANCA_ID = TCO.COBRANCA_ID
  
  where TIT.CONHECIMENTO_ID = pCONHECIMENTO_ID;  
  
begin

  select
    FORNECEDOR_ID,
    EMPRESA_ID,
    PLANO_FINANCEIRO_ID,
    CONHECIMENTO_FRETE_ID,
    STATUS,
    nvl(CENTRO_CUSTO_ID, 1),
    trunc(DATA_HORA_EMISSAO)
  into
    vFornecedorId,
    vEmpresaId,
    vPlanoFinanceiroId,
    vConhecimentoFreteId,
    vStatus,
    vCentroCustoId,
    vDataEmissao
  from
    ENTRADAS_NOTAS_FISCAIS
  where ENTRADA_ID = iENTRADA_ID;
  
  if vStatus <> 'ECO' then
    ERRO('Somente entrada com o estoque consolidado pode ter o financeiro consolidado!');
  end if;

  for xTitulos in cTitulos loop
  
    select SEQ_PAGAR_ID.nextval 
    into vPagarId 
    from dual;

    insert into CONTAS_PAGAR(
      PAGAR_ID,
      CADASTRO_ID,
      EMPRESA_ID,
      DOCUMENTO,
      ORIGEM,      
      ENTRADA_ID,
      COBRANCA_ID,
      PORTADOR_ID,
      PLANO_FINANCEIRO_ID,
      DATA_VENCIMENTO,
      DATA_VENCIMENTO_ORIGINAL,
      VALOR_DOCUMENTO,
      STATUS,      
      PARCELA,
      NUMERO_PARCELAS,
      CENTRO_CUSTO_ID,
      DATA_EMISSAO,
      DATA_CONTABIL
    )values(
      vPagarId,
      case when xTitulos.TIPO = 'O' then xTitulos.FORNECEDOR_ID else vFornecedorId end,
      vEmpresaId,      
      xTitulos.DOCUMENTO,
      'ENT', -- Tipo de movimento
      iENTRADA_ID,
      xTitulos.COBRANCA_ID, -- Tipo de cobrança previsão
      xTitulos.PORTADOR_ID,
      case when xTitulos.TIPO = 'O' then xTitulos.PLANO_FINANCEIRO_ID else vPlanoFinanceiroId end,
      xTitulos.DATA_VENCIMENTO,
      xTitulos.DATA_VENCIMENTO,
      xTitulos.VALOR_DOCUMENTO, -- Valor do financeiro
      'A',      
      xTitulos.PARCELA,
      xTitulos.NUMERO_PARCELAS,
      case when xTitulos.TIPO = 'O' then xTitulos.CENTRO_CUSTO_ID else vCentroCustoId end,
      vDataEmissao,
      vDataEmissao
    );  
  
  end loop;
  
  select
    count(PAGAR_ID)
  into
    vQtde
  from
    CONTAS_PAGAR
  where CONHECIMENTO_FRETE_ID = vConhecimentoFreteId;  
  
  /* Criandos os financeiros do conhecimento de frete caso necessário */
  /* Pode ser que alguma nota já os criou */
  if vConhecimentoFreteId is not null and vQtde = 0 then  
    select
      TRANSPORTADORA_ID,
      EMPRESA_ID
    into
      vTransportadoraId,
      vEmpresaConhecimentoId
    from
      CONHECIMENTOS_FRETES
    where CONHECIMENTO_ID = vConhecimentoFreteId;
  
    for xTitulos in cTitulosConhecimento(vConhecimentoFreteId) loop
    
      select SEQ_PAGAR_ID.nextval 
      into vPagarId 
      from dual;

      insert into CONTAS_PAGAR(
        PAGAR_ID,
        CADASTRO_ID,
        EMPRESA_ID,
        DOCUMENTO,
        ORIGEM,      
        CONHECIMENTO_FRETE_ID,
        COBRANCA_ID,
        PORTADOR_ID,
        PLANO_FINANCEIRO_ID,
        DATA_VENCIMENTO,
        DATA_VENCIMENTO_ORIGINAL,
        VALOR_DOCUMENTO,
        STATUS,      
        PARCELA,
        NUMERO_PARCELAS
      )values(
        vPagarId,
        vTransportadoraId,
        vEmpresaConhecimentoId,      
        xTitulos.DOCUMENTO,
        'CON', -- Tipo de movimento
        vConhecimentoFreteId,
        xTitulos.COBRANCA_ID, -- Tipo de cobrança previsão
        xTitulos.PORTADOR_ID,
        vPlanoFinanceiroId,
        xTitulos.DATA_VENCIMENTO,
        xTitulos.DATA_VENCIMENTO,
        xTitulos.VALOR, -- Valor do financeiro
        'A',      
        xTitulos.PARCELA,
        xTitulos.NUMERO_PARCELAS
      );  
    
    end loop;
  
  end if;
  
  update ENTRADAS_NOTAS_FISCAIS set
    STATUS = 'BAI'
  where ENTRADA_ID = iENTRADA_ID;
  
end CONS_FINANC_ENTR_NOTA_FISCAL;
/