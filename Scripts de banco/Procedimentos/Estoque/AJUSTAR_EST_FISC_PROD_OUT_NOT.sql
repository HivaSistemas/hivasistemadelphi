create or replace procedure AJUSTAR_EST_FISC_PROD_OUT_NOT
is

  cursor cProdutos is
  select
    NOF.NOTA_FISCAL_ID,
    NOF.EMPRESA_ID,
    NFI.PRODUTO_ID,
    SUM(
      case
        when SUBSTR(NFI.CFOP_ID, 1,1) in('1','2','3')
        then NFI.QUANTIDADE
        else NFI.QUANTIDADE * -1
      end) as QUANTIDADE
  from NOTAS_FISCAIS_ITENS NFI

  inner join NOTAS_FISCAIS NOF
  on NOF.NOTA_FISCAL_ID = NFI.NOTA_FISCAL_ID
  and NOF.TIPO_MOVIMENTO = 'OUT'
  and NOF.MOVIMENTAR_EST_OUTRAS_NOTAS = 'N'

  group by
    NOF.NOTA_FISCAL_ID,
    NOF.EMPRESA_ID,
    NFI.PRODUTO_ID;

begin

  for xProdutos in cProdutos loop
    MOVIMENTAR_ESTOQUE_FISCAL(
      xProdutos.EMPRESA_ID,
      xProdutos.PRODUTO_ID,
      xProdutos.QUANTIDADE
    );
  end loop;

  for xProdutos in cProdutos loop
    update
      NOTAS_FISCAIS
    set MOVIMENTAR_EST_OUTRAS_NOTAS = 'S'

    where EMPRESA_ID = xProdutos.EMPRESA_ID
    and NOTA_FISCAL_ID = xProdutos.NOTA_FISCAL_ID;
  end loop;

end AJUSTAR_EST_FISC_PROD_OUT_NOT;
