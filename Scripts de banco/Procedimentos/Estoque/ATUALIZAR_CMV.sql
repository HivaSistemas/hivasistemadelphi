﻿create or replace procedure ATUALIZAR_CMV(
  iEMPRESA_ID    in number,
  iPRODUTO_ID    in number,
  iPRECO_FINAL   in number,  
  iQUANTIDADE    in number
)
is
  vProdutoPaiId  PRODUTOS.PRODUTO_PAI_ID%type;
  vQtdeVezesPai  PRODUTOS.QUANTIDADE_VEZES_PAI%type;  
  
  vCMVAtual               number(20,4);  
  vDisponivelAtual        ESTOQUES.DISPONIVEL%type;
begin

select
    QUANTIDADE_VEZES_PAI,
    PRODUTO_PAI_ID
  into
    vQtdeVezesPai,
    vProdutoPaiId
  from
    PRODUTOS
  where PRODUTO_ID = iPRODUTO_ID;

  begin
    select            
      case when EST.DISPONIVEL <= 0 then 0 else EST.DISPONIVEL * vQtdeVezesPai * CUS.CMV end,
      case when EST.DISPONIVEL <= 0 then 0 else EST.DISPONIVEL end
    into            
      vCMVAtual,
      vDisponivelAtual
    from
      ESTOQUES EST

    inner join CUSTOS_PRODUTOS CUS
    on EST.EMPRESA_ID = CUS.EMPRESA_ID
    and EST.PRODUTO_ID = CUS.PRODUTO_ID

    where EST.PRODUTO_ID = vProdutoPaiId
    and EST.EMPRESA_ID = iEMPRESA_ID;
  exception
    when others then
      erro(
        'Problemas na busca do CMV. Produto ' || vProdutoPaiId || chr(13) ||
        sqlerrm
      );
  end;
  
  begin
    if vDisponivelAtual + iQUANTIDADE <= 0 then
      vCMVAtual := 0;
    else
      vCMVAtual := (vCMVAtual + ( iPRECO_FINAL * iQUANTIDADE )) / ( vDisponivelAtual + iQUANTIDADE );	 
	  
	  if vCMVAtual <= 0 then
	    vCMVAtual := 0;
	  end if;	
    end if;
  exception
    when others then
      erro(
        'Problemas no calculo do CMV. Produto ' || vProdutoPaiId || chr(13) ||
        sqlerrm
      );
  end;
   
  begin
    update CUSTOS_PRODUTOS set      
      CMV = vCMVAtual,
      CMV_ANTERIOR = CMV
    where EMPRESA_ID = iEMPRESA_ID
    and PRODUTO_ID = vProdutoPaiId;
  exception
    when others then
      ERRO(
        'Problemas ao consolidar os novos custos medio. Produto ' || vProdutoPaiId || chr(13) || 
        sqlerrm
      );
  end;  

end ATUALIZAR_CMV;
