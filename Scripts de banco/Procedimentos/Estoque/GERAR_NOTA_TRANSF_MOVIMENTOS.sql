﻿create or replace procedure GERAR_NOTA_TRANSF_MOVIMENTOS(
  iEMPRESA_ORIGEM_ID in number,
  iEMPRESA_DESTINO_ID in number,
  iDATA_INICIAL in date,
  iDATA_FINAL in date,
  oNOTA_FISCAL_ID out number
)
is
  i                       number default 0;
  vQtde                   number default 0;
  vValorMaisSignificativo number default 0;

  vNotaFiscalId           NOTAS_FISCAIS.NOTA_FISCAL_ID%type;
  vSerieNota              NOTAS_FISCAIS.SERIE_NOTA%type;

  vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;
  vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;
  vDadosNota              RecordsNotasFiscais.RecNotaFiscal;
  vDadosItens             RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;

  vRegimeTributario       PARAMETROS_EMPRESA.REGIME_TRIBUTARIO%type;
  vSerieNFe               PARAMETROS_EMPRESA.SERIE_NFE%type;
  vTipoEmpresaOrigem      EMPRESAS.TIPO_EMPRESA%type;

  cursor cItens is
  select
    PRODUTO_ID,
    sum(QUANTIDADE) as QUANTIDADE,
    NOME_PRODUTO,
    PRECO_UNITARIO,
    CODIGO_NCM,
    UNIDADE_VENDA,
    CODIGO_BARRAS,
    CEST
  from
    VW_MOV_ITE_PEND_EMISSAO_NF_TR
  where EMPRESA_ORIGEM_ID = iEMPRESA_ORIGEM_ID
  and EMPRESA_DESTINO_ID = iEMPRESA_DESTINO_ID
  and DATA_MOVIMENTO between iDATA_INICIAL and iDATA_FINAL
  group by
    PRODUTO_ID,
    NOME_PRODUTO,
    PRECO_UNITARIO,
    CODIGO_NCM,
    UNIDADE_VENDA,
    CODIGO_BARRAS,
    CEST;

begin
  vDadosNota.EMPRESA_ID := iEMPRESA_ORIGEM_ID;

  begin
    select
      CAD.CADASTRO_ID,

      /* Emitente */
      EMP.RAZAO_SOCIAL,
      EMP.NOME_FANTASIA,
      EMP.CNPJ,
      EMP.INSCRICAO_ESTADUAL,
      EMP.LOGRADOURO,
      EMP.COMPLEMENTO,
      BAE.NOME as NOME_BAIRRO_EMITENTE,
      CIE.NOME as NOME_CIDADE_EMITENTE,
      EMP.NUMERO,
      ESE.ESTADO_ID,
      EMP.CEP,
      EMP.TELEFONE_PRINCIPAL,
      CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,
      ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,

      /* Destinatario */
      CAD.NOME_FANTASIA,
      CAD.RAZAO_SOCIAL,
      CAD.TIPO_PESSOA,
      CAD.CPF_CNPJ,
      CAD.INSCRICAO_ESTADUAL,
      CAD.LOGRADOURO,
      CAD.COMPLEMENTO,
      CAD.NUMERO,
      CAD.CEP,
      BAI.NOME as NOME_BAIRRO,
      CID.NOME as NOME_CIDADE,
      CID.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_DEST,
      CID.ESTADO_ID as ESTADO_ID_DESTINATARIO,

      PAE.REGIME_TRIBUTARIO,
      PAE.SERIE_NFE,
      EMP.TIPO_EMPRESA
    into
      vDadosNota.CADASTRO_ID,

      /* Emitente */
      vDadosNota.RAZAO_SOCIAL_EMITENTE,
      vDadosNota.NOME_FANTASIA_EMITENTE,
      vDadosNota.CNPJ_EMITENTE,
      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,
      vDadosNota.LOGRADOURO_EMITENTE,
      vDadosNota.COMPLEMENTO_EMITENTE,
      vDadosNota.NOME_BAIRRO_EMITENTE,
      vDadosNota.NOME_CIDADE_EMITENTE,
      vDadosNota.NUMERO_EMITENTE,
      vDadosNota.ESTADO_ID_EMITENTE,
      vDadosNota.CEP_EMITENTE,
      vDadosNota.TELEFONE_EMITENTE,
      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,
      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,

      /* Destinatario */
      vDadosNota.NOME_FANTASIA_DESTINATARIO,
      vDadosNota.RAZAO_SOCIAL_DESTINATARIO,
      vDadosNota.TIPO_PESSOA_DESTINATARIO,
      vDadosNota.CPF_CNPJ_DESTINATARIO,
      vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,
      vDadosNota.LOGRADOURO_DESTINATARIO,
      vDadosNota.COMPLEMENTO_DESTINATARIO,
      vDadosNota.NUMERO_DESTINATARIO,
      vDadosNota.CEP_DESTINATARIO,
      vDadosNota.NOME_BAIRRO_DESTINATARIO,
      vDadosNota.NOME_CIDADE_DESTINATARIO,
      vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,
      vDadosNota.ESTADO_ID_DESTINATARIO,

      /* Parâmetros da empresa emitente */
      vRegimeTributario,
      vSerieNFe,
      vTipoEmpresaOrigem
    from
      EMPRESAS EMP

    inner join EMPRESAS EMD
    on EMD.EMPRESA_ID = iEMPRESA_DESTINO_ID

    inner join CADASTROS CAD
    on EMD.CADASTRO_ID = CAD.CADASTRO_ID

    inner join BAIRROS BAI
    on CAD.BAIRRO_ID = BAI.BAIRRO_ID

    inner join CIDADES CID
    on BAI.CIDADE_ID = CID.CIDADE_ID

    inner join BAIRROS BAE
    on EMP.BAIRRO_ID = BAE.BAIRRO_ID

    inner join CIDADES CIE
    on BAE.CIDADE_ID = CIE.CIDADE_ID

    inner join ESTADOS ESE
    on CIE.ESTADO_ID = ESE.ESTADO_ID

    inner join PARAMETROS_EMPRESA PAE
    on PAE.EMPRESA_ID = EMP.EMPRESA_ID

    cross join PARAMETROS PAR

    where EMP.EMPRESA_ID = iEMPRESA_ORIGEM_ID;
  exception
    when others then
      ERRO('Houve um problema ao buscar os dados das empresas para geração da NFe! ' + sqlerrm);
  end;

  vDadosNota.BASE_CALCULO_ICMS    := 0;
  vDadosNota.VALOR_ICMS           := 0;
  vDadosNota.BASE_CALCULO_ICMS_ST := 0;
  vDadosNota.VALOR_ICMS_ST        := 0;
  vDadosNota.BASE_CALCULO_PIS     := 0;
  vDadosNota.VALOR_PIS            := 0;
  vDadosNota.BASE_CALCULO_COFINS  := 0;
  vDadosNota.VALOR_COFINS         := 0;
  vDadosNota.VALOR_IPI            := 0;
  vDadosNota.VALOR_FRETE          := 0;
  vDadosNota.VALOR_SEGURO         := 0;
  vDadosNota.PESO_LIQUIDO         := 0;
  vDadosNota.PESO_BRUTO           := 0;

  for vItens in cItens loop
    if length(vItens.CODIGO_NCM) < 8 then
      Erro('O código NCM do produto ' || vItens.PRODUTO_ID || ' - ' || vItens.NOME_PRODUTO || ' está incorreto, o NCM é composto de 8 digítos, verifique no cadastro de produtos!');
    end if;

    vDadosItens(i).PRODUTO_ID    := vItens.PRODUTO_ID;
    vDadosItens(i).ITEM_ID       := i + 1;
    vDadosItens(i).NOME_PRODUTO  := vItens.NOME_PRODUTO;
    vDadosItens(i).UNIDADE       := vItens.UNIDADE_VENDA;
    vDadosItens(i).CODIGO_NCM    := vItens.CODIGO_NCM;
    vDadosItens(i).QUANTIDADE    := vItens.QUANTIDADE;
    vDadosItens(i).CODIGO_BARRAS := vItens.CODIGO_BARRAS;
    vDadosItens(i).CEST          := vItens.CEST;

    vDadosItens(i).PRECO_UNITARIO              := vItens.PRECO_UNITARIO;
    vDadosItens(i).VALOR_TOTAL                 := round(vDadosItens(i).PRECO_UNITARIO * vItens.QUANTIDADE, 2);
    vDadosItens(i).VALOR_TOTAL_DESCONTO        := 0;
    vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := 0;

    /* Buscando os dados de impostos item a item da venda */
    BUSCAR_CALC_IMPOSTOS_PRODUTO(
      vItens.PRODUTO_ID,
      vDadosNota.EMPRESA_ID,
      vDadosNota.CADASTRO_ID,

      vDadosItens(i).VALOR_TOTAL,
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,
      vDadosItens(i).VALOR_TOTAL_DESCONTO,

      vDadosItens(i).CST,
      vDadosItens(i).BASE_CALCULO_ICMS,
      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,
      vDadosItens(i).PERCENTUAL_ICMS,
      vDadosItens(i).VALOR_ICMS,

      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,
      vDadosItens(i).BASE_CALCULO_ICMS_ST,
      vDadosItens(i).PERCENTUAL_ICMS_ST,
      vDadosItens(i).VALOR_ICMS_ST,
      vDadosItens(i).PRECO_PAUTA,
      vDadosItens(i).IVA,

      vDadosItens(i).VALOR_IPI,
      vDadosItens(i).PERCENTUAL_IPI,

      vDadosItens(i).CST_PIS,
      vDadosItens(i).BASE_CALCULO_PIS,
      vDadosItens(i).PERCENTUAL_PIS,
      vDadosItens(i).VALOR_PIS,

      vDadosItens(i).CST_COFINS,
      vDadosItens(i).BASE_CALCULO_COFINS,
      vDadosItens(i).PERCENTUAL_COFINS,
      vDadosItens(i).VALOR_COFINS,

      vDadosItens(i).BASE_CALCULO_ICMS_INTER,
      vDadosItens(i).PERCENTUAL_ICMS_INTER,
      vDadosItens(i).VALOR_ICMS_INTER,
      vDadosNota.ESTADO_ID_EMITENTE
		);

    /* Se for para deposito fechado, zerando os impostos */
    if vTipoEmpresaOrigem = 'D' then
      vDadosItens(i).CST                  := '41';
      vDadosItens(i).CST_PIS              := '08';
      vDadosItens(i).CST_COFINS           := '08';

      vDadosItens(i).BASE_CALCULO_ICMS    := 0;
      vDadosItens(i).VALOR_ICMS           := 0;
      vDadosItens(i).BASE_CALCULO_ICMS_ST := 0;
      vDadosItens(i).VALOR_ICMS_ST        := 0;

      vDadosItens(i).BASE_CALCULO_PIS     := 0;
      vDadosItens(i).PERCENTUAL_PIS       := 0;
      vDadosItens(i).VALOR_PIS            := 0;

      vDadosItens(i).BASE_CALCULO_COFINS  := 0;
      vDadosItens(i).PERCENTUAL_COFINS    := 0;
      vDadosItens(i).VALOR_COFINS         := 0;

      vDadosItens(i).CFOP_ID := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vDadosItens(i).CST, 'RRD', 'I');
    else
      vDadosItens(i).CFOP_ID := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vDadosItens(i).CST, 'TPE', 'I');
    end if;

		vDadosItens(i).NATUREZA_OPERACAO := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);

    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */
    vDadosNota.BASE_CALCULO_ICMS    := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;
    vDadosNota.VALOR_ICMS           := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;
    vDadosNota.BASE_CALCULO_ICMS_ST := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;
    vDadosNota.VALOR_ICMS_ST        := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;
    vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;
    vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;
    vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;
    vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;

    vDadosNota.VALOR_TOTAL :=
      vDadosNota.VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
      vDadosItens(i).VALOR_ICMS_ST +
      vDadosItens(i).VALOR_IPI -
      vDadosItens(i).VALOR_TOTAL_DESCONTO;

    vDadosNota.VALOR_TOTAL_PRODUTOS  := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;
    vDadosNota.VALOR_DESCONTO        := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;
    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;
    /* ------------------------------------------------------------------------------------------------------ */

		if
      vDadosItens(i).VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
      vDadosItens(i).VALOR_ICMS_ST +
      vDadosItens(i).VALOR_IPI -
      vDadosItens(i).VALOR_TOTAL_DESCONTO >
      vValorMaisSignificativo
    then
			vValorMaisSignificativo :=
        vDadosItens(i).VALOR_TOTAL +
        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
        vDadosItens(i).VALOR_ICMS_ST +
        vDadosItens(i).VALOR_IPI -
        vDadosItens(i).VALOR_TOTAL_DESCONTO;

			vCfopIdCapa 	  	:= vDadosItens(i).CFOP_ID;
			vNaturezaOperacao := vDadosItens(i).NATUREZA_OPERACAO;
		end if;

    i := i + 1;
  end loop;

  if vDadosItens.count < 1 then
    ERRO('Os produtos da nota fiscal não foram encontrados!');
  end if;

	if vCfopIdCapa is null then
		vCfopIdCapa 			:= vDadosItens(0).CFOP_ID;
		vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;
	end if;

	vDadosNota.CFOP_ID           := vCfopIdCapa;
  vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;

  vDadosNota.VALOR_RECEBIDO_COBRANCA := vDadosNota.VALOR_TOTAL;

  vSerieNota  := to_char(nvl(vSerieNFe, 1));

  if vTipoEmpresaOrigem = 'D' then
    vDadosNota.INFORMACOES_COMPLEMENTARES := 'Não incidencia do ICMS, nos termos do Art. 79, inciso I, "j" e Art. 19 do Anexo XII do RCTE-GO';
  else
    vDadosNota.INFORMACOES_COMPLEMENTARES := 'Transferência de produtos para suprir ESTOQUE.';
  end if;

  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;

	insert into NOTAS_FISCAIS(
    NOTA_FISCAL_ID,
    CADASTRO_ID,
    CFOP_ID,
    EMPRESA_ID,
		MODELO_NOTA,
		SERIE_NOTA,
    NATUREZA_OPERACAO,
		STATUS,
    TIPO_MOVIMENTO,
    RAZAO_SOCIAL_EMITENTE,
    NOME_FANTASIA_EMITENTE,
    REGIME_TRIBUTARIO,
    CNPJ_EMITENTE,
    INSCRICAO_ESTADUAL_EMITENTE,
    LOGRADOURO_EMITENTE,
    COMPLEMENTO_EMITENTE,
    NOME_BAIRRO_EMITENTE,
    NOME_CIDADE_EMITENTE,
    NUMERO_EMITENTE,
    ESTADO_ID_EMITENTE,
    CEP_EMITENTE,
    TELEFONE_EMITENTE,
    CODIGO_IBGE_MUNICIPIO_EMIT,
    CODIGO_IBGE_ESTADO_EMITENT,
    NOME_FANTASIA_DESTINATARIO,
    RAZAO_SOCIAL_DESTINATARIO,
    TIPO_PESSOA_DESTINATARIO,
    CPF_CNPJ_DESTINATARIO,
    INSCRICAO_ESTADUAL_DESTINAT,
    NOME_CONSUMIDOR_FINAL,
    TELEFONE_CONSUMIDOR_FINAL,
    TIPO_NOTA,
    LOGRADOURO_DESTINATARIO,
    COMPLEMENTO_DESTINATARIO,
    NOME_BAIRRO_DESTINATARIO,
    NOME_CIDADE_DESTINATARIO,
    ESTADO_ID_DESTINATARIO,
    CEP_DESTINATARIO,
    NUMERO_DESTINATARIO,
    CODIGO_IBGE_MUNICIPIO_DEST,
    VALOR_TOTAL,
    VALOR_TOTAL_PRODUTOS,
    VALOR_DESCONTO,
    VALOR_OUTRAS_DESPESAS,
    BASE_CALCULO_ICMS,
    VALOR_ICMS,
    BASE_CALCULO_ICMS_ST,
    VALOR_ICMS_ST,
    BASE_CALCULO_PIS,
    VALOR_PIS,
    BASE_CALCULO_COFINS,
    VALOR_COFINS,
    VALOR_IPI,
    VALOR_FRETE,
    VALOR_SEGURO,
    VALOR_RECEBIDO_COBRANCA,
    PESO_LIQUIDO,
    PESO_BRUTO,
		INFORMACOES_COMPLEMENTARES
  )values(
		vNotaFiscalId,
    vDadosNota.CADASTRO_ID,
    vDadosNota.CFOP_ID,
    vDadosNota.EMPRESA_ID,
		'55',
		vSerieNota,
    vDadosNota.NATUREZA_OPERACAO,
		'N',
    'TFI',
    vDadosNota.RAZAO_SOCIAL_EMITENTE,
    vDadosNota.NOME_FANTASIA_EMITENTE,
    vRegimeTributario,
    vDadosNota.CNPJ_EMITENTE,
    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,
    vDadosNota.LOGRADOURO_EMITENTE,
    vDadosNota.COMPLEMENTO_EMITENTE,
    vDadosNota.NOME_BAIRRO_EMITENTE,
    vDadosNota.NOME_CIDADE_EMITENTE,
    vDadosNota.NUMERO_EMITENTE,
    vDadosNota.ESTADO_ID_EMITENTE,
    vDadosNota.CEP_EMITENTE,
    vDadosNota.TELEFONE_EMITENTE,
    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,
    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,
    vDadosNota.NOME_FANTASIA_DESTINATARIO,
    vDadosNota.RAZAO_SOCIAL_DESTINATARIO,
    vDadosNota.TIPO_PESSOA_DESTINATARIO,
    vDadosNota.CPF_CNPJ_DESTINATARIO,
    vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,
    vDadosNota.NOME_CONSUMIDOR_FINAL,
    vDadosNota.TELEFONE_CONSUMIDOR_FINAL,
    'N',
    vDadosNota.LOGRADOURO_DESTINATARIO,
    vDadosNota.COMPLEMENTO_DESTINATARIO,
    vDadosNota.NOME_BAIRRO_DESTINATARIO,
    vDadosNota.NOME_CIDADE_DESTINATARIO,
    vDadosNota.ESTADO_ID_DESTINATARIO,
    vDadosNota.CEP_DESTINATARIO,
    vDadosNota.NUMERO_DESTINATARIO,
    vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,
    vDadosNota.VALOR_TOTAL,
    vDadosNota.VALOR_TOTAL_PRODUTOS,
    vDadosNota.VALOR_DESCONTO,
    vDadosNota.VALOR_OUTRAS_DESPESAS,
    vDadosNota.BASE_CALCULO_ICMS,
    vDadosNota.VALOR_ICMS,
    vDadosNota.BASE_CALCULO_ICMS_ST,
    vDadosNota.VALOR_ICMS_ST,
    vDadosNota.BASE_CALCULO_PIS,
    vDadosNota.VALOR_PIS,
    vDadosNota.BASE_CALCULO_COFINS,
    vDadosNota.VALOR_COFINS,
    vDadosNota.VALOR_IPI,
    vDadosNota.VALOR_FRETE,
    vDadosNota.VALOR_SEGURO,
    vDadosNota.VALOR_RECEBIDO_COBRANCA,
    vDadosNota.PESO_LIQUIDO,
    vDadosNota.PESO_BRUTO,
		vDadosNota.INFORMACOES_COMPLEMENTARES
  );

  for i in 0..vDadosItens.count - 1 loop
    insert into NOTAS_FISCAIS_ITENS(
      NOTA_FISCAL_ID,
      PRODUTO_ID,
      ITEM_ID,
      NOME_PRODUTO,
      UNIDADE,
      CFOP_ID,
      CST,
      CODIGO_NCM,
      VALOR_TOTAL,
      PRECO_UNITARIO,
      QUANTIDADE,
      VALOR_TOTAL_DESCONTO,
      VALOR_TOTAL_OUTRAS_DESPESAS,
      BASE_CALCULO_ICMS,
      PERCENTUAL_ICMS,
      VALOR_ICMS,
      BASE_CALCULO_ICMS_ST,
      VALOR_ICMS_ST,
      CST_PIS,
      BASE_CALCULO_PIS,
      PERCENTUAL_PIS,
      VALOR_PIS,
      CST_COFINS,
      BASE_CALCULO_COFINS,
      PERCENTUAL_COFINS,
      VALOR_COFINS,
      INDICE_REDUCAO_BASE_ICMS,
      IVA,
      PRECO_PAUTA,
      CODIGO_BARRAS,
      VALOR_IPI,
      PERCENTUAL_IPI,
      INDICE_REDUCAO_BASE_ICMS_ST,
      PERCENTUAL_ICMS_ST,
      CEST
    )values(
      vNotaFiscalId,
      vDadosItens(i).PRODUTO_ID,
      vDadosItens(i).ITEM_ID,
      vDadosItens(i).NOME_PRODUTO,
      vDadosItens(i).UNIDADE,
      vDadosItens(i).CFOP_ID,
      vDadosItens(i).CST,
      vDadosItens(i).CODIGO_NCM,
      vDadosItens(i).VALOR_TOTAL,
      vDadosItens(i).PRECO_UNITARIO,
      vDadosItens(i).QUANTIDADE,
      vDadosItens(i).VALOR_TOTAL_DESCONTO,
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,
      vDadosItens(i).BASE_CALCULO_ICMS,
      vDadosItens(i).PERCENTUAL_ICMS,
      vDadosItens(i).VALOR_ICMS,
      vDadosItens(i).BASE_CALCULO_ICMS_ST,
      vDadosItens(i).VALOR_ICMS_ST,
      vDadosItens(i).CST_PIS,
      vDadosItens(i).BASE_CALCULO_PIS,
      vDadosItens(i).PERCENTUAL_PIS,
      vDadosItens(i).VALOR_PIS,
      vDadosItens(i).CST_COFINS,
      vDadosItens(i).BASE_CALCULO_COFINS,
      vDadosItens(i).PERCENTUAL_COFINS,
      vDadosItens(i).VALOR_COFINS,
      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,
      vDadosItens(i).IVA,
      vDadosItens(i).PRECO_PAUTA,
      vDadosItens(i).CODIGO_BARRAS,
      vDadosItens(i).VALOR_IPI,
      vDadosItens(i).PERCENTUAL_IPI,
      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,
      vDadosItens(i).PERCENTUAL_ICMS_ST,
      vDadosItens(i).CEST
    );
  end loop;

  update RETIRADAS set
    NOTA_TRANSF_ESTOQUE_FISCAL_ID = vNotaFiscalId
  where RETIRADA_ID in(
    select
      MOVIMENTO_ID
    from
      VW_MOV_PEND_EMISSAO_NF_TRANSF
    where EMPRESA_ORIGEM_ID = iEMPRESA_ORIGEM_ID
    and trunc(DATA_HORA_EMISSAO) between iDATA_INICIAL and iDATA_FINAL
    and TIPO_MOVIMENTO in('VRE', 'VRA')
  );

  update ENTREGAS set
    NOTA_TRANSF_ESTOQUE_FISCAL_ID = vNotaFiscalId
  where ENTREGA_ID in(
    select
      MOVIMENTO_ID
    from
      VW_MOV_PEND_EMISSAO_NF_TRANSF
    where EMPRESA_ORIGEM_ID = iEMPRESA_ORIGEM_ID
    and trunc(DATA_HORA_EMISSAO) between iDATA_INICIAL and iDATA_FINAL
    and TIPO_MOVIMENTO = 'VEN'
  );

  oNOTA_FISCAL_ID := vNotaFiscalId;

end GERAR_NOTA_TRANSF_MOVIMENTOS;
/
