﻿create or replace function QUANTIDADE_KITS_DISP_GRUPO(
  iEMPRESA_ID     in number, 
  iPRODUTO_KIT_ID in number, 
  pTIPO_ENTREGA   in string
)
return number
is  
  vRetorno            number default 0;
  
  cursor cItensKit is
  select
    EMPRESA_ID,
    min(DISPONIVEL) as DISPONIVEL
  from (
    select
      GES.EMPRESA_GRUPO_ID as EMPRESA_ID,
      case when PRO.ACEITAR_ESTOQUE_NEGATIVO = 'S' then 999999 else sum(ZERO_NEGATIVO(DIV.DISPONIVEL) / PRK.QUANTIDADE) end as DISPONIVEL
    from
      PRODUTOS_KIT PRK

    inner join PRODUTOS PRO
    on PRK.PRODUTO_ID = PRO.PRODUTO_ID

    inner join GRUPOS_ESTOQUES GES
    on GES.EMPRESA_ID = iEMPRESA_ID
    and GES.TIPO = pTIPO_ENTREGA
    
    inner join ESTOQUES_DIVISAO DIV
    on GES.EMPRESA_GRUPO_ID = DIV.EMPRESA_ID
    and PRO.PRODUTO_PAI_ID = DIV.PRODUTO_ID

    where PRK.PRODUTO_KIT_ID = iPRODUTO_KIT_ID

    group by
      GES.EMPRESA_GRUPO_ID,
      DIV.PRODUTO_ID,
      PRO.ACEITAR_ESTOQUE_NEGATIVO
  )
  group by
    EMPRESA_ID;
    
begin   
  
  for xItens in cItensKit loop    
    /* trunc pois não existem kits quebrados */
    vRetorno := vRetorno + trunc(xItens.DISPONIVEL, 0);
  end loop;
  
  return vRetorno;

end QUANTIDADE_KITS_DISP_GRUPO;