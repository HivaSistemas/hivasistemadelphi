﻿create or replace procedure VER_BAIXAR_CANCELAR_COMPRA(iCOMPRA_ID in number)
is
  vQtdeProdutos   number;  
  vQtdeEntregues  number;
  vStatus         COMPRAS.STATUS%type;
begin

  AJUSTAR_PREVISAO_COMPRA_FINANC(iCOMPRA_ID);

  select
    count(PRODUTO_ID)
  into
    vQtdeProdutos
  from
    COMPRAS_ITENS
  where COMPRA_ID = iCOMPRA_ID
  and SALDO > 0;
  
  /* Verificando se ainda faltam produtos com saldo, se houver reabrindo o pedido */
  if vQtdeProdutos > 0 then
    update COMPRAS set
      STATUS = 'A'
    where COMPRA_ID = iCOMPRA_ID;  
  
    return;
  end if;
  
  select
    count(PRODUTO_ID)
  into
    vQtdeEntregues
  from
    COMPRAS_ITENS
  where COMPRA_ID = iCOMPRA_ID
  and ENTREGUES + BAIXADOS > 0;

  if vQtdeEntregues > 0 then
    vStatus := 'B';
  else
    vStatus := 'C';  
  end if;
  
  update COMPRAS set
    STATUS = vStatus
  where COMPRA_ID = iCOMPRA_ID;

end VER_BAIXAR_CANCELAR_COMPRA;
/