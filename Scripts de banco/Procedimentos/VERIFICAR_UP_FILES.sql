create or replace procedure VERIFICAR_UP_FILES(
  iNOME_ARQUIVO in string,
  iDATA_HORA_ARQUIVO in date
)
is
begin

  delete from UP_FILES
  where lower(NOME) = lower(iNOME_ARQUIVO);

  insert into UP_FILES(
    NOME,
    DATA_HORA_INSERCAO,
    DATA_HORA_ARQUIVO
  )values(
    lower(iNOME_ARQUIVO),
    sysdate,
    iDATA_HORA_ARQUIVO
  );

end VERIFICAR_UP_FILES;
/
