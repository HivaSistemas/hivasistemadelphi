create or replace procedure BAIXAR_ENTREGA(
  iENTREGA_ID      in number,
  iSTATUS          in string,
  
  iPRODUTOS_IDS    in TIPOS.ArrayOfNumeros,
  iITENS_IDS       in TIPOS.ArrayOfNumeros,  
  iLOTES           in TIPOS.ArrayOfString,
  iRETORNADOS      in TIPOS.ArrayOfNumeros  
)
is
  i            number;
  vQtde        number default 0;  

  vPrevisaoEntrega     ENTREGAS.PREVISAO_ENTREGA%type;
  vLocalId             ENTREGAS.LOCAL_ID%type;
  vEmpresaId           ENTREGAS.EMPRESA_ENTREGA_ID%type;
  vOrcamentoId         ENTREGAS.ORCAMENTO_ID%type;
  vPendenteRecebimento char(1);

begin

  select
    ENT.EMPRESA_ENTREGA_ID,
    ENT.LOCAL_ID,
    ENT.ORCAMENTO_ID,
    ENT.PREVISAO_ENTREGA,
    case when PAE.PERM_BAIXAR_ENT_REC_PENDENTE = 'N' and ORC.RECEBER_NA_ENTREGA = 'S' and ORC.STATUS <> 'RE' then 'S' else 'N' end as PENDENTE_RECEBIMENTO
  into
    vEmpresaId,
    vLocalId,
    vOrcamentoId,
    vPrevisaoEntrega,
    vPendenteRecebimento
  from
    ENTREGAS ENT

  inner join ORCAMENTOS ORC
  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID

  inner join PARAMETROS_EMPRESA PAE
  on ORC.EMPRESA_ID = PAE.EMPRESA_ID

  where ENT.ENTREGA_ID = iENTREGA_ID;

  if vPendenteRecebimento = 'S' and iSTATUS <> 'RTO' then
    ERRO('N�o � permitido realizar baixa de uma entrega onde o pedido � para recebimento na entrega e o mesmo ainda n�o foi recebido no caixa!');
  end if;

  if iSTATUS in('RPA', 'RTO') then
  
    for i in iITENS_IDS.first..iITENS_IDS.last loop 
      if iRETORNADOS(i) = 0 then
        continue;
      end if;
      
      update ENTREGAS_ITENS set
        RETORNADOS = iRETORNADOS(i)
      where ENTREGA_ID = iENTREGA_ID
      and LOTE = iLOTES(i)
      and ITEM_ID = iITENS_IDS(i);
      
      VERIFICAR_PENDENCIA_ENTREGA(vEmpresaId, vLocalId, vOrcamentoId, vPrevisaoEntrega, iSTATUS);
            
      INSERIR_ENTREGAS_ITENS_PEND(
        vEmpresaId,
        vLocalId,
        vOrcamentoId,
        vPrevisaoEntrega,
        iPRODUTOS_IDS(i),
        iITENS_IDS(i),
        iLOTES(i),
        iRETORNADOS(i)
      );      
    end loop;

    if iSTATUS = 'RTO' then
      delete from NOTAS_FISCAIS_ITENS
      where NOTA_FISCAL_ID in(
        select
          NOTA_FISCAL_ID
        from
          NOTAS_FISCAIS
        where ENTREGA_ID = iENTREGA_ID
        and NUMERO_NOTA is null
      );

      delete from NOTAS_FISCAIS
      where ENTREGA_ID = iENTREGA_ID
      and NUMERO_NOTA is null;
    end if;
  
  end if;

  GERAR_NOTA_RETORNO_ENTREGA(iENTREGA_ID);

end BAIXAR_ENTREGA;
/