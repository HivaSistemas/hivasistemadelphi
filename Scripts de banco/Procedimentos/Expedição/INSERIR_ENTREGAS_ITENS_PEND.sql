create or replace procedure INSERIR_ENTREGAS_ITENS_PEND(
  pEMPRESA_ID       in number,
  pLOCAL_ID         in number,
  pORCAMENTO_ID     in number,
  pPREVISAO_ENTREGA in date,
  pPRODUTO_ID       in number,
  pITEM_ID          in number,
  pLOTE             in string,
  pQUANTIDADE       in number
)
is
  vQtde number;
begin

  if SESSAO.ROTINA not in('BAIXAR_ENTREGA','AGENDAR_ITENS_SEM_PREVISAO', 'GERAR_ENT_PEND_RETIRA', 'ATU_CONF_SAIDA_ENTREGA') then
    update ORCAMENTOS_ITENS set
      QUANTIDADE_ENTREGAR = QUANTIDADE_ENTREGAR - pQUANTIDADE
    where ORCAMENTO_ID = pORCAMENTO_ID
    and ITEM_ID = pITEM_ID;
  end if;
  
  select
    count(*)
  into
    vQtde
  from
    ENTREGAS_ITENS_PENDENTES
  where EMPRESA_ID = pEMPRESA_ID
  and LOCAL_ID = pLOCAL_ID
  and ITEM_ID = pITEM_ID
  and ORCAMENTO_ID = pORCAMENTO_ID
  and PREVISAO_ENTREGA = pPREVISAO_ENTREGA
  and LOTE = pLOTE;
  
  if vQtde = 0 then
    begin
    insert into ENTREGAS_ITENS_PENDENTES(
      EMPRESA_ID,
      LOCAL_ID,
      ORCAMENTO_ID,
      PREVISAO_ENTREGA,
      PRODUTO_ID,
      ITEM_ID,
      LOTE,
      QUANTIDADE
    )values(
      pEMPRESA_ID,
      pLOCAL_ID,
      pORCAMENTO_ID,
      pPREVISAO_ENTREGA,
      pPRODUTO_ID,
      pITEM_ID,
      pLOTE,
      pQUANTIDADE
    );
    exception
      when others then
        ERRO('PROD ' || pPRODUTO_ID || '  LOTE ' || pLOTE);
    end;
  else
    update ENTREGAS_ITENS_PENDENTES set
      QUANTIDADE = SALDO + greatest(ENTREGUES - pQUANTIDADE, 0) + DEVOLVIDOS + pQUANTIDADE,
      ENTREGUES = greatest(ENTREGUES - pQUANTIDADE, 0)
    where EMPRESA_ID = pEMPRESA_ID
    and LOCAL_ID = pLOCAL_ID
    and ITEM_ID = pITEM_ID
    and ORCAMENTO_ID = pORCAMENTO_ID
    and PREVISAO_ENTREGA = pPREVISAO_ENTREGA
    and LOTE = pLOTE;
  end if;
    
end INSERIR_ENTREGAS_ITENS_PEND;
/