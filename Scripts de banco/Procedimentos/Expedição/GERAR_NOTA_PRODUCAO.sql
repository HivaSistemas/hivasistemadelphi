create or replace procedure GERAR_NOTA_PRODUCAO(
  iPRODUCAO_ID       in number,
  iNATUREZA_PRODUCAO in string,
  oNOTA_FISCAL_ID    out number,
  oTIPO_NOTA_GERAR   out string
)
is
  vIndice                 number default 0;
  vIndiceDescVenda        number default 1;
  vEmpresaId              number;

  i                       number default 0;
  vQtde                   number default 0;
  vValorMaisSignificativo number default 0;
  vItemId                 number;

  vNotaFiscalId           NOTAS_FISCAIS.NOTA_FISCAL_ID%type;
  vTipoNotaGerar          varchar2(2) default 'NE';
  vTipoMovimento          RETIRADAS.TIPO_MOVIMENTO%type;

  vModeloNota             NOTAS_FISCAIS.MODELO_NOTA%type;
  vSerieNota              NOTAS_FISCAIS.SERIE_NOTA%type;

  vValorTotalPedido       ORCAMENTOS.VALOR_TOTAL%type;

  vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;
  vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;
  vDadosNota              RecordsNotasFiscais.RecNotaFiscal;
  vDadosItens             RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;

  vRegimeTributario       PARAMETROS_EMPRESA.REGIME_TRIBUTARIO%type;
  vSerieNFe               PARAMETROS_EMPRESA.SERIE_NFE%type;

  vTipoOperacao           varchar(2) default 'I';

  cursor cItens(pEstadoId in string, pEmpresaNotaId in number) is
  select
    RIT.PRODUTO_ID,
    RIT.PRECO_UNITARIO,
    RIT.QUANTIDADE,
    RIT.PRECO_UNITARIO * RIT.QUANTIDADE as VALOR_TOTAL,
    0 as VALOR_TOTAL_DESCONTO,
    0 as VALOR_TOTAL_OUTRAS_DESPESAS,
    0 as VALOR_TOTAL_FRETE,
    CUS.CUSTO_ULTIMO_PEDIDO,
    CUS.PRECO_LIQUIDO,
    CUS.CMV,
    PRO.NOME as NOME_PRODUTO,
    PRO.CODIGO_NCM,
    PRO.UNIDADE_VENDA,
    PRO.CODIGO_BARRAS,
    PRO.CEST,
    nvl(IMP.PERC_ESTADUAL, 0) as PERC_ESTADUAL,
    nvl(IMP.PERC_FEDERAL_NACIONAL, 0) as PERC_FEDERAL,
    nvl(IMP.PERC_MUNICIPAL, 0) as PERC_MUNICIPAL
  from
    /* Este select e feito desta forma por causa dos LOTES! */
    (
      select
        PRODUCAO_ESTOQUE_ID,
        PRODUTO_ID,
        sum(QUANTIDADE) as QUANTIDADE,
        PRECO_UNITARIO,
        NATUREZA
      from
        PRODUCAO_ESTOQUE_ITENS
      where PRODUCAO_ESTOQUE_ID = iPRODUCAO_ID
      group by
        PRODUCAO_ESTOQUE_ID,
        PRODUTO_ID,
        PRECO_UNITARIO,
        NATUREZA
    ) RIT

  inner join PRODUCAO_ESTOQUE RET
  on RIT.PRODUCAO_ESTOQUE_ID = RET.PRODUCAO_ESTOQUE_ID

  inner join PRODUTOS PRO
  on RIT.PRODUTO_ID = PRO.PRODUTO_ID

  inner join VW_CUSTOS_PRODUTOS CUS
  on RIT.PRODUTO_ID = CUS.PRODUTO_ID
  and CUS.EMPRESA_ID = pEmpresaNotaId

  left join IMPOSTOS_IBPT IMP
  on PRO.CODIGO_NCM = IMP.CODIGO_NCM
  and IMP.ESTADO_ID = pEstadoId

  where RIT.PRODUCAO_ESTOQUE_ID = iPRODUCAO_ID
  and RIT.NATUREZA = iNATUREZA_PRODUCAO;

begin
  select
    count(*)
  into
    vQtde
  from
    NOTAS_FISCAIS
  where PRODUCAO_ESTOQUE_ID = iPRODUCAO_ID
  and NATUREZA_PRODUCAO = iNATUREZA_PRODUCAO;


  select
    EMPRESA_ID
  into
    vEmpresaId
  from
    PRODUCAO_ESTOQUE
  where PRODUCAO_ESTOQUE_ID = iPRODUCAO_ID;


  if vQtde > 0 then
    ERRO('A produ��o ja gerou nota!');
  end if;

  vIndiceDescVenda := 1;
  vIndice := 1;
  vTipoNotaGerar := 'NF';

  /* Buscando a empresa do redirecionamento de nota fiscal */
  vDadosNota.EMPRESA_ID := vEmpresaId;

  begin
    select
      /* Emitente */
      EMP.RAZAO_SOCIAL,
      EMP.NOME_FANTASIA,
      EMP.CNPJ,
      EMP.INSCRICAO_ESTADUAL,
      EMP.LOGRADOURO,
      EMP.COMPLEMENTO,
      BAE.NOME as NOME_BAIRRO_EMITENTE,
      CIE.NOME as NOME_CIDADE_EMITENTE,
      EMP.NUMERO,
      ESE.ESTADO_ID,
      EMP.CEP,
      EMP.TELEFONE_PRINCIPAL,
      CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,
      ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,
      EMP.CADASTRO_ID
    into
      /* Emitente */
      vDadosNota.RAZAO_SOCIAL_EMITENTE,
      vDadosNota.NOME_FANTASIA_EMITENTE,
      vDadosNota.CNPJ_EMITENTE,
      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,
      vDadosNota.LOGRADOURO_EMITENTE,
      vDadosNota.COMPLEMENTO_EMITENTE,
      vDadosNota.NOME_BAIRRO_EMITENTE,
      vDadosNota.NOME_CIDADE_EMITENTE,
      vDadosNota.NUMERO_EMITENTE,
      vDadosNota.ESTADO_ID_EMITENTE,
      vDadosNota.CEP_EMITENTE,
      vDadosNota.TELEFONE_EMITENTE,
      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,
      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,
      vDadosNota.CADASTRO_ID
    from
      EMPRESAS EMP

    inner join BAIRROS BAE
    on EMP.BAIRRO_ID = BAE.BAIRRO_ID

    inner join CIDADES CIE
    on BAE.CIDADE_ID = CIE.CIDADE_ID

    inner join ESTADOS ESE
    on CIE.ESTADO_ID = ESE.ESTADO_ID

    inner join PARAMETROS_EMPRESA PAE
    on PAE.EMPRESA_ID = vDadosNota.EMPRESA_ID

    cross join PARAMETROS PAR

    where EMP.EMPRESA_ID = vEmpresaId;
  exception
    when others then
      ERRO('Houve um erro ao buscar os dados da empresa para gerac�o dos dados de NF! ' + sqlerrm);
  end;

  vDadosNota.BASE_CALCULO_ICMS    := 0;
  vDadosNota.VALOR_ICMS           := 0;
  vDadosNota.BASE_CALCULO_ICMS_ST := 0;
  vDadosNota.VALOR_ICMS_ST        := 0;
  vDadosNota.BASE_CALCULO_PIS     := 0;
  vDadosNota.VALOR_PIS            := 0;
  vDadosNota.BASE_CALCULO_COFINS  := 0;
  vDadosNota.VALOR_COFINS         := 0;
  vDadosNota.VALOR_IPI            := 0;
  vDadosNota.VALOR_FRETE          := 0;
  vDadosNota.VALOR_SEGURO         := 0;
  vDadosNota.PESO_LIQUIDO         := 0;
  vDadosNota.PESO_BRUTO           := 0;

  vItemId := 0;
  for vItens in cItens(vDadosNota.ESTADO_ID_EMITENTE, vDadosNota.EMPRESA_ID) loop
    if length(vItens.CODIGO_NCM) < 8 then
      Erro('O codigo NCM do produto ' || vItens.PRODUTO_ID || ' - ' || vItens.NOME_PRODUTO || ' esta incorreto, o NCM e composto de 8 digitos, verifique no cadastro de produtos!');
    end if;

    vItemId := vItemId + 1;

    vDadosItens(i).PRODUTO_ID    := vItens.PRODUTO_ID;
    vDadosItens(i).ITEM_ID       := vItemId;
    vDadosItens(i).NOME_PRODUTO  := vItens.NOME_PRODUTO;
    vDadosItens(i).UNIDADE       := vItens.UNIDADE_VENDA;
    vDadosItens(i).CODIGO_NCM    := vItens.CODIGO_NCM;
    vDadosItens(i).QUANTIDADE    := vItens.QUANTIDADE;
    vDadosItens(i).CODIGO_BARRAS := vItens.CODIGO_BARRAS;
    vDadosItens(i).CEST          := vItens.CEST;

    vDadosItens(i).PRECO_UNITARIO              := trunc(vItens.PRECO_UNITARIO * vIndice, 2);
    vDadosItens(i).VALOR_TOTAL                 := trunc(vDadosItens(i).PRECO_UNITARIO * vItens.QUANTIDADE, 2);
    vDadosItens(i).VALOR_TOTAL_DESCONTO        := 0;
    vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := 0;

    /* Buscando os dados de impostos item a item da venda */
    BUSCAR_CALC_IMPOSTOS_PRODUTO(
      vItens.PRODUTO_ID,
      vDadosNota.EMPRESA_ID,
      vDadosNota.CADASTRO_ID,

      vDadosItens(i).VALOR_TOTAL,
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,
      vDadosItens(i).VALOR_TOTAL_DESCONTO,

      vDadosItens(i).CST,
      vDadosItens(i).BASE_CALCULO_ICMS,
      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,
      vDadosItens(i).PERCENTUAL_ICMS,
      vDadosItens(i).VALOR_ICMS,

      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,
      vDadosItens(i).BASE_CALCULO_ICMS_ST,
      vDadosItens(i).PERCENTUAL_ICMS_ST,
      vDadosItens(i).VALOR_ICMS_ST,
      vDadosItens(i).PRECO_PAUTA,
      vDadosItens(i).IVA,

      vDadosItens(i).VALOR_IPI,
      vDadosItens(i).PERCENTUAL_IPI,

      vDadosItens(i).CST_PIS,
      vDadosItens(i).BASE_CALCULO_PIS,
      vDadosItens(i).PERCENTUAL_PIS,
      vDadosItens(i).VALOR_PIS,

      vDadosItens(i).CST_COFINS,
      vDadosItens(i).BASE_CALCULO_COFINS,
      vDadosItens(i).PERCENTUAL_COFINS,
      vDadosItens(i).VALOR_COFINS,

      vDadosItens(i).BASE_CALCULO_ICMS_INTER,
      vDadosItens(i).PERCENTUAL_ICMS_INTER,
      vDadosItens(i).VALOR_ICMS_INTER,
      vDadosNota.ESTADO_ID_EMITENTE
    );

    vTipoOperacao := 'I';

    if iNATUREZA_PRODUCAO = 'E' then
      vDadosItens(i).CFOP_ID           := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vDadosItens(i).CST, 'PEN', vTipoOperacao);
    else
      vDadosItens(i).CFOP_ID           := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vDadosItens(i).CST, 'PSA', vTipoOperacao);
    end if;

    vDadosItens(i).NATUREZA_OPERACAO := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);

    /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */
    vDadosNota.BASE_CALCULO_ICMS    := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;
    vDadosNota.VALOR_ICMS           := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;
    vDadosNota.BASE_CALCULO_ICMS_ST := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;
    vDadosNota.VALOR_ICMS_ST        := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;
    vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;
    vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;
    vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;
    vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;

    vDadosNota.VALOR_TOTAL :=
      vDadosNota.VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL +
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
      vDadosItens(i).VALOR_ICMS_ST +
      vDadosItens(i).VALOR_IPI -
      vDadosItens(i).VALOR_TOTAL_DESCONTO;

    vDadosNota.VALOR_TOTAL_PRODUTOS := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;
    vDadosNota.VALOR_DESCONTO := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;
    vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;
    /* ------------------------------------------------------------------------------------------------------ */

    vCfopIdCapa       := vDadosItens(i).CFOP_ID;
    vNaturezaOperacao := vDadosItens(i).NATUREZA_OPERACAO;

    /* Calculando os tributos aproximados IBPT, Lei 12.741 de 2012 */
    vDadosNota.TOTAL_IMPOSTOS_ESTADUAL := vDadosNota.TOTAL_IMPOSTOS_ESTADUAL + round(vDadosItens(i).VALOR_TOTAL * vItens.PERC_ESTADUAL * 0.01, 2);
    vDadosNota.TOTAL_IMPOSTOS_FEDERAL  := vDadosNota.TOTAL_IMPOSTOS_FEDERAL + round(vDadosItens(i).VALOR_TOTAL * vItens.PERC_FEDERAL * 0.01, 2);

    i := i + 1;
  end loop;

  if vDadosItens.count < 1 then
    ERRO('Os produtos da nota fiscal n�o foram encontrados!');
  end if;

  if vCfopIdCapa is null then
    vCfopIdCapa       := vDadosItens(0).CFOP_ID;
    vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;
  end if;

  vDadosNota.CFOP_ID := vCfopIdCapa;
  vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;

  /* Ajustando as formas de pagamento da nota */
  vDadosNota.VALOR_RECEBIDO_DINHEIRO    := 0;
  vDadosNota.VALOR_RECEBIDO_PIX         := 0;
  vDadosNota.VALOR_RECEBIDO_CARTAO_CRED := 0;
  vDadosNota.VALOR_RECEBIDO_CARTAO_DEB  := 0;
  vDadosNota.VALOR_RECEBIDO_CREDITO     := 0;
  vDadosNota.VALOR_RECEBIDO_COBRANCA    := 0;
  vDadosNota.VALOR_RECEBIDO_CHEQUE      := 0;
  vDadosNota.VALOR_RECEBIDO_FINANCEIRA  := 0;
  vDadosNota.VALOR_RECEBIDO_ACUMULADO  := 0;

  select
    REGIME_TRIBUTARIO,
    SERIE_NFE
  into
    vRegimeTributario,
    vSerieNFe
  from
    PARAMETROS_EMPRESA
  where EMPRESA_ID = vDadosNota.EMPRESA_ID;

  vModeloNota := '55';
  vSerieNota  := to_char(nvl(vSerieNFe, 1));
  vTipoNotaGerar := 'N';

  vDadosNota.INFORMACOES_COMPLEMENTARES :=
    vDadosNota.INFORMACOES_COMPLEMENTARES || '. Trib. aprox. R$ ' || NFORMAT(vDadosNota.TOTAL_IMPOSTOS_FEDERAL, 2) || ' federal, R$ ' || NFORMAT(vDadosNota.TOTAL_IMPOSTOS_ESTADUAL, 2) || ' estadual. ';

  vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;

  insert into NOTAS_FISCAIS(
    NOTA_FISCAL_ID,
    CADASTRO_ID,
    CFOP_ID,
    EMPRESA_ID,
    ORCAMENTO_ID,
    ORCAMENTO_BASE_ID,
    PRODUCAO_ESTOQUE_ID,
    MODELO_NOTA,
    SERIE_NOTA,
    NATUREZA_OPERACAO,
    STATUS,
    TIPO_MOVIMENTO,
    RAZAO_SOCIAL_EMITENTE,
    NOME_FANTASIA_EMITENTE,
    REGIME_TRIBUTARIO,
    CNPJ_EMITENTE,
    INSCRICAO_ESTADUAL_EMITENTE,
    LOGRADOURO_EMITENTE,
    COMPLEMENTO_EMITENTE,
    NOME_BAIRRO_EMITENTE,
    NOME_CIDADE_EMITENTE,
    NUMERO_EMITENTE,
    ESTADO_ID_EMITENTE,
    CEP_EMITENTE,
    TELEFONE_EMITENTE,
    CODIGO_IBGE_MUNICIPIO_EMIT,
    CODIGO_IBGE_ESTADO_EMITENT,
    NOME_FANTASIA_DESTINATARIO,
    RAZAO_SOCIAL_DESTINATARIO,
    TIPO_PESSOA_DESTINATARIO,
    CPF_CNPJ_DESTINATARIO,
    INSCRICAO_ESTADUAL_DESTINAT,
    NOME_CONSUMIDOR_FINAL,
    CPF_CONSUMIDOR_FINAL,
    TELEFONE_CONSUMIDOR_FINAL,
    TIPO_NOTA,
    LOGRADOURO_DESTINATARIO,
    COMPLEMENTO_DESTINATARIO,
    NOME_BAIRRO_DESTINATARIO,
    NOME_CIDADE_DESTINATARIO,
    ESTADO_ID_DESTINATARIO,
    CEP_DESTINATARIO,
    NUMERO_DESTINATARIO,
    CODIGO_IBGE_MUNICIPIO_DEST,
    VALOR_TOTAL,
    VALOR_TOTAL_PRODUTOS,
    VALOR_DESCONTO,
    VALOR_OUTRAS_DESPESAS,
    BASE_CALCULO_ICMS,
    VALOR_ICMS,
    BASE_CALCULO_ICMS_ST,
    VALOR_ICMS_ST,
    BASE_CALCULO_PIS,
    VALOR_PIS,
    BASE_CALCULO_COFINS,
    VALOR_COFINS,
    VALOR_IPI,
    VALOR_FRETE,
    VALOR_SEGURO,
    VALOR_RECEBIDO_DINHEIRO,
    VALOR_RECEBIDO_PIX,
    VALOR_RECEBIDO_ACUMULADO,
    VALOR_RECEBIDO_CARTAO_CRED,
    VALOR_RECEBIDO_CARTAO_DEB,
    VALOR_RECEBIDO_CREDITO,
    VALOR_RECEBIDO_COBRANCA,
    VALOR_RECEBIDO_CHEQUE,
    VALOR_RECEBIDO_FINANCEIRA,
    PESO_LIQUIDO,
    PESO_BRUTO,
    INFORMACOES_COMPLEMENTARES
  )values(
    vNotaFiscalId,
    vDadosNota.CADASTRO_ID,
    vDadosNota.CFOP_ID,
    vDadosNota.EMPRESA_ID,
    vDadosNota.ORCAMENTO_ID,
    vDadosNota.ORCAMENTO_ID,
    iPRODUCAO_ID,
    vModeloNota,
    vSerieNota,
    vDadosNota.NATUREZA_OPERACAO,
    'N',
    case when iNATUREZA_PRODUCAO = 'E' then 'PEN' else 'PSA' end,
    vDadosNota.RAZAO_SOCIAL_EMITENTE,
    vDadosNota.NOME_FANTASIA_EMITENTE,
    vRegimeTributario,
    vDadosNota.CNPJ_EMITENTE,
    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,
    vDadosNota.LOGRADOURO_EMITENTE,
    vDadosNota.COMPLEMENTO_EMITENTE,
    vDadosNota.NOME_BAIRRO_EMITENTE,
    vDadosNota.NOME_CIDADE_EMITENTE,
    vDadosNota.NUMERO_EMITENTE,
    vDadosNota.ESTADO_ID_EMITENTE,
    vDadosNota.CEP_EMITENTE,
    vDadosNota.TELEFONE_EMITENTE,
    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,
    vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,
    vDadosNota.NOME_FANTASIA_EMITENTE,
    vDadosNota.RAZAO_SOCIAL_EMITENTE,
    'J',
    vDadosNota.CNPJ_EMITENTE,
    vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,
    vDadosNota.NOME_CONSUMIDOR_FINAL,
    vDadosNota.CPF_CONSUMIDOR_FINAL,
    vDadosNota.TELEFONE_EMITENTE,
    vTipoNotaGerar,
    vDadosNota.LOGRADOURO_EMITENTE,
    vDadosNota.COMPLEMENTO_EMITENTE,
    vDadosNota.NOME_BAIRRO_EMITENTE,
    vDadosNota.NOME_CIDADE_EMITENTE,
    vDadosNota.ESTADO_ID_EMITENTE,
    vDadosNota.CEP_EMITENTE,
    vDadosNota.NUMERO_EMITENTE,
    vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,
    vDadosNota.VALOR_TOTAL,
    vDadosNota.VALOR_TOTAL_PRODUTOS,
    vDadosNota.VALOR_DESCONTO,
    vDadosNota.VALOR_OUTRAS_DESPESAS,
    vDadosNota.BASE_CALCULO_ICMS,
    vDadosNota.VALOR_ICMS,
    vDadosNota.BASE_CALCULO_ICMS_ST,
    vDadosNota.VALOR_ICMS_ST,
    vDadosNota.BASE_CALCULO_PIS,
    vDadosNota.VALOR_PIS,
    vDadosNota.BASE_CALCULO_COFINS,
    vDadosNota.VALOR_COFINS,
    vDadosNota.VALOR_IPI,
    vDadosNota.VALOR_FRETE,
    vDadosNota.VALOR_SEGURO,
    vDadosNota.VALOR_RECEBIDO_DINHEIRO,
    vDadosNota.VALOR_RECEBIDO_PIX,
    vDadosNota.VALOR_RECEBIDO_ACUMULADO,
    vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,
    vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,
    vDadosNota.VALOR_RECEBIDO_CREDITO,
    vDadosNota.VALOR_RECEBIDO_COBRANCA,
    vDadosNota.VALOR_RECEBIDO_CHEQUE,
    vDadosNota.VALOR_RECEBIDO_FINANCEIRA,
    vDadosNota.PESO_LIQUIDO,
    vDadosNota.PESO_BRUTO,
    vDadosNota.INFORMACOES_COMPLEMENTARES
  );

  for i in 0..vDadosItens.count - 1 loop
    insert into NOTAS_FISCAIS_ITENS(
      NOTA_FISCAL_ID,
      PRODUTO_ID,
      ITEM_ID,
      NOME_PRODUTO,
      UNIDADE,
      CFOP_ID,
      CST,
      CODIGO_NCM,
      VALOR_TOTAL,
      PRECO_UNITARIO,
      QUANTIDADE,
      VALOR_TOTAL_DESCONTO,
      VALOR_TOTAL_OUTRAS_DESPESAS,
      BASE_CALCULO_ICMS,
      PERCENTUAL_ICMS,
      VALOR_ICMS,
      BASE_CALCULO_ICMS_ST,
      VALOR_ICMS_ST,
      CST_PIS,
      BASE_CALCULO_PIS,
      PERCENTUAL_PIS,
      VALOR_PIS,
      CST_COFINS,
      BASE_CALCULO_COFINS,
      PERCENTUAL_COFINS,
      VALOR_COFINS,
      INDICE_REDUCAO_BASE_ICMS,
      IVA,
      PRECO_PAUTA,
      CODIGO_BARRAS,
      VALOR_IPI,
      PERCENTUAL_IPI,
      INDICE_REDUCAO_BASE_ICMS_ST,
      PERCENTUAL_ICMS_ST,
      CEST
    )values(
      vNotaFiscalId,
      vDadosItens(i).PRODUTO_ID,
      vDadosItens(i).ITEM_ID,
      vDadosItens(i).NOME_PRODUTO,
      vDadosItens(i).UNIDADE,
      vDadosItens(i).CFOP_ID,
      vDadosItens(i).CST,
      vDadosItens(i).CODIGO_NCM,
      vDadosItens(i).VALOR_TOTAL,
      vDadosItens(i).PRECO_UNITARIO,
      vDadosItens(i).QUANTIDADE,
      vDadosItens(i).VALOR_TOTAL_DESCONTO,
      vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,
      vDadosItens(i).BASE_CALCULO_ICMS,
      vDadosItens(i).PERCENTUAL_ICMS,
      vDadosItens(i).VALOR_ICMS,
      vDadosItens(i).BASE_CALCULO_ICMS_ST,
      vDadosItens(i).VALOR_ICMS_ST,
      vDadosItens(i).CST_PIS,
      vDadosItens(i).BASE_CALCULO_PIS,
      vDadosItens(i).PERCENTUAL_PIS,
      vDadosItens(i).VALOR_PIS,
      vDadosItens(i).CST_COFINS,
      vDadosItens(i).BASE_CALCULO_COFINS,
      vDadosItens(i).PERCENTUAL_COFINS,
      vDadosItens(i).VALOR_COFINS,
      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,
      vDadosItens(i).IVA,
      vDadosItens(i).PRECO_PAUTA,
      vDadosItens(i).CODIGO_BARRAS,
      vDadosItens(i).VALOR_IPI,
      vDadosItens(i).PERCENTUAL_IPI,
      vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,
      vDadosItens(i).PERCENTUAL_ICMS_ST,
      vDadosItens(i).CEST
    );
  end loop;

  oNOTA_FISCAL_ID  := vNotaFiscalId;
  oTIPO_NOTA_GERAR := vTipoNotaGerar;

end GERAR_NOTA_PRODUCAO;
