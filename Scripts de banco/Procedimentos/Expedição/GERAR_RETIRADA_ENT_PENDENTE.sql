﻿create or replace procedure GERAR_RETIRADA_ENT_PENDENTE(
  iORCAMENTO_ID     in number,
  iEMPRESA_PEND_ID  in number,
  iLOCAL_ID         in number,
  iPREVISAO_ENTREGA in date,
  iTIPO_NOTA_GERAR  in string,
  iAGRUPADOR_ID     in number,
  iPRODUTOS_IDS     in TIPOS.ArrayOfNumeros,
  iITENS_IDS        in TIPOS.ArrayOfNumeros,
  iLOCAIS_IDS       in TIPOS.ArrayOfNumeros,
  iQUANTIDADES      in TIPOS.ArrayOfNumeros,
  iLOTES            in TIPOS.ArrayOfString,
  oRETIRADA_ID      out number
  
)
is
  i            number;
  vQtde        number;
  vRetiradaId  number;
  vEProdutoKit char(1);
  
  
  cursor cItensPendKit( pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pLOCAL_ID in number, pQUANTIDADE_PRODUTO_KIT in number ) is
  select
    EIP.PRODUTO_ID,
    EIP.ITEM_ID,
    EIP.LOTE,
    KIT.QUANTIDADE * pQUANTIDADE_PRODUTO_KIT as QUANTIDADE
  from
    ENTREGAS_ITENS_PENDENTES EIP

  inner join ORCAMENTOS_ITENS OIT
  on EIP.ORCAMENTO_ID = OIT.ORCAMENTO_ID
  and EIP.ITEM_ID = OIT.ITEM_ID

  inner join PRODUTOS_KIT KIT
  on KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID
  and OIT.PRODUTO_ID = KIT.PRODUTO_ID

  where EIP.ORCAMENTO_ID = iORCAMENTO_ID
  and EIP.EMPRESA_ID = iEMPRESA_PEND_ID
  and OIT.ITEM_KIT_ID = pITEM_KIT_ID
  and EIP.LOCAL_ID = pLOCAL_ID
  and EIP.PREVISAO_ENTREGA = iPREVISAO_ENTREGA;
  
  
  procedure VERIFICAR_PENDENCIA( pITEM_ID in number, pLOCAL_ID in number, pLOTE in string, pQUANTIDADE in number )
  is
    vPodeDeletarPendencia char(1);
  begin

    select
      case when QUANTIDADE = pQUANTIDADE then 'S' else 'N' end as PODE_DELETAR_PENDENCIA
    into
      vPodeDeletarPendencia
    from
      ENTREGAS_ITENS_PENDENTES
    where ORCAMENTO_ID = iORCAMENTO_ID
    and EMPRESA_ID = iEMPRESA_PEND_ID
    and LOCAL_ID = pLOCAL_ID
    and ITEM_ID = pITEM_ID
    and LOTE = pLOTE
    and PREVISAO_ENTREGA = iPREVISAO_ENTREGA;

    /* Caso a quantidade sendo desagendada seja a mesma quantidade da pendencia, quer dizer que o usuário está desagendando toda a pendência, */
    /* Neste caso vamos deleta-la */
    if vPodeDeletarPendencia = 'S' then
      delete from ENTREGAS_ITENS_PENDENTES
      where ORCAMENTO_ID = iORCAMENTO_ID
      and EMPRESA_ID = iEMPRESA_PEND_ID
      and LOCAL_ID = pLOCAL_ID
      and ITEM_ID = pITEM_ID
      and LOTE = pLOTE
      and PREVISAO_ENTREGA = iPREVISAO_ENTREGA;
    else
      /* Baixando a pendência */
      update ENTREGAS_ITENS_PENDENTES set
        QUANTIDADE = QUANTIDADE - pQUANTIDADE
      where ORCAMENTO_ID = iORCAMENTO_ID
      and EMPRESA_ID = iEMPRESA_PEND_ID
      and LOCAL_ID = pLOCAL_ID
      and ITEM_ID = pITEM_ID
      and LOTE = pLOTE
      and PREVISAO_ENTREGA = iPREVISAO_ENTREGA;
    end if;

  end;
  
  
  procedure INSERIR_ITEM( pPRODUTO_ID in number, pITEM_ID in number, pLOTE in string, pQUANTIDADES in number )
  is
    vQtde number;
  begin
    select
      count(*)
    into
      vQtde
    from
      RETIRADAS_ITENS_PENDENTES
    where EMPRESA_ID = iEMPRESA_PEND_ID
    and LOCAL_ID = iLOCAL_ID
    and ORCAMENTO_ID = iORCAMENTO_ID
    and PRODUTO_ID = pPRODUTO_ID
    and ITEM_ID = pITEM_ID
    and LOTE = pLOTE;

    if vQtde = 0 then
      insert into RETIRADAS_ITENS_PENDENTES(
        EMPRESA_ID,
        LOCAL_ID,
        ORCAMENTO_ID,
        PRODUTO_ID,
        ITEM_ID,
        LOTE,
        QUANTIDADE
      )values(
        iEMPRESA_PEND_ID,
        iLOCAL_ID,
        iORCAMENTO_ID,
        pPRODUTO_ID,
        pITEM_ID,
        pLOTE,
        pQUANTIDADES
      );
    else
      update RETIRADAS_ITENS_PENDENTES set
        QUANTIDADE = QUANTIDADE + pQUANTIDADES
      where EMPRESA_ID = iEMPRESA_PEND_ID
      and LOCAL_ID = iLOCAL_ID
      and ORCAMENTO_ID = iORCAMENTO_ID
      and PRODUTO_ID = pPRODUTO_ID
      and ITEM_ID = pITEM_ID
      and LOTE = pLOTE;
    end if;
  end;
  
  
begin

  select
    count(*)
  into
    vQtde
  from
    RETIRADAS_PENDENTES
  where EMPRESA_ID = iEMPRESA_PEND_ID
  and LOCAL_ID = iLOCAL_ID
  and ORCAMENTO_ID = iORCAMENTO_ID;

  if vQtde = 0 then
    insert into RETIRADAS_PENDENTES(
      EMPRESA_ID,
      LOCAL_ID,
      ORCAMENTO_ID
    )values(
      iEMPRESA_PEND_ID,
      iLOCAL_ID,
      iORCAMENTO_ID
    );
  end if;

  for i in iPRODUTOS_IDS.first..iPRODUTOS_IDS.last loop

    /* Baixando a pendência */
    VERIFICAR_PENDENCIA( iITENS_IDS(i), iLOCAIS_IDS(i), iLOTES(i), iQUANTIDADES(i) );

    /* Inserindo o item na retirada */
    INSERIR_ITEM( iPRODUTOS_IDS(i), iITENS_IDS(i), iLOTES(i), iQUANTIDADES(i) );
    select
      case when TIPO_CONTROLE_ESTOQUE in('K', 'A') then 'S' else 'N' end
    into
      vEProdutoKit
    from
      ORCAMENTOS_ITENS
    where ORCAMENTO_ID = iORCAMENTO_ID
    and ITEM_ID = iITENS_IDS(i);

    /* Se for o kit, desagendando os componentes do kit */
    if vEProdutoKit = 'S' then
      for xItensPendKit in cItensPendKit( iPRODUTOS_IDS(i), iITENS_IDS(i), iLOCAIS_IDS(i), iQUANTIDADES(i) ) loop
        /* Baixando a pendência do produto que compõe o kit */
        VERIFICAR_PENDENCIA( xItensPendKit.ITEM_ID, iLOCAIS_IDS(i), xItensPendKit.LOTE, xItensPendKit.QUANTIDADE );

        /* Inserindo o item na retirada */
        INSERIR_ITEM( xItensPendKit.PRODUTO_ID, xItensPendKit.ITEM_ID, xItensPendKit.LOTE, xItensPendKit.QUANTIDADE );
      end loop;
    end if;
  end loop;

  select
    count(*)
  into
    vQtde
  from
    ENTREGAS_ITENS_PENDENTES
  where ORCAMENTO_ID = iORCAMENTO_ID
  and EMPRESA_ID = iEMPRESA_PEND_ID
  and LOCAL_ID = iLOCAL_ID
  and PREVISAO_ENTREGA = iPREVISAO_ENTREGA;

  if vQtde = 0 then
    delete from ENTREGAS_PENDENTES
    where ORCAMENTO_ID = iORCAMENTO_ID
    and EMPRESA_ID = iEMPRESA_PEND_ID
    and LOCAL_ID = iLOCAL_ID
    and PREVISAO_ENTREGA = iPREVISAO_ENTREGA;
  end if;

  GERAR_RETIRADA_RETIRA_PENDENTE(
    iORCAMENTO_ID,
    iEMPRESA_PEND_ID,
    iLOCAL_ID,
    iTIPO_NOTA_GERAR,
    iAGRUPADOR_ID,
    iPRODUTOS_IDS,
    iITENS_IDS,
    iLOCAIS_IDS,
    iQUANTIDADES,
    iLOTES,
    vRetiradaId
  );

  oRETIRADA_ID := vRetiradaId;

end GERAR_RETIRADA_ENT_PENDENTE;
/