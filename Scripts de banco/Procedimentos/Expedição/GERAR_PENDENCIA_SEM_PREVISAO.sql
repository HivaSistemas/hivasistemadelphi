create or replace procedure GERAR_PENDENCIA_SEM_PREVISAO(iORCAMENTO_ID in number)
is

  cursor cItens is
  select
    ITE.PRODUTO_ID,
    ITE.ITEM_ID,
    ITE.QUANTIDADE        
  from
    ENTREGAS_ITENS_A_GERAR_TEMP ITE      
  where ITE.ORCAMENTO_ID = iORCAMENTO_ID
  and ITE.TIPO_ENTREGA in('SP', 'SE')
  order by
    ITE.TIPO_ENTREGA;
  
begin
  
  for xItens in cItens loop
    update ORCAMENTOS_ITENS set
      QUANTIDADE_SEM_PREVISAO = QUANTIDADE_SEM_PREVISAO - xItens.QUANTIDADE
    where ORCAMENTO_ID = iORCAMENTO_ID
    and ITEM_ID = xItens.ITEM_ID;

    insert into ENTREGAS_ITENS_SEM_PREVISAO(
      ORCAMENTO_ID,
      PRODUTO_ID,
      ITEM_ID,
      QUANTIDADE,
      AGUARDAR_CONTATO_CLIENTE
    )values(
      iORCAMENTO_ID,
      xItens.PRODUTO_ID,
      xItens.ITEM_ID,
      xItens.QUANTIDADE,
      'N'
    );    
  end loop;
  
  /* Limpando a tabela no final do processo */
  delete from ENTREGAS_ITENS_A_GERAR_TEMP
  where ORCAMENTO_ID = iORCAMENTO_ID
  and TIPO_ENTREGA in('SP', 'SE');

end GERAR_PENDENCIA_SEM_PREVISAO;
/