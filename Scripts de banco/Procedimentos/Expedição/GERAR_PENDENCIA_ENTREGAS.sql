﻿create or replace procedure GERAR_PENDENCIA_ENTREGAS(
  iORCAMENTO_ID       in number,
  iEMPRESA_GERACAO_ID in number
)
is    
  vQtdeAgendar             number;
  vQtdeFaltaAgendar        number;
  vQtdeFaltaAgendarLote    number;

  vQtdeAgendarLoteItemKit  number;
  vQtdeFaltaAgendarItemKit number;
  vQtdeAgendarKitEmpresa   number;
  vDefinirLocalManual    PARAMETROS.DEFINIR_LOCAL_MANUAL%type;
  
  cursor cLocaisDefinidos(pPRODUTO_ID in number, pORCAMENTO_ID in number) is
  select
    ORCAMENTO_ID,
	  PRODUTO_ID,
	  LOCAL_ID,
	  QUANTIDADE_ENTREGAR,
    EMPRESA_ID,
	  ITEM_ID,
    LOTE
  from
    ORCAMENTOS_ITENS_DEF_LOCAIS
  
  where PRODUTO_ID = pPRODUTO_ID
  and ORCAMENTO_ID = pORCAMENTO_ID
  and QUANTIDADE_ENTREGAR > 0
  and STATUS = 'PEN'
  
  order by LOCAL_ID;  

  cursor cItens is
  select
    ITE.PRODUTO_ID,
    ITE.ITEM_ID,
    ITE.QUANTIDADE,    
    PRO.ACEITAR_ESTOQUE_NEGATIVO,
    ITE.PREVISAO_ENTREGA,
    OIT.TIPO_CONTROLE_ESTOQUE
  from
    ENTREGAS_ITENS_A_GERAR_TEMP ITE    

  inner join PRODUTOS PRO
  on ITE.PRODUTO_ID = PRO.PRODUTO_ID

  inner join ORCAMENTOS_ITENS OIT
  on ITE.ORCAMENTO_ID = OIT.ORCAMENTO_ID
  and ITE.PRODUTO_ID = OIT.PRODUTO_ID
  and ITE.ITEM_ID = OIT.ITEM_ID
  and OIT.ITEM_KIT_ID is null

  where ITE.ORCAMENTO_ID = iORCAMENTO_ID
  and ITE.TIPO_ENTREGA = 'EN'

  order by
    ITE.TIPO_ENTREGA;  

  cursor cEstoques(pPRODUTO_ID in number) is
  select
    GES.EMPRESA_GRUPO_ID as EMPRESA_ID,
    DIV.PRODUTO_ID,
    DIV.DISPONIVEL,    
    DIV.LOTE,
    DIV.LOCAL_ID
  from
    VW_ESTOQUES_DIVISAO DIV

  inner join GRUPOS_ESTOQUES GES
  on DIV.EMPRESA_ID = GES.EMPRESA_GRUPO_ID
  and GES.TIPO = 'E'
  and GES.EMPRESA_ID = iEMPRESA_GERACAO_ID

  inner join PRODUTOS_ORDENS_LOC_ENTREGAS POL
  on DIV.EMPRESA_ID = POL.EMPRESA_ID
  and DIV.PRODUTO_PAI_ID = POL.PRODUTO_ID
  and DIV.LOCAL_ID = POL.LOCAL_ID
  and POL.TIPO = 'E'

  where DIV.PRODUTO_ID = pPRODUTO_ID
  and case when DIV.ACEITAR_ESTOQUE_NEGATIVO = 'S' then 1 else DIV.DISPONIVEL end > 0
  order by
    GES.ORDEM,
    POL.ORDEM;

    
  cursor cEmpresasEstoqueKit is
  select
    EMPRESA_GRUPO_ID as EMPRESA_ID
  from
    GRUPOS_ESTOQUES
  where TIPO = 'E'
  and EMPRESA_ID = iEMPRESA_GERACAO_ID
  order by
    ORDEM;    
    
    
  cursor cItensKit(pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pQUANTIDADE_KITS in number) is
  select
    KIT.PRODUTO_ID,
    ITE.ITEM_ID,
    KIT.QUANTIDADE * pQUANTIDADE_KITS as QUANTIDADE,
    PRO.ACEITAR_ESTOQUE_NEGATIVO
  from
    ORCAMENTOS_ITENS ITE

  inner join PRODUTOS_KIT KIT
  on ITE.PRODUTO_ID = KIT.PRODUTO_ID
  and KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID

  inner join PRODUTOS PRO
  on KIT.PRODUTO_ID = PRO.PRODUTO_ID

  where ITE.ITEM_KIT_ID = pITEM_KIT_ID
  and ORCAMENTO_ID = iORCAMENTO_ID;    
  
  
  cursor cEstoqueItemKit(pEMPRESA_ID in number, pPRODUTO_ID in number, pLOCAL_ID in number) is
  select
    DIV.LOCAL_ID,
    DIV.DISPONIVEL,
    DIV.LOTE
  from
    VW_ESTOQUES_DIVISAO DIV

  where DIV.PRODUTO_ID = pPRODUTO_ID
  and DIV.EMPRESA_ID = pEMPRESA_ID
  and DIV.LOCAL_ID = pLOCAL_ID

  order by
    DIV.DISPONIVEL desc;


  cursor cItensDefLotes( pITEM_ID in number ) is
  select
    ITE.PRODUTO_ID,
    OIL.LOTE,
    OIL.QUANTIDADE_ENTREGAR as QUANTIDADE_AGENDAR
  from
    ORCAMENTOS_ITENS_DEF_LOTES OIL

  inner join ORCAMENTOS_ITENS ITE
  on OIL.ORCAMENTO_ID = ITE.ORCAMENTO_ID
  and OIL.ITEM_ID = ITE.ITEM_ID

  where OIL.ORCAMENTO_ID = iORCAMENTO_ID
  and OIL.ITEM_ID = pITEM_ID;


  cursor cItensDefLotesEstoques( pPRODUTO_ID in number, pLOTE in string ) is
  select
    DIV.EMPRESA_ID,
    DIV.LOCAL_ID,
    DIV.DISPONIVEL
  from
    VW_ESTOQUES_DIVISAO DIV

  inner join GRUPOS_ESTOQUES GRU
  on GRU.TIPO = 'E'
  and GRU.EMPRESA_GRUPO_ID = iEMPRESA_GERACAO_ID
  and DIV.EMPRESA_ID = GRU.EMPRESA_ID

  inner join PRODUTOS_ORDENS_LOC_ENTREGAS POL
  on DIV.EMPRESA_ID = POL.EMPRESA_ID
  and DIV.PRODUTO_ID = POL.PRODUTO_ID
  and DIV.LOCAL_ID = POL.LOCAL_ID
  and POL.TIPO = 'E'

  where DIV.PRODUTO_ID = pPRODUTO_ID
  and DIV.LOTE = pLOTE
  order by
    case when DIV.DISPONIVEL <= 0 then 1 else 0 end,
    POL.ORDEM;

  cursor cOrdemLocaisProdutoKit( pPRODUTO_KIT_ID in number, pEMPRESA_ESTOQUE_ID in number )
  is
  select
    LOCAL_ID
  from
    PRODUTOS_ORDENS_LOC_ENTREGAS
  where PRODUTO_ID = pPRODUTO_KIT_ID
  and EMPRESA_ID = pEMPRESA_ESTOQUE_ID
  and TIPO = 'E'
  order by
    ORDEM;

begin
  
  select
    DEFINIR_LOCAL_MANUAL
  into
    vDefinirLocalManual
  from
    PARAMETROS;  
  
  for xItens in cItens loop
     vQtdeFaltaAgendar := xItens.QUANTIDADE;
    /* Verificando se o usuário já selecionou os lotes */
    /* Se sim apenas agendando e passando pra frente */
    if xItens.TIPO_CONTROLE_ESTOQUE in('L', 'G', 'P') then
      if vDefinirLocalManual = 'S' then
        for xLocaisDefinidos in cLocaisDefinidos(xItens.PRODUTO_ID, iORCAMENTO_ID) loop

		      VERIFICAR_PENDENCIA_ENTREGA(xLocaisDefinidos.EMPRESA_ID, xLocaisDefinidos.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, 'NHR');

          INSERIR_ENTREGAS_ITENS_PEND(
		        xLocaisDefinidos.EMPRESA_ID,
			      xLocaisDefinidos.LOCAL_ID,
			      iORCAMENTO_ID,
            xItens.PREVISAO_ENTREGA,
            xLocaisDefinidos.PRODUTO_ID,
            xItens.ITEM_ID,
            xLocaisDefinidos.LOTE,
            xLocaisDefinidos.QUANTIDADE_ENTREGAR
          );

          update ORCAMENTOS_ITENS_DEF_LOCAIS set
            QUANTIDADE_ENTREGAR = 0
          where ITEM_ID = xLocaisDefinidos.ITEM_ID
          and ORCAMENTO_ID = iORCAMENTO_ID;

        end loop;
      else
        for xItensDefLotes in cItensDefLotes( xItens.ITEM_ID ) loop
          vQtdeFaltaAgendarLote := xItensDefLotes.QUANTIDADE_AGENDAR;

          for xItensDefLoteEstoq in cItensDefLotesEstoques( xItens.PRODUTO_ID, xItensDefLotes.LOTE ) loop
            VERIFICAR_PENDENCIA_ENTREGA(xItensDefLoteEstoq.EMPRESA_ID, xItensDefLoteEstoq.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, 'NHR');

            if xItensDefLoteEstoq.DISPONIVEL >= vQtdeFaltaAgendarLote then
              vQtdeAgendar := vQtdeFaltaAgendarLote;
              vQtdeFaltaAgendarLote := 0;
            else
              vQtdeAgendar := xItensDefLoteEstoq.DISPONIVEL;
              vQtdeFaltaAgendarLote := vQtdeFaltaAgendarLote - vQtdeAgendar;
            end if;

            begin
              INSERIR_ENTREGAS_ITENS_PEND(
                xItensDefLoteEstoq.EMPRESA_ID,
                xItensDefLoteEstoq.LOCAL_ID,
                iORCAMENTO_ID,
                xItens.PREVISAO_ENTREGA,
                xItens.PRODUTO_ID,
                xItens.ITEM_ID,
                xItensDefLotes.LOTE,
                vQtdeAgendar
              );
            exception
              when others then
                ERRO(vQtdeAgendar || '  ' || sqlerrm);
            end;

            vQtdeFaltaAgendar := vQtdeFaltaAgendar - vQtdeAgendar;

            if vQtdeFaltaAgendarLote = 0 then
              exit;
            end if;
          end loop;

          if vQtdeFaltaAgendarLote > 0 then
            ERRO('Estoque insuficiente para o produto ' || xItens.PRODUTO_ID || ', lote ' || xItensDefLotes.LOTE  || '!');
          end if;
        end loop;

        if vQtdeFaltaAgendar > 0 then
          ERRO('Estoque insuficiente para o produto ' || xItens.PRODUTO_ID || '!');
        end if;
      end if;
    /* Se for produto com controle de estoque por kit agrupado ou desmembrado */
    elsif xItens.TIPO_CONTROLE_ESTOQUE in('K', 'A') then

      vQtdeFaltaAgendar := xItens.QUANTIDADE;

      if xItens.TIPO_CONTROLE_ESTOQUE = 'K' then
        for xItensCompoeKit in cItensKit(xItens.PRODUTO_ID, xItens.ITEM_ID, xItens.QUANTIDADE) loop

          vQtdeFaltaAgendarItemKit := xItensCompoeKit.QUANTIDADE;

          for xEstoques in cEstoques(xItensCompoeKit.PRODUTO_ID) loop
            if xItensCompoeKit.ACEITAR_ESTOQUE_NEGATIVO = 'S' then
              vQtdeAgendar := vQtdeFaltaAgendarItemKit;
              vQtdeFaltaAgendarItemKit := 0;
            elsif xEstoques.DISPONIVEL > 0 then
              if xEstoques.DISPONIVEL >= vQtdeFaltaAgendar  then
                vQtdeAgendar := vQtdeFaltaAgendarItemKit;
                vQtdeFaltaAgendarItemKit := 0;
              else
                vQtdeAgendar := xEstoques.DISPONIVEL;
                vQtdeFaltaAgendarItemKit := vQtdeFaltaAgendarItemKit - vQtdeAgendar;
              end if;
            else
              /* Se não aceita estoque negativo, passando para o próximo local. */
              continue;
            end if;

            VERIFICAR_PENDENCIA_ENTREGA(xEstoques.EMPRESA_ID, xEstoques.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, 'NHR');

            INSERIR_ENTREGAS_ITENS_PEND(
              xEstoques.EMPRESA_ID,
              xEstoques.LOCAL_ID,
              iORCAMENTO_ID,
              xItens.PREVISAO_ENTREGA,
              xItensCompoeKit.PRODUTO_ID,
              xItensCompoeKit.ITEM_ID,
              xEstoques.LOTE,
              vQtdeAgendar
            );

            if vQtdeFaltaAgendarItemKit = 0 then
              vQtdeFaltaAgendar := 0;
              exit;
            end if;
          end loop;

          if vQtdeFaltaAgendarItemKit > 0 then
            ERRO(
              'Estoque insuficiente para o produto ' || xItensCompoeKit.PRODUTO_ID || ' que compõe o kit ' ||  xItens.PRODUTO_ID || '!' || chr(13) ||
              'Faltam: ' || NFORMAT(vQtdeFaltaAgendarItemKit, 0)
            );
          end if;

        end loop;

      /* Se kit entrega agrupada, detalhe é que neste todos os itens devem estar no mesmo local */
      else
        for xEmpresasEstoqueKit in cEmpresasEstoqueKit loop
          vQtdeAgendarKitEmpresa := QUANTIDADE_KITS_DISP_EMPRESA(xEmpresasEstoqueKit.EMPRESA_ID, xItens.PRODUTO_ID);

          -- Se não pode agendar nada, passa pra frente
          if xItens.ACEITAR_ESTOQUE_NEGATIVO = 'S' then
            vQtdeAgendarKitEmpresa := xItens.QUANTIDADE;
          elsif vQtdeAgendarKitEmpresa = 0 then
            continue;
          end if;

          if vQtdeAgendarKitEmpresa >= vQtdeFaltaAgendar then
            vQtdeAgendarKitEmpresa := vQtdeFaltaAgendar;
          end if;

          for xOrdemLocaisProdutoKit in cOrdemLocaisProdutoKit(xItens.PRODUTO_ID, xEmpresasEstoqueKit.EMPRESA_ID) loop

            if xItens.ACEITAR_ESTOQUE_NEGATIVO = 'S' then
              vQtdeAgendar := vQtdeAgendarKitEmpresa;
            else
              vQtdeAgendar := QUANTIDADE_KITS_DISP_LOCAL(xEmpresasEstoqueKit.EMPRESA_ID, xOrdemLocaisProdutoKit.LOCAL_ID, xItens.PRODUTO_ID);
              if vQtdeAgendar >= vQtdeAgendarKitEmpresa then
                vQtdeAgendar := vQtdeAgendarKitEmpresa;
              end if;
            end if;

            /* Se não pode agendar no local passando para o próximo */
            if vQtdeAgendar = 0 then
              continue;
            end if;

            /* Verificando o cabeçalho da pendência */
            VERIFICAR_PENDENCIA_ENTREGA(xEmpresasEstoqueKit.EMPRESA_ID, xOrdemLocaisProdutoKit.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, 'NHR');

            for xItensKit in cItensKit(xItens.PRODUTO_ID, xItens.ITEM_ID, vQtdeAgendar) loop

              vQtdeFaltaAgendarItemKit := xItens.QUANTIDADE;

              for xEstoqueItemKit in cEstoqueItemKit(xEmpresasEstoqueKit.EMPRESA_ID, xItensKit.PRODUTO_ID, xOrdemLocaisProdutoKit.LOCAL_ID) loop

                if xEstoqueItemKit.DISPONIVEL >= vQtdeFaltaAgendarItemKit then
                  vQtdeAgendarLoteItemKit := vQtdeFaltaAgendarItemKit;
                  vQtdeFaltaAgendarItemKit := 0;
                else
                  vQtdeAgendarLoteItemKit := xEstoqueItemKit.DISPONIVEL;
                  vQtdeFaltaAgendarItemKit := vQtdeFaltaAgendarItemKit - vQtdeAgendarLoteItemKit;
                end if;

                begin
                  INSERIR_ENTREGAS_ITENS_PEND(
                    xEmpresasEstoqueKit.EMPRESA_ID,
                    xOrdemLocaisProdutoKit.LOCAL_ID,
                    iORCAMENTO_ID,
                    xItens.PREVISAO_ENTREGA,
                    xItensKit.PRODUTO_ID,
                    xItensKit.ITEM_ID,
                    xEstoqueItemKit.LOTE,
                    vQtdeAgendarLoteItemKit
                  );
                exception
                  when others then
                    erro(vQtdeAgendarLoteItemKit || '  ' || xItensKit.PRODUTO_ID || '  ' || xEmpresasEstoqueKit.EMPRESA_ID || '  ' || vQtdeAgendarLoteItemKit || '  ' || sqlerrm);
                end;

                if vQtdeFaltaAgendarItemKit = 0 then
                  exit;
                end if;
              end loop;
            end loop;

            /* Inserindo o próprio item kit */
            INSERIR_ENTREGAS_ITENS_PEND(
              xEmpresasEstoqueKit.EMPRESA_ID,
              xOrdemLocaisProdutoKit.LOCAL_ID,
              iORCAMENTO_ID,
              xItens.PREVISAO_ENTREGA,
              xItens.PRODUTO_ID,
              xItens.ITEM_ID,
              '???',
              vQtdeAgendar
            );

            vQtdeAgendarKitEmpresa := vQtdeAgendarKitEmpresa - vQtdeAgendar;
            vQtdeFaltaAgendar      := vQtdeFaltaAgendar - vQtdeAgendar;

            if vQtdeAgendarKitEmpresa = 0 or vQtdeFaltaAgendar = 0 then
              exit;
            end if;
          end loop;

          if vQtdeAgendarKitEmpresa > 0 then
            ERRO(
              'Estoque insuficiente para o produto kit ' || xItens.PRODUTO_ID || '!' || chr(13) ||
              'Faltam: ' || NFORMAT(vQtdeAgendarKitEmpresa, 0) || chr(13) ||
              'Verifique os locais dos produtos que compõe este kit!'
            );
          end if;

        end loop;
      end if;

      if vQtdeFaltaAgendar > 0 then
        ERRO(
          'Estoque insuficiente para o produto kit ' || xItens.PRODUTO_ID || '!' || chr(13) ||
          'Faltam: ' || NFORMAT(vQtdeFaltaAgendar, 0)
        );
      end if;

    /* Se for produto normal */
    else
	
      if vDefinirLocalManual = 'S' then
        for xLocaisDefinidos in cLocaisDefinidos(xItens.PRODUTO_ID, iORCAMENTO_ID) loop
			
		      VERIFICAR_PENDENCIA_ENTREGA(xLocaisDefinidos.EMPRESA_ID, xLocaisDefinidos.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, 'NHR');

          INSERIR_ENTREGAS_ITENS_PEND(
		        xLocaisDefinidos.EMPRESA_ID,
			      xLocaisDefinidos.LOCAL_ID,
			      iORCAMENTO_ID,
            xItens.PREVISAO_ENTREGA,
            xLocaisDefinidos.PRODUTO_ID,
            xItens.ITEM_ID,
            '???',
            xLocaisDefinidos.QUANTIDADE_ENTREGAR
          );
		  
          update ORCAMENTOS_ITENS_DEF_LOCAIS set
            QUANTIDADE_ENTREGAR = 0
          where ITEM_ID = xLocaisDefinidos.ITEM_ID
          and ORCAMENTO_ID = iORCAMENTO_ID;
	  
        end loop;
      else	
		  vQtdeFaltaAgendar := xItens.QUANTIDADE;

		  for xEstoques in cEstoques(xItens.PRODUTO_ID) loop
			if xItens.ACEITAR_ESTOQUE_NEGATIVO = 'S' then
			  vQtdeAgendar := vQtdeFaltaAgendar;
			  vQtdeFaltaAgendar := 0;
			elsif xEstoques.DISPONIVEL > 0 then
			  if xEstoques.DISPONIVEL >= vQtdeFaltaAgendar then
				vQtdeAgendar := vQtdeFaltaAgendar;
				vQtdeFaltaAgendar := 0;
			  else
				vQtdeAgendar := xEstoques.DISPONIVEL;
				vQtdeFaltaAgendar := vQtdeFaltaAgendar - vQtdeAgendar;
			  end if;
			else
			  /* Se não aceita estoque negativo, passando para o próximo local. */
			  continue;
			end if;

			VERIFICAR_PENDENCIA_ENTREGA(xEstoques.EMPRESA_ID, xEstoques.LOCAL_ID, iORCAMENTO_ID, xItens.PREVISAO_ENTREGA, 'NHR');

			INSERIR_ENTREGAS_ITENS_PEND(
			  xEstoques.EMPRESA_ID,
			  xEstoques.LOCAL_ID,
			  iORCAMENTO_ID,
			  xItens.PREVISAO_ENTREGA,
			  xItens.PRODUTO_ID,
			  xItens.ITEM_ID,
			  xEstoques.LOTE,
			  vQtdeAgendar
			);

			if vQtdeFaltaAgendar = 0 then
			  exit;
			end if;
		  end loop;
		end if;
      if vQtdeFaltaAgendar > 0 and vDefinirLocalManual = 'N' then
        ERRO( 'Estoque insuficiente para geração da entrega! Produto: ' || xItens.PRODUTO_ID );
      end if;

    end if;
  end loop;
  
  /* Limpando a tabela no final do processo */
  delete from ENTREGAS_ITENS_A_GERAR_TEMP
  where ORCAMENTO_ID = iORCAMENTO_ID
  and TIPO_ENTREGA = 'EN';

end GERAR_PENDENCIA_ENTREGAS;
/