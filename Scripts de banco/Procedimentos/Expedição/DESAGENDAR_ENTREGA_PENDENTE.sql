﻿create or replace procedure DESAGENDAR_ENTREGA_PENDENTE(
  iEMPRESA_PEND_ID  in number,
  iORCAMENTO_ID     in number,
  iPREVISAO_ENTREGA in date,
  iLOCAL_ID         in number,
  iPRODUTOS_IDS     in TIPOS.ArrayOfNumeros,
  iITENS_IDS        in TIPOS.ArrayOfNumeros,
  iQUANTIDADES      in TIPOS.ArrayOfNumeros,
  iLOTES            in TIPOS.ArrayOfString
)
is
  i            number;
  vQtde        number;
  vEProdutoKit char(1);
  
  cursor cItensPendKit( pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pQUANTIDADE_PRODUTO_KIT in number ) is
  select
    EIP.PRODUTO_ID,
    EIP.ITEM_ID,
    EIP.LOTE,
    KIT.QUANTIDADE * pQUANTIDADE_PRODUTO_KIT as QUANTIDADE
  from
    ENTREGAS_ITENS_PENDENTES EIP
  
  inner join ORCAMENTOS_ITENS OIT
  on EIP.ORCAMENTO_ID = OIT.ORCAMENTO_ID
  and EIP.ITEM_ID = OIT.ITEM_ID
  
  inner join PRODUTOS_KIT KIT
  on KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID
  and OIT.PRODUTO_ID = KIT.PRODUTO_ID
    
  where EIP.ORCAMENTO_ID = iORCAMENTO_ID
  and EIP.EMPRESA_ID = iEMPRESA_PEND_ID
  and OIT.ITEM_KIT_ID = pITEM_KIT_ID
  and EIP.PREVISAO_ENTREGA = iPREVISAO_ENTREGA
  and EIP.LOCAL_ID = iLOCAL_ID;
  
  procedure VERIFICAR_PENDENCIA( pITEM_ID in number, pLOTE in string, pQUANTIDADE in number ) 
  is
    vPodeDeletarPendencia char(1);
  begin
   
   select
      case when QUANTIDADE = pQUANTIDADE then 'S' else 'N' end as PODE_DELETAR_PENDENCIA
    into
      vPodeDeletarPendencia
    from
      ENTREGAS_ITENS_PENDENTES
    where ORCAMENTO_ID = iORCAMENTO_ID
    and EMPRESA_ID = iEMPRESA_PEND_ID
    and PREVISAO_ENTREGA = iPREVISAO_ENTREGA
    and LOCAL_ID = iLOCAL_ID
    and ITEM_ID = pITEM_ID
    and LOTE = pLOTE;  
    
    /* Caso a quantidade sendo desagendada seja a mesma quantidade da pendencia, quer dizer que o usuário está desagendando toda a pendência, */
    /* Neste caso vamos deleta-la */
    if vPodeDeletarPendencia = 'S' then
      delete from ENTREGAS_ITENS_PENDENTES
      where ORCAMENTO_ID = iORCAMENTO_ID
      and EMPRESA_ID = iEMPRESA_PEND_ID
      and PREVISAO_ENTREGA = iPREVISAO_ENTREGA
      and LOCAL_ID = iLOCAL_ID
      and ITEM_ID = pITEM_ID
      and LOTE = pLOTE;      
    else
      /* Baixando a pendência */
      update ENTREGAS_ITENS_PENDENTES set
        QUANTIDADE = QUANTIDADE - pQUANTIDADE
      where ORCAMENTO_ID = iORCAMENTO_ID
      and EMPRESA_ID = iEMPRESA_PEND_ID
      and PREVISAO_ENTREGA = iPREVISAO_ENTREGA
      and LOCAL_ID = iLOCAL_ID
      and ITEM_ID = pITEM_ID
      and LOTE = pLOTE;
    end if;
    
  end;
  
  procedure VERIFICAR_SEM_PREVISAO( pPRODUTO_ID in number, pITEM_ID in number, pQUANTIDADE in number ) 
  is
    vQtde number;  
  begin
    select
      count(*)
    into
      vQtde
    from
      ENTREGAS_ITENS_SEM_PREVISAO
    where ORCAMENTO_ID = iORCAMENTO_ID
    and ITEM_ID = pITEM_ID;
    
    if vQtde = 0 then
      /* Inserindo a entrega sem previsão caso não exista */
      insert into ENTREGAS_ITENS_SEM_PREVISAO(        
        ORCAMENTO_ID,
        PRODUTO_ID,
        ITEM_ID,
        QUANTIDADE,
        AGUARDAR_CONTATO_CLIENTE
      )values(
        iORCAMENTO_ID,
        pPRODUTO_ID,
        pITEM_ID,        
        pQUANTIDADE,
        'N'
      );
    else      
      update ENTREGAS_ITENS_SEM_PREVISAO set        
        QUANTIDADE = QUANTIDADE + pQUANTIDADE
      where ORCAMENTO_ID = iORCAMENTO_ID
      and PRODUTO_ID = pPRODUTO_ID
      and ITEM_ID = pITEM_ID;
    end if;   
    
  end;
  
begin

  for i in iPRODUTOS_IDS.first..iPRODUTOS_IDS.last loop
  
    /* Baixando a pendência */
    VERIFICAR_PENDENCIA( iITENS_IDS(i), iLOTES(i), iQUANTIDADES(i) );
    
    /* Criando o sem previsão */
    VERIFICAR_SEM_PREVISAO( iPRODUTOS_IDS(i), iITENS_IDS(i), iQUANTIDADES(i) );

    select
      case when TIPO_CONTROLE_ESTOQUE in('K', 'A') then 'S' else 'N' end
    into
      vEProdutoKit
    from
      ORCAMENTOS_ITENS
    where ORCAMENTO_ID = iORCAMENTO_ID
    and ITEM_ID = iITENS_IDS(i);  

    /* Se for o kit, desagendando os componentes do kit */
    if vEProdutoKit = 'S' then                  
      for xItensPendKit in cItensPendKit( iPRODUTOS_IDS(i), iITENS_IDS(i), iQUANTIDADES(i) ) loop
        /* Baixando a pendência */
        VERIFICAR_PENDENCIA( xItensPendKit.ITEM_ID, xItensPendKit.LOTE, xItensPendKit.QUANTIDADE );

        /* Criando o sem previsão */
        VERIFICAR_SEM_PREVISAO( xItensPendKit.PRODUTO_ID, xItensPendKit.ITEM_ID, xItensPendKit.QUANTIDADE );
      end loop;
    end if;    
  end loop;

end DESAGENDAR_ENTREGA_PENDENTE;
/