﻿create or replace procedure AGENDAR_ITENS_SEM_PREVISAO(
  iORCAMENTO_ID       in number,
  iEMPRESA_GERACAO_ID in number,
  iPREVISAO_ENTREGA   in date,
  iVetPRODUTOS_IDS    in TIPOS.ArrayOfNumeros,
  iVetITENS_IDS       in TIPOS.ArrayOfNumeros,
  iVetQUANTIDADES     in TIPOS.ArrayOfNumeros
)
is
  i            number;
  vStatus      ORCAMENTOS.STATUS%type;
  vEProdutoKit char(1) default 'N';

  cursor cItensKit(pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pQUANTIDADE_KITS in number) is
  select
    KIT.PRODUTO_ID,
    ITE.ITEM_ID,
    KIT.QUANTIDADE * pQUANTIDADE_KITS as QUANTIDADE
  from
    ORCAMENTOS_ITENS ITE

  inner join PRODUTOS_KIT KIT
  on ITE.PRODUTO_ID = KIT.PRODUTO_ID
  and KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID

  where ITE.ITEM_KIT_ID = pITEM_KIT_ID
  and ORCAMENTO_ID = iORCAMENTO_ID;

begin
  select
    STATUS
  into
    vStatus
  from
    ORCAMENTOS
  where ORCAMENTO_ID = iORCAMENTO_ID;

  if vStatus not in('VE', 'RE') then
    ERRO('O orçamento ainda não foi recebido, verifique!');
  end if;

  for i in iVetPRODUTOS_IDS.first..iVetPRODUTOS_IDS.last loop

    select
      case when TIPO_CONTROLE_ESTOQUE in('K', 'A') then 'S' else 'N' end
    into
      vEProdutoKit
    from
      ORCAMENTOS_ITENS
    where ORCAMENTO_ID = iORCAMENTO_ID
    and ITEM_ID = iVetITENS_IDS(i);

    if vEProdutoKit = 'S' then
      for xItensKit in cItensKit(iVetPRODUTOS_IDS(i), iVetITENS_IDS(i), iVetQUANTIDADES(i)) loop
        update ENTREGAS_ITENS_SEM_PREVISAO set
          ENTREGUES = ENTREGUES + xItensKit.QUANTIDADE
        where ORCAMENTO_ID = iORCAMENTO_ID
        and PRODUTO_ID = xItensKit.PRODUTO_ID
        and ITEM_ID = xItensKit.ITEM_ID;
      end loop;
    end if;

    update ENTREGAS_ITENS_SEM_PREVISAO set
      ENTREGUES = ENTREGUES + iVetQUANTIDADES(i)
    where ORCAMENTO_ID = iORCAMENTO_ID
    and PRODUTO_ID = iVetPRODUTOS_IDS(i)
    and ITEM_ID = iVetITENS_IDS(i);

    insert into ENTREGAS_ITENS_A_GERAR_TEMP(
      ORCAMENTO_ID,
      PREVISAO_ENTREGA,
      PRODUTO_ID,
      ITEM_ID,
      QUANTIDADE,
      TIPO_ENTREGA
    )values(
      iORCAMENTO_ID,
      iPREVISAO_ENTREGA,
      iVetPRODUTOS_IDS(i),
      iVetITENS_IDS(i),
      iVetQUANTIDADES(i),
      'EN'
    );
  end loop;

  GERAR_PENDENCIA_ENTREGAS(iORCAMENTO_ID, iEMPRESA_GERACAO_ID);

  /* Removendo as pendência já que não existem mais */
  delete from ENTREGAS_ITENS_SEM_PREVISAO
  where ORCAMENTO_ID = iORCAMENTO_ID
  and SALDO = 0;

end AGENDAR_ITENS_SEM_PREVISAO;
/
