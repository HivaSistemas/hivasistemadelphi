﻿create or replace procedure CANCELAR_ENTREGA(iENTREGA_ID in number)
is

  vQtde            number;
  vEmpresaId       ORCAMENTOS.EMPRESA_ID%type;
  vOrcamentoId     ORCAMENTOS.ORCAMENTO_ID%type;
  vStatus          ORCAMENTOS.STATUS%type;
  vLocalId         ENTREGAS.LOCAL_ID%type;
  vPrevisaoEntrega ENTREGAS.PREVISAO_ENTREGA%type;

  cursor cItens is
  select
    PRODUTO_ID,
    QUANTIDADE,
    ITEM_ID,
    LOTE
  from
    ENTREGAS_ITENS
  where ENTREGA_ID = iENTREGA_ID;

begin

  select
    ENT.ORCAMENTO_ID,
    ENT.EMPRESA_ENTREGA_ID as EMPRESA_ID,
    ORC.STATUS,
    ENT.LOCAL_ID,
    ENT.PREVISAO_ENTREGA
  into
    vOrcamentoId,
    vEmpresaId,
    vStatus,
    vLocalId,
    vPrevisaoEntrega
  from
    ENTREGAS ENT

  inner join ORCAMENTOS ORC
  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID

  where ENT.ENTREGA_ID = iENTREGA_ID;

  if vStatus not in('VE', 'RE') then
    ERRO('O pedido que esta sendo cancelado n?o esta recebido, verifique!');
  end if;

  select
    count(*)
  into
    vQtde
  from
    DEVOLUCOES_ITENS_ENTREGAS
  where ENTREGA_ID = iENTREGA_ID;

  if vQtde > 0 then
    ERRO('Existem devoluc?es realizadas para esta entrega, por favor cancele as devoluc?es antes de cancelar a entrega!');
  end if;

  select
    count(*)
  into
    vQtde
  from
    ENTREGAS_PENDENTES
  where ORCAMENTO_ID = vOrcamentoId
  and EMPRESA_ID = vEmpresaId
  and trunc(PREVISAO_ENTREGA) = trunc(vPrevisaoEntrega)
  and LOCAL_ID = vLocalId;

  if vQtde = 0 then
    insert into ENTREGAS_PENDENTES(
      EMPRESA_ID,
      ORCAMENTO_ID,
      PREVISAO_ENTREGA,
      LOCAL_ID
    )values(
      vEmpresaId,
      vOrcamentoId,
      vPrevisaoEntrega,
      vLocalId
    );
  end if;

  for vItens in cItens loop
    delete from ENTREGAS_ITENS
    where ENTREGA_ID = iENTREGA_ID
    and ITEM_ID = vItens.ITEM_ID;

    select
      count(*)
    into
      vQtde
    from
      ENTREGAS_ITENS_PENDENTES
    where ORCAMENTO_ID = vOrcamentoId
    and EMPRESA_ID = vEmpresaId
    and ITEM_ID = vItens.ITEM_ID
    and LOTE = vItens.LOTE
    and trunc(PREVISAO_ENTREGA) = trunc(vPrevisaoEntrega)
    and LOCAL_ID = vLocalId;

    if vQtde = 0 then
      insert into ENTREGAS_ITENS_PENDENTES(
        EMPRESA_ID,
        ORCAMENTO_ID,
        PRODUTO_ID,
        ITEM_ID,
        LOTE,
        QUANTIDADE,
        PREVISAO_ENTREGA,
        LOCAL_ID
      )values(
        vEmpresaId,
        vOrcamentoId,
        vItens.PRODUTO_ID,
        vItens.ITEM_ID,
        vItens.LOTE,
        vItens.QUANTIDADE,
        trunc(vPrevisaoEntrega),
        vLocalId
      );
    else
      update ENTREGAS_ITENS_PENDENTES set
        QUANTIDADE = SALDO + greatest(ENTREGUES - vItens.QUANTIDADE, 0) + DEVOLVIDOS + vItens.QUANTIDADE,
        ENTREGUES = greatest(ENTREGUES - vItens.QUANTIDADE, 0)
      where ORCAMENTO_ID = vOrcamentoId
      and EMPRESA_ID = vEmpresaId
      and ITEM_ID = vItens.ITEM_ID
      and LOTE = vItens.LOTE
      and trunc(PREVISAO_ENTREGA) = trunc(vPrevisaoEntrega)
      and LOCAL_ID = vLocalId;
    end if;

  end loop;

  delete from NOTAS_FISCAIS_ITENS
  where NOTA_FISCAL_ID in(
    select
      NOTA_FISCAL_ID
    from
      NOTAS_FISCAIS
    where ENTREGA_ID = iENTREGA_ID
    and NUMERO_NOTA is null
  );

  delete from NOTAS_FISCAIS
  where ENTREGA_ID = iENTREGA_ID
  and NUMERO_NOTA is null;

  delete from ENTREGAS
  where ENTREGA_ID = iENTREGA_ID;

end CANCELAR_ENTREGA;