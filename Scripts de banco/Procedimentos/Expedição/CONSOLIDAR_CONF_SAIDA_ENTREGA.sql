create or replace procedure CONSOLIDAR_CONF_SAIDA_ENTREGA(
  iENTREGA_ID      in number,
  
  iPRODUTOS_IDS    in TIPOS.ArrayOfNumeros,
  iITENS_IDS       in TIPOS.ArrayOfNumeros,  
  iLOTES           in TIPOS.ArrayOfString,
  iNAO_SEPARADOS   in TIPOS.ArrayOfNumeros  
)
is
  i            number;
  vQtde        number default 0;  
  
  vPrevisaoEntrega ENTREGAS.PREVISAO_ENTREGA%type;
  vLocalId         ENTREGAS.LOCAL_ID%type;
  vEmpresaId       ENTREGAS.EMPRESA_ENTREGA_ID%type;
  vOrcamentoId     ENTREGAS.ORCAMENTO_ID%type;
  
begin
  
  select
    EMPRESA_ENTREGA_ID,
    LOCAL_ID,
    ORCAMENTO_ID,
    PREVISAO_ENTREGA
  into
    vEmpresaId,
    vLocalId,
    vOrcamentoId,
    vPrevisaoEntrega
  from
    ENTREGAS
  where ENTREGA_ID = iENTREGA_ID;

  for i in iITENS_IDS.first..iITENS_IDS.last loop 
    if iNAO_SEPARADOS(i) = 0 then
      continue;
    end if;
    
    update ENTREGAS_ITENS set
      NAO_SEPARADOS = iNAO_SEPARADOS(i)
    where ENTREGA_ID = iENTREGA_ID
    and LOTE = iLOTES(i)
    and ITEM_ID = iITENS_IDS(i);
    
    INSERIR_ENTREGAS_ITENS_PEND(
      vEmpresaId,
      vLocalId,
      vOrcamentoId,
      vPrevisaoEntrega,
      iPRODUTOS_IDS(i),
      iITENS_IDS(i),
      iLOTES(i),
      iNAO_SEPARADOS(i)
    );      
  end loop;
  
  update ENTREGAS set
    STATUS = 'AGC'
  where ENTREGA_ID = iENTREGA_ID;

end CONSOLIDAR_CONF_SAIDA_ENTREGA;
/