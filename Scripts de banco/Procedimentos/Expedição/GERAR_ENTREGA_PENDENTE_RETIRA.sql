﻿create or replace procedure GERAR_ENTREGA_PENDENTE_RETIRA(
  iORCAMENTO_ID     in number,
  iEMPRESA_PEND_ID  in number,
  iLOCAL_ID         in number,
  iPREVISAO_ENTREGA in date,
  iPRODUTOS_IDS     in TIPOS.ArrayOfNumeros,
  iITENS_IDS        in TIPOS.ArrayOfNumeros,
  iLOCAIS_IDS       in TIPOS.ArrayOfNumeros,
  iQUANTIDADES      in TIPOS.ArrayOfNumeros,
  iLOTES            in TIPOS.ArrayOfString

)
is
  i            number;
  vQtde        number;
  vRetiradaId  number;
  vEProdutoKit char(1);

  vLogradouro        CADASTROS.LOGRADOURO%type;
  vComplemento       CADASTROS.COMPLEMENTO%type;
  vNumero            CADASTROS.NUMERO%type;
  vBairroId          CADASTROS.BAIRRO_ID%type;
  vPontoReferencia   CADASTROS.PONTO_REFERENCIA%type;
  vCep               CADASTROS.CEP%type;
  vInscricaoEstadual CADASTROS.INSCRICAO_ESTADUAL%type;

  
  cursor cItensPendKit( pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pQUANTIDADE_PRODUTO_KIT in number ) is
  select
    RIP.PRODUTO_ID,
    RIP.ITEM_ID,
    RIP.LOTE,
    KIT.QUANTIDADE * pQUANTIDADE_PRODUTO_KIT as QUANTIDADE
  from
    RETIRADAS_ITENS_PENDENTES RIP
  
  inner join ORCAMENTOS_ITENS OIT
  on RIP.ORCAMENTO_ID = OIT.ORCAMENTO_ID
  and RIP.ITEM_ID = OIT.ITEM_ID
  
  inner join PRODUTOS_KIT KIT
  on KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID
  and OIT.PRODUTO_ID = KIT.PRODUTO_ID
    
  where RIP.ORCAMENTO_ID = iORCAMENTO_ID
  and RIP.EMPRESA_ID = iEMPRESA_PEND_ID
  and OIT.ITEM_KIT_ID = pITEM_KIT_ID;
  
  
  procedure VERIFICAR_PENDENCIA(
    pLOCAL_ID in number,
    pITEM_ID in number,
    pLOTE in string,
    pQUANTIDADE in number
  )
  is
    vPodeDeletarPendencia char(1);
  begin
   
   select
      case when QUANTIDADE = pQUANTIDADE then 'S' else 'N' end as PODE_DELETAR_PENDENCIA
    into
      vPodeDeletarPendencia
    from
      RETIRADAS_ITENS_PENDENTES
    where ORCAMENTO_ID = iORCAMENTO_ID
    and EMPRESA_ID = iEMPRESA_PEND_ID
    and LOCAL_ID = pLOCAL_ID
    and ITEM_ID = pITEM_ID
    and LOTE = pLOTE;
    
    /* Caso a quantidade sendo desagendada seja a mesma quantidade da pendencia, quer dizer que o usuário está desagendando toda a pendência, */
    /* Neste caso vamos deleta-la */
    if vPodeDeletarPendencia = 'S' then
      delete from RETIRADAS_ITENS_PENDENTES
      where ORCAMENTO_ID = iORCAMENTO_ID
      and EMPRESA_ID = iEMPRESA_PEND_ID
      and LOCAL_ID = pLOCAL_ID
      and ITEM_ID = pITEM_ID
      and LOTE = pLOTE;
    else
      /* Baixando a pendência */
      update RETIRADAS_ITENS_PENDENTES set
        QUANTIDADE = QUANTIDADE - pQUANTIDADE
      where ORCAMENTO_ID = iORCAMENTO_ID
      and EMPRESA_ID = iEMPRESA_PEND_ID
      and LOCAL_ID = pLOCAL_ID
      and ITEM_ID = pITEM_ID
      and LOTE = pLOTE;
    end if;
  end;
  
begin

  for i in iPRODUTOS_IDS.first..iPRODUTOS_IDS.last loop  
    /* Baixando a pendência */
    VERIFICAR_PENDENCIA( iLOCAIS_IDS(i), iITENS_IDS(i), iLOTES(i), iQUANTIDADES(i) );

    insert into ENTREGAS_ITENS_A_GERAR_TEMP(
      ORCAMENTO_ID,
      PREVISAO_ENTREGA,
      PRODUTO_ID,
      ITEM_ID,
      QUANTIDADE,
      TIPO_ENTREGA
    )values(
      iORCAMENTO_ID,
      iPREVISAO_ENTREGA,
      iPRODUTOS_IDS(i),
      iITENS_IDS(i),
      iQUANTIDADES(i),
      'EN'
    );
    
    select
      case when TIPO_CONTROLE_ESTOQUE = 'K' then 'S' else 'N' end
    into
      vEProdutoKit
    from
      ORCAMENTOS_ITENS
    where ORCAMENTO_ID = iORCAMENTO_ID
    and ITEM_ID = iITENS_IDS(i);  

    /* Se for o kit, desagendando os componentes do kit */
    if vEProdutoKit = 'S' then                  
      for xItensPendKit in cItensPendKit( iPRODUTOS_IDS(i), iITENS_IDS(i), iQUANTIDADES(i) ) loop
        /* Baixando a pendência do produto que compõe o kit */
        VERIFICAR_PENDENCIA( iLOCAIS_IDS(i), xItensPendKit.ITEM_ID, xItensPendKit.LOTE, xItensPendKit.QUANTIDADE );

        insert into ENTREGAS_ITENS_A_GERAR_TEMP(
          ORCAMENTO_ID,
          PREVISAO_ENTREGA,
          PRODUTO_ID,
          ITEM_ID,
          QUANTIDADE,
          TIPO_ENTREGA
        )values(
          iORCAMENTO_ID,
          iPREVISAO_ENTREGA,
          xItensPendKit.PRODUTO_ID,
          xItensPendKit.ITEM_ID,
          xItensPendKit.QUANTIDADE,
          'EN'
        );
      end loop;
    end if;    
  end loop;

  GERAR_PENDENCIA_ENTREGAS(iORCAMENTO_ID, iEMPRESA_PEND_ID);

  select
    count(*)
  into
    vQtde
  from
    RETIRADAS_ITENS_PENDENTES
  where ORCAMENTO_ID = iORCAMENTO_ID
  and EMPRESA_ID = iEMPRESA_PEND_ID
  and LOCAL_ID = iLOCAL_ID;

  if vQtde = 0 then
    delete from RETIRADAS_PENDENTES
    where ORCAMENTO_ID = iORCAMENTO_ID
    and EMPRESA_ID = iEMPRESA_PEND_ID
    and LOCAL_ID = iLOCAL_ID;
  end if;

  select
    CAD.LOGRADOURO,
    CAD.COMPLEMENTO,
    CAD.NUMERO,
    CAD.BAIRRO_ID,
    CAD.PONTO_REFERENCIA,
    CAD.CEP,
    CAD.INSCRICAO_ESTADUAL
  into
    vLogradouro,
    vComplemento,
    vNumero,
    vBairroId,
    vPontoReferencia,
    vCep,
    vInscricaoEstadual
  from
    CADASTROS CAD

  inner join ORCAMENTOS ORC
  on CAD.CADASTRO_ID = ORC.CLIENTE_ID

  where ORC.ORCAMENTO_ID = iORCAMENTO_ID;

  /* Atualizando o bairro se não houver */
  /* Isso acontece quando a venda era retirar lojas redes e passou a ser para entrega */
  update ORCAMENTOS set
    LOGRADOURO = vLogradouro,
    COMPLEMENTO = vComplemento,
    NUMERO = vNumero,
    PONTO_REFERENCIA = vPontoReferencia,
    BAIRRO_ID = vBairroId,
    CEP = vCep,
    INSCRICAO_ESTADUAL = vInscricaoEstadual
  where ORCAMENTO_ID = iORCAMENTO_ID
  and BAIRRO_ID is null;

end GERAR_ENTREGA_PENDENTE_RETIRA;
/