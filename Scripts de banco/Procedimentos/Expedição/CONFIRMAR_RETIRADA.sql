create or replace procedure CONFIRMAR_RETIRADA(
  iRETIRADA_ID in number,
  iNOME_PESSOA_RETIRADA in string
)
is

  vEmpresaId     ORCAMENTOS.EMPRESA_ID%type;
  vLocalId       RETIRADAS.LOCAL_ID%type;
  vOrcamentoId   ORCAMENTOS.ORCAMENTO_ID%type;
  vTipoMovimento RETIRADAS.TIPO_MOVIMENTO%type;
  vStatus        ORCAMENTOS.STATUS%type;
  vQtde          number;

  cursor cItens is
  select
    RIT.PRODUTO_ID,
    RIT.QUANTIDADE,
    RIT.ITEM_ID,
    RIT.LOTE
  from
    RETIRADAS_ITENS RIT

  inner join RETIRADAS RET
  on RIT.RETIRADA_ID = RET.RETIRADA_ID

  inner join ORCAMENTOS_ITENS OIT
  on RIT.ITEM_ID = OIT.ITEM_ID
  and RET.ORCAMENTO_ID = OIT.ORCAMENTO_ID
  and OIT.TIPO_CONTROLE_ESTOQUE not in('K', 'A')

  where RIT.RETIRADA_ID = iRETIRADA_ID;

begin
  select
    RET.ORCAMENTO_ID,
    RET.LOCAL_ID,
    RET.TIPO_MOVIMENTO,
    RET.EMPRESA_ID,
    ORC.STATUS
  into
    vOrcamentoId,
    vLocalId,
    vTipoMovimento,
    vEmpresaId,
    vStatus
  from
    RETIRADAS RET

  inner join ORCAMENTOS ORC
  on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID

  where RET.RETIRADA_ID = iRETIRADA_ID;

  if vStatus not in('VE', 'RE') then
    ERRO('O pedido que esta sendo confirmado n?o esta recebido, verifique!');
  end if;

  /* Verificando se existem notas com numero gerado e pendentes de emiss?o */
  select
    count(*)
  into
    vQtde
  from
    NOTAS_FISCAIS
  where ORCAMENTO_BASE_ID = vOrcamentoId
  and nvl(NUMERO_NOTA, 0) > 0
  and STATUS = 'N';

  if vQtde > 0 then
    ERRO('Existe nota fiscal para o orcamento ' || vOrcamentoId || ' pendente de emiss?o, faca a emiss?o da nota primeiramente!');
  end if;

  update RETIRADAS set
    CONFIRMADA = 'S',
    NOME_PESSOA_RETIRADA = iNOME_PESSOA_RETIRADA
  where RETIRADA_ID = iRETIRADA_ID;

  for vItens in cItens loop
    /* Movimentando o estoque fisico */
    MOVIMENTAR_ESTOQUE(
      vEmpresaId,
      vItens.PRODUTO_ID,
      vLocalId,
      vItens.LOTE,
      vItens.QUANTIDADE,
      'RTA',
      null,
      null
    );
  end loop;

end CONFIRMAR_RETIRADA;
