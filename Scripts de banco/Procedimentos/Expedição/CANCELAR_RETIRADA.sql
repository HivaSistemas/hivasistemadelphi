﻿create or replace procedure CANCELAR_RETIRADA(iRETIRADA_ID in number)
is

  vQtde            number;
  vEmpresaId       RETIRADAS.EMPRESA_ID%type;
  vLocalId         RETIRADAS.LOCAL_ID%type;
  vOrcamentoId     ORCAMENTOS.ORCAMENTO_ID%type;
  vTipoMovimento   RETIRADAS.TIPO_MOVIMENTO%type;
  vStatus          ORCAMENTOS.STATUS%type;  

  cursor cItens is
  select
    PRODUTO_ID,
    QUANTIDADE,
    ITEM_ID,
    LOTE
  from
    RETIRADAS_ITENS
  where RETIRADA_ID = iRETIRADA_ID;   

begin
  select
    count(*)
  into
    vQtde
  from
    NOTAS_FISCAIS
  where RETIRADA_ID = iRETIRADA_ID
  and STATUS = 'E';

  if vQtde > 0 then
    ERRO('Existe nota fiscal emitida para esta retirada, cancelamento não permitido!');
  end if;

  select
    RET.ORCAMENTO_ID,
    RET.TIPO_MOVIMENTO,
    RET.EMPRESA_ID,
    ORC.STATUS,
    RET.LOCAL_ID
  into
    vOrcamentoId,
    vTipoMovimento,
    vEmpresaId,
    vStatus,
    vLocalId
  from
    RETIRADAS RET
    
  inner join ORCAMENTOS ORC
  on RET.ORCAMENTO_ID = ORC.ORCAMENTO_ID
  
  where RET.RETIRADA_ID = iRETIRADA_ID;

  if vStatus <> 'RE' then
    ERRO('O pedido que está sendo cancelado não está recebido, verifique!');
  end if;
  
  select count(*)
  into vQtde
  from
    RETIRADAS_PENDENTES
  where ORCAMENTO_ID = vOrcamentoId
  and EMPRESA_ID = vEmpresaId
  and LOCAL_ID = vLocalId;
    
  if vQtde = 0 then
    insert into RETIRADAS_PENDENTES(
      EMPRESA_ID,
      ORCAMENTO_ID,
      LOCAL_ID
    )values(
      vEmpresaId,
      vOrcamentoId,
      vLocalId
    );    
  end if;

  for vItens in cItens loop

    delete from RETIRADAS_ITENS
    where RETIRADA_ID = iRETIRADA_ID
    and ITEM_ID = vItens.ITEM_ID;

    select
      count(*)
    into
      vQtde
    from
      RETIRADAS_ITENS_PENDENTES
    where ORCAMENTO_ID = vOrcamentoId
    and EMPRESA_ID = vEmpresaId
    and ITEM_ID = vItens.ITEM_ID
    and LOTE = vItens.LOTE
    and LOCAL_ID = vLocalId;

    if vQtde = 0 then
      insert into RETIRADAS_ITENS_PENDENTES(
        EMPRESA_ID,
        LOCAL_ID,
        ORCAMENTO_ID,
        PRODUTO_ID,
        ITEM_ID,
        LOTE,
        QUANTIDADE
      )values(
        vEmpresaId,
        vLocalId,
        vOrcamentoId,
        vItens.PRODUTO_ID,
        vItens.ITEM_ID,
        vItens.LOTE,
        vItens.QUANTIDADE
      );      
    else
      update RETIRADAS_ITENS_PENDENTES set
        QUANTIDADE = SALDO + greatest(ENTREGUES - vItens.QUANTIDADE, 0) + DEVOLVIDOS + vItens.QUANTIDADE,
        ENTREGUES = greatest(ENTREGUES - vItens.QUANTIDADE, 0)
      where ORCAMENTO_ID = vOrcamentoId
      and EMPRESA_ID = vEmpresaId
      and LOCAL_ID = vLocalId
      and ITEM_ID = vItens.ITEM_ID
      and LOTE = vItens.LOTE;
    end if;
    
  end loop;

  delete from NOTAS_FISCAIS_ITENS
  where NOTA_FISCAL_ID in(
    select
      NOTA_FISCAL_ID
    from
      NOTAS_FISCAIS
    where RETIRADA_ID = iRETIRADA_ID
    and NUMERO_NOTA is null
  );

  delete from NOTAS_FISCAIS
  where RETIRADA_ID = iRETIRADA_ID
  and NUMERO_NOTA is null;
  
  delete from RETIRADAS
  where RETIRADA_ID = iRETIRADA_ID;

end CANCELAR_RETIRADA;
/
