﻿create or replace procedure GERAR_RETIRADAS_ATO(
  iORCAMENTO_ID       in number,
  iEMPRESA_GERACAO_ID in number
)
is  
  type RecRetiradasGeradas is record(
    EMPRESA_ID   EMPRESAS.EMPRESA_ID%type,
    LOCAL_ID     RETIRADAS.LOCAL_ID%type,
    RETIRADA_ID  RETIRADAS.RETIRADA_ID%type
  );  
  type ArrayOfRecRetiradasGeradas is table of RecRetiradasGeradas index by naturaln; 

  vRetiradaId            RETIRADAS.RETIRADA_ID%type;
  vQtdeAgendar           number;
  vQuantidadeRestante    number;  

  vQtdeRestanteItemKit   number;
  vQtdeAgendarKitEmpresa number;

  vRetiradasGeradas      ArrayOfRecRetiradasGeradas;
  vDefinirLocalManual    PARAMETROS.DEFINIR_LOCAL_MANUAL%type;
  
  cursor cLocaisDefinidos(pPRODUTO_ID in number, pORCAMENTO_ID in number) is
  select
    ORCAMENTO_ID,
	  PRODUTO_ID,
	  LOCAL_ID,
	  QUANTIDADE_ATO,
    EMPRESA_ID,
	  ITEM_ID,
    LOTE
  from
    ORCAMENTOS_ITENS_DEF_LOCAIS
  
  where PRODUTO_ID = pPRODUTO_ID
  and ORCAMENTO_ID = pORCAMENTO_ID
  and QUANTIDADE_ATO > 0
  and STATUS = 'PEN'
  
  order by LOCAL_ID;

  cursor cItens is
  select
    ITE.PRODUTO_ID,
    ITE.ITEM_ID,
    ITE.QUANTIDADE,
    ITE.TIPO_ENTREGA,
    PRO.ACEITAR_ESTOQUE_NEGATIVO,
    ITE.PREVISAO_ENTREGA,
    OIT.TIPO_CONTROLE_ESTOQUE
  from
    ENTREGAS_ITENS_A_GERAR_TEMP ITE    
  
  inner join PRODUTOS PRO
  on ITE.PRODUTO_ID = PRO.PRODUTO_ID      
  
  inner join ORCAMENTOS_ITENS OIT
  on ITE.ORCAMENTO_ID = OIT.ORCAMENTO_ID
  and ITE.PRODUTO_ID = OIT.PRODUTO_ID
  and ITE.ITEM_ID = OIT.ITEM_ID
  and OIT.ITEM_KIT_ID is null /* Não trazer produtos que compõe o kit, este tratamento será feito na hora do agendamento */
  
  where ITE.ORCAMENTO_ID = iORCAMENTO_ID
  and ITE.TIPO_ENTREGA = 'RA'
  
  order by
    ITE.ITEM_ID;

    
  cursor cEstoques(pPRODUTO_ID in number) is
  select
    GES.EMPRESA_GRUPO_ID as EMPRESA_ID,
    DIV.PRODUTO_ID,
    DIV.DISPONIVEL,
    DIV.LOTE,
    DIV.LOCAL_ID
  from
    VW_ESTOQUES_DIVISAO DIV

  inner join GRUPOS_ESTOQUES GES
  on DIV.EMPRESA_ID = GES.EMPRESA_GRUPO_ID
  and GES.TIPO = 'A'
  and GES.EMPRESA_ID = iEMPRESA_GERACAO_ID

  inner join PRODUTOS_ORDENS_LOC_ENTREGAS POL
  on DIV.EMPRESA_ID = POL.EMPRESA_ID
  and DIV.PRODUTO_PAI_ID = POL.PRODUTO_ID
  and DIV.LOCAL_ID = POL.LOCAL_ID
  and POL.TIPO = 'A'

  where DIV.PRODUTO_ID = pPRODUTO_ID
  and case when DIV.ACEITAR_ESTOQUE_NEGATIVO = 'S' then 1 else DIV.DISPONIVEL end > 0
  order by
    GES.ORDEM,
    POL.ORDEM;


  cursor cEmpresasEstoqueKit is
  select
    EMPRESA_GRUPO_ID as EMPRESA_ID
  from
    GRUPOS_ESTOQUES
  where TIPO = 'A'
  and EMPRESA_ID = iEMPRESA_GERACAO_ID
  order by
    ORDEM;


  cursor cOrdemLocaisProdutoKit( pPRODUTO_KIT_ID in number, pEMPRESA_ESTOQUE_ID in number )
  is
  select
    LOCAL_ID
  from
    PRODUTOS_ORDENS_LOC_ENTREGAS
  where PRODUTO_ID = pPRODUTO_KIT_ID
  and EMPRESA_ID = pEMPRESA_ESTOQUE_ID
  and TIPO = 'A'
  order by
    ORDEM;

    
  cursor cItensKit(pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pQUANTIDADE_KITS in number) is
  select
    KIT.PRODUTO_ID,
    ITE.ITEM_ID,
    KIT.QUANTIDADE * pQUANTIDADE_KITS as QUANTIDADE,
    PRO.ACEITAR_ESTOQUE_NEGATIVO
  from
    ORCAMENTOS_ITENS ITE

  inner join PRODUTOS_KIT KIT
  on ITE.PRODUTO_ID = KIT.PRODUTO_ID
  and KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID

  inner join PRODUTOS PRO
  on KIT.PRODUTO_ID = PRO.PRODUTO_ID

  where ITE.ITEM_KIT_ID = pITEM_KIT_ID
  and ORCAMENTO_ID = iORCAMENTO_ID;


  cursor cItensDefLotes( pITEM_ID in number ) is
  select
    ITE.PRODUTO_ID,
    OIL.LOTE,
    OIL.QUANTIDADE_RETIRAR_ATO as QUANTIDADE_AGENDAR
  from
    ORCAMENTOS_ITENS_DEF_LOTES OIL

  inner join ORCAMENTOS_ITENS ITE
  on OIL.ORCAMENTO_ID = ITE.ORCAMENTO_ID
  and OIL.ITEM_ID = ITE.ITEM_ID

  where OIL.ORCAMENTO_ID = iORCAMENTO_ID
  and OIL.ITEM_ID = pITEM_ID;


  cursor cItensDefLotesEstoques( pPRODUTO_ID in number, pLOTE in string ) is
  select
    DIV.EMPRESA_ID,
    DIV.LOCAL_ID,
    DIV.DISPONIVEL
  from
    VW_ESTOQUES_DIVISAO DIV

  inner join GRUPOS_ESTOQUES GRU
  on GRU.TIPO = 'A'
  and GRU.EMPRESA_GRUPO_ID = iEMPRESA_GERACAO_ID
  and DIV.EMPRESA_ID = GRU.EMPRESA_ID

  inner join PRODUTOS_ORDENS_LOC_ENTREGAS POL
  on DIV.EMPRESA_ID = POL.EMPRESA_ID
  and DIV.PRODUTO_ID = POL.PRODUTO_ID
  and DIV.LOCAL_ID = POL.LOCAL_ID
  and POL.TIPO = 'A'

  where DIV.PRODUTO_ID = pPRODUTO_ID
  and DIV.LOTE = pLOTE

  order by
    case when DIV.DISPONIVEL <= 0 then 1 else 0 end,
    POL.ORDEM,
    DIV.DISPONIVEL;


  cursor cEstoqueItemKit( pEMPRESA_ID in number, pPRODUTO_ID in number, pLOCAL_ID in number ) is
  select
    DIV.LOCAL_ID,
    DIV.DISPONIVEL,
    DIV.LOTE
  from
    VW_ESTOQUES_DIVISAO DIV

  where DIV.EMPRESA_ID = pEMPRESA_ID
  and DIV.PRODUTO_ID = pPRODUTO_ID
  and DIV.LOCAL_ID = pLOCAL_ID

  order by
    DIV.DISPONIVEL desc;

    
  procedure ADD_VETOR_RETIRADAS(pEMPRESA_ID in number, pLOCAL_ID in number, pRETIRADA_ID in number)
  is        
    vPosic number;
  begin
    vPosic := nvl(vRetiradasGeradas.last, -1) + 1;
 
    vRetiradasGeradas(vPosic).EMPRESA_ID  := pEMPRESA_ID;
    vRetiradasGeradas(vPosic).LOCAL_ID    := pLOCAL_ID;
    vRetiradasGeradas(vPosic).RETIRADA_ID := pRETIRADA_ID;
  end;
    
  function GET_RETIRADA_ID(pEMPRESA_ID in number, pLOCAL_ID in number)
  return number
  is
    i        number;
    vRetorno number;
  begin
    vRetorno := -1;         

    if vRetiradasGeradas.count = 0 then
      return vRetorno;
    end if;
        
    for i in vRetiradasGeradas.first..vRetiradasGeradas.last loop
      if vRetiradasGeradas(i).EMPRESA_ID = pEMPRESA_ID and vRetiradasGeradas(i).LOCAL_ID = pLOCAL_ID then
        vRetorno := vRetiradasGeradas(i).RETIRADA_ID;
        return vRetorno;
      end if;
    end loop;   
    
    return vRetorno;
  end;
  
  procedure INSERIR_CAPA_RETIRADA(pEMPRESA_ID in number, pLOCAL_ID in number) is
  begin
    vRetiradaId := GET_RETIRADA_ID(pEMPRESA_ID, pLOCAL_ID);
    if vRetiradaId = -1 then
      select SEQ_RETIRADAS_ID.nextval
      into vRetiradaId
      from dual;

      insert into RETIRADAS(
        RETIRADA_ID,
        EMPRESA_ID,
        LOCAL_ID,
        ORCAMENTO_ID,
        CONFIRMADA,
        TIPO_MOVIMENTO
      )values(
        vRetiradaId,
        pEMPRESA_ID,
        pLOCAL_ID,
        iORCAMENTO_ID,
        'N',
        'A'
      );
      
      ADD_VETOR_RETIRADAS(pEMPRESA_ID, pLOCAL_ID, vRetiradaId);
    end if;
  end;
  
  procedure INSERIR_RETIRADA_ITEM(
    pPRODUTO_ID in number,
    pITEM_ID in number,
    pLOTE in string,
    pQUANTIDADE_AGENDAR in number
  ) is
  begin
    update ORCAMENTOS_ITENS set
      QUANTIDADE_RETIRAR_ATO = QUANTIDADE_RETIRAR_ATO - pQUANTIDADE_AGENDAR
    where ORCAMENTO_ID = iORCAMENTO_ID
    and ITEM_ID = pITEM_ID;

    insert into RETIRADAS_ITENS(
      RETIRADA_ID,
      PRODUTO_ID,
      ITEM_ID,
      LOTE,
      QUANTIDADE
    )values(
      vRetiradaId,
      pPRODUTO_ID,
      pITEM_ID,
      pLOTE,
      pQUANTIDADE_AGENDAR
    );
  end;
  
begin
  select
    DEFINIR_LOCAL_MANUAL
  into
    vDefinirLocalManual
  from
    PARAMETROS;
  
  for xItens in cItens loop

    /* SE o controle de estoque do prodtuo é por LOTE */
    if xItens.TIPO_CONTROLE_ESTOQUE in ('L', 'G', 'P') then
      if vDefinirLocalManual = 'S' then
        for xLocaisDefinidos in cLocaisDefinidos(xItens.PRODUTO_ID, iORCAMENTO_ID) loop

          INSERIR_CAPA_RETIRADA(xLocaisDefinidos.EMPRESA_ID, xLocaisDefinidos.LOCAL_ID);

          INSERIR_RETIRADA_ITEM(
            xLocaisDefinidos.PRODUTO_ID,
            xItens.ITEM_ID,
            xLocaisDefinidos.LOTE,
            xLocaisDefinidos.QUANTIDADE_ATO
          );

          update ORCAMENTOS_ITENS_DEF_LOCAIS set
            QUANTIDADE_ATO = 0
          where ITEM_ID = xLocaisDefinidos.ITEM_ID
          and ORCAMENTO_ID = iORCAMENTO_ID;

        end loop;
      else
        for xItensDefLotes in cItensDefLotes( xItens.ITEM_ID ) loop
          vQuantidadeRestante := xItensDefLotes.QUANTIDADE_AGENDAR;

          for xItensDefLoteEstoq in cItensDefLotesEstoques( xItens.PRODUTO_ID, xItensDefLotes.LOTE ) loop
            INSERIR_CAPA_RETIRADA(xItensDefLoteEstoq.EMPRESA_ID, xItensDefLoteEstoq.LOCAL_ID);

            if xItensDefLoteEstoq.DISPONIVEL >= vQuantidadeRestante then
              vQtdeAgendar := vQuantidadeRestante;
              vQuantidadeRestante := 0;
            else
              vQtdeAgendar := xItensDefLoteEstoq.DISPONIVEL;
              vQuantidadeRestante := vQuantidadeRestante - vQtdeAgendar;
            end if;

            INSERIR_RETIRADA_ITEM(
              xItens.PRODUTO_ID,
              xItens.ITEM_ID,
              xItensDefLotes.LOTE,
              vQtdeAgendar
            );

            if vQuantidadeRestante = 0 then
              exit;
            end if;
          end loop;

          if vQuantidadeRestante > 0 then
            ERRO(
              'Estoque insuficiente para o produto ' || xItens.PRODUTO_ID || '!' || chr(13) ||
              'Faltam: ' || NFORMAT(vQuantidadeRestante, 0)
            );
          end if;
        end loop;
      end if;

    /* SE o controle do estoque for Kit */
    /* K - Kit desmembrado */
    /* A - Kit agruapado */
    elsif xItens.TIPO_CONTROLE_ESTOQUE in('K', 'A') then

      vQuantidadeRestante := xItens.QUANTIDADE;

      if xItens.TIPO_CONTROLE_ESTOQUE = 'K' then
        for xItensCompoeKit in cItensKit(xItens.PRODUTO_ID, xItens.ITEM_ID, xItens.QUANTIDADE) loop

          vQtdeRestanteItemKit := xItensCompoeKit.QUANTIDADE;

          for xEstoques in cEstoques(xItensCompoeKit.PRODUTO_ID) loop
            if xItensCompoeKit.ACEITAR_ESTOQUE_NEGATIVO = 'S' then
              vQtdeAgendar := vQtdeRestanteItemKit;
              vQtdeRestanteItemKit := 0;
            elsif xEstoques.DISPONIVEL > 0 then
              if xEstoques.DISPONIVEL >= vQuantidadeRestante  then
                vQtdeAgendar := vQtdeRestanteItemKit;
                vQtdeRestanteItemKit := 0;
              else
                vQtdeAgendar := xEstoques.DISPONIVEL;
                vQtdeRestanteItemKit := vQtdeRestanteItemKit - vQtdeAgendar;
              end if;
            else
              /* Se não aceita estoque negativo, passando para o próximo local. */
              continue;
            end if;

            INSERIR_CAPA_RETIRADA(xEstoques.EMPRESA_ID, xEstoques.LOCAL_ID);

            INSERIR_RETIRADA_ITEM(
              xItensCompoeKit.PRODUTO_ID,
              xItensCompoeKit.ITEM_ID,
              xEstoques.LOTE,
              vQtdeAgendar
            );

            if vQtdeRestanteItemKit = 0 then
              vQuantidadeRestante := 0;
              exit;
            end if;
          end loop;

          if vQtdeRestanteItemKit > 0 then
            ERRO(
              'Estoque insuficiente para o produto ' || xItensCompoeKit.PRODUTO_ID || ' que compõe o kit ' ||  xItens.PRODUTO_ID || '!' || chr(13) ||
              'Faltam: ' || NFORMAT(vQtdeRestanteItemKit, 0)
            );
          end if;

        end loop;

      /* Se kit entrega agrupada, detalhe é que neste todos os itens devem estar no mesmo local */
      else
        for xEmpresasEstoqueKit in cEmpresasEstoqueKit loop
          vQtdeAgendarKitEmpresa := QUANTIDADE_KITS_DISP_EMPRESA(xEmpresasEstoqueKit.EMPRESA_ID, xItens.PRODUTO_ID);

          -- Se não pode agendar nada, passa pra frente
          if xItens.ACEITAR_ESTOQUE_NEGATIVO = 'S' then
            vQtdeAgendarKitEmpresa := xItens.QUANTIDADE;
          elsif vQtdeAgendarKitEmpresa = 0 then
            continue;
          end if;

          if vQtdeAgendarKitEmpresa >= vQuantidadeRestante then
            vQtdeAgendarKitEmpresa := vQuantidadeRestante;
          end if;

          for xOrdemLocaisProdutoKit in cOrdemLocaisProdutoKit(xItens.PRODUTO_ID, xEmpresasEstoqueKit.EMPRESA_ID) loop

            if xItens.ACEITAR_ESTOQUE_NEGATIVO = 'S' then
              vQtdeAgendar := vQtdeAgendarKitEmpresa;
            else
              vQtdeAgendar := QUANTIDADE_KITS_DISP_LOCAL(xEmpresasEstoqueKit.EMPRESA_ID, xOrdemLocaisProdutoKit.LOCAL_ID, xItens.PRODUTO_ID);
              if vQtdeAgendar >= vQtdeAgendarKitEmpresa then
                vQtdeAgendar := vQtdeAgendarKitEmpresa;
              end if;
            end if;

            /* Se não pode agendar no local passando para o próximo */
            if vQtdeAgendar = 0 then
              continue;
            end if;

            vQtdeAgendarKitEmpresa := vQtdeAgendarKitEmpresa - vQtdeAgendar;
            INSERIR_CAPA_RETIRADA(xEmpresasEstoqueKit.EMPRESA_ID, xOrdemLocaisProdutoKit.LOCAL_ID);

            for xItensKit in cItensKit(xItens.PRODUTO_ID, xItens.ITEM_ID, vQtdeAgendar) loop

              for xEstoqueItemKit in cEstoqueItemKit(xEmpresasEstoqueKit.EMPRESA_ID, xItensKit.PRODUTO_ID, xOrdemLocaisProdutoKit.LOCAL_ID) loop

                INSERIR_RETIRADA_ITEM(
                  xItensKit.PRODUTO_ID,
                  xItensKit.ITEM_ID,
                  xEstoqueItemKit.LOTE,
                  vQtdeAgendar
                );

                if vQtdeAgendarKitEmpresa = 0 then
                  exit;
                end if;
              end loop;
            end loop;

            /* Inserindo o próprio produto kit */
            INSERIR_RETIRADA_ITEM(
              xItens.PRODUTO_ID,
              xItens.ITEM_ID,
              '???',
              vQtdeAgendar
            );

            vQuantidadeRestante := vQuantidadeRestante - vQtdeAgendar;

          end loop;

          if vQtdeAgendarKitEmpresa > 0 then
            ERRO(
              'Estoque insuficiente para o produto kit ' || xItens.PRODUTO_ID || '!' || chr(13) ||
              'Faltam: ' || NFORMAT(vQtdeAgendarKitEmpresa, 0) || chr(13) ||
              'Verifique os locais dos produtos que compõe este kit!'
            );
          end if;

        end loop;
      end if;

      if vQuantidadeRestante > 0 then
        ERRO(
          'Estoque insuficiente para o produto kit ' || xItens.PRODUTO_ID || '!' || chr(13) ||
          'Faltam: ' || NFORMAT(vQuantidadeRestante, 0)
        );
      end if;

    /* Se for produto normal */
    else
      if vDefinirLocalManual = 'S' then
        for xLocaisDefinidos in cLocaisDefinidos(xItens.PRODUTO_ID, iORCAMENTO_ID) loop
		
          INSERIR_CAPA_RETIRADA(xLocaisDefinidos.EMPRESA_ID, xLocaisDefinidos.LOCAL_ID);

          INSERIR_RETIRADA_ITEM(
            xLocaisDefinidos.PRODUTO_ID,
            xItens.ITEM_ID,
            '???',
            xLocaisDefinidos.QUANTIDADE_ATO
          );
		  
          update ORCAMENTOS_ITENS_DEF_LOCAIS set
            QUANTIDADE_ATO = 0
          where ITEM_ID = xLocaisDefinidos.ITEM_ID
          and ORCAMENTO_ID = iORCAMENTO_ID;
		  
        end loop;
      else

        vQuantidadeRestante := xItens.QUANTIDADE;
        for xEstoques in cEstoques(xItens.PRODUTO_ID) loop

          if xItens.ACEITAR_ESTOQUE_NEGATIVO = 'S' then
            vQtdeAgendar := vQuantidadeRestante;
            vQuantidadeRestante := 0;
          elsif xEstoques.DISPONIVEL > 0 then
            if xEstoques.DISPONIVEL >= vQuantidadeRestante  then
              vQtdeAgendar := vQuantidadeRestante;
              vQuantidadeRestante := 0;
            else
              vQtdeAgendar := xEstoques.DISPONIVEL;
              vQuantidadeRestante := vQuantidadeRestante - vQtdeAgendar;
            end if;
          else
            /* Se não aceita estoque negativo, passando para o próximo local. */
            continue;
          end if;

          INSERIR_CAPA_RETIRADA(xEstoques.EMPRESA_ID, xEstoques.LOCAL_ID);

          INSERIR_RETIRADA_ITEM(
            xItens.PRODUTO_ID,
            xItens.ITEM_ID,
            xEstoques.LOTE,
            vQtdeAgendar
          );

          if vQuantidadeRestante = 0 then
            exit;
          end if;
        end loop;

        if vQuantidadeRestante > 0 then
          ERRO(
            'Estoque insuficiente para o produto ' || xItens.PRODUTO_ID || '!' || chr(13) ||
            'Faltam: ' || NFORMAT(vQuantidadeRestante, 0)
          );
        end if;

      end if;
    end if;
  end loop;
  
  /* Limpando a tabela no final do processo */
  delete from ENTREGAS_ITENS_A_GERAR_TEMP
  where ORCAMENTO_ID = iORCAMENTO_ID
  and TIPO_ENTREGA = 'RA';

end GERAR_RETIRADAS_ATO;
/
