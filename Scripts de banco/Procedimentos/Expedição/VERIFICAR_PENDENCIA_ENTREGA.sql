create or replace procedure VERIFICAR_PENDENCIA_ENTREGA(
  pEMPRESA_ID           in number,
  pLOCAL_ID             in number,
  pORCAMENTO_ID         in number,
  pPREVISAO_ENTREGA     in date,
  pTIPO_RETORNO_ENTREGA in string
)
is
  vQtde number;
begin

  select
    count(*)
  into
    vQtde
  from
    ENTREGAS_PENDENTES
  where EMPRESA_ID = pEMPRESA_ID
  and LOCAL_ID = pLOCAL_ID
  and ORCAMENTO_ID = pORCAMENTO_ID
  and PREVISAO_ENTREGA = pPREVISAO_ENTREGA;

  if vQtde = 0 then
    insert into ENTREGAS_PENDENTES(
      EMPRESA_ID,
      LOCAL_ID,
      ORCAMENTO_ID,
      PREVISAO_ENTREGA,
      TIPO_RETORNO_ENTREGA
    )values(
      pEMPRESA_ID,
      pLOCAL_ID,
      pORCAMENTO_ID,
      pPREVISAO_ENTREGA,
      pTIPO_RETORNO_ENTREGA
    );
  else
    update ENTREGAS_PENDENTES set
      TIPO_RETORNO_ENTREGA = pTIPO_RETORNO_ENTREGA
    where EMPRESA_ID = pEMPRESA_ID
    and LOCAL_ID = pLOCAL_ID
    and ORCAMENTO_ID = pORCAMENTO_ID
    and PREVISAO_ENTREGA = pPREVISAO_ENTREGA;
  end if;

end VERIFICAR_PENDENCIA_ENTREGA;