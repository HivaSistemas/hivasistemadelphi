﻿create or replace procedure GERAR_RETIRADA_RETIRA_PENDENTE(
  iORCAMENTO_ID    in number,
  iEMPRESA_PEND_ID in number,
  iLOCAL_ID        in number,
  iTIPO_NOTA_GERAR in string,
  iAGRUPADOR_ID    in number,
  iPRODUTOS_IDS    in TIPOS.ArrayOfNumeros,
  iITENS_IDS       in TIPOS.ArrayOfNumeros,
  iLOCAIS_IDS      in TIPOS.ArrayOfNumeros,
  iQUANTIDADES     in TIPOS.ArrayOfNumeros,
  iLOTES           in TIPOS.ArrayOfString,
  oRETIRADA_ID     out number
)
is
  i                     number;
  vQtde                 number default 0;
  vRetiradaId           number;
  vEProdutoKitAgrupado  char(1);
  
  cursor cItensKit(pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pQUANTIDADE_KITS in number) is
  select
    KIT.PRODUTO_ID,
    ITE.ITEM_ID,
    KIT.QUANTIDADE * pQUANTIDADE_KITS as QUANTIDADE
  from
    ORCAMENTOS_ITENS ITE

  inner join PRODUTOS_KIT KIT
  on ITE.PRODUTO_ID = KIT.PRODUTO_ID
  and KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID

  where ITE.ITEM_KIT_ID = pITEM_KIT_ID
  and ORCAMENTO_ID = iORCAMENTO_ID;
  
  cursor cItensPendKit( pITEM_ID in number, pLOCAL_ID in number ) is
  select
    PRODUTO_ID,
    LOTE,
    SALDO
  from
    RETIRADAS_ITENS_PENDENTES
  where ORCAMENTO_ID = iORCAMENTO_ID
  and EMPRESA_ID = iEMPRESA_PEND_ID
  and ITEM_ID = pITEM_ID
  and LOCAL_ID = pLOCAL_ID
  order by
    SALDO desc;
  
begin

  select SEQ_RETIRADAS_ID.nextval
  into vRetiradaId
  from dual;  

  insert into RETIRADAS(
    RETIRADA_ID,
    EMPRESA_ID,
    LOCAL_ID,
    ORCAMENTO_ID,
    CONFIRMADA,
    TIPO_MOVIMENTO,
    TIPO_NOTA_GERAR,
    AGRUPADOR_ID
  )values(
    vRetiradaId,
    iEMPRESA_PEND_ID,
    iLOCAL_ID,
    iORCAMENTO_ID,
    'N',
    'R',
    iTIPO_NOTA_GERAR,
    nvl(iAGRUPADOR_ID, 0)
  );

  for i in iPRODUTOS_IDS.first..iPRODUTOS_IDS.last loop
  
    /* Baixando a pendência */
    update RETIRADAS_ITENS_PENDENTES set
      ENTREGUES = ENTREGUES + iQUANTIDADES(i)
    where ORCAMENTO_ID = iORCAMENTO_ID
    and EMPRESA_ID = iEMPRESA_PEND_ID
    and ITEM_ID = iITENS_IDS(i)
    and LOCAL_ID = iLOCAIS_IDS(i)
    and LOTE = iLOTES(i);

    /* Inserindo de fato a retirada pelo cliente */
    insert into RETIRADAS_ITENS(
      RETIRADA_ID,
      PRODUTO_ID,
      ITEM_ID,
      LOTE,
      QUANTIDADE
    )values(
      vRetiradaId,
      iPRODUTOS_IDS(i),
      iITENS_IDS(i),
      iLOTES(i),
      iQUANTIDADES(i)
    );

    select
      case when TIPO_CONTROLE_ESTOQUE = 'A' then 'S' else 'N' end
    into
      vEProdutoKitAgrupado
    from
      ORCAMENTOS_ITENS
    where ORCAMENTO_ID = iORCAMENTO_ID
    and ITEM_ID = iITENS_IDS(i);

    /* Se for o kit, baixando os componentes do kit */
    if vEProdutoKitAgrupado = 'S' then
      
      for xItensKit in cItensKit(iPRODUTOS_IDS(i), iITENS_IDS(i), iQUANTIDADES(i)) loop

        for xItensPendKit in cItensPendKit( xItensKit.ITEM_ID, iLOCAIS_IDS(i)) loop
          update RETIRADAS_ITENS_PENDENTES set
            ENTREGUES = ENTREGUES + xItensPendKit.SALDO
          where ORCAMENTO_ID = iORCAMENTO_ID
          and EMPRESA_ID = iEMPRESA_PEND_ID
          and ITEM_ID = xItensKit.ITEM_ID
          and LOCAL_ID = iLOCAIS_IDS(i)
          and LOTE = xItensPendKit.LOTE;
          
          /* Inserindo de fato a retirada pelo cliente */
          insert into RETIRADAS_ITENS(
            RETIRADA_ID,
            PRODUTO_ID,
            ITEM_ID,
            LOTE,
            QUANTIDADE
          )values(
            vRetiradaId,
            xItensPendKit.PRODUTO_ID,
            xItensKit.ITEM_ID,
            xItensPendKit.LOTE,
            xItensPendKit.SALDO
          );
        end loop;
      end loop;
      
    end if;
    
  end loop;

  oRETIRADA_ID := vRetiradaId;

end GERAR_RETIRADA_RETIRA_PENDENTE;
/