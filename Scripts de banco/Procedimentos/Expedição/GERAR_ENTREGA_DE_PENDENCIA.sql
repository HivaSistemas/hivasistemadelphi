﻿create or replace procedure GERAR_ENTREGA_DE_PENDENCIA(
  iORCAMENTO_ID     in number,
  iEMPRESA_PEND_ID  in number,
  iLOCAL_ID         in number,
  iPREVISAO_ENTREGA in date,
  iAGRUPADOR_ID     in number,
  iTIPO_NOTA_GERAR  in string,

  iPRODUTOS_IDS    in TIPOS.ArrayOfNumeros,
  iITENS_IDS       in TIPOS.ArrayOfNumeros,
  iLOCAIS_IDS      in TIPOS.ArrayOfNumeros,
  iQUANTIDADES     in TIPOS.ArrayOfNumeros,
  iLOTES           in TIPOS.ArrayOfString,
  oENTREGA_ID     out number
)
is
  i                           number;
  vQtde                       number default 0;
  vEntregaId                  number;
  vEProdutoKit                char(1);
  vTemItensExigirSeparacao    PRODUTOS.EXIGIR_SEPARACAO%type default 'N';
  vTrabalharControleSeparacao PARAMETROS_EMPRESA.TRABALHAR_CONTROLE_SEPARACAO%type;
  vUtilizaControleManifesto   PARAMETROS.UTILIZA_CONTROLE_MANIFESTO%type;
  vUsuarioSessaoId            number;
  vUsuarioSessaoNome          FUNCIONARIOS.NOME%type;

  cursor cItensKit(pPRODUTO_KIT_ID in number, pITEM_KIT_ID in number, pQUANTIDADE_KITS in number) is
  select
    KIT.PRODUTO_ID,
    ITE.ITEM_ID,
    KIT.QUANTIDADE * pQUANTIDADE_KITS as QUANTIDADE
  from
    ORCAMENTOS_ITENS ITE

  inner join PRODUTOS_KIT KIT
  on ITE.PRODUTO_ID = KIT.PRODUTO_ID
  and KIT.PRODUTO_KIT_ID = pPRODUTO_KIT_ID

  where ITE.ITEM_KIT_ID = pITEM_KIT_ID
  and ORCAMENTO_ID = iORCAMENTO_ID;
  
  cursor cItensPendKit( pITEM_ID in number, pLOCAL_ID in number ) is
  select
    PRODUTO_ID,
    LOTE,
    SALDO
  from
    ENTREGAS_ITENS_PENDENTES
  where ORCAMENTO_ID = iORCAMENTO_ID
  and EMPRESA_ID = iEMPRESA_PEND_ID
  and LOCAL_ID = pLOCAL_ID
  and ITEM_ID = pITEM_ID
  and PREVISAO_ENTREGA = iPREVISAO_ENTREGA
  order by
    SALDO desc;
  
begin

  select
    UTILIZA_CONTROLE_MANIFESTO
  into
    vUtilizaControleManifesto
  from
    PARAMETROS;

  select
    TRABALHAR_CONTROLE_SEPARACAO
  into
    vTrabalharControleSeparacao
  from
    PARAMETROS_EMPRESA
  where EMPRESA_ID = iEMPRESA_PEND_ID;

  select SEQ_ENTREGAS_ID.nextval
  into vEntregaId
  from dual;

  if vUtilizaControleManifesto = 'N' then
    vUsuarioSessaoId := SESSAO.USUARIO_SESSAO_ID;

    select NOME
    into vUsuarioSessaoNome
    from FUNCIONARIOS
    where FUNCIONARIO_ID = vUsuarioSessaoId;

    insert into ENTREGAS(
      ENTREGA_ID,
      EMPRESA_ENTREGA_ID,
      EMPRESA_GERACAO_ID,
      LOCAL_ID,
      ORCAMENTO_ID,
      PREVISAO_ENTREGA,
      AGRUPADOR_ID,
      STATUS,
      TIPO_NOTA_GERAR,
      USUARIO_CONFIRMACAO_ID,
      NOME_PESSOA_RECEBEU_ENTREGA
    )values(
      vEntregaId,
      iEMPRESA_PEND_ID,
      iEMPRESA_PEND_ID,
      iLOCAL_ID,
      iORCAMENTO_ID,
      iPREVISAO_ENTREGA,
      iAGRUPADOR_ID,
      'ENT',
      iTIPO_NOTA_GERAR,
      vUsuarioSessaoId,
      vUsuarioSessaoNome
    );
  else
    insert into ENTREGAS(
      ENTREGA_ID,
      EMPRESA_ENTREGA_ID,
      EMPRESA_GERACAO_ID,
      LOCAL_ID,
      ORCAMENTO_ID,
      PREVISAO_ENTREGA,
      AGRUPADOR_ID,
      STATUS,
      TIPO_NOTA_GERAR
    )values(
      vEntregaId,
      iEMPRESA_PEND_ID,
      iEMPRESA_PEND_ID,
      iLOCAL_ID,
      iORCAMENTO_ID,
      iPREVISAO_ENTREGA,
      iAGRUPADOR_ID,
      'AGC',
      iTIPO_NOTA_GERAR
    );
  end if;

  for i in iPRODUTOS_IDS.first..iPRODUTOS_IDS.last loop

    /* Baixando a pendência */
    update ENTREGAS_ITENS_PENDENTES set
      ENTREGUES = ENTREGUES + iQUANTIDADES(i)
    where ORCAMENTO_ID = iORCAMENTO_ID
    and EMPRESA_ID = iEMPRESA_PEND_ID
    and PREVISAO_ENTREGA = iPREVISAO_ENTREGA
    and LOCAL_ID = iLOCAIS_IDS(i)
    and ITEM_ID = iITENS_IDS(i)
    and LOTE = iLOTES(i);

    /* Inserindo o produto na entrega */
    insert into ENTREGAS_ITENS(
      ENTREGA_ID,
      PRODUTO_ID,
      ITEM_ID,
      LOTE,
      QUANTIDADE
    )values(
      vEntregaId,
      iPRODUTOS_IDS(i),
      iITENS_IDS(i),
      iLOTES(i),
      iQUANTIDADES(i)
    );

    select
      case when TIPO_CONTROLE_ESTOQUE in('K', 'A') then 'S' else 'N' end
    into
      vEProdutoKit
    from
      ORCAMENTOS_ITENS
    where ORCAMENTO_ID = iORCAMENTO_ID
    and ITEM_ID = iITENS_IDS(i);

    /* Se for o kit, baixando os componentes do kit */
    if vEProdutoKit = 'S' then
      for xItensKit in cItensKit(iPRODUTOS_IDS(i), iITENS_IDS(i), iQUANTIDADES(i)) loop

        for xItensPendKit in cItensPendKit( xItensKit.ITEM_ID, iLOCAIS_IDS(i) ) loop
          update ENTREGAS_ITENS_PENDENTES set
            ENTREGUES = ENTREGUES + xItensKit.QUANTIDADE
          where ORCAMENTO_ID = iORCAMENTO_ID
          and EMPRESA_ID = iEMPRESA_PEND_ID
          and LOCAL_ID = iLOCAIS_IDS(i)
          and PREVISAO_ENTREGA = iPREVISAO_ENTREGA
          and ITEM_ID = xItensKit.ITEM_ID
          and LOTE = xItensPendKit.LOTE;

          /* Inserindo de fato a retirada pelo cliente */
          insert into ENTREGAS_ITENS(
            ENTREGA_ID,
            PRODUTO_ID,
            ITEM_ID,
            LOTE,
            QUANTIDADE
          )values(
            vEntregaId,
            xItensPendKit.PRODUTO_ID,
            xItensKit.ITEM_ID,
            xItensPendKit.LOTE,
            xItensKit.QUANTIDADE
          );
        end loop;
      end loop;

    end if;

    if vTrabalharControleSeparacao = 'S' and vTemItensExigirSeparacao = 'N' then
      select
        EXIGIR_SEPARACAO
      into
        vTemItensExigirSeparacao
      from
        PRODUTOS
      where PRODUTO_ID = iPRODUTOS_IDS(i);
    end if;

  end loop;

  if vTemItensExigirSeparacao = 'S' then
    update ENTREGAS set
      STATUS = 'AGS'
    where ENTREGA_ID = vEntregaId;
  end if;

  oENTREGA_ID := vEntregaId;

end GERAR_ENTREGA_DE_PENDENCIA;
/