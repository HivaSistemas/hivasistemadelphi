﻿create or replace procedure CONFIRMAR_ENTREGA(iENTREGA_ID in number) is

  vEmpresaId     ORCAMENTOS.EMPRESA_ID%type;    
  vLocalId       ENTREGAS.LOCAL_ID%type;
  vStatus        ORCAMENTOS.STATUS%type;

  cursor cItens is
  select
    PRODUTO_ID,
    QUANTIDADE,
    ITEM_ID,
    LOTE
  from
    ENTREGAS_ITENS
  where ENTREGA_ID = iENTREGA_ID;

begin

  select        
    ENT.EMPRESA_ENTREGA_ID,
    ORC.STATUS,
    ENT.LOCAL_ID
  into
    vEmpresaId,
    vStatus,
    vLocalId
  from
    ENTREGAS ENT
    
  inner join ORCAMENTOS ORC
  on ENT.ORCAMENTO_ID = ORC.ORCAMENTO_ID
  
  where ENT.ENTREGA_ID = iENTREGA_ID;

  if vStatus <> 'RE' then
    ERRO('O pedido que está sendo confirmado não está recebido, verifique!');
  end if;

  for vItens in cItens loop
    /* Movimentando o estoque físico */
    MOVIMENTAR_ESTOQUE(
      vEmpresaId,
      vItens.PRODUTO_ID,
      vLocalId,
      vItens.LOTE,
      vItens.QUANTIDADE,
      'ENT',
      null,
      null
    );
  end loop;

end CONFIRMAR_ENTREGA;
/