create or replace procedure SETAR_NFE_EMITIDA(
  iNOTA_FISCAL_ID          in positiven,
  iCHAVE_NFE               in string,
  iPROTOCOLO_NFE           in string,
  iDATA_HORA_EMISSAO       in date,
  iDATA_HORA_PROTOCOLO_NFE in date
)
is
  vEmpresaId            EMPRESAS.EMPRESA_ID%type;
  vTipoMovimento        NOTAS_FISCAIS.TIPO_MOVIMENTO%type;
  vMovimentarEstOutras  NOTAS_FISCAIS.MOVIMENTAR_EST_OUTRAS_NOTAS%type;
  vNaturezaEntrada      char(1);
  vEntradaId            ENTRADAS_NOTAS_FISCAIS.ENTRADA_ID%type;
  vMovEstFis            NOTAS_FISCAIS.MOV_EST_FIS%type;

  cursor cItens is
  select
    PRODUTO_ID,
    QUANTIDADE
  from
    NOTAS_FISCAIS_ITENS
  where NOTA_FISCAL_ID = iNOTA_FISCAL_ID;

begin

  update NOTAS_FISCAIS set
    DATA_HORA_EMISSAO = iDATA_HORA_EMISSAO,
    CHAVE_NFE =  iCHAVE_NFE,
    PROTOCOLO_NFE = iPROTOCOLO_NFE,
    DATA_HORA_PROTOCOLO_NFE = iDATA_HORA_PROTOCOLO_NFE,
    STATUS_NFE = '100',
    STATUS = 'E',
    MOTIVO_STATUS_NFE = 'AUTORIZADO O USO DA NF-E'
  where NOTA_FISCAL_ID = iNOTA_FISCAL_ID;
  
  select
    EMPRESA_ID,
    TIPO_MOVIMENTO,
    MOVIMENTAR_EST_OUTRAS_NOTAS,
    case when substr(CFOP_ID, 1, 1) in('1', '2', '3') then 'S' else 'N' end,
    MOV_EST_FIS
  into
    vEmpresaId,
    vTipoMovimento,
    vMovimentarEstOutras,
    vNaturezaEntrada,
    vMovEstFis
  from NOTAS_FISCAIS
  where NOTA_FISCAL_ID = iNOTA_FISCAL_ID;

  for vItens in cItens loop
    if ((vTipoMovimento <> 'OUT') or (vMovimentarEstOutras = 'S')) and (vMovEstFis = 'S')  then
      MOVIMENTAR_ESTOQUE_FISCAL(vEmpresaId, vItens.PRODUTO_ID, vItens.QUANTIDADE * case when vNaturezaEntrada = 'S' then 1 else -1 end);
    end if;
  end loop;

  if vTipoMovimento = 'TPE' then
    GERAR_ENTR_TRANS_PRODUTOS_EMP(iNOTA_FISCAL_ID, 'TPE');
  elsif vTipoMovimento in('TFR', 'TFE') then
    GERAR_ENTR_TRANS_PRODUTOS_EMP(iNOTA_FISCAL_ID, 'TFP');

    update ENTRADAS_NOTAS_FISCAIS set
      STATUS = 'ECO'
    where NOTA_TRANSF_PROD_ORIGEM_ID = iNOTA_FISCAL_ID;

    select
       ENTRADA_ID
    into
      vEntradaId
    from
      ENTRADAS_NOTAS_FISCAIS
    where NOTA_TRANSF_PROD_ORIGEM_ID = iNOTA_FISCAL_ID;

    CONS_ESTOQUE_ENTR_NOTA_FISCAL(vEntradaId);
  end if;

end SETAR_NFE_EMITIDA;
/