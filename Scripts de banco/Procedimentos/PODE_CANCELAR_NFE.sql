create or replace procedure PODE_CANCELAR_NFE(pNOTA_FISCAL_ID in number) is
  vDataHoraEmissao date;
  vStatus          NOTAS_FISCAIS.STATUS%type;
begin

  select
    STATUS,
    DATA_HORA_EMISSAO
  into
    vStatus,
    vDataHoraEmissao
  from
    NOTAS_FISCAIS
  where NOTA_FISCAL_ID = pNOTA_FISCAL_ID;
  
  if vStatus <> 'E' then
    ERRO('Somente NFe emitida pode ser cancelada!');
  end if;
  
  if sysdate - 0.99 > vDataHoraEmissao then
    ERRO('NFe fora do prazo de 24hrs para cancelamento!');
  end if;

end PODE_CANCELAR_NFE;