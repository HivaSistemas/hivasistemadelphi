create or replace procedure CANCELAR_FECHAMENTO_PEDIDO(iORCAMENTO_ID in number)
is
  vQtde         number;
  vValorCredito ORCAMENTOS.VALOR_CREDITO%type;
  vClienteId    ORCAMENTOS.CLIENTE_ID%type;
  vStatus       ORCAMENTOS.STATUS%type;

  cursor cProdutos is
  select
    PRODUTO_ID,
    QUANTIDADE
  from
    ORCAMENTOS_ITENS
  where ORCAMENTO_ID = iORCAMENTO_ID
  order by PRODUTO_ID, ITEM_ID;

  cursor cAdiantamentoAcumulado is
  SELECT
    PAGAR_ID
  FROM CONTAS_PAGAR
  WHERE ORCAMENTO_ID = iORCAMENTO_ID
  AND ORIGEM = 'ADA';

  cursor cCreditos is
  select distinct
    PAG.BAIXA_ID
  from
    ORCAMENTOS_CREDITOS ORC

  inner join CONTAS_PAGAR PAG
  on ORC.PAGAR_ID = PAG.PAGAR_ID

  where ORC.ORCAMENTO_ID = iORCAMENTO_ID
  and PAG.BAIXA_ID is not null;

begin

  select
    CLIENTE_ID,
    VALOR_CREDITO,
    STATUS
  into
    vClienteId,
    vValorCredito,
    vStatus
  from ORCAMENTOS
  where ORCAMENTO_ID = iORCAMENTO_ID;

  if vStatus not in('VR', 'VB', 'VE', 'OB') then
    ERRO('A venda n�o est� no status de "Ag. recebimento", "venda bloqueada", "Ag.recebimento entrega" ou "Or�amento bloqueado", por favor verifique!');
  end if;

  if vStatus = 'VE' then
    select
      count(*)
    into
      vQtde
    from
      ENTREGAS
    where ORCAMENTO_ID = iORCAMENTO_ID;

    if vQtde > 0 then
      ERRO('N�o � permitido cancelar o fechamento de um pedido para recebimento na entrega que j� possui entregas geradas!');
    end if;

    select
      count(*)
    into
      vQtde
    from
      DEVOLUCOES
    where ORCAMENTO_ID = iORCAMENTO_ID;

    if vQtde > 0 then
      ERRO('N�o � permitido cancelar o fechamento de um pedido para recebimento na entrega que j� possui devolu��es!');
    end if;

    /* Apagando as previs�es dos recebimentos na entrega */
    delete from CONTAS_RECEBER
    where ORCAMENTO_ID = iORCAMENTO_ID
    and COBRANCA_ID = SESSAO.PARAMETROS_GERAIS.TIPO_COB_RECEB_ENTREGA_ID;
  end if;

  /* ----- Deletando as pend�ncias de retiradas e entregas ------ */
  delete from RETIRADAS_ITENS
  where RETIRADA_ID in(
    select
      RETIRADA_ID
    from
      RETIRADAS
    where ORCAMENTO_ID = iORCAMENTO_ID
  );

  delete from RETIRADAS
  where ORCAMENTO_ID = iORCAMENTO_ID;

  delete from RETIRADAS_ITENS_PENDENTES
  where ORCAMENTO_ID = iORCAMENTO_ID;

  delete from RETIRADAS_PENDENTES
  where ORCAMENTO_ID = iORCAMENTO_ID;

  delete from ENTREGAS_ITENS_PENDENTES
  where ORCAMENTO_ID = iORCAMENTO_ID;

  delete from ENTREGAS_PENDENTES
  where ORCAMENTO_ID = iORCAMENTO_ID;

  delete from ENTREGAS_ITENS_SEM_PREVISAO
  where ORCAMENTO_ID = iORCAMENTO_ID;
  
  delete from ORCAMENTOS_ITENS_DEF_LOCAIS
  where ORCAMENTO_ID = iORCAMENTO_ID;
  /* ----------------------------------------------------------- */

  /* Deletando os produtos que comp�e os kits */
  delete from ORCAMENTOS_ITENS
  where ORCAMENTO_ID = iORCAMENTO_ID
  and ITEM_KIT_ID is not null;

  if vValorCredito > 0 then
    for xCreditos in cCreditos loop
      CANCELAR_BAIXA_CONTAS_PAGAR(xCreditos.BAIXA_ID);
    end loop;

    delete from ORCAMENTOS_CREDITOS
    where ORCAMENTO_ID = iORCAMENTO_ID;
  end if;

  for xAdiantamentoAcumulado in cAdiantamentoAcumulado loop
    delete from CONTAS_PAGAR
    where PAGAR_ID = xAdiantamentoAcumulado.PAGAR_ID;
  end loop;

  delete from ORCAMENTOS_BLOQUEIOS
  where ORCAMENTO_ID = iORCAMENTO_ID;

  update ORCAMENTOS set
    STATUS = 'OE',
    VALOR_DINHEIRO = 0,
    VALOR_CHEQUE = 0,
    VALOR_CARTAO_DEBITO = 0,
    VALOR_CARTAO_CREDITO = 0,
    VALOR_COBRANCA = 0,
    VALOR_ACUMULATIVO = 0,
    VALOR_CREDITO = 0
  where ORCAMENTO_ID = iORCAMENTO_ID;

end CANCELAR_FECHAMENTO_PEDIDO;
/