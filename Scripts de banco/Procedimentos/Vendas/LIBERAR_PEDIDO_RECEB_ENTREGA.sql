﻿create or replace procedure LIBERAR_PEDIDO_RECEB_ENTREGA(iORCAMENTO_ID in number)
is
  vTipoCobRecebEntregaId PARAMETROS.TIPO_COB_RECEB_ENTREGA_ID%type;
  vValorCobranca         ORCAMENTOS.VALOR_COBRANCA%type;
  vStatus                ORCAMENTOS.STATUS%type;
  vClienteId             CLIENTES.CADASTRO_ID%type;
  vEmpresaId             ORCAMENTOS.EMPRESA_ID%type;
  vVendedorId            ORCAMENTOS.VENDEDOR_ID%type;
  vDiasPrazo             number;
begin

  begin
    select
      VALOR_DINHEIRO + VALOR_CARTAO_CREDITO + VALOR_CARTAO_DEBITO + VALOR_CHEQUE,
      STATUS,
      CLIENTE_ID,
      EMPRESA_ID,
      VENDEDOR_ID
    into
      vValorCobranca,
      vStatus,
      vClienteId,
      vEmpresaId,
      vVendedorId
    from
      ORCAMENTOS
    where ORCAMENTO_ID = iORCAMENTO_ID;
  exception
    when others then
      ERRO('Não foi encontrado os valores de pagamento para o orçamento ' || iORCAMENTO_ID || '!' || chr(13) || chr(10) || sqlerrm);
  end;

  if vStatus in('VE', 'RE') then
    ERRO('Este pedido teve os documentos liberados ou já foi recebido!');
  end if;

  if vStatus not in('VR') then
    ERRO('Este pedido não está aguardando recebimento, verifique!');
  end if;

  select
    TIPO_COB_RECEB_ENTREGA_ID
  into
    vTipoCobRecebEntregaId
  from
    PARAMETROS;

  if vTipoCobRecebEntregaId is null then
    ERRO('O tipo de cobrança para recebimento na entrega não foi definido nos parametros gerais, entre em contato com a Hiva!');
  end if;

  select
    DIAS
  into
    vDiasPrazo
  from
    TIPO_COBRANCA_DIAS_PRAZO
  where COBRANCA_ID = vTipoCobRecebEntregaId
  and rownum = 1;

  if vValorCobranca > 0 then
    insert into CONTAS_RECEBER(
      RECEBER_ID,
      CADASTRO_ID,
      EMPRESA_ID,
      ORCAMENTO_ID,
      COBRANCA_ID,
      PORTADOR_ID,
      PLANO_FINANCEIRO_ID,
      TURNO_ID,
      VENDEDOR_ID,
      DOCUMENTO,
      DATA_EMISSAO,
      DATA_VENCIMENTO,
      DATA_VENCIMENTO_ORIGINAL,
      VALOR_DOCUMENTO,
      STATUS,
      PARCELA,
      NUMERO_PARCELAS,
      ORIGEM
    )values(
      SEQ_RECEBER_ID.nextval,
      vClienteId,
      vEmpresaId,
      iORCAMENTO_ID,
      vTipoCobRecebEntregaId,
      '9999',
      '1.001.006',
      null,
      vVendedorId,
      'REC.ENT-' || iORCAMENTO_ID || '1/COB',
      trunc(sysdate),
      trunc(sysdate) + vDiasPrazo,
      trunc(sysdate) + vDiasPrazo,
      vValorCobranca,
      'A',
      1,
      1,
      'ORC'
    );
  end if;    
  
  update ORCAMENTOS set
    STATUS = 'VE'
  where ORCAMENTO_ID = iORCAMENTO_ID;

end LIBERAR_PEDIDO_RECEB_ENTREGA;
/