﻿create or replace procedure CANCELAR_RECEBIMENTO_PEDIDO(iORCAMENTO_ID in number)
is
  vBAIXA_PIX_ID number;
  vNotaFiscalId   NOTAS_FISCAIS.NOTA_FISCAL_ID%type;
  vStatus         NOTAS_FISCAIS.STATUS%type;
  vNumeroNota     NOTAS_FISCAIS.NUMERO_NOTA%type;
begin

  delete from NOTAS_FISCAIS_ITENS
  where NOTA_FISCAL_ID in (
    select NOTA_FISCAL_ID
    from NOTAS_FISCAIS
    where nvl(NUMERO_NOTA, 0) > 0
    and ORCAMENTO_ID = iORCAMENTO_ID
  );

  delete from NOTAS_FISCAIS
  where NOTA_FISCAL_ID in (
    select NOTA_FISCAL_ID
    from NOTAS_FISCAIS
    where nvl(NUMERO_NOTA, 0) > 0
    and ORCAMENTO_ID = iORCAMENTO_ID
  );

  delete from CONTAS_RECEBER
  where ORCAMENTO_ID = iORCAMENTO_ID;

  select
    BAIXA_RECEBER_PIX_ID
  into
    vBAIXA_PIX_ID
  from
    ORCAMENTOS
  where ORCAMENTO_ID = iORCAMENTO_ID;

  if vBAIXA_PIX_ID <> 0 then
    update ORCAMENTOS SET
      BAIXA_RECEBER_PIX_ID = null
    where ORCAMENTO_ID = iORCAMENTO_ID;

    delete from CONTAS_REC_BAIXAS_PAGTOS_PIX
    where BAIXA_ID = vBAIXA_PIX_ID;
  end if;

  update ORCAMENTOS set
    STATUS = 'VR',
    TURNO_ID = null,
    DATA_HORA_RECEBIMENTO = null
  where ORCAMENTO_ID = iORCAMENTO_ID;

  CANCELAR_FECHAMENTO_PEDIDO(iORCAMENTO_ID);

end CANCELAR_RECEBIMENTO_PEDIDO;
/