﻿create or replace procedure CANCELAR_FECHAMENTO_ACUMULADO(iACUMULADO_ID in number)
is

  vQtde          number;
  vStatus        ACUMULADOS.STATUS%type;
  vValorCredito  ACUMULADOS.VALOR_CREDITO%type;

  cursor cCreditos is
  select distinct
    PAG.BAIXA_ID
  from
    ACUMULADOS_CREDITOS ACU

  inner join CONTAS_PAGAR PAG
  on ACU.PAGAR_ID = PAG.PAGAR_ID

  where ACU.ACUMULADO_ID = iACUMULADO_ID
  and PAG.BAIXA_ID is not null;

  cursor cOrcs is
  select
    ORCAMENTO_ID
  from
    ACUMULADOS_ORCAMENTOS
  where ACUMULADO_ID = iACUMULADO_ID;

begin

  select
    STATUS,
    VALOR_CREDITO
  into
    vStatus,
    vValorCredito
  from
    ACUMULADOS
  where ACUMULADO_ID = iACUMULADO_ID;

  if vStatus not in('AR') then
    ERRO('O acumulado não está aguardando recebimento, por favor verifique!');
  end if;
  
  if vValorCredito > 0 then
    for xCreditos in cCreditos loop
      CANCELAR_BAIXA_CONTAS_PAGAR(xCreditos.BAIXA_ID);
    end loop;

    delete from ACUMULADOS_CREDITOS
    where ACUMULADO_ID = iACUMULADO_ID;
  end if;

  delete from ACUMULADOS_PAGAMENTOS
  where ACUMULADO_ID = iACUMULADO_ID;

  delete from ACUMULADOS_PAGAMENTOS_CHEQUES
  where ACUMULADO_ID = iACUMULADO_ID;

  for xOrcs in cOrcs loop
    AJUSTAR_CONTAS_REC_ACUMULATIVO(xOrcs.ORCAMENTO_ID);
  end loop;

  update ORCAMENTOS set
    ACUMULADO_ID = null
  where ACUMULADO_ID = iACUMULADO_ID;

  select
    count(*)
  into
    vQtde
  from
    NOTAS_FISCAIS
  where ACUMULADO_ID = iACUMULADO_ID
  and STATUS in('E', 'C');

  if vQtde > 0 then
    update ACUMULADOS set
      STATUS = 'CA'
    where ACUMULADO_ID = iACUMULADO_ID;
  else
    delete from ACUMULADOS_ORCAMENTOS
    where ACUMULADO_ID = iACUMULADO_ID;

    delete from ACUMULADOS_ITENS
    where ACUMULADO_ID = iACUMULADO_ID;

    delete from ACUMULADOS
    where ACUMULADO_ID = iACUMULADO_ID;
  end if;

end CANCELAR_FECHAMENTO_ACUMULADO;
/