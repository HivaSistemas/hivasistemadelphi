  create or replace procedure GERAR_NOTA_ACUMULADO(iACUMULADO_ID in number)
  is
    vIndice                 number default 1;
    vIndiceDescVenda        number default 1;
  
    i                       number default 0;
    vQtde                   number default 0;
    vValorMaisSignificativo number default 0;
    vValorTotalAcumulado    ACUMULADOS.VALOR_TOTAL%type;
    vNotaFiscalId           NOTAS_FISCAIS.NOTA_FISCAL_ID%type;
    vTipoNotaGerar          varchar2(2) default 'NE';

    vModeloNota             NOTAS_FISCAIS.MODELO_NOTA%type;
    vSerieNota              NOTAS_FISCAIS.SERIE_NOTA%type;

    vCfopIdCapa             NOTAS_FISCAIS.CFOP_ID%type;
    vNaturezaOperacao       NOTAS_FISCAIS.NATUREZA_OPERACAO%type;
    vDadosNota              RecordsNotasFiscais.RecNotaFiscal;
    vDadosImpItens          RecordsNotasFiscais.RecImpostosCalculados;
    vDadosItens             RecordsNotasFiscais.ArrayOfRecNotasFiscaisItens;
    vDadosDescontoVenda     RecordsNotasFiscais.RecDescontoConcedido;

    vRegimeTributario       PARAMETROS_EMPRESA.REGIME_TRIBUTARIO%type;
    vSerieNFCe              PARAMETROS_EMPRESA.SERIE_NFCE%type;
    vSerieNFe               PARAMETROS_EMPRESA.SERIE_NFE%type;

    vTipoOperacao           char(1);

    cursor cItens(pEstadoId in string, pTipoCliente in string, pEmpresaNotaId in number) is
    select
      ITE.PRODUTO_ID,
      ITE.PRECO_UNITARIO,
      ITE.QUANTIDADE,
      ITE.VALOR_TOTAL,
      ITE.VALOR_TOTAL_DESCONTO,
      ITE.VALOR_TOTAL_OUTRAS_DESPESAS,
      ITE.VALOR_TOTAL_FRETE,
      CUS.CUSTO_ULTIMO_PEDIDO,
      CUS.PRECO_LIQUIDO,
      CUS.CMV,
      PRO.NOME as NOME_PRODUTO,
      PRO.CODIGO_NCM,
      PRO.UNIDADE_VENDA,
      PRO.CODIGO_BARRAS,
      PRO.CEST,
      nvl(IMP.PERC_ESTADUAL, 0) as PERC_ESTADUAL,
      nvl(IMP.PERC_FEDERAL_NACIONAL, 0) as PERC_FEDERAL,
      nvl(IMP.PERC_MUNICIPAL, 0) as PERC_MUNICIPAL
    from
      ACUMULADOS_ITENS ITE

    inner join ACUMULADOS ACU
    on ITE.ACUMULADO_ID = ACU.ACUMULADO_ID

    inner join PRODUTOS PRO
    on ITE.PRODUTO_ID = PRO.PRODUTO_ID
    and PRO.TIPO_CONTROLE_ESTOQUE not in('K', 'A')

    inner join VW_CUSTOS_PRODUTOS CUS
    on ITE.PRODUTO_ID = CUS.PRODUTO_ID
    and CUS.EMPRESA_ID = pEmpresaNotaId

    left join IMPOSTOS_IBPT IMP
    on PRO.CODIGO_NCM = IMP.CODIGO_NCM
    and IMP.ESTADO_ID = pEstadoId

    where ITE.ACUMULADO_ID = iACUMULADO_ID
    order by
      ITE.PRODUTO_ID;

  begin
    select
      count(*)
    into
      vQtde
    from
      NOTAS_FISCAIS
    where ACUMULADO_ID = iACUMULADO_ID
    and STATUS = 'E';

    if vQtde > 0 then
      ERRO('Este acumulado ja gerou nota!');
    end if;

    select
      ACU.TIPO_NOTA_GERAR,
      ACU.VALOR_TOTAL,
      IDV.INDICE_ID,
      IDV.PERCENTUAL_DESCONTO,
      IDV.PRECO_CUSTO,
      IDV.TIPO_CUSTO,
      IDV.TIPO_DESCONTO_PRECO_CUSTO
    into
      vTipoNotaGerar,
      vValorTotalAcumulado,
      vDadosDescontoVenda.IndiceDescontoVendaId,
      vDadosDescontoVenda.PercentualDesconto,
      vDadosDescontoVenda.PrecoCusto,
      vDadosDescontoVenda.TipoCusto,
      vDadosDescontoVenda.TipoDescontoPrecoCusto
    from
      ACUMULADOS ACU
    
    left join INDICES_DESCONTOS_VENDA IDV
    on ACU.INDICE_DESCONTO_VENDA_ID = IDV.INDICE_ID
  
    where ACU.ACUMULADO_ID = iACUMULADO_ID;
  
    if vDadosDescontoVenda.IndiceDescontoVendaId is not null then
      if vDadosDescontoVenda.PrecoCusto = 'N' then
        vIndiceDescVenda := 1 - vDadosDescontoVenda.PercentualDesconto * 0.01;
      else
        if vDadosDescontoVenda.TipoDescontoPrecoCusto = 'A' then
          vIndiceDescVenda := 1 + vDadosDescontoVenda.PercentualDesconto * 0.01;
        elsif vDadosDescontoVenda.TipoDescontoPrecoCusto = 'D' then
          vIndiceDescVenda := 1 - vDadosDescontoVenda.PercentualDesconto * 0.01;
        else
          vIndiceDescVenda := 1;
        end if;
      end if;

      vIndice := vIndiceDescVenda;
    end if;

    begin
      select
        ACU.ACUMULADO_ID,
        ACU.EMPRESA_ID,
        ACU.CLIENTE_ID,
        ACU.VALOR_DINHEIRO,
        ACU.VALOR_PIX,
        ACU.VALOR_CARTAO_CREDITO,
        ACU.VALOR_CARTAO_DEBITO,
        ACU.VALOR_CREDITO,
        ACU.VALOR_COBRANCA,
        ACU.VALOR_CHEQUE,
        ACU.VALOR_FINANCEIRA,
      
        /* Emitente */
        EMP.RAZAO_SOCIAL,
        EMP.NOME_FANTASIA,
        EMP.CNPJ,
        EMP.INSCRICAO_ESTADUAL,
        EMP.LOGRADOURO,
        EMP.COMPLEMENTO,
        BAE.NOME as NOME_BAIRRO_EMITENTE,
        CIE.NOME as NOME_CIDADE_EMITENTE,
        EMP.NUMERO,
        ESE.ESTADO_ID,
        EMP.CEP,
        EMP.TELEFONE_PRINCIPAL,
        CIE.CODIGO_IBGE as CODIGO_IBGE_MUNICIPIO_EMIT,
        ESE.CODIGO_IBGE as CODIGO_IBGE_ESTADO_EMITENT,
      
        /* Destinatario */
        CAD.NOME_FANTASIA,
        CAD.RAZAO_SOCIAL,
        CAD.TIPO_PESSOA,
        CAD.CPF_CNPJ,

        ACU.INSCRICAO_ESTADUAL,
        ACU.LOGRADOURO,
        ACU.COMPLEMENTO,
        ACU.NUMERO,
        ACU.CEP,
        BAI.NOME_BAIRRO,
        BAI.NOME_CIDADE,
        BAI.CODIGO_IBGE_CIDADE as CODIGO_IBGE_MUNICIPIO_DEST,
        BAI.ESTADO_ID as ESTADO_ID_DESTINATARIO,
      
        CLI.TIPO_CLIENTE,
        PAE.INF_COMPL_DOCS_ELETRONICOS || ' ' || ACU.OBSERVACOES_NFE
      into
        vDadosNota.ACUMULADO_ID,
        vDadosNota.EMPRESA_ID,
        vDadosNota.CADASTRO_ID,
        vDadosNota.VALOR_RECEBIDO_DINHEIRO,
        vDadosNota.VALOR_RECEBIDO_PIX,
        vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,
        vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,
        vDadosNota.VALOR_RECEBIDO_CREDITO,
        vDadosNota.VALOR_RECEBIDO_COBRANCA,
        vDadosNota.VALOR_RECEBIDO_CHEQUE,
        vDadosNota.VALOR_RECEBIDO_FINANCEIRA,
      
        /* Emitente */
        vDadosNota.RAZAO_SOCIAL_EMITENTE,
        vDadosNota.NOME_FANTASIA_EMITENTE,
        vDadosNota.CNPJ_EMITENTE,
        vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,
        vDadosNota.LOGRADOURO_EMITENTE,
        vDadosNota.COMPLEMENTO_EMITENTE,
        vDadosNota.NOME_BAIRRO_EMITENTE,
        vDadosNota.NOME_CIDADE_EMITENTE,
        vDadosNota.NUMERO_EMITENTE,
        vDadosNota.ESTADO_ID_EMITENTE,
        vDadosNota.CEP_EMITENTE,
        vDadosNota.TELEFONE_EMITENTE,
        vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,
        vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,
      
        /* Destinatario */
        vDadosNota.NOME_FANTASIA_DESTINATARIO,
        vDadosNota.RAZAO_SOCIAL_DESTINATARIO,
        vDadosNota.TIPO_PESSOA_DESTINATARIO,
        vDadosNota.CPF_CNPJ_DESTINATARIO,
        vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,
        vDadosNota.LOGRADOURO_DESTINATARIO,
        vDadosNota.COMPLEMENTO_DESTINATARIO,
        vDadosNota.NUMERO_DESTINATARIO,
        vDadosNota.CEP_DESTINATARIO,
        vDadosNota.NOME_BAIRRO_DESTINATARIO,
        vDadosNota.NOME_CIDADE_DESTINATARIO,
        vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,
        vDadosNota.ESTADO_ID_DESTINATARIO,

        vDadosNota.TIPO_CLIENTE,
        vDadosNota.INFORMACOES_COMPLEMENTARES
      from
        ACUMULADOS ACU

      inner join CADASTROS CAD
      on ACU.CLIENTE_ID = CAD.CADASTRO_ID

      inner join CLIENTES CLI
      on ACU.CLIENTE_ID = CLI.CADASTRO_ID

      inner join VW_BAIRROS BAI
      on ACU.BAIRRO_ID = BAI.BAIRRO_ID

      inner join EMPRESAS EMP
      on ACU.EMPRESA_ID = EMP.EMPRESA_ID

      inner join BAIRROS BAE
      on EMP.BAIRRO_ID = BAE.BAIRRO_ID

      inner join CIDADES CIE
      on BAE.CIDADE_ID = CIE.CIDADE_ID

      inner join ESTADOS ESE
      on CIE.ESTADO_ID = ESE.ESTADO_ID

      inner join PARAMETROS_EMPRESA PAE
      on ACU.EMPRESA_ID = PAE.EMPRESA_ID

      cross join PARAMETROS PAR

      where ACU.ACUMULADO_ID = iACUMULADO_ID;
    exception
      when others then
        ERRO('Houve um problema ao buscar os dados do acumulado para gerac?o dos dados da NF! ' || sqlerrm);
    end;

    vDadosNota.BASE_CALCULO_ICMS    := 0;
    vDadosNota.VALOR_ICMS           := 0;
    vDadosNota.VALOR_ICMS_INTER     := 0;
    vDadosNota.BASE_CALCULO_ICMS_ST := 0;
    vDadosNota.VALOR_ICMS_ST        := 0;
    vDadosNota.BASE_CALCULO_PIS     := 0;
    vDadosNota.VALOR_PIS            := 0;
    vDadosNota.BASE_CALCULO_COFINS  := 0;
    vDadosNota.VALOR_COFINS         := 0;
    vDadosNota.VALOR_IPI            := 0;
    vDadosNota.VALOR_FRETE          := 0;
    vDadosNota.VALOR_SEGURO         := 0;
    vDadosNota.PESO_LIQUIDO         := 0;
    vDadosNota.PESO_BRUTO           := 0;

    for vItens in cItens(vDadosNota.ESTADO_ID_DESTINATARIO, vDadosNota.TIPO_CLIENTE, vDadosNota.EMPRESA_ID) loop

      if length(vItens.CODIGO_NCM) < 8 then
        Erro('O codigo NCM do produto ' || vItens.PRODUTO_ID || ' - ' || vItens.NOME_PRODUTO || ' esta incorreto, o NCM e composto de 8 digitos, verifique no cadastro de produtos!');
      end if;

      vDadosItens(i).PRODUTO_ID    := vItens.PRODUTO_ID;
      vDadosItens(i).ITEM_ID       := i + 1;
      vDadosItens(i).NOME_PRODUTO  := vItens.NOME_PRODUTO;
      vDadosItens(i).UNIDADE       := vItens.UNIDADE_VENDA;
      vDadosItens(i).CODIGO_NCM    := vItens.CODIGO_NCM;
      vDadosItens(i).QUANTIDADE    := vItens.QUANTIDADE;
      vDadosItens(i).CODIGO_BARRAS := vItens.CODIGO_BARRAS;
      vDadosItens(i).CEST          := vItens.CEST;
    
      if vDadosDescontoVenda.IndiceDescontoVendaId is not null and vDadosDescontoVenda.PrecoCusto = 'S' then
        if vDadosDescontoVenda.TipoCusto = 'C' and vItens.CUSTO_ULTIMO_PEDIDO > 0 then
          vDadosItens(i).PRECO_UNITARIO := vItens.CUSTO_ULTIMO_PEDIDO;
        elsif vDadosDescontoVenda.TipoCusto = 'F' and vItens.PRECO_LIQUIDO > 0 then
          vDadosItens(i).PRECO_UNITARIO := vItens.PRECO_LIQUIDO;
        elsif vDadosDescontoVenda.TipoCusto = 'M' and vItens.CMV > 0 then
          vDadosItens(i).PRECO_UNITARIO := vItens.CMV;
        else
          vDadosItens(i).PRECO_UNITARIO := vItens.PRECO_UNITARIO;
        end if;

        vDadosItens(i).PRECO_UNITARIO              := trunc(vDadosItens(i).PRECO_UNITARIO * vIndiceDescVenda, 2);
        vDadosItens(i).VALOR_TOTAL                 := round(vDadosItens(i).PRECO_UNITARIO * vItens.QUANTIDADE, 2);

        if vDadosItens(i).VALOR_TOTAL < 0.01 then
          vDadosItens(i).VALOR_TOTAL := 0.01;
        end if;

        vDadosItens(i).VALOR_TOTAL_DESCONTO        := 0;
        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := 0;

        vDadosDescontoVenda.ValorTotalCusto := vDadosDescontoVenda.ValorTotalCusto + vDadosItens(i).VALOR_TOTAL;
      else
        vDadosItens(i).PRECO_UNITARIO              := trunc(vItens.PRECO_UNITARIO * vIndice, 2);
        vDadosItens(i).VALOR_TOTAL                 := round(vDadosItens(i).PRECO_UNITARIO * vItens.QUANTIDADE, 2);

        if vDadosItens(i).VALOR_TOTAL < 0.01 then
          vDadosItens(i).VALOR_TOTAL := 0.01;
        end if;

        vDadosItens(i).VALOR_TOTAL_DESCONTO        := trunc(vItens.VALOR_TOTAL_DESCONTO * vIndice, 2);
        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS := trunc((vItens.VALOR_TOTAL_OUTRAS_DESPESAS + vItens.VALOR_TOTAL_FRETE) * vIndice, 2);
      end if;

      /* Buscando os dados de impostos item a item da venda */
      BUSCAR_CALC_IMPOSTOS_PRODUTO(
        vItens.PRODUTO_ID,
        vDadosNota.EMPRESA_ID,
        vDadosNota.CADASTRO_ID,

        vDadosItens(i).VALOR_TOTAL,
        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,
        vDadosItens(i).VALOR_TOTAL_DESCONTO,

        vDadosItens(i).CST,
        vDadosItens(i).BASE_CALCULO_ICMS,
        vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,
        vDadosItens(i).PERCENTUAL_ICMS,
        vDadosItens(i).VALOR_ICMS,

        vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,
        vDadosItens(i).BASE_CALCULO_ICMS_ST,
        vDadosItens(i).PERCENTUAL_ICMS_ST,
        vDadosItens(i).VALOR_ICMS_ST,
        vDadosItens(i).PRECO_PAUTA,
        vDadosItens(i).IVA,

        vDadosItens(i).VALOR_IPI,
        vDadosItens(i).PERCENTUAL_IPI,

        vDadosItens(i).CST_PIS,
        vDadosItens(i).BASE_CALCULO_PIS,
        vDadosItens(i).PERCENTUAL_PIS,
        vDadosItens(i).VALOR_PIS,

        vDadosItens(i).CST_COFINS,
        vDadosItens(i).BASE_CALCULO_COFINS,
        vDadosItens(i).PERCENTUAL_COFINS,
        vDadosItens(i).VALOR_COFINS,

        vDadosItens(i).BASE_CALCULO_ICMS_INTER,
        vDadosItens(i).PERCENTUAL_ICMS_INTER,
        vDadosItens(i).VALOR_ICMS_INTER,
        vDadosNota.ESTADO_ID_EMITENTE
      );

      if vDadosNota.ESTADO_ID_EMITENTE = vDadosNota.ESTADO_ID_DESTINATARIO or vTipoNotaGerar = 'NE' then
        vTipoOperacao := 'I';
      else
        vTipoOperacao := 'E';
      end if;

      vDadosItens(i).CFOP_ID           := BUSCAR_CFOP_ID_OPERACAO(vDadosNota.EMPRESA_ID, vDadosItens(i).CST, 'VIT', vTipoOperacao);
      vDadosItens(i).NATUREZA_OPERACAO := BUSCAR_CFOP_NATUREZA_OPERACAO(vDadosItens(i).CFOP_ID);
      /* --------------------------- Preenchendo os dados da capa da nota ------------------------------------- */
      vDadosNota.BASE_CALCULO_ICMS    := vDadosNota.BASE_CALCULO_ICMS + vDadosItens(i).BASE_CALCULO_ICMS;
      vDadosNota.VALOR_ICMS           := vDadosNota.VALOR_ICMS + vDadosItens(i).VALOR_ICMS;
      vDadosNota.BASE_CALCULO_ICMS_ST := vDadosNota.BASE_CALCULO_ICMS_ST + vDadosItens(i).BASE_CALCULO_ICMS_ST;
      vDadosNota.VALOR_ICMS_ST        := vDadosNota.VALOR_ICMS_ST + vDadosItens(i).VALOR_ICMS_ST;
      vDadosNota.BASE_CALCULO_PIS     := vDadosNota.BASE_CALCULO_PIS + vDadosItens(i).BASE_CALCULO_PIS;
      vDadosNota.VALOR_PIS            := vDadosNota.VALOR_PIS + vDadosItens(i).VALOR_PIS;
      vDadosNota.VALOR_COFINS         := vDadosNota.VALOR_COFINS + vDadosItens(i).VALOR_COFINS;
      vDadosNota.VALOR_IPI            := vDadosNota.VALOR_IPI + vDadosItens(i).VALOR_IPI;

      vDadosNota.VALOR_ICMS_INTER     := vDadosNota.VALOR_ICMS_INTER + vDadosItens(i).VALOR_ICMS_INTER;

      vDadosNota.VALOR_TOTAL :=
        vDadosNota.VALOR_TOTAL +
        vDadosItens(i).VALOR_TOTAL +
        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
        vDadosItens(i).VALOR_ICMS_ST +
        vDadosItens(i).VALOR_IPI -
        vDadosItens(i).VALOR_TOTAL_DESCONTO;

      vDadosNota.VALOR_TOTAL_PRODUTOS  := vDadosNota.VALOR_TOTAL_PRODUTOS + vDadosItens(i).VALOR_TOTAL;
      vDadosNota.VALOR_DESCONTO        := vDadosNota.VALOR_DESCONTO + vDadosItens(i).VALOR_TOTAL_DESCONTO;
      vDadosNota.VALOR_OUTRAS_DESPESAS := vDadosNota.VALOR_OUTRAS_DESPESAS + vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS;
      /* ------------------------------------------------------------------------------------------------------ */

      if
        vDadosItens(i).VALOR_TOTAL +
        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
        vDadosItens(i).VALOR_ICMS_ST +
        vDadosItens(i).VALOR_IPI -
        vDadosItens(i).VALOR_TOTAL_DESCONTO >
        vValorMaisSignificativo
      then
        vValorMaisSignificativo :=
          vDadosItens(i).VALOR_TOTAL +
          vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS +
          vDadosItens(i).VALOR_ICMS_ST +
          vDadosItens(i).VALOR_IPI -
          vDadosItens(i).VALOR_TOTAL_DESCONTO;

        vCfopIdCapa       := vDadosItens(i).CFOP_ID;
        vNaturezaOperacao := vDadosItens(i).NATUREZA_OPERACAO;
      end if;

      /* Calculando os tributos aproximados IBPT, Lei 12.741 de 2012 */
      vDadosNota.TOTAL_IMPOSTOS_ESTADUAL := vDadosNota.TOTAL_IMPOSTOS_ESTADUAL + round(vDadosItens(i).VALOR_TOTAL * vItens.PERC_ESTADUAL * 0.01, 2);
      vDadosNota.TOTAL_IMPOSTOS_FEDERAL  := vDadosNota.TOTAL_IMPOSTOS_FEDERAL + round(vDadosItens(i).VALOR_TOTAL * vItens.PERC_FEDERAL * 0.01, 2);

      i := i + 1;
    end loop;

    if vDadosItens.count < 1 then
      ERRO('Os produtos da nota fiscal n?o foram encontrados!');
    end if;

    if vCfopIdCapa is null then
      vCfopIdCapa       := vDadosItens(0).CFOP_ID;
      vNaturezaOperacao := vDadosItens(0).NATUREZA_OPERACAO;
    end if;
  
    /* Ajustando as formas de pagamento da nota */
    if vDadosDescontoVenda.IndiceDescontoVendaId is not null and vDadosDescontoVenda.PrecoCusto = 'S' then
      vIndice := vDadosDescontoVenda.ValorTotalCusto / vValorTotalAcumulado * vIndiceDescVenda;
    end if;

    vDadosNota.VALOR_RECEBIDO_DINHEIRO    := vDadosNota.VALOR_RECEBIDO_DINHEIRO * vIndice;
    vDadosNota.VALOR_RECEBIDO_PIX         := vDadosNota.VALOR_RECEBIDO_PIX * vIndice;
    vDadosNota.VALOR_RECEBIDO_CARTAO_CRED := vDadosNota.VALOR_RECEBIDO_CARTAO_CRED * vIndice;
    vDadosNota.VALOR_RECEBIDO_CARTAO_DEB  := vDadosNota.VALOR_RECEBIDO_CARTAO_DEB * vIndice;
    vDadosNota.VALOR_RECEBIDO_CREDITO     := vDadosNota.VALOR_RECEBIDO_CREDITO * vIndice;
    vDadosNota.VALOR_RECEBIDO_COBRANCA    := vDadosNota.VALOR_RECEBIDO_COBRANCA * vIndice;
    vDadosNota.VALOR_RECEBIDO_CHEQUE      := vDadosNota.VALOR_RECEBIDO_CHEQUE * vIndice;
    vDadosNota.VALOR_RECEBIDO_FINANCEIRA  := vDadosNota.VALOR_RECEBIDO_FINANCEIRA * vIndice;

    RATEAR_VALORES_FORMAS_PAGTO_NF(
      vDadosNota.VALOR_RECEBIDO_DINHEIRO,
      vDadosNota.VALOR_RECEBIDO_PIX,
      vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,
      vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,
      vDadosNota.VALOR_RECEBIDO_COBRANCA,
      vDadosNota.VALOR_RECEBIDO_CHEQUE,
      vDadosNota.VALOR_RECEBIDO_FINANCEIRA,
      vDadosNota.VALOR_RECEBIDO_CREDITO,
      vDadosNota.VALOR_RECEBIDO_ACUMULADO,
      vDadosNota.VALOR_TOTAL
    );

    -- vTipoNotaGerar := BUSCAR_TIPO_NOTA_GERAR(vOrcamentoId);

    select
      REGIME_TRIBUTARIO,
      SERIE_NFE,
      SERIE_NFCE
    into
      vRegimeTributario,
      vSerieNFCe,
      vSerieNFe
    from
      PARAMETROS_EMPRESA
    where EMPRESA_ID = vDadosNota.EMPRESA_ID;

    if vTipoNotaGerar = 'NI' then
      if vDadosNota.TIPO_PESSOA_DESTINATARIO = 'F' then
      vModeloNota := '55';
      vSerieNota  := to_char(nvl(vSerieNFe, 1));
      vTipoNotaGerar := 'N';
      else
        vModeloNota := '55';
        vSerieNota  := to_char(nvl(vSerieNFe, 1));
        vTipoNotaGerar := 'N';
      end if;
    elsif (vTipoNotaGerar = 'NF') or (vDadosNota.TIPO_PESSOA_DESTINATARIO = 'J') then
      vModeloNota := '55';
      vSerieNota  := to_char(nvl(vSerieNFe, 1));
      vTipoNotaGerar := 'N';
    else
     vModeloNota := '55';
      vSerieNota  := to_char(nvl(vSerieNFe, 1));
      vTipoNotaGerar := 'N';
    end if;

    vDadosNota.CFOP_ID := vCfopIdCapa;
    vDadosNota.NATUREZA_OPERACAO := vNaturezaOperacao;

    vDadosNota.INFORMACOES_COMPLEMENTARES :=
      vDadosNota.INFORMACOES_COMPLEMENTARES || '. Trib. aprox. R$ ' || NFORMAT(vDadosNota.TOTAL_IMPOSTOS_FEDERAL, 2) || ' federal, R$ ' || NFORMAT(vDadosNota.TOTAL_IMPOSTOS_ESTADUAL, 2) || ' estadual. ';

    vNotaFiscalId := SEQ_NOTA_FISCAL_ID.nextval;

    insert into NOTAS_FISCAIS(
      NOTA_FISCAL_ID,
      CADASTRO_ID,
      CFOP_ID,
      EMPRESA_ID,
      ACUMULADO_ID,
      MODELO_NOTA,
      SERIE_NOTA,
      NATUREZA_OPERACAO,
      STATUS,
      TIPO_MOVIMENTO,
      RAZAO_SOCIAL_EMITENTE,
      NOME_FANTASIA_EMITENTE,
      REGIME_TRIBUTARIO,
      CNPJ_EMITENTE,
      INSCRICAO_ESTADUAL_EMITENTE,
      LOGRADOURO_EMITENTE,
      COMPLEMENTO_EMITENTE,
      NOME_BAIRRO_EMITENTE,
      NOME_CIDADE_EMITENTE,
      NUMERO_EMITENTE,
      ESTADO_ID_EMITENTE,
      CEP_EMITENTE,
      TELEFONE_EMITENTE,
      CODIGO_IBGE_MUNICIPIO_EMIT,
      CODIGO_IBGE_ESTADO_EMITENT,
      NOME_FANTASIA_DESTINATARIO,
      RAZAO_SOCIAL_DESTINATARIO,
      TIPO_PESSOA_DESTINATARIO,
      CPF_CNPJ_DESTINATARIO,
      INSCRICAO_ESTADUAL_DESTINAT,
      NOME_CONSUMIDOR_FINAL,
      TELEFONE_CONSUMIDOR_FINAL,
      TIPO_NOTA,
      LOGRADOURO_DESTINATARIO,
      COMPLEMENTO_DESTINATARIO,
      NOME_BAIRRO_DESTINATARIO,
      NOME_CIDADE_DESTINATARIO,
      ESTADO_ID_DESTINATARIO,
      CEP_DESTINATARIO,
      NUMERO_DESTINATARIO,
      CODIGO_IBGE_MUNICIPIO_DEST,
      VALOR_TOTAL,
      VALOR_TOTAL_PRODUTOS,
      VALOR_DESCONTO,
      VALOR_OUTRAS_DESPESAS,
      BASE_CALCULO_ICMS,
      VALOR_ICMS,
      BASE_CALCULO_ICMS_ST,
      VALOR_ICMS_ST,
      BASE_CALCULO_PIS,
      VALOR_PIS,
      BASE_CALCULO_COFINS,
      VALOR_COFINS,
      VALOR_IPI,
      VALOR_FRETE,
      VALOR_SEGURO,
      VALOR_RECEBIDO_DINHEIRO,
      VALOR_RECEBIDO_PIX,
      VALOR_RECEBIDO_CARTAO_CRED,
      VALOR_RECEBIDO_CARTAO_DEB,
      VALOR_RECEBIDO_CREDITO,
      VALOR_RECEBIDO_COBRANCA,
      VALOR_RECEBIDO_CHEQUE,
      VALOR_RECEBIDO_FINANCEIRA,
      PESO_LIQUIDO,
      PESO_BRUTO,
      INFORMACOES_COMPLEMENTARES,
      VALOR_ICMS_INTER
    )values(
      vNotaFiscalId,
      vDadosNota.CADASTRO_ID,
      vDadosNota.CFOP_ID,
      vDadosNota.EMPRESA_ID,
      iACUMULADO_ID,
      vModeloNota,
      vSerieNota,
      vDadosNota.NATUREZA_OPERACAO,
      'N',
      'ACU',
      vDadosNota.RAZAO_SOCIAL_EMITENTE,
      vDadosNota.NOME_FANTASIA_EMITENTE,
      vRegimeTributario,
      vDadosNota.CNPJ_EMITENTE,
      vDadosNota.INSCRICAO_ESTADUAL_EMITENTE,
      vDadosNota.LOGRADOURO_EMITENTE,
      vDadosNota.COMPLEMENTO_EMITENTE,
      vDadosNota.NOME_BAIRRO_EMITENTE,
      vDadosNota.NOME_CIDADE_EMITENTE,
      vDadosNota.NUMERO_EMITENTE,
      vDadosNota.ESTADO_ID_EMITENTE,
      vDadosNota.CEP_EMITENTE,
      vDadosNota.TELEFONE_EMITENTE,
      vDadosNota.CODIGO_IBGE_MUNICIPIO_EMIT,
      vDadosNota.CODIGO_IBGE_ESTADO_EMITENT,
      vDadosNota.NOME_FANTASIA_DESTINATARIO,
      vDadosNota.RAZAO_SOCIAL_DESTINATARIO,
      vDadosNota.TIPO_PESSOA_DESTINATARIO,
      vDadosNota.CPF_CNPJ_DESTINATARIO,
      vDadosNota.INSCRICAO_ESTADUAL_DESTINAT,
      vDadosNota.NOME_CONSUMIDOR_FINAL,
      vDadosNota.TELEFONE_CONSUMIDOR_FINAL,
      vTipoNotaGerar,
      vDadosNota.LOGRADOURO_DESTINATARIO,
      vDadosNota.COMPLEMENTO_DESTINATARIO,
      vDadosNota.NOME_BAIRRO_DESTINATARIO,
      vDadosNota.NOME_CIDADE_DESTINATARIO,
      vDadosNota.ESTADO_ID_DESTINATARIO,
      vDadosNota.CEP_DESTINATARIO,
      vDadosNota.NUMERO_DESTINATARIO,
      vDadosNota.CODIGO_IBGE_MUNICIPIO_DEST,
      vDadosNota.VALOR_TOTAL,
      vDadosNota.VALOR_TOTAL_PRODUTOS,
      vDadosNota.VALOR_DESCONTO,
      vDadosNota.VALOR_OUTRAS_DESPESAS,
      vDadosNota.BASE_CALCULO_ICMS,
      vDadosNota.VALOR_ICMS,
      vDadosNota.BASE_CALCULO_ICMS_ST,
      vDadosNota.VALOR_ICMS_ST,
      vDadosNota.BASE_CALCULO_PIS,
      vDadosNota.VALOR_PIS,
      vDadosNota.BASE_CALCULO_COFINS,
      vDadosNota.VALOR_COFINS,
      vDadosNota.VALOR_IPI,
      vDadosNota.VALOR_FRETE,
      vDadosNota.VALOR_SEGURO,
      vDadosNota.VALOR_RECEBIDO_DINHEIRO,
      vDadosNota.VALOR_RECEBIDO_PIX,
      vDadosNota.VALOR_RECEBIDO_CARTAO_CRED,
      vDadosNota.VALOR_RECEBIDO_CARTAO_DEB,
      vDadosNota.VALOR_RECEBIDO_CREDITO,
      vDadosNota.VALOR_RECEBIDO_COBRANCA,
      vDadosNota.VALOR_RECEBIDO_CHEQUE,
      vDadosNota.VALOR_RECEBIDO_FINANCEIRA,
      vDadosNota.PESO_LIQUIDO,
      vDadosNota.PESO_BRUTO,
      vDadosNota.INFORMACOES_COMPLEMENTARES,
      vDadosNota.VALOR_ICMS_INTER
    );

    for i in 0..vDadosItens.count - 1 loop
      insert into NOTAS_FISCAIS_ITENS(
        NOTA_FISCAL_ID,
        PRODUTO_ID,
        ITEM_ID,
        NOME_PRODUTO,
        UNIDADE,
        CFOP_ID,
        CST,
        CODIGO_NCM,
        VALOR_TOTAL,
        PRECO_UNITARIO,
        QUANTIDADE,
        VALOR_TOTAL_DESCONTO,
        VALOR_TOTAL_OUTRAS_DESPESAS,
        BASE_CALCULO_ICMS,
        PERCENTUAL_ICMS,
        VALOR_ICMS,
        BASE_CALCULO_ICMS_ST,
        VALOR_ICMS_ST,
        CST_PIS,
        BASE_CALCULO_PIS,
        PERCENTUAL_PIS,
        VALOR_PIS,
        CST_COFINS,
        BASE_CALCULO_COFINS,
        PERCENTUAL_COFINS,
        VALOR_COFINS,
        INDICE_REDUCAO_BASE_ICMS,
        IVA,
        PRECO_PAUTA,
        CODIGO_BARRAS,
        VALOR_IPI,
        PERCENTUAL_IPI,
        INDICE_REDUCAO_BASE_ICMS_ST,
        PERCENTUAL_ICMS_ST,
        CEST,
        PERCENTUAL_ICMS_INTER,
        VALOR_ICMS_INTER,
        BASE_CALCULO_ICMS_INTER
      )values(
        vNotaFiscalId,
        vDadosItens(i).PRODUTO_ID,
        vDadosItens(i).ITEM_ID,
        vDadosItens(i).NOME_PRODUTO,
        vDadosItens(i).UNIDADE,
        vDadosItens(i).CFOP_ID,
        vDadosItens(i).CST,
        vDadosItens(i).CODIGO_NCM,
        vDadosItens(i).VALOR_TOTAL,
        vDadosItens(i).PRECO_UNITARIO,
        vDadosItens(i).QUANTIDADE,
        vDadosItens(i).VALOR_TOTAL_DESCONTO,
        vDadosItens(i).VALOR_TOTAL_OUTRAS_DESPESAS,
        vDadosItens(i).BASE_CALCULO_ICMS,
        vDadosItens(i).PERCENTUAL_ICMS,
        vDadosItens(i).VALOR_ICMS,
        vDadosItens(i).BASE_CALCULO_ICMS_ST,
        vDadosItens(i).VALOR_ICMS_ST,
        vDadosItens(i).CST_PIS,
        vDadosItens(i).BASE_CALCULO_PIS,
        vDadosItens(i).PERCENTUAL_PIS,
        vDadosItens(i).VALOR_PIS,
        vDadosItens(i).CST_COFINS,
        vDadosItens(i).BASE_CALCULO_COFINS,
        vDadosItens(i).PERCENTUAL_COFINS,
        vDadosItens(i).VALOR_COFINS,
        vDadosItens(i).INDICE_REDUCAO_BASE_ICMS,
        vDadosItens(i).IVA,
        vDadosItens(i).PRECO_PAUTA,
        vDadosItens(i).CODIGO_BARRAS,
        vDadosItens(i).VALOR_IPI,
        vDadosItens(i).PERCENTUAL_IPI,
        vDadosItens(i).INDICE_REDUCAO_BASE_ICMS_ST,
        vDadosItens(i).PERCENTUAL_ICMS_ST,
        vDadosItens(i).CEST,
        vDadosItens(i).PERCENTUAL_ICMS_INTER,
        vDadosItens(i).VALOR_ICMS_INTER,
        vDadosItens(i).BASE_CALCULO_ICMS_INTER
      );
    end loop;
  
  end GERAR_NOTA_ACUMULADO;
