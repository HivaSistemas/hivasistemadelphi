﻿create or replace procedure REPROCESSAR_CUSTOS_COMPRA_PED(iORCAMENTO_ID in number)
is

  vDataRecebimento  date;
  
  cursor cItensAtual is
  select
    ITE.ITEM_ID,
    
    CUS.PRECO_FINAL,
    CUS.PRECO_FINAL_MEDIO,
    CUS.PRECO_LIQUIDO,
    CUS.PRECO_LIQUIDO_MEDIO,
    CUS.CMV,
    CUS.CUSTO_PEDIDO_MEDIO,
    CUS.CUSTO_ULTIMO_PEDIDO,
    
    case PAE.TIPO_CUSTO_VIS_LUCRO_VENDA 
      when 'FIN' then zvl(CUS.PRECO_LIQUIDO, CUS.CUSTO_ULTIMO_PEDIDO) 
      when 'COM' then zvl(CUS.CUSTO_ULTIMO_PEDIDO, CUS.PRECO_LIQUIDO) 
    else CUS.CMV end as CUSTO_COMPRA_COMERCIAL
  from
    ORCAMENTOS_ITENS ITE
  
  inner join ORCAMENTOS ORC
  on ITE.ORCAMENTO_ID = ORC.ORCAMENTO_ID

  inner join VW_CUSTOS_PRODUTOS CUS
  on ORC.EMPRESA_ID = CUS.EMPRESA_ID
  and ITE.PRODUTO_ID = CUS.PRODUTO_ID
  
  inner join PARAMETROS_EMPRESA PAE
  on ORC.EMPRESA_ID = PAE.EMPRESA_ID
  
  where ITE.ORCAMENTO_ID = iORCAMENTO_ID;
  
begin

  select
    trunc(DATA_HORA_RECEBIMENTO)
  into
    vDataRecebimento
  from
    ORCAMENTOS
  where ORCAMENTO_ID = iORCAMENTO_ID;
  
  /* Se for a data atual, pegando os custos atuais */
 /* if to_char(vDataRecebimento, 'mm/yyyyy') = to_char(trunc(sysdate), 'mm/yyyy') then */
    for xItens in cItensAtual loop
      update ORCAMENTOS_ITENS set
        PRECO_FINAL         = round(xItens.PRECO_FINAL * QUANTIDADE, 2),
        PRECO_FINAL_MEDIO   = round(xItens.PRECO_FINAL_MEDIO * QUANTIDADE, 2),
        PRECO_LIQUIDO       = round(xItens.PRECO_LIQUIDO * QUANTIDADE, 2),
        PRECO_LIQUIDO_MEDIO = round(xItens.PRECO_LIQUIDO_MEDIO * QUANTIDADE, 2),
        CMV                 = round(xItens.CMV * QUANTIDADE, 2),
        CUSTO_ULTIMO_PEDIDO = round(xItens.CUSTO_ULTIMO_PEDIDO * QUANTIDADE, 2),
        CUSTO_PEDIDO_MEDIO  = round(xItens.CUSTO_PEDIDO_MEDIO * QUANTIDADE, 2),
        VALOR_CUSTO_FINAL   = VALOR_ENCARGOS + VALOR_IMPOSTOS + VALOR_CUSTO_VENDA + round(xItens.CUSTO_COMPRA_COMERCIAL * QUANTIDADE, 2)
      where ORCAMENTO_ID = iORCAMENTO_ID
      and ITEM_ID = xItens.ITEM_ID;
    end loop;
 /* else
    /* Falta finalizar a finalização dos custos, etc. */
  /*  ERRO('FALHA AO PROCESSAR OS CUSTOS DO PEDIDO, ENTRE EM CONTATO COM A Hiva!');
  end if; */

end REPROCESSAR_CUSTOS_COMPRA_PED;
/