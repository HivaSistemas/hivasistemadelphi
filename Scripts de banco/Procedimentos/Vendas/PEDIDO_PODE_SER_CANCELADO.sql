﻿create or replace procedure PEDIDO_PODE_SER_CANCELADO(iORCAMENTO_ID in number)
is
  vQtde         number;
  vAcumuladoId  ORCAMENTOS.ACUMULADO_ID%type;
begin

  select
    count(ORC.ORCAMENTO_ID)
  into
    vQtde
  from
    ORCAMENTOS ORC

  inner join TURNOS_CAIXA TUR
  on ORC.TURNO_ID = TUR.TURNO_ID
  and TUR.DATA_HORA_FECHAMENTO is not null

  where ORCAMENTO_ID = iORCAMENTO_ID;

  if vQtde > 0 then
    ERRO('O turno em que este pedido foi recebido já foi fechado, por favor faça a devolução pela tela de "Devoluções de pedidos"!');
  end if;


  select
    ACUMULADO_ID
  into
    vAcumuladoId
  from
    ORCAMENTOS
  where ORCAMENTO_ID = iORCAMENTO_ID;

  if vAcumuladoId is not null then
    ERRO('Este pedido pertence a um acumulado, cancelamento não permitido!');
  end if;


  select
    count(*)
  into
    vQtde
  from
    CONTAS_RECEBER_BAIXAS_ITENS BXI

  inner join CONTAS_RECEBER COR
  on BXI.RECEBER_ID = COR.RECEBER_ID

  inner join TIPOS_COBRANCA TCO
  on COR.COBRANCA_ID = TCO.COBRANCA_ID

  where COR.ORCAMENTO_ID = iORCAMENTO_ID
  and TCO.FORMA_PAGAMENTO <> 'DIN';

  if vQtde > 0 then
    ERRO('Existem baixas de títulos deste pedido que são diferentes de dinheiro, por favor faça o estorno destes títulos primeiro!');
  end if;


  select
    count(*)
  into
    vQtde
  from
    ENTREGAS
  where ORCAMENTO_ID = iORCAMENTO_ID;

  if vQtde > 0 then
    ERRO('O pedido que está sendo cancelado já possui entregas, por favor cancele as entregas antes de continuar!');
  end if;

end
;
/