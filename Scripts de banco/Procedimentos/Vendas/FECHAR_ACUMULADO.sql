create or replace procedure FECHAR_ACUMULADO(iACUMULADO_ID in number)
is

  vValorBaixarCredito   number default 0;
  vValorRestante        number default 0;

  vBaixaCreditoPagarId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;
  vValorDinheiro        ACUMULADOS.VALOR_DINHEIRO%type;
  vValorCheque          ACUMULADOS.VALOR_CHEQUE%type;
  vValorCartaoDebito    ACUMULADOS.VALOR_CARTAO_DEBITO%type;
  vValorCartaoCredito   ACUMULADOS.VALOR_CARTAO_CREDITO%type;
  vValorCobranca        ACUMULADOS.VALOR_COBRANCA%type;
  vValorFinanceira      ACUMULADOS.VALOR_FINANCEIRA%type;
  vValorCredito         ACUMULADOS.VALOR_CREDITO%type;
  vValorTotal           ACUMULADOS.VALOR_TOTAL%type;

  cursor cCreditos is
  select
    ACU.PAGAR_ID,
    PAG.VALOR_DOCUMENTO - PAG.VALOR_ADIANTADO - PAG.VALOR_DESCONTO as VALOR_LIQUIDO
  from
    ACUMULADOS_CREDITOS ACU

  inner join CONTAS_PAGAR PAG
  on ACU.PAGAR_ID = PAG.PAGAR_ID

  where ACU.ACUMULADO_ID = iACUMULADO_ID
  order by
    case when PAG.ORIGEM = 'ADA' then 0 else 1 end;

  cursor cOrcs is
  select
    ORCAMENTO_ID
  from
    ACUMULADOS_ORCAMENTOS
  where ACUMULADO_ID = iACUMULADO_ID;

begin

  select
    ACU.VALOR_DINHEIRO,
    ACU.VALOR_CHEQUE,
    ACU.VALOR_CARTAO_DEBITO,
    ACU.VALOR_CARTAO_CREDITO,
    ACU.VALOR_COBRANCA,
    ACU.VALOR_FINANCEIRA,
    ACU.VALOR_CREDITO,
    ACU.VALOR_TOTAL
  into
    vValorDinheiro,
    vValorCheque,
    vValorCartaoDebito,
    vValorCartaoCredito,
    vValorCobranca,
    vValorFinanceira,
    vValorCredito,
    vValorTotal
  from
    ACUMULADOS ACU
  where ACU.ACUMULADO_ID = iACUMULADO_ID;

  if vValorCredito > 0 then
    vValorRestante := vValorTotal - (vValorDinheiro + vValorCheque + vValorCartaoDebito + vValorCartaoCredito + vValorCobranca + vValorFinanceira);
    for xCreditos in cCreditos loop
      if xCreditos.VALOR_LIQUIDO >= vValorRestante then
        vValorBaixarCredito := vValorRestante;
        vValorRestante := 0;
      else
        vValorBaixarCredito := xCreditos.VALOR_LIQUIDO;
        vValorRestante := vValorRestante - vValorBaixarCredito;
      end if;

      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iACUMULADO_ID, 'ACU', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);

      if vValorRestante = 0 then
        exit;
      end if;
    end loop;
  end if;
  
  for xOrcs in cOrcs loop
    delete from CONTAS_RECEBER
    where ORCAMENTO_ID = xOrcs.ORCAMENTO_ID
    and COBRANCA_ID = (select TIPO_COB_ACUMULATIVO_ID from PARAMETROS);
  end loop;

end FECHAR_ACUMULADO;
/