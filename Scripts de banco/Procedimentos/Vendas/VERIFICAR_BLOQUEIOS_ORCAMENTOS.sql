create or replace procedure VERIFICAR_BLOQUEIOS_ORCAMENTOS(iORCAMENTO_ID in number)
is
  vQtde number;
begin

  select 
    count(*)
  into
    vQtde
  from ORCAMENTOS_BLOQUEIOS
  where ORCAMENTO_ID = iORCAMENTO_ID
  and USUARIO_LIBERACAO_ID is null;
  
  if vQtde = 0 then
    update ORCAMENTOS set 
      STATUS = case when STATUS = 'OB' then 'OE' else 'VR' end
    where ORCAMENTO_ID = iORCAMENTO_ID;
  end if;

end VERIFICAR_BLOQUEIOS_ORCAMENTOS;
/