﻿create or replace procedure DISTRIBUIR_KIT_FECHAMENTO_PED(iORCAMENTO_ID in number)
is

  /* Variaveis de controle do kit */
  i                     number;
  vPosicMaiorValorKit   number;
  vItensKit             RecordsVendas.ArrayOfRecItensKitFechamento;
  vProdutoId            ORCAMENTOS_ITENS.PRODUTO_ID%type;
  vMaiorValorKit        ORCAMENTOS_ITENS.VALOR_TOTAL%type;
  vDiferenca            number;
  vMaiorId              ORCAMENTOS_ITENS.ITEM_ID%type;

  cursor cItensKit is
  select
    ITE.PRODUTO_ID as PRODUTO_KIT_ID,
    ITE.ITEM_ID as PRODUTO_KIT_ITEM_ID,
    KIT.PRODUTO_ID,
    KIT.PERC_PARTICIPACAO,
    ITE.QUANTIDADE as QUANTIDADE_VENDIDAS_KIT,
    KIT.QUANTIDADE * ITE.QUANTIDADE as QUANTIDADE_ITEM_KIT,
    ITE.QUANTIDADE_RETIRAR_ATO * KIT.QUANTIDADE as QTDE_RET_ATO_ITEM_KIT,
    ITE.QUANTIDADE_RETIRAR * KIT.QUANTIDADE as QTDE_RETIRAR_ITEM_KIT,
    ITE.QUANTIDADE_ENTREGAR * KIT.QUANTIDADE as QTDE_ENTREGAR_ITEM_KIT,
    ITE.QUANTIDADE_SEM_PREVISAO * KIT.QUANTIDADE as QTDE_SEM_PREV_ITEM_KIT,
    ITE.PRECO_UNITARIO,
    ITE.VALOR_TOTAL,
    ITE.VALOR_TOTAL_DESCONTO,
    ITE.VALOR_TOTAL_OUTRAS_DESPESAS,

    zvl(round(ITE.VALOR_TOTAL / ITE.QUANTIDADE / KIT.QUANTIDADE * KIT.PERC_PARTICIPACAO * 0.01, 2), 0.001) as PRECO_UNITARIO_ITEM_KIT,
    zvl(round(ITE.VALOR_TOTAL / ITE.QUANTIDADE / KIT.QUANTIDADE * KIT.PERC_PARTICIPACAO * 0.01, 2), 0.001) * (KIT.QUANTIDADE * ITE.QUANTIDADE) as VALOR_TOTAL_ITEM_KIT,
    round(ITE.VALOR_TOTAL_DESCONTO / ITE.QUANTIDADE / KIT.QUANTIDADE * KIT.PERC_PARTICIPACAO * 0.01, 2) as VALOR_TOT_DESC_ITEM_KIT,
    round(ITE.VALOR_TOTAL_OUTRAS_DESPESAS / ITE.QUANTIDADE * KIT.PERC_PARTICIPACAO * 0.01, 2) as VALOR_TOT_OUT_DESP_ITEM_KIT,
    zvl(round(ITE.PRECO_BASE / KIT.QUANTIDADE * KIT.PERC_PARTICIPACAO * 0.01, 2), 0.01) as PRECO_BASE
  from
    ORCAMENTOS_ITENS ITE

  inner join PRODUTOS_KIT KIT
  on ITE.PRODUTO_ID = KIT.PRODUTO_KIT_ID
    
  where ITE.ORCAMENTO_ID = iORCAMENTO_ID
  and ITE.TIPO_CONTROLE_ESTOQUE in('K', 'A')
  order by
    ITE.PRODUTO_ID;

begin
  select
    max(ITEM_ID)
  into
    vMaiorId
  from
    ORCAMENTOS_ITENS
  where ORCAMENTO_ID = iORCAMENTO_ID;
  
  /* Incrementando mais 1 porque o contador i se inicia com 0 */
  vMaiorId := vMaiorId + 1;

  /* Quebrando os produtos kits */
  i := 0;  
  vDiferenca := 0;
  vProdutoId := -1;  
  for vItens in cItensKit loop
    if vProdutoId <> vItens.PRODUTO_KIT_ID then
      if vDiferenca <> 0 then
        if vDiferenca > 0 then
          vItensKit(vPosicMaiorValorKit).VALOR_TOTAL_DESCONTO := vItensKit(vPosicMaiorValorKit).VALOR_TOTAL_DESCONTO + vDiferenca;
        else
          vItensKit(vPosicMaiorValorKit).VALOR_TOTAL_OUTRAS_DESPESAS := vItensKit(vPosicMaiorValorKit).VALOR_TOTAL_OUTRAS_DESPESAS + vDiferenca;
        end if;
      end if;
      
      vMaiorValorKit := 0;
      vProdutoId := vItens.PRODUTO_KIT_ID;            
      vDiferenca := vItens.VALOR_TOTAL;
    end if;
      
    vItensKit(i).PRODUTO_ID                  := vItens.PRODUTO_ID;
    vItensKit(i).ITEM_ID                     := i + vMaiorId;
    vItensKit(i).ITEM_KIT_ID                 := vItens.PRODUTO_KIT_ITEM_ID;
    vItensKit(i).PRECO_UNITARIO              := vItens.PRECO_UNITARIO_ITEM_KIT;
    vItensKit(i).QUANTIDADE                  := vItens.QUANTIDADE_ITEM_KIT;
    vItensKit(i).VALOR_TOTAL                 := vItens.VALOR_TOTAL_ITEM_KIT;
    vItensKit(i).VALOR_TOTAL_DESCONTO        := vItens.VALOR_TOT_DESC_ITEM_KIT;
    vItensKit(i).VALOR_TOTAL_OUTRAS_DESPESAS := vItens.VALOR_TOT_OUT_DESP_ITEM_KIT;
   
    vItensKit(i).QUANTIDADE_RETIRAR_ATO      := vItens.QTDE_RET_ATO_ITEM_KIT;
    vItensKit(i).QUANTIDADE_RETIRAR          := vItens.QTDE_RETIRAR_ITEM_KIT;
    vItensKit(i).QUANTIDADE_ENTREGAR         := vItens.QTDE_ENTREGAR_ITEM_KIT;
    vItensKit(i).QUANTIDADE_SEM_PREVISAO     := vItens.QTDE_SEM_PREV_ITEM_KIT;
    vItensKit(i).PRECO_BASE                  := vItens.PRECO_BASE;
    
    /* Salvando a posição de quem tem o valor mais significativo */
    if vItens.VALOR_TOTAL_ITEM_KIT > vMaiorValorKit then
      vPosicMaiorValorKit := i;
      vMaiorValorKit := vItens.VALOR_TOTAL_ITEM_KIT;
    end if;
    
    i := i + 1;
    vDiferenca := vDiferenca - vItens.VALOR_TOTAL_ITEM_KIT;
  end loop;
      
  /* Voltando um para trás caso tenha saído do loop */
  i := i - 1;
  if vDiferenca <> 0 then
    if vDiferenca > 0 then      
      vItensKit(vPosicMaiorValorKit).VALOR_TOTAL_OUTRAS_DESPESAS := vItensKit(vPosicMaiorValorKit).VALOR_TOTAL_OUTRAS_DESPESAS + vDiferenca;
    else    
      vItensKit(vPosicMaiorValorKit).VALOR_TOTAL_DESCONTO := vItensKit(vPosicMaiorValorKit).VALOR_TOTAL_DESCONTO + abs(vDiferenca);      
    end if;
  end if;
  
  for i in 0..vItensKit.count - 1 loop
    begin
    insert into ORCAMENTOS_ITENS(
      ORCAMENTO_ID,
      PRODUTO_ID,
      ITEM_ID,
      ITEM_KIT_ID,
      PRECO_UNITARIO,
      QUANTIDADE,
      VALOR_TOTAL,
      VALOR_TOTAL_DESCONTO,
      VALOR_TOTAL_OUTRAS_DESPESAS,
      QUANTIDADE_RETIRAR_ATO,
      QUANTIDADE_RETIRAR,
      QUANTIDADE_ENTREGAR,
      QUANTIDADE_SEM_PREVISAO,
      PRECO_BASE
    )values(
      iORCAMENTO_ID,
      vItensKit(i).PRODUTO_ID,
      vItensKit(i).ITEM_ID,
      vItensKit(i).ITEM_KIT_ID,
      vItensKit(i).PRECO_UNITARIO,
      vItensKit(i).QUANTIDADE,
      vItensKit(i).VALOR_TOTAL,
      vItensKit(i).VALOR_TOTAL_DESCONTO,
      vItensKit(i).VALOR_TOTAL_OUTRAS_DESPESAS,
      vItensKit(i).QUANTIDADE_RETIRAR_ATO,
      vItensKit(i).QUANTIDADE_RETIRAR,
      vItensKit(i).QUANTIDADE_ENTREGAR,
      vItensKit(i).QUANTIDADE_SEM_PREVISAO,
      vItensKit(i).PRECO_BASE

    );
    exception
      when others then
        erro( 'Base ' || vItensKit(i).PRECO_BASE || ' Unitario ' || vItensKit(i).PRECO_UNITARIO);
    end;
  end loop;

end DISTRIBUIR_KIT_FECHAMENTO_PED;