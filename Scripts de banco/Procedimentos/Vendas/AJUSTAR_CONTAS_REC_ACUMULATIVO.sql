﻿create or replace procedure AJUSTAR_CONTAS_REC_ACUMULATIVO(iORCAMENTO_ID in number)
is 
  i                     number default 0;
  vValorAcumulado       number not null default 0;
  vValorRestante        number not null default 0;
  vDadosPagamentoPedido RecordsVendas.RecPedidosPagamentos;
  vPrevisoes            RecordsFinanceiros.ArrayOfRecParcelasReceber;
  vReceberId            CONTAS_RECEBER.RECEBER_ID%type;
  vDataRecebimento      ORCAMENTOS.DATA_CADASTRO%type;
  
  cursor c_dias_prazo(pCOBRANCA_ID positive) is
  select
    DIAS,
    rownum as PARCELA,
    count(*) over (partition by count(*)) as NUMERO_PARCELAS
  from
    TIPO_COBRANCA_DIAS_PRAZO      
  where COBRANCA_ID = pCOBRANCA_ID  
  group by
    DIAS,
    rownum
  order by DIAS;
  
begin
  delete from CONTAS_RECEBER
  where ORCAMENTO_ID = iORCAMENTO_ID
  and COBRANCA_ID = (select TIPO_COB_ACUMULATIVO_ID from PARAMETROS);

  begin
    select
      VALOR_DINHEIRO,
      VALOR_CARTAO_DEBITO,
      VALOR_CARTAO_CREDITO,
      VALOR_CREDITO,
      VALOR_COBRANCA,
      VALOR_CHEQUE,
      VALOR_FINANCEIRA,
      ORC.VALOR_ACUMULATIVO - ORC.VALOR_DEV_ACUMULADO_ABERTO as VALOR_ACUMULATIVO,
      ORC.VENDEDOR_ID,
      ORC.EMPRESA_ID,
      ORC.CLIENTE_ID,
      ORC.STATUS,
      ORC.TURNO_ID,
      ORC.DATA_HORA_RECEBIMENTO
    into
      vDadosPagamentoPedido.VALOR_DINHEIRO,
      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO,
      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO,
      vDadosPagamentoPedido.VALOR_CREDITO,
      vDadosPagamentoPedido.VALOR_COBRANCA,
      vDadosPagamentoPedido.VALOR_CHEQUE,
      vDadosPagamentoPedido.VALOR_FINANCEIRA,
      vDadosPagamentoPedido.VALOR_ACUMULATIVO,
      vDadosPagamentoPedido.VENDEDOR_ID,
      vDadosPagamentoPedido.EMPRESA_ID,
      vDadosPagamentoPedido.CLIENTE_ID,
      vDadosPagamentoPedido.STATUS,
      vDadosPagamentoPedido.TURNO_ID,
      vDadosPagamentoPedido.DATA_HORA_RECEBIMENTO
    from
      ORCAMENTOS ORC
    where ORC.ORCAMENTO_ID = iORCAMENTO_ID;
  exception
    when others then
     ERRO('Não foi encontrado os valores de pagamento para o orçamento ' || iORCAMENTO_ID || '!' || sqlerrm);
  end;

   /* Inserindo os cartões no financeiro */
  if vDadosPagamentoPedido.VALOR_ACUMULATIVO > 0 then
    vValorRestante :=
      vDadosPagamentoPedido.VALOR_ACUMULATIVO +
      vDadosPagamentoPedido.VALOR_DINHEIRO +
      vDadosPagamentoPedido.VALOR_CARTAO_DEBITO +
      vDadosPagamentoPedido.VALOR_CARTAO_CREDITO +
      vDadosPagamentoPedido.VALOR_CREDITO +
      vDadosPagamentoPedido.VALOR_COBRANCA +
      vDadosPagamentoPedido.VALOR_CHEQUE +
      vDadosPagamentoPedido.VALOR_FINANCEIRA;
    for v_dias_prazo in c_dias_prazo(SESSAO.PARAMETROS_GERAIS.TIPO_COB_ACUMULATIVO_ID) loop
      vPrevisoes(i).COBRANCA_ID         := SESSAO.PARAMETROS_GERAIS.TIPO_COB_ACUMULATIVO_ID;
      vPrevisoes(i).PARCELA             := v_dias_prazo.PARCELA;
      vPrevisoes(i).NUMERO_PARCELAS     := v_dias_prazo.NUMERO_PARCELAS;
      vPrevisoes(i).VALOR_PARCELA       := round(
        (
          vDadosPagamentoPedido.VALOR_ACUMULATIVO +
          vDadosPagamentoPedido.VALOR_DINHEIRO +
          vDadosPagamentoPedido.VALOR_CARTAO_DEBITO +
          vDadosPagamentoPedido.VALOR_CARTAO_CREDITO +
          vDadosPagamentoPedido.VALOR_CREDITO +
          vDadosPagamentoPedido.VALOR_COBRANCA +
          vDadosPagamentoPedido.VALOR_CHEQUE +
          vDadosPagamentoPedido.VALOR_FINANCEIRA
        ) / v_dias_prazo.NUMERO_PARCELAS, 2);

      if SESSAO.ROTINA = 'CANCELAR_FECHAMENTO_ACUMULADO' then
        vPrevisoes(i).DATA_VENCIMENTO     := trunc(vDadosPagamentoPedido.DATA_HORA_RECEBIMENTO) + v_dias_prazo.DIAS;
      else
        vPrevisoes(i).DATA_VENCIMENTO     := trunc(sysdate) + v_dias_prazo.DIAS;
      end if;

      vPrevisoes(i).PORTADOR_ID         := '9999';
      vPrevisoes(i).PLANO_FINANCEIRO_ID := '1.001.005';
      
      vValorRestante := vValorRestante - vPrevisoes(i).VALOR_PARCELA;
      
      if v_dias_prazo.PARCELA = v_dias_prazo.NUMERO_PARCELAS and vValorRestante <> 0 then
        vPrevisoes(i).VALOR_PARCELA := vPrevisoes(i).VALOR_PARCELA + vValorRestante;
      end if;
      
      i := i + 1;
      if SESSAO.ROTINA = 'CANCELAR_FECHAMENTO_ACUMULADO' then
        vDataRecebimento := trunc(vDadosPagamentoPedido.DATA_HORA_RECEBIMENTO);
      end if;
    end loop;

    if vPrevisoes.first is null then
      ERRO('O Hiva não conseguiu gerar as parcelas da previsão do acumulado corretamente, por favor verifique se o tipo de cobrança está parametrizado corretamente!');
    end if;

    for i in vPrevisoes.first..vPrevisoes.last loop
      vReceberId := SEQ_RECEBER_ID.nextval;

      insert into CONTAS_RECEBER(
        RECEBER_ID,
        CADASTRO_ID,
        EMPRESA_ID,
        ORIGEM,
        ORCAMENTO_ID,
        COBRANCA_ID,
        PORTADOR_ID,
        PLANO_FINANCEIRO_ID,
        VENDEDOR_ID,
        DOCUMENTO,
        DATA_VENCIMENTO,
        DATA_VENCIMENTO_ORIGINAL,        
        VALOR_DOCUMENTO,
        STATUS,
        PARCELA,
        NUMERO_PARCELAS
      )values(
        vReceberId,
        vDadosPagamentoPedido.CLIENTE_ID,
        vDadosPagamentoPedido.EMPRESA_ID,
        'ORC',
        iORCAMENTO_ID,
        vPrevisoes(i).COBRANCA_ID,
        vPrevisoes(i).PORTADOR_ID,
        vPrevisoes(i).PLANO_FINANCEIRO_ID,        
        vDadosPagamentoPedido.VENDEDOR_ID,
        'PREV-' || iORCAMENTO_ID || '-' || vPrevisoes(i).PARCELA || '/' || vPrevisoes(i).NUMERO_PARCELAS || '/ACU',
        vPrevisoes(i).DATA_VENCIMENTO,
        vPrevisoes(i).DATA_VENCIMENTO,        
        vPrevisoes(i).VALOR_PARCELA,
        'A',
        vPrevisoes(i).PARCELA,
        vPrevisoes(i).NUMERO_PARCELAS
      );
    end loop;

    if SESSAO.ROTINA = 'CANCELAR_FECHAMENTO_ACUMULADO' then
      update CONTAS_RECEBER set
        DATA_CADASTRO = vDataRecebimento,
        DATA_CONTABIL = vDataRecebimento,
        DATA_EMISSAO = vDataRecebimento
      where ORCAMENTO_ID = iORCAMENTO_ID
      and COBRANCA_ID = (select TIPO_COB_ACUMULATIVO_ID from PARAMETROS);
    end if;
  end if;

end AJUSTAR_CONTAS_REC_ACUMULATIVO;
/