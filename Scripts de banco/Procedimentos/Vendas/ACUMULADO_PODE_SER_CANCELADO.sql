create or replace procedure ACUMULADO_PODE_SER_CANCELADO(iACUMULADO_ID in number)
is
  vQtde         number;
  vAcumuladoId  ORCAMENTOS.ACUMULADO_ID%type;
begin

  select
    count(ACU.ACUMULADO_ID)
  into
    vQtde
  from
    ACUMULADOS ACU

  inner join TURNOS_CAIXA TUR
  on ACU.TURNO_ID = TUR.TURNO_ID
  and TUR.DATA_HORA_FECHAMENTO is not null

  where ACUMULADO_ID = iACUMULADO_ID;
  
  if vQtde > 0 then
    ERRO('O turno em que este acumulado foi recebido já foi fechado, faça a devolução pela tela de "Devoluções de pedidos"!');
  end if;

  select
    count(*)
  into
    vQtde
  from
    CONTAS_RECEBER_BAIXAS_ITENS BXI

  inner join CONTAS_RECEBER COR
  on BXI.RECEBER_ID = COR.RECEBER_ID

  inner join TIPOS_COBRANCA TCO
  on COR.COBRANCA_ID = TCO.COBRANCA_ID

  where COR.ACUMULADO_ID = iACUMULADO_ID;

  if vQtde > 0 then
    ERRO('Existem baixas de títulos deste acumulado, faça o estorno destes títulos primeiro!');
  end if;

end ACUMULADO_PODE_SER_CANCELADO;
/