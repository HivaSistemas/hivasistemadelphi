create or replace procedure BUSCAR_CALC_IMPOSTOS_PRODUTO(
  iPRODUTO_ID          in number, -- 0
  iEMPRESA_ID          in number, -- 1
  iCLIENTE_ID          in number, -- 2
  iVALOR_TOTAL_PRODUTO in number, -- 3
  iVALOR_OUTRAS_DESP   in number, -- 4
  iVALOR_DESCONTO      in number, -- 5 

  oCST                       out string, -- 6
  oBASE_CALCULO_ICMS         out number, -- 7
  oINDICE_REDUCAO_BASE_ICMS  out number, -- 8
  oPERCENTUAL_ICMS           out number, -- 9
  oVALOR_ICMS                out number, -- 10

  oINDICE_REDUCAO_BC_ICMS_ST out number, -- 11
  oBASE_CALCULO_ICMS_ST      out number, -- 12
  oPERCENTUAL_ICMS_ST        out number, -- 13
  oVALOR_ICMS_ST             out number, -- 14

  oPRECO_PAUTA               out number, -- 15
  oIVA                       out number, -- 16

  oVALOR_IPI                 out number, -- 17
  oPERCENTUAL_IPI            out number, -- 18

  oCST_PIS                   out string, -- 19
  oBASE_CALCULO_PIS          out number, -- 20
  oPERCENTUAL_PIS            out number, -- 21
  oVALOR_PIS                 out number, -- 22

  oCST_COFINS                out string, -- 23
  oBASE_CALCULO_COFINS       out number, -- 24
  oPERCENTUAL_COFINS         out number, -- 25
  oVALOR_COFINS              out number,  -- 26
  oBASE_CALCULO_ICMS_INTER   out number,  -- 27
  oPERCENTUAL_ICMS_INTER     out number,  -- 28
  oVALOR_ICMS_INTER          out number, -- 29
  iESTADO_ID_EMITENTE        in string -- 30
)
is

  type RecDadosProduto is record(
    CST                      GRUPOS_TRIB_ESTADUAL_VENDA_EST.CST_NAO_CONTRIBUINTE%type,
    PERCENTUAL_ICMS          GRUPOS_TRIB_ESTADUAL_VENDA_EST.PERCENTUAL_ICMS%type,
    PERCENTUAL_ICMS_INTER    GRUPOS_TRIB_ESTADUAL_VENDA_EST.PERCENTUAL_ICMS_INTER%type,
    PERCENTUAL_ICMS_DESTINO  GRUPOS_TRIB_ESTADUAL_VENDA_EST.PERCENTUAL_ICMS_INTER%type,
    INDICE_REDUCAO_BASE_ICMS GRUPOS_TRIB_ESTADUAL_VENDA_EST.INDICE_REDUCAO_BASE_ICMS%type,
    IVA                      GRUPOS_TRIB_ESTADUAL_VENDA_EST.IVA%type,
    PERCENTUAL_PIS           PERCENTUAIS_CUSTOS_EMPRESA.PERCENTUAL_PIS%type,
    CST_PIS                  GRUPOS_TRIB_FEDERAL_VENDA.CST_PIS%type,
    PERCENTUAL_COFINS        PERCENTUAIS_CUSTOS_EMPRESA.PERCENTUAL_COFINS%type,
    CST_COFINS               GRUPOS_TRIB_FEDERAL_VENDA.CST_COFINS%type
  );

  vTipoCliente     CLIENTES.TIPO_CLIENTE%type;
  vIECliente       CADASTROS.INSCRICAO_ESTADUAL%type;
  vEstadoClienteId ESTADOS.ESTADO_ID%type;
  vDadosProduto    RecDadosProduto;
  vImpostos        RecordsNotasFiscais.RecImpostosCalculados;
begin

  select
    CLI.TIPO_CLIENTE,
    CID.ESTADO_ID,
    CAD.INSCRICAO_ESTADUAL
  into
    vTipoCliente,
    vEstadoClienteId,
    vIECliente
  from 
    CLIENTES CLI
  
  inner join CADASTROS CAD
  on CLI.CADASTRO_ID = CAD.CADASTRO_ID
  
  inner join BAIRROS BAI
  on CAD.BAIRRO_ID = BAI.BAIRRO_ID
  
  inner join CIDADES CID
  on BAI.CIDADE_ID = CID.CIDADE_ID
  
  where CLI.CADASTRO_ID = iCLIENTE_ID;

  select
    case vTipoCliente 
      when 'NC' then PRI.CST_NAO_CONTRIBUINTE_VENDA
      when 'CO' then PRI.CST_CONTRIBUINTE_VENDA
      when 'OP' then PRI.CST_ORGAO_PUBLICO_VENDA
      when 'RE' then PRI.CST_REVENDA_VENDA
      when 'CT' then PRI.CST_CONSTRUTORA_VENDA
      when 'CH' then PRI.CST_CLINICA_HOSPITAL_VENDA
      when 'PR' then PRI.CST_PRODUTOR_RURAL_VENDA
    end as CST,
    
    PRI.PERC_ICMS_VENDA,
    PRI.INDICE_REDUCAO_BASE_ICMS_VENDA,
    PRI.IVA_VENDA,
    PAE.ALIQUOTA_PIS,
    GTF.CST_PIS_VENDA as CST_PIS,
    PAE.ALIQUOTA_COFINS,
    GTF.CST_COFINS_VENDA as CST_COFINS
  into
    vDadosProduto.CST,
    vDadosProduto.PERCENTUAL_ICMS,
    vDadosProduto.INDICE_REDUCAO_BASE_ICMS,
    vDadosProduto.IVA,
    vDadosProduto.PERCENTUAL_PIS,
    vDadosProduto.CST_PIS,
    vDadosProduto.PERCENTUAL_COFINS,
    vDadosProduto.CST_COFINS
  from
    VW_GRUPOS_TRIBUTACOES_ESTADUAL PRI

  inner join PRODUTOS PRO
  on PRI.PRODUTO_ID = PRO.PRODUTO_ID

  inner join PARAMETROS_EMPRESA PAE
  on PAE.EMPRESA_ID = iEMPRESA_ID

  inner join VW_GRUPOS_TRIBUTACOES_FEDERAL GTF
  on PRO.PRODUTO_ID = GTF.PRODUTO_ID
  and GTF.EMPRESA_ID = iEMPRESA_ID

  where PRI.PRODUTO_ID = iPRODUTO_ID
  and PRI.EMPRESA_ID = iEMPRESA_ID
  and PRI.ESTADO_ID_COMPRA = vEstadoClienteId
  and PRI.ESTADO_ID_VENDA = vEstadoClienteId;

  if (vIECliente = 'ISENTO') and (iESTADO_ID_EMITENTE <> vEstadoClienteId) then
    select
      PRI.PERCENTUAL_ICMS_INTER
    into
      vDadosProduto.PERCENTUAL_ICMS_INTER
    from
      VW_GRUPOS_TRIBUTACOES_ESTADUAL PRI

    inner join PRODUTOS PRO
    on PRI.PRODUTO_ID = PRO.PRODUTO_ID

    inner join PARAMETROS_EMPRESA PAE
    on PAE.EMPRESA_ID = iEMPRESA_ID

    where PRI.PRODUTO_ID = iPRODUTO_ID
    and PRI.EMPRESA_ID = iEMPRESA_ID
    and PRI.ESTADO_ID_COMPRA = vEstadoClienteId
    and PRI.ESTADO_ID_VENDA = vEstadoClienteId;

    --vDadosProduto.PERCENTUAL_ICMS := vDadosProduto.PERCENTUAL_ICMS_INTER;
  end if;
 
  vImpostos :=
    BUSCAR_CALC_IMPOSTOS_NOTA(
      iEMPRESA_ID,
      iVALOR_TOTAL_PRODUTO,
      iVALOR_OUTRAS_DESP,
      iVALOR_DESCONTO,
      vDadosProduto.CST,
      vDadosProduto.PERCENTUAL_ICMS,
      vDadosProduto.INDICE_REDUCAO_BASE_ICMS,
      vDadosProduto.IVA,
      vDadosProduto.PERCENTUAL_PIS,
      vDadosProduto.CST_PIS,
      vDadosProduto.PERCENTUAL_COFINS,
      vDadosProduto.CST_COFINS,
      vDadosProduto.PERCENTUAL_ICMS_INTER
    );

  oCST                       := vDadosProduto.CST;

  oBASE_CALCULO_ICMS         := vImpostos.BASE_CALCULO_ICMS;
  oINDICE_REDUCAO_BASE_ICMS  := vImpostos.INDICE_REDUCAO_BASE_ICMS;
  oPERCENTUAL_ICMS           := vImpostos.PERCENTUAL_ICMS;
  oVALOR_ICMS                := vImpostos.VALOR_ICMS;

  oINDICE_REDUCAO_BC_ICMS_ST := vImpostos.INDICE_REDUCAO_BASE_ICMS_ST;
  oBASE_CALCULO_ICMS_ST      := vImpostos.BASE_CALCULO_ICMS_ST;
  oPERCENTUAL_ICMS_ST        := vImpostos.PERCENTUAL_ICMS_ST;
  oVALOR_ICMS_ST             := vImpostos.VALOR_ICMS_ST;
  oIVA                       := vImpostos.IVA;

  oVALOR_IPI                 := vImpostos.VALOR_IPI;
  oPERCENTUAL_IPI            := vImpostos.PERCENTUAL_IPI;
  oPRECO_PAUTA               := vImpostos.PRECO_PAUTA;

  oCST_PIS                   := vDadosProduto.CST_PIS;
  oBASE_CALCULO_PIS          := vImpostos.BASE_CALCULO_PIS;
  oPERCENTUAL_PIS            := vImpostos.PERCENTUAL_PIS;
  oVALOR_PIS                 := vImpostos.VALOR_PIS;

  oCST_COFINS                := vDadosProduto.CST_COFINS;
  oBASE_CALCULO_COFINS       := vImpostos.BASE_CALCULO_COFINS;
  oPERCENTUAL_COFINS         := vImpostos.PERCENTUAL_COFINS;
  oVALOR_COFINS              := vImpostos.VALOR_COFINS;

  oBASE_CALCULO_ICMS_INTER   := vImpostos.BASE_CALCULO_ICMS_INTER;
  oPERCENTUAL_ICMS_INTER     := vImpostos.PERCENTUAL_ICMS_INTER;
  oVALOR_ICMS_INTER          := vImpostos.VALOR_ICMS_INTER;

end BUSCAR_CALC_IMPOSTOS_PRODUTO;
/