create or replace procedure PEDIDO_PODE_GERAR_NFE(iORCAMENTO_ID in number)
is
  vClienteId  ORCAMENTOS.CLIENTE_ID%type;
  
begin

  select
    CLIENTE_ID
  into
    vClienteId
  from
    ORCAMENTOS
  where ORCAMENTO_ID = iORCAMENTO_ID;
  
  if vClienteId = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then
    ERRO('Não é permitido fazer venda com emissão de NFe para consumidor final!');
  end if;
  
end PEDIDO_PODE_GERAR_NFE;