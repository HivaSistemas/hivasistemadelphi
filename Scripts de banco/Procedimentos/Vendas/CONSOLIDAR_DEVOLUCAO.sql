﻿create or replace procedure CONSOLIDAR_DEVOLUCAO(iDEVOLUCAO_ID in number)
is
  vDadosDevolucao  RecordsVendas.RecDadosDevolucao;

  vQtde            number;
  vQtdeRestante    number;
  vQtdeUtilizar    number;

  vTemEntregasSemPrevisao  boolean default false;

  vNotaFiscalId   NOTAS_FISCAIS.NOTA_FISCAL_ID%type;
  vStatus         NOTAS_FISCAIS.STATUS%type;
  vNumeroNota     NOTAS_FISCAIS.NUMERO_NOTA%type;
  vEmitirNotaFechamentoPedido PARAMETROS.EMITIR_NOTA_ACUMULADO_FECH_PED%type;
  vDeletarNFPendenteEmissao   PARAMETROS.DELETAR_NF_PENDENTE_EMISSAO%type;
  
  cursor cItens is
  select
    ITE.PRODUTO_ID,
    ITE.ITEM_ID,

    ITE.DEVOLVIDOS_ENTREGUES,
    ITE.DEVOLVIDOS_PENDENCIAS,
    ITE.DEVOLVIDOS_SEM_PREVISAO,

    ITE.VALOR_BRUTO,
    ITE.VALOR_LIQUIDO,
    ITE.VALOR_DESCONTO,
    ITE.VALOR_OUTRAS_DESPESAS
  from
    DEVOLUCOES_ITENS ITE
  where DEVOLUCAO_ID = iDEVOLUCAO_ID;


  cursor cRetiradasEntregas is
  select distinct
    'RET' as TIPO_MOVIMENTO,
    RETIRADA_ID as MOVIMENTO_ID
  from
    DEVOLUCOES_ITENS_RETIRADAS
  where DEVOLUCAO_ID = iDEVOLUCAO_ID

  union all

  select distinct
    'ENT' as TIPO_MOVIMENTO,
    ENTREGA_ID as MOVIMENTO_ID
  from
    DEVOLUCOES_ITENS_ENTREGAS
  where DEVOLUCAO_ID = iDEVOLUCAO_ID;

begin

  begin
    select
      EMITIR_NOTA_ACUMULADO_FECH_PED,
      DELETAR_NF_PENDENTE_EMISSAO
    into
      vEmitirNotaFechamentoPedido,
      vDeletarNFPendenteEmissao
    from
      PARAMETROS;
  exception
    when others then
      vEmitirNotaFechamentoPedido := 'N';
      vDeletarNFPendenteEmissao := 'N';
  end;

  select 
    ORC.CLIENTE_ID,
    ORC.EMPRESA_ID,
    ORC.ORCAMENTO_ID,
    DEV.VALOR_LIQUIDO,
    ORC.TIPO_ANALISE_CUSTO,
    CON.ACUMULATIVO,
    nvl(ACU.STATUS, 'X'),
    ACU.ACUMULADO_ID,
    DEV.SOMENTE_FISCAL
  into
    vDadosDevolucao
  from
    DEVOLUCOES DEV

  inner join ORCAMENTOS ORC
  on DEV.ORCAMENTO_ID = ORC.ORCAMENTO_ID

  inner join CONDICOES_PAGAMENTO CON
  on ORC.CONDICAO_ID = CON.CONDICAO_ID

  left join ACUMULADOS ACU
  on ORC.ACUMULADO_ID = ACU.ACUMULADO_ID

  where DEV.DEVOLUCAO_ID = iDEVOLUCAO_ID;

  if vDadosDevolucao.ACUMULADO = 'S' and vDadosDevolucao.STATUS_ACUMULADO = 'AR' then
    ERRO('Este pedido é um acumulado que teve fechamento e ainda não foi recebido, por favor faça o recebimento do acumulado!');
  end if;

  for xItens in cItens loop
    if not vTemEntregasSemPrevisao and xItens.DEVOLVIDOS_SEM_PREVISAO > 0 then
      vTemEntregasSemPrevisao := true;
      exit;
    end if;
  end loop;

  /* Deletando os itens sem previsão caso não houver mais saldo */
  /*if vTemEntregasSemPrevisao then
    delete from ENTREGAS_ITENS_SEM_PREVISAO
    where ORCAMENTO_ID = vDadosDevolucao.ORCAMENTO_ID
    and SALDO = 0;
  end if;*/

  if (vDadosDevolucao.ACUMULADO = 'S') and (vEmitirNotaFechamentoPedido = 'N') then
    if vDadosDevolucao.STATUS_ACUMULADO = 'RE' then
      select
        count(*)
      into
        vQtde
      from
        NOTAS_FISCAIS
      where ACUMULADO_ID = vDadosDevolucao.ACUMULADO_ID
      and TIPO_MOVIMENTO = 'ACU';

      if vQtde > 0 then
        select
          NOTA_FISCAL_ID,
          STATUS,
          nvl(NUMERO_NOTA, 0)
        into
          vNotaFiscalId,
          vStatus,
          vNumeroNota
        from
          NOTAS_FISCAIS
        where ACUMULADO_ID = vDadosDevolucao.ACUMULADO_ID
        and TIPO_MOVIMENTO = 'ACU';

        if vStatus <> 'C' then
          if vStatus = 'E' then
            GERAR_NOTA_DEVOLUCAO_ACUMULADO(iDEVOLUCAO_ID);
          elsif vStatus = 'N' then
            if vNumeroNota > 0 then
              ERRO('A nota fiscal do acumulado está pendente de emissão, por favor faça a emissão antes de continuar!' || chr(13) || 'Acumulado: ' || vDadosDevolucao.ACUMULADO_ID);
            end if;

            delete from NOTAS_FISCAIS_ITENS
            where NOTA_FISCAL_ID = vNotaFiscalId;

            delete from NOTAS_FISCAIS
            where NOTA_FISCAL_ID = vNotaFiscalId;
          end if;
        end if;
      end if;
    end if;
  else
    /* Gerando as notas de devolução se necessário */
    for xRetiradasEntregas in cRetiradasEntregas loop
      if xRetiradasEntregas.TIPO_MOVIMENTO = 'RET' then
        select
          count(*)
        into
          vQtde
        from
          NOTAS_FISCAIS
        where ORCAMENTO_BASE_ID = vDadosDevolucao.ORCAMENTO_ID
        and RETIRADA_ID = xRetiradasEntregas.MOVIMENTO_ID
        and TIPO_MOVIMENTO = 'VRA';

        if vQtde > 0 then
          select
            NOTA_FISCAL_ID,
            STATUS,
            nvl(NUMERO_NOTA, 0)
          into
            vNotaFiscalId,
            vStatus,
            vNumeroNota
          from
            NOTAS_FISCAIS
          where ORCAMENTO_BASE_ID = vDadosDevolucao.ORCAMENTO_ID
          and RETIRADA_ID = xRetiradasEntregas.MOVIMENTO_ID
          and TIPO_MOVIMENTO = 'VRA';

          if vStatus <> 'C' then
            if vStatus = 'E' then
              GERAR_NOTA_DEV_RETIRADA(iDEVOLUCAO_ID, xRetiradasEntregas.MOVIMENTO_ID);
            elsif vStatus = 'N' then
              if vNumeroNota > 0 then
                ERRO('A nota fiscal da retirada está pendente de emissão, por favor faça a emissão antes de continuar!' || chr(13) || 'Retirada: ' || xRetiradasEntregas.MOVIMENTO_ID);
              end if;

              delete from NOTAS_FISCAIS_ITENS
              where NOTA_FISCAL_ID = vNotaFiscalId;

              delete from NOTAS_FISCAIS
              where NOTA_FISCAL_ID = vNotaFiscalId;
            end if;
          end if;
        end if;
      else
        select
          count(*)
        into
          vQtde
        from
          NOTAS_FISCAIS
        where ORCAMENTO_BASE_ID = vDadosDevolucao.ORCAMENTO_ID
        and ENTREGA_ID = xRetiradasEntregas.MOVIMENTO_ID
        and TIPO_MOVIMENTO = 'VEN';

        if vQtde > 0 then
          select
            NOTA_FISCAL_ID,
            STATUS,
            nvl(NUMERO_NOTA, 0)
          into
            vNotaFiscalId,
            vStatus,
            vNumeroNota
          from
            NOTAS_FISCAIS
          where ORCAMENTO_BASE_ID = vDadosDevolucao.ORCAMENTO_ID
          and ENTREGA_ID = xRetiradasEntregas.MOVIMENTO_ID
          and TIPO_MOVIMENTO = 'VEN';

          if vStatus <> 'C' then
            if vStatus = 'E' then
              GERAR_NOTA_DEVOLUCAO_ENTREGA(iDEVOLUCAO_ID, xRetiradasEntregas.MOVIMENTO_ID);
            else
              if vDeletarNFPendenteEmissao = 'S' then
                if vNumeroNota > 0 then
                  ERRO('A nota fiscal da retirada está pendente de emissão, por favor faça a emissão antes de continuar!' || chr(13) || 'Retirada: ' || xRetiradasEntregas.MOVIMENTO_ID);
                end if;

                delete from NOTAS_FISCAIS_ITENS
                where NOTA_FISCAL_ID = vNotaFiscalId;

                delete from NOTAS_FISCAIS
                where NOTA_FISCAL_ID = vNotaFiscalId;
              else
                ERRO('A nota fiscal da entrega está pendente de emissão, por favor faça a emissão antes de continuar!' || chr(13) || 'Entrega: ' || xRetiradasEntregas.MOVIMENTO_ID);
              end if;
            end if;
          end if;
        end if;
      end if;
    end loop;
  end if;

  update DEVOLUCOES set
    STATUS = 'B'
  where DEVOLUCAO_ID = iDEVOLUCAO_ID;

  if vDadosDevolucao.SOMENTE_FISCAL <> 'S' then
    if (vDadosDevolucao.ACUMULADO = 'N') or (vDadosDevolucao.ACUMULADO = 'S' and vDadosDevolucao.STATUS_ACUMULADO = 'RE') then
      /* Gerando o crédito do cliente */
      GERAR_CREDITO_DEVOLUCAO(iDEVOLUCAO_ID);
    else
      update ORCAMENTOS set
        VALOR_DEV_ACUMULADO_ABERTO = VALOR_DEV_ACUMULADO_ABERTO + vDadosDevolucao.VALOR_LIQUIDO
      where ORCAMENTO_ID = vDadosDevolucao.ORCAMENTO_ID;

      AJUSTAR_CONTAS_REC_ACUMULATIVO(vDadosDevolucao.ORCAMENTO_ID);
    end if;
  end if;

end CONSOLIDAR_DEVOLUCAO;
/