﻿create or replace procedure FECHAR_PEDIDO(
  iORCAMENTO_ID in number
)
is  
  vEmpresaId            ORCAMENTOS.EMPRESA_ID%type;  
  vClienteId            CLIENTES.CADASTRO_ID%type;
  vPrevisaoEntrega      date;
  vValorBaixarCredito   number default 0;
  vValorRestante        number default 0;

  vBaixaCreditoPagarId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;
  vValorDinheiro        ORCAMENTOS.VALOR_DINHEIRO%type;
  vValorCheque          ORCAMENTOS.VALOR_CHEQUE%type;
  vValorCartaoDebito    ORCAMENTOS.VALOR_CARTAO_DEBITO%type;
  vValorCartaoCredito   ORCAMENTOS.VALOR_CARTAO_CREDITO%type;
  vValorCobranca        ORCAMENTOS.VALOR_COBRANCA%type;
  vValorFinanceira      ORCAMENTOS.VALOR_FINANCEIRA%type;
  vValorCredito         ORCAMENTOS.VALOR_CREDITO%type;
  vValorTotal           ORCAMENTOS.VALOR_TOTAL%type;

  cursor cItens is
  select
    PRODUTO_ID,
    ITEM_ID,
    QUANTIDADE,
    QUANTIDADE_RETIRAR_ATO,
    QUANTIDADE_RETIRAR,
    QUANTIDADE_ENTREGAR,
    QUANTIDADE_SEM_PREVISAO
  from
    ORCAMENTOS_ITENS
  where ORCAMENTO_ID = iORCAMENTO_ID
  order by 
    ITEM_ID;


  cursor cCreditos is
  select
    ORC.PAGAR_ID,
    PAG.VALOR_DOCUMENTO - PAG.VALOR_DESCONTO - PAG.VALOR_ADIANTADO as VALOR_LIQUIDO
  from
    ORCAMENTOS_CREDITOS ORC

  inner join CONTAS_PAGAR PAG
  on ORC.PAGAR_ID = PAG.PAGAR_ID

  where ORC.ORCAMENTO_ID = iORCAMENTO_ID
  and ORC.MOMENTO_USO = 'F';

  
  procedure INSERIR_ITEM_GERAR_ENTREGA(  
    pPRODUTO_ID in number,
    pITEM_ID in number,
    pQUANTIDADE in number,
    pPREVISAO_ENTREGA in date,
    pTIPO_ENTREGA in string
  )
  is begin
    insert into ENTREGAS_ITENS_A_GERAR_TEMP(
      ORCAMENTO_ID,
      PRODUTO_ID,
      ITEM_ID,
      QUANTIDADE,
      TIPO_ENTREGA,
      PREVISAO_ENTREGA
    )values(
      iORCAMENTO_ID,
      pPRODUTO_ID,
      pITEM_ID,
      pQUANTIDADE,
      pTIPO_ENTREGA,
      pPREVISAO_ENTREGA
    );
  end;

begin

  select
    ORC.EMPRESA_ID,    
    ORC.CLIENTE_ID,
    ORC.DATA_ENTREGA,
    ORC.VALOR_DINHEIRO,
    ORC.VALOR_CHEQUE,
    ORC.VALOR_CARTAO_DEBITO,
    ORC.VALOR_CARTAO_CREDITO,
    ORC.VALOR_COBRANCA,
    ORC.VALOR_FINANCEIRA,
    ORC.VALOR_CREDITO,
    ORC.VALOR_TOTAL
  into
    vEmpresaId,
    vClienteId,
    vPrevisaoEntrega,
    vValorDinheiro,
    vValorCheque,
    vValorCartaoDebito,
    vValorCartaoCredito,
    vValorCobranca,
    vValorFinanceira,
    vValorCredito,
    vValorTotal
  from
    ORCAMENTOS ORC
  where ORC.ORCAMENTO_ID = iORCAMENTO_ID;

  /* Criando os produtos que compôe o kit na tabela  ORCAMENTOS_ITENS */
  DISTRIBUIR_KIT_FECHAMENTO_PED(iORCAMENTO_ID);

  /* Adicionando na tabela temporária que faz a mágica dos agendamentos, simplismente o coração das entregas do sistema Hiva */
  /* Aproveitando o mesmo loop já para gravar os custos dos itens */
  for vItens in cItens loop
    if vItens.QUANTIDADE_RETIRAR_ATO > 0 then
      INSERIR_ITEM_GERAR_ENTREGA(  
        vItens.PRODUTO_ID,
        vItens.ITEM_ID,
        vItens.QUANTIDADE_RETIRAR_ATO,
        null,
        'RA'
      );
    end if;

    if vItens.QUANTIDADE_RETIRAR > 0 then
      INSERIR_ITEM_GERAR_ENTREGA(
        vItens.PRODUTO_ID,
        vItens.ITEM_ID,
        vItens.QUANTIDADE_RETIRAR,
        null,
        'RE'
      );
    end if;

    if vItens.QUANTIDADE_ENTREGAR > 0 then
      INSERIR_ITEM_GERAR_ENTREGA(
        vItens.PRODUTO_ID,
        vItens.ITEM_ID,
        vItens.QUANTIDADE_ENTREGAR,
        vPrevisaoEntrega,
        'EN'
      );
    end if;

    if vItens.QUANTIDADE_SEM_PREVISAO > 0 then
      INSERIR_ITEM_GERAR_ENTREGA(
        vItens.PRODUTO_ID,
        vItens.ITEM_ID,
        vItens.QUANTIDADE_SEM_PREVISAO,
        null,
        'SP'
      );
    end if;
  end loop;

  /* Gerando as retiradas no ato */
  GERAR_RETIRADAS_ATO(iORCAMENTO_ID, vEmpresaId);

  /* Gerando as retiradas pendentes */
  GERAR_PENDENCIA_RETIRADAS(iORCAMENTO_ID, vEmpresaId);

  /* Gerando as entregas pendentes */
  GERAR_PENDENCIA_ENTREGAS(iORCAMENTO_ID, vEmpresaId);

  /* Gerando os itens sem previsão */
  GERAR_PENDENCIA_SEM_PREVISAO(iORCAMENTO_ID);

  delete from ORCAMENTOS_ITENS_DEF_LOTES
  where ORCAMENTO_ID = iORCAMENTO_ID;

  if vValorCredito > 0 then
    /* vValorRestante := vValorTotal - (vValorDinheiro + vValorCheque + vValorCartaoDebito + vValorCartaoCredito + vValorCobranca + vValorFinanceira); */
    for xCreditos in cCreditos loop

     /* if xCreditos.VALOR_LIQUIDO >= vValorRestante then
        vValorBaixarCredito := vValorRestante;
        vValorRestante := 0;
      else
        vValorBaixarCredito := xCreditos.VALOR_LIQUIDO;
        vValorRestante := vValorRestante - vValorBaixarCredito;
      end if; */

      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iORCAMENTO_ID, 'ORC', xCreditos.VALOR_LIQUIDO, vBaixaCreditoPagarId, vBaixaCreditoPagarId);
      
    /*  if vValorRestante = 0 then
        exit;
      end if; */
    end loop;
  end if;

end FECHAR_PEDIDO;
/