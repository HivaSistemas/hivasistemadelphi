﻿create or replace procedure PODE_GERAR_DEVOLUCAO(iORCAMENTO_ID in number)
is
  vQtde number;
begin
  
  /* Verificando se existem notas com numero gerado e pendentes de emissão */
  select 
    count(*)
  into
    vQtde
  from
    NOTAS_FISCAIS
  where ORCAMENTO_BASE_ID = iORCAMENTO_ID
  and nvl(NUMERO_NOTA, 0) > 0
  and STATUS = 'N';
  
  if vQtde > 0 then
    ERRO('Existe nota fiscal para o orçamento ' || iORCAMENTO_ID || ' pendente de emissão, faça a emissão da nota primeiramente!');
  end if;
  
end PODE_GERAR_DEVOLUCAO;
/