create or replace procedure CANCELAR_DEVOLUCAO(iDEVOLUCAO_ID in number)
is
  vQtde number;
  vValorDevAcumuladoAberto  ORCAMENTOS.VALOR_DEV_ACUMULADO_ABERTO%type;
  vOrcamentoId              ORCAMENTOS.ORCAMENTO_ID%type;
  vValorLiquido             DEVOLUCOES.VALOR_LIQUIDO%type;
begin

  select
    count(*)
  into
    vQtde
  from
    NOTAS_FISCAIS
  where DEVOLUCAO_ID = iDEVOLUCAO_ID
  and (STATUS in('E', 'C') or NUMERO_NOTA is not null);

  if vQtde > 0 then
    ERRO('O cancelamento deste devolu��o n�o ser� permitido pois existe nota fiscal emitida, cancelada ou com numero gerado!');
  end if;

  delete from CONTAS_PAGAR
  where DEVOLUCAO_ID = iDEVOLUCAO_ID;

  delete from DEVOLUCOES_ITENS_RETIRADAS
  where DEVOLUCAO_ID = iDEVOLUCAO_ID;

  delete from DEVOLUCOES_ITENS_ENTREGAS
  where DEVOLUCAO_ID = iDEVOLUCAO_ID;
  
  delete from DEVOLUCOES_ITENS
  where DEVOLUCAO_ID = iDEVOLUCAO_ID;

  select
    ORC.VALOR_DEV_ACUMULADO_ABERTO,
    ORC.ORCAMENTO_ID,
    DEV.VALOR_LIQUIDO
  into
    vValorDevAcumuladoAberto,
    vOrcamentoId,
    vValorLiquido
  from ORCAMENTOS ORC

  join DEVOLUCOES DEV
  on DEV.ORCAMENTO_ID = ORC.ORCAMENTO_ID

  where DEV.DEVOLUCAO_ID = iDEVOLUCAO_ID;

  if vValorDevAcumuladoAberto > 0 then
    update ORCAMENTOS set
      VALOR_DEV_ACUMULADO_ABERTO = VALOR_DEV_ACUMULADO_ABERTO - vValorLiquido
    where ORCAMENTO_ID = vOrcamentoId;
  end if;

  delete from DEVOLUCOES
  where DEVOLUCAO_ID = iDEVOLUCAO_ID;

end CANCELAR_DEVOLUCAO;
/