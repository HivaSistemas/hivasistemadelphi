create or replace procedure CANCELAR_RECEBIMENTO_ACUMULADO(iACUMULADO_ID in number)
is
  vQtde number;
begin

  select
    count(*)
  into
    vQtde
  from
    NOTAS_FISCAIS
  where ACUMULADO_ID = iACUMULADO_ID
  and STATUS = 'N'
  and NUMERO_NOTA is not null;

  if vQtde > 0 then
    ERRO('Este acumulado gerou um numero de nota fiscal e ainda n�o foi emitido, por favor verifique antes de continuar!');
  end if;

  delete from CONTAS_RECEBER
  where ACUMULADO_ID = iACUMULADO_ID;

  delete from CONTAS_PAGAR
  where ACUMULADO_ID = iACUMULADO_ID;

  delete from NOTAS_FISCAIS
  where ACUMULADO_ID = iACUMULADO_ID
  and STATUS not in('E', 'C')
  and NUMERO_NOTA is null;
  
  update ACUMULADOS set
    STATUS = 'AR',
    TURNO_ID = null,
    DATA_HORA_RECEBIMENTO = null,
    USUARIO_RECEBIMENTO_ID = null
  where ACUMULADO_ID = iACUMULADO_ID;
  
  CANCELAR_FECHAMENTO_ACUMULADO(iACUMULADO_ID);  
  
end CANCELAR_RECEBIMENTO_ACUMULADO;
/