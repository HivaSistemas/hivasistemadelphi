﻿create or replace procedure GERAR_CREDITO_TROCO(
  iMOVIMENTO_ID in number,
  iTIPO_MOVIMENTO in string,
  iCLIENTE_ID in number,
  iVALOR_TROCO in number
)
is
  vClienteId   CONTAS_PAGAR.CADASTRO_ID%type;
  vEmpresaId   CONTAS_PAGAR.EMPRESA_ID%type;
  vPagarId     CONTAS_PAGAR.PAGAR_ID%type;
  vCobrancaId  PARAMETROS.TIPO_COBRANCA_GERACAO_CRED_ID%type;
  vIndice      CONTAS_PAGAR.INDICE_CONDICAO_PAGAMENTO%type;
begin

  if iTIPO_MOVIMENTO not in('ORC', 'ACU', 'BXR') then
    ERRO('O tipo de movimento só pode ser "ORC", "ACU" ou "BXR"!');
  end if;

  if iTIPO_MOVIMENTO = 'ORC' then
    select
      ORC.EMPRESA_ID,
      ORC.CLIENTE_ID,
      ORC.INDICE_CONDICAO_PAGAMENTO
    into
      vEmpresaId,
      vClienteId,
      vIndice
    from
      ORCAMENTOS ORC
    where ORC.ORCAMENTO_ID = iMOVIMENTO_ID;
  elsif iTIPO_MOVIMENTO = 'ACU' then
    select
      ACU.EMPRESA_ID,
      ACU.CLIENTE_ID,
      max(ORC.INDICE_CONDICAO_PAGAMENTO)
    into
      vEmpresaId,
      vClienteId,
      vIndice
    from
      ACUMULADOS ACU

    inner join ORCAMENTOS ORC
    on ACU.ACUMULADO_ID = ORC.ACUMULADO_ID

    where ACU.ACUMULADO_ID = iMOVIMENTO_ID
    group by
      ACU.EMPRESA_ID,
      ACU.CLIENTE_ID;
  else
    select
      EMPRESA_ID
    into
      vEmpresaId
    from
      CONTAS_RECEBER_BAIXAS

    where BAIXA_ID = iMOVIMENTO_ID;

    vIndice := 1;
    vClienteId := iCLIENTE_ID;
  end if;

  if vClienteId = SESSAO.PARAMETROS_GERAIS.CADASTRO_CONSUMIDOR_FINAL_ID then
    ERRO('Não é permitido gerar crédito para consumidor final!');
  end if;

  select
    TIPO_COBRANCA_GERACAO_CRED_ID
  into
    vCobrancaId
  from
    PARAMETROS;

  if vCobrancaId is null then
    ERRO('O tipo de cobrança para geração do crédito não foi parametrizado, faça a parametrização nos "Parâmetros"!');
  end if;

  select SEQ_PAGAR_ID.nextval
  into vPagarId
  from dual;

  insert into CONTAS_PAGAR(
    PAGAR_ID,
    CADASTRO_ID,
    EMPRESA_ID,
    DOCUMENTO,
    ORIGEM,
    ORCAMENTO_ID,
    ACUMULADO_ID,
    BAIXA_RECEBER_ORIGEM_ID,
    COBRANCA_ID,
    PORTADOR_ID,
    PLANO_FINANCEIRO_ID,
    DATA_VENCIMENTO,
    DATA_VENCIMENTO_ORIGINAL,
    VALOR_DOCUMENTO,
    STATUS,
    PARCELA,
    NUMERO_PARCELAS,
    INDICE_CONDICAO_PAGAMENTO
  )values(
    vPagarId,
    vClienteId,
    vEmpresaId,
    decode(iTIPO_MOVIMENTO, 'ORC', 'CVE', 'ACU', 'CAC', 'BXR', 'CBR') || '-' || NFORMAT(iMOVIMENTO_ID, 0), -- Documento
    decode(iTIPO_MOVIMENTO, 'ORC', 'CVE', 'ACU', 'CAC', 'BXR', 'CBR'),
    case when iTIPO_MOVIMENTO = 'ORC' then iMOVIMENTO_ID else null end,
    case when iTIPO_MOVIMENTO = 'ACU' then iMOVIMENTO_ID else null end,
    case when iTIPO_MOVIMENTO = 'BXR' then iMOVIMENTO_ID else null end,
    vCobrancaId,
    '9999',
    '1.002.001',
    trunc(sysdate),
    trunc(sysdate),
    iVALOR_TROCO,
    'A',
    1,
    1,
    vIndice
  );

end GERAR_CREDITO_TROCO;
/
