create or replace procedure SETAR_NFE_CANCELADA(
  iNOTA_FISCAL_ID                in positiven,
  iNUMERO_PROTOCOLO_CANCELAMENTO in string,
  iMOTIVO_CANCELAMENTO           in string,
  iDATA_HORA_PROTOCOLO_CANCELAM  in date
)
is
  vEmpresaId            EMPRESAS.EMPRESA_ID%type;
  vTipoMovimento        NOTAS_FISCAIS.TIPO_MOVIMENTO%type;
  vMovimentarEstOutras  NOTAS_FISCAIS.MOVIMENTAR_EST_OUTRAS_NOTAS%type;
  vNaturezaEntrada      char(1);

  cursor cItens is
  select
    PRODUTO_ID,
    QUANTIDADE
  from
    NOTAS_FISCAIS_ITENS
  where NOTA_FISCAL_ID = iNOTA_FISCAL_ID;

begin

  update NOTAS_FISCAIS set      
    PROTOCOLO_CANCELAMENTO_NFE = iNUMERO_PROTOCOLO_CANCELAMENTO,
    DATA_HORA_CANCELAMENTO_NFE = iDATA_HORA_PROTOCOLO_CANCELAM,    
    STATUS = 'C',
    MOTIVO_CANCELAMENTO_NFE = iMOTIVO_CANCELAMENTO
  where NOTA_FISCAL_ID = iNOTA_FISCAL_ID;

  select
    EMPRESA_ID,
    TIPO_MOVIMENTO,
    MOVIMENTAR_EST_OUTRAS_NOTAS,
    case when substr(CFOP_ID, 1, 1) in('1', '2', '3') then 'S' else 'N' end
  into
    vEmpresaId,
    vTipoMovimento,
    vMovimentarEstOutras,
    vNaturezaEntrada
  from NOTAS_FISCAIS
  where NOTA_FISCAL_ID = iNOTA_FISCAL_ID;

  for vItens in cItens loop
    if (vTipoMovimento <> 'OUT') or (vMovimentarEstOutras = 'S') then
      MOVIMENTAR_ESTOQUE_FISCAL(vEmpresaId, vItens.PRODUTO_ID, vItens.QUANTIDADE * case when vNaturezaEntrada = 'S' then 1 else -1 end);
    end if;
  end loop;

end SETAR_NFE_CANCELADA;
/