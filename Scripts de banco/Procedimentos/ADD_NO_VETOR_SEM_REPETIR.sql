create or replace procedure ADD_NO_VETOR_SEM_REPETIR(
  ioVETOR in out TIPOS.ArrayOfNumeros,
  iVALOR in number
)
is
  vAchou boolean;
begin

  vAchou := false;
  
  if ioVetor.count > 0 then
    for i in ioVETOR.first..ioVETOR.last loop
      if iVALOR = ioVetor(i) then
        vAchou := true;
        exit;
      end if;
    end loop;
  end if;
  
  if not vAchou then
    ioVETOR(ioVETOR.count + 1) := iVALOR;
  end if;

end ADD_NO_VETOR_SEM_REPETIR;
/