﻿create or replace procedure GERAR_NOSSO_NUMERO(
  iRECEBER_ID   in number,
  oNOSSO_NUMERO out string
)
is
  vPortadorId   CONTAS_RECEBER.PORTADOR_ID%type;
  vNossoNumero  CONTAS_RECEBER.NOSSO_NUMERO%type;
  vEmpresaId    CONTAS_RECEBER.EMPRESA_ID%type;
  vContaId      CONTAS.CONTA%type;
begin
  select
    PORTADOR_ID,
    NOSSO_NUMERO,
    EMPRESA_ID
  into
    vPortadorId,
    vNossoNumero,
    vEmpresaId
  from
    CONTAS_RECEBER
  where RECEBER_ID = iRECEBER_ID;

  if vNossoNumero is null then

    begin
      select
        CON.PROXIMO_NOSSO_NUMERO,
        CON.CONTA
      into
        vNossoNumero,
        vContaId
      from
        CONTAS CON

      inner join PORTADORES_CONTAS PCO
      on PCO.EMPRESA_ID = vEmpresaId
      and PCO.CONTA_ID = CON.CONTA

      where PCO.PORTADOR_ID = vPortadorId;
    exception
      when others then
        ERRO('Falha ao buscar parâmetros do portador e conta, verifique se todos os parâmetros estão corretos!' || sqlerrm);
    end;

    if vNossoNumero is null then
      ERRO('Falha ao buscar parâmetros do portador e conta, verifique se todos os parâmetros estão corretos!');
    end if;

    update CONTAS set
      PROXIMO_NOSSO_NUMERO = PROXIMO_NOSSO_NUMERO + 1
    where CONTA = vContaId;

    update CONTAS_RECEBER set
      NOSSO_NUMERO = vNossoNumero,
      DATA_EMISSAO = trunc(sysdate)
    where RECEBER_ID = iRECEBER_ID;
  end if;

  oNOSSO_NUMERO := vNossoNumero;

end GERAR_NOSSO_NUMERO;
/