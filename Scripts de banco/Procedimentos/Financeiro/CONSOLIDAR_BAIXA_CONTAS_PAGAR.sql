create or replace procedure CONSOLIDAR_BAIXA_CONTAS_PAGAR(iBAIXA_ID in number)
is
  
  vPagarId     CONTAS_PAGAR.PAGAR_ID%type;
  vCadastroId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;
  vEmpresaId   CONTAS_PAGAR_BAIXAS.EMPRESA_ID%type;

  vValorBaixarCredito   number default 0;
  vValorRestante        number default 0;

  vValorDinheiro        CONTAS_PAGAR_BAIXAS.VALOR_DINHEIRO%type;
  vValorCheque          CONTAS_PAGAR_BAIXAS.VALOR_CHEQUE%type;
  vValorCartao          CONTAS_PAGAR_BAIXAS.VALOR_CARTAO%type;
  vValorCobranca        CONTAS_PAGAR_BAIXAS.VALOR_COBRANCA%type;
  vValorCredito         CONTAS_PAGAR_BAIXAS.VALOR_CREDITO%type;
  vValorTotal           CONTAS_PAGAR_BAIXAS.VALOR_LIQUIDO%type;
  
  cursor cCheques is
  select
    CHQ.COBRANCA_ID,
    CHQ.DATA_VENCIMENTO,
    CHQ.PARCELA,
    CHQ.NUMERO_PARCELAS,    
    CHQ.VALOR_CHEQUE,
    CHQ.NUMERO_CHEQUE,
    CHQ.ITEM_ID
  from
    CONTAS_PAGAR_BAIXAS_PAGTOS_CHQ CHQ
  where CHQ.BAIXA_ID = iBAIXA_ID;
  
  
  cursor cCobrancas(pTIPO in string) is
  select
    PAG.COBRANCA_ID,
    PAG.ITEM_ID,
    PAG.VALOR,
    PAG.DATA_VENCIMENTO,
    PAG.PARCELA,
    PAG.NUMERO_PARCELAS,
    TIP.BOLETO_BANCARIO,
    PAG.NSU_TEF,
    TIP.PORTADOR_ID
  from
    CONTAS_PAGAR_BAIXAS_PAGAMENTOS PAG
  
  join TIPOS_COBRANCA TIP
  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    
  and TIP.FORMA_PAGAMENTO = pTIPO

  where PAG.BAIXA_ID = iBAIXA_ID  
  order by
		COBRANCA_ID,
		PARCELA,
		DATA_VENCIMENTO, 
		ITEM_ID;
    
    
  cursor cCartoes is
  select
    ITEM_ID,
    NSU_TEF
  from
    CONTAS_PAGAR_BAIXAS_PAGAMENTOS
  where BAIXA_ID = iBAIXA_ID
  and TIPO = 'CR';


  cursor cCreditos is
  select
    BAI.RECEBER_ID,
    REC.VALOR_DOCUMENTO
  from
    CONTAS_PAGAR_BAIXAS_CREDITOS BAI

  inner join CONTAS_RECEBER REC
  on BAI.RECEBER_ID = REC.RECEBER_ID

  where BAI.BAIXA_ID = iBAIXA_ID;

begin

  select
    EMPRESA_ID,
    CADASTRO_ID,
    VALOR_DINHEIRO,
    VALOR_CHEQUE,
    VALOR_CARTAO,
    VALOR_COBRANCA,
    VALOR_CREDITO,
    VALOR_LIQUIDO
  into
    vEmpresaId,
    vCadastroId,
    vValorDinheiro,
    vValorCheque,
    vValorCartao,
    vValorCobranca,
    vValorCredito,
    vValorTotal
  from
    CONTAS_PAGAR_BAIXAS
  where BAIXA_ID = iBAIXA_ID;

  for xCheques in cCheques loop      
    select SEQ_PAGAR_ID.nextval
    into vPagarId
    from dual;

    insert into CONTAS_PAGAR(
      PAGAR_ID,
      CADASTRO_ID,
      EMPRESA_ID,      
      COBRANCA_ID,
      PORTADOR_ID,
      PLANO_FINANCEIRO_ID,      
      DOCUMENTO,      
      NUMERO_CHEQUE,      
      DATA_VENCIMENTO,
      DATA_VENCIMENTO_ORIGINAL,
      VALOR_DOCUMENTO,
      STATUS,
      PARCELA,
      NUMERO_PARCELAS,
      ORIGEM,
      BAIXA_ORIGEM_ID
    )values(
      vPagarId,
      vCadastroId,
      vEmpresaId,
      xCheques.COBRANCA_ID,
      '9998',
      '1.001.002',      
      'BCP-' || iBAIXA_ID || xCheques.ITEM_ID || '/CHQ',      
      xCheques.NUMERO_CHEQUE,      
      xCheques.DATA_VENCIMENTO,
      xCheques.DATA_VENCIMENTO,
      xCheques.VALOR_CHEQUE,
      'A',
      xCheques.PARCELA,
      xCheques.NUMERO_PARCELAS,
      'BCP',
      iBAIXA_ID
    );    
  end loop;
  
  for xCobrancas in cCobrancas('COB') loop
    select SEQ_PAGAR_ID.nextval
    into vPagarId
    from dual;

    insert into CONTAS_PAGAR(
      PAGAR_ID,
      CADASTRO_ID,
      EMPRESA_ID,      
      COBRANCA_ID,
      PORTADOR_ID,
      PLANO_FINANCEIRO_ID,        
      DOCUMENTO,      
      DATA_VENCIMENTO,
      DATA_VENCIMENTO_ORIGINAL,
      VALOR_DOCUMENTO,
      STATUS,
      PARCELA,
      NUMERO_PARCELAS,
      ORIGEM,
      BAIXA_ORIGEM_ID
    )values(
      vPagarId,
      vCadastroId,
      vEmpresaId,      
      xCobrancas.COBRANCA_ID,
      xCobrancas.PORTADOR_ID,
      case when xCobrancas.BOLETO_BANCARIO = 'N' then '1.001.005' else '1.001.006' end,            
      'BCP-' || iBAIXA_ID || xCobrancas.ITEM_ID || case when xCobrancas.BOLETO_BANCARIO = 'S' then '/BOL' else '/COB' end,      
      xCobrancas.DATA_VENCIMENTO,
      xCobrancas.DATA_VENCIMENTO,
      xCobrancas.VALOR,
      'A',
      xCobrancas.PARCELA,
      xCobrancas.NUMERO_PARCELAS,
      'BCP',
      iBAIXA_ID
    );
  end loop;  
  
  if vValorCredito > 0 then
    vValorRestante := vValorTotal - (vValorDinheiro + vValorCheque + vValorCartao + vValorCobranca);
    for xCreditos in cCreditos loop
      if xCreditos.VALOR_DOCUMENTO >= vValorRestante then
        vValorBaixarCredito := vValorRestante;
        vValorRestante := 0;
      else
        vValorBaixarCredito := xCreditos.VALOR_DOCUMENTO;
        vValorRestante := vValorRestante - vValorBaixarCredito;
      end if;

      BAIXAR_CREDITO_RECEBER(xCreditos.RECEBER_ID, iBAIXA_ID, 'BCP', vValorBaixarCredito);

      if vValorRestante = 0 then
        exit;
      end if;
    end loop;
  end if;

end CONSOLIDAR_BAIXA_CONTAS_PAGAR;
/