﻿create or replace procedure ATUALIZAR_RETENCOES_CONTAS_REC(iMOVIMENTO_ID in number, iTIPO_MOVIMENTO in string)
is
  
  cursor cCartoesPedido is
  select
    COR.RECEBER_ID,
    round(COR.VALOR_DOCUMENTO * TPC.TAXA_RETENCAO_MES * 0.01, 2) as VALOR_RETENCAO
  from
    CONTAS_RECEBER COR

  inner join TIPOS_COBRANCA TPC
  on COR.COBRANCA_ID = TPC.COBRANCA_ID

  where COR.ORCAMENTO_ID = iMOVIMENTO_ID
  and TPC.FORMA_PAGAMENTO = 'CRT'
  and COR.STATUS = 'A';


  cursor cCartoesAcumulado is
  select
    COR.RECEBER_ID,
    round(COR.VALOR_DOCUMENTO * TPC.TAXA_RETENCAO_MES * 0.01, 2) as VALOR_RETENCAO
  from
    CONTAS_RECEBER COR

  inner join TIPOS_COBRANCA TPC
  on COR.COBRANCA_ID = TPC.COBRANCA_ID

  where COR.ACUMULADO_ID = iMOVIMENTO_ID
  and TPC.FORMA_PAGAMENTO = 'CRT'
  and COR.STATUS = 'A';


  cursor cCartoesBaixa is
  select
    COR.RECEBER_ID,
    round(COR.VALOR_DOCUMENTO * TPC.TAXA_RETENCAO_MES * 0.01, 2) as VALOR_RETENCAO
  from
    CONTAS_RECEBER COR

  inner join TIPOS_COBRANCA TPC
  on COR.COBRANCA_ID = TPC.COBRANCA_ID

  where COR.BAIXA_ORIGEM_ID = iMOVIMENTO_ID
  and TPC.FORMA_PAGAMENTO = 'CRT'
  and COR.STATUS = 'A';
  
   
begin

  if iTIPO_MOVIMENTO not in('ORC', 'BXR', 'ACU') then
    ERRO('O tipo de movimento só pode ser "ORC", "ACU" ou "BXR", entre em contato com a Altis!');
  end if;

  if iTIPO_MOVIMENTO = 'ORC' then
    for xCartoes in cCartoesPedido loop
      update CONTAS_RECEBER set
        VALOR_RETENCAO = xCartoes.VALOR_RETENCAO
      where RECEBER_ID = xCartoes.RECEBER_ID;
    end loop;
  elsif iTIPO_MOVIMENTO = 'ACU' then
    for xCartoes in cCartoesAcumulado loop
      update CONTAS_RECEBER set
        VALOR_RETENCAO = xCartoes.VALOR_RETENCAO
      where RECEBER_ID = xCartoes.RECEBER_ID;
    end loop;
  else
     for xBaixas in cCartoesBaixa loop     
       update CONTAS_RECEBER set
         VALOR_RETENCAO = xBaixas.VALOR_RETENCAO
       where RECEBER_ID = xBaixas.RECEBER_ID;     
     end loop;
  end if;

end ATUALIZAR_RETENCOES_CONTAS_REC;
/