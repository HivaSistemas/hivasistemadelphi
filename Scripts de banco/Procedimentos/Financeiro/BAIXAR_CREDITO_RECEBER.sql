﻿create or replace procedure BAIXAR_CREDITO_RECEBER(
  iRECEBER_ID in number, 
  iMOVIMENTO_ID in number, 
  iTIPO_MOVIMENTO in string, 
  iVALOR_UTILIZAR in number
)
is
  vReceberId     CONTAS_RECEBER.RECEBER_ID%type;
  vBaixaId       CONTAS_RECEBER_BAIXAS.BAIXA_ID%type;  
  vValorCredito  CONTAS_RECEBER.VALOR_DOCUMENTO%type;
  vCadastroId    CONTAS_RECEBER.CADASTRO_ID%type;
  vEmpresaId     CONTAS_RECEBER_BAIXAS.EMPRESA_ID%type;
  vObservacoes   CONTAS_PAGAR_BAIXAS.OBSERVACOES%type;

  vQtde          number;
  vStatus        CONTAS_RECEBER.STATUS%type;
  vTipoBaixa     CONTAS_PAGAR_BAIXAS.TIPO%type;
begin

  if iTIPO_MOVIMENTO not in('BCP') then
    ERRO('O tipo de baixa só pode ser ORC ou BCR!');
  end if;

  select
    EMPRESA_ID
  into
    vEmpresaId
  from
    CONTAS_PAGAR_BAIXAS
  where BAIXA_ID = iMOVIMENTO_ID;

  vTipoBaixa := 'ECR';
  vObservacoes := 'Baixa automática proveniente de uso na baixa de contas a pagar ' || NFORMAT(iMOVIMENTO_ID, 0);

  select
    VALOR_DOCUMENTO,
    CADASTRO_ID,
    STATUS
  into
    vValorCredito,
    vCadastroId,
    vStatus
  from
    CONTAS_RECEBER
  where RECEBER_ID = iRECEBER_ID;

  if vStatus = 'B' then
    ERRO('O crédito a pagar que está sendo utilizado já está baixado! Crédito: ' || iRECEBER_ID);
  end if;

  /* Verificando se já existe uma baixa, pode ser que esteja sendo utilizado mais de um crédito */
  select
    count(*)
  into
    vQtde
  from
    CONTAS_RECEBER_BAIXAS
  where BAIXA_PAGAR_ORIGEM_ID = iMOVIMENTO_ID
  and TIPO = vTipoBaixa;

  if vQtde > 0 then
    select
      BAIXA_ID
    into
      vBaixaId
    from
      CONTAS_RECEBER_BAIXAS
    where BAIXA_PAGAR_ORIGEM_ID = iMOVIMENTO_ID
    and TIPO = vTipoBaixa;

    update CONTAS_RECEBER_BAIXAS set
      VALOR_TITULOS = VALOR_TITULOS + iVALOR_UTILIZAR,
      VALOR_CREDITO = VALOR_CREDITO + iVALOR_UTILIZAR
    where BAIXA_ID = vBaixaId;
  else
    vBaixaId := SEQ_CONTAS_RECEBER_BAIXAS.nextval;

    insert into CONTAS_RECEBER_BAIXAS(
      BAIXA_ID,
      EMPRESA_ID,
      CADASTRO_ID,
      VALOR_TITULOS,
      VALOR_CREDITO,
      OBSERVACOES,
      DATA_PAGAMENTO,
      TIPO,
      BAIXA_PAGAR_ORIGEM_ID,
      RECEBER_CAIXA,
      RECEBIDO
    )values(
      vBaixaId,
      vEmpresaId,
      vCadastroId,
      iVALOR_UTILIZAR,
      iVALOR_UTILIZAR,
      vObservacoes,
      trunc(sysdate),
      vTipoBaixa,
      iMOVIMENTO_ID,
      'N',
      'S'
    );
  end if;

  insert into CONTAS_RECEBER_BAIXAS_ITENS(
    BAIXA_ID,
    RECEBER_ID,
    VALOR_DINHEIRO
  )values(
    vBaixaId,
    iRECEBER_ID,
    iVALOR_UTILIZAR
  );
  
  /* Se ainda for sobrar um restante, gerando um novo crédigo a pagar */
  if iVALOR_UTILIZAR < vValorCredito then
    vReceberId := SEQ_RECEBER_ID.nextval;
  
    insert into CONTAS_RECEBER(
      RECEBER_ID,
      CADASTRO_ID,
      EMPRESA_ID,
      COBRANCA_ID,
      PORTADOR_ID,      
      DOCUMENTO,
      PLANO_FINANCEIRO_ID,
      CENTRO_CUSTO_ID,
      DATA_VENCIMENTO,
      DATA_VENCIMENTO_ORIGINAL,
      VALOR_DOCUMENTO,
      STATUS,
      PARCELA,
      NUMERO_PARCELAS,
      ORIGEM,
      BAIXA_ORIGEM_ID
    )
    select
      vReceberId,
      CADASTRO_ID,
      EMPRESA_ID,
      COBRANCA_ID,
      PORTADOR_ID,
      'CRE' || vReceberId,      
      PLANO_FINANCEIRO_ID,
      CENTRO_CUSTO_ID,
      DATA_VENCIMENTO,
      DATA_VENCIMENTO_ORIGINAL,
      vValorCredito - iVALOR_UTILIZAR, -- Apenas o saldo restante
      'A',
      PARCELA,
      NUMERO_PARCELAS,
      'BXR',
      vBaixaId
    from
      CONTAS_RECEBER
    where RECEBER_ID = iRECEBER_ID;
  end if;

end BAIXAR_CREDITO_RECEBER;
/