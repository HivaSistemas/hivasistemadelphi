﻿create or replace procedure BAIXAR_CREDITO_PAGAR(
  iPAGAR_ID       in number,
  iMOVIMENTO_ID   in number,
  iTIPO_MOVIMENTO in string,
  iVALOR_UTILIZAR in number,
  iBAIXA_ID       in number, -- Este código vem deste próprio procedimento
  oBAIXA_ID       out number
)
is
  vPagarId       CONTAS_PAGAR.PAGAR_ID%type;
  vBaixaId       CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;
  vValorCredito  CONTAS_PAGAR.VALOR_DOCUMENTO%type;
  vCadastroId    CONTAS_PAGAR.CADASTRO_ID%type;
  vEmpresaId     ORCAMENTOS.EMPRESA_ID%type;
  vObservacoes   CONTAS_PAGAR_BAIXAS.OBSERVACOES%type;

  vQtde          number;
  vStatus        CONTAS_PAGAR.STATUS%type;
  vStatusPedido  ORCAMENTOS.STATUS%type;
  vTipoBaixa     CONTAS_PAGAR_BAIXAS.TIPO%type;
  vTurnoId       CONTAS_RECEBER_BAIXAS.TURNO_ID%type;
begin

  if iTIPO_MOVIMENTO not in('ORC', 'BCR', 'ACU') then
    ERRO('O tipo de baixa só pode ser "ORC", "ACU" ou "BCR"!');
  end if;

  if iTIPO_MOVIMENTO = 'ORC' then
    select
      EMPRESA_ID,
      STATUS
    into
      vEmpresaId,
      vStatusPedido
    from
      ORCAMENTOS
    where ORCAMENTO_ID = iMOVIMENTO_ID;

    vTipoBaixa := 'FPE';
    if vStatusPedido = 'VE' then
      vObservacoes := 'Baixa automática proveniente de uso no recebimento do pedido(Receber na entrega) ' || NFORMAT(iMOVIMENTO_ID, 0);
    else
      vObservacoes := 'Baixa automática proveniente de uso no fechamento do pedido ' || NFORMAT(iMOVIMENTO_ID, 0);
    end if;
  elsif iTIPO_MOVIMENTO = 'ACU' then
    select
      EMPRESA_ID
    into
      vEmpresaId
    from
      ACUMULADOS
    where ACUMULADO_ID = iMOVIMENTO_ID;

    vTipoBaixa := 'FAC';
    vObservacoes := 'Baixa automática proveniente de uso no fechamento do acumulado ' || NFORMAT(iMOVIMENTO_ID, 0);
  elsif iTIPO_MOVIMENTO = 'BCR' then
    select
      EMPRESA_ID,
      TURNO_ID
    into
      vEmpresaId,
      vTurnoId
    from
      CONTAS_RECEBER_BAIXAS
    where BAIXA_ID = iMOVIMENTO_ID;

    vTipoBaixa := 'ECP';
    vObservacoes := 'Baixa automática proveniente de uso na baixa de contas a receber ' || NFORMAT(iMOVIMENTO_ID, 0);
  end if;

  select
    VALOR_DOCUMENTO - VALOR_DESCONTO - VALOR_ADIANTADO,
    CADASTRO_ID,
    STATUS
  into
    vValorCredito,
    vCadastroId,
    vStatus
  from
    CONTAS_PAGAR
  where PAGAR_ID = iPAGAR_ID;

  if vStatus = 'B' then
    ERRO('O crédito a pagar que está sendo utilizado já está baixado! Crédito: ' || iPAGAR_ID);
  end if;

  /* Verificando se já existe uma baixa, pode ser que esteja sendo utilizado mais de um crédito */
  if iBAIXA_ID is not null then
    vBaixaId := iBAIXA_ID;

    update CONTAS_PAGAR_BAIXAS set
      VALOR_TITULOS = VALOR_TITULOS + iVALOR_UTILIZAR,
      VALOR_CREDITO = VALOR_CREDITO + case when vTipoBaixa = 'ECP' then iVALOR_UTILIZAR else 0 end,
      VALOR_TROCA = VALOR_TROCA + case when vTipoBaixa in('FPE', 'FAC') then iVALOR_UTILIZAR else 0 end
    where BAIXA_ID = vBaixaId;
  else
    vBaixaId := SEQ_CONTAS_PAGAR_BAIXAS.nextval;

    insert into CONTAS_PAGAR_BAIXAS(
      BAIXA_ID,
      EMPRESA_ID,
      CADASTRO_ID,
      VALOR_TITULOS,
      VALOR_CREDITO,
      VALOR_TROCA,
      OBSERVACOES,
      DATA_PAGAMENTO,
      TIPO,
      BAIXA_RECEBER_ORIGEM_ID,
      ACUMULADO_ID,
      ORCAMENTO_ID,
      TURNO_ID
    )values(
      vBaixaId,
      vEmpresaId,
      vCadastroId,
      iVALOR_UTILIZAR,
      case when vTipoBaixa = 'ECP' then iVALOR_UTILIZAR else 0 end,
      case when vTipoBaixa in('FPE', 'FAC') then iVALOR_UTILIZAR else 0 end,
      vObservacoes,
      trunc(sysdate),
      vTipoBaixa,
      case when vTipoBaixa = 'ECP' then iMOVIMENTO_ID else null end,
      case when vTipoBaixa = 'FAC' then iMOVIMENTO_ID else null end,
      case when vTipoBaixa = 'FPE' then iMOVIMENTO_ID else null end,
      vTurnoId
    );
  end if;

  insert into CONTAS_PAGAR_BAIXAS_ITENS(
    BAIXA_ID,
    PAGAR_ID,
    VALOR_DINHEIRO,
    VALOR_TROCA
  )values(
    vBaixaId,
    iPAGAR_ID,
    case when vTipoBaixa = 'ECP' then iVALOR_UTILIZAR else 0 end,
    case when vTipoBaixa in('FPE', 'FAC') then iVALOR_UTILIZAR else 0 end
  );

  /* Se ainda for sobrar um restante, gerando um novo crédigo a pagar */
  if iVALOR_UTILIZAR < vValorCredito then
    vPagarId := SEQ_PAGAR_ID.nextval;

    insert into CONTAS_PAGAR(
      PAGAR_ID,
      CADASTRO_ID,
      EMPRESA_ID,
      COBRANCA_ID,
      PORTADOR_ID,      
      DOCUMENTO,
      NUMERO_CHEQUE,      
      PLANO_FINANCEIRO_ID,
      CENTRO_CUSTO_ID,      
      DATA_VENCIMENTO,
      DATA_VENCIMENTO_ORIGINAL,
      VALOR_DOCUMENTO,
      STATUS,
      PARCELA,
      NUMERO_PARCELAS,
      ORIGEM,
      BAIXA_ORIGEM_ID
    )
    select
      vPagarId,
      CADASTRO_ID,
      EMPRESA_ID,
      COBRANCA_ID,
      PORTADOR_ID,      
      'CRE' || vPagarId,
      NUMERO_CHEQUE,      
      PLANO_FINANCEIRO_ID,
      CENTRO_CUSTO_ID,      
      DATA_VENCIMENTO,
      DATA_VENCIMENTO_ORIGINAL,
      vValorCredito - iVALOR_UTILIZAR, -- Apenas o saldo restante
      'A',
      PARCELA,
      NUMERO_PARCELAS,
      'BCP',
      vBaixaId
    from
      CONTAS_PAGAR
    where PAGAR_ID = iPAGAR_ID;
  end if;

  oBAIXA_ID := vBaixaId;

end BAIXAR_CREDITO_PAGAR;
/