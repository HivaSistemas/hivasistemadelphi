﻿create or replace procedure CANCELAR_BAIXA_CONTAS_PAGAR(iBAIXA_ID in number)
is

  vMensagem         varchar2(400);
  vTurnoBaixado     char(1) default 'N';
  vTipo             CONTAS_PAGAR_BAIXAS.TIPO%type;

  cursor cTitulosOutrasBaixas is
  select
    COP.PAGAR_ID,
    BAX.BAIXA_ID,
    COP.STATUS
  from
    CONTAS_PAGAR COP
    
  inner join CONTAS_PAGAR_BAIXAS_ITENS BAX
  on COP.PAGAR_ID = BAX.PAGAR_ID
  
  where COP.BAIXA_ORIGEM_ID = iBAIXA_ID
  
  order by
    BAX.BAIXA_ID,
    COP.PAGAR_ID;
    
begin
  select
    TIPO
  into
    vTipo
  from
    CONTAS_PAGAR_BAIXAS
  where BAIXA_ID = iBAIXA_ID;

  if vTipo in('FAC', 'FPE') and SESSAO.ROTINA not in('CANCELAR_FECHAMENTO_PEDIDO', 'CANCELAR_RECEBIMENTO_PEDIDO', 'CANCELAR_RECEBIMENTO_ACUMULADO', 'CANCELAR_FECHAMENTO_ACUMULADO') then
    ERRO('Não é permitido cancelar baixa proveniente de fechamento de pedido ou fechamento de acumulado!');
  end if;


  -- Verificando se existe nos títulos que foram gerados pela baixa que está para ser estornada alguma baixa  
  for xTitulosOutrasBaixas in cTitulosOutrasBaixas loop
    if xTitulosOutrasBaixas.STATUS = 'B' and xTitulosOutrasBaixas.BAIXA_ID <> iBAIXA_ID then
      vMensagem := vMensagem ||
        'Cód. baixa.: ' || xTitulosOutrasBaixas.BAIXA_ID || chr(13) || chr(10) ||
        'Cód. título: ' || xTitulosOutrasBaixas.PAGAR_ID || chr(13) || chr(10);
    end if;
  end loop;
  
  if vMensagem is not null then
    ERRO('Existe(m) título(s) gerado(s) por esta baixa ao qual já foram baixados, é necessário fazer o estorno destes antes de concluir esta operação! ' || chr(13) || chr(10) || vMensagem);
  end if;

  select
    case when count(*) > 0 then 'S' else 'N' end
  into
    vTurnoBaixado
  from
    CONTAS_PAGAR_BAIXAS CON

  inner join TURNOS_CAIXA TUR
  on CON.TURNO_ID = TUR.TURNO_ID

  where CON.BAIXA_ID = iBAIXA_ID
  and TUR.STATUS = 'B';

  if vTurnoBaixado = 'S' then
    ERRO('A baixa que está sendo cancelada pertence a um turno baixado, cancelamento não permitido!');
  end if;

  delete from CONTAS_PAGAR_BAIXAS_CREDITOS
  where BAIXA_ID = iBAIXA_ID;

  /* Apagando os logs dos contas a pagar */
  delete from LOGS_CONTAS_PAGAR
  where PAGAR_ID in(
    select
      PAGAR_ID
    from
      CONTAS_PAGAR
    where BAIXA_ORIGEM_ID = iBAIXA_ID
  );

  /* Apagando os financeiros que haviam sido baixados */
  delete from CONTAS_PAGAR_BAIXAS_ITENS
  where BAIXA_ID = iBAIXA_ID;

  /* Apagando os logs dos contas a pagar */
  delete from LOGS_CONTAS_PAGAR
  where PAGAR_ID in(
    select
      PAGAR_ID
    from
      CONTAS_PAGAR
    where BAIXA_ORIGEM_ID = iBAIXA_ID
  );

  /* Apagando os títulos gerados pela baixa */
  delete from CONTAS_PAGAR
  where BAIXA_ORIGEM_ID = iBAIXA_ID;

  /* Apagando os logs dos contas a receber */
  delete from LOGS_CONTAS_RECEBER
  where RECEBER_ID in(
    select
      RECEBER_ID
    from
      CONTAS_RECEBER
    where BAIXA_PAGAR_ORIGEM_ID = iBAIXA_ID
  );
  
  /* Apagando os créditos gerados a receber */
  delete from CONTAS_RECEBER
  where BAIXA_PAGAR_ORIGEM_ID = iBAIXA_ID;
  
  /* Apagando as formas de pagamento */
  delete from CONTAS_PAGAR_BAIXAS_PAGAMENTOS
  where BAIXA_ID = iBAIXA_ID;
  
  /* Apagando as formas de pagamento */
  delete from CONTAS_PAGAR_BAIXAS_PAGTOS_DIN
  where BAIXA_ID = iBAIXA_ID;
  
  /* Apagando as formas de pagamento */
  delete from CONTAS_PAGAR_BAIXAS_PAGTOS_CHQ
  where BAIXA_ID = iBAIXA_ID;  
  
  /* Apagando a baixa */
  delete from CONTAS_PAGAR_BAIXAS
  where BAIXA_ID = iBAIXA_ID;
		
end CANCELAR_BAIXA_CONTAS_PAGAR;
/