create or replace procedure LANCAR_ADIANT_CONTAS_PAGAR(iBAIXA_ID in number, oPAGAR_ID out number)
is
  vEmpresaId          CONTAS_PAGAR_BAIXAS.EMPRESA_ID%type;
  vClienteId          CONTAS_PAGAR_BAIXAS.CADASTRO_ID%type;
  vValorBaixa         CONTAS_PAGAR_BAIXAS.VALOR_LIQUIDO%type;
  vPagarAdiantadoId   CONTAS_PAGAR_BAIXAS.PAGAR_ADIANTADO_ID%type;
begin

  select
    EMPRESA_ID,
    CADASTRO_ID,
    VALOR_LIQUIDO,
    PAGAR_ADIANTADO_ID
  into
    vEmpresaId,
    vClienteId,
    vValorBaixa,
    vPagarAdiantadoId
  from
    CONTAS_PAGAR_BAIXAS
  where BAIXA_ID = iBAIXA_ID
  and TIPO = 'ADI';
  
  update CONTAS_PAGAR set
    VALOR_ADIANTADO = VALOR_ADIANTADO + vValorBaixa
  where PAGAR_ID = vPagarAdiantadoId;
  
  insert into CONTAS_PAGAR(
    PAGAR_ID,
    CADASTRO_ID,
    EMPRESA_ID,      
    COBRANCA_ID,
    PORTADOR_ID,
    PLANO_FINANCEIRO_ID,      
    DOCUMENTO,
    DATA_EMISSAO,
    DATA_VENCIMENTO,
    DATA_VENCIMENTO_ORIGINAL,
    VALOR_DOCUMENTO,
    STATUS,
    PARCELA,
    NUMERO_PARCELAS,
    ORIGEM,
    BAIXA_ORIGEM_ID
  )values(
    SEQ_PAGAR_ID.nextval,
    vClienteId,
    vEmpresaId,
    SESSAO.PARAMETROS_GERAIS.TIPO_COB_ADIANTAMENTO_FIN_ID,
    '9999',
    '2.012.001',
    'BXP-' || iBAIXA_ID || '/ADI',
    trunc(sysdate),
    trunc(sysdate),
    trunc(sysdate),
    vValorBaixa,
    'A',
    1,
    1,
    'BCP',
    iBAIXA_ID
  );

  oPAGAR_ID := SEQ_PAGAR_ID.currval;

end LANCAR_ADIANT_CONTAS_PAGAR;
/