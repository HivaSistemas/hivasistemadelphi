create or replace procedure MOVIMENTAR_SALDO_CONTA( iCONTA_ID in string, iVALOR in number, iTIPO_MOVIMENTO in string )
is
begin

  if iTIPO_MOVIMENTO not in('A', 'C', 'T') then
    ERRO('O tipo de movimento de saldo s� pode ser "A", "C" ou "T"!');
  end if;

  if iTIPO_MOVIMENTO = 'A' then
    update CONTAS set
      SALDO_ATUAL = SALDO_ATUAL + iVALOR
    where CONTA = iCONTA_ID;
  elsif iTIPO_MOVIMENTO = 'C' then
    update CONTAS set
      SALDO_CONCILIADO_ATUAL = SALDO_CONCILIADO_ATUAL + iVALOR
    where CONTA = iCONTA_ID;
  else
    update CONTAS set
      SALDO_ATUAL = SALDO_ATUAL + iVALOR,
      SALDO_CONCILIADO_ATUAL = SALDO_CONCILIADO_ATUAL + iVALOR
    where CONTA = iCONTA_ID;
  end if;

end MOVIMENTAR_SALDO_CONTA;
/