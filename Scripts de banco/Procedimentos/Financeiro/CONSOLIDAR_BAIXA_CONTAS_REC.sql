create or replace procedure CONSOLIDAR_BAIXA_CONTAS_REC(iBAIXA_RECEBER_ID in number, iTURNO_ID in number)
is
  vReceberId     CONTAS_RECEBER.RECEBER_ID%type;
  vCadastroId    CONTAS_RECEBER_BAIXAS.BAIXA_ID%type;
  vEmpresaId     CONTAS_RECEBER_BAIXAS.EMPRESA_ID%type;
  vValorRetencao CONTAS_RECEBER_BAIXAS.VALOR_RETENCAO%type;

  vValorBaixarCredito   number default 0;
  vValorRestante        number default 0;

  vBaixaCreditoPagarId  CONTAS_PAGAR_BAIXAS.BAIXA_ID%type;
  vValorDinheiro        CONTAS_RECEBER_BAIXAS.VALOR_DINHEIRO%type;
  vValorCheque          CONTAS_RECEBER_BAIXAS.VALOR_CHEQUE%type;
  vValorCartaoDeb       CONTAS_RECEBER_BAIXAS.VALOR_CARTAO_DEBITO%type;
  vValorCartaoCred      CONTAS_RECEBER_BAIXAS.VALOR_CARTAO_CREDITO%type;
  vValorCobranca        CONTAS_RECEBER_BAIXAS.VALOR_COBRANCA%type;
  vValorCredito         CONTAS_RECEBER_BAIXAS.VALOR_CREDITO%type;
  vValorTotal           CONTAS_RECEBER_BAIXAS.VALOR_LIQUIDO%type;

  cursor cCheques is
  select
    CHQ.COBRANCA_ID,
    CHQ.DATA_VENCIMENTO,
    CHQ.PARCELA,
    CHQ.NUMERO_PARCELAS,
    CHQ.BANCO,
    CHQ.AGENCIA,     
    CHQ.CONTA_CORRENTE,
    CHQ.NOME_EMITENTE,
    CHQ.TELEFONE_EMITENTE,
    CHQ.VALOR_CHEQUE,
    CHQ.NUMERO_CHEQUE,
    CHQ.ITEM_ID
  from
    CONTAS_REC_BAIXAS_PAGTOS_CHQ CHQ
  where CHQ.BAIXA_ID = iBAIXA_RECEBER_ID;


  cursor cCobrancas(pTIPO in string) is
  select
    PAG.COBRANCA_ID,
    PAG.ITEM_ID,
    PAG.VALOR,
    PAG.DATA_VENCIMENTO,
    PAG.PARCELA,
    PAG.NUMERO_PARCELAS,
    TIP.BOLETO_BANCARIO,
    PAG.NSU_TEF,
    TIP.PORTADOR_ID
  from
    CONTAS_REC_BAIXAS_PAGAMENTOS PAG

  join TIPOS_COBRANCA TIP
  on PAG.COBRANCA_ID = TIP.COBRANCA_ID    
  and TIP.FORMA_PAGAMENTO = pTIPO

  where PAG.BAIXA_ID = iBAIXA_RECEBER_ID  
  order by 
    COBRANCA_ID,
    PARCELA,
    DATA_VENCIMENTO, 
    ITEM_ID;


  cursor cCartoes is
  select
    ITEM_ID,
    NSU_TEF,
    NUMERO_CARTAO,
    CODIGO_AUTORIZACAO
  from
    CONTAS_REC_BAIXAS_PAGAMENTOS
  where BAIXA_ID = iBAIXA_RECEBER_ID
  and TIPO = 'CR';

  cursor cCreditos is
  select
    BAI.PAGAR_ID,
    (PAG.VALOR_DOCUMENTO - PAG.VALOR_DESCONTO - PAG.VALOR_ADIANTADO) AS VALOR_DOCUMENTO
  from
    CONTAS_REC_BAIXAS_CREDITOS BAI

  inner join CONTAS_PAGAR PAG
  on BAI.PAGAR_ID = PAG.PAGAR_ID

  where BAI.BAIXA_ID = iBAIXA_RECEBER_ID;

begin

  select
    EMPRESA_ID,
    CADASTRO_ID,
    VALOR_DINHEIRO + VALOR_PIX,
    VALOR_CHEQUE,
    VALOR_CARTAO_DEBITO,
    VALOR_CARTAO_CREDITO,
    VALOR_COBRANCA,
    VALOR_CREDITO,
    VALOR_LIQUIDO,
    VALOR_RETENCAO
  into
    vEmpresaId,
    vCadastroId,
    vValorDinheiro,
    vValorCheque,
    vValorCartaoDeb,
    vValorCartaoCred,
    vValorCobranca,
    vValorCredito,
    vValorTotal,
    vValorRetencao
  from
    CONTAS_RECEBER_BAIXAS
  where BAIXA_ID = iBAIXA_RECEBER_ID;

  for xCheques in cCheques loop
    select SEQ_RECEBER_ID.nextval
    into vReceberId
    from dual;

    /*Se n�o tiver turno informado n�o grava 0 no TURNO_ID que estava fazendo ocorrer erro*/
    if iTURNO_ID > 0 then
      insert into CONTAS_RECEBER(
        RECEBER_ID,
        CADASTRO_ID,
        EMPRESA_ID,
        COBRANCA_ID,
        PORTADOR_ID,
        PLANO_FINANCEIRO_ID,
        DOCUMENTO,
        BANCO,
        AGENCIA,
        CONTA_CORRENTE,
        NUMERO_CHEQUE,
        NOME_EMITENTE,
        TELEFONE_EMITENTE,
        DATA_EMISSAO,
        DATA_VENCIMENTO,
        DATA_VENCIMENTO_ORIGINAL,
        VALOR_DOCUMENTO,
        STATUS,
        PARCELA,
        NUMERO_PARCELAS,
        ORIGEM,
        BAIXA_ORIGEM_ID,
        TURNO_ID
      )values(
        vReceberId,
        vCadastroId,
        vEmpresaId,
        xCheques.COBRANCA_ID,
        '9998',
        '1.001.002',
        'BXR-' || iBAIXA_RECEBER_ID || xCheques.ITEM_ID || '/CHQ',
        xCheques.BANCO,
        xCheques.AGENCIA,
        xCheques.CONTA_CORRENTE,
        xCheques.NUMERO_CHEQUE,
        xCheques.NOME_EMITENTE,
        xCheques.TELEFONE_EMITENTE,
        trunc(sysdate),
        xCheques.DATA_VENCIMENTO,
        xCheques.DATA_VENCIMENTO,
        xCheques.VALOR_CHEQUE,
        'A',
        xCheques.PARCELA,
        xCheques.NUMERO_PARCELAS,
        'BXR',
        iBAIXA_RECEBER_ID,
        iTURNO_ID
      );
    else
      insert into CONTAS_RECEBER(
        RECEBER_ID,
        CADASTRO_ID,
        EMPRESA_ID,
        COBRANCA_ID,
        PORTADOR_ID,
        PLANO_FINANCEIRO_ID,
        DOCUMENTO,
        BANCO,
        AGENCIA,
        CONTA_CORRENTE,
        NUMERO_CHEQUE,
        NOME_EMITENTE,
        TELEFONE_EMITENTE,
        DATA_EMISSAO,
        DATA_VENCIMENTO,
        DATA_VENCIMENTO_ORIGINAL,
        VALOR_DOCUMENTO,
        STATUS,
        PARCELA,
        NUMERO_PARCELAS,
        ORIGEM,
        BAIXA_ORIGEM_ID
      )values(
        vReceberId,
        vCadastroId,
        vEmpresaId,
        xCheques.COBRANCA_ID,
        '9998',
        '1.001.002',
        'BXR-' || iBAIXA_RECEBER_ID || xCheques.ITEM_ID || '/CHQ',
        xCheques.BANCO,
        xCheques.AGENCIA,
        xCheques.CONTA_CORRENTE,
        xCheques.NUMERO_CHEQUE,
        xCheques.NOME_EMITENTE,
        xCheques.TELEFONE_EMITENTE,
        trunc(sysdate),
        xCheques.DATA_VENCIMENTO,
        xCheques.DATA_VENCIMENTO,
        xCheques.VALOR_CHEQUE,
        'A',
        xCheques.PARCELA,
        xCheques.NUMERO_PARCELAS,
        'BXR',
        iBAIXA_RECEBER_ID
      );
    end if;
  end loop;

  for xCobrancas in cCobrancas('COB') loop
    select SEQ_RECEBER_ID.nextval
    into vReceberId
    from dual;

    insert into CONTAS_RECEBER(
      RECEBER_ID,
      CADASTRO_ID,
      EMPRESA_ID,      
      COBRANCA_ID,
      PORTADOR_ID,
      PLANO_FINANCEIRO_ID,        
      DOCUMENTO,
      DATA_EMISSAO,
      DATA_VENCIMENTO,
      DATA_VENCIMENTO_ORIGINAL,
      VALOR_DOCUMENTO,
      STATUS,
      PARCELA,
      NUMERO_PARCELAS,
      ORIGEM,
      BAIXA_ORIGEM_ID
    )values(
      vReceberId,
      vCadastroId,
      vEmpresaId,
      xCobrancas.COBRANCA_ID,
      xCobrancas.PORTADOR_ID,
      case when xCobrancas.BOLETO_BANCARIO = 'N' then '1.001.005' else '1.001.006' end,            
      'BXR-' || iBAIXA_RECEBER_ID || xCobrancas.ITEM_ID || case when xCobrancas.BOLETO_BANCARIO = 'S' then '/BOL' else '/COB' end,
      trunc(sysdate),
      xCobrancas.DATA_VENCIMENTO,
      xCobrancas.DATA_VENCIMENTO,
      xCobrancas.VALOR,
      'A',
      xCobrancas.PARCELA,
      xCobrancas.NUMERO_PARCELAS,
      'BXR',
      iBAIXA_RECEBER_ID
    );
  end loop;

  for xCartoes in cCartoes loop
    GERAR_CARTOES_RECEBER_BAIXA(iBAIXA_RECEBER_ID, xCartoes.ITEM_ID, xCartoes.NSU_TEF, xCartoes.CODIGO_AUTORIZACAO, xCartoes.NUMERO_CARTAO);
  end loop;

  ATUALIZAR_RETENCOES_CONTAS_REC(iBAIXA_RECEBER_ID, 'BXR');

  if vValorCredito > 0 then
    vValorRestante := vValorTotal - (vValorDinheiro + vValorCheque + vValorCartaoDeb + vValorCartaoCred + vValorCobranca);
    for xCreditos in cCreditos loop
      if xCreditos.VALOR_DOCUMENTO >= vValorRestante then
        vValorBaixarCredito := vValorRestante;
        vValorRestante := 0;
      else
        vValorBaixarCredito := xCreditos.VALOR_DOCUMENTO;
        vValorRestante := vValorRestante - vValorBaixarCredito;
      end if;

      BAIXAR_CREDITO_PAGAR(xCreditos.PAGAR_ID, iBAIXA_RECEBER_ID, 'BCR', vValorBaixarCredito, vBaixaCreditoPagarId, vBaixaCreditoPagarId);

      if vValorRestante = 0 then
        exit;
      end if;
    end loop;
  end if;

end CONSOLIDAR_BAIXA_CONTAS_REC;
