create or replace procedure LANCAR_ADIANTAMENTO_CONTAS_REC(iBAIXA_ID in number, oRECEBER_ID out number)
is
  vEmpresaId          CONTAS_RECEBER_BAIXAS.EMPRESA_ID%type;
  vClienteId          CONTAS_RECEBER_BAIXAS.CADASTRO_ID%type;
  vValorBaixa         CONTAS_RECEBER_BAIXAS.VALOR_LIQUIDO%type;
  vReceberAdiantadoId CONTAS_RECEBER_BAIXAS.RECEBER_ADIANTADO_ID%type;
begin

  select
    EMPRESA_ID,
    CADASTRO_ID,
    VALOR_LIQUIDO,
    RECEBER_ADIANTADO_ID
  into
    vEmpresaId,
    vClienteId,
    vValorBaixa,
    vReceberAdiantadoId
  from
    CONTAS_RECEBER_BAIXAS
  where BAIXA_ID = iBAIXA_ID
  and TIPO = 'ADI';
  
  update CONTAS_RECEBER set
    VALOR_ADIANTADO = VALOR_ADIANTADO + vValorBaixa
  where RECEBER_ID = vReceberAdiantadoId;
  
  insert into CONTAS_RECEBER(
    RECEBER_ID,
    CADASTRO_ID,
    EMPRESA_ID,      
    COBRANCA_ID,
    PORTADOR_ID,
    PLANO_FINANCEIRO_ID,      
    DOCUMENTO,
    DATA_EMISSAO,
    DATA_VENCIMENTO,
    DATA_VENCIMENTO_ORIGINAL,
    VALOR_DOCUMENTO,
    STATUS,
    PARCELA,
    NUMERO_PARCELAS,
    ORIGEM,
    BAIXA_ORIGEM_ID
  )values(
    SEQ_RECEBER_ID.nextval,
    vClienteId,
    vEmpresaId,
    SESSAO.PARAMETROS_GERAIS.TIPO_COB_ADIANTAMENTO_FIN_ID,
    '9999',
    '1.002.001',
    'BXR-' || iBAIXA_ID || '/ADI',
    trunc(sysdate),
    trunc(sysdate),
    trunc(sysdate),
    vValorBaixa,
    'A',
    1,
    1,
    'BXR',
    iBAIXA_ID
  );

  oRECEBER_ID := SEQ_RECEBER_ID.currval;

end LANCAR_ADIANTAMENTO_CONTAS_REC;
/