create or replace procedure CANCELAR_BAIXA_CONTAS_RECEBER(iBAIXA_ID in number, iPROCESSO_CAIXA in string)
is
  vQtde             number default 0;
  vMensagem         varchar2(400);
  vPodeEstornar     char(1) default 'N';
  vTurnoBaixado     char(1) default 'N';
  xReceber_Id       number default 0;
  xBaixa_Id         number default 0;
  xValor_Documento  number default 0;
  xTipo             char(3);

  cursor cTitulosOutrasBaixas is
  select
    COR.RECEBER_ID,
    BAX.BAIXA_ID,
    COR.STATUS,
    COR.VALOR_DOCUMENTO,
    CBA.TIPO,
    CBA.RECEBER_ADIANTADO_ID	
  from
    CONTAS_RECEBER COR

  inner join CONTAS_RECEBER_BAIXAS_ITENS BAX
  on COR.RECEBER_ID = BAX.RECEBER_ID    
  
  inner join CONTAS_RECEBER_BAIXAS CBA
  on CBA.BAIXA_ID = BAX.BAIXA_ID  

  where COR.BAIXA_ORIGEM_ID = iBAIXA_ID

  order by
    BAX.BAIXA_ID,
    COR.RECEBER_ID;

begin

  /* ********** REALIZANDO CHECAGENS SE PODE ESTORNAR OU N?O ********** */
  -- Verificando se existe nos t?tulos que foram gerados pela baixa que est? para ser estornada alguma baixa  
  for xTitulosOutrasBaixas in cTitulosOutrasBaixas loop
    if xTitulosOutrasBaixas.STATUS = 'B' and xTitulosOutrasBaixas.BAIXA_ID <> iBAIXA_ID then /* Se for a mesma baixa ? os adiantamentos */
      vMensagem := vMensagem ||
        'C?d. baixa.: ' || xTitulosOutrasBaixas.BAIXA_ID || chr(13) || chr(10) ||
        'C?d. t?tulo: ' || xTitulosOutrasBaixas.RECEBER_ID || chr(13) || chr(10);
    end if;
	
	if xTitulosOutrasBaixas.BAIXA_ID = iBAIXA_ID and xTitulosOutrasBaixas.TIPO = 'ADI' then
	  update CONTAS_RECEBER set
	    VALOR_ADIANTADO = VALOR_ADIANTADO - xTitulosOutrasBaixas.VALOR_DOCUMENTO
	  where RECEBER_ID = xTitulosOutrasBaixas.RECEBER_ADIANTADO_ID;
	end if;
  end loop;

  if vMensagem is not null then
    ERRO(
      'Existe(m) t?tulo(s) gerado(s) por esta baixa ao qual j? foram baixados, ' ||
      '? necess?rio fazer o estorno destes antes de concluir esta opera??o! ' || chr(13) || chr(10) || vMensagem
    );
  end if;

  if iPROCESSO_CAIXA = 'N' then
    select
      case when RECEBER_CAIXA = 'S' and DATA_PAGAMENTO is null then 'N' else 'S' end
    into
      vPodeEstornar
    from
      CONTAS_RECEBER_BAIXAS
    where BAIXA_ID = iBAIXA_ID;

    if vPodeEstornar = 'N' then
      ERRO('Est? baixa n?o pode ser estornada por estar pendente de recebimento no caixa!');
    end if;
  end if;
  /* **********       FIM CHECAGENS         ********** */

  select
    case when count(*) > 0 then 'S' else 'N' end
  into
    vTurnoBaixado
  from
    CONTAS_RECEBER_BAIXAS CON

  inner join TURNOS_CAIXA TUR
  on CON.TURNO_ID = TUR.TURNO_ID

  where CON.BAIXA_ID = iBAIXA_ID
  and TUR.STATUS = 'B';

  if vTurnoBaixado = 'S' then
    ERRO('A baixa que est? sendo cancelada pertence a um turno baixado, cancelamento n?o permitido!');
  end if;
  
  delete from CONTAS_REC_BAIXAS_CREDITOS
  where BAIXA_ID = iBAIXA_ID;

  delete from LOGS_CONTAS_RECEBER
  where RECEBER_ID in(select RECEBER_ID from CONTAS_RECEBER where BAIXA_ORIGEM_ID = iBAIXA_ID);

  delete from CONTAS_RECEBER_BAIXAS_ITENS
  where BAIXA_ID = iBAIXA_ID;
  
  delete from LOGS_CONTAS_RECEBER
  where RECEBER_ID in(select RECEBER_ID from CONTAS_RECEBER where BAIXA_ORIGEM_ID = iBAIXA_ID);  
  
  /* Deletando os t?tulos gerados atrav?s da baixa */
  delete from CONTAS_RECEBER
  where BAIXA_ORIGEM_ID = iBAIXA_ID;
  
  delete from LOGS_CONTAS_PAGAR
  where PAGAR_ID in(select PAGAR_ID from CONTAS_PAGAR where BAIXA_RECEBER_ORIGEM_ID = iBAIXA_ID);

  delete from CONTAS_PAGAR
  where BAIXA_RECEBER_ORIGEM_ID = iBAIXA_ID;

  delete from CONTAS_REC_BAIXAS_PAGAMENTOS
  where BAIXA_ID = iBAIXA_ID;

  delete from CONTAS_REC_BAIXAS_PAGTOS_CHQ
  where BAIXA_ID = iBAIXA_ID;

  delete from CONTAS_REC_BAIXAS_PAGTOS_DIN
  where BAIXA_ID = iBAIXA_ID;

  /* Deletando a baixa */
  delete from CONTAS_RECEBER_BAIXAS
  where BAIXA_ID = iBAIXA_ID;

end CANCELAR_BAIXA_CONTAS_RECEBER;
