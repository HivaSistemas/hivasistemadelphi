create or replace package SESSAO
as  
  USUARIO_SESSAO_ID            number(4);
  USUARIO_LIBERACAO_ID         number(4);
  SISTEMA                      varchar2(20);
  NOME_COMPUTADOR_SESSAO       varchar2(30) default 'SEM DEFINICAO';
  ROTINA                       varchar2(30) default 'SEM DEFINICAO'; 
  EXECUTANDO_TRIGGERS_ENTREGAS boolean default false;
  
  PARAMETROS_GERAIS            PARAMETROS%rowtype;
  PARAMETROS_EMPRESA_LOGADA    PARAMETROS_EMPRESA%rowtype;
  
end SESSAO;
/