﻿create table DESENVOLVIMENTOS(
  DESENVOLVIMENTO_ID         number(5) not null,
  CLIENTE_ID                 NUMBER(10,0),
  FUNCIONARIO_ID             NUMBER(10,0),
  PRIORIDADE                 char(1) not null,
  DATA_CADASTRO              date default trunc(sysdate) not null,
  STATUS                     char(2) default 'AD' not null,  
  TIPO_SOLICITACAO           char(2) not null,
  VERSAO                     VARCHAR2(20),
  TEXTO_HISTORICO            clob,
  TEXTO_ANALISE              clob,
  TEXTO_DESENVOLVIMENTO      clob,
  TEXTO_TESTES               clob,
  TEXTO_DOCUMENTACAO         clob,
  TEXTO_LIBERACAO            clob,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* PRIORIDADE
A - Alta
M - Média
B - Baixa
U - Urgente
*/

alter table DESENVOLVIMENTOS
add constraint PK_DESEN_DESENVOLVIMENTO_ID 
primary key(DESENVOLVIMENTO_ID)
using index tablespace INDICES;

alter table DESENVOLVIMENTOS
add constraint CK_DESEN_PRIORIDADE
check(PRIORIDADE in('U','A', 'M', 'B'));

/* STATUS
AA - Aguardando analise
EA - Em analise
AN - Analisado
AD - Aguardando Desenvolvimento
ED - Em desenvolvimento
AT - Aguardando Teste
ET - Em testes
AC - Aguardando Documentação
DC - Documentado
AL - Aguardando Liberacao
LI - Liberado
NE - Negado 
FI - Atendimento Finalizado
*/
alter table DESENVOLVIMENTOS
add constraint CK_DESEN_STATUS
check(STATUS in('AA', 'EA', 'AN', 'AD', 'ED', 'AT','ET','AC','DC','AL','LI','NE','FI'));

/* TIPO_SOLICITACAO
CE - Correção de Erro
ND - Novo Desenvolvimento
GD - Gerar Documentação
AP - Analise de Procedimento
OS - Ordem Serviço
*/
alter table DESENVOLVIMENTOS
add constraint CK_DESEN_TP_SOLICIT
check(TIPO_SOLICITACAO in('CE','ND', 'GD', 'AP','OS'));

