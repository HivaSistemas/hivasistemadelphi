create table UP_FILES(
  NOME               varchar2(100),
  DATA_HORA_INSERCAO date,
  DATA_HORA_ARQUIVO  date,
  ARQUIVO            blob
);

alter table UP_FILES
add constraint PK_UP_FILES
primary key(NOME)
using index tablespace INDICES;
