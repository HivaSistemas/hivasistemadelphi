create table NOTAS_FISCAIS_CARTAS_CORRECOES(  
  NOTA_FISCAL_ID        number(12) not null,
  SEQUENCIA             number(2) not null,  
  TEXTO_CORRECAO        varchar2(1000) not null,
  USUARIO_CADASTRO_ID   number(4) not null,
  DATA_HORA_CADASTRO    date not null,
  STATUS                char(1) default 'N' not null,
  DATA_HORA_EMISSAO     date,
  PROTOCOLO_NFE         varchar2(15),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table NOTAS_FISCAIS_CARTAS_CORRECOES
add constraint PK_NOTAS_FISCAIS_CARTAS_CORR
primary key(NOTA_FISCAL_ID, SEQUENCIA)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table NOTAS_FISCAIS_CARTAS_CORRECOES
add constraint FK_NF_CARTAS_CORR_NOTA_FISC_ID
foreign key(NOTA_FISCAL_ID)
references NOTAS_FISCAIS(NOTA_FISCAL_ID);

alter table NOTAS_FISCAIS_CARTAS_CORRECOES
add constraint FK_NF_CARTAS_CORR_USU_CAD_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* Checagens */
alter table NOTAS_FISCAIS_CARTAS_CORRECOES
add constraint CK_NF_CARTAS_CORR_NOTA_SEQUENC
check(SEQUENCIA > 0);

/* 
  STATUS
  N - Não emitida
  E - Emitida
*/
alter table NOTAS_FISCAIS_CARTAS_CORRECOES
add constraint CK_NF_CARTAS_CORR_NOTA_STATUS
check(
  STATUS = 'N' and DATA_HORA_EMISSAO is null and PROTOCOLO_NFE is null
  or
  STATUS = 'E' and DATA_HORA_EMISSAO is not null and PROTOCOLO_NFE is not null
);