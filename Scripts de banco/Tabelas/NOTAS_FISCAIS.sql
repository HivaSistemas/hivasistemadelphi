﻿create table NOTAS_FISCAIS(
  NOTA_FISCAL_ID              number(12),
  OUTRA_NOTA_ID               number(12),
  CADASTRO_ID                 number(10) not null,
  CFOP_ID                     varchar2(7) not null,
  EMPRESA_ID                  number(3) not null,
  ORCAMENTO_BASE_ID           number(10),
  ORCAMENTO_ID                number(10),
  DEVOLUCAO_ID                number(10),
  DEVOLUCAO_ENTRADA_ID        number(12),
  RETIRADA_ID                 number(12),
  PRODUCAO_ESTOQUE_ID         number(12),
  NATUREZA_PRODUCAO           char(1),
  AJUSTE_ESTOQUE_ID           number(12),
  NATUREZA_AJUSTE             char(1),
  ENTREGA_ID                  number(12),
  ACUMULADO_ID                number(10),
  TRANSFERENCIA_PRODUTOS_ID   number(10),
  TRANSF_FISCAL_RETIRADA_ID   number(12),
  TRANSF_FISCAL_ENTREGA_ID    number(12),
  NUMERO_NOTA                 number(9),
	MODELO_NOTA                 varchar2(3) not null,
	SERIE_NOTA                  char(3) not null,

  /* Dados da empresa emitente da NF */
  RAZAO_SOCIAL_EMITENTE       varchar2(60) not null,
  NOME_FANTASIA_EMITENTE      varchar2(60) not null,
  REGIME_TRIBUTARIO           char(2) not null,
  CNPJ_EMITENTE               varchar2(19) not null,
  INSCRICAO_ESTADUAL_EMITENTE varchar2(20) not null,
  LOGRADOURO_EMITENTE         varchar2(100) not null,
  COMPLEMENTO_EMITENTE        varchar2(100) not null,
  NOME_BAIRRO_EMITENTE        varchar2(60) not null,
  NOME_CIDADE_EMITENTE        varchar2(60) not null,
  NUMERO_EMITENTE             varchar2(10) not null,
  ESTADO_ID_EMITENTE          char(2) not null,
  CEP_EMITENTE                varchar2(9) not null,
  TELEFONE_EMITENTE           varchar2(15),
  CODIGO_IBGE_MUNICIPIO_EMIT  number(7) not null,
  CODIGO_IBGE_ESTADO_EMITENT  number(2) not null,
  
  /* Dados do cliente */
  NOME_FANTASIA_DESTINATARIO  varchar2(100) not null,
  RAZAO_SOCIAL_DESTINATARIO   varchar2(100),
  TIPO_PESSOA_DESTINATARIO    char(1) not null,
  CPF_CNPJ_DESTINATARIO       varchar2(19) not null,
  INSCRICAO_ESTADUAL_DESTINAT varchar2(20) not null,
  LOGRADOURO_DESTINATARIO     varchar2(100),
  COMPLEMENTO_DESTINATARIO    varchar2(100),
  NOME_BAIRRO_DESTINATARIO    varchar2(60),
  NOME_CIDADE_DESTINATARIO    varchar2(60),
  NUMERO_DESTINATARIO         varchar2(10),
  ESTADO_ID_DESTINATARIO      char(2),
  CEP_DESTINATARIO            varchar2(9),
  CODIGO_IBGE_MUNICIPIO_DEST  number(7) not null,
  E_MAIL_DESTINATARIO         varchar2(30),

  /* Endereço de entrega */
  INSCRICAO_ESTADUAL_DESTINAT varchar2(20) not null,
  LOGRADOURO_DESTINATARIO     varchar2(100),
  COMPLEMENTO_DESTINATARIO    varchar2(100),
  NOME_BAIRRO_DESTINATARIO    varchar2(60),
  NOME_CIDADE_DESTINATARIO    varchar2(60),
  NUMERO_DESTINATARIO         varchar2(10),
  ESTADO_ID_DESTINATARIO      char(2),
  CEP_DESTINATARIO            varchar2(9),
  CODIGO_IBGE_MUNICIPIO_DEST  number(7) not null,

  INSCRICAO_EST_ENTR_DESTINAT   varchar2(20),
  LOGRADOURO_ENTR_DESTINATARIO  varchar2(100),
  COMPLEMENTO_ENTR_DESTINATARIO varchar2(100),
  NOME_BAIRRO_ENTR_DESTINATARIO varchar2(60),
  NOME_CIDADE_ENTR_DESTINATARIO varchar2(60),
  NUMERO_ENTR_DESTINATARIO      varchar2(10),
  ESTADO_ID_ENTR_DESTINATARIO   char(2),
  CEP_ENTR_DESTINATARIO         varchar2(9),
  CODIGO_IBGE_MUNIC_ENTR_DEST   number(7),

  NOME_CONSUMIDOR_FINAL       varchar2(60),
  CPF_CONSUMIDOR_FINAL        varchar2(14),
  TELEFONE_CONSUMIDOR_FINAL   varchar2(15), 
  TIPO_NOTA                   char(1) not null,
  
  NATUREZA_OPERACAO           varchar2(60) not null,  
  
  /* Valores da Nota fiscal */
  VALOR_TOTAL                 number(8,2) not null,
  VALOR_TOTAL_PRODUTOS        number(8,2) not null,  
  VALOR_DESCONTO              number(8,2) default 0 not null,
  VALOR_OUTRAS_DESPESAS       number(8,2) default 0 not null,
  VALOR_FRETE                 number(8,2) default 0 not null,
  VALOR_SEGURO                number(8,2) default 0 not null,
  BASE_CALCULO_ICMS           number(8,2) default 0 not null,
  VALOR_ICMS                  number(8,2) default 0 not null,
  BASE_CALCULO_ICMS_ST        number(8,2) default 0 not null,
  VALOR_ICMS_ST               number(8,2) default 0 not null,
  BASE_CALCULO_PIS            number(8,2) default 0 not null,
  VALOR_PIS                   number(8,2) default 0 not null,
  BASE_CALCULO_COFINS         number(8,2) default 0 not null,
  VALOR_COFINS                number(8,2) default 0 not null,
  VALOR_IPI                   number(8,2) default 0 not null,

  PESO_LIQUIDO                number(10,2) default 0 not null,
  PESO_BRUTO                  number(10,2) default 0 not null,
  
  VALOR_RECEBIDO_DINHEIRO     number(8,2) default 0 not null,
  VALOR_RECEBIDO_PIX          number(8,2) default 0 not null,
  VALOR_RECEBIDO_CARTAO_CRED  number(8,2) default 0 not null,
  VALOR_RECEBIDO_CARTAO_DEB   number(8,2) default 0 not null,
  VALOR_RECEBIDO_CREDITO      number(8,2) default 0 not null,
  VALOR_RECEBIDO_COBRANCA     number(8,2) default 0 not null,
  VALOR_RECEBIDO_CHEQUE       number(8,2) default 0 not null,
  VALOR_RECEBIDO_FINANCEIRA   number(8,2) default 0 not null,
  VALOR_RECEBIDO_ACUMULADO    number(8,2) default 0 not null,

  DATA_HORA_CADASTRO          date default sysdate not null,
  DATA_HORA_EMISSAO           date,
  DATA_HORA_CANCELAMENTO_NOTA date,
  STATUS                      char(1) not null,
  
  TIPO_MOVIMENTO              char(3) not null,
  MOV_EST_FIS                 char(1) default 'S' NOT NULL,

  /* Nota fiscal eletrônica */    
  NUMERO_RECIBO_LOTE_NFE      varchar2(15),  -- Dropar esta coluna
  PROTOCOLO_NFE               varchar2(15),
  DATA_HORA_PROTOCOLO_NFE     date,
  CHAVE_NFE                   varchar2(50),
  STATUS_NFE                  varchar2(3),
  MOTIVO_STATUS_NFE           varchar2(255),
  PROTOCOLO_CANCELAMENTO_NFE  varchar2(15),
  DATA_HORA_CANCELAMENTO_NFE  date,
  MOTIVO_CANCELAMENTO_NFE     varchar2(255),
  ENVIOU_EMAIL_NFE_CLIENTE    char(1) default 'N' not null,
  DANFE_IMPRESSO              char(1) default 'N' not null,

  INDICE_FORMAS_PAGAMENTO     number(9,8) default 1 not null,
	
	NOTA_FISCAL_ORIGEM_CUPOM_ID number(12),
	
	INFORMACOES_COMPLEMENTARES  varchar2(1000),

  MOVIMENTAR_EST_OUTRAS_NOTAS char(1) default 'N' not null,
  ESPECIE_NFE                 VARCHAR2(5),
  QUANTIDADE_NFE              number(8,2) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table NOTAS_FISCAIS
add constraint PK_NOTAS_FISCAIS
primary key(NOTA_FISCAL_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISCAIS_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISCAIS_CFOP_ID
foreign key(CFOP_ID)
references CFOP(CFOP_PESQUISA_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISCAIS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISCAIS_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISCAIS_ORC_BASE_ID
foreign key(ORCAMENTO_BASE_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISCAIS_DEVOLUCAO_ID
foreign key(DEVOLUCAO_ID)
references DEVOLUCOES(DEVOLUCAO_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISCAIS_DEVOL_ENT_ID
foreign key(DEVOLUCAO_ENTRADA_ID)
references DEVOLUCOES_ENTRADAS_NOTAS_FISC(DEVOLUCAO_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISCAIS_RETIRADA_ID
foreign key(RETIRADA_ID)
references RETIRADAS(RETIRADA_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISCAIS_PROD_EST_ID
foreign key(PRODUCAO_ESTOQUE_ID)
references PRODUCAO_ESTOQUE(PRODUCAO_ESTOQUE_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISCAIS_ENTREGA_ID
foreign key(ENTREGA_ID)
references ENTREGAS(ENTREGA_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISCAIS_ACUMULADO_ID
foreign key(ACUMULADO_ID)
references ACUMULADOS(ACUMULADO_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISCAIS_TRANSF_PROD_ID
foreign key(TRANSFERENCIA_PRODUTOS_ID)
references TRANSF_PRODUTOS_EMPRESAS(TRANSFERENCIA_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FISC_NF_ORIG_CUPOM_ID
foreign key(NOTA_FISCAL_ORIGEM_CUPOM_ID)
references NOTAS_FISCAIS(NOTA_FISCAL_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FIS_TR_FISC_RET_ID
foreign key(TRANSF_FISCAL_RETIRADA_ID)
references RETIRADAS(RETIRADA_ID);

alter table NOTAS_FISCAIS
add constraint FK_NOTAS_FIS_TR_FISC_ENT_ID
foreign key(TRANSF_FISCAL_ENTREGA_ID)
references ENTREGAS(ENTREGA_ID);

/* Constraints */

/* REGIME_TRIBUTARIO
  SN - Simples Nacional
  LR - Lucro Real
  LP - Lucro Presumido
*/ 
alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FISCAIS_REGIME_TRIBUT
check(REGIME_TRIBUTARIO in('SN', 'LP', 'LR'));

alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FISCAIS_VALORES
check(
  VALOR_TOTAL > 0 and
  VALOR_TOTAL_PRODUTOS > 0 and
  VALOR_DESCONTO >= 0 and
  VALOR_OUTRAS_DESPESAS >= 0 and
  BASE_CALCULO_ICMS >= 0 and
  VALOR_ICMS >= 0 and
  BASE_CALCULO_ICMS_ST >= 0 and
  VALOR_ICMS_ST >= 0 and
  BASE_CALCULO_PIS >= 0 and
  VALOR_PIS >= 0 and
  BASE_CALCULO_COFINS >= 0 and
  VALOR_COFINS >= 0 and
  VALOR_IPI >= 0 and
  VALOR_FRETE >= 0 and
  VALOR_SEGURO >= 0 and
  PESO_LIQUIDO >= 0 and
  PESO_BRUTO >= 0 
);

alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FISCAIS_NUMERO_NOTA
check(NUMERO_NOTA > 0);

/* MODELO_NOTA
	2 - Cupom fiscal
	55 - Nota fiscal eletrônica
  65 - NFCe
*/
alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FISCAIS_MODELO_NOTA
check(MODELO_NOTA in('2', '55', '65'));

/* TIPO_NOTA
  C - NFC-e
  N - Nota fiscal eletrônica
*/ 
alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FISCAIS_TIPO_NOTA
check(TIPO_NOTA in('C','N'));

alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FIS_STATUS_DATA_EMI
check(
  (STATUS = 'N' and DATA_HORA_EMISSAO is null)
  or
  (STATUS <> 'N' and DATA_HORA_EMISSAO is not null)
);

/* STATUS 
  E - Emitida
  N - Não emitida
  C - Cancelada
  D - Denegada
*/
alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FISCAIS_STATUS
check(
  (
    STATUS = 'N' and
    CHAVE_NFE is null and
    PROTOCOLO_NFE is null and
    DATA_HORA_CANCELAMENTO_NFE is null and
    MOTIVO_CANCELAMENTO_NFE is null and
    PROTOCOLO_CANCELAMENTO_NFE is null
  )
  or
  (STATUS = 'E' and CHAVE_NFE is not null and PROTOCOLO_NFE is not null)
  or
	(STATUS = 'C' and DATA_HORA_CANCELAMENTO_NFE is not null and MOTIVO_CANCELAMENTO_NFE is not null and PROTOCOLO_CANCELAMENTO_NFE is not null)
);

/* TIPO_MOVIMENTO
  VRA - Venda retirar ato
  VRE - Venda retirar
  VEN - Venda entregar
  DRE - Devoluções de retiradas
  DEN - Devoluções de entregas
  OUT - Outras notas
  ACU - Acumulados
  DAC - Devolução de acumulados
  TPE - Transferência de produtos entre empresas
  TFR - Transferência fiscal de retirada
  TFE - Transferência fiscal de entrega
  NNE - Nota fiscal de NFCe
  REN - Retorno de parcial/total de entrega,
  TFI - Transferencia fiscal
  DEF - Devolução de entrada de NF
  PEN - Produção entrada
  PSA - Produção saída
  AEE - Ajuste entrada
  AES - Ajuste saída
*/
alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FISCAIS_TIPO_MOVIMENT
check(
  TIPO_MOVIMENTO = 'VRA' and ORCAMENTO_BASE_ID is not null 
  or
  TIPO_MOVIMENTO = 'VRE' and ORCAMENTO_BASE_ID is not null and RETIRADA_ID is not null
  or
  TIPO_MOVIMENTO = 'VEN' and ORCAMENTO_BASE_ID is not null and ENTREGA_ID is not null
  or
  TIPO_MOVIMENTO = 'DRE' and ORCAMENTO_BASE_ID is not null and DEVOLUCAO_ID is not null and RETIRADA_ID is not null
  or
  TIPO_MOVIMENTO = 'DEN' and ORCAMENTO_BASE_ID is not null and DEVOLUCAO_ID is not null and ENTREGA_ID is not null
  or
  TIPO_MOVIMENTO = 'OUT' and OUTRA_NOTA_ID is not null
  or
  TIPO_MOVIMENTO = 'ACU' and ACUMULADO_ID is not null
  or
  TIPO_MOVIMENTO = 'DAC' and ORCAMENTO_BASE_ID is not null and DEVOLUCAO_ID is not null
  or
  TIPO_MOVIMENTO = 'TPE' and TRANSFERENCIA_PRODUTOS_ID is not null
  or
  TIPO_MOVIMENTO = 'NNE' and NOTA_FISCAL_ORIGEM_CUPOM_ID is not null
  or
  TIPO_MOVIMENTO = 'REN' and ORCAMENTO_BASE_ID is not null and ENTREGA_ID is not null
  or
  TIPO_MOVIMENTO = 'TFI'
  or
  TIPO_MOVIMENTO = 'DEF' and DEVOLUCAO_ENTRADA_ID is not null
  or
  (TIPO_MOVIMENTO = 'PEN' or TIPO_MOVIMENTO = 'PSA') and PRODUCAO_ESTOQUE_ID is not null
  or
  (TIPO_MOVIMENTO = 'AEE' or TIPO_MOVIMENTO = 'AES') and AJUSTE_ESTOQUE_ID is not null
);

alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FISCAIS_STATUS_NFE
check(
  (STATUS_NFE is null and MOTIVO_STATUS_NFE is null)  
  or
  (STATUS_NFE is not null and MOTIVO_STATUS_NFE is not null)      
);

alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FISCAIS_ENV_EMAIL_NFE
check(ENVIOU_EMAIL_NFE_CLIENTE in('S','N'));

alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FISCAIS_DANFE_IMP
check(DANFE_IMPRESSO in('S','N'));

alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FISCAIS_NF_ORIGEM_ID
check(
  substr(CFOP_ID, 1, 4) not in('5929', '6929') and NOTA_FISCAL_ORIGEM_CUPOM_ID is null
	or
	substr(CFOP_ID, 1, 4) in('5929', '6929') and NOTA_FISCAL_ORIGEM_CUPOM_ID is not null
);

alter table NOTAS_FISCAIS
add constraint CK_NOTAS_FISCAIS_IND_FOR_PAGTO
check(INDICE_FORMAS_PAGAMENTO > 0);

alter table NOTAS_FISCAIS
add constraint CK_NOTAS_F_MOV_EST_OUT_NOTAS
check(
  MOVIMENTAR_EST_OUTRAS_NOTAS = 'N'
  or
  MOVIMENTAR_EST_OUTRAS_NOTAS = 'S' and TIPO_MOVIMENTO = 'OUT'
);
