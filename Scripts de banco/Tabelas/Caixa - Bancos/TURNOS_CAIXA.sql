﻿create table TURNOS_CAIXA(
  TURNO_ID                      number(10),
  EMPRESA_ID                    number(3) not null,
  FUNCIONARIO_ID                number(4) not null,
  CONTA_ORIGEM_DIN_INI_ID       varchar2(8),
  VALOR_INICIAL                 number(8,2) default 0 not null,  
  DATA_HORA_ABERTURA            date not null,
  USUARIO_ABERTURA_ID           number(4) not null,
  DATA_HORA_FECHAMENTO          date,
  USUARIO_FECHAMENTO_ID         number(4),
  VALOR_DINHEIRO_INF_FECHAMENTO number(8,2),
  VALOR_CHEQUE_INF_FECHAMENTO   number(8,2),
  VALOR_CARTAO_INF_FECHAMENTO   number(8,2),
  VALOR_COBRANCA_INF_FECHAMENTO number(8,2),
  STATUS                        char(1) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chaves primária */
alter table TURNOS_CAIXA
add constraint PK_TURNOS_CAIXA_TURNO_ID
primary key(TURNO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table TURNOS_CAIXA
add constraint FK_TURNOS_CAIXA_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table TURNOS_CAIXA
add constraint FK_TURNOS_CAIXA_FUNCIONARIO_ID
foreign key(FUNCIONARIO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table TURNOS_CAIXA
add constraint FK_TURNOS_CAIXA_CON_OR_DIN_IN
foreign key(CONTA_ORIGEM_DIN_INI_ID)
references CONTAS(CONTA);

alter table TURNOS_CAIXA
add constraint FK_TURNOS_CAIXA_USU_ABERT_ID
foreign key(USUARIO_ABERTURA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table TURNOS_CAIXA
add constraint FK_TURNOS_CAIXA_USU_FECHAM_ID
foreign key(USUARIO_FECHAMENTO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* Constraints */
alter table TURNOS_CAIXA
add constraint CK_TURNOS_CAIXA_ORIGEM_DIN
check(
  VALOR_INICIAL > 0 and CONTA_ORIGEM_DIN_INI_ID is not null
  or
  VALOR_INICIAL = 0 and CONTA_ORIGEM_DIN_INI_ID is null
);

alter table TURNOS_CAIXA
add constraint CK_TURNOS_CAIXA_DATA_ABERT_FEC
check(
  nvl(DATA_HORA_FECHAMENTO, DATA_HORA_ABERTURA) >= DATA_HORA_ABERTURA
);

alter table TURNOS_CAIXA
add constraint CK_TURNOS_CAIXA_VALOR_FECHAMEN
check(
  (
	  DATA_HORA_FECHAMENTO is null and
	  USUARIO_FECHAMENTO_ID is null and
	  VALOR_DINHEIRO_INF_FECHAMENTO is null and
	  VALOR_CHEQUE_INF_FECHAMENTO is null and
	  VALOR_CARTAO_INF_FECHAMENTO is null and
	  VALOR_COBRANCA_INF_FECHAMENTO is null
  )
  or
  (
    DATA_HORA_FECHAMENTO is not null and
	  USUARIO_FECHAMENTO_ID is not null and
	  VALOR_DINHEIRO_INF_FECHAMENTO is not null and
	  VALOR_CHEQUE_INF_FECHAMENTO is not null and
	  VALOR_CARTAO_INF_FECHAMENTO is not null and
	  VALOR_COBRANCA_INF_FECHAMENTO is not null
  )
);

alter table TURNOS_CAIXA
add constraint CK_TURNOS_CAIXA_STATUS
check(
  (STATUS = 'A' and DATA_HORA_FECHAMENTO is null and USUARIO_FECHAMENTO_ID is null)
  or
  (STATUS = 'B' and DATA_HORA_FECHAMENTO is not null and USUARIO_FECHAMENTO_ID is not null)
);

