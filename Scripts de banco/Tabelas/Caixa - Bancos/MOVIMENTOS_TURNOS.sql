create table MOVIMENTOS_TURNOS(
  MOVIMENTO_TURNO_ID     number(10),
  TURNO_ID               number(10) not null,
  CONTA_ID               varchar(8),

  TIPO_MOVIMENTO         char(3) not null,
  VALOR_DINHEIRO         number(8,2) not null,
  VALOR_PIX              number(8,2) default not null,
  VALOR_CHEQUE           number(8,2) not null,
  VALOR_CARTAO           number(8,2) not null,
  VALOR_COBRANCA         number(8,2) not null,

  DATA_HORA_MOVIMENTO    date not null,
  USUARIO_CADASTRO_ID    number(4) not null,
  OBSERVACAO             varchar2(200),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);
/* Chave prim�ria */ 
alter table MOVIMENTOS_TURNOS
add constraint PK_MOVIMENTOS_TURNOS
primary key(MOVIMENTO_TURNO_ID)
using index tablespace INDICES;

/* Foreing Key */
alter table MOVIMENTOS_TURNOS
add constraint FK_MOVIMENTOS_TURNOS_TURNO_ID
foreign key(TURNO_ID)
references TURNOS_CAIXA(TURNO_ID);

alter table MOVIMENTOS_TURNOS
add constraint FK_MOVIMENTOS_TURNOS_CONTA_ID
foreign key(CONTA_ID)
references CONTAS(CONTA);

alter table MOVIMENTOS_TURNOS
add constraint FK_MOV_TURNOS_USUARIO_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* -------------- Constraints --------------- */
/* TIPO_MOVIMENTO
  SUP - Suprimento
  SAN - Sangria
  FEC - Fechamento
*/
alter table MOVIMENTOS_TURNOS
add constraint CK_MOV_TURNOS_TIPO_MOVIM
check(
  TIPO_MOVIMENTO = 'SUP' and CONTA_ID is not null
  or
  TIPO_MOVIMENTO in('SAN', 'FEC')
);

alter table MOVIMENTOS_TURNOS
add constraint CK_MOVIMENTOS_TURNOS_VALORES
check(
  VALOR_DINHEIRO >= 0 and
  VALOR_CHEQUE >= 0 and
  VALOR_CARTAO >= 0 and
  VALOR_COBRANCA >= 0
);
