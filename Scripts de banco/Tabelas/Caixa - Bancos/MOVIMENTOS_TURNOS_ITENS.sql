﻿create table MOVIMENTOS_TURNOS_ITENS(
  MOVIMENTO_TURNO_ID  number(10) not null,
  TIPO                char(1) not null,
  ID                  number(12) not null
);

/* Chave primária */
alter table MOVIMENTOS_TURNOS_ITENS
add constraint PK_MOVIMENTOS_TURNOS_ITENS
primary key(MOVIMENTO_TURNO_ID, TIPO, ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table MOVIMENTOS_TURNOS_ITENS
add constraint FK_MOV_TURNOS_MOVIMENTO_ID
foreign key(MOVIMENTO_TURNO_ID)
references MOVIMENTOS_TURNOS(MOVIMENTO_TURNO_ID);

/* Constraints */
/* TIPO
  R - Contas a receber
  P - Contas a pagar
*/
alter table MOVIMENTOS_TURNOS_ITENS
add constraint CK_MOV_TURNOS_ITENS_TIPO
check(TIPO in('R','P'));