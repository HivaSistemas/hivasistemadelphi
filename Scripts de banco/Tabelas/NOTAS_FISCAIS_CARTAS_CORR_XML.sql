create table NOTAS_FISCAIS_CARTAS_CORR_XML(  
  NOTA_FISCAL_ID        number(12) not null,
  SEQUENCIA             number(2) not null,  
  XML                   blob,
  XML_TEXTO             clob,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table NOTAS_FISCAIS_CARTAS_CORR_XML
add constraint PK_NOTAS_FIS_CARTAS_CORR_XML
primary key(NOTA_FISCAL_ID, SEQUENCIA)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table NOTAS_FISCAIS_CARTAS_CORR_XML
add constraint PK_NF_CARTAS_COR_XML_NF_SEQ_ID
foreign key(NOTA_FISCAL_ID, SEQUENCIA)
references NOTAS_FISCAIS_CARTAS_CORRECOES(NOTA_FISCAL_ID, SEQUENCIA);