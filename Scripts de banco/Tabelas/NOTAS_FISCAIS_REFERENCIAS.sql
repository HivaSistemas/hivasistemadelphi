﻿create table NOTAS_FISCAIS_REFERENCIAS(
  NOTA_FISCAL_ID              number(12) not null,
  ITEM_ID                     number(2) not null,
  TIPO_REFERENCIA             char(1) not null,
  
  TIPO_NOTA                   char(1),
  NOTA_FISCAL_ID_REFERENCIADA number(12),
  CHAVE_NFE_REFERENCIADA      varchar2(54),

   /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table NOTAS_FISCAIS_REFERENCIAS
add constraint PK_NOTAS_FISCAIS_REFERENCIAS
primary key(NOTA_FISCAL_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table NOTAS_FISCAIS_REFERENCIAS
add constraint FK_NF_REF_NOTA_FISCAL_ID
foreign key(NOTA_FISCAL_ID)
references NOTAS_FISCAIS(NOTA_FISCAL_ID);

alter table NOTAS_FISCAIS_REFERENCIAS
add constraint FK_NF_REF_NOTA_FIS_REF_ID
foreign key(NOTA_FISCAL_ID_REFERENCIADA)
references NOTAS_FISCAIS(NOTA_FISCAL_ID);

/* Checagens */
/* 
  TIPO_REFENCIA
  M - Manual
  A - Automatica, documentos fiscais próprios do Hiva
*/
alter table NOTAS_FISCAIS_REFERENCIAS
add constraint CK_NF_REF_TIPO_REFENCIA
check(
 TIPO_REFERENCIA = 'M' and TIPO_NOTA is not null and CHAVE_NFE_REFERENCIADA is not null and NOTA_FISCAL_ID_REFERENCIADA is null
 or
 TIPO_REFERENCIA = 'A' and TIPO_NOTA is null and CHAVE_NFE_REFERENCIADA is null and NOTA_FISCAL_ID_REFERENCIADA is not null
);

/* 
  TIPO_NOTA
  C - NFCe
  N - NFe
*/
alter table NOTAS_FISCAIS_REFERENCIAS
add constraint CK_NF_REF_TIPO_NOTA
check(TIPO_NOTA in('C', 'N', null));

alter table NOTAS_FISCAIS_REFERENCIAS
add constraint CK_NF_REF_CHAVE_NFE_REFERENC
check(
  CHAVE_NFE_REFERENCIADA is null
  or
  length(CHAVE_NFE_REFERENCIADA) = 44
);