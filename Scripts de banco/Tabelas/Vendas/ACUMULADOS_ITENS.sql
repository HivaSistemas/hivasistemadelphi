create table ACUMULADOS_ITENS(
  ACUMULADO_ID                number(10) not null,
  PRODUTO_ID                  number(10) not null,  
  PRECO_UNITARIO              number(8,2) not null,
  QUANTIDADE                  number(20,4) not null,
  VALOR_TOTAL                 number(8,2) not null,
  VALOR_TOTAL_DESCONTO        number(20,4) not null,
  VALOR_TOTAL_OUTRAS_DESPESAS number(20,4) not null,
  VALOR_TOTAL_FRETE           number(20,4) default 0 not null,
  
  /* CUSTOS */
  PRECO_FINAL                     number(9,4) default 0 not null,
  PRECO_FINAL_MEDIO               number(9,4) default 0 not null,
  PRECO_LIQUIDO                   number(9,4) default 0 not null,
  PRECO_LIQUIDO_MEDIO             number(9,4) default 0 not null,
  CMV                             number(9,4) default 0 not null,
  CUSTO_ULTIMO_PEDIDO             number(9,4) default 0 not null,
  CUSTO_PEDIDO_MEDIO              number(9,4) default 0 not null,

  VALOR_IMPOSTOS                  number(8,2) default 0 not null,
  VALOR_ENCARGOS                  number(8,2) default 0 not null,
  VALOR_CUSTO_VENDA               number(8,2) default 0 not null,
  VALOR_CUSTO_FINAL               number(8,2) default 0 not null,  

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table ACUMULADOS_ITENS
add constraint PK_ACUMULADOS_ITENS
primary key(ACUMULADO_ID, PRODUTO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ACUMULADOS_ITENS
add constraint FK_ACUM_ITENS_ACUMULADO_ID
foreign key(ACUMULADO_ID)
references ACUMULADOS(ACUMULADO_ID);

alter table ACUMULADOS_ITENS
add constraint FK_ACUMULADOS_ITENS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* Constraints */
alter table ACUMULADOS_ITENS
add constraint CK_ACUM_ITENS_PRECO_UNITARIO
check(PRECO_UNITARIO > 0);

alter table ACUMULADOS_ITENS
add constraint CK_ACUM_ITENS_QUANTIDADE
check(QUANTIDADE > 0);

alter table ACUMULADOS_ITENS
add constraint CK_ACUM_ITENS_VALOR_TOTAL
check(VALOR_TOTAL > 0);

alter table ACUMULADOS_ITENS
add constraint CK_ACUM_ITENS_VALOR_TOTAL_DESC
check(VALOR_TOTAL_DESCONTO >= 0);

alter table ACUMULADOS_ITENS
add constraint CK_ACUM_ITE_VALOR_TOT_OUT_DESP
check(VALOR_TOTAL_OUTRAS_DESPESAS >= 0);

alter table ACUMULADOS_ITENS
add constraint CK_ACUM_ITE_VALOR_TOTAL_FRETE
check(VALOR_TOTAL_FRETE >= 0);
