create table ORCAMENTOS_CREDITOS(
  ORCAMENTO_ID        number(10) not null,
  PAGAR_ID            number(12) not null,
  MOMENTO_USO         char(1) default 'F' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table ORCAMENTOS_CREDITOS
add constraint PK_ORCAMENTOS_CREDITOS
primary key(ORCAMENTO_ID, PAGAR_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ORCAMENTOS_CREDITOS
add constraint FK_ORCAMENTOS_CREDITOS_ORC_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table ORCAMENTOS_CREDITOS
add constraint FK_ORCAMENTOS_CREDITOS_PAG_ID
foreign key(PAGAR_ID)
references CONTAS_PAGAR(PAGAR_ID);

/* Checagens */
/*
  MOMENTO_USO
  F - Fechamendo do pedido
  R - Recebimendo do pedido
*/
alter table ORCAMENTOS_CREDITOS
add constraint CK_ORC_CREDITOS_MOMENTO_USO
check(MOMENTO_USO in('F', 'R'));
