﻿create table ORCAMENTOS_PAGAMENTOS(
  ORCAMENTO_ID         number(10) not null,
  COBRANCA_ID          number(3) not null,
  ITEM_ID              number(3) not null,
  TIPO                 char(2) not null,
  DATA_VENCIMENTO      date,
  
  NSU_TEF              varchar2(20),
  CODIGO_AUTORIZACAO   varchar2(50),
  NUMERO_CARTAO        varchar2(19),
  TIPO_RECEB_CARTAO    char(3),
  
  VALOR                number(8,2) not null,
	PARCELA              number(2) not null,
	NUMERO_PARCELAS      number(2) not null
);

alter table ORCAMENTOS_PAGAMENTOS
add constraint PK_ORCAMENTOS_PAGAMENTOS
primary key(ORCAMENTO_ID, COBRANCA_ID, ITEM_ID)
using index tablespace INDICES;

alter table ORCAMENTOS_PAGAMENTOS
add constraint FK_ORCAMENTOS_PAGTOS_ORCAM_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table ORCAMENTOS_PAGAMENTOS
add constraint FK_ORCAMENTOS_PAGTOS_COBRAN_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

alter table ORCAMENTOS_PAGAMENTOS
add constraint CK_ORCAMENTOS_PAGTOS_ITEM_ID
check(ITEM_ID > 0);

alter table ORCAMENTOS_PAGAMENTOS
add constraint CK_ORC_PAGTOS_DATA_VENCIMENTO
check(
  (TIPO = 'CR' and DATA_VENCIMENTO is null and TIPO_RECEB_CARTAO is not null)
  or
  (TIPO in('CO', 'FI') and DATA_VENCIMENTO is not null and TIPO_RECEB_CARTAO is null)
);

/* TIPO
  CR - Cartão
  CO - Cobrança
  FI - Financeira
*/
alter table ORCAMENTOS_PAGAMENTOS
add constraint CK_ORCAMENTOS_PAGAMENTOS_TIPO
check(TIPO in('CR','CO','FI'));

alter table ORCAMENTOS_PAGAMENTOS
add constraint CK_ORCAMENTOS_PAGAMENTOS_VALOR
check(VALOR > 0);

alter table ORCAMENTOS_PAGAMENTOS
add constraint CK_ORC_PAGAMENTOS_PARCELAS
check(PARCELA > 0 and NUMERO_PARCELAS > 0 and PARCELA <= NUMERO_PARCELAS);

alter table ORCAMENTOS_PAGAMENTOS
add constraint CK_ORC_PAGTOS_TIPO_REC_CRT
check(TIPO_RECEB_CARTAO in('null', 'TEF', 'POS'));
