create table ORCAMENTOS_ITENS_DEF_LOCAIS(
  ORCAMENTO_ID           number(10) not null,
  PRODUTO_ID             number(10) not null,
  EMPRESA_ID             number(3,0) not null,
  LOCAL_ID               number(5,0) not null,
  STATUS                 varchar2(3) default 'PEN' not null,
  QUANTIDADE_ATO         number(20,4) default 0 not null,
  QUANTIDADE_RETIRAR     number(20,4) default 0 not null,
  QUANTIDADE_ENTREGAR    number(20,4) default 0 not null,
  ITEM_ID                number(10,0),
  LOTE                   VARCHAR2(80) default '???' not null
);

alter table ORCAMENTOS_ITENS_DEF_LOCAIS
add constraint FK_ORC_ITE_DEF_LOC_QTDE_RET_AT
check(QUANTIDADE_RETIRAR_ATO >= 0);

alter table ORCAMENTOS_ITENS_DEF_LOCAIS
add constraint FK_ORC_ITE_DEF_LOC_QTDE_RETIR
check(QUANTIDADE_RETIRAR >= 0);

alter table ORCAMENTOS_ITENS_DEF_LOCAIS
add constraint FK_ORC_ITE_DEF_LOC_QTDE_ENTREG
check(QUANTIDADE_ENTREGAR >= 0);
