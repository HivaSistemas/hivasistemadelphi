create table DEVOLUCOES_ITENS(
  DEVOLUCAO_ID                   number(10),
  ITEM_ID                        number(3) not null,
  PRODUTO_ID                     number(10) not null,

  DEVOLVIDOS_ENTREGUES           number(20,4) not null,
  DEVOLVIDOS_PENDENCIAS          number(20,4) not null,
  DEVOLVIDOS_SEM_PREVISAO        number(20,4) not null,

  PRECO_UNITARIO                 number(8,2) not null,
  VALOR_BRUTO                    number(8,2) not null,
  VALOR_LIQUIDO                  number(8,2) not null,
  VALOR_DESCONTO                 number(8,2) default 0 not null,
  VALOR_OUTRAS_DESPESAS          number(8,2) default 0 not null,
  VALOR_FRETE                    number(8,2) default 0 not null,
  VALOR_ST                       number(8,2) default 0 not null,
  VALOR_IPI                      number(8,2) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table DEVOLUCOES_ITENS
add constraint PK_DEVOLUCOES_ITENS
primary key(DEVOLUCAO_ID, ITEM_ID)
using index tablespace INDICES;

/* Chave estrangeiras */
alter table DEVOLUCOES_ITENS
add constraint FK_DEV_ITENS_DOVOLUCAO_ID
foreign key(DEVOLUCAO_ID)
references DEVOLUCOES(DEVOLUCAO_ID);

alter table DEVOLUCOES_ITENS
add constraint FK_DEV_ITENS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* Constraints */
alter table DEVOLUCOES_ITENS
add constraint CK_DEVOLUCOES_ITENS_PRECO_UNIT
check(PRECO_UNITARIO > 0);

alter table DEVOLUCOES_ITENS
add constraint CK_DEVOLUCOES_ITENS_VALOR_BRUT
check(VALOR_BRUTO > 0);

alter table DEVOLUCOES_ITENS
add constraint CK_DEVOLUCOES_ITENS_VALOR_LIQU
check(VALOR_LIQUIDO > 0);

alter table DEVOLUCOES_ITENS
add constraint CK_DEVOLUCOES_ITENS_VALOR_DESC
check(VALOR_DESCONTO >= 0);

alter table DEVOLUCOES_ITENS
add constraint CK_DEVOLUCOES_ITE_VAL_OUT_DESP
check(VALOR_OUTRAS_DESPESAS >= 0);

alter table DEVOLUCOES_ITENS
add constraint CK_DEVOLUCOES_ITE_VALOR_FRETE
check(VALOR_FRETE >= 0);

alter table DEVOLUCOES_ITENS
add constraint CK_DEVOLUCOES_ITENS_VALOR_ST
check(VALOR_ST >= 0);

alter table DEVOLUCOES_ITENS
add constraint CK_DEVOLUCOES_ITENS_VALOR_IPI
check(VALOR_IPI >= 0);

alter table DEVOLUCOES_ITENS
add constraint CK_DEV_ITENS_DEVOLV_ENTREGUES
check(DEVOLVIDOS_ENTREGUES >= 0);

alter table DEVOLUCOES_ITENS
add constraint CK_DEV_ITENS_DEVOLVIDOS_PENDEN
check(DEVOLVIDOS_PENDENCIAS >= 0);

alter table DEVOLUCOES_ITENS
add constraint CK_DEV_ITENS_DEVOLV_SEM_PREV
check(DEVOLVIDOS_SEM_PREVISAO >= 0);
