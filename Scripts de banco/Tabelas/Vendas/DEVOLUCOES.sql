create table DEVOLUCOES(
  DEVOLUCAO_ID            number(10),
  ORCAMENTO_ID            number(10) not null,
  EMPRESA_ID              number(3) not null,
  USUARIO_CADASTRO_ID     number(4) not null,
  CLIENTE_CREDITO_ID      number(10),

  VALOR_BRUTO             number(8,2) not null,
  VALOR_LIQUIDO           number(8,2) not null,
  VALOR_OUTRAS_DESPESAS   number(8,2) default 0 not null,
  VALOR_DESCONTO          number(8,2) default 0 not null,
  VALOR_FRETE             number(8,2) default 0 not null,
  VALOR_ST                number(8,2) default 0 not null,
  VALOR_IPI               number(8,2) default 0 not null,

  DEVOLVER_FRETE           char(1) default 'N' not null,
  DEVOLVER_OUTRAS_DESPESAS char(1) default 'N' not null,

  DATA_HORA_DEVOLUCAO     date not null,
  OBSERVACOES             varchar2(200),

  STATUS                  char(1) default 'A' not null,
  USUARIO_CONFIRMACAO_ID  number(4),
  DATA_HORA_CONFIRMACAO   date,
  MOTIVO_DEVOLUCAO        varchar2(400),
  QTDE_DIAS_DEVOLUCAO     number(6,0),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table DEVOLUCOES
add constraint PK_DEVOLUCOES_DEV_ID
primary key(DEVOLUCAO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table DEVOLUCOES
add constraint FK_DEVOLUCOES_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table DEVOLUCOES
add constraint FK_DEVOLUCOES_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table DEVOLUCOES
add constraint FK_DEVOLUCOES_USUARIO_CAD_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table DEVOLUCOES
add constraint FK_DEVOLUCOES_CLI_CRED_ID
foreign key(CLIENTE_CREDITO_ID)
references CLIENTES(CLIENTE_ID);

/* constraints */
alter table DEVOLUCOES
add constraint CK_DEVOLUCOES_VALOR_BRUTO
check(VALOR_BRUTO > 0);

alter table DEVOLUCOES
add constraint CK_DEVOLUCOES_VALOR_LIQUIDO
check(VALOR_LIQUIDO > 0);

alter table DEVOLUCOES
add constraint CK_DEVOLUCOES_VALOR_OUT_DESP
check(VALOR_OUTRAS_DESPESAS >= 0);

alter table DEVOLUCOES
add constraint CK_DEVOLUCOES_VALOR_DESCONTO
check(VALOR_DESCONTO >= 0);

alter table DEVOLUCOES
add constraint CK_DEVOLUCOES_VALOR_FRETE
check(VALOR_FRETE >= 0);

alter table DEVOLUCOES
add constraint CK_DEVOLUCOES_VALOR_ST
check(VALOR_ST >= 0);

alter table DEVOLUCOES
add constraint CK_DEVOLUCOES_VALOR_IPI
check(VALOR_IPI >= 0);

/*
  STATUS
  A - Aberto
  B - Baixado
*/
alter table DEVOLUCOES
add constraint CK_DEVOLUCOES_STATUS
check(
  STATUS = 'A' and DATA_HORA_CONFIRMACAO is null and USUARIO_CONFIRMACAO_ID is null
  or
  STATUS = 'B' and DATA_HORA_CONFIRMACAO is not null and USUARIO_CONFIRMACAO_ID is not null
);

alter table DEVOLUCOES
add constraint CK_DEVOLUCOES_DEV_FRETE
check(DEVOLVER_FRETE in('S', 'N'));

alter table DEVOLUCOES
add constraint CK_DEVOLUCOES_DEV_OUTRAS_DESP
check(DEVOLVER_OUTRAS_DESPESAS in('S', 'N'));
