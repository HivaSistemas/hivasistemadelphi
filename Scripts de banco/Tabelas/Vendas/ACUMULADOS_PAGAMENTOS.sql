﻿create table ACUMULADOS_PAGAMENTOS(
  ACUMULADO_ID         number(10) not null,
  COBRANCA_ID          number(3) not null,
  ITEM_ID              number(3) not null,
  TIPO                 char(2) not null,  
  DATA_VENCIMENTO      date,

  NSU_TEF              varchar2(20),
  CODIGO_AUTORIZACAO   varchar2(50),
  NUMERO_CARTAO        varchar2(19),
  TIPO_RECEB_CARTAO    char(3),

  VALOR                number(8,2) not null,
	PARCELA              number(2) not null,
	NUMERO_PARCELAS      number(2) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID            number(4) not null,
  DATA_HORA_ALTERACAO          date not null,
  ROTINA_ALTERACAO             varchar2(30) not null,
  ESTACAO_ALTERACAO            varchar2(30) not null  
);

/* Chave primaria */
alter table ACUMULADOS_PAGAMENTOS
add constraint PK_ACUMULADOS_PAGAMENTOS
primary key(ACUMULADO_ID, COBRANCA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ACUMULADOS_PAGAMENTOS
add constraint FK_ACUMULADOS_PAGTOS_ACU_ID
foreign key(ACUMULADO_ID)
references ACUMULADOS(ACUMULADO_ID);

alter table ACUMULADOS_PAGAMENTOS
add constraint FK_ACUMULADOS_PAGTOS_COBRAN_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

/* Constraints */
alter table ACUMULADOS_PAGAMENTOS
add constraint CK_ACUMULADOS_PAGTOS_ITEM_ID
check(ITEM_ID > 0);

alter table ACUMULADOS_PAGAMENTOS
add constraint CK_ACU_PAGTOS_DATA_VENCIMENTO
check(
  (TIPO = 'CR' and DATA_VENCIMENTO is null and TIPO_RECEB_CARTAO is not null)
  or
  (TIPO in('CO', 'FI') and DATA_VENCIMENTO is not null and TIPO_RECEB_CARTAO is null)
);

/* TIPO
  CR - Cartão
  CO - Cobrança
  FI - Financeira
*/
alter table ACUMULADOS_PAGAMENTOS
add constraint CK_ACUMULADOS_PAGAMENTOS_TIPO
check(TIPO in('CR','CO','FI'));

alter table ACUMULADOS_PAGAMENTOS
add constraint CK_ACUMULADOS_PAGAMENTOS_VALOR
check(VALOR > 0);

alter table ACUMULADOS_PAGAMENTOS
add constraint CK_ACU_PAGAMENTOS_PARCELAS
check(PARCELA > 0 and NUMERO_PARCELAS > 0 and PARCELA <= NUMERO_PARCELAS);

alter table ACUMULADOS_PAGAMENTOS
add constraint CK_ACU_PAG_TIPO_REC_CARTAO
check(TIPO_RECEB_CARTAO in(null, 'TEF', 'POS'));
