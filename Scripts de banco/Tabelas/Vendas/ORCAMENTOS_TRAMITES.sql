create table ORCAMENTOS_TRAMITES(
  ORCAMENTO_ID   number(10),
  STATUS         char(3) not null,
  VALOR_TOTAL    number(8,2) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table ORCAMENTOS_TRAMITES
add constraint PK_ORCAMENTOS_TRAMITES
primary key(ORCAMENTO_ID, DATA_HORA_ALTERACAO, STATUS)
using index tablespace INDICES;

/* Foreigns keys */
alter table ORCAMENTOS_TRAMITES
add constraint FK_ORCAMENTOS_TRAMITES_ORC_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

/* Constraints */

/* STATUS
  OE - Orcamento em aberto
  OB - Orcamento bloqueado  
  VB - Venda bloqueada
  VR - Venda aguardando recebimento
  VE - Venda ag. recebimento entrega
  RE - Recebido
  CA - Cancelado
*/
alter table ORCAMENTOS_TRAMITES
add constraint CK_ORCAMENTOS_TRAMITES_STATUS
check(STATUS in('OE','OB','VB','VR','VE','RE','CA'));

alter table ORCAMENTOS_TRAMITES
add constraint CK_ORCAMENTOS_TRAMIT_VALOR_TOT
check(VALOR_TOTAL > 0);