﻿create table ORCAMENTOS(
  ORCAMENTO_ID                    number(10),
  EMPRESA_ID                      number(3) not null,
  CLIENTE_ID                      number(10) not null,
  DATA_CADASTRO                   date default trunc(sysdate) not null,
  VENDEDOR_ID                     number(4),
  CONDICAO_ID                     number(3) not null,
  ACUMULADO_ID                    number(10),
  STATUS                          char(2) default 'OE' not null,
  INDICE_OVER_PRICE               number(5,4) default 1 not null,
  VALOR_TOTAL_PRODUTOS            number(8,2) not null,
  VALOR_TOTAL_PRODUTOS_PROMOCAO   number(8,2) default 0 not null,
  VALOR_TOTAL                     number(8,2) not null, /* Total líquido a ser pago pelo cliente */
  VALOR_OUTRAS_DESPESAS           number(8,2) not null,
  VALOR_DESCONTO                  number(8,2) not null,
  VALOR_DEV_ACUMULADO_ABERTO      number(8,2) default 0 not null,
  VALOR_ADIANTADO_ACUMULADO_ABER  number(8,2) default 0 not null,
  TURNO_ID                        number(10),
  ORIGEM_VENDA                    char(1) default 'N' not null,
  TIPO_ENTREGA                    char(2) default 'RA' not null,
  INDICE_CONDICAO_PAGAMENTO       number(5,4) default 1 not null,
  RECEBER_NA_ENTREGA              char(1) default 'N' not null,
  STATUS_NF_SIMPLES_FATURAMENTO   char(2) default 'NE' not null,

  PEDIDO_LIBERADO                 CHAR(1) DEFAULT 'N' NOT NULL,
  PERCENTUAL_LIBERADO             NUMBER(8,2) DEFAULT 0 NOT NULL,

  OBSERVACOES_CAIXA               varchar2(800),
  OBSERVACOES_EXPEDICAO           varchar2(800),
  OBSERVACOES_NFE                 varchar2(800),
  OBSERVACOES_TITULOS             varchar2(800),

  NOME_CONSUMIDOR_FINAL           varchar2(50),
  CPF_CONSUMIDOR_FINAL            varchar2(14),
  TELEFONE_CONSUMIDOR_FINAL       varchar2(15),
  USUARIO_CADASTRO_ID             number(4) not null,
  DATA_HORA_RECEBIMENTO           date,

  ORC_POR_AMBIENTE                char(1) default 'N' not null,

  /* Abaixo os tipos de recebimentos que podem ser efetuados na hora da venda */
  VALOR_DINHEIRO                  number(8,2) default 0 not null,
  VALOR_CHEQUE                    number(8,2) default 0 not null,
  VALOR_CARTAO_DEBITO             number(8,2) default 0 not null,
  VALOR_CARTAO_CREDITO            number(8,2) default 0 not null,
  VALOR_COBRANCA                  number(8,2) default 0 not null,
  VALOR_PIX                       number(8,2) default 0 not null,
  VALOR_ACUMULATIVO               number(8,2) default 0 not null,
  VALOR_FINANCEIRA                number(8,2) default 0 not null,
  VALOR_CREDITO                   number(8,2) default 0 not null,
  VALOR_TROCO                     number(8,2) default 0 not null,

  TIPO_ANALISE_CUSTO              char(1) not null,

  VALOR_FRETE                     number(8,2) default 0 not null,
  VALOR_FRETE_ITENS               number(8,2) default 0 not null,
  DATA_ENTREGA                    date,
  HORA_ENTREGA                    date,

  LOGRADOURO                      varchar2(100),
  COMPLEMENTO                     varchar2(100),
  NUMERO                          varchar2(10),
  PONTO_REFERENCIA                varchar2(100),
  BAIRRO_ID                       number(15),
  CEP                             varchar2(9),
  INSCRICAO_ESTADUAL              varchar2(20),

  INDICE_DESCONTO_VENDA_ID        number(1),
  STATUS_RECIBO_PAGAMENTO         char(2) default 'NI' not null,
  PROFISSIONAL_ID                 number(10),
  BAIXA_RECEBER_PIX_ID            number(10),

  TIPO_ACOMPANHAMENTO_ID          number(3),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table ORCAMENTOS
add constraint PK_ORCAMENTOS_ORCAMENTO_ID
primary key(ORCAMENTO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ORCAMENTOS
add constraint FK_ORCAMENTOS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table ORCAMENTOS
add constraint FK_TIPO_ACO_ORC_ID_ID
foreign key(TIPO_ACOMPANHAMENTO_ID)
references TIPO_ACOMPANHAMENTO_ORCAMENTO(TIPO_ACOMPANHAMENTO_ID);

alter table ORCAMENTOS
add constraint FK_ORCAMENTOS_CLIENTE_ID
foreign key(CLIENTE_ID)
references CLIENTES(CADASTRO_ID);

alter table ORCAMENTOS
add constraint FK_ORCAMENTOS_VENDEDOR_ID
foreign key(VENDEDOR_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table ORCAMENTOS
add constraint FK_ORCAMENTOS_CONDICAO_ID
foreign key(CONDICAO_ID)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

alter table ORCAMENTOS
add constraint FK_ORCAMENTOS_TURNO_ID
foreign key(TURNO_ID)
references TURNOS_CAIXA(TURNO_ID);

alter table ORCAMENTOS
add constraint FK_ORCAMENTOS_USUARIO_CAD_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table ORCAMENTOS
add constraint FK_ORCAMENTOS_BAIRRO_ID
foreign key(BAIRRO_ID)
references BAIRROS(BAIRRO_ID);

alter table ORCAMENTOS
add constraint FK_ORCAMENTOS_ACUMULADO_ID
foreign key(ACUMULADO_ID)
references ACUMULADOS(ACUMULADO_ID);

alter table ORCAMENTOS
add constraint FK_ORC_IND_DESC_VENDA_ID
foreign key(INDICE_DESCONTO_VENDA_ID)
references INDICES_DESCONTOS_VENDA(INDICE_ID);

alter table ORCAMENTOS
add constraint FK_ORCAMENTOS_PROFISSIONAL_ID
foreign key(PROFISSIONAL_ID)
references PROFISSIONAIS(CADASTRO_ID);

alter table ORCAMENTOS
add constraint FK_ORCAMENTOS_BX_REC_PIX_ID
foreign key(BAIXA_RECEBER_PIX_ID)
references CONTAS_RECEBER_BAIXAS(BAIXA_ID);

/* Constraints */
alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_IND_OVER_PRICE
check(INDICE_OVER_PRICE >= 1);

/* STATUS
  OE - Orcamento em aberto
  OB - Orcamento bloqueado
  VB - Venda bloqueada
  VR - Venda aguardando recebimento
  VE - Venda aguardando receb.entrega
  RE - Recebido
  CA - Cancelado
*/
alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_STATUS
check(STATUS in('OE','OB','VB','VR','VE','RE','CA'));

/* ORIGEM_VENDA
  N - Normal, SIGO_VAREJO
  P - PDV, SIGO_PDV
*/
alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_ORIGEM_VENDA
check(ORIGEM_VENDA in('N', 'P'));

/* TIPO_ENTREGA
  RA - Retirar no ato
  RE - A retirar
  EN - A entregar
  SP - Sem previsão
  SE - Sem previsão a entregar
*/
alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_TIPO_ENTREGA
check(TIPO_ENTREGA in('RA', 'RE', 'EN', 'SP', 'SE'));

alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_IND_COND_PAGTO
check(INDICE_CONDICAO_PAGAMENTO > 0);

alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_REC_NA_ENTREGA
check(RECEBER_NA_ENTREGA in('S', 'N'));

/*
  STATUS_NF_SIMPLES_FATURAMENTO
  NE - Não emitir
  EM - Emitida
*/
alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_STATUS_NF_SIM_FAT
check(STATUS_NF_SIMPLES_FATURAMENTO in('S', 'N'));

alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_STATUS_TURNO_ID
check(
  STATUS in('OE','OB','VB','VE','VR','CA') and TURNO_ID is null and DATA_HORA_RECEBIMENTO is null
  or
  STATUS = 'RE' and TURNO_ID is not null and DATA_HORA_RECEBIMENTO is not null
);

alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_PAGAMENTOS
check(
  STATUS = 'RE' and (VALOR_DINHEIRO + VALOR_PIX + VALOR_CHEQUE + VALOR_CARTAO_DEBITO + VALOR_CARTAO_CREDITO + VALOR_COBRANCA + VALOR_ACUMULATIVO + VALOR_FINANCEIRA + VALOR_CREDITO > 0)
  or
  (STATUS in('OE','OB','VB','VE','VR','CA')) and (VALOR_DINHEIRO + VALOR_PIX + VALOR_CHEQUE + VALOR_CARTAO_DEBITO + VALOR_CARTAO_CREDITO + VALOR_COBRANCA + VALOR_ACUMULATIVO + VALOR_FINANCEIRA + VALOR_CREDITO >= 0)
);

alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_VALORES
check(
  VALOR_TOTAL > 0 and
  VALOR_DESCONTO >= 0 and
  VALOR_TOTAL_PRODUTOS > 0 and
  VALOR_OUTRAS_DESPESAS >= 0 and
  VALOR_CHEQUE >= 0 and
  VALOR_CARTAO_DEBITO >= 0 and
  VALOR_CARTAO_CREDITO >= 0 and
  VALOR_DINHEIRO >= 0 and
  VALOR_COBRANCA >= 0 and
  VALOR_ACUMULATIVO >= 0 and
  VALOR_FINANCEIRA >= 0 and
  VALOR_CREDITO >= 0
);

alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_VLR_TOT_PROD_PRO
check(VALOR_TOTAL_PRODUTOS_PROMOCAO >= 0);

alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_VLR_ADI_ACU_ABER
check(VALOR_ADIANTADO_ACUMULADO_ABER >= 0);

/* TIPO_ANALISE_CUSTO
  A - Emitir somente documento fiscal ( Movimenta apenas o estoque fiscal )
  B - Somente faturar ( Movimenta apenas o estoque real + Geração financeiro )
  C - A + B
*/
alter table ORCAMENTOS
add constraint CK_ORC_TIPO_ANALISE_CUSTO
check(TIPO_ANALISE_CUSTO in('A', 'B', 'C'));

alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_VALOR_FRETE
check(
  VALOR_FRETE = 0
  or
  TIPO_ENTREGA in('EN', 'SE') and VALOR_FRETE >= 0
);

alter table ORCAMENTOS
add constraint CK_ORC_VALOR_FRETE_ITENS
check(
  VALOR_FRETE_ITENS = 0
  or
  TIPO_ENTREGA in('EN', 'SE') and VALOR_FRETE_ITENS >= 0
);

/* STATUS_RECIBO_PAGAMENTO
 NI - Não imprimir
 AG - Aguardar recebimento TEF
 IP - Imprimir
 IM - Impresso
*/
alter table ORCAMENTOS
add constraint CK_ORC_STATUS_RECIBO_PAGTO
check(STATUS_RECIBO_PAGAMENTO in('NI','AG','IP','IM'));

alter table ORCAMENTOS
add constraint CK_ORCAMENTOS_VALOR_TROCO
check(VALOR_TROCO >= 0);


// alter table ORCAMENTOS add (ORC_POR_AMBIENTE char(1) default 'N' not null )


/*
ALTER TABLE ORCAMENTOS ADD (
  PEDIDO_LIBERADO CHAR(1) DEFAULT 'N' NOT NULL,
  PERCENTUAL_LIBERADO NUMBER(8,2) DEFAULT 0 NOT NULL
)
*/
