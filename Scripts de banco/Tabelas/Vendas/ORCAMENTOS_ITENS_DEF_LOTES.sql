create table ORCAMENTOS_ITENS_DEF_LOTES(
  ORCAMENTO_ID           number(10) not null,
  ITEM_ID                number(10) not null,
  LOTE                   varchar2(80) not null,
  QUANTIDADE_RETIRAR_ATO number(20,4) default 0 not null,
  QUANTIDADE_RETIRAR     number(20,4) default 0 not null,
  QUANTIDADE_ENTREGAR    number(20,4) default 0 not null
);

/* Chave primaria */
alter table ORCAMENTOS_ITENS_DEF_LOTES
add constraint PK_ORCAMENTOS_ITENS_DEF_LOTES
primary key(ORCAMENTO_ID, ITEM_ID, LOTE)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ORCAMENTOS_ITENS_DEF_LOTES
add constraint FK_ORC_ITE_DEF_LOT_ORC_ITEM_ID
foreign key(ORCAMENTO_ID, ITEM_ID)
references ORCAMENTOS_ITENS(ORCAMENTO_ID, ITEM_ID);

/* Constraints */
alter table ORCAMENTOS_ITENS_DEF_LOTES
add constraint FK_ORC_ITE_DEF_LOT_QTDE_RET_AT
check(QUANTIDADE_RETIRAR_ATO >= 0);

alter table ORCAMENTOS_ITENS_DEF_LOTES
add constraint FK_ORC_ITE_DEF_LOT_QTDE_RETIR
check(QUANTIDADE_RETIRAR >= 0);

alter table ORCAMENTOS_ITENS_DEF_LOTES
add constraint FK_ORC_ITE_DEF_LOT_QTDE_ENTREG
check(QUANTIDADE_ENTREGAR >= 0);