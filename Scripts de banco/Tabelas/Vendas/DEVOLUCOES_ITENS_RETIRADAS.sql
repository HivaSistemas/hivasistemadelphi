create table DEVOLUCOES_ITENS_RETIRADAS(
  DEVOLUCAO_ID           number(10) not null,
  RETIRADA_ID            number(12) not null,
  ITEM_ID                number(3) not null,
  LOTE                   varchar2(80) not null,
  LOCAL_DEVOLUCAO_ID     number(5) not null,      
  QUANTIDADE             number(20,4) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table DEVOLUCOES_ITENS_RETIRADAS
add constraint PK_DEVOLUCOES_ITENS_RETIRADAS
primary key(DEVOLUCAO_ID, RETIRADA_ID, ITEM_ID, LOTE)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table DEVOLUCOES_ITENS_RETIRADAS
add constraint FK_DEV_ITE_RET_DEVOL_ITEM_ID
foreign key(DEVOLUCAO_ID, ITEM_ID)
references DEVOLUCOES_ITENS(DEVOLUCAO_ID, ITEM_ID);

alter table DEVOLUCOES_ITENS_RETIRADAS
add constraint FK_DEV_ITE_RET_RET_ITEM_ID
foreign key(RETIRADA_ID, ITEM_ID, LOTE)
references RETIRADAS_ITENS(RETIRADA_ID, ITEM_ID, LOTE);

alter table DEVOLUCOES_ITENS_RETIRADAS
add constraint FK_DEV_ITE_RET_LOCAL_DEVOL_ID
foreign key(LOCAL_DEVOLUCAO_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

/* Checagens */
alter table DEVOLUCOES_ITENS_RETIRADAS
add constraint CK_DEV_ITENS_RET_QUANTIDADE
check(QUANTIDADE > 0);