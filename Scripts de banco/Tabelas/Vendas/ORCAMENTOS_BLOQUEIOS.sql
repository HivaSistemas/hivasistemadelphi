create table ORCAMENTOS_BLOQUEIOS(
  ORCAMENTO_ID         number(10),
  ITEM_ID              number(3) not null,
  DESCRICAO            varchar2(1000) not null,  
  DATA_HORA_LIBERACAO  date,
  USUARIO_LIBERACAO_ID number(4)  
);

alter table ORCAMENTOS_BLOQUEIOS
add constraint PK_ORCAMENTOS_BLOQUEIOS
primary key(ORCAMENTO_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ORCAMENTOS_BLOQUEIOS
add constraint FK_ORC_BLOQUEIOS_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table ORCAMENTOS_BLOQUEIOS
add constraint FK_ORC_BLOQUEIOS_USU_LIBER_ID
foreign key(USUARIO_LIBERACAO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID); 

/* Checagens */
alter table ORCAMENTOS_BLOQUEIOS
add constraint CK_ORC_BLOQUEIOS_DATA_LIB
check(
  (DATA_HORA_LIBERACAO is null and USUARIO_LIBERACAO_ID is null)
  or
  (DATA_HORA_LIBERACAO is not null and USUARIO_LIBERACAO_ID is not null)
);