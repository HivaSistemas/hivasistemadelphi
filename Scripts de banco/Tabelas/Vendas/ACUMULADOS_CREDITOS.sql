create table ACUMULADOS_CREDITOS(
  ACUMULADO_ID number(10) not null,
  PAGAR_ID     number(12) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table ACUMULADOS_CREDITOS
add constraint PK_ACUMULADOS_CREDITOS
primary key(ACUMULADO_ID, PAGAR_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ACUMULADOS_CREDITOS
add constraint FK_ACUMULADOS_CREDITOS_ACUM_ID
foreign key(ACUMULADO_ID)
references ACUMULADOS(ACUMULADO_ID);

alter table ACUMULADOS_CREDITOS
add constraint FK_ACUMULADOS_CREDITOS_PAG_ID
foreign key(PAGAR_ID)
references CONTAS_PAGAR(PAGAR_ID);