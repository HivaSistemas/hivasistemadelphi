create table ACUMULADOS_ORCAMENTOS(
  ACUMULADO_ID       number(10) not null,
  ORCAMENTO_ID       number(10) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table ACUMULADOS_ORCAMENTOS
add constraint PK_ACUMULADOS_ORCAMENTOS
primary key(ACUMULADO_ID, ORCAMENTO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ACUMULADOS_ORCAMENTOS
add constraint FK_ACUMULADOS_ORC_ACUMULADO_ID
foreign key(ACUMULADO_ID)
references ACUMULADOS(ACUMULADO_ID);

alter table ACUMULADOS_ORCAMENTOS
add constraint FK_ACUMULADOS_ORC_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);