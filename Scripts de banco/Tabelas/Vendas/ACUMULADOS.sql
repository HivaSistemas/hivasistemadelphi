﻿create table ACUMULADOS(
  ACUMULADO_ID                  number(10) not null,
  EMPRESA_ID                    number(3) not null,
  CLIENTE_ID                    number(10) not null,
  CONDICAO_ID                   number(3) not null,
  TURNO_ID                      number(10),
  
  DATA_HORA_FECHAMENTO          date not null,
  USUARIO_FECHAMENTO_ID         number(4) not null,
  
  DATA_HORA_RECEBIMENTO         date,
  USUARIO_RECEBIMENTO_ID        number(4),

  DATA_HORA_CANCELAMENTO        date,
  USUARIO_CANCELAMENTO_ID       number(4),
  
  STATUS                        char(2) not null,
  TIPO_NOTA_GERAR               char(2) default 'NI' not null,
  
  VALOR_TOTAL_PRODUTOS          number(8,2) not null,  
  VALOR_TOTAL                   number(8,2) not null, /* Total líquido a ser pago pelo cliente */
  VALOR_OUTRAS_DESPESAS         number(8,2) not null,
  VALOR_DESCONTO                number(8,2) not null,
  VALOR_FRETE                   number(8,2) default 0 not null,
  VALOR_JUROS                   number(8,2) default 0 not null,
  INDICE_PEDIDOS                number(9,8) default 1 not null,
  TEM_PRODUTOS                  char(1) default 'S' not null,
  
  /* Abaixo os tipos de recebimentos que podem ser efetuados na hora do fechamento do acumulado */
  VALOR_DINHEIRO                number(8,2) default 0 not null,
  VALOR_PIX                     number(8,2) default 0 not null,
  VALOR_CHEQUE                  number(8,2) default 0 not null,
  VALOR_CARTAO_DEBITO           number(8,2) default 0 not null,
  VALOR_CARTAO_CREDITO          number(8,2) default 0 not null,
  VALOR_COBRANCA                number(8,2) default 0 not null,
  VALOR_FINANCEIRA              number(8,2) default 0 not null,
  VALOR_CREDITO                 number(8,2) default 0 not null,
  VALOR_TROCO                   number(8,2) default 0 not null,
  VALOR_PIX                     number(8,2) default 0 not null,
  
  OBSERVACOES_NFE               varchar2(800),

  /* Endereço da capa da nota fiscal */
  LOGRADOURO                   varchar2(100) not null,
  COMPLEMENTO                  varchar2(100) not null,
  NUMERO                       varchar2(10) not null,
  PONTO_REFERENCIA             varchar2(100),
  BAIRRO_ID                    number(15) not null,
  CEP                          varchar2(9) not null,
  INSCRICAO_ESTADUAL           varchar2(20) not null,

  INDICE_DESCONTO_VENDA_ID     number(1),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID            number(4) not null,
  DATA_HORA_ALTERACAO          date not null,
  ROTINA_ALTERACAO             varchar2(30) not null,
  ESTACAO_ALTERACAO            varchar2(30) not null
);

/* Chave primária */
alter table ACUMULADOS
add constraint PK_ACUMULADOS
primary key(ACUMULADO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ACUMULADOS
add constraint FK_ACUMULADOS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table ACUMULADOS
add constraint FK_ACUMULADOS_CLIENTE_ID
foreign key(CLIENTE_ID)
references CLIENTES(CADASTRO_ID);

alter table ACUMULADOS
add constraint FK_ACUMULADOS_CONDICAO_ID
foreign key(CONDICAO_ID)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

alter table ACUMULADOS
add constraint FK_ACUMULADOS_TURNO_ID
foreign key(TURNO_ID)
references TURNOS_CAIXA(TURNO_ID);

alter table ACUMULADOS
add constraint FK_ACUMULADOS_USUARIO_FECH_ID
foreign key(USUARIO_FECHAMENTO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table ACUMULADOS
add constraint FK_ACUMULADOS_USUARIO_RECEB_ID
foreign key(USUARIO_RECEBIMENTO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table ACUMULADOS
add constraint FK_ACUMULADOS_USUARIO_CANC_ID
foreign key(USUARIO_CANCELAMENTO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table ACUMULADOS
add constraint FK_ACUMULADOS_BAIRRO_ID
foreign key(BAIRRO_ID)
references BAIRROS(BAIRRO_ID);

alter table ACUMULADOS
add constraint FK_ACUMULADOS_IND_DESC_VEN_ID
foreign key(INDICE_DESCONTO_VENDA_ID)
references INDICES_DESCONTOS_VENDA(INDICE_ID);

/* Constraints */

/* TIPO_NOTA_GERAR
  NI - Não imprimir
  NE - NFCe
  NF - NFe
*/
alter table ACUMULADOS
add constraint CK_ACUMULADOS_TIPO_NF_GERAR
check(TIPO_NOTA_GERAR in('NI', 'NE', 'NF'));

/* 
  STATUS
  CA - Cancelado
  AR - Aguardando recebimento
  RE - Recebido
*/
alter table ACUMULADOS
add constraint CK_ACUMULADOS_STATUS
check(
  (STATUS = 'AR' and USUARIO_RECEBIMENTO_ID is null and DATA_HORA_RECEBIMENTO is null)
  or
  (STATUS = 'RE' and USUARIO_RECEBIMENTO_ID is not null and DATA_HORA_RECEBIMENTO is not null)
  or
  (STATUS = 'CA') and USUARIO_CANCELAMENTO_ID is not null and DATA_HORA_CANCELAMENTO is not null)
);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_PRODUTOS
check(VALOR_TOTAL_PRODUTOS >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_TOTAL
check(VALOR_TOTAL >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_OUT_DESP
check(VALOR_OUTRAS_DESPESAS >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_DESCONTO
check(VALOR_DESCONTO >= 0);
            
alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_FRETE
check(VALOR_FRETE >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_JUROS
check(VALOR_JUROS >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_INDICE_PEDIDOS
check(INDICE_PEDIDOS >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_TEM_PRODUTOS
check(TEM_PRODUTOS in('S', 'N'));

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_DINHEIRO
check(VALOR_DINHEIRO >= 0);
                    
alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_CHEQUE
check(VALOR_CHEQUE >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_CARTAO_DEB
check(VALOR_CARTAO_DEBITO >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_CARTAO_CRE
check(VALOR_CARTAO_CREDITO >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_COBRANCA
check(VALOR_COBRANCA >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_FINANCEIRA
check(VALOR_FINANCEIRA >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_CREDITO
check(VALOR_CREDITO >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALOR_TROCO
check(VALOR_TROCO >= 0);

alter table ACUMULADOS
add constraint CK_ACUMULADOS_VALORES_PAGTO
check(
  VALOR_TOTAL <= VALOR_DINHEIRO + VALOR_CHEQUE + VALOR_CARTAO_DEBITO + VALOR_CARTAO_CREDITO + VALOR_COBRANCA + VALOR_FINANCEIRA + VALOR_CREDITO + VALOR_PIX
);
