create table ORCAMENTOS_ITENS_AMBIENTE(
  ORCAMENTO_ID                    number(10) not null,
  PRODUTO_ID                      number(10) not null,
  AMBIENTE                        varchar2(200) not null,
  QUANTIDADE                      number(10) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);