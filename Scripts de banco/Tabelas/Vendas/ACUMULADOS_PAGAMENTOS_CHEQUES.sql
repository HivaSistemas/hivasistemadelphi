create table ACUMULADOS_PAGAMENTOS_CHEQUES(
  ACUMULADO_ID    	number(10) not null,
  COBRANCA_ID     	number(3) not null,
  ITEM_ID         	number(3) not null,
  DATA_VENCIMENTO 	date not null,
  BANCO           	varchar2(5) not null,
  AGENCIA         	varchar2(8) not null,
  CONTA_CORRENTE  	varchar2(8) not null,
  NUMERO_CHEQUE   	number(10) not null,
  VALOR_CHEQUE    	number(8,2) not null,
  NOME_EMITENTE     varchar2(80),
  TELEFONE_EMITENTE varchar2(15),
	PARCELA           number(2) not null,
	NUMERO_PARCELAS   number(2) not null,
  CPF_CNPJ_EMITENTE varchar2(19)
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID            number(4) not null,
  DATA_HORA_ALTERACAO          date not null,
  ROTINA_ALTERACAO             varchar2(30) not null,
  ESTACAO_ALTERACAO            varchar2(30) not null    
);

/* Chave primaria */
alter table ACUMULADOS_PAGAMENTOS_CHEQUES
add constraint PK_ACUMULADOS_PAGAMENTOS_CHEQ
primary key(ACUMULADO_ID, COBRANCA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ACUMULADOS_PAGAMENTOS_CHEQUES
add constraint FK_ACUMULADOS_PAGTO_CHQ_ACU_ID
foreign key(ACUMULADO_ID)
references ACUMULADOS(ACUMULADO_ID);

alter table ACUMULADOS_PAGAMENTOS_CHEQUES
add constraint FK_ACUMULADOS_PAGTO_CHQ_COB_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

/* Checagens */
alter table ACUMULADOS_PAGAMENTOS_CHEQUES
add constraint CK_ACUMULADOS_PAGT_CHQ_ITEM_ID
check(ITEM_ID > 0);

alter table ACUMULADOS_PAGAMENTOS_CHEQUES
add constraint CK_ACUMULADOS_PAGTOS_CHQ_VALOR
check(VALOR_CHEQUE > 0);

alter table ACUMULADOS_PAGAMENTOS_CHEQUES
add constraint CK_ACU_PAGTOS_CHQ_PARCELAS
check(PARCELA > 0 and NUMERO_PARCELAS > 0 and PARCELA <= NUMERO_PARCELAS);