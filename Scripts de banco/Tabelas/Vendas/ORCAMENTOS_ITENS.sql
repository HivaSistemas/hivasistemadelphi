create table ORCAMENTOS_ITENS(
  ORCAMENTO_ID                    number(10) not null,
  PRODUTO_ID                      number(10) not null,
  ITEM_ID                         number(3) not null,
  ITEM_KIT_ID                     number(3),

  VALOR_TOTAL                     number(8,2) not null,
  VALOR_TOTAL_DESCONTO            number(8,2) not null,
  VALOR_TOTAL_OUTRAS_DESPESAS     number(8,2) not null,
  VALOR_TOTAL_FRETE               number(8,2) default 0 not null,
  VALOR_ORIGINAL                  number(8,2) default 0 not null,
  
  TIPO_CONTROLE_ESTOQUE           char(1) default 'N' not null,
  QUANTIDADE                      number(20,4) not null,  
  
  QUANTIDADE_RETIRAR_ATO          number(20,4) default 0 not null,
  QUANTIDADE_RETIRAR              number(20,4) default 0 not null,
  QUANTIDADE_ENTREGAR             number(20,4) default 0 not null,
  QUANTIDADE_SEM_PREVISAO         number(20,4) default 0 not null,

  QUANTIDADE_RETIRADOS            number(20,4) default 0 not null,
  QUANTIDADE_ENTREGUES            number(20,4) default 0 not null,

  QUANTIDADE_DEVOLV_ENTREGUES     number(20,4) default 0 not null,
  QUANTIDADE_DEVOLV_PENDENTES     number(20,4) default 0 not null,
  QUANTIDADE_DEVOLV_SEM_PREVISAO  number(20,4) default 0 not null,
  
  PRECO_BASE                      number(8,2) not null,
  PRECO_UNITARIO                  number(8,2) not null,
  
  TIPO_PRECO_UTILIZADO            char(3) default 'VAR' not null,
  PERC_DESCONTO_PRECO_MANUAL      number(8,5) default 0 not null,
  VALOR_DESC_UNIT_PRECO_MANUAL    number(8,2) default 0 not null,

  /* CUSTOS */
  PRECO_FINAL                     number(12,4) default 0 not null,
  PRECO_FINAL_MEDIO               number(12,4) default 0 not null,
  PRECO_LIQUIDO                   number(12,4) default 0 not null,
  PRECO_LIQUIDO_MEDIO             number(12,4) default 0 not null,
  CMV                             number(14,4) default 0 not null,
  CUSTO_ULTIMO_PEDIDO             number(12,4) default 0 not null,
  CUSTO_PEDIDO_MEDIO              number(12,4) default 0 not null,

  VALOR_IMPOSTOS                  number(10,2) default 0 not null,
  VALOR_ENCARGOS                  number(10,2) default 0 not null,
  VALOR_CUSTO_VENDA               number(10,2) default 0 not null,
  VALOR_CUSTO_FINAL               number(10,2) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table ORCAMENTOS_ITENS
add constraint PK_ORCAMENTOS_ITENS
primary key(ORCAMENTO_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ORCAMENTOS_ITENS
add constraint FK_ORCAMENTOS_ITENS_ORCAMEN_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table ORCAMENTOS_ITENS
add constraint FK_ORCAMENTOS_ITENS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* Constraints */
alter table ORCAMENTOS_ITENS
add constraint CK_ORCAMENTOS_ITENS_ITEM_ID
check(ITEM_ID > 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORCAMENTOS_ITENS_VALOR_TOT
check(VALOR_TOTAL > 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_VALOR_TOTAL_DESC
check(VALOR_TOTAL_DESCONTO >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_VAL_TOT_OUT_DESP
check(VALOR_TOTAL_OUTRAS_DESPESAS >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_VALOR_TOTAL_FRETE
check(VALOR_TOTAL_FRETE >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORCAMENTOS_ITENS_QUANTIDADE
check(QUANTIDADE > 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_QTDE_RETIRAR_ATO
check(QUANTIDADE_RETIRAR_ATO >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_QTDE_RETIRAR
check(QUANTIDADE_RETIRAR >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_QTDE_ENTREGAR
check(QUANTIDADE_ENTREGAR >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_QTDE_SEM_PREVISAO
check(QUANTIDADE_SEM_PREVISAO >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_QTDE_DEV_ENTREG
check(QUANTIDADE_DEVOLV_ENTREGUES >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_QTDE_DEV_PENDENT
check(QUANTIDADE_DEVOLV_PENDENTES >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_QTDE_DEV_SEM_PREV
check(QUANTIDADE_DEVOLV_SEM_PREVISAO >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_QUANTIDADES
check(
  QUANTIDADE >=
  QUANTIDADE_RETIRAR_ATO +
  QUANTIDADE_RETIRAR +
  QUANTIDADE_ENTREGAR +
  QUANTIDADE_SEM_PREVISAO +

  QUANTIDADE_RETIRADOS +
  QUANTIDADE_ENTREGUES +

  QUANTIDADE_DEVOLV_ENTREGUES +
  QUANTIDADE_DEVOLV_PENDENTES +
  QUANTIDADE_DEVOLV_SEM_PREVISAO
);
  
alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_QTDE_RETIRADOS
check(QUANTIDADE_RETIRADOS >= 0);
            
alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_QTDE_ENTREGUES
check(QUANTIDADE_ENTREGUES >= 0);
          
alter table ORCAMENTOS_ITENS
add constraint CK_ORCAMENTOS_ITENS_PRECO_UNIT
check(PRECO_UNITARIO > 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORCAMENTOS_ITENS_PRECO_BASE
check(
  PRECO_BASE > 0
  and (
    (PRECO_BASE <= PRECO_UNITARIO)
    or
    (TIPO_PRECO_UTILIZADO = 'MAN' and PRECO_BASE <= PRECO_UNITARIO + VALOR_DESC_UNIT_PRECO_MANUAL)
  )
);

alter table ORCAMENTOS_ITENS
add constraint CK_ORCAM_ITENS_UTIL_PRECO_PROM
check(UTILIZOU_PRECO_PROMOCAO in('S','N'));

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_TIPO_CONTROLE_EST
check(TIPO_CONTROLE_ESTOQUE in('N', 'L', 'P', 'G', 'K', 'A', 'I'));

/*
  TIPO_PRECO_UTILILIZADO
  VAR - Varejo
  PRO - Promocional
  AT1 - Atacado 1
  AT2 - Atacado 2
  AT3 - Atacado 3
  PDV
  PES - Ponta de estoque
  MAN - Manualmente
*/
alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_TIPO_PRECO_UTIL
check(TIPO_PRECO_UTILIZADO in('VAR', 'PRO', 'AT1', 'AT2', 'AT3', 'PDV', 'PES', 'MAN'));

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_PERC_DESC_PR_MAN
check(
  PERC_DESCONTO_PRECO_MANUAL = 0
  or
  (TIPO_PRECO_UTILIZADO = 'MAN' and PERC_DESCONTO_PRECO_MANUAL <> 0 and VALOR_DESC_UNIT_PRECO_MANUAL <> 0)
);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_PRECO_FINAL
check(PRECO_FINAL >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_PRECO_FIN_MED
check(PRECO_FINAL_MEDIO >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_PRECO_LIQUIDO
check(PRECO_LIQUIDO >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_PRECO_LIQ_MEDIO
check(PRECO_LIQUIDO_MEDIO >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_CMV
check(CMV >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_CUSTO_ULT_PEDIDO
check(CUSTO_ULTIMO_PEDIDO >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_CUSTO_PED_MEDIO
check(CUSTO_PEDIDO_MEDIO >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_VALOR_IMPOSTOS
check(VALOR_IMPOSTOS >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_VALOR_ENCARGOS
check(VALOR_ENCARGOS >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_VLR_CUS_VENDA
check(VALOR_CUSTO_VENDA >= 0);

alter table ORCAMENTOS_ITENS
add constraint CK_ORC_ITENS_VlR_CUS_FINAL
check(VALOR_CUSTO_FINAL >= 0);

/* Unique */
alter table ORCAMENTOS_ITENS
add constraint UN_ORC_ITENS_PROD_ITEM_ID
unique(ORCAMENTO_ID, ITEM_ID)
using index tablespace INDICES;

alter table ORCAMENTOS_ITENS
add constraint UN_ORC_ITENS_ORC_ID_PROD_ID
unique(ORCAMENTO_ID, PRODUTO_ID)
using index tablespace INDICES;