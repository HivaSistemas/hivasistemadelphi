create table ORCAMENTOS_PAGAMENTOS_CHEQUES(
  ORCAMENTO_ID    	number(10) not null,
  COBRANCA_ID     	number(3) not null,
  ITEM_ID         	number(3) not null,
  DATA_VENCIMENTO 	date not null,
  BANCO           	varchar2(5) not null,
  AGENCIA         	varchar2(8) not null,
  CONTA_CORRENTE  	varchar2(8) not null,
  NUMERO_CHEQUE   	number(10) not null,
  VALOR_CHEQUE    	number(8,2) not null,
  NOME_EMITENTE     varchar2(80),
  TELEFONE_EMITENTE varchar2(15),
	PARCELA           number(2) not null,
	NUMERO_PARCELAS   number(2) not null,
  CPF_CNPJ_EMITENTE varchar2(19)
);

alter table ORCAMENTOS_PAGAMENTOS_CHEQUES
add constraint PK_ORCAMENTOS_PAGAMENTOS_CHEQ
primary key(ORCAMENTO_ID, COBRANCA_ID, ITEM_ID)
using index tablespace INDICES;

alter table ORCAMENTOS_PAGAMENTOS_CHEQUES
add constraint FK_ORCAMENTOS_PAGTO_CHQ_ORC_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table ORCAMENTOS_PAGAMENTOS_CHEQUES
add constraint FK_ORCAMENTOS_PAGTO_CHQ_COB_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

alter table ORCAMENTOS_PAGAMENTOS_CHEQUES
add constraint CK_ORCAMENTOS_PAGT_CHQ_ITEM_ID
check(ITEM_ID > 0);

alter table ORCAMENTOS_PAGAMENTOS_CHEQUES
add constraint CK_ORCAMENTOS_PAGTOS_CHQ_VALOR
check(VALOR_CHEQUE > 0);

alter table ORCAMENTOS_PAGAMENTOS_CHEQUES
add constraint CK_ORC_PAGTOS_CHQ_PARCELAS
check(PARCELA > 0 and NUMERO_PARCELAS > 0 and PARCELA <= NUMERO_PARCELAS);
