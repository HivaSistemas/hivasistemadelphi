create table CONTAS_PAGAR_BAIXAS_CREDITOS(
  BAIXA_ID     number(10) not null,
  RECEBER_ID   number(12) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table CONTAS_PAGAR_BAIXAS_CREDITOS
add constraint PK_CONTAS_PAG_BAIXAS_CREDITOS
primary key(BAIXA_ID, RECEBER_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_PAGAR_BAIXAS_CREDITOS
add constraint FK_CONTAS_PAG_BX_CRED_BAIXA_ID
foreign key(BAIXA_ID)
references CONTAS_PAGAR_BAIXAS(BAIXA_ID);

alter table CONTAS_PAGAR_BAIXAS_CREDITOS
add constraint FK_CONTAS_PAG_BX_CRED_PAGAR_ID
foreign key(RECEBER_ID)
references CONTAS_RECEBER(RECEBER_ID);