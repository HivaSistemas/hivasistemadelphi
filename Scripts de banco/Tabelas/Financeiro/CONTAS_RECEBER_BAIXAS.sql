create table CONTAS_RECEBER_BAIXAS(
  BAIXA_ID   		    		    number(10),
  BAIXA_PAGAR_ORIGEM_ID     number(10),  -- Utilizado quando h� um encontro de contas
  EMPRESA_ID        		    number(3) not null,

  CADASTRO_ID               number(10),

  USUARIO_BAIXA_ID  		    number(4),
  CONTA_ID          	      varchar2(8),
	TURNO_ID                  number(10),

	VALOR_TITULOS             number(8,2) not null,
	VALOR_MULTA               number(8,2) not null,
	VALOR_JUROS               number(8,2) not null,
  VALOR_DESCONTO    	   	  number(8,2) not null, -- Desconto efetuado na hora de pagar o t�tulo
  VALOR_ADIANTADO           number(8,2) default 0 not null,
	VALOR_LIQUIDO             number(8,2) not null,

	VALOR_DINHEIRO    	   	  number(8,2) default 0 not null,
	VALOR_PIX         	   	  number(8,2) default 0 not null,
  VALOR_CHEQUE      	   	  number(8,2) default 0 not null,
  VALOR_CARTAO_DEBITO	      number(8,2) default 0 not null,
  VALOR_CARTAO_CREDITO      number(8,2) default 0 not null,
  VALOR_COBRANCA    	   	  number(8,2) default 0 not null,
  VALOR_CREDITO     	   	  number(8,2) default 0 not null,
  VALOR_RETENCAO     	   	  number(8,2) default 0 not null,

  VALOR_TROCO               number(8,2) default 0 not null,
  RECEBIDO                  char(1) not null,
  TIPO                      char(3) not null,

	DATA_HORA_BAIXA	 		      date not null, -- Data que foi efetuado no sistema a baixa.
  DATA_PAGAMENTO    	   	  date, -- Data no qual realmente foi efetuado o pagamento pelo cliente, por�m n�o havia sido sinalizado no sistema.
	USUARIO_ENVIO_CAIXA_ID    number(4),
	RECEBER_CAIXA     		    char(1) not null,
  OBSERVACOES       		    varchar2(200),
  BAIXA_PIX_REC_CX          char(1) not null default 'N',

  RECEBER_ADIANTADO_ID      number(12),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID         number(4) not null,
  DATA_HORA_ALTERACAO       date not null,
  ROTINA_ALTERACAO          varchar2(30) not null,
  ESTACAO_ALTERACAO         varchar2(30) not null
);

alter table CONTAS_RECEBER_BAIXAS
add constraint PK_CONTAS_RECEBER_BAIXAS
primary key(BAIXA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_RECEBER_BAIXAS
add constraint FK_CONTAS_REC_BX_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table CONTAS_RECEBER_BAIXAS
add constraint FK_CONTAS_REC_BX_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);

alter table CONTAS_RECEBER_BAIXAS
add constraint FK_CONTAS_REC_BX_USUARIO_BX_ID
foreign key(USUARIO_BAIXA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table CONTAS_RECEBER_BAIXAS
add constraint FK_CONTAS_REC_BX_USU_ENV_CX_ID
foreign key(USUARIO_ENVIO_CAIXA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table CONTAS_RECEBER_BAIXAS
add constraint FK_CONTAS_RECEBER_BX_TURNO_ID
foreign key(TURNO_ID)
references TURNOS_CAIXA(TURNO_ID);

alter table CONTAS_RECEBER_BAIXAS
add constraint FK_CONTAS_REC_BX_BX_PAG_OR_ID
foreign key(BAIXA_PAGAR_ORIGEM_ID)
references CONTAS_PAGAR_BAIXAS(BAIXA_ID);

alter table CONTAS_RECEBER_BAIXAS
add constraint FK_CONTAS_REC_BX_REC_ADI_ID
foreign key(RECEBER_ADIANTADO_ID)
references CONTAS_RECEBER(RECEBER_ID);


/* Constraints */
alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VALOR_DESCONT
check(VALOR_DESCONTO >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VALOR_ADIANT
check(VALOR_ADIANTADO >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VALOR_DINHEIR
check(VALOR_DINHEIRO >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VALOR_PIX
check(VALOR_PIX >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VALOR_CHEQUE
check(VALOR_CHEQUE >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VLR_CRT_DEB
check(VALOR_CARTAO_DEBITO >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VLR_CRT_CRE
check(VALOR_CARTAO_CREDITO >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VALOR_COBRANC
check(VALOR_COBRANCA >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VALOR_CREDITO
check(VALOR_CREDITO >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VALOR_RETEN
check(VALOR_RETENCAO >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VALOR_TROCO
check(VALOR_TROCO >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VALOR_MULTA
check(VALOR_MULTA >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VALOR_JUROS
check(VALOR_JUROS >= 0);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_SOMA_PAGTOS
check(
  VALOR_DINHEIRO +
  VALOR_PIX +
  VALOR_CHEQUE +
  VALOR_CARTAO_DEBITO +
  VALOR_CARTAO_CREDITO +
  VALOR_COBRANCA +
  VALOR_CREDITO -
  VALOR_DESCONTO -
  VALOR_TROCO > 0
);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_VALOR_LIQ
check(
  VALOR_LIQUIDO = VALOR_TITULOS + VALOR_JUROS + VALOR_MULTA - VALOR_DESCONTO - VALOR_ADIANTADO - VALOR_RETENCAO
  and
  VALOR_LIQUIDO <= (
    VALOR_DINHEIRO +
    VALOR_CHEQUE +
    VALOR_CARTAO_DEBITO +
    VALOR_CARTAO_CREDITO +
    VALOR_COBRANCA +
    VALOR_CREDITO +
    VALOR_PIX
  )
);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_REC_CAIXA
check(RECEBER_CAIXA in('S','N'));

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_RECEBIDO
check(
	RECEBIDO = 'N' and USUARIO_BAIXA_ID is null
	or
	RECEBIDO = 'S' and USUARIO_BAIXA_ID is not null
);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_ENVIO_CAIXA
check(
	(RECEBIDO = 'S' and RECEBER_CAIXA = 'N' and USUARIO_ENVIO_CAIXA_ID is null)
	or
	(RECEBIDO = 'S' and RECEBER_CAIXA = 'S') /* Se j� recebido */
	or
	(RECEBIDO = 'N' and RECEBER_CAIXA = 'S' and USUARIO_ENVIO_CAIXA_ID is not null) /* Se � para receber no caixa, mas ainda n�o foi recebido, tem que ter o usu�rio que enviou, mas sem caixa */
);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_TURNO_ID
check(
  (RECEBIDO = 'S' and RECEBER_CAIXA = 'N' and DATA_PAGAMENTO is not null)
	or
	(RECEBIDO = 'N' and RECEBER_CAIXA = 'S' and TURNO_ID is null and DATA_PAGAMENTO is null)
	or
	(RECEBIDO = 'S' and RECEBER_CAIXA = 'S' and TURNO_ID is not null and DATA_PAGAMENTO is not null)
);

/*
  TIPO
  CRE - Cr�dito, gerando cr�dito a pagar ( � a pagar mesmo )
  BCR - Baixa de contas a receber
  ECR - Encontro de contas a receber ( Encontro de contas, um t�tulo do contas a a pagar abate o contas a receber )
  BCA - Baixa de cart�es ( Concilia��o com o arquivo )
  BBO - Baixa de boletos ( Concilia��o com o arquivo )
  ADI - Adiantamento
*/
alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_RECEBER_BAIXAS_TIPO
check(
  TIPO in('CRE', 'BCR', 'BCA', 'PIX')
  or
  (TIPO = 'ECR' and BAIXA_PAGAR_ORIGEM_ID is not null)
  or
  (TIPO = 'ADI' and RECEBER_ADIANTADO_ID is not null)
);

alter table CONTAS_RECEBER_BAIXAS
add constraint CK_CONTAS_REC_BX_TIPO_CAD_ID
check(
  (TIPO in('CRE', 'BCR', 'ECR', 'ADI', 'PIX') and CADASTRO_ID is not null)
  or
  (TIPO in('BCA', 'BBO') and CADASTRO_ID is null)
);
