create table LOGS_CONTAS_PAGAR(
  TIPO_ALTERACAO_LOG_ID   number(4) not null,
  PAGAR_ID                number(12) not null,
  VALOR_ANTERIOR          varchar2(300),
  NOVO_VALOR              varchar2(300),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chaves estrangeiras */
alter table LOGS_CONTAS_PAGAR
add constraint FK_LOGS_CONTAS_PAGAR_ID
foreign key(PAGAR_ID)
references CONTAS_PAGAR(PAGAR_ID);
