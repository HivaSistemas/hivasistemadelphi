﻿create table MOVIMENTOS_CREDITOS_CLIENTES(
  MOVIMENTO_ID         number(12),
  CLIENTE_ID           number(10) not null,
  TIPO_MOVIMENTO       char(3) not null,
  VALOR_ANTERIOR       number(10,2) not null,
  VALOR                number(10,2) not null,
  VALOR_NOVO           number(10,2) not null,
  DATA_HORA_MOVIMENTO  date not null,
  USUARIO_CADASTRO_ID  number(4) not null,
  DEVOLUCAO_ID         number(10),
  ORCAMENTO_ID         number(10),
  RECEBER_BAIXA_ID     number(10)
);

alter table MOVIMENTOS_CREDITOS_CLIENTES
add constraint PK_MOV_CREDITOS_CLIENTES
primary key(MOVIMENTO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table MOVIMENTOS_CREDITOS_CLIENTES
add constraint FK_MOV_CRED_CLI_CLIENTE_ID
foreign key(CLIENTE_ID)
references CLIENTES(CADASTRO_ID);

alter table MOVIMENTOS_CREDITOS_CLIENTES
add constraint FK_MOV_CRED_CLI_USUARIO_CAD_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table MOVIMENTOS_CREDITOS_CLIENTES
add constraint FK_MOV_CRED_CLI_DEVOLUCAO_ID
foreign key(DEVOLUCAO_ID)
references DEVOLUCOES(DEVOLUCAO_ID);

/* Constraints */

/* 
  TIPO_MOVIMENTO
  VEN - Vendas
  CVE - Cancelamento vendas
  DEV - Devoluções
  CRB - Contas receber baixas
*/
alter table MOVIMENTOS_CREDITOS_CLIENTES
add constraint CK_MOV_CRED_CLI_TIPO_MOVIMENTO
check(TIPO_MOVIMENTO in('VEN','CVE','DEV','CRB'));

alter table MOVIMENTOS_CREDITOS_CLIENTES
add constraint CK_MOV_CRED_CLI_MOVIMENTOS_ID
check(
  TIPO_MOVIMENTO = 'DEV' and DEVOLUCAO_ID is not null
  or
  TIPO_MOVIMENTO in('VEN', 'CVE') and ORCAMENTO_ID is not null
  or
  TIPO_MOVIMENTO = 'CRB' and RECEBER_BAIXA_ID is not null
);