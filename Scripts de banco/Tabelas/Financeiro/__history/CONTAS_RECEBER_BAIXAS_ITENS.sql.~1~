﻿create table CONTAS_RECEBER_BAIXAS_ITENS(
  BAIXA_ID   		          number(10),
  RECEBER_ID              number(12) not null,

  VALOR_DINHEIRO          number(8,2) default 0 not null,
  VALOR_CHEQUE            number(8,2) default 0 not null,
  VALOR_CARTAO_DEBITO     number(8,2) default 0 not null,
  VALOR_CARTAO_CREDITO    number(8,2) default 0 not null,
  VALOR_COBRANCA          number(8,2) default 0 not null,
  VALOR_CREDITO           number(8,2) default 0 not null,

  VALOR_MULTA             number(8,2) default 0 not null,
  VALOR_JUROS             number(8,2) default 0 not null,
  VALOR_DESCONTO          number(8,2) not null, -- Desconto efetuado na hora de pagar o título

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint PK_CONTAS_RECEBER_BAIXAS_ITENS
primary key(BAIXA_ID, RECEBER_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint FK_CONTAS_REC_BX_ITE_BAIXA_ID
foreign key(BAIXA_ID)
references CONTAS_RECEBER_BAIXAS(BAIXA_ID);

alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint FK_CONTAS_REC_BX_ITENS_REC_ID
foreign key(RECEBER_ID)
references CONTAS_RECEBER(RECEBER_ID);

/* Constraints */
alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint CK_CONTAS_REC_BX_ITE_VALOR_DES
check(VALOR_DESCONTO >= 0);

alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint CK_CONTAS_REC_BX_ITE_VALOR_DIN
check(VALOR_DINHEIRO >= 0);

alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint CK_CONTAS_REC_BX_ITE_VALOR_CHQ
check(VALOR_CHEQUE >= 0);

alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint CK_CONT_RE_BX_IT_VLR_CRT_DEB
check(VALOR_CARTAO_DEBITO >= 0);

alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint CK_CONT_RE_BX_IT_VLR_CRT_CRE
check(VALOR_CARTAO_CREDITO >= 0);

alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint CK_CONTAS_REC_BX_ITE_VALOR_COB
check(VALOR_COBRANCA >= 0);

alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint CK_CONTAS_REC_BX_ITE_VALOR_CRE
check(VALOR_CREDITO >= 0);

alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint CK_CONTAS_REC_BX_ITE_SOMA_PAGT
check(VALOR_DINHEIRO + VALOR_CHEQUE + VALOR_CARTAO_DEBITO + VALOR_CARTAO_CREDITO + VALOR_COBRANCA + VALOR_CREDITO - VALOR_DESCONTO > 0);

alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint CK_CONTAS_REC_BX_ITE_VLR_MULTA
check(VALOR_MULTA >= 0);

alter table CONTAS_RECEBER_BAIXAS_ITENS
add constraint CK_CONTAS_REC_BX_ITE_VLR_JUROS
check(VALOR_JUROS >= 0);