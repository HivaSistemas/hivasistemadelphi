create table CONTAS_REC_HISTORICO_COBRANCAS(
  RECEBER_ID            number(12,0) not null,
  ITEM_ID               number(3) not null,
  
  HISTORICO             varchar2(800) not null,
  
  USUARIO_COBRANCA_ID   number(4) not null,
  DATA_HORA_COBRANCA    date not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table CONTAS_REC_HISTORICO_COBRANCAS
add constraint CK_CON_REC_HISTORICO_COBRANCAS
primary key(RECEBER_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_REC_HISTORICO_COBRANCAS
add constraint FK_CON_REC_HIST_COBR_RECEB_ID
foreign key(RECEBER_ID)
references CONTAS_RECEBER(RECEBER_ID);

alter table CONTAS_REC_HISTORICO_COBRANCAS
add constraint FK_CON_REC_HIS_COBR_USU_COB_ID
foreign key(USUARIO_COBRANCA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

