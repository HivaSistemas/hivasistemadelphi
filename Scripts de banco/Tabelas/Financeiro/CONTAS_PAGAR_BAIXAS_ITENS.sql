﻿create table CONTAS_PAGAR_BAIXAS_ITENS(
  BAIXA_ID   		          number(10),
  PAGAR_ID                number(12) not null,

  VALOR_DINHEIRO          number(8,2) default 0 not null,
  VALOR_CHEQUE            number(8,2) default 0 not null,
  VALOR_CARTAO            number(8,2) default 0 not null,
  VALOR_COBRANCA          number(8,2) default 0 not null,
  VALOR_CREDITO           number(8,2) default 0 not null,
  VALOR_TROCA             number(8,2) default 0 not null,

  VALOR_JUROS             number(8,2) default 0 not null,
  VALOR_DESCONTO          number(8,2) default 0 not null, -- Desconto efetuado na hora de pagar o título

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table CONTAS_PAGAR_BAIXAS_ITENS
add constraint PK_CONTAS_PAGAR_BAIXAS_ITENS
primary key(BAIXA_ID, PAGAR_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_PAGAR_BAIXAS_ITENS
add constraint FK_CONTAS_PAG_BAIXA_ID
foreign key(BAIXA_ID)
references CONTAS_PAGAR_BAIXAS(BAIXA_ID);

alter table CONTAS_PAGAR_BAIXAS_ITENS
add constraint FK_CONTAS_PAG_BX_ITENS_REC_ID
foreign key(PAGAR_ID)
references CONTAS_PAGAR(PAGAR_ID);

/* Constraints */
alter table CONTAS_PAGAR_BAIXAS_ITENS
add constraint CK_CONTAS_PAG_BX_ITE_VALOR_DES
check(VALOR_DESCONTO >= 0);

alter table CONTAS_PAGAR_BAIXAS_ITENS
add constraint CK_CONTAS_PAG_BX_ITE_VALOR_DIN
check(VALOR_DINHEIRO >= 0);

alter table CONTAS_PAGAR_BAIXAS_ITENS
add constraint CK_CONTAS_PAG_BX_ITE_VALOR_CHQ
check(VALOR_CHEQUE >= 0);

alter table CONTAS_PAGAR_BAIXAS_ITENS
add constraint CK_CONTAS_PAG_BX_ITE_VALOR_CRT
check(VALOR_CARTAO >= 0);

alter table CONTAS_PAGAR_BAIXAS_ITENS
add constraint CK_CONTAS_PAG_BX_ITE_VALOR_COB
check(VALOR_COBRANCA >= 0);

alter table CONTAS_PAGAR_BAIXAS_ITENS
add constraint CK_CONTAS_PAG_BX_ITE_VALOR_CRE
check(VALOR_CREDITO >= 0);

alter table CONTAS_PAGAR_BAIXAS_ITENS
add constraint CK_CONTAS_PAG_BX_ITE_VALOR_TRO
check(VALOR_TROCA >= 0);

alter table CONTAS_PAGAR_BAIXAS_ITENS
add constraint CK_CONTAS_PAG_BX_ITE_SOMA_PAGT
check(VALOR_DINHEIRO + VALOR_CHEQUE + VALOR_CARTAO + VALOR_COBRANCA + VALOR_CREDITO + VALOR_TROCA - VALOR_DESCONTO >= 0);
