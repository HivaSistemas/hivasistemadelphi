create table CONTAS_PAGAR_BAIXAS_PAGAMENTOS(
  BAIXA_ID     		 number(10),
  COBRANCA_ID  		 number(3) not null,	
	ITEM_ID      		 number(2) not null,	
	TIPO             char(2) not null,
	PARCELA          number(2),
	NUMERO_PARCELAS  number(2),
  DATA_VENCIMENTO  date,
  NSU_TEF          varchar2(20),
  VALOR            number(8,2) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table CONTAS_PAGAR_BAIXAS_PAGAMENTOS
add constraint PK_CONTAS_PAGAR_BAIXAS_PAGTOS
primary key(BAIXA_ID, COBRANCA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_PAGAR_BAIXAS_PAGAMENTOS
add constraint FK_CONTAS_PAG_PAGTOS_BAIXA_ID
foreign key(BAIXA_ID)
references CONTAS_PAGAR_BAIXAS(BAIXA_ID);

alter table CONTAS_PAGAR_BAIXAS_PAGAMENTOS
add constraint FK_CONTAS_PAG_PAGTOS_COBRAN_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

/* Constraints */
alter table CONTAS_PAGAR_BAIXAS_PAGAMENTOS
add constraint CK_CONTAS_PAG_BX_PAGT_ITEM_ID
check(ITEM_ID > 0);

alter table CONTAS_PAGAR_BAIXAS_PAGAMENTOS
add constraint CK_CON_PAG_BX_PAGTOS_DATA_VENC
check(
  (TIPO = 'CR' and DATA_VENCIMENTO is null and PARCELA is null and NUMERO_PARCELAS is null)
  or
  (TIPO = 'CO' and DATA_VENCIMENTO is not null and PARCELA is not null and NUMERO_PARCELAS is not null)  
);

/* TIPO
  CR - Cartão
  CO - Cobrança
*/
alter table CONTAS_PAGAR_BAIXAS_PAGAMENTOS
add constraint CK_CONTAS_PAGAR_BX_PAGTOS_TIPO
check(TIPO in('CR','CO'));

alter table CONTAS_PAGAR_BAIXAS_PAGAMENTOS
add constraint CK_CONTAS_PAG_BX_PAGTOS_VALOR
check(VALOR > 0);
