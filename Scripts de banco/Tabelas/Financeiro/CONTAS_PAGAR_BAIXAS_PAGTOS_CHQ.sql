create table CONTAS_PAGAR_BAIXAS_PAGTOS_CHQ(
  BAIXA_ID        	number(10) not null,
  COBRANCA_ID     	number(3) not null,
  ITEM_ID         	number(3) not null,
  DATA_VENCIMENTO 	date not null,  
  NUMERO_CHEQUE   	number(10) not null,
  VALOR_CHEQUE    	number(8,2) not null,  
	PARCELA           number(2) not null,
	NUMERO_PARCELAS   number(2) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

alter table CONTAS_PAGAR_BAIXAS_PAGTOS_CHQ
add constraint PK_CONTAS_PAGAR_BX_PAGTOS_CHQ
primary key(BAIXA_ID, COBRANCA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_PAGAR_BAIXAS_PAGTOS_CHQ
add constraint FK_CONTAS_PAG_BX_PAGT_CHQ_B_ID
foreign key(BAIXA_ID)
references CONTAS_PAGAR_BAIXAS(BAIXA_ID);

alter table CONTAS_PAGAR_BAIXAS_PAGTOS_CHQ
add constraint FK_CONTAS_PAG_BX_PAGT_CHQ_C_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

/* Constraints */
alter table CONTAS_PAGAR_BAIXAS_PAGTOS_CHQ
add constraint CK_CONTAS_PAG_BX_P_CHQ_ITEM_ID
check(ITEM_ID > 0);

alter table CONTAS_PAGAR_BAIXAS_PAGTOS_CHQ
add constraint CK_CONTAS_PAG_BX_PAGT_CHQ_VLR
check(VALOR_CHEQUE > 0);

alter table CONTAS_PAGAR_BAIXAS_PAGTOS_CHQ
add constraint CK_CONTAS_PAG_BX_PAGT_CHQ_PARC
check(PARCELA > 0 and NUMERO_PARCELAS > 0);