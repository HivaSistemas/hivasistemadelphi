﻿create table CONTAS_RECEBER(
  RECEBER_ID                  number(12),
  CADASTRO_ID                 number(10) not null,
  EMPRESA_ID                  number(3) not null,

  ORIGEM                      char(3) not null,
  ORCAMENTO_ID                number(10),

  COBRANCA_ID                 number(3) not null,
  PORTADOR_ID                 varchar2(50) not null,
  TURNO_ID                    number(10),
  VENDEDOR_ID                 number(4),
  NOTA_FISCAL_ID              number(12),
  DEVOLUCAO_ENTRADA_ID        number(12),
  ACUMULADO_ID                number(10),
  USUARIO_CADASTRO_ID         number(4) not null,

  CONTA_CUSTODIA_ID           number(3),
  OBSERVACOES_CONTA_CUSTODIA  varchar2(200),

  PLANO_FINANCEIRO_ID         varchar2(9)	not null,
  CENTRO_CUSTO_ID             number(3) default 1 not null,
  DOCUMENTO				            varchar2(25) not null,

  BANCO                       varchar2(5),
  AGENCIA                     varchar2(8),
  CONTA_CORRENTE              varchar2(10),
  NUMERO_CHEQUE               number(10),
  NOME_EMITENTE               varchar2(80),
  TELEFONE_EMITENTE           varchar2(15),

  DATA_CADASTRO               date default trunc(sysdate) not null,
  DATA_EMISSAO                date,
  DATA_VENCIMENTO             date not null,
  DATA_VENCIMENTO_ORIGINAL    date,
    
  VALOR_DOCUMENTO             number(8,2) not null,
  VALOR_RETENCAO              number(8,2) default 0 not null,
  VALOR_ADIANTADO             number(8,2) default 0 not null,

  STATUS                      char(1) default 'A' not null,

  NOSSO_NUMERO                varchar2(30),
  CODIGO_BARRAS               varchar2(44),

  -- TEF
  NSU                         varchar2(20),
  CODIGO_AUTORIZACAO_TEF      varchar2(20),
  ITEM_ID_CRT_ORCAMENTO       number(3),
  NUMERO_CARTAO_TRUNCADO     varchar2(19),

  PARCELA                     number(3) default 1 not null,
  NUMERO_PARCELAS             number(3) default 1 not null,

  BAIXA_ID                    number(10),
  BAIXA_ORIGEM_ID             number(10), -- Título originado de uma baixa de diversos títulos
  BAIXA_PAGAR_ORIGEM_ID       number(10), -- Esta coluna é utilizada quando é gerado um título a pagar, ex: Geração de um crédito a receber.

  OBSERVACOES                varchar2(200),

  REMESSA_ID                 number(10),
  NOME_ARQUIVO_REMESSA       varchar2(100),


  CHAVE_IMPORTACAO           number(12),
  COMISSIONAR                char(1) default 'N' not null,

  /* Colunas ainda não utilizadas importadas do sistema ADM */
  NUMERO_NOTA                   number(10,0),
  DATA_CONTABIL                 date not null,
  DATA_ENVIO_EMAIL_BOLETO_DUPL  date,
  DATA_PROTESTO                 date,
  DATA_REEMISSAO_BOLETO_DUPL    date,
  EMITIU_BOLETO_DUPLICATA       char(1) default 'N' not null,
  ENVIOU_EMAIL_BOLETO_DUPLICATA char(1) default 'N' not null,
  CNPJ_CPF_EMITENTE	        varchar2(19),
  INDICE_CONDICAO_PAGAMENTO     number(10,5),
  COBRANCA_JUDICIAL             char(1) default 'N' not null,
  DATA_ENVIO_COBRANCA_JUDICIAL  date,
  CHEQUE_MORADIA                char(1) default 'N' not null,
  CLIENTE_RECEBEU_AVISO_COB     char(1) default 'N' not null,
  CLIENTE_RECEBEU_INST_PROTESTO char(1) default 'N' not null,
  CMC7                          varchar2(30),
  PREJUIZO                      char(1) default 'N' not null,
  SAIU_DO_PREJUIZO              char(1),
  STATUS_PROTESTO               char(3) default 'NEN' not null,
  VALOR_CUSTOS_CARTORIO         number(13,4) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table CONTAS_RECEBER
add constraint PK_CONTAS_RECEBER
primary key(RECEBER_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_RECEBER
add constraint FK_CONTAS_RECEBER_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_RECEBER_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_RECEBER_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_RECEBER_COBRANCA_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_REC_PORTADOR_ID
foreign key(PORTADOR_ID)
references PORTADORES(PORTADOR_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_RECEBER_TURNO_ID
foreign key(TURNO_ID)
references TURNOS_CAIXA(TURNO_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_RECEBER_VENDEDOR_ID
foreign key(VENDEDOR_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_REC_USUARIO_CADAS_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_REC_NOTA_FISCAL_ID
foreign key(NOTA_FISCAL_ID)
references NOTAS_FISCAIS(NOTA_FISCAL_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_REC_DEV_ENTRADA_ID
foreign key(DEVOLUCAO_ENTRADA_ID)
references DEVOLUCOES_ENTRADAS_NOTAS_FISC(DEVOLUCAO_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_REC_BAIXA_ORIGEM_ID
foreign key(BAIXA_ORIGEM_ID)
references CONTAS_RECEBER_BAIXAS(BAIXA_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_REC_PLANO_FINANC_ID
foreign key(PLANO_FINANCEIRO_ID)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_REC_CENTRO_CUSTO_ID
foreign key(CENTRO_CUSTO_ID)
references CENTROS_CUSTOS(CENTRO_CUSTO_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_REC_BAIXA_ID
foreign key(BAIXA_ID)
references CONTAS_RECEBER_BAIXAS(BAIXA_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_PAGAR_BAIXA_ID
foreign key(BAIXA_PAGAR_ORIGEM_ID)
references CONTAS_PAGAR_BAIXAS(BAIXA_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_REC_REMESSA_COBR_ID
foreign key(REMESSA_ID)
references REMESSAS_COBRANCAS(REMESSA_ID);

alter table CONTAS_RECEBER
add constraint FK_CONTAS_REC_CON_CUSTODIA_ID
foreign key(CONTA_CUSTODIA_ID)
references CONTAS_CUSTODIA(CONTA_CUSTODIA_ID);
/* ----- Constraints ----- */

/* STATUS
  A - Aberto
  B - Baixado
*/
alter table CONTAS_RECEBER
add constraint CK_CONTAS_RECEBER_STATUS
check(
  (STATUS = 'A' and BAIXA_ID is null)
  or
  (STATUS = 'B' and BAIXA_ID is not null)
);

alter table CONTAS_RECEBER
add constraint CK_CONTAS_REC_VALOR_DOCUMENTO
check(VALOR_DOCUMENTO > 0 and VALOR_DOCUMENTO = round(VALOR_DOCUMENTO, 2));

alter table CONTAS_RECEBER
add constraint CK_CONTAS_REC_VALOR_RETENCAO
check(VALOR_RETENCAO >= 0);

alter table CONTAS_RECEBER
add constraint CK_CONTAS_REC_VALOR_ADIANTADO
check(
  VALOR_ADIANTADO >= 0
  and
  VALOR_ADIANTADO < VALOR_DOCUMENTO - VALOR_RETENCAO
);

alter table CONTAS_RECEBER
add constraint CK_CONTAS_PARCELAS
check(
  (PARCELA between 1 and 360) and (NUMERO_PARCELAS between 1 and 360) and (PARCELA <= NUMERO_PARCELAS)
);

/*
  ORIGEM
  MAN - Manual
  ORC - Pedidos
  BXR - Baixa de contas a receber
  BAP - Baixa de contas a pagar
  DTU - Diferença de turno
  ACU - Acumulado
  ADA - Adiantamento pedido acumulado
  DEN - Devolução de entrada de NF
*/
alter table CONTAS_RECEBER
add constraint CK_CONTAS_RECEBER_ORIGEM
check(
  (ORIGEM = 'MAN')
  or
  (ORIGEM = 'ORC' and ORCAMENTO_ID is not null)
  or
  (ORIGEM = 'BXR' and BAIXA_ORIGEM_ID is not null)
  or
  (ORIGEM = 'BAP' and BAIXA_PAGAR_ORIGEM_ID is not null)
  or
  (ORIGEM = 'DTU')
  or
  (ORIGEM = 'ACU' and ACUMULADO_ID is not null)
  or
  (ORIGEM = 'ADA' and ORCAMENTO_ID is not null)
  or
  (ORIGEM = 'DEN' and DEVOLUCAO_ENTRADA_ID is not null)
);

alter table CONTAS_RECEBER
add constraint CK_CONTAS_REC_OBSEV_CON_CUS
check(
  CONTA_CUSTODIA_ID is null and OBSERVACOES_CONTA_CUSTODIA is null
  or
  CONTA_CUSTODIA_ID is not null and OBSERVACOES_CONTA_CUSTODIA is not null
);

alter table CONTAS_RECEBER
add constraint CK_CONTAS_REC_COMISSIONAR
check(
  COMISSIONAR = 'N'
  or
  COMISSIONAR = 'S' and VENDEDOR_ID is not null
);
