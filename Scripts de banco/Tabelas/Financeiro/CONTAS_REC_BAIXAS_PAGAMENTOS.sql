﻿create table CONTAS_REC_BAIXAS_PAGAMENTOS(
  BAIXA_ID     		     number(10),
  COBRANCA_ID  		     number(3) not null,
	ITEM_ID      		     number(2) not null,
	TIPO                 char(2) not null,
	PARCELA              number(2),
	NUMERO_PARCELAS      number(2),
  DATA_VENCIMENTO      date,

  NSU_TEF              varchar2(20),
  CODIGO_AUTORIZACAO   varchar2(50),
  NUMERO_CARTAO        varchar2(19),
  TIPO_RECEB_CARTAO    char(3),

  VALOR                number(8,2) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table CONTAS_REC_BAIXAS_PAGAMENTOS
add constraint PK_CONTAS_REC_BAIXAS_PAGTOS
primary key(BAIXA_ID, COBRANCA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_REC_BAIXAS_PAGAMENTOS
add constraint FK_CONTAS_REC_PAGTOS_BAIXA_ID
foreign key(BAIXA_ID)
references CONTAS_RECEBER_BAIXAS(BAIXA_ID);

alter table CONTAS_REC_BAIXAS_PAGAMENTOS
add constraint FK_CONTAS_REC_PAGTOS_COBRAN_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

/* Constraints */
alter table CONTAS_REC_BAIXAS_PAGAMENTOS
add constraint CK_CONTAS_REC_BX_PAGT_ITEM_ID
check(ITEM_ID > 0);

alter table CONTAS_REC_BAIXAS_PAGAMENTOS
add constraint CK_CON_REC_BX_PAGTOS_DATA_VENC
check(
  (TIPO = 'CR' and DATA_VENCIMENTO is null and PARCELA is null and NUMERO_PARCELAS is null and TIPO_RECEB_CARTAO is not null)
  or
  (TIPO = 'CO' and DATA_VENCIMENTO is not null and PARCELA is not null and NUMERO_PARCELAS is not null and TIPO_RECEB_CARTAO is null)
);

/* TIPO
  CR - Cartão
  CO - Cobrança
*/
alter table CONTAS_REC_BAIXAS_PAGAMENTOS
add constraint CK_CONTAS_REC_BX_PAGTOS_TIPO
check(TIPO in('CR','CO'));

alter table CONTAS_REC_BAIXAS_PAGAMENTOS
add constraint CK_CONTAS_REC_BX_PAGTOS_VALOR
check(VALOR > 0);

alter table CONTAS_REC_BAIXAS_PAGAMENTOS
add constraint CK_CON_REC_BX_PAG_TP_REC_CRT
check(TIPO_RECEB_CARTAO in(null, 'TEF', 'POS'));
