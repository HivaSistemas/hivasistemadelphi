create table REMESSAS_COBRANCAS(
  REMESSA_ID            number(10) not null,
  CONTA_ID           	  varchar2(20) not null,
  NUMERO_REMESSA        number(10) not null,
  QUANTIDADE_TITULOS    number(5) not null,
  DATA_HORA_CADASTRO    date not null,
  DATA_EMISSAO_INICIAL  date not null,
  DATA_EMISSAO_FINAL    date not null,	  
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave Primária */
alter table REMESSAS_COBRANCAS
add constraint PK_REMESSAS_COBRANCAS
primary key (REMESSA_ID)
using index tablespace INDICES;

/* Chaves Estrangeiras */
alter table REMESSAS_COBRANCAS
add constraint FK_REMESSAS_COBRANCAS_CONTA
foreign key (CONTA_ID)
references CONTAS(CONTA);

/* Checagens */
alter table REMESSAS_COBRANCAS
add constraint CK_REMESSAS_COBRANCAS_QTD_TIT
check ( QUANTIDADE_TITULOS > 0 );

alter table REMESSAS_COBRANCAS
add constraint CK_REMESSAS_COBRANCAS_DATAS
check ( DATA_EMISSAO_INICIAL <= DATA_EMISSAO_FINAL );

alter table REMESSAS_COBRANCAS
add constraint CK_REMESSAS_COBRANCAS_NUM_REM
check ( NUMERO_REMESSA > 0 );