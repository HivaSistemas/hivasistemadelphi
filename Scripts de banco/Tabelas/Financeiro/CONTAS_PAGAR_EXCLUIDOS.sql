create table CONTAS_PAGAR_EXCLUIDOS(
  EMPRESA_ID                number(3,0) not null,
  PAGAR_ID                  number(12) not null,
  CADASTRO_ID               number(10) not null,
  DATA_VENCIMENTO           date not null,
  DATA_VENCIMENTO_ORIGINAL  date not null,
  COBRANCA_ID               number(3) not null,
  DATA_CADASTRO             date not null,
  VALOR_DOCUMENTO           number(8,2) not null,
  USUARIO_CADASTRO_ID       number(4) not null,
  USUARIO_EXCLUSAO_ID       number(4) not null,
  DATA_HORA_EXCLUSAO        date,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null    
);

/* Chave primaria */
alter table CONTAS_PAGAR_EXCLUIDOS
add constraint PK_CONTAS_PAGAR_EXCLUIDOS
primary key(PAGAR_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_PAGAR_EXCLUIDOS
add constraint FK_CONTAS_PAGAR_EXC_CAD_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);

alter table CONTAS_PAGAR_EXCLUIDOS
add constraint FK_CONTAS_PAGAR_EXC_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table CONTAS_PAGAR_EXCLUIDOS
add constraint FK_CONTAS_PAGAR_EXC_COBR_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

/* Checagens */
alter table CONTAS_PAGAR_EXCLUIDOS
add constraint CK_CONTAS_PAGAR_EXC_VALOR_DOC
check(VALOR_DOCUMENTO > 0);