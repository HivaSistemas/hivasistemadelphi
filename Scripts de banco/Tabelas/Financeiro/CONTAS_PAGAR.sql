create table CONTAS_PAGAR(
  PAGAR_ID                  number(12),
  CADASTRO_ID               number(10) not null,
  EMPRESA_ID                number(3) not null,
  
  COMPRA_ID                 number(10),
  COMPRA_BAIXA_ID           number(10),
  ENTRADA_ID                number(12),
  ENTRADA_SERVICO_ID        number(10),
  CONHECIMENTO_FRETE_ID     number(12),
  
  COBRANCA_ID               number(3) not null,
  PORTADOR_ID               varchar2(50) not null,
  USUARIO_CADASTRO_ID       number(4) not null,  
  DOCUMENTO				          varchar2(24) not null,     
  NUMERO_CHEQUE             number(10),
  
  ORIGEM                    char(3) not null,
  BLOQUEADO                 char(1) default 'N' not null,
  MOTIVO_BLOQUEIO           varchar2(400),

  PLANO_FINANCEIRO_ID       varchar2(9)	not null,
  CENTRO_CUSTO_ID           number(3) default 1 not null,

  DATA_CADASTRO             date default trunc(sysdate) not null,
  DATA_VENCIMENTO           date not null,
  DATA_VENCIMENTO_ORIGINAL  date,

  VALOR_DOCUMENTO           number(8,2) not null,
  VALOR_DESCONTO            number(8,2) default 0 not null,
  VALOR_ADIANTADO           number(8,2) default 0 not null,

  STATUS                    char(1) default 'A' not null,

  NOSSO_NUMERO              varchar2(30),
  CODIGO_BARRAS             varchar2(47),

  PARCELA                   number(3) default 1 not null,
  NUMERO_PARCELAS           number(3) default 1 not null,

  BAIXA_ID                  number(10),
  BAIXA_ORIGEM_ID           number(10),
  BAIXA_RECEBER_ORIGEM_ID   number(10), -- Esta coluna � utilizada quando � gerado um t�tulo a receber, ex: Gera��o de um cr�dito a pagar.

  ORCAMENTO_ID              number(10),
  ACUMULADO_ID              number(10),
  DEVOLUCAO_ID              number(10),

  INDICE_CONDICAO_PAGAMENTO number(10,5),

  OBSERVACOES               varchar2(200),
  
  NUMERO_NOTA                   number(10,0),
  LIBERADO_CREDITO_MOV_FINANC   char(1) default 'N' not null,
  LIBERADO_PAGAMENTO_FINANCEIRO char(1) default 'N' not null,
  DATA_CONTABIL                 date not null,
  DATA_EMISSAO                  date not null,  
  PREVISAO                      char(1),
  USUARIO_LIB_PAG_FINANCEIRO    number(8,0),
  DATA_LIBERACAO_PAG_FINANCEIRO date,
  USUARIO_LIBERACAO_BLOQUEIO    number(8,0),
  DATA_LIBERACAO_BLOQUEIO       date, 
  CHAVE_IMPORTACAO              number(12), 
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null    
);

alter table CONTAS_PAGAR
add constraint PK_CONTAS_PAGAR
primary key(PAGAR_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAGAR_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAGAR_ENTRADA_ID
foreign key(ENTRADA_ID)
references ENTRADAS_NOTAS_FISCAIS(ENTRADA_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAGAR_ENTRADA_ID
foreign key(ENTRADA_SERVICO_ID)
references ENTRADAS_NOTAS_FISC_SERVICOS(ENTRADA_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAGAR_COMPRA_ID
foreign key(COMPRA_ID)
references COMPRAS(COMPRA_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAGAR_COMPRA_BX_ID
foreign key(COMPRA_BAIXA_ID)
references COMPRAS_BAIXAS(BAIXA_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAGAR_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAGAR_COBRANCA_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAG_PORTADOR_ID
foreign key(PORTADOR_ID)
references PORTADORES(PORTADOR_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAG_USUARIO_CADAS_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAG_BAIXA_ORIGEM_ID
foreign key(BAIXA_ORIGEM_ID)
references CONTAS_PAGAR_BAIXAS(BAIXA_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAG_PLANO_FINANC_ID
foreign key(PLANO_FINANCEIRO_ID)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAG_CENTRO_CUSTO_ID
foreign key(CENTRO_CUSTO_ID)
references CENTROS_CUSTOS(CENTRO_CUSTO_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAG_BX_REC_ORIGEM_ID
foreign key(BAIXA_RECEBER_ORIGEM_ID)
references CONTAS_RECEBER_BAIXAS(BAIXA_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAG_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAG_ACUMULADO_ID
foreign key(ACUMULADO_ID)
references ACUMULADOS(ACUMULADO_ID);

alter table CONTAS_PAGAR
add constraint FK_CONTAS_PAG_DEVOLUCAO_ID
foreign key(DEVOLUCAO_ID)
references DEVOLUCOES(DEVOLUCAO_ID);
/* ----- Constraints ----- */
 
/* STATUS
  A - Aberto
  B - Baixado
*/
alter table CONTAS_PAGAR
add constraint CK_CONTAS_PAGAR_STATUS
check(
  (STATUS = 'A' and BAIXA_ID is null)
  or
  (STATUS = 'B' and BAIXA_ID is not null)
);

alter table CONTAS_PAGAR
add constraint CK_CONTAS_PAGAR_PARCELAS
check(
  PARCELA between 1 and 360 and 
  NUMERO_PARCELAS between 1 and 360 and 
  PARCELA <= NUMERO_PARCELAS
);

alter table CONTAS_PAGAR
add constraint CK_CONTAS_PAGAR_VALOR_DOC
check(VALOR_DOCUMENTO > 0);

/* 
  ORIGEM
  MAN - Manual
  COM - Compras
  ENT - Entradas
  ENS - Entradas de notas de servi�os
  CON - Conhecimento de frete
  BCP - Baixa de contas a pagar
  BCR - Baixa de contas a receber
  DEV - Devolu��es
  CVE - Cr�dito de venda ( Troco )
  CAC - Cr�dito de acumulado ( Troco )
  CBR - Cr�dito de baixa do contas a receber ( Troco )
  ADA - Adiantamento de pedido acumulado
*/
alter table CONTAS_PAGAR
add constraint CK_CONTAS_PAGAR_ORIGEM
check(
  (ORIGEM = 'COM' and COMPRA_ID is not null)
  or
  (ORIGEM = 'ENT' and ENTRADA_ID is not null)
  or
  (ORIGEM = 'ENS' and ENTRADA_SERVICO_ID is not null)
  or
  (ORIGEM = 'CBX' and COMPRA_BAIXA_ID is not null)
  or
  (ORIGEM = 'CON' and CONHECIMENTO_FRETE_ID is not null)
  or
  (ORIGEM = 'BCP' and BAIXA_ORIGEM_ID is not null)
  or
  (ORIGEM = 'BCR' and BAIXA_RECEBER_ORIGEM_ID is not null)
  or
  (ORIGEM = 'DEV' and DEVOLUCAO_ID is not null and INDICE_CONDICAO_PAGAMENTO is not null)
  or
  (ORIGEM = 'CVE' and ORCAMENTO_ID is not null and INDICE_CONDICAO_PAGAMENTO is not null)
  or
  (ORIGEM = 'CAC' and ACUMULADO_ID is not null and INDICE_CONDICAO_PAGAMENTO is not null)
  or
  (ORIGEM = 'CBR' and BAIXA_RECEBER_ORIGEM_ID is not null and INDICE_CONDICAO_PAGAMENTO is not null)
  or
  (ORIGEM = 'ADA' and BAIXA_RECEBER_ORIGEM_ID is not null and ORCAMENTO_ID is not null)
  or
  (ORIGEM = 'MAN')
);

alter table CONTAS_PAGAR
add constraint CK_CONTAS_PAGAR_BLOQUEADO
check(
  (BLOQUEADO = 'S' and MOTIVO_BLOQUEIO is not null)
  or
  (BLOQUEADO = 'N' and MOTIVO_BLOQUEIO is null)
);

alter table CONTAS_PAGAR
add constraint CK_CONTAS_PAGAR_VALOR_DESC
check(VALOR_DESCONTO >= 0 and VALOR_DESCONTO < VALOR_DOCUMENTO);

alter table CONTAS_PAGAR
add constraint CK_CONTAS_PAGAR_VALOR_ADIANT
check(VALOR_ADIANTADO >= 0 and VALOR_ADIANTADO < VALOR_DOCUMENTO - VALOR_DESCONTO);
