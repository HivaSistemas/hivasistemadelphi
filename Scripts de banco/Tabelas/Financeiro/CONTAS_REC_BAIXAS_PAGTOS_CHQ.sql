create table CONTAS_REC_BAIXAS_PAGTOS_CHQ(
  BAIXA_ID        	number(10) not null,
  COBRANCA_ID     	number(3) not null,
  ITEM_ID         	number(3) not null,
  DATA_VENCIMENTO 	date not null,
  BANCO           	varchar2(5) not null,
  AGENCIA         	varchar2(8) not null,
  CONTA_CORRENTE  	varchar2(8) not null,
  NUMERO_CHEQUE   	number(10) not null,
  VALOR_CHEQUE    	number(8,2) not null,
  NOME_EMITENTE   	varchar2(80),
  TELEFONE_EMITENTE varchar2(15),
	PARCELA           number(2) not null,
	NUMERO_PARCELAS   number(2) not null,
  CPF_CNPJ_EMITENTE varchar2(19)
);

alter table CONTAS_REC_BAIXAS_PAGTOS_CHQ
add constraint PK_CONTAS_REC_BX_PAGTOS_CHQ
primary key(BAIXA_ID, COBRANCA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_REC_BAIXAS_PAGTOS_CHQ
add constraint FK_CONTAS_REC_BX_PAGT_CHQ_B_ID
foreign key(BAIXA_ID)
references CONTAS_RECEBER_BAIXAS(BAIXA_ID);

alter table CONTAS_REC_BAIXAS_PAGTOS_CHQ
add constraint FK_CONTAS_REC_BX_PAGT_CHQ_C_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

/* Constraints */
alter table CONTAS_REC_BAIXAS_PAGTOS_CHQ
add constraint CK_CONTAS_REC_BX_P_CHQ_ITEM_ID
check(ITEM_ID > 0);

alter table CONTAS_REC_BAIXAS_PAGTOS_CHQ
add constraint CK_CONTAS_REC_BX_PAGT_CHQ_VLR
check(VALOR_CHEQUE > 0);

alter table CONTAS_REC_BAIXAS_PAGTOS_CHQ
add constraint CK_CONTAS_REC_BX_PAGT_CHQ_PARC
check(PARCELA > 0 and NUMERO_PARCELAS > 0);