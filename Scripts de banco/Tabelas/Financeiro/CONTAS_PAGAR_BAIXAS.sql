﻿create table CONTAS_PAGAR_BAIXAS(
  BAIXA_ID   		            number(10),
  BAIXA_RECEBER_ORIGEM_ID   number(10),  -- Utilizado quando há um encontro de contas
  ACUMULADO_ID              number(10),
  ORCAMENTO_ID              number(10),
  EMPRESA_ID                number(3) not null,
  CADASTRO_ID               number(10),
  USUARIO_BAIXA_ID          number(4) not null,

  VALOR_TITULOS             number(8,2) not null
  VALOR_JUROS               number(8,2) default 0 not null,
  VALOR_DESCONTO            number(8,2) default 0 not null, -- Desconto efetuado na hora de pagar o título
  VALOR_LIQUIDO             number(8,2) not null,

  VALOR_DINHEIRO            number(8,2) default 0 not null,
  VALOR_CHEQUE              number(8,2) default 0 not null,
  VALOR_CARTAO              number(8,2) default 0 not null,
  VALOR_COBRANCA            number(8,2) default 0 not null,
  VALOR_CREDITO             number(8,2) default 0 not null,
  VALOR_TROCA               number(8,2) default 0 not null, -- Utilizado quando se baixa um crédito a pagar em uma venda, como não há recebimentos caracteriza-se a troca.

  DATA_HORA_BAIXA	          date not null, -- Data que foi efetuado no sistema a baixa.
  DATA_PAGAMENTO            date not null, -- Data no qual realmente foi efetuado o pagamento pelo cliente, porém não havia sido sinalizado no sistema.
  OBSERVACOES               varchar2(200),

  TIPO                      char(3) not null,
  TURNO_ID                  number(10),
  STATUS_CAIXA              char(2) default 'PG' not null,

  PAGAR_ADIANTADO_ID      number(12),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table CONTAS_PAGAR_BAIXAS
add constraint PK_CONTAS_PAGAR_BAIXAS
primary key(BAIXA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_PAGAR_BAIXAS
add constraint FK_CONTAS_PAG_BX_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table CONTAS_PAGAR_BAIXAS
add constraint FK_CONTAS_PAG_BX_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);

alter table CONTAS_PAGAR_BAIXAS
add constraint FK_CONTAS_PAG_BX_USUARIO_BX_ID
foreign key(USUARIO_BAIXA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table CONTAS_PAGAR_BAIXAS
add constraint FK_CONTAS_PAG_BX_BX_CON_R_ID
foreign key(BAIXA_RECEBER_ORIGEM_ID)
references CONTAS_RECEBER_BAIXAS(BAIXA_ID);

alter table CONTAS_PAGAR_BAIXAS
add constraint FK_CONTAS_PG_BX_ACUMULADO_ID
foreign key(ACUMULADO_ID)
references ACUMULADOS(ACUMULADO_ID);

alter table CONTAS_PAGAR_BAIXAS
add constraint FK_CONTAS_PG_BX_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table CONTAS_PAGAR_BAIXAS
add constraint FK_CONTAS_PAG_BX_TURNO_ID
foreign key(TURNO_ID)
references TURNOS_CAIXA(TURNO_ID);

alter table CONTAS_PAGAR_BAIXAS
add constraint FK_CONTAS_PAG_BX_PAG_ADI_ID
foreign key(PAGAR_ADIANTADO_ID)
references CONTAS_PAGAR(PAGAR_ID);

/* Constraints */
/*
  TIPO
  CRE - Crédito, gerando crédito a receber ( É a receber mesmo )
  BCP - Baixa de contas a receber
  BXN - Baixa de títulos normais
  ECP - Encontro de contas com o contas a pagar
  FAC - Fechamento de acumulado
  FPE - Fechamento de pedido
  ADI - Adiantamento
  PIX - Pix
*/
alter table CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAGAR_BAIXAS_TIPO
check(
  TIPO in('CRE', 'BCP', 'BXN', 'PIX')
  or
  (TIPO = 'ECP') and BAIXA_RECEBER_ORIGEM_ID is not null
  or
  TIPO = 'FAC' and ACUMULADO_ID is not null
  or
  TIPO = 'FPE' and ORCAMENTO_ID is not null
  or
  TIPO = 'ADI' and PAGAR_ADIANTADO_ID is not null
);

alter table CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAG_BX_TIPO_CAD_ID
check(TIPO in('CRE', 'BCP', 'BXN', 'ECP', 'FAC', 'FPE', 'ADI') and CADASTRO_ID is not null);

alter table CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAG_BX_VALOR_DESCONT
check(VALOR_DESCONTO >= 0);

alter table CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAG_BX_VALOR_JUROS
check(VALOR_JUROS >= 0);

alter table CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAG_BX_VALOR_TITULOS
check(VALOR_TITULOS > 0);

alter table CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAG_BX_VALOR_DINHEIR
check(VALOR_DINHEIRO >= 0);

alter table CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAG_BX_VALOR_CHEQUE
check(VALOR_CHEQUE >= 0);

alter table CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAG_BX_VALOR_CARTAO
check(VALOR_CARTAO >= 0);

alter table CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAG_BX_VALOR_COBRANC
check(VALOR_COBRANCA >= 0);

alter table CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAG_BX_VALOR_CREDITO
check(VALOR_CREDITO >= 0);

alter table CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAG_BX_VALOR_TROCA
check(VALOR_TROCA >= 0);

alter table CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAG_BX_SOMA_PAGTOS
check(VALOR_DINHEIRO + VALOR_CHEQUE + VALOR_CARTAO + VALOR_COBRANCA + VALOR_CREDITO + VALOR_TROCA) >= VALOR_TITULOS + VALOR_JUROS - VALOR_DESCONTO);

/*
  STATUS_CAIXA
  EC - Enviando para o caixa pagar
  PG - Pago
*/
alter talbe CONTAS_PAGAR_BAIXAS
add constraint CK_CONTAS_PAG_BX_STATUS_CAIXA
check(
  (STATUS_CAIXA = 'EC' and TURNO_ID is null)
  or
  (STATUS_CAIXA = 'PG')
);
