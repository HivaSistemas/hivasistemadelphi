create table APURACOES_ENTRADAS(
  ENTRADA_ID            number(12) not null,
  EMPRESA_ID            number(3) not null,
  
  CFOP_ID               varchar2(7) not null,
  
  VALOR_CONTABIL        number(8,2) default 0 not null,
  DATA_CONTABIL         date not null,
  
  BASE_CALCULO_ICMS     number(8,2) default 0 not null,
  VALOR_ICMS            number(8,2) default 0 not null,
  
  BASE_CACULO_ICMS_ST   number(8,2) default 0 not null,
  VALOR_ICMS_ST         number(8,2) default 0 not null,  
  
  VALOR_IPI             number(8,2) default 0 not null,
  
  BASE_CACULO_PIS       number(8,2) default 0 not null,
  VALOR_PIS             number(8,2) default 0 not null,
  
  BASE_CACULO_COFINS    number(8,2) default 0 not null,
  VALOR_COFINS          number(8,2) default 0 not null,  
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table APURACOES_ENTRADAS
add constraint PK_APURACOES_ENTRADAS
primary key(ENTRADA_ID, CFOP_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table APURACOES_ENTRADAS
add constraint FK_APUR_ENTRADAS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table APURACOES_ENTRADAS
add constraint FK_APUR_ENTR_NOTA_FISCAL_ID
foreign key(ENTRADA_ID)
references ENTRADAS_NOTAS_FISCAIS(ENTRADA_ID);

alter table APURACOES_ENTRADAS
add constraint FK_APURACOES_ENTRADAS_CFOP_ID
foreign key(CFOP_ID)
references CFOP(CFOP_PESQUISA_ID);

/* Checagens */
alter table APURACOES_ENTRADAS
add constraint CK_APURACOES_ENTR_VALOR_CONT
check(VALOR_CONTABIL >= 0);

alter table APURACOES_ENTRADAS
add constraint CK_APURACOES_ENT_BASE_CAL_ICMS
check(BASE_CALCULO_ICMS >= 0);

alter table APURACOES_ENTRADAS
add constraint CK_APURACOES_ENTR_VALOR_ICMS
check(VALOR_ICMS >= 0);

alter table APURACOES_ENTRADAS
add constraint CK_APURACOES_ENTR_BC_ICMS_ST
check(BASE_CACULO_ICMS_ST >= 0);

alter table APURACOES_ENTRADAS
add constraint CK_APURACOES_ENT_VALOR_ICMS_ST
check(VALOR_ICMS_ST >= 0);
  
alter table APURACOES_ENTRADAS
add constraint CK_APURACOES_ENTR_VALOR_IPI
check(VALOR_IPI >= 0);
                 
alter table APURACOES_ENTRADAS
add constraint CK_APURACOES_ENTR_BASE_CAC_PIS
check(BASE_CACULO_PIS >= 0);

alter table APURACOES_ENTRADAS
add constraint CK_APURACOES_ENTR_VALOR_PIS
check(VALOR_PIS >= 0);

alter table APURACOES_ENTRADAS
add constraint CK_APURACOES_ENTR_BASE_CAC_COF
check(BASE_CACULO_COFINS >= 0);

alter table APURACOES_ENTRADAS
add constraint CK_APURACOES_ENTR_VALOR_COFINS
check(VALOR_COFINS >= 0);