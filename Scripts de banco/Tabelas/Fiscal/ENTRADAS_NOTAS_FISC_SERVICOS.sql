﻿create table ENTRADAS_NOTAS_FISC_SERVICOS(
  ENTRADA_ID          number(10) not null,
  EMPRESA_ID          number(3) not null,
  FORNECEDOR_ID       number(10) not null,
  NUMERO_NOTA         number(9) not null,
  MODELO_NOTA         varchar2(3) default '99' not null,
  SERIE_NOTA          varchar2(3) default '1' not null,
  DATA_EMISSAO        date not null,
  DATA_CONTABIL       date not null,
  CFOP_ID             varchar2(7) not null,
  VALOR_TOTAL_NOTA    number(8,2) not null,
  PLANO_FINANCEIRO_ID varchar2(9) not null,
  CENTRO_CUSTO_ID     number(3),
  
  DATA_HORA_CADASTRO  date not null,
  USUARIO_CADASTRO_ID number(4) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table ENTRADAS_NOTAS_FISC_SERVICOS
add constraint PK_ENTRADAS_NOTAS_FISC_SERV
primary key(ENTRADA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ENTRADAS_NOTAS_FISC_SERVICOS
add constraint FK_ENT_NF_SERV_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table ENTRADAS_NOTAS_FISC_SERVICOS
add constraint FK_ENT_NF_SERV_FORNECEDOR_ID
foreign key(FORNECEDOR_ID)
references FORNECEDORES(CADASTRO_ID);

alter table ENTRADAS_NOTAS_FISC_SERVICOS
add constraint FK_ENT_NOTAS_FISC_SERV_CFOP_ID
foreign key(CFOP_ID)
references CFOP(CFOP_PESQUISA_ID);

alter table ENTRADAS_NOTAS_FISC_SERVICOS
add constraint FK_ENT_NF_SERV_PLANO_FINANC_ID
foreign key(PLANO_FINANCEIRO_ID)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

alter table ENTRADAS_NOTAS_FISC_SERVICOS
add constraint FK_ENT_NF_SERV_USUARIO_CADR_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table ENTRADAS_NOTAS_FISC_SERVICOS
add constraint FK_ENT_NF_SERVIC_CENTRO_CUS_ID
foreign key(CENTRO_CUSTO_ID)
references CENTROS_CUSTOS(CENTRO_CUSTO_ID);

/* Checagens */
alter table ENTRADAS_NOTAS_FISC_SERVICOS
add constraint CK_ENT_NF_SERV_VALOR_TOTAL_NOT
check(VALOR_TOTAL_NOTA > 0);