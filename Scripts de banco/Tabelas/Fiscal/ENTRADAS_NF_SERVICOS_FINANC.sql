create table ENTRADAS_NF_SERVICOS_FINANC(
  ENTRADA_ID        number(10) not null,
  ITEM_ID           number(3) not null,
  COBRANCA_ID       number(3) not null,

  DATA_VENCIMENTO   date not null,
  PARCELA           number(3) not null,
  NUMERO_PARCELAS   number(3) not null,
  VALOR_DOCUMENTO   number(8,2) not null,
  
  NOSSO_NUMERO     varchar2(30),
  CODIGO_BARRAS    varchar2(44),
  DOCUMENTO        varchar2(20) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table ENTRADAS_NF_SERVICOS_FINANC
add constraint PK_ENTRADAS_NF_SERVICOS_FINANC
primary key(ENTRADA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ENTRADAS_NF_SERVICOS_FINANC
add constraint FK_ENT_NF_SERV_FIN_ENTRADA_ID
foreign key(ENTRADA_ID)
references ENTRADAS_NOTAS_FISC_SERVICOS(ENTRADA_ID);

alter table ENTRADAS_NF_SERVICOS_FINANC
add constraint FK_ENT_NF_SERV_FIN_COBR_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

/* Constraints */
alter table ENTRADAS_NF_SERVICOS_FINANC
add constraint CK_ENT_NF_SERVICOS_PARCELA
check(PARCELA > 0);

alter table ENTRADAS_NF_SERVICOS_FINANC
add constraint CK_ENT_NF_SERVICOS_NUM_PARC
check(NUMERO_PARCELAS >= PARCELA);

alter table ENTRADAS_NF_SERVICOS_FINANC
add constraint CK_ENT_NF_SERVICOS_VALOR_DOC
check(VALOR_DOCUMENTO > 0);