create table NOTAS_FISCAIS_ITENS(
  NOTA_FISCAL_ID              number(12) not null,
  PRODUTO_ID                  number(10) not null,
  ITEM_ID                     number(3) not null,
  NOME_PRODUTO                varchar2(60) not null,
  UNIDADE                     varchar2(5) not null,
  CFOP_ID                     varchar2(7) not null,
  CST                         char(2) not null,
  CODIGO_NCM                  varchar2(8),
  VALOR_TOTAL                 number(8,2) not null,
  PRECO_UNITARIO              number(8,2) not null,
  QUANTIDADE                  number(20,4) not null,
  VALOR_TOTAL_DESCONTO        number(8,2) not null,
  VALOR_TOTAL_OUTRAS_DESPESAS number(8,2) not null,
  CODIGO_BARRAS               varchar2(14),
  ORIGEM_PRODUTO              number(1) default 0 not null,
  CEST                        varchar2(8),
  
  INDICE_REDUCAO_BASE_ICMS    number(5,4) not null,  
  BASE_CALCULO_ICMS           number(8,2) not null,
  PERCENTUAL_ICMS             number(4,2) not null,
  VALOR_ICMS                  number(8,2) not null,    
  
  INDICE_REDUCAO_BASE_ICMS_ST number(5,4) not null,
  BASE_CALCULO_ICMS_ST        number(8,2) not null,
  PERCENTUAL_ICMS_ST          number(4,2) not null,
  VALOR_ICMS_ST               number(8,2) not null, 
  /* PIS */
  CST_PIS                     char(2) default '99' not null,
  BASE_CALCULO_PIS            number(8,2) not null,
  PERCENTUAL_PIS              number(4,2) not null,
  VALOR_PIS                   number(8,2) not null,
  /* ------------------------------------------------ */
  /* COFINS */
  CST_COFINS                  char(2) default '99' not null,
  BASE_CALCULO_COFINS         number(8,2) not null,
  PERCENTUAL_COFINS           number(4,2) not null,
  VALOR_COFINS                number(8,2) not null,
  /* ----------------------------------------------- */
  /* IPI */
  VALOR_IPI                   number(8,2) not null,
  PERCENTUAL_IPI              number(4,2) not null,
  /* ----------------------------------------------- */
  IVA                         number(4,2) default 0 not null,
  PRECO_PAUTA                 number(8,2) default 0 not null,
  
  INFORMACOES_ADICIONAIS      varchar2(500),
  CODIGO_PRODUTO_NFE          varchar2(60),
  NUMERO_ITEM_PEDIDO          number(10,0) default 0 not null,
  NUMERO_PEDIDO               number(10,0) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table NOTAS_FISCAIS_ITENS
add constraint PK_NOTAS_FISCAIS_ITENS
primary key(NOTA_FISCAL_ID, PRODUTO_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table NOTAS_FISCAIS_ITENS
add constraint FK_NF_ITENS_NOTA_FISCAL_ID
foreign key(NOTA_FISCAL_ID)
references NOTAS_FISCAIS(NOTA_FISCAL_ID);

alter table NOTAS_FISCAIS_ITENS
add constraint FK_NF_ITENS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table NOTAS_FISCAIS_ITENS
add constraint FK_NF_ITENS_CFOP_ID
foreign key(CFOP_ID)
references CFOP(CFOP_PESQUISA_ID);

/* Constraints */
alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_PERCENTUAIS_TRIBUT
check(
  (PERCENTUAL_ICMS between 0 and 99.99) 
  and
  (PERCENTUAL_COFINS between 0 and 99.99)
  and
  (PERCENTUAL_IPI between 0 and 99.99)
);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_VALOR_TOTAL
check(VALOR_TOTAL > 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_PRECO_UNITARIO
check(
  PRECO_UNITARIO >= 0 and substr(CFOP_ID, 1, 4) in('5601', '5602')
  or
  PRECO_UNITARIO > 0
);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_QUANTIDADE
check(QUANTIDADE > 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_VALOR_TOTAL_DESC
check(VALOR_TOTAL_DESCONTO >= 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_VLR_TOT_OUT_DESP
check(VALOR_TOTAL_OUTRAS_DESPESAS >= 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_BASE_CALCULO_ICMS
check(BASE_CALCULO_ICMS >= 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_VALOR_ICMS
check(VALOR_ICMS >= 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_BASE_CALC_ICMS_ST
check(BASE_CALCULO_ICMS_ST >= 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_BASE_CALC_PIS
check(BASE_CALCULO_PIS >= 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_VALOR_PIS
check(VALOR_PIS >= 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_BASE_CALC_COF
check(BASE_CALCULO_COFINS >= 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_VALOR_COFINS
check(VALOR_COFINS >= 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITENS_BASE_VALOR_IPI
check(VALOR_IPI >= 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NOTAS_FISCAIS_ITENS_CST
check(CST in('00','10','20','30','40','41','50','51','60','70','90'));

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NOTAS_FIS_ITENS_CST_PIS_COF
check(
  CST_PIS in('01','02','03','04','05','06','07','08','09','49','50','51','52','53','54','55','56','60','61','62','63','64','65','66','67','70','71','72','73','74','75','98','99')
  and
  CST_COFINS in('01','02','03','04','05','06','07','08','09','49','50','51','52','53','54','55','56','60','61','62','63','64','65','66','67','70','71','72','73','74','75','98','99')
);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITE_IND_RED_BASE_ICMS
check(INDICE_REDUCAO_BASE_ICMS > 0);

alter table NOTAS_FISCAIS_ITENS
add constraint CK_NF_ITE_IND_RED_BASE_ICMS_ST
check(INDICE_REDUCAO_BASE_ICMS_ST > 0);
