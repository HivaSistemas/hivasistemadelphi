create table NOTAS_FISCAIS_XML(
  NOTA_FISCAL_ID number(12),
  XML            blob,
  XML_TEXTO      clob
);

alter table NOTAS_FISCAIS_XML
add constraint PK_NOTAS_FISCAIS_XML
primary key(NOTA_FISCAL_ID)
using index tablespace INDICES;
