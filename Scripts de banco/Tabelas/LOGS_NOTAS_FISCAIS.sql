create table LOGS_NOTAS_FISCAIS(
  TIPO_ALTERACAO_LOG_ID   number(4) not null,
  NOTA_FISCAL_ID          number(12) not null,
  VALOR_ANTERIOR          varchar2(300),
  NOVO_VALOR              varchar2(300),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chaves estrangeiras */
alter table LOGS_NOTAS_FISCAIS
add constraint FK_LOGS_NOTAS_FISCAIS_ID
foreign key(NOTA_FISCAL_ID)
references NOTAS_FISCAIS(NOTA_FISCAL_ID);
