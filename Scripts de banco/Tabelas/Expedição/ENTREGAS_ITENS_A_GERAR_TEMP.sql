﻿create table ENTREGAS_ITENS_A_GERAR_TEMP(
  ORCAMENTO_ID     number(10) not null,
  PRODUTO_ID       number(10) not null,
  ITEM_ID          number(3) not null,
  QUANTIDADE       number(20,4) not null,
  TIPO_ENTREGA     char(2) not null,
  PREVISAO_ENTREGA date
);

/* Chave primária */
alter table ENTREGAS_ITENS_A_GERAR_TEMP
add constraint CK_ENTREGAS_ITENS_A_GERAR_TEMP
primary key(ORCAMENTO_ID, PRODUTO_ID, ITEM_ID, TIPO_ENTREGA)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ENTREGAS_ITENS_A_GERAR_TEMP
add constraint CK_ENTREGAS_ITE_GERAR_TEMP_FK
foreign key(ORCAMENTO_ID, PRODUTO_ID, ITEM_ID)
references ORCAMENTOS_ITENS(ORCAMENTO_ID, PRODUTO_ID, ITEM_ID);

/* Constraints */
alter table ENTREGAS_ITENS_A_GERAR_TEMP
add constraint CK_ENT_ITE_GERAR_TEMP_QTDE
check(QUANTIDADE > 0);

alter table ENTREGAS_ITENS_A_GERAR_TEMP
add constraint CK_ENT_ITE_GERAR_TEMP_TIPO_ENT
check(TIPO_ENTREGA in('RA', 'RE', 'EN', 'SP', 'SE'));

alter table ENTREGAS_ITENS_A_GERAR_TEMP
add constraint CK_ENT_ITE_GERAR_TEMP_PREV_ENT
check(
  TIPO_ENTREGA <> 'EN' and PREVISAO_ENTREGA is null
  or 
  TIPO_ENTREGA = 'EN' and PREVISAO_ENTREGA is not null
);