create table CONTR_SEPARAC_ENTREGAS_ITENS(
  CONTROLE_SEPARACAO_ID number(12) not null,
  ENTREGA_ID            number(12) not null,
  ITEM_ID               number(3) not null,
  LOTE                  varchar2(80) default '???' not null,
  
  QUANTIDADE            number(20,4) not null,
  SEPARADOS             number(20,4) default 0 not null,
  NAO_SEPARADOS         number(20,4) default 0 not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null    
);

/* Chave primária */
alter table CONTROLES_SEPARACOES
add constraint PK_CONTROLES_SEPARACOES
primary key(CONTROLE_SEPARACAO_ID)
using index TABLESPACE INDICES;