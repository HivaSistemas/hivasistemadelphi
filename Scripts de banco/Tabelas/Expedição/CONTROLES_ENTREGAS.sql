create table CONTROLES_ENTREGAS(
  CONTROLE_ENTREGA_ID number(10) not null,
  EMPRESA_ID          number(3) not null,
  VEICULO_ID          number(5) not null,
  MOTORISTA_ID        number(10) not null,
  USUARIO_CADASTRO_ID number(4) not null,
  DATA_HORA_CADASTRO  date not null,
  STATUS              char(1) default 'T' not null,
  USUARIO_BAIXA_ID    number(4),
  DATA_HORA_BAIXA     date,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table CONTROLES_ENTREGAS
add constraint PK_CONTROLES_ENTREGAS
primary key(CONTROLE_ENTREGA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTROLES_ENTREGAS
add constraint FK_CONTROLES_ENTREG_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table CONTROLES_ENTREGAS
add constraint FK_CONTROLES_ENTREG_VEICULO_ID
foreign key(VEICULO_ID)
references VEICULOS(VEICULO_ID);

alter table CONTROLES_ENTREGAS
add constraint FK_CONTROLES_ENTR_MOTORISTA_ID
foreign key(MOTORISTA_ID)
references MOTORISTAS(CADASTRO_ID);

alter table CONTROLES_ENTREGAS
add constraint FK_CONTROLES_ENTR_USUAR_CAD_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table CONTROLES_ENTREGAS
add constraint FK_CONTROLES_ENTR_USUAR_BAI_ID
foreign key(USUARIO_BAIXA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* Checagens */
/*
  STATUS
  T - Transporte
  B - Baixado
*/
alter table CONTROLES_ENTREGAS
add constraint CK_CONTROLES_ENTREGAS_STATUS
check(
  (STATUS = 'T' and USUARIO_BAIXA_ID is null and DATA_HORA_BAIXA is null)
  or
  (STATUS = 'B' and USUARIO_BAIXA_ID is not null and DATA_HORA_BAIXA is not null)
);