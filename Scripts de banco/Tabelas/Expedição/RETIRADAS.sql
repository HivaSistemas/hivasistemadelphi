create table RETIRADAS(
  RETIRADA_ID                   number(12),
  EMPRESA_ID                    number(3) not null,
  LOCAL_ID                      number(5) not null
  ORCAMENTO_ID                  number(10) not null,
  USUARIO_CADASTRO_ID           number(4) not null,
  DATA_HORA_CADASTRO            date not null,
  NOTA_TRANSF_ESTOQUE_FISCAL_ID number(12),
  TIPO_MOVIMENTO                char(1) default 'A' not null,
  CONFIRMADA                    char(1) not null,
  AGRUPADOR_ID                  number(12) default 0 not null,
  USUARIO_CONFIRMACAO_ID        number(4),
  DATA_HORA_RETIRADA            date,
  NOME_PESSOA_RETIRADA          varchar2(100),
  CPF_PESSOA_RETIRADA           varchar2(14),
  TIPO_NOTA_GERAR               char(2) default 'NI' not null,
  OBSERVACOES                   varchar2(200),
  QTDE_VIAS_IMPRESSAS_COMP_RET  number(2) default 0 not null,
  QTDE_VIAS_IMPRESSAS_LISTA_SEP number(2) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table RETIRADAS
add constraint PK_RETIRADAS
primary key(RETIRADA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table RETIRADAS
add constraint FK_RETIRADAS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table RETIRADAS
add constraint FK_RETIRADAS_LOCAL_ID
foreign key(LOCAL_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table RETIRADAS
add constraint FK_RETIRADAS_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table RETIRADAS
add constraint FK_RETIRADAS_USU_CADASTRO_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table RETIRADAS
add constraint FK_RETIRADAS_USU_CONFIRMAC_ID
foreign key(USUARIO_CONFIRMACAO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table RETIRADAS
add constraint FK_RETIRADAS_NF_TR_EST_FIS_ID
foreign key(NOTA_TRANSF_ESTOQUE_FISCAL_ID)
references NOTAS_FISCAIS(NOTA_FISCAL_ID);

/* Constraints */
alter table RETIRADAS
add constraint CK_RETIRADAS_CONFIRMADA
check(CONFIRMADA in('S', 'N'));

/* 
  TIPO_MOVIMENTO
  A - Ato
  R - Retirada
*/
alter table RETIRADAS
add constraint CK_RETIRADAS_TIPO_MOVIMENTO
check(TIPO_MOVIMENTO in('A','R'));

alter table RETIRADAS
add constraint CK_RETIRADAS_CONFIRMACOES
check(
  CONFIRMADA = 'N' and USUARIO_CONFIRMACAO_ID is null and DATA_HORA_RETIRADA is null and NOME_PESSOA_RETIRADA is null and CPF_PESSOA_RETIRADA is null and OBSERVACOES is null
  or
  CONFIRMADA = 'S' and USUARIO_CONFIRMACAO_ID is not null and DATA_HORA_RETIRADA is not null and NOME_PESSOA_RETIRADA is not null
);

/* TIPO_NOTA_GERAR
  NI - N�o imprimir
  NE - NFCe
  NF - NFe
*/
alter table RETIRADAS
add constraint CK_RATIRADAS_TIPO_NF_GERAR
check(TIPO_NOTA_GERAR in('NI', 'NE', 'NF'));

alter table RETIRADAS
add constraint CK_RET_QTDE_VIAS_IMPR_COMP_RET
check(QTDE_VIAS_IMPRESSAS_COMP_RET >= 0);

alter table RETIRADAS
add constraint CK_RET_QTDE_VIAS_IMP_LISTA_SEP
check(QTDE_VIAS_IMPRESSAS_LISTA_SEP >= 0);
