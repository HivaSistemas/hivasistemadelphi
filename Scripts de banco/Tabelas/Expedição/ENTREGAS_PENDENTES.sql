create table ENTREGAS_PENDENTES(
  EMPRESA_ID           number(3) not null,
  LOCAL_ID             number(5) not null,
  ORCAMENTO_ID         number(12) not null,
  PREVISAO_ENTREGA     date not null,
  DATA_HORA_CADASTRO   date,
  USUARIO_CADASTRO_ID  number(4),

  TIPO_RETORNO_ENTREGA char(3) default 'NHR' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

alter table ENTREGAS_PENDENTES
add constraint PK_ENTREGAS_PENDENTES
primary key(EMPRESA_ID, LOCAL_ID, ORCAMENTO_ID, PREVISAO_ENTREGA)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ENTREGAS_PENDENTES
add constraint FK_ENTREGAS_PEND_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table ENTREGAS_PENDENTES
add constraint FK_ENTREGAS_PEND_LOCAL_ID
foreign key(LOCAL_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table ENTREGAS_PENDENTES
add constraint FK_ENTREGAS_PEND_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table ENTREGAS_PENDENTES
add constraint FK_ENTREGAS_PEN_USUARIO_CAD_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* Checagens */
/*
  TIPO_RETORNO_ENTREGA
  NHR - N�o houve retorno
  NHS - N�o houve separa��o
  RTO - Retorno total
  RPA - Retorno parcial
*/
alter table ENTREGAS_PENDENTES
add constraint CK_ENTREGAS_PEND_TIPO_RET_ENT
check(TIPO_RETORNO_ENTREGA in('NHR', 'NHS', 'RTO', 'RPA'));