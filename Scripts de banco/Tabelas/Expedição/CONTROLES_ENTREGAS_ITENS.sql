create table CONTROLES_ENTREGAS_ITENS(
  CONTROLE_ENTREGA_ID number(10) not null,
  ENTREGA_ID          number(10) not null,
  
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table CONTROLES_ENTREGAS_ITENS
add constraint PK_CONTROLES_ENTREGAS_ITENS
primary key(CONTROLE_ENTREGA_ID, ENTREGA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTROLES_ENTREGAS_ITENS
add constraint FK_CONT_ENTR_ITE_CONT_ENTRE_ID
foreign key(CONTROLE_ENTREGA_ID)
references CONTROLES_ENTREGAS(CONTROLE_ENTREGA_ID);

alter table CONTROLES_ENTREGAS_ITENS
add constraint FK_CONTROL_ENTR_ITE_ENTREGA_ID
foreign key(ENTREGA_ID)
references ENTREGAS(ENTREGA_ID);