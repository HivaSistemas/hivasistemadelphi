﻿create table ENTREGAS_ITENS(
  ENTREGA_ID           number(12),
  PRODUTO_ID           number(10) not null,
  ITEM_ID              number(3) not null,
  LOTE                 varchar2(80) default '???' not null,

  QUANTIDADE           number(20,4) not null,
  RETORNADOS           number(20,4) default 0 not null,
  DEVOLVIDOS           number(20,4) default 0 not null,
  NAO_SEPARADOS        number(20,4) default 0 not null, -- Produtos que não foram encontrados durante a conferência

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table ENTREGAS_ITENS
add constraint PK_ENTREGAS_ITENS
primary key(ENTREGA_ID, ITEM_ID, LOTE)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ENTREGAS_ITENS
add constraint FK_ENTREGAS_ITENS_ENTREGA_ID
foreign key(ENTREGA_ID)
references ENTREGAS(ENTREGA_ID);

alter table ENTREGAS_ITENS
add constraint FK_ENTREGAS_ITENS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* Constraints */
alter table ENTREGAS_ITENS
add constraint CK_ENTREGAS_ITENS_QUANTIDADE
check(QUANTIDADE > 0 and QUANTIDADE >= RETORNADOS + DEVOLVIDOS);

alter table ENTREGAS_ITENS
add constraint CK_ENTREGAS_ITENS_RETORNADOS
check(RETORNADOS >= 0);

alter table ENTREGAS_ITENS
add constraint CK_ENTREGAS_ITENS_DEVOLVIDOS
check(DEVOLVIDOS >= 0);

alter table ENTREGAS_ITENS
add constraint CK_ENTREGAS_ITENS_NAO_SEP
check(NAO_SEPARADOS >= 0 and NAO_SEPARADOS <= NAO_SEPARADOS);

alter table ENTREGAS_ITENS
add constraint CK_ENTREGAS_ITENS_NAO_SEP
check(NAO_SEPARADOS >= 0 and NAO_SEPARADOS <= NAO_SEPARADOS);