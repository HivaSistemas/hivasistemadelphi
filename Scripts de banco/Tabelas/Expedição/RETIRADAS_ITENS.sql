create table RETIRADAS_ITENS(
  RETIRADA_ID          number(12),
  PRODUTO_ID           number(10) not null,
  ITEM_ID              number(3) not null,
  LOTE                 varchar2(80) not null,
  QUANTIDADE           number(20,4) not null,
  DEVOLVIDOS           number(20,4) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table RETIRADAS_ITENS
add constraint PK_RETIRADAS_ITENS
primary key(RETIRADA_ID, ITEM_ID, LOTE)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table RETIRADAS_ITENS
add constraint FK_RETIRADAS_ITENS_RETIRADA_ID
foreign key(RETIRADA_ID)
references RETIRADAS(RETIRADA_ID);

alter table RETIRADAS_ITENS
add constraint FK_RETIRADAS_ITENS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* Constraints */
alter table RETIRADAS_ITENS
add constraint CK_RETIRADAS_ITENS_QUANTIDADE
check(QUANTIDADE > 0);

alter table RETIRADAS_ITENS
add constraint CK_RETIRADAS_ITENS_DEVOLVIDOS
check(DEVOLVIDOS >= 0);
