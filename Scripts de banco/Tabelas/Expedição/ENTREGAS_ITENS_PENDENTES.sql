create table ENTREGAS_ITENS_PENDENTES(
  EMPRESA_ID           number(3) not null,
  LOCAL_ID             number(5) not null,
  LOCAL_ID             number(5) not null,
  ORCAMENTO_ID         number(12),
  PRODUTO_ID           number(10) not null,
  ITEM_ID              number(3) not null,  
  LOTE                 varchar2(80) default '???' not null,
  PREVISAO_ENTREGA     date not null,
  
  QUANTIDADE           number(20, 4) not null,
  ENTREGUES            number(20, 4) default 0 not null,
  DEVOLVIDOS           number(20, 4) default 0 not null,
  SALDO                number(20, 4) not null,

  EM_SEPARACAO         number(20, 4) default 0 not null,
  SEPARADOS            number(20, 4) default 0 not null,

  AGUARDAR_CONTATO_CLIENTE char(1) default 'N' not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID     number(4) not null,
  DATA_HORA_ALTERACAO   date not null,
  ROTINA_ALTERACAO      varchar2(30) not null,
  ESTACAO_ALTERACAO     varchar2(30) not null
);

alter table ENTREGAS_ITENS_PENDENTES
add constraint PK_ENTREGAS_ITENS_PENDENTES
primary key(EMPRESA_ID, LOCAL_ID, ORCAMENTO_ID, PRODUTO_ID, ITEM_ID, LOTE, PREVISAO_ENTREGA)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ENTREGAS_ITENS_PENDENTES
add constraint FK_ENT_ITE_PEND_EMP_ORC_LOC_ID
foreign key(EMPRESA_ID, ORCAMENTO_ID, LOCAL_ID, PREVISAO_ENTREGA)
references ENTREGAS_PENDENTES(EMPRESA_ID, ORCAMENTO_ID, LOCAL_ID, PREVISAO_ENTREGA);

alter table ENTREGAS_ITENS_PENDENTES
add constraint FK_ENT_ITE_PEND_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table ENTREGAS_ITENS_PENDENTES
add constraint FK_ENT_ITE_PEND_LOCAL_ID
foreign key(LOCAL_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table ENTREGAS_ITENS_PENDENTES
add constraint FK_ENT_ITE_PEND_ORC_PRO_ITE_ID
foreign key(ORCAMENTO_ID, PRODUTO_ID, ITEM_ID)
references ORCAMENTOS_ITENS(ORCAMENTO_ID, PRODUTO_ID, ITEM_ID);

/* Constraints */
alter table ENTREGAS_ITENS_PENDENTES
add constraint CK_ENTRE_ITENS_PEND_QUANTIDADE
check(QUANTIDADE > 0 and QUANTIDADE = ENTREGUES + DEVOLVIDOS + SALDO);

alter table ENTREGAS_ITENS_PENDENTES
add constraint CK_ENTRE_ITENS_PEND_ENTREGUES
check(ENTREGUES >= 0);

alter table ENTREGAS_ITENS_PENDENTES
add constraint CK_ENTRE_ITENS_PEND_DEVOLVIDOS
check(DEVOLVIDOS >= 0);

alter table ENTREGAS_ITENS_PENDENTES
add constraint CK_ENTREGAS_ITENS_PEND_SALDO
check(SALDO >= 0);

alter table ENTREGAS_ITENS_PENDENTES
add constraint CK_ENT_ITENS_PEND_AGUA_CON_CLI
check(AGUARDAR_CONTATO_CLIENTE in('S', 'N'));

alter table RETIRADAS_ITENS_PENDENTES
add constraint CK_RET_ITENS_PEN_EM_SEPARAC
check(
  EM_SEPARACAO = 0
  or
  SALDO >= EM_SEPARACAO
);

alter table RETIRADAS_ITENS_PENDENTES
add constraint CK_RET_ITENS_PEN_SEPARADOS
check(
  SEPARADOS = 0
  or
  QUANTIDADE >= SEPARADOS
);