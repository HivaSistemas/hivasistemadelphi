create table ENTREGAS_ITENS_SEM_PREVISAO(
  ORCAMENTO_ID             number(10) not null, 
  PRODUTO_ID               number(10) not null,
  ITEM_ID                  number(3) not null,
  QUANTIDADE               number(20,4) not null,
  ENTREGUES                number(20,4) default 0 not null,
  DEVOLVIDOS               number(20,4) default 0 not null,
  SALDO                    number(20,4) not null,
  AGUARDAR_CONTATO_CLIENTE char(1) default 'N' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table ENTREGAS_ITENS_SEM_PREVISAO
add constraint PK_ENTREGAS_ITENS_SEM_PREVISAO
primary key(ORCAMENTO_ID, PRODUTO_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ENTREGAS_ITENS_SEM_PREVISAO
add constraint FK_ENT_ITE_SEM_PR_ORC_PR_IT_ID
foreign key(ORCAMENTO_ID, PRODUTO_ID, ITEM_ID)
references ORCAMENTOS_ITENS(ORCAMENTO_ID, PRODUTO_ID, ITEM_ID);

/* Constraints */
alter table ENTREGAS_ITENS_SEM_PREVISAO
add constraint CK_ENT_ITE_SEM_PR_QUANTIDADE
check(QUANTIDADE > 0 and QUANTIDADE >= ENTREGUES + DEVOLVIDOS);

alter table ENTREGAS_ITENS_SEM_PREVISAO
add constraint CK_ENT_ITE_SEM_PR_ENTREGUES
check(ENTREGUES >= 0);

alter table ENTREGAS_ITENS_SEM_PREVISAO
add constraint CK_ENT_ITE_SEM_PR_DEVOLVIDOS
check(DEVOLVIDOS >= 0);

alter table ENTREGAS_ITENS_SEM_PREVISAO
add constraint CK_ENT_ITE_SEM_PR_SALDO
check(SALDO >= 0 and QUANTIDADE >= SALDO);

alter table ENTREGAS_ITENS_SEM_PREVISAO
add constraint CK_ENT_ITE_SEM_PR_AGU_CONT_CLI
check(AGUARDAR_CONTATO_CLIENTE in('S', 'N'));
