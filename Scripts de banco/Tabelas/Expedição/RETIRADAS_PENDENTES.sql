create table RETIRADAS_PENDENTES(
  EMPRESA_ID           number(3) not null,
  LOCAL_ID             number(5) not null,
  ORCAMENTO_ID         number(12) not null,
  DATA_HORA_CADASTRO   date,
  USUARIO_CADASTRO_ID  number(4),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

alter table RETIRADAS_PENDENTES
add constraint PK_RETIRADAS_PENDENTES
primary key(EMPRESA_ID, LOCAL_ID, ORCAMENTO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table RETIRADAS_PENDENTES
add constraint FK_RETIRADAS_PEND_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table RETIRADAS_PENDENTES
add constraint FK_RETIRADAS_PEND_LOCAL_ID
foreign key(LOCAL_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table RETIRADAS_PENDENTES
add constraint FK_RETIRADAS_PEND_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table RETIRADAS_PENDENTES
add constraint FK_RET_PEND_USUARIO_CAD_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);