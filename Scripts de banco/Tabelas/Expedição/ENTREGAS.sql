create table ENTREGAS(
  ENTREGA_ID                    number(12),
  EMPRESA_ENTREGA_ID            number(3) not null,
  EMPRESA_GERACAO_ID            number(3) not null,
  LOCAL_ID                      number(5) not null,
  ORCAMENTO_ID                  number(10) not null,
  USUARIO_CADASTRO_ID           number(4) not null,
  DATA_HORA_CADASTRO            date not null,
  NOTA_TRANSF_ESTOQUE_FISCAL_ID number(12),
  STATUS                        char(3) not null,

  PREVISAO_ENTREGA              date not null,
  AGRUPADOR_ID                  number(12) not null,
  CONTROLE_ENTREGA_ID           number(10),
  USUARIO_CONFIRMACAO_ID        number(4),

  DATA_HORA_REALIZOU_ENTREGA    date,
  NOME_PESSOA_RECEBEU_ENTREGA   varchar2(60),
  CPF_PESSOA_RECEBEU_ENTREGA    varchar2(14),
  OBSERVACOES                   varchar2(200),

  TIPO_NOTA_GERAR               char(2) default 'NI' not null,

  USUARIO_INICIO_SEPARACAO_ID   number(4),
  DATA_HORA_INICIO_SEPARACAO    date,

  USUARIO_FINALIZOU_SEP_ID      number(4),
  DATA_HORA_FINALIZOU_SEP       date,

  QTDE_VIAS_IMPRESSAS_COMP_ENT  number(2) default 0 not null,
  QTDE_VIAS_IMPRESSAS_LISTA_SEP number(2) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table ENTREGAS
add constraint PK_ENTREGAS
primary key(ENTREGA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ENTREGAS
add constraint FK_ENTREGAS_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table ENTREGAS
add constraint FK_ENTREGAS_EMPRESA_ENT_ID
foreign key(EMPRESA_ENTREGA_ID)
references EMPRESAS(EMPRESA_ID);

alter table ENTREGAS
add constraint FK_ENTREGAS_EMPRESA_GER_ID
foreign key(EMPRESA_GERACAO_ID)
references EMPRESAS(EMPRESA_ID);

alter table ENTREGAS
add constraint FK_ENTREGAS_EMPRESA_LOCAL_ID
foreign key(LOCAL_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table ENTREGAS
add constraint FK_ENTREGAS_USUARIO_CADAST_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table ENTREGAS
add constraint FK_ENTREGAS_USUARIO_CONFIR_ID
foreign key(USUARIO_CONFIRMACAO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table ENTREGAS
add constraint FK_ENTREGAS_CONTROLE_ENTR_ID
foreign key(CONTROLE_ENTREGA_ID)
references CONTROLES_ENTREGAS(CONTROLE_ENTREGA_ID);

alter table ENTREGAS
add constraint FK_ENTREGAS_USUARIO_IN_SEP_ID
foreign key(USUARIO_INICIO_SEPARACAO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table ENTREGAS
add constraint FK_ENTREGAS_USUARIO_FIN_SEP_ID
foreign key(USUARIO_FINALIZOU_SEP_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table ENTREGAS
add constraint FK_ENTREGAS_NF_TR_EST_FIS_ID
foreign key(NOTA_TRANSF_ESTOQUE_FISCAL_ID)
references NOTAS_FISCAIS(NOTA_FISCAL_ID);

/* Checagens */
/* STATUS
  AGS - Aguardando separa��o
  ESE - Em separa��o
  AGC - Aguardando carregamento
  ERO - Em rota de entrega
  ENT - Entregue
  RPA - Retorno parcial
  RTO - Retorno total
*/
alter table ENTREGAS
add constraint CK_ENTREGAS_STATUS
check(STATUS in('AGS','ESE','AGC','ERO','ENT','RPA','RTO'));

alter table ENTREGAS
add constraint CK_ENTREGAS_STATUS_SEPARACAO
check(
  (STATUS = 'AGS' and USUARIO_INICIO_SEPARACAO_ID is null and USUARIO_FINALIZOU_SEP_ID is null)
  or
  (STATUS = 'ESE' and USUARIO_INICIO_SEPARACAO_ID is not null)
  or
  (STATUS not in('AGS', 'ESE'))
);

alter table ENTREGAS
add constraint CK_ENTREGAS_CONFIRMACOES
check(
  (STATUS not in('ENT', 'RPA') and USUARIO_CONFIRMACAO_ID is null and DATA_HORA_REALIZOU_ENTREGA is null and NOME_PESSOA_RECEBEU_ENTREGA is null and CPF_PESSOA_RECEBEU_ENTREGA is null and OBSERVACOES is null)
  or
  (STATUS in('ENT', 'RPA') and USUARIO_CONFIRMACAO_ID is not null and NOME_PESSOA_RECEBEU_ENTREGA is not null)
);

/* TIPO_NOTA_GERAR
  NI - N�o informado
  NE - NFCe
  NF - NFe
*/
alter table ENTREGAS
add constraint CK_ENTREGAS_TIPO_NF_GERAR
check(TIPO_NOTA_GERAR in('NI', 'NE', 'NF'));

alter table ENTREGAS
add constraint CK_ENT_QTDE_VIAS_IMP_COMP_ENT
check(QTDE_VIAS_IMPRESSAS_COMP_ENT >= 0);

alter table ENTREGAS
add constraint CK_ENT_QTDE_VIAS_IMP_LISTA_SEP
check(QTDE_VIAS_IMPRESSAS_LISTA_SEP >= 0);
