create table CONTROLES_ENTREGAS_AJUDANTES(
  CONTROLE_ENTREGA_ID               number(10) not null,
  AJUDANTE_ID                       number(4) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID                 number(4) not null,
  DATA_HORA_ALTERACAO               date not null,
  ROTINA_ALTERACAO                  varchar2(30) not null,
  ESTACAO_ALTERACAO                 varchar2(30) not null
);

/* Chaves primaria */
alter table CONTROLES_ENTREGAS_AJUDANTES
add constraint PK_CONTROLES_ENTRE_AJUDANTES
primary key(CONTROLE_ENTREGA_ID, AJUDANTE_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTROLES_ENTREGAS_AJUDANTES
add constraint FK_CONT_ENTRE_AJUD_CONT_ENT_ID
foreign key(CONTROLE_ENTREGA_ID)
references CONTROLES_ENTREGAS(CONTROLE_ENTREGA_ID);

alter table CONTROLES_ENTREGAS_AJUDANTES
add constraint FK_CONT_ENTRE_AJUD_AJUDANTE_ID
foreign key(AJUDANTE_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);