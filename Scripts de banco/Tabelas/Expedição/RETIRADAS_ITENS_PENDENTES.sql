create table RETIRADAS_ITENS_PENDENTES(
  EMPRESA_ID               number(3) not null,
  LOCAL_ID                 number(5) not null,
  ORCAMENTO_ID             number(12),
  PRODUTO_ID               number(10) not null,
  ITEM_ID                  number(3) not null,
  LOTE                     varchar2(80) default '???' not null,

  QUANTIDADE               number(20, 4) not null,
  ENTREGUES                number(20, 4) default 0 not null,
  DEVOLVIDOS               number(20, 4) default 0 not null,
  SALDO                    number(20, 4) not null,

  EM_SEPARACAO             number(20, 4) default 0 not null,
  SEPARADOS                number(20, 4) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID        number(4) not null,
  DATA_HORA_ALTERACAO      date not null,
  ROTINA_ALTERACAO         varchar2(30) not null,
  ESTACAO_ALTERACAO        varchar2(30) not null
);

alter table RETIRADAS_ITENS_PENDENTES
add constraint PK_RETIRADAS_ITENS_PENDENTES
primary key(EMPRESA_ID, LOCAL_ID, ORCAMENTO_ID, PRODUTO_ID, ITEM_ID, LOTE)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table RETIRADAS_ITENS_PENDENTES
add constraint FK_RET_ITENS_PEND_ORC_EMP_ID
foreign key(EMPRESA_ID, LOCAL_ID, ORCAMENTO_ID)
references RETIRADAS_PENDENTES(EMPRESA_ID, LOCAL_ID, ORCAMENTO_ID);

alter table RETIRADAS_ITENS_PENDENTES
add constraint FK_RET_ITENS_PEND_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table RETIRADAS_ITENS_PENDENTES
add constraint FK_RET_ITE_PEND_ORC_PRO_ITE_ID
foreign key(ORCAMENTO_ID, PRODUTO_ID, ITEM_ID)
references ORCAMENTOS_ITENS(ORCAMENTO_ID, PRODUTO_ID, ITEM_ID);

/* Constraints */
alter table RETIRADAS_ITENS_PENDENTES
add constraint CK_RETIR_ITENS_PEND_QUANTIDADE
check(QUANTIDADE > 0 and QUANTIDADE >= ENTREGUES + DEVOLVIDOS + SALDO);

alter table RETIRADAS_ITENS_PENDENTES
add constraint CK_RETIRA_ITENS_PEND_ENTREGUES
check(ENTREGUES >= 0);

alter table RETIRADAS_ITENS_PENDENTES
add constraint CK_RETIR_ITENS_PEND_DEVOLVIDOS
check(DEVOLVIDOS >= 0);

alter table RETIRADAS_ITENS_PENDENTES
add constraint CK_RETIRADAS_ITENS_PEND_SALDO
check(SALDO >= 0);
