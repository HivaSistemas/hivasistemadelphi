/* TABELA GRUPOS FINANCEIROS*/
    create table GRUPOS_FINANCEIROS(
      GRUPO_FINANCEIRO_ID  number(3),
      NOME      varchar(30) not null,
    
      /* Colunas de logs do sistema */
      USUARIO_SESSAO_ID       number(4) not null,
      DATA_HORA_ALTERACAO     date not null,
      ROTINA_ALTERACAO        varchar2(30) not null,
      ESTACAO_ALTERACAO       varchar2(30) not null
    );
    
    /* Chave primaria */
    alter table GRUPOS_FINANCEIROS
    add constraint PK_GRUPOS_FINANCEIROS
    primary key(GRUPO_FINANCEIRO_ID)
    using index tablespace INDICES;'
    
    
    '/* Uniques */
    alter table GRUPOS_FINANCEIROS
    add constraint UN_GRUPOS_FINANCEIROS_NOME
    unique(NOME);    