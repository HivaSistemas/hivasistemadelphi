create table PARAMETROS_ESTACOES(
  ESTACAO               varchar2(30) not null,
  UTILIZAR_TEF          char(1) default 'N' not null,
  GERENCIADOR_CARTAO_ID number(2),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table PARAMETROS_ESTACOES
add constraint PK_PARAMETROS_ESTACOES
primary key(ESTACAO)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PARAMETROS_ESTACOES
add constraint FK_PAR_EST_GER_CARTAO_ID
foreign key(GERENCIADOR_CARTAO_ID)
references GERENCIADORES_CARTOES_TEF(GERENCIADOR_CARTAO_ID);

/* Checagens */
alter table PARAMETROS_ESTACOES
add constraint CK_PAR_EST_UTILIZAR_TEF
check(
  UTILIZAR_TEF = 'N' and GERENCIADOR_CARTAO_ID is null
  or
  UTILIZAR_TEF = 'S' and GERENCIADOR_CARTAO_ID is not null
);