﻿create table DIVERSOS_ENDERECOS(
  CADASTRO_ID            number(10),
  ORDEM                  number(3),
  TIPO_ENDERECO          char(1) not null,
  LOGRADOURO             varchar2(100) not null,
  COMPLEMENTO            varchar2(100) not null,
  NUMERO                 varchar2(10) default 'SN' not null,
  BAIRRO_ID              number(15),
  PONTO_REFERENCIA       varchar2(200),
  CEP                    varchar2(9),
  INSCRICAO_ESTADUAL     varchar2(20)
);

alter table DIVERSOS_ENDERECOS
add constraint PK_DIVERSOS_ENDERECOS_ID
primary key(CADASTRO_ID, ORDEM)
using index tablespace INDICES;

alter table DIVERSOS_ENDERECOS
add constraint FK_DIVERSOS_END_BAIRRO_ID
foreign key(BAIRRO_ID)
references BAIRROS(BAIRRO_ID);

/* TIPO_ENDERECO
  C - Cobrança
  E - Entrega
*/

alter table DIVERSOS_ENDERECOS
add constraint CK_DIVER_END_TIPO_END_INSC_EST
check(
  (TIPO_ENDERECO = 'C' and INSCRICAO_ESTADUAL is null) or
  (TIPO_ENDERECO = 'E' and INSCRICAO_ESTADUAL is not null)
);
