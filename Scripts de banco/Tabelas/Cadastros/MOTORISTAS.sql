﻿create table MOTORISTAS(
  CADASTRO_ID         number(10) not null,
  NUMERO_CNH          varchar2(15) not null,
  DATA_VALIDADE_CNH   date not null,
  DATA_HORA_CADASTRO  date not null,
  ATIVO               char(1) default 'S' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table MOTORISTAS
add constraint PK_MOTORISTAS_CADASTRO_ID
primary key(CADASTRO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table MOTORISTAS
add constraint FK_MOTORISTAS_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);