﻿create table LOCAIS_PRODUTOS(
  LOCAL_ID                number(5),
  NOME                    varchar2(60) not null,
  TIPO_LOCAL              char(1) default 'L' not null,
  PERMITE_DEVOLUCAO_VENDA char(1) default 'S' not null,
  ATIVO                   char(1) default 'S' not null,
  EMPRESA_DIRECIONAR_NOTA number(3)
);

alter table LOCAIS_PRODUTOS
add constraint PK_LOCAIS_PROD_LOCAL_ID
primary key(LOCAL_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table LOCAIS_PRODUTOS
add constraint FK_LOCAIS_PROD_EMP_DIREC_NOTA
foreign key(EMPRESA_DIRECIONAR_NOTA)
references EMPRESAS(EMPRESA_ID);


/* TIPO 
  L - Loja
  D - Depósito
  E - Exposição
*/
alter table LOCAIS_PRODUTOS
add constraint CK_LOCAIS_PROD_TIPO_LOCAL
check(TIPO_LOCAL in('L', 'D', 'E'));


alter table LOCAIS_PRODUTOS
add constraint CK_LOCAIS_PRODUTOS_ATIVO
check(ATIVO in('S', 'N'));


/* COMMANDOS RODAR

alter table LOCAIS_PRODUTOS add (EMPRESA_DIRECIONAR_NOTA number(3));

alter table LOCAIS_PRODUTOS
add constraint FK_LOCAIS_PROD_EMP_DIREC_NOTA
foreign key(EMPRESA_DIRECIONAR_NOTA)
references EMPRESAS(EMPRESA_ID);

*/
