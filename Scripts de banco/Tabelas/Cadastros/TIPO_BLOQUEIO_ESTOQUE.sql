create table TIPO_BLOQUEIO_ESTOQUE(
  TIPO_BLOQUEIO_ID   number(3),
  DESCRICAO          varchar2(200) not null,
  ATIVO              char(1)
);

/* Chave prim�ria */
alter table TIPO_BLOQUEIO_ESTOQUE
add constraint PK_TIP_BLOQUEIO_ID
primary key(TIPO_BLOQUEIO_ID)
using index tablespace INDICES;