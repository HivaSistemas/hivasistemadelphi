create table ORDENS_LOCAIS_ENTREGAS(
  EMPRESA_ID         number(3) not null,
  TIPO               char(1) not null,
  LOCAL_ID           number(5) not null,
  ORDEM              number(5) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table ORDENS_LOCAIS_ENTREGAS
add constraint PK_ORDENS_LOCAIS_ENTREGAS
primary key(EMPRESA_ID, TIPO, LOCAL_ID, ORDEM)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ORDENS_LOCAIS_ENTREGAS
add constraint FK_ORD_LOC_ENT_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table ORDENS_LOCAIS_ENTREGAS
add constraint FK_ORD_LOC_ENT_LOCAL_ID
foreign key(LOCAL_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

/* Checagens */
alter table ORDENS_LOCAIS_ENTREGAS
add constraint FK_ORD_LOC_ENTREGAS_TIPO
check(TIPO in('A', 'R', 'E'));

/* Checagens */
alter table ORDENS_LOCAIS_ENTREGAS
add constraint FK_ORD_LOC_ENTREGAS_ORDEM
check(ORDEM >= 0);