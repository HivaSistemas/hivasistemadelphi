create table GRUPOS_TRIB_ESTADUAL_VENDA_EST(
  GRUPO_TRIB_ESTADUAL_VENDA_ID number(10) not null,  
	ESTADO_ID                    char(2) not null,	
  
	CST_NAO_CONTRIBUINTE      varchar2(3) not null,
	CST_CONTRIBUINTE          varchar2(3) not null,
  CST_ORGAO_PUBLICO         varchar2(3) not null,
  CST_REVENDA               varchar2(3) not null,
  CST_CONSTRUTORA           varchar2(3) not null,
  CST_CLINICA_HOSPITAL      varchar2(3) not null,
  CST_PRODUTOR_RURAL        varchar2(3) not null,

  INDICE_REDUCAO_BASE_ICMS  number(5,4) default 1 not null,
  PERCENTUAL_ICMS           number(4,2) not null,
  IVA                       number(4,2) default 0 not null,
  PRECO_PAUTA               number(8,2) default 0 not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID             number(4) not null,
  DATA_HORA_ALTERACAO           date not null,
  ROTINA_ALTERACAO              varchar2(30) not null,
  ESTACAO_ALTERACAO             varchar2(30) not null    
);

/* Chave primária */
alter table GRUPOS_TRIB_ESTADUAL_VENDA_EST
add constraint PK_GRUPOS_TRIB_EST_VENDA_EST
primary key(GRUPO_TRIB_ESTADUAL_VENDA_ID, ESTADO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table GRUPOS_TRIB_ESTADUAL_VENDA_EST
add constraint FK_GRU_TR_EST_VEN_EST_GRUPO_ID
foreign key(GRUPO_TRIB_ESTADUAL_VENDA_ID)
references GRUPOS_TRIB_ESTADUAL_VENDA(GRUPO_TRIB_ESTADUAL_VENDA_ID);

alter table GRUPOS_TRIB_ESTADUAL_VENDA_EST
add constraint FK_GRU_TR_EST_VEN_EST_EST_ID
foreign key(ESTADO_ID)
references ESTADOS(ESTADO_ID);

/* Checagens */
alter table GRUPOS_TRIB_ESTADUAL_VENDA_EST
add constraint GRUPOS_TRIB_EST_VEN_EST_CST_NC
check(CST_NAO_CONTRIBUINTE in('00','10','20','30','40','41','50','51','60','70','90'));

alter table GRUPOS_TRIB_ESTADUAL_VENDA_EST
add constraint GRUPOS_TRIB_EST_VEN_EST_CST_CO
check(CST_CONTRIBUINTE in('00','10','20','30','40','41','50','51','60','70','90'));

alter table GRUPOS_TRIB_ESTADUAL_VENDA_EST
add constraint GRUPOS_TRIB_EST_VEN_EST_CST_OP
check(CST_ORGAO_PUBLICO in('00','10','20','30','40','41','50','51','60','70','90'));

alter table GRUPOS_TRIB_ESTADUAL_VENDA_EST
add constraint GRUPOS_TRIB_EST_VEN_EST_CST_RE
check(CST_REVENDA in('00','10','20','30','40','41','50','51','60','70','90'));

alter table GRUPOS_TRIB_ESTADUAL_VENDA_EST
add constraint GRUPOS_TRIB_EST_VEN_EST_CST_CT
check(CST_CONSTRUTORA in('00','10','20','30','40','41','50','51','60','70','90'));

alter table GRUPOS_TRIB_ESTADUAL_VENDA_EST
add constraint GRUPOS_TRIB_EST_VEN_EST_CST_CH
check(CST_CLINICA_HOSPITAL in('00','10','20','30','40','41','50','51','60','70','90'));

alter table GRUPOS_TRIB_ESTADUAL_VENDA_EST
add constraint GRUPOS_TRIB_EST_VEN_EST_CST_PR
check(CST_PRODUTOR_RURAL in('00','10','20','30','40','41','50','51','60','70','90'));

alter table GRUPOS_TRIB_ESTADUAL_VENDA_EST
add constraint GRUPOS_TRIB_EST_VEN_EST_IRBCI
check(INDICE_REDUCAO_BASE_ICMS between 0.0001 and 1);
