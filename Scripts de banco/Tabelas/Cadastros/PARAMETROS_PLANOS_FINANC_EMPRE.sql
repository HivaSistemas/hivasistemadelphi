create table PARAMETROS_PLANOS_FINANC_EMPRE(
  EMPRESA_ID                      number(3) not null,
  
  PLANO_FIN_AJUSTE_EST_NORMAL_ID  varchar2(9) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Primary key */
alter table PARAMETROS_PLANOS_FINANC_EMPRE
add constraint PK_PARAMETROS_PLANOS_FINAN_EMP
primary key(EMPRESA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PARAMETROS_PLANOS_FINANC_EMPRE
add constraint FK_PAR_PL_FIN_EM_PL_F_A_EST_NO
foreign key(PLANO_FIN_AJUSTE_EST_NORMAL_ID)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);