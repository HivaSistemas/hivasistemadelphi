create table PRODUTOS_ORDENS_LOC_ENTREGAS(
  EMPRESA_ID         number(3) not null,
  PRODUTO_ID         number(10) not null,
  TIPO               char(1) not null,
  LOCAL_ID           number(5) not null,
  ORDEM              number(5) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table PRODUTOS_ORDENS_LOC_ENTREGAS
add constraint PK_PRODUTOS_ORDENS_LOC_ENTR
primary key(EMPRESA_ID, PRODUTO_ID, TIPO, LOCAL_ID, ORDEM)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PRODUTOS_ORDENS_LOC_ENTREGAS
add constraint FK_PROD_ORD_LOC_ENT_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table PRODUTOS_ORDENS_LOC_ENTREGAS
add constraint FK_PROD_ORD_LOC_ENT_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table PRODUTOS_ORDENS_LOC_ENTREGAS
add constraint FK_PROD_ORD_LOC_ENT_LOCAL_ID
foreign key(LOCAL_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

/* Checagens */
/* A - Retirar Ato 
   R - Retirar 
   E - Entregar*/
alter table PRODUTOS_ORDENS_LOC_ENTREGAS
add constraint FK_PROD_ORD_LOC_ENTREGAS_TIPO
check(TIPO in('A', 'R', 'E'));



/* Checagens */
alter table PRODUTOS_ORDENS_LOC_ENTREGAS
add constraint FK_PROD_ORD_LOC_ENTREGAS_ORDEM
check(ORDEM >= 0);