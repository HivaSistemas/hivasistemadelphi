create table CRM_BANCO_DADOS(
  CENTRAL_ID    number(10),
  ITEM_ID       number(10),
  USUARIO       varchar2(100),
  SENHA         varchar2(100),
  IP_SERVIDOR   varchar2(100),
  PORTA         varchar2(20),
  SERVICO       varchar2(50)
);

alter table CRM_BANCO_DADOS
add constraint FK_CRM_BAN_DAD_CENTRAL_ID
foreign key(CENTRAL_ID)
references CRM(CENTRAL_ID);