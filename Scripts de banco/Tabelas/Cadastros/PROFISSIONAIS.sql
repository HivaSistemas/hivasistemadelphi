create table PROFISSIONAIS(  
  CADASTRO_ID                 number(10,0),  
  ATIVO                       char(1) default 'S'   not null,    
  CARGO_ID                    number(6,0) not null,  
  NUMERO_CREA                 varchar2(30),  
  PERCENTUAL_COMISSAO         number(5,2) not null,
  PERCENTUAL_COMISSAO_PRAZO   number(5,2) not null,
  PONTOS                      number(10,0) default 0 not null,  
  PROFISSIONAL_INDICOU_ID     number(10,0),  
  REGRA_GERAL_COMISSAO_GRUPOS char(1) default 'S'   not null,  
  TIPO_COMISSAO               char(3) default 'GGE'   not null,  
  UTILIZAR_PRECO_ESPECIAL     char(1) default 'N'   not null,  
  VIP                         char(1) default 'N'   not null,  
  INFORMACOES                 varchar2(400),  
     
  /* COLUNAS DE LOGS DO SISTEMA */  
  USUARIO_SESSAO_ID       number(4) not null,  
  DATA_HORA_ALTERACAO     date not null,  
  ROTINA_ALTERACAO        varchar2(30) not null,  
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table PROFISSIONAIS
add constraint PK_PROFISSIONAIS
primary key(CADASTRO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PROFISSIONAIS
add constraint FK_PROFISSIONAIS_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);

alter table PROFISSIONAIS
add constraint FK_PROFISSIONAIS_CARGO_ID
foreign key(CARGO_ID)
references PROFISSIONAIS_CARGOS(CARGO_ID);

alter table PROFISSIONAIS
add constraint FK_PROFISSIONAIS_INDIC_CAD_ID
foreign key(PROFISSIONAL_INDICOU_ID)
references CADASTROS(CADASTRO_ID);