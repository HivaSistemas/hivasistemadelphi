﻿create table SERIES_NOTAS_FISCAIS(
  SERIE                   varchar2(5) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table SERIES_NOTAS_FISCAIS
add constraint PK_SERIES_NOTAS_FISCAIS
primary key(SERIE)
using index tablespace INDICES;
