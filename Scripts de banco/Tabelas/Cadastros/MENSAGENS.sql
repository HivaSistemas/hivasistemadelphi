create table MENSAGENS(
  MENSAGEM_ID                 number(12),
  AGRUPADOR_MENSAGEM_ID       number(12) not null,
  ASSUNTO                     varchar2(60) default 'Sem assunto' not null,
  MENSAGEM                    varchar2(2000) not null,
  USUARIO_ENVIO_MENSAGEM_ID   number(10) not null,
  USUARIO_DESTINO_MENSAGEM_ID number(10) not null,
  PERMITIR_RESPOSTA           char(1) default 'S' not null,
  RESPONDIDA                  char(1) default 'N' not null,
  GRUPO_MENSAGEM              char(3) not null,
  DATA_HORA_ENVIO             date default sysdate not null,
  LIDA                        char(1) default 'N' not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table MENSAGENS
add constraint PK_MENSAGENS
primary key(MENSAGEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table MENSAGENS
add constraint FK_MENSAGENS_USU_ENV_MENS_ID
foreign key(USUARIO_ENVIO_MENSAGEM_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table MENSAGENS
add constraint FK_MENSAGENS_USU_DEST_MENS_ID
foreign key(USUARIO_DESTINO_MENSAGEM_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* Checagens */
alter table MENSAGENS
add constraint CK_MENSAGENS_PERMITIR_RESPOSTA
check(PERMITIR_RESPOSTA in('S', 'N'));

alter table MENSAGENS
add constraint CK_MENSAGENS_RESPONDIDA
check(RESPONDIDA in('S', 'N'));

alter table MENSAGENS
add constraint CK_MENSAGENS_LIDA
check(LIDA in('S', 'N'));

/* GRUPO_MENSAGEM 
  SIS - Mensagens do sistema
  IMP - Importantes
  ENV - Mensagens enviadas
  ENT - Mensagens recebidas
*/
alter table MENSAGENS
add constraint CK_MENSAGENS_GRUPO_MENSAGEM
check(GRUPO_MENSAGEM in('SIS', 'IMP', 'ENV', 'ENT'));
