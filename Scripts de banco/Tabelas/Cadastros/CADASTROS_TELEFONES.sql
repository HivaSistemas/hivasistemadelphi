create table CADASTROS_TELEFONES(
  CADASTRO_ID   number(10) not null,
  ORDEM         number(2) default 0 not null,
  TIPO_TELEFONE char(1) not null,
  RAMAL         number(4,0),
  TELEFONE      varchar2(30),
  OBSERVACAO    varchar2(100),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave prim�ria */
alter table CADASTROS_TELEFONES
add constraint PK_CADASTROS_TELEFONES
primary key(CADASTRO_ID, ORDEM)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CADASTROS_TELEFONES
add constraint FK_CADASTROS_TELEF_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);


/* TIPO_TELEFONE
  R - Residencial
  C - Comercial
  F - Fax
  E - Celular
  A - Recado
  P - Pai/Mae
  G - Gratuito
*/
alter table CADASTROS_TELEFONES
add constraint CADASTROS_TELEF_TIPO_TELEFONE
check(TIPO_TELEFONE in('R', 'C', 'F', 'E', 'A', 'P', 'G', null));
