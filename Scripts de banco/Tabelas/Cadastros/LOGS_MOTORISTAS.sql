create table LOGS_MOTORISTAS(
  TIPO_ALTERACAO_LOG_ID   number(4) not null,
  CADASTRO_ID             number(10) not null,
  VALOR_ANTERIOR          varchar2(300),
  NOVO_VALOR              varchar2(300),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chaves estrangeiras */
alter table LOGS_MOTORISTAS
add constraint FK_LOGS_MOTORISTAS_CADASTRO_ID
foreign key(CADASTRO_ID)
references MOTORISTAS(CADASTRO_ID);