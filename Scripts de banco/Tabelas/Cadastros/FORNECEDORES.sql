﻿create table FORNECEDORES(
  CADASTRO_ID       number(10),
  SUPER_SIMPLES     char(1) default 'N' not null,
  DATA_CADASTRO     date default trunc(sysdate) not null,
  TIPO_FORNECEDOR   char(1) default 'R' not null,
  OBSERVACOES       varchar2(1000),
  ATIVO             char(1) default 'S' not null,
  TIPO_RATEIO_FRETE char(1) default 'M' not null,

  REVENDA                        char(1) default 'S' not null,
  USO_CONSUMO                    char(1) default 'N' not null,
  SERVICO                        char(1) default 'N' not null,

  PLANO_FINANCEIRO_REVENDA       varchar2(9),
  PLANO_FINANCEIRO_USO_CONSUMO   varchar2(9),
  PLANO_FINANCEIRO_SERVICO       varchar2(9),

  GRUPO_FORNECEDOR_ID            number(4),
  
  PORTADOR_ID                    number(4),
  DIAS_ESTOQUE_MINIMO            number(3,0) default 0 not null,
  PRAZO_ENTREGA                  number(3,0) default 0 not null,
  TIPO_FRETE                     char(2) default 'NE' not null,
  PERCENTUAL_FRETE_COMPRA        number(4,2) default 0 not null,
  VALOR_FRETE_QUANTIDADE_COMPRA  number(10,2) default 0 not null,
  VALOR_FRETE_TONELADA_COMPRA    number(10,2) default 0 not null,
  NUNCA_ALTERAR_CUSTO_ENTRADA    char(1) default 'N' not null,
  GERA_DESPESA_MENSAL            char(1) default 'N' not null,
  TIPO_DESPESA_MENSAL            varchar2(100),


  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table FORNECEDORES
add constraint PK_FORNECEDORES_CADASTRO_ID
primary key(CADASTRO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table FORNECEDORES
add constraint FK_FORNECEDORES_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);

alter table FORNECEDORES
add constraint FK_FORNECEDORES_GRUPO_FORN_ID
foreign key(GRUPO_FORNECEDOR_ID)
references GRUPOS_FORNECEDORES(GRUPO_ID);

alter table FORNECEDORES
add constraint FK_FORNEC_PLAN_FINANC_REVENDA
foreign key(PLANO_FINANCEIRO_REVENDA)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

alter table FORNECEDORES
add constraint FK_FORNEC_PLAN_FIN_USO_CONSUMO
foreign key(PLANO_FINANCEIRO_USO_CONSUMO)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

alter table FORNECEDORES
add constraint FK_FORNEC_PLAN_FINANC_SERVICO
foreign key(PLANO_FINANCEIRO_SERVICO)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

/* Constraints */
alter table FORNECEDORES
add constraint CK_FORNECEDORES_SUPER_SIMPLES
check (SUPER_SIMPLES in('S','N'));

/* TIPO_FORNECEDOR
  '' - Vazio caso seja CPF 
  R - REVENDA
  D - DISTRIBUIDORA
  I - INDUSTRIA
  O - OUTRO
*/
alter table FORNECEDORES
add constraint CK_FORNEC_TIPO_FORNECEDOR
check (TIPO_FORNECEDOR in('','R','D','I','O'));

alter table FORNECEDORES
add constraint CK_FORNECEDORES_ATIVO
check (ATIVO in('S','N'));

alter table FORNECEDORES
add constraint CK_FORNECEDORES_REVENDA
check(
  (REVENDA = 'N' and PLANO_FINANCEIRO_REVENDA is null)
  or
  REVENDA in('S','N')
);

alter table FORNECEDORES
add constraint CK_FORNECEDORES_USO_CONSUMO
check(
  (USO_CONSUMO = 'N' and PLANO_FINANCEIRO_USO_CONSUMO is null)
  or
  (USO_CONSUMO in('S','N'))
);

alter table FORNECEDORES
add constraint CK_FORNECEDORES_SERVICO
check (
  (SERVICO = 'N' and PLANO_FINANCEIRO_SERVICO is null)
  or
  SERVICO in('S', 'N')
);

/* TIPO_RATEIO_FRETE
  V - Por valor
  P - Por peso
  Q - Quantidade
  M - Manual
*/
alter table FORNECEDORES
add constraint CK_FORNECEDORES_TP_RAT_FRETE
check(TIPO_RATEIO_FRETE in('V','P','Q','M'));

alter table FORNECEDORES
add constraint CK_FORNECEDORES_TIPO_FRETE
check (TIPO_FRETE in('NE','CI','FN','FC','FR'));

 /* N - NE - NENHUM
    C - CI - CIF
    O - FN - FOB NOTA
    C - FC - FOB CONHECIMENTO
   FR - FOB FRETEIRO */

alter table FORNECEDORES
add constraint CK_FORNECEDORES_NUN_ALT_CT_ENT
check (NUNCA_ALTERAR_CUSTO_ENTRADA in('S','N'));

alter table FORNECEDORES
add constraint CK_FORNECEDORES_GERA_DESP_MENS
check (GERA_DESPESA_MENSAL in('S','N'));
