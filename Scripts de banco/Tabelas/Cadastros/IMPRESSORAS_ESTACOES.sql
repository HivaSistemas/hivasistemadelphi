create table IMPRESSORAS_ESTACOES(
  ESTACAO                         varchar2(30) not null,
  
  TIPO_IMPRESSORA_NFE             char(1) default 'G' not null,
  IMPRESSORA_NFE                  varchar2(100),
  ABRIR_PREVIEW_NFE               char(1) default 'N' not null,
    
  TIPO_IMPRESSORA_NFCE            char(1) default 'N' not null,
  IMPRESSORA_NFCE                 varchar2(100),
  ABRIR_PREVIEW_NFCE              char(1) default 'N' not null,
  
  TIPO_IMPRESSORA_COMP_PAGAMENTO  char(1) default 'N' not null,
  IMPRESSORA_COMP_PAGAMENTO       varchar2(100),
  ABRIR_PREVIEW_COMP_PAGAMENTO    char(1) default 'N' not null,
  
  TIPO_IMPRESSORA_COMPRO_ENTREGA  char(1) default 'N' not null,
  IMPRESSORA_COMPRO_ENTREGA       varchar2(100),
  ABRIR_PREVIEW_COMPRO_ENTREGA    char(1) default 'N' not null,
  
  TIPO_IMPRESSORA_LISTA_SEPARAC   char(1) default 'N' not null,
  IMPRESSORA_LISTA_SEPARAC        varchar2(100),
  ABRIR_PREVIEW_LISTA_SEPARACAO   char(1) default 'N' not null,
  
  TIPO_IMPRESSORA_ORCAMENTO       char(1) default 'G' not null,
  IMPRESSORA_ORCAMENTO            varchar2(100),  
  ABRIR_PREVIEW_ORCAMENTO         char(1) default 'N' not null,

  TIPO_IMP_COMP_PAGTO_FINANCEIRO  char(1) default 'G' not null,
  IMP_COMP_PAGTO_FINANCEIRO       varchar2(100),
  ABRIR_PREV_COMP_PAGTO_FINANC    char(1) default 'N' not null,

  TIPO_IMPRESSORA_COMP_DEVOLUCAO  char(1) default 'G' not null,
  IMPRESSORA_COMP_DEVOLUCAO       varchar2(100),
  ABRIR_PREV_COMP_DEVOLUCAO       char(1) default 'N' not null,

  TIPO_IMPRESSORA_CTZ_PROMO       CHAR(1) DEFAULT 'G' NOT NULL,
  IMPRESSORA_CTZ_PROMO            VARCHAR2(100),
  ABRIR_PREV_CTZ_PROMO            CHAR(1) DEFAULT 'N' NOT NULL,
  
  TIPO_IMPRESSORA_COMP_CAIXA      char(1) default 'G' not null,
  IMPRESSORA_COMP_CAIXA           varchar2(100),
  ABRIR_PREV_COMP_CAIXA           char(1) default 'N' not null,
  MODELO_BALANCA        	  varchar2(20),
  PORTA_BALANCA       		  varchar2(5),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Primary key */
alter table IMPRESSORAS_ESTACOES
add constraint PK_IMPRESSORAS_ESTACOES
primary key(ESTACAO)
using index tablespace INDICES;

/* Checagens */

/* TIPO_IMPRESSORA
  N - N�o fiscal
  G - Gr�fica
*/
alter table IMPRESSORAS_ESTACOES
add constraint CK_IMPRESS_EST_TIPO_IMPRE_NFE
check(TIPO_IMPRESSORA_NFE = 'G');

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMPRESS_EST_TIPO_IMPRE_NFCE
check(TIPO_IMPRESSORA_NFCE in('N','G'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMPR_EST_TIPO_IMPR_COMP_PAG
check(TIPO_IMPRESSORA_COMP_PAGAMENTO in('N','G'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMPR_EST_TIPO_IMPR_COMP_ENT
check(TIPO_IMPRESSORA_COMPRO_ENTREGA in('N','G'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMPR_EST_TIPO_IMPR_LIST_SEP
check(TIPO_IMPRESSORA_LISTA_SEPARAC in('N','G'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMPR_EST_TIPO_IMPR_ORCAMENT
check(TIPO_IMPRESSORA_ORCAMENTO in('N','G'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMP_EST_ABRIR_PREVIEW_NFE
check(ABRIR_PREVIEW_NFE in ('N', 'S'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMP_EST_ABRIR_PREVIEW_NFCE
check(ABRIR_PREVIEW_NFCE in ('N', 'S'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMP_EST_ABRIR_PR_COMP_PAGTO
check(ABRIR_PREVIEW_COMP_PAGAMENTO in ('N', 'S'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMP_EST_ABRIR_PREV_COMP_ENT
check(ABRIR_PREVIEW_COMPRO_ENTREGA in ('N', 'S'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMP_EST_ABRIR_PRE_LISTA_SEP
check(ABRIR_PREVIEW_LISTA_SEPARACAO in ('N', 'S'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMP_EST_ABRIR_PREVIEW_ORC
check(ABRIR_PREVIEW_ORCAMENTO in ('N', 'S'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMP_EST_TP_IMP_C_PG_FINANC
check(TIPO_IMP_COMP_PAGTO_FINANCEIRO in('N','G'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMP_EST_ABRIR_PREV_C_PG_F
check(ABRIR_PREV_COMP_PAGTO_FINANC in ('N', 'S'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMP_EST_ABR_PREV_COMP_DEV
check(ABRIR_PREV_COMP_DEVOLUCAO in ('N', 'S'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMP_EST_TIPO_IMP_COMP_DEV
check(TIPO_IMPRESSORA_COMP_DEVOLUCAO in ('N', 'G'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMP_EST_ABR_PREV_COMP_CAIXA
check(ABRIR_PREV_COMP_CAIXA in ('N', 'S'));

alter table IMPRESSORAS_ESTACOES
add constraint CK_IMP_EST_TIPO_IMP_COMP_CAIXA
check(TIPO_IMPRESSORA_COMP_CAIXA in ('N', 'G'));

/*

ALTER TABLE IMPRESSORAS_ESTACOES ADD(
  TIPO_IMPRESSORA_CTZ_PROMO CHAR(1) DEFAULT 'G' NOT NULL,
  IMPRESSORA_CTZ_PROMO VARCHAR2(100),
  ABRIR_PREV_CTZ_PROMO CHAR(1) DEFAULT 'N' NOT NULL
)

*/