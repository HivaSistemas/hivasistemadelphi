﻿create table TIPOS_COBRANCA(
  COBRANCA_ID             	    number(3),
  PORTADOR_ID             	    varchar2(50) not null,
  NOME                    	    varchar2(60) not null,
  TAXA_RETENCAO_MES       	    number(4,2) default 0 not null,
  FORMA_PAGAMENTO         	    char(3) not null,
  REDE_CARTAO             	    char(1),
  TIPO_CARTAO             	    char(1),
  TIPO_RECEBIMENTO_CARTAO 	    char(1),
  TIPO_PAGAMENTO_COMISSAO 	    char(1) default 'R' not null,
  PERCENTUAL_COMISSAO           number(4,2) default 0 not null,
  UTILIZAR_LIMITE_CRED_CLIENTE  char(1) default 'N' not null,
  BOLETO_BANCARIO               char(1) default 'N' not null,
  INCIDENCIA_FINANCEIRA         char(1) default 'A' not null,
  PERMITIR_PRAZO_MEDIO          char(1) default 'S' not null,
  ATIVO                   	    char(1) not null,
  PRAZO_MEDIO                   number(5,2) default 0 not null,
  EMITIR_DUPLIC_MERCANTIL_VENDA char(1) default 'N' not null,

  PERMITIR_VINCULO_AUT_TEF      char(1) default 'S' not null,
  REDE_RETORNO_TEF              varchar2(50),
  BADEIRA_RETORNO_TEF           varchar2(50),
  ADMINISTRADORA_CARTAO_ID      number(3),
  BANDEIRA_CARTAO_NFE           char(2),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);
/* Primary key */
alter table TIPOS_COBRANCA
add constraint PK_COBRANCA_ID
primary key(COBRANCA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table TIPOS_COBRANCA
add constraint FK_TP_COBRANCA_PORTADOR_ID
foreign key(PORTADOR_ID)
references PORTADORES(PORTADOR_ID);

alter table TIPOS_COBRANCA
add constraint FK_TP_COB_ADMIN_CARTAO_ID
foreign key(ADMINISTRADORA_CARTAO_ID)
references ADMINISTRADORAS_CARTOES(ADMINISTRADORA_ID);

/* Constraints */
alter table TIPOS_COBRANCA
add constraint CK_TP_COBRANCA_TAXA_RET_MES
check(TAXA_RETENCAO_MES between 0 and 99.99);

/* FORMA_PAGAMENTO
  DIN - Dinheiro
  CHQ - Cheque
  CRT - Cartão
  COB - Cobrança
  ACU - Acumulado
  FIN - Financeira
*/
alter table TIPOS_COBRANCA
add constraint CK_TIPO_COBRANCA_FORMA_PAGTO
check(FORMA_PAGAMENTO in('DIN','CHQ','CRT','COB','ACU','FIN'));

/* TIPO_RECEBIMENTO_CARTAO 
  T - TEF
  P - POS
  A - AMBOS
*/
alter table TIPOS_COBRANCA
add constraint CK_TIPO_COB_TP_RECEBIMENTO_CRT
check(TIPO_RECEBIMENTO_CARTAO in(null, 'T', 'P','A'));

alter table TIPOS_COBRANCA
add constraint CK_TP_COBRANCA_FORMA_PAGTO_CRT
check(
  FORMA_PAGAMENTO <> 'CRT' and TIPO_CARTAO is null and REDE_CARTAO is null and TIPO_RECEBIMENTO_CARTAO is null and BANDEIRA_CARTAO_NFE is null
  or
  FORMA_PAGAMENTO = 'CRT' and TIPO_CARTAO is not null and REDE_CARTAO is not null and TIPO_RECEBIMENTO_CARTAO is not null and BANDEIRA_CARTAO_NFE is not null
);

/* REDE_CARTAO
  C - Cielo
  V - Visa
  R - RedeCard
  E - ELO
  null
*/
alter table TIPOS_COBRANCA
add constraint CK_TP_COBRANCA_REDE
check(REDE_CARTAO in('C', 'V', 'R','E', null));

/* TIPO_CARTAO
  C - Crédito
  D - Débito
  null
*/
alter table TIPOS_COBRANCA
add constraint CK_TP_CRT_COBRANCA_TIPO_CARTAO
check(TIPO_CARTAO in('C', 'D', null));

/* TIPO_PAGAMENTO_COMISSAO
  R - No recebimento do pedido
  B - Na baixa do financeiro
*/
alter table TIPOS_COBRANCA
add constraint CK_TP_COB_TIPO_PAGTO_COMISSAO
check(TIPO_PAGAMENTO_COMISSAO in('R','B'));

alter table TIPOS_COBRANCA
add constraint CK_TIPOS_COBR_PERC_COMISSAO
check(PERCENTUAL_COMISSAO between 0 and 99.99);

alter table TIPOS_COBRANCA
add constraint CK_TP_COBR_UTIL_LIM_CRED_CLI
check(UTILIZAR_LIMITE_CRED_CLIENTE in('S', 'N'));

alter table TIPOS_COBRANCA
add constraint CK_TP_COBR_BOLETO_BANCARIO
check(BOLETO_BANCARIO in('S', 'N'));

/* INCIDENCIA_FINANCEIRA
  A - Ambos
  R - Contas a receber
  P - Contas a pagar
*/

alter table TIPOS_COBRANCA
add constraint CK_TP_COBR_INCIDENCIA_FINANC
check(INCIDENCIA_FINANCEIRA in('R', 'P', 'A'));

alter table TIPOS_COBRANCA
add constraint CK_TP_COBR_PERM_PRAZO_MEDIO
check(PERMITIR_PRAZO_MEDIO in('S', 'N'));

alter table TIPOS_COBRANCA
add constraint CK_TP_COBRANCA_ATIVO
check(ATIVO in('S', 'N'));

alter table TIPOS_COBRANCA
add constraint CK_TP_COBR_PER_VINC_AUT_TEF
check(PERMITIR_VINCULO_AUT_TEF in('S', 'N'));

alter table TIPOS_COBRANCA
add constraint CK_TIPOS_COB_PRAZO_MEDIO
check(PRAZO_MEDIO >= 0);

alter table TIPOS_COBRANCA
add constraint CK_TP_COB_EMITIR_DUP_MERC_VEN
check(EMITIR_DUPLIC_MERCANTIL_VENDA in('S', 'N'));
