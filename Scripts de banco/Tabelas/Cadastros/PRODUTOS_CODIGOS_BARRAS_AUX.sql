create table PRODUTOS_CODIGOS_BARRAS_AUX(
  PRODUTO_ID              number(10) not null,
  CODIGO_BARRAS           varchar2(14) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table PRODUTOS_CODIGOS_BARRAS_AUX
add constraint PK_PRODUTOS_CODIGOS_BARRAS_AUX
primary key(PRODUTO_ID, CODIGO_BARRAS)
using index tablespace INDICES;