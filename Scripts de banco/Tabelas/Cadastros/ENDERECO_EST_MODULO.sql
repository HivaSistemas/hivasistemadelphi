create table ENDERECO_EST_MODULO(
  MODULO_ID               number(3),
  DESCRICAO               varchar2(30) not null
);

/* Chave prim�ria */
alter table ENDERECO_EST_MODULO
add constraint PK_END_EST_MOD_MOD_ID
primary key(MODULO_ID)
using index tablespace INDICES;
