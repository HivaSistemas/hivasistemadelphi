create table CONTAS(
  CONTA                        varchar2(20) not null,
  DIGITO_CONTA                 varchar2(3),
  CODIGO_BANCO                 char(4),
  AGENCIA                      varchar2(5),
  DIGITO_AGENCIA               varchar2(3),
  
  EMPRESA_ID                   number(3) not null,
  NOME                         varchar2(60) not null,
  TIPO                         number(1) not null,  
  ACEITA_SALDO_NEGATIVO        char(1) default 'N' not null,
  EMITE_CHEQUE                 char(1) default 'N' not null,
  NUMERO_ULTIMO_CHEQUE_EMITIDO number(4) default 0 not null,

  EMITE_BOLETO                 char(1) default 'N' not null,
  NOSSO_NUMERO_INICIAL         number(12),
  NOSSO_NUMERO_FINAL           number(12),
  PROXIMO_NOSSO_NUMERO         number(12),

  ATIVO                        char(1) default 'S' not null,

  CARTEIRA                     varchar2(3),
  NUMERO_REMESSA_BOLETO        number(7),
  CODIGO_EMPRESA_COBRANCA      varchar2(20),

  SALDO_ATUAL                  number(10,2) default 0 not null,
  SALDO_CONCILIADO_ATUAL       number(10,2) default 0 not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID             number(4) not null,
  DATA_HORA_ALTERACAO           date not null,
  ROTINA_ALTERACAO              varchar2(30) not null,
  ESTACAO_ALTERACAO             varchar2(30) not null
);

alter table CONTAS
add constraint PK_CONTAS
primary key(CONTA)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS
add constraint FK_CONTAS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

/* TIPO
0 - Caixa
1 - Banco
*/
alter table CONTAS
add constraint CK_CONTAS_TIPO
check(
  TIPO = 0 and EMITE_CHEQUE = 'N' and EMITE_BOLETO = 'N' and CODIGO_BANCO is null
  or
  TIPO = 1 and CODIGO_BANCO is not null
);

alter table CONTAS
add constraint CK_CONTAS_TP_COD_AG_CON
check(
  (TIPO = 0 and CODIGO_BANCO is null and AGENCIA is null and CONTA is null and DIGITO_CONTA is null)
  or
  (TIPO = 1 and CODIGO_BANCO is not null and AGENCIA is not null and CONTA is not null)    
);

alter table CONTAS
add constraint CK_CONTAS_SALDO_ATUAL
check(
  ACEITA_SALDO_NEGATIVO = 'N' and SALDO_ATUAL >= 0
  or
  ACEITA_SALDO_NEGATIVO = 'S'
);

alter table CONTAS
add constraint CK_CONTAS_EMITE_CHEQUE
check(EMITE_CHEQUE in('S', 'N'));

alter table CONTAS
add constraint CK_CONTAS_NRO_ULT_CHQ_EMIT
check(NUMERO_ULTIMO_CHEQUE_EMITIDO >= 0);

alter table CONTAS
add constraint CK_CONTAS_EMIT_CHQ_TIPO
check(
  EMITE_CHEQUE = 'N'
  or
  EMITE_CHEQUE = 'S' and TIPO = 1
);

alter table CONTAS
add constraint CK_CONTAS_EMIT_BOLETO
check(
  EMITE_BOLETO = 'N' and NOSSO_NUMERO_INICIAL is null and NOSSO_NUMERO_FINAL is null and PROXIMO_NOSSO_NUMERO is null
  or
  EMITE_BOLETO = 'S' and NOSSO_NUMERO_INICIAL is not null and NOSSO_NUMERO_FINAL is not null and PROXIMO_NOSSO_NUMERO is not null
);

alter table CONTAS
add constraint CK_CONTAS_ATIVO
check(ATIVO in('S', 'N'));