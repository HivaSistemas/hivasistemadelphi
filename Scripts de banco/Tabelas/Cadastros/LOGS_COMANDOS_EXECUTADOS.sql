create table LOGS_COMANDOS_EXECUTADOS(
  COMANDO_ANTERIOR      number(10,0),
  COMANDO_NOVO          number(10,0),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);