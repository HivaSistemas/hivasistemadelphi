create table MARCAS(
  MARCA_ID     number(6),
  NOME         varchar2(40) not null,
  ATIVO        char(1) default 'S' not null,
  CHAVE_IMPORTACAO varchar2(20)
);

/* Chave primaria */
alter table MARCAS
add constraint PK_MARCAS_MARCA_ID
primary key(MARCA_ID)
using index tablespace INDICES;

/* Checagens */
alter table MARCAS
add constraint CK_MARCAS_ATIVO
check(ATIVO in('S', 'N'));

alter table MARCAS
add constraint CK_MARCAS_NOME
unique(NOME);