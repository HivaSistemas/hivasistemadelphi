create table ENDERECO_EST_NIVEL(
  NIVEL_ID                number(3),
  DESCRICAO               varchar2(30) not null
);

/* Chave prim�ria */
alter table ENDERECO_EST_NIVEL
add constraint PK_END_EST_MOD_NIV_ID
primary key(NIVEL_ID)
using index tablespace INDICES;