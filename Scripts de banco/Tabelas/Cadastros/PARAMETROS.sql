create table PARAMETROS(
  CADASTRO_CONSUMIDOR_FINAL_ID    number(10) not null,
  INICIAR_VENDA_CONSUMIDOR_FINAL  char(1) default 'N' not null,
  USUARIO_GERENTE_SISTEMA_ID      number(4) not null,
  UTILIZAR_ANALISE_CUSTO          char(1) default 'N' not null,
  QTDE_MENSAGENS_OBRIGAR_LEITURA  number(3) default 0 not null,
  QUANTIDADE_DIAS_VALIDADE_SENHA  number(3) default 90 not null,
  MOTIVO_AJU_EST_BX_COMPRA_ID     number(3),

  TIPO_COBRANCA_GERACAO_CRED_ID   number(3),
  TIPO_COBRANCA_GER_CRED_IMP_ID   number(3),
  TIPO_COB_RECEB_ENTREGA_ID       number(3),
  TIPO_COB_ACUMULATIVO_ID         number(3),
  TIPO_COB_ADIANTAMENTO_ACU_ID    number(3),
  TIPO_COB_ADIANTAMENTO_FIN_ID    number(3),
  TIPO_COB_FECHAMENTO_TURNO_ID    number(3),
  TIPO_COB_ENTRADA_NFE_XML_ID     number(3),

  BLOQUEAR_CREDITO_DEVOLUCAO      char(1) default 'N' not null,
  BLOQUEAR_CREDITO_PAGAR_MANUAL   char(1) default 'N' not null,

  ULTIMO_ANO_MES_FINALIZADO       number(6),

  VERSAO_ATUAL                    varchar2(12),
  COMANDO_ATUAL                   number(10, 0) not null,

  QTDE_SEG_TRAVAR_ALTIS_OCIOSO    number(4) default 3600 not null,
  
  QTDE_MAXIMA_DIAS_DEVOLUCAO      number(3) default 30 not null,

  UTIL_MODULO_PRE_ENTRADAS        char(1) default 'N' not null),
  UTILIZA_VENDA_RAPIDA	          char(1) default 'N' not null),
  UTILIZA_CONTROLE_ENTREGA        char(1) default 'N' not null),
  UTILIZA_ENDERECO_ESTOQUE        char(1) default 'N' not null),
  IMPRIME_LISTA_SEPARACAO_VEND    char(1) default 'N' not null),
  DEFINIR_LOCAL_MANUAL            char(1) default 'N' not null),
  PREENCHER_LOCAIS_AUTO           char(1) default 'N' not null),
  SOMENTE_LOCAL_EST_POSITIVO      char(1) default 'N' not null),
  UTILIZA_MARKETPLACE             char(1) default 'N' not null),
  APLICAR_INDICE_DEDUCAO          char(1) default 'N' not null,
  EMITIR_NOTA_ACUMULADO_FECH_PED  char(1) default 'N' not null,
  UTILIZA_DIRECIONAMENTO_POR_LOC  char(1) default 'N' not null,
  QUANTIDADE_VIAS_RECIBO_PAG      number(6) default 1 not null,
  CONFIRMAR_TRANSF_LOCAIS         char(1) default 'N' not null,
  ATUALIZAR_PRECO_VENDA_ENT_NOT   char(1) default 'N' not null,
  DELETAR_NF_PENDENTE_EMISSAO     char(1) default 'N' not null,
  ATU_CUSTO_PROD_AGUAR_CHE        char(1) default 'N' not null,
  CALC_COMISS_ACU_REC_FIN         char(1) default 'N' not null,
  CAL_COMISS_ACU_REC_FIN_DATA     date,
  UTILIZA_CONTROLE_MANIFESTO      CHAR(1) DEFAULT 'S' NOT NULL,
  AJUSTE_ESTOQUE_REAL             char(1) default 'N' not null,
  ORCAMENTO_AMBIENTE              char(1) default 'N' not null
);

/* Chaves estrangeiras */
alter table PARAMETROS
add constraint FK_PARAM_CAD_CONS_FINAL_ID
foreign key(CADASTRO_CONSUMIDOR_FINAL_ID)
references CLIENTES(CADASTRO_ID);

alter table PARAMETROS
add constraint FK_PARAM_USUARIO_GER_SIST_ID
foreign key(USUARIO_GERENTE_SISTEMA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table PARAMETROS
add constraint FK_PAR_MOT_AJ_EST_BX_COMPRA_ID
foreign key(MOTIVO_AJU_EST_BX_COMPRA_ID)
references MOTIVOS_AJUSTE_ESTOQUE(MOTIVO_AJUSTE_ID);

alter table PARAMETROS
add constraint FK_PAR_TIPO_COB_GER_CR_ID
foreign key(TIPO_COBRANCA_GERACAO_CRED_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

alter table PARAMETROS
add constraint FK_PAR_TP_C_GER_CR_IMP_ID
foreign key(TIPO_COBRANCA_GER_CRED_IMP_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

alter table PARAMETROS
add constraint FK_PAR_TP_COB_REC_ENT_ID
foreign key(TIPO_COB_RECEB_ENTREGA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

alter table PARAMETROS
add constraint FK_PAR_TP_COB_ACUM_ID
foreign key(TIPO_COB_ACUMULATIVO_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

alter table PARAMETROS
add constraint FK_PAR_TIPO_COBR_ADIAN_ACUM_ID
foreign key(TIPO_COB_ADIANTAMENTO_ACU_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

alter table PARAMETROS
add constraint FK_PAR_TIPO_COBR_ADIAN_FINA_ID
foreign key(TIPO_COB_ADIANTAMENTO_FIN_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

alter table PARAMETROS
add constraint FK_PAR_TIPO_COBR_FECH_TURN_ID
foreign key(TIPO_COB_FECHAMENTO_TURNO_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

/* Constraints */
alter table PARAMETROS
add constraint CK_PAR_INI_VENDA_CONS_FINAL
check(INICIAR_VENDA_CONSUMIDOR_FINAL in('S','N'));

alter table PARAMETROS
add constraint CK_PAR_UTILIZAR_ANALISE_CUSTO
check(UTILIZAR_ANALISE_CUSTO in('S','N'));

alter table PARAMETROS
add constraint CK_PAR_QTDE_MENS_OBRIGAR_LEIT
check(QTDE_MENSAGENS_OBRIGAR_LEITURA >= 0);

alter table PARAMETROS
add constraint CK_PAR_QTDE_DIAS_VALIDADE_SEN
check(QUANTIDADE_DIAS_VALIDADE_SENHA between 30 and 370);

alter table PARAMETROS
add constraint CK_PAR_BLOQUEAR_CREDITO_DEVOL
check(BLOQUEAR_CREDITO_DEVOLUCAO in('S','N'));

alter table PARAMETROS
add constraint CK_PAR_BLOQ_CRED_PAGAR_MANUAL
check(BLOQUEAR_CREDITO_PAGAR_MANUAL in('S','N'));

alter table PARAMETROS
add constraint CK_PAR_UTIL_MODULO_PRE_ENTRA
check(UTIL_MODULO_PRE_ENTRADAS in('S','N'));

alter table PARAMETROS
add constraint CK_PAR_QTDE_SEG_TRAVAR_ALTIS_OCIOSO
check(QTDE_SEG_TRAVAR_ALTIS_OCIOSO > 0);

alter table PARAMETROS
add constraint CK_PAR_QTDE_MAX_DIAS_DEVOLUCAO
check(QTDE_MAXIMA_DIAS_DEVOLUCAO > 0);

alter table PARAMETROS
add constraint FK_PAR_TIPO_COBR_ENT_NFEXML_ID
foreign key(TIPO_COB_ENTRADA_NFE_XML_ID)
references TIPOS_COBRANCA(COBRANCA_ID);


/*  COMANDOS PARA EXECUTAR
  alter table PARAMETROS add (UTILIZA_DIRECIONAMENTO_POR_LOC  char(1) default 'N' not null)
  alter table PARAMETROS add (SOMENTE_LOCAL_EST_POSITIVO  char(1) default 'N' not null)
  alter table PARAMETROS add (CALC_ADI_COMISSAO_META_FUN  char(1) default 'N' not null)


  ALTER TABLE PARAMETROS
  ADD UTILIZA_CONTROLE_MANIFESTO CHAR(1) DEFAULT 'S' NOT NULL
  */




