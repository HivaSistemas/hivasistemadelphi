create table TIPO_COBRANCA_DIAS_PRAZO(
  COBRANCA_ID   number(3),
  DIAS          number(3) not null
);

alter table TIPO_COBRANCA_DIAS_PRAZO
add constraint PK_TIPO_COBRANCA_DIAS_PRAZO
primary key(COBRANCA_ID, DIAS)
using index tablespace INDICES;

alter table TIPO_COBRANCA_DIAS_PRAZO
add constraint CK_TIPO_COBRANCA_DIAS_PRAZO
check(DIAS > 0);