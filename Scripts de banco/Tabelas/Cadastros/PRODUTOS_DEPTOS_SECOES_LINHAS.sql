create table PRODUTOS_DEPTOS_SECOES_LINHAS(
  DEPTO_SECAO_LINHA_ID     varchar2(11) not null,
  NOME                     varchar2(60) not null,
  DEPTO_SECAO_LINHA_PAI_ID varchar2(11),
  ATIVO                    char(1) default 'S' not null,
  COMISSAO_VENDA_PRAZO number(6,4) default 0 not null,
  COMISSAO_VENDA_VISTA number(6,4) default 0 not null,
  GRUPO_PRODUTOS_WEB_ID varchar2(17),
  PERC_COMISSAO_PROFISSIONAL number(10,4) default 0 not null,
  PERC_MARG_PRETEND_PRECO_VAR number(5,2) default 0 not null,
  PERC_MARG_PRETEND_PRECO_AT1 number(5,2) default 0 not null,
  PERC_MARG_PRETEND_PRECO_AT2 number(5,2) default 0 not null,
  PERC_MARG_PRETEND_PRECO_AT3 number(5,2) default 0 not null,
  PERC_MARG_PRETEND_PRECO_PON number(5,2) default 0 not null
);

alter table PRODUTOS_DEPTOS_SECOES_LINHAS
add constraint PK_PRODUTOS_DEPTOS_SECOES_LINH
primary key(DEPTO_SECAO_LINHA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PRODUTOS_DEPTOS_SECOES_LINHAS
add constraint FK_PRO_DEPTO_SEC_L_DEPT_PAI_ID
foreign key(DEPTO_SECAO_LINHA_PAI_ID)
references PRODUTOS_DEPTOS_SECOES_LINHAS(DEPTO_SECAO_LINHA_ID);

/* Constraints */
alter table PRODUTOS_DEPTOS_SECOES_LINHAS
add constraint CK_PRO_DEPTO_SEC_L_DEPT_PAI_ID
check(
  length(DEPTO_SECAO_LINHA_ID) = 3 and DEPTO_SECAO_LINHA_PAI_ID is null
  or
  length(DEPTO_SECAO_LINHA_ID) in(7,11) and DEPTO_SECAO_LINHA_PAI_ID is not null
);

alter table PRODUTOS_DEPTOS_SECOES_LINHAS
add constraint CK_PRO_DEPTOS_SEC_LINHAS_ATIVO
check(ATIVO in('S','N'));