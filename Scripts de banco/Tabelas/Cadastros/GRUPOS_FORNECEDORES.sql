create table GRUPOS_FORNECEDORES(
  GRUPO_ID                number(4) not null,
  DESCRICAO               varchar2(50) not null,
  ATIVO                   char(1) default 'S' not null,
    
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table GRUPOS_FORNECEDORES
add constraint PK_GRUPOS_FORNECEDORES
primary key(GRUPO_ID)
using index tablespace INDICES;

/* Constraints */
alter table GRUPOS_FORNECEDORES
add constraint CK_GRUPOS_FORNECEDORES_ATIVO
check(ATIVO in('S', 'N'));

alter table GRUPOS_FORNECEDORES
add constraint UN_GRUPOS_FORN_DESCRICAO
unique(DESCRICAO);