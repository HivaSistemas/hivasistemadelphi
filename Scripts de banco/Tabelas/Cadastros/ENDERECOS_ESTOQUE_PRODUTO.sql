create table ENDERECOS_ESTOQUE_PRODUTO(
  POSICAO                      number(3),
  PRODUTO_ID                   number(10),
  ENDERECO_ID                  number(10)
);

/* Chaves estrangeiras */
alter table ENDERECOS_ESTOQUE_PRODUTO
add constraint FK_END_EST_PROD_PROD_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* Chaves estrangeiras */
alter table ENDERECOS_ESTOQUE_PRODUTO
add constraint FK_END_EST_PROD_END_ID
foreign key(ENDERECO_ID)
references ENDERECO_ESTOQUE(ENDERECO_ID);
