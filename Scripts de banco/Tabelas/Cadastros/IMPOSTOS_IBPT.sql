create table IMPOSTOS_IBPT(
  CODIGO_NCM              varchar2(8) not null,
  ESTADO_ID               char(2) not null,
  
  FONTE                   varchar2(100) not null,  
  PERC_FEDERAL_NACIONAL   number(15,2) not null,
  PERC_FEDERAL_IMPORTADO  number(15,2) not null,
  PERC_ESTADUAL           number(15,2) not null,
  PERC_MUNICIPAL          number(15,2) not null
);

/* Chave prim�ria */
alter table IMPOSTOS_IBPT
add constraint PK_IMPOSTOS_IBPT
primary key (CODIGO_NCM, ESTADO_ID)
using index tablespace INDICES;

/* Chave estrangeira */
alter table IMPOSTOS_IBPT
add constraint FK_ESTADOS_IMPOSTOS_IBPT
foreign key (ESTADO_ID)
references ESTADOS(ESTADO_ID);

/* Checagens */
alter table IMPOSTOS_IBPT
add constraint CK_IMP_IBPT_PERC_IMP_FED_NAC
check (PERC_FEDERAL_NACIONAL >= 0);

alter table IMPOSTOS_IBPT
add constraint CK_IMP_IBPT_PERC_IMP_FED_IMP
check (PERC_FEDERAL_IMPORTADO >= 0);

alter table IMPOSTOS_IBPT
add constraint CK_IMPOSTOS_IBPT_PERC_ESTAD
check (PERC_ESTADUAL >= 0);

alter table IMPOSTOS_IBPT
add constraint CK_IMPOSTOS_IBPT_PERC_MUNIC
check (PERC_MUNICIPAL >= 0);
