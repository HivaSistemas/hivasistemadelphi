﻿create table CLIENTES_GRUPOS(
  CLIENTE_GRUPO_ID    number(3,0),
  NOME                varchar2(30) not null,
  ATIVO               char(1) default 'S' not null,
  
  /* Logs do sistema*/
  USUARIO_SESSAO_ID   number(4,0) not null,
  DATA_HORA_ALTERACAO date not null,
  ROTINA_ALTERACAO    varchar2(30) not null,
  ESTACAO_ALTERACAO   varchar2(30) not null
);

/* Chave primária */
alter table CLIENTES_GRUPOS
add constraint PK_CLIENTES_GRUPOS
primary key(CLIENTE_GRUPO_ID)
using index tablespace INDICES;

/* Checagens */
alter table CLIENTES_GRUPOS
add constraint CK_CLIENTES_GRUPOS_ATIVO
check(ATIVO in('S', 'N'));

/* Uniques */
alter table CLIENTES_GRUPOS
add constraint UN_CLIENTES_GRUPOS_NOME
unique (NOME);
