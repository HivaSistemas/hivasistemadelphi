create table CIDADES(
  CIDADE_ID                     number(15) not null,
  NOME                          varchar2(60) not null,
  CODIGO_IBGE                   number(8),
  ESTADO_ID                     char(2) not null,
  TIPO_BLOQUEIO_VENDA_ENTREGAR  char(1) default 'N' not null,

  ATIVO                         char(1) default 'S' not null,

  CHAVE_IMPORTACAO              varchar2(100),
  VALOR_CALCULO_FRETE_KM        number(8),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table CIDADES
add constraint PK_CIDADES_CIDADE_ID
primary key(CIDADE_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CIDADES
add constraint FK_CIDADES_ESTADO_ID
foreign key(ESTADO_ID)
references ESTADOS(ESTADO_ID);

/* Checagens */
alter table CIDADES
add constraint CK_CIDADES_ATIVO
check(ATIVO in('S', 'N'));

/*
  TIPO_BLOQUEIO_VENDA_ENTREGAR
  N - N�o bloquear
  S - Sempre bloquear
  V - N�o permitir venda
*/
alter table CIDADES
add constraint CK_CIDADES_TP_BLOQ_VENDA_ENTRE
check(TIPO_BLOQUEIO_VENDA_ENTREGAR in('N', 'S', 'V'));

alter table CIDADES
add constraint CK_CIDADES_NOME_ESTADO_ID
unique(NOME, ESTADO_ID);
