create table CFOP(
  CFOP_ID                        varchar2(4) not null,
  FLUTUACAO                      number(2) default 0 not null,  
  CFOP_PESQUISA_ID               varchar2(7) not null, -- Neste vem concatenada a FLUTUACAO
  CFOP_CORRESPONDENTE_ENTRADA_ID varchar2(7),
  NOME                           varchar2(250) not null,
  DESCRICAO                      varchar2(400) not null,
  ATIVO                          char(1) default 'S' not null,
  PLANO_FINANCEIRO_ENTRADA_ID    varchar2(9),
  
  ENTRADA_MERCADORIAS            char(1) default 'N' not null,
  ENTRADA_MERCADORIAS_REVENDA    char(1) default 'N' not null,
  ENTRADA_MERC_USO_CONSUMO       char(1) default 'N' not null,
  ENTRADA_MERCADORIAS_BONIFIC    char(1) default 'N' not null,
  ENTRADA_SERVICOS               char(1) default 'N' not null,
  ENTRADA_CONHECIMENTO_FRETE     char(1) default 'N' not null,
  NAO_EXIGIR_ITENS_ENTRADA       char(1) default 'N' not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave prim�ria */
alter table CFOP
add constraint PK_CFOP_CFOP_ID
primary key(CFOP_ID, FLUTUACAO)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CFOP
add constraint FK_CFOP_CFOP_CORRESPON_ENT_ID
foreign key(CFOP_CORRESPONDENTE_ENTRADA_ID)
references CFOP(CFOP_PESQUISA_ID);

alter table CFOP
add constraint FK_CFOP_PLANO_FINAN_ENTRADA_ID
foreign key(PLANO_FINANCEIRO_ENTRADA_ID)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

/* Checagens */
alter table CFOP
add constraint CK_CFOP_FLUTUACAO
check(FLUTUACAO >= 0);

alter table CFOP
add constraint CK_CFOP_ATIVO
check(ATIVO in('S', 'N'));

alter table CFOP
add constraint CK_CFOP_ENTRADA_MERCADORIAS
check(ENTRADA_MERCADORIAS in('S', 'N'));

alter table CFOP
add constraint CK_CFOP_ENTRADA_MERC_REVENDA
check(ENTRADA_MERCADORIAS_REVENDA in('S', 'N'));
  
alter table CFOP
add constraint CK_CFOP_ENTR_MERC_USO_CONSUMO
check(ENTRADA_MERC_USO_CONSUMO in('S', 'N'));

alter table CFOP
add constraint CK_CFOP_ENTRADA_MERC_BONIFIC
check(ENTRADA_MERCADORIAS_BONIFIC in('S', 'N'));

alter table CFOP
add constraint CK_CFOP_ENTRADA_SERVICOS
check(ENTRADA_SERVICOS in('S', 'N'));

alter table CFOP
add constraint CK_CFOP_ENTRADA_CONHEC_FRETE
check(ENTRADA_CONHECIMENTO_FRETE in('S', 'N'));

alter table CFOP
add constraint CK_CFOP_NAO_EXIGIR_ITENS_ENTR
check(NAO_EXIGIR_ITENS_ENTRADA in('S', 'N'));

/* Uniques */
alter table CFOP
add constraint UN_CFOP_CFOP_PESQUISA_ID
unique(CFOP_PESQUISA_ID)
using index tablespace INDICES;