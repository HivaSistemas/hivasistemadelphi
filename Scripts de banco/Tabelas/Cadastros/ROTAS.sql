create table ROTAS(
  ROTA_ID    number(3) not null,
  NOME       varchar2(30) not null,
  ATIVO      char(1) default 'S' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table ROTAS
add constraint PK_ROTAS
primary key(ROTA_ID)
using index tablespace INDICES;

/* Checagens */
alter table ROTAS
add constraint CK_ROTAS_ATIVO
check(ATIVO in('S', 'N'));

/* Uniques */
alter table ROTAS
add constraint CK_ROTAS_NOME
unique(NOME)
using index tablespace INDICES;