create table CLIENTES_CONDIC_PAGTO_RESTRIT(
  CLIENTE_ID  number(10) not null,
  CONDICAO_ID number(3) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table CLIENTES_CONDIC_PAGTO_RESTRIT
add constraint PK_CLI_CONDIC_PAGTO_RESTRITAS
primary key(CLIENTE_ID, CONDICAO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CLIENTES_CONDIC_PAGTO_RESTRIT
add constraint FK_CLI_CON_RESTRIT_CLIENTE_ID
foreign key(CLIENTE_ID)
references CLIENTES(CADASTRO_ID);

alter table CLIENTES_CONDIC_PAGTO_RESTRIT
add constraint FK_CLI_CON_RESTRITA_COND_PAGTO
foreign key(CONDICAO_ID)
references CONDICOES_PAGAMENTO(CONDICAO_ID);
