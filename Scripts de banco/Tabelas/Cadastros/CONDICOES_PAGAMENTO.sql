﻿create table CONDICOES_PAGAMENTO(
  CONDICAO_ID 					         number(3),
  NOME        					         varchar2(60) not null,
  TIPO        					         char(1) not null,
  INDICE_ACRESCIMO  			       number(5,4) not null,
  VALOR_MINIMO_VENDA    		     number(8,2) default 0 not null,
  VALOR_MINIMO_PARCELA  		     number(8,2) default 0 not null,
  PERCENTUAL_DESCONTO_PERM_VENDA number(4,2) default 0 not null,
  
  TIPO_PRECO                     char(1) default 'V' not null, 
  PERCENTUAL_JUROS               number(4,2) default 0 not null,
  PERCENTUAL_ENCARGOS            number(4,2) default 0 not null,
  PERCENTUAL_DESCONTO_COMERCIAL  number(4,2) default 0 not null,
  PERCENTUAL_CUSTO_VENDA         number(4,2) default 0 not null,
  PRAZO_MEDIO                    number(3) default 0 not null,
  PERMITIR_PRECO_PROMOCIONAL     char(1) default 'N' not null,

  EXIGIR_MODELO_NOTA_FISCAL      char(1) default 'N' not null,
  APLICAR_IND_PRECO_PROMOCIONAL  char(1) default 'N' not null,
  PERMITIR_USO_PONTA_ESTOQUE     char(1) default 'N' not null,
  PERIMTIR_USO_FECHAMENTO_ACUMU  char(1) default 'S' not null

  NAO_PERMITIR_CONS_FINAL        char(1) default 'N' not null,

  RESTRITA                       char(1) default 'N' not null,
  BLOQUEAR_VENDA_SEMPRE          char(1) default 'N' not null,
  RECEBIMENTO_NA_ENTREGA         char(1) default 'N' not null,
  ACUMULATIVO                    char(1) default 'N' not null,
  /* Nova Hiva
  IMPRIMIR_CONFISSAO_DIVIDA 	 char(1) default 'N' not null */
  IMP_COMP_FATURAMENTO_VENDA     char(1) default 'N' not null,
	

  ATIVO                 		     char(1) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table CONDICOES_PAGAMENTO
add constraint PK_CONDICAO_ID
primary key(CONDICAO_ID)
using index tablespace INDICES;


/* Checagens */
/* TIPO
  A - A Vista
  P - A Prazo
*/
alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGAMENTO_TIPO
check(TIPO in('A', 'P'));

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGAMENTO_IND_ACRESCIM
check(INDICE_ACRESCIMO >= 1);

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_VLR_MIN_VENDA
check(VALOR_MINIMO_VENDA >= 0);

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_VLR_MIN_PARCELA
check(VALOR_MINIMO_PARCELA >= 0);

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_PERC_DESC_PERM_V
check(PERCENTUAL_DESCONTO_PERM_VENDA between 0 and 99.99);

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGAMENTO_ATIVO
check(ATIVO in('S', 'N'));

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_PRAZO_MEDIO
check(
  (PRAZO_MEDIO = 0)
  or
  (PRAZO_MEDIO >= 0 and TIPO = 'P')
);

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_PERCENTUAL_JUROS
check(
  (TIPO = 'A' and PERCENTUAL_JUROS = 0)
  or
  (TIPO = 'P' and PERCENTUAL_JUROS between 0 and 99.99)
);

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_PERC_ENCARGOS
check(PERCENTUAL_ENCARGOS >= 0);

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_PERC_DESC_COMERC
check(PERCENTUAL_DESCONTO_COMERCIAL between 0 and 99.99);

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_PERC_CUSTO_VENDA
check(PERCENTUAL_CUSTO_VENDA between 0 and 99.99);

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_EXI_MOD_NOTA_FIS
check(EXIGIR_MODELO_NOTA_FISCAL in('S', 'N'));
  
alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_APLI_IND_PR_PROM
check(APLICAR_IND_PRECO_PROMOCIONAL in('S', 'N'));

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_PER_USO_PONT_EST
check(PERMITIR_USO_PONTA_ESTOQUE in('S', 'N'));  
    
alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_PER_USO_FEC_ACUM
check(PERIMTIR_USO_FECHAMENTO_ACUMU in('S', 'N'));

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_NAO_P_CONS_FINAL
check(NAO_PERMITIR_CONS_FINAL in('S', 'N'));

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_RESTRITA
check(RESTRITA in('S', 'N'));

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_BLOQ_VENDA_SEMP
check(BLOQUEAR_VENDA_SEMPRE in('S', 'N'));

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_REC_NA_ENTREGA
check(RECEBIMENTO_NA_ENTREGA in('S', 'N'));

/*
  TIPO_PRECO
  V - Varejo
  A - Atacado
  T - Todos, Varejo e atacado
  I - Ambos ignorando regra do cliente
*/
alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_TIPO_PRECO
check(TIPO_PRECO in('V', 'A', 'T', 'I'));

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_PER_PRECO_PROMOC
check(PERMITIR_PRECO_PROMOCIONAL in('S', 'N'));

alter table CONDICOES_PAGAMENTO
add constraint CK_COND_PAGTO_ACUMULATIVO
check(ACUMULATIVO in('S', 'N'));

