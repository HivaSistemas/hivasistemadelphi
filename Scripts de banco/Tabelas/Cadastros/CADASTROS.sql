﻿create table CADASTROS(
  CADASTRO_ID            number(10),
  NOME_FANTASIA          varchar2(100) not null,
  RAZAO_SOCIAL           varchar2(100) not null,
  TIPO_PESSOA            char(1) not null,
  CPF_CNPJ               varchar2(19) not null,
  LOGRADOURO             varchar2(100),
  COMPLEMENTO            varchar2(100),
  NUMERO                 varchar2(10) default 'SN' not null,
  BAIRRO_ID              number(15) not null,
  PONTO_REFERENCIA       varchar2(200),
  CEP                    varchar2(9),
  TELEFONE_PRINCIPAL     varchar2(15),
  TELEFONE_CELULAR       varchar2(15),
  TELEFONE_FAX           varchar2(15),  
  E_MAIL                 varchar2(80),
  E_MAIL_NFE2            varchar2(80),
  SEXO                   char(1),
  ESTADO_CIVIL           char(1),
  DATA_NASCIMENTO        date,
  DATA_CADASTRO          date default trunc(sysdate) not null,
  DATA_EMISSAO_RG        date,
  INSCRICAO_ESTADUAL     varchar(20) default 'ISENTO' not null,
  CNAE                   varchar(12),
  RG                     varchar2(20),
  ORGAO_EXPEDIDOR_RG     varchar2(10),
  NATURALIDADE           varchar2(40),
    
  E_CLIENTE              char(1) default 'N' not null,
  E_FORNECEDOR           char(1) default 'N' not null,
  E_MOTORISTA            char(1) default 'N' not null,
  E_TRANSPORTADORA       char(1) default 'N' not null,
  E_PROFISSIONAL         char(1) default 'N' not null,
  E_CONCORRENTE          char(1) default 'N' not null, 
  E_FUNCIONARIO          char(1) default 'N' not null,
  ATIVO                  char(1) default 'S' not null,

  CHAVE_IMPORTACAO       varchar2(100),
  OBSERVACOES            varchar2(4000)
);

/* Chave primária */
alter table CADASTROS
add constraint PK_CADASTROS_CADASTRO_ID
primary key(CADASTRO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CADASTROS
add constraint FK_CADASTROS_BAIRRO_ID
foreign key(BAIRRO_ID)
references BAIRROS(BAIRRO_ID);

/* Checagens */
/* TIPO_PESSOA
  F - Fisica
  J - Jurídica
 */
alter table CADASTROS
add constraint CK_CADASTROS_TIPO_PESSOA
check(TIPO_PESSOA in('F', 'J'));

/* SEXO
  M - Masculino
  F - Feminino
*/
alter table CADASTROS
add constraint CK_CADASTROS_SEXO
check(SEXO in('M', 'F', null));

/* ESTADO_CIVIL
  S - Solteiro
  C - Casado
  D - Divorciado
  V - Viuvo
  E - Separado
*/
alter table CADASTROS
add constraint CK_CADASTROS_ESTADO_CIVIL
check(ESTADO_CIVIL in('S', 'C', 'D', 'V','E' null));

alter table CADASTROS
add constraint CK_CADASTROS_E_CLIENTE
check(E_CLIENTE in('S', 'N'));

alter table CADASTROS
add constraint CK_CADASTROS_E_FORNECEDOR
check(E_FORNECEDOR in('S', 'N'));

alter table CADASTROS
add constraint CK_CADASTROS_E_MOTORISTA
check(E_MOTORISTA in('S', 'N'));

alter table CADASTROS
add constraint CK_CADASTROS_E_TRANSPORTADORA
check(E_TRANSPORTADORA in('S', 'N'));

alter table CADASTROS
add constraint CK_CADASTROS_ATIVO
check(ATIVO in('S', 'N'));

alter table CADASTROS
add constraint CK_CADASTROS_CPF_CNPJ_FORM
check(
  (TIPO_PESSOA = 'F' and length(CPF_CNPJ) = 14)
  or
  (TIPO_PESSOA = 'J' and length(CPF_CNPJ) = 18)
);

/* Uniques */
alter table CADASTROS
add constraint CK_CADASTROS_CPF_CNPJ
unique(CPF_CNPJ);

/* Indices */
create index IDX_CADASTROS_NOME_FANTASIA
on CADASTROS(NOME_FANTASIA)
tablespace INDICES;
