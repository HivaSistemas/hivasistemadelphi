create table VEICULOS(
  VEICULO_ID            number(5) not null,
  MOTORISTA_ID          number(10) not null,
  NOME                  varchar2(40) not null,
  PLACA                 varchar2(8) not null,
  ANO                   number(4),
  TIPO_COMBUSTIVEL      char(1) default 'G' not null,
  CHASSSI               varchar2(20),
  COR                   varchar2(15),
  TIPO_VEICULO          char(3) default 'CAR' not null,
  PESO_MAXIMO           number(5) default 0 not null,
  TARA                  number(5) default 0 not null,
  ATIVO                 char(1) default 'S' not null,
  MARCA                 varchar2(30),
  DESCRICAO             varchar2(500),
  CODIGO_ANTT           varchar2(20));
    
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table VEICULOS
add constraint PK_VEICULOS
primary key(VEICULO_ID);

/* Chaves estrangeiras */
/*alter table VEICULOS
add constraint FK_VEICULOS_MOTORISTA_ID
foreign key(MOTORISTA_ID)
references MOTORISTAS(CADASTRO_ID); */

/* Checagens */
/* 
  TIPO_COMBUSTIVEL
  A - Alcool
  G - Gasolina
  F - Flex
  D - Diesel
  N - GNV
*/
alter table VEICULOS
add constraint CK_VEICULOS_TIPO_COMBUSTIVEL
check(TIPO_COMBUSTIVEL in('A', 'G', 'F', 'D', 'N'));

/* 
  TIPO_VEICULO
  CAC - Carroça
  MOT - Moto
  CAR - Carro
  CAI - Caminhonete
  CAM - Caminhão
*/
alter table VEICULOS
add constraint CK_VEICULOS_TIPO_VEICULO
check(TIPO_VEICULO in('CAC', 'MOT', 'CAR', 'CAI', 'CAM'));

alter table VEICULOS
add constraint CK_VEICULOS_PESO_MAXIMO
check(PESO_MAXIMO >= 0);

alter table VEICULOS
add constraint CK_VEICULOS_TARA
check(TARA >= 0);

alter table VEICULOS
add constraint CK_VEICULOS_ATIVO
check(ATIVO in('S', 'N'));