create table PRODUTOS_CUSTOS_ANO_MES(
  EMPRESA_ID         number(3) not null,
  PRODUTO_ID         number(10) not null,      
  
  CUSTO_MEDIO        number(9,4) default 0 not null,
  CUSTO_ENTRADA      number(9,4) default 0 not null,    
  CUSTO_MEDIO_REAL   number(9,4) default 0 not null,
  CUSTO_ENTRADA_REAL number(9,4) default 0 not null, 
  CUSTO_COMERCIAL    number(9,4) default 0 not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table PRODUTOS_CUSTOS_ANO_MES
add constraint PK_PRODUTOS_CUSTOS_ANO_MES
primary key(EMPRESA_ID, PRODUTO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CUSTOS_PRODUTOS
add constraint FK_CUSTOS_PRODUTOS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table CUSTOS_PRODUTOS
add constraint FK_CUSTOS_PRODUTOS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* Constraints */
alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PRODUTOS_CUSTO_MEDIO
check(CUSTO_MEDIO >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PRODUTOS_CUSTO_ENTRA
check(CUSTO_ENTRADA >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_CUSTO_MED_REAL
check(CUSTO_MEDIO_REAL >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_CUSTO_ENTR_REAL
check(CUSTO_ENTRADA_REAL >= 0);