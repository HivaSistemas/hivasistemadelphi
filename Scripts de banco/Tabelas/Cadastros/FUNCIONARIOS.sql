create table FUNCIONARIOS(
  FUNCIONARIO_ID  			         number(4),
  NOME                           varchar2(60) not null,
  SENHA_SISTEMA                  varchar2(50) default 'djSXaD8sI5eUIIfBcT85hNVPHIW3K9IC7Gzx6ypTd4U=' not null,
  SENHA_ALTIS_W                  varchar2(50) default '123456' not null,
  APELIDO                        varchar2(40),
  DATA_CADASTRO                  date default trunc(sysdate) not null,
  DATA_NASCIMENTO                date not null,
  CARGO_ID				               number(3) not null,
  CPF                            varchar2(14),
  RG                             varchar2(20),
  ORGAO_EXPEDIDOR_RG             varchar2(10),
  DATA_ADMISSAO 			           date not null,
  LOGRADOURO                     varchar2(100) not null,
  COMPLEMENTO                    varchar2(100) not null,
  NUMERO                         varchar2(10) default 'SN' not null,
  BAIRRO_ID                      number(15) not null,
  CEP                            varchar2(9),
  E_MAIL                         varchar2(30),
  SERVIDOR_SMTP                  varchar2(50),
  USUARIO_E_MAIL                 varchar2(30),
  SENHA_E_MAIL                   varchar2(32),
  PORTA_E_MAIL                   number(5), 
  CADASTRO_ID                    number(10) not null,
  FOTO                           blob,

  PERCENTUAL_COMISSAO_A_VISTA    number(4,2) default 0 not null,
  PERCENTUAL_COMISSAO_A_PRAZO    number(4,2) default 0 not null,
  PERCENTUAL_DESCONTO_ADIC_VENDA number(4,2) default 0 not null,
  PERCENTUAL_DESCONTO_ADIC_FINAN number(4,2) default 0 not null,
  PERC_DESC_ITEM_PRECO_MANUAL    number(4,2) default 0 not null,

  SALARIO					               number(7,2) default 0 not null,
  ATIVO                          char(1) default 'S' not null,

  VENDEDOR                       char(1) default 'N' not null,
  CAIXA                          char(1) default 'N' not null,
  COMPRADOR                      char(1) default 'N' not null,
  MOTORISTA                      char(1) default 'N' not null,
  AJUDANTE                       char(1) default 'N' not null,

  NUMERO_CARTEIRA_TRABALHO       varchar2(30),
  SERIE_CARTEIRA_TRABALHO        varchar2(10),
  PIS_PASEP                      varchar2(30),
  DATA_DESLIGAMENTO              date,

  DATA_ULTIMA_SENHA              date default trunc(sysdate) not null,
  ACESSA_ALTIS_W                 char(1) default 'N' not null,

  TITULO_ELEITORAL               varchar2(15),
  SECAO_TITULO_ELEITORAL         varchar2(2),
  ZONA_TITULO_ELEITORAL          varchar2(5),
  RESERVISTA                     varchar2(15),
  DATA_EMISSAO_RG                date,
  OBSERVACAO                     varchar2(400),
  QUANTIDADE_FILHOS              number(2,0) DEFAULT 0 NOT NULL,
  USUARIO_ALTIS                  char(1) DEFAULT 'N'  NOT NULL

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table FUNCIONARIOS
add constraint PK_FUNCIONARIOS_FUNCIONARIO_ID
primary key(FUNCIONARIO_ID)
using index tablespace INDICES;

/* Chaves estrangeira */
alter table FUNCIONARIOS
add constraint FK_FUNCIONARIOS_CARGO_ID
foreign key(CARGO_ID)
references CARGOS_FUNCIONARIOS(CARGO_ID);

alter table FUNCIONARIOS
add constraint FK_FUNCIONARIOS_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);

/* Checagens */
alter table FUNCIONARIOS
add constraint CK_FUNCIONARIOS_CPF
check(trim(replace(CPF, '.', '')) = '');

alter table FUNCIONARIOS
add constraint CK_FUNCIONARIOS_CPF_UNICO
unique(CPF);

alter table FUNCIONARIOS
add constraint CK_FUNC_PERC_COMIS_A_VISTA
check(PERCENTUAL_COMISSAO_A_VISTA between 0 and 99.99);

alter table FUNCIONARIOS
add constraint CK_FUNC_PERC_COMIS_A_PRAZO
check(PERCENTUAL_COMISSAO_A_PRAZO between 0 and 99.99);

alter table FUNCIONARIOS
add constraint CK_FUNC_PERC_DESCON_ADIC_VENDA
check(PERCENTUAL_DESCONTO_ADIC_VENDA between 0 and 99.99);

alter table FUNCIONARIOS
add constraint CK_FUNC_PERC_DESCONTO_ADIC_FIN
check(PERCENTUAL_DESCONTO_ADIC_FINAN between 0 and 99.99);

alter table FUNCIONARIOS
add constraint CK_FUNCIONARIOS_ATIVO
check(ATIVO in('S', 'N'));

alter table FUNCIONARIOS
add constraint CK_FUNCIONARIOS_VENDEDOR
check(VENDEDOR in('S', 'N'));

alter table FUNCIONARIOS
add constraint CK_FUNCIONARIOS_CAIXA
check(CAIXA in('S', 'N'));

alter table FUNCIONARIOS
add constraint CK_FUNCIONARIOS_COMPRADOR
check(COMPRADOR in('S', 'N'));

alter table FUNCIONARIOS
add constraint CK_FUNCIONARIOS_MOTORISTA
check(MOTORISTA in('S', 'N'));

alter table FUNCIONARIOS
add constraint CK_FUNCIONARIOS_AJUDANTE
check(AJUDANTE in('S', 'N'));

alter table FUNCIONARIOS
add constraint CK_FUNCIONARIOS_DATA_DESLI
check(
  DATA_DESLIGAMENTO is null
  or
  DATA_DESLIGAMENTO > DATA_ADMISSAO
);

alter table FUNCIONARIOS
add constraint CK_FUNC_PERC_DESC_ITEM_PRE_MAN
check(PERC_DESC_ITEM_PRECO_MANUAL >= 0);

alter table FUNCIONARIOS
add constraint CK_FUNCIONARIOS_ACESSA_ALTIS_W
check(ACESSA_ALTIS_W in('S', 'N'));

/* Uniques */
alter table FUNCIONARIOS
add constraint CK_FUNCIONARIOS_UN_CADASTRO_ID
unique(CADASTRO_ID);
