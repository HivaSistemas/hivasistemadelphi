﻿create table MOTIVOS_AJUSTE_ESTOQUE(
  MOTIVO_AJUSTE_ID        number(3),
  DESCRICAO               varchar2(30) not null,
  PLANO_FINANCEIRO_ID     varchar2(9) not null,
  ATIVO                   char(1) not null,
  INCIDE_FINANCEIRO       char(1) default 'N' not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table MOTIVOS_AJUSTE_ESTOQUE
add constraint PK_MOT_AJUSTE_EST_MOT_AJUS_ID
primary key(MOTIVO_AJUSTE_ID)
using index tablespace INDICES;

/* Constraints */
alter table MOTIVOS_AJUSTE_ESTOQUE
add constraint CK_MOT_AJUSTE_ESTOQUE_ATIVO
check(ATIVO in('S', 'N'));

alter table MOTIVOS_AJUSTE_ESTOQUE
add constraint CK_MOT_AJUSTE_INCIDE_FINANC
check(INCIDE_FINANCEIRO in('S', 'N'));
