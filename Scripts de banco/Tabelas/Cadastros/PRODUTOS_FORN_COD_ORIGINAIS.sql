create table PRODUTOS_FORN_COD_ORIGINAIS(  
  PRODUTO_ID              number(10) not null,
  FORNECEDOR_ID           number(10) not null,
  CODIGO_ORIGINAL         varchar2(35) not null,
 
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table PRODUTOS_FORN_COD_ORIGINAIS
add constraint PK_PRODUTOS_FORN_COD_ORIGINAIS
primary key(PRODUTO_ID, FORNECEDOR_ID, CODIGO_ORIGINAL)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PRODUTOS_FORN_COD_ORIGINAIS
add constraint FK_PROD_FORN_COD_ORIG_PROD_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table PRODUTOS_FORN_COD_ORIGINAIS
add constraint FK_PROD_FORN_COD_ORIG_FORN_ID
foreign key(FORNECEDOR_ID)
references FORNECEDORES(CADASTRO_ID);