create table PORTADORES_CONTAS(
  EMPRESA_ID             number(3) not null,
  PORTADOR_ID            varchar2(50) not null,
  CONTA_ID               varchar2(8) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table PORTADORES_CONTAS
add constraint PK_PORTADORES_CONTAS
primary key(EMPRESA_ID, PORTADOR_ID, CONTA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PORTADORES_CONTAS
add constraint FK_PORTADORES_CONTAS_EMPR_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table PORTADORES_CONTAS
add constraint FK_PORTADORES_CONT_PORTADOR_ID
foreign key(PORTADOR_ID)
references PORTADORES(PORTADOR_ID);

alter table PORTADORES_CONTAS
add constraint FK_PORTADORES_CONT_CONTA_ID
foreign key(CONTA_ID)
references CONTAS(CONTA);