create table UNIDADES(
  UNIDADE_ID   varchar2(5),
  DESCRICAO    varchar2(40) not null,
  ATIVO        char(1) default 'S' not null,
  CHAVE_IMPORTACAO varchar2(20)
);

alter table UNIDADES
add constraint PK_UNIDADES_UNIDADE_ID
primary key(UNIDADE_ID)
using index tablespace INDICES;

alter table UNIDADES
add constraint CK_UNIDADES_ATIVO
check(ATIVO in('S', 'N'));
