create table LOGS_FUNCIONARIOS(
  TIPO_ALTERACAO_LOG_ID   number(4) not null,
  FUNCIONARIO_ID          number(4) not null,
  VALOR_ANTERIOR          varchar2(300),
  NOVO_VALOR              varchar2(300),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chaves estrangeiras */
alter table LOGS_FUNCIONARIOS
add constraint FK_LOGS_FUNC_FUNCIONARIO_ID
foreign key(FUNCIONARIO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);