﻿create table CONTAS_CUSTODIA(
  CONTA_CUSTODIA_ID           number(3) not null,
  NOME                        varchar2(60) not null,  
  ATIVO                       char(1) default 'S' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table CONTAS_CUSTODIA
add constraint PK_CONTAS_CUSTODIA
primary key(CONTA_CUSTODIA_ID)
using index tablespace INDICES;

/* Checagens */
alter table CONTAS_CUSTODIA
add constraint CK_CONTAS_CUSTODIA_ATIVO
check(ATIVO in('S', 'N'));
