create table GRUPOS_TRIB_ESTADUAL_VENDA(
  GRUPO_TRIB_ESTADUAL_VENDA_ID number(10) not null,
  DESCRICAO                    varchar2(60) not null,
  
  ATIVO                        char(1) default 'S' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID             number(4) not null,
  DATA_HORA_ALTERACAO           date not null,
  ROTINA_ALTERACAO              varchar2(30) not null,
  ESTACAO_ALTERACAO             varchar2(30) not null    
);

/* Chave primária */
alter table GRUPOS_TRIB_ESTADUAL_VENDA
add constraint PK_GRUPOS_TRIB_ESTADUAL_VENDA
primary key(GRUPO_TRIB_ESTADUAL_VENDA_ID)
using index tablespace INDICES;

/* Checagens */
alter table GRUPOS_TRIB_ESTADUAL_VENDA
add constraint CK_GRUPOS_TRIB_EST_VENDA_ATIVO
check(ATIVO in('S', 'N'));