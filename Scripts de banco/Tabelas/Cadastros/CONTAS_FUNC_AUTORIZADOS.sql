create table CONTAS_FUNC_AUTORIZADOS(
  CONTA_ID        varchar2(8) not null,
  FUNCIONARIO_ID  number(4) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table CONTAS_FUNC_AUTORIZADOS
add constraint PK_CONTAS_FUNC_AUTORIZADOS
primary key(CONTA_ID, FUNCIONARIO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONTAS_FUNC_AUTORIZADOS
add constraint FK_CONTAS_FUNC_AUTOR_CONTA_ID
foreign key(CONTA_ID)
references CONTAS(CONTA);

alter table CONTAS_FUNC_AUTORIZADOS
add constraint FK_CONTAS_FUNC_AUTOR_FUNC_ID
foreign key(FUNCIONARIO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);