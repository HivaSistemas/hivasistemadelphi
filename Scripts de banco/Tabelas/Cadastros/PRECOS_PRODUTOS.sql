create table PRECOS_PRODUTOS(
  PRODUTO_ID                  number(10) not null,
  EMPRESA_ID                  number(3) not null,

  PERC_MARGEM_PRECO_VAREJO    number(8,5) default 0 not null,
  PRECO_VAREJO                number(9,4) default 0 not null,
  QUANTIDADE_MIN_PRECO_VAREJO number(10,4) default 0 not null,
  DATA_PRECO_VAREJO           date default trunc(sysdate) not null,
  
  PERC_MARGEM_PRECO_PONTA_EST number(8,5) default 0 not null,
  PRECO_PONTA_ESTOQUE         number(9,4) default 0 not null,  
  DATA_PRECO_PONTA_ESTOQUE    date default trunc(sysdate) not null,
  
  PERC_MARGEM_PRECO_ATACADO_1 number(8,5) default 0 not null,
  PRECO_ATACADO_1             number(9,4) default 0 not null,
  QUANTIDADE_MIN_PRECO_ATAC_1 number(10,4) default 0 not null,
  DATA_PRECO_ATACADO_1        date default trunc(sysdate) not null,
  
  PERC_MARGEM_PRECO_ATACADO_2 number(8,5) default 0 not null,
  PRECO_ATACADO_2             number(9,4) default 0 not null,
  QUANTIDADE_MIN_PRECO_ATAC_2 number(10,4) default 0 not null,
  DATA_PRECO_ATACADO_2        date default trunc(sysdate) not null,
  
  PERC_MARGEM_PRECO_ATACADO_3 number(8,5) default 0 not null,
  PRECO_ATACADO_3             number(9,4) default 0 not null,  
  QUANTIDADE_MIN_PRECO_ATAC_3 number(10,4) default 0 not null,
  DATA_PRECO_ATACADO_3        date default trunc(sysdate) not null,
  
  PERC_MARGEM_PRECO_PDV       number(8,5) default 0 not null,
  PRECO_PDV                   number(9,4) default 0 not null,  
  QUANTIDADE_MINIMA_PDV       number(10,4) default 0 not null,
  DATA_PRECO_PDV              date default trunc(sysdate) not null,
  
  PERC_COMISSAO_A_VISTA       number(4,2) default 0 not null,
  PERC_COMISSAO_A_PRAZO       number(4,2) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null 
);

alter table PRECOS_PRODUTOS
add constraint PK_PRECOS_PRODUTOS
primary key(PRODUTO_ID, EMPRESA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PRECOS_PRODUTOS
add constraint FK_PRECOS_PRODUTOS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table PRECOS_PRODUTOS
add constraint FK_PRECOS_PRODUTOS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

/* Constraints */
alter table PRECOS_PRODUTOS
add constraint CK_PRECOS_PRECO_VAREJO
check(PRECO_VAREJO >= 0);

alter table PRECOS_PRODUTOS
add constraint CK_PRECOS_PRECO_PONTA_ESTOQUE
check(PRECO_PONTA_ESTOQUE >= 0);

alter table PRECOS_PRODUTOS
add constraint CK_PRECOS_PRECO_PRECO_ATACAD_1
check(PRECO_ATACADO_1 >= 0);

alter table PRECOS_PRODUTOS
add constraint CK_PRECOS_PRECO_PRECO_ATACAD_2
check(PRECO_ATACADO_2 >= 0);

alter table PRECOS_PRODUTOS
add constraint CK_PRECOS_PRECO_PRECO_ATACAD_3
check(PRECO_ATACADO_3 >= 0);

alter table PRECOS_PRODUTOS
add constraint CK_PRECOS_PRECO_PRECO_PDV
check(PRECO_PDV >= 0);

alter table PRECOS_PRODUTOS
add constraint CK_PRECOS_PROD_PERC_COM_VISTA
check(PERC_COMISSAO_A_VISTA >= 0);

alter table PRECOS_PRODUTOS
add constraint CK_PRECOS_PROD_PERC_COM_PRAZO
check(PERC_COMISSAO_A_PRAZO >= 0);

alter table PRECOS_PRODUTOS
add constraint CK_PREC_PROD_PERC_MARG_PRE_VAR
check(PERC_MARGEM_PRECO_VAREJO between -50 and 999.99999);

alter table PRECOS_PRODUTOS
add constraint CK_PREC_PROD_PERC_MAR_PRE_PT_E
check(PERC_MARGEM_PRECO_PONTA_EST between -50 and 999.99999);

alter table PRECOS_PRODUTOS
add constraint CK_PRE_PROD_PERC_M_PRECO_ATAC1
check(PERC_MARGEM_PRECO_ATACADO_1 between -50 and 999.99999);

alter table PRECOS_PRODUTOS
add constraint CK_PRE_PROD_PERC_M_PRECO_ATAC2
check(PERC_MARGEM_PRECO_ATACADO_2 between -50 and 999.99999);

alter table PRECOS_PRODUTOS
add constraint CK_PRE_PROD_PERC_M_PRECO_ATAC3
check(PERC_MARGEM_PRECO_ATACADO_3 between -50 and 999.99999);

alter table PRECOS_PRODUTOS
add constraint CK_PRECOS_PROD_PERC_MAR_PR_PDV
check(PERC_MARGEM_PRECO_PDV between -50 and 999.99999);

/* Indices */
create index IDX_PRECOS_PROD_EMPRESA_ID
on PRECOS_PRODUTOS
(EMPRESA_ID)
tablespace INDICES;

create index IDX_PRECOS_PROD_PRODUTO_ID
on PRECOS_PRODUTOS
(PRODUTO_ID)
tablespace INDICES;