create table LOGS_TELAS(
  FUNCIONARIO_ID      number(4),
  FORM                varchar2(100),
  DATA_HORA           date
);

/* Chave primaria */
alter table LOGS_TELAS
add constraint PK_LOGS_TELAS
primary key(FUNCIONARIO_ID, FORM, DATA_HORA)
using index tablespace INDICES;

/* Chave estrangeria */
alter table LOGS_TELAS
add constraint FK_LOGS_TELAS_FUNCIONARIO_ID
foreign key(FUNCIONARIO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);