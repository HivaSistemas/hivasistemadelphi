﻿create table PARAMETROS_EMPRESA(
  EMPRESA_ID                      number(3),
  
  TIPO_AMBIENTE_NFE               char(1) default 'H' not null,
  TIPO_AMBIENTE_NFCE              char(1) default 'H' not null,

  LOGO_EMPRESA                    blob,
  VALOR_MIN_DIF_TURNO_CONTAS_REC  number(8,2) default 0 not null,

  EXIRGIR_DADOS_CARTAO char(1) default 'S' not null,

  ULTIMO_ANO_MES_FINALIZADO       number(6),

	UTILIZA_NFE                     char(1) default 'N' not null,  
	SERIE_NFE                       number(2),
  NUMERO_ULT_NFE_EMITIDA  	      number(8) default 0 not null,
  
  UTILIZA_NFCE                    char(1) default 'N' not null,  
	SERIE_NFCE                      number(2),
  NUMERO_ULT_NFCE_EMITIDA  	      number(8) default 0 not null,
	
  EXIGIR_NOME_CONS_FINAL_VENDA    char(1) default 'N' not null,
  PERMITE_ALT_FORMA_PAGTO_CAIXA   char(1) default 'S' not null,
	PERM_ALT_FORMA_PAGTO_CX_FINANC  char(1) default 'S' not null,
	
  CONDICAO_PAGAMENTO_PDV          number(3),
  CONDICAO_PAGTO_PAD_VENDA_ASSIS  number(3),
  COND_PAGTO_PADRAO_PESQ_VENDA    number(3),

  ULTIMO_NSU_CONSULTADO_DIST_NFE  varchar2(15) default '0' not null,
  ULTIMO_NSU_CONSULTADO_DIST_CTE  varchar2(15) default '0' not null,
  
  COR_LUCRATIVIDADE_1		          number(10) default 8388608 not null,
	COR_LUCRATIVIDADE_2		          number(10) default 32768 not null,
	COR_LUCRATIVIDADE_3		          number(10) default 65280 not null,
  COR_LUCRATIVIDADE_4		          number(10) default 65535 not null,

  CONDICAO_PAGAMENTO_CONS_PROD_2  number(3),
  CONDICAO_PAGAMENTO_CONS_PROD_3  number(3),
  CONDICAO_PAGAMENTO_CONS_PROD_4  number(3),

  QTDE_DIAS_VALIDADE_ORCAMENTO    number(3) default 15 not null,
  OBRIGAR_VENDEDOR_SEL_TIPO_COBR  char(1) default 'N' not null,
  
  REGIME_TRIBUTARIO               char(2) default 'SN' not null,
  ALIQUOTA_SIMPLES_NACIONAL       number(4,2),
  INF_COMPL_DOCS_ELETRONICOS      varchar2(200),
  
  ALIQUOTA_PIS                    number(4,2) default 0 not null,
  ALIQUOTA_COFINS                 number(4,2) default 0 not null,
  ALIQUOTA_CSLL                   number(4,2) default 0 not null,
  ALIQUOTA_IRPJ                   number(4,2) default 0 not null,

  TRABALHA_RETIRAR_ATO            char(1) default 'S' not null,
  TRABALHA_RETIRAR                char(1) default 'S' not null,
  TRABALHA_ENTREGAR               char(1) default 'S' not null,
  TRABALHA_SEM_PREVISAO           char(1) default 'S' not null,
  TRABALHA_SEM_PREVISAO_ENTREGAR  char(1) default 'S' not null,

  CONFIRMAR_SAIDA_PRODUTOS        char(1) default 'S' not null,
  TRABALHAR_CONFERENCIA_RET_ATO   char(1) default 'N' not null,
  TRABALHAR_CONFERENCIA_RETIRAR   char(1) default 'N' not null,
  TRABALHAR_CONFERENCIA_ENTREGA   char(1) default 'N' not null,
  PERM_BAIXAR_ENT_REC_PENDENTE    char(1) default 'N' not null,

  INDICE_SABADO                   number(2,1) default 0.5 not null,
  INDICE_DOMINGO                  number(2,1) default 0 not null,
  QUANTIDADE_MIN_DIAS_ENTREGA     number(2) default 0 not null,

  TIPO_PRECO_UTILIZAR_VENDA       char(3) default 'APA' not null,
  TIPO_CUSTO_VIS_LUCRO_VENDA      char(3) default 'CMV' not null,
  TIPO_CUSTO_AJUSTE_ESTOQUE       char(3) default 'CMV' not null,

  PERMITIR_DEV_APOS_PRAZO         CHAR(1) DEFAULT 'N' NOT NULL,

  IMPRIMIR_LOTE_COMP_ENTREGA      char(1) default 'S' not null,
  IMPRIMIR_COD_ORIGINAL_COMP_ENT  char(1) default 'N' not null,
  IMPRIMIR_COD_BARRAS_COMP_ENT    char(1) default 'N' not null,
  TRABALHAR_CONTROLE_SEPARACAO    char(1) default 'N' not null,

  PERCENTUAL_JUROS_MENSAL         number(4,2) default 5 not null,
  PERCENTUAL_MULTA                number(4,2) default 2 not null,

  TIPO_REDIRECIONAMENTO_NOTA      char(3) default 'NRE' not null,
  EMPRESA_RED_NOTA_RETIRA_ATO_ID  number(3),
  EMPRESA_RED_NOTA_RETIRAR_ID     number(3),
  EMPRESA_RED_NOTA_ENTREGAR_ID    number(3),

  NUMERO_ESTABELECIMENTO_CIELO    varchar2(20),

  VALOR_FRETE_PADRAO_VENDA        number(8,2) default 0 not null,
  QTDE_DIAS_BLOQ_VENDA_TIT_VENC   number(3) default 15 not null,

  CALCULAR_FRETE_VENDA_KM         char(1) default 'N' not null,
  HABILITAR_RECEBIMENTO_PIX       char(1) default 'S' not null,

  /* Baixa de contas a receber via pix cair no turno aberto*/
  BX_CON_REC_PIX_SOM_TURN_ABERTO  char(1) default 'S' not null,
  INF_COMPL_COMP_PAGAMENTOS       varchar2(200),
  ALERTA_ENTREGA_PENDENTE         char(1) default 'N' not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

alter table PARAMETROS_EMPRESA
add constraint PK_PARAMETROS_EMPRESA
primary key(EMPRESA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PARAMETROS_EMPRESA
add constraint FK_PARAMETROS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table PARAMETROS_EMPRESA
add constraint FK_PAR_EMP_RED_NOTA_RET_ATO_ID
foreign key(EMPRESA_RED_NOTA_RETIRA_ATO_ID)
references EMPRESAS(EMPRESA_ID);

alter table PARAMETROS_EMPRESA
add constraint FK_PAR_EMP_RED_NOTA_RET_ID
foreign key(EMPRESA_RED_NOTA_RETIRAR_ID)
references EMPRESAS(EMPRESA_ID);

alter table PARAMETROS_EMPRESA
add constraint FK_PAR_EMP_RED_NOTA_ENTR_ID
foreign key(EMPRESA_RED_NOTA_ENTREGAR_ID)
references EMPRESAS(EMPRESA_ID);

alter table PARAMETROS_EMPRESA
add constraint FK_PAR_EMPRESA_COND_PAGTO_PDV
foreign key(CONDICAO_PAGAMENTO_PDV)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

alter table PARAMETROS_EMPRESA
add constraint FK_PAR_EMP_COND_PAGTO_PAD_V_AS
foreign key(CONDICAO_PAGTO_PAD_VENDA_ASSIS)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

alter table PARAMETROS_EMPRESA
add constraint FK_PAR_EMP_COND_PAGTO_PAD_PESQ
foreign key(COND_PAGTO_PADRAO_PESQ_VENDA)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

alter table PARAMETROS_EMPRESA
add constraint FK_PAR_EMP_COND_PAGTO_CON_PR_2
foreign key(CONDICAO_PAGAMENTO_CONS_PROD_2)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

alter table PARAMETROS_EMPRESA
add constraint FK_PAR_EMP_COND_PAGTO_CON_PR_3
foreign key(CONDICAO_PAGAMENTO_CONS_PROD_3)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

alter table PARAMETROS_EMPRESA
add constraint FK_PAR_EMP_COND_PAGTO_CON_PR_4
foreign key(CONDICAO_PAGAMENTO_CONS_PROD_4)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

/* Constraints */
alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_VAL_MIN_DIF_TUR_CR
check(VALOR_MIN_DIF_TURNO_CONTAS_REC >= 0);

alter table PARAMETROS_EMPRESA
add constraint CK_PARAMETROS_EMP_UTILIZA_NFE
check(UTILIZA_NFE in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PARAMETROS_EMP_SERIE_NFE
check(
  UTILIZA_NFE = 'N' and SERIE_NFE is null
	or
	UTILIZA_NFE = 'S' and nvl(SERIE_NFE, 0) > 0
);

alter table PARAMETROS_EMPRESA
add constraint CK_PARAMETROS_EMP_UTILIZA_NFCE
check(UTILIZA_NFCE in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PARAMETROS_EMP_SERIE_NFCE
check(
  UTILIZA_NFCE = 'N' and SERIE_NFCE is null
	or
	UTILIZA_NFCE = 'S' and nvl(SERIE_NFCE, 0) > 0
);

alter table PARAMETROS_EMPRESA
add constraint CK_PARAMETROS_EMP_NR_ULT_NFE
check(NUMERO_ULT_NOTA_EMITIDA >= 0);

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_EXIG_NOME_CONS_FIN
check(EXIGIR_NOME_CONS_FINAL_VENDA in('S', 'N'));
 
alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_PER_ALT_FORMA_PAGTO
check(PERMITE_ALT_FORMA_PAGTO_CAIXA in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_E_PER_ALT_FOR_PAG_CX_FI
check(PERM_ALT_FORMA_PAGTO_CX_FINANC in('S', 'N'));

/*
  TIPO_AMBIENTE_NFE
  H - Homologação
  P - Produção
*/
alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMPRESA_TP_AMBIENTE_NFE
check(TIPO_AMBIENTE_NFE in('H', 'P'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMPRESA_TP_AMBIENT_NFCE
check(TIPO_AMBIENTE_NFCE in('H', 'P'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_ULT_NSU_CONS_DI_NFE
check(length(ULTIMO_NSU_CONSULTADO_DIST_NFE) > 0);

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_ULT_NSU_CONS_DI_CTE
check(length(ULTIMO_NSU_CONSULTADO_DIST_CTE) > 0);


/* Regime tributário
  SN - Simples Nacional
  LR - Lucro Real
  LP - Lucro Presumido
*/
alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_REGIME_TRIBUTARIO
check(REGIME_TRIBUTARIO in('SN', 'LR', 'LP'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_ALIQ_SIMPLES_NACIO
check(
  (REGIME_TRIBUTARIO <> 'SN' and ALIQUOTA_SIMPLES_NACIONAL is null)
  or
  (REGIME_TRIBUTARIO = 'SN' and nvl(ALIQUOTA_SIMPLES_NACIONAL, 0) in(4.00, 7.30, 9.50, 10.70, 14.30, 19.00))
);

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_ALIQUOTAS_IMPOSTOS
check(
  ALIQUOTA_PIS >= 0 and
  ALIQUOTA_COFINS >= 0 and
  ALIQUOTA_CSLL >= 0 and
  ALIQUOTA_IRPJ >= 0
);

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMPRESA_TRAB_RETIRA_ATO
check(TRABALHA_RETIRAR_ATO in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMPRESA_TRABALHA_RETIRA
check(TRABALHA_RETIRAR in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMPRESA_TRABALHA_ENTREG
check(TRABALHA_ENTREGAR in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMPRESA_TRAB_SEM_PREV
check(TRABALHA_SEM_PREVISAO in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMPR_TRAB_SEM_PREV_ENT
check(TRABALHA_SEM_PREVISAO_ENTREGAR in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMPRESA_CONF_SAIDA_PROD
check(CONFIRMAR_SAIDA_PRODUTOS in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMPRESA_IMPR_LOTE_COMP_ENTR
check(IMPRIMIR_LOTE_COMP_ENTREGA in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_IMPR_COD_ORIG_CO_EN
check(IMPRIMIR_COD_ORIGINAL_COMP_ENT in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_IMPR_LOTE_CO_ENTR
check(IMPRIMIR_COD_BARRAS_COMP_ENT in('S', 'N'));

/*
  TIPO_PRECO_UTILIZAR_VENDA
  MEP - Utilizar o menor preço
  MAP - Utilizar o maior preço
  DMA - Definir manualmente
  APA - Apresentar preços automaticamente
*/
alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_TIPO_PRECO_UTI_VEND
check(TIPO_PRECO_UTILIZAR_VENDA in('MEP', 'MAP', 'DMA', 'APA'));

/*
  TIPO_CUSTO_VIS_LUCRO_VENDA
  FIN - Preço final
  COM - Preço de compra
  CMV - Custo médio
*/
alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_TP_CUS_VIS_LUC_VEN
check(TIPO_CUSTO_VIS_LUCRO_VENDA in('FIN', 'COM', 'CMV'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_TP_CUS_AJUST_EST
check(TIPO_CUSTO_AJUSTE_ESTOQUE in('FIN', 'COM', 'CMV'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMPRESA_INDICE_SABADO
check(INDICE_SABADO in(0, 0.5, 1));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMPRESA_INDICE_DOMINGO
check(INDICE_DOMINGO in(0, 0.5, 1));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_QTD_DIAS_VAL_ORC
check(QTDE_DIAS_VALIDADE_ORCAMENTO > 0);

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_OBR_VEN_SEL_TP_COBR
check(OBRIGAR_VENDEDOR_SEL_TIPO_COBR in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_PERC_JUROS_MENSAL
check(PERCENTUAL_JUROS_MENSAL between 0 and 99.99);

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_PERC_MULTA
check(PERCENTUAL_MULTA between 0 and 99.99);

/*
  TIPO_REDIRECIONAMENTO_NOTA
  NRE - Não redirecionar
  RPJ - Redicionar todas notas pessoas jurídicas
  TOD - Redirecionar todas as notas fiscasi
*/
alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_TIPO_REDIREC_NOTA
check(TIPO_REDIRECIONAMENTO_NOTA in('NRE', 'RPJ', 'TOD'));

alter table PARAEMTROS_EMPRESA
add constraint CK_TRABALHAR_CONT_SEPARACAO
check(TRABALHAR_CONTROLE_SEPARACAO in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_VLR_FRETE_PAD_VENDA
check(VALOR_FRETE_PADRAO_VENDA >= 0);

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_QTDE_MIN_DIAS_ENTR
check(QUANTIDADE_MIN_DIAS_ENTREGA >= 0);

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_TRAB_CONF_RET_ATO
check(TRABALHAR_CONFERENCIA_RET_ATO in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_TRAB_CONF_RETIRAR
check(TRABALHAR_CONFERENCIA_RETIRAR in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_TRAB_CONF_ENTREGAR
check(TRABALHAR_CONFERENCIA_ENTREGA in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_PER_BX_ENT_REC_PEND
check(PERM_BAIXAR_ENT_REC_PENDENTE in('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EM_QTD_DIAS_BLOQ_V_T_VE
check(QTDE_DIAS_BLOQ_VENDA_TIT_VENC >= 0);

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EXIR_DAD_CRT
check EXIRGIR_DADOS_CARTAOin('S', 'N'));

alter table PARAMETROS_EMPRESA
add constraint CK_PAR_EMP_CALC_FRETE_VENDA_KM
check(CALCULAR_FRETE_VENDA_KM in('S', 'N'));

//ALTER TABLE PARAMETROS_EMPRESA ADD PERMITIR_DEV_APOS_PRAZO CHAR(1) DEFAULT 'N' NOT NULL
