create table PLANOS_FINANCEIROS(
	PLANO_FINANCEIRO_ID             varchar2(9)	not null,
	DESCRICAO 			                varchar2(50) not null,
	PLANO_FINANCEIRO_PAI_ID         varchar2(9),
	ULTIMO_SUB_GRUPO		            number(3),
	TIPO						                char(1)	default 'D'	not null,
  EXIBIR_DRE                      char(1) default 'S' not null,
  PERMITE_ALTERACAO               char(1) default 'S' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table PLANOS_FINANCEIROS
add constraint PK_PLANOS_FINANCEIROS
primary key(PLANO_FINANCEIRO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PLANOS_FINANCEIROS
add constraint FK_PLA_FIN_PL_FINANCEIR_PAI_ID
foreign key(PLANO_FINANCEIRO_PAI_ID)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

/* Checagens */
alter table PLANOS_FINANCEIROS
add constraint PK_PLANOS_FIN_ULTIMO_SUB_GRUPO
check( ULTIMO_SUB_GRUPO >= 0 );

/*
  TIPO
  D - Despesas
  R - Receita
*/
alter table PLANOS_FINANCEIROS
add constraint CK_PLANOS_FINANCEIROS_TIPO
check( TIPO in('D', 'R') );

alter table PLANOS_FINANCEIROS
add constraint CK_PLANOS_FINANCEIR_EXIBIR_DRE
check( EXIBIR_DRE in('S', 'N') );

alter table PLANOS_FINANCEIROS
add constraint CK_PLANOS_FINAN_PERM_ALTERACAO
check( PERMITE_ALTERACAO in('S', 'N') );
