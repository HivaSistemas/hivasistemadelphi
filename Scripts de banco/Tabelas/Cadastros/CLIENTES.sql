﻿create table CLIENTES(
  CADASTRO_ID                        number(10),
  TIPO_CLIENTE                       char(2) default 'NC' not null,
  DATA_CADASTRO                      date default trunc(sysdate) not null,
	CODIGO_SPC					               varchar2(9),
	DATA_PESQUISA_SPC			             date,
	ATENDENTE_SPC				               varchar2(30),
	RESTRICAO_SPC				               char(1)	not null,
	DATA_RESTRICAO_SPC			           date,
	LOCAL_SPC					                 varchar2(40),
	DATA_PESQUISA_SERASA		           date,
	ATENDENTE_SERASA			             varchar2(30),
	RESTRICAO_SERASA			             char(1)	not null,
	DATA_RESTRICAO_SERASA		           date,
	LOCAL_SERASA				               varchar2(40),
  CONDICAO_ID                        number(3),
  REGIME_EMPRESA                     CHAR(2),
  VENDEDOR_ID                        number(4),
  EMITIR_SOMENTE_NFE                 char(1) default 'N' not null,
  NAO_GERAR_COMISSAO                 char(1) default 'N' not null,
  EXIGIR_MODELO_NOTA_FISCAL          char(1) default 'N' not null,
  IGNORAR_REDIR_REGRA_NOTA_FISC      char(1) default 'N' not null,

  LIMITE_CREDITO                     number(10,2) default 0 not null,
  LIMITE_CREDITO_MENSAL              number(10,2) default 0 not null,
  DATA_APROVACAO_LIMITE_CREDITO date,
  USUARIO_APROV_LIMITE_CRED_ID       number(4),

  EXIGIR_NOTA_SIMPLES_FAT            char(1) default 'N' not null,
  TIPO_PRECO                         char(1) default 'V' not null,
  QTDE_DIAS_VENCIMENTO_ACUMULADO     number(3) default 15 not null,

  DATA_VALIDADE_LIMITE_CREDITO       date,

  INDICE_DESCONTO_VENDA_ID           number(1),
  ATIVO                              char(1) default 'S' not null,

  LISTA_NEGRA_ID                     number(3),
  MOTIVO_LISTA_NEGRA                 varchar2(200),
  IGNORAR_BLOQUEIOS_VENDA            char(1) default 'N' not null,

  E_CLIENTE_ALTIS                    char(1) default 'N' not null,
  VERSAO_MINIMA_ALTIS                number(10,0),
  MOTIVO_NOVA_VERSAO                 varchar2(400),
  
  PROTESTO_AUTOMATICO                char(1) default 'S'  not null,
  EXIGE_PEDIDO_COMPRA                char(1) default 'N'  not null,
  PERMITE_RECEBER_NA_ENTREGA         char(1) default 'N'  not null,
  PERMITE_CHEQUE_AVISTA              char(1) default 'N'  not null,
  PERMITE_CHEQUE_APRAZO              char(1) default 'N'  not null,
  PERMITE_DUPLICATA                  char(1) default 'N'  not null,
  PERMITE_BOLETO                     char(1) default 'N'  not null,
  EMBUTIR_FRETE_NO_ITEM              char(1) default 'S'  not null,
  MOTIVO_DO_CREDITO                  varchar2(200),
  ORIGEM_NOTA_DEVOLUCAO              char(1) default 'E'  not null,
  MENSAGEM_AVISO_VENDA               varchar2(200),
  MENSAGEM_AVISO_CAIXA               varchar2(200),
  NOME_MAE                           varchar2(50),
  NOME_PAI                           varchar2(50),
  NOME_CONJUGE                       varchar2(50),
  CLIENTE_ORIGEM_ID                  number(6,0),
  CLIENTE_GRUPO_ID                   number(4,0),
  GRUPO_FINANCEIRO_ID                number(4,0), 
  USUARIO_BANCO    	 	     varchar2(50),
  SENHA_BANCO_DADOS    		     varchar2(50),
  IP_SERVIDOR     		     varchar2(50),
  SV_BANCO_DADOS     		     varchar2(50),
  QTD_ESTACOES    		     varchar2(50),
  RESP_EMPRESA  		     varchar2(50),
  VL_ADESAO    			     varchar2(50),
  VL_MENSAL 			     varchar2(10),
  DATA_ADESAO	  		     DATE,
  DATA_VENC_MENSAL   		 varchar2(10), 
  CALCULAR_INDICE_DED        char(1) default 'N'  not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID                  number(4) not null,
  DATA_HORA_ALTERACAO                date not null,
  ROTINA_ALTERACAO                   varchar2(30) not null,
  ESTACAO_ALTERACAO                  varchar2(30) not null
);

/* Chave primaria */
alter table CLIENTES
add constraint PK_CLIENTES_CADASTRO_ID
primary key(CADASTRO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CLIENTES
add constraint FK_CLIENTES_CLIENTE_ORIGEM_ID
foreign key(CLIENTE_ORIGEM_ID)
references CLIENTES_ORIGENS(CLIENTE_ORIGEM_ID);

alter table CLIENTES
add constraint FK_CLIENTES_CLIENTE_GRUPO_ID
foreign key(CLIENTE_GRUPO_ID)
references CLIENTES_GRUPOS(CLIENTE_GRUPO_ID);

alter table CLIENTES
add constraint FK_CLIENTES_GRUPO_FINANC_ID
foreign key(GRUPO_FINANCEIRO_ID)
references GRUPOS_FINANCEIROS(GRUPO_FINANCEIRO_ID);

alter table CLIENTES
add constraint FK_CLIENTES_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);

alter table CLIENTES
add constraint FK_CLIENTES_CONDICAO_ID
foreign key(CONDICAO_ID)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

alter table CLIENTES
add constraint FK_CLIENTES_VENDEDOR_ID
foreign key(VENDEDOR_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table CLIENTES
add constraint FK_CLIE_USU_APROV_LIM_CRED_ID
foreign key(USUARIO_APROV_LIMITE_CRED_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table CLIENTES
add constraint FK_CLIENTES_IND_DESC_VENDA_ID
foreign key(INDICE_DESCONTO_VENDA_ID)
references INDICES_DESCONTOS_VENDA(INDICE_ID);

alter table CLIENTES
add constraint FK_CLIENTES_LISTA_NEGRA_ID
foreign key(LISTA_NEGRA_ID)
references LISTA_NEGRA(LISTA_ID);


/* Checagens */
/* TIPO_CLIENTE 
  NC - Não contribuinte
  CO - Contribuiente
  OP - Orgão público
  RE - Revenda
  CT - Construtora
  CH - Clínica / Hospital
  PR - Produtor rural
*/
alter table CLIENTES
add constraint CK_CLIENTES_TIPO_CLIENTE
check (TIPO_CLIENTE in('NC','CO','OP','RE','CT','CH','PR'));

alter table CLIENTES
add constraint CK_CLIENTES_RESTRICAO_SPC
check (RESTRICAO_SPC in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_RESTRICAO_SERASA
check (RESTRICAO_SERASA in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_EMITIR_SOMENTE_NFE
check (EMITIR_SOMENTE_NFE in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_NAO_GERAR_COMISSAO
check (NAO_GERAR_COMISSAO in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_ATIVO
check (ATIVO in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_EXIGIR_MOD_NOTA_FI
check (EXIGIR_MODELO_NOTA_FISCAL in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_IGN_RED_REGRA_NF
check (IGNORAR_REDIR_REGRA_NOTA_FISC in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_PROTESTO_AUTOM
check (PROTESTO_AUTOMATICO in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_EX_PED_COMPRA
check (EXIGE_PEDIDO_COMPRA in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_PERMITE_REC_ENTR
check (PERMITE_RECEBER_NA_ENTREGA in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_PERMITE_CHQ_AVISTA
check (PERMITE_CHEQUE_AVISTA in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_PERMITE_CHQ_APRAZO
check (PERMITE_CHEQUE_APRAZO in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_PERMITE_DUPLICATA
check (PERMITE_DUPLICATA in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_PERMITE_BOLETO
check (PERMITE_BOLETO in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_EMB_FRETE_NO_ITEM
check (EMBUTIR_FRETE_NO_ITEM in('S','N'));

alter table CLIENTES
add constraint CK_CLIENTES_ORIGEM_NOTA_DEVOL
check (ORIGEM_NOTA_DEVOLUCAO in('E','C'));
/* E- EMPRESA
   C- CLIENTE*/
alter table CLIENTES
add constraint CK_CLIENTES_LIMITE_CREDITO
check(LIMITE_CREDITO >= 0);

alter table CLIENTES
add constraint CK_CLIENTES_LIM_CREDITO_MENSAL
check(LIMITE_CREDITO_MENSAL >= 0 and LIMITE_CREDITO_MENSAL <= LIMITE_CREDITO);

alter table CLIENTES
add constraint CK_CLI_DATA_APROV_LIMITE_CRED
check(
  DATA_APROVACAO_LIMITE_CREDITO is null and USUARIO_APROV_LIMITE_CRED_ID is null and LIMITE_CREDITO = 0
  or
  DATA_APROVACAO_LIMITE_CREDITO is not null and USUARIO_APROV_LIMITE_CRED_ID is not null and LIMITE_CREDITO > 0
);

alter table CLIENTES
add constraint CK_CLI_EXIGIR_NOTA_SIMPLES_FAT
check(EXIGIR_NOTA_SIMPLES_FAT in('S', 'N'));

/* 
  TIPO_PRECO
  V - Varejo
  A - Atacado
  T - Todos, Varejo e atacado
*/
alter table CLIENTES
add constraint CK_CLIENTES_TIPO_PRECO
check(TIPO_PRECO in('V', 'A', 'T'));

alter table CLIENTES
add constraint CK_CLIENTES_DT_VAL_LIM_CRE
check(
  (DATA_VALIDADE_LIMITE_CREDITO is null)
  or
  (DATA_VALIDADE_LIMITE_CREDITO is not null and LIMITE_CREDITO + LIMITE_CREDITO_MENSAL > 0 )
);

alter table CLIENTES
add constraint CK_CLI_QTDE_DIAS_VENCTO_ACUM
check(QTDE_DIAS_VENCIMENTO_ACUMULADO between 0 and 999);

alter table CLIENTES
add constraint CK_CLIENTES_E_CLIENTE_ALTIS
check(
  E_CLIENTE_ALTIS = 'N' and VERSAO_MINIMA_ALTIS is null and MOTIVO_NOVA_VERSAO is null
  or
  E_CLIENTE_ALTIS = 'S'
);

alter table CLIENTES
add constraint CK_CLIENTES_LISTA_NEGRA
check(
  LISTA_NEGRA_ID is null and MOTIVO_LISTA_NEGRA is null
  or
  LISTA_NEGRA_ID is not null and MOTIVO_LISTA_NEGRA is not null
);

alter table CLIENTES
add constraint CK_CLIENTES_IGNORAR_BLOQ_VENDA
check(IGNORAR_BLOQUEIOS_VENDA in('S', 'N'));


/*
  ALTER TABLE CLIENTES ADD REGIME_EMPRESA CHAR(2)
*/
