﻿create table PORTADORES(
  PORTADOR_ID            varchar2(50) not null,
  DESCRICAO              varchar2(80) not null,
  ATIVO                  char(1) default 'S' not null,
  TIPO                   char(1) not null,
  INCIDENCIA_FINANCEIRO  char(1) not null,
  INSTRUCAO_BOLETO       varchar2(400),
  EXIGE_NOTA_MOD_55      char(1) default 'N' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table PORTADORES
add constraint PK_PORTADORES
primary key(PORTADOR_ID)
using index tablespace INDICES;

/* Checagens */
alter table PORTADORES
add constraint CK_PORTADORES_ATIVO
check(ATIVO in('S', 'N'));

/* 
  TIPO 
  C - Carteira
  B - Bancária
*/
alter table PORTADORES
add constraint CK_PORTADORES_TIPO
check(TIPO in('C', 'B'));

/* 
  INCIDENCIA_FINANCEIRA 
  R - Contas a receber
  P - Contas a pagar
  A - Ambos
*/
alter table PORTADORES
add constraint CK_PORTADORES_INC_FINANCEIRO
check(INCIDENCIA_FINANCEIRO in('R', 'P', 'A'));
