create table PRODUTOS_KIT(
  PRODUTO_KIT_ID     number(10),
  PRODUTO_ID         number(10),
  ORDEM              number(2),
  PERC_PARTICIPACAO  number(8,5),
  QUANTIDADE         number(20,4),
 
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table PRODUTOS_KIT
add constraint PK_PRODUTOS_KIT
primary key(PRODUTO_KIT_ID, PRODUTO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PRODUTOS_KIT
add constraint FK_PRODUTOS_KIT_PRODUTO_KIT_ID
foreign key(PRODUTO_KIT_ID)
references PRODUTOS(PRODUTO_ID);

alter table PRODUTOS_KIT
add constraint FK_PRODUTOS_KIT_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* Constraints */
alter table PRODUTOS_KIT
add constraint CK_PRODUTOS_KIT_PERC_PARTICIPA
check(PERC_PARTICIPACAO > 0);

alter table PRODUTOS_KIT
add constraint CK_PRODUTOS_KIT_QUANTIDADE
check(QUANTIDADE > 0);

alter table PRODUTOS_KIT
add constraint CK_PRODUTOS_ORDEM
check(ORDEM > 0);