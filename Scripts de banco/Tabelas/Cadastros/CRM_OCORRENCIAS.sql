create table CRM_OCORRENCIAS(
  OCORRENCIA_ID  number(10),
  CADASTRO_ID    number(10),
  DESCRICAO      clob,
  SOLUCAO        clob,
  STATUS         char(2),
  DATA_CADASTRO  date not null,
  DATA_CONCLUSAO date,
  USUARIO        number(4),
  SOLICITACAO    number(10)
);

alter table CRM_OCORRENCIAS
add constraint PK_CRM_OCORRENCIAS
primary key(OCORRENCIA_ID)
using index tablespace INDICES;

alter table CRM_OCORRENCIAS
add constraint FK_CRM_OCO_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);
