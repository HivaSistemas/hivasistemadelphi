create table CRM_ACESSO_REMOTO(
  CENTRAL_ID    number(10),
  ITEM_ID       number(10),
  IP            varchar2(50),
  SENHA         varchar2(50),
  OBSERVACAO    varchar2(200),
  USUARIO       varchar2(50)
);

alter table CRM_ACESSO_REMOTO
add constraint FK_CRM_ACE_REMOTO_CENTRAL_ID
foreign key(CENTRAL_ID)
references CRM(CENTRAL_ID);
