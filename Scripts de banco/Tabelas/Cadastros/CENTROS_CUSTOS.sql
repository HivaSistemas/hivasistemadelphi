create table CENTROS_CUSTOS(
  CENTRO_CUSTO_ID number(3) not null,
  NOME            varchar2(30) not null,
  ATIVO           char(1) default 'S' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table CENTROS_CUSTOS
add constraint PK_CENTROS_CUSTOS
primary key(CENTRO_CUSTO_ID)
using index tablespace INDICES;

/* Checagens */
alter table CENTROS_CUSTOS
add constraint CK_CENTROS_CUSTOS_ATIVO
check(ATIVO in('S', 'N'));