create table ENDERECO_EST_VAO(
  VAO_ID                  number(3),
  DESCRICAO               varchar2(30) not null
);

/* Chave prim�ria */
alter table ENDERECO_EST_VAO
add constraint PK_END_EST_MOD_VAO_ID
primary key(VAO_ID)
using index tablespace INDICES;
