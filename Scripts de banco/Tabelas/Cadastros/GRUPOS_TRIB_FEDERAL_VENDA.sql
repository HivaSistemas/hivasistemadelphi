﻿create table GRUPOS_TRIB_FEDERAL_VENDA(
  GRUPO_TRIB_FEDERAL_VENDA_ID   number(10) not null,  	
  DESCRICAO                     varchar2(60) not null,
  ORIGEM_PRODUTO                number(1) not null,
  CST_PIS                       char(2) not null,
  CST_COFINS                    char(2) not null,
  ATIVO                         char(1) default 'S' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID             number(4) not null,
  DATA_HORA_ALTERACAO           date not null,
  ROTINA_ALTERACAO              varchar2(30) not null,
  ESTACAO_ALTERACAO             varchar2(30) not null  
);

/* Chave primária */
alter table GRUPOS_TRIB_FEDERAL_VENDA
add constraint PK_GRUPOS_TRIB_FEDERAL_VENDA
primary key(GRUPO_TRIB_FEDERAL_VENDA_ID)
using index tablespace INDICES;

/* Constraints */
alter table GRUPOS_TRIB_FEDERAL_VENDA
add constraint CK_GRUPOS_TRIB_FED_VENDA_ATIVO
check(ATIVO in('S', 'N'));

/*
  ORIGEM_PRODUTO
  0 - Nacional
  1 - Estrangeira - Importação direta, exceto a indicada no código 6
  2 - Estrangeira - Adquirida no mercado interno, exceto a indicada no código 7
  3 - Nacional, mercadoria ou bem com Conteúdo de Importação superior a 40% (quarenta por cento)
  4 - Nacional, cuja produção tenha sido feita em conformidade com os processos produtivos básicos de que tratam o Decreto-Lei nº 288/67, e as Leis nºs 8.248/91, 8.387/91, 10.176/01 e 11 . 4 8 4 / 0 7
  5 - Nacional, mercadoria ou bem com Conteúdo de Importação inferior ou igual a 40% (quarenta por cento)
  6 - Estrangeira - Importação direta, sem similar nacional, constante em lista de Resolução CAMEX
  7 - Estrangeira - Adquirida no mercado interno, sem similar nacional, constante em lista de Resolução CAMEX”
  8 – Nacional, mercadoria ou bem com Conteúdo de Importação superior a 70% (setenta por cento)
*/
alter table GRUPOS_TRIB_FEDERAL_VENDA
add constraint CK_GRUPOS_TRIB_FED_VEN_ORIG_PR
check(ORIGEM_PRODUTO in(0, 1, 2, 3, 4, 5, 6, 7, 8));

alter table GRUPOS_TRIB_FEDERAL_VENDA
add constraint CK_GRU_TRIB_FED_VENDA_CST_PIS
check(CST_PIS in('01','02','03','04','05','06','07','08','09','49','50','51','52','53','54','55','56','60','61','62','63','64','65','66','67','70','71','72','73','74','75','98','99'));

alter table GRUPOS_TRIB_FEDERAL_VENDA
add constraint CK_GRU_TRIB_FED_VENDA_CST_COF
check(CST_COFINS in('01','02','03','04','05','06','07','08','09','49','50','51','52','53','54','55','56','60','61','62','63','64','65','66','67','70','71','72','73','74','75','98','99'));