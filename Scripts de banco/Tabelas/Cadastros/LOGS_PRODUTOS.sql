create table LOGS_PRODUTOS(
  TIPO_ALTERACAO_LOG_ID   number(4) not null,
  PRODUTO_ID              number(10) not null,
  VALOR_ANTERIOR          varchar2(300),
  NOVO_VALOR              varchar2(300),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave estrangeira */
alter table LOGS_PRODUTOS
add constraint FK_LOGS_PRODUTOS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);