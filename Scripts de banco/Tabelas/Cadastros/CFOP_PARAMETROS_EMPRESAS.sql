create table CFOP_PARAMETROS_FISCAIS_EMPR(
  EMPRESA_ID              number(3) not null,
  CFOP_ID                 varchar2(7) not null,
  CALCULAR_PIS_COFINS     char(1) default 'S' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chaves estrangeiros */
alter table CFOP_PARAMETROS_FISCAIS_EMPR
add constraint PK_CFOP_PARAMETROS_FISCAIS_EMP
primary key(EMPRESA_ID, CFOP_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CFOP_PARAMETROS_FISCAIS_EMPR
add constraint FK_CFOP_PAR_EMPR_FISC_EMPR_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table CFOP_PARAMETROS_FISCAIS_EMPR
add constraint FK_CFOP_PAR_EMPR_FISC_CFOP_ID
foreign key(CFOP_ID)
references CFOP(CFOP_PESQUISA_ID);

/* Checagens */
alter table CFOP_PARAMETROS_FISCAIS_EMPR
add constraint CK_CFOP_PAR_EM_FIS_CAL_PIS_COF
check(CALCULAR_PIS_COFINS in('S', 'N'));