create table CONDICOES_CARTAO_CREDITO_PDV(
  EMPRESA_ID          number(3) not null,
  CONDICAO_ID         number(3) not null,
  ORDEM               number(1) not null,
  QUANTIDADE_PARCELAS number(2) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table CONDICOES_CARTAO_CREDITO_PDV
add constraint PK_COND_CARTAO_CREDITO_PDV
primary key(EMPRESA_ID, CONDICAO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONDICOES_CARTAO_CREDITO_PDV
add constraint FK_COND_CAR_CRED_PDV_EMPR_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table CONDICOES_CARTAO_CREDITO_PDV
add constraint FK_COND_CAR_CRED_PDV_COND_ID
foreign key(CONDICAO_ID)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

/* Checagens */
alter table CONDICOES_CARTAO_CREDITO_PDV
add constraint CK_COND_CAR_CRE_PDV_ORDEM
check(ORDEM > 0);

alter table CONDICOES_CARTAO_CREDITO_PDV
add constraint CK_COND_CAR_CRE_PDV_QTDE_PARC
check(QUANTIDADE_PARCELAS > 0);