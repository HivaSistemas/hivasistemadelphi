create table CARGOS_FUNCIONARIOS(
  CARGO_ID  number(3),
  NOME      varchar(30) not null,
  ATIVO     char(1) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table CARGOS_FUNCIONARIOS
add constraint PK_CARGOS_FUNCIONARIOS
primary key(CARGO_ID)
using index tablespace INDICES;

/* Checagens */
alter table CARGOS_FUNCIONARIOS
add constraint CK_CARGOS_FUNCIONARIOS_ATIVO
check(ATIVO in('S', 'N'));

alter table CARGOS_FUNCIONARIOS
add constraint UN_CARGOS_FUNCIONARIOS_NOME
unique(NOME);