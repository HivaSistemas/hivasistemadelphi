create table COLUNAS_PADROES_TELAS_USUARIO(
  USUARIO_ID      number(4) not null,
  TELA            varchar2(100) not null,
  CAMPO           varchar2(100) not null,
  COLUNA		  varchar2(200) not null,
  TAMANHO		  number(4) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table COLUNAS_PADROES_TELAS_USUARIO
add constraint PK_COL_PADROES_TELAS_USUARIO
primary key(USUARIO_ID, TELA, CAMPO, COLUNA)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table COLUNAS_PADROES_TELAS_USUARIO
add constraint FK_COL_PAD_TELAS_USUARIO_ID
foreign key(USUARIO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);