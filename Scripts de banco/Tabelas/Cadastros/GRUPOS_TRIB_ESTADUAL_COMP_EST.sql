create table GRUPOS_TRIB_ESTADUAL_COMP_EST(
  GRUPO_TRIB_ESTADUAL_COMPRA_ID number(10) not null,  
	ESTADO_ID                     char(2) not null,	  
  
  CST_REVENDA                   varchar2(2) not null,
  CST_DISTRIBUIDORA             varchar2(2) not null,
  CST_INDUSTRIA                 varchar2(2) not null,

  INDICE_REDUCAO_BASE_ICMS      number(5,4) default 1 not null,
  IVA                           number(4,2) default 0 not null,
  PRECO_PAUTA                   number(8,2) default 0 not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID             number(4) not null,
  DATA_HORA_ALTERACAO           date not null,
  ROTINA_ALTERACAO              varchar2(30) not null,
  ESTACAO_ALTERACAO             varchar2(30) not null  
);

/* Chave primária */
alter table GRUPOS_TRIB_ESTADUAL_COMP_EST
add constraint PK_GRUPOS_TRIB_ESTAD_COMP_EST
primary key(GRUPO_TRIB_ESTADUAL_COMPRA_ID, ESTADO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table GRUPOS_TRIB_ESTADUAL_COMP_EST
add constraint FK_GRU_TRI_EST_COM_G_T_E_C_ID
foreign key(GRUPO_TRIB_ESTADUAL_COMPRA_ID)
references GRUPOS_TRIB_ESTADUAL_COMPRA(GRUPO_TRIB_ESTADUAL_COMPRA_ID);

alter table GRUPOS_TRIB_ESTADUAL_COMP_EST
add constraint FK_GRUPOS_TRIB_EST_COM_EST_ID
foreign key(ESTADO_ID)
references ESTADOS(ESTADO_ID);

/* Constraints */
alter table GRUPOS_TRIB_ESTADUAL_COMP_EST
add constraint CK_GRU_TRI_EST_COMP_EST_CST_RV
check(CST_REVENDA in('00','10','20','30','40','41','50','51','60','70','90'));

alter table GRUPOS_TRIB_ESTADUAL_COMP_EST
add constraint CK_GRU_TRI_EST_COMP_EST_CST_DT
check(CST_DISTRIBUIDORA in('00','10','20','30','40','41','50','51','60','70','90'));

alter table GRUPOS_TRIB_ESTADUAL_COMP_EST
add constraint CK_GRU_TRI_EST_COMP_EST_CST_IN
check(CST_INDUSTRIA in('00','10','20','30','40','41','50','51','60','70','90'));

alter table GRUPOS_TRIB_ESTADUAL_COMP_EST
add constraint CK_GRU_T_EST_COMP_EST_IND_R_BI
check(INDICE_REDUCAO_BASE_ICMS between 0.0001 and 1);