create table LISTA_NEGRA(
  LISTA_ID    number(3) not null,
  NOME        varchar2(60) not null,
  ATIVO       char(1) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chaves primarias */
alter table LISTA_NEGRA
add constraint PK_LISTA_NEGRA
primary key(LISTA_ID)
using index tablespace INDICES;

/* Checagens */
alter table LISTA_NEGRA
add constraint CK_LISTA_NEGRA_ATIVO
check(ATIVO in('S', 'N'));