create table BAIRROS(
  BAIRRO_ID number(15) not null,
  NOME      varchar2(60) not null,
  CIDADE_ID number(15) not null,
  ROTA_ID   number(3),
  ATIVO     char(1) default 'S' not null,

  CHAVE_IMPORTACAO  varchar2(100),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave prim�ria */
alter table BAIRROS
add constraint PK_BAIRROS_ID
primary key(BAIRRO_ID)
using index tablespace INDICES;

/* Chaves entrangeiras */
alter table BAIRROS
add constraint FK_BAIRROS_CIDADE_ID
foreign key(CIDADE_ID)
references CIDADES(CIDADE_ID);

alter table BAIRROS
add constraint FK_BAIRROS_ROTA_ID
foreign key(ROTA_ID)
references ROTAS(ROTA_ID);

/* Checagens */
alter table BAIRROS
add constraint CK_BAIRROS_ATIVO
check(ATIVO in('S', 'N'));

/* Unique */
alter table BAIRROS
add constraint CK_BAIRROS_NOME_CIDADE_ID
unique(NOME, CIDADE_ID);