﻿create table FERIADOS(
  EMPRESA_ID  number(3) not null,
  DATA        date not null,
  INDICE_DIA  number(2,1) default 0 not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table FERIADOS
add constraint PK_FERIADOS
primary key(EMPRESA_ID, DATA)
using index tablespace INDICES;

/* Chaves estrageiras */
alter table FERIADOS
add constraint FK_FERIADOS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

/* Checagens */
alter table FERIADOS
add constraint CK_FERIADOS_INDICE_DIA
check(INDICE_DIA in(0, 0.5, 1));

alter table FERIADOS
add constraint CK_FERIADOS_DATA
check(DATA = trunc(DATA));