create table AUTORIZACOES(
  FUNCIONARIO_ID number(4) not null,
  TIPO           char(1) not null,
  ID             varchar2(100) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table AUTORIZACOES
add constraint CK_AUTORIZACOES
primary key(FUNCIONARIO_ID, TIPO, ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table AUTORIZACOES
add constraint FK_AUTORIZACOES_FUNCIONARIO_ID
foreign key(FUNCIONARIO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* Constraints */

/* 
  TIPO
  T - Tela
  R - Rotina
*/
alter table AUTORIZACOES
add constraint CK_AUTORIZACOES_TIPO
check(TIPO in('T', 'R'));

