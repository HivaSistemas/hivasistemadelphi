create table EMPRESAS(
  EMPRESA_ID                   number(3),
  RAZAO_SOCIAL                 varchar2(60) not null,
  NOME_FANTASIA                varchar2(60) not null,
  NOME_RESUMIDO_IMPRESSOES_NF  varchar2(30),
  CNPJ                         varchar2(19) not null,
  LOGRADOURO                   varchar2(100) not null,
  COMPLEMENTO                  varchar2(100) not null,
  NUMERO                       varchar2(10) default 'SN' not null,
  BAIRRO_ID                    number(15) not null,
  PONTO_REFERENCIA             varchar2(100),
  CEP                          varchar2(9) not null,
  TELEFONE_PRINCIPAL           varchar2(15),
  TELEFONE_FAX                 varchar2(15),
  E_MAIL                       varchar2(30),
  DATA_FUNDACAO                date,
  INSCRICAO_ESTADUAL           varchar2(20) not null,
  INSCRICAO_MUNICIPAL          varchar2(20),
  CADASTRO_ID                  number(10),
  CNAE                         varchar(12),
  TIPO_EMPRESA                 char(1) default 'M' not null,
  ATIVO                        char(1) default 'S' not null,
  CNPJ_ESCRITORIO_CONTADOR     varchar2(18),
  CPF_CONTADOR                 varchar2(18),
  DATA_EMISSAO_CRC_CONTADOR    date,
  DATA_VALIDADE_CRC_CONTADOR   date,
  E_MAIL_CONTADOR              varchar2(80),
  LOGRADOURO_CONTADOR          varchar2(50),
  CEP_CONTADOR                 char(9),
  COMPLEMENTO_CONTADOR         varchar2(50),
  BAIRRO_ID_CONTADOR           number(15,0) ,
  NOME_CONTADOR                varchar2(50),
  TELEFONE_CONTADOR            varchar2(20),
  NR_CONTR_EMIS_CRC_CONTADOR   varchar2(15),
  NUMERO_CRC_CONTADOR          varchar2(30),
  NUMERO_JUNTA_COMERCIAL       varchar2(40),
  NOME_ADMINISTRADOR           varchar2(50),
  FAX_CONTADOR                 varchar2(20),
  CPF_ADMINISTRADOR            varchar2(14),
  E_MAIL_NFE                   varchar2(80),
  SITE                         varchar2(50)
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave prim�ria */
alter table EMPRESAS
add constraint PK_EMPRESAS_EMPRESA_ID
primary key(EMPRESA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table EMPRESAS
add constraint FK_EMPRESAS_BAIRRO_ID
foreign key(BAIRRO_ID)
references BAIRROS(BAIRRO_ID);

alter table EMPRESAS
add constraint FK_EMPRESAS_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);

/* Constraints */
alter table EMPRESAS
add constraint CK_EMPRESAS_ATIVO
check(ATIVO in('S', 'N'));

/*
  TIPO_EMPRESA
  M - Matriz
  F - Filial
  D - Deposito fechado
  C - CD
*/
alter table EMPRESAS
add constraint CK_EMPRESAS_TIPO_EMPRESA
check(TIPO_EMPRESA in('M', 'F', 'D', 'C'));

alter table EMPRESAS
add constraint CK_EMPRESAS_CNPJ
unique(CNPJ);