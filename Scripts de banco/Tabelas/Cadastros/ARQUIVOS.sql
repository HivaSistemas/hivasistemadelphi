create table ARQUIVOS(
  ARQUIVO_ID              number(20) not null,
      
  USUARIO_CADASTRO_ID     number(4) not null,
  DATA_HORA_CADASTRO      date not null,
      
  NOME_ARQUIVO            varchar2(200) not null,
  ARQUIVO                 blob,
      
  ORIGEM                  char(3) not null,
  
  CLIENTE_ID              number(10),
  FORNECEDOR_ID           number(10),
  CADASTRO_ID             number(10),
  ORCAMENTO_ID            number(10),
  OCORRENCIA_ID           number(10),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table ARQUIVOS
add constraint PK_ARQUIVOS
primary key(ARQUIVO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ARQUIVOS
add constraint FK_ARQUIVOS_USUARIO_CAD_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(CADASTRO_ID);

alter table ARQUIVOS
add constraint FK_ARQUIVOS_CLIENTE_ID
foreign key(CLIENTE_ID)
references CLIENTES(CADASTRO_ID);

alter table ARQUIVOS
add constraint FK_ARQUIVOS_FORNECEDOR_ID
foreign key(FORNECEDOR_ID)
references FORNECEDORES(CADASTRO_ID);

alter table ARQUIVOS
add constraint FK_ARQUIVOS_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);

alter table ARQUIVOS
add constraint FK_ARQUIVOS_OCORRENCIA_ID
foreign key(OCORRENCIA_ID)
references CRM_OCORRENCIAS(OCORRENCIA_ID);

alter table ARQUIVOS
add constraint FK_ARQUIVOS_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

/* Checagens */
alter table ARQUIVOS
add constraint CK_ARQUIVOS_ORIGEM
check(
  ORIGEM = 'CLI' and CLIENTE_ID is not null
  or
  ORIGEM = 'FOR' and FORNECEDOR_ID is not null
  or
  ORIGEM = 'CAD' and CADASTRO_ID is not null
  or
  ORIGEM = 'ORC' and ORCAMENTO_ID is not null
  or
  ORIGEM = 'CRM' and OCORRENCIA_ID is not null
);