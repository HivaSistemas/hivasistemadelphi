create table ETIQUETAS_CONFIGURACOES_TELA(
  EMPRESA_ID            number(3) not null,
  CONDICAO_ID 					number(3),
  TIPO_CODIGO_IMPRESSAO char(1) not null,
  CAMINHO_IMPRESSAO     varchar2(100),
  MODELO_ID             number(2) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID     number(4) not null,
  DATA_HORA_ALTERACAO   date not null,
  ROTINA_ALTERACAO      varchar2(30) not null,
  ESTACAO_ALTERACAO     varchar2(30) not null
);

/* Chave prim�ria */
alter table ETIQUETAS_CONFIGURACOES_TELA
add constraint PK_ETIQUETAS_CONFIG_TELA
primary key(EMPRESA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ETIQUETAS_CONFIGURACOES_TELA
add constraint FK_ETIQUETAS_CONF_TELA_EMP_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table ETIQUETAS_CONFIGURACOES_TELA
add constraint FK_ETIQUETAS_CONF_TELA_COND_ID
foreign key(CONDICAO_ID)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

/* Constraints */
/*
  TIPO_EMPRESA
  A - Hiva
  F - Fornecedor
*/
alter table ETIQUETAS_CONFIGURACOES_TELA
add constraint CK_TIPO_CODIGO_IMPRESSAO
check(TIPO_CODIGO_IMPRESSAO in('A', 'F'));

alter table ETIQUETAS_CONFIGURACOES_TELA
add constraint CK_MODELO_ID
check(MODELO_ID >= 0);
