create table CONDICOES_PAGAMENTO_EMPRESA(
  EMPRESA_ID                   number(3),
  CONDICAO_ID                  number(4)
);

/* Chaves estrangeiras */
alter table CONDICOES_PAGAMENTO_EMPRESA
add constraint FK_COND_PAG_EMP_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

/* Chaves estrangeiras */
alter table CONDICOES_PAGAMENTO_EMPRESA
add constraint FK_COND_PAG_EMP_CONDICAO_ID
foreign key(CONDICAO_ID)
references CONDICOES_PAGAMENTO(CONDICAO_ID);


/* Comando para alimentar todas empresas para todos os funcionarios */

declare
  cursor cCondicoes is
    select
      CONDICAO_ID
    from
      CONDICOES_PAGAMENTO;

  cursor cEmpresas is
    select
      EMPRESA_ID
    from
      EMPRESAS;
begin
  for vCondicoes in cCondicoes loop

    for vEmpresa in cEmpresas loop
      insert into CONDICOES_PAGAMENTO_EMPRESA(
        CONDICAO_ID,
        EMPRESA_ID
      ) values (
        vCondicoes.CONDICAO_ID,
        vEmpresa.EMPRESA_ID
      );
    end loop;
  end loop;
end;