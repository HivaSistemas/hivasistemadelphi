create table FAIXAS_ACRESC_COMIS_META_FUNC(
  EMPRESA_ID                      number(3) not null,
  FUNCIONARIO_ID                  NUMBER(4,0) not null,
  FAIXA                           number(3) not null,
  VALOR_INICIAL                   number(11,2) not null,
  VALOR_FINAL                     number(11,2) not null,
  PERCENTUAL                      number(8,2) not null
);

/* Chaves estrangeiras */
alter table FAIXAS_ACRESC_COMIS_META_FUNC
add constraint FK_FA_ACRESC_COM_EMP_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);