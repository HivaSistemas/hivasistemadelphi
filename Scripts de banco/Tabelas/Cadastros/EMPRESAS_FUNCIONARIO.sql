create table EMPRESAS_FUNCIONARIO(
  EMPRESA_ID                   number(3),
  FUNCIONARIO_ID               number(4)
);

/* Chaves estrangeiras */
alter table EMPRESAS_FUNCIONARIO
add constraint FK_EMP_FUNC_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

/* Chaves estrangeiras */
alter table EMPRESAS_FUNCIONARIO
add constraint FK_EMP_FUNC_FUNCIONARIO_ID
foreign key(FUNCIONARIO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);



/* Comando para alimentar todas empresas para todos os funcionarios */

declare
  cursor cFuncionarios is
    select
      FUNCIONARIO_ID
    from
      FUNCIONARIOS;

  cursor cEmpresas is
    select
      EMPRESA_ID
    from
      EMPRESAS;
begin
  for vFuncionario in cFuncionarios loop

    for vEmpresa in cEmpresas loop
      insert into EMPRESAS_FUNCIONARIO(
        FUNCIONARIO_ID,
        EMPRESA_ID
      ) values (
        vFuncionario.FUNCIONARIO_ID,
        vEmpresa.EMPRESA_ID
      );
    end loop;
  end loop;
end;
