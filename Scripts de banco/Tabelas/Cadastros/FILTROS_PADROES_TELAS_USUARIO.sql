﻿create table FILTROS_PADROES_TELAS_USUARIO(
  USUARIO_ID      number(4) not null,
  TELA            varchar2(100) not null,
  CAMPO           varchar2(100) not null,
  ORDEM           number(2) default 0 not null,
  VALOR_PESQUISA  varchar2(200) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table FILTROS_PADROES_TELAS_USUARIO
add constraint PK_FILTROS_PADROES_TEL_USUARIO
primary key(USUARIO_ID, TELA, CAMPO)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table FILTROS_PADROES_TELAS_USUARIO
add constraint FK_FIL_PAD_TELAS_USUARIO_ID
foreign key(USUARIO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* Checagens */
alter table FILTROS_PADROES_TELAS_USUARIO
add constraint FK_FIL_PAD_TELAS_USUARIO_ORDEM
check(ORDEM >= 0);