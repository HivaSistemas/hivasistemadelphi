﻿create table CONDICOES_PAGAMENTO_PRAZOS(
  CONDICAO_ID number not null,
  DIAS        number(3) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table CONDICOES_PAGAMENTO_PRAZOS
add constraint PK_CONDICOES_PAGAMENTO_PRAZOS
primary key(CONDICAO_ID, DIAS)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONDICOES_PAGAMENTO_PRAZOS
add constraint FK_COND_PAGTO_PRAZ_CONDICAO_ID
foreign key(CONDICAO_ID)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

/* Checagens */
alter table CONDICOES_PAGAMENTO_PRAZOS
add constraint CK_COND_PAGTO_PRAZ_DIAS
check(DIAS >= 0);