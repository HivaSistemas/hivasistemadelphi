create table TIPOS_COBR_COND_PAGAMENTO(
  CONDICAO_ID number(3),
  COBRANCA_ID number(3)
);

/* Chave prim�ria */
alter table TIPOS_COBR_COND_PAGAMENTO
add constraint PK_TIPOS_COBR_COND_PAGTO
primary key(CONDICAO_ID, COBRANCA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table TIPOS_COBR_COND_PAGAMENTO
add constraint FK_TIPOS_COB_COND_PAGTO_COND
foreign key(CONDICAO_ID)
references CONDICOES_PAGAMENTO(CONDICAO_ID);

alter table TIPOS_COBR_COND_PAGAMENTO
add constraint FK_TIPOS_COB_COND_PAGTO_COBR
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);