create table PRODUTOS_ICMS(
  PRODUTO_ID                number(10) not null,
  EMPRESA_ID                number(3) not null,
	ESTADO_ID                 char(2) not null,	
  
	CST_NAO_CONTRIBUINTE      varchar2(3) not null,
	CST_CONTRIBUINTE          varchar2(3) not null,
  CST_ORGAO_PUBLICO         varchar2(3) not null,
  CST_REVENDA               varchar2(3) not null,
  CST_CONSTRUTORA           varchar2(3) not null,
  CST_CLINICA_HOSPITAL      varchar2(3) not null,
  CST_PRODUTOR_RURAL        varchar2(3) not null,

  INDICE_REDUCAO_BASE_ICMS  number(5,4) default 1 not null,
  PERCENTUAL_ICMS           number(4,2) not null,
  IVA                       number(4,2) default 0 not null,
  PRECO_PAUTA               number(8,2) default 0 not null
);

alter table PRODUTOS_ICMS
add constraint PK_PRODUTOS_ICMS
primary key(PRODUTO_ID, EMPRESA_ID, ESTADO_ID)
using index tablespace INDICES;

alter table PRODUTOS_ICMS
add constraint FK_PRODUTOS_ICMS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table PRODUTOS_ICMS
add constraint FK_PRODUTOS_ICMS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table PRODUTOS_ICMS
add constraint FK_PRODUTOS_ICMS_ESTADO_ID
foreign key(ESTADO_ID)
references ESTADOS(ESTADO_ID);

alter table PRODUTOS_ICMS
add constraint FK_PROD_ICMS_CFOP_ID_NAO_CONT
foreign key(CFOP_ID_NAO_CONTRIBUINTE)
references CFOP(CFOP_ID);

alter table PRODUTOS_ICMS
add constraint FK_PROD_ICMS_CFOP_ID_CONTRIB
foreign key(CFOP_ID_CONTRIBUINTE)
references CFOP(CFOP_ID);

alter table PRODUTOS_ICMS
add constraint FK_PROD_ICMS_CFOP_ID_ORG_PUBLI
foreign key(CFOP_ID_ORGAO_PUBLICO)
references CFOP(CFOP_ID);

alter table PRODUTOS_ICMS
add constraint FK_PROD_ICMS_CFOP_ID_REVENDA
foreign key(CFOP_ID_REVENDA)
references CFOP(CFOP_ID);

alter table PRODUTOS_ICMS
add constraint FK_PROD_ICMS_CFOP_ID_CONSTRUT
foreign key(CFOP_ID_CONSTRUTORA)
references CFOP(CFOP_ID);

alter table PRODUTOS_ICMS
add constraint FK_PROD_ICMS_CFOP_ID_CLIN_HOSP
foreign key(CFOP_ID_CLINICA_HOSPITAL)
references CFOP(CFOP_ID);

alter table PRODUTOS_ICMS
add constraint FK_PROD_ICMS_CCFOP_ID_PROD_RUR
foreign key(CFOP_ID_PRODUTOR_RURAL)
references CFOP(CFOP_ID);

/* Constraints */
alter table PRODUTOS_ICMS
add constraint CK_PRODUTOS_ICMS_CST_NAO_CONTR
check(CST_NAO_CONTRIBUINTE in('00','10','20','30','40','41','50','51','60','70','90'));

alter table PRODUTOS_ICMS
add constraint CK_PRODUTOS_ICMS_CST_CONTRIB
check(CST_CONTRIBUINTE in('00','10','20','30','40','41','50','51','60','70','90'));

alter table PRODUTOS_ICMS
add constraint CK_PRODUTOS_ICMS_CST_ORGAO_PUB
check(CST_ORGAO_PUBLICO in('00','10','20','30','40','41','50','51','60','70','90'));

alter table PRODUTOS_ICMS
add constraint CK_PRODUTOS_ICMS_CST_REVENDA
check(CST_REVENDA in('00','10','20','30','40','41','50','51','60','70','90'));

alter table PRODUTOS_ICMS
add constraint CK_PRODUTOS_ICMS_CST_CONSTRU
check(CST_CONSTRUTORA in('00','10','20','30','40','41','50','51','60','70','90'));

alter table PRODUTOS_ICMS
add constraint CK_PRODUTOS_ICMS_CST_CLIN_HOSP
check(CST_CLINICA_HOSPITAL in('00','10','20','30','40','41','50','51','60','70','90'));

alter table PRODUTOS_ICMS
add constraint CK_PRODUTOS_FIS_CST_PROD_RURAL
check(CST_PRODUTOR_RURAL in('00','10','20','30','40','41','50','51','60','70','90'));

alter table PRODUTOS_ICMS
add constraint CK_PRODUTOS_FIS_IND_RED_BASE
check(INDICE_REDUCAO_BASE_ICMS between 0.0001 and 1);