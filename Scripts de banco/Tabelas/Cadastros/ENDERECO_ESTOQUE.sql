create table ENDERECO_ESTOQUE(
  ENDERECO_ID             number(3),
  RUA_ID                  number(3),
  MODULO_ID               number(3),
  NIVEL_ID                number(3),
  VAO_ID                  number(3)
);

/* Chave prim�ria */
alter table ENDERECO_ESTOQUE
add constraint PK_END_EST_ENDERECO_ID
primary key(ENDERECO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ENDERECO_ESTOQUE
add constraint FK_END_EST_RUA_ID
foreign key(RUA_ID)
references ENDERECO_EST_RUA(RUA_ID);

alter table ENDERECO_ESTOQUE
add constraint FK_END_EST_MODULO_ID
foreign key(MODULO_ID)
references ENDERECO_EST_MODULO(MODULO_ID);

alter table ENDERECO_ESTOQUE
add constraint FK_END_EST_NIVEL_ID
foreign key(NIVEL_ID)
references ENDERECO_EST_NIVEL(NIVEL_ID);

alter table ENDERECO_ESTOQUE
add constraint FK_END_EST_VAO_ID
foreign key(VAO_ID)
references ENDERECO_EST_VAO(VAO_ID);
