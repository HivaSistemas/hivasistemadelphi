create table CADASTROS_EMAILS(
  CADASTRO_ID   number(10),
  E_MAIL        varchar2(80),
  OBSERVACAO    varchar2(100),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table CADASTROS_EMAILS
add constraint FK_CADASTROS_EMAIL_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);