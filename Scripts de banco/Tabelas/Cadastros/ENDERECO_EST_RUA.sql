create table ENDERECO_EST_RUA(
  RUA_ID                  number(3),
  DESCRICAO               varchar2(30) not null
);

/* Chave prim�ria */
alter table ENDERECO_EST_RUA
add constraint PK_END_EST_RUA_RUA_ID
primary key(RUA_ID)
using index tablespace INDICES;
