create table PROFISSIONAIS_CARGOS(
  CARGO_ID  number(3),
  NOME      varchar(30) not null,
  ATIVO     char(1) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table PROFISSIONAIS_CARGOS
add constraint PK_PROFISSIONAIS_CARGOS
primary key(CARGO_ID)
using index tablespace INDICES;

/* Checagens */
alter table PROFISSIONAIS_CARGOS
add constraint CK_PROFISSIONAIS_CARGOS_ATIVO
check(ATIVO in('S', 'N'));

/* Uniques */
alter table PROFISSIONAIS_CARGOS
add constraint UN_PROFISSIONAIS_CARGOS_NOME
unique(NOME);