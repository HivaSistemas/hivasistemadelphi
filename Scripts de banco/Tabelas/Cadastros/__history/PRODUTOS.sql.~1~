﻿create table PRODUTOS(
  PRODUTO_ID                    number(10),
  DATA_CADASTRO                 date default trunc(sysdate) not null,
  NOME                          varchar2(60) not null,
  NOME_COMPRA                   varchar2(60) not null,
  FORNECEDOR_ID                 number(10) not null,
  UNIDADE_VENDA                 varchar2(5) not null,
  MARCA_ID                      number(6) not null,
  MULTIPLO_VENDA                number(7,3) not null,
  PESO                          number(14,6) not null,
  VOLUME                        number(18,7) not null,
  CODIGO_NCM                    varchar2(8),
  LINHA_PRODUTO_ID              varchar2(11) not null,
  CODIGO_BARRAS                 varchar2(14),
  CODIGO_BALANCA                varchar2(5),
  ACEITAR_ESTOQUE_NEGATIVO      char(1) default 'N' not null,
  INATIVAR_ZERAR_ESTOQUE        char(1) default 'S' not null,
  BLOQUEADO_COMPRAS             char(1) default 'N' not null,
  BLOQUEADO_VENDAS              char(1) default 'N' not null,
  PERMITIR_DEVOLUCAO            char(1) default 'S' not null,
  CARACTERISTICAS               varchar2(2000),
  UNIDADE_ENTREGA_ID            varchar2(5),

  FOTO_1                        blob,
  FOTO_2                        blob,
  FOTO_3                        blob,

  CEST                          varchar2(8),
  PRODUTO_DIVERSOS_PDV          char(1) default 'N' not null,

  ATIVO                         char(1) default 'S' not null,
  MOTIVO_INATIVIDADE            varchar2(200),

  PRODUTO_PAI_ID                number(10) not null,
  QUANTIDADE_VEZES_PAI          number(15,8) default 1 not null,

  TIPO_CONTROLE_ESTOQUE          char(1) default 'N' not null,
  EXIGIR_DATA_FABRICACAO_LOTE    char(1) default 'N' not null,
  EXIGIR_DATA_VENCIMENTO_LOTE    char(1) default 'N' not null,
  CODIGO_ORIGINAL_FABRICANTE     varchar2(35),
  TIPO_DEF_AUTOMATICA_LOTE       char(1) default 'Q' not null,
  CONF_SOMENTE_CODIGO_BARRAS     char(1) default 'N' not null,
  
  SOB_ENCOMENDA                  char(1) default 'N' not null,
  DIAS_AVISO_VENCIMENTO          number(4) default 0 not null,
  REVENDA                        char(1) default 'S' not null,
  USO_CONSUMO                    char(1) default 'N' not null,
  SUJEITO_COMISSAO               char(1) default 'S' not null,
  BLOQUEAR_ENTRAD_SEM_PED_COMPRA char(1) default 'N' not null,
  EXIGIR_MODELO_NOTA_FISCAL      char(1) default 'N' not null,
  EXIGIR_SEPARACAO               char(1) default 'N' not null,
  VALOR_ADICIONAL_FRETE          number(7,2) default 0 not null,
  PERCENTUAL_COMISSAO_VISTA      number(4,2) default 0 not null,
  PERCENTUAL_COMISSAO_PRAZO      number(4,2) default 0 not null,

  PAGAR_COMISSAO_PROFISSIONAL   char(1) default 'S'   not null,
  SERVICO                       char(1) default 'N'   not null,
  PERC_FRETE_ADICIONAL_VENDA    number(5,2) default 0 not null,
  PERC_IPI                      number(5,2) not null,
  MENSAGEM_notA_FISCAL          varchar2(200),
  KIT_ACEITA_DEV_DESMEMBRADA    char(1) default 'N'    not null,
  KIT_ATUALIZ_PCO_ENTRADA_COMP  char(1) default 'N'   not null,
  KIT_ATUALIZ_PCO_VENDA_COMP    char(1) default 'N'   not null,
  QTD_DIAS_GARANTIA_FABRICANTE  number(4,0) default 0 not null,
  CODIGO_ORIGEM_FISCAL          number(1,0) default 0 not null,
  ATIVO_IMOBILIZADO             char(1) default 'N'   not null,
  PRODUTO_RETORNAVEL            char(1) default 'N'   not null,
  PERMITE_VENDA_AUTO_SERVICO    char(1) default 'S'   not null,
  PERTENCE_CESTA_BASICA         char(1) default 'N'   not null,
  CONTROLAR_NUMERO_SERIE        char(1) default 'N'   not null,
  DATA_PRIMEIRA_ENTRADA         date,
  DIAS_ENTREGA_SOB_ENCOMENDA    number(3,0) default 0 not null,
  QTD_EMBALAGEM_COMPRA          number(7,0) default 1 not null,
  MAPA_RECLASSIFICACAO_ID       number(10,0),
  PERMITE_ENTREGAR              char(1) default 'S'   not null,
  PERMITE_ENTREGA_ATO           char(1) default 'S'   not null,
  PERMITIR_VENDER_ACUMULADO     char(1) default 'S'   not null,
  PRECO_TABELA_COMPRA           number(16,6) default 0 not null,
  SIMILAR_ID                    number(10,0),
  TIPO_VENDA_SEM_PREVISAO       char(3) default 'NEN' not null,
  VOLUME_LITROS                 number(18,6) default 0 not null,
  
  CHAVE_IMPORTACAO               varchar2(100)
);

alter table PRODUTOS
add constraint PK_PRODUTOS_PRODUTO_ID
primary key(PRODUTO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PRODUTOS
add constraint FK_PRODUTOS_FORNECEDOR_ID
foreign key(FORNECEDOR_ID)
references FORNECEDORES(CADASTRO_ID);

alter table PRODUTOS
add constraint FK_PRODUTOS_MARCA_ID
foreign key(MARCA_ID)
references MARCAS(MARCA_ID);

alter table PRODUTOS
add constraint FK_PRODUTOS_UNIDADE_VENDA
foreign key(UNIDADE_VENDA)
references UNIDADES(UNIDADE_ID);

alter table PRODUTOS
add constraint FK_PRODUTOS_UNID_ENTREGA_ID
foreign key(UNIDADE_ENTREGA_ID)
references UNIDADES(UNIDADE_ID);

alter table PRODUTOS
add constraint FK_PRODUTOS_LINHA_ID
foreign key(LINHA_PRODUTO_ID)
references PRODUTOS_DEPTOS_SECOES_LINHAS(DEPTO_SECAO_LINHA_ID);

alter table PRODUTOS
add constraint FK_PRODUTOS_PRODUTO_PAI_ID
foreign key(PRODUTO_PAI_ID)
references PRODUTOS(PRODUTO_ID);

/* Checagens */
alter table PRODUTOS
add constraint CK_PRODUTOS_ACEITAR_EST_NEGAT
check(ACEITAR_ESTOQUE_NEGATIVO in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_BLOQUEADO_COMPRAS
check(BLOQUEADO_COMPRAS in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_BLOQUEADO_VENDAS
check(BLOQUEADO_VENDAS in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_PERMITIR_DEVOLUCAO
check(PERMITIR_DEVOLUCAO in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_MOT_INAT
CHECK
((ATIVO = 'S' and MOTIVO_INATIVIDADE is null)
or
(ATIVO = 'N' and MOTIVO_INATIVIDADE is not null)
or
(INATIVAR_ZERAR_ESTOQUE = 'S' and MOTIVO_INATIVIDADE is not null)
or
(INATIVAR_ZERAR_ESTOQUE = 'N' and MOTIVO_INATIVIDADE is null));
     
alter table PRODUTOS
add constraint CK_PRODUTOS_PROD_DIVERSOS_PDV
check(PRODUTO_DIVERSOS_PDV in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_QTDE_VEZES_PAI
check(QUANTIDADE_VEZES_PAI > 0);

/* TIPO_CONTROLE_ESTOQUE
  N - Normal
  L - Lote
  P - Piso
  G - Grade
  K - Kit entrega desmembrada
  T - Tintométrico
  A - Kit entrega agrupada
*/
alter table PRODUTOS
add constraint CK_PRODUTOS_TIPO_CONTROLE_EST
check(TIPO_CONTROLE_ESTOQUE in('N', 'L', 'P','G', 'K', 'A'));

alter table PRODUTOS
add constraint CK_PROD_EXIGIR_DATA_FABRI_LOTE
check(EXIGIR_DATA_FABRICACAO_LOTE in('S', 'N'));

alter table PRODUTOS
add constraint CK_PROD_EXIGIR_DATA_VENCI_LOTE
check(EXIGIR_DATA_VENCIMENTO_LOTE in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_SOB_ENCOMENDA
check(SOB_ENCOMENDA in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_DIAS_AVISO_VENCTO
check(
  (EXIGIR_DATA_VENCIMENTO_LOTE = 'N' and DIAS_AVISO_VENCIMENTO = 0)
  or
  (EXIGIR_DATA_VENCIMENTO_LOTE = 'S' and DIAS_AVISO_VENCIMENTO between 0 and 999)
);

alter table PRODUTOS
add constraint CK_PRODUTOS_REVENDA
check(REVENDA in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_USO_CONSUMO
check(USO_CONSUMO in('S', 'N'));
  
alter table PRODUTOS
add constraint CK_PRODUTOS_SUJEITO_COMISSAO
check(SUJEITO_COMISSAO in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_PERC_COMISSAO_VIST
check(
  (SUJEITO_COMISSAO = 'N' and PERCENTUAL_COMISSAO_VISTA = 0)
  or
  (PERCENTUAL_COMISSAO_VISTA between 0 and 99.99)
);

alter table PRODUTOS
add constraint CK_PRODUTOS_PERC_COMISSAO_PRAZ
check(
  (SUJEITO_COMISSAO = 'N' and PERCENTUAL_COMISSAO_PRAZO = 0)
  or
  (PERCENTUAL_COMISSAO_PRAZO between 0 and 99.99)
);
  
alter table PRODUTOS
add constraint CK_PROD_BLOQ_ENT_SEM_PED_COMPR
check(BLOQUEAR_ENTRAD_SEM_PED_COMPRA in('S', 'N'));
  
alter table PRODUTOS
add constraint CK_PROD_EXIGIR_MOD_NOTA_FISC
check(EXIGIR_MODELO_NOTA_FISCAL in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_EXIGIR_SEPARACAO
check(EXIGIR_SEPARACAO in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_VALOR_ADIC_FRETE
check(VALOR_ADICIONAL_FRETE >= 0);

/*
  TIPO_DEF_AUTOMATICA_LOTE
  Q - Atendam a quantidade em um mesmo lote
  V - priorizar o vencimento
*/
alter table PRODUTOS
add constraint CK_PRODUTOS_TIPO_DEF_AUT_LOTE
check(TIPO_DEF_AUTOMATICA_LOTE in('Q', 'V'));

alter table PRODUTOS
add constraint CK_PRODUTOS_UNIDADE_VENDA_ENT
check(
  UNIDADE_ENTREGA_ID is null
  or
  UNIDADE_VENDA <> UNIDADE_ENTREGA_ID
);

alter table PRODUTOS
add constraint CK_PRODUTOS_CON_SOM_COD_BARRAS
check(
  CONF_SOMENTE_CODIGO_BARRAS = 'N'
  or
  CONF_SOMENTE_CODIGO_BARRAS = 'S' and CODIGO_BARRAS is not null
);

alter table PRODUTOS
add constraint CK_PRODUTOS_PAG_COM_PROF
check(PAGAR_COMISSAO_PROFISSIONAL in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_SERVICO
check(SERVICO in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_KIT_AC_DEV_DESM
check(KIT_ACEITA_DEV_DESMEMBRADA in('S', 'N'));

alter table PRODUTOS
add constraint CK_PROD_KIT_ATU_PCO_ENTR_COMP
check(KIT_ATUALIZ_PCO_ENTRADA_COMP in('S', 'N'))

alter table PRODUTOS
add constraint CK_PROD_KIT_ATU_PCO_VENDA_COMP
check(KIT_ATUALIZ_PCO_VENDA_COMP in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_ATIVO_IMOBILIZADO
check(ATIVO_IMOBILIZADO in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_PRODUTO_RETORNAVEL
check(PRODUTO_RETORNAVEL in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_PERM_VEND_AUT_SERV
check(PERMITE_VENDA_AUTO_SERVICO in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_PERTENC_CEST_BASIC
check(PERTENCE_CESTA_BASICA in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_CONTROL_NUM_SERIE
check(CONTROLAR_NUMERO_SERIE in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_PERMITE_ENTREGAR
check(PERMITE_ENTREGAR in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_PERMITE_ENTREG_ATO
check(PERMITE_ENTREGA_ATO in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_PERMIT_VEND_ACUMUL
check(PERMITIR_VENDER_ACUMULADO in('S', 'N'));

alter table PRODUTOS
add constraint CK_PRODUTOS_TP_VENDA_SEM_PREV
check(TIPO_VENDA_SEM_PREVISAO in('NEN', 'ALT','WEB','AMB'));
/*
  TIPO_VENDA_SEM_PREVISAO
  NEM - Nenhum
  ALT - Somente no ALtis
  WEB - Somente no site
  SAT - 
*/

/* Unique */
alter table PRODUTOS
add constraint CK_PRODUTOS_CODIGO_BARRAS
unique(CODIGO_BARRAS);

alter table PRODUTOS
add constraint CK_PRODUTOS_CODIGO_BALANCA
unique(CODIGO_BALANCA);

alter table PRODUTOS
add constraint CK_PRODUTOS_CODIGO_ORIG_FABR
unique(FORNECEDOR_ID, PRODUTO_ID, CODIGO_ORIGINAL_FABRICANTE);

alter table PRODUTOS
add constraint UN_PRODUTOS_NOME_MARCA
unique(NOME,MARCA_ID);

/* Indices */
create index IDX_PRODUTOS_ATIVO
on PRODUTOS(ATIVO)
tablespace INDICES;

create index IDX_PRODUTOS_PROD_PAI_ID
on PRODUTOS(PRODUTO_PAI_ID)
tablespace INDICES;

create index IDX_PRODUTOS_FORNECEDOR_ID
on PRODUTOS(FORNECEDOR_ID)
tablespace INDICES;


