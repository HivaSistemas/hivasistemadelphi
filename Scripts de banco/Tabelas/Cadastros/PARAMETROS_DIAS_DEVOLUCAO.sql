create table PARAMETROS_DIAS_DEVOLUCAO(
  EMPRESA_ID                      number(3) not null,
  FAIXA                           number(3) not null,
  VALOR_INICIAL                   number(5) not null,
  VALOR_FINAL                     number(5) not null,
  PERCENTUAL                      number(8,2) not null
);

/* Chaves estrangeiras */
alter table PARAMETROS_DIAS_DEVOLUCAO
add constraint FK_PAR_DIA_DEVOL_EMP_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);