create table PARAMETROS_COMISSAO(
  EMPRESA_ID                      number(3) not null,
  FAIXA                           number(3) not null,
  VALOR_INICIAL                   number(11,2) not null,
  VALOR_FINAL                     number(11,2) not null,
  PERCENTUAL                      number(8,2) not null
);

/* Chaves estrangeiras */
alter table PARAMETROS_COMISSAO
add constraint FK_PAR_COMISSSAO_EMP_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);