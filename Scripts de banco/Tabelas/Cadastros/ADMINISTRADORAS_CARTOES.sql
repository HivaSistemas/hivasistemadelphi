create table ADMINISTRADORAS_CARTOES(
  ADMINISTRADORA_ID       number(3) not null,
  DESCRICAO               varchar2(60) not null,
  CNPJ                    varchar2(18) not null,
  ATIVO                   char(1) default 'S' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table ADMINISTRADORAS_CARTOES
add constraint PK_ADMINISTRADORAS_CARTOES
primary key(ADMINISTRADORA_ID)
using index tablespace INDICES;

/* Checagens */
alter table ADMINISTRADORAS_CARTOES
add constraint CK_ADMIN_CARTOES_CNPJ
check( length(CNPJ) = 18 );

alter table ADMINISTRADORAS_CARTOES
add constraint CK_ADMIN_CARTOES_ATIVO
check( ATIVO in('S', 'N') );