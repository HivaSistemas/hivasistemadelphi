﻿create table MOVIMENTOS_CONTAS(
  MOVIMENTO_ID            number(10) not null,
  CONTA_ID                varchar2(8) not null,
  VALOR_MOVIMENTO         number(12,2) not null,
  DATA_HORA_MOVIMENTO     date not null,
  USUARIO_MOVIMENTO_ID    number(4) not null,
  CONCILIADO              char(1) default 'N' not null,
  
  TIPO_MOVIMENTO          char(3) not null,
  
  PLANO_FINANCEIRO_ID     varchar2(9),
  CENTRO_CUSTO_ID         number(3) not null,

  USUARIO_CONCILIACAO_ID  number(4),
  DATA_HORA_CONCILIACAO   date,
  
  CONTA_DESTINO_ID        varchar2(8),
  BAIXA_RECEBER_ID        number(10),
  BAIXA_PAGAR_ID          number(10),
  MOVIMENTO_TURNO_ID      number(10),

  MOVIMENTO_ID_BANCO     varchar2(20),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Primary key */
alter table MOVIMENTOS_CONTAS
add constraint PK_MOVIMENTOS_CONTAS
primary key(MOVIMENTO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table MOVIMENTOS_CONTAS
add constraint FK_MOVIMENTOS_CONTAS_CONTA_ID
foreign key(CONTA_ID)
references CONTAS(CONTA);

alter table MOVIMENTOS_CONTAS
add constraint FK_MOV_CONTAS_CONTA_DEST_ID
foreign key(CONTA_DESTINO_ID)
references CONTAS(CONTA);

alter table MOVIMENTOS_CONTAS
add constraint FK_MOVIMENTOS_CON_PLANO_FIN_ID
foreign key(PLANO_FINANCEIRO_ID)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

alter table MOVIMENTOS_CONTAS
add constraint FK_MOV_CON_PLANO_CENTRO_CUS_ID
foreign key(CENTRO_CUSTO_ID)
references CENTROS_CUSTOS(CENTRO_CUSTO_ID);

alter table MOVIMENTOS_CONTAS
add constraint FK_MOV_CONTAS_BX_RECEBER_ID
foreign key(BAIXA_RECEBER_ID)
references CONTAS_RECEBER_BAIXAS(BAIXA_ID);

alter table MOVIMENTOS_CONTAS
add constraint FK_MOV_CONTAS_BX_PAGAR_ID
foreign key(BAIXA_PAGAR_ID)
references CONTAS_PAGAR_BAIXAS(BAIXA_ID);

alter table MOVIMENTOS_CONTAS
add constraint FK_MOV_CONTAS_USUARIO_MOV_ID
foreign key(USUARIO_MOVIMENTO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table MOVIMENTOS_CONTAS
add constraint FK_MOV_CONTAS_MOV_TURN_ID
foreign key(MOVIMENTO_TURNO_ID)
references MOVIMENTOS_TURNOS(MOVIMENTO_TURNO_ID);
/* Checagens */

/* 
  TIPO_MOVIMENTO
  FTC - Fechamento de turno de caixa
  BXR - Baixa de contas a receber
  BXP - Baixa de contas a pagar
  ENM - Entrada manual
  SAM - Saída manual  
  TRA - Transferência
  PIX - PIX
*/
alter table MOVIMENTOS_CONTAS
add constraint CK_MOV_CONTAS_TIPO_MOVIMENTO
check(
  TIPO_MOVIMENTO in('BXR', 'BXP', 'ENM', 'SAM', 'TRA', 'PIX')
  or
  (TIPO_MOVIMENTO = 'FTC' and MOVIMENTO_TURNO_ID is not null)
);

alter table MOVIMENTOS_CONTAS
add constraint CK_MOV_CONTAS_CONCILIADO
check(
  (CONCILIADO = 'N' and USUARIO_CONCILIACAO_ID is null and DATA_HORA_CONCILIACAO is null)
  or
  (CONCILIADO = 'S' and USUARIO_CONCILIACAO_ID is not null and DATA_HORA_CONCILIACAO is not null)
);

alter table MOVIMENTOS_CONTAS
add constraint CK_MOV_CONTAS_VALOR_MOVIMENTO
check(VALOR_MOVIMENTO > 0);

alter table MOVIMENTOS_CONTAS
add constraint CK_MOV_CONTAS_DATA_HORA_MOVIM
check(to_number(to_char(DATA_HORA_MOVIMENTO, 'YYYY')) > 2009);
