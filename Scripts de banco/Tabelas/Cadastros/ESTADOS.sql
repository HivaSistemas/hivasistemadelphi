﻿create table ESTADOS(
  ESTADO_ID               char(2)      not null,
  NOME                    varchar2(60) not null,
  CODIGO_IBGE             number(2),
  ATIVO                   char(1)      default 'S' not null,

  PERC_ICMS_INTERNO       number(4,2)  default 0 not null,

  CHAVE_IMPORTACAO        varchar2(100),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table ESTADOS
add constraint PK_ESTADOS_ESTADO_ID
primary key(ESTADO_ID)
using index tablespace INDICES;

/* Checagens */
alter table ESTADOS
add constraint CK_ESTADOS_ATIVO
check(ATIVO in('S', 'N'));

alter table ESTADOS
add constraint CK_ESTADOS_ESTADO_ID
check(ESTADO_ID in('GO','DF','AC','AL','AP','AM','BA','CE','ES','MA','MT','MS','MG','PA','PB','PR','PE','PI','RJ','RN','RS','RO','RR','SC','SP','SE','TO'));

alter table ESTADOS
add constraint CK_ESTADOS_PERC_ICMS_INTERNO
check(PERC_ICMS_INTERNO between 0 and 99.99);

/* Uniques */
alter table ESTADOS
add constraint CK_ESTADOS_CODIGO_IBGE
unique(CODIGO_IBGE);
