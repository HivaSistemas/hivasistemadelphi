﻿create table TRANSPORTADORAS(
  CADASTRO_ID         number(10) not null,
  DATA_HORA_CADASTRO  date not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table TRANSPORTADORAS
add constraint PK_TRANSPORTADORAS_CADASTRO_ID
primary key(CADASTRO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table TRANSPORTADORAS
add constraint FK_TRANSPORTADORAS_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);