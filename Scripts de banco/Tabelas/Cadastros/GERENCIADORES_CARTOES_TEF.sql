create table GERENCIADORES_CARTOES_TEF(
  GERENCIADOR_CARTAO_ID          number(2),
  NOME                           varchar2(20) not null,
  DIRETORIO_ARQUIVOS_REQUISICOES varchar2(200) not null,
  DIRETORIO_ARQUIVOS_RESPOSTAS   varchar2(200) not null,
  
  TEXTO_PROCURAR_BANDEIRA_1      varchar2(20),
  POSICAO_INICIAL_PROC_BAND_1    number(3),
  QTDE_CARACTERES_COPIAR_BAND_1  number(3),  
  
  TEXTO_PROCURAR_BANDEIRA_1      varchar2(20),
  POSICAO_INICIAL_PROC_BAND_2    number(3),
  QTDE_CARACTERES_COPIAR_BAND_2  number(3),
  
  TEXTO_PROCURAR_REDE_1          varchar2(20),
  POSICAO_INICIAL_PROC_REDE_1    number(3),
  QTDE_CARACTERES_COPIAR_REDE_1  number(3),
  
  TEXTO_PROCURAR_REDE_2          varchar2(20),
  POSICAO_INICIAL_PROC_REDE_2    number(3),
  QTDE_CARACTERES_COPIAR_REDE_2  number(3),
  
  TEXTO_PROCURAR_NUMERO_CARTAO_1 varchar2(20),
  POSIC_INI_PROC_NUMERO_CARTAO_1 number(3),
  QTDE_CARAC_COPIAR_NR_CARTAO_1  number(3),
  
  TEXTO_PROCURAR_NUMERO_CARTAO_2 varchar2(20),
  POSIC_INI_PROC_NUMERO_CARTAO_2 number(3),
  QTDE_CARAC_COPIAR_NR_CARTAO_2  number(3),
  
  TEXTO_PROCURAR_NSU_1           varchar2(20),
  POSICAO_INI_PROCURAR_NSU_1     number(3),
  QTDE_CARAC_COPIAR_NSU_1        number(3),
  
  TEXTO_PROCURAR_NSU_2           varchar2(20),
  POSICAO_INI_PROCURAR_NSU_2     number(3),
  QTDE_CARAC_COPIAR_NSU_2        number(3),
  
  TEXTO_PROCURAR_COD_AUTORIZ_1   varchar2(20),
  POSICAO_INI_PRO_COD_AUTORIZ_1  number(3),
  QTDE_CARAC_COP_COD_AUTORIZ_1   number(3),
  
  TEXTO_PROCURAR_COD_AUTORIZ_2   varchar2(20),
  POSICAO_INI_PRO_COD_AUTORIZ_2  number(3),
  QTDE_CARAC_COP_COD_AUTORIZ_2   number(3),
  
  ATIVO                          char(1) not null
);

alter table GERENCIADORES_CARTOES_TEF
add constraint PK_GERENCIADORES_CARTOES_ID
primary key(GERENCIADOR_CARTAO_ID)
using index tablespace INDICES;

/* Constraints */
alter table GERENCIADORES_CARTOES_TEF
add constraint CK_GERENCIADORES_CARTOES_NOME
unique(NOME);

alter table GERENCIADORES_CARTOES_TEF
add constraint CK_GERENCIADORES_CARTOES_ATIVO
check(ATIVO in('S', 'N'));