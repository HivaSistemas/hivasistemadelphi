create table PRODUTOS_MULTIPLOS_COMPRA(
  PRODUTO_ID number(10),
  UNIDADE_ID varchar2(5),
  MULTIPLO   number(20,4) default 1 not null,
  QUANTIDADE_EMBALAGEM  number(20,4) default 1 not null,
  ORDEM      number(2) not null
);

alter table PRODUTOS_MULTIPLOS_COMPRA
add constraint PK_PRODUTOS_MULTIPLOS_COMPRA
primary key(PRODUTO_ID, UNIDADE_ID, MULTIPLO, QUANTIDADE_EMBALAGEM)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PRODUTOS_MULTIPLOS_COMPRA
add constraint FK_PROD_MULT_COMPRA_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table PRODUTOS_MULTIPLOS_COMPRA
add constraint FK_PROD_MULT_COMPRA_UNIDADE_ID
foreign key(UNIDADE_ID)
references UNIDADES(UNIDADE_ID);

/* Constraints */
alter table PRODUTOS_MULTIPLOS_COMPRA
add constraint CK_PROD_MULT_COMPRA_MULTIPLO
check(MULTIPLO > 0);

alter table PRODUTOS_MULTIPLOS_COMPRA
add constraint CK_PROD_MULT_COMPRA_QTDE_EMB
check(QUANTIDADE_EMBALAGEM > 0);

alter table PRODUTOS_MULTIPLOS_COMPRA
add constraint CK_PROD_MULT_COMPRA_ORDEM
check(ORDEM > 0);
