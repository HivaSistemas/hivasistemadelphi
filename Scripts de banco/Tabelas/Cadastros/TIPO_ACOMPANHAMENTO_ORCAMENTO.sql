create table TIPO_ACOMPANHAMENTO_ORCAMENTO(
  TIPO_ACOMPANHAMENTO_ID  number(3),
  DESCRICAO               varchar2(200) not null,
  ATIVO                   char(1)
);

/* Chave prim�ria */
alter table TIPO_ACOMPANHAMENTO_ORCAMENTO
add constraint PK_TIP_ACO_ORC_ID
primary key(TIPO_ACOMPANHAMENTO_ID)
using index tablespace INDICES;