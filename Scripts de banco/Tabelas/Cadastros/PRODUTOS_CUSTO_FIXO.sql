create table PRODUTOS_CUSTO_FIXO(
  EMPRESA_ID              number(10) not null,
  PRODUTO_ID              number(10) not null,
  CUSTO_FIXO              number(8,2) not null,
  ZERAR_CUSTO             char(1) default 'N' not null
);

/* Chave prim�ria */
alter table PRODUTOS_CUSTO_FIXO
add constraint PK_PRODUTOS_CUSTO_FIXO
primary key(EMPRESA_ID, PRODUTO_ID)
using index tablespace INDICES;