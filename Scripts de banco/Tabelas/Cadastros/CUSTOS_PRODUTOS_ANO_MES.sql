﻿create table CUSTOS_PRODUTOS_ANO_MES(
  EMPRESA_ID                     number(3) not null,
  PRODUTO_ID                     number(10) not null,
  ANO_MES                        number(6) not null,
  
  PRECO_FINAL                    number(14,4) default 0 not null, /* Esta coluna guarda o custo da ultima entrada SEM DESCONTAR os créditos de impostos */
  PRECO_FINAL_MEDIO              number(14,4) default 0 not null, /* Média ponderada da coluna PRECO_FINAL */  
  
  PRECO_LIQUIDO                  number(14,4) default 0 not null, /* Esta coluna guarda o custo da ultima entrada desconto os créditos de impostos */
  PRECO_LIQUIDO_MEDIO            number(14,4) default 0 not null,  
  
  CMV                            number(14,4) default 0 not null,  

  CUSTO_PEDIDO_MEDIO             number(14,4) default 0 not null, /* Esta coluna guarda a média ponderada da compra não fiscal */  
  CUSTO_ULTIMO_PEDIDO            number(14,4) default 0 not null, /* Esta coluna guarda o custo final da ultima compra */
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null 
);

alter table CUSTOS_PRODUTOS_ANO_MES
add constraint PK_CUSTOS_PRODUTOS_ANO_MES
primary key(EMPRESA_ID, PRODUTO_ID, ANO_MES)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CUSTOS_PRODUTOS_ANO_MES
add constraint FK_CUS_PROD_ANO_MES_EMP_PRO_ID
foreign key(EMPRESA_ID, PRODUTO_ID)
references CUSTOS_PRODUTOS(EMPRESA_ID, PRODUTO_ID);

/* Constraints */
alter table CUSTOS_PRODUTOS_ANO_MES
add constraint CK_CUSTOS_PROD_ANO_MES_PRE_FIN
check(PRECO_FINAL >= 0);

alter table CUSTOS_PRODUTOS_ANO_MES
add constraint CK_CUS_PROD_ANO_M_PRE_FIN_MED
check(PRECO_FINAL_MEDIO >= 0);

alter table CUSTOS_PRODUTOS_ANO_MES
add constraint CK_CUSTOS_PROD_ANO_MES_PRE_LIQ
check(PRECO_LIQUIDO >= 0);

alter table CUSTOS_PRODUTOS_ANO_MES
add constraint CK_CUSTOS_PROD_ANO_M_P_LIQ_MED
check(PRECO_LIQUIDO_MEDIO >= 0);

alter table CUSTOS_PRODUTOS_ANO_MES
add constraint CK_CUSTOS_PRODUTOS_ANO_MES_CMV
check(CMV >= 0);

alter table CUSTOS_PRODUTOS_ANO_MES
add constraint CK_CUSTOS_PROD_ANO_M_CT_PED_ME
check(CUSTO_PEDIDO_MEDIO >= 0);

alter table CUSTOS_PRODUTOS_ANO_MES
add constraint CK_CUS_PROD_ANO_MES_CT_ULT_PED
check(CUSTO_ULTIMO_PEDIDO >= 0);