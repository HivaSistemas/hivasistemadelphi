create table CRM(
  CENTRAL_ID    number(10),
  CADASTRO_ID   number(10),
  OBSERVACAO    varchar2(200)
);

/* Chave primaria */
alter table CRM
add constraint PK_CRM
primary key(CENTRAL_ID)
using index tablespace INDICES;

alter table CRM
add constraint FK_CRM_CADASTRO_ID
foreign key(CADASTRO_ID)
references CADASTROS(CADASTRO_ID);