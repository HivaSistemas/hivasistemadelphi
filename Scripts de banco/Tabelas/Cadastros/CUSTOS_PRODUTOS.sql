create table CUSTOS_PRODUTOS(
  EMPRESA_ID                     number(3) not null,
  PRODUTO_ID                     number(10) not null, 
  
  PRECO_TABELA                   number(14,4) default 0 not null,
  PERCENTUAL_DESCONTO_ENTRADA    number(8,5) default 0 not null, /* (-) */
  PERCENTUAL_IPI_ENTRADA         number(8,5) default 0 not null, /* (+) */
  PERCENTUAL_OUTRAS_DESPESAS_ENT number(8,5) default 0 not null, /* (+) */
  PERCENTUAL_FRETE_ENTRADA       number(8,5) default 0 not null, /* (+) */
  PERCENTUAL_ST_ENTRADA          number(8,5) default 0 not null, /* (+) */
  PERCENTUAL_ICMS_ENTRADA        number(8,5) default 0 not null, /* (-) */
  PERCENTUAL_ICMS_FRETE_ENTRADA  number(8,5) default 0 not null, /* (-) */
  PERCENTUAL_PIS_ENTRADA         number(8,5) default 0 not null, /* (-) */
  PERCENTUAL_COFINS_ENTRADA      number(8,5) default 0 not null, /* (-) */
  PERCENTUAL_PIS_FRETE_ENTRADA   number(8,5) default 0 not null, /* (-) */
  PERCENTUAL_COFINS_FRETE_ENT    number(8,5) default 0 not null, /* (-) */
  PERCENTUAL_OUTROS_CUSTOS_ENTR  number(8,5) default 0 not null, /* (+) */
  PERCENTUAL_CUSTO_FIXO_ENT      number(8,5) default 0 not null, /* (+) */
  PERCENTUAL_DIFAL_ENTRADA       number(8,5) default 0 not null, /* (+) */

  PRECO_FINAL                    number(14,4) default 0 not null, /* Esta coluna guarda o custo da ultima entrada SEM DESCONTAR os cr�ditos de impostos */
  PRECO_FINAL_MEDIO              number(14,4) default 0 not null, /* M�dia ponderada da coluna PRECO_FINAL */  
  
  PRECO_LIQUIDO                  number(14,4) default 0 not null, /* Esta coluna guarda o custo da ultima entrada desconto os cr�ditos de impostos */
  PRECO_LIQUIDO_MEDIO            number(14,4) default 0 not null,  
  
  CMV                            number(14,4) default 0 not null,  

  CUSTO_PEDIDO_MEDIO             number(14,4) default 0 not null, /* Esta coluna guarda a m�dia ponderada da compra n�o fiscal */  
  CUSTO_ULTIMO_PEDIDO            number(14,4) default 0 not null, /* Esta coluna guarda o custo final da ultima compra */

  ORIGEM                         char(3) default 'NEM' not null,
  ORIGEM_ID                      number(12),
  
  CUSTO_COMPRA_COMERCIAL         number(14,4) default 0 not null,
  
  /* Custos anteriores quando h� altera��o pela entrada ou compra */
  PRECO_TABELA_ANTERIOR           number(14,4) default 0 not null,
  PERCENTUAL_DESCONTO_ENTR_ANT    number(8,5) default 0 not null,
  PERCENTUAL_IPI_ENTRADA_ANT      number(8,5) default 0 not null,
  PERCENTUAL_OUTRAS_DESP_ENT_ANT  number(8,5) default 0 not null,
  PERCENTUAL_FRETE_ENTR_ANTERIOR  number(8,5) default 0 not null,
  PERCENTUAL_ST_ENTRADA_ANTERIOR  number(8,5) default 0 not null,
  PERCENTUAL_ICMS_ENTR_ANTERIOR   number(8,5) default 0 not null,
  PERCENTUAL_ICMS_FRETE_ENTR_ANT  number(8,5) default 0 not null,
  PERCENTUAL_PIS_ENTRADA_ANTER    number(8,5) default 0 not null,
  PERCENTUAL_COFINS_ENT_ANTERIOR  number(8,5) default 0 not null,
  PERCENTUAL_PIS_FRETE_ENT_ANT    number(8,5) default 0 not null,
  PERCENTUAL_COF_FRETE_ENT_ANT    number(8,5) default 0 not null,
  PERCENTUAL_OUT_CUS_ENTR_ANTER   number(8,5) default 0 not null,
  PERCENTUAL_CUSTO_FIXO_ENT_ANT   number(8,5) default 0 not null,
  PERCENTUAL_DIFAL_ENTRADA_ANTER  number(8,5) default 0 not null,
  PRECO_FINAL_ANTERIOR            number(14,4) default 0 not null,
  PRECO_LIQUIDO_ANTERIOR          number(14,4) default 0 not null,
  CMV_ANTERIOR                    number(14,4) default 0 not null,
  CUSTO_PEDIDO_MEDIO_ANTERIOR     number(14,4) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null 
);

alter table CUSTOS_PRODUTOS
add constraint PK_CUSTOS_PRODUTOS
primary key(EMPRESA_ID, PRODUTO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CUSTOS_PRODUTOS
add constraint FK_CUSTOS_PRODUTOS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table CUSTOS_PRODUTOS
add constraint FK_CUSTOS_PRODUTOS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* Constraints */
alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PRECO_TABELA
check(PRECO_TABELA >= 0);

/* Percentuais */
alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PERC_DESC_ENTRA
check(PERCENTUAL_DESCONTO_ENTRADA between 0 and 99.99999);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PERC_IPI_ENTR
check(PERCENTUAL_IPI_ENTRADA between 0 and 99.99999);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PERC_OUT_DES_EN
check(PERCENTUAL_OUTRAS_DESPESAS_ENT between 0 and 999.99999);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PERC_FRETE_ENTR
check(PERCENTUAL_FRETE_ENTRADA between 0 and 999.99999);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PERC_ST_ENTR
check(PERCENTUAL_ST_ENTRADA between 0 and 99.99999);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PERC_ICMS_ENTR
check(PERCENTUAL_ICMS_ENTRADA between 0 and 99.99999);

alter table CUSTOS_PRODUTOS
add constraint CK_CUST_PROD_PERC_ICMS_FRE_ENT
check(PERCENTUAL_ICMS_FRETE_ENTRADA between 0 and 99.99999);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PERC_PIS_ENTRAD
check(PERCENTUAL_PIS_ENTRADA between 0 and 99.99999);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PERC_COFINS_ENT
check(PERCENTUAL_COFINS_ENTRADA between 0 and 99.99999);

alter table CUSTOS_PRODUTOS
add constraint CK_CUST_PROD_PERC_OUT_CUST_ENT
check(PERCENTUAL_OUTROS_CUSTOS_ENTR between 0 and 99.99999);

alter table CUSTOS_PRODUTOS
add constraint CK_CUST_PROD_PERC_CUS_FIXO_ENT
check(PERCENTUAL_CUSTO_FIXO_ENT between 0 and 99.99999);

/*
  ORIGEM
  NEN - N�o houve altera��o
  ENT - Altera��o via entradas
  COM - Altera��o via compras
  FCP - Altera��o via forma��o de custos
*/
alter table CUSTOS_PRODUTOS
add constraint CK_CUST_PROD_ORIGEM
check(
  ORIGEM = 'NEN' and ORIGEM_ID is null
  or
  ORIGEM <> 'NEN' and ORIGEM_ID is not null
);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PRODUTOS_PRECO_FINAL
check(PRECO_FINAL >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PRECO_FINAL_MED
check(PRECO_FINAL_MEDIO >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PRECO_FINAL_ANT
check(PRECO_FINAL_ANTERIOR >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PRECO_LIQUIDO
check(PRECO_LIQUIDO >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PRECO_LIQ_MEDIO
check(PRECO_LIQUIDO_MEDIO >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_PRECO_LIQ_ANT
check(PRECO_LIQUIDO_ANTERIOR >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PRODUTOS_CMV
check(CMV >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PRODUTOS_CMV_ANT
check(CMV_ANTERIOR >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_CT_PED_MED
check(CUSTO_PEDIDO_MEDIO >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_CT_PED_MED_ANT
check(CUSTO_PEDIDO_MEDIO_ANTERIOR >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_CT_ULT_PEDIDO
check(CUSTO_ULTIMO_PEDIDO >= 0);

alter table CUSTOS_PRODUTOS
add constraint CK_CUSTOS_PROD_CT_COM_COMERC
check(CUSTO_COMPRA_COMERCIAL >= 0);