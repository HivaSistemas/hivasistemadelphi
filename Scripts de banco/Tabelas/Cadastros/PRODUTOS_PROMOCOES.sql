create table PRODUTOS_PROMOCOES(
  EMPRESA_ID                  number(3) not null,
  PRODUTO_ID                  number(10) not null,  
  
  PRECO_PROMOCIONAL           number(8,2) not null,
  DATA_INICIAL_PROMOCAO       date not null,
  DATA_FINAL_PROMOCAO         date not null,
  QUANTIDADE_MINIMA           number(10,4) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);  
  
/* Chave primaria */
alter table PRODUTOS_PROMOCOES
add constraint PK_PRODUTOS_PROMOCOES
primary key(PRODUTO_ID, EMPRESA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PRODUTOS_PROMOCOES
add constraint PK_PRODUTOS_PROMOC_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table PRODUTOS_PROMOCOES
add constraint PK_PRODUTOS_PROMOC_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* Constraints */
alter table PRODUTOS_PROMOCOES
add constraint CK_PRODUTOS_PROM_PRECO_PROMOC
check(PRECO_PROMOCIONAL > 0);

alter table PRODUTOS_PROMOCOES
add constraint CK_PROD_PROM_PRECO_DATA_INIC_F
check(DATA_FINAL_PROMOCAO > DATA_INICIAL_PROMOCAO);

alter table PRODUTOS_PROMOCOES
add constraint CK_PROD_PROM_QUANTIDADE_MINIMA
check(QUANTIDADE_MINIMA >= 0);

