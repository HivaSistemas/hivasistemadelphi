create table PERCENTUAIS_CUSTOS_EMPRESA(
  EMPRESA_ID                      number(3) not null,
  
  PERCENTUAL_COMISSAO             number(7,4) default 0 not null,
  PERCENTUAL_FRETE_VENDA          number(7,4) default 0 not null,
  PERCENTUAL_CUSTO_FIXO           number(7,4) default 0 not null,
  
  PERCENTUAL_PIS                  number(7,4) default 0 not null,
  PERCENTUAL_COFINS               number(7,4) default 0 not null,
  PERCENTUAL_CSLL                 number(7,4) default 0 not null,
  PERCENTUAL_IRPJ                 number(7,4) default 0 not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);
  
/* Chave primaria */
alter table PERCENTUAIS_CUSTOS_EMPRESA
add constraint PK_PERCENTUAIS_CUSTOS_EMPRESA
primary key(EMPRESA_ID)
using index tablespace INDICES;

/* Chave estrangeira */
alter table PERCENTUAIS_CUSTOS_EMPRESA
add constraint FK_PERCENTUAIS_CUST_EMP_EMP_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

/* Constraints */
alter table PERCENTUAIS_CUSTOS_EMPRESA
add constraint CK_PERC_CUS_EMP_PERC_COMISSAO
check(PERCENTUAL_COMISSAO >= 0);

alter table PERCENTUAIS_CUSTOS_EMPRESA
add constraint CK_PERC_CUS_EMP_PERC_FRETE_VEN
check(PERCENTUAL_FRETE_VENDA >= 0);

alter table PERCENTUAIS_CUSTOS_EMPRESA
add constraint CK_PERC_CUS_EMP_PERC_CUSTO_FIX
check(PERCENTUAL_CUSTO_FIXO >= 0);

alter table PERCENTUAIS_CUSTOS_EMPRESA
add constraint CK_PERC_CUS_EMP_PERCENTUAL_PIS
check(PERCENTUAL_PIS >= 0);

alter table PERCENTUAIS_CUSTOS_EMPRESA
add constraint CK_PERC_CUS_EMP_PERCENT_COFINS
check(PERCENTUAL_COFINS >= 0);

alter table PERCENTUAIS_CUSTOS_EMPRESA
add constraint CK_PERC_CUS_EMP_PERCENT_CSLL
check(PERCENTUAL_CSLL >= 0);

alter table PERCENTUAIS_CUSTOS_EMPRESA
add constraint CK_PERC_CUS_EMP_PERCENT_IRPJ
check(PERCENTUAL_IRPJ >= 0);