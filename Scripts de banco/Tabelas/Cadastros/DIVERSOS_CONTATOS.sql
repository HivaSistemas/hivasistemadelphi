create table DIVERSOS_CONTATOS(
  CADASTRO_ID number(10),
  ORDEM       number(3),
  DESCRICAO   varchar2(60) not null,
  TELEFONE    varchar2(30),
  RAMAL       number(5),
  CELULAR     varchar2(30),
  E_MAIL      varchar2(80), 
  OBSERVACAO  varchar2(200),
  FUNCAO      varchar2(30),
  SEXO        char(1),
  DATA_NASCIMENTO date
);

alter table DIVERSOS_CONTATOS
add constraint PK_DIVERSOS_CONTATOS_ID
primary key(CADASTRO_ID, ORDEM)
using index tablespace INDICES;

alter table DIVERSOS_CONTATOS
add constraint CK_DIVERSOS_CONTATOS
check(TELEFONE is not null or CELULAR is not null or E_MAIL is not null);

alter table DIVERSOS_CONTATOS
add constraint CK_DIVERSOS_CONTATOS_SEXO
check(SEXO in('M', 'F', null));