create table INDICES_DESCONTOS_VENDA(
  INDICE_ID                 number(1) not null,
  DESCRICAO                 varchar2(3) not null,
  PRECO_CUSTO               char(1) default 'N' not null,
  TIPO_CUSTO                char(1) default 'N' not null,
  TIPO_DESCONTO_PRECO_CUSTO char(1) default 'N' not null,
  PERCENTUAL_DESCONTO       number(4,2) default 0 not null,
  ATIVO                     char(1) default 'S' not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table INDICES_DESCONTOS_VENDA
add constraint PK_INDICES_DESCONTOS_VENDA
primary key(INDICE_ID)
using index tablespace INDICES;

/* Checagens */
alter table INDICES_DESCONTOS_VENDA
add constraint CK_IND_DESC_VENDA_PRECO_CUSTO
check(PRECO_CUSTO in('S', 'N'));

alter table INDICES_DESCONTOS_VENDA
add constraint CK_IND_DESC_VENDA_PERC_DESCONT
check(PERCENTUAL_DESCONTO between 0 and 99.99);

/*
  TIPO_CUSTO
  N - Nenhum
  C - Pre�o de compra
  F - Pre�o l�quido
  M - CMV
*/
alter table INDICES_DESCONTOS_VENDA
add constraint CK_IND_DESC_VENDA_TIPO_CUSTO
check(
  PRECO_CUSTO = 'N' and TIPO_CUSTO = 'N' and TIPO_DESCONTO_PRECO_CUSTO = 'N'
  or
  PRECO_CUSTO = 'S' and TIPO_CUSTO in('C', 'F', 'M')
);

/*
  TIPO_DESCONTO_PRECO_CUSTO
  N - Nenhum
  A - Aumentar
  D - Diminuir
*/
alter table INDICES_DESCONTOS_VENDA
add constraint CK_IND_DESC_VENDA_TP_DESC_PR
check(
  PRECO_CUSTO = 'N' and TIPO_DESCONTO_PRECO_CUSTO = 'N'
  or
  PRECO_CUSTO = 'S' and TIPO_DESCONTO_PRECO_CUSTO in('N', 'A', 'D')
);