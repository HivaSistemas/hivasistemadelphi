create table CLIENTES_ORIGENS(
  CLIENTE_ORIGEM_ID   number(3,0),
  NOME                varchar2(30) not null,
  
  /*Logs do sistema*/
  USUARIO_SESSAO_ID   number(4,0) not null,
  DATA_HORA_ALTERACAO date not null,
  ROTINA_ALTERACAO    varchar2(30) not null,
  ESTACAO_ALTERACAO   varchar2(30) not null
);

/* Chave primária */
alter table CLIENTES_ORIGENS
add constraint PK_CLIENTES_ORIGENS
primary key(CLIENTE_ORIGEM_ID)
using index tablespace INDICES;

alter table CLIENTES_ORIGENS
add constraint UN_CLIENTES_ORIGENS_NOME
unique (NOME);  