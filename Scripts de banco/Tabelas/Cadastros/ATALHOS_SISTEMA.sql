create table ATALHOS_SISTEMA(
  ATALHO_SISTEMA_ID    number(3),
  ATALHO               varchar2(30) not null,
  DESCRICAO            varchar2(200) not null
);

/* Chave prim�ria */
alter table ATALHOS_SISTEMA
add constraint PK_ATALHO_SISTEMA_ID
primary key(ATALHO_SISTEMA_ID)
using index tablespace INDICES;
