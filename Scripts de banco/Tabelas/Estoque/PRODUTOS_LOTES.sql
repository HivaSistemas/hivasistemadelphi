create table PRODUTOS_LOTES(
  PRODUTO_ID       number(10),
  LOTE             varchar2(80) not null,
  DATA_VENCIMENTO  date,   
  DATA_FABRICACAO  date,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Primary key */
alter table PRODUTOS_LOTES
add constraint PK_PRODUTOS_LOTES
primary key(PRODUTO_ID, LOTE)
using index tablespace INDICES;

/* Foreign keys */
alter table PRODUTOS_LOTES
add constraint FK_PRODUTOS_LOT_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);