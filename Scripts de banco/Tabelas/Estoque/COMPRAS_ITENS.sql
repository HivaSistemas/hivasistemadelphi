create table COMPRAS_ITENS(
  COMPRA_ID           number(10) not null,
  ITEM_ID             number(3) not null,
  PRODUTO_ID          number(10) not null,
  
  MULTIPLO_COMPRA     number(20,4) not null,
  UNIDADE_COMPRA_ID   varchar2(5) not null,
  QUANTIDADE_EMBALAGEM number(20,4) default 1 not null,
  
  QUANTIDADE            number(20,4) not null,
  PRECO_UNITARIO        number(8,2) not null,
  VALOR_TOTAL           number(8,2) not null,
  VALOR_DESCONTO        number(8,2) default 0 not null,
  VALOR_OUTRAS_DESPESAS number(8,2) default 0 not null,
  VALOR_LIQUIDO         number(8,2) not null,

  PRECO_FINAL           number(8,2) default 0 not null,
  
  ENTREGUES             number(20,4) default 0 not null,
  BAIXADOS              number(20,4) default 0 not null,
  CANCELADOS            number(20,4) default 0 not null,
  SALDO                 number(20,4) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table COMPRAS_ITENS
add constraint PK_COMPRAS_ITENS
primary key(COMPRA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangerias */
alter table COMPRAS_ITENS
add constraint FK_COMPRAS_ITENS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* Checagens */
alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_ITEM_ID
check(ITEM_ID > 0);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_QUANTIDADE
check(
  QUANTIDADE > 0
  and 
  QUANTIDADE = ENTREGUES + BAIXADOS + CANCELADOS + SALDO
);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_PRECO_UNIT
check(PRECO_UNITARIO > 0);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_VALOR_TOTAL
check(VALOR_TOTAL > 0);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_VALOR_DESCONT
check(VALOR_DESCONTO >= 0);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_VAL_OUT_DESP
check(VALOR_OUTRAS_DESPESAS >= 0);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_VALOR_LIQUIDO
check(VALOR_LIQUIDO >= 0);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_ENTREGUES
check(ENTREGUES >= 0);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_BAIXADOS
check(BAIXADOS >= 0);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_CANCELADOS
check(CANCELADOS >= 0);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_SALDO
check(SALDO = QUANTIDADE - ENTREGUES - BAIXADOS - CANCELADOS);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_QTDE_EMBALAG
check(QUANTIDADE_EMBALAGEM > 0);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_MULTIPLO_COM
check(MULTIPLO_COMPRA > 0);

alter table COMPRAS_ITENS
add constraint CK_COMPRAS_ITENS_PRECO_FINAL
check(PRECO_FINAL > 0);
