create table ENTRADAS_NF_ITENS_COMPRAS(
  ENTRADA_ID                  number(12) not null,  
  ITEM_ID                     number(3) not null,
  COMPRA_ID                   number(10) not null,
  ITEM_COMPRA_ID              number(3) not null,
  QUANTIDADE                  number(20,4) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */  
alter table ENTRADAS_NF_ITENS_COMPRAS
add constraint PK_ENTRADAS_NF_ITENS_COMPRAS
primary key(ENTRADA_ID, COMPRA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ENTRADAS_NF_ITENS_COMPRAS
add constraint FK_ENTR_NF_ITE_COMP_ENTRADA_ID
foreign key(ENTRADA_ID)
references ENTRADAS_NOTAS_FISCAIS(ENTRADA_ID);

alter table ENTRADAS_NF_ITENS_COMPRAS
add constraint FK_ENTR_NF_ITE_COMP_ITEM_ID
foreign key(ENTRADA_ID, ITEM_ID)
references ENTRADAS_NOTAS_FISCAIS_ITENS(ENTRADA_ID, ITEM_ID);

alter table ENTRADAS_NF_ITENS_COMPRAS
add constraint FK_ENT_NF_ITE_CO_COMPRA_ITE_ID
foreign key(COMPRA_ID, ITEM_COMPRA_ID)
references COMPRAS_ITENS(COMPRA_ID, ITEM_ID);

/* Constraints */
alter table ENTRADAS_NF_ITENS_COMPRAS
add constraint CK_ENT_NF_ITE_CO_QUANTIDADE
check(QUANTIDADE > 0);