create table ENTRADAS_NOTAS_FISCAIS_ITENS(
  ENTRADA_ID                  number(12) not null,
  PRODUTO_ID                  number(10) not null,
  ITEM_ID                     number(3) not null,    
  CFOP_ID                     varchar2(7) not null,
  LOCAL_ENTRADA_ID            number(5) not null,
  CST                         char(2) not null,
  CODIGO_NCM                  varchar2(8),
  VALOR_TOTAL                 number(8,2) not null,
  PRECO_UNITARIO              number(8,2) not null,
  QUANTIDADE                  number(20,4) not null,
  UNIDADE_ENTRADA_ID          varchar2(6) default 'UND' not null, /* Guarda a unidade comercial que veio na nota fiscal de entrada */
  VALOR_TOTAL_DESCONTO        number(8,2) not null,
  VALOR_TOTAL_OUTRAS_DESPESAS number(8,2) not null,
  CODIGO_BARRAS               varchar2(14),
  ORIGEM_PRODUTO              number(1) default 0 not null,

  QUANTIDADE_EMBALAGEM        number(20,4) default 1 not null,
  MULTIPLO_COMPRA             number(20,4) default 1 not null,
  UNIDADE_COMPRA_ID           varchar2(6) default 'UND' not null, /* Guarda a unidade que ser� utilizada no Hiva para venda */

  QUANTIDADE_ENTRADA_ALTIS    number(20,4) not null,
  
  /* ICMS NORMAL */
  INDICE_REDUCAO_BASE_ICMS    number(5,4) not null,
  BASE_CALCULO_ICMS           number(8,2) not null,
  PERCENTUAL_ICMS             number(4,2) not null,
  VALOR_ICMS                  number(8,2) not null,    
  /* ------------------------------------------------ */
  
  /* ST */
  INDICE_REDUCAO_BASE_ICMS_ST number(5,4) not null,
  BASE_CALCULO_ICMS_ST        number(8,2) not null,
  PERCENTUAL_ICMS_ST          number(4,2) not null,
  VALOR_ICMS_ST               number(8,2) not null,
  IVA                         number(4,2) default 0 not null,
  /* ------------------------------------------------ */
  
  /* PIS */
  CST_PIS                     char(2) default '99' not null,
  BASE_CALCULO_PIS            number(8,2) not null,
  PERCENTUAL_PIS              number(4,2) not null,
  VALOR_PIS                   number(8,2) not null,
  /* ------------------------------------------------ */
  
  /* COFINS */
  CST_COFINS                  char(2) default '99' not null,
  BASE_CALCULO_COFINS         number(8,2) not null,
  PERCENTUAL_COFINS           number(4,2) not null,
  VALOR_COFINS                number(8,2) not null,
  /* ----------------------------------------------- */
  
  /* IPI */
  VALOR_IPI                   number(8,2) not null,
  PERCENTUAL_IPI              number(4,2) not null,
  /* ----------------------------------------------- */

  PESO_UNITARIO               number(8,3) default 0 not null,

  PRECO_PAUTA                 number(8,2) default 0 not null,

  INFORMACOES_ADICIONAIS      varchar2(500),

  /* CUSTOS */
  VALOR_FRETE_CUSTO           number(12,6) default 0 not null,
  VALOR_IPI_CUSTO             number(12,6) default 0 not null,
  VALOR_ICMS_ST_CUSTO         number(12,6) default 0 not null,
  VALOR_OUTROS_CUSTO          number(12,6) default 0 not null,
  VALOR_DIFAL_CUSTO           number(12,6) default 0 not null,
  PRECO_FINAL                 number(12,6) not null, /* Total pago pelo item, custo pago pela entrada */

  VALOR_ICMS_CUSTO            number(12,6) default 0 not null,
  VALOR_ICMS_FRETE_CUSTO      number(12,6) default 0 not null,
  VALOR_PIS_CUSTO             number(12,6) default 0 not null,
  VALOR_COFINS_CUSTO          number(12,6) default 0 not null,
  VALOR_PIS_FRETE_CUSTO       number(12,6) default 0 not null,
  VALOR_COFINS_FRETE_CUSTO    number(12,6) default 0 not null,
  PRECO_LIQUIDO               number(12,6) not null, /* Total pago menos os cr�ditos de impostos */

  ENTRADA_ANTERIOR_ID             number(12),
  PRECO_TABELA_ANTERIOR           number(9,4) default 0 not null,
  PERCENTUAL_DESCONTO_ENTR_ANT    number(8,5) default 0 not null,
  PERCENTUAL_IPI_ENTRADA_ANT      number(8,5) default 0 not null,
  PERCENTUAL_OUTRAS_DESP_ENT_ANT  number(8,5) default 0 not null,
  PERCENTUAL_FRETE_ENTR_ANTERIOR  number(8,5) default 0 not null,
  PERCENTUAL_ST_ENTRADA_ANTERIOR  number(9,5) default 0 not null,
  PERCENTUAL_ICMS_ENTR_ANTERIOR   number(8,5) default 0 not null,
  PERCENTUAL_ICMS_FRETE_ENTR_ANT  number(8,5) default 0 not null,
  PERCENTUAL_PIS_ENTRADA_ANTER    number(8,5) default 0 not null,
  PERCENTUAL_COFINS_ENT_ANTERIOR  number(8,5) default 0 not null,
  PERCENTUAL_PIS_FRETE_ENT_ANT    number(8,5) default 0 not null,
  PERCENTUAL_COF_FRETE_ENT_ANT    number(8,5) default 0 not null,
  PERCENTUAL_OUT_CUS_ENTR_ANTER   number(8,5) default 0 not null,
  PERCENTUAL_CUSTO_FIXO_ENT_ANT   number(8,5) default 0 not null,
  PERCENTUAL_DIFAL_ENTRADA_ANTER  number(8,5) default 0 not null,

  PRECO_FINAL_ANTERIOR            number(9,4) default 0 not null,
  PRECO_FINAL_MEDIO_ANTERIOR      number(9,4) default 0 not null,

  PRECO_LIQUIDO_ANTERIOR          number(9,4) default 0 not null,
  PRECO_LIQUIDO_MEDIO_ANTERIOR    number(9,4) default 0 not null,

  CMV_ANTERIOR                    number(9,4) default 0 not null

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint PK_ENTRADAS_NOTAS_FISCAIS_ITE
primary key(ENTRADA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */ 
alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint FK_ENT_NOT_FISC_ITE_ENTRADA_ID
foreign key(ENTRADA_ID)
references ENTRADAS_NOTAS_FISCAIS(ENTRADA_ID);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint FK_ENT_NOT_FISC_ITE_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint FK_ENT_NOT_FISC_ITE_CFOP_ID
foreign key(CFOP_ID)
references CFOP(CFOP_PESQUISA_ID);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint FK_ENT_NOT_FISC_LOCAL_ENTR_ID
foreign key(LOCAL_ENTRADA_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint FK_ENT_NF_ITENS_ENT_ANT_ID
foreign key(ENTRADA_ANTERIOR_ID)
references ENTRADAS_NOTAS_FISCAIS(ENTRADA_ID);

/* Constraints */
alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_FISC_ITE_PRECO_UNIT
check(PRECO_UNITARIO > 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_FISC_ITE_QUANTIDADE
check(QUANTIDADE > 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_FISC_ITE_VALOR_TOT
check(VALOR_TOTAL > 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_FISC_VALOR_TOT_DESC
check(VALOR_TOTAL_DESCONTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NF_ITE_VLR_TOT_OUT_DESP
check(VALOR_TOTAL_OUTRAS_DESPESAS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NF_ITE_BASE_CALC_ICMS
check(BASE_CALCULO_ICMS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_FISC_ITE_VALOR_ICMS
check(VALOR_ICMS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NF_ITE_BAS_CALC_ICMS_ST
check(BASE_CALCULO_ICMS_ST >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NF_ITE_VALOR_ICMS_ST
check(VALOR_ICMS_ST >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NF_ITE_BASE_CALC_PIS
check(BASE_CALCULO_PIS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_FISC_ITE_VALOR_PIS
check(VALOR_PIS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NF_ITE_BASE_CALC_COFINS
check(BASE_CALCULO_COFINS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_FISC_ITE_VALOR_COF
check(VALOR_COFINS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_FISC_ITE_VALOR_IPI
check(VALOR_IPI >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_FISC_ITE_PRECO_PAUT
check(PRECO_PAUTA >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_F_ITE_VLR_FRET_CUS
check(VALOR_FRETE_CUSTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_F_ITE_VLR_IPI_CUS
check(VALOR_IPI_CUSTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_F_ITE_VLR_ICMS_ST_C
check(VALOR_ICMS_ST_CUSTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_F_ITE_VLR_OUT_CUS
check(VALOR_OUTROS_CUSTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_F_ITE_VLR_DIF_CUS
check(VALOR_DIFAL_CUSTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_F_ITE_VLR_ICMS_CUS
check(VALOR_ICMS_CUSTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_FISC_ITE_PESO_UNIT
check(PESO_UNITARIO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_FIS_ITENS_CST
check(CST in('00','10','20','30','40','41','50','51','60','70','90'));

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENT_NOT_FIS_ITE_CST_PIS_COF
check(
  CST_PIS in('01','02','03','04','05','06','07','08','09','49','50','51','52','53','54','55','56','60','61','62','63','64','65','66','67','70','71','72','73','74','75','98','99')
  and
  CST_COFINS in('01','02','03','04','05','06','07','08','09','49','50','51','52','53','54','55','56','60','61','62','63','64','65','66','67','70','71','72','73','74','75','98','99')
);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENTRADAS_NF_ITENS_PERC_TRIB
check(
  (PERCENTUAL_ICMS between 0 and 99.99)
  and
  (PERCENTUAL_COFINS between 0 and 99.99)
  and
  (PERCENTUAL_IPI between 0 and 99.99)
);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENTRADAS_NF_ITENS_QTDE_EMB
check(QUANTIDADE_EMBALAGEM > 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint CK_ENTRADAS_NF_ITENS_MULT_COMP
check(MULTIPLO_COMPRA > 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint ENT_NF_ITE_IND_RED_BASE_ICMS
check(INDICE_REDUCAO_BASE_ICMS > 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint ENT_NF_ITE_IND_R_BASE_ICMS_ST
check(INDICE_REDUCAO_BASE_ICMS_ST > 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint ENT_NF_ITE_VALOR_PIS_FR_CUSTO
check(VALOR_PIS_FRETE_CUSTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint ENT_NF_ITE_VALOR_COF_FR_CUSTO
check(VALOR_COFINS_FRETE_CUSTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint ENT_NF_ITE_VALOR_ICMS_FR_CUSTO
check(VALOR_ICMS_FRETE_CUSTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint ENT_NF_ITE_PRECO_FINAL
check(PRECO_FINAL >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint ENT_NF_ITE_PRECO_LIQUIDO
check(PRECO_LIQUIDO <= PRECO_FINAL);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint ENT_NF_ITE_VALOR_PIS_CUSTO
check(VALOR_PIS_CUSTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint ENT_NF_ITE_VALOR_COFINS_CUSTO
check(VALOR_COFINS_CUSTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS_ITENS
add constraint ENT_NF_ITE_QTDE_ENT_ALTIS
check(QUANTIDADE_ENTRADA_ALTIS > 0);
