create table PRODUCAO_ESTOQUE(
  PRODUCAO_ESTOQUE_ID   number(10),
  EMPRESA_ID            number(3) not null,
  DATA_HORA_PRODUCAO    date not null,
  USUARIO_PRODUCAO_ID   number(4) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table PRODUCAO_ESTOQUE
add constraint PK_PRODUCAO_ESTOQUE_ID
primary key(PRODUCAO_ESTOQUE_ID)
using index tablespace INDICES;

/* Chaves estrangeias */
alter table PRODUCAO_ESTOQUE
add constraint FK_PRODUCAO_ESTOQUE_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table PRODUCAO_ESTOQUE
add constraint FK_PRODUCA_EST_USUARIO_AJUS_ID
foreign key(USUARIO_PRODUCAO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);
