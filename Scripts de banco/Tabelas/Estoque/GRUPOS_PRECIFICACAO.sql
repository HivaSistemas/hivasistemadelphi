create table GRUPOS_PRECIFICACAO(
  EMPRESA_ID        number(3) not null,
  EMPRESA_GRUPO_ID  number(3) not null,
  ORDEM             number(2) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Primary key */
alter table GRUPOS_PRECIFICACAO
add constraint PK_GRUPOS_PRECIFICACAO
primary key(EMPRESA_ID, EMPRESA_GRUPO_ID)
using index tablespace INDICES;

/* Foreigns keys */
alter table GRUPOS_PRECIFICACAO
add constraint FK_GRUPOS_PRE_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table GRUPOS_PRECIFICACAO
add constraint FK_GRUPOS_PRE_EMPRESA_GRUPO_ID
foreign key(EMPRESA_GRUPO_ID)
references EMPRESAS(EMPRESA_ID);
