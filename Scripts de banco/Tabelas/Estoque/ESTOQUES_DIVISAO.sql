create table ESTOQUES_DIVISAO(
  EMPRESA_ID              number(3) not null,
  PRODUTO_ID              number(10) not null,
  LOTE                    varchar2(80) not null,
  LOCAL_ID                number(5,0) not null,
  
  FISICO                  number(20,4) default 0 not null,
  DISPONIVEL              number(20,4) default 0 not null,
  RESERVADO               number(20,4) default 0 not null,
  BLOQUEADO               number(20,4) default 0 not null,

  PONTA_ESTOQUE           char(1) default 'N' not null,
  MULTIPLO_PONTA_ESTOQUE  number(7,3) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Primary key */
alter table ESTOQUES_DIVISAO
add constraint PK_ESTOQUES_DIVISAO
primary key(EMPRESA_ID, LOCAL_ID, PRODUTO_ID, LOTE)
using index tablespace INDICES;

/* Foreign keys */
alter table ESTOQUES_DIVISAO
add constraint FK_ESTOQUES_DIV_EMP_ID_PROD_ID
foreign key(EMPRESA_ID, PRODUTO_ID)
references ESTOQUES(EMPRESA_ID, PRODUTO_ID);

alter table ESTOQUES_DIVISAO
add constraint FK_ESTOQUES_DIVISAO_LOTE
foreign key(PRODUTO_ID, LOTE)
references PRODUTOS_LOTES(PRODUTO_ID, LOTE);

/* Checagens */
alter table ESTOQUES_DIVISAO
add constraint CK_EST_DIVISAO_PONTA_ESTOQUE
check(PONTA_ESTOQUE in('S', 'N'));

alter table ESTOQUES_DIVISAO
add constraint CK_EST_DIVISAO_MULT_PONTA_EST
check(
  PONTA_ESTOQUE = 'N' and MULTIPLO_PONTA_ESTOQUE = 0
  or
  PONTA_ESTOQUE = 'S' and MULTIPLO_PONTA_ESTOQUE >= 0
);

alter table ESTOQUES_DIVISAO
add constraint CK_EST_DIVISAO_RESERVADO
check(RESERVADO >= 0);

alter table ESTOQUES_DIVISAO
add constraint CK_EST_DIVISAO_BLOQUEADO
check(BLOQUEADO >= 0);

alter table ESTOQUES_DIVISAO
add constraint CK_EST_DIVISAO_FISICO
check(
  FISICO >= DISPONIVEL
  and
  FISICO >= BLOQUEADO
  and
  FISICO >= RESERVADO
);
