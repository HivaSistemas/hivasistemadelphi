﻿create table BLOQUEIOS_ESTOQUES_ITENS(
  BLOQUEIO_ID         number(12) not null,
  ITEM_ID             number(3) not null,
  PRODUTO_ID          number(10) not null,
  LOTE                varchar2(80) default '???' not null,
  LOCAL_ID            number(5) not null,
  QUANTIDADE          number(20,4) not null,
  BAIXADOS            number(20,4) default 0 not null,
  CANCELADOS          number(20,4) default 0 not null,
  SALDO               number(20,4) default 0 not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table BLOQUEIOS_ESTOQUES_ITENS
add constraint PK_BLOQUEIOS_ESTOQUES_ITENS
primary key(BLOQUEIO_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrageiras */
alter table BLOQUEIOS_ESTOQUES_ITENS
add constraint FK_BLOQUEIOS_EST_ITENS_BLOQ_ID
foreign key(BLOQUEIO_ID)
references BLOQUEIOS_ESTOQUES(BLOQUEIO_ID);

alter table BLOQUEIOS_ESTOQUES_ITENS
add constraint FK_BLOQUEIOS_EST_ITENS_PROD_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table BLOQUEIOS_ESTOQUES_ITENS
add constraint FK_BLOQUEIOS_EST_ITENS_LOC_ID
foreign key(LOCAL_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table BLOQUEIOS_ESTOQUES_ITENS
add constraint FK_BLOQUEIOS_EST_ITE_PRO_LOTE
foreign key(PRODUTO_ID, LOTE)
references PRODUTOS_LOTES(PRODUTO_ID, LOTE);

/* Checagens */
alter table BLOQUEIOS_ESTOQUES_ITENS
add constraint CK_BLOQ_EST_ITENS_ITEM_ID
check(ITEM_ID > 0);

alter table BLOQUEIOS_ESTOQUES_ITENS
add constraint CK_BLOQ_EST_ITENS_QUANTIDADE
check(QUANTIDADE > 0);

alter table BLOQUEIOS_ESTOQUES_ITENS
add constraint CK_BLOQ_EST_ITENS_BAIXADOS
check(BAIXADOS >= 0);

alter table BLOQUEIOS_ESTOQUES_ITENS
add constraint CK_BLOQ_EST_ITENS_CANCELADOS
check(CANCELADOS >= 0);

alter table BLOQUEIOS_ESTOQUES_ITENS
add constraint CK_BLOQ_EST_ITENS_SALDO
check(SALDO = QUANTIDADE - CANCELADOS - BAIXADOS);

/* Uniques */
alter table BLOQUEIOS_ESTOQUES_ITENS
add constraint UN_BLOQUEIOS_EST_ITENS_PRO_LOT
unique(BLOQUEIO_ID, PRODUTO_ID, LOTE)
using index tablespace INDICES;