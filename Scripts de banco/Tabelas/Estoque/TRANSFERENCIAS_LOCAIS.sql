create table TRANSFERENCIAS_LOCAIS(
  TRANSFERENCIA_LOCAL_ID number(10) not null,
  EMPRESA_ID             number(3) not null,
  LOCAL_ORIGEM_ID        number(3) not null,
  OBSERVACAO             varchar2(200),
  DATA_HORA_CADASTRO     date not null,
  USUARIO_CADASTRO_ID    number(4) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table TRANSFERENCIAS_LOCAIS
add constraint PK_TRANSF_LOCAIS_TRANSF_LOC_ID
primary key(TRANSFERENCIA_LOCAL_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table TRANSFERENCIAS_LOCAIS
add constraint FK_TRANSFERE_LOCAIS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table TRANSFERENCIAS_LOCAIS
add constraint FK_TRANSF_LOC_LOCAL_ORIGEM_ID
foreign key(LOCAL_ORIGEM_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table TRANSFERENCIAS_LOCAIS
add constraint FK_TRANSFER_LOCAIS_USU_CAD_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);