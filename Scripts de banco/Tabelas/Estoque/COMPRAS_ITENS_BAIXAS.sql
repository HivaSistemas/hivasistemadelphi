create table COMPRAS_ITENS_BAIXAS(
  BAIXA_ID              number(10) not null,  
  ITEM_ID               number(3) not null,
  QUANTIDADE            number(20,4) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table COMPRAS_ITENS_BAIXAS
add constraint PK_COMPRAS_ITENS_BAIXAS
primary key(BAIXA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangerias */
alter table COMPRAS_ITENS_BAIXAS
add constraint FK_COMPRAS_ITENS_BX_LOCAL_ID
foreign key(BAIXA_ID)
references COMPRAS_BAIXAS(BAIXA_ID);

/* Checagens */
alter table COMPRAS_ITENS_BAIXAS
add constraint CK_COMPRAS_ITENS_BAIXAS_QUANT
check(QUANTIDADE > 0);