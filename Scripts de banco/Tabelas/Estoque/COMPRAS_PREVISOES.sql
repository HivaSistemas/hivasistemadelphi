create table COMPRAS_PREVISOES(
  COMPRA_ID             number(10) not null,
  ITEM_ID               number(3) not null,
  COBRANCA_ID           number(3,0) not null,
  
  VALOR                 number(8,2) not null,
  PARCELA               number(2) not null,
  NUMERO_PARCELAS       number(2) not null,  
  VENCIMENTO            date not null,
  DOCUMENTO             varchar2(20) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table COMPRAS_PREVISOES
add constraint PK_COMPRAS_PREVISOES
primary key(COMPRA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangerias */
alter table COMPRAS_PREVISOES
add constraint FK_COMPRAS_PREVISOES_COMPRA_ID
foreign key(COMPRA_ID)
references COMPRAS(COMPRA_ID);

alter table COMPRAS_PREVISOES
add constraint FK_COMPRAS_PREVIS_COBRANCA_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

/* Checagens */
alter table COMPRAS_PREVISOES
add constraint CK_COMPRAS_PREVISOES_ITEM_ID
check(ITEM_ID > 0);

alter table COMPRAS_PREVISOES
add constraint CK_COMPRAS_PREVISOES_VALOR
check(VALOR > 0);

alter table COMPRAS_PREVISOES
add constraint CK_COMPRAS_PREVISOES_PARCELA
check(PARCELA > 0);

alter table COMPRAS_PREVISOES
add constraint CK_COMPRAS_PREVISOES_NUM_PARC
check(NUMERO_PARCELAS > 0);