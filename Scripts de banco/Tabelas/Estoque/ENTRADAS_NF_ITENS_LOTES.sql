﻿create table ENTRADAS_NF_ITENS_LOTES(
  ENTRADA_ID      number(12) not null,
  ITEM_ID         number(3)  not null,
  LOTE            varchar2(80) not null,
  QUANTIDADE      number(20,4) not null,

  DATA_FABRICACAO date,
  DATA_VENCIMENTO date,


  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table ENTRADAS_NF_ITENS_LOTES
add constraint PK_ENTRADAS_NF_ITENS_LOTES
primary key(ENTRADA_ID, ITEM_ID, LOTE)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ENTRADAS_NF_ITENS_LOTES
add constraint FK_ENT_NF_ITE_LOTES_ENT_ITE_ID
foreign key(ENTRADA_ID, ITEM_ID)
references ENTRADAS_NOTAS_FISCAIS_ITENS(ENTRADA_ID, ITEM_ID);

/* Checagens */
alter table ENTRADAS_NF_ITENS_LOTES
add constraint CK_ENTRADAS_NF_ITE_LOTES_QTDE
check(QUANTIDADE > 0);