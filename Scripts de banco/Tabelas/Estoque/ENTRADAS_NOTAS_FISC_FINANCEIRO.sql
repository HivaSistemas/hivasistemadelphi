create table ENTRADAS_NOTAS_FISC_FINANCEIRO(
  ENTRADA_ID        number(12),
  ITEM_ID           number(3) not null,
  COBRANCA_ID       number(3) not null,

  DATA_VENCIMENTO   date not null,
  PARCELA           number(3) not null,
  NUMERO_PARCELAS   number(3) not null,
  VALOR_DOCUMENTO   number(8,2) not null,
  
  NOSSO_NUMERO     varchar2(30),
  CODIGO_BARRAS    varchar2(44),
  DOCUMENTO        varchar2(20) not null,

  TIPO                char(1) default 'N' not null,
  FORNECEDOR_ID       number(10),
  PLANO_FINANCEIRO_ID varchar2(9),
  CENTRO_CUSTO_ID     number(3),

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table ENTRADAS_NOTAS_FISC_FINANCEIRO
add constraint PK_ENT_NOTAS_FISC_FINANC_ID
primary key(ENTRADA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ENTRADAS_NOTAS_FISC_FINANCEIRO
add constraint FK_ENT_NF_FIN_ENTRADA_ID
foreign key(ENTRADA_ID)
references ENTRADAS_NOTAS_FISCAIS(ENTRADA_ID);

alter table ENTRADAS_NOTAS_FISC_FINANCEIRO
add constraint FK_ENT_NOTAS_FISC_FIN_COBR_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

alter table ENTRADAS_NOTAS_FISC_FINANCEIRO
add constraint FK_ENT_NOTAS_FISC_FIN_FORN_ID
foreign key(FORNECEDOR_ID)
references FORNECEDORES(CADASTRO_ID);

alter table ENTRADAS_NOTAS_FISC_FINANCEIRO
add constraint FK_ENT_NOTAS_FIN_PLAN_FIN_ID
foreign key(PLANO_FINANCEIRO_ID)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

alter table ENTRADAS_NOTAS_FISC_FINANCEIRO
add constraint FK_ENT_NOTAS_FIN_CENTRO_CUS_ID
foreign key(CENTRO_CUSTO_ID)
references CENTROS_CUSTOS(CENTRO_CUSTO_ID);

/* Constraints */
alter table ENTRADAS_NOTAS_FISC_FINANCEIRO
add constraint CK_ENT_NOTAS_FISC_PARCELA
check(PARCELA > 0);

alter table ENTRADAS_NOTAS_FISC_FINANCEIRO
add constraint CK_ENT_NOTAS_FISC_NUM_PARC
check(NUMERO_PARCELAS >= PARCELA);

alter table ENTRADAS_NOTAS_FISC_FINANCEIRO
add constraint CK_ENT_NOTAS_FISC_VALOR_DOC
check(VALOR_DOCUMENTO > 0);

/*
  TIPO
  N - Normal
  O - Outros custos
*/
alter table ENTRADAS_NOTAS_FISC_FINANCEIRO
add constraint CK_ENT_NOTAS_FISC_TIPO
check(
  TIPO = 'N' and FORNECEDOR_ID is null and PLANO_FINANCEIRO_ID is null and CENTRO_CUSTO_ID is null
  or
  TIPO = 'O' and FORNECEDOR_ID is not null and PLANO_FINANCEIRO_ID is not null and CENTRO_CUSTO_ID is not null
);
