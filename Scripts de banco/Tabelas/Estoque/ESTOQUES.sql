create table ESTOQUES(
  EMPRESA_ID              number(3) not null,
  PRODUTO_ID              number(10) not null,
  
  ESTOQUE                 number(20,4) default 0 not null, /* Estoque conceitual */

  FISICO                  number(20,4) default 0 not null,
  DISPONIVEL              number(20,4) default 0 not null,
  RESERVADO               number(20,4) default 0 not null,
  BLOQUEADO               number(20,4) default 0 not null

  COMPRAS_PENDENTES       number(20,4) default 0 not null,

  ULTIMA_ENTRADA_ID       number(12),
  PENULTIMA_ENTRADA_ID    number(12),

  MINIMO                  number(20,4) default 0 not null,
  MAXIMO                  number(20,4) default 0 not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

alter table ESTOQUES
add constraint PK_ESTOQUES
primary key(PRODUTO_ID, EMPRESA_ID)
using index tablespace INDICES;

/* Foreign keys */
alter table ESTOQUES
add constraint FK_ESTOQUES_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table ESTOQUES
add constraint FK_ESTOQUES_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table ESTOQUES
add constraint FK_ESTOQUES_ULTIMA_ENTRADA_ID
foreign key(ULTIMA_ENTRADA_ID)
references ENTRADAS_NOTAS_FISCAIS(ENTRADA_ID);

alter table ESTOQUES
add constraint FK_ESTOQUES_PENULTIMA_ENTRA_ID
foreign key(PENULTIMA_ENTRADA_ID)
references ENTRADAS_NOTAS_FISCAIS(ENTRADA_ID);

/* Constraints */
alter table ESTOQUES
add constraint CK_ESTOQUES_FISICO
check(FISICO >= DISPONIVEL + RESERVADO + BLOQUEADO);

alter table ESTOQUES
add constraint CK_ESTOQUES_RESERVADO
check(RESERVADO >= 0);

alter table ESTOQUES
add constraint CK_ESTOQUES_BLOQUEADO
check(BLOQUEADO >= 0);

alter table ESTOQUES
add constraint CK_ESTOQUES_COMPRAS_PENDENTES
check(COMPRAS_PENDENTES >= 0);

alter table ESTOQUES
add constraint CK_ESTOQUES_MINIMO_MAXIMO
check(
  MINIMO >= 0
  and
  MAXIMO >= 0
  and
  MAXIMO >= MINIMO
);

/* Indices */
create index IDX_ESTOQUES_EMPRESA_ID
on ESTOQUES
(EMPRESA_ID)
tablespace INDICES;

create index IDX_ESTOQUES_PRODUTO_ID
on ESTOQUES
(PRODUTO_ID)
tablespace INDICES;
