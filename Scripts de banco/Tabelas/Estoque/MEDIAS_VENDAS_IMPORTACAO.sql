create table MEDIAS_VENDAS_IMPORTACAO(
  EMPRESA_ID      number(3) not null,
  PRODUTO_ID      number(10) not null,
  ANO_MES         number(6) not null,
  QUANTIDADE      number(20,4) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table MEDIAS_VENDAS_IMPORTACAO
add constraint PK_MEDIAS_VENDAS_IMPORTACAO
primary key(EMPRESA_ID, PRODUTO_ID, ANO_MES)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table MEDIAS_VENDAS_IMPORTACAO
add constraint FK_MEDIAS_VENDAS_IMPOR_EMP_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table MEDIAS_VENDAS_IMPORTACAO
add constraint FK_MEDIAS_VENDAS_IMPOR_PROD_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);