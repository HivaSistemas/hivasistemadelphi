create table BLOQUEIOS_ESTOQUES_BAIXAS(
  BAIXA_ID            number(12) not null,
  BLOQUEIO_ID         number(12) not null,
  
  TIPO                char(1) not null,
  
  USUARIO_BAIXA_ID    number(4) not null,
  DATA_HORA_BAIXA     date not null,
  
  OBSERVACOES         varchar2(800),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table BLOQUEIOS_ESTOQUES_BAIXAS
add constraint PK_BLOQUEIOS_ESTOQUES_BAIXAS
primary key(BAIXA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table BLOQUEIOS_ESTOQUES_BAIXAS
add constraint FK_BLOQ_EST_BAIXAS_BLOQUEIO_ID
foreign key(BLOQUEIO_ID)
references BLOQUEIOS_ESTOQUES(BLOQUEIO_ID);

/* Checagens */
/* 
  TIPO 
  B - Baixa
  C - Cancelamento
*/
alter table BLOQUEIOS_ESTOQUES_BAIXAS
add constraint CK_BLOQ_EST_BAIXAS_TIPO
check(TIPO in('B', 'C'));