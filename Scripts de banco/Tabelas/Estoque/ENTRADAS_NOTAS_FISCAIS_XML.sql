create table ENTRADAS_NOTAS_FISCAIS_XML(
  ENTRADA_ID     number(12) not null,
  XML_TEXTO      clob,
  XML            blob,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table ENTRADAS_NOTAS_FISCAIS_XML
add constraint PK_ENTRADAS_NOTAS_FISCAIS_XML
primary key(ENTRADA_ID)
using index tablespace INDICES;
