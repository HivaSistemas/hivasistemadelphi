create table AJUSTES_ESTOQUE(
  AJUSTE_ESTOQUE_ID     number(10),
  MOTIVO_AJUSTE_ID      number(3) not null,
  EMPRESA_ID            number(3) not null,
  OBSERVACAO            varchar2(200),
  TIPO                  char(3) default 'NOR' not null,
  PLANO_FINANCEIRO_ID   varchar2(9),
  DATA_HORA_AJUSTE      date not null,
  USUARIO_AJUSTE_ID     number(4) not null,
  VALOR_TOTAL           number(10,2) not null,
  BAIXA_COMPRA_ID       number(10),
  NOTA_FISCAL_ID        number(12),
  TIPO_ESTOQUE_AJUSTADO char(6) NOT NULL DEFAULT 'FISICO',

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table AJUSTES_ESTOQUE
add constraint PK_AJUSTES_ESTOQUE_ID
primary key(AJUSTE_ESTOQUE_ID)
using index tablespace INDICES;

/* Chaves estrangeias */
alter table AJUSTES_ESTOQUE
add constraint FK_AJUSTES_EST_MOTIVO_AJUS_ID
foreign key(MOTIVO_AJUSTE_ID)
references MOTIVOS_AJUSTE_ESTOQUE(MOTIVO_AJUSTE_ID);

alter table AJUSTES_ESTOQUE
add constraint FK_AJUSTES_ESTOQUE_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table AJUSTES_ESTOQUE
add constraint FK_AJUSTES_EST_USUARIO_AJUS_ID
foreign key(USUARIO_AJUSTE_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table AJUSTES_ESTOQUE
add constraint FK_AJUSTES_EST_PLANO_FIN_ID
foreign key(PLANO_FINANCEIRO_ID)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

/* Checagens */
/*
  TIPO
  NOR - Normal
  RCO - Rebimento de compras
*/
alter table AJUSTES_ESTOQUE
add constraint FK_AJUSTES_ESTOQUE_TIPO
check(
  TIPO = 'NOR'
  or
  TIPO = 'RCO' and BAIXA_COMPRA_ID is not null
);



alter table AJUSTES_ESTOQUE
add constraint CK_AJUSTES_EST_VALOR_TOTAL
check(VALOR_TOTAL > 0);
