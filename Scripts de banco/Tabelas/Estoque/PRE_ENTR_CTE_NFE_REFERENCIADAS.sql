create table PRE_ENTR_CTE_NFE_REFERENCIADAS(
  PRE_ENTRADA_ID                number(10) not null,
  ITEM_ID                       number(3) not null,
  CHAVE_ACESSO_NFE              varchar2(44) not null,
  
  /* Logs do sistema */
  USUARIO_SESSAO_ID        number(4) not null,
  DATA_HORA_ALTERACAO      date not null,
  ROTINA_ALTERACAO         varchar2(30) not null,
  ESTACAO_ALTERACAO        varchar2(30) not null
);

/* Chave primaria */
alter table PRE_ENTR_CTE_NFE_REFERENCIADAS
add constraint PK_PRE_ENTR_CTE_NFE_REFERENC
primary key(PRE_ENTRADA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PRE_ENTR_CTE_NFE_REFERENCIADAS
add constraint FK_PRE_ENT_CTE_NFE_REF_PR_E_ID
foreign key(PRE_ENTRADA_ID)
references PRE_ENTRADAS_CTE(PRE_ENTRADA_ID);