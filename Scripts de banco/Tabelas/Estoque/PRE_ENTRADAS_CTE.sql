create table PRE_ENTRADAS_CTE(
  PRE_ENTRADA_ID                number(10) not null,
  EMPRESA_ID                    number(3) not null,
  CHAVE_ACESSO_CTE              varchar2(44) not null,
  
  CFOP                          number(4) not null,
  
  CNPJ_REMETENTE                varchar2(19) not null,
  RAZAO_SOCIAL_REMETENTE        varchar2(120) not null,
  INSCRICAO_ESTADUAL_REMETENTE  varchar2(20) not null,
  
  CNPJ_EMITENTE                 varchar2(19) not null,
  RAZAO_SOCIAL_EMITENTE         varchar2(120) not null,
  INSCRICAO_ESTADUAL_EMITENTE   varchar2(20) not null,
  
  DATA_HORA_EMISSAO             date not null,
  NUMERO_CTE                    number(9) not null,
  SERIE_CTE                     varchar2(3) not null,
  VALOR_TOTAL_CTE               number(9,2) not null,  
  
  BASE_CALCULO_ICMS             number(8,2) default 0 not null,
  PERCENTUAL_ICMS               number(4,2) default 0 not null,
  VALOR_ICMS                    number(8,2) default 0 not null,

  STATUS                        char(3) default 'AEN' not null,
  STATUS_SEFAZ                  char(1) default '1' not null,
  CONHECIMENTO_ID               number(12),
  
  XML_TEXTO                     clob,
  
  /* Logs do sistema */
  USUARIO_SESSAO_ID        number(4) not null,
  DATA_HORA_ALTERACAO      date not null,
  ROTINA_ALTERACAO         varchar2(30) not null,
  ESTACAO_ALTERACAO        varchar2(30) not null
);

/* Chave primaria */
alter table PRE_ENTRADAS_CTE
add constraint PK_PRE_ENTRADAS_CTE_FISCAIS
primary key(PRE_ENTRADA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PRE_ENTRADAS_CTE
add constraint FK_PRE_ENTRADAS_CTE_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table PRE_ENTRADAS_CTE
add constraint FK_PRE_ENTRADAS_CTE_CONHEC_ID
foreign key(CONHECIMENTO_ID)
references CONHECIMENTOS_FRETES(CONHECIMENTO_ID);
/* Constraints */
/* 
  STATUS
  AEN - Aguardando entrada  
  ENT - Entrada realizada  
*/
alter table PRE_ENTRADAS_CTE
add constraint CK_PRE_ENTRADAS_CTE_STATUS
check(
  STATUS = 'AEN' and CONHECIMENTO_ID is null
  or
  STATUS = 'ENT' and CONHECIMENTO_ID is not null
);

/* 
  STATUS_SEFAZ
  1 - Uso autorizado
  2 - Denegada
  3 - Cancelada
*/
alter table PRE_ENTRADAS_CTE
add constraint CK_PRE_ENTRADAS_STATUS_SEFAZ
check(STATUS_SEFAZ in('1', '2', '3'));

/* Uniques */
alter table PRE_ENTRADAS_CTE
add constraint UN_PRE_ENT_CTE_CHAVE_ACESS_CTE
unique(CHAVE_ACESSO_CTE)
using index tablespace INDICES;
