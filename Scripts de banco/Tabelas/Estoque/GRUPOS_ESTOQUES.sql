create table GRUPOS_ESTOQUES(
  EMPRESA_ID        number(3) not null,
  EMPRESA_GRUPO_ID  number(3) not null,
  ORDEM             number(2) not null,
  TIPO              char(1),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Primary key */
alter table GRUPOS_ESTOQUES
add constraint PK_GRUPOS_ESTOQUES
primary key(EMPRESA_ID, EMPRESA_GRUPO_ID, TIPO)
using index tablespace INDICES;

/* Foreigns keys */
alter table GRUPOS_ESTOQUES
add constraint FK_GRUPOS_EST_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table GRUPOS_ESTOQUES
add constraint FK_GRUPOS_EST_EMPRESA_GRUPO_ID
foreign key(EMPRESA_GRUPO_ID)
references EMPRESAS(EMPRESA_ID);

/* Constraints */

/*
  TIPO
  A - Ato
  R - Retirar
  E - Entregar
*/
alter table GRUPOS_ESTOQUES
add constraint CK_GRUPOS_ESTOQUES_TIPO
check(TIPO in('A', 'R', 'E'));