create table TRANSF_PRODUTOS_EMPRESAS_ITENS(
  TRANSFERENCIA_ID      number(10) not null,
  ITEM_ID               number(10) not null,
  PRODUTO_ID            number(10) not null,
  LOCAL_ORIGEM_ID       number(5) not null,
  LOTE                  varchar2(80) default '???' not null,
  QUANTIDADE            number(20,4) not null,
  TIPO_CONTROLE_ESTOQUE char(1) default 'N' not null,
  
  PRECO_UNITARIO        number(8,2) not null,
  VALOR_TOTAL           number(8,2) not null,  
 
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null   
);

/* Chave primária */
alter table TRANSF_PRODUTOS_EMPRESAS_ITENS
add constraint PK_TRANSF_PRODUTOS_EMP_ITENS
primary key(TRANSFERENCIA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table TRANSF_PRODUTOS_EMPRESAS_ITENS
add constraint FK_TRANSF_PROD_EMP_ITE_TRAN_ID
foreign key(TRANSFERENCIA_ID)
references TRANSF_PRODUTOS_EMPRESAS(TRANSFERENCIA_ID);

alter table TRANSF_PRODUTOS_EMPRESAS_ITENS
add constraint FK_TRANSF_PROD_EMP_ITE_PROD_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table TRANSF_PRODUTOS_EMPRESAS_ITENS
add constraint FK_TRA_PRO_EMP_ITE_LOCAL_OR_ID
foreign key(LOCAL_ORIGEM_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table TRANSF_PRODUTOS_EMPRESAS_ITENS
add constraint FK_TRANS_PRO_EMPR_ITE_LOTE
foreign key(PRODUTO_ID, LOTE)
references PRODUTOS_LOTES(PRODUTO_ID, LOTE);

/* Checagens */
alter table TRANSF_PRODUTOS_EMPRESAS_ITENS
add constraint CK_TRANS_PRO_EMPR_ITE_QUANTID
check(QUANTIDADE > 0);

alter table TRANSF_PRODUTOS_EMPRESAS_ITENS
add constraint CK_TRANS_PRO_EMPR_ITE_PR_UNIT
check(PRECO_UNITARIO > 0);

alter table TRANSF_PRODUTOS_EMPRESAS_ITENS
add constraint CK_TRANS_PRO_EMPR_ITE_VLR_TOT
check(VALOR_TOTAL > 0);