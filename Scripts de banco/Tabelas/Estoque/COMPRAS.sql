﻿create table COMPRAS(
  COMPRA_ID           number(10) not null,
  FORNECEDOR_ID       number(10) not null,
  EMPRESA_ID          number(3) not null,
  COMPRADOR_ID        number(4) not null,
  DATA_FATURAMENTO    date not null,
  PREVISAO_ENTREGA    date not null,
  DATA_HORA_COMPRA    date not null,
  USUARIO_CADASTRO_ID number not null,
  VALOR_LIQUIDO       number(8,2) not null,

  PLANO_FINANCEIRO_ID varchar2(9),
     
  STATUS              char(1) default 'A' not null,  
  DATA_HORA_BAIXA     date,
  USUARIO_BAIXA_ID    number(4),
  
  DATA_HORA_CANCELAMENTO date,
  USUARIO_CANCELAMENTO_ID number(4),
  
  OBSERVACOES            varchar2(800),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table COMPRAS
add constraint PK_COMPRAS
primary key(COMPRA_ID)
using index tablespace INDICES;

/* Chaves estrangerias */
alter table COMPRAS
add constraint FK_COMPRAS_FORNECEDOR_ID
foreign key(FORNECEDOR_ID)
references FORNECEDORES(CADASTRO_ID);

alter table COMPRAS
add constraint FK_COMPRAS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table COMPRAS
add constraint FK_COMPRAS_COMPRADOR_ID
foreign key(COMPRADOR_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table COMPRAS
add constraint FK_COMPRAS_USUARIO_CADASTRO_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table COMPRAS
add constraint FK_COMPRAS_USUARIO_BAIXA_ID
foreign key(USUARIO_BAIXA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table COMPRAS
add constraint FK_COMPRAS_PLANO_FINANCEIRO_ID
foreign key(PLANO_FINANCEIRO_ID)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

/* Checagens */

/* 
  STATUS 
  A - Aberto
  B - Baixado
  C - Cancelado
*/
alter table COMPRAS
add constraint CK_COMPRAS_STATUS
check(
  (STATUS = 'A' and USUARIO_BAIXA_ID is null and DATA_HORA_BAIXA is null)
  or
  (STATUS = 'B' and USUARIO_BAIXA_ID is not null and DATA_HORA_BAIXA is not null)
  or
  (STATUS = 'C' and USUARIO_CANCELAMENTO_ID is not null and DATA_HORA_CANCELAMENTO is not null)
);

alter table COMPRAS
add constraint CK_COMPRAS_VALOR_LIQUIDO
check(VALOR_LIQUIDO > 0);
