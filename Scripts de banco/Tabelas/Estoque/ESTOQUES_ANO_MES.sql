create table ESTOQUES_ANO_MES(
  EMPRESA_ID              number(3) not null,
  PRODUTO_ID              number(10) not null,
  ANO_MES                 number(6) not null,

  ESTOQUE                 number(20,4) default 0 not null, /* Estoque conceitual */

  FISICO                  number(20,4) default 0 not null,
  DISPONIVEL              number(20,4) default 0 not null,
  RESERVADO               number(20,4) default 0 not null,
  BLOQUEADO               number(20,4) default 0 not null,

  COMPRAS_PENDENTES       number(20,4) default 0 not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table ESTOQUES_ANO_MES
add constraint PK_ESTOQUES_ANO_MES
primary key(EMPRESA_ID, PRODUTO_ID, ANO_MES)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table ESTOQUES_ANO_MES
add constraint FK_ESTO_ANO_MES_EMP_PROD_ID
foreign key(EMPRESA_ID, PRODUTO_ID)
references ESTOQUES(EMPRESA_ID, PRODUTO_ID);

/* Constraints */
alter table ESTOQUES_ANO_MES
add constraint CK_ESTOQUES_ANO_MES_ANO_MES
check(ANO_MES > 200501);