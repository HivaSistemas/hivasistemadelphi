﻿create table CONHECIMENTOS_FRETES_ITENS(
  CONHECIMENTO_ID         number(12) not null,  
  ITEM_ID                 number(3) not null, 
  
  FORNECEDOR_NOTA_ID      number(10) not null,  
  NUMERO_NOTA             number(10) not null,
  MODELO_NOTA             varchar2(3) not null,
  SERIE_NOTA              varchar2(5) not null,
  DATA_EMISSAO_NOTA       date,
  PESO_NOTA               number(10,3) default 0 not null,
  QUANTIDADADE_NOTA       number(20,4) not null,
  VALOR_TOTAL_NOTA        number(8,2) not null,
  INDICE                  number(11,10) default 1 not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table CONHECIMENTOS_FRETES_ITENS
add constraint PK_CONHECIMENTOS_FRETES_ITENS
primary key(CONHECIMENTO_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONHECIMENTOS_FRETES_ITENS
add constraint FK_CONHEC_FRETES_ITE_CONHEC_ID
foreign key(CONHECIMENTO_ID)
references CONHECIMENTOS_FRETES(CONHECIMENTO_ID);

alter table CONHECIMENTOS_FRETES_ITENS
add constraint FK_CONHEC_FRE_ITE_FORN_NOTA_ID
foreign key(FORNECEDOR_NOTA_ID)
references FORNECEDORES(CADASTRO_ID);

/* Checagens */
alter table CONHECIMENTOS_FRETES_ITENS
add constraint CK_CONHEC_FRETES_ITENS_ITEM_ID
check(ITEM_ID > 0);

alter table CONHECIMENTOS_FRETES_ITENS
add constraint CK_CONHEC_FRE_ITE_NUMERO_NOTA
check(NUMERO_NOTA > 0);

alter table CONHECIMENTOS_FRETES_ITENS
add constraint CK_CONHEC_FRE_ITE_PESO_NOTA
check(PESO_NOTA >= 0);

alter table CONHECIMENTOS_FRETES_ITENS
add constraint CK_CONHEC_FRE_ITE_QTDE_NOTA
check(QUANTIDADADE_NOTA >= 0);

alter table CONHECIMENTOS_FRETES_ITENS
add constraint CK_CONHEC_FRE_ITE_VAL_TOT_NOTA
check(VALOR_TOTAL_NOTA >= 0);

alter table CONHECIMENTOS_FRETES_ITENS
add constraint CK_CONHEC_FRE_ITENS_INDICE
check(INDICE > 0);

