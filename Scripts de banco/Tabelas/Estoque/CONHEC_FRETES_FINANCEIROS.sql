﻿create table CONHEC_FRETES_FINANCEIROS(
  CONHECIMENTO_ID         number(12) not null,
  ITEM_ID                 number(3) not null,
  COBRANCA_ID       number(3) not null,
  
  NOSSO_NUMERO      varchar2(30),
  CODIGO_BARRAS     varchar2(44),
  DOCUMENTO         varchar2(20) not null,

  DATA_VENCIMENTO   date not null,
  PARCELA           number(3) not null,
  NUMERO_PARCELAS   number(3) not null,
  VALOR             number(8,2) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table CONHEC_FRETES_FINANCEIROS
add constraint PK_CONHEC_FRETES_FINANCEIROS
primary key(CONHECIMENTO_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONHEC_FRETES_FINANCEIROS
add constraint FK_CONHEC_FRETES_FINANC_CON_ID
foreign key(CONHECIMENTO_ID)
references CONHECIMENTOS_FRETES(CONHECIMENTO_ID);

alter table CONHEC_FRETES_FINANCEIROS
add constraint FK_CONHEC_FRETES_FINANC_COB_ID
foreign key(COBRANCA_ID)
references TIPOS_COBRANCA(COBRANCA_ID);

/* Checagens */
alter table CONHEC_FRETES_FINANCEIROS
add constraint CK_CONHEC_FRETES_FINANC_VALOR
check(VALOR > 0);

alter table CONHEC_FRETES_FINANCEIROS
add constraint CK_CONHEC_FRETES_FINANC_PARC
check(PARCELA > 0);

alter table CONHEC_FRETES_FINANCEIROS
add constraint CK_CONHEC_FRETES_FIN_NUM_PARC
check(NUMERO_PARCELAS >= PARCELA);