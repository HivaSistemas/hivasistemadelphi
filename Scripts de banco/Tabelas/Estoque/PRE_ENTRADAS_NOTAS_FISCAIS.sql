﻿create table PRE_ENTRADAS_NOTAS_FISCAIS(
  PRE_ENTRADA_ID                number(10) not null,
  EMPRESA_ID                    number(3) not null,
  CHAVE_ACESSO_NFE              varchar2(44) not null,
  CNPJ                          varchar2(19) not null,
  RAZAO_SOCIAL                  varchar2(120) not null,
  INSCRICAO_ESTADUAL            varchar2(20) not null,
  DATA_HORA_EMISSAO             date not null,
  NUMERO_NOTA                   number(9) not null,
  SERIE_NOTA                    varchar2(3) not null,
  VALOR_TOTAL_NOTA              number(9,2) not null,

  STATUS                        char(3) default 'AMA' not null,
  TIPO_NOTA                     char(1) not null,
  STATUS_NOTA                   char(1) not null,

  ENTRADA_ID                    number(12),

  USUARIO_MANIFESTACAO_ID       number(4),
  DATA_HORA_MANIFESTACAO        date,
      
  USUARIO_DESCONHECIMENTO_ID    number(4),
  DATA_HORA_DESCONHECIMENTO     date,
  
  USUARIO_OPER_NAO_REALIZADA_ID number(4),
  DATA_HORA_OPER_NAO_REALIZADA  date,
  MOTIVO_OPERACAO_NAO_REALIZADA varchar2(255),
  
  USUARIO_CONFIRMACAO_ID        number(4),
  DATA_HORA_CONFIRMACAO         date,
  
  XML_TEXTO                     clob,
  
  /* Logs do sistema */
  USUARIO_SESSAO_ID        number(4) not null,
  DATA_HORA_ALTERACAO      date not null,
  ROTINA_ALTERACAO         varchar2(30) not null,
  ESTACAO_ALTERACAO        varchar2(30) not null
);

/* Chave primaria */
alter table PRE_ENTRADAS_NOTAS_FISCAIS
add constraint PK_PRE_ENTRADAS_NOTAS_FISCAIS
primary key(PRE_ENTRADA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table PRE_ENTRADAS_NOTAS_FISCAIS
add constraint FK_PRE_ENT_NF_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table PRE_ENTRADAS_NOTAS_FISCAIS
add constraint FK_PRE_ENT_NF_ENTRADA_ID
foreign key(ENTRADA_ID)
references ENTRADAS_NOTAS_FISCAIS(ENTRADA_ID);

alter table PRE_ENTRADAS_NOTAS_FISCAIS
add constraint FK_PRE_ENT_NF_USU_MANIFEST_ID
foreign key(USUARIO_MANIFESTACAO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table PRE_ENTRADAS_NOTAS_FISCAIS
add constraint FK_PRE_ENT_NF_USU_DESCONHEC_ID
foreign key(USUARIO_DESCONHECIMENTO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table PRE_ENTRADAS_NOTAS_FISCAIS
add constraint FK_PRE_ENT_NF_USU_OPER_N_RE_ID
foreign key(USUARIO_OPER_NAO_REALIZADA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table PRE_ENTRADAS_NOTAS_FISCAIS
add constraint FK_PRE_ENT_NF_USU_CONFIRM_ID
foreign key(USUARIO_CONFIRMACAO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* Constraints */
/* 
  STATUS
  AMA - Aguardando manifestação
  MAN - Manifestada
  ENT - Entrada realizada
  ONR - Operação não realizada
  DES - Desconhecimento da operação
  CON - Operação concluída
*/
alter table PRE_ENTRADAS_NOTAS_FISCAIS
add constraint CK_PRE_ENT_NF_STATUS
check(
  STATUS = 'AMA' and USUARIO_MANIFESTACAO_ID is null and DATA_HORA_MANIFESTACAO is null
  or
  STATUS = 'MAN' and USUARIO_MANIFESTACAO_ID is not null and DATA_HORA_MANIFESTACAO is not null
  or
  STATUS = 'ENT' and ENTRADA_ID is not null
  or
  STATUS = 'ONR' and USUARIO_OPER_NAO_REALIZADA_ID is not null and DATA_HORA_OPER_NAO_REALIZADA is not null and MOTIVO_OPERACAO_NAO_REALIZADA is not null
  or
  STATUS = 'DES' and USUARIO_DESCONHECIMENTO_ID is not null and DATA_HORA_DESCONHECIMENTO is not null
  or
  STATUS = 'CON' and USUARIO_CONFIRMACAO_ID is not null and DATA_HORA_CONFIRMACAO is not null
);

/* TIPO_NOTA
  C - NFCe
  N - NFe  
*/
alter table PRE_ENTRADAS_NOTAS_FISCAIS
add constraint CK_PRE_ENT_NF_TIPO_NOTA
check(TIPO_NOTA in('C', 'N'));

/* STATUS_NOTA
  1 - Uso autorizado
  2 - Uso denegado
  3 - Cancelada
*/
alter table PRE_ENTRADAS_NOTAS_FISCAIS
add constraint CK_PRE_ENT_NF_STATUS_NOTA
check(STATUS_NOTA in('1', '2', '3'));

/* Uniques */
alter table PRE_ENTRADAS_NOTAS_FISCAIS
add constraint UN_PRE_ENT_NF_CHAVE_ACESSO_NFE
unique(CHAVE_ACESSO_NFE)
using index tablespace INDICES;
