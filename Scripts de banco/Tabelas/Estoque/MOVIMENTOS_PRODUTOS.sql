﻿create table MOVIMENTOS_PRODUTOS(
  MOVIMENTO_ID              number(12),
  PRODUTO_ID                number(10) not null,
  EMPRESA_ID                number(10) not null,
  QUANTIDADE_ANTERIOR       number(20,4) not null,
  QUANTIDADE                number(20,4) not null,
  QUANTIDADE_NOVA           number(20,4) not null,
  DATA_HORA_MOVIMENTO       date not null,
  FUNCIONARIO_MOVIMENTO_ID  number(4) not null,
  TIPO_MOVIMENTO            char(3) not null,
  TIPO_ANALISE_CUSTO        char(1) not null,
  ORCAMENTO_ID              number(10),
  ENTRADA_ID                number(10),
  AJUSTE_ESTOQUE_ID         number(10),
  DEVOLUCAO_ID              number(10)
);

alter table MOVIMENTOS_PRODUTOS
add constraint PK_MOVIMENTOS_PRODUTOS_ID
primary key(MOVIMENTO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table MOVIMENTOS_PRODUTOS
add constraint FK_MOV_PRODUTOS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table MOVIMENTOS_PRODUTOS
add constraint FK_MOV_PRODUTOS_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table MOVIMENTOS_PRODUTOS
add constraint FK_MOV_PRODUTOS_FUNC_MOV_ID
foreign key(FUNCIONARIO_MOVIMENTO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table MOVIMENTOS_PRODUTOS
add constraint FK_MOV_PRODUTOS_ORCAMENTO_ID
foreign key(ORCAMENTO_ID)
references ORCAMENTOS(ORCAMENTO_ID);

alter table MOVIMENTOS_PRODUTOS
add constraint FK_MOV_PRODUTOS_ENTRADA_ID
foreign key(ENTRADA_ID)
references ENTRADAS_NOTAS_FISCAIS(ENTRADA_ID);

alter table MOVIMENTOS_PRODUTOS
add constraint FK_MOV_PROD_AJUSTE_ESTOQUE_ID
foreign key(AJUSTE_ESTOQUE_ID)
references AJUSTES_ESTOQUE(AJUSTE_ESTOQUE_ID);
/* Checagens */

/* 
  TIPO_MOVIMENTO
  ENT - Entrada de notas
  VEN - Vendas
  CAN - Cancelamento de vendas
  AJU - Ajuste de estoque
  DEV - Devolução de venda
  RES - Reserva
  CRE - Cancelamento de reserva
*/
alter table MOVIMENTOS_PRODUTOS
add constraint CK_MOV_PRODUTOS_TIPO_MOVIMENTO
check(TIPO_MOVIMENTO in('ENT', 'VEN','CAN','AJU','DEV','RES','CRE'));

/* TIPO_ANALISE_CUSTO
  A - Fiscal
  B - Real
  C - A + B
*/
alter table MOVIMENTOS_PRODUTOS
add constraint CK_MOV_PROD_TIPO_ANALISE_CUSTO
check(TIPO_ANALISE_CUSTO in('A', 'B', 'C'));

alter table MOVIMENTOS_PRODUTOS
add constraint CK_MOV_PRODUTOS_TIPO_MOV_IDS
check(
  TIPO_MOVIMENTO in('VEN','CAN','RES','CRE') and ORCAMENTO_ID is not null
  or
  TIPO_MOVIMENTO = 'AJU' and AJUSTE_ESTOQUE_ID is not null
  or
  TIPO_MOVIMENTO = 'ENT' and ENTRADA_ID is not null
  or
  TIPO_MOVIMENTO = 'DEV' and DEVOLUCAO_ID is not null
);
