create table TRANSF_PRODUTOS_EMPRESAS(
  TRANSFERENCIA_ID          number(10) not null,
  EMPRESA_ORIGEM_ID         number(3) not null,
  EMPRESA_DESTINO_ID        number(3) not null,  
  DATA_HORA_CADASTRO        date not null,
  
  MOTORISTA_ID              number(10),
  
  USUARIO_CADASTRO_ID       number(4) not null,
  STATUS                    char(1) default 'A' not null,
  
  VALOR_TRANSFERENCIA       number(8,2) not null,
  
  DATA_HORA_BAIXA           date,
  USUARIO_BAIXA_ID          number(4),
  
  DATA_HORA_CANCELAMENTO    date,
  USUARIO_CANCELAMENTO_ID   number(4),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table TRANSF_PRODUTOS_EMPRESAS
add constraint PK_TRANSF_PRODUTOS_EMPRESAS
primary key(TRANSFERENCIA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table TRANSF_PRODUTOS_EMPRESAS
add constraint FK_TRA_PROD_EMP_EMP_ORIGEM_ID
foreign key(EMPRESA_ORIGEM_ID)
references EMPRESAS(EMPRESA_ID);

alter table TRANSF_PRODUTOS_EMPRESAS
add constraint FK_TRA_PROD_EMP_EMP_DEST_ID
foreign key(EMPRESA_DESTINO_ID)
references EMPRESAS(EMPRESA_ID);

alter table TRANSF_PRODUTOS_EMPRESAS
add constraint FK_TRA_PROD_EMP_MOTORISTA_ID
foreign key(MOTORISTA_ID)
references MOTORISTAS(CADASTRO_ID);

alter table TRANSF_PRODUTOS_EMPRESAS
add constraint FK_TRA_PROD_EMP_USUARIO_CAD_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table TRANSF_PRODUTOS_EMPRESAS
add constraint FK_TRA_PROD_EMP_USUARIO_BX_ID
foreign key(USUARIO_BAIXA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* Checagens */

alter table TRANSF_PRODUTOS_EMPRESAS
add constraint CK_TRANSF_PROD_EMP_EMPRESAS
check(EMPRESA_ORIGEM_ID <> EMPRESA_DESTINO_ID);

/* 
  STATUS
  A - Ag. carregamento
  T - Em trânsito
  B - Baixada
  C - Cancelada
*/
alter table TRANSF_PRODUTOS_EMPRESAS
add constraint CK_TRANSF_PROD_EMP_STATUS
check(STATUS in('A', 'T', 'B', 'C'));

alter table TRANSF_PRODUTOS_EMPRESAS
add constraint CK_TRANSF_PROD_EMP_STATUS_USU
check( 
  STATUS in('A', 'T') and USUARIO_BAIXA_ID is null and DATA_HORA_BAIXA is null and USUARIO_CANCELAMENTO_ID is null and DATA_HORA_CANCELAMENTO is null
  or
  STATUS = 'B' and USUARIO_BAIXA_ID is not null and DATA_HORA_BAIXA is not null and USUARIO_CANCELAMENTO_ID is null and DATA_HORA_CANCELAMENTO is null
  or
  STATUS = 'C' and USUARIO_CANCELAMENTO_ID is not null and DATA_HORA_CANCELAMENTO is not null
);