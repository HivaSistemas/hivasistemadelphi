﻿create table CONHECIMENTOS_FRETES(
  CONHECIMENTO_ID         number(12) not null,
  EMPRESA_ID              number(3) not null,
  
  TRANSPORTADORA_ID       number(10) not null,
  NUMERO                  number(10) not null,
  MODELO                  varchar2(3) not null,
  SERIE                   varchar2(5) not null,

  USUARIO_CADASTRO_ID     number(4) not null,
  DATA_HORA_CADASTRO      date not null,
      
  BASE_CALCULO_ICMS       number(8,2) default 0 not null,
  PERCENTUAL_ICMS         number(4,2) default 0 not null,
  VALOR_ICMS              number(8,2) default 0 not null,
  VALOR_FRETE             number(8,2) not null,
      
  CHAVE_CONHECIMENTO      varchar2(54) not null,
  CFOP_ID                 varchar2(7) not null,
  DATA_EMISSAO            date,
  DATA_CONTABIL           date,

  TIPO_RATEIO_CONHECIMENTO char(1) default 'V' not null,
  DATA_PRIMEIRO_VENCIMENTO date not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primária */
alter table CONHECIMENTOS_FRETES
add constraint PK_CONHECIMENTOS_FRETES
primary key(CONHECIMENTO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table CONHECIMENTOS_FRETES
add constraint FK_CONHEC_FRETES_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table CONHECIMENTOS_FRETES
add constraint FK_CONHEC_FRETES_TRANSPORT_ID
foreign key(TRANSPORTADORA_ID)
references TRANSPORTADORAS(CADASTRO_ID);

alter table CONHECIMENTOS_FRETES
add constraint FK_CONHEC_FRETES_CFOP_ID
foreign key(CFOP_ID)
references CFOP(CFOP_PESQUISA_ID);

alter table CONHECIMENTOS_FRETES
add constraint FK_CONHEC_FRETES_CFOP_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* Checagens */
alter table CONHECIMENTOS_FRETES
add constraint CK_CONHE_FRETES_BASE_CALC_ICMS
check(BASE_CALCULO_ICMS >= 0);

alter table CONHECIMENTOS_FRETES
add constraint CK_CONHEC_FRETES_PERCENT_ICMS
check(PERCENTUAL_ICMS >= 0);

alter table CONHECIMENTOS_FRETES
add constraint CK_CONHEC_FRETES_VALOR_ICMS
check(VALOR_ICMS >= 0);

alter table CONHECIMENTOS_FRETES
add constraint CK_CONHEC_FRETES_VALOR_FRETE
check(VALOR_FRETE >= 0);

alter table CONHECIMENTOS_FRETES
add constraint CK_CONHEC_FRETES_DATA_CONTABIL
check(DATA_CONTABIL >= DATA_EMISSAO);

/*
  TIPO_RATEIO_CONHECIMENTO
  Q - Quantidade
  V - Valor
  P - Peso
*/
alter table CONHECIMENTOS_FRETES
add constraint CK_CONHEC_FRET_TP_RAT_CONHEC
check(TIPO_RATEIO_CONHECIMENTO in('Q', 'V', 'P'));