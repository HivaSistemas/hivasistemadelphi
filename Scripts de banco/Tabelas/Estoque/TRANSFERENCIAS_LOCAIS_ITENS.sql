create table TRANSFERENCIAS_LOCAIS_ITENS(
  TRANSFERENCIA_LOCAL_ID         number(10) not null,
  LOCAL_DESTINO_ID               number(5,0) not null,
  ITEM_ID                        number(3) not null,
  LOTE                           varchar2(80) not null,
  
  PRODUTO_ID                     number(10) not null,  
  QUANTIDADE                     number(20,4) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID    number(4) not null,
  DATA_HORA_ALTERACAO  date not null,
  ROTINA_ALTERACAO     varchar2(30) not null,
  ESTACAO_ALTERACAO    varchar2(30) not null  
);

alter table TRANSFERENCIAS_LOCAIS_ITENS
add constraint PK_TRANSFERENCIAS_LOC_ITENS_ID
primary key(TRANSFERENCIA_LOCAL_ID, LOCAL_DESTINO_ID, ITEM_ID, LOTE)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table TRANSFERENCIAS_LOCAIS_ITENS
add constraint FK_TR_LO_ITE_TRANSF_LOC_DES_ID
foreign key(TRANSFERENCIA_LOCAL_ID)
references TRANSFERENCIAS_LOCAIS(TRANSFERENCIA_LOCAL_ID);

alter table TRANSFERENCIAS_LOCAIS_ITENS
add constraint FK_TRANSF_LOC_ITE_LOC_DEST_ID
foreign key(LOCAL_DESTINO_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table TRANSFERENCIAS_LOCAIS_ITENS
add constraint FK_TRANSF_LOC_ITE_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table TRANSFERENCIAS_LOCAIS_ITENS
add constraint FK_TRANSF_LOCAL_ITENS_LOTE
foreign key(PRODUTO_ID, LOTE)
references PRODUTOS_LOTES(PRODUTO_ID, LOTE);