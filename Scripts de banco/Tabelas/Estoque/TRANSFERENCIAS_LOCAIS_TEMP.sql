create table TRANSFERENCIAS_LOCAIS_TEMP(
  TRANSFERENCIA_LOCAL_ID number(10) not null,
  EMPRESA_ID             number(3) not null,
  LOCAL_ORIGEM_ID        number(3) not null,
  STATUS                 char(1)  default 'P' not null,
  OBSERVACAO             varchar2(200),
  DATA_HORA_CADASTRO     date not null,
  USUARIO_CADASTRO_ID    number(4) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table TRANSFERENCIAS_LOCAIS_TEMP
add constraint PK_TRANSF_LOC_TMP_TRANS_LOC_ID
primary key(TRANSFERENCIA_LOCAL_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table TRANSFERENCIAS_LOCAIS_TEMP
add constraint FK_TRANS_LOCAIS_TMP_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table TRANSFERENCIAS_LOCAIS_TEMP
add constraint FK_TRANS_LOC_TMP_LOC_ORIGEM_ID
foreign key(LOCAL_ORIGEM_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table TRANSFERENCIAS_LOCAIS_TEMP
add constraint FK_TRANS_LOC_TMP_USU_CAD_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);