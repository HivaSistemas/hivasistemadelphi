﻿create table ENTRADAS_NOTAS_FISCAIS(
  ENTRADA_ID                  number(12),
  FORNECEDOR_ID               number(10) not null,
  CFOP_ID                     varchar2(7) not null,
  EMPRESA_ID                  number(3) not null,

  NUMERO_NOTA                 number(9),
  MODELO_NOTA                 varchar2(3) not null,
  SERIE_NOTA                  varchar2(3) not null,

  MODALIDADE_FRETE            number(1) default 9 not null,
  TIPO_RATEIO_FRETE           char(1) not null,
  /* Valores da Nota fiscal */
  VALOR_TOTAL                 number(8,2) not null,
  VALOR_TOTAL_PRODUTOS        number(8,2) not null,
  VALOR_DESCONTO              number(8,2) default 0 not null,
  VALOR_OUTRAS_DESPESAS       number(8,2) default 0 not null,
  VALOR_FRETE                 number(8,2) default 0 not null,
  VALOR_OUTRAS_DESPESAS_TERC  number(8,2) default 0 not null,

  BASE_CALCULO_ICMS           number(8,2) not null,
  VALOR_ICMS                  number(8,2) not null,

  BASE_CALCULO_ICMS_ST        number(8,2) not null,
  VALOR_ICMS_ST               number(8,2) not null,

  BASE_CALCULO_PIS            number(8,2) not null,
  VALOR_PIS                   number(8,2) not null,
  BASE_CALCULO_COFINS         number(8,2) not null,

  VALOR_COFINS                number(8,2) not null,
  VALOR_IPI                   number(8,2) default 0 not null,

  PESO_LIQUIDO                number(10,2) not null,
  PESO_BRUTO                  number(10,2) not null,

  VALOR_OUTROS_CUSTOS_FINANC  number(8,2) default 0 not null,

  DATA_HORA_CADASTRO          date default sysdate not null,
  DATA_HORA_EMISSAO           date,
  DATA_ENTRADA                date not null,
  DATA_PRIMEIRA_PARCELA       date not null,

  STATUS                      char(3) default 'ACM',
  ATUALIZAR_CUSTO_ENTRADA     char(1) default 'N' not null,
  ATUALIZAR_CUSTO_COMPRA      char(1) default 'N' not null,

  PLANO_FINANCEIRO_ID         varchar2(9) not null,
  CENTRO_CUSTO_ID             number(3),
  CONHECIMENTO_FRETE_ID       number(12),
  VALOR_CONHECIMENTO_FRETE    number(8,2) default 0 not null,

  ORIGEM_ENTRADA              char(3) not null,
  NOTA_TRANSF_PROD_ORIGEM_ID  number(12),

  /* Nota fiscal eletrônica */
  CHAVE_NFE                   varchar2(54),

  USUARIO_CADASTRO_ID         number(4) not null,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint PK_ENT_NOTAS_FISC_ENTRADA_ID
primary key(ENTRADA_ID)
using index tablespace INDICES;

/* Foreign keys */
alter table ENTRADAS_NOTAS_FISCAIS
add constraint FK_ENT_NOTAS_FISC_FORNEC_ID
foreign key(FORNECEDOR_ID)
references FORNECEDORES(CADASTRO_ID);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint FK_ENT_NOTAS_FISC_CFOP_ID
foreign key(CFOP_ID)
references CFOP(CFOP_PESQUISA_ID);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint FK_ENT_NOTAS_FISC_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint FK_ENT_NOTAS_FISC_USU_CAD_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint FK_ENT_NOTAS_PLAN_FIN_ID
foreign key(PLANO_FINANCEIRO_ID)
references PLANOS_FINANCEIROS(PLANO_FINANCEIRO_ID);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint FK_ENT_NOTAS_CENTRO_CUS_ID
foreign key(CENTRO_CUSTO_ID)
references CENTROS_CUSTOS(CENTRO_CUSTO_ID);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint FK_ENT_NOTAS_CONHEC_FRETE_ID
foreign key(CONHECIMENTO_FRETE_ID)
references CONHECIMENTOS_FRETES(CONHECIMENTO_ID);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint FK_ENT_NF_TRANS_PR_ORIG_ID
foreign key(NOTA_TRANSF_PROD_ORIGEM_ID)
references NOTAS_FISCAIS(NOTA_FISCAL_ID);

/* Constraints */
alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_NUMERO_NOTA
check(NUMERO_NOTA > 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_MODELO_NOTA
check(MODELO_NOTA in('1','55'));

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_VALORES_TOT
check(
  VALOR_TOTAL > 0 and
  VALOR_TOTAL_PRODUTOS > 0
);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_VLR_CON_FRE
check(
  (VALOR_CONHECIMENTO_FRETE = 0 and CONHECIMENTO_FRETE_ID is null)
  or
  (VALOR_CONHECIMENTO_FRETE > 0 and CONHECIMENTO_FRETE_ID is not null)
);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_VALOR_DESC
check(VALOR_DESCONTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NF_VALOR_OUTRAS_DESP
check(VALOR_OUTRAS_DESPESAS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_VALOR_FRETE
check(VALOR_FRETE >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NF_VALOR_OUTR_DESP_TERC
check(VALOR_OUTRAS_DESPESAS_TERC >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FIS_BASE_CAL_ICMS
check(BASE_CALCULO_ICMS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_VALOR_ICMS
check(VALOR_ICMS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NF_BASE_CALC_ICMS_ST
check(BASE_CALCULO_ICMS_ST >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FIS_VALOR_ICMS_ST
check(VALOR_ICMS_ST >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FIS_BASE_CALC_PIS
check(BASE_CALCULO_PIS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_VALOR_PIS
check(VALOR_PIS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NF_BASE_CALCULO_COFINS
check(BASE_CALCULO_COFINS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_VALOR_COFINS
check(VALOR_COFINS >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_VALOR_IPI
check(VALOR_IPI >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_PESO_LIQUIDO
check(PESO_LIQUIDO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_PESO_BRUTO
check(PESO_BRUTO >= 0);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_CHAVE_NFE
check(
  MODELO_NOTA = '55' and length(CHAVE_NFE) = 54
  or
  MODELO_NOTA <> '55' and CHAVE_NFE is null
);

/* MODALIDADE_FRETE
  0 - Por conta do emitente
  1 - Por conta do destinatário/remetente
  2 - Por conta de terceiros
  9 - Sem frete
*/
alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FIS_MODALID_FRETE
check(MODALIDADE_FRETE in(0,1,2,9));

/* TIPO_RATEIO_FRETE
  V - Por valor
  P - Por peso
  Q - Quantidade
  M - Manual
*/
alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FIS_TP_RATE_FRETE
check(TIPO_RATEIO_FRETE in('V','P','Q','M'));

/* STATUS
  ACM - Aguardando chegada de mercadoria
  ACE - Aguardando consolidação de estoque
  ECO - Estoque consolidado
  FCO - Financeiro consolidado
  BAI - Baixada
*/
alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_CONS_ESTOQUE
check(STATUS in('ACM','ACE','ECO','FCO','BAI'));

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_ATU_CUS_ENTR
check(ATUALIZAR_CUSTO_ENTRADA in('S','N'));

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_ATU_CUS_COMP
check(ATUALIZAR_CUSTO_COMPRA in('S','N'));

/*
  ORIGEM_ENTRADA
  XML - Carregou um XML
  MAN - Lançada manualmente
  TPE - Transferência de produtos entre empresas
  TFP - Transferência fiscal de produtos
*/
alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENT_NOTAS_FISC_ORIGEM_ENT
check(
  ORIGEM_ENTRADA in('XML', 'MAN') and NOTA_TRANSF_PROD_ORIGEM_ID is null
  or
  ORIGEM_ENTRADA in('TPE') and NOTA_TRANSF_PROD_ORIGEM_ID is not null
  or
  ORIGEM_ENTRADA in('TFP') and NOTA_TRANSF_PROD_ORIGEM_ID is not null
);

alter table ENTRADAS_NOTAS_FISCAIS
add constraint CK_ENTR_NF_VALOR_OUT_CUS_FIN
check(VALOR_OUTROS_CUSTOS_FINANC >= 0);
