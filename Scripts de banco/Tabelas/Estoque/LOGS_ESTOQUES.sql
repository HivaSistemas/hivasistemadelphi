/* 
  Esta tabela guarda apenas os logs das colunas que controlam o estoque.
*/
create table LOGS_ESTOQUES(
  TIPO_ALTERACAO_LOG_ID   number(3) not null,
  EMPRESA_ID              number(4) not null,
  PRODUTO_ID              number(10) not null,
  VALOR_ANTERIOR          varchar2(60),
  NOVO_VALOR              varchar2(60),
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave estrangeira */
alter table LOGS_ESTOQUES
add constraint FK_LOGS_ESTOQUES_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table LOGS_ESTOQUES
add constraint FK_LOGS_ESTOQUES_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* 
  TIPO_ALTERACAO_LOG_ID
  1 - Disponivel
  2 - Físico
  3 - Reservado
  4 - Fiscal
*/