create table COMPRAS_BAIXAS(
  BAIXA_ID              number(10) not null,
  COMPRA_ID             number(10) not null,
  
  USUARIO_BAIXA_ID      number(4) not null,  
  DATA_HORA_BAIXA       date not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null  
);

/* Chave primaria */
alter table COMPRAS_BAIXAS
add constraint PK_COMPRAS_BAIXAS
primary key(BAIXA_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table COMPRAS_BAIXAS
add constraint FK_COMPRAS_BAIXAS_COMPRA_ID
foreign key(COMPRA_ID)
references COMPRAS(COMPRA_ID);

alter table COMPRAS_BAIXAS
add constraint FK_COMPRAS_BAIXAS_USU_BAIXA_ID
foreign key(USUARIO_BAIXA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);