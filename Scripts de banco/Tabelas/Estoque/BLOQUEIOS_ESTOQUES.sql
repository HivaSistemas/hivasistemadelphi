create table BLOQUEIOS_ESTOQUES(
  BLOQUEIO_ID         number(12) not null,
  EMPRESA_ID          number(3) not null,
  TIPO_BLOQUEIO_ID    number(3)
  
  USUARIO_CADASTRO_ID number(4) not null,
  DATA_HORA_CADASTRO  date not null,
  
  STATUS              char(1) default 'A' not null,
  OBSERVACOES         varchar2(800),
  
  USUARIO_BAIXA_ID    number(4),
  DATA_HORA_BAIXA     date,

  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primaria */
alter table BLOQUEIOS_ESTOQUES
add constraint PK_BLOQUEIOS_ESTOQUES
primary key(BLOQUEIO_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table BLOQUEIOS_ESTOQUES
add constraint FK_BLOQ_ESTOQUES_EMPRESA_ID
foreign key(EMPRESA_ID)
references EMPRESAS(EMPRESA_ID);

alter table BLOQUEIOS_ESTOQUES
add constraint FK_TIPO_BLO_EST_ID_ID
foreign key(TIPO_BLOQUEIO_ID)
references TIPO_BLOQUEIO_ESTOQUE(TIPO_BLOQUEIO_ID);

alter table BLOQUEIOS_ESTOQUES
add constraint FK_BLOQ_EST_USU_CADASTRO_ID
foreign key(USUARIO_CADASTRO_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

alter table BLOQUEIOS_ESTOQUES
add constraint FK_BLOQ_EST_USU_BAIXA_ID
foreign key(USUARIO_BAIXA_ID)
references FUNCIONARIOS(FUNCIONARIO_ID);

/* Checagens */
/* 
  STATUS
  A - Aberto
  B - Baixado
*/
alter table BLOQUEIOS_ESTOQUES
add constraint CK_BLOQ_ESTOQUES_STATUS
check(
  STATUS = 'A' and USUARIO_BAIXA_ID is null and DATA_HORA_BAIXA is null
  or
  STATUS = 'B' and USUARIO_BAIXA_ID is not null and DATA_HORA_BAIXA is not null
);