create table BLOQUEIOS_EST_BAIXAS_ITENS(
  BAIXA_ID            number(12) not null,  
  ITEM_ID             number(3) not null,
  QUANTIDADE          number(20,4) not null,
  
  /* Colunas de logs do sistema */
  USUARIO_SESSAO_ID       number(4) not null,
  DATA_HORA_ALTERACAO     date not null,
  ROTINA_ALTERACAO        varchar2(30) not null,
  ESTACAO_ALTERACAO       varchar2(30) not null
);

/* Chave primária */
alter table BLOQUEIOS_EST_BAIXAS_ITENS
add constraint PK_BLOQUEIOS_EST_BAIXAS_ITENS
primary key(BAIXA_ID, ITEM_ID)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table BLOQUEIOS_EST_BAIXAS_ITENS
add constraint FK_BLOQ_EST_BAIXAS_BAIXA_ID
foreign key(BAIXA_ID)
references BLOQUEIOS_ESTOQUES_BAIXAS(BAIXA_ID);

/* Checagens */
alter table BLOQUEIOS_EST_BAIXAS_ITENS
add constraint CK_BLOQ_EST_BAIXAS_QUANTIDADE
check(QUANTIDADE > 0);