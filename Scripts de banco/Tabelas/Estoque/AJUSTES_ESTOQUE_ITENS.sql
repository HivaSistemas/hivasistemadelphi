create table AJUSTES_ESTOQUE_ITENS(
  AJUSTE_ESTOQUE_ID    number(10) not null,
  LOCAL_ID             number(5,0) not null,
  ITEM_ID              number(10) not null,

  LOTE                 varchar2(80) default '???' not null,
  DATA_FABRICACAO      date,
  DATA_VENCIMENTO      date,

  PRODUTO_ID           number(10) not null,
  NATUREZA             char(1) not null,
  QUANTIDADE           number(20,4) not null,
  ESTOQUE_INFORMADO    number(20,4),
  PRECO_UNITARIO       number(9,3) not null,
  VALOR_TOTAL          number(8,2) not null
);

alter table AJUSTES_ESTOQUE_ITENS
add constraint PK_AJUSTES_ESTOQUE_ITENS_ID
primary key(AJUSTE_ESTOQUE_ID, LOCAL_ID, ITEM_ID, LOTE)
using index tablespace INDICES;

/* Chaves estrangeiras */
alter table AJUSTES_ESTOQUE_ITENS
add constraint FK_AJUS_EST_ITENS_AJUS_EST_ID
foreign key(AJUSTE_ESTOQUE_ID)
references AJUSTES_ESTOQUE(AJUSTE_ESTOQUE_ID);

alter table AJUSTES_ESTOQUE_ITENS
add constraint FK_AJUS_EST_ITENS_AJUS_LOC_ID
foreign key(LOCAL_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table AJUSTES_ESTOQUE_ITENS
add constraint FK_AJUS_EST_ITENS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

alter table AJUSTES_ESTOQUE_ITENS
add constraint FK_AJUS_EST_ITENS_LOTE
foreign key(PRODUTO_ID, LOTE)
references PRODUTOS_LOTES(PRODUTO_ID, LOTE);

/* Constraints */
alter table AJUSTES_ESTOQUE_ITENS
add constraint CK_AJU_EST_IT_NATUREZA
check(NATUREZA in ('S', 'E'));

alter table AJUSTES_ESTOQUE_ITENS
add constraint CK_AJU_EST_IT_QUANTIDADE
check(QUANTIDADE > 0);

alter table AJUSTES_ESTOQUE_ITENS
add constraint CK_AJU_EST_ITENS_PRECO_UNIT
check(PRECO_UNITARIO > 0);

alter table AJUSTES_ESTOQUE_ITENS
add constraint CK_AJU_EST_ITENS_VALOR_TOT
check(VALOR_TOTAL > 0);
