create table PRODUCAO_ESTOQUE_ITENS(
  PRODUCAO_ESTOQUE_ID  number(10) not null,
  LOCAL_ID             number(5,0) not null,
  PRODUTO_ID           number(10) not null,
  NATUREZA             char(1) not null,
  QUANTIDADE           number(20,4) not null,
  PRECO_UNITARIO       number(9,3),
  VALOR_TOTAL          number(8,2)
);

/* Chaves estrangeiras */
alter table PRODUCAO_ESTOQUE_ITENS
add constraint FK_PROD_EST_ITENS_PROD_EST_ID
foreign key(PRODUCAO_ESTOQUE_ID)
references PRODUCAO_ESTOQUE(PRODUCAO_ESTOQUE_ID);

alter table PRODUCAO_ESTOQUE_ITENS
add constraint FK_PROD_EST_ITENS_PROD_LOC_ID
foreign key(LOCAL_ID)
references LOCAIS_PRODUTOS(LOCAL_ID);

alter table PRODUCAO_ESTOQUE_ITENS
add constraint FK_PROD_EST_ITENS_PRODUTO_ID
foreign key(PRODUTO_ID)
references PRODUTOS(PRODUTO_ID);

/* Constraints */
alter table PRODUCAO_ESTOQUE_ITENS
add constraint CK_PRO_EST_IT_NATUREZA
check(NATUREZA in ('S', 'E'));

alter table PRODUCAO_ESTOQUE_ITENS
add constraint CK_PRO_EST_IT_QUANTIDADE
check(QUANTIDADE > 0);
